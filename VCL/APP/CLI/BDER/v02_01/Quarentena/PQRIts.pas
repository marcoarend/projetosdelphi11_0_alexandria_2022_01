unit PQRIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.DBGrids, Vcl.Menus,
  BlueDermConsts, UnDmkProcFunc;

type
  TFmPQRIts = class(TForm)
    PnDados: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrPQ1: TmySQLQuery;
    QrPQ1NOMEPQ: TWideStringField;
    QrPQ1Controle: TIntegerField;
    QrPQ1IQ: TIntegerField;
    QrPQ1PQ: TIntegerField;
    QrPQ1CI: TIntegerField;
    QrPQ1Prazo: TWideStringField;
    QrPQ1Lk: TIntegerField;
    QrPQ1DataCad: TDateField;
    QrPQ1DataAlt: TDateField;
    QrPQ1UserCad: TIntegerField;
    QrPQ1UserAlt: TIntegerField;
    DsPQ1: TDataSource;
    QrPQRBxa: TmySQLQuery;
    DsPQRBxa: TDataSource;
    QrPQRInn: TmySQLQuery;
    DsPQRInn: TDataSource;
    QrPQRInnDataX: TDateField;
    QrPQRInnAbertoKg: TFloatField;
    QrPQRInnPRECO: TFloatField;
    QrPQRInnNF_CC: TIntegerField;
    QrPQRBxaDataX: TDateField;
    QrPQRBxaAbertoKg: TFloatField;
    QrPQRBxaPRECO: TFloatField;
    QrPQRBxaOrigemCodi: TIntegerField;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TDBEdit;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    EdProduto: TdmkEditCB;
    CBProduto: TdmkDBLookupComboBox;
    QrSumInn: TmySQLQuery;
    QrSumBxa: TmySQLQuery;
    DsSumBxa: TDataSource;
    DsSumInn: TDataSource;
    QrSumInnAbertoKg: TFloatField;
    QrSumInnPRECO: TFloatField;
    QrSumBxaAbertoKg: TFloatField;
    QrSumBxaPRECO: TFloatField;
    BtLimpeza: TBitBtn;
    PMLimpeza: TPopupMenu;
    ReduzirsaldodaNFdecobertura1: TMenuItem;
    QrPQRBxaOrigemCtrl: TIntegerField;
    QrPQRBxaTipo: TIntegerField;
    QrPQRInnOrigemCtrl: TIntegerField;
    QrPQRInnTipo: TIntegerField;
    QrPQRInnOrigemCodi: TIntegerField;
    NFdecobertura1: TMenuItem;
    BaixanoEstoque1: TMenuItem;
    ReduzirSaldonasBaixas1: TMenuItem;
    Panel3: TPanel;
    GroupBox4: TGroupBox;
    DBGPQRBxa: TDBGrid;
    Panel6: TPanel;
    Label2: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Panel5: TPanel;
    GroupBox3: TGroupBox;
    DBGPQRInn: TDBGrid;
    Panel7: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Splitter1: TSplitter;
    GroupBox5: TGroupBox;
    DBGrid1: TDBGrid;
    QrRedInn: TmySQLQuery;
    DsRedInn: TDataSource;
    QrRedInnOrigemCtrl: TIntegerField;
    QrRedInnPeso: TFloatField;
    QrRedBxa: TmySQLQuery;
    DsRedBxa: TDataSource;
    GroupBox6: TGroupBox;
    DBGrid3: TDBGrid;
    QrRedBxaOrigemCtrl: TIntegerField;
    QrRedBxaPeso: TFloatField;
    Splitter2: TSplitter;
    Excluirreduo1: TMenuItem;
    Excluirreduo2: TMenuItem;
    QrIts: TmySQLQuery;
    QrItsOriCodiInn: TIntegerField;
    QrItsOriCtrlInn: TIntegerField;
    QrItsTipoInn: TIntegerField;
    QrItsOriCodiOut: TIntegerField;
    QrItsOriCtrlOut: TIntegerField;
    QrItsTipoOut: TIntegerField;
    QrRedInnOrigemCodi: TIntegerField;
    QrRedInnTipo: TIntegerField;
    QrItsControle: TIntegerField;
    N1: TMenuItem;
    Aumentarsaldoinicial1: TMenuItem;
    Excluiraumento1: TMenuItem;
    QrPQRInnRetQtd: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdProdutoChange(Sender: TObject);
    procedure BtLimpezaClick(Sender: TObject);
    procedure ReduzirsaldodaNFdecobertura1Click(Sender: TObject);
    procedure PMLimpezaPopup(Sender: TObject);
    procedure ReduzirSaldonasBaixas1Click(Sender: TObject);
    procedure EdProdutoEnter(Sender: TObject);
    procedure EdProdutoExit(Sender: TObject);
    procedure Excluirreduo1Click(Sender: TObject);
    procedure Excluirreduo2Click(Sender: TObject);
    procedure Aumentarsaldoinicial1Click(Sender: TObject);
    procedure Excluiraumento1Click(Sender: TObject);
  private
    { Private declarations }
    FAtuPrd, FProduto: Integer;
    //
    procedure ReopenPQRIts(Controle: Integer);
    procedure ReopenDadosProdto();
    procedure ReopenPQRBxa();
    procedure ReopenPQRInn();
    procedure ReopenInn(Tipo, OrigemCodi, OrigemCtrl: Integer);
    procedure ReopenBxa(Tipo, OrigemCodi, OrigemCtrl: Integer);

  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    FData: TDateTime;
    FCodigo: Integer;
    FCliente: Integer;
    //
    FLastInsumo, FLastItemNF, FLastPQRIts: Integer;
    procedure ReopenPQ(Todos: Boolean);
  end;

  var
  FmPQRIts: TFmPQRIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
GetValor, PQRInnPlus;

{$R *.DFM}

procedure TFmPQRIts.Aumentarsaldoinicial1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPQRInnPlus, FmPQRInnPlus, afmoNegarComAviso) then
  begin
(*
    FmPQRInnPlus.ImgTipo.SQLType := SQLType;
    FmPQRInnPlus.FQrCab := QrPQRCab;
    FmPQRInnPlus.FDsCab := DsPQRCab;
    FmPQRInnPlus.FQrIts := QrPQRIts;*)
    FmPQRInnPlus.FCliInt := FCliente;
    FmPQRInnPlus.FInsumo := FProduto ;
(*    FmPQRInnPlus.FData    := QrPQRCabData.Value;
    FmPQRInnPlus.FCodigo  := QrPQRCabCodigo.Value;
    FmPQRInnPlus.ReopenPQ(False);*)
    //
    FmPQRInnPlus.ShowModal;
    ReopenPQRInn();
    FmPQRInnPlus.Destroy;
    //
  end;
end;

procedure TFmPQRIts.BtLimpezaClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMLimpeza, BtLimpeza);
end;

procedure TFmPQRIts.BtOKClick(Sender: TObject);
const
  ValTitle = 'Retorno de Insumo';
  ValCaption = 'Informe a quantidade a retornar:';
  WidthCaption = Length(ValCaption) * 7;
  Ocorrencia = VAR_PQR_OCORR_EMI_RET;
var
  Insumo: Integer;
  AllQtd, Peso, Valor, RetQtd, ValMax, ValInn, ValBxa: Double;
  //
  ValVar: Variant;
  ValorMin, ValorMax: String;
  OriCodiInn, OriCtrlInn, TipoInn, OriCodiOut, OriCtrlOut, TipoOut,
  Controle: Integer;
begin
  if (QrSumInnAbertoKg.Value > 0.001) and
     (QrSumBxaAbertoKg.Value > 0.001) then
  begin
    QrPQRBxa.First;
    DBGPQRBxa.Enabled := False;
    QrPQRBxa.DisableControls;
    //
    QrPQRInn.First;
    DBGPQRInn.Enabled := False;
    QrPQRInn.DisableControls;
    //
    try
      ValMax := QrSumBxaAbertoKg.Value;
      if QrSumInnAbertoKg.Value < ValMax then
        ValMax := QrSumInnAbertoKg.Value ;
      //
      ValorMin := Geral.FFT(0.001, 3, siNegativo);
      ValorMax := Geral.FFT(ValMax, 3, siNegativo);
      Peso := 0;
      if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, Peso, 3, 0,
      ValorMin, ValorMax, True, ValTitle, ValCaption, WidthCaption, ValVar) then
      begin
        AllQtd := Geral.DMV(ValVar);
        ValInn := QrPQRInnAbertoKg.Value;
        ValBxa := QrPQRBxaAbertoKg.Value;
        Insumo := FProduto;
        while AllQtd >= 0.001 do
        begin
          //
          Peso  := dmkPF.MenorFloat([AllQtd, ValInn, ValBxa]);
          Valor := Peso * QrPQRInnPRECO.Value;
          //
          OriCodiInn := QrPQRInnOrigemCodi.Value;
          OriCtrlInn := QrPQRInnOrigemCtrl.Value;
          TipoInn    := QrPQRInnTipo.Value;
          OriCodiOut := QrPQRBxaOrigemCodi.Value;
          OriCtrlOut := QrPQRBxaOrigemCtrl.Value;
          TipoOut    := QrPQRBxaTipo.Value;
          //
          Controle := Dmod.PQR_InsereItemPQRIts(FCodigo,
            OriCodiInn, OriCtrlInn, TipoInn,
            OriCodiOut, OriCtrlOut, TipoOut,
            Insumo, Peso, Valor, Ocorrencia);
          //
          AllQtd := AllQtd - Peso;
          ValInn := ValInn - Peso;
          ValBxa := ValBxa - Peso;
          //
          if ValInn < 0.001 then
          begin
            QrPQRInn.Next;
            //
            ValInn := QrPQRInnAbertoKg.Value;
          end;
          //
          if ValBxa < 0.001 then
          begin
            QrPQRBxa.Next;
            //
            ValBxa := QrPQRBxaAbertoKg.Value;
          end;
        end;
        ReopenPQRIts(Controle);
        ReopenDadosProdto();
        FLastInsumo  := Insumo;
        FLastItemNF  := OriCodiInn;
        FLastPQRIts := Controle;
      end;
    finally
      QrPQRBxa.EnableControls;
      DBGPQRBxa.Enabled := True;
      QrPQRInn.EnableControls;
      DBGPQRInn.Enabled := True;
    end;
  end;
end;

procedure TFmPQRIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQRIts.EdProdutoChange(Sender: TObject);
begin
  FProduto := EdProduto.ValueVariant;
  PnDados.Visible := False;
  QrPQRBxa.Close;
  QrPQRInn.Close;
  if not EdProduto.Focused then
    ReopenDadosProdto();
end;

procedure TFmPQRIts.EdProdutoEnter(Sender: TObject);
begin
  FAtuPrd := FProduto;
end;

procedure TFmPQRIts.EdProdutoExit(Sender: TObject);
begin
  if FAtuPrd <> FProduto then
    ReopenDadosProdto();
end;

procedure TFmPQRIts.Excluiraumento1Click(Sender: TObject);
var
  Tipo, OrigemCodi, OrigemCtrl: Integer;
begin
  if QrPQRInnRetQtd.Value > - 0.001 then
  begin
    if Geral.MB_Pergunta(
    'Confirma a exclus�o da item de aumento de saldo atual?') = ID_YES then
    begin
      Tipo       := QrPQRInnTipo.Value;
      OrigemCodi := QrPQRInnOrigemCodi.Value;
      OrigemCtrl := QrPQRInnOrigemCtrl.Value;
      //
      if UMyMod.ExcluiRegistroIntArr('', 'pqx', [
      'Tipo', 'OrigemCodi', 'OrigemCtrl'
      ], [
      Tipo, OrigemCodi, OrigemCtrl
      ]) = ID_YES then
        ReopenPQRInn();
    end;
  end else
    Geral.MB_Aviso('Exclus�o cancelada! Item j� possui retorno!');
end;

procedure TFmPQRIts.Excluirreduo1Click(Sender: TObject);
var
  Tipo, OrigemCodi, OrigemCtrl, Controle,
  OriCodi, OriCtrl: Integer;
begin
  Tipo       := VAR_FATID__003;
  OrigemCodi := FCodigo;
  OrigemCtrl := QrRedInnOrigemCtrl.Value;
  //
  ReopenInn(Tipo, OrigemCodi, OrigemCtrl);
  Controle := QrItsControle.Value;
  //
  if UMyMod.ExcluiRegistroIntArr(
  'Confirma a exclus�o do item de redu��o do saldo da NF de cobertura?',
  'pqrits', ['Controle'], [Controle]) = ID_YES then
  begin
    UMyMod.ExcluiRegistroIntArr('', 'pqx', [
    'Tipo', 'OrigemCodi', 'OrigemCtrl'
    ], [
    Tipo, OrigemCodi, OrigemCtrl
    ]);
    //
    OriCodi := QrItsOriCodiInn.Value;
    OriCtrl := QrItsOriCtrlInn.Value;
    Tipo    := QrItsTipoInn.Value;
    //
    Dmod.PQR_AtualizaRetQtdInn(OriCodi, OriCtrl, Tipo);
    //
    ReopenPQRInn();
  end;
end;

procedure TFmPQRIts.Excluirreduo2Click(Sender: TObject);
var
  Tipo, OrigemCodi, OrigemCtrl, Controle,
  OriCodi, OriCtrl: Integer;
begin
  Tipo       := VAR_FATID__004;
  OrigemCodi := FCodigo;
  OrigemCtrl := QrRedBxaOrigemCtrl.Value;
  //
  ReopenBxa(Tipo, OrigemCodi, OrigemCtrl);
  Controle := QrItsControle.Value;
  //
  if UMyMod.ExcluiRegistroIntArr(
  'Confirma a exclus�o do item de redu��o do saldo da NF de cobertura?',
  'pqrits', ['Controle'], [Controle]) = ID_YES then
  begin
    UMyMod.ExcluiRegistroIntArr('', 'pqx', [
    'Tipo', 'OrigemCodi', 'OrigemCtrl'
    ], [
    Tipo, OrigemCodi, OrigemCtrl
    ]);
    //
    OriCodi := QrItsOriCodiOut.Value;
    OriCtrl := QrItsOriCtrlOut.Value;
    Tipo    := QrItsTipoOut.Value;
    //
    Dmod.PQR_AtualizaRetQtdOut(OriCodi, OriCtrl, Tipo);
    //
    ReopenPQRBxa();
  end;
end;

procedure TFmPQRIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmPQRIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FProduto := 0;
  FLastInsumo  := 0;
  FLastItemNF  := 0;
  FLastPQRIts  := 0;
  //
  CBProduto.LocF7SQLText.Text := Geral.ATS([
  'SELECT pqf.PQ _Codigo, pq_.Nome _Nome ',
  'FROM pqfor pqf ',
  'LEFT JOIN pq pq_ ON pq_.Codigo=pqf.PQ ',
  'WHERE pq_.Nome LIKE "%$#%"',
  '']);
end;

procedure TFmPQRIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQRIts.PMLimpezaPopup(Sender: TObject);
begin
  ReduzirsaldodaNFdecobertura1.Enabled :=
    (QrPQRInn.State <> dsInactive) and
    (QrPQRInnAbertoKg.Value > 0);
  //
  Excluirreduo1.Enabled :=
    (QrRedInn.State <> dsInactive) and
    (QrRedInn.RecordCount > 0);
  //
  Excluirreduo2.Enabled :=
    (QrRedBxa.State <> dsInactive) and
    (QrRedBxa.RecordCount > 0);
  //
  Excluiraumento1.Enabled :=
    (QrPQRInn.State <> dsInactive) and
    (QrPQRInnOrigemCodi.Value = 0);
end;

procedure TFmPQRIts.ReduzirsaldodaNFdecobertura1Click(Sender: TObject);
const
  ValTitle = 'NF de cobertura';
  ValCaption = 'Informe a quantidade a reduzir:';
  WidthCaption = Length(ValCaption) * 7;
  Ocorrencia = VAR_PQR_OCORR_RED_SDO;
var
  DataX: String;
  OrigemCodi, OrigemCtrl, Tipo, CliOrig, CliDest, Insumo, Retorno, StqMovIts: Integer;
  Peso, Valor, RetQtd: Double;
  //
  ValVar: Variant;
  ValorMin, ValorMax: String;
  OriCodiInn, OriCtrlInn, TipoInn, OriCodiOut, OriCtrlOut, TipoOut,
  Controle: Integer;
begin
  if QrPQRInnAbertoKg.Value > 0 then
  begin
    ValorMin := Geral.FFT(0.001, 3, siNegativo);
    ValorMax := Geral.FFT(QrPQRInnAbertoKg.Value, 3, siNegativo);
    Peso := 0;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, Peso, 3, 0,
    ValorMin, ValorMax, True, ValTitle, ValCaption, WidthCaption, ValVar) then
    begin
      DataX          := Geral.FDT(FData, 1);
      OrigemCodi     := FCodigo;
      Tipo           := VAR_FATID__003;
      CliOrig        := FCliente;
      CliDest        := FCliente;
      Insumo         := FProduto;
      Peso           := - Geral.DMV(ValVar);
      Valor          := Peso * QrPQRInnPRECO.Value;
  (*    Retorno        := ;
      StqMovIts      := ;
  *)
      RetQtd         := -Peso;
      //
      OrigemCtrl     := UMyMod.BPGS1I32('pqx', 'OrigemCtrl',
                        'Tipo='+ Geral.FF0(Tipo), '', tsPos, stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqx', False, [
      'DataX', 'CliOrig', 'CliDest',
      'Insumo', 'Peso', 'Valor',
      (*'Retorno', 'StqMovIts',*) 'RetQtd'], [
      'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
      DataX, CliOrig, CliDest,
      Insumo, Peso, Valor,
      (*Retorno, StqMovIts,*) RetQtd], [
      OrigemCodi, OrigemCtrl, Tipo], False) then
      begin
        Peso  := - Peso;
        Valor := - Valor;
        //
        OriCodiInn := QrPQRInnOrigemCodi.Value;
        OriCtrlInn := QrPQRInnOrigemCtrl.Value;
        TipoInn    := QrPQRInnTipo.Value;
        OriCodiOut := OrigemCodi; //QrPQRBxaOrigemCodi.Value; // Nao computar
        OriCtrlOut := OrigemCtrl; //QrPQRBxaOrigemCtrl.Value; // saldo inicial
        TipoOut    := Tipo; //QrPQRBxaTipo.Value;       // de NFe de entrada!
        Controle := Dmod.PQR_InsereItemPQRIts(FCodigo,
          OriCodiInn, OriCtrlInn, TipoInn,
          OriCodiOut, OriCtrlOut, TipoOut,
          Insumo, Peso, Valor, Ocorrencia);
        //
        ReopenPQRIts(Controle);
        //
        ReopenDadosProdto();
      end;
    end;
  end;
end;

procedure TFmPQRIts.ReduzirSaldonasBaixas1Click(Sender: TObject);
const
  ValTitle = 'Baixas';
  ValCaption = 'Informe a quantidade a reduzir:';
  WidthCaption = Length(ValCaption) * 7;
  Ocorrencia = VAR_PQR_OCORR_RED_SDO;
var
  DataX: String;
  OrigemCodi, OrigemCtrl, Tipo, CliOrig, CliDest, Insumo, Retorno, StqMovIts: Integer;
  AllQtd, Peso, Valor, RetQtd: Double;
  //
  ValVar: Variant;
  ValorMin, ValorMax: String;
  OriCodiInn, OriCtrlInn, TipoInn, OriCodiOut, OriCtrlOut, TipoOut,
  Controle: Integer;
begin
  if QrSumBxaAbertoKg.Value > 0 then
  begin
    QrPQRBxa.First;
    DBGPQRBxa.Enabled := False;
    QrPQRBxa.DisableControls;
    try
      ValorMin := Geral.FFT(0.001, 3, siNegativo);
      ValorMax := Geral.FFT(QrSumBxaAbertoKg.Value, 3, siNegativo);
      Peso := 0;
      if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, Peso, 3, 0,
      ValorMin, ValorMax, True, ValTitle, ValCaption, WidthCaption, ValVar) then
      begin
        DataX          := Geral.FDT(FData, 1);
        OrigemCodi     := FCodigo;
        Tipo           := VAR_FATID__004;
        CliOrig        := FCliente;
        CliDest        := FCliente;
        Insumo         := FProduto;
        AllQtd         := Geral.DMV(ValVar);
        //
        while AllQtd >= 0.001 do
        begin
          Peso           := QrPQRBxaAbertoKg.Value;
          if Peso >= 0.001 then
          begin
            if Peso > AllQtd then
              Peso := AllQtd;
            AllQtd := AllQtd - Peso;
        (*    Retorno        := ;
            StqMovIts      := ;
        *)
            Peso           := - Peso;
            RetQtd         := - Peso;
            Valor          := Peso * QrPQRBxaPRECO.Value;
            //
            OrigemCtrl := UMyMod.BPGS1I32('pqx', 'OrigemCtrl',
                       'Tipo='+ Geral.FF0(Tipo), '', tsPos, stIns, 0);
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqx', False, [
            'DataX', 'CliOrig', 'CliDest',
            'Insumo', 'Peso', 'Valor',
            (*'Retorno', 'StqMovIts',*) 'RetQtd'], [
            'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
            DataX, CliOrig, CliDest,
            Insumo, Peso, Valor,
            (*Retorno, StqMovIts,*) RetQtd], [
            OrigemCodi, OrigemCtrl, Tipo], False) then
            begin
              OriCodiInn := OrigemCodi; //QrPQRInnOrigemCodi.Value;
              OriCtrlInn := OrigemCtrl; //QrPQRInnOrigemCtrl.Value;
              TipoInn    := Tipo; //QrPQRInnTipo.Value;
              OriCodiOut := QrPQRBxaOrigemCodi.Value;
              OriCtrlOut := QrPQRBxaOrigemCtrl.Value;
              TipoOut    := QrPQRBxaTipo.Value;
              //
              Peso  := - Peso;
              Valor := - Valor;
              //
              Controle := Dmod.PQR_InsereItemPQRIts(FCodigo,
                OriCodiInn, OriCtrlInn, TipoInn,
                OriCodiOut, OriCtrlOut, TipoOut,
                Insumo, Peso, Valor, Ocorrencia);
              //
            end;
          end;
          QrPQRBxa.Next;
        end;
        ReopenPQRIts(Controle);
        ReopenDadosProdto();
      end;
    finally
      QrPQRBxa.EnableControls;
      DBGPQRBxa.Enabled := True;
    end;
  end;
end;

procedure TFmPQRIts.ReopenPQRIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmPQRIts.ReopenBxa(Tipo, OrigemCodi, OrigemCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
  'SELECT Controle, ',
  'OriCodiInn, OriCtrlInn, TipoInn, ',
  'OriCodiOut, OriCtrlOut, TipoOut ',
  'FROM pqrits ',
  'WHERE OriCodiInn=' + Geral.FF0(OrigemCodi),
  'AND OriCtrlInn=' + Geral.FF0(OrigemCtrl),
  'AND TipoInn=' + Geral.FF0(Tipo),
  '']);
end;

procedure TFmPQRIts.ReopenDadosProdto();
begin
  ReopenPQRBxa();
  ReopenPQRInn();
  //
  PnDados.Visible := True;
end;

procedure TFmPQRIts.ReopenInn(Tipo, OrigemCodi, OrigemCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
  'SELECT Controle, ',
  'OriCodiInn, OriCtrlInn, TipoInn, ',
  'OriCodiOut, OriCtrlOut, TipoOut ',
  'FROM pqrits ',
  'WHERE OriCodiOut=' + Geral.FF0(OrigemCodi),
  'AND OriCtrlOut=' + Geral.FF0(OrigemCtrl),
  'AND TipoOut=' + Geral.FF0(Tipo),
  '']);
end;

procedure TFmPQRIts.ReopenPQ(Todos: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQ1, Dmod.MyDB, [
  'SELECT pq.Nome NOMEPQ, pfo.* ',
  'FROM pqfor pfo ',
  'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo ',
  Geral.ATS_If(not Todos, ['WHERE pfo.CI=' + Geral.FF0(FCliente)]),
  'ORDER BY pq.Nome ',
  '']);
end;

procedure TFmPQRIts.ReopenPQRBxa();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRBxa, Dmod.MyDB, [
  'SELECT pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo,',
  'pqx.DataX, -(pqx.RetQtd + pqx.Peso) AbertoKg, ',
  'IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) PRECO,',
  'pqx.OrigemCodi',
  'FROM pqx pqx',
  'WHERE pqx.CliDest=' + Geral.FF0(FCliente),
  'AND pqx.Insumo=' + Geral.FF0(FProduto),
  'AND pqx.RetQtd + pqx.Peso <> 0',
  'AND pqx.Peso < 0 ',
  'AND (',
  '    pqx.Tipo = -1 OR pqx.Tipo > 100',
  '    ) ',
  'ORDER BY pqx.DataX, pqx.OrigemCodi',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumBxa, Dmod.MyDB, [
  'SELECT -SUM(pqx.RetQtd + pqx.Peso) AbertoKg, ',
  'IF(SUM(pqx.Peso) = 0, 0, SUM(pqx.Valor) / SUM(pqx.Peso)) PRECO ',
  'FROM pqx pqx ',
  'WHERE pqx.CliDest=' + Geral.FF0(FCliente),
  'AND pqx.Insumo=' + Geral.FF0(FProduto),
  'AND pqx.RetQtd + pqx.Peso <> 0 ',
  'AND pqx.Peso < 0 ',
  'AND ( ',
  '    pqx.Tipo = -1 OR pqx.Tipo > 100 ',
  '    ) ',
  'ORDER BY pqx.DataX ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRedBxa, Dmod.MyDB, [
  'SELECT pqx.OrigemCtrl, pqx.Peso ',
  'FROM pqx ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID__004),
  'AND pqx.OrigemCodi=' + Geral.FF0(FCodigo),
  'AND pqx.Insumo=' + Geral.FF0(FProduto),
  '']);
end;

procedure TFmPQRIts.ReopenPQRInn();
begin
  //adicionar Tipo =-3 na soma
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRInn, Dmod.MyDB, [
  'SELECT pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo,',
  'pqx.DataX, (pqx.Peso + pqx.RetQtd) AbertoKg, ',
  'IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) PRECO,',
  'pqx.RetQtd, pqe.NF_CC ',
  'FROM pqx',
  'LEFT JOIN pqe pqe ON pqe.Codigo=pqx.OrigemCodi',
  'WHERE pqx.CliDest=' + Geral.FF0(FCliente),
  'AND pqx.Insumo=' + Geral.FF0(FProduto),
  'AND pqx.RetQtd + pqx.Peso <> 0',
  'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
  'AND pqx.Peso > 0 ',
  'ORDER BY pqx.DataX',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumInn, Dmod.MyDB, [
  'SELECT SUM(pqx.Peso + pqx.RetQtd) AbertoKg, ',
  'IF(SUM(pqx.Peso) = 0, 0, SUM(pqx.Valor) / SUM(pqx.Peso)) PRECO ',
  'FROM pqx ',
  'WHERE pqx.CliDest=' + Geral.FF0(FCliente),
  'AND pqx.Insumo=' + Geral.FF0(FProduto),
  'AND pqx.RetQtd + pqx.Peso <> 0 ',
  'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
  'AND pqx.Peso > 0 ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRedInn, Dmod.MyDB, [
  'SELECT pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo, pqx.Peso ',
  'FROM pqx ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID__003),
  'AND pqx.OrigemCodi=' + Geral.FF0(FCodigo),
  'AND pqx.Insumo=' + Geral.FF0(FProduto),
  '']);
end;

{
DELETE FROM pqrcab;
DELETE FROM pqrits;
UPDATE pqx SET RetQtd=0;
}

end.
