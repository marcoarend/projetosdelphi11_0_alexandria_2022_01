object FmPQRMul: TFmPQRMul
  Left = 339
  Top = 185
  Caption = 'QUI-RETOR-006 :: Itens a Retornar'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 196
        Height = 32
        Caption = 'Itens a Retornar'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 196
        Height = 32
        Caption = 'Itens a Retornar'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 196
        Height = 32
        Caption = 'Itens a Retornar'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 52
    Width = 812
    Height = 452
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 452
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 452
        Align = alClient
        TabOrder = 0
        object DBGRet: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 808
          Height = 435
          Align = alClient
          DataSource = DsBaixar
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 280
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdRet'
              Title.Caption = 'Qtd.Ret.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VlURet'
              Title.Caption = '$ Unit.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VlTRet'
              Title.Caption = '$ Tot. Ret'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdInn'
              Title.Caption = 'Qtd. NFs'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdBxa'
              Title.Caption = 'Qtd.bxa.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdStq'
              Title.Caption = 'Qtd. estq.'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 812
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPrepara: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Prepara'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPreparaClick
      end
      object BtRetorna: TBitBtn
        Tag = 10
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Retorna'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtRetornaClick
      end
    end
  end
  object QrPQ1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Nome NOMEPQ, pfo.* '
      'FROM pqfor pfo'
      'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo'
      'WHERE pfo.CI=:P0'
      'AND pfo.IQ=:P1'
      'ORDER BY pq.Nome')
    Left = 32
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
    object QrPQ1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQ1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrSumInn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 264
    object QrSumInnAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumInnPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object QrSumBxa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 216
    object QrSumBxaAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumBxaPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object QrEstq: TmySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 168
    object QrEstqValor: TFloatField
      FieldName = 'Valor'
    end
    object QrEstqPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEstqPRECO: TFloatField
      FieldName = 'PRECO'
    end
  end
  object QrPQRMul: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _pqrmul_')
    Left = 128
    Top = 120
    object QrPQRMulCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQRMulNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrPQRMulQtdInn: TFloatField
      FieldName = 'QtdInn'
    end
    object QrPQRMulVlUInn: TFloatField
      FieldName = 'VlUInn'
    end
    object QrPQRMulVlTInn: TFloatField
      FieldName = 'VlTInn'
    end
    object QrPQRMulQtdBxa: TFloatField
      FieldName = 'QtdBxa'
    end
    object QrPQRMulVlUBxa: TFloatField
      FieldName = 'VlUBxa'
    end
    object QrPQRMulVlTBxa: TFloatField
      FieldName = 'VlTBxa'
    end
    object QrPQRMulQtdRet: TFloatField
      FieldName = 'QtdRet'
    end
    object QrPQRMulVlURet: TFloatField
      FieldName = 'VlURet'
    end
    object QrPQRMulVlTRet: TFloatField
      FieldName = 'VlTRet'
    end
    object QrPQRMulQtdStq: TFloatField
      FieldName = 'QtdStq'
    end
    object QrPQRMulVlUStq: TFloatField
      FieldName = 'VlUStq'
    end
    object QrPQRMulVlTStq: TFloatField
      FieldName = 'VlTStq'
    end
    object QrPQRMulCodErr: TIntegerField
      FieldName = 'CodErr'
    end
    object QrPQRMulTxtErr: TWideStringField
      FieldName = 'TxtErr'
      Size = 255
    end
    object QrPQRMulAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQRMulQtdErr: TFloatField
      FieldName = 'QtdErr'
    end
  end
  object frxDsPQRMul: TfrxDBDataset
    UserName = 'frxDsPQRMul'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'QtdInn=QtdInn'
      'VlUInn=VlUInn'
      'VlTInn=VlTInn'
      'QtdBxa=QtdBxa'
      'VlUBxa=VlUBxa'
      'VlTBxa=VlTBxa'
      'QtdRet=QtdRet'
      'VlURet=VlURet'
      'VlTRet=VlTRet'
      'QtdStq=QtdStq'
      'VlUStq=VlUStq'
      'VlTStq=VlTStq'
      'CodErr=CodErr'
      'TxtErr=TxtErr'
      'Ativo=Ativo'
      'QtdErr=QtdErr')
    DataSet = QrPQRMul
    BCDToCurrency = False
    Left = 128
    Top = 168
  end
  object frxQUI_RETOR_006_01: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41825.822973125000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxQUI_RETOR_006_01GetValue
    Left = 128
    Top = 216
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPQRMul
        DataSetName = 'frxDsPQRMul'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Height = 60.472470240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Status Pr'#233'-retorno')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        Height = 15.118110240000000000
        Top = 196.535560000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPQRMul
        DataSetName = 'frxDsPQRMul'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 68.031540000000010000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Nome'
          DataSet = frxDsPQRMul
          DataSetName = 'frxDsPQRMul'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQRMul."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDsPQRMul
          DataSetName = 'frxDsPQRMul'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRMul."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 529.134199999999900000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'QtdBxa'
          DataSet = frxDsPQRMul
          DataSetName = 'frxDsPQRMul'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRMul."QtdBxa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'QtdInn'
          DataSet = frxDsPQRMul
          DataSetName = 'frxDsPQRMul'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRMul."QtdInn"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValValorT: TfrxMemoView
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'QtdStq'
          DataSet = frxDsPQRMul
          DataSetName = 'frxDsPQRMul'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRMul."QtdStq"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 302.362400000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'QtdRet'
          DataSet = frxDsPQRMul
          DataSetName = 'frxDsPQRMul'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRMul."QtdRet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'QtdErr'
          DataSet = frxDsPQRMul
          DataSetName = 'frxDsPQRMul'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRMul."QtdErr"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 34.015760240000000000
        Top = 139.842610000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPQRMul."CodErr"'
        object Memo1: TfrxMemoView
          Width = 680.315204720000000000
          Height = 18.897640240000000000
          ShowHint = False
          DataField = 'TxtErr'
          DataSet = frxDsPQRMul
          DataSetName = 'frxDsPQRMul'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQRMul."TxtErr"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 68.031540000000010000
          Top = 18.897650000000000000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do insumo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 529.134199999999900000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Baixas')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 453.543600000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Entradas')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitValorT: TfrxMemoView
          Left = 377.953000000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Estoque')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 302.362400000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd A Retornar')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 604.724800000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd ERRO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 3.779530000000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object DsBaixar: TDataSource
    DataSet = QrBaixar
    Left = 224
    Top = 168
  end
  object QrBaixar: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _pqrmul_ '
      'WHERE QtdRet >= 0.001 ')
    Left = 224
    Top = 120
    object QrBaixarCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBaixarNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrBaixarQtdInn: TFloatField
      FieldName = 'QtdInn'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrBaixarVlUInn: TFloatField
      FieldName = 'VlUInn'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrBaixarVlTInn: TFloatField
      FieldName = 'VlTInn'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBaixarQtdBxa: TFloatField
      FieldName = 'QtdBxa'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrBaixarVlUBxa: TFloatField
      FieldName = 'VlUBxa'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrBaixarVlTBxa: TFloatField
      FieldName = 'VlTBxa'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBaixarQtdRet: TFloatField
      FieldName = 'QtdRet'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrBaixarVlURet: TFloatField
      FieldName = 'VlURet'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrBaixarVlTRet: TFloatField
      FieldName = 'VlTRet'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBaixarQtdStq: TFloatField
      FieldName = 'QtdStq'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrBaixarVlUStq: TFloatField
      FieldName = 'VlUStq'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrBaixarVlTStq: TFloatField
      FieldName = 'VlTStq'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBaixarCodErr: TIntegerField
      FieldName = 'CodErr'
    end
    object QrBaixarTxtErr: TWideStringField
      FieldName = 'TxtErr'
      Size = 255
    end
    object QrBaixarAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBaixarQtdErr: TFloatField
      FieldName = 'QtdErr'
      DisplayFormat = '#,###,###,##0.000'
    end
  end
  object QrPQRBxa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 224
    Top = 216
    object QrPQRBxaOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQRBxaOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQRBxaTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQRBxaDataX: TDateField
      FieldName = 'DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQRBxaAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQRBxaPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object QrPQRInn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 224
    Top = 264
    object QrPQRInnOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQRInnOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQRInnTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQRInnDataX: TDateField
      FieldName = 'DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQRInnAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQRInnPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQRInnNF_CC: TIntegerField
      FieldName = 'NF_CC'
      DisplayFormat = '0;-0; '
    end
    object QrPQRInnRetQtd: TFloatField
      FieldName = 'RetQtd'
    end
  end
end
