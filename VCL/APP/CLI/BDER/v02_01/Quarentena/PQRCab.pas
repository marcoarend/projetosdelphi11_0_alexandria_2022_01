unit PQRCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, BlueDermConsts, frxClass, frxDBSet;

type
  TFmPQRCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrPQRCab: TmySQLQuery;
    DsPQRCab: TDataSource;
    QrPQRIts: TmySQLQuery;
    DsPQRIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrClientes: TmySQLQuery;
    QrClientesNome: TWideStringField;
    QrClientesCodigo: TIntegerField;
    DsClientes: TDataSource;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    TPData: TdmkEditDateTimePicker;
    Label4: TLabel;
    EdNF_RC: TdmkEdit;
    QrPQRCabCodigo: TIntegerField;
    QrPQRCabNome: TWideStringField;
    QrPQRCabCliente: TIntegerField;
    QrPQRCabData: TDateField;
    QrPQRCabNF_RC: TIntegerField;
    QrPQRCabLk: TIntegerField;
    QrPQRCabDataCad: TDateField;
    QrPQRCabDataAlt: TDateField;
    QrPQRCabUserCad: TIntegerField;
    QrPQRCabUserAlt: TIntegerField;
    QrPQRCabAlterWeb: TSmallintField;
    QrPQRCabAtivo: TSmallintField;
    QrPQRCabNO_CLI: TWideStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrPQRItsCodigo: TIntegerField;
    QrPQRItsControle: TIntegerField;
    QrPQRItsPreco: TFloatField;
    QrPQRItsPeso: TFloatField;
    QrPQRItsValor: TFloatField;
    QrPQRItsLk: TIntegerField;
    QrPQRItsDataCad: TDateField;
    QrPQRItsDataAlt: TDateField;
    QrPQRItsUserCad: TIntegerField;
    QrPQRItsUserAlt: TIntegerField;
    QrPQRItsAlterWeb: TSmallintField;
    QrPQRItsAtivo: TSmallintField;
    GroupBox1: TGroupBox;
    DGDados: TDBGrid;
    QrPQRItsOriCodiInn: TIntegerField;
    QrPQRItsOriCtrlInn: TIntegerField;
    QrPQRItsTipoInn: TIntegerField;
    QrPQRItsOriCodiOut: TIntegerField;
    QrPQRItsOriCtrlOut: TIntegerField;
    QrPQRItsTipoOut: TIntegerField;
    QrPQRPQs: TmySQLQuery;
    DsPQRPQs: TDataSource;
    QrPQRPQsInsumo: TIntegerField;
    QrPQRPQsPeso: TFloatField;
    QrPQRPQsValor: TFloatField;
    QrPQRPQsNO_PQ: TWideStringField;
    DBGrid1: TDBGrid;
    frxQUI_RETOR_001_00_A: TfrxReport;
    frxDsPQRCab: TfrxDBDataset;
    frxDsPQRPQs: TfrxDBDataset;
    frxDsPQRIts: TfrxDBDataset;
    QrPQRPQsPRECOMEDIO: TFloatField;
    QrPQRNFs: TmySQLQuery;
    QrPQRNFsCodigo: TIntegerField;
    QrPQRNFsDataE: TDateField;
    QrPQRNFsNF_CC: TIntegerField;
    DsPQRNFs: TDataSource;
    frxDsPQRNFs: TfrxDBDataset;
    DBGrid2: TDBGrid;
    QrPQRItsInsumo: TIntegerField;
    QrPQRItsOcorrecia: TIntegerField;
    QrPQRItsNO_RECEITA: TWideStringField;
    QrPQRItsObs: TWideStringField;
    PMImprime: TPopupMenu;
    Somenteinsumos1: TMenuItem;
    Insumoseitensdebaixa1: TMenuItem;
    QrPQRItsObsCONCAT: TWideStringField;
    QrBalanCli: TmySQLQuery;
    QrBalanCliPeriodo: TIntegerField;
    QrBalanCliCliInt: TIntegerField;
    QrBalanCliGerouDif: TSmallintField;
    N1: TMenuItem;
    Retornosorfos1: TMenuItem;
    QrRetOrf: TmySQLQuery;
    QrRetOrfCodigo: TIntegerField;
    QrRetOrfControle: TIntegerField;
    QrRetOrfInsumo: TIntegerField;
    QrRetOrfOcorrecia: TIntegerField;
    QrRetOrfOriCodiInn: TIntegerField;
    QrRetOrfOriCtrlInn: TIntegerField;
    QrRetOrfTipoInn: TIntegerField;
    QrRetOrfOriCodiOut: TIntegerField;
    QrRetOrfOriCtrlOut: TIntegerField;
    QrRetOrfTipoOut: TIntegerField;
    QrRetOrfPreco: TFloatField;
    QrRetOrfPeso: TFloatField;
    QrRetOrfValor: TFloatField;
    QrRetOrfLk: TIntegerField;
    QrRetOrfDataCad: TDateField;
    QrRetOrfDataAlt: TDateField;
    QrRetOrfUserCad: TIntegerField;
    QrRetOrfUserAlt: TIntegerField;
    QrRetOrfAlterWeb: TSmallintField;
    QrRetOrfAtivo: TSmallintField;
    QrRetOrfNO_PQ: TWideStringField;
    frxDsRetOrf: TfrxDBDataset;
    frxQUI_RETOR_001_01: TfrxReport;
    Insumonico1: TMenuItem;
    Multiitens1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPQRCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPQRCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrPQRCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrPQRCabBeforeClose(DataSet: TDataSet);
    procedure ImgTipoChange(Sender: TObject);
    procedure QrPQRPQsAfterScroll(DataSet: TDataSet);
    procedure QrPQRPQsBeforeClose(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxQUI_RETOR_001_00_AGetValue(const VarName: string;
      var Value: Variant);
    procedure QrPQRNFsAfterScroll(DataSet: TDataSet);
    procedure QrPQRNFsBeforeClose(DataSet: TDataSet);
    procedure Somenteinsumos1Click(Sender: TObject);
    procedure Insumoseitensdebaixa1Click(Sender: TObject);
    procedure QrPQRItsCalcFields(DataSet: TDataSet);
    procedure Retornosorfos1Click(Sender: TObject);
    procedure Insumonico1Click(Sender: TObject);
    procedure Multiitens1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraFormPQRIts(SQLType: TSQLType);
    procedure ReopenPQRIts(Controle: Integer);
    procedure ReopenPQRNFs(Codigo: Integer);
    procedure Imprime(Tudo: Boolean);
    function  ImpedePeloBalanco(): Boolean;

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    FImprimeTudo: Boolean;
    //
    procedure ReopenPQRPQs(Insumo: Integer);
    procedure LocalizaItem(Insumo, ItemNF, ItemPQx: Integer);
  end;

var
  FmPQRCab: TFmPQRCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, PQRIts, PQRMul, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQRCab.LocalizaItem(Insumo, ItemNF, ItemPQx: Integer);
begin
  LocCod(QrPQRCabCodigo.Value, QrPQRCabCodigo.Value);
  if QrPQRPQs.State <> dsInactive then
    QrPQRPQs.Locate('Insumo', Insumo, []);
  if QrPQRNFs.State <> dsInactive then
    QrPQRNFs.Locate('Codigo', ItemNF, []);
  if QrPQRIts.State <> dsInactive then
    QrPQRIts.Locate('Controle', ItemPQx, []);
end;

procedure TFmPQRCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQRCab.MostraFormPQRIts(SQLType: TSQLType);
begin
  if ImpedePeloBalanco() then
    Exit;
  //
  if DBCheck.CriaFm(TFmPQRIts, FmPQRIts, afmoNegarComAviso) then
  begin
    FmPQRIts.ImgTipo.SQLType := SQLType;
    FmPQRIts.FQrCab := QrPQRCab;
    FmPQRIts.FDsCab := DsPQRCab;
    FmPQRIts.FQrIts := QrPQRIts;
    FmPQRIts.FCliente := QrPQRCabCliente.Value;
    FmPQRIts.FData    := QrPQRCabData.Value;
    FmPQRIts.FCodigo  := QrPQRCabCodigo.Value;
    FmPQRIts.ReopenPQ(False);
    //
    if SQLType = stIns then

    else
    begin
      FmPQRIts.EdControle.ValueVariant := QrPQRItsControle.Value;
      //
(*
      FmPQRIts.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrPQRItsCNPJ_CPF.Value);
      FmPQRIts.EdNomeEmiSac.Text := QrPQRItsNome.Value;
      FmPQRIts.EdCPF1.ReadOnly := True;
*)
    end;
    FmPQRIts.ShowModal;
    if FmPQRIts.FLastPQRIts <> 0 then
      LocalizaItem(FmPQRIts.FLastInsumo, FmPQRIts.FLastItemNF, FmPQRIts.FLastPQRIts)
    else
      LocalizaItem(QrPQRPQsInsumo.Value, QrPQRNFsCodigo.Value, QrPQRItsControle.Value);

    FmPQRIts.Destroy;
    //
  end;
end;

procedure TFmPQRCab.Multiitens1Click(Sender: TObject);
begin
  if ImpedePeloBalanco() then
    Exit;
  //
  if DBCheck.CriaFm(TFmPQRMul, FmPQRMul, afmoNegarComAviso) then
  begin
    //FmPQRMul.ImgTipo.SQLType := SQLType;
    FmPQRMul.FCliente := QrPQRCabCliente.Value;
    FmPQRMul.FCodigo  := QrPQRCabCodigo.Value;
    FmPQRMul.FNO_Cli  := QrPQRCabNO_CLI.Value;
    //
    FmPQRMul.ShowModal;
    ReopenPQRPQs(0);
    FmPQRMul.Destroy;
  end;
end;

procedure TFmPQRCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrPQRCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrPQRCab, QrPQRPQs);
end;

procedure TFmPQRCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrPQRCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrPQRIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrPQRIts);
end;

procedure TFmPQRCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQRCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQRCab.DefParams;
begin
  VAR_GOTOTABELA := 'pqrcab';
  VAR_GOTOMYSQLTABLE := QrPQRCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pqc.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLI');
  VAR_SQLx.Add('FROM pqrcab pqc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=pqc.Cliente');
  //
  VAR_SQL1.Add('WHERE pqc.Codigo=:P0');
  //
  //VAR_SQL2.Add('WHERE CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE pqc.Nome Like :P0');
  //
end;

procedure TFmPQRCab.ImgTipoChange(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (ImgTipo.SQLType = stIns) or
  ((QrPQRPQs.State <> dsInactive) and (QrPQRPQs.RecordCount = 0));
  //
  EdCliente.Enabled := Habilita;
  CBCliente.Enabled := Habilita;
  //TPData.Enabled    := Habilita;
end;

function TFmPQRCab.ImpedePeloBalanco(): Boolean;
var
  Periodo, CliInt: Integer;
begin
  Periodo := Geral.Periodo2000(QrPQRCabData.Value);
  CliInt  := QrPQRCabCliente.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBalanCli, Dmod.MyDB, [
  'SELECT * ',
  'FROM balancli ',
  'WHERE Periodo=' + Geral.FF0(Periodo),
  'AND CliInt=' + Geral.FF0(CliInt),
  '']);
  //
  Result := (QrBalanCli.RecordCount = 0) or (QrBalanCliGerouDif.Value = 0);
  if Result then
    Geral.MB_Aviso('Inclus�o abortada! ' + slineBreak +
    'Balan�o sem gera��o de diferen�as do m�s anterior!');
end;

procedure TFmPQRCab.Imprime(Tudo: Boolean);
begin
  FImprimeTudo := Tudo;
  MyObjects.frxMostra(frxQUI_RETOR_001_00_A, 'Retorno PQ');
end;

procedure TFmPQRCab.Insumonico1Click(Sender: TObject);
begin
  MostraFormPQRIts(stIns);
end;

procedure TFmPQRCab.Insumoseitensdebaixa1Click(Sender: TObject);
begin
  Imprime(True);
end;

procedure TFmPQRCab.ItsAltera1Click(Sender: TObject);
begin
  //MostraFormPQRIts(stUpd);
end;

procedure TFmPQRCab.CabExclui1Click(Sender: TObject);
const
  Pergunta = 'Deseja realmente excluir este retorno?';
  Tabela =  'pqrcab';
  Campo = 'Codigo';
var
  Inteiro: Integer;
begin
  //Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  //Caption + sLineBreak + TMenuItem(Sender).Name);
  Inteiro := QrPQRCabCodigo.Value;
  if UMyMod.ExcluiRegistroInt1(Pergunta, Tabela, Campo, Inteiro, Dmod.MyDB) = ID_YES then
    LocCod(Inteiro + 1, Inteiro);
end;

procedure TFmPQRCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQRCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPQRCab.ItsExclui1Click(Sender: TObject);
var
  OriCodiInn, OriCtrlInn, TipoInn,
  OriCodiOut, OriCtrlOut, TipoOut, //Insumo,
  Controle: Integer;
begin
  OriCodiInn := QrPQRItsOriCodiInn.Value;
  OriCtrlInn := QrPQRItsOriCtrlInn.Value;
  TipoInn    := QrPQRItsTipoInn   .Value;
  OriCodiOut := QrPQRItsOriCodiOut.Value;
  OriCtrlOut := QrPQRItsOriCtrlOut.Value;
  TipoOut    := QrPQRItsTipoOut   .Value;
  //Insumo     := QrPQRItsInsumo    .Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?' +
  sLineBreak + 'Insumo: ' + QrPQRPQsNO_PQ.Value +
  sLineBreak + 'ID: ' + Geral.FF0(QrPQRItsControle.Value) +
  sLineBreak + 'Quantidade: ' + Geral.FFT(QrPQRItsPeso.Value, 3, siNegativo),
  'pqrits', 'Controle', QrPQRItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    // Nao computar saldo inicial de baixa!
    if TipoOut <> VAR_FATID__004 then
      Dmod.PQR_AtualizaRetQtdInn(OriCodiInn, OriCtrlInn, TipoInn);
    // Nao computar saldo inicial de NFe de entrada!
    if TipoOut <> VAR_FATID__003 then
      Dmod.PQR_AtualizaRetQtdOut(OriCodiOut, OriCtrlOut, TipoOut);
    //
    Controle := GOTOy.LocalizaPriorNextIntQr(QrPQRIts,
      QrPQRItsControle, QrPQRItsControle.Value);
    ReopenPQRIts(Controle);
  end;
end;

procedure TFmPQRCab.ReopenPQRIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRIts, Dmod.MyDB, [
(*
  'SELECT pqi.*, emi.Nome NO_RECEITA, emi.Obs',
  'FROM pqrits pqi ',
  'LEFT JOIN emit emi ON emi.Codigo=pqi.OriCodiOut ',
*)
  'SELECT pqi.*, ',
  ' ',
  'CASE ',
  'WHEN pqi.TipoOut=110 THEN emi.Nome ',
  'WHEN pqi.TipoOut=190 THEN pqo.Nome ',
  'ELSE "" END NO_RECEITA, ',
  ' ',
  'CASE ',
  'WHEN pqi.TipoOut=110 THEN emi.Obs ',
  'WHEN pqi.TipoOut=190 THEN "Baixa Manual" ',
  'ELSE CONCAT("Baixa Tipo ", pqi.TipoOut) END OBS ',
  ' ',
  'FROM pqrits pqi ',
  'LEFT JOIN emit emi ON emi.Codigo=pqi.OriCodiOut ',
  'LEFT JOIN pqo pqo ON pqo.Codigo=pqi.OriCodiOut ',
  //
  'WHERE pqi.Codigo=' + Geral.FF0(QrPQRCabCodigo.Value),
  'AND pqi.Ocorrecia=' + Geral.FF0(VAR_PQR_OCORR_EMI_RET),
  'AND pqi.Insumo=' + Geral.FF0(QrPQRPQsInsumo.Value),
  'AND pqi.OriCodiInn=' + Geral.FF0(QrPQRNFsCodigo.Value),
  'ORDER BY Controle',
  '']);
  //
  QrPQRIts.Locate('Controle', Controle, []);
end;

procedure TFmPQRCab.ReopenPQRNFs(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRNFs, Dmod.MyDB, [
  'SELECT pqe.Codigo, pqe.DataE, pqe.NF_CC ',
  'FROM pqrits pqi ',
  'LEFT JOIN pqe pqe ON pqi.OriCodiInn=pqe.Codigo ',
  'WHERE pqi.Codigo=' + Geral.FF0(QrPQRCabCodigo.Value),
  'AND pqi.Ocorrecia=' + Geral.FF0(VAR_PQR_OCORR_EMI_RET),
  'AND pqi.Insumo=' + Geral.FF0(QrPQRPQsInsumo.Value),
  'GROUP BY pqi.OriCodiInn, pqi.OriCtrlInn, pqi.TipoInn ',
  'ORDER BY pqe.DataE ',
  '']);
end;

procedure TFmPQRCab.ReopenPQRPQs(Insumo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRPQs, Dmod.MyDB, [
  'SELECT pqi.Insumo, SUM(pqi.Peso ) Peso, SUM(pqi.Valor) Valor, ',
  'IF(SUM(pqi.Peso)  = 0, 0, SUM(pqi.Valor) / SUM(pqi.Peso )) PRECOMEDIO, ',
  'pq.Nome NO_PQ ',
  'FROM pqrits pqi ',
  'LEFT JOIN pq ON pq.Codigo=pqi.Insumo ',
  'WHERE pqi.Codigo=' + Geral.FF0(QrPQRCabCodigo.Value),
  'AND pqi.Ocorrecia=' + Geral.FF0(VAR_PQR_OCORR_EMI_RET),
  'GROUP BY pqi.Insumo ',
  'ORDER BY NO_PQ ',
  '']);
end;

procedure TFmPQRCab.Retornosorfos1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRetOrf, Dmod.MyDB, [
  'SELECT its.*, pq.Nome NO_PQ ',
  'FROM pqrits its ',
  'LEFT JOIN pq ON pq.Codigo=its.Insumo ',
  'WHERE its.TipoInn=-5 ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxQUI_RETOR_001_01, [
  DModG.frxDsDono,
  frxDsRetOrf
  ]);
  MyObjects.frxMostra(frxQUI_RETOR_001_01, 'Retornos Orf�os');
end;

procedure TFmPQRCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQRCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQRCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQRCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQRCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQRCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQRCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPQRCabCodigo.Value;
  Close;
end;

procedure TFmPQRCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPQRCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'pqrcab');
end;

procedure TFmPQRCab.BtConfirmaClick(Sender: TObject);
var
  Nome, Data: String;
  Codigo, Cliente, NF_RC: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  Cliente        := EdCliente.ValueVariant;
  Data           := Geral.FDT(TPData.Date, 1);
  NF_RC          := EdNF_RC.ValueVariant;
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Defina um cliente!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('pqrcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pqrcab', False, [
  'Nome', 'Cliente', 'Data',
  'NF_RC'], [
  'Codigo'], [
  Nome, Cliente, Data,
  NF_RC], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmPQRCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'pqrcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqrcab', 'Codigo');
end;

procedure TFmPQRCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmPQRCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmPQRCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GroupBox1.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
end;

procedure TFmPQRCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQRCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPQRCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPQRCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQRCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPQRCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPQRCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQRCab.QrPQRCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPQRCab.QrPQRCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPQRPQs(0);
end;

procedure TFmPQRCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrPQRCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmPQRCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPQRCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'pqrcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPQRCab.Somenteinsumos1Click(Sender: TObject);
begin
  Imprime(False);
end;

procedure TFmPQRCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQRCab.frxQUI_RETOR_001_00_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_CLIINT' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', QrPQRCabNO_CLI.Value, QrPQRCabCliente.Value, 'TODAS')
  else
  if VarName = 'VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_IMP_TUDO' then
    Value := FImprimeTudo
  else
end;

procedure TFmPQRCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPQRCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'pqrcab');
  TPData.Date := Date;
end;

procedure TFmPQRCab.QrPQRCabBeforeClose(
  DataSet: TDataSet);
begin
  QrPQRPQs.Close;
end;

procedure TFmPQRCab.QrPQRCabBeforeOpen(DataSet: TDataSet);
begin
  QrPQRCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPQRCab.QrPQRItsCalcFields(DataSet: TDataSet);
begin
 QrPQRItsObsCONCAT.Value := Geral.Substitui(QrPQRItsObs.Value, sLineBreak, ' | ');
end;

procedure TFmPQRCab.QrPQRNFsAfterScroll(DataSet: TDataSet);
begin
  ReopenPQRIts(0);
end;

procedure TFmPQRCab.QrPQRNFsBeforeClose(DataSet: TDataSet);
begin
  QrPQRIts.Close;
end;

procedure TFmPQRCab.QrPQRPQsAfterScroll(DataSet: TDataSet);
begin
  ReopenPQRNFs(0);
end;

procedure TFmPQRCab.QrPQRPQsBeforeClose(DataSet: TDataSet);
begin
  QrPQRNFs.Close;
end;

end.

