unit PQRMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  frxClass, frxDBSet;

type
  TFmPQRMul = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPrepara: TBitBtn;
    LaTitulo1C: TLabel;
    QrPQ1: TmySQLQuery;
    QrSumInn: TmySQLQuery;
    QrSumInnAbertoKg: TFloatField;
    QrSumInnPRECO: TFloatField;
    QrSumBxa: TmySQLQuery;
    QrSumBxaAbertoKg: TFloatField;
    QrSumBxaPRECO: TFloatField;
    QrEstq: TmySQLQuery;
    QrEstqValor: TFloatField;
    QrEstqPeso: TFloatField;
    QrEstqPRECO: TFloatField;
    PB1: TProgressBar;
    QrPQRMul: TmySQLQuery;
    DBGRet: TdmkDBGridZTO;
    QrPQ1Codigo: TIntegerField;
    QrPQ1Nome: TWideStringField;
    frxDsPQRMul: TfrxDBDataset;
    frxQUI_RETOR_006_01: TfrxReport;
    SbImprime: TBitBtn;
    DsBaixar: TDataSource;
    QrBaixar: TmySQLQuery;
    QrPQRMulCodigo: TIntegerField;
    QrPQRMulNome: TWideStringField;
    QrPQRMulQtdInn: TFloatField;
    QrPQRMulVlUInn: TFloatField;
    QrPQRMulVlTInn: TFloatField;
    QrPQRMulQtdBxa: TFloatField;
    QrPQRMulVlUBxa: TFloatField;
    QrPQRMulVlTBxa: TFloatField;
    QrPQRMulQtdRet: TFloatField;
    QrPQRMulVlURet: TFloatField;
    QrPQRMulVlTRet: TFloatField;
    QrPQRMulQtdStq: TFloatField;
    QrPQRMulVlUStq: TFloatField;
    QrPQRMulVlTStq: TFloatField;
    QrPQRMulCodErr: TIntegerField;
    QrPQRMulTxtErr: TWideStringField;
    QrPQRMulAtivo: TSmallintField;
    QrPQRMulQtdErr: TFloatField;
    BtRetorna: TBitBtn;
    QrBaixarCodigo: TIntegerField;
    QrBaixarNome: TWideStringField;
    QrBaixarQtdInn: TFloatField;
    QrBaixarVlUInn: TFloatField;
    QrBaixarVlTInn: TFloatField;
    QrBaixarQtdBxa: TFloatField;
    QrBaixarVlUBxa: TFloatField;
    QrBaixarVlTBxa: TFloatField;
    QrBaixarQtdRet: TFloatField;
    QrBaixarVlURet: TFloatField;
    QrBaixarVlTRet: TFloatField;
    QrBaixarQtdStq: TFloatField;
    QrBaixarVlUStq: TFloatField;
    QrBaixarVlTStq: TFloatField;
    QrBaixarCodErr: TIntegerField;
    QrBaixarTxtErr: TWideStringField;
    QrBaixarAtivo: TSmallintField;
    QrBaixarQtdErr: TFloatField;
    QrPQRBxa: TmySQLQuery;
    QrPQRBxaOrigemCodi: TIntegerField;
    QrPQRBxaOrigemCtrl: TIntegerField;
    QrPQRBxaTipo: TIntegerField;
    QrPQRBxaDataX: TDateField;
    QrPQRBxaAbertoKg: TFloatField;
    QrPQRBxaPRECO: TFloatField;
    QrPQRInn: TmySQLQuery;
    QrPQRInnOrigemCodi: TIntegerField;
    QrPQRInnOrigemCtrl: TIntegerField;
    QrPQRInnTipo: TIntegerField;
    QrPQRInnDataX: TDateField;
    QrPQRInnAbertoKg: TFloatField;
    QrPQRInnPRECO: TFloatField;
    QrPQRInnNF_CC: TIntegerField;
    QrPQRInnRetQtd: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPreparaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxQUI_RETOR_006_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtRetornaClick(Sender: TObject);
  private
    { Private declarations }
    FPQRMul: String;
    //
    procedure ReopenPQ(Todos: Boolean);
    procedure ImprimeStatus();
  public
    { Public declarations }
    FCliente, FCodigo: Integer;
    FNO_Cli: String;
    //
  end;

  var
  FmPQRMul: TFmPQRMul;

implementation

uses UnMyObjects, Module, ModuleGeral, CreateBlueDerm, DmkDAC_PF, UnDmkProcFunc,
PQx, UMySQLModule, BlueDermConsts;

{$R *.DFM}

procedure TFmPQRMul.BtPreparaClick(Sender: TObject);
var
  Nome, TxtErr: String;
  Codigo, CodErr: Integer;
  QtdInn, VlUInn, VlTInn, QtdBxa, VlUBxa, VlTBxa, QtdRet, VlURet, VlTRet,
  QtdStq, VlUStq, VlTStq, QtdErr: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    SbImprime.Enabled := False;
    FPQRMul := UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQRMul, DModG.QrUpdPID1, False);
    ReopenPQ(False);
    PB1.Position := 0;
    PB1.Max := QrPQ1.RecordCount;
    QrPQ1.First;
    while not QrPQ1.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      Codigo         := QrPQ1Codigo.Value;
      Nome           := QrPQ1Nome.Value;;
      // Estoque
      UnPQx.AtualizaEstoquePQ(FCliente, Codigo, aeNenhum, CO_VAZIO, False);
      UnDmkDAC_PF.AbreMySQLQuery0(QrEstq, Dmod.MyDB, [
      'SELECT Peso, Valor, ',
      'IF(Peso = 0, 0, Valor / Peso) PRECO ',
      'FROM pqcli',
      'WHERE CI=' + Geral.FF0(FCliente),
      'AND PQ=' + Geral.FF0(Codigo),
      '']);
      QtdStq         := QrEstqPeso.Value;
      VlUStq         := QrEstqPRECO.Value;
      VlTStq         := QrEstqValor.Value;
      // Entradas
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumInn, Dmod.MyDB, [
      'SELECT SUM(pqx.Peso + pqx.RetQtd) AbertoKg,',
      'IF(SUM(pqx.Peso) = 0, 0, SUM(pqx.Valor) / SUM(pqx.Peso)) PRECO',
      'FROM pqx',
      'WHERE pqx.CliDest=' + Geral.FF0(FCliente),
      'AND pqx.Insumo=' + Geral.FF0(Codigo),
      'AND pqx.RetQtd + pqx.Peso <> 0',
      'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
      'AND pqx.Peso > 0',
      '']);
      QtdInn         := QrSumInnAbertoKg.Value;
      VlUInn         := QrSumInnPRECO.Value;
      VlTInn         := QrSumInnAbertoKg.Value * QrSumInnPRECO.Value;
      // Baixas
      UnDmkDAC_PF.AbreMySQLQuery0(QrSumBxa, Dmod.MyDB, [
      'SELECT -SUM(pqx.RetQtd + pqx.Peso) AbertoKg, ',
      'IF(SUM(pqx.Peso) = 0, 0, SUM(pqx.Valor) / SUM(pqx.Peso)) PRECO ',
      'FROM pqx pqx ',
      'WHERE pqx.CliDest=' + Geral.FF0(FCliente),
      'AND pqx.Insumo=' + Geral.FF0(Codigo),
      'AND pqx.RetQtd + pqx.Peso <> 0 ',
      'AND pqx.Peso < 0 ',
      'AND ( ',
      '    pqx.Tipo = -1 OR pqx.Tipo > 100 ',
      '    ) ',
      'ORDER BY pqx.DataX ',
      '']);
      QtdBxa         := QrSumBxaAbertoKg.Value;
      VlUBxa         := QrSumBxaPRECO.Value;
      VlTBxa         := QrSumBxaAbertoKg.Value * QrSumBxaPRECO.Value;
      // A Retornar
      QtdRet         := dmkPF.MenorFloat([QtdInn, QtdBxa]);
      VlURet         := VlUInn;
      VlTRet         := QtdRet * VlURet;
      //
      QtdErr := QtdStq + QtdBxa - QtdInn;
      if (QtdErr > - 0.001) and (QtdErr < 0.001) and (QtdErr <> 0) then
        QtdErr := 0;
      if (QtdBxa = 0) and (QtdStq <> QtdInn) then
      begin
        CodErr := -900;
        QtdErr := QtdInn - QtdStq;
      end else
      //if (QtdBxa <> 0) and (QtdStq + QtdBxa <> QtdInn) then
      if (QtdBxa <> 0) and (QtdErr <> 0) then
      begin
        CodErr := -800;
        QtdErr := QtdInn - (QtdStq + QtdBxa);
      end else
      //if (QtdStq >= QtdBxa) and (QtdInn >= QtdBxa) and (QtdStq = QtdInn) then
      if QtdErr = 0 then
        CodErr := 0
      else
        CodErr := -999;
      if CodErr = 0 then
      begin
        if (QtdBxa = 0) then
          CodErr := 100
        else
          CodErr := 999
      end;
      //
      case CodErr of
        -900: TxtErr := 'ERRO: estoque difere de entradas';
        -800: TxtErr := 'ERRO: entradas difere de estoque menos baixas';
         100: TxtErr := 'Itens sem necessidade de retorno';
         999: TxtErr := 'Itens aptos a retornar';
        else TxtErr := 'ERRO: ' + Geral.FF0(CodErr) + ' *** C�digo de erro n�o implementado ***';
      end;
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FPQRMul, False, [
      'Nome',
      'QtdInn', 'VlUInn', 'VlTInn',
      'QtdBxa', 'VlUBxa', 'VlTBxa',
      'QtdRet', 'VlURet', 'VlTRet',
      'QtdStq', 'VlUStq', 'VlTStq',
      'CodErr', 'TxtErr', 'QtdErr'
      ], [
      'Codigo'], [
      Nome,
      QtdInn, VlUInn, VlTInn,
      QtdBxa, VlUBxa, VlTBxa,
      QtdRet, VlURet, VlTRet,
      QtdStq, VlUStq, VlTStq,
      CodErr, TxtErr, QtdErr
      ], [
      Codigo], False);
      //
      QrPQ1.Next;
    end;
    //
    SbImprime.Enabled := True;
    BtPrepara.Enabled := False;
    PB1.Position := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrBaixar, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + FPQRMul,
    'WHERE QtdRet >= 0.001 ',
    '']);
    BtRetorna.Enabled := QrBaixar.RecordCount > 0;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    ImprimeStatus();
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPQRMul.BtRetornaClick(Sender: TObject);
const
  Ocorrencia = VAR_PQR_OCORR_EMI_RET;
var
(*
  //
  ValVar: Variant;
  ValorMin, ValorMax: String;
*)
  Insumo: Integer;
  AllQtd, Peso, Valor, (*RetQtd, ValMax,*) ValInn, ValBxa: Double;
  OriCodiInn, OriCtrlInn, TipoInn, OriCodiOut, OriCtrlOut, TipoOut,
  Controle: Integer;
begin
  try
    QrBaixar.DisableControls;
    DBGRet.Enabled := False;
    //
    PB1.Position := 0;
    PB1.Max := QrBaixar.RecordCount;
    QrBaixar.First;
    while not QrBaixar.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      Insumo := QrBaixarCodigo.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQRInn, Dmod.MyDB, [
      'SELECT pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo,',
      'pqx.DataX, (pqx.Peso + pqx.RetQtd) AbertoKg, ',
      'IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) PRECO,',
      'pqx.RetQtd, pqe.NF_CC ',
      'FROM pqx',
      'LEFT JOIN pqe pqe ON pqe.Codigo=pqx.OrigemCodi',
      'WHERE pqx.CliDest=' + Geral.FF0(FCliente),
      'AND pqx.Insumo=' + Geral.FF0(Insumo),
      'AND pqx.RetQtd + pqx.Peso <> 0',
      'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
      'AND pqx.Peso > 0 ',
      'ORDER BY pqx.DataX',
      '']);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQRBxa, Dmod.MyDB, [
      'SELECT pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo,',
      'pqx.DataX, -(pqx.RetQtd + pqx.Peso) AbertoKg, ',
      'IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) PRECO,',
      'pqx.OrigemCodi',
      'FROM pqx pqx',
      'WHERE pqx.CliDest=' + Geral.FF0(FCliente),
      'AND pqx.Insumo=' + Geral.FF0(Insumo),
      'AND pqx.RetQtd + pqx.Peso <> 0',
      'AND pqx.Peso < 0 ',
      'AND (',
      '    pqx.Tipo = -1 OR pqx.Tipo > 100',
      '    ) ',
      'ORDER BY pqx.DataX, pqx.OrigemCodi',
      '']);
      //
////////////////////////////////////////////////////////////////////////////////
      AllQtd := QrBaixarQtdRet.Value;
      ValInn := QrPQRInnAbertoKg.Value;
      ValBxa := QrPQRBxaAbertoKg.Value;
      while AllQtd >= 0.001 do
      begin
        //
        Peso  := dmkPF.MenorFloat([AllQtd, ValInn, ValBxa]);
        Valor := Peso * QrPQRInnPRECO.Value;
        //
        OriCodiInn := QrPQRInnOrigemCodi.Value;
        OriCtrlInn := QrPQRInnOrigemCtrl.Value;
        TipoInn    := QrPQRInnTipo.Value;
        OriCodiOut := QrPQRBxaOrigemCodi.Value;
        OriCtrlOut := QrPQRBxaOrigemCtrl.Value;
        TipoOut    := QrPQRBxaTipo.Value;
        //
        Controle := Dmod.PQR_InsereItemPQRIts(FCodigo,
          OriCodiInn, OriCtrlInn, TipoInn,
          OriCodiOut, OriCtrlOut, TipoOut,
          Insumo, Peso, Valor, Ocorrencia);
        //
        AllQtd := AllQtd - Peso;
        ValInn := ValInn - Peso;
        ValBxa := ValBxa - Peso;
        //
        if ValInn < 0.001 then
        begin
          QrPQRInn.Next;
          //
          ValInn := QrPQRInnAbertoKg.Value;
        end;
        //
        if ValBxa < 0.001 then
        begin
          QrPQRBxa.Next;
          //
          ValBxa := QrPQRBxaAbertoKg.Value;
        end;
      end;
////////////////////////////////////////////////////////////////////////////////
      //
      QrBaixar.Next;
    end;
    QrBaixar.Close;
  finally
    QrBaixar.EnableControls;
    DBGRet.Enabled := True;
    PB1.Position := 0;
    Close;
  end;
end;

procedure TFmPQRMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQRMul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQRMul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPQRMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQRMul.frxQUI_RETOR_006_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_CLIINT' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FNO_Cli, FCliente, 'TODAS')
  else
  if VarName = 'VARF_DATA' then
    Value := Now()
  else

end;

procedure TFmPQRMul.ImprimeStatus;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRMul, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FPQRMul,
  'WHERE Codigo > 0',
  'AND (',
  '       QtdBxa <> 0 ',
  '    OR QtdInn <> 0',
  '    OR QtdStq <> 0',
  ')',
  'ORDER BY CodErr, QtdErr, Nome',
  '']);
  //
  MyObjects.frxDefineDataSets(frxQUI_RETOR_006_01, [
  DModG.frxDsDono,
  frxDsPQRMul
  ]);
  //
  MyObjects.frxMostra(frxQUI_RETOR_006_01, 'Status Pr�-retorno');
end;

procedure TFmPQRMul.ReopenPQ(Todos: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQ1, Dmod.MyDB, [
  'SELECT DISTINCT pq.Codigo, pq.Nome',
  'FROM pqfor pfo ',
  'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo ',
  Geral.ATS_If(not Todos, ['WHERE pfo.CI=' + Geral.FF0(FCliente)]),
  'ORDER BY pq.Nome ',
  '']);
end;


procedure TFmPQRMul.SbImprimeClick(Sender: TObject);
begin
  ImprimeStatus();
end;

end.
