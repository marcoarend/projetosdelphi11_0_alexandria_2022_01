unit NFeMPAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkDBGrid, DB, DmkDAC_PF,
  mySQLDbTables, Mask, DBCtrls, dmkImage, UnDmkEnums;

type
  TFmNFeMPAdd = class(TForm)
    Panel1: TPanel;
    DBGAdd: TdmkDBGrid;
    QrTot: TmySQLQuery;
    DsTot: TDataSource;
    QrTotPecas: TFloatField;
    QrTotPLE: TFloatField;
    QrTotCMPValor: TFloatField;
    Panel3: TPanel;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    QrTotAtivos: TLargeintField;
    QrTotM2: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGAddCellClick(Column: TColumn);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrTotBeforeClose(DataSet: TDataSet);
    procedure QrTotAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  procedure AtivarTodos(Ativo: Byte);
  procedure ReopenTot();

  public
    { Public declarations }
  end;

  var
  FmNFeMPAdd: TFmNFeMPAdd;

implementation

uses UnMyObjects, NFeMPInn, ModuleGeral, Module;

{$R *.DFM}

procedure TFmNFeMPAdd.AtivarTodos(Ativo: Byte);
var
  Conta: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Conta := FmNFeMPInn.QrNFeMPAddConta.Value;
    DmodG.QrUpdPID1.Close;
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FmNFeMPInn.FNFeMPAdd + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Ativo=:P0');
    DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
    DmodG.QrUpdPID1.ExecSQL;
    //
    FmNFeMPInn.ReopenQrNFeMPAdd(Conta);
    ReopenTot();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFeMPAdd.BtNenhumClick(Sender: TObject);
begin
  AtivarTodos(0);
end;

procedure TFmNFeMPAdd.BtOKClick(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE mpinits SET ');
    Dmod.QrUpd.SQL.Add('NF_Inn_Its=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Conta=:P1 ');
    //
    Codigo   := FmNFeMPInn.QrNFeMPInnCodigo.Value;
    Controle := FmNFeMPInn.QrNFeMPItsControle.Value;
    FmNFeMPInn.QrNFeMPAdd.First;
    while not FmNFeMPInn.QrNFeMPAdd.Eof do
    begin
      if FmNFeMPInn.QrNFeMPAddAtivo.Value = 1 then
      begin
        Dmod.QrUpd.Params[00].AsInteger := Controle;
        Dmod.QrUpd.Params[01].AsInteger := FmNFeMPInn.QrNFeMPAddConta.Value;
        Dmod.QrUpd.ExecSQL;
      end;
      //
      FmNFeMPInn.QrNFeMPAdd.Next;
    end;
    //
    FmNFeMPInn.AtualizaItem(Controle, Codigo);
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFeMPAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeMPAdd.BtTodosClick(Sender: TObject);
begin
  AtivarTodos(1);
end;

procedure TFmNFeMPAdd.DBGAddCellClick(Column: TColumn);
var
  Conta, Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Screen.Cursor := crHourGlass;
    try
      Conta := FmNFeMPInn.QrNFeMPAddConta.Value;
      if FmNFeMPInn.QrNFeMPAddAtivo.Value = 0 then Ativo := 1 else Ativo := 0;
      DmodG.QrUpdPID1.Close;
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FmNFeMPInn.FNFeMPAdd + ' SET ');
      DmodG.QrUpdPID1.SQL.Add('Ativo=:P0');
      DmodG.QrUpdPID1.SQL.Add('WHERE Conta=:P1');
      DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
      DmodG.QrUpdPID1.Params[01].AsInteger := Conta;
      DmodG.QrUpdPID1.ExecSQL;
      //
      FmNFeMPInn.ReopenQrNFeMPAdd(Conta);
      ReopenTot();
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmNFeMPAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeMPAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeMPAdd.QrTotAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := QrTotAtivos.Value > 0;
end;

procedure TFmNFeMPAdd.QrTotBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

procedure TFmNFeMPAdd.ReopenTot;
begin
  QrTot.Database := DModG.MyPID_DB;
  QrTot.Close;
  UnDmkDAC_PF.AbreQuery(QrTot, DModG.MyPID_DB);
end;

end.
