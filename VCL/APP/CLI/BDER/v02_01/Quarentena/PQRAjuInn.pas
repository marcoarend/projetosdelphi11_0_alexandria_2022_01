unit PQRAjuInn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, dmkDBGridZTO, dmkEditDateTimePicker;

type
  TFmPQRAjuInn = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaTitulo1C: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesNome: TWideStringField;
    QrClientesCodigo: TIntegerField;
    DsClientes: TDataSource;
    PnCliente: TPanel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label3: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Label27: TLabel;
    SbSelArq: TSpeedButton;
    SbAbre: TSpeedButton;
    EdArq: TdmkEdit;
    Grade1: TStringGrid;
    TabSheet2: TTabSheet;
    QrPQRAjuInn: TmySQLQuery;
    DsPQRAjuInn: TDataSource;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    QrNaoLoc: TmySQLQuery;
    DsNaoLoc: TDataSource;
    QrPQRAjuInnData: TDateField;
    QrPQRAjuInnNF: TIntegerField;
    QrPQRAjuInnCodigo: TWideStringField;
    QrPQRAjuInnNome: TWideStringField;
    QrPQRAjuInnUnd: TWideStringField;
    QrPQRAjuInnQtd: TFloatField;
    QrPQRAjuInnVlUni: TFloatField;
    QrPQRAjuInnVlTot: TFloatField;
    QrPQRAjuInnNO_Cliente: TWideStringField;
    QrPQRAjuInnCFOP: TWideStringField;
    QrPQRAjuInnAtivo: TSmallintField;
    QrNaoLocCodigo: TWideStringField;
    QrNaoLocNome: TWideStringField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAbrir: TBitBtn;
    GroupBox2: TGroupBox;
    Panel8: TPanel;
    BitBtn1: TBitBtn;
    Panel9: TPanel;
    BtImporta: TBitBtn;
    QrPQRCab: TmySQLQuery;
    QrPQRCabData: TDateField;
    QrPQRCabNF: TIntegerField;
    QrPQRCabQTD: TFloatField;
    QrPQRCabVLTOT: TFloatField;
    QrLoc1: TmySQLQuery;
    QrLoc1Codigo: TIntegerField;
    QrPQRIts: TmySQLQuery;
    QrPQRItsData: TDateField;
    QrPQRItsNF: TIntegerField;
    QrPQRItsCodigo: TWideStringField;
    QrPQRItsNome: TWideStringField;
    QrPQRItsUnd: TWideStringField;
    QrPQRItsQtd: TFloatField;
    QrPQRItsVlUni: TFloatField;
    QrPQRItsVlTot: TFloatField;
    QrPQRItsNO_Cliente: TWideStringField;
    QrPQRItsCFOP: TWideStringField;
    QrPQRItsAtivo: TSmallintField;
    PB1: TProgressBar;
    PB2: TProgressBar;
    QrPQCli_: TmySQLQuery;
    QrPQCli_PQ: TIntegerField;
    TPFinal: TdmkEditDateTimePicker;
    Label1: TLabel;
    QrPQRItsPQ: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbSelArqClick(Sender: TObject);
    procedure SbAbreClick(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure QrPQRAjuInnAfterOpen(DataSet: TDataSet);
    procedure QrPQRAjuInnBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FPQRAjuInn: String;
  public
    { Public declarations }
  end;

  var
  FmPQRAjuInn: TFmPQRAjuInn;

implementation

uses UnMyObjects, Module, DmkDAC_PF, CreateBlueDerm, ModuleGeral, UMySQLModule,
BlueDermConsts;

{$R *.DFM}

procedure TFmPQRAjuInn.BtAbrirClick(Sender: TObject);
var
  I, K: Integer;
  _Data, _NF, _Codigo, _Nome, _Unid, _Qtde, _VlUni, _VlTot, _NO_Cli, _CFOP: String;
var
  Data, Codigo, Nome, Und, NO_Cliente, CFOP: String;
  NF: Integer;
  Qtd, VlUni, VlTot: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    QrPQRAjuInn.Close;
    QrNaoLoc.Close;
    //
    FPQRAjuInn :=
      UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQRAjuInn, DModG.QrUpdPID1, False);
    //
    PB1.Position := 0;
    PB1.Max := Grade1.RowCount;
    for I := 1 to Grade1.RowCount do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      _Data    := Trim(Grade1.Cells[01, I]);
      _NF      := Trim(Grade1.Cells[02, I]);
      _Codigo  := Trim(Grade1.Cells[03, I]);
      _Nome    := Trim(Grade1.Cells[04, I]);
      _Unid    := Trim(Grade1.Cells[05, I]);
      _Qtde    := Trim(Grade1.Cells[06, I]);
      _VlUni   := Trim(Grade1.Cells[07, I]);
      _VlTot   := Trim(Grade1.Cells[08, I]);
      _NO_Cli  := Trim(Grade1.Cells[09, I]);
      _CFOP    := Trim(Grade1.Cells[10, I]);
      //
      if (_Data <> '') and (_Codigo <> '') and (_Qtde <> '') then
      begin
        K := pos('/', _NF);
        if K > 0 then
          _NF := Copy(_NF, 1, K -1);
        //
        Data := Geral.FDT(Geral.ValidaDataBR(_Data, False, False), 1);
        NF             := Geral.IMV(_NF);
        Codigo         := _Codigo;
        Nome           := _Nome;
        Und            := _Unid;
        Qtd            := Geral.DMV(_Qtde);
        VlUni          := Geral.DMV(_VlUni);
        VlTot          := Geral.DMV(_VlTot);
        NO_Cliente     := _NO_Cli;
        CFOP           := _CFOP;
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FPQRAjuInn, False, [
        'Data', 'NF', 'Codigo',
        'Nome', 'Und', 'Qtd',
        'VlUni', 'VlTot', 'NO_Cliente',
        'CFOP'], [
        ], [
        Data, NF, Codigo,
        Nome, Und, Qtd,
        VlUni, VlTot, NO_Cliente,
        CFOP], [
        ], False);
      end;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNaoLoc, DModG.MyPID_DB, [
    'SELECT DISTINCT pai.Codigo, pai.Nome ',
    'FROM ' + FPQRAjuInn + ' pai ',
    'LEFT JOIN ' + TMeuDB + '.pqcli pqc ON pqc.CodProprio=pai.Codigo ',
    'WHERE pqc.Controle IS NULL ',
    'ORDER BY pai.Codigo, pai.Nome ',
    '']);
    if QrNaoLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Importa��o cancelada!' + sLineBreak +
      Geral.FF0(QrNaoLoc.RecordCount) + ' insumos n�o foram localizados');
      //
      PageControl1.ActivePageIndex := 1;
      Exit;
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQRAjuInn, DModG.MyPID_DB, [
      'SELECT * ',
      'FROM ' + FPQRAjuInn,
      'ORDER BY Data, NF, Codigo  ',
      '']);
      PageControl1.ActivePageIndex := 2;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPQRAjuInn.BtImportaClick(Sender: TObject);
const
  Tipo = VAR_FATID_0010;
  HowLoad = CO_PQ_LOAD_001_AJUINN;
var
  //DataS,
  Data, DataE, Cancelado, refNFe: String;
  CliInt, Codigo, IQ, CI, NF, TipoNF, NF_RP, NF_CC, Volumes: Integer;
  Frete, PesoB, PesoL, ValorNF, RICMS, RICMSF, Juros, ValProd: Double;
  Continua: Boolean;
  //
var
  Controle, Conta, Insumo: Integer;
  PesoVB, PesoVL, ValorItem, TotalCusto, TotalPeso: Double;
var
  DataX, DataF: String;
  OrigemCodi, OrigemCtrl, CliOrig, CliDest: Integer;
  Peso, Valor: Double;
begin
  CliInt := EdCliente.ValueVariant;
  if MyObjects.FIC(CliInt = 0, EdCliente, 'Informe o cliente interno') then
    Exit;
  if MyObjects.FIC(TPFinal.Date < 2, TPFinal, 'Informe a data limite (final)!') then
    Exit;
  //
  PnCliente.Enabled := QrPQRAjuInn.RecordCount = 0;
  //
  DataF := Geral.FDT(TPFinal.Date, 1);
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRCab, DModG.MyPID_DB, [
  'SELECT Data, NF, SUM(Qtd) QTD, SUM(VlTot) VLTOT',
  'FROM ' + FPQRAjuInn,
  'WHERE Data <="' + DataF + '" ',
  'GROUP BY Data, NF ',
  'ORDER BY Data, NF',
  '']);
  PB1.Position := 0;
  PB1.Max := QrPQRCab.RecordCount;
  QrPQRCab.First;
  while not QrPQRCab.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    Continua := False;
    //
    Codigo         := 0;
    Data           := Geral.FDT(QrPQRCabData.Value, 1);
    IQ             := CliInt;
    CI             := CliInt;
    NF             := QrPQRCabNF.Value;
    Frete          := 0;
    PesoB          := 0;
    PesoL          := QrPQRCabQTD.Value;
    ValorNF        := QrPQRCabVLTOT.Value;
    RICMS          := 0;
    RICMSF         := 0;
    DataE          := Data;
    Juros          := 0;
    Cancelado      := '';
    TipoNF         := 0;
    refNFe         := '';
    ValProd        := ValorNF;
    Volumes        := 1;
    NF_RP          := NF;
    NF_CC          := NF;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc1, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM pqe ',
    'WHERE Data="' + Data + '" ',
    'AND CI=' + Geral.FF0(CliInt),
    'AND NF=' + Geral.FF0(NF),
    '']);
    if QrLoc1Codigo.Value <> 0 then
    begin
      Codigo := QrLoc1Codigo.Value;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM pqx ',
      'WHERE Tipo=' + Geral.FF0(Tipo),
      'AND OrigemCodi=' + Geral.FF0(Codigo),
      '']);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM pqeits ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      //
      Continua := True;
    end else
    begin
      Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'PQE', 'PQE', 'Codigo');
      Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqe', False, [
      'Data', 'IQ', 'CI',
      'NF', 'Frete',
      'PesoB', 'PesoL', 'ValorNF',
      'RICMS', 'RICMSF',
      'DataE', 'Juros',
      'TipoNF',
      'refNFe',
      'NF_RP', 'NF_CC', 'HowLoad'
      ], [
      'Codigo'], [
      Data, IQ, CI,
      NF, Frete,
      PesoB, PesoL, ValorNF,
      RICMS, RICMSF,
      DataE, Juros,
      TipoNF,
      refNFe,
      NF_RP, NF_CC, HowLoad
      ], [Codigo], True);
    end;
    if Continua then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQRIts, DModG.MyPID_DB, [
(*
      'SELECT * ',
      'FROM ' + FPQRAjuInn,
      'WHERE Data="' + Data + '" ',
      'AND NF=' + Geral.FF0(NF),
      'ORDER BY Codigo ',
*)
      'SELECT pqc.PQ, inn.*  ',
      'FROM _pqrajuinn_ inn ',
      'LEFT JOIN ' + TMeuDB + '.pqcli pqc ON  ',
      '     pqc.CodProprio=inn.Codigo ',
      '     AND pqc.CI=' + Geral.FF0(CliInt),
      'WHERE inn.Data="' + Data + '" ',
      'AND inn.NF=' + Geral.FF0(NF),
      'ORDER BY inn.Codigo  ',
      '']);
      PB2.Position := 0;
      PB2.Max := QrPQRIts.RecordCount;
      QrPQRIts.First;
      Conta := 0;
      while not QrPQRIts.Eof do
      begin
        MyObjects.UpdPB(PB2, nil, nil);
        //
(*
        UnDmkDAC_PF.AbreMySQLQuery0(QrPQCli, Dmod.MyDB, [
        'SELECT PQ ',
        'FROM pqcli ',
        'WHERE CI=' + Geral.FF0(CliInt),
        'AND CodProprio="' + QrPQRItsCodigo.Value + '" ',
        '']);
        if QrPQCli.RecordCount <> 1 then
        begin
          Geral.MB_Aviso('ERRO! Localizado(s) ' + Geral.FF0(QrPQCli.RecordCount) +
          ' relacionamentos de cadastros para o insumo: ' + sLineBreak +
          QrPQRItsCodigo.Value + ': ' + QrPQRItsNome.Value);
        end;
*)
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'PQEIts','PQEIts','Controle');
        //
        Conta          := Conta + 1;
        //Insumo         := QrPQCliPQ.Value;
        Insumo         := QrPQRItsPQ.Value;
        Volumes        := 1;
        PesoVB         := 0;
        PesoVL         := QrPQRItsQtd.Value;
        ValorItem      := QrPQRItsVlTot.Value;
        TotalCusto     := QrPQRItsVlTot.Value;
        TotalPeso      := QrPQRItsQtd.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqeits', False, [
        'Codigo', 'Conta', 'Insumo',
        'Volumes', 'PesoVB', 'PesoVL',
        'ValorItem', 'TotalCusto', 'TotalPeso',
        'HowLoad'], [
        'Controle'], [
        Codigo, Conta, Insumo,
        Volumes, PesoVB, PesoVL,
        ValorItem, TotalCusto, TotalPeso,
        HowLoad], [
        Controle], True) then
        begin
          DataX          := Data;
          OrigemCodi     := Codigo;
          OrigemCtrl     := Controle;
          //Tipo           := ;
          CliOrig        := CliInt;
          CliDest        := CliInt;
          //Insumo         := ;
          Peso           := TotalPeso;
          Valor          := TotalCusto;
          //Retorno        := ;
          //StqMovIts      := ;
          //RetQtd         := ;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqx', False, [
          'DataX', 'CliOrig', 'CliDest',
          'Insumo', 'Peso', 'Valor',
          //, 'Retorno', 'StqMovIts', 'RetQtd'
          'HowLoad'
          ], [
          'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
          DataX, CliOrig, CliDest,
          Insumo, Peso, Valor,
          //, Retorno, StqMovIts, RetQtd
          HowLoad
          ], [
          OrigemCodi, OrigemCtrl, Tipo], False);
          //
          QrPQRIts.Next;
        end;
      end;
    end;
    //
    QrPQRCab.Next;
  end;
end;

procedure TFmPQRAjuInn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQRAjuInn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQRAjuInn.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  //
  Geral.MB_Aviso(
  'Antes de usar esta janela � aconselh�vel:' + sLineBreak +
  '1. Imprimir o estoque atual antes da importa��o.' + sLineBreak +
  '2. Importar entradas e saidas.' + sLineBreak +
  '3. Imprimir o estoque atual novamente e comparar com a primeira impress�o.!');
end;

procedure TFmPQRAjuInn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQRAjuInn.QrPQRAjuInnAfterOpen(DataSet: TDataSet);
begin
  BtImporta.Enabled := QrPQRAjuInn.RecordCount > 0;
end;

procedure TFmPQRAjuInn.QrPQRAjuInnBeforeClose(DataSet: TDataSet);
begin
  BtImporta.Enabled := False;
end;

procedure TFmPQRAjuInn.SbAbreClick(Sender: TObject);
const
  ColIni = 1;
  RowIni = 1;
  ExcluiLinhas = True;
begin
  MyObjects.Xls_To_StringGrid(Grade1, EdArq.Text, PB1, LaAviso1, LaAviso2);
  BtAbrir.Enabled := Grade1.RowCount > 2;
end;

procedure TFmPQRAjuInn.SbSelArqClick(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Sele��o de arquivo', '', [], Arquivo) then
    EdArq.ValueVariant := Arquivo
  else
    EdArq.ValueVariant := '';
end;

end.
