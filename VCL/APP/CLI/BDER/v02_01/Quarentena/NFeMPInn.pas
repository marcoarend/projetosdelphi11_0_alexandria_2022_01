unit NFeMPInn;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, dmkDBLookupComboBox,
  dmkEditCB, Grids, DBGrids, dmkValUsu, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmNFeMPInn = class(TForm)
    PainelDados: TPanel;
    DsNFeMPInn: TDataSource;
    QrNFeMPInn: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrNFeMPInnCodigo: TIntegerField;
    QrNFeMPInnCodUsu: TIntegerField;
    QrNFeMPInnNome: TWideStringField;
    PMMaster: TPopupMenu;
    IncluiMaster1: TMenuItem;
    AlteraMaster1: TMenuItem;
    ExcluiMaster1: TMenuItem;
    PMDetail: TPopupMenu;
    QrNFeMPAdd: TmySQLQuery;
    QrNFeMPAddEmissao: TDateField;
    QrNFeMPAddFornece: TIntegerField;
    QrNFeMPAddCaminhao: TIntegerField;
    QrNFeMPAddPecas: TFloatField;
    QrNFeMPAddPLE: TFloatField;
    QrNFeMPAddCMPValor: TFloatField;
    QrNFeMPAddNO_FORNECE: TWideStringField;
    QrNFeMPAddNO_CLIINT: TWideStringField;
    QrNFeMPAddConta: TIntegerField;
    QrNFeMPAddAtivo: TSmallintField;
    DsNFeMPAdd: TDataSource;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    Label5: TLabel;
    QrDonos: TmySQLQuery;
    QrDonosCodigo: TIntegerField;
    QrDonosNOMEENTIDADE: TWideStringField;
    DsDonos: TDataSource;
    DsFornece: TDataSource;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTIDADE: TWideStringField;
    QrFornecePreDescarn: TSmallintField;
    QrForneceMaskLetras: TWideStringField;
    QrForneceMaskFormat: TWideStringField;
    QrForneceCorretor: TIntegerField;
    QrForneceComissao: TFloatField;
    QrForneceDescAdiant: TFloatField;
    QrForneceCondPagto: TWideStringField;
    QrForneceCondComis: TWideStringField;
    QrForneceLocalEntrg: TWideStringField;
    QrForneceAbate: TIntegerField;
    QrForneceTransporte: TIntegerField;
    QrNFeMPInnEmpresa: TIntegerField;
    QrNFeMPInnFornece: TIntegerField;
    QrNFeMPInnPecas: TFloatField;
    QrNFeMPInnPLE: TFloatField;
    QrNFeMPInnCMPValor: TFloatField;
    QrNFeMPInnNO_EMPRESA: TWideStringField;
    QrNFeMPInnNO_FORNECE: TWideStringField;
    DBGMPInnIts: TDBGrid;
    QrMPInIts: TmySQLQuery;
    QrMPInItsCodigo: TIntegerField;
    QrMPInItsControle: TIntegerField;
    QrMPInItsConta: TIntegerField;
    QrMPInItsPecas: TFloatField;
    QrMPInItsPecasNF: TFloatField;
    QrMPInItsM2: TFloatField;
    QrMPInItsPNF: TFloatField;
    QrMPInItsPLE: TFloatField;
    QrMPInItsPDA: TFloatField;
    QrMPInItsRecorte_PDA: TFloatField;
    QrMPInItsPTA: TFloatField;
    QrMPInItsRecorte_PTA: TFloatField;
    QrMPInItsRaspa_PTA: TFloatField;
    QrMPInItsLk: TIntegerField;
    QrMPInItsDataCad: TDateField;
    QrMPInItsDataAlt: TDateField;
    QrMPInItsUserCad: TIntegerField;
    QrMPInItsUserAlt: TIntegerField;
    QrMPInItsAlterWeb: TSmallintField;
    QrMPInItsAtivo: TSmallintField;
    QrMPInItsCaminhao: TIntegerField;
    QrMPInItsCMPValor: TFloatField;
    QrMPInItsCMPFrete: TFloatField;
    QrMPInItsCPLValor: TFloatField;
    QrMPInItsCPLFrete: TFloatField;
    QrMPInItsAGIValor: TFloatField;
    QrMPInItsAGIFrete: TFloatField;
    QrMPInItsCMIValor: TFloatField;
    QrMPInItsCMIFrete: TFloatField;
    QrMPInItsTotalInfo: TFloatField;
    QrMPInItsFreteInfo: TFloatField;
    QrMPInItsCustoInfo: TFloatField;
    QrMPInItsFinalInfo: TFloatField;
    QrMPInItsDolar: TFloatField;
    QrMPInItsEuro: TFloatField;
    QrMPInItsIndexador: TFloatField;
    QrMPInItsCMPDevol: TFloatField;
    QrMPInItsCPLDevol: TFloatField;
    QrMPInItsAGIDevol: TFloatField;
    QrMPInItsCMIDevol: TFloatField;
    QrMPInItsCMPUnida: TSmallintField;
    QrMPInItsCMPPreco: TFloatField;
    QrMPInItsSEQ: TIntegerField;
    QrMPInItsQUANTIDADE: TFloatField;
    QrMPInItsCMPMoeda: TSmallintField;
    QrMPInItsCMPCotac: TFloatField;
    QrMPInItsCMPReais: TFloatField;
    QrMPInItsFornece: TIntegerField;
    QrMPInItsPNF_Fat: TFloatField;
    QrMPInItsNOMEPROCEDENCIA: TWideStringField;
    QrMPInItsEmissao: TDateField;
    QrMPInItsVencto: TDateField;
    QrMPInItsNF: TIntegerField;
    QrMPInItsConheci: TIntegerField;
    QrMPInItsEmisFrete: TDateField;
    QrMPInItsVctoFrete: TDateField;
    QrMPInItsVENCTO_TXT: TWideStringField;
    QrMPInItsVCTOFRETE_TXT: TWideStringField;
    QrMPInItsEMISFRETE_TXT: TWideStringField;
    QrMPInItsPS_ValUni: TFloatField;
    QrMPInItsPS_QtdTot: TFloatField;
    QrMPInItsPS_ValTot: TFloatField;
    QrMPInItsNF_Inn_Fut: TSmallintField;
    DsMPInIts: TDataSource;
    QrNFeMPInnAreaM2: TFloatField;
    QrNFeMPInnAreaP2: TFloatField;
    PMNFe: TPopupMenu;
    RecriatodaNFe1: TMenuItem;
    QrNFeMPInnEncerrou: TDateTimeField;
    QrNFeMPIts: TmySQLQuery;
    DsNFeMPIts: TDataSource;
    QrNFeMPItsPecas: TFloatField;
    QrNFeMPItsPLE: TFloatField;
    QrNFeMPItsAreaM2: TFloatField;
    QrNFeMPItsAreaP2: TFloatField;
    QrNFeMPItsCMPValor: TFloatField;
    PMSubDetail: TPopupMenu;
    AdicionaSubDetail1: TMenuItem;
    RetiraSubDetail1: TMenuItem;
    QrSum: TmySQLQuery;
    QrSumPecas: TFloatField;
    QrSumPLE: TFloatField;
    QrSumCMPValor: TFloatField;
    QrSumAreaM2: TFloatField;
    QrSumAreaP2: TFloatField;
    QrSumAtivos: TLargeintField;
    IncluiDetail1: TMenuItem;
    AlteraDetail1: TMenuItem;
    ExcluiDetail1: TMenuItem;
    DBGNFeMPIts: TDBGrid;
    QrNFeMPAddM2: TFloatField;
    QrTot: TmySQLQuery;
    QrTotPecas: TFloatField;
    QrTotM2: TFloatField;
    QrTotCMPValor: TFloatField;
    QrTotPLE: TFloatField;
    QrMPInItsNF_Inn_Its: TIntegerField;
    QrNFeMPItsNO_GG1: TWideStringField;
    QrNFeMPItsNO_TAM: TWideStringField;
    QrNFeMPItsNO_COR: TWideStringField;
    QrNFeMPItsCodigo: TIntegerField;
    QrNFeMPItsControle: TIntegerField;
    QrNFeMPItsGraGruX: TIntegerField;
    QrNFeMPItsGerBxaEstq: TIntegerField;
    EdRegrFiscal: TdmkEditCB;
    CBRegrFiscal: TdmkDBLookupComboBox;
    SbRegrFiscal: TSpeedButton;
    QrNFeMPInnRegrFiscal: TIntegerField;
    QrNFeMPInnNO_RegrFiscal: TWideStringField;
    QrNFeMPInnModeloNF: TIntegerField;
    QrNFeMPInninfAdFisco: TWideStringField;
    QrNFeMPInnFinanceiro: TSmallintField;
    QrNFeMPInnEMP_UF: TLargeintField;
    QrNFeMPInnFRN_TIPO: TSmallintField;
    QrNFeMPInnFRN_IE: TWideStringField;
    QrNFeMPInnFRN_UF: TLargeintField;
    QrNFeMPInnNOMEEMP: TWideStringField;
    QrNFeMPInnFilial: TIntegerField;
    QrNFeMPInnAssociada: TIntegerField;
    QrNFeMPInnEMP_CtaServico: TIntegerField;
    QrNFeMPInnEMP_FaturaSeq: TSmallintField;
    QrNFeMPInnEMP_FaturaSep: TWideStringField;
    QrNFeMPInnEMP_FaturaDta: TSmallintField;
    QrNFeMPInnEMP_TxtServico: TWideStringField;
    QrNFeMPInnEMP_DupServico: TWideStringField;
    QrNFeMPInnEMP_FILIAL: TIntegerField;
    QrNFeMPInnASS_FILIAL: TIntegerField;
    QrNFeMPInnASS_CO_UF: TLargeintField;
    QrNFeMPInnASS_CtaServico: TIntegerField;
    QrNFeMPInnASS_FaturaSeq: TSmallintField;
    QrNFeMPInnASS_FaturaSep: TWideStringField;
    QrNFeMPInnASS_FaturaDta: TSmallintField;
    QrNFeMPInnASS_TxtServico: TWideStringField;
    QrNFeMPInnASS_DupServico: TWideStringField;
    QrNFeMPInnUF_TXT_Emp: TWideStringField;
    QrNFeMPInnUF_TXT_Cli: TWideStringField;
    EdAbertura: TdmkEdit;
    Label17: TLabel;
    QrNFeMPInnAbertura: TDateTimeField;
    VuRegrFiscal: TdmkValUsu;
    QrNFeMPInnCU_RegrFiscal: TIntegerField;
    Label20: TLabel;
    QrFisRegCad: TmySQLQuery;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadModeloNF: TIntegerField;
    QrFisRegCadNO_MODELO_NF: TWideStringField;
    DsFisRegCad: TDataSource;
    QrFatPedNFs: TmySQLQuery;
    QrFatPedNFsFilial: TIntegerField;
    QrFatPedNFsIDCtrl: TIntegerField;
    QrFatPedNFsTipo: TSmallintField;
    QrFatPedNFsOriCodi: TIntegerField;
    QrFatPedNFsEmpresa: TIntegerField;
    QrFatPedNFsNumeroNF: TIntegerField;
    QrFatPedNFsIncSeqAuto: TSmallintField;
    QrFatPedNFsAlterWeb: TSmallintField;
    QrFatPedNFsAtivo: TSmallintField;
    QrFatPedNFsSerieNFCod: TIntegerField;
    QrFatPedNFsSerieNFTxt: TWideStringField;
    QrFatPedNFsCO_ENT_EMP: TIntegerField;
    QrFatPedNFsDataCad: TDateField;
    QrFatPedNFsDataAlt: TDateField;
    QrFatPedNFsDataAlt_TXT: TWideStringField;
    QrFatPedNFsFreteVal: TFloatField;
    QrFatPedNFsSeguro: TFloatField;
    QrFatPedNFsOutros: TFloatField;
    QrFatPedNFsPlacaUF: TWideStringField;
    QrFatPedNFsPlacaNr: TWideStringField;
    QrFatPedNFsEspecie: TWideStringField;
    QrFatPedNFsMarca: TWideStringField;
    QrFatPedNFsNumero: TWideStringField;
    QrFatPedNFskgBruto: TFloatField;
    QrFatPedNFskgLiqui: TFloatField;
    QrFatPedNFsQuantidade: TWideStringField;
    QrFatPedNFsObservacao: TWideStringField;
    QrFatPedNFsCFOP1: TWideStringField;
    QrFatPedNFsDtEmissNF: TDateField;
    QrFatPedNFsDtEntraSai: TDateField;
    QrFatPedNFsStatus: TIntegerField;
    QrFatPedNFsinfProt_cStat: TIntegerField;
    QrFatPedNFsinfAdic_infCpl: TWideMemoField;
    QrFatPedNFsinfCanc_cStat: TIntegerField;
    QrFatPedNFsDTEMISSNF_TXT: TWideStringField;
    QrFatPedNFsDTENTRASAI_TXT: TWideStringField;
    QrFatPedNFsRNTC: TWideStringField;
    QrFatPedNFsUFembarq: TWideStringField;
    QrFatPedNFsxLocEmbarq: TWideStringField;
    DsFatPedNFs: TDataSource;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAStatus: TSmallintField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabAinfCanc_xMotivo: TWideStringField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    QrNFeCabAcStat: TIntegerField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAcStat_xMotivo: TWideStringField;
    DsNFeCabA: TDataSource;
    LaCondicaoPG: TLabel;
    EdCondicaoPG: TdmkEditCB;
    CBCondicaoPG: TdmkDBLookupComboBox;
    Label30: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    Label29: TLabel;
    EdFretePor: TdmkEditCB;
    CBFretePor: TdmkDBLookupComboBox;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    VuPediPrzCab: TdmkValUsu;
    QrTransportas: TmySQLQuery;
    QrTransportasCodigo: TIntegerField;
    QrTransportasNOMEENT: TWideStringField;
    QrTransportasCIDADE: TWideStringField;
    QrTransportasNOMEUF: TWideStringField;
    DsTransportas: TDataSource;
    QrNFeMPInnStatus: TSmallintField;
    QrNFeMPInnDataFat: TDateField;
    QrNFeMPInnFretePor: TSmallintField;
    QrNFeMPInnTransporta: TIntegerField;
    QrNFeMPInnFRETEPOR_TXT: TWideStringField;
    QrNFeMPInnNO_PediPrzCab: TWideStringField;
    QrNFeMPInnCU_PediPrzCab: TIntegerField;
    QrNFeMPInnNO_TRANSPORTA: TWideStringField;
    QrNFeMPInnCondicaoPG: TIntegerField;
    QrNFeMPInnMedDDSimpl: TFloatField;
    QrNFeCabAide_cNF: TIntegerField;
    PMEncerra: TPopupMenu;
    N1: TMenuItem;
    Desfazencerramento1: TMenuItem;
    Encerraentradadecouro1: TMenuItem;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeMPInnENCERROU_TXT: TWideStringField;
    QrNFeMPInnSerieDesfe: TIntegerField;
    QrNFeMPInnNFDesfeita: TIntegerField;
    Label25: TLabel;
    EdCartEmiss: TdmkEditCB;
    CBCartEmiss: TdmkDBLookupComboBox;
    QrNFeMPInnCartEmiss: TIntegerField;
    QrCartEmiss: TmySQLQuery;
    QrCartEmissCodigo: TIntegerField;
    QrCartEmissNome: TWideStringField;
    DsCartEmiss: TDataSource;
    QrFisRegCadFinanceiro: TSmallintField;
    QrNFeMPInnNO_CARTEMISS: TWideStringField;
    QrNFeMPItsNO_GerBxaEstq: TWideStringField;
    QrNFeMPInnTIPOCART: TIntegerField;
    Panel4: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Label18: TLabel;
    DBEdit13: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    Label16: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label21: TLabel;
    DBEdit20: TDBEdit;
    DBEdit17: TDBEdit;
    Label22: TLabel;
    DBEdit16: TDBEdit;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    DBEdit19: TDBEdit;
    DBEdit18: TDBEdit;
    Label24: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    Label14: TLabel;
    DBEdit9: TDBEdit;
    Label15: TLabel;
    DBEdit10: TDBEdit;
    DBEdit8: TDBEdit;
    Label13: TLabel;
    Label19: TLabel;
    DBEdit14: TDBEdit;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit24: TDBEdit;
    DBEdit23: TDBEdit;
    Label28: TLabel;
    DBMemo1: TDBMemo;
    QrNFeCabASTATUS_TXT: TWideStringField;
    QrNFeMPInnEMP_FaturaNum: TSmallintField;
    QrNFeMPInnASS_FaturaNum: TSmallintField;
    QrNFeCabAversao: TFloatField;
    QrFatPedNFsHrEntraSai: TTimeField;
    QrFatPedNFside_dhCont: TDateTimeField;
    QrFatPedNFside_xJust: TWideStringField;
    QrFatPedNFsemit_CRT: TSmallintField;
    QrFatPedNFsdest_email: TWideStringField;
    QrFatPedNFsvagao: TWideStringField;
    QrFatPedNFsbalsa: TWideStringField;
    QrFatPedNFsCompra_XNEmp: TWideStringField;
    QrFatPedNFsCompra_XPed: TWideStringField;
    QrFatPedNFsCompra_XCont: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtMaster: TBitBtn;
    BtDetail: TBitBtn;
    BtSubDetail: TBitBtn;
    BtEncerra: TBitBtn;
    BtNFe: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeMPInnAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeMPInnBeforeOpen(DataSet: TDataSet);
    procedure BtMasterClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure IncluiMaster1Click(Sender: TObject);
    procedure AlteraMaster1Click(Sender: TObject);
    procedure ExcluiMaster1Click(Sender: TObject);
    procedure QrNFeMPInnAfterScroll(DataSet: TDataSet);
    procedure RecriatodaNFe1Click(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure QrNFeMPInnBeforeClose(DataSet: TDataSet);
    procedure QrNFeMPItsBeforeClose(DataSet: TDataSet);
    procedure QrNFeMPItsAfterScroll(DataSet: TDataSet);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure IncluiDetail1Click(Sender: TObject);
    procedure AlteraDetail1Click(Sender: TObject);
    procedure PMDetailPopup(Sender: TObject);
    procedure ExcluiDetail1Click(Sender: TObject);
    procedure PMSubDetailPopup(Sender: TObject);
    procedure BtSubDetailClick(Sender: TObject);
    procedure BtDetailClick(Sender: TObject);
    procedure SbRegrFiscalClick(Sender: TObject);
    procedure QrFatPedNFsAfterScroll(DataSet: TDataSet);
    procedure QrFatPedNFsBeforeClose(DataSet: TDataSet);
    procedure QrNFeCabACalcFields(DataSet: TDataSet);
    procedure BtNFeClick(Sender: TObject);
    procedure Encerraentradadecouro1Click(Sender: TObject);
    procedure Desfazencerramento1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure QrNFeMPInnCalcFields(DataSet: TDataSet);
    procedure PMMasterPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure GerarNFe(Recria: Boolean);
    procedure ReopenFatPedNFs(Filial: Integer);
    procedure EncerraEntradaCouro();
  public
    { Public declarations }
    FNFeMPAdd: String;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenQrNFeMPIts(Controle: Integer);
    procedure ReopenQrNFeMPAdd(Conta: Integer);
    procedure ReopenMPInIts(Conta: Integer);
    procedure AtualizaTotais(Codigo: Integer);
    procedure AtualizaItem(Controle, Codigo: Integer);
  end;

var
  FmNFeMPInn: TFmNFeMPInn;

const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, UCreate, NFeMPAdd, MyDBCheck, QuaisItens,
  ModuleNFe_0000, NFeMPIts, ModPediVda, Principal, Grade_Tabs,
  NFeXMLGerencia, NFe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFeMPInn.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFeMPInn.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeMPInnCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeMPInn.DefParams;
begin
  VAR_GOTOTABELA := 'NFeMPInn';
  VAR_GOTOMYSQLTABLE := QrNFeMPInn;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT car.Nome NO_CARTEMISS, ppc.MedDDSimpl, nmi.*,');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.EUF, emp.PUF) EMP_UF,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('frc.Nome NO_RegrFiscal, frc.CodUsu CU_RegrFiscal,');
  VAR_SQLx.Add('frn.Tipo FRN_TIPO, frn.IE FRN_IE,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.EUF, frn.PUF) FRN_UF,');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP, ');
  VAR_SQLx.Add('emp.Filial, frc.ModeloNF, frc.infAdFisco,');
  VAR_SQLx.Add('frc.Financeiro, car.Tipo TIPOCART, ');
  VAR_SQLx.Add('lfp.Nome FRETEPOR_TXT,');
  VAR_SQLx.Add('ppc.Nome NO_PediPrzCab, ppc.CodUsu CU_PediPrzCab,');
  VAR_SQLx.Add('par.Associada,');
  VAR_SQLx.Add('par.CtaServico EMP_CtaServico,');
  VAR_SQLx.Add('par.FaturaSeq EMP_FaturaSeq,');
  VAR_SQLx.Add('par.FaturaSep EMP_FaturaSep,');
  VAR_SQLx.Add('par.FaturaDta EMP_FaturaDta,');
  VAR_SQLx.Add('par.FaturaNum EMP_FaturaNum,');
  VAR_SQLx.Add('par.TxtServico EMP_TxtServico,');
  VAR_SQLx.Add('par.DupServico  EMP_DupServico,');
  VAR_SQLx.Add('emp.Filial EMP_FILIAL,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('ase.Filial ASS_FILIAL,');
  VAR_SQLx.Add('IF(ase.Tipo=0, ase.EUF, ase.PUF) ASS_CO_UF,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('ass.CtaServico ASS_CtaServico,');
  VAR_SQLx.Add('ass.FaturaSeq ASS_FaturaSeq,');
  VAR_SQLx.Add('ass.FaturaSep ASS_FaturaSep,');
  VAR_SQLx.Add('ass.FaturaDta ASS_FaturaDta,');
  VAR_SQLx.Add('ass.FaturaNum ASS_FaturaNum,');
  VAR_SQLx.Add('ass.TxtServico ASS_TxtServico,');
  VAR_SQLx.Add('ass.DupServico  ASS_DupServico,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('ufe.Nome UF_TXT_Emp, ufc.Nome UF_TXT_Cli');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('FROM nfempinn nmi');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo=nmi.CartEmiss');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=nmi.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=nmi.Fornece');
  VAR_SQLx.Add('LEFT JOIN entidades tra ON tra.Codigo=nmi.Transporta');
  VAR_SQLx.Add('LEFT JOIN fisregcad frc ON frc.Codigo=nmi.RegrFiscal');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=nmi.CondicaoPg');
  VAR_SQLx.Add('LEFT JOIN paramsemp  par ON par.Codigo=nmi.Empresa');
  VAR_SQLx.Add('LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada');
  VAR_SQLx.Add('LEFT JOIN entidades ase ON ase.Codigo=ass.Codigo');
  VAR_SQLx.Add('LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, emp.PUF)');
  VAR_SQLx.Add('LEFT JOIN ufs ufc ON ufc.Codigo=IF(frn.Tipo=0, frn.EUF, frn.PUF)');
  VAR_SQLx.Add('LEFT JOIN lfretepor lfp ON lfp.Codigo=nmi.FretePor');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE nmi.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND nmi.Codigo=:P0');
  //
  VAR_SQL2.Add('AND nmi.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND nmi.Nome Like :P0');
  //
end;

procedure TFmNFeMPInn.Desfazencerramento1Click(Sender: TObject);
var
  FatNum, Empresa: Integer;
begin
  FatNum  := QrNFeMPInnCodigo.Value;
  Empresa := QrNFeMPInnEmpresa.Value;
  if DmNFe_0000.DesfazEncerramento('nfempinn', VAR_FATID_0103, FatNum, Empresa) then
    LocCod(FatNum, FatNum);
end;

procedure TFmNFeMPInn.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'NFeMPInn', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmNFeMPInn.ExcluiDetail1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeMPIts, TDBGrid(DBGNFeMPIts),
  'nfempits', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmNFeMPInn.ExcluiMaster1Click(Sender: TObject);
begin
//
end;

procedure TFmNFeMPInn.MenuItem1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    FNFeMPAdd := UCriar.RecriaTempTable('NFeMPAdd', DmodG.QrUpdPID1, False);
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FNFeMPAdd);

    DmodG.QrUpdPID1.SQL.Text :=
'INSERT INTO ' + FNFeMPAdd + ' ' +
'SELECT mps.Conta, mps.Emissao, mps.Fornece, ' +
'mps.Caminhao, mps.Pecas, mps.PLE, mps.M2, mps.CMPValor, ' +
'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ' +
'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIINT, ' +
'0 Ativo ' +
'FROM      '+TMeuDB+'.mpinits   mps ' +
'LEFT JOIN '+TMeuDB+'.mpin      mpi ON mpi.Controle=mps.Controle ' +
'LEFT JOIN '+TMeuDB+'.entidades frn ON frn.Codigo=mps.Fornece ' +
'LEFT JOIN '+TMeuDB+'.entidades cli ON cli.Codigo=mpi.ClienteI ' +
'WHERE mps.NF_Inn_Fut = 1 ' +
'AND mps.NF_Inn_Its = 0 ';
    DmodG.QrUpdPID1.ExecSQL;
    //
    QrNFeMPAdd.Close;
    UnDmkDAC_PF.AbreQuery(QrNFeMPAdd, DModG.MyPID_DB);
    //
    if DBCheck.CriaFm(TFmNFeMPAdd, FmNFeMPAdd, afmoNegarComAviso) then
    begin
      FmNFeMPAdd.ShowModal;
      FmNFeMPAdd.Destroy;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFeMPInn.MenuItem2Click(Sender: TObject);
  function ExecutaSQL(): Boolean;
  {
  var
    i, j: Integer;
    Valor: String;
  }
  begin
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mpinits', False, [
    'NF_Inn_Its'], ['Conta'], [0], [QrMPInItsConta.Value], True);
  end;
var
  n, m: integer;
  q: TSelType;
//  P2: Double;
begin
  if not DBCheck.Quais_Selecionou(QrMPInIts, DBGMPInnIts, q) then Exit;
  //
  {
  q := istNenhum;
  if (QrMPInIts.State <> dsBrowse) or (QrMPInIts.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� item a ser exclu�do!');
    Exit;
  end;
  if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
  begin
    with FmQuaisItens do
    begin
      ShowModal;
      if not FSelecionou then
      begin
        Geral.MB_Aviso('Exclus�o cancelada pelo usu�rio!');
        q := istDesiste;
      end else q := FEscolha;
      Destroy;
    end;
  end;
  if q = istDesiste then Exit;
  if (q = istSelecionados) and (DBGMPInnIts.SelectedRows.Count < 2) then
    q := istAtual;
  }
  //
  Screen.Cursor := crHourGlass;
  m := 0;
  case q of
    istAtual:
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o do item selecionado?') = ID_YES then
        if ExecutaSQL() then m := 1;
    end;
    istSelecionados:
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o dos ' +
      IntToStr(DBGMPInnIts.SelectedRows.Count) + ' itens selecionados?') = ID_YES then
      begin
        with DBGMPInnIts.DataSource.DataSet do
        for n := 0 to DBGMPInnIts.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGMPInnIts.SelectedRows.Items[n]));
          if ExecutaSQL() then inc(m, 1);
        end;
      end;
    end;
    istTodos:
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o de todos os ' +
      IntToStr(QrMPInIts.RecordCount) + ' itens pesquisados?') = ID_YES then
      begin
        QrMPInIts.First;
        while not QrMPInIts.Eof do
        begin
          if ExecutaSQL() then
            inc(m, 1);
          QrMPInIts.Next;
        end;
      end;
    end;
  end;
  if m > 0 then
  begin
    AtualizaItem(QrNFeMPItsControle.Value, QrNFeMPInnCodigo.Value);
    if m = 1 then
      Geral.MB_Info('Um item foi exclu�do!')
    else
      Geral.MB_Info(IntToStr(m) + ' itens foram exclu�dos!');
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmNFeMPInn.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      if DBCheck.CriaFm(TFmNFeMPIts, FmNFeMPIts, afmoNegarComAviso) then
      begin
        FmNFeMPIts.ImgTipo.SQLType := SQLType;
        if SQLType = stUpd then
        begin
          FmNFeMPIts.EdGraGruX.ValueVariant  := QrNFeMPItsGraGruX.Value;
          FmNFeMPIts.EdPecas.ValueVariant    := QrNFeMPItsPecas.Value;
          FmNFeMPIts.EdAreaM2.ValueVariant   := QrNFeMPItsAreaM2.Value;
          FmNFeMPIts.EdAreaP2.ValueVariant   := QrNFeMPItsAreaP2.Value;
          FmNFeMPIts.EdPLE.ValueVariant      := QrNFeMPItsPLE.Value;
          FmNFeMPIts.EdCMPValor.ValueVariant := QrNFeMPItsCMPValor.Value;
          FmNFeMPIts.RGGerBxaEstq.ItemIndex  := QrNFeMPItsGerBxaEstq.Value;
        end;
        //
        FmNFeMPIts.ShowModal;
        FmNFeMPIts.Destroy;
      end;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  if Mostra < 2 then
  begin
    ImgTipo.SQLType := SQLType;
    GOTOy.BotoesSb(ImgTipo.SQLType);
  end;
end;

procedure TFmNFeMPInn.PMSubDetailPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrNFeMPInn.State <> dsInactive) and (QrNFeMPInn.RecordCount > 0);
  Enab2 := (QrMPInIts.State <> dsInactive) and (QrMPInIts.RecordCount > 0);
  //
  AdicionaSubDetail1.Enabled := Enab;
  RetiraSubDetail1.Enabled   := Enab and Enab2;
end;

procedure TFmNFeMPInn.PMDetailPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrNFeMPInn.State <> dsInactive) and (QrNFeMPInn.RecordCount > 0);
  Enab2 := (QrNFeMPIts.State <> dsInactive) and (QrNFeMPIts.RecordCount > 0) and
    (QrMPInIts.RecordCount = 0);
  //
  IncluiDetail1.Enabled := Enab;
  AlteraDetail1.Enabled := Enab and Enab2;
  ExcluiDetail1.Enabled := Enab and Enab2;
end;

procedure TFmNFeMPInn.PMEncerraPopup(Sender: TObject);
var
  Enab, Habilita: Boolean;
begin
  Enab     := (QrNFeMPInn.State <> dsInactive) and (QrNFeMPInn.RecordCount > 0);
  Habilita := QrNFeMPInnEncerrou.Value = 0;
  //
  Encerraentradadecouro1.Enabled := Enab and Habilita;
  Desfazencerramento1.Enabled    := Enab and (not Habilita);
end;

procedure TFmNFeMPInn.PMMasterPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrNFeMPInn.State <> dsInactive) and (QrNFeMPInn.RecordCount > 0) and
    (QrNFeMPInnEncerrou.Value = 0);
  //
  AlteraMaster1.Enabled := Enab;
end;

procedure TFmNFeMPInn.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFeMPInn.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeMPInn.RecriatodaNFe1Click(Sender: TObject);
begin
  GerarNFe(True);
end;

procedure TFmNFeMPInn.GerarNFe(Recria: Boolean);
var
  ide_serie: Variant;
  ide_dEmi, ide_dSaiEnt: TDateTime;
  //FatID,
  FatNum, Empresa, Cliente, ide_indPag, ide_nNF, ide_tpNF, ide_tpEmis,
  //ide_tpImp, ide_finNFe,
  modFrete, Transporta, NFeStatus, FretePor: Integer;
  //
  IDCtrl(*, Retirada, Entrega, Status*): Integer;
  //
  CFOP1, CodTXT, EmpTXT, infAdic_infAdFisco, infAdic_infCpl,
  VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Exporta_UFEmbarq, Exporta_xLocEmbarq,
  //XMLGerado_Arq, XMLGerado_Dir, XMLAssinado_Dir, PathLote
  SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES, SQL_CUSTOMZ: String;
  //
  FreteVal, Seguro, Outros: Double;
  //Continua: Boolean;
  GravaCampos, RegrFiscal, CartEmiss, CondicaoPG: Integer;
  UF_TXT_Emp, UF_TXT_Cli: String;
  cNF_Atual, Financeiro: Integer;
  //
  //2.00
  ide_hSaiEnt: TTime;
  ide_dhCont: TDateTime;
  emit_CRT: Integer;
  ide_xJust, dest_email, Vagao, Balsa: String;
  // fim 2.00
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
begin
  FatNum     := QrNFeMPInnCodigo.Value;
  RegrFiscal := QrNFeMPInnRegrFiscal.Value;
  CartEmiss  := QrNFeMPInnCartEmiss.Value;
  CondicaoPG := QrNFeMPInnCondicaoPG.Value;
  Empresa    := QrFatPedNFsEmpresa.Value;
  IDCtrl     := QrFatPedNFsIDCtrl.Value;
  FreteVal   := QrFatPedNFsFreteVal.Value;
  Seguro     := QrFatPedNFsSeguro.Value;
  Outros     := QrFatPedNFsOutros.Value;
  Financeiro := QrNFeMPInnFinanceiro.Value;
  //
  if not DmodG.QrFiliLog.Locate('Codigo', Empresa, []) then
  begin
    Application.MessageBox(PChar('Empresa (' + IntToStr(Empresa) +
    ') diferente da logada (' + VAR_LIB_EMPRESAS +
    ')! � necess�rio logoff!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  { Parei aqui (???)
  if Geral.IMV(EdFilialPesq.Text) = 0 then
  begin
    Application.MessageBox('Empresa n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdFilialPesq.SetFocus;
    Exit;
  end;
  if Geral.IMV(EdClientePesq.Text) = 0 then
  begin
    Application.MessageBox('Cliente n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdClientePesq.SetFocus;
    Exit;
  end;
  }
  if not DModG.DefineFretePor(QrNFeMPInnFretePor.Value, FretePor, modFrete) then
    Exit;
  Transporta  := QrNFeMPInnTransporta.Value;
  ide_indPag  := dmkPF.EscolhaDe2Int(QrNFeMPInnMedDDSimpl.Value = 0, 0, 1);
  //
  Cliente     := QrNFeMPInnFornece.Value;
  ide_Serie   := QrFatPedNFsSerieNFTxt.Value;
  ide_nNF     := QrFatPedNFsNumeroNF.Value;
  ide_dEmi    := QrFatPedNFsDtEmissNF.Value;
  ide_dSaiEnt := QrFatPedNFsDtEntraSai.Value;
  // Parei Aqui
  ide_tpNF    := 0; // 0=Entrada 1=Sa�da
  //ide_tpImp   :=  1-Retrato/ 2-Paisagem
  ide_tpEmis  := 1;
  (*
   1 � Normal � emiss�o normal;
   2 � Conting�ncia FS � emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a;
   3 � Conting�ncia SCAN � emiss�o em conting�ncia no Sistema de Conting�ncia do Ambiente Nacional � SCAN;
   4 � Conting�ncia DPEC - emiss�o em conting�ncia com envio da Declara��o Pr�via de Emiss�o em Conting�ncia � DPEC;
   5 � Conting�ncia FS-DA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a para Impress�o de Documento Auxiliar de Documento Fiscal Eletr�nico (FS-DA).
  *)
  infAdic_infAdFisco := QrNFeMPInninfAdFisco.Value;
  infAdic_infCpl     := QrFatPedNFsinfAdic_infCpl.Value;
  VeicTransp_Placa   := QrFatPedNFsPlacaNr.Value;
  VeicTransp_UF      := QrFatPedNFsPlacaUF.Value;
  VeicTransp_RNTC    := QrFatPedNFsRNTC.Value;
  Exporta_UFEmbarq   := QrFatPedNFsUFEmbarq.Value;
  Exporta_XLocEmbarq := QrFatPedNFsxLocEmbarq.Value;
  //
  CFOP1 := QrFatPedNFsCFOP1.Value;
  CodTXT := dmkPF.FFP(QrNFeMPInnCodigo.Value, 0);
  EmpTXT := dmkPF.FFP(QrNFeMPInnEmpresa.Value, 0);
  UF_TXT_Emp := QrNFeMPInnUF_TXT_Emp.Value;
  UF_TXT_Cli := QrNFeMPInnUF_TXT_Cli.Value;
  SQL_ITS_ITS := DmNFe_0000.SQL_ITS_ITS_Prod1_Padrao(VAR_FATID_0103, FatNum, Empresa);
  SQL_ITS_TOT := DmNFe_0000.SQL_ITS_TOT_Prod1_Padrao(VAR_FATID_0103, FatNum, Empresa);
  SQL_FAT_ITS := DmNFe_0000.SQL_FAT_ITS_Prod1_Padrao(VAR_FATID_0103, FatNum, Empresa);
  SQL_FAT_TOT := DmNFe_0000.SQL_FAT_TOT_Prod1_Padrao(VAR_FATID_0103, FatNum, Empresa);
  SQL_VOLUMES := DmNFe_0000.SQL_VOLUMES_Prod1_Padrao(VAR_FATID_0103, FatNum, Empresa);

  NFeStatus := QrNFeCabAStatus.Value;
  //
  GravaCampos := ID_NO;
  cNF_Atual := QrNFeCabAide_cNF.Value;

  // 2.00  
  ide_hSaiEnt := QrFatPedNFsHrEntraSai.Value;
  ide_dhCont  := 0;   // Parei aqui! Ver o que fazer!
  ide_xJust   := ''; // Parei aqui! Ver o que fazer!
  emit_CRT    := QrFatPedNFsemit_CRT.Value;
  dest_email  := QrFatPedNFsdest_EMail.Value;
  Vagao       := QrFatPedNFsVagao.Value;
  Balsa       := QrFatPedNFsBalsa.Value;
  // fim 2.00
  Compra_XNEmp := QrFatPedNFsCompra_XNEmp.Value;
  Compra_XPed  := QrFatPedNFsCompra_XPed.Value;
  Compra_XCont := QrFatPedNFsCompra_XCont.Value;

  if UnNFe_PF.CriaNFe_vXX_XX(Recria, NFeStatus, VAR_FATID_0103, FatNum, Empresa,
        IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
        RegrFiscal, CartEmiss, 0, CondicaoPG, FreteVal, Seguro, Outros, ide_Serie,
        ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
        infAdic_infAdFisco, infAdic_infCpl,
        VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
        Exporta_UFEmbarq, Exporta_xLocEmbarq,
        SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
        SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos, cNF_Atual, Financeiro,
        ide_hSaiEnt, ide_dhCont, ide_xJust, emit_CRT, dest_email, Vagao, Balsa,
        Compra_XNEmp, Compra_XPed, Compra_XCont, False) then
{
  if DBCheck.CriaFm(TFmNFeSteps_xxxx, FmNFeSteps_xxxx, afmoNegarComAviso) then
  begin
    FmNFeSteps_xxxx.Show;
    if GravaCampos <> ID_CANCEL then
      FmNFeSteps_xxxx.Cria NFe Normal(Recria, NFeStatus, VAR_FATID_0103, FatNum, Empresa,
        IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
        RegrFiscal, FreteVal, Seguro, Outros, ide_Serie,
        ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
        infAdic_infAdFisco, infAdic_infCpl,
        VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
        Exporta_UFEmbarq, Exporta_xLocEmbarq,
        SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
        SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos, cNF_Atual, Financeiro)
    else Geral.MB_Aviso('Gera��o total da NF-e cancelada pelo usu�rio!');
    FmNFeSteps_xxxx.Destroy;
    //
}
    ReopenFatPedNFs(QrFatPedNFsFilial.Value);
end;

procedure TFmNFeMPInn.ReopenFatPedNFs(Filial: Integer);
begin
  QrFatPedNFs.Close;
  QrFatPedNFs.Params[00].AsInteger := VAR_FATID_0103;
  QrFatPedNFs.Params[01].AsInteger := QrNFeMPInnCodigo.Value;
  QrFatPedNFs.Params[02].AsInteger := QrNFeMPInnEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrFatPedNFs, Dmod.MyDB);
  //
  QrFatPedNFs.Locate('Filial', Filial, []);
end;

procedure TFmNFeMPInn.ReopenMPInIts(Conta: Integer);
begin
  QrMPInIts.Close;
  QrMPInIts.Params[0].AsInteger := QrNFeMPItsControle.Value;
  UnDmkDAC_PF.AbreQuery(QrMPInIts, Dmod.MyDB);
  //
  QrMPInIts.Locate('Conta', Conta, []);
end;

procedure TFmNFeMPInn.ReopenQrNFeMPAdd(Conta: Integer);
begin
  QrNFeMPAdd.Close;
  UnDmkDAC_PF.AbreQuery(QrNFeMPAdd, DModG.MyPID_DB);
  QrNFeMPAdd.Locate('Conta', Conta, []);
end;

procedure TFmNFeMPInn.ReopenQrNFeMPIts(Controle: Integer);
begin
  QrNFeMPIts.Close;
  QrNFeMPIts.Params[0].AsInteger := QrNFeMPInnCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeMPIts, Dmod.MyDB);
  QrNFeMPIts.Locate('Controle', Controle, []);
end;

procedure TFmNFeMPInn.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeMPInn.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeMPInn.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeMPInn.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeMPInn.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeMPInn.AlteraDetail1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmNFeMPInn.AlteraMaster1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFeMPInn, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'NFeMPInn');
end;

procedure TFmNFeMPInn.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeMPInnCodigo.Value;
  Close;
end;

procedure TFmNFeMPInn.BtSubDetailClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSubDetail, BtSubDetail);
end;

procedure TFmNFeMPInn.AtualizaTotais(Codigo: Integer);
begin
  QrSum.Close;
  QrSum.Params[0].AsInteger := QrNFeMPInnCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSum, Dmod.MyDB);
  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfempinn', False, [
    'Pecas', 'PLE',
    'CMPValor', 'AreaM2',
    'AreaP2'
  ], ['Codigo'], [
    QrSumPecas.Value, QrSumPLE.Value,
    QrSumCMPValor.Value, QrSumAreaM2.Value,
    QrSumAreaP2.Value
  ], [Codigo], True);
end;

procedure TFmNFeMPInn.BtConfirmaClick(Sender: TObject);
var
  //RegrFiscal,
  Codigo: Integer;
  //Nome: String;
begin
  if MyObjects.FIC(EdNome.Text = '', EdNome,
    'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
    'Informe a Empresa (Cliente Interno)!') then Exit;
  if MyObjects.FIC(EdFornece.ValueVariant = 0, EdFornece,
    'Informe o Fornecedor!') then Exit;
  if MyObjects.FIC(EdRegrFiscal.ValueVariant = 0, EdRegrFiscal,
    'Informe a movimenta��o (fiscal)!') then Exit;
  //if MyObjects.FIC(EdFretePor.ValueVariant = 0, EdFretePor,
    //'Informe o campo "Frete por"!') then Exit;
  if MyObjects.FIC((EdFretePor.ValueVariant > 1) and (DModG.QrParamsEmpversao.Value < 2),
    EdFretePor, 'Tipo de frete inv�lido na vers�o atual da NFe! Selecione 0 ou 1') then Exit;
  if MyObjects.FIC(EdCondicaoPG.ValueVariant = 0, EdCondicaoPG,
    'Informe a condi��o de pagamento!') then Exit;
  //if MyObjects.FIC(EdTransporta.ValueVariant = 0, EdTransporta,
    //'Informe a transportadora!') then Exit;
  //  
  // deve ser depois da regra fiscal!
  if MyObjects.FIC((QrFisRegCadFinanceiro.Value > 0)
    and (EdCartEmiss.ValueVariant = 0), EdCartEmiss,
    'Informe a carteira (Financeiro) pois a Regra Fiscal (Movimenta��o) selecionada exige lan�amento financeiro!') then Exit;
  //
   Codigo := UMyMod.BuscaEmLivreY_Def('NFeMPInn', 'Codigo', ImgTipo.SQLType,
    QrNFeMPInnCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmNFeMPInn, PainelEdit,
    'NFeMPInn', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFeMPInn.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'NFeMPInn', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFeMPInn', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFeMPInn', 'Codigo');
end;

procedure TFmNFeMPInn.BtDetailClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDetail, BtDetail);
end;

procedure TFmNFeMPInn.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmNFeMPInn.EncerraEntradaCouro();
  procedure ExcluiItens(OriCodi: Integer);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE ');
    Dmod.QrUpd.SQL.Add('Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.Params[00].AsInteger := VAR_FATID_0103;
    Dmod.QrUpd.Params[01].AsInteger := OriCodi;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovvala WHERE ');
    Dmod.QrUpd.SQL.Add('Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.Params[00].AsInteger := VAR_FATID_0103;
    Dmod.QrUpd.Params[01].AsInteger := OriCodi;
    Dmod.QrUpd.ExecSQL;
    //
  end;
var
  Especie, SerieNFTxt: String;
  NumeroNF, Quantidade, _OriCtrl_: Integer;
  //
  //IDCtrl, ID, SeqInReduz,
  NFe_FatID, OriCodi, OriCtrl, OriCnta, Empresa, Associada, Cliente, RegrFiscal,
  StqCenCad, FatSemEstq, GraGruX, InfAdCuztm, Preco_MedOrdem, TipoNF, modNF,
  Serie, nNF, SitDevolu, Servico, AFP_Sit, Cli_Tipo, Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy, PediVda, OriPart: Integer;
  Qtde, Preco_PrecoF, Total, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
  Preco_MedidaA, Preco_MedidaE, AFP_Per, Item_IPI_ALq, TotalPreCalc: Double;
  Cli_IE, refNFe, CFOP, NO_tablaPrc: String;
begin
  if (QrNFeMPInnFinanceiro.Value > 0) and (Dmod.QrControleCtaMPCompr.Value = 0) then
  begin
    Geral.MB_Aviso('Encerramento cancelado!'+sLineBreak+
    'Informe a conta (plano de contas) nas op��es espec�ficas do aplicativo ' +
    sLineBreak + 'para gera��o dos lan�amentos financeiros!');
    Exit;
  end;
  if (QrNFeMPIts.State <> dsBrowse) or (QrNFeMPIts.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� itens na NF para seu encerramento!');
    Exit;
  end;
  //SeqInReduz := 0;
  OriCodi      := QrNFeMPInnCodigo.Value;
  ExcluiItens(OriCodi);
  //
  ReopenQrNFeMPIts(0);
  QrNFeMPIts.First;
  while not QrNFeMPIts.Eof do
  begin
    NFe_FatID    := VAR_FATID_0103; // 103
    OriCtrl      := QrNFeMPItsControle.Value;
    OriCnta      := 0;
    Empresa      := QrNFeMPInnEmpresa.Value;
    Associada    := 0;
    Cliente      := QrNFeMPInnFornece.Value; // Nota de entrada!
    RegrFiscal   := QrNFeMPInnRegrFiscal.Value;
    StqCenCad    := 0; // Fazer??? Parei Aqui
    GraGruX      := QrNFeMPItsGraGruX.Value;
    FatSemEstq   := 1; // Sim > Ver se faz
    AFP_Per      := 0;
    AFP_Sit      := 0;
    Cli_IE       := QrNFeMPInnFRN_IE.Value;
    Cli_Tipo     := QrNFeMPInnFRN_Tipo.Value;
    Cli_UF       := QrNFeMPInnFRN_UF.Value;
    EMP_UF       := QrNFeMPInnEMP_UF.Value;
    EMP_FILIAL   := QrNFeMPInnEMP_FILIAL.Value;
    ASS_CO_UF    := QrNFeMPInnASS_CO_UF.Value;
    ASS_FILIAL   := QrNFeMPInnASS_FILIAL.Value;
    Item_MadeBy  := 0;//??? 0=Nenhum, 1=Pr�prio, 2=Terceiros
    Item_IPI_ALq := 0; // ???
    case QrNFeMPItsGerBxaEstq.Value of
      1: (*Pecas*) Qtde := QrNFeMPItsPecas.Value;
      2: (*Area*)  Qtde := QrNFeMPItsAreaM2.Value;
      3: (*Peso*)  Qtde := QrNFeMPItsPLE.Value;
      else         Qtde := 0;
    end;
    if Qtde = 0 then
    begin
      //Preco_PrecoF := 0;
      ExcluiItens(OriCodi);
      Geral.MB_Aviso('Quantidade do produto de reduzido n� ' +
      IntToStr(GraGruX) +
      ' n�o definido. Verifique se o item gerenciador est� correto!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    Preco_PrecoF   := QrNFeMPItsCMPValor.Value / Qtde;
    TotalPreCalc   := QrNFeMPItsCMPValor.Value;
    InfAdCuztm     := 0;//QrNFeMPIts.Value;
    Preco_PercCustom := 0;//QrNFeMPIts.Value;
    Preco_MedidaC  := 0;//QrNFeMPIts.Value;
    Preco_MedidaL  := 0;//QrNFeMPIts.Value;
    Preco_MedidaA  := 0;//QrNFeMPIts.Value;
    Preco_MedidaE  := 0;//QrNFeMPIts.Value;
    Preco_MedOrdem := 0;//QrNFeMPIts.Value;
    // NF referenciadas
    // Refererncia NF do fornecedor?
    TipoNF         := -1; //QrNFeMPItsTipoNF.Value;
    refNFe         := ''; //QrNFeMPItsrefNFe.Value;
    modNF          := 0;  //QrNFeMPItsmodNF.Value;
    Serie          := 0;  //QrNFeMPItsSerie.Value;
    nNF            := 0;  //QrNFeMPItsNFInn.Value;
    SitDevolu      := -1; //QrNFeMPItsSitDevolu.Value;
    Servico        := 0;  // Produto
    PediVda        := 0;
    OriPart        := 0;
    //
    if not DmNFe_0000.InsereItemStqMov(NFe_FatID, OriCodi, OriCnta, Empresa, Cliente,
             Associada, RegrFiscal, GraGruX, NO_tablaPrc,
             StqCenCad, FatSemEstq, AFP_Sit, AFP_Per, Qtde,
             Cli_Tipo, Cli_IE, Cli_UF, EMP_UF, EMP_FILIAL,
             ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq,
             Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
             Preco_MedidaA, Preco_MedidaE, Preco_MedOrdem,
             stIns, PediVda, OriPart, InfAdCuztm,
             TipoNF, modNF, Serie, nNF, SitDevolu, Servico,
             refNFe, TotalPreCalc, OriCtrl,
             0,0,0,0,1, 1, 0) then
    begin
      Geral.MB_Aviso('Encerramento cancelado!');
      Screen.Cursor := crDefault;
      Exit;
    end;

    //  FIM Produto / In�cio Servico

    {
    if QrNFeMPItsSrvCodigo.Value > 0 then
    begin
      Tipo         := VAR_FATID_0103; // 103
      OriCodi      := QrNFeMPInnCodigo.Value;
      OriCtrl      := QrNFeMPItsControle.Value;
      OriCnta      := 0;
      Empresa      := QrNFeMPInnEmpresa.Value;
      Associada    := 0;
      Cliente      := QrNFeMPInnCliente.Value;
      RegrFiscal   := QrNFeMPInnFisRegCad.Value;
      StqCenCad    := 0; // � servi�o
      GraGruX      := 0; // � servi�o
      FatSemEstq   := 1; // � servi�o
      AFP_Per      := 0;
      AFP_Sit      := 0;
      Cli_IE       := QrNFeMPInnCLI_IE.Value;
      Cli_Tipo     := QrNFeMPInnCLI_Tipo.Value;
      Cli_UF       := QrNFeMPInnCLI_UF.Value;
      EMP_UF       := QrNFeMPInnEMP_UF.Value;
      EMP_FILIAL   := QrNFeMPInnEMP_FILIAL.Value;
      ASS_CO_UF    := QrNFeMPInnASS_CO_UF.Value;
      ASS_FILIAL   := QrNFeMPInnASS_FILIAL.Value;
      Item_MadeBy  := 0; // � servi�o
      Item_IPI_ALq := 0; // � servi�o
      //case QrNFeMPItsComoCobra.Value of
      case QrNFeMPItsGerBxaEstq.Value of
        0: (*Pecas*) Qtde := QrNFeMPItsBenefiPc.Value;
        1: (*Area*)  Qtde := QrNFeMPItsBenefiM2.Value;
        2: (*Peso*)  Qtde := QrNFeMPItsBenefikg.Value;
        else Qtde := 0;
      end;
      if Qtde = 0 then Preco_PrecoF := 0 else
        Preco_PrecoF   := QrNFeMPItsSrvValUni.Value;
      TotalPreCalc   := QrNFeMPItsSrvValTot.Value;
      InfAdCuztm     := 0;//QrNFeMPIts.Value;
      Preco_PercCustom := 0;//QrNFeMPIts.Value;
      Preco_MedidaC  := 0;//QrNFeMPIts.Value;
      Preco_MedidaL  := 0;//QrNFeMPIts.Value;
      Preco_MedidaA  := 0;//QrNFeMPIts.Value;
      Preco_MedidaE  := 0;//QrNFeMPIts.Value;
      Preco_MedOrdem := 0;//QrNFeMPIts.Value;
      TipoNF         := 0;
      refNFe         := '';
      modNF          := 0;
      Serie          := 0;
      nNF            := 0;
      SitDevolu      := 0;
      Servico        := QrNFeMPItsSrvCodigo.Value;
      PediVda        := 0;
      OriPart        := 0;
      //
      if not DmNFe_0000.InsereItemStqMov(Tipo, OriCodi, OriCnta, Empresa, Cliente,
               Associada, RegrFiscal, GraGruX, NO_tablaPrc,
               StqCenCad, FatSemEstq, AFP_Sit, AFP_Per, Qtde,
               Cli_Tipo, Cli_IE, Cli_UF, EMP_UF, EMP_FILIAL,
               ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq,
               Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
               Preco_MedidaA, Preco_MedidaE, Preco_MedOrdem,
               stIns, PediVda, OriPart, InfAdCuztm,
               TipoNF, modNF, Serie, nNF, SitDevolu, Servico,
               refNFe, TotalPreCalc, _OriCtrl_)
    begin
      Geral.MB_Aviso('Encerramento cancelado!');
      Screen.Cursor := crDefault;
      Exit;
    end;

    end;
    }
    QrNFeMPIts.Next;
  end;
  //
  if DBCheck.CriaFm(TFmNFaEdit, FmNFaEdit, afmoNegarComAviso) then
  begin
    //DmPediVda.ReopenFatPedCab(QrFatPedCabCodigo.Value, True);
    // Configura��o do tipo
    FmNFaEdit.F_Tipo            := VAR_FATID_0103; // 103;
    FmNFaEdit.F_OriCodi         := QrNFeMPInnCodigo        .Value;
    // Integer:
    FmNFaEdit.F_Empresa         := QrNFeMPInnEmpresa       .Value;
    FmNFaEdit.F_ModeloNF        := QrNFeMPInnModeloNF      .Value;
    FmNFaEdit.F_Cliente         := QrNFeMPInnFornece       .Value;
    FmNFaEdit.F_EMP_FILIAL      := QrNFeMPInnFilial        .Value;
    FmNFaEdit.F_AFP_Sit         := 0;//QrNFeMPInnAFP_Sit       .Value;
    FmNFaEdit.F_Associada       := QrNFeMPInnAssociada     .Value;
    FmNFaEdit.F_ASS_FILIAL      := QrNFeMPInnASS_FILIAL    .Value;
    FmNFaEdit.F_EMP_CtaFaturas  := Dmod.QrControleCtaMPCompr.Value;//QrNFeMPInnEMP_CtaServico.Value;
    FmNFaEdit.F_ASS_CtaFaturas  := Dmod.QrControleCtaMPCompr.Value;//QrNFeMPInnASS_CtaServico.Value;
    FmNFaEdit.F_CartEmis        := QrNFeMPInnCartEmiss     .Value;
    FmNFaEdit.F_CondicaoPG      := QrNFeMPInnCondicaoPG    .Value;
    FmNFaEdit.F_Financeiro      := QrNFeMPInnFinanceiro    .Value;
    FmNFaEdit.F_EMP_FaturaDta   := QrNFeMPInnEMP_FaturaDta .Value;
    FmNFaEdit.F_EMP_IDDuplicata := QrNFeMPInnCodigo        .Value;
    FmNFaEdit.F_EMP_FaturaSeq   := QrNFeMPInnEMP_FaturaSeq .Value;
    FmNFaEdit.F_EMP_FaturaNum   := QrNFeMPInnEMP_FaturaNum .Value;
    FmNFaEdit.F_TIPOCART        := QrNFeMPInnTIPOCART      .Value;
    FmNFaEdit.F_Represen        := 0;//QrNFeMPInnRepresen      .Value;
    FmNFaEdit.F_ASS_IDDuplicata := QrNFeMPInnCodigo        .Value;
    FmNFaEdit.F_ASS_FaturaSeq   := QrNFeMPInnASS_FaturaSeq .Value;
    FmNFaEdit.F_ASS_FaturaNum   := QrNFeMPInnASS_FaturaNum .Value;
    // String:
    //N�o tem como saber! � o fornecedor que informa
    FmNFaEdit.F_EMP_FaturaSep   := '';//QrNFeMPInnEMP_FaturaSep .Value;
    FmNFaEdit.F_EMP_TxtFaturas  := '';//QrNFeMPInnEMP_TxtServico.Value;
    FmNFaEdit.F_EMP_TpDuplicata := '';//QrNFeMPInnEMP_DupServico.Value;
    FmNFaEdit.F_ASS_FaturaSep   := '';//QrNFeMPInnASS_FaturaSep .Value;
    FmNFaEdit.F_ASS_TxtFaturas  := '';//QrNFeMPInnASS_TxtServico.Value;
    FmNFaEdit.F_ASS_TpDuplicata := '';//QrNFeMPInnASS_DupServico.Value;
    // Double:
    FmNFaEdit.F_AFP_Per         := 0;//QrNFeMPInnAFP_Per       .Value;
    // TDateTime:
    FmNFaEdit.F_Abertura        := QrNFeMPInnAbertura      .Value;
    //
    // NF-e 2.00
    FmNFaEdit.RGCRT.ItemIndex           := DmodG.QrParamsEmpCRT.Value;
    FmNFaEdit.EdHrEntraSai.ValueVariant := Time();
    FmNFaEdit.Eddest_email.Text         := DmodG.ObtemPrimeiroEMail_NFe(QrNFeMPInnEmpresa.Value, QrNFeMPInnFornece.Value);
    FmNFaEdit.EdVagao.Text              := '';
    FmNFaEdit.EdBalsa.Text              := '';
    // fim NF-e 2.00
    FmNFaEdit.EdCompra_XNEmp.Text := '';
    FmNFaEdit.EdCompra_XPed.Text := '';
    FmNFaEdit.EdCompra_XCont.Text := '';

    FmNFaEdit.ReopenFatPedNFs(1,0);
    if FmNFaEdit.QrImprimeCtrl_nfs.Value = 0 then
    begin
      Geral.MB_Aviso( // Formato de impress�o > FmImprime
      'Formato de impress�o n�o definido para o modelo de impress�o selecionado!');
      Exit;
    end;

    //

    if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo(
    QrNFeMPInnSerieDesfe.Value,
    QrNFeMPInnNFDesfeita.Value,
    FmNFaEdit.QrImprimeSerieNF_Normal.Value,
    FmNFaEdit.QrImprimeCtrl_nfs.Value,
    QrNFeMPInnEmpresa.Value,
    FmNFaEdit.QrFatPedNFs.FieldByName('Filial').AsInteger,
    FmNFaEdit.QrImprimeMaxSeqLib.Value,
    FmNFaEdit.EdSerieNF, FmNFaEdit.EdNumeroNF(*),
    SerieNFTxt, NumeroNF*)) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;

    //

    Especie := '';
    Quantidade := 0;
    //
    (*
    QrVolumes.Close;
    QrVolumes.Params[00].AsInteger := QrFatPedCabCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrVolumes, Dmod.MyDB);
    QrVolumes.First;
    while not QrVolumes.Eof do
    begin
      Quantidade := Quantidade + QrVolumesVolumes.Value;
      if Especie <> '' then
        Especie := Especie + ' + ';
      Especie := Especie + ' ' +
        IntToStr(QrVolumesVolumes.Value) + ' ' +
        QrVolumesNO_UnidMed.Value;
      //
      QrVolumes.Next;
    end;
    *)
    FmNFaEdit.EdQuantidade.ValueVariant := FloatToStr(Quantidade);
    FmNFaEdit.EdEspecie.ValueVariant := Especie;
    //
    FmNFaEdit.EdNumeroNF.Enabled := (FmNFaEdit.QrImprimeIncSeqAuto.Value = 0);
    //
    (*
    FmNFaEdit.QrCFOP.Close;
    FmNFaEdit.QrCFOP.Params[00].AsInteger := VAR_FATID_0103;
    FmNFaEdit.QrCFOP.Params[00].AsInteger := FmFatPedCab.QrFatPedCabCodigo.Value;
    FmNFaEdit.QrCFOP.Params[01].AsInteger := FmFatPedCab.QrFatPedCabEmpresa.Value;
    UnDmkDAC_PF.AbreQuery(FmNFaEdit.QrCFOP, Dmod.MyDB);
    FmNFaEdit.EdCFOP1.Text := FmNFaEdit.QrCFOPCFOP.Value;
    FmNFaEdit.CBCFOP1.KeyValue := FmNFaEdit.QrCFOPCFOP.Value;
    *)
    //
    FmNFaEdit.ReopenStqMovValX(0);
    FmNFaEdit.ImgTipo.SQLType := stIns;
    FmNFaEdit.ShowModal;
    FmNFaEdit.Destroy;
    // Reabrir de novo!
    LocCod(QrNFeMPInnCodigo.Value, QrNFeMPInnCodigo.Value);
    //
(*
    if QrFatPedCabEncerrou.Value > 0 then
    begin
      if DBCheck.CriaFm(TFmFatPedNFs, FmFatPedNFs, afmoNegarComAviso) then
      begin
        FmFatPedNFs.EdFilial.ValueVariant  := QrFatPedCabEMP_FILIAL.Value;
        FmFatPedNFs.CBFilial.KeyValue      := QrFatPedCabEMP_FILIAL.Value;
        FmFatPedNFs.EdCliente.ValueVariant := QrFatPedCabCliente.Value;
        FmFatPedNFs.CBCliente.KeyValue     := QrFatPedCabCliente.Value;
        FmFatPedNFs.EdPedido.ValueVariant  := QrFatPedCabCU_PediVda.Value;
        FmFatPedNFs.ShowModal;
        FmFatPedNFs.Destroy;
      end;
    end;
*)
  end;
end;

procedure TFmNFeMPInn.Encerraentradadecouro1Click(Sender: TObject);
begin
  EncerraEntradaCouro();
end;

procedure TFmNFeMPInn.BtMasterClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMaster, BtMaster);
end;

procedure TFmNFeMPInn.BtNFeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFe, BtNFe);
end;

procedure TFmNFeMPInn.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrNFeMPAdd.Database := DModG.MyPID_DB;
  //
  QrTransportas.Close;
  UnDmkDAC_PF.AbreQuery(QrTransportas, Dmod.MyDB);
  //
  QrFisRegCad.Close;
  UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
  //
  QrCartEmiss.Close;
  UnDmkDAC_PF.AbreQuery(QrCartEmiss, Dmod.MyDB);
  //
  QrPediPrzCab.Close;
  QrPediPrzCab.Params[0].AsInteger := 2; // Pedido de compra
  UnDmkDAC_PF.AbreQuery(QrPediPrzCab, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreQuery(QrDonos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  //
  PainelEdit.Align  := alClient;
  DBGMPInnIts.Align := alClient;
  CriaOForm;
end;

procedure TFmNFeMPInn.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeMPInnCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFeMPInn.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNFeMPInn.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeMPInn.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrNFeMPInnCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmNFeMPInn.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFeMPInn.QrFatPedNFsAfterScroll(DataSet: TDataSet);
begin
  QrNFeCabA.Close;
  QrNFeCabA.Params[00].AsInteger := VAR_FATID_0103;
  QrNFeCabA.Params[01].AsInteger := QrNFeMPInnCodigo.Value;
  QrNFeCabA.Params[02].AsInteger := QrFatPedNFsEmpresa.Value;
  QrNFeCabA.Params[03].AsInteger := QrFatPedNFsNumeroNF.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeCabA, Dmod.MyDB);
end;

procedure TFmNFeMPInn.QrFatPedNFsBeforeClose(DataSet: TDataSet);
begin
  QrNFeCabA.Close;
end;

procedure TFmNFeMPInn.QrNFeCabACalcFields(DataSet: TDataSet);
begin
  if QrNFeCabAinfCanc_cStat.Value = 101 then
  begin
    QrNFeCabAcStat.Value := QrNFeCabAinfCanc_cStat.Value;
    QrNFeCabAxMotivo.Value := QrNFeCabAinfCanc_xMotivo.Value;
  end else begin
    QrNFeCabAcStat.Value := QrNFeCabAinfProt_cStat.Value;
    QrNFeCabAxMotivo.Value := QrNFeCabAinfProt_xMotivo.Value;
  end;
    QrNFeCabAcStat_xMotivo.Value := IntToStr(QrNFeCabAcStat.Value) + ': ' +
      QrNFeCabAxMotivo.Value;
  //
  QrNFeCabASTATUS_TXT.Value := NFeXMLGeren.Texto_StatusNFe(QrNFeCabAStatus.Value, QrNFeCabAversao.Value);
end;

procedure TFmNFeMPInn.QrNFeMPInnAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  QueryPrincipalAfterOpen;
  Habilita := QrNFeMPInn.RecordCount > 0;
  //BtVolume.Enabled  := Habilita;
  BtEncerra.Enabled := Habilita;
end;

procedure TFmNFeMPInn.QrNFeMPInnAfterScroll(DataSet: TDataSet);
var
  Habilitado: Boolean;
begin
  Habilitado          := QrNFeMPInnEncerrou.Value = 0;
  BtDetail.Enabled    := Habilitado;
  BtSubDetail.Enabled := Habilitado;
  //
  ReopenQrNFeMPIts(0);
  ReopenFatPedNFs(0);
end;

procedure TFmNFeMPInn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeMPInn.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFeMPInnCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NFeMPInn', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFeMPInn.SbRegrFiscalClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFisRegCad;
  if VAR_CADASTRO <> 0 then
  begin
    QrFisRegCad.Close;
    UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
    if QrFisRegCad.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdRegrFiscal.ValueVariant := QrFisRegCadCodUsu.Value;
      CBRegrFiscal.KeyValue     := QrFisRegCadCodUsu.Value;
    end;
  end;
end;

procedure TFmNFeMPInn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeMPInn.IncluiDetail1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmNFeMPInn.IncluiMaster1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeMPInn, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'NFeMPInn');
  {
  if ImgTipo.SQLType = stIns then
  }
    EdAbertura.Text := Geral.FDT(DModG.ObtemAgora(), 11)
  {
  else
    EdAbertura.Text := FormatDateTime('yyyy-mm-dd hh:nn:ss', QrNFeMPInnAbertura.Value);
  }
end;

procedure TFmNFeMPInn.QrNFeMPInnBeforeClose(DataSet: TDataSet);
begin
  QrNFeMPIts.Close;
  QrFatPedNFs.Close;
  BtEncerra.Enabled := False;
end;

procedure TFmNFeMPInn.QrNFeMPInnBeforeOpen(DataSet: TDataSet);
begin
  QrNFeMPInnCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFeMPInn.QrNFeMPInnCalcFields(DataSet: TDataSet);
begin
  QrNFeMPInnENCERROU_TXT.Value := dmkPF.FDT_Nulo(QrNFeMPInnEncerrou.Value, 11);
end;

procedure TFmNFeMPInn.QrNFeMPItsAfterScroll(DataSet: TDataSet);
begin
  ReopenMPInIts(0);
end;

procedure TFmNFeMPInn.QrNFeMPItsBeforeClose(DataSet: TDataSet);
begin
  QrMPInIts.Close;
end;

procedure TFmNFeMPInn.AtualizaItem(Controle, Codigo: Integer);
var
  P2: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    QrTot.Close;
    QrTot.Params[0].AsInteger := Controle;
    UnDmkDAC_PF.AbreQuery(QrTot, Dmod.MyDB);
    P2 := Geral.ConverteArea(QrTotM2.Value, ctM2toP2, cfQuarto);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfempits', False, [
      'Pecas', 'PLE', 'AreaM2', 'AreaP2',  'CMPValor'
    ], ['Controle'], [
      QrTotPecas.Value, QrTotPLE.Value, QrTotM2.Value, P2, QrTotCMPValor.Value
    ], [Controle], True) then
    begin
      FmNFeMPInn.AtualizaTotais(Codigo);
      FmNFeMPInn.LocCod(Codigo, Codigo);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.

