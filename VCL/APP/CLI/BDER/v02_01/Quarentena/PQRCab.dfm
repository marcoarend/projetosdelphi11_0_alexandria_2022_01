object FmPQRCab: TFmPQRCab
  Left = 368
  Top = 194
  Caption = 'QUI-RETOR-001 :: Retorno de Insumos de Terceiros'
  ClientHeight = 574
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 478
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 205
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 127
        Height = 13
        Caption = 'Data da sa'#237'da do estoque:'
      end
      object TLabel
        Left = 148
        Top = 96
        Width = 66
        Height = 13
        Caption = 'N'#250'mero NF-e:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 637
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsClientes
        TabOrder = 3
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPData: TdmkEditDateTimePicker
        Left = 16
        Top = 112
        Width = 128
        Height = 21
        Date = 41822.850512048610000000
        Time = 41822.850512048610000000
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'Data'
        UpdCampo = 'Data'
        UpdType = utYes
      end
      object EdNF_RC: TdmkEdit
        Left = 148
        Top = 112
        Width = 80
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NF_RC'
        UpdCampo = 'NF_RC'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '0'
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 415
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 478
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 168
    ExplicitTop = 176
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 97
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label6: TLabel
        Left = 552
        Top = 56
        Width = 127
        Height = 13
        Caption = 'Data da sa'#237'da do estoque:'
      end
      object TLabel
        Left = 684
        Top = 56
        Width = 66
        Height = 13
        Caption = 'N'#250'mero NF-e:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPQRCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPQRCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsPQRCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 76
        Top = 72
        Width = 473
        Height = 21
        DataField = 'NO_CLI'
        DataSource = DsPQRCab
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 684
        Top = 72
        Width = 80
        Height = 21
        DataField = 'NF_RC'
        DataSource = DsPQRCab
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 552
        Top = 72
        Width = 128
        Height = 21
        DataField = 'Data'
        DataSource = DsPQRCab
        TabOrder = 5
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 414
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 568
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Retorno'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 423
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Insumos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 97
      Width = 1008
      Height = 192
      Align = alTop
      Caption = ' Itens deste retorno:'
      TabOrder = 2
      object DGDados: TDBGrid
        Left = 2
        Top = 15
        Width = 415
        Height = 175
        Align = alLeft
        DataSource = DsPQRPQs
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Insumo'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PQ'
            Title.Caption = 'Nome do insumo'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Title.Caption = 'Quantidade'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Width = 80
            Visible = True
          end>
      end
      object DBGrid1: TDBGrid
        Left = 581
        Top = 15
        Width = 425
        Height = 175
        Align = alClient
        DataSource = DsPQRIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Title.Caption = 'Quantidade'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Caption = 'Pre'#231'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OriCodiOut'
            Title.Caption = 'Pesagem'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OriCtrlOut'
            Title.Caption = 'ID Baixa'
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 417
        Top = 15
        Width = 164
        Height = 175
        Align = alLeft
        DataSource = DsPQRNFs
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DataE'
            Title.Caption = 'Entrada'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NF_CC'
            Title.Caption = 'NF CC'
            Width = 66
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Retorno de Insumos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Retorno de Insumos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Retorno de Insumos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrPQRCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPQRCabBeforeOpen
    AfterOpen = QrPQRCabAfterOpen
    BeforeClose = QrPQRCabBeforeClose
    AfterScroll = QrPQRCabAfterScroll
    SQL.Strings = (
      'SELECT pqc.*, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLI'
      'FROM pqrcab pqc'
      'LEFT JOIN entidades ent ON ent.Codigo=pqc.Cliente')
    Left = 84
    Top = 285
    object QrPQRCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQRCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPQRCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPQRCabData: TDateField
      FieldName = 'Data'
    end
    object QrPQRCabNF_RC: TIntegerField
      FieldName = 'NF_RC'
    end
    object QrPQRCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQRCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQRCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQRCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQRCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQRCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQRCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQRCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
  end
  object DsPQRCab: TDataSource
    DataSet = QrPQRCab
    Left = 84
    Top = 333
  end
  object QrPQRIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPQRItsCalcFields
    SQL.Strings = (
      'SELECT pqi.*, emi.Nome NO_RECEITA, emi.Obs'
      'FROM pqrits pqi '
      'LEFT JOIN emit emi ON emi.Codigo=pqi.OriCodiOut'
      'WHERE pqi.Codigo=1'
      'AND pqi.Ocorrecia=2'
      'AND pqi.Insumo=37'
      'AND pqi.OriCodiInn=9'
      'ORDER BY Controle')
    Left = 308
    Top = 285
    object QrPQRItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQRItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPQRItsOriCodiInn: TIntegerField
      FieldName = 'OriCodiInn'
    end
    object QrPQRItsOriCtrlInn: TIntegerField
      FieldName = 'OriCtrlInn'
    end
    object QrPQRItsTipoInn: TIntegerField
      FieldName = 'TipoInn'
    end
    object QrPQRItsOriCodiOut: TIntegerField
      FieldName = 'OriCodiOut'
    end
    object QrPQRItsOriCtrlOut: TIntegerField
      FieldName = 'OriCtrlOut'
    end
    object QrPQRItsTipoOut: TIntegerField
      FieldName = 'TipoOut'
    end
    object QrPQRItsPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrPQRItsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrPQRItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPQRItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQRItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQRItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQRItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQRItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQRItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQRItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQRItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQRItsOcorrecia: TIntegerField
      FieldName = 'Ocorrecia'
    end
    object QrPQRItsNO_RECEITA: TWideStringField
      FieldName = 'NO_RECEITA'
      Size = 100
    end
    object QrPQRItsObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrPQRItsObsCONCAT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ObsCONCAT'
      Size = 512
      Calculated = True
    end
  end
  object DsPQRIts: TDataSource
    DataSet = QrPQRIts
    Left = 308
    Top = 333
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 228
    Top = 236
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      object Insumonico1: TMenuItem
        Caption = 'Insumo '#250'nico'
        OnClick = Insumonico1Click
      end
      object Multiitens1: TMenuItem
        Caption = 'Multi itens'
        OnClick = Multiitens1Click
      end
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      Visible = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 84
    Top = 240
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 280
    Top = 76
    object QrClientesNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.artigosgrupos.Nome'
      Size = 32
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.artigosgrupos.Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 280
    Top = 120
  end
  object QrPQRPQs: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPQRPQsBeforeClose
    AfterScroll = QrPQRPQsAfterScroll
    SQL.Strings = (
      'SELECT pqi.Insumo, SUM(pqi.Peso ) Peso, SUM(pqi.Valor) Valor, '
      
        'IF(SUM(pqi.Peso)  = 0, 0, SUM(pqi.Valor) / SUM(pqi.Peso )) PRECO' +
        'MEDIO, '
      'pq.Nome NO_PQ '
      'FROM pqrits pqi '
      'LEFT JOIN pq ON pq.Codigo=pqi.Insumo '
      'WHERE pqi.Codigo=1 '
      'AND pqi.Ocorrecia=2 '
      'GROUP BY pqi.Insumo '
      'ORDER BY NO_PQ '
      '  ')
    Left = 152
    Top = 284
    object QrPQRPQsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQRPQsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrPQRPQsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPQRPQsNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
    object QrPQRPQsPRECOMEDIO: TFloatField
      FieldName = 'PRECOMEDIO'
    end
  end
  object DsPQRPQs: TDataSource
    DataSet = QrPQRPQs
    Left = 152
    Top = 333
  end
  object frxQUI_RETOR_001_00_A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41825.822973125000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  DetailData1.Visible := <VARF_IMP_TUDO>;                       ' +
        '                                               '
      
        '  SubDetailData1.Visible := <VARF_IMP_TUDO>;                    ' +
        '                                                  '
      'end.')
    OnGetValue = frxQUI_RETOR_001_00_AGetValue
    Left = 228
    Top = 440
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPQRCab
        DataSetName = 'frxDsPQRCab'
      end
      item
        DataSet = frxDsPQRIts
        DataSetName = 'frxDsPQRIts'
      end
      item
        DataSet = frxDsPQRNFs
        DataSetName = 'frxDsPQRNFs'
      end
      item
        DataSet = frxDsPQRPQs
        DataSetName = 'frxDsPQRPQs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Height = 75.590590240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Retorno de Insumos de Clientes de Presta'#231#227'o de Servi'#231'os')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 102.047310000000000000
          Top = 41.574805590000000000
          Width = 476.220780000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 619.842920000000000000
          Top = 41.574805590000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRCab."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 578.268090000000000000
          Top = 41.574805590000000000
          Width = 41.574781180000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data NF:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 41.574830000000000000
          Top = 41.574805590000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRCab."NF_RC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Top = 41.574805590000000000
          Width = 41.574803150000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' NFe:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 68.031540000000010000
          Top = 60.472479999999990000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do insumo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 60.472479999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 453.543600000000000000
          Top = 60.472479999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ M'#233'dio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 377.953000000000000000
          Top = 60.472479999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitValorT: TfrxMemoView
          Left = 302.362400000000000000
          Top = 60.472479999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        Height = 15.118110240000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPQRPQs
        DataSetName = 'frxDsPQRPQs'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 68.031540000000010000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_PQ'
          DataSet = frxDsPQRPQs
          DataSetName = 'frxDsPQRPQs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQRPQs."NO_PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Insumo'
          DataSet = frxDsPQRPQs
          DataSetName = 'frxDsPQRPQs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRPQs."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PRECOMEDIO'
          DataSet = frxDsPQRPQs
          DataSetName = 'frxDsPQRPQs'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRPQs."PRECOMEDIO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Valor'
          DataSet = frxDsPQRPQs
          DataSetName = 'frxDsPQRPQs'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRPQs."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValValorT: TfrxMemoView
          Left = 302.362400000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Peso'
          DataSet = frxDsPQRPQs
          DataSetName = 'frxDsPQRPQs'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPQRPQs."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 351.496290000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        Height = 45.354350240000000000
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPQRNFs
        DataSetName = 'frxDsPQRNFs'
        RowCount = 0
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF CC')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 105.826840000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 173.858380000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID entrada')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 37.795300000000000000
          Top = 15.118120000000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NF_CC'
          DataSet = frxDsPQRNFs
          DataSetName = 'frxDsPQRNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRNFs."NF_CC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 105.826840000000000000
          Top = 15.118120000000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'DataE'
          DataSet = frxDsPQRNFs
          DataSetName = 'frxDsPQRNFs'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRNFs."DataE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 173.858380000000000000
          Top = 15.118120000000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDsPQRNFs
          DataSetName = 'frxDsPQRNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRNFs."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 75.590600000000000000
          Top = 30.236239999999920000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 143.622140000000000000
          Top = 30.236240000000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 211.653680000000000000
          Top = 30.236240000000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pre'#231'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 279.685220000000000000
          Top = 30.236240000000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 347.716760000000000000
          Top = 30.236240000000010000
          Width = 332.598596060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object SubdetailData1: TfrxSubdetailData
        Height = 30.236230240000000000
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPQRIts
        DataSetName = 'frxDsPQRIts'
        RowCount = 0
        object Memo11: TfrxMemoView
          Left = 75.590600000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'OriCtrlOut'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRIts."OriCtrlOut"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 143.622140000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Peso'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRIts."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 211.653680000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Preco'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRIts."Preco"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 279.685220000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Valor'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPQRIts."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 347.716760000000000000
          Width = 332.598596060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_RECEITA'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQRIts."NO_RECEITA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 75.590600000000000000
          Top = 15.118119999999860000
          Width = 604.724756060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'ObsCONCAT'
          DataSet = frxDsPQRIts
          DataSetName = 'frxDsPQRIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPQRIts."ObsCONCAT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPQRCab: TfrxDBDataset
    UserName = 'frxDsPQRCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Cliente=Cliente'
      'Data=Data'
      'NF_RC=NF_RC'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_CLI=NO_CLI')
    DataSet = QrPQRCab
    BCDToCurrency = False
    Left = 84
    Top = 380
  end
  object frxDsPQRPQs: TfrxDBDataset
    UserName = 'frxDsPQRPQs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Insumo=Insumo'
      'Peso=Peso'
      'Valor=Valor'
      'NO_PQ=NO_PQ'
      'PRECOMEDIO=PRECOMEDIO')
    DataSet = QrPQRPQs
    BCDToCurrency = False
    Left = 148
    Top = 380
  end
  object frxDsPQRIts: TfrxDBDataset
    UserName = 'frxDsPQRIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'OriCodiInn=OriCodiInn'
      'OriCtrlInn=OriCtrlInn'
      'TipoInn=TipoInn'
      'OriCodiOut=OriCodiOut'
      'OriCtrlOut=OriCtrlOut'
      'TipoOut=TipoOut'
      'Preco=Preco'
      'Peso=Peso'
      'Valor=Valor'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Insumo=Insumo'
      'Ocorrecia=Ocorrecia'
      'NO_RECEITA=NO_RECEITA'
      'Obs=Obs'
      'ObsCONCAT=ObsCONCAT')
    DataSet = QrPQRIts
    BCDToCurrency = False
    Left = 304
    Top = 380
  end
  object QrPQRNFs: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPQRNFsBeforeClose
    AfterScroll = QrPQRNFsAfterScroll
    SQL.Strings = (
      'SELECT pqe.Codigo, pqe.DataE, pqe.NF_CC'
      'FROM pqrits pqi'
      'LEFT JOIN pqe pqe ON pqi.OriCodiInn=pqe.Codigo'
      'WHERE pqi.Codigo=1'
      'AND pqi.Ocorrecia=2'
      'AND pqi.Insumo=37'
      'GROUP BY pqi.OriCodiInn, pqi.OriCtrlInn, pqi.TipoInn'
      'ORDER BY Controle')
    Left = 228
    Top = 284
    object QrPQRNFsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQRNFsDataE: TDateField
      FieldName = 'DataE'
    end
    object QrPQRNFsNF_CC: TIntegerField
      FieldName = 'NF_CC'
    end
  end
  object DsPQRNFs: TDataSource
    DataSet = QrPQRNFs
    Left = 228
    Top = 333
  end
  object frxDsPQRNFs: TfrxDBDataset
    UserName = 'frxDsPQRNFs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'DataE=DataE'
      'NF_CC=NF_CC')
    DataSet = QrPQRNFs
    BCDToCurrency = False
    Left = 228
    Top = 380
  end
  object PMImprime: TPopupMenu
    Left = 496
    Top = 140
    object Somenteinsumos1: TMenuItem
      Caption = 'Somente insumos'
      OnClick = Somenteinsumos1Click
    end
    object Insumoseitensdebaixa1: TMenuItem
      Caption = 'Insumos e itens de baixa'
      OnClick = Insumoseitensdebaixa1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Retornosorfos1: TMenuItem
      Caption = 'Retornos orf'#227'os'
      OnClick = Retornosorfos1Click
    end
  end
  object QrBalanCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM balancli'
      'WHERE Periodo=175'
      'AND CliInt=9')
    Left = 312
    Top = 236
    object QrBalanCliPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBalanCliCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrBalanCliGerouDif: TSmallintField
      FieldName = 'GerouDif'
    end
  end
  object QrRetOrf: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPQRItsCalcFields
    SQL.Strings = (
      'SELECT its.*, pq.Nome NO_PQ'
      'FROM pqrits its'
      'LEFT JOIN pq ON pq.Codigo=its.Insumo'
      'WHERE its.TipoInn=-5')
    Left = 380
    Top = 285
    object QrRetOrfCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRetOrfControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrRetOrfInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrRetOrfOcorrecia: TIntegerField
      FieldName = 'Ocorrecia'
    end
    object QrRetOrfOriCodiInn: TIntegerField
      FieldName = 'OriCodiInn'
    end
    object QrRetOrfOriCtrlInn: TIntegerField
      FieldName = 'OriCtrlInn'
    end
    object QrRetOrfTipoInn: TIntegerField
      FieldName = 'TipoInn'
    end
    object QrRetOrfOriCodiOut: TIntegerField
      FieldName = 'OriCodiOut'
    end
    object QrRetOrfOriCtrlOut: TIntegerField
      FieldName = 'OriCtrlOut'
    end
    object QrRetOrfTipoOut: TIntegerField
      FieldName = 'TipoOut'
    end
    object QrRetOrfPreco: TFloatField
      FieldName = 'Preco'
    end
    object QrRetOrfPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrRetOrfValor: TFloatField
      FieldName = 'Valor'
    end
    object QrRetOrfLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRetOrfDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRetOrfDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRetOrfUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRetOrfUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRetOrfAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrRetOrfAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrRetOrfNO_PQ: TWideStringField
      FieldName = 'NO_PQ'
      Size = 50
    end
  end
  object frxDsRetOrf: TfrxDBDataset
    UserName = 'frxDsRetOrf'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Insumo=Insumo'
      'Ocorrecia=Ocorrecia'
      'OriCodiInn=OriCodiInn'
      'OriCtrlInn=OriCtrlInn'
      'TipoInn=TipoInn'
      'OriCodiOut=OriCodiOut'
      'OriCtrlOut=OriCtrlOut'
      'TipoOut=TipoOut'
      'Preco=Preco'
      'Peso=Peso'
      'Valor=Valor'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PQ=NO_PQ')
    DataSet = QrRetOrf
    BCDToCurrency = False
    Left = 376
    Top = 332
  end
  object frxQUI_RETOR_001_01: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41825.822973125000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxQUI_RETOR_001_00_AGetValue
    Left = 404
    Top = 380
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsRetOrf
        DataSetName = 'frxDsRetOrf'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Height = 75.590590240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Retornos Orf'#227'os de Insumos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 204.094620000000000000
          Top = 60.472479999999990000
          Width = 249.448784720000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do insumo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 136.063080000000000000
          Top = 60.472479999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 604.724800000000000000
          Top = 60.472479999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ M'#233'dio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 529.134199999999900000
          Top = 60.472479999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitValorT: TfrxMemoView
          Left = 453.543600000000000000
          Top = 60.472479999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Top = 60.472479999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID Retorno')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 68.031540000000010000
          Top = 60.472479999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID Item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        Height = 15.118110240000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        DataSet = frxDsRetOrf
        DataSetName = 'frxDsRetOrf'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 204.094620000000000000
          Width = 249.448784720000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_PQ'
          DataSet = frxDsRetOrf
          DataSetName = 'frxDsRetOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRetOrf."NO_PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 136.063080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Insumo'
          DataSet = frxDsRetOrf
          DataSetName = 'frxDsRetOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRetOrf."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Preco'
          DataSet = frxDsRetOrf
          DataSetName = 'frxDsRetOrf'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRetOrf."Preco"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 529.134199999999900000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Valor'
          DataSet = frxDsRetOrf
          DataSetName = 'frxDsRetOrf'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRetOrf."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValValorT: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Peso'
          DataSet = frxDsRetOrf
          DataSetName = 'frxDsRetOrf'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRetOrf."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDsRetOrf
          DataSetName = 'frxDsRetOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRetOrf."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 68.031540000000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Controle'
          DataSet = frxDsRetOrf
          DataSetName = 'frxDsRetOrf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRetOrf."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
end
