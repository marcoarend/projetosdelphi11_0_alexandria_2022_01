object FmNFeMPAdd: TFmNFeMPAdd
  Left = 339
  Top = 185
  Caption = 'NFe-MPINN-003 :: Adi'#231#227'o de Entrada de Couros'
  ClientHeight = 563
  ClientWidth = 916
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 916
    Height = 407
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 101
    ExplicitHeight = 398
    object DBGAdd: TdmkDBGrid
      Left = 0
      Top = 48
      Width = 916
      Height = 359
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 22
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CLIINT'
          Title.Caption = 'Dono do couro'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emissao'
          Title.Caption = 'Emiss'#227'o'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Fornecedor do couro'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Caminhao'
          Title.Caption = 'Caminh'#227'o'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PLE'
          Title.Caption = 'PLE*'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CMPValor'
          Title.Caption = 'Valor'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Title.Caption = 'ID'
          Width = 56
          Visible = True
        end>
      Color = clWindow
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGAddCellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 22
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CLIINT'
          Title.Caption = 'Dono do couro'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emissao'
          Title.Caption = 'Emiss'#227'o'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Fornecedor do couro'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Caminhao'
          Title.Caption = 'Caminh'#227'o'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PLE'
          Title.Caption = 'PLE*'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CMPValor'
          Title.Caption = 'Valor'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Title.Caption = 'ID'
          Width = 56
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 916
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 914
      object Label2: TLabel
        Left = 648
        Top = 4
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        FocusControl = DBEdit1
      end
      object Label3: TLabel
        Left = 732
        Top = 4
        Width = 27
        Height = 13
        Caption = 'PLE*:'
        FocusControl = DBEdit2
      end
      object Label4: TLabel
        Left = 816
        Top = 4
        Width = 46
        Height = 13
        Caption = 'Valor MP:'
        FocusControl = DBEdit3
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 5
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 117
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtNenhumClick
      end
      object DBEdit1: TDBEdit
        Left = 648
        Top = 20
        Width = 80
        Height = 21
        DataField = 'Pecas'
        DataSource = DsTot
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 732
        Top = 20
        Width = 80
        Height = 21
        DataField = 'PLE'
        DataSource = DsTot
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 816
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CMPValor'
        DataSource = DsTot
        TabOrder = 4
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 916
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 868
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 820
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 357
        Height = 32
        Caption = 'Adi'#231#227'o de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 357
        Height = 32
        Caption = 'Adi'#231#227'o de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 357
        Height = 32
        Caption = 'Adi'#231#227'o de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 455
    Width = 916
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 912
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 499
    Width = 916
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 236
    ExplicitWidth = 635
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 912
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object Label1: TLabel
        Left = 144
        Top = 20
        Width = 164
        Height = 13
        Caption = 'PLE*: Peso l'#237'quido de entrada (kg)'
      end
      object PnSaiDesis: TPanel
        Left = 768
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrTot: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrTotAfterOpen
    BeforeClose = QrTotBeforeClose
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(PLE) PLE,'
      'SUM(CMPValor) CMPValor, SUM(M2) M2,'
      'COUNT(Ativo) Ativos'
      'FROM nfempadd'
      'WHERE Ativo=1')
    Left = 540
    Top = 128
    object QrTotPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrTotPLE: TFloatField
      FieldName = 'PLE'
      DisplayFormat = '#,###,##0.000'
    end
    object QrTotCMPValor: TFloatField
      FieldName = 'CMPValor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTotM2: TFloatField
      FieldName = 'M2'
    end
    object QrTotAtivos: TLargeintField
      FieldName = 'Ativos'
      Required = True
    end
  end
  object DsTot: TDataSource
    DataSet = QrTot
    Left = 568
    Top = 128
  end
end
