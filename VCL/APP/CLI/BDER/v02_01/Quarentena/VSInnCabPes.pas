unit VSInnCabPes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, mySQLDbTables;

type
  TFmVSInnCabPes = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    CBSerieFch: TdmkDBLookupComboBox;
    EdSerieFch: TdmkEditCB;
    Label11: TLabel;
    EdFicha: TdmkEdit;
    Label4: TLabel;
    Label12: TLabel;
    EdMarca: TdmkEdit;
    EdControle: TdmkEdit;
    Label6: TLabel;
    DsVSSerFch: TDataSource;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    BtReabre: TBitBtn;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqControle: TIntegerField;
    QrPesqNO_SerieFch: TWideStringField;
    QrPesqFicha: TIntegerField;
    QrPesqMarca: TWideStringField;
    QrPesqCodigo: TIntegerField;
    DGDados: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure LocalizaRegistro;
  public
    { Public declarations }
    FCodigo, FControle: Integer;
  end;

  var
  FmVSInnCabPes: TFmVSInnCabPes;

implementation

uses UnMyObjects, Module, UnVS_PF, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSInnCabPes.BtOKClick(Sender: TObject);
begin
  LocalizaRegistro;
end;

procedure TFmVSInnCabPes.BtReabreClick(Sender: TObject);
var
  Marca, SQL1, SQL2, SQL3, SQL4: String;
  Controle, SerieFch, Ficha: Integer;
begin
  Controle := EdControle.ValueVariant;
  SerieFch := EdSerieFch.ValueVariant;
  Ficha    := EdFicha.ValueVariant;
  Marca    := EdMarca.Text;
  //
  if Controle <> 0 then
    SQL1 := 'AND wmi.Controle=' + Geral.FF0(Controle)
  else
    SQL1 := '';
  //
  if SerieFch <> 0 then
    SQL2 := 'AND vsf.SerieFch=' + Geral.FF0(SerieFch)
  else
    SQL2 := '';
  //
  if Ficha <> 0 then
    SQL3 := 'AND wmi.Ficha=' + Geral.FF0(Ficha)
  else
    SQL3 := '';
  //
  if Marca <> '' then
    SQL4 := 'AND wmi.Marca=' + Marca
  else
    SQL4 := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT wmi.Codigo, wmi.Controle, vsf.Nome NO_SerieFch, ',
    'wmi.Ficha, wmi.Marca ',
    'FROM vsmovits wmi ',
    'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ',
    'WHERE wmi.Ativo = 1 ',
    SQL1,
    SQL2,
    SQL3,
    SQL4,
    '']);
end;

procedure TFmVSInnCabPes.BtSaidaClick(Sender: TObject);
begin
  FCodigo   := 0;
  FControle := 0;
  Close;
end;

procedure TFmVSInnCabPes.DGDadosDblClick(Sender: TObject);
begin
  LocalizaRegistro;
end;

procedure TFmVSInnCabPes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSInnCabPes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VS_PF.AbreVSSerFch(QrVSSerFch);
end;

procedure TFmVSInnCabPes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSInnCabPes.FormShow(Sender: TObject);
begin
  EdControle.SetFocus;
end;

procedure TFmVSInnCabPes.LocalizaRegistro;
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    FCodigo   := QrPesqCodigo.Value;
    FControle := QrPesqControle.Value;
    Close;
  end;
end;

end.
