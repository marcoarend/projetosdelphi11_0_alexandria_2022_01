object FmPQRAjuOut: TFmPQRAjuOut
  Left = 339
  Top = 185
  Caption = 'QUI-RETOR-004 :: Retorno Retroativo de Insumos'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 376
        Height = 32
        Caption = 'Retorno Retroativo de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 376
        Height = 32
        Caption = 'Retorno Retroativo de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 376
        Height = 32
        Caption = 'Retorno Retroativo de Insumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 508
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 508
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 508
        Align = alClient
        TabOrder = 0
        object PnCliente: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 0
            Width = 70
            Height = 13
            Caption = 'Cliente interno:'
          end
          object Label1: TLabel
            Left = 688
            Top = 0
            Width = 52
            Height = 13
            Caption = 'At'#233' a data:'
          end
          object EdCliente: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 64
            Top = 16
            Width = 617
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsClientes
            TabOrder = 1
            dmkEditCB = EdCliente
            QryCampo = 'Cliente'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object TPFinal: TdmkEditDateTimePicker
            Left = 688
            Top = 16
            Width = 112
            Height = 21
            Date = 401768.817570914400000000
            Time = 401768.817570914400000000
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 61
          Width = 808
          Height = 445
          ActivePage = TabSheet2
          Align = alClient
          TabOrder = 1
          object TabSheet1: TTabSheet
            Caption = 'Abrir arquivo excel'
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 800
              Height = 417
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 800
                Height = 44
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label27: TLabel
                  Left = 8
                  Top = 4
                  Width = 116
                  Height = 13
                  Caption = 'Arquivo a ser carregado:'
                end
                object SbSelArq: TSpeedButton
                  Left = 730
                  Top = 20
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbSelArqClick
                end
                object SbAbre: TSpeedButton
                  Left = 752
                  Top = 20
                  Width = 21
                  Height = 21
                  Caption = '>'
                  OnClick = SbAbreClick
                end
                object EdArq: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 721
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 
                    'C:\_MLArend\Clientes\Colorado Couros\2014 07\NOTAS FISCAS SAIDAS' +
                    ' - MINERVA.xlsx'
                  QryCampo = 'DirNFeGer'
                  UpdCampo = 'DirNFeGer'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 
                    'C:\_MLArend\Clientes\Colorado Couros\2014 07\NOTAS FISCAS SAIDAS' +
                    ' - MINERVA.xlsx'
                  ValWarn = False
                end
              end
              object Grade1: TStringGrid
                Left = 0
                Top = 44
                Width = 800
                Height = 303
                Align = alClient
                ColCount = 2
                DefaultColWidth = 44
                DefaultRowHeight = 18
                RowCount = 2
                TabOrder = 1
              end
              object GBRodaPe: TGroupBox
                Left = 0
                Top = 347
                Width = 800
                Height = 70
                Align = alBottom
                TabOrder = 2
                object PnSaiDesis: TPanel
                  Left = 654
                  Top = 15
                  Width = 144
                  Height = 53
                  Align = alRight
                  BevelOuter = bvNone
                  TabOrder = 1
                  object BtSaida: TBitBtn
                    Tag = 13
                    Left = 12
                    Top = 3
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Sa'#237'da'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtSaidaClick
                  end
                end
                object Panel1: TPanel
                  Left = 2
                  Top = 15
                  Width = 652
                  Height = 53
                  Align = alClient
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 0
                  object BtAbrir: TBitBtn
                    Tag = 10054
                    Left = 12
                    Top = 4
                    Width = 120
                    Height = 40
                    Caption = '&Pr'#233'-carregar'
                    Enabled = False
                    NumGlyphs = 2
                    TabOrder = 0
                    OnClick = BtAbrirClick
                  end
                end
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Produtos n'#227'o localizados'
            ImageIndex = 1
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 800
              Height = 417
              Align = alClient
              DataSource = DsNaoLoc
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Width = 74
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 641
                  Visible = True
                end>
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Dados pr'#233'-carregados'
            ImageIndex = 2
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 800
              Height = 347
              Align = alClient
              DataSource = DsPQRAjuOut
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 347
              Width = 800
              Height = 70
              Align = alBottom
              ParentBackground = False
              TabOrder = 1
              object Panel8: TPanel
                Left = 654
                Top = 15
                Width = 144
                Height = 53
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 1
                object BitBtn1: TBitBtn
                  Tag = 13
                  Left = 12
                  Top = 3
                  Width = 120
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Sa'#237'da'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaidaClick
                end
              end
              object Panel9: TPanel
                Left = 2
                Top = 15
                Width = 652
                Height = 53
                Align = alClient
                BevelOuter = bvNone
                ParentColor = True
                TabOrder = 0
                object BtImporta: TBitBtn
                  Tag = 14
                  Left = 12
                  Top = 4
                  Width = 120
                  Height = 40
                  Caption = '&Importar'
                  Enabled = False
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtImportaClick
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 556
    Width = 812
    Height = 73
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 56
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 307
        Height = 16
        Caption = 'Ser'#227'o baixados apenas as entradas e n'#227'o as baixas!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 307
        Height = 16
        Caption = 'Ser'#227'o baixados apenas as entradas e n'#227'o as baixas!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
      object PB2: TProgressBar
        Left = 0
        Top = 39
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 1
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 512
    Top = 8
    object QrClientesNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.artigosgrupos.Nome'
      Size = 32
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.artigosgrupos.Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 512
    Top = 52
  end
  object QrPQRAjuOut: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPQRAjuOutAfterOpen
    BeforeClose = QrPQRAjuOutBeforeClose
    SQL.Strings = (
      'SELECT *'
      'FROM _pqrajuinn_')
    Left = 168
    Top = 268
    object QrPQRAjuOutData: TDateField
      FieldName = 'Data'
    end
    object QrPQRAjuOutNF: TIntegerField
      FieldName = 'NF'
    end
    object QrPQRAjuOutCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 60
    end
    object QrPQRAjuOutNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrPQRAjuOutUnd: TWideStringField
      FieldName = 'Und'
      Size = 60
    end
    object QrPQRAjuOutQtd: TFloatField
      FieldName = 'Qtd'
    end
    object QrPQRAjuOutVlUni: TFloatField
      FieldName = 'VlUni'
    end
    object QrPQRAjuOutVlTot: TFloatField
      FieldName = 'VlTot'
    end
    object QrPQRAjuOutNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 255
    end
    object QrPQRAjuOutCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 10
    end
    object QrPQRAjuOutAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsPQRAjuOut: TDataSource
    DataSet = QrPQRAjuOut
    Left = 168
    Top = 316
  end
  object QrNaoLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 84
    Top = 268
    object QrNaoLocCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 60
    end
    object QrNaoLocNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsNaoLoc: TDataSource
    DataSet = QrNaoLoc
    Left = 84
    Top = 320
  end
  object QrPQRCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _pqrajuinn_')
    Left = 244
    Top = 268
    object QrPQRCabData: TDateField
      FieldName = 'Data'
    end
    object QrPQRCabNF: TIntegerField
      FieldName = 'NF'
    end
    object QrPQRCabQTD: TFloatField
      FieldName = 'QTD'
    end
    object QrPQRCabVLTOT: TFloatField
      FieldName = 'VLTOT'
    end
  end
  object QrPQRIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _pqrajuinn_')
    Left = 320
    Top = 272
    object QrPQRItsData: TDateField
      FieldName = 'Data'
    end
    object QrPQRItsNF: TIntegerField
      FieldName = 'NF'
    end
    object QrPQRItsCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 60
    end
    object QrPQRItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrPQRItsUnd: TWideStringField
      FieldName = 'Und'
      Size = 60
    end
    object QrPQRItsQtd: TFloatField
      FieldName = 'Qtd'
    end
    object QrPQRItsVlUni: TFloatField
      FieldName = 'VlUni'
    end
    object QrPQRItsVlTot: TFloatField
      FieldName = 'VlTot'
    end
    object QrPQRItsNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 255
    end
    object QrPQRItsCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 10
    end
    object QrPQRItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQRItsPQ: TIntegerField
      FieldName = 'PQ'
    end
  end
  object QrPQCli_: TmySQLQuery
    Database = Dmod.MyDB
    Left = 528
    Top = 248
    object QrPQCli_PQ: TIntegerField
      FieldName = 'PQ'
    end
  end
  object QrPQRBxa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 676
    Top = 141
    object QrPQRBxaOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQRBxaOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQRBxaTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQRBxaDataX: TDateField
      FieldName = 'DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQRBxaAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQRBxaPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object QrPQRInn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 752
    Top = 121
    object QrPQRInnOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQRInnOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQRInnTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQRInnDataX: TDateField
      FieldName = 'DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQRInnAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQRInnPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQRInnNF_CC: TIntegerField
      FieldName = 'NF_CC'
      DisplayFormat = '0;-0; '
    end
    object QrPQRInnRetQtd: TFloatField
      FieldName = 'RetQtd'
    end
  end
  object DsPQRBxa: TDataSource
    DataSet = QrPQRBxa
    Left = 676
    Top = 189
  end
  object DsPQRInn: TDataSource
    DataSet = QrPQRInn
    Left = 752
    Top = 189
  end
  object QrSumBxa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 676
    Top = 236
    object QrSumBxaAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumBxaPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object QrSumInn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 752
    Top = 236
    object QrSumInnAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumInnPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object DsSumBxa: TDataSource
    DataSet = QrSumBxa
    Left = 676
    Top = 285
  end
  object DsSumInn: TDataSource
    DataSet = QrSumInn
    Left = 752
    Top = 285
  end
end
