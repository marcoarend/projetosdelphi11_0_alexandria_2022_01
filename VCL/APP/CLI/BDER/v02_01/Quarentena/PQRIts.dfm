object FmPQRIts: TFmPQRIts
  Left = 339
  Top = 185
  Caption = 'QUI-RETOR-002 :: Sele'#231#227'o de Insumos para Retorno'
  ClientHeight = 561
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 109
    Width = 1008
    Height = 338
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object CkContinuar: TCheckBox
      Left = 168
      Top = 160
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 297
      Top = 0
      Width = 711
      Height = 338
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Splitter2: TSplitter
        Left = 0
        Top = 255
        Width = 711
        Height = 5
        Cursor = crVSplit
        Align = alBottom
        ExplicitTop = 261
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 711
        Height = 255
        Align = alClient
        Caption = ' Baixas sem retorno completo:'
        TabOrder = 0
        object DBGPQRBxa: TDBGrid
          Left = 2
          Top = 45
          Width = 707
          Height = 208
          Align = alClient
          DataSource = DsPQRBxa
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'DataX'
              Title.Caption = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrigemCodi'
              Title.Caption = 'Pesagem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRECO'
              Title.Caption = 'Pre'#231'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AbertoKg'
              Title.Caption = 'Qtd aberto'
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 707
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label2: TLabel
            Left = 136
            Top = 8
            Width = 53
            Height = 13
            Caption = 'Qtd aberto:'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 4
            Top = 8
            Width = 62
            Height = 13
            Caption = 'Pre'#231'o m'#233'dio:'
            FocusControl = DBEdit2
          end
          object DBEdit1: TDBEdit
            Left = 192
            Top = 4
            Width = 96
            Height = 21
            DataField = 'AbertoKg'
            DataSource = DsSumBxa
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 68
            Top = 4
            Width = 64
            Height = 21
            DataField = 'PRECO'
            DataSource = DsSumBxa
            TabOrder = 1
          end
        end
      end
      object GroupBox6: TGroupBox
        Left = 0
        Top = 260
        Width = 711
        Height = 78
        Align = alBottom
        Caption = ' Redu'#231#227'o de saldo: '
        TabOrder = 1
        object DBGrid3: TDBGrid
          Left = 2
          Top = 15
          Width = 707
          Height = 61
          Align = alClient
          DataSource = DsRedBxa
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'OrigemCtrl'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Width = 80
              Visible = True
            end>
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 297
      Height = 338
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object Splitter1: TSplitter
        Left = 0
        Top = 255
        Width = 297
        Height = 5
        Cursor = crVSplit
        Align = alBottom
        ExplicitLeft = 32
        ExplicitTop = 284
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 297
        Height = 255
        Align = alClient
        Caption = ' Entradas sem retorno completo:'
        TabOrder = 0
        object DBGPQRInn: TDBGrid
          Left = 2
          Top = 45
          Width = 293
          Height = 208
          Align = alClient
          DataSource = DsPQRInn
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'DataX'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF_CC'
              Title.Caption = 'NF Cob. Cli.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRECO'
              Title.Caption = 'Pre'#231'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AbertoKg'
              Title.Caption = 'Qtd aberto'
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 293
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label7: TLabel
            Left = 136
            Top = 8
            Width = 53
            Height = 13
            Caption = 'Qtd aberto:'
            FocusControl = DBEdit3
          end
          object Label8: TLabel
            Left = 4
            Top = 8
            Width = 62
            Height = 13
            Caption = 'Pre'#231'o m'#233'dio:'
            FocusControl = DBEdit4
          end
          object DBEdit3: TDBEdit
            Left = 192
            Top = 4
            Width = 96
            Height = 21
            DataField = 'AbertoKg'
            DataSource = DsSumInn
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 68
            Top = 4
            Width = 64
            Height = 21
            DataField = 'PRECO'
            DataSource = DsSumInn
            TabOrder = 1
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 260
        Width = 297
        Height = 78
        Align = alBottom
        Caption = ' Redu'#231#227'o de saldo: '
        TabOrder = 1
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 293
          Height = 61
          Align = alClient
          DataSource = DsRedInn
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'OrigemCtrl'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Width = 80
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 413
        Height = 32
        Caption = 'Sele'#231#227'o de Insumos para Retorno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 413
        Height = 32
        Caption = 'Sele'#231#227'o de Insumos para Retorno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 413
        Height = 32
        Caption = 'Sele'#231#227'o de Insumos para Retorno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 447
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 491
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Saida'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 10
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Baixa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtLimpeza: TBitBtn
        Tag = 68
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Limpeza'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtLimpezaClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 61
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GroupBox1: TGroupBox
      Left = 613
      Top = 0
      Width = 395
      Height = 61
      Align = alClient
      Caption = ' Dados do cabe'#231'alho:'
      Enabled = False
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 72
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 36
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TDBEdit
        Left = 72
        Top = 36
        Width = 285
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'Nome'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 613
      Height = 61
      Align = alLeft
      Caption = ' Dados do item: '
      TabOrder = 1
      object Label6: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label1: TLabel
        Left = 96
        Top = 16
        Width = 121
        Height = 13
        Caption = 'Insumo [F4 mostra todos]:'
      end
      object EdControle: TdmkEdit
        Left = 12
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdProduto: TdmkEditCB
        Left = 96
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdProdutoChange
        OnEnter = EdProdutoEnter
        OnExit = EdProdutoExit
        DBLookupComboBox = CBProduto
        IgnoraDBLookupComboBox = False
      end
      object CBProduto: TdmkDBLookupComboBox
        Left = 152
        Top = 32
        Width = 453
        Height = 21
        Color = clWhite
        KeyField = 'PQ'
        ListField = 'NOMEPQ'
        ListSource = DsPQ1
        TabOrder = 2
        dmkEditCB = EdProduto
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object QrPQ1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Nome NOMEPQ, pfo.* '
      'FROM pqfor pfo'
      'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo'
      'WHERE pfo.CI=:P0'
      'AND pfo.IQ=:P1'
      'ORDER BY pq.Nome')
    Left = 260
    Top = 46
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
    object QrPQ1NOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQ1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQ1IQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQ1PQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrPQ1CI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQ1Prazo: TWideStringField
      FieldName = 'Prazo'
    end
    object QrPQ1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQ1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQ1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQ1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQ1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsPQ1: TDataSource
    DataSet = QrPQ1
    Left = 256
    Top = 94
  end
  object QrPQRBxa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 316
    Top = 209
    object QrPQRBxaOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQRBxaOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQRBxaTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQRBxaDataX: TDateField
      FieldName = 'DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQRBxaAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQRBxaPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object DsPQRBxa: TDataSource
    DataSet = QrPQRBxa
    Left = 316
    Top = 257
  end
  object QrPQRInn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 392
    Top = 209
    object QrPQRInnOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQRInnOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQRInnTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQRInnDataX: TDateField
      FieldName = 'DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQRInnAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQRInnPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQRInnNF_CC: TIntegerField
      FieldName = 'NF_CC'
      DisplayFormat = '0;-0; '
    end
    object QrPQRInnRetQtd: TFloatField
      FieldName = 'RetQtd'
    end
  end
  object DsPQRInn: TDataSource
    DataSet = QrPQRInn
    Left = 392
    Top = 257
  end
  object QrSumInn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 392
    Top = 304
    object QrSumInnAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumInnPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object QrSumBxa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 316
    Top = 304
    object QrSumBxaAbertoKg: TFloatField
      FieldName = 'AbertoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumBxaPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object DsSumBxa: TDataSource
    DataSet = QrSumBxa
    Left = 316
    Top = 353
  end
  object DsSumInn: TDataSource
    DataSet = QrSumInn
    Left = 392
    Top = 353
  end
  object PMLimpeza: TPopupMenu
    OnPopup = PMLimpezaPopup
    Left = 188
    Top = 464
    object NFdecobertura1: TMenuItem
      Caption = '&NF de cobertura'
      object ReduzirsaldodaNFdecobertura1: TMenuItem
        Caption = 'Reduzir saldo (inicial)'
        OnClick = ReduzirsaldodaNFdecobertura1Click
      end
      object Excluirreduo1: TMenuItem
        Caption = '&Excluir redu'#231#227'o'
        OnClick = Excluirreduo1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Aumentarsaldoinicial1: TMenuItem
        Caption = 'Aumentar saldo (inicial)'
        OnClick = Aumentarsaldoinicial1Click
      end
      object Excluiraumento1: TMenuItem
        Caption = 'Excluir aumento'
        OnClick = Excluiraumento1Click
      end
    end
    object BaixanoEstoque1: TMenuItem
      Caption = '&Baixa no Estoque'
      object ReduzirSaldonasBaixas1: TMenuItem
        Caption = 'Reduzir Saldo (inicial)'
        OnClick = ReduzirSaldonasBaixas1Click
      end
      object Excluirreduo2: TMenuItem
        Caption = '&Excluir redu'#231#227'o'
        OnClick = Excluirreduo2Click
      end
    end
  end
  object QrRedInn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 392
    Top = 400
    object QrRedInnOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrRedInnPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrRedInnOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrRedInnTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsRedInn: TDataSource
    DataSet = QrRedInn
    Left = 392
    Top = 449
  end
  object QrRedBxa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 316
    Top = 400
    object QrRedBxaOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrRedBxaPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
  end
  object DsRedBxa: TDataSource
    DataSet = QrRedBxa
    Left = 316
    Top = 449
  end
  object QrIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 152
    Top = 252
    object QrItsOriCodiInn: TIntegerField
      FieldName = 'OriCodiInn'
    end
    object QrItsOriCtrlInn: TIntegerField
      FieldName = 'OriCtrlInn'
    end
    object QrItsTipoInn: TIntegerField
      FieldName = 'TipoInn'
    end
    object QrItsOriCodiOut: TIntegerField
      FieldName = 'OriCodiOut'
    end
    object QrItsOriCtrlOut: TIntegerField
      FieldName = 'OriCtrlOut'
    end
    object QrItsTipoOut: TIntegerField
      FieldName = 'TipoOut'
    end
    object QrItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
