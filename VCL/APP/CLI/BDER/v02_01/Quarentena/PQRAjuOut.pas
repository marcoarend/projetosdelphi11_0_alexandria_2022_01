unit PQRAjuOut;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, dmkDBGridZTO, dmkEditDateTimePicker;

type
  TFmPQRAjuOut = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaTitulo1C: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesNome: TWideStringField;
    QrClientesCodigo: TIntegerField;
    DsClientes: TDataSource;
    PnCliente: TPanel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label3: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Label27: TLabel;
    SbSelArq: TSpeedButton;
    SbAbre: TSpeedButton;
    EdArq: TdmkEdit;
    Grade1: TStringGrid;
    TabSheet2: TTabSheet;
    QrPQRAjuOut: TmySQLQuery;
    DsPQRAjuOut: TDataSource;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    QrNaoLoc: TmySQLQuery;
    DsNaoLoc: TDataSource;
    QrPQRAjuOutData: TDateField;
    QrPQRAjuOutNF: TIntegerField;
    QrPQRAjuOutCodigo: TWideStringField;
    QrPQRAjuOutNome: TWideStringField;
    QrPQRAjuOutUnd: TWideStringField;
    QrPQRAjuOutQtd: TFloatField;
    QrPQRAjuOutVlUni: TFloatField;
    QrPQRAjuOutVlTot: TFloatField;
    QrPQRAjuOutNO_Cliente: TWideStringField;
    QrPQRAjuOutCFOP: TWideStringField;
    QrPQRAjuOutAtivo: TSmallintField;
    QrNaoLocCodigo: TWideStringField;
    QrNaoLocNome: TWideStringField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAbrir: TBitBtn;
    GroupBox2: TGroupBox;
    Panel8: TPanel;
    BitBtn1: TBitBtn;
    Panel9: TPanel;
    BtImporta: TBitBtn;
    QrPQRCab: TmySQLQuery;
    QrPQRCabData: TDateField;
    QrPQRCabNF: TIntegerField;
    QrPQRCabQTD: TFloatField;
    QrPQRCabVLTOT: TFloatField;
    QrPQRIts: TmySQLQuery;
    QrPQRItsData: TDateField;
    QrPQRItsNF: TIntegerField;
    QrPQRItsCodigo: TWideStringField;
    QrPQRItsNome: TWideStringField;
    QrPQRItsUnd: TWideStringField;
    QrPQRItsQtd: TFloatField;
    QrPQRItsVlUni: TFloatField;
    QrPQRItsVlTot: TFloatField;
    QrPQRItsNO_Cliente: TWideStringField;
    QrPQRItsCFOP: TWideStringField;
    QrPQRItsAtivo: TSmallintField;
    PB1: TProgressBar;
    PB2: TProgressBar;
    QrPQCli_: TmySQLQuery;
    QrPQCli_PQ: TIntegerField;
    TPFinal: TdmkEditDateTimePicker;
    Label1: TLabel;
    QrPQRBxa: TmySQLQuery;
    QrPQRBxaOrigemCodi: TIntegerField;
    QrPQRBxaOrigemCtrl: TIntegerField;
    QrPQRBxaTipo: TIntegerField;
    QrPQRBxaDataX: TDateField;
    QrPQRBxaAbertoKg: TFloatField;
    QrPQRBxaPRECO: TFloatField;
    QrPQRInn: TmySQLQuery;
    QrPQRInnOrigemCodi: TIntegerField;
    QrPQRInnOrigemCtrl: TIntegerField;
    QrPQRInnTipo: TIntegerField;
    QrPQRInnDataX: TDateField;
    QrPQRInnAbertoKg: TFloatField;
    QrPQRInnPRECO: TFloatField;
    QrPQRInnNF_CC: TIntegerField;
    QrPQRInnRetQtd: TFloatField;
    DsPQRBxa: TDataSource;
    DsPQRInn: TDataSource;
    QrSumBxa: TmySQLQuery;
    QrSumBxaAbertoKg: TFloatField;
    QrSumBxaPRECO: TFloatField;
    QrSumInn: TmySQLQuery;
    QrSumInnAbertoKg: TFloatField;
    QrSumInnPRECO: TFloatField;
    DsSumBxa: TDataSource;
    DsSumInn: TDataSource;
    QrPQRItsPQ: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbSelArqClick(Sender: TObject);
    procedure SbAbreClick(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure QrPQRAjuOutAfterOpen(DataSet: TDataSet);
    procedure QrPQRAjuOutBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FPQRAjuOut: String;
    FItensSB: Integer;
    //
    procedure Baixa(PQRCab, Insumo, CliInt: Integer);
    procedure ReopenDadosProdto(Insumo, Cliente: Integer);
    procedure ReopenPQRBxa(Insumo, Cliente: Integer);
    procedure ReopenPQRInn(Insumo, Cliente: Integer);
  public
    { Public declarations }
  end;

  var
  FmPQRAjuOut: TFmPQRAjuOut;

implementation

uses UnMyObjects, Module, DmkDAC_PF, CreateBlueDerm, ModuleGeral, UMySQLModule,
  UnDmkProcFunc, BlueDermConsts;

{$R *.DFM}

procedure TFmPQRAjuOut.Baixa(PQRCab, Insumo, CliInt: Integer);
const
  Ocorrencia = VAR_PQR_OCORR_EMI_RET;
(*
  ValTitle = 'Retorno de Insumo';
  ValCaption = 'Informe a quantidade a retornar:';
  WidthCaption = Length(ValCaption) * 7;
*)
var
  AllQtd, Peso, Valor, RetQtd, ValMax, ValInn, ValBxa: Double;
  //
  ValorMin, ValorMax: String;
  OriCodiInn, OriCtrlInn, TipoInn, OriCodiOut, OriCtrlOut, TipoOut,
  Controle: Integer;
var
  Data, CodProprio, Nome, Und, NO_Cliente, CFOP: String;
  Codigo, NF, Cliente: Integer;
  Qtd, VlUni, VlTot: Double;
begin
  ReopenDadosProdto(Insumo, CliInt);
  // Nao diminuir das baixas pois podem nao existir!
  // Baixar apenas das entradas!
  if (QrSumInnAbertoKg.Value > 0.001) (*and
     (QrSumBxaAbertoKg.Value > 0.001)*) then
  begin
    //QrPQRBxa.First;
    //QrPQRBxa.DisableControls;
    //
    QrPQRInn.First;
    QrPQRInn.DisableControls;
    //
    try
      //ValMax := QrSumBxaAbertoKg.Value;
      //if QrSumInnAbertoKg.Value < ValMax then
        ValMax := QrSumInnAbertoKg.Value ;
      //
      ValorMin := Geral.FFT(0.001, 3, siNegativo);
      ValorMax := Geral.FFT(ValMax, 3, siNegativo);
      Peso := 0;
      AllQtd := QrPQRItsQtd.Value;
      ValInn := QrPQRInnAbertoKg.Value;
      //ValBxa := QrPQRBxaAbertoKg.Value;
      ValBxa := 0;
      //Insumo := FProduto;
      while AllQtd >= 0.001 do
      begin
        //if (ValInn >= 0.001) and (ValBxa >= 0.001) then
        if (ValInn >= 0.001) (*and (ValBxa >= 0.001)*) then
        begin
          //
          Peso  := dmkPF.MenorFloat([AllQtd, ValInn(*, ValBxa*)]);
          Valor := Peso * QrPQRInnPRECO.Value;
          //
          OriCodiInn := QrPQRInnOrigemCodi.Value;
          OriCtrlInn := QrPQRInnOrigemCtrl.Value;
          TipoInn    := QrPQRInnTipo.Value;
          OriCodiOut := 0;//QrPQRBxaOrigemCodi.Value;
          OriCtrlOut := 0;//QrPQRBxaOrigemCtrl.Value;
          TipoOut    := 0;//QrPQRBxaTipo.Value;
          //
          Controle := Dmod.PQR_InsereItemPQRIts(PQRCab,
            OriCodiInn, OriCtrlInn, TipoInn,
            OriCodiOut, OriCtrlOut, TipoOut,
            Insumo, Peso, Valor, Ocorrencia);
          //
          AllQtd := AllQtd - Peso;
          ValInn := ValInn - Peso;
          //ValBxa := ValBxa - Peso;
          //
          if ValInn < 0.001 then
          begin
            if QrPQRInn.RecNo <> QrPQRInn.RecordCount then
            begin
              QrPQRInn.Next;
              //
              ValInn := QrPQRInnAbertoKg.Value;
            end;
          end;
          //
          (*
          if ValBxa < 0.001 then
          begin
            if QrPQRBxa.RecNo <> QrPQRBxa.RecordCount then
            begin
              QrPQRBxa.Next;
              //
              ValBxa := QrPQRBxaAbertoKg.Value;
            end;
          end;
          *)
        end else
        begin
          FItensSB := FItensSB + 1;
          //
          Peso  := AllQtd;
          Valor := Peso * QrPQRInnPRECO.Value;
          //
          OriCodiInn := 0; //QrPQRInnOrigemCodi.Value;
          OriCtrlInn := 0; //QrPQRInnOrigemCtrl.Value;
          TipoInn    := VAR_FATID__005;
          OriCodiOut := 0; //QrPQRBxaOrigemCodi.Value;
          OriCtrlOut := 0; //QrPQRBxaOrigemCtrl.Value;
          TipoOut    := 0; //VAR_FATID__005;
          //
          Controle := Dmod.PQR_InsereItemPQRIts(PQRCab,
            OriCodiInn, OriCtrlInn, TipoInn,
            OriCodiOut, OriCtrlOut, TipoOut,
            Insumo, Peso, Valor, Ocorrencia);
          //
          AllQtd := AllQtd - Peso;
          //ValInn := ValInn - Peso;
          //ValBxa := ValBxa - Peso;
          //
(*
          Data           := Geral.FDT(QrPQRItsQtd.Value);
          NF             := QrPQRItsNF.Value;
          CodProprio     := QrPQRItsCodigo.Value;
          Nome           := QrPQRItsNome.Value;
          Und            := QrPQRItsUnd.Value;
          Qtd            := QrPQRItsQtd.Value;
          VlUni          := QrPQRItsVlUni.Value;
          VlTot          := QrPQRItsVlTot.Value;
          Cliente        := CliInt;
          NO_Cliente     := QrPQRItsNO_Cliente.Value;
          CFOP           := QrPQRItsCFOP.Value;
          //
          Codigo := UMyMod.BPGS1I32('pqrant', 'Codigo', '', '', tsPos, stIns, 0);
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqrant', False, [
          'Data', 'NF', 'CodProprio',
          'Nome', 'Und', 'Qtd',
          'VlUni', 'VlTot', 'Cliente',
          'NO_Cliente', 'CFOP'], [
          'Codigo'], [
          Data, NF, CodProprio,
          Nome, Und, Qtd,
          VlUni, VlTot, Cliente,
          NO_Cliente, CFOP], [
          Codigo], True) then
          begin
          end;
*)
        end;
      end;
    finally
      //QrPQRBxa.EnableControls;
      QrPQRInn.EnableControls;
    end;
  end;
end;

procedure TFmPQRAjuOut.BtAbrirClick(Sender: TObject);
var
  I, K: Integer;
  _Data, _NF, _Codigo, _Nome, _Unid, _Qtde, _VlUni, _VlTot, _NO_Cli, _CFOP: String;
var
  Data, Codigo, Nome, Und, NO_Cliente, CFOP: String;
  NF: Integer;
  Qtd, VlUni, VlTot: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    QrPQRAjuOut.Close;
    QrNaoLoc.Close;
    //
    FPQRAjuOut :=
      UnCreateBlueDerm.RecriaTempTableNovo(ntrttPQRAjuOut, DModG.QrUpdPID1, False);
    //
    PB1.Position := 0;
    PB1.Max := Grade1.RowCount;
    for I := 1 to Grade1.RowCount do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      _Data    := Trim(Grade1.Cells[01, I]);
      _NF      := Trim(Grade1.Cells[02, I]);
      _Codigo  := Trim(Grade1.Cells[03, I]);
      _Nome    := Trim(Grade1.Cells[04, I]);
      _Unid    := Trim(Grade1.Cells[05, I]);
      _Qtde    := Trim(Grade1.Cells[06, I]);
      _VlUni   := Trim(Grade1.Cells[07, I]);
      _VlTot   := Trim(Grade1.Cells[08, I]);
      _CFOP    := Trim(Grade1.Cells[09, I]);
      _NO_Cli  := Trim(Grade1.Cells[10, I]);
      //
  (*
      Geral.MB_Aviso(
      'Data    =  ' + Data   + sLineBreak +
      'NF      =  ' + NF     + sLineBreak +
      'Codigo  =  ' + Codigo + sLineBreak +
      'Nome    =  ' + Nome   + sLineBreak +
      'Unid    =  ' + Unid   + sLineBreak +
      'Qtde    =  ' + Qtde   + sLineBreak +
      'VlUni   =  ' + VlUni  + sLineBreak +
      'VlTot   =  ' + VlTot  + sLineBreak +
      'NO_Cli  =  ' + NO_Cli + sLineBreak +
      'CFOP    =  ' + CFOP   + sLineBreak +
      '');
  *)
      if (_Data <> '') and (_Codigo <> '') and (_Qtde <> '') then
      begin
        K := pos('/', _NF);
        if K > 0 then
          _NF := Copy(_NF, 1, K -1);
        //
        Data := Geral.FDT(Geral.ValidaDataBR(_Data, False, False), 1);
        NF             := Geral.IMV(_NF);
        Codigo         := Geral.SoNumero_TT(_Codigo);
        Nome           := _Nome;
        Und            := _Unid;
        Qtd            := Geral.DMV(_Qtde);
        VlUni          := Geral.DMV(_VlUni);
        VlTot          := Geral.DMV(_VlTot);
        NO_Cliente     := _NO_Cli;
        CFOP           := _CFOP;
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FPQRAjuOut, False, [
        'Data', 'NF', 'Codigo',
        'Nome', 'Und', 'Qtd',
        'VlUni', 'VlTot', 'NO_Cliente',
        'CFOP'], [
        ], [
        Data, NF, Codigo,
        Nome, Und, Qtd,
        VlUni, VlTot, NO_Cliente,
        CFOP], [
        ], False);
      end;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNaoLoc, DModG.MyPID_DB, [
    'SELECT DISTINCT pai.Codigo, pai.Nome ',
    'FROM ' + FPQRAjuOut + ' pai ',
    'LEFT JOIN ' + TMeuDB + '.pqcli pqc ON pqc.CodProprio=pai.Codigo ',
    'WHERE pqc.Controle IS NULL ',
    'ORDER BY pai.Codigo, pai.Nome ',
    '']);
    if QrNaoLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Importa��o cancelada!' + sLineBreak +
      Geral.FF0(QrNaoLoc.RecordCount) + ' insumos n�o foram localizados');
      //
      PageControl1.ActivePageIndex := 1;
      //
      Exit;
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQRAjuOut, DModG.MyPID_DB, [
      'SELECT * ',
      'FROM ' + FPQRAjuOut,
      'ORDER BY Data, NF, Codigo  ',
      '']);
      //
      PageControl1.ActivePageIndex := 1;
      //
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPQRAjuOut.BtImportaClick(Sender: TObject);
const
  Nome = 'Retorno retroativo Automatico';
var
  Data: String;
  Codigo, CliInt, NF, NF_RC, Cliente: Integer;
  Continua: Boolean;
  //
var
  Controle, Conta, Insumo: Integer;
  PesoVB, PesoVL, ValorItem, TotalCusto, TotalPeso: Double;
var
  DataX, DataF: String;
  OrigemCodi, OrigemCtrl, CliOrig, CliDest: Integer;
  Peso, Valor: Double;
begin
  FItensSB := 0;
  CliInt := EdCliente.ValueVariant;
  if MyObjects.FIC(CliInt = 0, EdCliente, 'Informe o cliente interno') then
    Exit;
  if MyObjects.FIC(TPFinal.Date < 2, TPFinal, 'Informe a data limite (final)!') then
    Exit;
  //
  PnCliente.Enabled := QrPQRAjuOut.RecordCount = 0;
  //
  DataF := Geral.FDT(TPFinal.Date, 1);
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRCab, DModG.MyPID_DB, [
  'SELECT Data, NF, SUM(Qtd) QTD, SUM(VlTot) VLTOT',
  'FROM ' + FPQRAjuOut,
  'WHERE Data <="' + DataF + '" ',
  'GROUP BY Data, NF ',
  'ORDER BY Data, NF',
  '']);
  PB1.Position := 0;
  PB1.Max := QrPQRCab.RecordCount;
  QrPQRCab.First;
  while not QrPQRCab.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    Continua := False;
    NF := QrPQRCabNF.Value;
    //
    Data           := Geral.FDT(QrPQRCabData.Value, 1);
    NF_RC          := NF;
    Cliente        := CliInt;
    //
    Codigo := UMyMod.BPGS1I32('pqrcab', 'Codigo', '', '', tsPos, stIns, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqrcab', False, [
    'Nome', 'Cliente', 'Data',
    'NF_RC'], [
    'Codigo'], [
    Nome, Cliente, Data,
    NF_RC], [
    Codigo], True) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPQRIts, DModG.MyPID_DB, [
(*
      'SELECT * ',
      'FROM ' + FPQRAjuOut,
      'WHERE Data="' + Data + '" ',
      'AND NF=' + Geral.FF0(NF),
      'ORDER BY Codigo ',
*)
      'SELECT pqc.PQ, bxa.*  ',
      'FROM ' + FPQRAjuOut + ' bxa',
      'LEFT JOIN ' + TMeuDB + '.pqcli pqc ON  ',
      '     pqc.CodProprio=bxa.Codigo ',
      '     AND pqc.CI=' + Geral.FF0(CliInt),
      'WHERE bxa.Data="' + Data + '" ',
      'AND bxa.NF=' + Geral.FF0(NF),
      'ORDER BY bxa.Codigo  ',
      '']);
      PB2.Position := 0;
      PB2.Max := QrPQRIts.RecordCount;
      QrPQRIts.First;
      Conta := 0;
      while not QrPQRIts.Eof do
      begin
        MyObjects.UpdPB(PB2, nil, nil);
        //
(*
        UnDmkDAC_PF.AbreMySQLQuery0(QrPQCli, Dmod.MyDB, [
        'SELECT PQ ',
        'FROM pqcli ',
        'WHERE CI=' + Geral.FF0(CliInt),
        'AND CodProprio="' + QrPQRItsCodigo.Value + '" ',
        '']);
        if QrPQCli.RecordCount <> 1 then
        begin
          Geral.MB_Aviso('ERRO! Localizado(s) ' + Geral.FF0(QrPQCli.RecordCount) +
          ' relacionamentos de cadastros para o insumo: ' + sLineBreak +
          QrPQRItsCodigo.Value + ': ' + QrPQRItsNome.Value);
        end;
        Baixa(Codigo, QrPQCliPQ.Value, Cliente);
*)
        Baixa(Codigo, QrPQRItsPQ.Value, Cliente);
        //
        QrPQRIts.Next;
      end;
    end;
    QrPQRCab.Next;
  end;
  if FItensSB > 0 then
    Geral.MB_Aviso(Geral.FF0(FItensSB) +
    ' itens de retorno n�o puderam ser atrelados por falta de entrada ou baixa!');
  //
  Geral.MB_Aviso('Importa��o finalizada!' + sLineBreak +
  '� aconselh�vel entrar no �ltimo invent�rio e dar um refresh!');
end;

procedure TFmPQRAjuOut.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQRAjuOut.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQRAjuOut.FormCreate(Sender: TObject);
begin
  //ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
end;

procedure TFmPQRAjuOut.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQRAjuOut.QrPQRAjuOutAfterOpen(DataSet: TDataSet);
begin
  BtImporta.Enabled := QrPQRAjuOut.RecordCount > 0;
end;

procedure TFmPQRAjuOut.QrPQRAjuOutBeforeClose(DataSet: TDataSet);
begin
  BtImporta.Enabled := False;
end;

procedure TFmPQRAjuOut.ReopenDadosProdto(Insumo, Cliente: Integer);
begin
  ReopenPQRBxa(Insumo, Cliente);
  ReopenPQRInn(Insumo, Cliente);
end;

procedure TFmPQRAjuOut.ReopenPQRBxa(Insumo, Cliente: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRBxa, Dmod.MyDB, [
  'SELECT pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo,',
  'pqx.DataX, -(pqx.RetQtd + pqx.Peso) AbertoKg, ',
  'IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) PRECO,',
  'pqx.OrigemCodi',
  'FROM pqx pqx',
  'WHERE pqx.CliDest=' + Geral.FF0(Cliente),
  'AND pqx.Insumo=' + Geral.FF0(Insumo),
  'AND pqx.RetQtd + pqx.Peso <> 0',
  'AND pqx.Peso < 0 ',
  'AND (',
  '    pqx.Tipo = -1 OR pqx.Tipo > 100',
  '    ) ',
  'ORDER BY pqx.DataX, pqx.OrigemCodi',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumBxa, Dmod.MyDB, [
  'SELECT -SUM(pqx.RetQtd + pqx.Peso) AbertoKg, ',
  'IF(SUM(pqx.Peso) = 0, 0, SUM(pqx.Valor) / SUM(pqx.Peso)) PRECO ',
  'FROM pqx pqx ',
  'WHERE pqx.CliDest=' + Geral.FF0(Cliente),
  'AND pqx.Insumo=' + Geral.FF0(Insumo),
  'AND pqx.RetQtd + pqx.Peso <> 0 ',
  'AND pqx.Peso < 0 ',
  'AND ( ',
  '    pqx.Tipo = -1 OR pqx.Tipo > 100 ',
  '    ) ',
  'ORDER BY pqx.DataX ',
  '']);
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrRedBxa, Dmod.MyDB, [
  'SELECT pqx.OrigemCtrl, pqx.Peso ',
  'FROM pqx ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID__004),
  'AND pqx.OrigemCodi=' + Geral.FF0(FCodigo),
  'AND pqx.Insumo=' + Geral.FF0(FProduto),
  '']);
*)
end;

procedure TFmPQRAjuOut.ReopenPQRInn(Insumo, Cliente: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQRInn, Dmod.MyDB, [
  'SELECT pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo,',
  'pqx.DataX, (pqx.Peso + pqx.RetQtd) AbertoKg, ',
  'IF(pqx.Peso = 0, 0, pqx.Valor / pqx.Peso) PRECO,',
  'pqx.RetQtd, pqe.NF_CC ',
  'FROM pqx',
  'LEFT JOIN pqe pqe ON pqe.Codigo=pqx.OrigemCodi',
  'WHERE pqx.CliDest=' + Geral.FF0(Cliente),
  'AND pqx.Insumo=' + Geral.FF0(Insumo),
  'AND pqx.RetQtd + pqx.Peso <> 0',
  'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
  'AND pqx.Peso > 0 ',
  'ORDER BY pqx.DataX',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumInn, Dmod.MyDB, [
  'SELECT SUM(pqx.Peso + pqx.RetQtd) AbertoKg, ',
  'IF(SUM(pqx.Peso) = 0, 0, SUM(pqx.Valor) / SUM(pqx.Peso)) PRECO ',
  'FROM pqx ',
  'WHERE pqx.CliDest=' + Geral.FF0(Cliente),
  'AND pqx.Insumo=' + Geral.FF0(Insumo),
  'AND pqx.RetQtd + pqx.Peso <> 0 ',
  'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
  'AND pqx.Peso > 0 ',
  '']);
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrRedInn, Dmod.MyDB, [
  'SELECT pqx.OrigemCodi, pqx.OrigemCtrl, pqx.Tipo, pqx.Peso ',
  'FROM pqx ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID__003),
  'AND pqx.OrigemCodi=' + Geral.FF0(FCodigo),
  'AND pqx.Insumo=' + Geral.FF0(FProduto),
  '']);
*)
end;

procedure TFmPQRAjuOut.SbAbreClick(Sender: TObject);
const
  ColIni = 1;
  RowIni = 1;
  ExcluiLinhas = True;
begin
  MyObjects.Xls_To_StringGrid(Grade1, EdArq.Text, PB1, LaAviso1, LaAviso2);
  BtAbrir.Enabled := Grade1.RowCount > 2;
end;

procedure TFmPQRAjuOut.SbSelArqClick(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Sele��o de arquivo', '', [], Arquivo) then
    EdArq.ValueVariant := Arquivo
  else
    EdArq.ValueVariant := '';
end;

end.
