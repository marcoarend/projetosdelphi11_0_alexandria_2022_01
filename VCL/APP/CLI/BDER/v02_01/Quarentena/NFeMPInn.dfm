object FmNFeMPInn: TFmNFeMPInn
  Left = 368
  Top = 194
  Caption = 'NFe-MPINN-001 :: Agrupamento de Entrada de Couros'
  ClientHeight = 694
  ClientWidth = 1004
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1004
    Height = 598
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1004
      Height = 216
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 4
        Top = 44
        Width = 150
        Height = 13
        Caption = 'Cliente interno (Dono do couro):'
      end
      object Label5: TLabel
        Left = 396
        Top = 44
        Width = 102
        Height = 13
        Caption = 'Fornecedor do couro:'
      end
      object SbRegrFiscal: TSpeedButton
        Left = 570
        Top = 104
        Width = 21
        Height = 21
        OnClick = SbRegrFiscalClick
      end
      object Label17: TLabel
        Left = 668
        Top = 4
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label20: TLabel
        Left = 4
        Top = 88
        Width = 587
        Height = 13
        Caption = 
          'Movimenta'#231#227'o: (aplica'#231#227'o="Entrada de mat'#233'ria-prima", Tipo de mov' +
          'imento="entrada" e movimenta'#231#227'o de estoque="Nulo") '
      end
      object LaCondicaoPG: TLabel
        Left = 4
        Top = 129
        Width = 119
        Height = 13
        Caption = 'Condi'#231#227'o de pagamento:'
      end
      object Label30: TLabel
        Left = 388
        Top = 128
        Width = 75
        Height = 13
        Caption = 'Transportadora:'
      end
      object Label29: TLabel
        Left = 592
        Top = 89
        Width = 45
        Height = 13
        Caption = 'Frete por:'
      end
      object Label25: TLabel
        Left = 4
        Top = 168
        Width = 97
        Height = 13
        Caption = 'Carteira (Financeiro):'
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 516
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdEmpresa: TdmkEditCB
        Left = 4
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 60
        Top = 60
        Width = 332
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDonos
        TabOrder = 5
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFornece: TdmkEditCB
        Left = 396
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fornece'
        UpdCampo = 'Fornece'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 452
        Top = 60
        Width = 332
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornece
        TabOrder = 7
        dmkEditCB = EdFornece
        QryCampo = 'Fornece'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdRegrFiscal: TdmkEditCB
        Left = 4
        Top = 104
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRegrFiscal
        IgnoraDBLookupComboBox = False
      end
      object CBRegrFiscal: TdmkDBLookupComboBox
        Left = 64
        Top = 104
        Width = 505
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsFisRegCad
        TabOrder = 9
        dmkEditCB = EdRegrFiscal
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdAbertura: TdmkEdit
        Left = 668
        Top = 20
        Width = 116
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '30/12/1899 00:00'
        QryCampo = 'Abertura'
        UpdCampo = 'Abertura'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCondicaoPG: TdmkEditCB
        Left = 4
        Top = 144
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCondicaoPG
        IgnoraDBLookupComboBox = False
      end
      object CBCondicaoPG: TdmkDBLookupComboBox
        Left = 64
        Top = 144
        Width = 321
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPediPrzCab
        TabOrder = 13
        dmkEditCB = EdCondicaoPG
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdTransporta: TdmkEditCB
        Left = 388
        Top = 144
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 448
        Top = 144
        Width = 337
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsTransportas
        TabOrder = 15
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdFretePor: TdmkEditCB
        Left = 592
        Top = 104
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FretePor'
        UpdCampo = 'FretePor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFretePor
        IgnoraDBLookupComboBox = False
      end
      object CBFretePor: TdmkDBLookupComboBox
        Left = 616
        Top = 104
        Width = 169
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DmPediVda.DsFretePor
        TabOrder = 11
        dmkEditCB = EdFretePor
        QryCampo = 'FretePor'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdCartEmiss: TdmkEditCB
        Left = 4
        Top = 184
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CartEmiss'
        UpdCampo = 'CartEmiss'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCartEmiss
        IgnoraDBLookupComboBox = False
      end
      object CBCartEmiss: TdmkDBLookupComboBox
        Left = 60
        Top = 184
        Width = 725
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCartEmiss
        TabOrder = 17
        dmkEditCB = EdCartEmiss
        QryCampo = 'CartEmiss'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 534
      Width = 1004
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 1000
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 856
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1004
    Height = 598
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1004
      Height = 212
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      ParentBackground = False
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 788
        Height = 212
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label3: TLabel
          Left = 64
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label2: TLabel
          Left = 148
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label18: TLabel
          Left = 668
          Top = 4
          Width = 43
          Height = 13
          Caption = 'Abertura:'
        end
        object Label6: TLabel
          Left = 4
          Top = 48
          Width = 150
          Height = 13
          Caption = 'Cliente interno (Dono do couro):'
        end
        object Label10: TLabel
          Left = 396
          Top = 48
          Width = 102
          Height = 13
          Caption = 'Fornecedor do couro:'
        end
        object Label16: TLabel
          Left = 4
          Top = 88
          Width = 587
          Height = 13
          Caption = 
            'Movimenta'#231#227'o: (aplica'#231#227'o="Entrada de mat'#233'ria-prima", Tipo de mov' +
            'imento="entrada" e movimenta'#231#227'o de estoque="Nulo") '
        end
        object Label21: TLabel
          Left = 592
          Top = 89
          Width = 45
          Height = 13
          Caption = 'Frete por:'
        end
        object Label22: TLabel
          Left = 4
          Top = 129
          Width = 119
          Height = 13
          Caption = 'Condi'#231#227'o de pagamento:'
        end
        object Label23: TLabel
          Left = 388
          Top = 128
          Width = 75
          Height = 13
          Caption = 'Transportadora:'
        end
        object Label24: TLabel
          Left = 4
          Top = 168
          Width = 97
          Height = 13
          Caption = 'Carteira (Financeiro):'
        end
        object Label11: TLabel
          Left = 304
          Top = 168
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
          FocusControl = DBEdit6
        end
        object Label12: TLabel
          Left = 368
          Top = 168
          Width = 23
          Height = 13
          Caption = 'PLE:'
          FocusControl = DBEdit7
        end
        object Label14: TLabel
          Left = 444
          Top = 168
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
          FocusControl = DBEdit9
        end
        object Label15: TLabel
          Left = 520
          Top = 168
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
          FocusControl = DBEdit10
        end
        object Label13: TLabel
          Left = 596
          Top = 168
          Width = 46
          Height = 13
          Caption = 'Valor MP:'
          FocusControl = DBEdit8
        end
        object Label19: TLabel
          Left = 668
          Top = 168
          Width = 69
          Height = 13
          Caption = 'Encerramento:'
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsNFeMPInn
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit1: TDBEdit
          Left = 64
          Top = 20
          Width = 80
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsNFeMPInn
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object DBEdNome: TdmkDBEdit
          Left = 148
          Top = 20
          Width = 516
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsNFeMPInn
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit13: TDBEdit
          Left = 668
          Top = 20
          Width = 116
          Height = 21
          DataField = 'Abertura'
          DataSource = DsNFeMPInn
          TabOrder = 3
        end
        object DBEdit2: TDBEdit
          Left = 4
          Top = 64
          Width = 56
          Height = 21
          DataField = 'Empresa'
          DataSource = DsNFeMPInn
          TabOrder = 4
        end
        object DBEdit4: TDBEdit
          Left = 60
          Top = 64
          Width = 332
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsNFeMPInn
          TabOrder = 5
        end
        object DBEdit3: TDBEdit
          Left = 396
          Top = 64
          Width = 56
          Height = 21
          DataField = 'Fornece'
          DataSource = DsNFeMPInn
          TabOrder = 6
        end
        object DBEdit5: TDBEdit
          Left = 452
          Top = 64
          Width = 332
          Height = 21
          DataField = 'NO_FORNECE'
          DataSource = DsNFeMPInn
          TabOrder = 7
        end
        object DBEdit11: TDBEdit
          Left = 4
          Top = 104
          Width = 56
          Height = 21
          DataField = 'CU_RegrFiscal'
          DataSource = DsNFeMPInn
          TabOrder = 8
        end
        object DBEdit12: TDBEdit
          Left = 64
          Top = 104
          Width = 525
          Height = 21
          DataField = 'NO_RegrFiscal'
          DataSource = DsNFeMPInn
          TabOrder = 9
        end
        object DBEdit20: TDBEdit
          Left = 592
          Top = 104
          Width = 21
          Height = 21
          DataField = 'FretePor'
          DataSource = DsNFeMPInn
          TabOrder = 10
        end
        object DBEdit17: TDBEdit
          Left = 616
          Top = 104
          Width = 169
          Height = 21
          DataField = 'FRETEPOR_TXT'
          DataSource = DsNFeMPInn
          TabOrder = 11
        end
        object DBEdit16: TDBEdit
          Left = 4
          Top = 144
          Width = 56
          Height = 21
          DataField = 'CU_PediPrzCab'
          DataSource = DsNFeMPInn
          TabOrder = 12
        end
        object DBEdit15: TDBEdit
          Left = 64
          Top = 144
          Width = 321
          Height = 21
          DataField = 'NO_PediPrzCab'
          DataSource = DsNFeMPInn
          TabOrder = 13
        end
        object DBEdit19: TDBEdit
          Left = 388
          Top = 144
          Width = 57
          Height = 21
          DataField = 'Transporta'
          DataSource = DsNFeMPInn
          TabOrder = 14
        end
        object DBEdit18: TDBEdit
          Left = 448
          Top = 144
          Width = 337
          Height = 21
          DataField = 'NO_TRANSPORTA'
          DataSource = DsNFeMPInn
          TabOrder = 15
        end
        object DBEdit21: TDBEdit
          Left = 4
          Top = 184
          Width = 56
          Height = 21
          DataField = 'CartEmiss'
          DataSource = DsNFeMPInn
          TabOrder = 16
        end
        object DBEdit22: TDBEdit
          Left = 60
          Top = 184
          Width = 240
          Height = 21
          DataField = 'NO_CARTEMISS'
          DataSource = DsNFeMPInn
          TabOrder = 17
        end
        object DBEdit6: TDBEdit
          Left = 304
          Top = 184
          Width = 60
          Height = 21
          DataField = 'Pecas'
          DataSource = DsNFeMPInn
          TabOrder = 18
        end
        object DBEdit7: TDBEdit
          Left = 368
          Top = 184
          Width = 72
          Height = 21
          DataField = 'PLE'
          DataSource = DsNFeMPInn
          TabOrder = 19
        end
        object DBEdit9: TDBEdit
          Left = 444
          Top = 184
          Width = 72
          Height = 21
          DataField = 'AreaM2'
          DataSource = DsNFeMPInn
          TabOrder = 20
        end
        object DBEdit10: TDBEdit
          Left = 520
          Top = 184
          Width = 72
          Height = 21
          DataField = 'AreaP2'
          DataSource = DsNFeMPInn
          TabOrder = 21
        end
        object DBEdit8: TDBEdit
          Left = 596
          Top = 184
          Width = 68
          Height = 21
          DataField = 'CMPValor'
          DataSource = DsNFeMPInn
          TabOrder = 22
        end
        object DBEdit14: TDBEdit
          Left = 668
          Top = 184
          Width = 116
          Height = 21
          DataField = 'Encerrou'
          DataSource = DsNFeMPInn
          TabOrder = 23
        end
      end
      object GroupBox1: TGroupBox
        Left = 788
        Top = 0
        Width = 216
        Height = 212
        Align = alClient
        Caption = ' NF-e: '
        TabOrder = 1
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 212
          Height = 58
          Align = alTop
          TabOrder = 0
          object Label26: TLabel
            Left = 8
            Top = 4
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
            FocusControl = DBEdit23
          end
          object Label27: TLabel
            Left = 40
            Top = 4
            Width = 57
            Height = 13
            Caption = 'N'#250'mero NF:'
            FocusControl = DBEdit24
          end
          object Label28: TLabel
            Left = 8
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Status:'
          end
          object DBEdit24: TDBEdit
            Left = 40
            Top = 20
            Width = 72
            Height = 21
            DataField = 'ide_nNF'
            DataSource = DsNFeCabA
            TabOrder = 0
          end
          object DBEdit23: TDBEdit
            Left = 8
            Top = 20
            Width = 28
            Height = 21
            DataField = 'ide_serie'
            DataSource = DsNFeCabA
            TabOrder = 1
          end
        end
        object DBMemo1: TDBMemo
          Left = 2
          Top = 73
          Width = 212
          Height = 137
          Align = alClient
          DataField = 'STATUS_TXT'
          DataSource = DsNFeCabA
          TabOrder = 1
        end
      end
    end
    object DBGMPInnIts: TDBGrid
      Left = 0
      Top = 419
      Width = 1004
      Height = 115
      Align = alBottom
      DataSource = DsMPInIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEPROCEDENCIA'
          Title.Caption = 'Proced'#234'ncia'
          Width = 236
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Caminhao'
          Title.Caption = 'Caminh'#227'o'
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PecasNF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PNF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PLE'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PDA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NF'
          Title.Caption = 'N. F. '
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CMPValor'
          Title.Caption = 'Valor M.P.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emissao'
          Title.Caption = 'Emiss'#227'o'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PS_QtdTot'
          Title.Caption = 'M.O. kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PS_ValUni'
          Title.Caption = 'M.O. $/kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PS_ValTot'
          Title.Caption = 'M.O. $ total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VENCTO_TXT'
          Title.Caption = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conheci'
          Title.Caption = 'Conhec.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CMPFrete'
          Title.Caption = 'Valor frete'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VCTOFRETE_TXT'
          Title.Caption = 'Vcto frete'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EmisFrete'
          Width = 56
          Visible = True
        end>
    end
    object DBGNFeMPIts: TDBGrid
      Left = 0
      Top = 212
      Width = 1004
      Height = 128
      Align = alTop
      DataSource = DsNFeMPIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_GG1'
          Title.Caption = 'Descri'#231#227'o'
          Width = 271
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GerBxaEstq'
          Title.Caption = 'Grandeza'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_COR'
          Title.Caption = 'Cor'
          Width = 134
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TAM'
          Title.Caption = 'Tamanho'
          Width = 57
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PLE'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = 'Area m'#178
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaP2'
          Title.Caption = 'Area ft'#178
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CMPValor'
          Title.Caption = 'Valor'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 56
          Visible = True
        end>
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 534
      Width = 1004
      Height = 64
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 308
        Top = 15
        Width = 694
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 561
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtMaster: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtMasterClick
        end
        object BtDetail: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtDetailClick
        end
        object BtSubDetail: TBitBtn
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'E&ntradas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtSubDetailClick
        end
        object BtEncerra: TBitBtn
          Left = 292
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Encerrar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtEncerraClick
        end
        object BtNFe: TBitBtn
          Tag = 5
          Left = 384
          Top = 4
          Width = 90
          Height = 40
          Caption = '&NF-e'
          NumGlyphs = 2
          TabOrder = 5
          OnClick = BtNFeClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1004
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 956
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 740
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 436
        Height = 32
        Caption = 'Agrupamento de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 436
        Height = 32
        Caption = 'Agrupamento de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 436
        Height = 32
        Caption = 'Agrupamento de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1004
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1000
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFeMPInn: TDataSource
    DataSet = QrNFeMPInn
    Left = 84
    Top = 56
  end
  object QrNFeMPInn: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeMPInnBeforeOpen
    AfterOpen = QrNFeMPInnAfterOpen
    BeforeClose = QrNFeMPInnBeforeClose
    AfterScroll = QrNFeMPInnAfterScroll
    OnCalcFields = QrNFeMPInnCalcFields
    SQL.Strings = (
      'SELECT car.Nome NO_CARTEMISS, ppc.MedDDSimpl, nmi.*,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(emp.Tipo=0, emp.EUF, emp.PUF) EMP_UF,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_TRANSPORTA,'
      'frc.Nome NO_RegrFiscal, frc.CodUsu CU_RegrFiscal,'
      'frn.Tipo FRN_TIPO, frn.IE FRN_IE,'
      'IF(frn.Tipo=0, frn.EUF, frn.PUF) FRN_UF,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'emp.Filial, frc.ModeloNF, frc.infAdFisco,'
      'frc.Financeiro, car.Tipo TIPOCART,'
      'IF(nmi.FretePor=1, "Emitente", "Destinatario") FRETEPOR_TXT,'
      'ppc.Nome NO_PediPrzCab, ppc.CodUsu CU_PediPrzCab,'
      'par.Associada,'
      'par.CtaServico EMP_CtaServico,'
      'par.FaturaSeq EMP_FaturaSeq,'
      'par.FaturaSep EMP_FaturaSep,'
      'par.FaturaDta EMP_FaturaDta,'
      'par.FaturaNum EMP_FaturaNum,'
      'par.TxtServico EMP_TxtServico,'
      'par.DupServico  EMP_DupServico,'
      'emp.Filial EMP_FILIAL,'
      ''
      'ase.Filial ASS_FILIAL,'
      'IF(ase.Tipo=0, ase.EUF, ase.PUF) ASS_CO_UF,'
      ''
      'ass.CtaServico ASS_CtaServico,'
      'ass.FaturaSeq ASS_FaturaSeq,'
      'ass.FaturaSep ASS_FaturaSep,'
      'ass.FaturaDta ASS_FaturaDta,'
      'ass.FaturaNum ASS_FaturaNum,'
      'ass.TxtServico ASS_TxtServico,'
      'ass.DupServico  ASS_DupServico,'
      ''
      'ufe.Nome UF_TXT_Emp, ufc.Nome UF_TXT_Cli'
      ''
      'FROM nfempinn nmi'
      'LEFT JOIN carteiras car ON car.Codigo=nmi.CartEmiss'
      'LEFT JOIN entidades emp ON emp.Codigo=nmi.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=nmi.Fornece'
      'LEFT JOIN entidades tra ON tra.Codigo=nmi.Transporta'
      'LEFT JOIN fisregcad frc ON frc.Codigo=nmi.RegrFiscal'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=nmi.CondicaoPg'
      'LEFT JOIN paramsemp  par ON par.Codigo=nmi.Empresa'
      'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada'
      'LEFT JOIN entidades ase ON ase.Codigo=ass.Codigo'
      'LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, emp.PUF)'
      'LEFT JOIN ufs ufc ON ufc.Codigo=IF(frn.Tipo=0, frn.EUF, frn.PUF)'
      ''
      'WHERE nmi.Codigo > 0'
      '')
    Left = 56
    Top = 56
    object QrNFeMPInnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeMPInnCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeMPInnNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrNFeMPInnEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeMPInnFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrNFeMPInnPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrNFeMPInnPLE: TFloatField
      FieldName = 'PLE'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrNFeMPInnCMPValor: TFloatField
      FieldName = 'CMPValor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeMPInnNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrNFeMPInnNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrNFeMPInnAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeMPInnAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeMPInnEncerrou: TDateTimeField
      FieldName = 'Encerrou'
    end
    object QrNFeMPInnRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrNFeMPInnNO_RegrFiscal: TWideStringField
      FieldName = 'NO_RegrFiscal'
      Size = 50
    end
    object QrNFeMPInnModeloNF: TIntegerField
      FieldName = 'ModeloNF'
    end
    object QrNFeMPInninfAdFisco: TWideStringField
      FieldName = 'infAdFisco'
      Size = 255
    end
    object QrNFeMPInnFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrNFeMPInnEMP_UF: TLargeintField
      FieldName = 'EMP_UF'
    end
    object QrNFeMPInnFRN_TIPO: TSmallintField
      FieldName = 'FRN_TIPO'
    end
    object QrNFeMPInnFRN_IE: TWideStringField
      FieldName = 'FRN_IE'
    end
    object QrNFeMPInnFRN_UF: TLargeintField
      FieldName = 'FRN_UF'
    end
    object QrNFeMPInnNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrNFeMPInnFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrNFeMPInnAssociada: TIntegerField
      FieldName = 'Associada'
    end
    object QrNFeMPInnEMP_CtaServico: TIntegerField
      FieldName = 'EMP_CtaServico'
    end
    object QrNFeMPInnEMP_FaturaSeq: TSmallintField
      FieldName = 'EMP_FaturaSeq'
    end
    object QrNFeMPInnEMP_FaturaSep: TWideStringField
      FieldName = 'EMP_FaturaSep'
      Size = 1
    end
    object QrNFeMPInnEMP_FaturaDta: TSmallintField
      FieldName = 'EMP_FaturaDta'
    end
    object QrNFeMPInnEMP_TxtServico: TWideStringField
      FieldName = 'EMP_TxtServico'
      Size = 100
    end
    object QrNFeMPInnEMP_DupServico: TWideStringField
      FieldName = 'EMP_DupServico'
      Size = 3
    end
    object QrNFeMPInnEMP_FILIAL: TIntegerField
      FieldName = 'EMP_FILIAL'
    end
    object QrNFeMPInnASS_FILIAL: TIntegerField
      FieldName = 'ASS_FILIAL'
    end
    object QrNFeMPInnASS_CO_UF: TLargeintField
      FieldName = 'ASS_CO_UF'
    end
    object QrNFeMPInnASS_CtaServico: TIntegerField
      FieldName = 'ASS_CtaServico'
    end
    object QrNFeMPInnASS_FaturaSeq: TSmallintField
      FieldName = 'ASS_FaturaSeq'
    end
    object QrNFeMPInnASS_FaturaSep: TWideStringField
      FieldName = 'ASS_FaturaSep'
      Size = 1
    end
    object QrNFeMPInnASS_FaturaDta: TSmallintField
      FieldName = 'ASS_FaturaDta'
    end
    object QrNFeMPInnASS_TxtServico: TWideStringField
      FieldName = 'ASS_TxtServico'
      Size = 100
    end
    object QrNFeMPInnASS_DupServico: TWideStringField
      FieldName = 'ASS_DupServico'
      Size = 3
    end
    object QrNFeMPInnUF_TXT_Emp: TWideStringField
      FieldName = 'UF_TXT_Emp'
      Required = True
      Size = 2
    end
    object QrNFeMPInnUF_TXT_Cli: TWideStringField
      FieldName = 'UF_TXT_Cli'
      Required = True
      Size = 2
    end
    object QrNFeMPInnAbertura: TDateTimeField
      FieldName = 'Abertura'
    end
    object QrNFeMPInnCU_RegrFiscal: TIntegerField
      FieldName = 'CU_RegrFiscal'
      Required = True
    end
    object QrNFeMPInnStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrNFeMPInnDataFat: TDateField
      FieldName = 'DataFat'
    end
    object QrNFeMPInnFretePor: TSmallintField
      FieldName = 'FretePor'
    end
    object QrNFeMPInnFRETEPOR_TXT: TWideStringField
      DisplayWidth = 50
      FieldName = 'FRETEPOR_TXT'
      Required = True
      Size = 50
    end
    object QrNFeMPInnTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrNFeMPInnNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrNFeMPInnCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
    end
    object QrNFeMPInnNO_PediPrzCab: TWideStringField
      FieldName = 'NO_PediPrzCab'
      Size = 50
    end
    object QrNFeMPInnCU_PediPrzCab: TIntegerField
      FieldName = 'CU_PediPrzCab'
      Required = True
    end
    object QrNFeMPInnMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
    end
    object QrNFeMPInnENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Calculated = True
    end
    object QrNFeMPInnSerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrNFeMPInnNFDesfeita: TIntegerField
      FieldName = 'NFDesfeita'
    end
    object QrNFeMPInnCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrNFeMPInnNO_CARTEMISS: TWideStringField
      FieldName = 'NO_CARTEMISS'
      Size = 100
    end
    object QrNFeMPInnTIPOCART: TIntegerField
      FieldName = 'TIPOCART'
      Required = True
    end
    object QrNFeMPInnEMP_FaturaNum: TSmallintField
      FieldName = 'EMP_FaturaNum'
    end
    object QrNFeMPInnASS_FaturaNum: TSmallintField
      FieldName = 'ASS_FaturaNum'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 112
    Top = 56
  end
  object PMMaster: TPopupMenu
    OnPopup = PMMasterPopup
    Left = 300
    Top = 504
    object IncluiMaster1: TMenuItem
      Caption = '&Inclui Novo Agrupamento'
      OnClick = IncluiMaster1Click
    end
    object AlteraMaster1: TMenuItem
      Caption = '&Altera o Agrupamento Atual'
      OnClick = AlteraMaster1Click
    end
    object ExcluiMaster1: TMenuItem
      Caption = '&Exclui o Agrupamento Atual'
      Enabled = False
      OnClick = ExcluiMaster1Click
    end
  end
  object PMDetail: TPopupMenu
    OnPopup = PMDetailPopup
    Left = 388
    Top = 504
    object IncluiDetail1: TMenuItem
      Caption = '&Inclui novo item'
      OnClick = IncluiDetail1Click
    end
    object AlteraDetail1: TMenuItem
      Caption = '&Altera item atual'
      OnClick = AlteraDetail1Click
    end
    object ExcluiDetail1: TMenuItem
      Caption = '&Exclui item atual'
      Enabled = False
      OnClick = ExcluiDetail1Click
    end
  end
  object QrNFeMPAdd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfempadd')
    Left = 112
    Top = 284
    object QrNFeMPAddAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrNFeMPAddConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNFeMPAddEmissao: TDateField
      FieldName = 'Emissao'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeMPAddFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrNFeMPAddCaminhao: TIntegerField
      FieldName = 'Caminhao'
    end
    object QrNFeMPAddPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.0'
    end
    object QrNFeMPAddPLE: TFloatField
      FieldName = 'PLE'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrNFeMPAddM2: TFloatField
      FieldName = 'M2'
    end
    object QrNFeMPAddCMPValor: TFloatField
      FieldName = 'CMPValor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeMPAddNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrNFeMPAddNO_CLIINT: TWideStringField
      FieldName = 'NO_CLIINT'
      Size = 100
    end
  end
  object DsNFeMPAdd: TDataSource
    DataSet = QrNFeMPAdd
    Left = 140
    Top = 284
  end
  object QrDonos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 168
    Top = 284
    object QrDonosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDonosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDonos: TDataSource
    DataSet = QrDonos
    Left = 196
    Top = 284
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 196
    Top = 312
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emp.Corretor, emp.Comissao, emp.DescAdiant,'
      'emp.CondPagto, emp.CondComis, emp.LocalEntrg,'
      'emp.PreDescarn, emp.MaskLetras, emp.MaskFormat, '
      'emp.Abate, emp.Transporte, ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'LEFT JOIN entimp emp ON emp.Codigo=ent.Codigo'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 168
    Top = 312
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrFornecePreDescarn: TSmallintField
      FieldName = 'PreDescarn'
    end
    object QrForneceMaskLetras: TWideStringField
      FieldName = 'MaskLetras'
      Size = 10
    end
    object QrForneceMaskFormat: TWideStringField
      FieldName = 'MaskFormat'
      Size = 10
    end
    object QrForneceCorretor: TIntegerField
      FieldName = 'Corretor'
    end
    object QrForneceComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrForneceDescAdiant: TFloatField
      FieldName = 'DescAdiant'
    end
    object QrForneceCondPagto: TWideStringField
      FieldName = 'CondPagto'
      Size = 30
    end
    object QrForneceCondComis: TWideStringField
      FieldName = 'CondComis'
      Size = 30
    end
    object QrForneceLocalEntrg: TWideStringField
      FieldName = 'LocalEntrg'
      Size = 50
    end
    object QrForneceAbate: TIntegerField
      FieldName = 'Abate'
    end
    object QrForneceTransporte: TIntegerField
      FieldName = 'Transporte'
    end
  end
  object QrMPInIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mi.*, '
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEPROCEDENCIA'
      'FROM mpinits mi'
      'LEFT JOIN entidades fo ON fo.Codigo=mi.Fornece'
      'WHERE mi.NF_Inn_Its=:P0'
      '')
    Left = 256
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMPInItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mpinits.Codigo'
    end
    object QrMPInItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'mpinits.Controle'
    end
    object QrMPInItsConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'mpinits.Conta'
    end
    object QrMPInItsPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'mpinits.Pecas'
    end
    object QrMPInItsPecasNF: TFloatField
      FieldName = 'PecasNF'
      Origin = 'mpinits.PecasNF'
    end
    object QrMPInItsM2: TFloatField
      FieldName = 'M2'
      Origin = 'mpinits.M2'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
    object QrMPInItsPNF: TFloatField
      FieldName = 'PNF'
      Origin = 'mpinits.PNF'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInItsPLE: TFloatField
      FieldName = 'PLE'
      Origin = 'mpinits.PLE'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInItsPDA: TFloatField
      FieldName = 'PDA'
      Origin = 'mpinits.PDA'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInItsRecorte_PDA: TFloatField
      FieldName = 'Recorte_PDA'
      Origin = 'mpinits.Recorte_PDA'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInItsPTA: TFloatField
      FieldName = 'PTA'
      Origin = 'mpinits.PTA'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInItsRecorte_PTA: TFloatField
      FieldName = 'Recorte_PTA'
      Origin = 'mpinits.Recorte_PTA'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInItsRaspa_PTA: TFloatField
      FieldName = 'Raspa_PTA'
      Origin = 'mpinits.Raspa_PTA'
      DisplayFormat = '###,###,##0.000;-###,###,##0.000; '
    end
    object QrMPInItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'mpinits.Lk'
    end
    object QrMPInItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'mpinits.DataCad'
    end
    object QrMPInItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'mpinits.DataAlt'
    end
    object QrMPInItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'mpinits.UserCad'
    end
    object QrMPInItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'mpinits.UserAlt'
    end
    object QrMPInItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'mpinits.AlterWeb'
    end
    object QrMPInItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'mpinits.Ativo'
    end
    object QrMPInItsCaminhao: TIntegerField
      FieldName = 'Caminhao'
      Origin = 'mpinits.Caminhao'
    end
    object QrMPInItsCMPValor: TFloatField
      FieldName = 'CMPValor'
      Origin = 'mpinits.CMPValor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsCMPFrete: TFloatField
      FieldName = 'CMPFrete'
      Origin = 'mpinits.CMPFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsCPLValor: TFloatField
      FieldName = 'CPLValor'
      Origin = 'mpinits.CPLValor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsCPLFrete: TFloatField
      FieldName = 'CPLFrete'
      Origin = 'mpinits.CPLFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsAGIValor: TFloatField
      FieldName = 'AGIValor'
      Origin = 'mpinits.AGIValor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsAGIFrete: TFloatField
      FieldName = 'AGIFrete'
      Origin = 'mpinits.AGIFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsCMIValor: TFloatField
      FieldName = 'CMIValor'
      Origin = 'mpinits.CMIValor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsCMIFrete: TFloatField
      FieldName = 'CMIFrete'
      Origin = 'mpinits.CMIFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsTotalInfo: TFloatField
      FieldName = 'TotalInfo'
      Origin = 'mpinits.TotalInfo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsFreteInfo: TFloatField
      FieldName = 'FreteInfo'
      Origin = 'mpinits.FreteInfo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsCustoInfo: TFloatField
      FieldName = 'CustoInfo'
      Origin = 'mpinits.CustoInfo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsFinalInfo: TFloatField
      FieldName = 'FinalInfo'
      Origin = 'mpinits.FinalInfo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsDolar: TFloatField
      FieldName = 'Dolar'
      Origin = 'mpinits.Dolar'
    end
    object QrMPInItsEuro: TFloatField
      FieldName = 'Euro'
      Origin = 'mpinits.Euro'
    end
    object QrMPInItsIndexador: TFloatField
      FieldName = 'Indexador'
      Origin = 'mpinits.Indexador'
    end
    object QrMPInItsCMPDevol: TFloatField
      FieldName = 'CMPDevol'
      Origin = 'mpinits.CMPDevol'
    end
    object QrMPInItsCPLDevol: TFloatField
      FieldName = 'CPLDevol'
      Origin = 'mpinits.CPLDevol'
    end
    object QrMPInItsAGIDevol: TFloatField
      FieldName = 'AGIDevol'
      Origin = 'mpinits.AGIDevol'
    end
    object QrMPInItsCMIDevol: TFloatField
      FieldName = 'CMIDevol'
      Origin = 'mpinits.CMIDevol'
    end
    object QrMPInItsCMPUnida: TSmallintField
      FieldName = 'CMPUnida'
      Origin = 'mpinits.CMPUnida'
    end
    object QrMPInItsCMPPreco: TFloatField
      FieldName = 'CMPPreco'
      Origin = 'mpinits.CMPPreco'
    end
    object QrMPInItsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrMPInItsQUANTIDADE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'QUANTIDADE'
      Calculated = True
    end
    object QrMPInItsCMPMoeda: TSmallintField
      FieldName = 'CMPMoeda'
      Origin = 'mpinits.CMPMoeda'
    end
    object QrMPInItsCMPCotac: TFloatField
      FieldName = 'CMPCotac'
      Origin = 'mpinits.CMPCotac'
    end
    object QrMPInItsCMPReais: TFloatField
      FieldName = 'CMPReais'
      Origin = 'mpinits.CMPReais'
    end
    object QrMPInItsFornece: TIntegerField
      FieldName = 'Fornece'
      Origin = 'mpinits.Fornece'
    end
    object QrMPInItsPNF_Fat: TFloatField
      FieldName = 'PNF_Fat'
      Origin = 'mpinits.PNF_Fat'
    end
    object QrMPInItsNOMEPROCEDENCIA: TWideStringField
      FieldName = 'NOMEPROCEDENCIA'
      Size = 100
    end
    object QrMPInItsEmissao: TDateField
      FieldName = 'Emissao'
      Origin = 'mpinits.Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPInItsVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'mpinits.Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPInItsNF: TIntegerField
      FieldName = 'NF'
      Origin = 'mpinits.NF'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrMPInItsConheci: TIntegerField
      FieldName = 'Conheci'
      Origin = 'mpinits.Conheci'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrMPInItsEmisFrete: TDateField
      FieldName = 'EmisFrete'
      Origin = 'mpinits.EmisFrete'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPInItsVctoFrete: TDateField
      FieldName = 'VctoFrete'
      Origin = 'mpinits.VctoFrete'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPInItsVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 12
      Calculated = True
    end
    object QrMPInItsVCTOFRETE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VCTOFRETE_TXT'
      Size = 12
      Calculated = True
    end
    object QrMPInItsEMISFRETE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMISFRETE_TXT'
      Size = 12
      Calculated = True
    end
    object QrMPInItsPS_ValUni: TFloatField
      FieldName = 'PS_ValUni'
      Required = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrMPInItsPS_QtdTot: TFloatField
      FieldName = 'PS_QtdTot'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrMPInItsPS_ValTot: TFloatField
      FieldName = 'PS_ValTot'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrMPInItsNF_Inn_Fut: TSmallintField
      FieldName = 'NF_Inn_Fut'
    end
    object QrMPInItsNF_Inn_Its: TIntegerField
      FieldName = 'NF_Inn_Its'
    end
  end
  object DsMPInIts: TDataSource
    DataSet = QrMPInIts
    Left = 284
    Top = 56
  end
  object PMNFe: TPopupMenu
    Left = 668
    Top = 508
    object RecriatodaNFe1: TMenuItem
      Caption = '&Recria toda NFe'
      OnClick = RecriatodaNFe1Click
    end
  end
  object QrNFeMPIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeMPItsBeforeClose
    AfterScroll = QrNFeMPItsAfterScroll
    SQL.Strings = (
      'SELECT gg1.Nome NO_GG1, gti.Nome NO_TAM, gcc.Nome NO_COR, '
      'CASE '
      '   WHEN nms.GerBxaEstq=0 THEN "Pe'#231'as"'
      '   WHEN nms.GerBxaEstq=1 THEN "'#193'rea m'#178'" '
      '   WHEN nms.GerBxaEstq=2 THEN "Peso kg" '
      '   ELSE "???" END NO_GerBxaEstq, '
      'nms.* '
      'FROM nfempits nms'
      'LEFT JOIN gragrux ggx ON ggx.Controle=nms.GraGruX'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Codigo=ggx.GraTamI'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE nms.Codigo=:P0')
    Left = 200
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeMPItsNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrNFeMPItsNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrNFeMPItsNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrNFeMPItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeMPItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeMPItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFeMPItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrNFeMPItsPLE: TFloatField
      FieldName = 'PLE'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrNFeMPItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeMPItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeMPItsCMPValor: TFloatField
      FieldName = 'CMPValor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeMPItsGerBxaEstq: TIntegerField
      FieldName = 'GerBxaEstq'
    end
    object QrNFeMPItsNO_GerBxaEstq: TWideStringField
      FieldName = 'NO_GerBxaEstq'
      Required = True
      Size = 7
    end
  end
  object DsNFeMPIts: TDataSource
    DataSet = QrNFeMPIts
    Left = 228
    Top = 56
  end
  object PMSubDetail: TPopupMenu
    OnPopup = PMSubDetailPopup
    Left = 484
    Top = 500
    object AdicionaSubDetail1: TMenuItem
      Caption = '&Adiciona Item'
      OnClick = MenuItem1Click
    end
    object RetiraSubDetail1: TMenuItem
      Caption = '&Retira Item'
      OnClick = MenuItem2Click
    end
  end
  object QrSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(PLE) PLE,'
      'SUM(CMPValor) CMPValor, SUM(AreaM2) AreaM2,'
      'SUM(AreaP2) AreaP2, COUNT(Ativo) Ativos'
      'FROM nfempits'
      'WHERE Codigo=:P0'
      '')
    Left = 168
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPLE: TFloatField
      FieldName = 'PLE'
    end
    object QrSumCMPValor: TFloatField
      FieldName = 'CMPValor'
    end
    object QrSumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSumAtivos: TLargeintField
      FieldName = 'Ativos'
      Required = True
    end
  end
  object QrTot: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(PLE) PLE,  SUM(M2) M2,'
      'SUM(CMPValor) CMPValor'
      'FROM mpinits mi'
      'WHERE mi.NF_Inn_Its=:P0')
    Left = 196
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrTotPLE: TFloatField
      FieldName = 'PLE'
    end
    object QrTotM2: TFloatField
      FieldName = 'M2'
    end
    object QrTotCMPValor: TFloatField
      FieldName = 'CMPValor'
    end
  end
  object VuRegrFiscal: TdmkValUsu
    dmkEditCB = EdRegrFiscal
    Panel = PainelEdita
    QryCampo = 'RegrFiscal'
    UpdCampo = 'RegrFiscal'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 140
    Top = 56
  end
  object QrFisRegCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.ModeloNF,'
      'frc.Nome, imp.Nome NO_MODELO_NF, frc.Financeiro'
      'FROM fisregcad frc'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'LEFT JOIN fisregmvt mvt ON mvt.Codigo=frc.Codigo'
      'WHERE frc.Aplicacao=16'
      'AND mvt.TipoMov=0'
      'AND mvt.TipoCalc=0'
      'ORDER BY Nome')
    Left = 112
    Top = 312
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Required = True
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 140
    Top = 312
  end
  object QrFatPedNFs: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFatPedNFsBeforeClose
    AfterScroll = QrFatPedNFsAfterScroll
    SQL.Strings = (
      'SELECT nfe.Status, nfe.infProt_cStat, nfe.infCanc_cStat, '
      'ent.Filial, ent.Codigo CO_ENT_EMP, smna.*'
      'FROM stqmovnfsa smna '
      'LEFT JOIN entidades ent ON ent.Codigo=smna.Empresa'
      'LEFT JOIN nfecaba nfe ON nfe.IDCtrl=smna.IDCtrl'
      'WHERE smna.Tipo=:P0'
      'AND smna.OriCodi=:P1'
      'AND smna.Empresa=:P2'
      'ORDER BY ent.Filial')
    Left = 112
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFatPedNFsFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
      DisplayFormat = '000'
    end
    object QrFatPedNFsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'stqmovnfsa.IDCtrl'
      Required = True
    end
    object QrFatPedNFsTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'stqmovnfsa.Tipo'
    end
    object QrFatPedNFsOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovnfsa.OriCodi'
    end
    object QrFatPedNFsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovnfsa.Empresa'
    end
    object QrFatPedNFsNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
      Origin = 'stqmovnfsa.NumeroNF'
      DisplayFormat = '000000;-000000; '
    end
    object QrFatPedNFsIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'stqmovnfsa.IncSeqAuto'
    end
    object QrFatPedNFsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'stqmovnfsa.AlterWeb'
    end
    object QrFatPedNFsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqmovnfsa.Ativo'
    end
    object QrFatPedNFsSerieNFCod: TIntegerField
      FieldName = 'SerieNFCod'
      Origin = 'stqmovnfsa.SerieNFCod'
    end
    object QrFatPedNFsSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Origin = 'stqmovnfsa.SerieNFTxt'
      Size = 5
    end
    object QrFatPedNFsCO_ENT_EMP: TIntegerField
      FieldName = 'CO_ENT_EMP'
      Origin = 'entidades.Codigo'
    end
    object QrFatPedNFsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'stqmovnfsa.DataCad'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'stqmovnfsa.DataAlt'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDataAlt_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataAlt_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFsFreteVal: TFloatField
      FieldName = 'FreteVal'
      Origin = 'stqmovnfsa.FreteVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsSeguro: TFloatField
      FieldName = 'Seguro'
      Origin = 'stqmovnfsa.Seguro'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsOutros: TFloatField
      FieldName = 'Outros'
      Origin = 'stqmovnfsa.Outros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsPlacaUF: TWideStringField
      FieldName = 'PlacaUF'
      Origin = 'stqmovnfsa.PlacaUF'
      Size = 2
    end
    object QrFatPedNFsPlacaNr: TWideStringField
      FieldName = 'PlacaNr'
      Origin = 'stqmovnfsa.PlacaNr'
      Size = 8
    end
    object QrFatPedNFsEspecie: TWideStringField
      FieldName = 'Especie'
      Origin = 'stqmovnfsa.Especie'
      Size = 30
    end
    object QrFatPedNFsMarca: TWideStringField
      FieldName = 'Marca'
      Origin = 'stqmovnfsa.Marca'
      Size = 30
    end
    object QrFatPedNFsNumero: TWideStringField
      FieldName = 'Numero'
      Origin = 'stqmovnfsa.Numero'
      Size = 30
    end
    object QrFatPedNFskgBruto: TFloatField
      FieldName = 'kgBruto'
      Origin = 'stqmovnfsa.kgBruto'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFskgLiqui: TFloatField
      FieldName = 'kgLiqui'
      Origin = 'stqmovnfsa.kgLiqui'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFsQuantidade: TWideStringField
      FieldName = 'Quantidade'
      Origin = 'stqmovnfsa.Quantidade'
      Size = 30
    end
    object QrFatPedNFsObservacao: TWideStringField
      FieldName = 'Observacao'
      Origin = 'stqmovnfsa.Observacao'
      Size = 255
    end
    object QrFatPedNFsCFOP1: TWideStringField
      FieldName = 'CFOP1'
      Origin = 'stqmovnfsa.CFOP1'
      Size = 6
    end
    object QrFatPedNFsDtEmissNF: TDateField
      FieldName = 'DtEmissNF'
      Origin = 'stqmovnfsa.DtEmissNF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDtEntraSai: TDateField
      FieldName = 'DtEntraSai'
      Origin = 'stqmovnfsa.DtEntraSai'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'nfecaba.Status'
    end
    object QrFatPedNFsinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
      Origin = 'nfecaba.infProt_cStat'
    end
    object QrFatPedNFsinfAdic_infCpl: TWideMemoField
      FieldName = 'infAdic_infCpl'
      Origin = 'stqmovnfsa.infAdic_infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFatPedNFsinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = 'nfecaba.infCanc_cStat'
    end
    object QrFatPedNFsDTEMISSNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTEMISSNF_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFsDTENTRASAI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTENTRASAI_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFsRNTC: TWideStringField
      FieldName = 'RNTC'
      Origin = 'stqmovnfsa.RNTC'
    end
    object QrFatPedNFsUFembarq: TWideStringField
      FieldName = 'UFembarq'
      Origin = 'stqmovnfsa.UFembarq'
      Size = 2
    end
    object QrFatPedNFsxLocEmbarq: TWideStringField
      FieldName = 'xLocEmbarq'
      Origin = 'stqmovnfsa.xLocEmbarq'
      Size = 60
    end
    object QrFatPedNFsHrEntraSai: TTimeField
      FieldName = 'HrEntraSai'
    end
    object QrFatPedNFside_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrFatPedNFside_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrFatPedNFsemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrFatPedNFsdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrFatPedNFsvagao: TWideStringField
      FieldName = 'vagao'
    end
    object QrFatPedNFsbalsa: TWideStringField
      FieldName = 'balsa'
    end
    object QrFatPedNFsCompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrFatPedNFsCompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrFatPedNFsCompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
  end
  object DsFatPedNFs: TDataSource
    DataSet = QrFatPedNFs
    Left = 140
    Top = 340
  end
  object QrNFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeCabACalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND ide_nNF=:P3')
    Left = 112
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeCabAversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeCabAStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrNFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrNFeCabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrNFeCabAcStat: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'cStat'
      Calculated = True
    end
    object QrNFeCabAxMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAcStat_xMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'cStat_xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      DisplayFormat = '000'
    end
    object QrNFeCabASTATUS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_TXT'
      Size = 255
      Calculated = True
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 140
    Top = 368
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 168
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 196
    Top = 368
  end
  object VuPediPrzCab: TdmkValUsu
    dmkEditCB = EdCondicaoPG
    Panel = PainelEdita
    QryCampo = 'CondicaoPG'
    UpdCampo = 'CondicaoPG'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 168
    Top = 56
  end
  object QrTransportas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'#13
      'ufs.Nome NOMEUF'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Fornece2="V"'
      'ORDER BY NOMEENT')
    Left = 112
    Top = 396
    object QrTransportasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportasNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrTransportasCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrTransportasNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
  end
  object DsTransportas: TDataSource
    DataSet = QrTransportas
    Left = 140
    Top = 396
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 592
    Top = 504
    object Encerraentradadecouro1: TMenuItem
      Caption = '&Encerra entrada de couro'
      OnClick = Encerraentradadecouro1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Desfazencerramento1: TMenuItem
      Caption = '&Desfaz encerramento'
      OnClick = Desfazencerramento1Click
    end
  end
  object QrCartEmiss: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 168
    Top = 396
    object QrCartEmissCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCartEmissNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsCartEmiss: TDataSource
    DataSet = QrCartEmiss
    Left = 196
    Top = 396
  end
end
