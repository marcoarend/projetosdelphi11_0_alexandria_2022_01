object FmPQRet: TFmPQRet
  Left = 368
  Top = 194
  Caption = 'QUI-RETOR-001 :: Retorno de Insumos de Terceiros'
  ClientHeight = 441
  ClientWidth = 816
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 816
    Height = 345
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 816
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 64
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPQRet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 148
        Top = 20
        Width = 633
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPQRet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsPQRet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 48
      Width = 816
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 816
        Height = 52
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label5: TLabel
          Left = 4
          Top = 4
          Width = 59
          Height = 13
          Caption = 'Regra fiscal:'
          FocusControl = DBEdit3
        end
        object DBEdit2: TDBEdit
          Left = 60
          Top = 20
          Width = 721
          Height = 21
          DataField = 'NO_FisRegCad'
          DataSource = DsPQRet
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          DataField = 'CU_FisRegCad'
          DataSource = DsPQRet
          TabOrder = 1
        end
      end
    end
    object DBGPQ: TdmkDBGrid
      Left = 0
      Top = 108
      Width = 816
      Height = 104
      Align = alTop
      Color = clWindow
      DataSource = DsPQRetIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 281
      Width = 816
      Height = 64
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 293
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtRetorno: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Retorno'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtRetornoClick
        end
        object BtInsumos: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Insumos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtInsumosClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 816
    Height = 345
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdi2: TPanel
      Left = 0
      Top = 52
      Width = 816
      Height = 156
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Painel2: TPanel
        Left = 0
        Top = 48
        Width = 816
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object SpeedButton5: TSpeedButton
          Left = 760
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object dmkLabel4: TdmkLabel
          Left = 4
          Top = 4
          Width = 422
          Height = 13
          Caption = 
            'Regra fiscal (precisa ser aplica'#231#227'o igual a "Retorno de Insumos"' +
            ' e c'#225'lculo igual a "Nulo":'
          UpdType = utYes
          SQLType = stNil
        end
        object EdFisRegCad: TdmkEditCB
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBFisRegCad
          IgnoraDBLookupComboBox = False
        end
        object CBFisRegCad: TdmkDBLookupComboBox
          Left = 60
          Top = 20
          Width = 700
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsFisRegCad
          TabOrder = 1
          dmkEditCB = EdFisRegCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 816
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label12: TLabel
          Left = 4
          Top = 4
          Width = 67
          Height = 13
          Caption = 'Data emiss'#227'o:'
        end
        object Label18: TLabel
          Left = 112
          Top = 4
          Width = 71
          Height = 13
          Caption = 'Cliente Interno:'
        end
        object SpeedButton6: TSpeedButton
          Left = 760
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton6Click
        end
        object TPData: TdmkEditDateTimePicker
          Left = 4
          Top = 20
          Width = 105
          Height = 21
          Date = 39789.688972615740000000
          Time = 39789.688972615740000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'Data'
          UpdCampo = 'Data'
          UpdType = utYes
        end
        object EdCliente: TdmkEditCB
          Left = 112
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cliente'
          UpdCampo = 'Cliente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 168
          Top = 20
          Width = 592
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMECLIENTEI'
          ListSource = DsClientesI
          TabOrder = 2
          dmkEditCB = EdCliente
          QryCampo = 'Cliente'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 816
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 632
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 281
      Width = 816
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel9: TPanel
        Left = 2
        Top = 15
        Width = 812
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 668
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 816
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 812
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 816
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 768
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 552
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Retorno de Insumos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Retorno de Insumos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Retorno de Insumos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsPQRet: TDataSource
    DataSet = QrPQRet
    Left = 40
    Top = 12
  end
  object QrPQRet: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPQRetBeforeOpen
    AfterOpen = QrPQRetAfterOpen
    BeforeClose = QrPQRetBeforeClose
    AfterScroll = QrPQRetAfterScroll
    SQL.Strings = (
      'SELECT pqr.*, frc.Nome NO_FisRegCad, frc.CodUsu CU_FisRegCad'
      'FROM pqr pqr'
      'LEFT JOIN fisregcad frc ON frc.Codigo=pqr.FisRegCad')
    Left = 12
    Top = 12
    object QrPQRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQRetCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPQRetNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPQRetCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPQRetData: TDateField
      FieldName = 'Data'
    end
    object QrPQRetFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrPQRetLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQRetDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQRetDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQRetUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQRetUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQRetAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQRetAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQRetNO_FisRegCad: TWideStringField
      FieldName = 'NO_FisRegCad'
      Size = 50
    end
    object QrPQRetCU_FisRegCad: TIntegerField
      FieldName = 'CU_FisRegCad'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInsumos
    CanUpd01 = BtRetorno
    Left = 68
    Top = 12
  end
  object PMRetorno: TPopupMenu
    Left = 384
    Top = 332
    object Incluinovoretorno1: TMenuItem
      Caption = '&Inclui novo retorno'
      OnClick = Incluinovoretorno1Click
    end
    object Alteraretornoatual1: TMenuItem
      Caption = '&Altera retorno atual'
      OnClick = Alteraretornoatual1Click
    end
    object Excluiretornoatual1: TMenuItem
      Caption = '&Exclui retorno atual'
      Enabled = False
    end
  end
  object PMInsumos: TPopupMenu
    OnPopup = PMInsumosPopup
    Left = 472
    Top = 332
    object Adicionainsumos1: TMenuItem
      Caption = '&Adiciona insumo(s)'
      OnClick = Adicionainsumos1Click
    end
    object Retirainsumosselecionados1: TMenuItem
      Caption = '&Retira insumo(s) selecionado(s)'
      OnClick = Retirainsumosselecionados1Click
    end
  end
  object QrFisRegCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.Nome, frm.TipoMov,'
      'ELT(frm.TipoMov+1,"Entrada","Saida","?") NO_TipoMov, '
      'frm.TipoCalc, ELT(frm.TipoCalc+1,"Nulo","Adiciona",'
      '"Subtrai","?") NO_TipoCalc '
      'FROM fisregmvt frm'
      'LEFT JOIN fisregcad frc ON frc.Codigo=frm.Codigo'
      'WHERE frc.Aplicacao=15'
      'AND frm.TipoCalc = 0'
      '/*'
      'AND frm.Empresa=:P0'
      'AND frm.StqCenCad=:P1'
      '*/')
    Left = 652
    Top = 16
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
      Required = True
    end
    object QrFisRegCadNO_TipoMov: TWideStringField
      FieldName = 'NO_TipoMov'
      Size = 7
    end
    object QrFisRegCadTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
      Required = True
    end
    object QrFisRegCadNO_TipoCalc: TWideStringField
      FieldName = 'NO_TipoCalc'
      Size = 8
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 680
    Top = 16
  end
  object VuFisRegCad: TdmkValUsu
    dmkEditCB = EdFisRegCad
    Panel = PainelEdita
    QryCampo = 'FisRegCad'
    UpdCampo = 'FisRegCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 96
    Top = 12
  end
  object QrClientesI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial'
      'ELSE ci.Nome END NOMECLIENTEI '
      'FROM entidades ci'
      'WHERE ci.Cliente2="V"'
      'ORDER BY NOMECLIENTEI')
    Left = 708
    Top = 16
    object QrClientesINOMECLIENTEI: TWideStringField
      FieldName = 'NOMECLIENTEI'
      Size = 100
    end
    object QrClientesICodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
  end
  object DsClientesI: TDataSource
    DataSet = QrClientesI
    Left = 736
    Top = 16
  end
  object QrPQRetIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM pqretits'
      'WHERE Codigo=:P0')
    Left = 124
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQRetItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQRetItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPQRetItsInnOriCodi: TIntegerField
      FieldName = 'InnOriCodi'
    end
    object QrPQRetItsInnOriCtrl: TIntegerField
      FieldName = 'InnOriCtrl'
    end
    object QrPQRetItsInnTipo: TIntegerField
      FieldName = 'InnTipo'
    end
    object QrPQRetItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQRetItsPreco: TFloatField
      FieldName = 'Preco'
    end
    object QrPQRetItsPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPQRetItsValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object DsPQRetIts: TDataSource
    DataSet = QrPQRetIts
    Left = 152
    Top = 12
  end
  object QrPQ010: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pq010'
      '')
    Left = 652
    Top = 44
    object QrPQ010Data: TDateField
      FieldName = 'Data'
    end
    object QrPQ010OrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQ010OrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQ010Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPQ010Insumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQ010Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPQ010Peso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPQ010Valor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQ010Ativo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
end
