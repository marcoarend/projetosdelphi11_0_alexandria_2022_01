unit PQRetIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, Grids, DBGrids, dmkDBGrid,
  Variants, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmPQRetIts = class(TForm)
    Panel1: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    DsPQ010: TDataSource;
    QrDupl: TmySQLQuery;
    QrDuplControle: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetaTodosItens(Ativo: Byte);
    procedure ReopenPQ010(OrigemCodi, OrigemCtrl: Integer);
  public
    { Public declarations }
  end;

  var
  FmPQRetIts: TFmPQRetIts;

implementation

{$R *.DFM}

uses UnMyObjects, Module, PQRet, ModuleGeral, dmkGeral, UMySQLModule;

procedure TFmPQRetIts.BtNenhumClick(Sender: TObject);
begin
  SetaTodosItens(0);
end;

procedure TFmPQRetIts.BtOKClick(Sender: TObject);
var
  SQLType: TSQLType;
  Controle, CodAtual, Codigo, InnOriCodi, InnOriCtrl, InnTipo, Insumo: Integer;
  Preco, Peso, Valor: Double;
begin
  Controle := 0;
  FmPQRet.QrPQ010.First;
  while not FmPQRet.QrPQ010.Eof do
  begin
    if FmPQRet.QrPQ010Ativo.Value = 1 then
    begin
{
SELECT Controle
FROM pqretits
WHERE Codigo=:P0
AND InnOriCodi=:P1
AND InnOriCtrl=:P2
AND InnTipo=:P3
}
      QrDupl.Close;
      QrDupl.Params[00].AsInteger := FmPQRet.QrPQRetCodigo.Value;
      QrDupl.Params[01].AsInteger := FmPQRet.QrPQ010OrigemCodi.Value;
      QrDupl.Params[02].AsInteger := FmPQRet.QrPQ010OrigemCtrl.Value;
      QrDupl.Params[03].AsInteger := FmPQRet.QrPQ010Tipo.Value;
      UnDmkDAC_PF.AbreQuery(QrDupl, Dmod.MyDB);
      if QrDuplControle.Value > 0 then
      begin
        SQLType  := stUpd;
        CodAtual := QrDuplControle.Value;
      end else begin
        SQLType  := stIns;
        CodAtual := 0;
      end;
      Codigo     := FmPQRet.QrPQRetCodigo.Value;
      InnOriCodi := FmPQRet.QrPQ010OrigemCodi.Value;
      InnOriCtrl := FmPQRet.QrPQ010OrigemCtrl.Value;
      InnTipo    := FmPQRet.QrPQ010Tipo.Value;
      Insumo     := FmPQRet.QrPQ010Insumo.Value;
      Peso       := FmPQRet.QrPQ010Peso.Value;
      Valor      := FmPQRet.QrPQ010Valor.Value;
      if Valor = 0 then Preco := 0 else Preco := Valor / Peso;
      Controle := UMyMod.BuscaEmLivreY_Def('pqretits', 'Controle', SQLType, CodAtual);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pqretits', False, [
      'Codigo', 'InnOriCodi', 'InnOriCtrl',
      'InnTipo', 'Insumo', 'Preco', 'Peso',
      'Valor'], ['Controle'], [
      Codigo, InnOriCodi, InnOriCtrl,
      InnTipo, Insumo, Preco, Peso,
      Valor], [Controle], True) then
      begin
        //
      end;
    end;
    FmPQRet.QrPQ010.Next;
  end;
  FmPQRet.ReopenPQRetIts(Controle);
  Close;
end;

procedure TFmPQRetIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQRetIts.BtTudoClick(Sender: TObject);
begin
  SetaTodosItens(1);
end;

procedure TFmPQRetIts.dmkDBGrid1CellClick(Column: TColumn);
var
  Ativo, OrigemCodi, OrigemCtrl: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    OrigemCodi := FmPQRet.QrPQ010OrigemCodi.Value;
    OrigemCtrl := FmPQRet.QrPQ010OrigemCtrl.Value;
    if FmPQRet.QrPQ010Ativo.Value = 1 then Ativo := 0 else Ativo := 1;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE pq010 SET Ativo=:P0');
    DModG.QrUpdPID1.SQL.Add('WHERE OrigemCodi=:P1');
    DModG.QrUpdPID1.SQL.Add('AND OrigemCtrl=:P2');
    //
    DModG.QrUpdPID1.Params[00].AsInteger := Ativo;
    DModG.QrUpdPID1.Params[01].AsFloat   := OrigemCodi;
    DModG.QrUpdPID1.Params[02].AsFloat   := OrigemCtrl;
    DModG.QrUpdPID1.ExecSQL;
    //
    ReopenPQ010(OrigemCodi, OrigemCtrl);
  end;
end;

procedure TFmPQRetIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQRetIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DsPQ010.DataSet := FmPQRet.QrPQ010;
{
  QrPQ010.Close;
  QrPQ010.Params[00].AsInteger := FmPQRet.QrPQRetCliente.Value;
  QrPQ010.Params[01].AsInteger := FmPQRet.QrPQRetCliente.Value;
  QrPQ010.Params[02].AsInteger := FmPQRet.QrPQRetCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPQ010, Dmod.MyDB);
}
end;

procedure TFmPQRetIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQRetIts.ReopenPQ010(OrigemCodi, OrigemCtrl: Integer);
begin
  FmPQRet.QrPQ010.Close;
  UnDmkDAC_PF.AbreQueryApenas(FmPQRet.QrPQ010);
  //
  FmPQRet.QrPQ010.Locate('OrigemCodi;OrigemCtrl',
    VarArrayOf([OrigemCodi,OrigemCtrl]), []);
end;

procedure TFmPQRetIts.SetaTodosItens(Ativo: Byte);
var
  OrigemCodi, OrigemCtrl: Integer;
begin
  OrigemCodi := FmPQRet.QrPQ010OrigemCodi.Value;
  OrigemCtrl := FmPQRet.QrPQ010OrigemCtrl.Value;
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE pq010');
  DModG.QrUpdPID1.SQL.Add('SET Ativo=' + IntToStr(Ativo));
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenPQ010(OrigemCodi, OrigemCtrl);
end;

end.
