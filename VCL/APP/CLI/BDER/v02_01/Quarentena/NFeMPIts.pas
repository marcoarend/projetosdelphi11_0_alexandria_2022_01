unit NFeMPIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, Mask, DBCtrls, dmkEdit, DB,
  mySQLDbTables, dmkRadioGroup, dmkGeral, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmNFeMPIts = class(TForm)
    Panel1: TPanel;
    Panel5: TPanel;
    Label11: TLabel;
    SpeedButton1: TSpeedButton;
    EdGraGruX: TdmkEdit;
    PainelGGX: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox1: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdPecas: TdmkEdit;
    EdPLE: TdmkEdit;
    EdAreaM2: TdmkEdit;
    EdAreaP2: TdmkEdit;
    EdCMPValor: TdmkEdit;
    DsGraGruX: TDataSource;
    QrGraGruX: TmySQLQuery;
    QrGraGruXNO_GG1: TWideStringField;
    QrGraGruXNO_COR: TWideStringField;
    QrGraGruXNO_TAM: TWideStringField;
    QrGraGruXGraGruX: TIntegerField;
    RGGerBxaEstq: TdmkRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaPainelGGX();
  public
    { Public declarations }
  end;

  var
  FmNFeMPIts: TFmNFeMPIts;

implementation

uses UnMyObjects, GraGruPesq1, MyDBCheck, NFeMPInn, UMySQLModule, Module;

{$R *.DFM}

procedure TFmNFeMPIts.BtOKClick(Sender: TObject);
var
  Codigo, GraGruX, Controle, GerBxaEstq: Integer;
  Pecas, PLE, AreaM2, AreaP2, CMPValor: Double;
begin
  GerBxaEstq := RGGerBxaEstq.ItemIndex;
  if MyObjects.FIC(GerBxaEstq < 1, RGGerBxaEstq, 'Defina uma grandeza!') then Exit;
  GraGruX    := EdGraGruX.ValueVariant;
  Pecas      := EdPecas.ValueVariant;
  PLE        := EdPLE.ValueVariant;
  AreaM2     := EdAreaM2.ValueVariant;
  AreaP2     := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  CMPValor   := EdCMPValor.ValueVariant;
  Codigo     := FmNFeMPInn.QrNFeMPInnCodigo.Value;
  Controle   := UMyMod.BuscaEmLivreY_Def('nfempits', 'Controle', ImgTipo.SQLType,
    FmNFeMPInn.QrNFeMPItsControle.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfempits', False, [
    'Codigo', 'GraGruX', 'Pecas',
    'PLE', 'AreaM2', 'AreaP2',
    'CMPValor', 'GerBxaEstq'
  ], ['Controle'], [
    Codigo, GraGruX, Pecas,
    PLE, AreaM2, AreaP2,
    CMPValor, GerBxaEstq
  ], [Controle], True) then
  begin
    FmNFeMPInn.AtualizaTotais(Codigo);
    FmNFeMPInn.LocCod(Codigo, Codigo);
    Close;
  end;
end;

procedure TFmNFeMPIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeMPIts.EdGraGruXChange(Sender: TObject);
begin
  VerificaPainelGGX();
end;

procedure TFmNFeMPIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeMPIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
end;

procedure TFmNFeMPIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeMPIts.SpeedButton1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruPesq1, FmGraGruPesq1, afmoNegarComAviso) then
  begin
    FmGraGruPesq1.QrGraGru1.Close;
    FmGraGruPesq1.QrGraGru1.SQL.Clear;
    FmGraGruPesq1.QrGraGru1.SQL.Add('SELECT gg1.CodUsu, gg1.Nivel1, ');
    FmGraGruPesq1.QrGraGru1.SQL.Add('gg1.GraTamCad, gg1.Nome');
    FmGraGruPesq1.QrGraGru1.SQL.Add('FROM gragru1 gg1');
    FmGraGruPesq1.QrGraGru1.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    FmGraGruPesq1.QrGraGru1.SQL.Add('WHERE pgt.TipPrd=2');
    FmGraGruPesq1.QrGraGru1.SQL.Add('ORDER BY gg1.Nome');
    UnDmkDAC_PF.AbreQuery(FmGraGruPesq1.QrGraGru1, Dmod.MyDB);
    FmGraGruPesq1.ShowModal;
    EdGraGruX.Text := FmGraGruPesq1.FGraGruX;
    FmGraGruPesq1.Destroy;
  end;
end;

procedure TFmNFeMPIts.VerificaPainelGGX;
var
  GraGruX: Integer;
begin
  GraGruX := Geral.IMV(EdGraGruX.Text);
  PainelGGX.Visible := QrGraGruX.Locate('GraGruX', GraGruX, []);
end;

end.
