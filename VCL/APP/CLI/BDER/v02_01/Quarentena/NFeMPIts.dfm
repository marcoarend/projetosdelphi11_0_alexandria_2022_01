object FmNFeMPIts: TFmNFeMPIts
  Left = 339
  Top = 185
  Caption = 'NFe-MPINN-002 :: Item de Entrada de Couros'
  ClientHeight = 330
  ClientWidth = 583
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 583
    Height = 174
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 220
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 583
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 581
      object Label11: TLabel
        Left = 8
        Top = 8
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
      end
      object SpeedButton1: TSpeedButton
        Left = 88
        Top = 24
        Width = 21
        Height = 21
        Caption = '?'
        OnClick = SpeedButton1Click
      end
      object EdGraGruX: TdmkEdit
        Left = 8
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
      end
      object PainelGGX: TPanel
        Left = 112
        Top = 4
        Width = 468
        Height = 47
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object Label12: TLabel
          Left = 4
          Top = 4
          Width = 76
          Height = 13
          Caption = 'Nome do grupo:'
        end
        object Label13: TLabel
          Left = 248
          Top = 4
          Width = 19
          Height = 13
          Caption = 'Cor:'
          FocusControl = DBEdit2
        end
        object Label14: TLabel
          Left = 392
          Top = 4
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
          FocusControl = DBEdit3
        end
        object DBEdit1: TDBEdit
          Left = 4
          Top = 20
          Width = 240
          Height = 21
          TabStop = False
          DataField = 'NO_GG1'
          DataSource = DsGraGruX
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 248
          Top = 20
          Width = 140
          Height = 21
          TabStop = False
          DataField = 'NO_COR'
          DataSource = DsGraGruX
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 392
          Top = 20
          Width = 69
          Height = 21
          TabStop = False
          DataField = 'NO_TAM'
          DataSource = DsGraGruX
          TabOrder = 2
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 99
      Width = 583
      Height = 65
      Align = alTop
      Caption = 
        ' Quantidades: (informe somente para corrigir! altera'#231#245'es nos ite' +
        'ns alteram estes dados! '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      ExplicitLeft = 1
      ExplicitTop = 100
      ExplicitWidth = 581
      object Label16: TLabel
        Left = 8
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label17: TLabel
        Left = 72
        Top = 16
        Width = 23
        Height = 13
        Caption = 'PLE:'
      end
      object Label18: TLabel
        Left = 148
        Top = 16
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object Label19: TLabel
        Left = 224
        Top = 16
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
      end
      object Label20: TLabel
        Left = 300
        Top = 16
        Width = 46
        Height = 13
        Caption = 'Valor MP:'
      end
      object EdPecas: TdmkEdit
        Left = 8
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 1
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPLE: TdmkEdit
        Left = 72
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PLE'
        UpdCampo = 'PLE'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEdit
        Left = 148
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaP2: TdmkEdit
        Left = 224
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCMPValor: TdmkEdit
        Left = 300
        Top = 32
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'CMPValor'
        UpdCampo = 'CMPValor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object RGGerBxaEstq: TdmkRadioGroup
      Left = 0
      Top = 56
      Width = 583
      Height = 43
      Align = alTop
      Caption = ' Grandeza: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        '? ? ? '
        'Pe'#231'as'
        #193'rea (m'#178')'
        'Peso (kg)')
      TabOrder = 1
      UpdType = utYes
      OldValor = 0
      ExplicitLeft = 1
      ExplicitTop = 57
      ExplicitWidth = 581
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 583
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = -52
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 535
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 487
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 327
        Height = 32
        Caption = 'Item de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 327
        Height = 32
        Caption = 'Item de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 327
        Height = 32
        Caption = 'Item de Entrada de Couros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 222
    Width = 583
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = -52
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 579
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 266
    Width = 583
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -52
    ExplicitTop = 236
    ExplicitWidth = 635
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 579
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 435
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 36
    Top = 12
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX, gg1.Nome NO_GG1, '
      'gcc.Nome NO_COR,  gti.Nome NO_TAM'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pgt.TipPrd=2')
    Left = 8
    Top = 12
    object QrGraGruXNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrGraGruXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraGruXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
end
