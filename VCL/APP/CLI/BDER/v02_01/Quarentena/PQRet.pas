unit PQRet;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  DmkDAC_PF, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, Grids, DBGrids, Menus, dmkDBGrid, ComCtrls,
  dmkEditDateTimePicker, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmPQRet = class(TForm)
    PainelDados: TPanel;
    DsPQRet: TDataSource;
    QrPQRet: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    PainelEdi2: TPanel;
    Painel2: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    PMRetorno: TPopupMenu;
    PMInsumos: TPopupMenu;
    DBGPQ: TdmkDBGrid;
    Incluinovoretorno1: TMenuItem;
    Alteraretornoatual1: TMenuItem;
    Excluiretornoatual1: TMenuItem;
    QrFisRegCad: TmySQLQuery;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadTipoMov: TSmallintField;
    QrFisRegCadNO_TipoMov: TWideStringField;
    QrFisRegCadTipoCalc: TSmallintField;
    QrFisRegCadNO_TipoCalc: TWideStringField;
    DsFisRegCad: TDataSource;
    VuFisRegCad: TdmkValUsu;
    EdFisRegCad: TdmkEditCB;
    CBFisRegCad: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    dmkLabel4: TdmkLabel;
    QrPQRetCodigo: TIntegerField;
    QrPQRetCodUsu: TIntegerField;
    QrPQRetNome: TWideStringField;
    QrPQRetCliente: TIntegerField;
    QrPQRetData: TDateField;
    QrPQRetFisRegCad: TIntegerField;
    QrPQRetLk: TIntegerField;
    QrPQRetDataCad: TDateField;
    QrPQRetDataAlt: TDateField;
    QrPQRetUserCad: TIntegerField;
    QrPQRetUserAlt: TIntegerField;
    QrPQRetAlterWeb: TSmallintField;
    QrPQRetAtivo: TSmallintField;
    QrPQRetNO_FisRegCad: TWideStringField;
    QrPQRetCU_FisRegCad: TIntegerField;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Panel7: TPanel;
    TPData: TdmkEditDateTimePicker;
    Label12: TLabel;
    QrClientesI: TmySQLQuery;
    QrClientesINOMECLIENTEI: TWideStringField;
    QrClientesICodigo: TIntegerField;
    DsClientesI: TDataSource;
    Label18: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    SpeedButton6: TSpeedButton;
    Adicionainsumos1: TMenuItem;
    QrPQRetIts: TmySQLQuery;
    DsPQRetIts: TDataSource;
    QrPQ010: TmySQLQuery;
    QrPQ010Data: TDateField;
    QrPQ010OrigemCodi: TIntegerField;
    QrPQ010OrigemCtrl: TIntegerField;
    QrPQ010Insumo: TIntegerField;
    QrPQ010Nome: TWideStringField;
    QrPQ010Peso: TFloatField;
    QrPQ010Valor: TFloatField;
    QrPQ010Ativo: TSmallintField;
    QrPQ010Tipo: TIntegerField;
    Retirainsumosselecionados1: TMenuItem;
    QrPQRetItsCodigo: TIntegerField;
    QrPQRetItsControle: TIntegerField;
    QrPQRetItsInnOriCodi: TIntegerField;
    QrPQRetItsInnOriCtrl: TIntegerField;
    QrPQRetItsInnTipo: TIntegerField;
    QrPQRetItsInsumo: TIntegerField;
    QrPQRetItsPreco: TFloatField;
    QrPQRetItsPeso: TFloatField;
    QrPQRetItsValor: TFloatField;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtRetorno: TBitBtn;
    BtInsumos: TBitBtn;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPQRetAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPQRetBeforeOpen(DataSet: TDataSet);
    procedure BtRetornoClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrPQRetBeforeClose(DataSet: TDataSet);
    procedure QrPQRetAfterScroll(DataSet: TDataSet);
    procedure BtInsumosClick(Sender: TObject);
    procedure Incluinovoretorno1Click(Sender: TObject);
    procedure Alteraretornoatual1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure Adicionainsumos1Click(Sender: TObject);
    procedure Retirainsumosselecionados1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMInsumosPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FPQ010: String;
    procedure ReopenPQRetIts(Controle: Integer);
    procedure ReopenPQ010();
  end;

var
  FmPQRet: TFmPQRet;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, Principal, Entidade2, PQRetIts, ModuleGeral, UCreate,
  ModProd;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPQRet.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPQRet.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPQRetCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPQRet.DefParams;
begin
  VAR_GOTOTABELA := 'pqret';
  VAR_GOTOMYSQLTABLE := QrPQRet;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pqr.*, frc.Nome NO_FisRegCad, frc.CodUsu CU_FisRegCad');
  VAR_SQLx.Add('FROM pqret pqr');
  VAR_SQLx.Add('LEFT JOIN fisregcad frc ON frc.Codigo=pqr.FisRegCad');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE pqr.Codigo > -1000');
  //
  VAR_SQL1.Add('AND pqr.Codigo=:P0');
  //
  VAR_SQL2.Add('AND pqr.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND pqr.Nome Like :P0');
  //
end;

procedure TFmPQRet.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'pqret', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmPQRet.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPQRet.PMInsumosPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPQRet.State <> dsInactive) and (QrPQRet.RecordCount > 0);
  //
  Adicionainsumos1.Enabled           := Enab;
  Retirainsumosselecionados1.Enabled := Enab;
end;

procedure TFmPQRet.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPQRet.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPQRet.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPQRet.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPQRet.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPQRet.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPQRet.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPQRet.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFisRegCad;
  if VAR_CADASTRO <> 0 then
  begin
    QrFisRegCad.Close;
    UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
    if QrFisRegCad.Locate('Codigo', QrFisRegCadCodigo.Value, []) then
    begin
      EdFisRegCad.ValueVariant := QrFisRegCadCodUsu.Value;
      CBFisRegCad.KeyValue     := QrFisRegCadCodUsu.Value;
    end;
  end;
end;

procedure TFmPQRet.SpeedButton6Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    if VAR_ENTIDADE <> 0 then
    begin
      QrClientesI.Close;
      UnDmkDAC_PF.AbreQuery(QrClientesI, Dmod.MyDB);
      if QrClientesI.Locate('Codigo', VAR_ENTIDADE, []) then
      begin
        EdCliente.ValueVariant := VAR_ENTIDADE;
        CBCliente.KeyValue     := VAR_ENTIDADE;
      end;
    end;
  end;
end;

procedure TFmPQRet.Adicionainsumos1Click(Sender: TObject);
var
  DB: String;
begin
  Screen.Cursor := crHourGlass;
  try
    DB := DMod.MyDB.DatabaseName;
    FPQ010 := UCriar.RecriaTempTable('pq010', DmodG.QrUpdPID1, False);
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FPQ010);
    //
    DmodG.QrUpdPID1.SQL.Add('SELECT pqx.DataX, pqx.OrigemCodi, pqx.OrigemCtrl,');
    DmodG.QrUpdPID1.SQL.Add('pqx.Tipo, pqx.Insumo, pq.Nome, pqx.Peso, ');
    DmodG.QrUpdPID1.SQL.Add('pqx.Valor, 0 Ativo');
    DmodG.QrUpdPID1.SQL.Add('FROM ' + DB + '.pqx pqx');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + DB + '.pq pq ON pq.Codigo=pqx.Insumo');
    DmodG.QrUpdPID1.SQL.Add('WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0010));
    DmodG.QrUpdPID1.SQL.Add('AND Retorno=0');
    DmodG.QrUpdPID1.SQL.Add('AND pqx.CliOrig=:P0');
    DmodG.QrUpdPID1.SQL.Add('AND pqx.CliDest=:P1');
    DmodG.QrUpdPID1.Params[00].AsInteger := QrPQRetCliente.Value;
    DmodG.QrUpdPID1.Params[01].AsInteger := QrPQRetCliente.Value;
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenPQ010();
  finally
    Screen.Cursor := crDefault;
  end;
  if QrPQ010.RecordCount = 0 then
  begin
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrPQ010, '', nil, True, True);
    Application.MessageBox('N�o h� insumos a serem retornados!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if DBCheck.CriaFm(TFmPQRetIts, FmPQRetIts, afmoNegarComAviso) then
  begin
    FmPQRetIts.ShowModal;
    FmPQRetIts.Destroy;
  end;
end;

procedure TFmPQRet.Alteraretornoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPQRet, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'pqret');
end;

procedure TFmPQRet.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPQRetCodigo.Value;
  Close;
end;

procedure TFmPQRet.BtInsumosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInsumos, BtInsumos);
end;

procedure TFmPQRet.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  //Nome: String;
begin
  if MyObjects.FIC(EdFisRegCad.ValueVariant = 0, EdFisRegCad, 'Defina a regra fiscal!') then Exit;  
  if DModG.VerificaDuplicidadeCodUsu('pqret', 'Nome', ['CodUsu'], EdNome, [EdCodUsu.ValueVariant], ImgTipo, True) then Exit;
  {
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  }
  Codigo := UMyMod.BuscaEmLivreY_Def('pqret', 'Codigo', ImgTipo.SQLType,
    QrPQRetCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmPQRet, PainelEdita,
    'pqret', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPQRet.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'pqret', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqret', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pqret', 'Codigo');
end;

procedure TFmPQRet.BtRetornoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRetorno, BtRetorno);
end;

procedure TFmPQRet.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPData.Date       := Date;
  PainelEdi2.Align  := alClient;
  //Panel4.Align      := alClient;
  DBGPQ.Align       := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientesI, Dmod.MyDB);
  QrPQ010.DataBase := DModG.MyPID_DB;
end;

procedure TFmPQRet.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPQRetCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPQRet.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmPQRet.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPQRet.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPQRetCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPQRet.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPQRet.QrPQRetAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPQRet.QrPQRetAfterScroll(DataSet: TDataSet);
begin
  ReopenPQRetIts(0);
end;

procedure TFmPQRet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQRet.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPQRetCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'pqret', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPQRet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPQRet.Incluinovoretorno1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrPQRet, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'pqret');
end;

procedure TFmPQRet.QrPQRetBeforeClose(DataSet: TDataSet);
begin
  QrPQRetIts.Close;
end;

procedure TFmPQRet.QrPQRetBeforeOpen(DataSet: TDataSet);
begin
  QrPQRetCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPQRet.ReopenPQ010;
begin
  UnDmkDAC_PF.AbreQuery(QrPQ010, DModG.MyPID_DB);
end;

procedure TFmPQRet.ReopenPQRetIts(Controle: Integer);
begin
  QrPQRetIts.Close;
  QrPQRetIts.Params[0].AsInteger := QrPQRetCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPQRetIts, Dmod.MyDB);
  //
  QrPQRetIts.Locate('Controle', Controle, []);
end;

procedure TFmPQRet.Retirainsumosselecionados1Click(Sender: TObject);
  procedure RetiraItemAtual();
  begin
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('');
    //Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM pqretits ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[0].AsInteger := QrPQRetItsControle.Value;
    Dmod.QrUpd.ExecSQL;
  end;
var
  i: Integer;
begin
  if DBGPQ.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma a retirada dos ' + IntToStr(
    DBGPQ.SelectedRows.Count) + ' itens selecionados?') = ID_YES then
    begin
      with DBGPQ.DataSource.DataSet do
      for i:= 0 to DBGPQ.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGPQ.SelectedRows.Items[i]));
        RetiraItemAtual();
      end;
      ReopenPQRetIts(0);
    end;
  end else
  begin
    if Geral.MB_Pergunta('Confirma a retirada do item selecionado?') = ID_YES then
    begin
      RetiraItemAtual();
      ReopenPQRetIts(0);
    end;
  end;
end;

end.

