unit PQRInnPlus;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit;

type
  TFmPQRInnPlus = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PainelDados: TPanel;
    Label10: TLabel;
    Label19: TLabel;
    EdValor: TdmkEdit;
    EdPeso: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCliInt, FInsumo: Integer;
  end;

  var
  FmPQRInnPlus: TFmPQRInnPlus;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmPQRInnPlus.BtOKClick(Sender: TObject);
var
  DataX: String;
  OrigemCodi, OrigemCtrl, Tipo, CliOrig, CliDest, Insumo, //Retorno, StqMovIts,
  Controle: Integer;
  Peso, Valor(*, RetQtd*): Double;
begin
  DataX          := '0000-00-00';
  OrigemCodi     := 0;
  //OrigemCtrl     := Controle;
  Tipo           := 10;
  CliOrig        := FCliInt;
  CliDest        := FCliInt;
  Insumo         := FInsumo;
  Peso           := EdPeso.ValueVariant;
  Valor          := EdValor.ValueVariant;
(*  Retorno        := ;
  StqMovIts      := ;
  RetQtd         := ;*)

  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PQEIts','PQEIts','Controle');
  OrigemCtrl     := Controle;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqx', False, [
  'DataX', 'CliOrig', 'CliDest',
  'Insumo', 'Peso', 'Valor'(*,
  'Retorno', 'StqMovIts', 'RetQtd'*)], [
  'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
  DataX, CliOrig, CliDest,
  Insumo, Peso, Valor(*,
  Retorno, StqMovIts, RetQtd*)], [
  OrigemCodi, OrigemCtrl, Tipo], False) then
    Close;
end;

procedure TFmPQRInnPlus.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPQRInnPlus.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPQRInnPlus.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPQRInnPlus.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
