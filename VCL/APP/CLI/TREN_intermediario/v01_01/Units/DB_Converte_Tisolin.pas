unit DB_Converte_Tisolin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnMLAGeral,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts, (*DBSDDadoss,*)
  Math, UnDmkProcFunc, UnDmkEnums, Data.DBXFirebird, Datasnap.DBClient,
  SimpleDS, Data.SqlExpr, MySQLBatch, dmkDBGridZTO, AppListas;

type
  TTabLocIts = (tliNenhum = 0, tliPrincipal = 1, tliSecundario = 2,
                tliAcessorio = 3, tliUso = 4, tliConsumo = 5);
  //
  TFmDB_Converte_Tisolin = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel5: TPanel;
    Panel6: TPanel;
    DsDBF: TDataSource;
    EdRegistrosTb: TdmkEdit;
    Label1: TLabel;
    DsCadCli: TDataSource;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    DsPsq: TDataSource;
    Panel7: TPanel;
    Panel9: TPanel;
    DBGrid2: TDBGrid;
    MeSQL1: TMemo;
    Panel8: TPanel;
    BtExecuta: TBitBtn;
    Splitter1: TSplitter;
    Label2: TLabel;
    EdRegistrosQr: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel10: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdCliCodIni: TdmkEdit;
    EdForCodIni: TdmkEdit;
    EdEntCodIni: TdmkEdit;
    QrPsqEnt: TmySQLQuery;
    QrPsqEntCodigo: TIntegerField;
    CGDBF: TdmkCheckGroup;
    TabSheet4: TTabSheet;
    Memo1: TMemo;
    PB1: TProgressBar;
    Label6: TLabel;
    EdTabela: TdmkEdit;
    SpeedButton1: TSpeedButton;
    DsCadFor: TDataSource;
    DsCadGru: TDataSource;
    DataSource1: TDataSource;
    QrExiste: TmySQLQuery;
    QrGraGXPatr: TmySQLQuery;
    QrGraGXPatrAGRPAT: TWideStringField;
    DataSource2: TDataSource;
    DsCadVde: TDataSource;
    TabSheet5: TTabSheet;
    Memo2: TMemo;
    QrGraFabMar: TmySQLQuery;
    QrGraFabMarControle: TIntegerField;
    QrGraGXPatrGraGruX: TIntegerField;
    Button1: TButton;
    QrSituacao: TmySQLQuery;
    QrSituacaoSituacao: TWordField;
    QrSituacaoSIGLA: TWideStringField;
    QrSituacaoITENS: TLargeintField;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    DataSource6: TDataSource;
    DBFB: TSQLConnection;
    SDTabelas: TSimpleDataSet;
    SDDados: TSimpleDataSet;
    SDCliente: TSimpleDataSet;
    SDCidade: TSimpleDataSet;
    TabSheet6: TTabSheet;
    MeErrosGerais: TMemo;
    SDCidadC: TSimpleDataSet;
    SDClienteCLIENTE: TFMTBCDField;
    SDClienteRAZAOSOCIAL: TStringField;
    SDClienteFANTASIA: TStringField;
    SDClienteTIPOPESSOA: TStringField;
    SDClienteCPFCGC: TStringField;
    SDClienteRGINSCRICAOESTADUAL: TStringField;
    SDClienteNATURALIDADE: TStringField;
    SDClienteENDERECO: TStringField;
    SDClienteENDERECONUMERO: TFMTBCDField;
    SDClienteBAIRRO: TStringField;
    SDClienteCOMPLEMENTO: TStringField;
    SDClienteCIDADE: TFMTBCDField;
    SDClienteCEP: TStringField;
    SDClienteFONE1: TStringField;
    SDClienteFONE2: TStringField;
    SDClienteCELULAR: TStringField;
    SDClienteANIVERSARIOABERTURA: TDateField;
    SDClienteTIPOCADASTRO: TFMTBCDField;
    SDClienteCIDADECOBRANCA: TFMTBCDField;
    SDClienteENDERECOCOBRANCA: TStringField;
    SDClienteBAIRROENDERECOCOBRANCA: TStringField;
    SDClienteCEPENDERECOCOBRANCA: TStringField;
    SDClienteCOMPLEMENTOCOBRANCA: TStringField;
    SDClienteLOCALTRABALHO: TStringField;
    SDClienteENDERECOTRABALHO: TStringField;
    SDClienteFONETRABALHO: TStringField;
    SDClienteTEMPOTRABALHO: TStringField;
    SDClienteFUNCAOTRABALHO: TStringField;
    SDClienteRENDATRABALHO: TFMTBCDField;
    SDClienteNOMECONJUGE: TStringField;
    SDClienteLOCALTRABALHOCONJUGE: TStringField;
    SDClienteENDERECOTRABALHOCONJUGE: TStringField;
    SDClienteFONETRABALHOCONJUGE: TStringField;
    SDClienteTEMPOTRABALHOCONJUGE: TStringField;
    SDClienteFUNCAOTRABALHOCONJUGE: TStringField;
    SDClienteRENDACONJUGE: TFMTBCDField;
    SDClientePAI: TStringField;
    SDClienteMAE: TStringField;
    SDClienteREFERENCIA1: TStringField;
    SDClienteREFERENCIAFONE_1: TStringField;
    SDClienteREFERENCIA2: TStringField;
    SDClienteREFERENCIAFONE_2: TStringField;
    SDClienteREFERENCIA3: TStringField;
    SDClienteREFERENCIAFONE_3: TStringField;
    SDClienteOBSERVACAO: TStringField;
    SDClienteINFORMACOES: TStringField;
    SDClienteEMAILPRIMARIO: TStringField;
    SDClienteCREDITO: TFMTBCDField;
    SDClienteISENTO: TStringField;
    SDClienteCONDICAO: TFMTBCDField;
    SDClientePARCELAS: TFMTBCDField;
    SDClienteINTERVALO: TFMTBCDField;
    SDClienteDATACADASTRO: TDateField;
    SDClienteDATAATUALIZACAO: TDateField;
    SDClienteULTIMACOMPRA: TDateField;
    SDClienteUSUARIOCADASTRO: TStringField;
    SDClienteUSUARIOATUALIZACAO: TStringField;
    SDClienteENDERECOENTREGA: TStringField;
    SDClienteOBSERVACAOCLIENTE: TStringField;
    SDClienteLIBERADODOCUMENTOVENCIDO: TStringField;
    SDClienteOBSERVACAOVENDA: TStringField;
    SDClienteDESCONTO: TFMTBCDField;
    SDClienteVENDEDOR: TFMTBCDField;
    SDClienteOBSERVACAOTRAVAMENTO: TStringField;
    SDClienteTRAVAMENTO: TStringField;
    SDClienteLIBERADOUMAFATURA: TStringField;
    SDClienteATIVO: TStringField;
    SDClienteORGAOPUBLICO: TStringField;
    SDClienteNCM: TStringField;
    SDClientePASTAXML: TStringField;
    SDClienteDIACOBRANCA: TFMTBCDField;
    SDClienteOBSERVACAODIACOBRANCA: TStringField;
    SDClienteEMAILFINANCEIRO: TStringField;
    SDClienteNAOCONTRIBUINTE: TStringField;
    SDClienteCONTRIBUINTEISENTO: TStringField;
    SDClienteCONTATO: TStringField;
    SDClienteFATURAR: TStringField;
    SDClienteCOBRADOR: TFMTBCDField;
    SDClienteCODCTC: TFMTBCDField;
    SDClienteIMPRIMEPRECOLIQUIDO: TStringField;
    SDClienteNUMEROPEDIDOITEM: TStringField;
    SDClienteINSCRICAOMUNICIPAL: TStringField;
    SDClienteSIMPLESNACIONAL: TStringField;
    SDClienteALIQUOTAISS: TFMTBCDField;
    SDClienteRETENCAOISS: TStringField;
    SDClienteLISTASTATUS: TStringField;
    MePronto: TMemo;
    SDClienteCIDADE_DESCRICAO: TStringField;
    SDClienteCIDADE_IBGE: TStringField;
    SDClienteCIDADE_PAIS: TStringField;
    SDClienteCIDADE_UF: TStringField;
    SDClienteCIDADC_IBGE: TStringField;
    SDClienteCIDADC_DESCRICAO: TStringField;
    SDLocacao: TSimpleDataSet;
    SDLocacaoLOCACAO: TFMTBCDField;
    SDLocacaoCLIENTE: TFMTBCDField;
    SDLocacaoEMISSAODATA: TDateField;
    SDLocacaoEMISSAOHORA: TTimeField;
    SDLocacaoDEVOLUCAODATA: TDateField;
    SDLocacaoDEVOLUCAOHORA: TTimeField;
    SDLocacaoSAIDADATA: TDateField;
    SDLocacaoSAIDAHORA: TTimeField;
    SDLocacaoFINALIZADODATA: TDateField;
    SDLocacaoFINALIZADOHORA: TTimeField;
    SDLocacaoVALOREQUIPAMENTO: TFMTBCDField;
    SDLocacaoVALORLOCACAO: TFMTBCDField;
    SDLocacaoVALORADICIONAL: TFMTBCDField;
    SDLocacaoVALORADIANTAMENTO: TFMTBCDField;
    SDLocacaoTIPOALUGUEL: TStringField;
    SDLocacaoOBSERVACAO: TStringField;
    SDLocacaoFECHAMENTODATA: TDateField;
    SDLocacaoFECHAMENTOHORA: TTimeField;
    SDLocacaoUSUARIO: TStringField;
    SDLocacaoSTATUS: TFMTBCDField;
    SDLocacaoVALORDESCONTO: TFMTBCDField;
    SDLocacaoOBRA: TStringField;
    SDLocacaoCANCELADA: TStringField;
    SDLocacaoULTIMACOBRANCAMENSAL: TDateField;
    SDAux: TSimpleDataSet;
    Panel11: TPanel;
    CkLimit: TCheckBox;
    Label7: TLabel;
    EdSkip: TdmkEdit;
    Label8: TLabel;
    EdFirst: TdmkEdit;
    PB2: TProgressBar;
    LaAvisoG1: TLabel;
    LaAvisoG2: TLabel;
    MySQLBatchExecute1: TMySQLBatchExecute;
    SDLocacaoItens: TSimpleDataSet;
    SDStatusCliente: TSimpleDataSet;
    SDStatusClienteSTATUS: TFMTBCDField;
    SDStatusClienteDESCRICAO: TStringField;
    SDLocacaoItensLOCACAO: TFMTBCDField;
    SDLocacaoItensSEQUENCIA: TFMTBCDField;
    SDLocacaoItensPRODUTO: TFMTBCDField;
    SDLocacaoItensQUANTIDADEPRODUTO: TFMTBCDField;
    SDLocacaoItensVALORPRODUTO: TFMTBCDField;
    SDLocacaoItensQUANTIDADELOCACAO: TFMTBCDField;
    SDLocacaoItensVALORLOCACAO: TFMTBCDField;
    SDLocacaoItensQUANTIDADEDEVOLUCAO: TFMTBCDField;
    SDLocacaoItensTROCA: TStringField;
    SDLocacaoItensCATEGORIA: TStringField;
    SDLocacaoItensCOBRANCALOCACAO: TStringField;
    SDLocacaoItensCOBRANCACONSUMO: TFMTBCDField;
    SDLocacaoItensCOBRANCAREALIZADAVENDA: TStringField;
    SDLocacaoItensDATAINICIALCOBRANCA: TDateField;
    SDLocacaoItensHORAINICIALCOBRANCA: TTimeField;
    SDLocacaoItensDATASAIDA: TDateField;
    SDLocacaoItensHORASAIDA: TTimeField;
    SDClienteStatus: TSimpleDataSet;
    SDClienteStatusCLIENTE: TFMTBCDField;
    SDClienteStatusSTATUS: TFMTBCDField;
    SDProduto: TSimpleDataSet;
    SDProdutoPRODUTO: TFMTBCDField;
    SDProdutoDESCRICAO: TStringField;
    SDProdutoMAQ_BASE: TStringField;
    SDProdutoMARCA: TStringField;
    SDProdutoIMPORTADO: TStringField;
    SDProdutoGRUPO: TFMTBCDField;
    SDProdutoMARGEM: TFMTBCDField;
    SDProdutoUNIDADE: TStringField;
    SDProdutoCUSTO: TFMTBCDField;
    SDProdutoESTOQUE: TFMTBCDField;
    SDProdutoPRECOVENDA: TFMTBCDField;
    SDProdutoATIVO: TStringField;
    SDProdutoMARGEMSUBSTITUICAO: TFMTBCDField;
    SDProdutoPRODUTODECIMAL: TStringField;
    SDProdutoDUPLICA: TStringField;
    SDProdutoPRECOPROMOCAO: TFMTBCDField;
    SDProdutoDATAPROMOCAO: TDateField;
    SDProdutoCLASSIFICACAOFISCAL: TFMTBCDField;
    SDProdutoULTIMOFORNECEDOR: TFMTBCDField;
    SDProdutoULTIMANOTA: TStringField;
    SDProdutoULTIMAQUANTIDADE: TFMTBCDField;
    SDProdutoULTIMADATA: TDateField;
    SDProdutoPULTIMOFORNECEDOR: TFMTBCDField;
    SDProdutoPULTIMANOTA: TStringField;
    SDProdutoPULTIMAQUANTIDADE: TFMTBCDField;
    SDProdutoPULTIMADATA: TDateField;
    SDProdutoPULTIMOCUSTO: TFMTBCDField;
    SDProdutoPULTIMOPRECOVENDA: TFMTBCDField;
    SDProdutoDATAMVTOESTOQUE: TSQLTimeStampField;
    SDProdutoMENSAGEM: TStringField;
    SDProdutoNCM: TStringField;
    SDProdutoULTIMAATUALIZACAO: TSQLTimeStampField;
    SDProdutoULTIMOVALOR: TFMTBCDField;
    SDProdutoPULTIMOVALOR: TFMTBCDField;
    SDProdutoLINHA: TStringField;
    SDProdutoESTOQUEEXTERNO: TFMTBCDField;
    SDProdutoDESCONTO: TFMTBCDField;
    SDProdutoBOCACAIXA: TFMTBCDField;
    SDProdutoSOMACUSTO: TFMTBCDField;
    SDProdutoTIPOITEM: TFMTBCDField;
    SDProdutoGENERO: TFMTBCDField;
    SDProdutoMODELO: TFMTBCDField;
    SDProdutoSERIE: TStringField;
    SDProdutoREFERENCIA: TStringField;
    SDProdutoPESOMAQUINA: TFMTBCDField;
    SDProdutoCODIGOBARRAS: TStringField;
    SDProdutoCODIGOFABRICA: TStringField;
    SDProdutoUSUARIOCADASTRO: TStringField;
    SDProdutoUSUARIOALTERACAO: TStringField;
    SDProdutoDATAHORACADASTRO: TSQLTimeStampField;
    SDProdutoCLA_TRI: TStringField;
    SDProdutoPERCIPI: TFMTBCDField;
    SDProdutoPERCFRETEPROPRIO: TFMTBCDField;
    SDProdutoPERCFRETETERCEIRO: TFMTBCDField;
    SDProdutoPERCDESPESAS: TFMTBCDField;
    SDProdutoPERCFINANCEIRO: TFMTBCDField;
    SDProdutoPERCPISCOFINSCREDITO: TFMTBCDField;
    SDProdutoPERCICMORIGEM: TFMTBCDField;
    SDProdutoPERCICMDESTINO: TFMTBCDField;
    SDProdutoTABELAFORNECEDOR: TFMTBCDField;
    SDProdutoCUSTOREPOSICAO: TFMTBCDField;
    SDProdutoPULTIMOPERCFRETEPROPRIO: TFMTBCDField;
    SDProdutoPULTIMOPERCFRETETERCEIRO: TFMTBCDField;
    SDProdutoPULTIMOPERCDESPESAS: TFMTBCDField;
    SDProdutoPULTIMOPERCFINANCEIRO: TFMTBCDField;
    SDProdutoPULTIMOPERCPISCOFINSCREDITO: TFMTBCDField;
    SDProdutoPULTIMOPERCICMORIGEM: TFMTBCDField;
    SDProdutoPULTIMOPERCICMDESTINO: TFMTBCDField;
    SDProdutoPULTIMOCUSTOREPOSICAO: TFMTBCDField;
    SDProdutoPULTIMOPERCIPI: TFMTBCDField;
    SDProdutoPULTIMAMARGEMSUBSTITUICAO: TFMTBCDField;
    SDProdutoPULTIMATABELAFORNECEDOR: TFMTBCDField;
    SDProdutoPULTIMAMARGEM: TFMTBCDField;
    SDProdutoQUANTIDADEEMBALAGEMCOMPRA: TFMTBCDField;
    SDProdutoSERVICO: TStringField;
    SDProdutoLOCALIZACAO: TStringField;
    SDProdutoMULTIPLOVENDA: TFMTBCDField;
    SDProdutoDESCRICAOETIQUETA: TStringField;
    SDProdutoFICHATECNICA: TStringField;
    SDProdutoIMAGEM: TMemoField;
    SDProdutoTEMFICHATECNICA: TStringField;
    SDProdutoTEMIMAGEM: TStringField;
    SDProdutoULTIMAATUALIZACAOIMAGEM: TDateField;
    SDProdutoPRODUTONOVO: TFMTBCDField;
    SDProdutoPATRIMONIO: TStringField;
    SDProdutoCATEGORIA: TStringField;
    SDProdutoVALORDIARIA: TFMTBCDField;
    SDProdutoVALORSEMANAL: TFMTBCDField;
    SDProdutoVALORQUINZENAL: TFMTBCDField;
    SDProdutoVALORMENSAL: TFMTBCDField;
    SDProdutoPRECOFDS: TFMTBCDField;
    SDProdutoDIASCARENCIA: TFMTBCDField;
    SDProdutoREDUTORDIARIA: TFMTBCDField;
    SDProdutoCOBRANCACONSUMO: TFMTBCDField;
    SDGrupo: TSimpleDataSet;
    SDGrupoGRUPO: TFMTBCDField;
    SDGrupoDESCRICAO: TStringField;
    SDGrupoATUALIZA: TStringField;
    SDGrupoULTIMAATUALIZACAO: TSQLTimeStampField;
    SDGrupoMENSAGEM: TStringField;
    TabSheet7: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    SDLinha: TSimpleDataSet;
    BitBtn1: TBitBtn;
    SDLinhaDESCRICAO: TStringField;
    SDLinhaGRUPO: TFMTBCDField;
    SDLinhaLINHA: TStringField;
    SDLinhaITENS: TIntegerField;
    SDMarca: TSimpleDataSet;
    SDMarcaMARCA: TStringField;
    SDMarcaULTIMAATUALIZACAO: TSQLTimeStampField;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    QrGraGX: TMySQLQuery;
    SDGraGX: TSimpleDataSet;
    SDGraGXPRODUTO: TFMTBCDField;
    SDGraGXACESSORIO: TFMTBCDField;
    SDProdutoAcessorio: TSimpleDataSet;
    SDProdutoAcessorioPRODUTO: TFMTBCDField;
    SDProdutoAcessorioACESSORIO: TFMTBCDField;
    SDProdutoAcessorioQUANTIDADE: TFMTBCDField;
    QrItem: TMySQLQuery;
    QrItemGraGruX: TIntegerField;
    QrItemTABELA: TWideStringField;
    QrItemAplicacao: TIntegerField;
    QrGraGruY: TMySQLQuery;
    FbDB: TMySQLDatabase;
    QrGraGruYTipoProd: TWideStringField;
    QrGraGruYGGY_KIND: TWideStringField;
    QrGraGruYAtivo: TWideStringField;
    QrGraGruYDESCRICAO: TWideStringField;
    QrGraGruYPRODUTO: TLargeintField;
    QrGraGruYLinha: TWideStringField;
    QrGraGruYCategoria: TWideStringField;
    QrGraGruYReferencia: TWideStringField;
    QrGraGruYPatrimonio: TWideStringField;
    QrLocados: TMySQLQuery;
    QrLocadosPRODUTO: TLargeintField;
    QrLocadosQtdLocado: TLargeintField;
    BitBtn2: TBitBtn;
    QrSeqIL: TMySQLQuery;
    QrSeqILGraGruY: TIntegerField;
    QrSeqILLOCACAO: TLargeintField;
    QrSeqILSEQUENCIA: TLargeintField;
    QrSeqILPRODUTO: TLargeintField;
    QrSeqILQUANTIDADEPRODUTO: TLargeintField;
    QrSeqILVALORPRODUTO: TFloatField;
    QrSeqILQUANTIDADELOCACAO: TLargeintField;
    QrSeqILVALORLOCACAO: TFloatField;
    QrSeqILQUANTIDADEDEVOLUCAO: TLargeintField;
    QrSeqILTROCA: TWideStringField;
    QrSeqILCATEGORIA: TWideStringField;
    QrSeqILCOBRANCALOCACAO: TWideStringField;
    QrSeqILCOBRANCACONSUMO: TFloatField;
    QrSeqILCOBRANCAREALIZADAVENDA: TWideStringField;
    QrSeqILDATAINICIALCOBRANCA: TDateField;
    QrSeqILHORAINICIALCOBRANCA: TTimeField;
    QrSeqILDATASAIDA: TDateField;
    QrSeqILHORASAIDA: TTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbDBFBeforeClose(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure BtExecutaClick(Sender: TObject);
    procedure QrPsqBeforeClose(DataSet: TDataSet);
    procedure MeSQL1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure CGDBFClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SDDadosAfterOpen(DataSet: TDataSet);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    FTabLctA, FTabela, FTabStr: String;
    FCargoSocio, FPrincipalCli, FConComprador, FPrincipalFor,
    FFoneFix, FFoneCel, FFoneFax, FEMail, FETP_SERASA, FETP_SCPC, FETP_ACIM: Integer;
    //
    procedure Avisa(Str: String; Aguarde: Boolean = True);
    procedure ReabrePsqEnt(Antigo: String);
    procedure ReabreTabela(Table: TSimpleDataSet);
    //
    function PV_N(Texto: String): String;
    function PV_A(Texto: String): String;
    //
    function  VerificaCadastro(Tabela, Codigo, CodUsu, Nome,
              Texto: String; NovoCodigo: TNovoCodigo): Integer;
{
    procedure IncluiSocio(Codigo, CargoSocio: Integer;
              Nome, _RG, _CPF: String; Natal: TDateTime);
    procedure IncluiContato(Codigo, Cargo: Integer;
              Nome, Tel, Cel, Fax, Mail: String);
    procedure IncluiEntiTel(Codigo, Controle, EntiTipCto: Integer;
              Tel: String);
    procedure IncluiEntiMail(Codigo, Controle, EntiTipCto: Integer;
              EMail: String);
    procedure IncluiBanco(Codigo, Ordem: Integer; Nome, Bco, Age: String);
    procedure IncluiConsulta(Codigo, Orgao: Integer;
              Numero: String; _Data: TDateTime; _Hora, _Resultado: String);
}
    //
    procedure CadastraPrdGrupTip();
    procedure CadastraGraGru1SemGrade(PrdGrupTip, Nivel5, Nivel4, Nivel3,
              Nivel2, Nivel1, CodUsu: Integer; Nome, Referencia: String);
    function  TabelaEstahSelecionada(Tabela: String): Boolean;
    //
    function  ObtemGraGruXDeCODMATX(CODMATX: String): Integer;
    //
    procedure CadastraSituacoes();

    //
{
    procedure ImportaTabela_CadCli();
    procedure ImportaTabela_CadFor();
    procedure ImportaTabela_CadGru();
    procedure ImportaTabela_CadPat();
    procedure ImportaTabela_CadCtr();
    procedure ImportaTabela_CadVde();
    procedure ImportaTabela_CadBco();
    procedure ImportaTabela_CadNat();
    procedure ImportaTabela_PagDup();
    procedure ImportaTabela_RecDup();
}

////////////////////////////////////////////////////////////////////////////////
    // deprecado
    procedure ImportaTabelaFB_Cliente2();
    //
    function  GetNivel2(Grupo, Linha: Integer): Integer;
    procedure ImportaTabelaFB_StatusEnti();
    procedure ImportaTabelaFB_Cliente();
    procedure ImportaTabelaFB_ClienteStatus();
    procedure ImportaTabelaFB_AnexoDocumento();
    procedure ImportaTabelaFB_Marca();
    procedure ImportaTabelaFB_Grupo();
    procedure ImportaTabelaFB_Linha();
    procedure ImportaTabelaFB_Produto();
    procedure ImportaTabelaFB_ProdutoAcessorio();
    procedure ImportaTabelaFB_Locacao();
    procedure ImportaTabelaFB_LocacaoItens();

    procedure AbreTabela(Tabela: String);
{
    procedure ImportaTabela_PagDup;
    procedure ImportaTabela_RecDup;
}
    procedure AtualizaSaldosLocados();
  public
    { Public declarations }
  end;

  var
  FmDB_Converte_Tisolin: TFmDB_Converte_Tisolin;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModProd, ModuleGeral,
  UnFinanceiro, UnGrade_Jan, GraGruX;

{$R *.DFM}

var
  LocDatas: array of String;
  LocHoras: array of String;
  DevDatas: array of String;
  DevHoras: array of String;
  Acessorio: array of Boolean;
  TipoLoc: array of String[1];

procedure TFmDB_Converte_Tisolin.AbreTabela(Tabela: String);
begin
  SDDados.Active := False;
  SDDados.DataSet.CommandType := ctQuery;
  SDDados.DataSet.CommandText :=
  //'select * from ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
  'select * from ' + Tabela;
  SDDados.Active := True;
end;

procedure TFmDB_Converte_Tisolin.AtualizaSaldosLocados;
var
  GraGruX: Integer;
  //EstqAnt, EstqEnt, EstqSdo,
  EstqSai: Double;
  //SQLType: TSQLType;
  sEstqSai: String;
begin
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Atualiza��o de estoque de produtos de loca��o');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocados, FbDB, [
  'DROP TABLE IF EXISTS _LOCACAOES_ABERTAS_; ',
  'CREATE TABLE _LOCACAOES_ABERTAS_ ',
  '      SELECT DISTINCT LOCACAO ',
  '      FROM LOCACAO ',
  '      WHERE FECHAMENTODATA < "1900-01-01" ',
  '      AND STATUS<3; ',
  'SELECT PRODUTO,  ',
  '(QUANTIDADEPRODUTO-QUANTIDADEDEVOLUCAO) QtdLocado ',
  'FROM LOCACAOITENS its ',
  'WHERE CATEGORIA="L" ',
  '/*AND PRODUTO  = 37*/ ',
  'AND QUANTIDADEDEVOLUCAO<QUANTIDADEPRODUTO ',
  'AND LOCACAO IN ( ',
  '  SELECT LOCACAO FROM _LOCACAOES_ABERTAS_ ',
  ') ',
  'GROUP BY PRODUTO ',
  'ORDER BY PRODUTO; ',
  '']);
  QrLocados.First;
  while not QrLocados.Eof do
  begin
    GraGruX        := QrLocadosPRODUTO.Value;
    EstqSai        := QrLocadosQtdLocado.Value;
    SEstqSai       := Geral.FFT_Dot(EstqSai, 3, siPositivo);
    //
    Dmod.MyDB.Execute(
      'UPDATE gragruepat SET EstqSai := ' + sEstqSai + ', EstqAnt = EstqAnt + '
      + sEstqSai +
      ' WHERE GraGruX=' + Geral.FF0(GraGruX));
    //
    QrLocados.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.Avisa(Str: String; Aguarde: Boolean);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, Aguarde, FTabStr + Str);
end;

procedure TFmDB_Converte_Tisolin.BitBtn2Click(Sender: TObject);
var
  //LocacaoAnt,
  LocacaoAtu, CtrID: Integer;
  CtrTemPri, CtrTemOut: Boolean;
  LocacaoesComProblema: String;
  ContaLCP: Integer;
begin
  LocacaoesComProblema := EmptyStr;
  ContaLCP := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSeqIL, Dmod.MyDB, [
  'SELECT ggx.GraGruY, Its.*  ',
  'FROM zzz_fb_db_ant.LOCACAOITENS its ',
  'LEFT JOIN ' + TMeuDB + '.GraGruX ggx  ',
  '  ON ggx.Controle=its.PRODUTO ',
  //'/*WHERE its.CATEGORIA = "L"*/ ',
  'WHERE its.CATEGORIA = "L"',
  'ORDER BY ggx.GraGruY, its.LOCACAO, its.SEQUENCIA ',
  '']);
  //LocacaoAnt := 0;
  LocacaoAtu := 0;
  CtrID := 0;
  CtrTemPri  := True;
  CtrTemOut  := True;
  //
  QrSeqIL.First;
  while not QrSeqIL.Eof do
  begin
    if QrSeqILLocacao.Value <> LocacaoAtu then
    begin
      if (CtrTemOut = True) and (CtrTemPri = False) then
      begin
        LocacaoesComProblema := LocacaoesComProblema + ',' + Geral.FF0(LocacaoAtu);
        ContaLCP := ContaLCP + 1;
      end;
      //
      LocacaoAtu := QrSeqILLocacao.Value;
      CtrID      := CtrID + 1;
      CtrTemPri  := False;
      CtrTemOut  := False;
    end;
    if QrSeqILGraGruY.Value = 1024 then
    begin
      if CtrTemPri = True then
        CtrID := CtrID + 1
      else
        CtrTemPri := True;
      //
      // Insere aqui PRI
      //
    end else
    begin
      if CtrTemPri = False then
      begin
        CtrTemOut := True;
      //
      // Insere aqui PRI
      //
      end;
    end;
    //
    QrSeqIL.Next;
  end;
  Geral.MB_Info('Loca��es com problema: ' + Geral.FF0(ContaLCP) + sLineBreak +
  LocacaoesComProblema);
end;

procedure TFmDB_Converte_Tisolin.BtExecutaClick(Sender: TObject);
begin
{20200920
  Screen.Cursor := crHourGlass;
  try
    QrPsq.Close;
    QrPsq.SQL.Text := MeSQL1.Text;
    QrPsq.Open;
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmDB_Converte_Tisolin.BtNenhumClick(Sender: TObject);
begin
  CGDBF.Value := 0;
end;

procedure TFmDB_Converte_Tisolin.BtOKClick(Sender: TObject);
var
  I, p: Integer;
begin
  if not FbDB.Connected then Exit;
  //
  MeErrosGerais.Lines.Clear;
  MePronto.Lines.Clear;
  DBFB.Connected := True;
  //
  if Application.MessageBox('Confirma a importa��o das tabelas selecionadas?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> IDYES then
    Exit;
  //
  //  Cadastros
  if TabelaEstahSelecionada('StatusCliente') then ImportaTabelaFB_StatusEnti();
  if TabelaEstahSelecionada('Cliente') then ImportaTabelaFB_Cliente();
  if TabelaEstahSelecionada('ClienteStatus') then ImportaTabelaFB_ClienteStatus();
  if TabelaEstahSelecionada('AnexoDocumento') then ImportaTabelaFB_AnexoDocumento();
  if TabelaEstahSelecionada('Marca') then ImportaTabelaFB_Marca();
  if TabelaEstahSelecionada('Grupo') then ImportaTabelaFB_Grupo();
  if TabelaEstahSelecionada('Linha') then ImportaTabelaFB_Linha();

  if TabelaEstahSelecionada('Produto') then ImportaTabelaFB_Produto();
  if TabelaEstahSelecionada('ProdutoAcessorio') then ImportaTabelaFB_ProdutoAcessorio();
  if TabelaEstahSelecionada('Locacao') then ImportaTabelaFB_Locacao();
  if TabelaEstahSelecionada('LocacaoItens') then ImportaTabelaFB_LocacaoItens();

{
  anexoDocumento

  produtos:
  GRUPO
}
end;

procedure TFmDB_Converte_Tisolin.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDB_Converte_Tisolin.BtTudoClick(Sender: TObject);
begin
  CGDBF.SetMaxValue;
end;

procedure TFmDB_Converte_Tisolin.Button1Click(Sender: TObject);
begin
  CadastraSituacoes();
end;

procedure TFmDB_Converte_Tisolin.CadastraGraGru1SemGrade(
  PrdGrupTip, Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, CodUsu: Integer;
  Nome, Referencia: String);
var
  Controle, GraCorCad: Integer;
  GraTamCad, GraGruC, GraGru1, GraTamI: Integer;
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragru1', False, [
  'Nivel5', 'Nivel4', 'Nivel3', 'Nivel2', 'CodUsu',
  'Nome', 'PrdGrupTip', 'Referencia'], [
  'Nivel1'], [
  Nivel5, Nivel4, Nivel3, Nivel2, CodUsu,
  Nome, PrdGrupTip, Referencia], [
  Nivel1], True) then
  begin
    // Definir Grade de tamanhos
    GraTamCad := DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
    'GraTamCad'], ['Nivel1'], [
    GraTamCad], [Nivel1], True);

    // Registrar a cor
    //Nivel1         := Nivel1;
    Controle       := UMyMod.BuscaEmLivreY_Def('gragruc', 'Controle', stIns, 0);
    GraCorCad      := DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
    'Nivel1', 'GraCorCad'], [
    'Controle'], [
    Nivel1, GraCorCad], [
    Controle], True) then
    begin
      // Cadastrar o item �nico da grade
      GraGruC        := Controle;
      GraGru1        := Nivel1;
      GraTamI        := DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger;
      Controle       := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, [
      'GraGruC', 'GraGru1', 'GraTamI'], [
      'Controle'], [
      GraGruC, GraGru1, GraTamI], [
      Controle], True) then
      begin
        VAR_CADASTRO2 := Controle;
      end;
    end;
  end;
end;

procedure TFmDB_Converte_Tisolin.CadastraPrdGrupTip();
var
  Nome, TitNiv1, TitNiv2, TitNiv3, TitNiv4, TitNiv5: String;
  Codigo, CodUsu, MadeBy, Fracio, Gradeado, TipPrd, NivCad, FaixaIni, FaixaFim,
  Customizav, ImpedeCad, LstPrcFisc, Tipo_Item, Genero: Integer;
  PerComissF, PerComissR: Double;
begin
  Codigo         := 1;
  CodUsu         := 1;
  Nome           := 'Patrim�nio';
  MadeBy         := 2;
  Fracio         := 0;
  Gradeado       := 0;
  TipPrd         := 3;
  NivCad         := 5;
  FaixaIni       := 0;
  FaixaFim       := 0;
  TitNiv1        := 'Nivel 1';
  TitNiv2        := 'Nivel 2';
  TitNiv3        := 'Nivel 3';
  TitNiv4        := 'Nivel 4';
  TitNiv5        := 'Nivel 5';
  Customizav     := 0;
  ImpedeCad      := 1;
  LstPrcFisc     := 5;
  PerComissF     := 0;
  PerComissR     := 0;
  Tipo_Item      := 0;
  Genero         := 0;

  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
  'CodUsu', 'Nome', 'MadeBy',
  'Fracio', 'Gradeado', 'TipPrd',
  'NivCad', 'FaixaIni', 'FaixaFim',
  'TitNiv1', 'TitNiv2', 'TitNiv3',
  'Customizav', 'ImpedeCad', 'LstPrcFisc',
  'PerComissF', 'PerComissR', 'Tipo_Item',
  'Genero', 'TitNiv4', 'TitNiv5'], [
  'Codigo'], [
  CodUsu, Nome, MadeBy,
  Fracio, Gradeado, TipPrd,
  NivCad, FaixaIni, FaixaFim,
  TitNiv1, TitNiv2, TitNiv3,
  Customizav, ImpedeCad, LstPrcFisc,
  PerComissF, PerComissR, Tipo_Item,
  Genero, TitNiv4, TitNiv5], [
  Codigo], True);
end;

procedure TFmDB_Converte_Tisolin.CadastraSituacoes();
var
  Nome, Sigla: String;
  Codigo, Aplicacao: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSituacao, Dmod.MyDB, [
  'SELECT DISTINCT Situacao, ',
  'CHAR(Situacao) SIGLA, ',
  'COUNT(Situacao) ITENS ',
  'FROM gragxpatr ',
  'GROUP BY Situacao ',
  'ORDER BY ITENS DESC ',
  '']);
  QrSituacao.First;
  while not QrSituacao.Eof do
  begin
    Codigo         := QrSituacaoSituacao.Value;
    Nome           := QrSituacaoSIGLA.Value;
    Sigla          := QrSituacaoSIGLA.Value;
    Aplicacao      := 0;
    if QrSituacaoSIGLA.Value = 'D' then
      Aplicacao := 1;
    //
    UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'graglsitu', False, [
    'Nome', 'Sigla', 'Aplicacao'], ['Codigo'], ['Ativo'], [
    Nome, Sigla, Aplicacao], [Codigo], [1], True);
    //
    QrSituacao.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.CGDBFClick(Sender: TObject);
var
  p: Integer;
  x: String;
begin
  x := Trim(CGDBF.Items[CGDBF.ItemIndex]);
  p := Pos(' ', x);
  if p = 0 then p := 1000;
  EdTabela.Text := Trim(Copy(x, 1, p));
end;

procedure TFmDB_Converte_Tisolin.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDB_Converte_Tisolin.FormCreate(Sender: TObject);
begin
  FTabLctA := 'lct0001a';
  //
  ImgTipo.SQLType := stLok;
  CGDBF.Value := Trunc(//1 +  // n�o importar carteiras!
    Power(2,  2) + //       4
    Power(2,  4) + //      16
    Power(2,  5) + //      32
    Power(2,  6) + //      64
    Power(2,  9) + //     512
    Power(2, 12) + //    4096
    power(2, 21) + // 2097152
    power(2, 22) + // 4194304
    0);
  try
    UnDmkDAC_PF.ConectaMyDB_DAC(FbDB, 'zzz_fb_db_ant', VAR_IP, VAR_PORTA, VAR_SQLUSER,
      VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
    FbDB.Connected := True;
  except
    Geral.MB_Erro('N�o foi poss�vel conectar no BD "zzz_fb_db_ant"');
  end;
  //
end;

procedure TFmDB_Converte_Tisolin.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmDB_Converte_Tisolin.GetNivel2(Grupo, Linha: Integer): Integer;
begin
  Result := (Integer(Grupo) * 1000) + Integer(Linha);
end;

procedure TFmDB_Converte_Tisolin.ReabrePsqEnt(Antigo: String);
begin
  QrPsqEnt.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqEnt, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM entidades ',
  'WHERE Antigo="' + Antigo + '"',
  'AND Codigo <> 0 ',
  'AND TRIM(Antigo) <> "" ',
  '']);
end;

procedure TFmDB_Converte_Tisolin.ReabreTabela(Table: TSimpleDataSet);
begin
  Avisa('Abrindo');
  Table.Close;
  Table.Open;
  Avisa('Aberta');
  PB1.Position := 0;
  PB1.Max := Table.RecordCount;
end;

procedure TFmDB_Converte_Tisolin.SDDadosAfterOpen(DataSet: TDataSet);
begin
  EdRegistrosTb.ValueVariant := SDDados.RecordCount;
end;

procedure TFmDB_Converte_Tisolin.SpeedButton1Click(Sender: TObject);
begin
  AbreTabela(EdTabela.Text);
end;

function TFmDB_Converte_Tisolin.TabelaEstahSelecionada(Tabela: String): Boolean;
var
  I, p: Integer;
  sTabSel: String;
begin
  FTabela := Tabela;
  Result := False;
  //
  for I := 0 to CGDBF.Items.Count - 1 do
  begin
    if CGDBF.Checked[I] then
    begin
      p := Pos(' ', Trim(CGDBF.Items[I]));
      sTabSel := Uppercase(Trim(Copy(CGDBF.Items[I], 1, p)));
      if sTabSel = Uppercase(FTabela) then
      begin
        (*
        TbDBF.Close;
        TbDBF.TableName := FTabela;
        TbDBF.Open;*)
        AbreTabela(FTabela);
        FTabStr := 'Tabela: [' + FTabela + '] ';
        Avisa('');
        //
        Result := True;
        Exit;
      end;
    end;
  end;
end;

procedure TFmDB_Converte_Tisolin.TbDBFBeforeClose(DataSet: TDataSet);
begin
  EdRegistrosTb.ValueVariant := 0;
end;


function TFmDB_Converte_Tisolin.VerificaCadastro(Tabela, Codigo, CodUsu, Nome,
  Texto: String; NovoCodigo: TNovoCodigo): Integer;
var
  Idx: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT ' + Codigo + ' FROM ' + Tabela,
  'WHERE ' + Nome + '="' + Texto + '"',
  '']);
  //
  if Dmod.QrAux.RecordCount > 0 then
    Result := Dmod.QrAux.FieldByName(Codigo).AsInteger
  else begin
    Idx := 0;
    case NovoCodigo of
      ncControle: Idx := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, Tabela, Codigo, [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: Idx := UMyMod.BPGS1I32(
        Tabela, Codigo, '', '', tsDef, stIns, 0);
      else
      begin
        Geral.MensagemBox('Tipo de obten��o de novo c�digo indefinido! [2]',
        'ERRO', MB_OK+MB_ICONERROR);
        Halt(0);
        //Exit;
      end;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO ' + Tabela + ' SET ');
    Dmod.QrUpd.SQL.Add(Codigo + '=' + Geral.FF0(Idx) + ',');
    if CodUsu <> '' then
      Dmod.QrUpd.SQL.Add(CodUsu + '=' + Geral.FF0(Idx) + ',');
    Dmod.QrUpd.SQL.Add(Nome + '="' + Texto + '"');
    Dmod.QrUpd.ExecSQL;
    //
    Result := Idx;
  end;
end;

//////////////////////// T A B E L A S  ////////////////////////////////////////

{
procedure TFmDB_Converte_Tisolin.ImportaTabela_CadBco();
var
  Nome, Nome2: String;
  Codigo, Tipo, ForneceI: Integer;
  SQLType: TSQLType;
begin
  ReabreTabela(TbCadBco);
  TbCadBco.First;
  while not TbCadBco.Eof do
  begin
    Codigo         := Trunc(TbCadBcoCODBCO.Value);
    Tipo           := 2;
    Nome           := TbCadBcoNOMBCO.Value;
    Nome2          := Nome;
    ForneceI       := -11;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Codigo ID',
    'FROM carteiras ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    if QrExiste.RecordCount = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    //  
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Nome);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'carteiras', False, [
    'Nome', 'Nome2', 'ForneceI'], [
    'Codigo', 'Tipo'], [
    Nome, Nome2, ForneceI], [
    Codigo, Tipo], True);
    //
    TbCadBco.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadCli();
const
  Mae = ''; NIRE = ''; FormaSociet = ''; Simples = ''; IEST = ''; Atividade = '';
  CPF_Pai = ''; CPF_Conjuge = ''; SSP = ''; DataRG = ''; CidadeNatal = '';
  EstCivil = ''; UFNatal = ''; Nacionalid = ''; EPais = ''; Ete3 = ''; ECel = '';
  PPais = ''; Pte3 = ''; PCel = '';
  //
  Cliente1 = 'V';
var
  ENDCLI: String;
  EAtividad, EUF, ECEP,
  PAtividad, PUF, PCEP,
  Codigo,    CUF, CCEP,
  Ordem, CodUsu, Tipo, Orgao: Integer;
  //
  CNPJ, IE, RazaoSocial, Fantasia, ELograd, ERua, ENumero, ECompl, EBairro, ECidade, ENatal, ETe1, ETe2, EFax, EContato, EEmail,
  CPF,  RG, Nome,        Apelido,  PLograd, PRua, PNumero, PCompl, PBairro, PCidade, PNatal, PTe1, PTe2, PFax, PContato, PEMail,
  Antigo, xBairro, xLogr, ECOCLI,  CLograd, CRua, CNumero, CCompl, CBairro, CCidade,
  //Numero, Compl, Bairro,
  Pai, Nivel, CreditosU, Observacoes, Comprador, Te1,
  Te2, Fax, Email, Respons1, Respons2, Txt: String;
  LimiCred: Double;
begin
(*
DELETE FROM entidades WHERE Codigo > 10000;
DELETE FROM enticontat;
DELETE FROM entimail;
DELETE FROM entirespon;
DELETE FROM entitel;
DELETE FROM entictas;
DELETE FROM entisrvpro;
*)
  ReabreTabela(TbCadCli);
  TbCadCli.First;
  while not TbCadCli.Eof do
  begin
    // N�o usa: TbCadCliFATCLI
    Antigo := 'C' + TbCadCliCODCLI.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Antigo);
    ReabrePsqEnt(Antigo);
    if QrPsqEnt.RecordCount = 0 then
    begin
      Codigo := Geral.IMV(TbCadCliCODCLI.Value) + EdCliCodIni.ValueVariant - 1;
      CodUsu := Codigo;
      ENDCLI := TbCadCliENDCLI.Value;
      CNPJ := Geral.SoNumero_TT(TbCadCliCGCCLI.Value);
      IE   := TbCadCliINSCLI.Value;
      if Geral.SoNumero_TT(IE) = '' then
        IE := '';
      CPF  := Geral.SoNumero_TT(TbCadCliCPFCLI.Value);
      RG   := TbCadCliRGECLI.Value;
      if Geral.SoNumero_TT(RG) = '' then
        RG := '';
      RazaoSocial := '';
      Fantasia := '';
      Nome := '';
      Apelido := '';
      EAtividad := 0; PAtividad := 0;
      ELograd := ''; ERua := ''; ENumero := ''; ECompl := '';
      PLograd := ''; PRua := ''; PNumero := ''; PCompl := '';
      EBairro := ''; PBairro := '';
      ECidade := ''; PCidade := '';
      EUF := 0; PUF := 0;
      ECEP := 0; PCEP := 0;
      ENatal := '0000-00-00';
      ETe1 := ''; ETe2 := ''; EFax := '';
      PTe1 := ''; PTe2 := ''; PFax := '';
      PNatal := '0000-00-00';
      PContato := ''; EContato := '';
      EEmail := ''; PEmail := '';
      //
      Comprador := Trim(TbCadCliCOMCLI.Value);
      Te1 := Geral.SoNumero_TT(TbCadCliTE1CLI.Value);
      Te2 := Geral.SoNumero_TT(TbCadCliTE2CLI.Value);
      Fax := Geral.SoNumero_TT(TbCadCliFAXCLI.Value);
      Email := Trim(Lowercase(TbCadCliEMACLI.Value));
      //
      PNumero := '';
      PCompl := '';
      xBairro := '';
      xLogr := '';
      PLograd := '';
      PRua := '';
      //
      if Length(CNPJ) >= 14 then
      begin
        Tipo := 0;
        Geral.SeparaEndereco(
          0, ENDCLI, ENumero, ECompl, xBairro, xLogr, ELograd, ERua,
          False, Memo1);
        //
        RazaoSocial := TbCadCliRAZCLI.Value;
        Fantasia := TbCadCliFANCLI.Value;
        EAtividad := Geral.IMV(TbCadCliRAMCLI.Value);
        EBairro := TbCadCliBAICLI.Value;
        ECidade := TbCadCliCIDCLI.Value;
        EUF := Geral.GetCodigoUF_da_SiglaUF(TbCadCliESTCLI.Value);
        ECEP := Geral.IMV(Geral.SoNumero_TT(TbCadCliCEPCLI.Value));
        ENatal := Geral.FDT(TbCadCliDTNCLI.Value, 1);
        //
        ETe1 := Te1;
        ETe2 := Te2;
        EFax := Fax;
        EContato := Comprador;
        EEmail := Email;
      end else
      begin
        Tipo := 1;
        Geral.SeparaEndereco(
        0, ENDCLI, PNumero, PCompl, xBairro, xLogr, PLograd, PRua,
        False, Memo1);
        //
        Nome := TbCadCliRAZCLI.Value;
        Apelido := TbCadCliFANCLI.Value;
        PAtividad := Geral.IMV(TbCadCliRAMCLI.Value);
        PBairro := TbCadCliBAICLI.Value;
        PCidade := TbCadCliCIDCLI.Value;
        PUF := Geral.GetCodigoUF_da_SiglaUF(TbCadCliESTCLI.Value);
        PCEP := Geral.IMV(Geral.SoNumero_TT(TbCadCliCEPCLI.Value));
        PNatal := Geral.FDT(TbCadCliDTNCLI.Value, 1);
        //
        PTe1 := Te1;
        PTe2 := Te2;
        PFax := Fax;
        PContato := Comprador;
        PEmail := Email;
      end;
      ECOCLI := TbCadCliECOCLI.Value;
      Geral.SeparaEndereco(
        0, ENDCLI, CNumero, CCompl, xBairro, xLogr, CLograd, CRua,
        False, Memo1);
      CBairro := TbCadCliBCOCLI.Value;
      CCidade := TbCadCliCIOCLI.Value;
      CUF := Geral.GetCodigoUF_da_SiglaUF(TbCadCliESOCLI.Value);
      CCEP := Geral.IMV(Geral.SoNumero_TT(TbCadCliCEOCLI.Value));
      //
      Pai := TbCadCliFILCLI.Value;
      Nivel := TbCadCliLIBCLI.Value;
      CreditosU :=  Geral.FDT(TbCadCliDTUCLI.Value, 1);
      LimiCred := TbCadCliLIMCLI.Value;
      try
        Observacoes := TbCadCliOBSCLI.Value;
      except
        Observacoes := '';
      end;
      //
      Respons1 := TbCadCliSO1CLI.Value;
      Respons2 := TbCadCliSO2CLI.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
      'CodUsu', 'RazaoSocial', 'Fantasia',
      'Respons1', 'Respons2', 'Pai',
      'Mae', 'CNPJ', 'IE',
      'NIRE', 'FormaSociet', 'Simples',
      'IEST', 'Atividade', 'Nome',
      'Apelido', 'CPF', 'CPF_Pai',
      'CPF_Conjuge', 'RG', 'SSP',
      'DataRG', 'CidadeNatal', 'EstCivil',
      'UFNatal', 'Nacionalid', 'ELograd',
      'ERua', 'ENumero', 'ECompl',
      'EBairro', 'ECidade', 'EUF',
      'ECEP', 'EPais', 'ETe1',
      'Ete2', 'Ete3', 'ECel',
      'EFax', 'EEmail', 'EContato',
      'ENatal', 'PLograd', 'PRua',
      'PNumero', 'PCompl', 'PBairro',
      'PCidade', 'PUF', 'PCEP',
      'PPais', 'PTe1', 'Pte2',
      'Pte3', 'PCel', 'PFax',
      'PEmail', 'PContato', 'PNatal',
      (*'Sexo', 'Responsavel', 'Profissao',
      'Cargo', 'Recibo', 'DiaRecibo',
      'AjudaEmpV', 'AjudaEmpP',*) 'Cliente1',
      (*'Cliente2', 'Cliente3', 'Cliente4',
      'Fornece1', 'Fornece2', 'Fornece3',
      'Fornece4', 'Fornece5', 'Fornece6',
      'Fornece7', 'Fornece8', 'Terceiro',
      'Cadastro', 'Informacoes', 'Logo',
      'Veiculo', 'Mensal',*) 'Observacoes',
      'Tipo', 'CLograd', 'CRua',
      'CNumero', 'CCompl', 'CBairro',
      'CCidade', 'CUF', 'CCEP',
      (*'CPais', 'CTel', 'CCel',
      'CFax', 'CContato', 'LLograd',
      'LRua', 'LNumero', 'LCompl',
      'LBairro', 'LCidade', 'LUF',
      'LCEP', 'LPais', 'LTel',
      'LCel', 'LFax', 'LContato',
      'Comissao', 'Situacao',*) 'Nivel',
      (*'Grupo', 'Account', 'Logo2',
      'ConjugeNome', 'ConjugeNatal', 'Nome1',
      'Natal1', 'Nome2', 'Natal2',
      'Nome3', 'Natal3', 'Nome4',
      'Natal4', 'CreditosI', 'CreditosL',
      'CreditosF2', 'CreditosD',*) 'CreditosU',
      (*'CreditosV', 'Motivo', 'QuantI1',
      'QuantI2', 'QuantI3', 'QuantI4', 
      'QuantN1', 'QuantN2', 'QuantN3', 
      'Agenda', 'SenhaQuer', 'Senha1',*) 
      'LimiCred', (*'Desco', 'CasasApliDesco',
      'TempA', 'TempD', 'Banco',
      'Agencia', 'ContaCorrente', 'FatorCompra',
      'AdValorem', 'DMaisC', 'DMaisD',
      'Empresa', 'CBE', 'SCB',*)
      'PAtividad', 'EAtividad', (*'PCidadeCod',
      'ECidadeCod', 'PPaisCod', 'EPaisCod',*)
      'Antigo' (*, 'CUF2', 'Contab',
      'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
      'Protestar', 'MultaCodi', 'MultaDias',
      'MultaValr', 'MultaPerc', 'MultaTiVe',
      'JuroSacado', 'CPMF', 'Corrido',
      'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
      'CartPref', 'RolComis', 'Filial',
      'EEndeRef', 'PEndeRef', 'CEndeRef',
      'LEndeRef', 'ECodMunici', 'PCodMunici',
      'CCodMunici', 'LCodMunici', 'CNAE',
      'SUFRAMA', 'ECodiPais', 'PCodiPais',
      'L_CNPJ', 'L_Ativo', 'CNAE20',
      'URL', 'CRT', 'COD_PART'*)], [
      'Codigo'], [
      CodUsu, RazaoSocial, Fantasia,
      Respons1, Respons2, Pai,
      Mae, CNPJ, IE,
      NIRE, FormaSociet, Simples,
      IEST, Atividade, Nome,
      Apelido, CPF, CPF_Pai,
      CPF_Conjuge, RG, SSP,
      DataRG, CidadeNatal, EstCivil,
      UFNatal, Nacionalid, ELograd,
      ERua, ENumero, ECompl,
      EBairro, ECidade, EUF,
      ECEP, EPais, ETe1,
      Ete2, Ete3, ECel,
      EFax, EEmail, EContato,
      ENatal, PLograd, PRua,
      PNumero, PCompl, PBairro,
      PCidade, PUF, PCEP,
      PPais, PTe1, Pte2,
      Pte3, PCel, PFax,
      PEmail, PContato, PNatal,
      (*Sexo, Responsavel, Profissao,
      Cargo, Recibo, DiaRecibo,
      AjudaEmpV, AjudaEmpP,*) Cliente1,
      (*Cliente2, Cliente3, Cliente4,
      Fornece1, Fornece2, Fornece3,
      Fornece4, Fornece5, Fornece6,
      Fornece7, Fornece8, Terceiro,
      Cadastro, Informacoes, Logo,
      Veiculo, Mensal,*) Observacoes,
      Tipo, CLograd, CRua,
      CNumero, CCompl, CBairro,
      CCidade, CUF, CCEP,
      (*CPais, CTel, CCel,
      CFax, CContato, LLograd,
      LRua, LNumero, LCompl,
      LBairro, LCidade, LUF,
      LCEP, LPais, LTel,
      LCel, LFax, LContato,
      Comissao, Situacao,*) Nivel,
      (*Grupo, Account, Logo2,
      ConjugeNome, ConjugeNatal, Nome1,
      Natal1, Nome2, Natal2,
      Nome3, Natal3, Nome4,
      Natal4, CreditosI, CreditosL,
      CreditosF2, CreditosD,*) CreditosU,
      (*CreditosV, Motivo, QuantI1,
      QuantI2, QuantI3, QuantI4,
      QuantN1, QuantN2, QuantN3,
      Agenda, SenhaQuer, Senha1,*)
      LimiCred, (*Desco, CasasApliDesco,
      TempA, TempD, Banco,
      Agencia, ContaCorrente, FatorCompra,
      AdValorem, DMaisC, DMaisD,
      Empresa, CBE, SCB,*)
      PAtividad, EAtividad, (*PCidadeCod,
      ECidadeCod, PPaisCod, EPaisCod,*)
      Antigo (*, CUF2, Contab,
      MSN1, PastaTxtFTP, PastaPwdFTP,
      Protestar, MultaCodi, MultaDias,
      MultaValr, MultaPerc, MultaTiVe,
      JuroSacado, CPMF, Corrido,
      CPF_Resp1, CliInt, AltDtPlaCt,
      CartPref, RolComis, Filial,
      EEndeRef, PEndeRef, CEndeRef,
      LEndeRef, ECodMunici, PCodMunici,
      CCodMunici, LCodMunici, CNAE,
      SUFRAMA, ECodiPais, PCodiPais,
      L_CNPJ, L_Ativo, CNAE20,
      URL, CRT, COD_PART*)], [
      Codigo], True) then
      begin
        //
        if (Comprador <> '') or (Te1 <> '') or (Te2 <> '') or (Fax <> '') or
        (Email <> '') then
          IncluiContato(Codigo, FConComprador, Comprador, Te1, Te2, Fax, Email);
        //
        //
        // S � C I O S
        //
          // S�cio 1
        if (TbCadCliSO1CLI.Value <> '') (*Nome*)
        or (TbCadCliRG1CLI.Value <> '') (*RG*)
        or (TbCadCliCP1CLI.Value <> '') (*CPF*) then
        //or (TbCadCliDN1CLI.Value <> 0) (*Nasc*) then
          IncluiSocio(Codigo, FCargoSocio,
          TbCadCliSO1CLI.Value, TbCadCliRG1CLI.Value, TbCadCliCP1CLI.Value,
          TbCadCliDN1CLI.Value);
          // S�cio 2
        if (TbCadCliSO2CLI.Value <> '') (*Nome*)
        or (TbCadCliRG2CLI.Value <> '') (*RG*)
        or (TbCadCliCP2CLI.Value <> '') (*CPF*)  then
        //or (TbCadCliDN2CLI.Value <> 0) (*Nasc*) then
          IncluiSocio(Codigo, FCargoSocio,
          TbCadCliSO2CLI.Value, TbCadCliRG2CLI.Value, TbCadCliCP2CLI.Value,
          TbCadCliDN2CLI.Value);

        // P r i n c i p a i s   c l i e n t e s
        if (TbCadCliCL1CLI.Value <> '') (*Nome*)
        or (TbCadCliCF1CLI.Value <> '') (*tel*) then
          // Cliente 1
          IncluiContato(Codigo, FPrincipalCli, TbCadCliCL1CLI.Value,
            TbCadCliCF1CLI.Value, '', '', '');
        if (TbCadCliCL2CLI.Value <> '') (*Nome*)
        or (TbCadCliCF2CLI.Value <> '') (*tel*) then
          // Cliente 2
          IncluiContato(Codigo, FPrincipalCli, TbCadCliCL2CLI.Value,
            TbCadCliCF2CLI.Value, '', '', '');

        // P r i n c i p a i s   f o r n e c e d o r e s
        if (TbCadCliFO1CLI.Value <> '') (*Nome*)
        or (TbCadCliFF1CLI.Value <> '') (*tel*) then
          // Fornecedor 1
          IncluiContato(Codigo, FPrincipalFor, TbCadCliFO1CLI.Value,
            TbCadCliFF1CLI.Value, '', '', '');
        if (TbCadCliFO2CLI.Value <> '') (*Nome*)
        or (TbCadCliFF2CLI.Value <> '') (*tel*) then
          // Fornecedor 2
          IncluiContato(Codigo, FPrincipalFor, TbCadCliFO2CLI.Value,
            TbCadCliFF2CLI.Value, '', '', '');

        // R e f e r e n c i a s   b a n c � r i a s
        if (TbCadCliBC1CLI.Value <> '') (*Txt*)
        or (TbCadCliNB1CLI.Value <> '') (*Bco*)
        or (TbCadCliNA1CLI.Value <> '') (*Ag*) then
        begin
          Ordem := 1;
          IncluiBanco(Codigo, Ordem,
            TbCadCliBC1CLI.Value, TbCadCliNB1CLI.Value, TbCadCliNA1CLI.Value);
        end;
        if (TbCadCliBC2CLI.Value <> '') (*Txt*)
        or (TbCadCliNB2CLI.Value <> '') (*Bco*)
        or (TbCadCliNA2CLI.Value <> '') (*Ag*) then
        begin
          Ordem := 2;
          IncluiBanco(Codigo, Ordem,
            TbCadCliBC2CLI.Value, TbCadCliNB2CLI.Value, TbCadCliNA2CLI.Value);
        end;

        // EntiSrvPro > SPC
        if (TbCadCliSPCCLI.Value <> '') (*Txt*)
        or (TbCadCliNRCCLI.Value <> '') (*Num*)
        or (TbCadCliHORCLI.Value <> '') (*Hora*) then
        begin
          Txt := TbCadCliSPCCLI.Value + TbCadCliNRCCLI.Value;
          if Pos('SERASA', Txt) > 0 then
            Orgao := FETP_SERASA
          else
          if Pos('ACIM', Txt) > 0 then
            Orgao := FETP_SERASA
          else
            Orgao := FETP_SCPC;
          //
          IncluiConsulta(Codigo, Orgao, TbCadCliNRCCLI.Value, TbCadCliDSPCLI.Value,
            TbCadCliHORCLI.Value, TbCadCliSPCCLI.Value);
        end;
      end;
    end;
    TbCadCli.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadCtr();
  procedure IncluiLocFCab();
  var
    DtHrFat: String;
    Codigo, Controle, NumNF: Integer;
    ValLocad, ValConsu, ValUsado, ValFrete, ValDesco, ValTotal: Double;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    Controle       := Trunc(TbCadCtrNRCTR.Value);
    DtHrFat        := Geral.FDT(TbCadCtrDTBAIXA.Value, 1);
    ValLocad       := TbCadCtrVALUNI1.Value + TbCadCtrVALUNI2.Value;
    ValConsu       := 0;
    ValUsado       := 0;
    ValFrete       := TbCadCtrFRETE.Value;
    ValDesco       := 0;
    ValTotal       := TbCadCtrTOTPEDG.Value;
    NumNF          := Trunc(TbCadCtrNRNF.Value);
    //
    //UMyMod.BPGS1I32('locfcab', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stins, 'locfcab', False, [
    'Codigo', 'DtHrFat', 'ValLocad',
    'ValConsu', 'ValUsado', 'ValFrete',
    'ValDesco', 'ValTotal', 'NumNF'], [
    'Controle'], [
    Codigo, DtHrFat, ValLocad,
    ValConsu, ValUsado, ValFrete,
    ValDesco, ValTotal, NumNF], [
    Controle], True);
  end;

  procedure IncluiLctFatRef(Valor: Double);
  var
    Codigo, Controle, Conta: Integer;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    Controle       := Trunc(TbCadCtrNRCTR.Value);
    //Conta          := ;
    //Valor          := ;
    //
    Conta := UMyMod.BPGS1I32('lctfatref', 'Conta', '', '', tsPos, stIns, 0);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'lctfatref', False, [
    'Codigo', 'Controle', 'Valor'], [
    'Conta'], [
    Codigo, Controle, Valor], [
    Conta], True);
  end;

  //

  procedure IncluiLocCCon();
  const
    Empresa = -11;
  var
    DtHrEmi, DtHrBxa, LocalObra, Obs0, Obs1, Obs2: String;
    Codigo, Contrato, Cliente, Vendedor: Integer;
    ValorTot: Double;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    Contrato       := -999;
    Cliente        := DModG.ObtemCodigoDeEntidadePeloAntigo('C' + FormatFloat('00000', TbCadCtrCODCLIP.Value));
    DtHrEmi        := Geral.FDT(TbCadCtrDTEMI.Value, 1);
    DtHrBxa        := Geral.FDT(TbCadCtrDTBAIXA.Value, 1);
    Vendedor       := TbCadCtrVEND.Value;
    ValorTot       := TbCadCtrTOTPEDG.Value;
    LocalObra      := TbCadCtrENDOBRA.Value;
    Obs0           := TbCadCtrOBS0.Value;
    Obs1           := TbCadCtrOBS1.Value;
    Obs2           := TbCadCtrOBS2.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'locccon', False, [
    'Empresa',
    'Contrato', 'Cliente', 'DtHrEmi',
    'DtHrBxa', 'Vendedor', 'ValorTot',
    'LocalObra', 'Obs0', 'Obs1',
    'Obs2'], [
    'Codigo'], [
    Empresa,
    Contrato, Cliente, DtHrEmi,
    DtHrBxa, Vendedor, ValorTot,
    LocalObra, Obs0, Obs1,
    Obs2], [
    Codigo], True);
  end;

  //

  procedure IncluiLocCPatPri(const GraGruX: Integer; var CtrID: Integer);
    function LiberadoPor(Data: TDateTime): Integer;
    begin
      if Data > 2 then
        Result := -1
      else
        Result := 0;
    end;
  var
    DtHrLocado, DtHrRetorn, LibDtHr, RELIB, HOLIB: String;
    Codigo, LibFunci: Integer;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    //CtrID          := ;
    //GraGruX        := ObtemGraGruXDeCODMATX(TbCadCtrCODMAT1.Value);
    DtHrLocado     := Geral.FDT(TbCadCtrDTEMI.Value, 1);
    DtHrRetorn     := Geral.FDT(TbCadCtrDTBAIXA.Value, 1);
    LibFunci       := LiberadoPor(TbCadCtrDTBAIXA.Value);
    LibDtHr        := Geral.FDT(TbCadCtrDTLIB.Value, 1);
    RELIB          := TbCadCtrRELIB.Value;
    HOLIB          := TbCadCtrHOLIB.Value;
    //
    CtrID := UMyMod.BPGS1I32('loccpatpri', 'CtrID', '', '', tsPos, stIns, 0);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatpri', False, [
    'Codigo', 'GraGruX', 'DtHrLocado',
    'DtHrRetorn', 'LibFunci', 'LibDtHr',
    'RELIB', 'HOLIB'], [
    'CtrID'], [
    Codigo, GraGruX, DtHrLocado,
    DtHrRetorn, LibFunci, LibDtHr,
    RELIB, HOLIB], [
    CtrID], True);
  end;

  //

var
  GraGruX, CtrID: Integer;
  CodX: String;
begin
  ReabreTabela(TbCadCtr);
  TbCadCtr.First;
  while not TbCadCtr.Eof do
  begin
    try
      CodX := Geral.FF0(Trunc(TbCadCtrNRCTR.Value));
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + CodX);
      //
      if TbCadCtrNRCTR.Value <> 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
        'SELECT Codigo ID',
        'FROM locccon ',
        'WHERE Codigo=' + CodX,
        '']);
        if QrExiste.RecordCount = 0 then
        begin
          IncluiLocCCon();
          //

          GraGruX := ObtemGraGruXDeCODMATX(TbCadCtrCODMAT1.Value);
          if GraGruX <> 0 then
          begin
            IncluiLocCPatPri(GraGrux, CtrID);
            if (TbCadCtrDTBAIXA.Value > 1) and (TbCadCtrVALUNI1.Value >= 0.01) then
              IncluiLctFatRef(TbCadCtrVALUNI1.Value);
          end;
          //
          GraGruX := ObtemGraGruXDeCODMATX(TbCadCtrCODMAT2.Value);
          if GraGruX <> 0 then
          begin
            IncluiLocCPatPri(GraGrux, CtrID);
            if (TbCadCtrDTBAIXA.Value > 1) and (TbCadCtrVALUNI2.Value >= 0.01) then
              IncluiLctFatRef(TbCadCtrVALUNI2.Value);
          end;
          //
          if (TbCadCtrDTBAIXA.Value > 1) then
            IncluiLocFCab();
        end;
      end;
    except
      Memo2.Lines.Add('Erro no c�digo: ' + Geral.FFT(TbCadCtrNRCTR.Value, 0, siPositivo));
    end;
    TbCadCtr.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadFor();
const
  Mae = ''; NIRE = ''; FormaSociet = ''; Simples = ''; IEST = ''; Atividade = '';
  CPF_Pai = ''; CPF_Conjuge = ''; SSP = ''; DataRG = ''; CidadeNatal = '';
  EstCivil = ''; UFNatal = ''; Nacionalid = ''; EPais = ''; Ete3 = ''; ECel = '';
  PPais = ''; Pte3 = ''; PCel = '';
  Respons1 = ''; Respons2 = ''; Pai = '';
  //
  Fornece1 = 'V';
var
  ENDFOR: String;
  EAtividad, EUF, ECEP,
  PAtividad, PUF, PCEP,
  Codigo,    //CUF, CCEP,
  //Ordem, Orgao,
  CodUsu, Tipo: Integer;
  //
  CNPJ, IE, RazaoSocial, Fantasia, ELograd, ERua, ENumero, ECompl, EBairro, ECidade, ENatal, ETe1, ETe2, EFax, EContato, EEmail,
  CPF,  RG, Nome,        Apelido,  PLograd, PRua, PNumero, PCompl, PBairro, PCidade, PNatal, PTe1, PTe2, PFax, PContato, PEMail,
  Antigo, xBairro, xLogr,
  //Numero, Compl, Bairro, Txt,
  Observacoes, Te1,
  Te2, Fax: String;
  //
  //LimiCred: Double;
  //ECOFOR, CLograd, CRua, CNumero, CCompl, CBairro, CCidade, Comprador, Email,
  //Respons1, Respons2, Pai, Nivel, CreditosU: String;
begin
(*
DELETE FROM entidades WHERE Codigo > 20000;
DELETE FROM enticontat;
DELETE FROM entimail;
DELETE FROM entirespon;
DELETE FROM entitel;
DELETE FROM entictas;
DELETE FROM entisrvpro;
*)
  ReabreTabela(TbCadFor);
  TbCadFor.First;
  while not TbCadFor.Eof do
  begin
    // N�o usa: TbCadForFATFOR
    Antigo := 'F' + TbCadForCODFOR.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Antigo);
    ReabrePsqEnt(Antigo);
    if QrPsqEnt.RecordCount = 0 then
    begin
      Codigo := Geral.IMV(TbCadForCODFOR.Value) + EdForCodIni.ValueVariant - 1;
      CodUsu := Codigo;
      ENDFOR := TbCadForENDFOR.Value;
      CNPJ := Geral.SoNumero_TT(TbCadForCGCFOR.Value);
      IE   := TbCadForINSFOR.Value;
      if Geral.SoNumero_TT(IE) = '' then
        IE := '';
      CPF  := Geral.SoNumero_TT(TbCadForCPFFOR.Value);
      RG   := TbCadForRGEFOR.Value;
      if Geral.SoNumero_TT(RG) = '' then
        RG := '';
      RazaoSocial := '';
      Fantasia := '';
      Nome := '';
      Apelido := '';
      EAtividad := 0; PAtividad := 0;
      ELograd := ''; ERua := ''; ENumero := ''; ECompl := '';
      PLograd := ''; PRua := ''; PNumero := ''; PCompl := '';
      EBairro := ''; PBairro := '';
      ECidade := ''; PCidade := '';
      EUF := 0; PUF := 0;
      ECEP := 0; PCEP := 0;
      ENatal := '0000-00-00';
      ETe1 := ''; ETe2 := ''; EFax := '';
      PTe1 := ''; PTe2 := ''; PFax := '';
      PNatal := '0000-00-00';
      PContato := ''; EContato := '';
      EEmail := ''; PEmail := '';
      //
      //Comprador := Trim(TbCadForCOMFOR.Value);
      Te1 := Geral.SoNumero_TT(TbCadForTE1FOR.Value);
      Te2 := Geral.SoNumero_TT(TbCadForTE2FOR.Value);
      Fax := Geral.SoNumero_TT(TbCadForFAXFOR.Value);
      //Email := Trim(Lowercase(TbCadForEMAFOR.Value));
      //
      PNumero := '';
      PCompl := '';
      xBairro := '';
      xLogr := '';
      PLograd := '';
      PRua := '';
      //
      if Length(CNPJ) >= 14 then
      begin
        Tipo := 0;
        Geral.SeparaEndereco(
          0, ENDFOR, ENumero, ECompl, xBairro, xLogr, ELograd, ERua,
          False, Memo1);
        //
        RazaoSocial := TbCadForRAZFOR.Value;
        Fantasia := TbCadForFANFOR.Value;
        EAtividad := Geral.IMV(TbCadForRAMFOR.Value);
        EBairro := TbCadForBAIFOR.Value;
        ECidade := TbCadForCIDFOR.Value;
        EUF := Geral.GetCodigoUF_da_SiglaUF(TbCadForESTFOR.Value);
        ECEP := Geral.IMV(Geral.SoNumero_TT(TbCadForCEPFOR.Value));
        //ENatal := Geral.FDT(TbCadForDTNFOR.Value, 1);
        //
        ETe1 := Te1;
        ETe2 := Te2;
        EFax := Fax;
        //EContato := Comprador;
        //EEmail := Email;
      end else
      begin
        Tipo := 1;
        Geral.SeparaEndereco(
        0, ENDFOR, PNumero, PCompl, xBairro, xLogr, PLograd, PRua,
        False, Memo1);
        //
        Nome := TbCadForRAZFOR.Value;
        Apelido := TbCadForFANFOR.Value;
        PAtividad := Geral.IMV(TbCadForRAMFOR.Value);
        PBairro := TbCadForBAIFOR.Value;
        PCidade := TbCadForCIDFOR.Value;
        PUF := Geral.GetCodigoUF_da_SiglaUF(TbCadForESTFOR.Value);
        PCEP := Geral.IMV(Geral.SoNumero_TT(TbCadForCEPFOR.Value));
        //PNatal := Geral.FDT(TbCadForDTNFOR.Value, 1);
        //
        PTe1 := Te1;
        PTe2 := Te2;
        PFax := Fax;
        //PContato := Comprador;
        //PEmail := Email;
      end;
      (*
      ECOFOR := TbCadForECOFOR.Value;
      Geral.SeparaEndereco(
        0, ENDFOR, CNumero, CCompl, xBairro, xLogr, CLograd, CRua,
        False, Memo1);
      CBairro := TbCadForBCOFOR.Value;
      CCidade := TbCadForCIOFOR.Value;
      CUF := Geral.GetCodigoUF_da_SiglaUF(TbCadForESOFOR.Value);
      CCEP := Geral.IMV(Geral.SoNumero_TT(TbCadForCEOFOR.Value));
      //
      Pai := TbCadForFILFOR.Value;
      Nivel := TbCadForLIBFOR.Value;
      CreditosU :=  Geral.FDT(TbCadForDTUFOR.Value, 1);
      LimiCred := TbCadForLIMFOR.Value;
      //
      Respons1 := TbCadForSO1FOR.Value;
      Respons2 := TbCadForSO2FOR.Value;
      *)
      try
        Observacoes := TbCadForOBSFOR.Value;
      except
        Observacoes := '';
      end;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
      'CodUsu', 'RazaoSocial', 'Fantasia',
      'Respons1', 'Respons2', 'Pai',
      'Mae', 'CNPJ', 'IE',
      'NIRE', 'FormaSociet', 'Simples',
      'IEST', 'Atividade', 'Nome',
      'Apelido', 'CPF', 'CPF_Pai',
      'CPF_Conjuge', 'RG', 'SSP',
      'DataRG', 'CidadeNatal', 'EstCivil',
      'UFNatal', 'Nacionalid', 'ELograd',
      'ERua', 'ENumero', 'ECompl',
      'EBairro', 'ECidade', 'EUF',
      'ECEP', 'EPais', 'ETe1',
      'Ete2', 'Ete3', 'ECel',
      'EFax', 'EEmail', 'EContato',
      'ENatal', 'PLograd', 'PRua',
      'PNumero', 'PCompl', 'PBairro',
      'PCidade', 'PUF', 'PCEP',
      'PPais', 'PTe1', 'Pte2',
      'Pte3', 'PCel', 'PFax',
      'PEmail', 'PContato', 'PNatal',
      (*'Sexo', 'Responsavel', 'Profissao',
      'Cargo', 'Recibo', 'DiaRecibo',
      'AjudaEmpV', 'AjudaEmpP', 'Cliente1',
      'Cliente2', 'Cliente3', 'Cliente4',*)
      'Fornece1', (*'Fornece2', 'Fornece3',
      'Fornece4', 'Fornece5', 'Fornece6',
      'Fornece7', 'Fornece8', 'Terceiro',
      'Cadastro', 'Informacoes', 'Logo',
      'Veiculo', 'Mensal',*) 'Observacoes',
      'Tipo', (*'CLograd', 'CRua',
      'CNumero', 'CCompl', 'CBairro',
      'CCidade', 'CUF', 'CCEP',
      'CPais', 'CTel', 'CCel',
      'CFax', 'CContato', 'LLograd',
      'LRua', 'LNumero', 'LCompl',
      'LBairro', 'LCidade', 'LUF',
      'LCEP', 'LPais', 'LTel',
      'LCel', 'LFax', 'LContato',
      'Comissao', 'Situacao', 'Nivel',
      'Grupo', 'Account', 'Logo2',
      'ConjugeNome', 'ConjugeNatal', 'Nome1',
      'Natal1', 'Nome2', 'Natal2',
      'Nome3', 'Natal3', 'Nome4',
      'Natal4', 'CreditosI', 'CreditosL',
      'CreditosF2', 'CreditosD', 'CreditosU',
      'CreditosV', 'Motivo', 'QuantI1',
      'QuantI2', 'QuantI3', 'QuantI4',
      'QuantN1', 'QuantN2', 'QuantN3',
      'Agenda', 'SenhaQuer', 'Senha1',
      'LimiCred', 'Desco', 'CasasApliDesco',
      'TempA', 'TempD', 'Banco',
      'Agencia', 'ContaCorrente', 'FatorCompra',
      'AdValorem', 'DMaisC', 'DMaisD',
      'Empresa', 'CBE', 'SCB',*)
      'PAtividad', 'EAtividad', (*'PCidadeCod',
      'ECidadeCod', 'PPaisCod', 'EPaisCod',*)
      'Antigo' (*, 'CUF2', 'Contab',
      'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
      'Protestar', 'MultaCodi', 'MultaDias',
      'MultaValr', 'MultaPerc', 'MultaTiVe',
      'JuroSacado', 'CPMF', 'Corrido',
      'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
      'CartPref', 'RolComis', 'Filial',
      'EEndeRef', 'PEndeRef', 'CEndeRef',
      'LEndeRef', 'ECodMunici', 'PCodMunici',
      'CCodMunici', 'LCodMunici', 'CNAE',
      'SUFRAMA', 'ECodiPais', 'PCodiPais',
      'L_CNPJ', 'L_Ativo', 'CNAE20',
      'URL', 'CRT', 'COD_PART'*)], [
      'Codigo'], [
      CodUsu, RazaoSocial, Fantasia,
      Respons1, Respons2, Pai,
      Mae, CNPJ, IE,
      NIRE, FormaSociet, Simples,
      IEST, Atividade, Nome,
      Apelido, CPF, CPF_Pai,
      CPF_Conjuge, RG, SSP,
      DataRG, CidadeNatal, EstCivil,
      UFNatal, Nacionalid, ELograd,
      ERua, ENumero, ECompl,
      EBairro, ECidade, EUF,
      ECEP, EPais, ETe1,
      Ete2, Ete3, ECel,
      EFax, EEmail, EContato,
      ENatal, PLograd, PRua,
      PNumero, PCompl, PBairro,
      PCidade, PUF, PCEP,
      PPais, PTe1, Pte2,
      Pte3, PCel, PFax,
      PEmail, PContato, PNatal,
      (*Sexo, Responsavel, Profissao,
      Cargo, Recibo, DiaRecibo,
      AjudaEmpV, AjudaEmpP, Cliente1,
      Cliente2, Cliente3, Cliente4,*)
      Fornece1, (*Fornece2, Fornece3,
      Fornece4, Fornece5, Fornece6,
      Fornece7, Fornece8, Terceiro,
      Cadastro, Informacoes, Logo,
      Veiculo, Mensal,*) Observacoes,
      Tipo, (*CLograd, CRua,
      CNumero, CCompl, CBairro,
      CCidade, CUF, CCEP,
      CPais, CTel, CCel,
      CFax, CContato, LLograd,
      LRua, LNumero, LCompl,
      LBairro, LCidade, LUF,
      LCEP, LPais, LTel,
      LCel, LFax, LContato,
      Comissao, Situacao, Nivel,
      Grupo, Account, Logo2,
      ConjugeNome, ConjugeNatal, Nome1,
      Natal1, Nome2, Natal2,
      Nome3, Natal3, Nome4,
      Natal4, CreditosI, CreditosL,
      CreditosF2, CreditosD, CreditosU,
      CreditosV, Motivo, QuantI1,
      QuantI2, QuantI3, QuantI4,
      QuantN1, QuantN2, QuantN3,
      Agenda, SenhaQuer, Senha1,
      LimiCred, Desco, CasasApliDesco,
      TempA, TempD, Banco,
      Agencia, ContaCorrente, FatorCompra,
      AdValorem, DMaisC, DMaisD,
      Empresa, CBE, SCB,*)
      PAtividad, EAtividad, (*PCidadeCod,
      ECidadeCod, PPaisCod, EPaisCod,*)
      Antigo (*, CUF2, Contab,
      MSN1, PastaTxtFTP, PastaPwdFTP,
      Protestar, MultaCodi, MultaDias,
      MultaValr, MultaPerc, MultaTiVe,
      JuroSacado, CPMF, Corrido,
      CPF_Resp1, CliInt, AltDtPlaCt,
      CartPref, RolComis, Filial,
      EEndeRef, PEndeRef, CEndeRef,
      LEndeRef, ECodMunici, PCodMunici,
      CCodMunici, LCodMunici, CNAE,
      SUFRAMA, ECodiPais, PCodiPais,
      L_CNPJ, L_Ativo, CNAE20,
      URL, CRT, COD_PART*)], [
      Codigo], True) then
      begin
2470        // N�o h� outras inclus�es!
      end;
    end;
    TbCadFor.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadGru;
var
  //Teste,
  CodsX, Cod, CodUsu, Nivel2, Nivel3, Nivel4, Nivel5, Nivel6: String;
  //I,
  Itens: Integer;
  ArrNiveis: array of String;
  Nome: String;
begin
  ReabreTabela(TbCadGru);
  TbCadGru.First;
  while not TbCadGru.Eof do
  begin
    Itens := 0;
    CodsX := TbCadGruCODGRU.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + CodsX);
    while CodsX <> '' do
    begin
      Itens := Itens + 1;
      SetLength(ArrNiveis, Itens);
      if Geral.SeparaPrimeiraOcorrenciaDeTexto('.', CodsX, Cod, CodsX) then
        ArrNiveis[Itens-1] := Cod
      else
      begin
        Geral.MensagemBox('Erro fatal na importa��o de grupos de grade!' +
        #13#10 + 'O aplicativo ser� finalizado!', 'ERRO', MB_OK+MB_ICONERROR);
        //
        Halt(0);
      end;
    end;
    Nivel2 := '0';
    Nivel3 := '0';
    Nivel4 := '0';
    Nivel5 := '0';
    //
    Nome := TbCadGruDESGRU.Value;
    case Itens of
      1:
      begin
        Nivel6 := '0';
        Nivel5 := ArrNiveis[0];
        CodUsu := Nivel5;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru5', False, [
        'CodUsu', 'Nome', 'Nivel6'], ['Nivel5'], ['Ativo'], [
        CodUsu, Nome, Nivel6], [Nivel5], [1], True);
      end;
      2:
      begin
        Nivel5         := ArrNiveis[0];
        Nivel4         := ArrNiveis[1];
        CodUsu         := Nivel4;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru4', False, [
        'CodUsu', 'Nome', 'Nivel5'], ['Nivel4'], ['Ativo'], [
        CodUsu, Nome, Nivel5], [Nivel4], [1], True);
      end;
      3:
      begin
        Nivel5         := ArrNiveis[0];
        Nivel4         := ArrNiveis[1];
        Nivel3         := ArrNiveis[2];
        CodUsu         := Nivel3;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru3', False, [
        'CodUsu', 'Nome', 'Nivel5', 'Nivel4'], ['Nivel3'], ['Ativo'], [
        CodUsu, Nome, Nivel5, Nivel4], [Nivel3], [1], True);
      end;
      4:
      begin
        Nivel5         := ArrNiveis[0];
        Nivel4         := ArrNiveis[1];
        Nivel3         := ArrNiveis[2];
        Nivel2         := ArrNiveis[3];
        CodUsu         := Nivel2;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru2', False, [
        'CodUsu', 'Nome', 'Nivel5', 'Nivel4', 'Nivel3'], [
        'Nivel2'], ['Ativo'], [
        CodUsu, Nome, Nivel5, Nivel4, Nivel3], [
        Nivel2], [1], True);
      end;
      else Geral.MensagemBox('Item de grupo de patrim�nio n�o importado: ' +
      #13#10 + TbCadGruCODGRU.Value + #13#10 + Nome,
      'Aviso', MB_OK+MB_ICONERROR);
    end;
    //
    TbCadGru.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadNat();
var
  Nome, Nome2, Debito: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  ReabreTabela(TbCadNat);
  TbCadNat.First;
  while not TbCadNat.Eof do
  begin
    Codigo         := TbCadNatCODNAT.Value;
    Nome           := TbCadNatDESNAT.Value;
    Nome2          := Nome;
    Debito         := 'V';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Codigo ID',
    'FROM contas ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    if QrExiste.RecordCount = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Nome);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'contas', False, [
    'Nome', 'Nome2', 'Debito'], [
    'Codigo'], [
    Nome, Nome2, Debito], [
    Codigo], True);
    //
    TbCadNat.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadPat();
  function MarcaCadastrada(Texto: String): Integer;
  const
    Codigo = 0;
  var
    Nome: String;
    Controle: Integer;
  begin
    if Trim(Texto) <> '' then
    begin
      Nome := Texto;
      if Nome = 'DINAPAC' then
        Nome := 'DYNAPAC'
      else
      if Nome = 'ERBELE' then
        Nome := 'EBERLE'
      else
      if Nome = 'JOWA/WACKE' then
        Nome := 'JOWA'
      else
      if Nome = 'KOLBACK' then
        Nome := 'KOLBACH'
      else
      if Nome = 'ROBIN' then
        Nome := 'ROBBIN'
      else
      if Nome = 'VIBROMAK' then
        Nome := 'VIBROMACK'
      else
      if Nome = 'WEGG' then
        Nome := 'WEG'
      ;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraFabMar, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM grafabmar ',
      'WHERE Nome="' + Nome + '"',
      '']);
      //
      Result := QrGraFabMarControle.Value;
      if Result = 0 then
      begin
        Controle := UMyMod.BuscaEmLivreY_Def('grafabmar', 'Controle', stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'grafabmar', False, [
        'Codigo', 'Nome'], [
        'Controle'], [
        Codigo, Nome], [
        Controle], True) then
          Result := Controle;
      end;
    end else Result := 0;
  end;
var
  Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia,
  Capacid, VendaData, VendaDocu, Observa, AGRPAT, CLVPAT: String;
  GraGruX, Nivel1, Situacao, Agrupador, VendaEnti: Integer;
  AquisValr, AtualValr, ValorMes, ValorQui, ValorSem, ValorDia, VendaValr: Double;
  Marca: Integer;
  //
  //GraGru1
  Cod, CodsX, Referencia, MARPAT: String;
  ArrNiveis: array of String;
  Nivel2, Nivel3, Nivel4, Nivel5,
  I, Itens, PrdGrupTip, CodUsu: Integer;
begin
  ReabreTabela(TbCadPat);
  SetLength(ArrNiveis, 4);
  TbCadPat.First;
  while not TbCadPat.Eof do
  begin
    Referencia := TbCadPatCODPAT.Value;
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Referencia);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Nivel1 ID',
    'FROM gragru1 ',
    'WHERE Referencia=''' + Referencia + '''',
    '']);
    // Caso n�o tenha sido inserido ainda!
    if QrExiste.RecordCount = 0 then
    begin
      for I := 0 to 3 do
        ArrNiveis[I] := '';
      Itens := 0;
      CodsX := TbCadPatGRUPAT.Value;
      while CodsX <> '' do
      begin
        Itens := Itens + 1;
        if Geral.SeparaPrimeiraOcorrenciaDeTexto('.', CodsX, Cod, CodsX) then
          ArrNiveis[Itens-1] := Cod
        else
        begin
          Geral.MensagemBox('Erro fatal na importa��o de grupos de grade!' +
          #13#10 + 'O aplicativo ser� finalizado!', 'ERRO', MB_OK+MB_ICONERROR);
          //
          Halt(0);
        end;
      end;
      Nivel5 := Geral.IMV(ArrNiveis[0]);
      Nivel4 := Geral.IMV(ArrNiveis[1]);
      Nivel3 := Geral.IMV(ArrNiveis[2]);
      Nivel2 := Geral.IMV(ArrNiveis[3]);
      //
      Nivel1 := UMyMod.BuscaEmLivreY_Def('gragru1', 'Nivel1', stIns, 0);
      CodUsu := Nivel1;
      PrdGrupTip := 1;
      GraGruX := Nivel1;
      //
      CadastraGraGru1SemGrade(PrdGrupTip, Nivel5, Nivel4, Nivel3,
      Nivel2, Nivel1, CodUsu, TbCadPatNOMPAT.Value, TbCadPatCODPAT.Value);
      //

      MARPAT         := TbCadPatMARPAT.Value;

      Complem        := TbCadPatCOMPAT.Value;
      AquisData      := Geral.FDT(TbCadPatDATPAT.Value, 1);
      AquisDocu      := TbCadPatDOCPAT.Value;
      AquisValr      := TbCadPatVLRPAT.Value;
      Situacao       := Dmod.CodigoDeSitPat(TbCadPatSITPAT.Value);
      AtualValr      := TbCadPatVLEPAT.Value;
      ValorMes       := TbCadPatVLMPAT.Value;
      ValorQui       := TbCadPatVLQPAT.Value;
      ValorSem       := TbCadPatVLSPAT.Value;
      ValorDia       := TbCadPatVLDPAT.Value;
      Agrupador      := 0; // Fazer depois!
      Marca          := MarcaCadastrada(MARPAT);
      Modelo         := TbCadPatMODPAT.Value;
      Serie          := TbCadPatSERPAT.Value;
      Voltagem       := TbCadPatENEPAT.Value;
      Potencia       := TbCadPatPOTPAT.Value;
      Capacid        := ''; // N�o tem
      VendaData      := Geral.FDT(TbCadPatDTVPAT.Value, 1);
      VendaDocu      := TbCadPatNFVPAT.Value;
      VendaValr      := TbCadPatVLVPAT.Value;
      VendaEnti      := DModG.ObtemCodigoDeEntidadePeloAntigo('C' + TbCadPatCLVPAT.Value);  // Cliente!
      Observa        := TbCadPatMOMPAT.Value;
      AGRPAT         := TbCadPatAGRPAT.Value;
      CLVPAT         := TbCadPatCLVPAT.Value;

      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragxpatr', False, [
      'GraGruX', 'Complem', 'AquisData',
      'AquisDocu', 'AquisValr', 'Situacao',
      'AtualValr', 'ValorMes', 'ValorQui',
      'ValorSem', 'ValorDia', 'Agrupador',
      'Marca', 'Modelo', 'Serie',
      'Voltagem', 'Potencia', 'Capacid',
      'VendaData', 'VendaDocu', 'VendaValr',
      'VendaEnti', 'Observa',
      'AGRPAT', 'CLVPAT', 'MARPAT'], [
      ], [
      GraGruX, Complem, AquisData,
      AquisDocu, AquisValr, Situacao,
      AtualValr, ValorMes, ValorQui,
      ValorSem, ValorDia, Agrupador,
      Marca, Modelo, Serie,
      Voltagem, Potencia, Capacid,
      VendaData, VendaDocu, VendaValr,
      VendaEnti, Observa,
      AGRPAT, CLVPAT, MARPAT], [
      ], True);
    end;
    //
    TbCadPat.Next;
  end;
  TbCadPat.First;
  PB1.Position := 0;
  QrGraGXPatr.Close;
  UMyMod.AbreQuery(QrGraGXPatr, Dmod.MyDB);
  while not QrGraGXPatr.Eof do
  begin
    GraGruX := QrGraGXPatrGraGRuX.Value;
    Referencia := QrGraGXPatrAGRPAT.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Geral.FF0(GraGruX));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Nivel1 ID',
    'FROM gragru1 ',
    'WHERE Referencia=''' + Referencia + '''',
    '']);
    // N�o testei Ainda!
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Controle ID',
    'FROM gragrux ',
    'WHERE GraGru1=' + Geral.FF0(QrExiste.FieldByName('ID').AsInteger),
    '']);
    Agrupador := QrExiste.FieldByName('ID').AsInteger;
    //
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragxpatr', False, [
    'Agrupador'], ['GraGruX'], [Agrupador], [GraGruX], True);
    //
    QrGraGXPatr.Next;
  end;

end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadVde();
var
  //Senha, DataSenha, SenhaDia, IP_Default,
  Login: String;
  //Perfil, Funcionario, SitSenha,
  Numero: Integer;
begin
  ReabreTabela(TbCadVde);
  TbCadVde.First;
  while not TbCadVde.Eof do
  begin
    Login := TbCadVdeNOMVDE.Value;
    Numero := Trunc(TbCadVdeCODVDE.Value);
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Login);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'senhas', False, [
    'Numero'], ['Login'], [Numero], [Login], True);
    //
    TbCadVde.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_PagDup();
var
  Fornecedor: Integer;
begin
  ReabreTabela(TbPagDup);
  TbPagDup.First;
  while not TbPagDup.Eof do
  begin
    Fornecedor := DModG.ObtemCodigoDeEntidadePeloAntigo('F' +
      FormatFloat('00000', Geral.IMV(TbPagDupFORPAG.Value)));
    //
    //
    UFinanceiro.LancamentoDefaultVARS;
    //
    FLAN_Data          := Geral.FDT(TbPagDupDTEPAG.Value, 1);
    FLAN_Vencimento    := Geral.FDT(TbPagDupDTVPAG.Value, 1);
    FLAN_DataCad       := Geral.FDT(date, 1);
    FLAN_Mez           := 0;
    FLAN_Descricao     := TbPagDupOBSPAG.Value;
    FLAN_Compensado    := Geral.FDT(TbPagDupDTPPAG.Value, 1);
    FLAN_Duplicata     := TbPagDupTITPAG.Value;
    FLAN_Doc2          := TbPagDupNOCPAG.Value;
    FLAN_SerieCH       := Geral.SoLetra_TT(TbPagDupNCHPAG.Value);

    FLAN_Documento     := Geral.IMV('0' + Geral.SoNumero_TT(TbPagDupNCHPAG.Value));
    FLAN_Tipo          := 2;
    FLAN_Credito       := 0;
    FLAN_Debito        := TbPagDupVLRPAG.Value;
    FLAN_Genero        := TbPagDupNATPAG.Value;
    FLAN_SerieNF       := Geral.SoLetra_TT(TbPagDupNNFPAG.Value);
    FLAN_Antigo        := TbPagDupCODPAG.Value;
    FLAN_NotaFiscal    := Geral.IMV('0' + Geral.SoNumero_TT(TbPagDupNNFPAG.Value));
    if TbPagDupDTPPAG.Value > 2 then
    begin
      FLAN_Sit           := 3;
      FLAN_Carteira      := 2; // Banco
    end else
    begin
      FLAN_Sit           := 0;
      FLAN_Carteira      := 3; // Duplicatas
    end;
    FLAN_Controle      := 0;
    FLAN_Sub           := 0;
    FLAN_ID_Pgto       := 0;
    FLAN_Cartao        := 0;
    FLAN_Linha         := 0;
    FLAN_Fornecedor    := Fornecedor;
    FLAN_Cliente       := 0;
    FLAN_MoraDia       := 0;
    FLAN_Multa         := 0;
    FLAN_UserCad       := VAR_USUARIO;
    FLAN_DataDoc       := Geral.FDT(Date, 1);
    FLAN_Vendedor      := 0;
    FLAN_Account       := 0;
    FLAN_ICMS_P        := 0;
    FLAN_ICMS_V        := 0;
    FLAN_CliInt        := -11;
    FLAN_Depto         := 0;
    FLAN_DescoPor      := 0;
    FLAN_ForneceI      := 0;
    FLAN_DescoVal      := 0;
    FLAN_NFVal         := 0;
    FLAN_Unidade       := 0;
    FLAN_Qtde          := 0;
    FLAN_FatID         := 0;
    FLAN_FatID_Sub     := 0;
    FLAN_FatNum        := 0;
    FLAN_FatParcela    := 0;
    FLAN_FatParcRef    := 0;
    FLAN_FatGrupo      := 0;
    //
    FLAN_MultaVal      := 0;
    FLAN_TaxasVal      := 0;
    FLAN_MoraVal       := TbPagDupJURPAG.Value;
    FLAN_CtrlIni       := 0;
    FLAN_Nivel         := 0;
    FLAN_CNAB_Sit      := 0;
    FLAN_TipoCH        := 0;
    FLAN_Atrelado      := 0;
    FLAN_SubPgto1      := 0;
    FLAN_MultiPgto     := 0;
    FLAN_EventosCad    := 0;
    FLAN_IndiPag       := 0;
    FLAN_FisicoSrc     := 0;
    FLAN_FisicoCod     := 0;
    //
    FLAN_Emitente      := '';
    FLAN_CNPJCPF       := '';
    FLAN_Banco         := 0;
    FLAN_Agencia       := 0;
    FLAN_ContaCorrente := '';
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Geral.FF0(TbPagDup.RecNo));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Controle ID',
    'FROM ' + FTabLctA,
    'WHERE Data=''' + FLAN_Data + '''',
    'AND Vencimento=''' + FLAN_Vencimento + '''',
    'AND Duplicata=''' + Geral.Substitui(dmkPF.DuplicaBarras(FLAN_Duplicata), '''', ' ') + '''',
    'AND Fornecedor=' + Geral.FF0(FLAN_Fornecedor),
    'AND Debito=' + Geral.FFT_Dot(FLAN_Debito, 2, siNegativo),
    '']);
    if QrExiste.RecordCount = 0 then
    begin
      FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', FTabLctA, LAN_CTOS, 'Controle');
      //
      UFinanceiro.InsereLancamento(FTabLctA);
    end;
    //
    TbPagDup.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_RecDup();
var
  Cliente: Integer;
begin
  ReabreTabela(TbRecDup);
  TbRecDup.First;
  while not TbRecDup.Eof do
  begin
    Cliente := DModG.ObtemCodigoDeEntidadePeloAntigo('C' +
      FormatFloat('00000', Geral.IMV(TbRecDupCLIREC.Value)));
    //
    //
    UFinanceiro.LancamentoDefaultVARS;
    //
    FLAN_Data          := Geral.FDT(TbRecDupDTEREC.Value, 1);
    FLAN_Vencimento    := Geral.FDT(TbRecDupDTVREC.Value, 1);
    FLAN_DataCad       := Geral.FDT(date, 1);
    FLAN_Mez           := 0;
    FLAN_Descricao     := TbRecDupOBSREC.Value;
    FLAN_Compensado    := Geral.FDT(TbRecDupDTPREC.Value, 1);
    FLAN_Duplicata     := TbRecDupTITREC.Value;
    FLAN_Doc2          := TbRecDupNRPREC.Value;
    FLAN_SerieCH       := '';

    FLAN_Documento     := 0;//Geral.IMV('0' + Geral.SoNumero_TT(TbRecDupNCHREC.Value));
    FLAN_Tipo          := 2;
    //FLAN_Carteira      := TbRecDupBCOREC.Value;
    FLAN_Credito       := TbRecDupVLRREC.Value;
    FLAN_Debito        := 0;
    FLAN_Genero        := 100;
    FLAN_SerieNF       := Geral.SoLetra_TT(TbRecDupNNFREC.Value);
    FLAN_Antigo        := TbRecDupCODREC.Value;
    FLAN_NotaFiscal    := Geral.IMV('0' + Geral.SoNumero_TT(TbRecDupNNFREC.Value));
    if TbRecDupDTPREC.Value > 2 then
    begin
      FLAN_Sit           := 3;
      FLAN_Carteira      := 2; // Banco
    end else
    begin
      FLAN_Sit           := 0;
      FLAN_Carteira      := 4; // Faturas
    end;
    FLAN_Controle      := 0;
    FLAN_Sub           := 0;
    FLAN_ID_Pgto       := 0;
    FLAN_Cartao        := 0;
    FLAN_Linha         := 0;
    FLAN_Fornecedor    := 0;
    FLAN_Cliente       := Cliente;
    FLAN_MoraDia       := 0;
    FLAN_Multa         := 0;
    FLAN_UserCad       := VAR_USUARIO;
    FLAN_DataDoc       := Geral.FDT(Date, 1);
    FLAN_Vendedor      := 0;
    FLAN_Account       := 0;
    FLAN_ICMS_P        := 0;
    FLAN_ICMS_V        := 0;
    FLAN_CliInt        := -11;
    FLAN_Depto         := 0;
    FLAN_DescoPor      := 0;
    FLAN_ForneceI      := 0;
    FLAN_DescoVal      := 0;
    FLAN_NFVal         := 0;
    FLAN_Unidade       := 0;
    FLAN_Qtde          := 0;
    FLAN_FatID         := 0;
    FLAN_FatID_Sub     := 0;
    FLAN_FatNum        := 0;
    FLAN_FatParcela    := 0;
    FLAN_FatParcRef    := 0;
    FLAN_FatGrupo      := 0;
    //
    FLAN_MultaVal      := 0;
    FLAN_TaxasVal      := 0;
    FLAN_MoraVal       := TbRecDupJURREC.Value;
    FLAN_CtrlIni       := 0;
    FLAN_Nivel         := 0;
    FLAN_CNAB_Sit      := 0;
    FLAN_TipoCH        := 0;
    FLAN_Atrelado      := 0;
    FLAN_SubPgto1      := 0;
    FLAN_MultiPgto     := 0;
    FLAN_EventosCad    := 0;
    FLAN_IndiPag       := 0;
    FLAN_FisicoSrc     := 0;
    FLAN_FisicoCod     := 0;
    //
    FLAN_Emitente      := '';
    FLAN_CNPJCPF       := '';
    FLAN_Banco         := 0;
    FLAN_Agencia       := 0;
    FLAN_ContaCorrente := '';
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Geral.FF0(TbRecDup.RecNo));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Controle ID',
    'FROM ' + FTabLctA,
    'WHERE Data=''' + FLAN_Data + '''',
    'AND Vencimento=''' + FLAN_Vencimento + '''',
    'AND Duplicata=''' + Geral.Substitui(dmkPF.DuplicaBarras(FLAN_Duplicata), '''', ' ') + '''',
    'AND Cliente=' + Geral.FF0(FLAN_Cliente),
    'AND Credito=' + Geral.FFT_Dot(FLAN_Credito, 2, siNegativo),
    '']);
    if QrExiste.RecordCount = 0 then
    begin
      FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', FTabLctA, LAN_CTOS, 'Controle');
      //
      UFinanceiro.InsereLancamento(FTabLctA);
    end;
    //
    TbRecDup.Next;
  end;
end;
}
procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_AnexoDocumento;
var
  Tipo, Descricao, Nome, TipoArquivo, DataAtualizacao: String;
  Chave, Sequencia: Double;
  SQLType: TSQLType;
begin
{
  Fazer!
  SQLType := ImgTipo.SQLType?;
  Tipo           := ;
  Chave          := ;
  Sequencia      := ;
  Descricao      := ;
  Nome           := ;
  TipoArquivo    := ;
  DataAtualizacao:= ;

  //
? := UMyMod.BuscaEmLivreY_Def('docspath', 'Tipo', 'Chave', 'Sequencia', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('docspath', 'Tipo', 'Chave', 'Sequencia', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'docspath', auto_increment?[
'Descricao', 'Nome', 'TipoArquivo',
'DataAtualizacao'], [
'Tipo', 'Chave', 'Sequencia'], [
Descricao, Nome, TipoArquivo,
DataAtualizacao], [
Tipo, Chave, Sequencia], UserDataAlterweb?, IGNORE?
}
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Cliente;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;

const
  OriTab = 'Cliente';
  DstTab = 'entidades';

var
  RazaoSocial, Fantasia, Respons1, Respons2, Pai, Mae, CNPJ, IE, NIRE,
  FormaSociet, IEST, Atividade, Nome, Apelido, CPF, CPF_Pai, CPF_Conjuge, RG,
  SSP, DataRG, CidadeNatal, Nacionalid, ERua, ECompl, EBairro, ECidade, EPais,
  ETe1, Ete2, Ete3, ECel, EFax, EEmail, EContato, ENatal, PRua, PCompl, PBairro,
  PCidade, PPais, PTe1, Pte2, Pte3, PCel, PFax, PEmail, PContato, PNatal, Sexo,
  Responsavel, Profissao, Cargo, Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7,
  Fornece8, Terceiro, Cadastro, Informacoes, (*Logo,*) Mensal, Observacoes, CRua,
  CCompl, CBairro, CCidade, CPais, CTel, CCel, CFax, CContato, LRua, LCompl,
  LBairro, LCidade, LPais, LTel, LCel, LFax, LContato, Nivel, (*Logo2,*)
  ConjugeNome, ConjugeNatal, Nome1, Natal1, Nome2, Natal2, Nome3, Natal3, Nome4,
  Natal4, CreditosD, CreditosU, CreditosV, Agenda, SenhaQuer, Senha1, Agencia,
  ContaCorrente, Antigo, CUF2, Contab, MSN1, PastaTxtFTP, PastaPwdFTP, CPF_Resp1,
  AltDtPlaCt, EEndeRef, PEndeRef, CEndeRef, LEndeRef, CNAE, SUFRAMA, L_CNPJ, URL,
  COD_PART, ESite, PSite, EstrangNum, L_CPF, L_Nome, LEmail, L_IE, ListaStatus:
  String;

  Codigo, CodUsu, Simples, EstCivil, UFNatal, ELograd, ENumero, EUF, ECEP,
  PLograd, PNumero, PUF, PCEP, Recibo, DiaRecibo, Veiculo, Tipo, CLograd,
  CNumero, CUF, CCEP, LLograd, LNumero, LUF, LCEP, Situacao, Grupo, Account,
  CreditosI, CreditosL, Motivo, QuantI1, QuantI2, QuantI3, QuantI4,
  CasasApliDesco, Banco, DMaisC, DMaisD, Empresa, CBE, SCB, PAtividad,
  EAtividad, PCidadeCod, ECidadeCod, PPaisCod, EPaisCod, Protestar, MultaCodi,
  MultaDias, MultaTiVe, Corrido, CliInt, CartPref, RolComis, Filial, ECodMunici,
  PCodMunici, CCodMunici, LCodMunici, ECodiPais, PCodiPais, L_Ativo, CRT, ETe1Tip,
  ECelTip, PTe1Tip, PCelTip, EstrangDef, EstrangTip, indIEDest, LCodiPais:
  Integer;

  AjudaEmpV, AjudaEmpP, Comissao, CreditosF2, QuantN1, QuantN2, QuantN3,
  LimiCred, Desco, TempA, TempD, FatorCompra, AdValorem, MultaValr, MultaPerc,
  JuroSacado, CPMF: Double;

begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    '  RazaoSocial, Fantasia, Respons1, Respons2, Pai, Mae, CNPJ, IE, NIRE,',
    '  FormaSociet, IEST, Atividade, Nome, Apelido, CPF, CPF_Pai, CPF_Conjuge, RG,',
    '  SSP, DataRG, CidadeNatal, Nacionalid, ERua, ECompl, EBairro, ECidade, EPais,',
    '  ETe1, Ete2, Ete3, ECel, EFax, EEmail, EContato, ENatal, PRua, PCompl, PBairro,',
    '  PCidade, PPais, PTe1, Pte2, Pte3, PCel, PFax, PEmail, PContato, PNatal, Sexo,',
    '  Responsavel, Profissao, Cargo, Cliente1, Cliente2, Cliente3, Cliente4,',
    '  Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7,',
    '  Fornece8, Terceiro, Cadastro, Informacoes, Mensal, Observacoes, CRua,',
    '  CCompl, CBairro, CCidade, CPais, CTel, CCel, CFax, CContato, LRua, LCompl,',
    '  LBairro, LCidade, LPais, LTel, LCel, LFax, LContato, Nivel,',
    '  ConjugeNome, ConjugeNatal, Nome1, Natal1, Nome2, Natal2, Nome3, Natal3, Nome4,',
    '  Natal4, CreditosD, CreditosU, CreditosV, Agenda, SenhaQuer, Senha1, Agencia,',
    '  ContaCorrente, Antigo, CUF2, Contab, MSN1, PastaTxtFTP, PastaPwdFTP, CPF_Resp1,',
    '  AltDtPlaCt, EEndeRef, PEndeRef, CEndeRef, LEndeRef, CNAE, SUFRAMA, L_CNPJ, URL,',
    '  COD_PART, ESite, PSite, EstrangNum, L_CPF, L_Nome, LEmail, L_IE, ListaStatus,',
    '  Codigo, CodUsu, Simples, EstCivil, UFNatal, ELograd, ENumero, EUF, ECEP,',
    '  PLograd, PNumero, PUF, PCEP, Recibo, DiaRecibo, Veiculo, Tipo, CLograd,',
    '  CNumero, CUF, CCEP, LLograd, LNumero, LUF, LCEP, Situacao, Grupo, Account,',
    '  CreditosI, CreditosL, Motivo, QuantI1, QuantI2, QuantI3, QuantI4,',
    '  CasasApliDesco, Banco, DMaisC, DMaisD, Empresa, CBE, SCB, PAtividad,',
    '  EAtividad, PCidadeCod, ECidadeCod, PPaisCod, EPaisCod, Protestar, MultaCodi,',
    '  MultaDias, MultaTiVe, Corrido, CliInt, CartPref, RolComis, Filial, ECodMunici,',
    '  PCodMunici, CCodMunici, LCodMunici, ECodiPais, PCodiPais, L_Ativo, CRT, ETe1Tip,',
    '  ECelTip, PTe1Tip, PCelTip, EstrangDef, EstrangTip, indIEDest, LCodiPais, ',
    '  AjudaEmpV, AjudaEmpP, Comissao, CreditosF2, QuantN1, QuantN2, QuantN3,',
    '  LimiCred, Desco, TempA, TempD, FatorCompra, AdValorem, MultaValr, MultaPerc,',
    '  JuroSacado, CPMF',
{
  RazaoSocial, Fantasia, Respons1, Respons2, Pai, Mae, CNPJ, IE, NIRE,
  FormaSociet, IEST, Atividade, Nome, Apelido, CPF, CPF_Pai, CPF_Conjuge, RG,
  SSP, DataRG, CidadeNatal, Nacionalid, ERua, ECompl, EBairro, ECidade, EPais,
  ETe1, Ete2, Ete3, ECel, EFax, EEmail, EContato, ENatal, PRua, PCompl, PBairro,
  PCidade, PPais, PTe1, Pte2, Pte3, PCel, PFax, PEmail, PContato, PNatal, Sexo,
  Responsavel, Profissao, Cargo, Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7,
  Fornece8, Terceiro, Cadastro, Informacoes, Mensal, Observacoes, CRua,
  CCompl, CBairro, CCidade, CPais, CTel, CCel, CFax, CContato, LRua, LCompl,
  LBairro, LCidade, LPais, LTel, LCel, LFax, LContato, Nivel,
  ConjugeNome, ConjugeNatal, Nome1, Natal1, Nome2, Natal2, Nome3, Natal3, Nome4,
  Natal4, CreditosD, CreditosU, CreditosV, Agenda, SenhaQuer, Senha1, Agencia,
  ContaCorrente, Antigo, CUF2, Contab, MSN1, PastaTxtFTP, PastaPwdFTP, CPF_Resp1,
  AltDtPlaCt, EEndeRef, PEndeRef, CEndeRef, LEndeRef, CNAE, SUFRAMA, L_CNPJ, URL,
  COD_PART, ESite, PSite, EstrangNum, L_CPF, L_Nome, LEmail, L_IE, ListaStatus,
  Codigo, CodUsu, Simples, EstCivil, UFNatal, ELograd, ENumero, EUF, ECEP,
  PLograd, PNumero, PUF, PCEP, Recibo, DiaRecibo, Veiculo, Tipo, CLograd,
  CNumero, CUF, CCEP, LLograd, LNumero, LUF, LCEP, Situacao, Grupo, Account,
  CreditosI, CreditosL, Motivo, QuantI1, QuantI2, QuantI3, QuantI4,
  CasasApliDesco, Banco, DMaisC, DMaisD, Empresa, CBE, SCB, PAtividad,
  EAtividad, PCidadeCod, ECidadeCod, PPaisCod, EPaisCod, Protestar, MultaCodi,
  MultaDias, MultaTiVe, Corrido, CliInt, CartPref, RolComis, Filial, ECodMunici,
  PCodMunici, CCodMunici, LCodMunici, ECodiPais, PCodiPais, L_Ativo, CRT, ETe1Tip,
  ECelTip, PTe1Tip, PCelTip, EstrangDef, EstrangTip, indIEDest, LCodiPais
  AjudaEmpV, AjudaEmpP, Comissao, CreditosF2, QuantN1, QuantN2, QuantN3,
  LimiCred, Desco, TempA, TempD, FatorCompra, AdValorem, MultaValr, MultaPerc,
  JuroSacado, CPMF
}
  ' ) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab +
  ' WHERE Codigo > -11');
  //

  SDCidade.Close;
  SDCidade.Open;
  //
  SDCidadC.Close;
  SDCidadC.Open;
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDCliente.Close;
    SDCliente.DataSet.CommandType := ctQuery;
    SDCliente.DataSet.CommandText := SQL;
    SDCliente.Open;
    Continua := SDCliente.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDCliente.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDCliente.First;
      SQLRec    := '';
      while not SDCliente.Eof do
      begin
        // Antes
        if Uppercase(SDClienteTIPOPESSOA.Value) = 'J' then
          Tipo           := 0   // Juridica
        else
          Tipo           := 1;  // Fisica
        // Sequencia no BD
        Codigo         := Integer(SDClienteCLIENTE.Value);
        CodUsu         := Codigo; // Acima
        Respons1       := ''; // SDCliente.Value;
        Respons2       := ''; // SDCliente.Value;
        Pai            := ''; // SDCliente.Value;
        Mae            := ''; // SDCliente.Value;
        if Tipo = 0 then
        begin
          RazaoSocial    := SDClienteRAZAOSOCIAL.Value;
          Fantasia       := SDClienteFANTASIA.Value;
          Nome           := '';
          Apelido        := '';
          CNPJ           := SDClienteCPFCGC.Value;
          IE             := SDClienteRGINSCRICAOESTADUAL.Value;
          CPF            := '';
          RG             := '';
          ELograd        := 0; // SDCliente.Value;
          ERua           := SDClienteENDERECO.Value;
          ENumero        := Integer(SDClienteENDERECONUMERO.Value);
          ECompl         := SDClienteCOMPLEMENTO.Value;
          EBairro        := SDClienteBAIRRO.Value;
          ECidade        := SDClienteCIDADE_DESCRICAO.Value;
          EUF            := Geral.GetCodigoUF_da_SiglaUF(SDClienteCIDADE_UF.Value);
          if SDClienteCEP.Value = EMptyStr then
            ECEP           := 0
          else
            ECEP           := Geral.IMV(Geral.SoNumero_TT(SDClienteCEP.Value));
          EPais          := 'Brasil';// SDCliente.Value;
          ETe1           := Geral.SoNumero_TT(SDClienteFONE1.Value);
          Ete2           := Geral.SoNumero_TT(SDClienteFONE2.Value);
          Ete3           := ''; // SDCliente.Value;
          ECel           := Geral.SoNumero_TT(SDClienteCELULAR.Value);
          EFax           := ''; // SDCliente.Value;
          EEmail         := SDClienteEMAILPRIMARIO.Value;
          EContato       := SDClienteCONTATO.Value;
          ENatal         := Geral.FDT(SDClienteANIVERSARIOABERTURA.Value, 1);

          PLograd        := 0;
          PRua           := '';
          PNumero        := 0;
          PCompl         := '';
          PBairro        := '';
          PCidade        := '';
          PUF            := 0;
          PCEP           := 0;
          PPais          := '';
          PTe1           := '';
          Pte2           := '';
          Pte3           := '';
          PCel           := '';
          PFax           := '';
          PEmail         := '';
          PContato       := '';
          PNatal         := '0000-00-00';

          PCidadeCod     := 0;
          ECidadeCod     := 0;
          PPaisCod       := 0;
          EPaisCod       := 0;

          PCodMunici     := 0;
          PCodiPais      := 0;
          if SDClienteCIDADE_IBGE.Value = EmptyStr then
            ECodMunici := 0
          else
            ECodMunici     := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_IBGE.Value));
          //
          if SDClienteCIDADE_PAIS.Value = EmptyStr then
            ECodiPais       := 0
          else
            ECodiPais       := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_PAIS.Value));

        end else
        begin
          RazaoSocial    := '';
          Fantasia       := '';
          Nome           := SDClienteRAZAOSOCIAL.Value;
          Apelido        := SDClienteFANTASIA.Value;
          CNPJ           := '';
          IE             := '';
          CPF            := SDClienteCPFCGC.Value;
          RG             := SDClienteRGINSCRICAOESTADUAL.Value;

          ELograd        := 0;
          ERua           := '';
          ENumero        := 0;
          ECompl         := '';
          EBairro        := '';
          ECidade        := '';
          EUF            := 0;
          ECEP           := 0;
          EPais          := '';
          ETe1           := '';
          Ete2           := '';
          Ete3           := '';
          ECel           := '';
          EFax           := '';
          EEmail         := '';
          EContato       := '';
          ENatal         := '0000-00-00';

          PLograd        := 0; // SDCliente.Value;
          PRua           := SDClienteENDERECO.Value;
          PNumero        := Integer(SDClienteENDERECONUMERO.Value);
          PCompl         := SDClienteCOMPLEMENTO.Value;
          PBairro        := SDClienteBAIRRO.Value;
          PCidade        := SDClienteCIDADE_DESCRICAO.Value;
          PUF            := Geral.GetCodigoUF_da_SiglaUF(SDClienteCIDADE_UF.Value);
          if SDClienteCEP.Value = EmptyStr then
            PCEP           := 0
          else
            PCEP           := Geral.IMV(Geral.SoNumero_TT(SDClienteCEP.Value));
          PPais          := 'Brasil'; // SDCliente.Value;
          PTe1           := Geral.SoNumero_TT(SDClienteFONE1.Value);
          Pte2           := Geral.SoNumero_TT(SDClienteFONE2.Value);
          Pte3           := ''; // SDCliente.Value;
          PCel           := Geral.SoNumero_TT(SDClienteCELULAR.Value);
          PFax           := ''; // SDCliente.Value;
          PEmail         := SDClienteEMAILPRIMARIO.Value;
          PContato       := SDClienteCONTATO.Value;
          PNatal         := Geral.FDT(SDClienteANIVERSARIOABERTURA.Value, 1);

          PCidadeCod     := 0;
          ECidadeCod     := 0;
          PPaisCod       := 0;
          EPaisCod       := 0;

          ECodMunici     := 0;
          ECodiPais      := 0;
          if SDClienteCIDADE_IBGE.Value = EmptyStr then
            PCodMunici := 0
          else
            PCodMunici     := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_IBGE.Value));
          //
          if SDClienteCIDADE_PAIS.Value = EmptyStr then
            PCodiPais       := 0
          else
            PCodiPais       := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_PAIS.Value));

        end;
        NIRE           := ''; // SDCliente.Value;
        FormaSociet    := ''; // SDCliente.Value;
        Simples        := 0; // SDCliente.Value;
        IEST           := ''; // SDCliente.Value;
        Atividade      := ''; // SDCliente.Value;
        //CPF            := Acima!!!
        CPF_Pai        := ''; // SDCliente.Value;
        CPF_Conjuge    := ''; // SDCliente.Value;
        //RG             := Acima
        SSP            := ''; // SDCliente.Value;
        DataRG         := '0000-00-00'; // SDCliente.Value;
        CidadeNatal    := ''; // SDClienteNATURALIDADE.Value;
        EstCivil       := 0; // SDCliente.Value;
        UFNatal        := 0; // SDCliente.Value;
        Nacionalid     := ''; // SDCliente.Value;
        Sexo           := ''; // SDCliente.Value;
        Responsavel    := ''; // SDCliente.Value;
        Profissao      := ''; // SDCliente.Value;
        Cargo          := ''; // SDCliente.Value;
        Recibo         := 0; // SDCliente.Value;
        DiaRecibo      := 0; // SDCliente.Value;
        AjudaEmpV      := 0; // SDCliente.Value;
        AjudaEmpP      := 0; // SDCliente.Value;
        //
        Cliente1       := 'F';
        Cliente2       := 'F';
        Cliente3       := 'F';
        Cliente4       := 'F';
        Fornece1       := 'F';
        Fornece2       := 'F';
        Fornece3       := 'F';
        Fornece4       := 'F';
        Fornece5       := 'F';
        Fornece6       := 'F';
        Fornece7       := 'F';
        Fornece8       := 'F';
        Terceiro       := 'F';
        case Integer(SDClienteTIPOCADASTRO.Value) of
          1: Cliente1       := 'V';
          2: Fornece1       := 'V';
          else MeErrosGerais.Lines.Add('Cliente ' + Geral.FF0(Codigo) + ' ' + Geral.FF0(Integer(SDClienteTIPOCADASTRO.Value)) + ' n�o implementado!!!');
        end;
        Cadastro       := Geral.FDT(SDClienteDATACADASTRO.Value, 1);
        Informacoes    := SDClienteINFORMACOES.Value;
        //Logo           := ''; // SDCliente.Value;
        Veiculo        := 0; // SDCliente.Value;
        Mensal         := ''; // SDCliente.Value;
        Observacoes    := SDClienteOBSERVACAO.Value;
        //Tipo           := Acima!!
        CLograd        := 0; //SDCliente.Value;
        CRua           := SDClienteENDERECOCOBRANCA.Value;
        CNumero        := 0; // SDCliente.Value;
        CCompl         := SDClienteCOMPLEMENTOCOBRANCA.Value;
        CBairro        := SDClienteBAIRROENDERECOCOBRANCA.Value;
        CCidade        := SDClienteCIDADC_DESCRICAO.Value;
        CUF            := 0; // SDCliente.Value;
        CCEP           := Geral.IMV(Geral.SoNumero_TT(SDClienteCEPENDERECOCOBRANCA.Value));
        CPais          := ''; // SDCliente.Value;
        if SDClienteCIDADC_IBGE.Value = EmptyStr then
          CCodMunici := 0
        else
          CCodMunici     := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADC_IBGE.Value));
        //
        CTel           := ''; // SDCliente.Value;
        CCel           := ''; // SDCliente.Value;
        CFax           := ''; // SDCliente.Value;
        CContato       := ''; // SDCliente.Value;
        LLograd        := 0; // SDCliente.Value;
        LRua           := ''; // SDCliente.Value;
        LNumero        := 0; // SDCliente.Value;
        LCompl         := ''; // SDCliente.Value;
        LBairro        := ''; // SDCliente.Value;
        LCidade        := ''; // SDCliente.Value;
        LUF            := 0; // SDCliente.Value;
        LCEP           := 0; // SDCliente.Value;
        LPais          := ''; // SDCliente.Value;
        LTel           := ''; // SDCliente.Value;
        LCel           := ''; // SDCliente.Value;
        LFax           := ''; // SDCliente.Value;
        LContato       := ''; // SDCliente.Value;
        Comissao       := 0.00; // SDCliente.Value;
        Situacao       := 0; // SDCliente.Value;
        Nivel          := ''; // SDCliente.Value;
        Grupo          := 0; // SDCliente.Value;
        Account        := 0; // SDCliente.Value;
        //Logo2          := ''; // SDCliente.Value;
        ConjugeNome    := ''; // SDCliente.Value;
        ConjugeNatal   := '0000-00-00'; // SDCliente.Value;
        Nome1          := ''; // SDCliente.Value;
        Natal1         := '0000-00-00'; // SDCliente.Value;
        Nome2          := ''; // SDCliente.Value;
        Natal2         := '0000-00-00'; // SDCliente.Value;
        Nome3          := ''; // SDCliente.Value;
        Natal3         := '0000-00-00'; // SDCliente.Value;
        Nome4          := ''; // SDCliente.Value;
        Natal4         := '0000-00-00'; // SDCliente.Value;
        CreditosI      := 0; // SDCliente.Value;
        CreditosL      := 0; // SDCliente.Value;
        CreditosF2     := 0;
        CreditosD      := '0000-00-00'; // SDCliente.Value;
        CreditosU      := Geral.FDT(SDClienteULTIMACOMPRA.Value, 1);
        CreditosV      := '0000-00-00'; // SDCliente.Value;
        Motivo         := 0; // SDCliente.Value;
        QuantI1        := 0; // SDCliente.Value;
        QuantI2        := 0; // SDCliente.Value;
        QuantI3        := 0; // SDCliente.Value;
        QuantI4        := 0; // SDCliente.Value;
        QuantN1        := 0.00; // SDCliente.Value;
        QuantN2        := 0.00; // SDCliente.Value;
        QuantN3        := 0.00; // SDCliente.Value;
        Agenda         := ''; // SDCliente.Value;
        SenhaQuer      := ''; // SDCliente.Value;
        Senha1         := ''; // SDCliente.Value;
        LimiCred       := Double(SDClienteCREDITO.Value);
        Desco          := 0; // SDCliente.Value;
        CasasApliDesco := 0; // SDCliente.Value;
        TempA          := 0.00; // SDCliente.Value;
        TempD          := 0.00; // SDCliente.Value;
        Banco          := 0; // SDCliente.Value;
        Agencia        := ''; // SDCliente.Value;
        ContaCorrente  := ''; // SDCliente.Value;
        FatorCompra    := 0.00; // SDCliente.Value;
        AdValorem      := 0.00; // SDCliente.Value;
        DMaisC         := 0; // SDCliente.Value;
        DMaisD         := 0; // SDCliente.Value;
        Empresa        := 0; // SDCliente.Value;
        CBE            := 0; // SDCliente.Value;
        SCB            := 0; // SDCliente.Value;
        PAtividad      := 0; // SDCliente.Value;
        EAtividad      := 0; // SDCliente.Value;
        Antigo         := ''; // SDCliente.Value;
        CUF2           := ''; // SDCliente.Value;
        Contab         := ''; // SDCliente.Value;
        MSN1           := ''; // SDCliente.Value;
        PastaTxtFTP    := ''; // SDCliente.Value;
        PastaPwdFTP    := ''; // SDCliente.Value;
        Protestar      := 0; // SDCliente.Value;
        MultaCodi      := 0; // SDCliente.Value;
        MultaDias      := 0; // SDCliente.Value;
        MultaValr      := 0.00; // SDCliente.Value;
        MultaPerc      := 0.00; // SDCliente.Value;
        MultaTiVe      := 0; // SDCliente.Value;
        JuroSacado     := 0.00; // SDCliente.Value;
        CPMF           := 0.00; // SDCliente.Value;
        Corrido        := 0; // SDCliente.Value;
        CPF_Resp1      := ''; // SDCliente.Value;
        CliInt         := 0; // SDCliente.Value;
        AltDtPlaCt     := '0000-00-00'; // SDCliente.Value;
        CartPref       := 0; // SDCliente.Value;
        RolComis       := 0; // SDCliente.Value;
        Filial         := 0; // SDCliente.Value;
        EEndeRef       := ''; // SDCliente.Value;
        PEndeRef       := ''; // SDCliente.Value;
        CEndeRef       := ''; // SDCliente.Value;
        LEndeRef       := ''; // SDCliente.Value;
        CNAE           := ''; // SDCliente.Value;
        SUFRAMA        := ''; // SDCliente.Value;
        L_CNPJ         := ''; // SDCliente.Value;
        L_Ativo        := 0; // SDCliente.Value;
        URL            := ''; // SDCliente.Value;
        CRT            := 0; // SDCliente.Value;
        COD_PART       := ''; // SDCliente.Value;
        ETe1Tip        := 0; // SDCliente.Value;
        ECelTip        := 0; // SDCliente.Value;
        PTe1Tip        := 0; // SDCliente.Value;
        PCelTip        := 0; // SDCliente.Value;
        ESite          := ''; // SDCliente.Value;
        PSite          := ''; // SDCliente.Value;
        EstrangDef     := 0; // SDCliente.Value;
        EstrangTip     := 0; // SDCliente.Value;
        EstrangNum     := ''; // SDCliente.Value;
        indIEDest      := 0; // SDCliente.Value;
        L_CPF          := ''; // SDCliente.Value;
        L_Nome         := ''; // SDCliente.Value;
        LCodiPais      := 0; // SDCliente.Value;
        LEmail         := SDClienteEMAILFINANCEIRO.Value;
        L_IE           := ''; // SDCliente.Value;
        // Campos Novos!!
        ListaStatus    := SDClienteLISTASTATUS.Value;


        //
        if SDCliente.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec

        +     (Geral.VariavelToString(RazaoSocial))
        + PV_N(Geral.VariavelToString(Fantasia))
        + PV_N(Geral.VariavelToString(Respons1))
        + PV_N(Geral.VariavelToString(Respons2))
        + PV_N(Geral.VariavelToString(Pai))
        + PV_N(Geral.VariavelToString(Mae))
        + PV_N(Geral.VariavelToString(CNPJ))
        + PV_N(Geral.VariavelToString(IE))
        + PV_N(Geral.VariavelToString(NIRE))
        + PV_N(Geral.VariavelToString(FormaSociet))
        + PV_N(Geral.VariavelToString(IEST))
        + PV_N(Geral.VariavelToString(Atividade))
        + PV_N(Geral.VariavelToString(Nome))
        + PV_N(Geral.VariavelToString(Apelido))
        + PV_N(Geral.VariavelToString(CPF))
        + PV_N(Geral.VariavelToString(CPF_Pai))
        + PV_N(Geral.VariavelToString(CPF_Conjuge))
        + PV_N(Geral.VariavelToString(RG))
        + PV_N(Geral.VariavelToString(SSP))
        + PV_N(Geral.VariavelToString(DataRG))
        + PV_N(Geral.VariavelToString(CidadeNatal))
        + PV_N(Geral.VariavelToString(Nacionalid))
        + PV_N(Geral.VariavelToString(ERua))
        + PV_N(Geral.VariavelToString(ECompl))
        + PV_N(Geral.VariavelToString(EBairro))
        + PV_N(Geral.VariavelToString(ECidade))
        + PV_N(Geral.VariavelToString(EPais))
        + PV_N(Geral.VariavelToString(ETe1))
        + PV_N(Geral.VariavelToString(Ete2))
        + PV_N(Geral.VariavelToString(Ete3))
        + PV_N(Geral.VariavelToString(ECel))
        + PV_N(Geral.VariavelToString(EFax))
        + PV_N(Geral.VariavelToString(EEmail))
        + PV_N(Geral.VariavelToString(EContato))
        + PV_N(Geral.VariavelToString(ENatal))
        + PV_N(Geral.VariavelToString(PRua))
        + PV_N(Geral.VariavelToString(PCompl))
        + PV_N(Geral.VariavelToString(PBairro))
        + PV_N(Geral.VariavelToString(PCidade))
        + PV_N(Geral.VariavelToString(PPais))
        + PV_N(Geral.VariavelToString(PTe1))
        + PV_N(Geral.VariavelToString(Pte2))
        + PV_N(Geral.VariavelToString(Pte3))
        + PV_N(Geral.VariavelToString(PCel))
        + PV_N(Geral.VariavelToString(PFax))
        + PV_N(Geral.VariavelToString(PEmail))
        + PV_N(Geral.VariavelToString(PContato))
        + PV_N(Geral.VariavelToString(PNatal))
        + PV_N(Geral.VariavelToString(Sexo))
        + PV_N(Geral.VariavelToString(Responsavel))
        + PV_N(Geral.VariavelToString(Profissao))
        + PV_N(Geral.VariavelToString(Cargo))
        + PV_N(Geral.VariavelToString(Cliente1))
        + PV_N(Geral.VariavelToString(Cliente2))
        + PV_N(Geral.VariavelToString(Cliente3))
        + PV_N(Geral.VariavelToString(Cliente4))
        + PV_N(Geral.VariavelToString(Fornece1))
        + PV_N(Geral.VariavelToString(Fornece2))
        + PV_N(Geral.VariavelToString(Fornece3))
        + PV_N(Geral.VariavelToString(Fornece4))
        + PV_N(Geral.VariavelToString(Fornece5))
        + PV_N(Geral.VariavelToString(Fornece6))
        + PV_N(Geral.VariavelToString(Fornece7))
        + PV_N(Geral.VariavelToString(Fornece8))
        + PV_N(Geral.VariavelToString(Terceiro))
        + PV_N(Geral.VariavelToString(Cadastro))
        + PV_N(Geral.VariavelToString(Informacoes))
        + PV_N(Geral.VariavelToString(Mensal))
        + PV_N(Geral.VariavelToString(Observacoes))
        + PV_N(Geral.VariavelToString(CRua))
        + PV_N(Geral.VariavelToString(CCompl))
        + PV_N(Geral.VariavelToString(CBairro))
        + PV_N(Geral.VariavelToString(CCidade))
        + PV_N(Geral.VariavelToString(CPais))
        + PV_N(Geral.VariavelToString(CTel))
        + PV_N(Geral.VariavelToString(CCel))
        + PV_N(Geral.VariavelToString(CFax))
        + PV_N(Geral.VariavelToString(CContato))
        + PV_N(Geral.VariavelToString(LRua))
        + PV_N(Geral.VariavelToString(LCompl))
        + PV_N(Geral.VariavelToString(LBairro))
        + PV_N(Geral.VariavelToString(LCidade))
        + PV_N(Geral.VariavelToString(LPais))
        + PV_N(Geral.VariavelToString(LTel))
        + PV_N(Geral.VariavelToString(LCel))
        + PV_N(Geral.VariavelToString(LFax))
        + PV_N(Geral.VariavelToString(LContato))
        + PV_N(Geral.VariavelToString(Nivel))
        + PV_N(Geral.VariavelToString(ConjugeNome))
        + PV_N(Geral.VariavelToString(ConjugeNatal))
        + PV_N(Geral.VariavelToString(Nome1))
        + PV_N(Geral.VariavelToString(Natal1))
        + PV_N(Geral.VariavelToString(Nome2))
        + PV_N(Geral.VariavelToString(Natal2))
        + PV_N(Geral.VariavelToString(Nome3))
        + PV_N(Geral.VariavelToString(Natal3))
        + PV_N(Geral.VariavelToString(Nome4))
        + PV_N(Geral.VariavelToString(Natal4))
        + PV_N(Geral.VariavelToString(CreditosD))
        + PV_N(Geral.VariavelToString(CreditosU))
        + PV_N(Geral.VariavelToString(CreditosV))
        + PV_N(Geral.VariavelToString(Agenda))
        + PV_N(Geral.VariavelToString(SenhaQuer))
        + PV_N(Geral.VariavelToString(Senha1))
        + PV_N(Geral.VariavelToString(Agencia))
        + PV_N(Geral.VariavelToString(ContaCorrente))
        + PV_N(Geral.VariavelToString(Antigo))
        + PV_N(Geral.VariavelToString(CUF2))
        + PV_N(Geral.VariavelToString(Contab))
        + PV_N(Geral.VariavelToString(MSN1))
        + PV_N(Geral.VariavelToString(PastaTxtFTP))
        + PV_N(Geral.VariavelToString(PastaPwdFTP))
        + PV_N(Geral.VariavelToString(CPF_Resp1))
        + PV_N(Geral.VariavelToString(AltDtPlaCt))
        + PV_N(Geral.VariavelToString(EEndeRef))
        + PV_N(Geral.VariavelToString(PEndeRef))
        + PV_N(Geral.VariavelToString(CEndeRef))
        + PV_N(Geral.VariavelToString(LEndeRef))
        + PV_N(Geral.VariavelToString(CNAE))
        + PV_N(Geral.VariavelToString(SUFRAMA))
        + PV_N(Geral.VariavelToString(L_CNPJ))
        + PV_N(Geral.VariavelToString(URL))
        + PV_N(Geral.VariavelToString(COD_PART))
        + PV_N(Geral.VariavelToString(ESite))
        + PV_N(Geral.VariavelToString(PSite))
        + PV_N(Geral.VariavelToString(EstrangNum))
        + PV_N(Geral.VariavelToString(L_CPF))
        + PV_N(Geral.VariavelToString(L_Nome))
        + PV_N(Geral.VariavelToString(LEmail))
        + PV_N(Geral.VariavelToString(L_IE))
        + PV_N(Geral.VariavelToString(ListaStatus))
        + PV_N(Geral.VariavelToString(Codigo))
        + PV_N(Geral.VariavelToString(CodUsu))
        + PV_N(Geral.VariavelToString(Simples))
        + PV_N(Geral.VariavelToString(EstCivil))
        + PV_N(Geral.VariavelToString(UFNatal))
        + PV_N(Geral.VariavelToString(ELograd))
        + PV_N(Geral.VariavelToString(ENumero))
        + PV_N(Geral.VariavelToString(EUF))
        + PV_N(Geral.VariavelToString(ECEP))
        + PV_N(Geral.VariavelToString(PLograd))
        + PV_N(Geral.VariavelToString(PNumero))
        + PV_N(Geral.VariavelToString(PUF))
        + PV_N(Geral.VariavelToString(PCEP))
        + PV_N(Geral.VariavelToString(Recibo))
        + PV_N(Geral.VariavelToString(DiaRecibo))
        + PV_N(Geral.VariavelToString(Veiculo))
        + PV_N(Geral.VariavelToString(Tipo))
        + PV_N(Geral.VariavelToString(CLograd))
        + PV_N(Geral.VariavelToString(CNumero))
        + PV_N(Geral.VariavelToString(CUF))
        + PV_N(Geral.VariavelToString(CCEP))
        + PV_N(Geral.VariavelToString(LLograd))
        + PV_N(Geral.VariavelToString(LNumero))
        + PV_N(Geral.VariavelToString(LUF))
        + PV_N(Geral.VariavelToString(LCEP))
        + PV_N(Geral.VariavelToString(Situacao))
        + PV_N(Geral.VariavelToString(Grupo))
        + PV_N(Geral.VariavelToString(Account))
        + PV_N(Geral.VariavelToString(CreditosI))
        + PV_N(Geral.VariavelToString(CreditosL))
        + PV_N(Geral.VariavelToString(Motivo))
        + PV_N(Geral.VariavelToString(QuantI1))
        + PV_N(Geral.VariavelToString(QuantI2))
        + PV_N(Geral.VariavelToString(QuantI3))
        + PV_N(Geral.VariavelToString(QuantI4))
        + PV_N(Geral.VariavelToString(CasasApliDesco))
        + PV_N(Geral.VariavelToString(Banco))
        + PV_N(Geral.VariavelToString(DMaisC))
        + PV_N(Geral.VariavelToString(DMaisD))
        + PV_N(Geral.VariavelToString(Empresa))
        + PV_N(Geral.VariavelToString(CBE))
        + PV_N(Geral.VariavelToString(SCB))
        + PV_N(Geral.VariavelToString(PAtividad))
        + PV_N(Geral.VariavelToString(EAtividad))
        + PV_N(Geral.VariavelToString(PCidadeCod))
        + PV_N(Geral.VariavelToString(ECidadeCod))
        + PV_N(Geral.VariavelToString(PPaisCod))
        + PV_N(Geral.VariavelToString(EPaisCod))
        + PV_N(Geral.VariavelToString(Protestar))
        + PV_N(Geral.VariavelToString(MultaCodi))
        + PV_N(Geral.VariavelToString(MultaDias))
        + PV_N(Geral.VariavelToString(MultaTiVe))
        + PV_N(Geral.VariavelToString(Corrido))
        + PV_N(Geral.VariavelToString(CliInt))
        + PV_N(Geral.VariavelToString(CartPref))
        + PV_N(Geral.VariavelToString(RolComis))
        + PV_N(Geral.VariavelToString(Filial))
        + PV_N(Geral.VariavelToString(ECodMunici))
        + PV_N(Geral.VariavelToString(PCodMunici))
        + PV_N(Geral.VariavelToString(CCodMunici))
        + PV_N(Geral.VariavelToString(LCodMunici))
        + PV_N(Geral.VariavelToString(ECodiPais))
        + PV_N(Geral.VariavelToString(PCodiPais))
        + PV_N(Geral.VariavelToString(L_Ativo))
        + PV_N(Geral.VariavelToString(CRT))
        + PV_N(Geral.VariavelToString(ETe1Tip))
        + PV_N(Geral.VariavelToString(ECelTip))
        + PV_N(Geral.VariavelToString(PTe1Tip))
        + PV_N(Geral.VariavelToString(PCelTip))
        + PV_N(Geral.VariavelToString(EstrangDef))
        + PV_N(Geral.VariavelToString(EstrangTip))
        + PV_N(Geral.VariavelToString(indIEDest))
        + PV_N(Geral.VariavelToString(LCodiPais))
        + PV_N(Geral.VariavelToString(AjudaEmpV))
        + PV_N(Geral.VariavelToString(AjudaEmpP))
        + PV_N(Geral.VariavelToString(Comissao))
        + PV_N(Geral.VariavelToString(CreditosF2))
        + PV_N(Geral.VariavelToString(QuantN1))
        + PV_N(Geral.VariavelToString(QuantN2))
        + PV_N(Geral.VariavelToString(QuantN3))
        + PV_N(Geral.VariavelToString(LimiCred))
        + PV_N(Geral.VariavelToString(Desco))
        + PV_N(Geral.VariavelToString(TempA))
        + PV_N(Geral.VariavelToString(TempD))
        + PV_N(Geral.VariavelToString(FatorCompra))
        + PV_N(Geral.VariavelToString(AdValorem))
        + PV_N(Geral.VariavelToString(MultaValr))
        + PV_N(Geral.VariavelToString(MultaPerc))
        + PV_N(Geral.VariavelToString(JuroSacado))
        + PV_N(Geral.VariavelToString(CPMF))
        + ')';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDCliente.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDCliente.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Cliente2;
var
  RazaoSocial, Fantasia, Respons1, Respons2, Pai, Mae, CNPJ, IE, NIRE,
  FormaSociet, IEST, Atividade, Nome, Apelido, CPF, CPF_Pai, CPF_Conjuge, RG,
  SSP, DataRG, CidadeNatal, Nacionalid, ERua, ECompl, EBairro, ECidade, EPais,
  ETe1, Ete2, Ete3, ECel, EFax, EEmail, EContato, ENatal, PRua, PCompl, PBairro,
  PCidade, PPais, PTe1, Pte2, Pte3, PCel, PFax, PEmail, PContato, PNatal, Sexo,
  Responsavel, Profissao, Cargo, Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7,
  Fornece8, Terceiro, Cadastro, Informacoes, (*Logo,*) Mensal, Observacoes, CRua,
  CCompl, CBairro, CCidade, CPais, CTel, CCel, CFax, CContato, LRua, LCompl,
  LBairro, LCidade, LPais, LTel, LCel, LFax, LContato, Nivel, (*Logo2,*)
  ConjugeNome, ConjugeNatal, Nome1, Natal1, Nome2, Natal2, Nome3, Natal3, Nome4,
  Natal4, CreditosD, CreditosU, CreditosV, Agenda, SenhaQuer, Senha1, Agencia,
  ContaCorrente, Antigo, CUF2, Contab, MSN1, PastaTxtFTP, PastaPwdFTP, CPF_Resp1,
  AltDtPlaCt, EEndeRef, PEndeRef, CEndeRef, LEndeRef, CNAE, SUFRAMA, L_CNPJ, URL,
  COD_PART, ESite, PSite, EstrangNum, L_CPF, L_Nome, LEmail, L_IE, ListaStatus:
  String;

  Codigo, CodUsu, Simples, EstCivil, UFNatal, ELograd, ENumero, EUF, ECEP,
  PLograd, PNumero, PUF, PCEP, Recibo, DiaRecibo, Veiculo, Tipo, CLograd,
  CNumero, CUF, CCEP, LLograd, LNumero, LUF, LCEP, Situacao, Grupo, Account,
  CreditosI, CreditosL, Motivo, QuantI1, QuantI2, QuantI3, QuantI4,
  CasasApliDesco, Banco, DMaisC, DMaisD, Empresa, CBE, SCB, PAtividad,
  EAtividad, PCidadeCod, ECidadeCod, PPaisCod, EPaisCod, Protestar, MultaCodi,
  MultaDias, MultaTiVe, Corrido, CliInt, CartPref, RolComis, Filial, ECodMunici,
  PCodMunici, CCodMunici, LCodMunici, ECodiPais, PCodiPais, L_Ativo, CRT, ETe1Tip,
  ECelTip, PTe1Tip, PCelTip, EstrangDef, EstrangTip, indIEDest, LCodiPais:
  Integer;

  AjudaEmpV, AjudaEmpP, Comissao, CreditosF2, QuantN1, QuantN2, QuantN3,
  LimiCred, Desco, TempA, TempD, FatorCompra, AdValorem, MultaValr, MultaPerc,
  JuroSacado, CPMF: Double;

  SQLType: TSQLType;

  ItemAtual, NextItens: Integer;
const
  Step = 20;
  Tabela = 'Cliente';
begin


  SQLType        := stIns;


  SDCidade.Close;
  SDCidade.Open;
  //
  SDCidadC.Close;
  SDCidadC.Open;
  //
  SDCliente.Close;
  SDCliente.Connection := DBFB;
  SDCliente.Open;
  SDCliente.First;
  ItemAtual := 0;
  NextItens := Step;
  PB1.Position := 0;
  PB1.Max := (SDCliente.RecordCount div Step) + 1;
  while not SDCliente.EOF do
  begin
    ItemAtual := ItemAtual + 1;
    if ItemAtual > NextItens then
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Tabela ' + Tabela + '. Registros verificados: ' + Geral.FF0(NextItens));
      NextItens := NextItens + Step;
    end;

    // Antes
    if Uppercase(SDClienteTIPOPESSOA.Value) = 'J' then
      Tipo           := 0   // Juridica
    else
      Tipo           := 1;  // Fisica
    // Sequencia no BD
    Codigo         := Integer(SDClienteCLIENTE.Value);
    CodUsu         := Codigo; // Acima
    Respons1       := ''; // SDCliente.Value;
    Respons2       := ''; // SDCliente.Value;
    Pai            := ''; // SDCliente.Value;
    Mae            := ''; // SDCliente.Value;
    if Tipo = 0 then
    begin
      RazaoSocial    := SDClienteRAZAOSOCIAL.Value;
      Fantasia       := SDClienteFANTASIA.Value;
      Nome           := '';
      Apelido        := '';
      CNPJ           := SDClienteCPFCGC.Value;
      IE             := SDClienteRGINSCRICAOESTADUAL.Value;
      CPF            := '';
      RG             := '';
      ELograd        := 0; // SDCliente.Value;
      ERua           := SDClienteENDERECO.Value;
      ENumero        := Integer(SDClienteENDERECONUMERO.Value);
      ECompl         := SDClienteCOMPLEMENTO.Value;
      EBairro        := SDClienteBAIRRO.Value;
      ECidade        := SDClienteCIDADE_DESCRICAO.Value;
      EUF            := Geral.GetCodigoUF_da_SiglaUF(SDClienteCIDADE_UF.Value);
      if SDClienteCEP.Value = EMptyStr then
        ECEP           := 0
      else
        ECEP           := Geral.IMV(Geral.SoNumero_TT(SDClienteCEP.Value));
      EPais          := 'Brasil';// SDCliente.Value;
      ETe1           := Geral.SoNumero_TT(SDClienteFONE1.Value);
      Ete2           := Geral.SoNumero_TT(SDClienteFONE2.Value);
      Ete3           := ''; // SDCliente.Value;
      ECel           := Geral.SoNumero_TT(SDClienteCELULAR.Value);
      EFax           := ''; // SDCliente.Value;
      EEmail         := SDClienteEMAILPRIMARIO.Value;
      EContato       := SDClienteCONTATO.Value;
      ENatal         := Geral.FDT(SDClienteANIVERSARIOABERTURA.Value, 1);

      PLograd        := 0;
      PRua           := '';
      PNumero        := 0;
      PCompl         := '';
      PBairro        := '';
      PCidade        := '';
      PUF            := 0;
      PCEP           := 0;
      PPais          := '';
      PTe1           := '';
      Pte2           := '';
      Pte3           := '';
      PCel           := '';
      PFax           := '';
      PEmail         := '';
      PContato       := '';
      PNatal         := '';

      PCidadeCod     := 0;
      ECidadeCod     := 0;
      PPaisCod       := 0;
      EPaisCod       := 0;

      PCodMunici     := 0;
      PCodiPais      := 0;
      if SDClienteCIDADE_IBGE.Value = EmptyStr then
        ECodMunici := 0
      else
        ECodMunici     := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_IBGE.Value));
      //
      if SDClienteCIDADE_PAIS.Value = EmptyStr then
        ECodiPais       := 0
      else
        ECodiPais       := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_PAIS.Value));

    end else
    begin
      RazaoSocial    := '';
      Fantasia       := '';
      Nome           := SDClienteRAZAOSOCIAL.Value;
      Apelido        := SDClienteFANTASIA.Value;
      CNPJ           := '';
      IE             := '';
      CPF            := SDClienteCPFCGC.Value;
      RG             := SDClienteRGINSCRICAOESTADUAL.Value;

      ELograd        := 0;
      ERua           := '';
      ENumero        := 0;
      ECompl         := '';
      EBairro        := '';
      ECidade        := '';
      EUF            := 0;
      ECEP           := 0;
      EPais          := '';
      ETe1           := '';
      Ete2           := '';
      Ete3           := '';
      ECel           := '';
      EFax           := '';
      EEmail         := '';
      EContato       := '';
      ENatal         := '';

      PLograd        := 0; // SDCliente.Value;
      PRua           := SDClienteENDERECO.Value;
      PNumero        := Integer(SDClienteENDERECONUMERO.Value);
      PCompl         := SDClienteCOMPLEMENTO.Value;
      PBairro        := SDClienteBAIRRO.Value;
      PCidade        := SDClienteCIDADE_DESCRICAO.Value;
      PUF            := Geral.GetCodigoUF_da_SiglaUF(SDClienteCIDADE_UF.Value);
      if SDClienteCEP.Value = EmptyStr then
        PCEP           := 0
      else
        PCEP           := Geral.IMV(Geral.SoNumero_TT(SDClienteCEP.Value));
      PPais          := 'Brasil'; // SDCliente.Value;
      PTe1           := Geral.SoNumero_TT(SDClienteFONE1.Value);
      Pte2           := Geral.SoNumero_TT(SDClienteFONE2.Value);
      Pte3           := ''; // SDCliente.Value;
      PCel           := Geral.SoNumero_TT(SDClienteCELULAR.Value);
      PFax           := ''; // SDCliente.Value;
      PEmail         := SDClienteEMAILPRIMARIO.Value;
      PContato       := SDClienteCONTATO.Value;
      PNatal         := Geral.FDT(SDClienteANIVERSARIOABERTURA.Value, 1);

      PCidadeCod     := 0;
      ECidadeCod     := 0;
      PPaisCod       := 0;
      EPaisCod       := 0;

      ECodMunici     := 0;
      ECodiPais      := 0;
      if SDClienteCIDADE_IBGE.Value = EmptyStr then
        PCodMunici := 0
      else
        PCodMunici     := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_IBGE.Value));
      //
      if SDClienteCIDADE_PAIS.Value = EmptyStr then
        PCodiPais       := 0
      else
        PCodiPais       := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_PAIS.Value));

    end;
    NIRE           := ''; // SDCliente.Value;
    FormaSociet    := ''; // SDCliente.Value;
    Simples        := 0; // SDCliente.Value;
    IEST           := ''; // SDCliente.Value;
    Atividade      := ''; // SDCliente.Value;
    //CPF            := Acima!!!
    CPF_Pai        := ''; // SDCliente.Value;
    CPF_Conjuge    := ''; // SDCliente.Value;
    //RG             := Acima
    SSP            := ''; // SDCliente.Value;
    DataRG         := ''; // SDCliente.Value;
    CidadeNatal    := ''; // SDClienteNATURALIDADE.Value;
    EstCivil       := 0; // SDCliente.Value;
    UFNatal        := 0; // SDCliente.Value;
    Nacionalid     := ''; // SDCliente.Value;
    Sexo           := ''; // SDCliente.Value;
    Responsavel    := ''; // SDCliente.Value;
    Profissao      := ''; // SDCliente.Value;
    Cargo          := ''; // SDCliente.Value;
    Recibo         := 0; // SDCliente.Value;
    DiaRecibo      := 0; // SDCliente.Value;
    AjudaEmpV      := 0; // SDCliente.Value;
    AjudaEmpP      := 0; // SDCliente.Value;
    //
    Cliente1       := 'F';
    Cliente2       := 'F';
    Cliente3       := 'F';
    Cliente4       := 'F';
    Fornece1       := 'F';
    Fornece2       := 'F';
    Fornece3       := 'F';
    Fornece4       := 'F';
    Fornece5       := 'F';
    Fornece6       := 'F';
    Fornece7       := 'F';
    Fornece8       := 'F';
    Terceiro       := 'F';
    case Integer(SDClienteTIPOCADASTRO.Value) of
      1: Cliente1       := 'V';
      2: Fornece1       := 'V';
      else MeErrosGerais.Lines.Add('Cliente ' + Geral.FF0(Codigo) + ' ' + Geral.FF0(Integer(SDClienteTIPOCADASTRO.Value)) + ' n�o implementado!!!');
    end;
    Cadastro       := Geral.FDT(SDClienteDATACADASTRO.Value, 1);
    Informacoes    := SDClienteINFORMACOES.Value;
    //Logo           := ''; // SDCliente.Value;
    Veiculo        := 0; // SDCliente.Value;
    Mensal         := ''; // SDCliente.Value;
    Observacoes    := SDClienteOBSERVACAO.Value;
    //Tipo           := Acima!!
    CLograd        := 0; //SDCliente.Value;
    CRua           := SDClienteENDERECOCOBRANCA.Value;
    CNumero        := 0; // SDCliente.Value;
    CCompl         := SDClienteCOMPLEMENTOCOBRANCA.Value;
    CBairro        := SDClienteBAIRROENDERECOCOBRANCA.Value;
    CCidade        := SDClienteCIDADC_DESCRICAO.Value;
    CUF            := 0; // SDCliente.Value;
    CCEP           := Geral.IMV(Geral.SoNumero_TT(SDClienteCEPENDERECOCOBRANCA.Value));
    CPais          := ''; // SDCliente.Value;
    if SDClienteCIDADC_IBGE.Value = EmptyStr then
      CCodMunici := 0
    else
      CCodMunici     := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADC_IBGE.Value));
    //
    CTel           := ''; // SDCliente.Value;
    CCel           := ''; // SDCliente.Value;
    CFax           := ''; // SDCliente.Value;
    CContato       := ''; // SDCliente.Value;
    LLograd        := 0; // SDCliente.Value;
    LRua           := ''; // SDCliente.Value;
    LNumero        := 0; // SDCliente.Value;
    LCompl         := ''; // SDCliente.Value;
    LBairro        := ''; // SDCliente.Value;
    LCidade        := ''; // SDCliente.Value;
    LUF            := 0; // SDCliente.Value;
    LCEP           := 0; // SDCliente.Value;
    LPais          := ''; // SDCliente.Value;
    LTel           := ''; // SDCliente.Value;
    LCel           := ''; // SDCliente.Value;
    LFax           := ''; // SDCliente.Value;
    LContato       := ''; // SDCliente.Value;
    Comissao       := 0.00; // SDCliente.Value;
    Situacao       := 0; // SDCliente.Value;
    Nivel          := ''; // SDCliente.Value;
    Grupo          := 0; // SDCliente.Value;
    Account        := 0; // SDCliente.Value;
    //Logo2          := ''; // SDCliente.Value;
    ConjugeNome    := ''; // SDCliente.Value;
    ConjugeNatal   := ''; // SDCliente.Value;
    Nome1          := ''; // SDCliente.Value;
    Natal1         := ''; // SDCliente.Value;
    Nome2          := ''; // SDCliente.Value;
    Natal2         := ''; // SDCliente.Value;
    Nome3          := ''; // SDCliente.Value;
    Natal3         := ''; // SDCliente.Value;
    Nome4          := ''; // SDCliente.Value;
    Natal4         := ''; // SDCliente.Value;
    CreditosI      := 0; // SDCliente.Value;
    CreditosL      := 0; // SDCliente.Value;
    CreditosF2     := 0;
    CreditosD      := ''; // SDCliente.Value;
    CreditosU      := Geral.FDT(SDClienteULTIMACOMPRA.Value, 1);
    CreditosV      := ''; // SDCliente.Value;
    Motivo         := 0; // SDCliente.Value;
    QuantI1        := 0; // SDCliente.Value;
    QuantI2        := 0; // SDCliente.Value;
    QuantI3        := 0; // SDCliente.Value;
    QuantI4        := 0; // SDCliente.Value;
    QuantN1        := 0.00; // SDCliente.Value;
    QuantN2        := 0.00; // SDCliente.Value;
    QuantN3        := 0.00; // SDCliente.Value;
    Agenda         := ''; // SDCliente.Value;
    SenhaQuer      := ''; // SDCliente.Value;
    Senha1         := ''; // SDCliente.Value;
    LimiCred       := Double(SDClienteCREDITO.Value);
    Desco          := 0; // SDCliente.Value;
    CasasApliDesco := 0; // SDCliente.Value;
    TempA          := 0.00; // SDCliente.Value;
    TempD          := 0.00; // SDCliente.Value;
    Banco          := 0; // SDCliente.Value;
    Agencia        := ''; // SDCliente.Value;
    ContaCorrente  := ''; // SDCliente.Value;
    FatorCompra    := 0.00; // SDCliente.Value;
    AdValorem      := 0.00; // SDCliente.Value;
    DMaisC         := 0; // SDCliente.Value;
    DMaisD         := 0; // SDCliente.Value;
    Empresa        := 0; // SDCliente.Value;
    CBE            := 0; // SDCliente.Value;
    SCB            := 0; // SDCliente.Value;
    PAtividad      := 0; // SDCliente.Value;
    EAtividad      := 0; // SDCliente.Value;
    Antigo         := ''; // SDCliente.Value;
    CUF2           := ''; // SDCliente.Value;
    Contab         := ''; // SDCliente.Value;
    MSN1           := ''; // SDCliente.Value;
    PastaTxtFTP    := ''; // SDCliente.Value;
    PastaPwdFTP    := ''; // SDCliente.Value;
    Protestar      := 0; // SDCliente.Value;
    MultaCodi      := 0; // SDCliente.Value;
    MultaDias      := 0; // SDCliente.Value;
    MultaValr      := 0.00; // SDCliente.Value;
    MultaPerc      := 0.00; // SDCliente.Value;
    MultaTiVe      := 0; // SDCliente.Value;
    JuroSacado     := 0.00; // SDCliente.Value;
    CPMF           := 0.00; // SDCliente.Value;
    Corrido        := 0; // SDCliente.Value;
    CPF_Resp1      := ''; // SDCliente.Value;
    CliInt         := 0; // SDCliente.Value;
    AltDtPlaCt     := ''; // SDCliente.Value;
    CartPref       := 0; // SDCliente.Value;
    RolComis       := 0; // SDCliente.Value;
    Filial         := 0; // SDCliente.Value;
    EEndeRef       := ''; // SDCliente.Value;
    PEndeRef       := ''; // SDCliente.Value;
    CEndeRef       := ''; // SDCliente.Value;
    LEndeRef       := ''; // SDCliente.Value;
    CNAE           := ''; // SDCliente.Value;
    SUFRAMA        := ''; // SDCliente.Value;
    L_CNPJ         := ''; // SDCliente.Value;
    L_Ativo        := 0; // SDCliente.Value;
    URL            := ''; // SDCliente.Value;
    CRT            := 0; // SDCliente.Value;
    COD_PART       := ''; // SDCliente.Value;
    ETe1Tip        := 0; // SDCliente.Value;
    ECelTip        := 0; // SDCliente.Value;
    PTe1Tip        := 0; // SDCliente.Value;
    PCelTip        := 0; // SDCliente.Value;
    ESite          := ''; // SDCliente.Value;
    PSite          := ''; // SDCliente.Value;
    EstrangDef     := 0; // SDCliente.Value;
    EstrangTip     := 0; // SDCliente.Value;
    EstrangNum     := ''; // SDCliente.Value;
    indIEDest      := 0; // SDCliente.Value;
    L_CPF          := ''; // SDCliente.Value;
    L_Nome         := ''; // SDCliente.Value;
    LCodiPais      := 0; // SDCliente.Value;
    LEmail         := SDClienteEMAILFINANCEIRO.Value;
    L_IE           := ''; // SDCliente.Value;
    // Campos Novos!!
    ListaStatus    := SDClienteLISTASTATUS.Value;
(* N�o possuem dados !!!!
    SDClienteLOCALTRABALHO: TStringField
    SDClienteENDERECOTRABALHO: TStringField
    SDClienteFONETRABALHO: TStringField
    SDClienteTEMPOTRABALHO: TStringField
    SDClienteFUNCAOTRABALHO: TStringField
    SDClienteRENDATRABALHO: TFMTBCDField
    SDClienteNOMECONJUGE: TStringField
    SDClienteLOCALTRABALHOCONJUGE: TStringField
    SDClienteENDERECOTRABALHOCONJUGE: TStringField
    SDClienteFONETRABALHOCONJUGE: TStringField
    SDClienteTEMPOTRABALHOCONJUGE: TStringField
    SDClienteFUNCAOTRABALHOCONJUGE: TStringField
    SDClienteRENDACONJUGE: TFMTBCDField
    SDClientePAI: TStringField
    SDClienteMAE: TStringField
    SDClienteREFERENCIA1: TStringField
    SDClienteREFERENCIAFONE_1: TStringField
    SDClienteREFERENCIA2: TStringField
    SDClienteREFERENCIAFONE_2: TStringField
    SDClienteREFERENCIA3: TStringField
    SDClienteREFERENCIAFONE_3: TStringField
    //
    SDClienteINFORMACOES: TStringField
    //
    SDClienteISENTO: TStringField
    SDClienteCONDICAO: TFMTBCDField
    SDClientePARCELAS: TFMTBCDField
    SDClienteINTERVALO: TFMTBCDField
    //
    SDClienteDATACADASTRO: TDateField
    SDClienteDATAATUALIZACAO: TDateField
    //
    SDClienteUSUARIOCADASTRO: TStringField
    SDClienteUSUARIOATUALIZACAO: TStringField
    SDClienteENDERECOENTREGA: TStringField
    SDClienteOBSERVACAOCLIENTE: TStringField
    SDClienteLIBERADODOCUMENTOVENCIDO: TStringField
    SDClienteOBSERVACAOVENDA: TStringField
    SDClienteDESCONTO: TFMTBCDField
    SDClienteVENDEDOR: TFMTBCDField
    SDClienteOBSERVACAOTRAVAMENTO: TStringField
    SDClienteTRAVAMENTO: TStringField
    SDClienteLIBERADOUMAFATURA: TStringField
    SDClienteATIVO: TStringField
    SDClienteORGAOPUBLICO: TStringField
    SDClienteNCM: TStringField
    SDClientePASTAXML: TStringField
    SDClienteDIACOBRANCA: TFMTBCDField
    SDClienteOBSERVACAODIACOBRANCA: TStringField
    //
    SDClienteNAOCONTRIBUINTE: TStringField
    SDClienteCONTRIBUINTEISENTO: TStringField
    //
    SDClienteFATURAR: TStringField
    SDClienteCOBRADOR: TFMTBCDField
    SDClienteCODCTC: TFMTBCDField
    SDClienteIMPRIMEPRECOLIQUIDO: TStringField
    SDClienteNUMEROPEDIDOITEM: TStringField
    SDClienteINSCRICAOMUNICIPAL: TStringField
    SDClienteSIMPLESNACIONAL: TStringField
    SDClienteALIQUOTAISS: TFMTBCDField
    SDClienteRETENCAOISS: TStringField

*)
    //
    if UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, SQLType, 'entidades', False, [
    'CodUsu', 'RazaoSocial', 'Fantasia',
    'Respons1', 'Respons2', 'Pai',
    'Mae', 'CNPJ', 'IE',
    'NIRE', 'FormaSociet', 'Simples',
    'IEST', 'Atividade', 'Nome',
    'Apelido', 'CPF', 'CPF_Pai',
    'CPF_Conjuge', 'RG', 'SSP',
    'DataRG', 'CidadeNatal', 'EstCivil',
    'UFNatal', 'Nacionalid', 'ELograd',
    'ERua', 'ENumero', 'ECompl',
    'EBairro', 'ECidade', 'EUF',
    'ECEP', 'EPais', 'ETe1',
    'Ete2', 'Ete3', 'ECel',
    'EFax', 'EEmail', 'EContato',
    'ENatal', 'PLograd', 'PRua',
    'PNumero', 'PCompl', 'PBairro',
    'PCidade', 'PUF', 'PCEP',
    'PPais', 'PTe1', 'Pte2',
    'Pte3', 'PCel', 'PFax',
    'PEmail', 'PContato', 'PNatal',
    'Sexo', 'Responsavel', 'Profissao',
    'Cargo', 'Recibo', 'DiaRecibo',
    'AjudaEmpV', 'AjudaEmpP', 'Cliente1',
    'Cliente2', 'Cliente3', 'Cliente4',
    'Fornece1', 'Fornece2', 'Fornece3',
    'Fornece4', 'Fornece5', 'Fornece6',
    'Fornece7', 'Fornece8', 'Terceiro',
    'Cadastro', 'Informacoes', (*'Logo',*)
    'Veiculo', 'Mensal', 'Observacoes',
    'Tipo', 'CLograd', 'CRua',
    'CNumero', 'CCompl', 'CBairro',
    'CCidade', 'CUF', 'CCEP',
    'CPais', 'CTel', 'CCel',
    'CFax', 'CContato', 'LLograd',
    'LRua', 'LNumero', 'LCompl',
    'LBairro', 'LCidade', 'LUF',
    'LCEP', 'LPais', 'LTel',
    'LCel', 'LFax', 'LContato',
    'Comissao', 'Situacao', 'Nivel',
    'Grupo', 'Account', (*'Logo2',*)
    'ConjugeNome', 'ConjugeNatal', 'Nome1',
    'Natal1', 'Nome2', 'Natal2',
    'Nome3', 'Natal3', 'Nome4',
    'Natal4', 'CreditosI', 'CreditosL',
    'CreditosF2', 'CreditosD', 'CreditosU',
    'CreditosV', 'Motivo', 'QuantI1',
    'QuantI2', 'QuantI3', 'QuantI4',
    'QuantN1', 'QuantN2', 'QuantN3',
    'Agenda', 'SenhaQuer', 'Senha1',
    'LimiCred', 'Desco', 'CasasApliDesco',
    'TempA', 'TempD', 'Banco',
    'Agencia', 'ContaCorrente', 'FatorCompra',
    'AdValorem', 'DMaisC', 'DMaisD',
    'Empresa', 'CBE', 'SCB',
    'PAtividad', 'EAtividad', 'PCidadeCod',
    'ECidadeCod', 'PPaisCod', 'EPaisCod',
    'Antigo', 'CUF2', 'Contab',
    'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
    'Protestar', 'MultaCodi', 'MultaDias',
    'MultaValr', 'MultaPerc', 'MultaTiVe',
    'JuroSacado', 'CPMF', 'Corrido',
    'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
    'CartPref', 'RolComis', 'Filial',
    'EEndeRef', 'PEndeRef', 'CEndeRef',
    'LEndeRef', 'ECodMunici', 'PCodMunici',
    'CCodMunici', 'LCodMunici', 'CNAE',
    'SUFRAMA', 'ECodiPais', 'PCodiPais',
    'L_CNPJ', 'L_Ativo', 'URL',
    'CRT', 'COD_PART', 'ETe1Tip',
    'ECelTip', 'PTe1Tip', 'PCelTip',
    'ESite', 'PSite', 'EstrangDef',
    'EstrangTip', 'EstrangNum', 'indIEDest',
    'L_CPF', 'L_Nome', 'LCodiPais',
    'LEmail', 'L_IE', 'ListaStatus'], [
    'Codigo'], [
    CodUsu, RazaoSocial, Fantasia,
    Respons1, Respons2, Pai,
    Mae, CNPJ, IE,
    NIRE, FormaSociet, Simples,
    IEST, Atividade, Nome,
    Apelido, CPF, CPF_Pai,
    CPF_Conjuge, RG, SSP,
    DataRG, CidadeNatal, EstCivil,
    UFNatal, Nacionalid, ELograd,
    ERua, ENumero, ECompl,
    EBairro, ECidade, EUF,
    ECEP, EPais, ETe1,
    Ete2, Ete3, ECel,
    EFax, EEmail, EContato,
    ENatal, PLograd, PRua,
    PNumero, PCompl, PBairro,
    PCidade, PUF, PCEP,
    PPais, PTe1, Pte2,
    Pte3, PCel, PFax,
    PEmail, PContato, PNatal,
    Sexo, Responsavel, Profissao,
    Cargo, Recibo, DiaRecibo,
    AjudaEmpV, AjudaEmpP, Cliente1,
    Cliente2, Cliente3, Cliente4,
    Fornece1, Fornece2, Fornece3,
    Fornece4, Fornece5, Fornece6,
    Fornece7, Fornece8, Terceiro,
    Cadastro, Informacoes, (*Logo,*)
    Veiculo, Mensal, Observacoes,
    Tipo, CLograd, CRua,
    CNumero, CCompl, CBairro,
    CCidade, CUF, CCEP,
    CPais, CTel, CCel,
    CFax, CContato, LLograd,
    LRua, LNumero, LCompl,
    LBairro, LCidade, LUF,
    LCEP, LPais, LTel,
    LCel, LFax, LContato,
    Comissao, Situacao, Nivel,
    Grupo, Account, (*Logo2,*)
    ConjugeNome, ConjugeNatal, Nome1,
    Natal1, Nome2, Natal2,
    Nome3, Natal3, Nome4,
    Natal4, CreditosI, CreditosL,
    CreditosF2, CreditosD, CreditosU,
    CreditosV, Motivo, QuantI1,
    QuantI2, QuantI3, QuantI4,
    QuantN1, QuantN2, QuantN3,
    Agenda, SenhaQuer, Senha1,
    LimiCred, Desco, CasasApliDesco,
    TempA, TempD, Banco,
    Agencia, ContaCorrente, FatorCompra,
    AdValorem, DMaisC, DMaisD,
    Empresa, CBE, SCB,
    PAtividad, EAtividad, PCidadeCod,
    ECidadeCod, PPaisCod, EPaisCod,
    Antigo, CUF2, Contab,
    MSN1, PastaTxtFTP, PastaPwdFTP,
    Protestar, MultaCodi, MultaDias,
    MultaValr, MultaPerc, MultaTiVe,
    JuroSacado, CPMF, Corrido,
    CPF_Resp1, CliInt, AltDtPlaCt,
    CartPref, RolComis, Filial,
    EEndeRef, PEndeRef, CEndeRef,
    LEndeRef, ECodMunici, PCodMunici,
    CCodMunici, LCodMunici, CNAE,
    SUFRAMA, ECodiPais, PCodiPais,
    L_CNPJ, L_Ativo, URL,
    CRT, COD_PART, ETe1Tip,
    ECelTip, PTe1Tip, PCelTip,
    ESite, PSite, EstrangDef,
    EstrangTip, EstrangNum, indIEDest,
    L_CPF, L_Nome, LCodiPais,
    LEmail, L_IE, ListaStatus], [
    Codigo], True) then
    begin
    // Parei aqui! Fazer!
    end;
    //
    SDCliente.Next;
  end;
  MePronto.Text := 'Tabela ' + Tabela + ': ' + IntToStr(ItemAtual) +
    ' registros verificados!' + sLineBreak + MePronto.Text;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_ClienteStatus;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo, Status: Integer;
const
  OriTab = 'ClienteStatus';
  DstTab = 'EntiStatus';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Status) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDClienteStatus.Close;
    SDClienteStatus.DataSet.CommandType := ctQuery;
    SDClienteStatus.DataSet.CommandText := SQL;
    SDClienteStatus.Open;
    Continua := SDClienteStatus.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDClienteStatus.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDClienteStatus.First;
      SQLRec    := '';
      while not SDClienteStatus.Eof do
      begin
        Codigo := Integer(SDClienteStatusCLIENTE.Value);
        Status := Integer(SDClienteStatusSTATUS.Value);
        //
        if SDClienteStatus.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //Empresa,
        + PV_N(Geral.FF0(Status))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDClienteStatus.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Grupo;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo, CodUsu, NivCad: Integer;
  Nome: String;
const
  OriTab = 'Grupo';
  DstTab = 'prdgruptip';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, COdUsu, Nome, NivCad) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDGrupo.Close;
    SDGrupo.DataSet.CommandType := ctQuery;
    SDGrupo.DataSet.CommandText := SQL;
    SDGrupo.Open;
    Continua := SDGrupo.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDGrupo.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDGrupo.First;
      SQLRec    := '';
      while not SDGrupo.Eof do
      begin
        Codigo := Integer(SDGrupoGRUPO.Value);
        CodUsu := Codigo;
        Nome   := SDGrupoDESCRICAO.Value;
        NivCad := 2;
        //
        if SDGrupo.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //CodUsu,
        + PV_N(Geral.FF0(CodUsu))
        //Nome,
        + PV_N(Geral.VariavelToString(Nome))
        //NivCad
        + PV_N(Geral.FF0(NivCad))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDGrupo.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDGrupo.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Linha();
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Nivel5, Nivel4, Nivel3, Nivel2, CodUsu, //ContaCTB,
  PrdGrupTip, Tipo_Item: Integer;
  Nome: String;
const
  OriTab = 'Linha';
  DstTab = 'gragru2';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Nivel5, Nivel4, Nivel3, Nivel2, CodUsu, Nome, ', //ContaCTB,
    'PrdGrupTip, Tipo_Item) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + Geral.ATS([' lin.Descricao, prd.Grupo, ',
    'prd.Linha, COUNT(prd.Produto) ITENS ',
    'FROM produto prd ',
    'LEFT JOIN linha lin ON (lin.Linha=prd.Linha) ',
    'GROUP BY prd.Linha, prd.Grupo, lin.Descricao ',
    'ORDER BY prd.Linha, prd.Grupo ',
    '']);
    //SQL := SQL + ' FROM ' + OriTab;
    SDLinha.Close;
    SDLinha.DataSet.CommandType := ctQuery;
    SDLinha.DataSet.CommandText := SQL;
    SDLinha.Open;
    Continua := SDLinha.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDLinha.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDLinha.First;
      SQLRec    := '';
      while not SDLinha.Eof do
      begin
        Nivel5      := 0;
        Nivel4      := 0;
        Nivel3      := 0;
        Nivel2      := GetNivel2(Integer(SDLinhaGRUPO.Value), Geral.IMV(SDLinhaLinha.Value));
        CodUsu      := Nivel2;
        Nome        := Copy(SDLinhaDESCRICAO.Value, 1, 30);
        //ContaCTB,
        PrdGrupTip  := Integer(SDLinhaGRUPO.Value);
        Tipo_Item   := 1;
        //
        if SDLinha.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Nivel5,
        + Geral.FF0(Nivel5)
        //Nivel4,
        + PV_N(Geral.FF0(Nivel4))
        //Nivel3,
        + PV_N(Geral.FF0(Nivel3))
        //Nivel2,
        + PV_N(Geral.FF0(Nivel2))
        //CodUsu,
        + PV_N(Geral.FF0(CodUsu))
        //Nome,
        + PV_N(Geral.VariavelToString(Nome))
        //PrdGrupTip,
        + PV_N(Geral.FF0(PrdGrupTip))
        //Tipo_Item
        + PV_N(Geral.FF0(Tipo_Item))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDLinha.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDLinha.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Locacao();
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step, Codigo: Integer;
  Continua: Boolean;

const
  OriTab = 'Locacao';
  DstTab = 'locccon';
  FormaCobrLoca = 2;  // Na Loca��o!
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Empresa, Contrato, Cliente, DtHrEmi, DtHrBxa, ',
    'Vendedor, ECComprou, ECRetirou, NumOC, ValorTot, EndCobra, LocalObra, ',
    'LocalCntat, LocalFone, Obs0, Obs1, Obs2, Historico, TipoAluguel, DtHrSai, ',
    'ValorEquipamentos, ValorAdicional, ValorAdiantamento, ValorDesconto, ',
    'Status, Importado, ',
    //
    'DtHrDevolver, ValorLocacao, DtUltCobMens, FormaCobrLoca ',
    ') VALUES ']);
  //


  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT MAX(Locacao) FROM ' + OriTab;
  SDAux.Open;
  //
  SetLength(LocDatas, SDAux.Fields[0].AsInteger + 1);
  SetLength(LocHoras, SDAux.Fields[0].AsInteger + 1);
  SetLength(DevDatas, SDAux.Fields[0].AsInteger + 1);
  SetLength(DevHoras, SDAux.Fields[0].AsInteger + 1);
  SetLength(TipoLoc, SDAux.Fields[0].AsInteger + 1);

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDLocacao.Close;
    SDLocacao.DataSet.CommandType := ctQuery;
    SDLocacao.DataSet.CommandText := SQL;
    SDLocacao.Open;
    Continua := SDLocacao.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDLocacao.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDLocacao.First;
      SQLRec    := '';
      while not SDLocacao.Eof do
      begin
        Codigo := Integer(SDLocacaoLOCACAO.Value);
        LocDatas[Codigo] := Geral.FDT(SDLocacaoSAIDADATA.Value, 1);
        LocHoras[Codigo] := Geral.FDT(SDLocacaoSAIDAHORA.Value, 100);
        DevDatas[Codigo] := Geral.FDT(SDLocacaoDEVOLUCAODATA.Value, 1);
        DevHoras[Codigo] := Geral.FDT(SDLocacaoDEVOLUCAOHORA.Value, 100);
        TipoLoc[Codigo] := SDLocacaoTIPOALUGUEL.Value;

        ///

        if SDLocacao.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //Empresa,
        + PV_N('-11')
        //Contrato,
        + PV_N('1')
        //Cliente,
        + PV_N(Geral.FF0(Integer(SDLocacaoCLIENTE.Value)))
        //DtHrEmi,
        + PV_A(Geral.FDT(SDLocacaoEMISSAODATA.Value, 1) + ' ' + Geral.FDT(SDLocacaoEMISSAOHORA.Value, 100))
        //DtHrBxa,
        + PV_A(Geral.FDT(SDLocacaoFECHAMENTODATA.Value, 1) + ' ' + Geral.FDT(SDLocacaoFECHAMENTOHORA.Value, 100))
        //Vendedor,
        + PV_N('-1')
        //ECComprou,
        + PV_N('0')
        //ECRetirou,
        + PV_N('0')
        //NumOC,
        + PV_A('')




        //ValorTot,
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('VALORLOCACAO').AsExtended, 2, siNegativo))
        //EndCobra,
        + PV_A('')
        //LocalObra,
        + PV_N(Geral.VariavelToString(Copy(SDLocacaoObra.Value, 1, 100)))
        //LocalCntat,
        + PV_A('')
        //LocalFone,
        + PV_A('')
        //Obs0,
        + PV_A('')
        //Obs1,
        + PV_A('')
        //Obs2
        + PV_N(Geral.VariavelToString(SDLocacaoUSUARIO.Value))
        //Historico
        + PV_N(Geral.VariavelToString(SDLocacao.FieldByName('OBSERVACAO').AsVariant) + sLineBreak + Geral.VariavelToString(SDLocacaoObra.Value))
        //TipoAluguel
        + PV_A(SDLocacaoTIPOALUGUEL.Value)
        //DtHrSai,
        + PV_A(Geral.FDT(SDLocacaoSAIDADATA.Value, 1) + ' ' + Geral.FDT(SDLocacaoSAIDAHORA.Value, 100))
        //ValorEquipamentos
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('ValorEquipamento').AsExtended, 2, siNegativo))
        //ValorAdicional
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('ValorAdicional').AsExtended, 2, siNegativo))
        //ValorAdiantamento
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('ValorAdiantamento').AsExtended, 2, siNegativo))
        //ValorDesconto
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('ValorDesconto').AsExtended, 2, siNegativo))
        //Status
        + PV_N(Geral.FF0(Integer(SDLocacaoStatus.Value)))
        // Importado
        + PV_N('1') // foi importado Tisolin
        //DtHrDevolver,
        + PV_A(Geral.FDT(SDLocacaoDEVOLUCAODATA.Value, 1) + ' ' + Geral.FDT(SDLocacaoDEVOLUCAOHORA.Value, 100))
        //ValorLocacao,
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('ValorLocacao').AsExtended, 2, siNegativo))
        //DtUltCobMens
        + PV_A(Geral.FDT(SDLocacaoULTIMACOBRANCAMENSAL.Value, 1))
        //FormaCobrLoca
        + PV_N(Geral.FF0(FormaCobrLoca))
        //
        + ') ';
  //object SDLocacaoLOCACAO: TFMTBCDField                      >>> Codigo
  //object SDLocacaoCLIENTE: TFMTBCDField                      >>> Cliente
  //object SDLocacaoEMISSAODATA: TDateField                    >>> DtHrEmi
  //object SDLocacaoEMISSAOHORA: TTimeField                    >>> DtHrEmi
  //object SDLocacaoDEVOLUCAODATA: TDateField                  >>> array para itens
  //object SDLocacaoDEVOLUCAOHORA: TTimeField                  >>> array para itens
  //object SDLocacaoSAIDADATA: TDateField                      >>> DtHrSai
  //object SDLocacaoSAIDAHORA: TTimeField                      >>> DtHrSai
  //object SDLocacaoFINALIZADODATA: TDateField                 N�o usa!
  //object SDLocacaoFINALIZADOHORA: TTimeField                 N�o usa!
  //object SDLocacaoVALOREQUIPAMENTO: TFMTBCDField             >>> ValorEquipamento
  //object SDLocacaoVALORLOCACAO: TFMTBCDField                 >>> ValorTot
  //object SDLocacaoVALORADICIONAL: TFMTBCDField               >>> ValorAdicional
  //object SDLocacaoVALORADIANTAMENTO: TFMTBCDField            >>> ValorAdiantamento
  //object SDLocacaoTIPOALUGUEL: TStringField                  >>> TipoAluguel
  //object SDLocacaoOBSERVACAO: TStringField                   >>> Historico
  //object SDLocacaoFECHAMENTODATA: TDateField                 >>> DtHrBxa
  //object SDLocacaoFECHAMENTOHORA: TTimeField                 >>> DtHrBxa
  //object SDLocacaoUSUARIO: TStringField                      >>> Obs2
  //object SDLocacaoSTATUS: TFMTBCDField                       >>> Status
  //object SDLocacaoVALORDESCONTO: TFMTBCDField                >>> ValorDesconto
  //object SDLocacaoOBRA: TStringField                         >>> LocalObra
  //object SDLocacaoCANCELADA: TStringField                    >>> Usa????
  //object SDLocacaoULTIMACOBRANCAMENSAL: TDateField           >>> DtUltCobMens
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDLocacao.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_LocacaoItens();
const
  sProcName = 'TFmDB_Converte_Tisolin.ImportaTabelaFB_LocacaoItens();';
var
  SQL,
  //SQL_FIELDS_PRI, SQL_FIELDS_SEC, SQL_FIELDS_ACE, SQL_FIELDS_USO,
  //SQL_FIELDS_CNS,
  SQL_FIELDS_LOC_ANT, SQL_FIELDS_VEN_ANT,
  SQL_FIELDS_PRI, SQL_FIELDS_SEC, SQL_FIELDS_ACE,
  SQL_VALUES_PRI, SQL_VALUES_SEC, SQL_VALUES_ACE,
  SQLRec_PRI, SQLRec_SEC, SQLRec_ACE, SQLRec_USO, SQLRec_CNS,
  SQL_VALUES_LOC_ANT, SQL_VALUES_VEN_ANT,
  SQLRec_LOC_ANT, SQLRec_VEN_ANT: String;
  ItsOK, Total, Step, Codigo, CtrIDAtu: Integer;
  Continua: Boolean;
  //
  CATEGORIA: String;
  //
  //CtrID,
  GraGruX,
  CodAnt, CodAtu: Integer;
  ErrosTxt: String;
  ErrosCount: Integer;
  TROCA, COBRANCALOCACAO, COBRANCAREALIZADAVENDA, DATAINICIALCOBRANCA,
  HORAINICIALCOBRANCA, DATASAIDA, HORASAIDA: String;
  LOCACAO, SEQUENCIA, PRODUTO, QUANTIDADEPRODUTO, QUANTIDADELOCACAO,
  QUANTIDADEDEVOLUCAO: Integer;
  VALORPRODUTO, VALORLOCACAO, COBRANCACONSUMO: Double;
  SQLType: TSQLType;
  I, Acessorios_Count, GGXPrincipal: Integer;
  EhAcessorio: Boolean;
  TabelaIts: TTabLocIts;
  ValorDia, ValorSem, ValorQui, ValorMes: Double;
  RetFunci, RetExUsr, LibExUsr, LibFunci, QtdeLocacao, QtdeDevolucao, Item,
  CtrID_LOC_ANT, CtrID_VEN_ANT, CtrID_PRI, Item_SEC, Item_ACE, Trocado: Integer;
  DtHrLocado, DtHrRetorn, LibDtHr, RELIB, HOLIB, CobranIniDtHr, SaiDtHr,
  TipoAluguel: String;
  TemDatasLocacao: Boolean;


const
  OriTab = 'LocacaoItens';
  DstTab_PRI = 'loccpatpri';
  DstTab_SEC = 'loccpatsec';
  DstTab_ACE = 'loccpatace';
  DstTab_USO = 'loccpatuso';
  DstTab_CNS = 'loccpatcns';
  DstTab_LOC_ANT = 'locclocant';
  DstTab_VEN_ANT = 'loccvenant';
begin
  ErrosTxt := '';
  ErrosCount := 0;
  TemDatasLocacao := Length(LocDatas) > 0;
  if TemDatasLocacao = False then
  if Geral.MB_Pergunta('ATEN��O!!!!!!!!!!!!' + sLineBreak +
  'As datas de loca��o de itens s�o definidas ao importar o cabe�alho da loca��o antes!'
  + sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then Exit;

{

////////////////////////////////////////////////////////////////////////////////
  Precisa cadastrar os produtos pimeiro!
////////////////////////////////////////////////////////////////////////////////
}


  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Verificando produtos que s�o acess�rios');
  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT DISTINCT ACESSORIO FROM PRODUTOACESSORIO ORDER BY ACESSORIO';
  SDAux.Open;
  //
  SDAux.Last;
  Acessorios_Count := SDAux.FieldByName('ACESSORIO').AsInteger + 1;
  SetLength(Acessorio, Acessorios_Count);
  for I := 0 to Acessorios_Count -1 do
    Acessorio[I] := False;
  //
  SDAux.First;
  while not SDAux.Eof do
  begin
    Acessorio[Integer(SDAux.FieldByName('ACESSORIO').AsInteger)] := True;
    //
    SDAux.Next;
  end;
  //
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS_PRI := Geral.ATS([
    'INSERT INTO ' + DstTab_PRI + ' ( ',
    'CtrID, ',
    'Codigo, GraGruX, ValorDia, ',
    'ValorSem, ValorQui, ValorMes, ',
    'RetFunci, RetExUsr, DtHrLocado, ',
    'DtHrRetorn, LibExUsr, LibFunci, ',
    'LibDtHr, RELIB, HOLIB, ',
    'QtdeProduto, ValorProduto, QtdeLocacao, ValorLocacao, ',
    'QtdeDevolucao, Troca, Categoria, ',
    'COBRANCALOCACAO, CobrancaConsumo, CobrancaRealizadaVenda, ',
    'CobranIniDtHr, SaiDtHr ',
    ') VALUES ']);


    //
  SQL_FIELDS_SEC := Geral.ATS([
    'INSERT INTO ' + DstTab_SEC + ' ( ',
    'Item, Codigo, CtrID, GraGruX, ',
    'QtdeProduto, ValorProduto, QtdeLocacao, ValorLocacao, ',
    'QtdeDevolucao, Troca, Categoria, ',
    'COBRANCALOCACAO, CobrancaConsumo, CobrancaRealizadaVenda, ',
    'CobranIniDtHr, SaiDtHr ',
    ') VALUES ']);
  //
  SQL_FIELDS_ACE := Geral.ATS([
    'INSERT INTO ' + DstTab_ACE + ' ( ',
    'Item, Codigo, CtrID, GraGruX, ',
    'ValBem, QtdeProduto, ValorProduto, QtdeLocacao, ',
    'ValorLocacao, QtdeDevolucao, Troca, ',
    'Categoria, COBRANCALOCACAO, CobrancaConsumo, ',
    'CobrancaRealizadaVenda, CobranIniDtHr, SaiDtHr ',
    ') VALUES ']);
{
  //
  SQL_FIELDS_USO := Geral.ATS([
    'INSERT INTO ' + DstTab_USO + ' ( ',
    'Item, Codigo, CtrID, GraGruX, ',
    'Unidade, AvalIni, AvalFim, PrcUni, ValTot ',
    ') VALUES ']);
  //
  SQL_FIELDS_CNS := Geral.ATS([
    'INSERT INTO ' + DstTab_CNS + ' ( ',
    'Item, Codigo, CtrID, GraGruX, ',
    'Unidade, QtdIni, QtdFim, ',
    'QtdUso, PrcUni, ValUso ',
    ') VALUES ']);
  //
}
  SQL_FIELDS_LOC_ANT := Geral.ATS([
    'INSERT INTO ' + DstTab_LOC_ANT + ' ( ',
    'Item, Codigo, CtrID, GraGruX, LOCACAO, ',
    'SEQUENCIA, PRODUTO, QUANTIDADEPRODUTO, ',
    'VALORPRODUTO, QUANTIDADELOCACAO, VALORLOCACAO, ',
    'QUANTIDADEDEVOLUCAO, TROCA, CATEGORIA, ',
    'COBRANCALOCACAO, COBRANCACONSUMO, COBRANCAREALIZADAVENDA, ',
    'DATAINICIALCOBRANCA, HORAINICIALCOBRANCA, DATASAIDA, ',
    'HORASAIDA',
    ') VALUES ']);
  //
  SQL_FIELDS_VEN_ANT := Geral.ATS([
    'INSERT INTO ' + DstTab_VEN_ANT + ' ( ',
    'Item, Codigo, CtrID, GraGruX, LOCACAO, ',
    'SEQUENCIA, PRODUTO, QUANTIDADEPRODUTO, ',
    'VALORPRODUTO, QUANTIDADELOCACAO, VALORLOCACAO, ',
    'QUANTIDADEDEVOLUCAO, TROCA, CATEGORIA, ',
    'COBRANCALOCACAO, COBRANCACONSUMO, COBRANCAREALIZADAVENDA, ',
    'DATAINICIALCOBRANCA, HORAINICIALCOBRANCA, DATASAIDA, ',
    'HORASAIDA',
    ') VALUES ']);
  //

  ItsOK := 0;
  CtrID_LOC_ANT  := 0;
  CtrID_VEN_ANT  := 0;
  CtrID_PRI := 0;
  Item_ACE  := 0;
  Item_SEC  := 0;

  //

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Limpando tabelas de itens de loca��o');
  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados PRI. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_PRI);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados SEC. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_SEC);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados ACE. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_ACE);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados USO. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_USO);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados CNS. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_CNS);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados ANT LOC. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_LOC_ANT);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados ANT VEN. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_VEN_ANT);
  //
  //QrUpd.SQL.Text := '';
  //
  CodAnt := 0;
  CodAtu := 0;
  CtrIDAtu := 0;
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'iniciando importa��o');
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    ///////////////////////////////////////////////////////////////////////////
    SQL := SQL + sLineBreak + 'ORDER BY LOCACAO, SEQUENCIA';
    ///////////////////////////////////////////////////////////////////////////
    ///
    SDLocacaoItens.Close;
    SDLocacaoItens.DataSet.CommandType := ctQuery;
    SDLocacaoItens.DataSet.CommandText := SQL;
    SDLocacaoItens.Open;
    Continua := SDLocacaoItens.RecordCount > 0;

    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDLocacaoItens.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES_LOC_ANT := '';
      SQL_VALUES_VEN_ANT := '';
      SDLocacaoItens.First;
      SQLRec_LOC_ANT     := '';
      SQLRec_VEN_ANT     := '';
      SQLRec_PRI         := '';
      SQLRec_ACE         := '';
      SQLRec_SEC         := '';
      SQLRec_USO         := '';
      SQLRec_CNS         := '';
      while not SDLocacaoItens.Eof do
      begin
        Codigo    := Integer(SDLocacaoItensLOCACAO.Value);
        GraGruX   := Integer(SDLocacaoItensPRODUTO.Value);
        CATEGORIA := SDLocacaoItensCATEGORIA.Value;
        //
        {
        if Codigo <> CodAtu then
        begin
          CodAnt := CodAtu;
          CodAtu := Codigo;
          CtrIDAtu := 0;
          QuantidadeLocacao := SDLocacaoItens.FieldByName('QUANTIDADELOCACAO').AsExtended;
          ValorLocacao := SDLocacaoItens.FieldByName('VALORLOCACAO').AsExtended;
          if (QuantidadeLocacao > 0.001) and (ValorLocacao > 0.001) then
          begin
            CtrID := CtrID + 1;
            CtrIDAtu := CtrID;
          end else
          begin
            ErrosCount := ErrosCount + 1;
            ErrosTxt := ErrosTxt + Geral.FF0(Codigo) + ',' + sLineBreak;
          end;
        end;
        }

        //Buscar aqui todos valores de campos antes de definir SQLs!

        LOCACAO                := Integer(SDLOCACAOITENSLOCACAO.Value);
        SEQUENCIA              := Integer(SDLOCACAOITENSSEQUENCIA.Value);
        PRODUTO                := Integer(SDLOCACAOITENSPRODUTO.Value); // GraGruX
        QUANTIDADEPRODUTO      := Integer(SDLOCACAOITENSQUANTIDADEPRODUTO.Value);
        VALORPRODUTO           := SDLocacaoItens.FieldByName('VALORPRODUTO').AsExtended;
        QUANTIDADELOCACAO      := Integer(SDLOCACAOITENSQUANTIDADELOCACAO.Value);
        VALORLOCACAO           := SDLocacaoItens.FieldByName('VALORLOCACAO').AsExtended;
        QUANTIDADEDEVOLUCAO    := Integer(SDLOCACAOITENSQUANTIDADEDEVOLUCAO.Value);
        TROCA                  := SDLOCACAOITENSTROCA.Value;
        if TROCA = 'S' then
          Trocado := 1
        else
          Trocado := 0;
        CATEGORIA              := UPPERCASE(SDLOCACAOITENSCATEGORIA.Value);
        COBRANCALOCACAO        := SDLOCACAOITENSCOBRANCALOCACAO.Value;
        COBRANCACONSUMO        := SDLocacaoItens.FieldByName('COBRANCACONSUMO').AsExtended;
        COBRANCAREALIZADAVENDA := SDLOCACAOITENSCOBRANCAREALIZADAVENDA.Value;
        DATAINICIALCOBRANCA    := Geral.FDT(SDLOCACAOITENSDATAINICIALCOBRANCA.Value, 1);
        HORAINICIALCOBRANCA    := Geral.FDT(SDLOCACAOITENSHORAINICIALCOBRANCA.Value, 100);
        DATASAIDA              := Geral.FDT(SDLOCACAOITENSDATASAIDA.Value, 1);
        HORASAIDA              := Geral.FDT(SDLOCACAOITENSHORASAIDA.Value, 100);
        //
        if HORAINICIALCOBRANCA = '' then
          HORAINICIALCOBRANCA := '00:00:00';
        if HORASAIDA = '' then
          HORASAIDA := '00:00:00';




        if Codigo <> CodAtu then
        begin
          CodAnt := CodAtu;
          CodAtu := Codigo;
          CtrIDAtu := 0;
          GGXPrincipal := 0;


          (*
          QuantidadeLocacao := SDLocacaoItens.FieldByName('QUANTIDADELOCACAO').AsExtended;
          ValorLocacao := SDLocacaoItens.FieldByName('VALORLOCACAO').AsExtended;
          if (QuantidadeLocacao > 0.001) and (ValorLocacao > 0.001) then
          begin
            CtrID := CtrID + 1;
            CtrIDAtu := CtrID;
          end else
          begin
            ErrosCount := ErrosCount + 1;
            ErrosTxt := ErrosTxt + Geral.FF0(Codigo) + ',' + sLineBreak;
          end;
          *)
        end;



        if (Uppercase(CATEGORIA) = 'L') // Loca��o?
        or (Uppercase(CATEGORIA) = 'M') then // Manuten��o?
        begin
          if GGXPrincipal = 0 then
            TabelaIts := TTabLocIts.tliPrincipal
          else
          if ValorLocacao > 0 then
            TabelaIts := TTabLocIts.tliPrincipal
          else
          if Acessorio[GraGruX] = True then
            TabelaIts := TTabLocIts.tliAcessorio
          else
            TabelaIts := TTabLocIts.tliSecundario;
          //
          case TabelaIts of
            TTabLocIts.tliPrincipal:
            begin
              GGXPrincipal := GraGruX;
              CtrID_PRI := CtrID_PRI + 1;

              //

              //loccpatpri
              //CtrID
              //Codigo
              //GraGruX
              ValorDia := 0.00;
              ValorSem := 0.00;
              ValorQui := 0.00;
              ValorMes := 0.00;
              RetFunci               := 0;
              RetExUsr               := 0;
              if TemDatasLocacao then
              begin
                DtHrLocado           := LocDatas[Codigo] + ' ' + LocHoras[Codigo];
                DtHrRetorn           := DevDatas[Codigo] + ' ' + DevHoras[Codigo];
                TipoAluguel          := TipoLoc[Codigo];
              end else
              begin
                DtHrLocado           := '0000-00-00 00:00:00';
                DtHrRetorn           := '0000-00-00 00:00:00';
                TipoAluguel          := '?';
              end;
              if TipoAluguel = 'D' then
                ValorDia := VALORLOCACAO
              else
              if TipoAluguel = 'S' then
                ValorSem := VALORLOCACAO
              else
              if TipoAluguel = 'Q' then
                ValorQui := VALORLOCACAO
              else
              if TipoAluguel = 'M' then
                ValorMes := VALORLOCACAO
              else
                ValorDia := VALORLOCACAO;
              //
              LibExUsr               := 0;
              LibFunci               := 0;
              LibDtHr                := '0000-00-00 00:00:00';
              RELIB                  := '';
              HOLIB                  := '';
              //ValorProduto           :=
              QtdeLocacao            := QUANTIDADELOCACAO;
              ValorLocacao           := VALORLOCACAO;
              QtdeDevolucao          := QUANTIDADEDEVOLUCAO;
              //Troca
              //Categoria
              //COBRANCALOCACAO
              //CobrancaConsumo
              //CobrancaRealizadaVenda
              CobranIniDtHr           := DATAINICIALCOBRANCA + ' ' + HORAINICIALCOBRANCA;
              SaiDtHr                 := DATASAIDA + ' ' + HORASAIDA;

              //

              if SQLRec_PRI = EmptyStr then
                SQLRec_PRI    := ' ('
              else
                SQLRec_PRI    := ', (';
              //
              SQLRec_PRI := SQLRec_PRI
              //CtrID
              + Geral.FF0(CtrID_PRI)
              //Codigo
              + PV_N(Geral.FF0(Codigo))
              //GraGruX
              + PV_N(Geral.FF0(GraGruX))
              //ValorDia
              + PV_N(Geral.FFT_Dot(ValorDia, 2, siNegativo))
              //ValorSem
              + PV_N(Geral.FFT_Dot(ValorSem, 2, siNegativo))
              //ValorQui
              + PV_N(Geral.FFT_Dot(ValorQui, 2, siNegativo))
              //ValorMes
              + PV_N(Geral.FFT_Dot(ValorMes, 2, siNegativo))
              //RetFunci
              + PV_N(Geral.FF0(RetFunci))
              //RetExUsr
              + PV_N(Geral.FF0(RetExUsr))
              //DtHrLocado
              + PV_A(DtHrLocado)
              //DtHrRetorn
              + PV_A(DtHrRetorn)
              //LibExUsr
              + PV_N(Geral.FF0(LibExUsr))
              //LibFunci
              + PV_N(Geral.FF0(LibFunci))
              //LibDtHr
              + PV_A(LibDtHr)
              // RELIB
              + PV_A(RELIB)
              //HOLIB
              + PV_A(HOLIB)
              //QtdeProduto
              + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
              //VALORPRODUTO
              + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
              //QtdeLocacao
              + PV_N(Geral.FF0(QUANTIDADELOCACAO))
              //ValorLocacao
              + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
              //QtdeDevolucao
              + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
              //TROCA
              + PV_N(Geral.FF0(TROCADO))
              //CATEGORIA
              + PV_A(CATEGORIA)
              //COBRANCALOCACAO
              + PV_A(COBRANCALOCACAO)
              //COBRANCACONSUMO
              + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
              //COBRANCAREALIZADAVENDA
              + PV_A(COBRANCAREALIZADAVENDA)
              //CobranIniDtHr
              + PV_A(CobranIniDtHr)
              //SaiDtHr
              + PV_A(SaiDtHr)
              //
              + ') ';
              //
              SQL_VALUES_PRI := SQL_VALUES_PRI + sLineBreak + SQLRec_PRI;

            end;
            //
            TTabLocIts.tliAcessorio:
            begin
              Item_ACE := ITEM_ACE + 1;
            //

              //

              if SQLRec_ACE = EmptyStr then
                SQLRec_ACE    := ' ('
              else
                SQLRec_ACE    := ', (';
              //
              SQLRec_ACE := SQLRec_ACE
              //Item
              + Geral.FF0(Item_ACE)
              //Codigo
              + PV_N(Geral.FF0(Codigo))
              //CtrID
              + PV_N(Geral.FF0(CtrID_PRI))
              //GraGruX
              + PV_N(Geral.FF0(GraGruX))
(*
              //ValorDia
              + PV_N(Geral.FFT_Dot(ValorDia, 2, siNegativo))
              //ValorSem
              + PV_N(Geral.FFT_Dot(ValorSem, 2, siNegativo))
              //ValorQui
              + PV_N(Geral.FFT_Dot(ValorQui, 2, siNegativo))
              //ValorMes
              + PV_N(Geral.FFT_Dot(ValorMes, 2, siNegativo))
              //RetFunci
              + PV_N(Geral.FF0(RetFunci))
              //RetExUsr
              + PV_N(Geral.FF0(RetExUsr))
              //DtHrLocado
              + PV_A(DtHrLocado)
              //DtHrRetorn
              + PV_A(DtHrRetorn)
              //LibExUsr
              + PV_N(Geral.FF0(LibExUsr))
              //LibFunci
              + PV_N(Geral.FF0(LibFunci))
              //LibDtHr
              + PV_A(LibDtHr)
              // RELIB
              + PV_A(RELIB)
              //HOLIB
              + PV_A(HOLIB)
*)
              //ValBem
              + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
              //QtdeProduto
              + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
              //VALORPRODUTO
              + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
              //QtdeLocacao
              + PV_N(Geral.FF0(QUANTIDADELOCACAO))
              //ValorLocacao
              + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
              //QtdeDevolucao
              + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
              //TROCA
              + PV_N(Geral.FF0(TROCADO))
              //CATEGORIA
              + PV_A(CATEGORIA)
              //COBRANCALOCACAO
              + PV_A(COBRANCALOCACAO)
              //COBRANCACONSUMO
              + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
              //COBRANCAREALIZADAVENDA
              + PV_A(COBRANCAREALIZADAVENDA)
              //CobranIniDtHr
              + PV_A(CobranIniDtHr)
              //SaiDtHr
              + PV_A(SaiDtHr)
              //
              + ') ';
              //
              SQL_VALUES_ACE := SQL_VALUES_ACE + sLineBreak + SQLRec_ACE;

            end;
            //
            TTabLocIts.tliSecundario:
            begin
              Item_SEC := ITEM_SEC + 1;
            //

              //

              if SQLRec_SEC = EmptyStr then
                SQLRec_SEC    := ' ('
              else
                SQLRec_SEC    := ', (';
              //
              SQLRec_SEC := SQLRec_SEC
              //Item
              + Geral.FF0(Item_SEC)
              //Codigo
              + PV_N(Geral.FF0(Codigo))
              //CtrID
              + PV_N(Geral.FF0(CtrID_PRI))
              //GraGruX
              + PV_N(Geral.FF0(GraGruX))
(*
              //ValorDia
              + PV_N(Geral.FFT_Dot(ValorDia, 2, siNegativo))
              //ValorSem
              + PV_N(Geral.FFT_Dot(ValorSem, 2, siNegativo))
              //ValorQui
              + PV_N(Geral.FFT_Dot(ValorQui, 2, siNegativo))
              //ValorMes
              + PV_N(Geral.FFT_Dot(ValorMes, 2, siNegativo))
              //RetFunci
              + PV_N(Geral.FF0(RetFunci))
              //RetExUsr
              + PV_N(Geral.FF0(RetExUsr))
              //DtHrLocado
              + PV_A(DtHrLocado)
              //DtHrRetorn
              + PV_A(DtHrRetorn)
              //LibExUsr
              + PV_N(Geral.FF0(LibExUsr))
              //LibFunci
              + PV_N(Geral.FF0(LibFunci))
              //LibDtHr
              + PV_A(LibDtHr)
              // RELIB
              + PV_A(RELIB)
              //HOLIB
              + PV_A(HOLIB)
*)
(*
              //ValBem
              + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
*)
              //QtdeProduto
              + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
              //VALORPRODUTO
              + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
              //QtdeLocacao
              + PV_N(Geral.FF0(QUANTIDADELOCACAO))
              //ValorLocacao
              + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
              //QtdeDevolucao
              + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
              //TROCA
              + PV_N(Geral.FF0(TROCADO))
              //CATEGORIA
              + PV_A(CATEGORIA)
              //COBRANCALOCACAO
              + PV_A(COBRANCALOCACAO)
              //COBRANCACONSUMO
              + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
              //COBRANCAREALIZADAVENDA
              + PV_A(COBRANCAREALIZADAVENDA)
              //CobranIniDtHr
              + PV_A(CobranIniDtHr)
              //SaiDtHr
              + PV_A(SaiDtHr)
              //
              + ') ';
              //
              SQL_VALUES_SEC := SQL_VALUES_SEC + sLineBreak + SQLRec_SEC;

            end;
          end;




















          CtrID_LOC_ANT := CtrID_LOC_ANT + 1;

          if SQLRec_LOC_ANT = EmptyStr then
            SQLRec_LOC_ANT    := ' ('
          else
            SQLRec_LOC_ANT    := ', (';
          //
          SQLRec_LOC_ANT := SQLRec_LOC_ANT
          //Item
          + Geral.FF0(CtrID_LOC_ANT)
          //Codigo
          + PV_N(Geral.FF0(Codigo))
          //CtrID
          + PV_N(Geral.FF0(CtrID_LOC_ANT))
          //GraGruX
          + PV_N(Geral.FF0(GraGruX))
          //Locacao
          + PV_N(Geral.FF0(Locacao))
          //Sequencia
          + PV_N(Geral.FF0(Sequencia))
          //Produto
          + PV_N(Geral.FF0(Produto))
          //QUANTIDADEPRODUTO
          + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
          //VALORPRODUTO
          + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
          //QUANTIDADELOCACAO
          + PV_N(Geral.FF0(QUANTIDADELOCACAO))
          //VALORLOCACAO
          + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
          //QUANTIDADEDEVOLUCAO
          + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
          //TROCA
          + PV_N(Geral.VariavelToString(TROCA))
          //CATEGORIA
          + PV_N(Geral.VariavelToString(CATEGORIA))
          //COBRANCALOCACAO
          + PV_N(Geral.VariavelToString(COBRANCALOCACAO))
          //COBRANCACONSUMO
          + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
          //COBRANCAREALIZADAVENDA
          + PV_N(Geral.VariavelToString(COBRANCAREALIZADAVENDA))
          //DATAINICIALCOBRANCA
          + PV_A(DATAINICIALCOBRANCA)
          //HORAINICIALCOBRANCA
          + PV_A(HORAINICIALCOBRANCA)
          //DATASAIDA
          + PV_A(DATASAIDA)
          //HORASAIDA
          + PV_A(HORASAIDA)
          //
          + ') ';
          //
          SQL_VALUES_LOC_ANT := SQL_VALUES_LOC_ANT + sLineBreak + SQLRec_LOC_ANT;
        end;
        //Venda
        if Uppercase(CATEGORIA) = 'V' then
        begin
          CtrID_VEN_ANT      := CtrID_VEN_ANT + 1;

(*
          LOCACAO                := Codigo;
          SEQUENCIA              := Integer(SDLocacaoItensSEQUENCIA.Value);
          PRODUTO                := GraGruX;
          QUANTIDADEPRODUTO      := Integer(SDLocacaoItensQUANTIDADEPRODUTO.Value);
          VALORPRODUTO           := SDLocacaoItens.FieldByName('VALORPRODUTO').AsExtended;
          QUANTIDADELOCACAO      := Integer(SDLocacaoItensQUANTIDADELOCACAO.Value);
          VALORLOCACAO           := SDLocacaoItens.FieldByName('VALORLOCACAO').AsExtended;
          QUANTIDADEDEVOLUCAO    := Integer(SDLocacaoItensQUANTIDADEDEVOLUCAO.Value);
          TROCA                  := SDLocacaoItensTROCA.Value;
          CATEGORIA              := SDLocacaoItensCATEGORIA.Value;
          COBRANCALOCACAO        := SDLocacaoItensCOBRANCALOCACAO.Value;
          COBRANCACONSUMO        := SDLocacaoItens.FieldByName('COBRANCACONSUMO').AsExtended;
          COBRANCAREALIZADAVENDA := SDLocacaoItensCOBRANCAREALIZADAVENDA.Value;
          DATAINICIALCOBRANCA    := Geral.FDT(SDLocacaoItensDATAINICIALCOBRANCA.Value, 1);
          HORAINICIALCOBRANCA    := Geral.FDT(SDLocacaoItensHORAINICIALCOBRANCA.Value, 100);
          if HORAINICIALCOBRANCA = '' then
            HORAINICIALCOBRANCA := '00:00:00';
          DATASAIDA              := Geral.FDT(SDLocacaoItensDATASAIDA.Value, 1);
          HORASAIDA              := Geral.FDT(SDLocacaoItensHORASAIDA.Value, 100);
          if HORASAIDA = '' then
            HORASAIDA := '00:00:00';
          //
*)
          if SQLRec_VEN_ANT = EmptyStr then
            SQLRec_VEN_ANT    := ' ('
          else
            SQLRec_VEN_ANT    := ', (';
          //
          SQLRec_VEN_ANT := SQLRec_VEN_ANT
          //Item
          + Geral.FF0(CtrID_VEN_ANT)
          //Codigo
          + PV_N(Geral.FF0(Codigo))
          //CtrID
          + PV_N(Geral.FF0(CtrID_VEN_ANT))
          //GraGruX
          + PV_N(Geral.FF0(GraGruX))
          //Locacao
          + PV_N(Geral.FF0(Locacao))
          //Sequencia
          + PV_N(Geral.FF0(Sequencia))
          //Produto
          + PV_N(Geral.FF0(Produto))
          //QUANTIDADEPRODUTO
          + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
          //VALORPRODUTO
          + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
          //QUANTIDADELOCACAO
          + PV_N(Geral.FF0(QUANTIDADELOCACAO))
          //VALORLOCACAO
          + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
          //QUANTIDADEDEVOLUCAO
          + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
          //TROCA
          + PV_N(Geral.VariavelToString(TROCA))
          //CATEGORIA
          + PV_N(Geral.VariavelToString(CATEGORIA))
          //COBRANCALOCACAO
          + PV_N(Geral.VariavelToString(COBRANCALOCACAO))
          //COBRANCACONSUMO
          + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
          //COBRANCAREALIZADAVENDA
          + PV_N(Geral.VariavelToString(COBRANCAREALIZADAVENDA))
          //DATAINICIALCOBRANCA
          + PV_A(DATAINICIALCOBRANCA)
          //HORAINICIALCOBRANCA
          + PV_A(HORAINICIALCOBRANCA)
          //DATASAIDA
          + PV_A(DATASAIDA)
          //HORASAIDA
          + PV_A(HORASAIDA)
          //
          + ') ';
          //
          SQL_VALUES_VEN_ANT := SQL_VALUES_VEN_ANT + sLineBreak + SQLRec_VEN_ANT;
        end;
        SDLocacaoItens.Next;
      end;

      //



















      if SQL_VALUES_PRI <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_PRI + SQL_VALUES_PRI;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_PRI + SQL_VALUES_PRI);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_PRI := EmptyStr;


      //


      if SQL_VALUES_ACE <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_ACE + SQL_VALUES_ACE;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_ACE + SQL_VALUES_ACE);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_ACE := EmptyStr;


      //


      if SQL_VALUES_SEC <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_SEC + SQL_VALUES_SEC;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_SEC + SQL_VALUES_SEC);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_SEC := EmptyStr;


      //


      if SQL_VALUES_LOC_ANT <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_LOC_ANT + SQL_VALUES_LOC_ANT;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_LOC_ANT + SQL_VALUES_LOC_ANT);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_LOC_ANT := EmptyStr;


      //


      if SQL_VALUES_VEN_ANT <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_VEN_ANT + SQL_VALUES_VEN_ANT;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_VEN_ANT + SQL_VALUES_VEN_ANT);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_VEN_ANT := EmptyStr;


      //


      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDLocacaoItens.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
  //
  AtualizaSaldosLocados();
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Marca;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo, Controle: Integer;
  Nome: String;
const
  OriTab = 'Marca';
  DstTab = 'grafabmar';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Controle, Nome) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDMarca.Close;
    SDMarca.DataSet.CommandType := ctQuery;
    SDMarca.DataSet.CommandText := SQL;
    SDMarca.Open;
    Continua := SDMarca.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDMarca.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDMarca.First;
      SQLRec    := '';
      while not SDMarca.Eof do
      begin
        Codigo   := 0;
        Controle := SDMarca.RecNo;
        Nome     := SDMarcaMARCA.Value;
        //
        if SDMarca.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //Controle,
        + PV_N(Geral.FF0(Controle))
        //Nome
        + PV_N(Geral.VariavelToString(Nome))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDMarca.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDMarca.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Produto();
var

  SQL_FIELDS_UNIDMED,  SQL_FIELDS_XXX,
  SQL_FIELDS_GraGruX, SQL_VALUES_GraGruX, SQLRec_GraGruX,
  SQL_FIELDS_GraGru1, SQL_VALUES_GraGru1, SQLRec_GraGru1,
  SQL_FIELDS_GraGruC, SQL_VALUES_GraGruC, SQLRec_GraGruC,
  SQL_FIELDS_GraGXPatr, SQL_VALUES_GraGXPatr, SQLRec_GraGXPatr,
  SQL_FIELDS_GraGXOutr, SQL_VALUES_GraGXOutr, SQLRec_GraGXOutr,
  SQL_FIELDS_GraGruEPat, SQL_VALUES_GraGruEPat, SQLRec_GraGruEPat,
  SQL: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Controle, GraGruC, GraGru1: Integer;


  //

////////////////////////////////////////////////////////////////////////////////
///  GraGru1
  Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, CodUsu: Integer;
  Nome: String;
  PrdGrupTip, GraTamCad, UnidMed: Integer;
(*
  CST_A, CST_B,
*)
  NCM: String;
(*
Peso, IPI_Alq, IPI_CST,
IPI_cEnq, IPI_vUnid, IPI_TpTrib,
TipDimens, PerCuztMin, PerCuztMax,
MedOrdem, PartePrinc, InfAdProd,
SiglaCustm, HowBxaEstq, GerBxaEstq,
PIS_CST, PIS_AlqP, PIS_AlqV,
PISST_AlqP, PISST_AlqV, COFINS_CST,
COFINS_AlqP, COFINS_AlqV, COFINSST_AlqP,
COFINSST_AlqV, ICMS_modBC, ICMS_modBCST,
ICMS_pRedBC, ICMS_pRedBCST, ICMS_pMVAST,
ICMS_pICMSST, ICMS_Pauta, ICMS_MaxTab,
*)
  IPI_pIPI: Double;

  cGTIN_EAN: String;
(*
  EX_TIPI,
PIS_pRedBC, PISST_pRedBCST, COFINS_pRedBC,
COFINSST_pRedBCST, ICMSRec_pRedBC, IPIRec_pRedBC,
PISRec_pRedBC, COFINSRec_pRedBC, ICMSRec_pAliq,
IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq,
ICMSRec_tCalc, IPIRec_tCalc, PISRec_tCalc,
COFINSRec_tCalc, ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA,
FatorClas, prod_indTot,
*)
  Referencia: String;
(*
Fabricante, PerComissF, PerComissR,
PerComissZ, NomeTemp, COD_LST,
SPEDEFD_ALIQ_ICMS, DadosFisc, GraTabApp,
ICMS_pDif, ICMS_pICMSDeson, ICMS_motDesICMS,
prod_CEST, prod_indEscala
*)

////////////////////////////////////////////////////////////////////////////////
///  GraGXPatr
var
(*  Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia, Capacid,
  VendaData, VendaDocu,*) Observa, AGRPAT, (*CLVPAT,*) MARPAT(*, DescrTerc*)
  : String;
  (*GraGruX, Situacao, Agrupador,*) Marca, (*VendaEnti,*) Aplicacao: Integer;
  AquisValr, AtualValr, ValorMes, ValorQui, ValorSem, ValorDia, VendaValr: Double;
  //
  _ATIVO, CATEGORIA: String;
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
///  GraGXPatr
  GraGruX, ItemUnid: Integer;
  ItemValr: Double;
////////////////////////////////////////////////////////////////////////////////
const
  OriTab   = 'Produto';
  GraTamI  = 1;
  EAN13    = '';
  COD_ITEM = '';
var
  DstTab, DstTab_GGX, DstTab_GG1, DstTab_GGC, DstTab_Patr, DstTab_Outr,
  DstTab_GGEPat: String; //= 'gragrux';
  GraGruY, I: Integer;
  //
  EquipamentosErr: String;
  GraGruYs: array of String[1];
  //
  TipoProd: String[1];
  Estoque: Double;
  Ativo: Integer;
begin
{ Fazer assim?????     2020-10-01
SELECT ggx.GraGruY, Its.*
FROM zzz_fb_db_ant.LOCACAOITENS its
LEFT JOIN toolrent_tisolin.GraGruX ggx
  ON ggx.Controle=its.PRODUTO
/*WHERE its.CATEGORIA = "L"*/
ORDER BY ggx.GraGruY, its.LOCACAO, its.SEQUENCIA




tem esse erro atualmente!

DROP TABLE IF EXISTS _REDUZIDOS_PRI_;
CREATE TABLE _REDUZIDOS_PRI_
SELECT DISTINCT GraGruX
FROM loccpatpri pri
;

DROP TABLE IF EXISTS _REDUZIDOS_SEC_;
CREATE TABLE _REDUZIDOS_SEC_
SELECT DISTINCT GraGruX
FROM loccpatsec pri
;

SELECT pri.GraGruX GgxPri, sec.GraGruX GgxSec
FROM _REDUZIDOS_PRI_ pri
LEFT JOIN _REDUZIDOS_SEC_ sec ON sec.GraGruX=pri.GraGruX
;



Erros doe erros!
SELECT ggx.GraGruY, Its.*
FROM zzz_fb_db_ant.LOCACAOITENS its
LEFT JOIN toolrent_tisolin.GraGruX ggx
  ON ggx.Controle=its.PRODUTO
WHERE its.CATEGORIA = "L"
AND its.LOCACAO IN(
12,15,23,26,28,29,30,32,33,36,42,43,49,57,65,67,69,73,82,86,89,96,99,104,110,116,
126,138,146,147,148,153,156,162,164,168,171,178,179,180,190,192,193,194,201,206,208,214,222,224,225,236,238,239,241,243,246,260,271,272,273,278,284,
296,303,309,316,320,321,324,329,332,336,343,347,349,359,361,362,368,372,397,407,408,412,414,417,420,425,432,435,437,438,440,441,447,448,451,452,462,
464,477,483,484,487,488,492,493,494,495,496,497,499,511,516,517,518,525,527,529,534,535,540,541,544,549,550,561,566,567,568,574,582,583,591,593,594,
600,605,606,610,612,613,617,620,630,636,640,645,664,666,680,683,684,686,693,696,697,698,700,707,710,723,724,725,733,737,738,739,740,741,745,746,748,
753,763,764,765,772,773,774,778,779,780,785,787,792,796,799,802,821,824,828,839,840,842,853,854,858,862,869,880,886,891,900,902,903,904,905,914,917,
920,923,924,925,933,938,945,946,948,949,952,954,957,962,967,968,986,993,994,998,999,1000,1015,1021,1024,1026,1030,1033,1039,1042,1044,1045,1049,1057,
1061,1064,1069,1072,1075,1094,1097,1101,1104,111





}
  EquipamentosErr :=  '';
////////////////////////////////////////////////////////////////////////////////
///  Lista de GraGruY  /////////////////////////////////////////////////////////
     //Provis�rio! Ver lista excel!!!
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Criando lista de GraGruY');

  SELECT * FROM PRODUTO
WHERE ValorDiaria>0
AND PATRIMONIO=""
AND CATEGORIA = "L"


  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruY, FbDB, [
  'DROP TABLE IF EXISTS _PRODUTO_ALL_; ',
  'CREATE TABLE _PRODUTO_ALL_ ',
  ' ',
  'SELECT "S" GGY_KIND, Ativo, DESCRICAO,  ',
  'PRODUTO, Linha, Categoria,  ',
  'Referencia, Patrimonio ',
  'FROM PRODUTO ',
  'WHERE Categoria = "L" ',
  'AND Patrimonio = "" ',
  ' ',
  'UNION ',
  ' ',
  'SELECT "P" GGY_KIND, Ativo, DESCRICAO,  ',
  'PRODUTO, Linha, Categoria,  ',
  'Referencia, Patrimonio ',
  'FROM PRODUTO ',
  'WHERE Categoria = "L" ',
  'AND Patrimonio <> "" ',
  ' ',
  'UNION',
  '',
  'SELECT "V" GGY_KIND, Ativo, DESCRICAO,  ',
  'PRODUTO, Linha, Categoria,  ',
  'Referencia, Patrimonio ',
  'FROM PRODUTO ',
  'WHERE Categoria = "V" ',
  ' ',
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRODUTO_ACE_; ',
  'CREATE TABLE _PRODUTO_ACE_ ',
  ' ',
  'SELECT DISTINCT ACESSORIO ',
  'FROM PRODUTOACESSORIO ',
  '; ',
  ' ',
  'DROP TABLE IF EXISTS PRODUTOS_GGY; ',
  'CREATE TABLE PRODUTOS_GGY ',
  'SELECT IF(ace.ACESSORIO > 0, "A", pal.GGY_KIND) TipoProd,  ',
  'pal.*  ',
  'FROM _PRODUTO_ALL_ pal ',
  'LEFT JOIN PRODUTOACESSORIO ace ON ace.ACESSORIO=pal.PRODUTO ',
  ' ',
  '; ',
  'SELECT * FROM PRODUTOS_GGY',
  //'ORDER BY GGY_KIND DESC, DESCRICAO ',
  'ORDER BY PRODUTO DESC ',
  ';',
  '']);
  //
  QrGraGruY.First;
  SetLength(GraGruYs, QrGraGruYPRODUTO.Value + 1);
  for I := 0 to QrGraGruYPRODUTO.Value do
    GraGruYs[I] := '?';
  while not QrGraGruY.Eof do
  begin
    GraGruYs[QrGraGruYPRODUTO.Value] := QrGraGruYTipoProd.Value;
    //
    QrGraGruY.Next;
  end;
////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela GraTamCad
////////////////////////////////////////////////////////////////////////////////
  DstTab := 'unidmed';
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ' + DstTab);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  SQL_FIELDS_UNIDMED := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, CodUsu, Sigla, Nome, Sequencia, Grandeza) VALUES ',
    '(1,     1,      "UN","UNIDADE",   0,     0)']);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_FIELDS_UNIDMED);
  // Codigo  CodUsu, Sigla, Nome, Sequencia, Grandeza

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela gracorcad
////////////////////////////////////////////////////////////////////////////////
  DstTab := 'gracorcad';
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ' + DstTab);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  SQL_FIELDS_XXX := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, CodUsu, PrintCor, Nome) VALUES (0,0,0,"�NICO"), (1,1,0,"�NICO")']);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_FIELDS_XXX);
  //
////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela GraTamCad
////////////////////////////////////////////////////////////////////////////////
  DstTab := 'gratamcad';
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ' + DstTab);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  SQL_FIELDS_XXX := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, CodUsu, PrintTam, Nome) VALUES (0,0,0,"�NICO"), (1,1,0,"�NICO")']);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_FIELDS_XXX);
  //
////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela GraTamIts
////////////////////////////////////////////////////////////////////////////////
  DstTab := 'gratamits';
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ' + DstTab);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  SQL_FIELDS_XXX := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Controle, PrintTam, Nome) VALUES ',
    '(0,0,0,"�NICO"), ',
    '(1,1,0,"�NICO") '
    ]);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_FIELDS_XXX);
  //
////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela GraTamIts
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela GraGuX e GraGru1
////////////////////////////////////////////////////////////////////////////////
  DstTab_GGC := 'gragruc';
  DstTab_GGX := 'gragrux';
  DstTab_GG1 := 'gragru1';
  DstTab_Patr := 'gragxpatr';
  DstTab_Outr := 'gragxoutr';
  DstTab_GGEPat := 'gragruepat';
  MySQLBatchExecute1.Database := Dmod.MyDB;
  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGruC := Geral.ATS([
    'INSERT INTO ' + DstTab_GGC + ' ( ',
    'Controle, Nivel1, GraCorCad, GraCmpCad) VALUES ']);
  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGruX := Geral.ATS([
    'INSERT INTO ' + DstTab_GGX + ' ( ',
    'Controle, GraGruC, GraGru1, GraTamI, EAN13, GraGruY, COD_ITEM, Ativo) VALUES ']);
  //////////////////////////////////////////////////////////
  SQL_FIELDS_GraGru1 := Geral.ATS([
    'INSERT INTO ' + DstTab_GG1 + ' ( ',

  'Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, CodUsu, ', //: Integer
  'Nome, ', //: String;
  'PrdGrupTip, GraTamCad, UnidMed, ', //: Integer;
(*
  CST_A, CST_B,
*)
  'NCM, ', //: String;
(*
Peso, IPI_Alq, IPI_CST,
IPI_cEnq, IPI_vUnid, IPI_TpTrib,
TipDimens, PerCuztMin, PerCuztMax,
MedOrdem, PartePrinc, InfAdProd,
SiglaCustm, HowBxaEstq, GerBxaEstq,
PIS_CST, PIS_AlqP, PIS_AlqV,
PISST_AlqP, PISST_AlqV, COFINS_CST,
COFINS_AlqP, COFINS_AlqV, COFINSST_AlqP,
COFINSST_AlqV, ICMS_modBC, ICMS_modBCST,
ICMS_pRedBC, ICMS_pRedBCST, ICMS_pMVAST,
ICMS_pICMSST, ICMS_Pauta, ICMS_MaxTab,
*)
  'IPI_pIPI, ', //: Double;

  'cGTIN_EAN, ', //: String;
(*
  EX_TIPI,
PIS_pRedBC, PISST_pRedBCST, COFINS_pRedBC,
COFINSST_pRedBCST, ICMSRec_pRedBC, IPIRec_pRedBC,
PISRec_pRedBC, COFINSRec_pRedBC, ICMSRec_pAliq,
IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq,
ICMSRec_tCalc, IPIRec_tCalc, PISRec_tCalc,
COFINSRec_tCalc, ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA,
FatorClas, prod_indTot,
*)
  'Referencia ', //: String;
(*
Fabricante, PerComissF, PerComissR,
PerComissZ, NomeTemp, COD_LST,
SPEDEFD_ALIQ_ICMS, DadosFisc, GraTabApp,
ICMS_pDif, ICMS_pICMSDeson, ICMS_motDesICMS,
prod_CEST, prod_indEscala
*)

    ') VALUES ']);
  //

  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGXPatr := Geral.ATS([
    'INSERT INTO ' + DstTab_Patr + ' ( ',
    'GraGruX, ',
  (*Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia, Capacid,
  VendaData, VendaDocu,*)
  'Observa, ',
  'AGRPAT, ',
  //CLVPAT,*)
  'MARPAT, ',
  (*, DescrTerc, Situacao, Agrupador,*)
  'Marca, ',(*VendaEnti,*)
  'Aplicacao, ',
  'AquisValr, AtualValr, ValorMes, ValorQui, ValorSem, ValorDia, VendaValr',
  ') VALUES ']);

  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGXOutr := Geral.ATS([
    'INSERT INTO ' + DstTab_Outr + ' ( ',
    'GraGruX, Aplicacao, ItemValr, ItemUnid) VALUES ']);

  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGruEPat := Geral.ATS([
    'INSERT INTO ' + DstTab_GGEPat + ' ( ',
    'GraGruX, EstqAnt, EstqEnt, EstqSai, EstqSdo) VALUES ']);
////////////////////////////////////////////////////////////////////////////////









  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Cores (GGX x GG1)');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_GGC);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Reduzidos');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_GGX);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. N�vel 1');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_GG1);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Patrim�nio');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_Patr);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Outros');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_Outr);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Estoque');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_GGEPat);
  //

  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDProduto.Close;
    SDProduto.DataSet.CommandType := ctQuery;
    SDProduto.DataSet.CommandText := SQL;
    SDProduto.Open;
    Continua := SDProduto.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDProduto.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES_GraGruX := '';
      SQL_VALUES_GraGru1 := '';
      SQL_VALUES_GraGruC := '';
      SDProduto.First;
      SQLRec_GraGruX    := '';
      SQLRec_GraGru1    := '';
      SQLRec_GraGruC    := '';
      while not SDProduto.Eof do
      begin
        Controle := Integer(SDProdutoProduto.Value);
        GraGruC  := Controle;
        GraGru1  := Controle;
        Nome     := SDProdutoDESCRICAO.Value;
        //
        TipoProd := GraGruYs[Controle];
        //
        ///  Ver tabela e tipo de equipamento!
        _ATIVO     := SDProdutoATIVO.Value;
        CATEGORIA := SDProdutoCATEGORIA.Value;  // "V" e "L"    Venda e Loca��o???
        //
        if TipoProd = 'P' then // Produto Principal
          GraGruY  := CO_GraGruY_1024_GXPatPri
        else
        if TipoProd = 'S' then // Produto Secund�rio
          GraGruY  := CO_GraGruY_2048_GXPatSec
        else
        if TipoProd = 'A' then // Acessorio
          GraGruY  := CO_GraGruY_3072_GXPatAce
        else
        if TipoProd = 'U' then // Uso
          GraGruY  := CO_GraGruY_4096_GXPatUso
        else
        if TipoProd = 'C' then // Consumo
          GraGruY  := CO_GraGruY_5120_GXPatCns
        else
          GraGruY  := CO_GraGruY_6144_GXPrdVen
        ;
        //
        if _ATIVO = 'S' then
          Ativo := 1
        else
          Ativo := 0;
        //
        if SDProduto.RecNo = 1 then
          SQLRec_GraGruX    := ' ('
        else
          SQLRec_GraGruX    := ', (';
        //
        SQLRec_GraGruX := SQLRec_GraGruX
        //Controle, GraGruC, GraGru1, GraTamI, EAN13, GraGruY, COD_ITEM
        +      Geral.FF0(Controle)
        + PV_N(Geral.FF0(GraGruC))
        + PV_N(Geral.FF0(GraGru1))
        + PV_N(Geral.FF0(GraTamI))
        + PV_A(EAN13)
        + PV_N(Geral.FF0(GraGruY))
        + PV_A(COD_ITEM)
        + PV_N(Geral.FF0(Ativo))
        //
        + ') ';
              //
        SQL_VALUES_GraGruX := SQL_VALUES_GraGruX + sLineBreak + SQLRec_GraGruX;

        //////////////////////////////////////////////////
        ///        GraGru1
        //////////////////////////////////////////////////
        Nivel5      := 0;
        Nivel4      := 0;
        Nivel3      := 0;
        Nivel2      := GetNivel2(Integer(SDProdutoGRUPO.Value), Geral.IMV(SDProdutoLinha.Value));
        Nivel1      := GraGru1;
        CodUsu      := Nivel1;
        Nome        := SDProdutoDESCRICAO.Value;
        PrdGrupTip  := Integer(SDProdutoGRUPO.Value);
        GraTamCad   := 1; // Fixo! para este cliente! Tisolin
        UnidMed     := 1; // Fixo! para este cliente! Tisolin
(*
CST_A, CST_B,
*)
        NCM         := SDProdutoNCM.Value;
(*
Peso, IPI_Alq, IPI_CST,
IPI_cEnq, IPI_vUnid, IPI_TpTrib,
TipDimens, PerCuztMin, PerCuztMax,
MedOrdem, PartePrinc, InfAdProd,
SiglaCustm, HowBxaEstq, GerBxaEstq,
PIS_CST, PIS_AlqP, PIS_AlqV,
PISST_AlqP, PISST_AlqV, COFINS_CST,
COFINS_AlqP, COFINS_AlqV, COFINSST_AlqP,
COFINSST_AlqV, ICMS_modBC, ICMS_modBCST,
ICMS_pRedBC, ICMS_pRedBCST, ICMS_pMVAST,
ICMS_pICMSST, ICMS_Pauta, ICMS_MaxTab,
*)

        IPI_pIPI    := SDProduto.FieldByName('PERCIPI').AsExtended;
        cGTIN_EAN   := SDProdutoCODIGOBARRAS.Value;

(*
  EX_TIPI,
PIS_pRedBC, PISST_pRedBCST, COFINS_pRedBC,
COFINSST_pRedBCST, ICMSRec_pRedBC, IPIRec_pRedBC,
PISRec_pRedBC, COFINSRec_pRedBC, ICMSRec_pAliq,
IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq,
ICMSRec_tCalc, IPIRec_tCalc, PISRec_tCalc,
COFINSRec_tCalc, ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA,
FatorClas, prod_indTot,
*)

(*
DROP TABLE IF EXISTS _REFERENCIAS_DUPLICADAS_;

CREATE TABLE _REFERENCIAS_DUPLICADAS_
SELECT REFERENCIA, COUNT(REFERENCIA) ITENS
FROM PRODUTO
GROUP BY Referencia;

SELECT PRODUTO, REFERENCIA, DESCRICAO
FROM PRODUTO
WHERE REFERENCIA IN (
  SELECT REFERENCIA
  FROM _REFERENCIAS_DUPLICADAS_
  WHERE ITENS > 1
)
ORDER BY REFERENCIA, PRODUTO;
*)
        Referencia := SDProdutoREFERENCIA.Value;
        if Referencia = EmptyStr then
          Referencia := 'Produto ' + Geral.FF0(Nivel1);
(*
Fabricante, PerComissF, PerComissR,
PerComissZ, NomeTemp, COD_LST,
SPEDEFD_ALIQ_ICMS, DadosFisc, GraTabApp,
ICMS_pDif, ICMS_pICMSDeson, ICMS_motDesICMS,
prod_CEST, prod_indEscala
*)
        //
        if SDProduto.RecNo = 1 then
          SQLRec_GraGru1    := ' ('
        else
          SQLRec_GraGru1    := ', (';
        //
        SQLRec_GraGru1 := SQLRec_GraGru1
        //Controle, GraGruC, GraGru1, GraTamI, EAN13, GraGruY, COD_ITEM
        +      Geral.FF0(Nivel5)
        + PV_N(Geral.FF0(Nivel4))
        + PV_N(Geral.FF0(Nivel3))
        + PV_N(Geral.FF0(Nivel2))
        + PV_N(Geral.FF0(Nivel1))
        + PV_N(Geral.FF0(CodUsu))
        + PV_N(Geral.VariavelToString(Nome))
        + PV_N(Geral.FF0(PrdGrupTip))
        + PV_N(Geral.FF0(GraTamCad))
        + PV_N(Geral.FF0(UnidMed))
        + PV_A(Geral.FormataNCM(NCM))
        + PV_N(Geral.FFT_Dot(IPI_pIPI, 2, siPositivo))
        + PV_A(cGTIN_EAN)
        + PV_N(Geral.VariavelToString(Referencia))
        //
        + ') ';
        //
        SQL_VALUES_GraGru1 := SQL_VALUES_GraGru1 + sLineBreak + SQLRec_GraGru1;




        if SDProduto.RecNo = 1 then
          SQLRec_GraGruC    := ' ('
        else
          SQLRec_GraGruC    := ', (';
        //
        SQLRec_GraGruC := SQLRec_GraGruC
        //Controle, GraGruC, GraGru1, GraTamI, EAN13, GraGruY, COD_ITEM
        +      Geral.FF0(Controle)
        + PV_N(Geral.FF0(Nivel1))
        + PV_N(Geral.FF0((*GraCorCad*)1))
        + PV_N(Geral.FF0((*GraCmpCad*)0))
        //
        + ') ';
        //
        SQL_VALUES_GraGruC := SQL_VALUES_GraGruC + sLineBreak + SQLRec_GraGruC;



(*
Complem, AquisData, AquisDocu,
*)
         AquisValr := SDProduto.FieldByName('CUSTO').AsExtended;
(*
Situacao,
*)
         AtualValr  := SDProduto.FieldByName('PRECOVENDA').AsExtended;
         ValorMes   := SDProduto.FieldByName('VALORMENSAL').AsExtended;
         ValorQui   := SDProduto.FieldByName('VALORQUINZENAL').AsExtended;
         ValorSem   := SDProduto.FieldByName('VALORSEMANAL').AsExtended;
         ValorDia   := SDProduto.FieldByName('VALORDIARIA').AsExtended;
(*
Agrupador,
*)
        UnDmkDAC_PF.AbreMySQLQuery0(QrGraFabMar, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM grafabmar ',
        'WHERE Nome="' + SDProdutoMARCA.Value + '"',
        '']);
        //
        Marca := QrGraFabMarControle.Value;
(*
Modelo, Serie, Voltagem,
Potencia, Capacid, VendaData,
VendaDocu, VendaValr, VendaEnti,
*)
        Observa := SDProdutoFICHATECNICA.Value;
        AGRPAT  := Geral.FF0(Integer(SDProdutoGRUPO.Value));
(*
CLVPAT,
*)

        MARPAT := SDProdutoMARCA.Value;
(*
, Aplicacao, DescrTerc], [
GraGruX]
*)
        /////














{
        if Uppercase(CATEGORIA) = 'L' then
        //Loca��o
        begin
          if Uppercase(ATIVO) = 'S' then
          begin
            SDGraGX.Close;
            SDGraGX.DataSet.CommandType := ctQuery;
            SDGraGX.DataSet.CommandText := Geral.ATS([
            'SELECT PRODUTO, ACESSORIO  ',
            'FROM produtoacessorio ',
            'WHERE PRODUTO=' + Geral.FF0(Controle),
            'OR ACESSORIO=' + Geral.FF0(Controle)
            ]);
            SDGraGX.Open;
            if SDGraGX.RecordCount > 0 then
            begin
              if Integer(SDGraGXPRODUTO.Value) = Controle then
                Aplicacao := 1
              else
                Aplicacao := 2
            end else
            begin
              //Geral.MB_ERRO('Reduzido de loca��o sem atrelamento de produto X acess�rio: ' + Geral.FF0(Controle))
              EquipamentosErr :=  EquipamentosErr + ', ' + Geral.FF0(Controle);
              Aplicacao := 1;
            end;
          end else
            Aplicacao := 0;

          //
          GraGruX   := Controle;
          //
          if SQL_VALUES_GraGXPatr = EmptyStr then
            SQLRec_GraGXPatr    := ' ('
          else
            SQLRec_GraGXPatr    := ', (';
          //
          SQLRec_GraGXPatr := SQLRec_GraGXPatr
          +      Geral.FF0(GraGruX)
          (*Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia, Capacid,
          VendaData, VendaDocu,*)
          + PV_N(Geral.VariavelToString(Observa))
          + PV_N(Geral.VariavelToString(AGRPAT))
          (*CLVPAT,*)
          + PV_N(Geral.VariavelToString(MARPAT))
          (*, DescrTerc, Situacao, Agrupador,*)
          + PV_N(Geral.FF0(Marca))
          (*VendaEnti,*)
          + PV_N(Geral.FF0(Aplicacao))
          + PV_N(Geral.FFT_Dot(AquisValr, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(AtualValr, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(ValorMes, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(ValorQui, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(ValorSem, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(ValorDia, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(VendaValr, 2, siNegativo))
          //
          + ') ';
          //
          SQL_VALUES_GraGXPatr := SQL_VALUES_GraGXPatr + sLineBreak + SQLRec_GraGXPatr;

        end else // "V"
        // Venda!
        begin
          GraGruX   := Controle;
          ItemValr  := AtualValr;
          ItemUnid  := 1;
          Aplicacao := 1;
          //
          //
          if SQL_VALUES_GraGXOutr = EmptyStr then
            SQLRec_GraGXOutr    := ' ('
          else
            SQLRec_GraGXOutr    := ', (';
          //
          SQLRec_GraGXOutr := SQLRec_GraGXOutr
          +      Geral.FF0(GraGruX)
          + PV_N(Geral.FF0(Aplicacao))
          + PV_N(Geral.FFT_Dot(ItemValr, 2, siPositivo))
          + PV_N(Geral.FF0(ItemUnid))
          //
          + ') ';
          //
          SQL_VALUES_GraGXOutr := SQL_VALUES_GraGXOutr + sLineBreak + SQLRec_GraGXOutr;
        end;
}


























        //if Controle = 862 then
          //Geral.MB_Info(Geral.FF0(Controle));

        //if pos('FRET', Nome) > 0 then
          //Geral.MB_Info(Nome);
        if Uppercase(CATEGORIA) = 'L' then
        //Loca��o
        begin
          SDGraGX.Close;
          SDGraGX.DataSet.CommandType := ctQuery;
          SDGraGX.DataSet.CommandText := Geral.ATS([
          'SELECT PRODUTO, ACESSORIO  ',
          'FROM produtoacessorio ',
          'WHERE PRODUTO=' + Geral.FF0(Controle),
          'OR ACESSORIO=' + Geral.FF0(Controle)
          ]);
          SDGraGX.Open;
          if (SDGraGX.RecordCount = 0) or (Integer(SDGraGXPRODUTO.Value) = Controle) then
          begin
            if Uppercase(_ATIVO) = 'S' then
            begin
              if Integer(SDProdutoGRUPO.Value) = 3 then //M�quina
                Aplicacao := 1
              else
                Aplicacao := 2;
            end else
              Aplicacao := 0;
            GraGruX   := Controle;
            //
            if SQL_VALUES_GraGXPatr = EmptyStr then
              SQLRec_GraGXPatr    := ' ('
            else
              SQLRec_GraGXPatr    := ', (';
            //
            SQLRec_GraGXPatr := SQLRec_GraGXPatr
            +      Geral.FF0(GraGruX)
            (*Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia, Capacid,
            VendaData, VendaDocu,*)
            + PV_N(Geral.VariavelToString(Observa))
            + PV_N(Geral.VariavelToString(AGRPAT))
            (*CLVPAT,*)
            + PV_N(Geral.VariavelToString(MARPAT))
            (*, DescrTerc, Situacao, Agrupador,*)
            + PV_N(Geral.FF0(Marca))
            (*VendaEnti,*)
            + PV_N(Geral.FF0(Aplicacao))
            + PV_N(Geral.FFT_Dot(AquisValr, 2, siNegativo))
            + PV_N(Geral.FFT_Dot(AtualValr, 2, siNegativo))
            + PV_N(Geral.FFT_Dot(ValorMes, 2, siNegativo))
            + PV_N(Geral.FFT_Dot(ValorQui, 2, siNegativo))
            + PV_N(Geral.FFT_Dot(ValorSem, 2, siNegativo))
            + PV_N(Geral.FFT_Dot(ValorDia, 2, siNegativo))
            + PV_N(Geral.FFT_Dot(VendaValr, 2, siNegativo))
            //
            + ') ';
            //
            SQL_VALUES_GraGXPatr := SQL_VALUES_GraGXPatr + sLineBreak + SQLRec_GraGXPatr;
          end else
          begin
            if Uppercase(_ATIVO) = 'S' then
            begin
              case Integer(SDProdutoGRUPO.Value) of
                // Peca
                1: Aplicacao := 2; // Uso
                // Acessorio
                2:
                begin
                  if AtualValr > 0 then
                    Aplicacao := 2 // Uso
                  else
                    Aplicacao := 1; // Acessorio
                end;
                // Maquina
                3:
                begin
                  Aplicacao := 2; // Uso
                  Geral.MB_Erro('Maquina como venda!!!' + IntToStr(GraGruX));
                end;
                // Servico
                4, //: Aplicacao := 3;  // Consumo
                // Outros
                5: //Aplicacao := 3;  // Consumo
                begin
                  if AtualValr > 0 then
                    Aplicacao := 3 // Consumo
                  else
                    Aplicacao := 1; // Acessorio
                end;
              end;
            end else
              Aplicacao := 0;
            //
            GraGruX   := Controle;
            ItemValr  := AtualValr;
            ItemUnid  := 1;
            //
            //
            if SQL_VALUES_GraGXOutr = EmptyStr then
              SQLRec_GraGXOutr    := ' ('
            else
              SQLRec_GraGXOutr    := ', (';
            //
            SQLRec_GraGXOutr := SQLRec_GraGXOutr
            +      Geral.FF0(GraGruX)
            + PV_N(Geral.FF0(Aplicacao))
            + PV_N(Geral.FFT_Dot(ItemValr, 2, siPositivo))
            + PV_N(Geral.FF0(ItemUnid))
            //
            + ') ';
            //
            SQL_VALUES_GraGXOutr := SQL_VALUES_GraGXOutr + sLineBreak + SQLRec_GraGXOutr;
          end;
        end else // "V"
        // Venda!
        begin
          if Uppercase(_ATIVO) = 'S' then
          begin
            case Integer(SDProdutoGRUPO.Value) of
              // Peca
              1: Aplicacao := 2; // Uso
              // Acessorio
              2:
              begin
                if AtualValr > 0 then
                  Aplicacao := 2 // Uso
                else
                  Aplicacao := 1; // Acessorio
              end;
              // Maquina
              3:
              begin
                Aplicacao := 2; // Uso
                //Geral.MB_Erro('Maquina como venda!!!' + IntToStr(GraGruX));
              end;
              // Servico
              4, //: Aplicacao := 3;  // Consumo
              // Outros
              5: //Aplicacao := 3;  // Consumo
              begin
                if AtualValr > 0 then
                  Aplicacao := 3 // Consumo
                else
                  Aplicacao := 1; // Acessorio
              end;
            end;
          end else
            Aplicacao := 0;
          //
          GraGruX   := Controle;
          ItemValr  := AtualValr;
          ItemUnid  := 1;
          //
          //
          if SQL_VALUES_GraGXOutr = EmptyStr then
            SQLRec_GraGXOutr    := ' ('
          else
            SQLRec_GraGXOutr    := ', (';
          //
          SQLRec_GraGXOutr := SQLRec_GraGXOutr
          +      Geral.FF0(GraGruX)
          + PV_N(Geral.FF0(Aplicacao))
          + PV_N(Geral.FFT_Dot(ItemValr, 2, siPositivo))
          + PV_N(Geral.FF0(ItemUnid))
          //
          + ') ';
          //
          SQL_VALUES_GraGXOutr := SQL_VALUES_GraGXOutr + sLineBreak + SQLRec_GraGXOutr;
        end;


















(*
DROP TABLE IF EXISTS _produtoacessorio_PRODUTOS;
CREATE TABLE _produtoacessorio_PRODUTOS
SELECT PRODUTO
FROM produtoacessorio;

DROP TABLE IF EXISTS _produtoacessorio_ACESSORIO;
CREATE TABLE _produtoacessorio_ACESSORIO
SELECT ACESSORIO
FROM produtoacessorio;

SELECT prd.PRODUTO, ace.ACESSORIO
FROM _produtoacessorio_PRODUTOS prd
LEFT JOIN _produtoacessorio_ACESSORIO ace
  ON prd.PRODUTO=ace.ACESSORIO
WHERE NOT(ace.ACESSORIO IS NULL)
;
*)


        Estoque := SDPRODUTO.FieldByName('ESTOQUE').AsExtended;
        if Estoque < 0 then
          Estoque := 0;
        //
        if SDProduto.RecNo = 1 then
          SQLRec_GraGruEPat    := ' ('
        else
          SQLRec_GraGruEPat    := ', (';
        //
        SQLRec_GraGruEPat := SQLRec_GraGruEPat
        //GraGruX, EstqAnt, EstqEnt, EstqSai, EstqSdo
        +      Geral.FF0(GraGruX)
        + PV_N(Geral.FFT_Dot(Estoque, 6, siNegativo))
        + PV_N('0.00')
        + PV_N('0.00')
        + PV_N(Geral.FFT_Dot(Estoque, 6, siNegativo))
        //
        + ') ';
        //
        SQL_VALUES_GraGruEPat := SQL_VALUES_GraGruEPat + sLineBreak + SQLRec_GraGruEPat;



        SDProduto.Next;
      end;

      if SQL_VALUES_GraGruX <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGruX + SQL_VALUES_GraGruX;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGruX + SQL_VALUES_GraGruX);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGruX := EmptyStr;

      //
      if SQL_VALUES_GraGru1 <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGru1 + SQL_VALUES_GraGru1;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGru1 + SQL_VALUES_GraGru1);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGru1 := EmptyStr;

      //
      if SQL_VALUES_GraGruC <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGruC + SQL_VALUES_GraGruC;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGruC + SQL_VALUES_GraGruC);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGruC := EmptyStr;

      //
      if SQL_VALUES_GraGXPatr <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGXPatr + SQL_VALUES_GraGXPatr;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGXPatr + SQL_VALUES_GraGXPatr);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGXPatr := EmptyStr;

      //
      if SQL_VALUES_GraGXOutr <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGXOutr + SQL_VALUES_GraGXOutr;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGXOutr + SQL_VALUES_GraGXOutr);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGXOutr := EmptyStr;

      //
      if SQL_VALUES_GraGruEPat <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGruEPat + SQL_VALUES_GraGruEPat;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGruEPat + SQL_VALUES_GraGruEPat);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGruEPat := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDProduto.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
  //
  if EquipamentosErr <> EmptyStr then
    Geral.MB_Erro('Equipamentos sem atrelamento: ' + sLineBreak +
    EquipamentosErr);
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_ProdutoAcessorio;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Conta, GraGXPatr, GraGXOutr, Quantidade: Integer;
const
  OriTab = 'ProdutoAcessorio';
  DstTab = 'gragxpits';
begin
  Conta := 0;
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Conta, GraGXPatr, GraGXOutr, Quantidade) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDProdutoAcessorio.Close;
    SDProdutoAcessorio.DataSet.CommandType := ctQuery;
    SDProdutoAcessorio.DataSet.CommandText := SQL;
    SDProdutoAcessorio.Open;
    Continua := SDProdutoAcessorio.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDProdutoAcessorio.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDProdutoAcessorio.First;
      SQLRec    := '';
      while not SDProdutoAcessorio.Eof do
      begin
        Conta      := Conta + 1;
        GraGXPatr  := Integer(SDProdutoAcessorioPRODUTO.Value);
        GraGXOutr  := Integer(SDProdutoAcessorioACESSORIO.Value);
        Quantidade := Integer(SDProdutoAcessorioQUANTIDADE.Value);
        //
        if SDProdutoAcessorio.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Conta
        + Geral.FF0(Conta)
        //GraGXPatr
        + PV_N(Geral.FF0(GraGXPatr))
        //GraGXOutr
        + PV_N(Geral.FF0(GraGXOutr))
        //Quantidade
        + PV_N(Geral.FF0(Quantidade))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDProdutoAcessorio.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDProdutoAcessorio.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_StatusEnti();
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo: Integer;
  Nome: String;
const
  OriTab = 'StatusCliente';
  DstTab = 'StatusEnti';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Nome) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDStatusCliente.Close;
    SDStatusCliente.DataSet.CommandType := ctQuery;
    SDStatusCliente.DataSet.CommandText := SQL;
    SDStatusCliente.Open;
    Continua := SDStatusCliente.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDStatusCliente.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDStatusCliente.First;
      SQLRec    := '';
      while not SDStatusCliente.Eof do
      begin
        Codigo := Integer(SDStatusClienteSTATUS.Value);
        Nome   := SDStatusClienteDESCRICAO.Value;
        //
        if SDStatusCliente.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //Empresa,
        + PV_N(Geral.VariavelToString(Nome))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDStatusCliente.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDStatusCliente.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

{
procedure TFmDB_Converte_Tisolin.IncluiBanco(Codigo, Ordem: Integer;
  Nome, Bco, Age: String);
var
  ContaCor, ContaTip, DAC_A, DAC_C, DAC_AC, Observ: String;
  Controle, Banco, Agencia: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('entictas', 'Controle', stIns, 0);
  Banco          := 0;
  Agencia        := 0;
  ContaCor       := '';
  ContaTip       := '';
  DAC_A          := '';
  DAC_C          := '';
  DAC_AC         := '';
  Observ         := Nome + ' ' + Bco + ' ' + Age;
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entictas', False, [
  'Codigo', 'Ordem', 'Banco',
  'Agencia', 'ContaCor', 'ContaTip',
  'DAC_A', 'DAC_C', 'DAC_AC',
  'Observ'], [
  'Controle'], [
  Codigo, Ordem, Banco,
  Agencia, ContaCor, ContaTip,
  DAC_A, DAC_C, DAC_AC,
  Observ], [
  Controle], True);
end;

procedure TFmDB_Converte_Tisolin.IncluiConsulta(Codigo, Orgao: Integer;
  Numero: String; _Data: TDateTime; _Hora, _Resultado: String);
var
  Data, Hora, Resultado: String;
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('entisrvpro', 'Controle', stIns, 0);
  Data           := Geral.FDT(_Data, 1);
  Hora           := Geral.SoHora_TT(_Hora, pt2pt60);
  Resultado      := _Resultado + ' ' + _Hora;
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entisrvpro', False, [
  'Codigo', 'Orgao', 'Data',
  'Hora', 'Numero', 'Resultado'], [
  'Controle'], [
  Codigo, Orgao, Data,
  Hora, Numero, Resultado], [
  Controle], True);
end;

procedure TFmDB_Converte_Tisolin.IncluiContato(Codigo, Cargo: Integer;
  Nome, Tel, Cel, Fax, Mail: String);
const
  DiarioAdd = 0;
var
  Controle: Integer;
  Telefone, EMail: String;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticontat', False, [
  'Codigo', 'Nome', 'Cargo',
  'DiarioAdd'], [
  'Controle'], [
  Codigo, Nome, Cargo,
  DiarioAdd], [
  Controle], True) then
  begin
    Telefone := Geral.SoNumero_TT(Tel);
    if Telefone <> '' then
      IncluiEntiTel(Codigo, Controle, FFoneFix, Telefone);
    //
    Telefone := Geral.SoNumero_TT(Cel);
    if Telefone <> '' then
      IncluiEntiTel(Codigo, Controle, FFoneCel, Telefone);
    //
    Telefone := Geral.SoNumero_TT(Fax);
    if Telefone <> '' then
      IncluiEntiTel(Codigo, Controle, FFoneFax, Telefone);
    //
    ////////////////////////////////////////////////////////////////////////////
    ///
    EMail := Lowercase(Trim(Mail));
    if EMail <> '' then
      IncluiEntiMail(Codigo, Controle, FEMail, EMail);
    //
  end;
end;

procedure TFmDB_Converte_Tisolin.IncluiEntiMail(Codigo, Controle,
  EntiTipCto: Integer; EMail: String);
const
  Ordem = 0;
  DiarioAdd = 0;
var
  Conta: Integer;
begin
  Conta := UMyMod.BuscaEmLivreY_Def('entimail', 'Conta', stIns, 0);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entimail', False, [
  'Codigo', 'Controle', 'EMail',
  'EntiTipCto', 'Ordem', 'DiarioAdd'], [
  'Conta'], [
  Codigo, Controle, EMail,
  EntiTipCto, Ordem, DiarioAdd], [
  Conta], True);
end;

procedure TFmDB_Converte_Tisolin.IncluiEntiTel(Codigo, Controle, EntiTipCto: Integer;
  Tel: String);
const
  Ramal = '';
  DiarioAdd = 0;
var
  Conta: Integer;
  Telefone: String;
begin
  Conta := UMyMod.BuscaEmLivreY_Def('entitel', 'Conta', stIns, 0);
  Telefone := Tel;
  if Copy(Telefone, 1, 2) = '00' then
    Telefone := Copy(Telefone, 2);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entitel', False, [
  'Codigo', 'Controle', 'Telefone',
  'EntiTipCto', 'Ramal', 'DiarioAdd'], [
  'Conta'], [
  Codigo, Controle, Telefone,
  EntiTipCto, Ramal, DiarioAdd], [
  Conta], True);
end;

procedure TFmDB_Converte_Tisolin.IncluiSocio(Codigo, CargoSocio: Integer;
  Nome, _RG, _CPF: String; Natal: TDateTime);
var
  Observ, MandatoIni, MandatoFim, CPF, RG, SSP, DataRG, DtaNatal: String;
  Controle, Cargo, (*Assina,*) OrdemLista: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('entirespon', 'Controle', stIns, 0);
  //Nome           := ;
  Cargo          := CargoSocio;
  //Assina         := ;
  OrdemLista     := Controle;
  Observ         := Nome + _RG + _CPF;
  MandatoIni     := '0000-00-00';
  MandatoFim     := '0000-00-00';
  CPF            := Geral.SoNumero_TT(_CPF);
  RG             := Geral.SoNumero_TT(_RG);
  SSP            := Geral.SoLetra_TT(_RG);
  DataRG         := '';
  DtaNatal       := Geral.FDT(Natal, 1);

  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entirespon', False, [
  'Codigo', 'Nome', 'Cargo',
  (*'Assina',*) 'OrdemLista', 'Observ',
  'MandatoIni', 'MandatoFim', 'CPF',
  'RG', 'SSP', 'DataRG',
  'DtaNatal'], [
  'Controle'], [
  Codigo, Nome, Cargo,
  (*Assina,*) OrdemLista, Observ,
  MandatoIni, MandatoFim, CPF,
  RG, SSP, DataRG,
  DtaNatal], [
  Controle], True);
end;
}

procedure TFmDB_Converte_Tisolin.MeSQL1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    BtExecutaClick(Sender);
end;

function TFmDB_Converte_Tisolin.ObtemGraGruXDeCODMATX(CODMATX: String): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
  'SELECT ggx.Controle ID',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON ggx.GraGru1=gg1.Nivel1 ',
  'WHERE gg1.Referencia=''' + CODMATX + '''',
  '']);
  //
  Result := QrExiste.FieldByName('ID').AsInteger;
end;

function TFmDB_Converte_Tisolin.PV_A(Texto: String): String;
begin
  Result := ', "' + Texto + '"';
end;

function TFmDB_Converte_Tisolin.PV_N(Texto: String): String;
begin
  Result := ', ' + Texto;
end;

procedure TFmDB_Converte_Tisolin.QrPsqBeforeClose(DataSet: TDataSet);
begin
  EdRegistrosQr.ValueVariant := 0;
end;

(*
DROP TABLE IF EXISTS _REFERENCIAS_DUPLICADAS_;

CREATE TABLE _REFERENCIAS_DUPLICADAS_
SELECT REFERENCIA, COUNT(REFERENCIA) ITENS
FROM PRODUTO
GROUP BY Referencia;

SELECT PRODUTO, REFERENCIA, DESCRICAO
FROM PRODUTO
WHERE REFERENCIA IN (
  SELECT REFERENCIA
  FROM _REFERENCIAS_DUPLICADAS_
  WHERE ITENS > 1
)
ORDER BY REFERENCIA, PRODUTO;
*)


{ Defini��o de produtos:
DROP TABLE IF EXISTS _PRODUTO_ALL_;
CREATE TABLE _PRODUTO_ALL_

SELECT "S" GGY_KIND, Ativo, DESCRICAO,
PRODUTO, Linha, Categoria,
Referencia, Patrimonio
FROM PRODUTO
WHERE Categoria = "L"
AND Patrimonio = ""

UNION

SELECT "P" GGY_KIND, Ativo, DESCRICAO,
PRODUTO, Linha, Categoria,
Referencia, Patrimonio
FROM PRODUTO
WHERE Categoria = "L"
AND Patrimonio <> ""

;

DROP TABLE IF EXISTS _PRODUTO_ACE_;
CREATE TABLE _PRODUTO_ACE_

SELECT DISTINCT ACESSORIO
FROM PRODUTOACESSORIO
;

DROP TABLE IF EXISTS PRODUTOS_GGY;
CREATE TABLE PRODUTOS_GGY
SELECT IF(ace.ACESSORIO > 0, "A", pal.GGY_KIND) TipoProd,
pal.*
FROM _PRODUTO_ALL_ pal
LEFT JOIN PRODUTOACESSORIO ace ON ace.ACESSORIO=pal.PRODUTO
ORDER BY pal.GGY_KIND DESC, pal.DESCRICAO

;
}


end.
