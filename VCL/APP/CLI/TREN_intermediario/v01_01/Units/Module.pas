unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, UnMLAGeral, UMySQLModule, dmkEdit,
  mySQLDbTables, UnGOTOy, Winsock, MySQLBatch, frxClass, frxDBSet, dmkGeral,
  Variants, StdCtrls, ComCtrls, IdBaseComponent, IdComponent,
  IdIPWatch, UnDmkEnums, UnGrl_Vars;

type
  TGraToolRent = (gbsIndef, gbsLocar, gbsOutrs);
  TDmod = class(TDataModule)
    QrMaster: TMySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrUpdU: TmySQLQuery;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrMasterECEP: TIntegerField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    MyLocDatabase: TmySQLDatabase;
    QrPriorNext: TmySQLQuery;
    QrControle: TmySQLQuery;
    QrMasterLogo2: TBlobField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrMasterLimite: TSmallintField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrMasterSolicitaSenha: TIntegerField;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrMasterENumero: TFloatField;
    QrTerminaisLicenca: TWideStringField;
    QrTransf: TmySQLQuery;
    QrTransfData: TDateField;
    QrTransfTipo: TSmallintField;
    QrTransfCarteira: TIntegerField;
    QrTransfControle: TIntegerField;
    QrTransfGenero: TIntegerField;
    QrTransfDebito: TFloatField;
    QrTransfCredito: TFloatField;
    QrTransfDocumento: TFloatField;
    frxDsMaster: TfrxDBDataset;
    QrControleContasU: TIntegerField;
    QrControleEntDefAtr: TIntegerField;
    QrControleEntAtrCad: TIntegerField;
    QrControleEntAtrIts: TIntegerField;
    QrControleBalTopoNom: TIntegerField;
    QrControleBalTopoTit: TIntegerField;
    QrControleBalTopoPer: TIntegerField;
    QrControleCasasProd: TSmallintField;
    QrControleNCMs: TIntegerField;
    QrControleParamsNFs: TIntegerField;
    QrControleCambioCot: TIntegerField;
    QrControleCambioMda: TIntegerField;
    QrControleMoedaBr: TIntegerField;
    QrControleSecuritStr: TWideStringField;
    QrControleLogoBig1: TWideStringField;
    QrControleEquiCom: TIntegerField;
    QrControleContasLnk: TIntegerField;
    QrControleLastBco: TIntegerField;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrControleEquiGru: TIntegerField;
    QrControleCNAB_Rem: TIntegerField;
    QrControleCNAB_Rem_I: TIntegerField;
    QrControlePreEmMsgIm: TIntegerField;
    QrControlePreEmMsg: TIntegerField;
    QrControlePreEmail: TIntegerField;
    QrControleContasMes: TIntegerField;
    QrControleMultiPgto: TIntegerField;
    QrControleImpObs: TIntegerField;
    QrControleProLaINSSr: TFloatField;
    QrControleProLaINSSp: TFloatField;
    QrControleVerSalTabs: TIntegerField;
    QrControleVerBcoTabs: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNAB_CaD: TIntegerField;
    QrControleCNAB_CaG: TIntegerField;
    QrControleAtzCritic: TIntegerField;
    QrControleContasTrf: TIntegerField;
    QrControleSomaIts: TIntegerField;
    QrControleContasAgr: TIntegerField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleLogoNF: TWideStringField;
    QrControleConfJanela: TIntegerField;
    QrControleDataPesqAuto: TSmallintField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleAtividad: TSmallintField;
    QrControleCidades: TSmallintField;
    QrControleChequez: TSmallintField;
    QrControlePaises: TSmallintField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrControleCambiosData: TDateField;
    QrControleCambiosUsuario: TIntegerField;
    QrControlereg10: TSmallintField;
    QrControlereg11: TSmallintField;
    QrControlereg50: TSmallintField;
    QrControlereg51: TSmallintField;
    QrControlereg53: TSmallintField;
    QrControlereg54: TSmallintField;
    QrControlereg56: TSmallintField;
    QrControlereg60: TSmallintField;
    QrControlereg75: TSmallintField;
    QrControlereg88: TSmallintField;
    QrControlereg90: TSmallintField;
    QrControleNumSerieNF: TSmallintField;
    QrControleSerieNF: TIntegerField;
    QrControleModeloNF: TSmallintField;
    QrControleMyPagTip: TSmallintField;
    QrControleMyPagCar: TSmallintField;
    QrControleControlaNeg: TSmallintField;
    QrControleFamilias: TSmallintField;
    QrControleFamiliasIts: TSmallintField;
    QrControleAskNFOrca: TSmallintField;
    QrControlePreviewNF: TSmallintField;
    QrControleOrcaRapido: TSmallintField;
    QrControleDistriDescoItens: TSmallintField;
    QrControleEntraSemValor: TSmallintField;
    QrControleMensalSempre: TSmallintField;
    QrControleBalType: TSmallintField;
    QrControleOrcaOrdem: TSmallintField;
    QrControleOrcaLinhas: TSmallintField;
    QrControleOrcaLFeed: TIntegerField;
    QrControleOrcaModelo: TSmallintField;
    QrControleOrcaRodaPos: TSmallintField;
    QrControleOrcaRodape: TSmallintField;
    QrControleOrcaCabecalho: TSmallintField;
    QrControleCoresRel: TSmallintField;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleCFiscalPadr: TWideStringField;
    QrControleSitTribPadr: TWideStringField;
    QrControleCFOPPadr: TWideStringField;
    QrControleAvisosCxaEdit: TSmallintField;
    QrControleTravaCidade: TSmallintField;
    QrControleChConfCab: TIntegerField;
    QrControleImpDOS: TIntegerField;
    QrControleUnidadePadrao: TIntegerField;
    QrControleProdutosV: TIntegerField;
    QrControleCartDespesas: TIntegerField;
    QrControleReserva: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleVersao: TIntegerField;
    QrControleVerWeb: TIntegerField;
    QrControleUFPadrao: TIntegerField;
    QrControleCidade: TWideStringField;
    QrControleDono: TIntegerField;
    QrControleSoMaiusculas: TWideStringField;
    QrControleMoeda: TWideStringField;
    QrControleErroHora: TIntegerField;
    QrControleSenhas: TIntegerField;
    QrControleSenhasIts: TIntegerField;
    QrControleSalarios: TIntegerField;
    QrControleEntidades: TIntegerField;
    QrControleEntiCtas: TIntegerField;
    QrControleUFs: TIntegerField;
    QrControleListaECivil: TIntegerField;
    QrControlePerfis: TIntegerField;
    QrControleUsuario: TIntegerField;
    QrControleContas: TIntegerField;
    QrControleCentroCusto: TIntegerField;
    QrControleDepartamentos: TIntegerField;
    QrControleDividas: TIntegerField;
    QrControleDividasIts: TIntegerField;
    QrControleDividasPgs: TIntegerField;
    QrControleCarteiras: TIntegerField;
    QrControleCarteirasU: TIntegerField;
    QrControleCartaG: TIntegerField;
    QrControleCartas: TIntegerField;
    QrControleConsignacao: TIntegerField;
    QrControleGrupo: TIntegerField;
    QrControleSubGrupo: TIntegerField;
    QrControleConjunto: TIntegerField;
    QrControlePlano: TIntegerField;
    QrControleInflacao: TIntegerField;
    QrControlekm: TIntegerField;
    QrControlekmMedia: TIntegerField;
    QrControlekmIts: TIntegerField;
    QrControleFatura: TIntegerField;
    QrControleLanctos: TLargeintField;
    QrControleLctoEndoss: TIntegerField;
    QrControleEntiGrupos: TIntegerField;
    QrControleEntiContat: TIntegerField;
    QrControleEntiCargos: TIntegerField;
    QrControleEntiMail: TIntegerField;
    QrControleEntiTel: TIntegerField;
    QrControleEntiTipCto: TIntegerField;
    QrControleAparencias: TIntegerField;
    QrControlePages: TIntegerField;
    QrControleMultiEtq: TIntegerField;
    QrControleEntiTransp: TIntegerField;
    QrControleEntiRespon: TIntegerField;
    QrControleEntiCfgRel: TIntegerField;
    QrControleExcelGru: TIntegerField;
    QrControleExcelGruImp: TIntegerField;
    QrControleMediaCH: TIntegerField;
    QrControleContraSenha: TWideStringField;
    QrControleImprime: TIntegerField;
    QrControleImprimeBand: TIntegerField;
    QrControleImprimeView: TIntegerField;
    QrControleComProdPerc: TIntegerField;
    QrControleComProdEdit: TIntegerField;
    QrControleComServPerc: TIntegerField;
    QrControleComServEdit: TIntegerField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrControlePadrPlacaCar: TWideStringField;
    QrControleServSMTP: TWideStringField;
    QrControleNomeMailOC: TWideStringField;
    QrControleDonoMailOC: TWideStringField;
    QrControleMailOC: TWideStringField;
    QrControleCorpoMailOC: TWideMemoField;
    QrControleConexaoDialUp: TWideStringField;
    QrControleMailCCCega: TWideStringField;
    QrControleContVen: TIntegerField;
    QrControleContCom: TIntegerField;
    QrControleCartVen: TIntegerField;
    QrControleCartCom: TIntegerField;
    QrControleCartDeS: TIntegerField;
    QrControleCartReS: TIntegerField;
    QrControleCartDeG: TIntegerField;
    QrControleCartReG: TIntegerField;
    QrControleCartCoE: TIntegerField;
    QrControleCartCoC: TIntegerField;
    QrControleCartEmD: TIntegerField;
    QrControleCartEmA: TIntegerField;
    QrControleMoedaVal: TFloatField;
    QrControleTela1: TIntegerField;
    QrControleChamarPgtoServ: TIntegerField;
    QrControleFormUsaTam: TIntegerField;
    QrControleFormHeight: TIntegerField;
    QrControleFormWidth: TIntegerField;
    QrControleFormPixEsq: TIntegerField;
    QrControleFormPixDir: TIntegerField;
    QrControleFormPixTop: TIntegerField;
    QrControleFormPixBot: TIntegerField;
    QrControleFormFoAlt: TIntegerField;
    QrControleFormFoPro: TFloatField;
    QrControleFormUsaPro: TIntegerField;
    QrControleFormSlides: TIntegerField;
    QrControleFormNeg: TIntegerField;
    QrControleFormIta: TIntegerField;
    QrControleFormSub: TIntegerField;
    QrControleFormExt: TIntegerField;
    QrControleFormFundoTipo: TIntegerField;
    QrControleFormFundoBMP: TWideStringField;
    QrControleServInterv: TIntegerField;
    QrControleServAntecip: TIntegerField;
    QrControleAdiLancto: TIntegerField;
    QrControleContaSal: TIntegerField;
    QrControleContaVal: TIntegerField;
    QrControlePronomeE: TWideStringField;
    QrControlePronomeM: TWideStringField;
    QrControlePronomeF: TWideStringField;
    QrControlePronomeA: TWideStringField;
    QrControleSaudacaoE: TWideStringField;
    QrControleSaudacaoM: TWideStringField;
    QrControleSaudacaoF: TWideStringField;
    QrControleSaudacaoA: TWideStringField;
    QrControleNiver: TSmallintField;
    QrControleNiverddA: TSmallintField;
    QrControleNiverddD: TSmallintField;
    QrControleLastPassD: TDateTimeField;
    QrControleMultiPass: TIntegerField;
    QrControleCodigo: TIntegerField;
    QrControleAlterWeb: TSmallintField;
    QrControleAtivo: TSmallintField;
    QrMasterUsaAccMngr: TSmallintField;
    QrNew: TmySQLQuery;
    QrNewCodigo: TIntegerField;
    QlLocal: TMySQLBatchExecute;
    QrUpdM: TmySQLQuery;
    QrSomaM: TmySQLQuery;
    QrSomaMValor: TFloatField;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrDuplicStrX: TmySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrDuplicIntX: TmySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrFields: TmySQLQuery;
    QrDelLogX: TmySQLQuery;
    QrInsLogX: TmySQLQuery;
    QrRecCountX: TmySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrSel: TmySQLQuery;
    QrSelMovix: TIntegerField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QrAuxL: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrBinaLigouA: TmySQLQuery;
    DsBinaLigouA: TDataSource;
    QrBinaLigouACodigo: TAutoIncField;
    QrBinaLigouANome: TWideStringField;
    QrBinaLigouAxData: TWideStringField;
    QrBinaLigouAxHora: TWideStringField;
    QrBinaLigouAdData: TDateField;
    QrBinaLigouAdHora: TTimeField;
    QrBinaLigouAUsuario: TIntegerField;
    QrBinaLigouATerminal: TIntegerField;
    QrBinaLigouAIP: TWideStringField;
    QrBinaLigouAForcado: TSmallintField;
    QrBinaAB: TmySQLQuery;
    QrBinaLigouALk: TIntegerField;
    QrBinaLigouADataCad: TDateField;
    QrBinaLigouADataAlt: TDateField;
    QrBinaLigouAUserCad: TIntegerField;
    QrBinaLigouAUserAlt: TIntegerField;
    QrBinaLigouAAlterWeb: TSmallintField;
    QrBinaLigouAAtivo: TSmallintField;
    QrBinaLigouATELEFONE: TWideStringField;
    QrControleBLQ_TopoAvisoV: TIntegerField;
    QrControleBLQ_MEsqAvisoV: TIntegerField;
    QrControleBLQ_AltuAvisoV: TIntegerField;
    QrControleBLQ_LargAvisoV: TIntegerField;
    QrControleBLQ_TopoDestin: TIntegerField;
    QrControleBLQ_MEsqDestin: TIntegerField;
    QrControleBLQ_AltuDestin: TIntegerField;
    QrControleBLQ_LargDestin: TIntegerField;
    QrOpcoesTRen: TmySQLQuery;
    QrOpcoesTRenCodigo: TIntegerField;
    QrOpcoesTRenDefContrat: TIntegerField;
    QrOpcoesTRenGraNivPatr: TIntegerField;
    QrOpcoesTRenGraCodPatr: TIntegerField;
    QrOpcoesTRenGraNivOutr: TIntegerField;
    QrOpcoesTRenGraCodOutr: TIntegerField;
    QrLCPP: TmySQLQuery;
    QrLCPPDtHrLocado: TDateTimeField;
    QrLCPPDtHrRetorn: TDateTimeField;
    QrLCPPLibDtHr: TDateTimeField;
    QrLCPPGraGruX: TIntegerField;
    QrGGXP: TmySQLQuery;
    QrGGXPSituacao: TWordField;
    QrOpcoesTRenBloPrdSPer: TSmallintField;
    QrOpcoesTRenTipCodPatr: TIntegerField;
    QrOpcoesTRenTipCodOutr: TIntegerField;
    QrUpd2: TmySQLQuery;
    MyDB: TMySQLDatabase;
    QrOpcoesTRenFormaCobrLoca: TSmallintField;
    QrOpcoesTRenHrLimPosDiaNaoUtil: TTimeField;
    QrOpcoesTRenLocArredMinut: TIntegerField;
    QrOpcoesTRenLocArredHrIniIni: TTimeField;
    QrOpcoesTRenLocArredHrIniFim: TTimeField;
    QrOpcoesTRenPermLocSemEstq: TSmallintField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrControleAfterOpen(DataSet: TDataSet);
    procedure QrBinaLigouACalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FCamposBinaLigou: String;
    FFmtPrc, FProdDelete: Integer;
    FStrFmtPrc, FStrFmtCus: String;
    function Privilegios(Usuario : Integer) : Boolean;
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
    procedure VerificaSenha(Index: Integer; Login, Senha: String);

    function BuscaProximoMovix: Integer;
    procedure InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
              DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
              CliInt: Integer; Campos: String; DescrSubstit: Boolean);
    function  TabelasQueNaoQueroCriar(): String;
    procedure ReopenControle();

    // Compatibilidade Estoque / NFe
    procedure AlteraSMIA(Qry: TmySQLQuery);
    procedure ExcluiItensEstqNFsCouroImportadas();
    procedure GeraEstoqueEm_2(Empresa_Txt: String; DataIni,
      DataFim: TDateTime; PGT, GG1, GG2, GG3, TipoPesq: Integer; GraCusPrc,
      TabePrcCab: Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
      GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer; QrPGT_SCC, QrAbertura2a,
      QrAbertura2b: TmySQLQuery; UsaTabePrcCab, SoPositivos: Boolean;
      LaAviso: TLabel; NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery; var TxtSQL_Part1,
      TxtSQL_Part2: String);
    // Fim Compatibilidade

    // Exclusivos ToolRent
    function  FiltroGrade(const Material: TGraToolRent; var Filtro: String):
              Boolean;
    function  CodigoDeSitPat(Codigo: String): Byte;
    procedure ReopenOpcoesTRen();
    procedure VerificaSituacaoPatrimonio(GraGruX: Integer);
    procedure ReopenFixEqui(Indice: Integer; Qry: TmySQLQuery);
    procedure ReopenParamsEspecificos(Empresa: Integer);
    // fim ToolRent
  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;

implementation

uses UnMyObjects, Principal, SenhaBoss, Servidor, VerifiDB, MyDBCheck,
  InstallMySQL41, ToolRent_Dmk, MyListas, ModuleGeral, ModuleFin, UnLic_Dmk,
  DmkDAC_PF, UnDmkProcFunc, VerifiDBTerceiros;

{$R *.DFM}

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  Versao, VerZero, Resp: Integer;
  BD: String;
  Verifica, VerificaDBTerc: Boolean;
begin
  VerificaDBTerc := False;
  //
  Verifica := False;
  if MyDB.Connected then
    Geral.MensagemBox('MyDB est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  if MyLocDataBase.Connected then
    Geral.MensagemBox('MyLocDataBase est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);

  MyDB.LoginPrompt := False;
  ZZDB.LoginPrompt := False;
  MyLocDatabase.LoginPrompt := False;

  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  VAR_BDSENHA := 'wkljweryhvbirt';
  if not GOTOy.OMySQLEstaInstalado(FmToolRent_Dmk.LaAviso1,
    FmToolRent_Dmk.LaAviso2, FmToolRent_Dmk.ProgressBar1) then
  begin
    //raise EAbort.Create('');
    //
    MyObjects.Informa2(FmToolrent_Dmk.LaAviso1, FmToolrent_Dmk.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    FmToolrent_Dmk.BtEntra.Visible := False;
    //
    Exit;
  end;
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  {
  ZZDB.Host         := VAR_SQLHOST;
  ZZDB.UserName     := VAR_SQLUSER;
  ZZDB.UserPassword := VAR_BDSENHA;
  ZZDB.Port         := VAR_PORTA;
  }
  UnDmkDAC_PF.ConectaMyDB_DAC(ZZDB, 'mysql', VAR_IP, VAR_PORTA, VAR_SQLUSER,
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE User SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then ShowMessage('Banco de dados teste n�o se conecta!');
    end;
  end;
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MensagemBox('M�quina cliente sem rede.', 'Erro', MB_OK+MB_ICONERROR);
        //Application.Terminate;
      end;
    end;
  except
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
  {
  MyDB.UserName     := VAR_SQLUSER;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host         := VAR_IP;
  MyDB.Port         := VAR_PORTA;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  }
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql', VAR_IP, VAR_PORTA, VAR_SQLUSER,
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  //
  QrAux.Close;
  QrAux.Database := MyDB;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  QrAux.Open;
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MensagemBox('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?', 'Banco de Dados',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      if MyDB.Connected then
        MyDB.Disconnect;
      MyDB.DatabaseName := TMeuDB;
      Verifica := True;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MensagemBox('O aplicativo ser� encerrado!', 'Banco de Dados',
      MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
  {
  MyLocDatabase.UserName     := VAR_SQLUSER;
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.Port         := VAR_PORTA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  }
  UnDmkDAC_PF.ConectaMyDB_DAC(MyLocDatabase, 'mysql', VAR_IP, VAR_PORTA,
    VAR_SQLUSER, VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True,
    (*Conecta*)False);
  //
{20200920
  QrAuxL.Close;
  QrAuxL.DataBase := MyDB;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  QrAuxL.Open;
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  QrAuxL.DataBase := MyLocDatabase;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MensagemBox('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?',
      'Banco de Dados Local', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MensagemBox('O aplicativo ser� encerrado!',
      'Banco de Dados Local', MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
}
  //
  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  if Versao < CO_Versao then
    Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Geral.MensagemBox('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?',
    'Aus�ncia de Informa��o de Vers�o no Registro',
    MB_YESNOCANCEL+MB_ICONWARNING);
    if Resp = ID_YES then
      Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  Mylist.ConfiguracoesIniciais(1, Application.Title);
  //
  if Verifica then
  begin
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
      //
      VerificaDBTerc := True;
    end;
  end;
  //
  ReopenOpcoesTRen();
  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados Geral', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo Financeiro', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;

  try
    QrMaster.Open;
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    on E: Exception do
    begin
      Geral.MB_Erro('ERRO: N�o foi possivel abrir a tabela master. ' +
      sLineBreak + E.ClassName + ': ' + E.Message + '.');
      try
        Application.CreateForm(TFmVerifiDB, FmVerifiDB);
        with FmVerifiDb do
        begin
          BtSair.Enabled := False;
          FVerifi := True;
          ShowModal;
          FVerifi := False;
          Destroy;
          //
          VerificaDBTerc := True;
        end;
      except;
        MyDB.DatabaseName := CO_VAZIO;
        raise;
      end;
    end;
  end;
  if VerificaDBTerc then
  begin
    Application.CreateForm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros);
    FmVerifiDBTerceiros.FVerifi := True;
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.FVerifi := False;
    FmVerifiDBTerceiros.Destroy;
  end;
  //
  {
  if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  //FmPrincipal.VerificaTerminal;
  }
  //
  //Lic_Dmk.LiberaUso5;
  //Geral.MB_Aviso('"Lic_Dmk" desabilitado!');
end;

procedure TDmod.ReopenControle();
begin
  QrControle.Close;
  QrControle.Open;
end;

procedure TDmod.ReopenFixEqui(Indice: Integer; Qry: TmySQLQuery);
begin
  //case RGTabela.ItemIndex of
  case Indice of
    1:
    begin
      //UnDMkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggx.Controle CNTRL_GGX, ',
      'gg1.Nome NO_EQUI ',
      'FROM gragrux ggx  ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'ORDER BY NO_EQUI ',
      ' ']);
    end;
    2:
    begin
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT gxp.GraGruX CNTRL_GGX, ',
      'gxp.DescrTerc NO_EQUI',
      'FROM fixgxpatr gxp ',
      'ORDER BY NO_EQUI',
      ' ']);
    end;
    else Qry.Close;
  end;
end;

procedure TDmod.ReopenOpcoesTRen();
const
  sProcName = 'TDmod.ReopenOpcoesTRen()';
begin
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrOpcoesTRen, MyDB, [
    'SELECT * FROM opcoestren ',
    '']);
  except
    Geral.MB_Erro('N�o foi poss�vel Abrir as opc�es do aplicativo ' + sProcName);
  end;
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  //Compatibilidade
end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
var
  Saldo: Double;
begin
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM Carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM Lanctos');
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM Lanctos');
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE Carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
(*  if Localiza = 3 then
  begin
    if Dmod.QrLanctos.State in [dsBrowse] then
    if (Dmod.QrLanctosTipo.Value = Tipo)
    and (Dmod.QrLanctosCarteira.Value = Carteira) then
    Localiza := 1;
    Dmod.DefParams;
  end;
  if Localiza = 1 then GOTOx.LocalizaCodigo(Carteira, Carteira);*)
end;

procedure TDmod.VerificaSenha(Index: Integer; Login, Senha: String);
begin
  if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
  else
  begin
    if Index = 7 then
    begin
(*      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      QrSenha.Open;
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        QrPerfis.Open;
        if QrPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else Geral.MensagemBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;                      *)
    end else ShowMessage('Em constru��o');
  end;
end;

procedure TDmod.VerificaSituacaoPatrimonio(GraGruX: Integer);
var
  SitAtu, SitNew: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLCPP, Dmod.MyDB, [
  'SELECT lpp.GraGruX, ',
  'lpp.DtHrLocado, lpp.DtHrRetorn, lpp.LibDtHr ',
  'FROM loccpatpri lpp ',
  'WHERE lpp.GraGruX=' + Geral.FF0(GraGruX),
  'ORDER BY lpp.DtHrLocado DESC ',
  'LIMIT 1 ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGGXP, Dmod.MyDB, [
  'SELECT Situacao ',
  'FROM gragxpatr ',
  'WHERE GraGruX=' + Geral.FF0(QrLCPPGraGruX.Value),
  '']);
  SitAtu := QrGGXPSituacao.Value;
  SitNew := QrGGXPSituacao.Value;
  //
  if (SitAtu = CO_SIT_PATR_070_F_Furtado)
  or (SitAtu = CO_SIT_PATR_079_O_Oficina)
  or (SitAtu = CO_SIT_PATR_082_R_Reservado)
  or (SitAtu = CO_SIT_PATR_086_V_Vendido)  then
  begin
    // n�o faz nada, pois foi setado manualmente!
  end else
  begin
    if ((QrLCPPLibDtHr.Value > 2)
    and (QrLCPPDtHrRetorn.Value > 2)) then
      SitNew := CO_SIT_PATR_068_D_Disponivel
    else
    if (QrLCPPDtHrRetorn.Value < 2) then
      SitNew := CO_SIT_PATR_076_L_Locado
    else
    if (QrLCPPLibDtHr.Value < 2) then
      SitNew := CO_SIT_PATR_073_I_Inspecao
  end;
  //
  if SitAtu <> SitNew then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragxpatr', False, [
    'Situacao'], ['GraGruX'], [SitNew], [GraGruX], True);
  end;
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
    GOTOy.AvisoIndef(4);
  //
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value+' # '+
  QrMasterCNPJ.Value;
  //
  ReopenControle();
  //
  if QrControleSoMaiusculas.Value = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
  VAR_MOEDA           := QrControleMoeda.Value;
  //
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
(*  case QrMasterTipo.Value of
    0: QrMasterEm.Value := QrMasterRazaoSocial.Value;
    1: QrMasterEm.Value := QrMasterNome.Value;
    else QrMasterEm.Value := QrMasterRazaoSocial.Value;
  end;*)
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value :=Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  //
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  WSACleanup;
end;

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  FM_MASTER := 'F';
  Dmod.QrPerfis.Params[0].AsInteger := Usuario;
  Dmod.QrPerfis.Open;
  if Dmod.QrPerfis.RecordCount > 0 then
  begin
    Result := True;
    //FM_EQUIPAMENTOS             := Dmod.QrPerfisEquipamentos.Value;
    //FM_MARCAS          := Dmod.QrPerfisMarcas.Value;
    //FM_MODELOS                := Dmod.QrPerfisModelos.Value;
    //FM_SERVICOS            := Dmod.QrPerfisServicos.Value;
    //FM_PRODUTOS            := Dmod.QrPerfisProdutos.Value;
  end;
  Dmod.QrPerfis.Close;
end;

procedure TDmod.ExcluiItensEstqNFsCouroImportadas();
begin
  // Compatibilidade
end;

function TDmod.FiltroGrade(const Material: TGraToolRent; var Filtro: String): Boolean;
var
  GraNiv: Integer;
  //Txt1, TxtA,
  GraCod, Txt2: String;
begin
  //Result := False;
  Filtro := '';
  //
{
  if Servico <> gbsIndef then
  begin
    TxtA := '';
    Txt1 := '';
    Txt2 := '';
    //
    case Material of
      //gbmIndef: ;
      //gbmEquiEProd: ;
      gbmEquipam: GraNiv := Dmod.QrOpcoesTRenGraNivEqAp.Value;
      gbmProduto: GraNiv := Dmod.QrOpcoesTRenGraNivPrAp.Value;
      else GraNiv := 0;
    end;
    if GraNiv > 0 then
    begin
      case Material of
        //gbmIndef: ;
        //gbmEquiEProd: ;
        gbmEquipam: GraCod := Geral.FF0(Dmod.QrOpcoesTRenGraCodEqAp.Value);
        gbmProduto: GraCod := Geral.FF0(Dmod.QrOpcoesTRenGraCodPrAp.Value);
        else GraNiv := 0;
      end;
      case GraNiv of
        //0: Txt := '';
        1: Txt1 := ' gg1.Nivel1=' + GraCod;
        2: Txt1 := ' gg1.Nivel2=' + GraCod;
        3: Txt1 := ' gg1.Nivel3=' + GraCod;
        4: Txt1 := ' gg1.Nivel4=' + GraCod;
        5: Txt1 := ' gg1.Nivel5=' + GraCod;
        6: Txt1 := ' gg1.PrdGrupTip=' + GraCod;
      end;
    end;
    //
}
    case Material of
      gbsLocar: GraNiv := Dmod.QrOpcoesTRenGraNivPatr.Value;
      gbsOutrs: GraNiv := Dmod.QrOpcoesTRenGraNivOutr.Value;
      else GraNiv := 0;
    end;
    if GraNiv > 0 then
    begin
      case Material of
        gbsLocar: GraCod := Geral.FF0(Dmod.QrOpcoesTRenGraCodPatr.Value);
        gbsOutrs: GraCod := Geral.FF0(Dmod.QrOpcoesTRenGraCodOutr.Value);
        else GraNiv := 0;
      end;
      case GraNiv of
        //0: Txt := '';
        1: Txt2 := ' gg1.Nivel1=' + GraCod;
        2: Txt2 := ' gg1.Nivel2=' + GraCod;
        3: Txt2 := ' gg1.Nivel3=' + GraCod;
        4: Txt2 := ' gg1.Nivel4=' + GraCod;
        5: Txt2 := ' gg1.Nivel5=' + GraCod;
        6: Txt2 := ' gg1.PrdGrupTip=' + GraCod;
        7: Txt2 := ' NOT (cpl.GraGruX IS NULL) ';
      end;
    end;
    //
{    case Servico of
      gbsIndef: TxtA := 'gg1.Nivel1 <> 0';
      gbsAplEMon:
      begin
        if (Txt1 <> '') and (Txt2 <> '') then
          TxtA := '((' + Txt1 + ') OR (' + Txt2 + '))'
        else if Txt1 <> '' then
          TxtA := Txt1
        else if Txt2 <> '' then
          TxtA := Txt2
        else
          TxtA := 'gg1.Nivel1 = 0';
      end;
      gbsAplica:
      begin
        if Txt1 <> '' then
          TxtA := Txt1
        else if Txt2 <> '' then
          TxtA := Txt2
        else
          TxtA := 'gg1.Nivel1 = 0';
      end;
      gbsMonitora:
      begin
        if Txt2 <> '' then
          TxtA := Txt2
        else
          TxtA := 'gg1.Nivel1 = 0';
      end;
    end;
    Result := TxtA;
  end;
}
  Filtro := Txt2;
  Result := Trim(Filtro) <> '';
  //
  if not Result then
  begin
    Geral.MensagemBox('Filtro de grade n�o definido!' + #13#10 +
    'Defina em op��es espec�ficas!', 'Aviso', MB_OK+MB_ICONWARNING);
    //
    //  Ter certeza que n�o trar� nenhum registro!
    Filtro := '((gg1.PrdGrupTip=0) AND (gg1.PrdGrupTip<>0))'
  end;
end;

procedure TDmod.GeraEstoqueEm_2(Empresa_Txt: String; DataIni,
  DataFim: TDateTime; PGT, GG1, GG2, GG3, TipoPesq: Integer; GraCusPrc,
  TabePrcCab: Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
  GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer; QrPGT_SCC, QrAbertura2a,
  QrAbertura2b: TmySQLQuery; UsaTabePrcCab, SoPositivos: Boolean;
  LaAviso: TLabel; NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery; var TxtSQL_Part1,
  TxtSQL_Part2: String);
begin
  Geral.MensagemBox('Gera��o de estoque n�o implementada!' + #13#10 +
  'Solicite � DERMATEK!', 'Mensagem', MB_OK+MB_ICONWARNING);
end;

procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: TMySQLQuery;
DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
CliInt: Integer; Campos: String; DescrSubstit: Boolean);
begin
  Geral.MensagemBox('Este balancete n�o possui dados industriais ' +
  'adicionais neste aplicativo!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TDmod.AlteraSMIA(Qry: TmySQLQuery);
begin
  // Compatibilidade?
end;

function TDmod.BuscaProximoMovix: Integer;
begin
  QrSel.Close;
  QrSel.Open;
  Result := QrSelMovix.Value;
end;

function TDmod.CodigoDeSitPat(Codigo: String): Byte;
begin
  if Length(Codigo) = 0 then
    Result := 0
  else
    Result := Ord(Codigo[1]);  
end;

procedure TDmod.QrBinaLigouACalcFields(DataSet: TDataSet);
begin
  QrBinaLigouATELEFONE.Value := Geral.FormataTelefone_TT(QrBinaLigouAnome.Value);
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  FFmtPrc    := Dmod.QrControleCasasProd.Value;
  FStrFmtPrc := dmkPF.StrFmt_Double(Dmod.QrControleCasasProd.Value);
  FStrFmtCus := dmkPF.StrFmt_Double(4);
  //
  VAR_MEULOGOPATH := QrControleMeuLogoPath.Value;
  if FileExists(VAR_MEULOGOPATH) then
    VAR_MEULOGOEXISTE := True
  else
    VAR_MEULOGOEXISTE := False;
end;

end.
