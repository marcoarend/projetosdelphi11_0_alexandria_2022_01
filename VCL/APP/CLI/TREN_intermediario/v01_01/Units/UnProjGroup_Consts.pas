unit UnProjGroup_Consts;

interface

uses
  UnDmkListas, UnDmkEnums, UnAppEnums;

type
  TInsUpdTMIPrcExecID = (iutpei000=000,
    iutpei001=001, iutpei002=002, iutpei003=003, iutpei004=004, iutpei005=005,
    iutpei006=006, iutpei007=007, iutpei008=008, iutpei009=009, iutpei010=010,
    iutpei011=011, iutpei012=012, iutpei013=013, iutpei014=014, iutpei015=015,
    iutpei016=016, iutpei017=017, iutpei018=018, iutpei019=019, iutpei020=020,
    iutpei021=021, iutpei022=022, iutpei023=023, iutpei024=024, iutpei025=025,
    iutpei026=026, iutpei027=027, iutpei028=028, iutpei029=029, iutpei030=030,
    iutpei031=031, iutpei032=032, iutpei033=033, iutpei034=034, iutpei035=035,
    iutpei036=036, iutpei037=037, iutpei038=038, iutpei039=039, iutpei040=040,
    iutpei041=041, iutpei042=042, iutpei043=043, iutpei044=044, iutpei045=045,
    iutpei046=046, iutpei047=047, iutpei048=048, iutpei049=049, iutpei050=050,
    iutpei051=051, iutpei052=052, iutpei053=053, iutpei054=054, iutpei055=055,
    iutpei056=056, iutpei057=057, iutpei058=058, iutpei059=059, iutpei060=060,
    iutpei061=061, iutpei062=062, iutpei063=063, iutpei064=064, iutpei065=065,
    iutpei066=066, iutpei067=067, iutpei068=068, iutpei069=069, iutpei070=070,
    iutpei071=071, iutpei072=072, iutpei073=073, iutpei074=074, iutpei075=075,
    iutpei076=076, iutpei077=077, iutpei078=078, iutpei079=079, iutpei080=080,
    iutpei081=081, iutpei082=082, iutpei083=083, iutpei084=084, iutpei085=085,
    iutpei086=086, iutpei087=087, iutpei088=088, iutpei089=089, iutpei090=090,
    iutpei091=091, iutpei092=092, iutpei093=093, iutpei094=094, iutpei095=095,
    iutpei096=096, iutpei097=097, iutpei098=098, iutpei099=099, iutpei100=100,
    iutpei101=101, iutpei102=102, iutpei103=103, iutpei104=104, iutpei105=105,
    iutpei106=106, iutpei107=107, iutpei108=108, iutpei109=109, iutpei110=110);

const
  CO_RGTIPOCOURO_COLUNAS = 3;
  CO_TX_MaxLin = 24;
  CO_DATA_HORA_SMI = 'DataHora';
  CO_DATA_HORA_GRL = 'DataHora';
  //CO_SEL_TAB_VMI = '*#*ERRO#*#';

  CO_0_GGXRcl = 0;
  CO_0_SerieTal = -1;
  CO_0_Talao = 0;
  CO_0_MovCodPai: Integer = 0;
  CO_0_IxxMovIX: TEstqMovInfo = TEstqMovInfo.eminfIndef;
  CO_0_IxxFolha: Integer = 0;
  CO_0_IxxLinha: Integer = 0;
  CO_TRUE_ExigeClientMO: Boolean = True;
  CO_TRUE_ExigeFornecMO: Boolean = True;
  CO_TRUE_ExigeStqLoc: Boolean = True;
  CO_FALSE_ExigeClientMO: Boolean = False;
  CO_FALSE_ExigeFornecMO: Boolean = False;
  CO_FALSE_ExigeStqLoc: Boolean = False;

  CO_TX___Cab = 'TX---Cab';
  CO_TXXxxTab = 'TX???Cab';
  CO_TAB_TMI = 'txmovits';
  CO_TAB_TMB = 'txmovitb';
  CO_SEL_TAB_XMI = CO_TAB_TMI;
  CO_SEL_TAB_TMI = CO_TAB_TMI;
  CO_INS_TAB_TMI = CO_TAB_TMI;
  CO_UPD_TAB_TMI = CO_TAB_TMI;
  CO_DEL_TAB_TMI = CO_TAB_TMI;
  CO_FLD_TAB_TMI = CO_TAB_TMI;
  CO_TAB_TAB_TMI = CO_TAB_TMI;
  //
  CO_TAB_XX_MOV_ID_LOC = 'txmovidloc';

  CO_DATA_HORA_TMI = 'DataHora';

  CO_TXT_iutpei000 = '';

  CO_TXT_iutpei001 = 'Item de entrada de mat�ria prima';

  CO_TXT_iutpei002 = 'Origem de mat�ria prima em processo (baixa)';
  CO_TXT_iutpei003 = 'Artigo em confec��o (em processo)';
  CO_TXT_iutpei004 = 'Artigo gerado em confec��o';
  CO_TXT_iutpei005 = 'Baixa de artigo em confec��o';

  CO_TXT_iutpei006 = ''; //'Gera��o de artigo durante o processo de caleiro';
  CO_TXT_iutpei007 = ''; //'Baixa de mat�ria prima origem durante o processo de caleiro';
  CO_TXT_iutpei008 = ''; //'Gera��o de subproduto (UNI) em processo de caleiro';
  CO_TXT_iutpei009 = ''; //'Gera��o de subproduto (MUL) em processo de caleiro';

  CO_TXT_iutpei010 = ''; //'Cabe�alho de processo de curtimento';
  CO_TXT_iutpei011 = ''; //'Mat�ria prima origem no processo de curtimento';
  CO_TXT_iutpei012 = ''; //'Gera��o autom�tica de couro caleado no curtimento';
  CO_TXT_iutpei013 = ''; //'Baixa de Couro PDA na gera��o autom�tica de couro caleado no curtimento';
  CO_TXT_iutpei014 = ''; //'Gera��o de Couro DTA no processo de curtimento';
  CO_TXT_iutpei015 = ''; //'Baixa de caleado na gera��o do DTA no processo de curtimento';
  CO_TXT_iutpei016 = ''; //'Gera��o de artigo durante o processo de curtimento';
  CO_TXT_iutpei017 = ''; //'Baixa de mat�ria prima origem durante processo de curtimento';

  CO_TXT_iutpei018 = ''; //'Cabe�alho de gera��o de artigo de ribeira';
  CO_TXT_iutpei019 = ''; //'Gera��o de artigo (UNI) de ribeira de curtimento';
  CO_TXT_iutpei020 = ''; //'Baixa de mat�ria prima na gera��o de artigo (UNI) de ribeira de curtimento';
  CO_TXT_iutpei021 = ''; //'Gera��o de artigo (MUL) de ribeira de curtimento';
  CO_TXT_iutpei022 = ''; //'Baixa de mat�ria prima na gera��o de artigo (MUL) de ribeira de curtimento';
  CO_TXT_iutpei023 = ''; //'Gera��o de artigo de ribeira direto da barraca';
  CO_TXT_iutpei024 = ''; //'Baixa de mat�ria prima na gera��o de artigo de ribeira direto da barraca';
  CO_TXT_iutpei025 = ''; //'Gera��o de artigo de ribeira (Altera��o de In Natura)';
  CO_TXT_iutpei026 = ''; //'Baixa de mat�ria prima na gera��o de artigo de ribeira (Altera��o de In Natura)';

  CO_TXT_iutpei027 = ''; //'Classifica��o de artigo de ribeira (UNI) 1/4';
  CO_TXT_iutpei028 = ''; //'Baixa de artigo de ribeira em classifica��o (UNI) 1/4';
  CO_TXT_iutpei029 = ''; //'Classifica��o de artigo de ribeira (UNI) 2/4';
  CO_TXT_iutpei030 = ''; //'Baixa de artigo de ribeira em classifica��o (UNI) 2/4';
  CO_TXT_iutpei031 = ''; //'Classifica��o de artigo de ribeira (UNI) 3/4';
  CO_TXT_iutpei032 = ''; //'Baixa de artigo de ribeira em classifica��o (UNI) 3/4';
  CO_TXT_iutpei033 = ''; //'Classifica��o de artigo de ribeira (UNI) 4/4';
  CO_TXT_iutpei034 = ''; //'Baixa de artigo de ribeira em classifica��o (UNI) 4/4';

  CO_TXT_iutpei035 = 'Item de Entrada de Artigo Semi Pronto por Compra';

  CO_TXT_iutpei036 = ''; //'Classifica��o de artigo de ribeira (MUL)';
  CO_TXT_iutpei037 = ''; //'Baixa de artigo de ribeira em classifica��o (MUL)';

  CO_TXT_iutpei038 = ''; //'Prepara��o de pallet para reclassifica��o 1/2';
  CO_TXT_iutpei039 = ''; //'Baixa na prepara��o de pallet para reclassifica��o 1/2';
  CO_TXT_iutpei040 = ''; //'Prepara��o de pallet para reclassifica��o 2/2';
  CO_TXT_iutpei041 = ''; //'Baixa na prepara��o de pallet para reclassifica��o 2/2';

  CO_TXT_iutpei042 = ''; //'Reclassifica��o de artigo de ribeira (UNI) 1/4';
  CO_TXT_iutpei043 = ''; //'Baixa de artigo de ribeira em reclassifica��o (UNI) 1/4';
  CO_TXT_iutpei044 = ''; //'Reclassifica��o de artigo de ribeira (UNI) 2/4';
  CO_TXT_iutpei045 = ''; //'Baixa de artigo de ribeira em reclassifica��o (UNI) 2/4';
  CO_TXT_iutpei046 = ''; //'Reclassifica��o de artigo de ribeira (UNI) 3/4';
  CO_TXT_iutpei047 = ''; //'Baixa de artigo de ribeira em reclassifica��o (UNI) 3/4';
  CO_TXT_iutpei048 = ''; //'Reclassifica��o de artigo de ribeira (UNI) 4/4';
  CO_TXT_iutpei049 = ''; //'Baixa de artigo de ribeira em reclassifica��o (UNI) 4/4';
  CO_TXT_iutpei050 = ''; //'Reclassifica��o de artigo de ribeira (MUL)';
  CO_TXT_iutpei051 = ''; //'Baixa de artigo de ribeira em reclassifica��o (MUL)';

  CO_TXT_iutpei052 = ''; //'Zeramento de estoque de pallet de origem 1/3';
  CO_TXT_iutpei053 = ''; //'Zeramento de estoque de pallet de origem 2/3';
  CO_TXT_iutpei054 = ''; //'Zeramento de estoque de pallet de origem 3/3';

  CO_TXT_iutpei055 = 'Ajuste de estoque'; //'Ajuste de estoque';

  CO_TXT_iutpei056 = ''; //'Entrada de couro sem cobertura';

  CO_TXT_iutpei057 = 'Baixa For�ada 1/2'; //'Baixa for�ada de couro na ribeira 1/2';
  CO_TXT_iutpei058 = 'Baixa For�ada 2/2'; //'Baixa for�ada de couro na ribeira 2/2';

  CO_TXT_iutpei059 = 'Baixa extra 1/2'; //'Baixa extra de couro na ribeira 1/2';
  CO_TXT_iutpei060 = 'Baixa extra 2/2'; //'Baixa extra de couro na ribeira 2/2';

  CO_TXT_iutpei061 = ''; //'Corre��o de baixa em reclassifica��o';

  CO_TXT_iutpei062 = ''; //'Gera��o de couro que est� em opera��o';
  CO_TXT_iutpei063 = ''; //'Baixa de couro em opera��o por IME-I';
  CO_TXT_iutpei064 = ''; //'Baixa de couro em opera��o por pallet';
  CO_TXT_iutpei065 = ''; //'Gera��o de couro resultante de opera��o';
  CO_TXT_iutpei066 = ''; //'Baixa de couro em opera��o na gera��o de couro resultante de opera��o';

  CO_TXT_iutpei067 = ''; //'Gera��o de couro que est� em processo';
  CO_TXT_iutpei068 = ''; //'Baixa de couro em processo por IME-I';
  CO_TXT_iutpei069 = ''; //'Baixa de couro em processo por pallet';
  CO_TXT_iutpei070 = ''; //'Gera��o de couro resultante de processo';
  CO_TXT_iutpei071 = ''; //'Baixa de couro em processo na gera��o de couro resultante de processo';
  CO_TXT_iutpei072 = ''; //'Alera��o de baixa de couro em processo por IME-I';
  CO_TXT_iutpei073 = ''; //'Desclassifica��o em processo de semi/acabado';
  CO_TXT_iutpei074 = ''; //'Baixa de origem na desclassifica��o em processo de semi/acabado';
  CO_TXT_iutpei075 = ''; //'Gera��o de raspa em processo de semi/acabado';
  CO_TXT_iutpei076 = ''; //'Baixa de origem na gera��o de raspa em processo de semi/acabado';

  CO_TXT_iutpei077 = 'Gera��o de material no destino em transfer�ncia de estoque por IME-I';
  CO_TXT_iutpei078 = 'Baixa de material da origem em transfer�ncia de estoque por IME-I';
  CO_TXT_iutpei079 = 'Gera��o de material no destino em transfer�ncia de estoque por pallet';
  CO_TXT_iutpei080 = 'Baixa de material da origem em transfer�ncia de estoque por pallet';

  CO_TXT_iutpei081 = ''; //'Gera��o de subproduto em processo em processamento de subproduto';
  CO_TXT_iutpei082 = ''; //'Baixa de subproduto origem em processamento de subproduto';
  CO_TXT_iutpei083 = ''; //'Gera��o de subproduto processado em processamento de subproduto';
  CO_TXT_iutpei084 = ''; //'Baixa de subproduto em processo na gera��o de produto processado em processamento de subproduto';

  CO_TXT_iutpei085 = ''; //'Entrada por devolu��o definitiva de cliente ';

  CO_TXT_iutpei086 = ''; //'Entrada por devolu��o de cliente para retrabalho';

  CO_TXT_iutpei087 = 'Gera��o de material que est� em reprocesso/reparo';
  CO_TXT_iutpei088 = 'Baixa de material em reprocesso/reparo por IME-I';
  CO_TXT_iutpei089 = 'Baixa de material em reprocesso/reparo por pallet';
  CO_TXT_iutpei090 = 'Gera��o de material resultante de reprocesso/reparo';
  CO_TXT_iutpei091 = 'Baixa de material em reprocesso/reparo na gera��o de material resultante de reprocesso/reparo';

  CO_TXT_iutpei092 = ''; //'Item de venda de produto com rastreio';
  CO_TXT_iutpei093 = ''; //'Item de venda de produto por pallet';
  CO_TXT_iutpei094 = ''; //'Item de venda de produto por peso';
  CO_TXT_iutpei095 = ''; //'Altera��o de item de venda de produto ';

  CO_TXT_iutpei096 = ''; //
  CO_TXT_iutpei097 = ''; //
  CO_TXT_iutpei098 = ''; //
  CO_TXT_iutpei099 = ''; //
  CO_TXT_iutpei100 = ''; //
  CO_TXT_iutpei101 = ''; //
  CO_TXT_iutpei102 = ''; //
  CO_TXT_iutpei103 = ''; //
  CO_TXT_iutpei104 = ''; //
  CO_TXT_iutpei105 = ''; //
  CO_TXT_iutpei106 = ''; //
  CO_TXT_iutpei107 = ''; //
  CO_TXT_iutpei108 = ''; //
  CO_TXT_iutpei109 = ''; //
  CO_TXT_iutpei110 = ''; //

  MaxIuvpei = 110;
  sListaIuvpeiTxt: array[0..MaxIuvpei] of String = (
    CO_TXT_iutpei000,
    CO_TXT_iutpei001,
    CO_TXT_iutpei002,
    CO_TXT_iutpei003,
    CO_TXT_iutpei004,
    CO_TXT_iutpei005,
    CO_TXT_iutpei006,
    CO_TXT_iutpei007,
    CO_TXT_iutpei008,
    CO_TXT_iutpei009,
    CO_TXT_iutpei010,
    CO_TXT_iutpei011,
    CO_TXT_iutpei012,
    CO_TXT_iutpei013,
    CO_TXT_iutpei014,
    CO_TXT_iutpei015,
    CO_TXT_iutpei016,
    CO_TXT_iutpei017,
    CO_TXT_iutpei018,
    CO_TXT_iutpei019,
    CO_TXT_iutpei020,
    CO_TXT_iutpei021,
    CO_TXT_iutpei022,
    CO_TXT_iutpei023,
    CO_TXT_iutpei024,
    CO_TXT_iutpei025,
    CO_TXT_iutpei026,
    CO_TXT_iutpei027,
    CO_TXT_iutpei028,
    CO_TXT_iutpei029,
    CO_TXT_iutpei030,
    CO_TXT_iutpei031,
    CO_TXT_iutpei032,
    CO_TXT_iutpei033,
    CO_TXT_iutpei034,
    CO_TXT_iutpei035,
    CO_TXT_iutpei036,
    CO_TXT_iutpei037,
    CO_TXT_iutpei038,
    CO_TXT_iutpei039,
    CO_TXT_iutpei040,
    CO_TXT_iutpei041,
    CO_TXT_iutpei042,
    CO_TXT_iutpei043,
    CO_TXT_iutpei044,
    CO_TXT_iutpei045,
    CO_TXT_iutpei046,
    CO_TXT_iutpei047,
    CO_TXT_iutpei048,
    CO_TXT_iutpei049,
    CO_TXT_iutpei050,
    CO_TXT_iutpei051,
    CO_TXT_iutpei052,
    CO_TXT_iutpei053,
    CO_TXT_iutpei054,
    CO_TXT_iutpei055,
    CO_TXT_iutpei056,
    CO_TXT_iutpei057,
    CO_TXT_iutpei058,
    CO_TXT_iutpei059,
    CO_TXT_iutpei060,
    CO_TXT_iutpei061,
    CO_TXT_iutpei062,
    CO_TXT_iutpei063,
    CO_TXT_iutpei064,
    CO_TXT_iutpei065,
    CO_TXT_iutpei066,
    CO_TXT_iutpei067,
    CO_TXT_iutpei068,
    CO_TXT_iutpei069,
    CO_TXT_iutpei070,
    CO_TXT_iutpei071,
    CO_TXT_iutpei072,
    CO_TXT_iutpei073,
    CO_TXT_iutpei074,
    CO_TXT_iutpei075,
    CO_TXT_iutpei076,
    CO_TXT_iutpei077,
    CO_TXT_iutpei078,
    CO_TXT_iutpei079,
    CO_TXT_iutpei080,
    CO_TXT_iutpei081,
    CO_TXT_iutpei082,
    CO_TXT_iutpei083,
    CO_TXT_iutpei084,
    CO_TXT_iutpei085,
    CO_TXT_iutpei086,
    CO_TXT_iutpei087,
    CO_TXT_iutpei088,
    CO_TXT_iutpei089,
    CO_TXT_iutpei090,
    CO_TXT_iutpei091,
    CO_TXT_iutpei092,
    CO_TXT_iutpei093,
    CO_TXT_iutpei094,
    CO_TXT_iutpei095,
    CO_TXT_iutpei096,
    CO_TXT_iutpei097,
    CO_TXT_iutpei098,
    CO_TXT_iutpei099,
    CO_TXT_iutpei100,
    CO_TXT_iutpei101,
    CO_TXT_iutpei102,
    CO_TXT_iutpei103,
    CO_TXT_iutpei104,
    CO_TXT_iutpei105,
    CO_TXT_iutpei106,
    CO_TXT_iutpei107,
    CO_TXT_iutpei108,
    CO_TXT_iutpei109,
    CO_TXT_iutpei110
  );
  //
  CO_PODE_ALTERAR_ESTOQUE_PRODUTO_ABA = 'Couros TX';
  CO_PODE_ALTERAR_ESTOQUE_PRODUTO_CHK = 'Permite alterar estoque ap�s invent�rio';

implementation

end.
