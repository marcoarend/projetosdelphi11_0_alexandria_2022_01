object FmDB_Converte_Tisolin: TFmDB_Converte_Tisolin
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Convers'#227'o de Banco de Dados'
  ClientHeight = 763
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 90
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 379
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 379
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 379
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 52
    Width = 1241
    Height = 544
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 544
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 544
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 1237
          Height = 527
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet1
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tabelas DBF'
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 1229
              Height = 499
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel6: TPanel
                Left = 0
                Top = 0
                Width = 1229
                Height = 499
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                TabOrder = 0
                object PageControl2: TPageControl
                  Left = 1
                  Top = 1
                  Width = 1227
                  Height = 497
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ActivePage = TabSheet7
                  Align = alClient
                  TabOrder = 0
                  object TabSheet2: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Registros'
                    object DBGrid1: TDBGrid
                      Left = 0
                      Top = 305
                      Width = 1219
                      Height = 120
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      DataSource = DsDBF
                      TabOrder = 0
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                    end
                    object Panel7: TPanel
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 276
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      TabOrder = 1
                      object GroupBox2: TGroupBox
                        Left = 1
                        Top = 1
                        Width = 98
                        Height = 274
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alLeft
                        Caption = ' C'#243'digos ini: '
                        TabOrder = 0
                        object Panel10: TPanel
                          Left = 2
                          Top = 15
                          Width = 94
                          Height = 257
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          BevelOuter = bvNone
                          ParentBackground = False
                          TabOrder = 0
                          object Label3: TLabel
                            Left = 2
                            Top = 5
                            Width = 35
                            Height = 13
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Cliente:'
                          end
                          object Label4: TLabel
                            Left = 2
                            Top = 54
                            Width = 57
                            Height = 13
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Fornecedor:'
                          end
                          object Label5: TLabel
                            Left = 2
                            Top = 103
                            Width = 45
                            Height = 13
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Entidade:'
                          end
                          object EdCliCodIni: TdmkEdit
                            Left = 1
                            Top = 25
                            Width = 74
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Alignment = taRightJustify
                            TabOrder = 0
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '10001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 10001
                            ValWarn = False
                          end
                          object EdForCodIni: TdmkEdit
                            Left = 1
                            Top = 74
                            Width = 74
                            Height = 26
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Alignment = taRightJustify
                            TabOrder = 1
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '20001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 20001
                            ValWarn = False
                          end
                          object EdEntCodIni: TdmkEdit
                            Left = 1
                            Top = 123
                            Width = 74
                            Height = 26
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Alignment = taRightJustify
                            TabOrder = 2
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '30001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 30001
                            ValWarn = False
                          end
                          object BtTudo: TBitBtn
                            Tag = 127
                            Left = 1
                            Top = 156
                            Width = 90
                            Height = 40
                            Caption = '&Todos'
                            NumGlyphs = 2
                            TabOrder = 3
                            OnClick = BtTudoClick
                          end
                          object BtNenhum: TBitBtn
                            Tag = 128
                            Left = 1
                            Top = 200
                            Width = 90
                            Height = 40
                            Caption = '&Nenhum'
                            NumGlyphs = 2
                            TabOrder = 4
                            OnClick = BtNenhumClick
                          end
                        end
                      end
                      object CGDBF: TdmkCheckGroup
                        Left = 99
                        Top = 1
                        Width = 1119
                        Height = 274
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        Caption = ' Tabelas: '
                        Columns = 2
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Courier New'
                        Font.Style = []
                        Items.Strings = (
                          
                            'StatusCliente    [SIM]             (Cadastro de Status de entida' +
                            'des)'
                          'Cliente          [SIM]             (Cadastro de entidades)'
                          'ClienteStatus    [SIM]             (Status das entidades)'
                          'AnexoDocumento   [SIM]             (documentos anexados)'
                          'Grupo            [SIM]             (Grupos de Grade)'
                          'Marca            [SIM]             (Marcas de equipamentos)'
                          'Linha            [SIM]             (Nivel 2 Grade)'
                          
                            'Produto          [SIM]             (Nivel 1 GradeProddutos loca'#231 +
                            #227'o e venda)'
                          'ProdutoAcessorio [SIM]             (Acess'#243'rios de equipamentos)'
                          ''
                          'Locacao          [SIM]             (Cabel'#231'alho das loca'#231#245'es)'
                          'LocacaoItens     [SIM]             (Itens de loca'#231#245'es)'
                          ''
                          'CADBCO        20 [SIM]             (Cadastro de Carteiras)'
                          'CADCOM       275 [NAO] 2002 / 2005 (???)'
                          'CADCLI      4852 [SIM]             (Cadastro de Clientes)'
                          'CADCSI      2779 [NAO] 2002 / 2007 (??/)'
                          'CADCTR     11381 [   ] 2002 / .... (Contratos?)'
                          'CADFOR      1313 [SIM]             (Cadastro Fornecedores)'
                          'CADGRU        85 [   ]             (Grup. equip. - 4 n'#237'v.)'
                          'CADIMP         5 [   ]             (% de Impostos)'
                          'CADMAT     30758 [   ]             (Materiais [????])'
                          'CADNAT        92 [   ]             (Plano de contas???)'
                          'CADNOP        15 [NAO]             (Natureza opera'#231#227'o)'
                          'CADORC      2764 [NAO] 2002 / 2007 (Orcamento)'
                          'CADPAT       398 [   ] 1999 / .... (PATRIMONIO)'
                          'CADPED      1367 [NAO] 2002 / 2007 (Pedidos???)'
                          'CADPGO       106 [   ]             (Cond. pagto)'
                          'CADRAM        26 [   ]             (Ramos de atividade)'
                          'CADREP         0                                      '
                          'CADREQ         0                                     '
                          'CADSIT        15 [   ]             (Situa'#231#227'o???)'
                          'CADVDE         7 [   ]             (Vendedores)'
                          'CTRAUX     15731 [   ] 2002 / .... (tab aux contrato [NFe?])'
                          'PAGDUP      8013 [   ] 2002 / .... (Contas a pagar???)'
                          'RECDUP     16388 [   ] 2002 / .... (Contas a receber ???)')
                        ParentFont = False
                        TabOrder = 1
                        OnClick = CGDBFClick
                        UpdType = utYes
                        Value = 0
                        OldValor = 0
                      end
                    end
                    object MePronto: TMemo
                      Left = 0
                      Top = 425
                      Width = 1219
                      Height = 44
                      Align = alBottom
                      TabOrder = 2
                    end
                    object Panel11: TPanel
                      Left = 0
                      Top = 276
                      Width = 1219
                      Height = 29
                      Align = alTop
                      Caption = 'Panel11'
                      TabOrder = 3
                      object Label7: TLabel
                        Left = 104
                        Top = 8
                        Width = 27
                        Height = 13
                        Caption = 'Pular:'
                        Enabled = False
                      end
                      object Label8: TLabel
                        Left = 244
                        Top = 8
                        Width = 47
                        Height = 13
                        Caption = 'Registros:'
                      end
                      object CkLimit: TCheckBox
                        Left = 4
                        Top = 8
                        Width = 97
                        Height = 17
                        Caption = 'Limitar dados:'
                        Checked = True
                        Enabled = False
                        State = cbChecked
                        TabOrder = 0
                      end
                      object EdSkip: TdmkEdit
                        Left = 140
                        Top = 4
                        Width = 92
                        Height = 21
                        Alignment = taRightJustify
                        Enabled = False
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                      end
                      object EdFirst: TdmkEdit
                        Left = 300
                        Top = 4
                        Width = 92
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 2
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '500'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 500
                        ValWarn = False
                      end
                    end
                  end
                  object TabSheet3: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'SQL'
                    ImageIndex = 1
                    object Splitter1: TSplitter
                      Left = 0
                      Top = 105
                      Width = 1219
                      Height = 6
                      Cursor = crVSplit
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      ExplicitWidth = 1214
                    end
                    object Panel9: TPanel
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 105
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      TabOrder = 0
                      object MeSQL1: TMemo
                        Left = 1
                        Top = 1
                        Width = 1052
                        Height = 103
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Courier New'
                        Font.Style = []
                        Lines.Strings = (
                          'SELECT * FROM CADCLI'
                          '')
                        ParentFont = False
                        TabOrder = 0
                        OnKeyDown = MeSQL1KeyDown
                      end
                      object Panel8: TPanel
                        Left = 1053
                        Top = 1
                        Width = 165
                        Height = 103
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alRight
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 1
                        object BtExecuta: TBitBtn
                          Tag = 14
                          Left = 6
                          Top = 5
                          Width = 120
                          Height = 40
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Caption = '&Executa'
                          NumGlyphs = 2
                          TabOrder = 0
                          OnClick = BtExecutaClick
                        end
                      end
                    end
                    object DBGrid2: TDBGrid
                      Left = 0
                      Top = 111
                      Width = 1219
                      Height = 358
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      DataSource = DsPsq
                      TabOrder = 1
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                    end
                  end
                  object TabSheet4: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Erros Endere'#231'os'
                    ImageIndex = 2
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object Memo1: TMemo
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 520
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      TabOrder = 0
                    end
                  end
                  object TabSheet5: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Erros Loca'#231#245'es'
                    ImageIndex = 3
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object Memo2: TMemo
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 520
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      TabOrder = 0
                    end
                  end
                  object TabSheet6: TTabSheet
                    Caption = ' Erros Gerais '
                    ImageIndex = 4
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object MeErrosGerais: TMemo
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 520
                      Align = alClient
                      TabOrder = 0
                      ExplicitLeft = 124
                      ExplicitTop = 96
                      ExplicitWidth = 185
                      ExplicitHeight = 89
                    end
                  end
                  object TabSheet7: TTabSheet
                    Caption = 'Teste'
                    ImageIndex = 5
                    object dmkDBGridZTO1: TdmkDBGridZTO
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 469
                      Align = alClient
                      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                      TabOrder = 0
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 596
    Width = 1241
    Height = 100
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1237
      Height = 83
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoG1: TLabel
        Left = 13
        Top = 34
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoG2: TLabel
        Left = 12
        Top = 33
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 68
        Width = 1237
        Height = 15
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
      end
      object PB2: TProgressBar
        Left = 0
        Top = 53
        Width = 1237
        Height = 15
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 1
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 696
    Width = 1241
    Height = 67
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 15
      Width = 177
      Height = 50
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 120
        Height = 44
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1060
      Height = 50
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 330
        Top = 5
        Width = 63
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Registros Tb:'
      end
      object Label2: TLabel
        Left = 442
        Top = 5
        Width = 47
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Registros:'
      end
      object Label6: TLabel
        Left = 192
        Top = 5
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tabela:'
      end
      object SpeedButton1: TSpeedButton
        Left = 290
        Top = 21
        Width = 21
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '>'
        OnClick = SpeedButton1Click
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 120
        Height = 44
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object EdRegistrosTb: TdmkEdit
        Left = 330
        Top = 21
        Width = 98
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdRegistrosQr: TdmkEdit
        Left = 443
        Top = 21
        Width = 99
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdTabela: TdmkEdit
        Left = 192
        Top = 21
        Width = 98
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'CADCLI.DBF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'CADCLI.DBF'
        ValWarn = False
      end
      object Button1: TButton
        Left = 689
        Top = 15
        Width = 93
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Situa'#231#227'o'
        TabOrder = 4
        OnClick = Button1Click
      end
      object BitBtn1: TBitBtn
        Left = 788
        Top = 16
        Width = 75
        Height = 25
        Caption = 'Nivel2'
        TabOrder = 5
      end
      object BitBtn2: TBitBtn
        Left = 888
        Top = 12
        Width = 145
        Height = 25
        Caption = 'Seq. its Loca'#231#245'es'
        TabOrder = 6
        OnClick = BitBtn2Click
      end
    end
  end
  object DsDBF: TDataSource
    Left = 496
    Top = 12
  end
  object DsCadCli: TDataSource
    Left = 556
    Top = 12
  end
  object DsPsq: TDataSource
    Left = 468
    Top = 40
  end
  object QrPsqEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE Antigo=:P0')
    Left = 496
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPsqEntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCadFor: TDataSource
    Left = 556
    Top = 40
  end
  object DsCadGru: TDataSource
    Left = 612
    Top = 12
  end
  object DataSource1: TDataSource
    Left = 612
    Top = 40
  end
  object QrExiste: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1 ID'
      'FROM gragru1'
      'WHERE Referencia=:P0')
    Left = 320
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraGruX, AGRPAT'
      'FROM gragxpatr ')
    Left = 364
    Top = 380
    object QrGraGXPatrAGRPAT: TWideStringField
      FieldName = 'AGRPAT'
      Required = True
      Size = 25
    end
    object QrGraGXPatrGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DataSource2: TDataSource
    Left = 668
    Top = 40
  end
  object DsCadVde: TDataSource
    Left = 668
    Top = 12
  end
  object QrGraFabMar: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM grafabmar'
      'WHERE Nome=:P0')
    Left = 408
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraFabMarControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSituacao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Situacao, '
      'CHAR(Situacao) SIGLA,'
      'COUNT(Situacao) ITENS'
      'FROM gragxpatr'
      'GROUP BY Situacao'
      'ORDER BY ITENS DESC')
    Left = 444
    Top = 380
    object QrSituacaoSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrSituacaoSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Required = True
      Size = 4
    end
    object QrSituacaoITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DataSource3: TDataSource
    Left = 724
    Top = 12
  end
  object DataSource4: TDataSource
    Left = 724
    Top = 40
  end
  object DataSource5: TDataSource
    Left = 780
    Top = 12
  end
  object DataSource6: TDataSource
    Left = 780
    Top = 40
  end
  object DBFB: TSQLConnection
    ConnectionName = 'FBConnection'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      'Database=C:\Teste\teste5.fdb'
      'RoleName=RoleName'
      'User_Name=sysdba'
      'Password=masterkey'
      'ServerCharSet='
      'SQLDialect=3'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'BlobSize=-1'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'IsolationLevel=ReadCommitted'
      'Trim Char=False')
    Connected = True
    Left = 36
    Top = 432
  end
  object SDTabelas: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 36
    Top = 480
  end
  object SDDados: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    AfterOpen = SDDadosAfterOpen
    Left = 36
    Top = 528
  end
  object SDCliente: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    FieldDefs = <
      item
        Name = 'CLIENTE'
        Attributes = [faRequired]
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'RAZAOSOCIAL'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'FANTASIA'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TIPOPESSOA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CPFCGC'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'RGINSCRICAOESTADUAL'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NATURALIDADE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECONUMERO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'BAIRRO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'COMPLEMENTO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CIDADE'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'CEP'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'FONE1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'FONE2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CELULAR'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'ANIVERSARIOABERTURA'
        DataType = ftDate
      end
      item
        Name = 'TIPOCADASTRO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'CIDADECOBRANCA'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'ENDERECOCOBRANCA'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'BAIRROENDERECOCOBRANCA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CEPENDERECOCOBRANCA'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'COMPLEMENTOCOBRANCA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'LOCALTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECOTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FONETRABALHO'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'TEMPOTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FUNCAOTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'RENDATRABALHO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'NOMECONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'LOCALTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECOTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FONETRABALHOCONJUGE'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'TEMPOTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FUNCAOTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'RENDACONJUGE'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'PAI'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MAE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIA1'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIAFONE_1'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'REFERENCIA2'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIAFONE_2'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'REFERENCIA3'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIAFONE_3'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'OBSERVACAO'
        DataType = ftString
        Size = 10000
      end
      item
        Name = 'INFORMACOES'
        DataType = ftString
        Size = 10000
      end
      item
        Name = 'EMAILPRIMARIO'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'CREDITO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'ISENTO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CONDICAO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'PARCELAS'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'INTERVALO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'DATACADASTRO'
        DataType = ftDate
      end
      item
        Name = 'DATAATUALIZACAO'
        DataType = ftDate
      end
      item
        Name = 'ULTIMACOMPRA'
        DataType = ftDate
      end
      item
        Name = 'USUARIOCADASTRO'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'USUARIOATUALIZACAO'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'ENDERECOENTREGA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'OBSERVACAOCLIENTE'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'LIBERADODOCUMENTOVENCIDO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OBSERVACAOVENDA'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DESCONTO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'VENDEDOR'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'OBSERVACAOTRAVAMENTO'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TRAVAMENTO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LIBERADOUMAFATURA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ATIVO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ORGAOPUBLICO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NCM'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PASTAXML'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DIACOBRANCA'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'OBSERVACAODIACOBRANCA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'EMAILFINANCEIRO'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'NAOCONTRIBUINTE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CONTRIBUINTEISENTO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CONTATO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FATURAR'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'COBRADOR'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'CODCTC'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'IMPRIMEPRECOLIQUIDO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NUMEROPEDIDOITEM'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'INSCRICAOMUNICIPAL'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SIMPLESNACIONAL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ALIQUOTAISS'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'RETENCAOISS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LISTASTATUS'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 384
    Top = 484
    object SDClienteCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDClienteRAZAOSOCIAL: TStringField
      FieldName = 'RAZAOSOCIAL'
      Size = 200
    end
    object SDClienteFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 100
    end
    object SDClienteTIPOPESSOA: TStringField
      FieldName = 'TIPOPESSOA'
      Size = 1
    end
    object SDClienteCPFCGC: TStringField
      FieldName = 'CPFCGC'
    end
    object SDClienteRGINSCRICAOESTADUAL: TStringField
      FieldName = 'RGINSCRICAOESTADUAL'
    end
    object SDClienteNATURALIDADE: TStringField
      FieldName = 'NATURALIDADE'
      Size = 50
    end
    object SDClienteENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 50
    end
    object SDClienteENDERECONUMERO: TFMTBCDField
      FieldName = 'ENDERECONUMERO'
      Precision = 18
      Size = 0
    end
    object SDClienteBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 50
    end
    object SDClienteCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 50
    end
    object SDClienteCIDADE: TFMTBCDField
      FieldName = 'CIDADE'
      Precision = 18
      Size = 0
    end
    object SDClienteCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object SDClienteFONE1: TStringField
      FieldName = 'FONE1'
      Size = 30
    end
    object SDClienteFONE2: TStringField
      FieldName = 'FONE2'
      Size = 30
    end
    object SDClienteCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 30
    end
    object SDClienteANIVERSARIOABERTURA: TDateField
      FieldName = 'ANIVERSARIOABERTURA'
    end
    object SDClienteTIPOCADASTRO: TFMTBCDField
      FieldName = 'TIPOCADASTRO'
      Precision = 18
      Size = 0
    end
    object SDClienteCIDADECOBRANCA: TFMTBCDField
      FieldName = 'CIDADECOBRANCA'
      Precision = 18
      Size = 0
    end
    object SDClienteENDERECOCOBRANCA: TStringField
      FieldName = 'ENDERECOCOBRANCA'
      Size = 80
    end
    object SDClienteBAIRROENDERECOCOBRANCA: TStringField
      FieldName = 'BAIRROENDERECOCOBRANCA'
      Size = 50
    end
    object SDClienteCEPENDERECOCOBRANCA: TStringField
      FieldName = 'CEPENDERECOCOBRANCA'
      Size = 9
    end
    object SDClienteCOMPLEMENTOCOBRANCA: TStringField
      FieldName = 'COMPLEMENTOCOBRANCA'
      Size = 50
    end
    object SDClienteLOCALTRABALHO: TStringField
      FieldName = 'LOCALTRABALHO'
      Size = 50
    end
    object SDClienteENDERECOTRABALHO: TStringField
      FieldName = 'ENDERECOTRABALHO'
      Size = 50
    end
    object SDClienteFONETRABALHO: TStringField
      FieldName = 'FONETRABALHO'
      Size = 15
    end
    object SDClienteTEMPOTRABALHO: TStringField
      FieldName = 'TEMPOTRABALHO'
      Size = 50
    end
    object SDClienteFUNCAOTRABALHO: TStringField
      FieldName = 'FUNCAOTRABALHO'
      Size = 50
    end
    object SDClienteRENDATRABALHO: TFMTBCDField
      FieldName = 'RENDATRABALHO'
      Precision = 18
      Size = 2
    end
    object SDClienteNOMECONJUGE: TStringField
      FieldName = 'NOMECONJUGE'
      Size = 50
    end
    object SDClienteLOCALTRABALHOCONJUGE: TStringField
      FieldName = 'LOCALTRABALHOCONJUGE'
      Size = 50
    end
    object SDClienteENDERECOTRABALHOCONJUGE: TStringField
      FieldName = 'ENDERECOTRABALHOCONJUGE'
      Size = 50
    end
    object SDClienteFONETRABALHOCONJUGE: TStringField
      FieldName = 'FONETRABALHOCONJUGE'
      Size = 15
    end
    object SDClienteTEMPOTRABALHOCONJUGE: TStringField
      FieldName = 'TEMPOTRABALHOCONJUGE'
      Size = 50
    end
    object SDClienteFUNCAOTRABALHOCONJUGE: TStringField
      FieldName = 'FUNCAOTRABALHOCONJUGE'
      Size = 50
    end
    object SDClienteRENDACONJUGE: TFMTBCDField
      FieldName = 'RENDACONJUGE'
      Precision = 18
      Size = 2
    end
    object SDClientePAI: TStringField
      FieldName = 'PAI'
      Size = 50
    end
    object SDClienteMAE: TStringField
      FieldName = 'MAE'
      Size = 50
    end
    object SDClienteREFERENCIA1: TStringField
      FieldName = 'REFERENCIA1'
      Size = 50
    end
    object SDClienteREFERENCIAFONE_1: TStringField
      FieldName = 'REFERENCIAFONE_1'
      Size = 15
    end
    object SDClienteREFERENCIA2: TStringField
      FieldName = 'REFERENCIA2'
      Size = 50
    end
    object SDClienteREFERENCIAFONE_2: TStringField
      FieldName = 'REFERENCIAFONE_2'
      Size = 15
    end
    object SDClienteREFERENCIA3: TStringField
      FieldName = 'REFERENCIA3'
      Size = 50
    end
    object SDClienteREFERENCIAFONE_3: TStringField
      FieldName = 'REFERENCIAFONE_3'
      Size = 15
    end
    object SDClienteOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 10000
    end
    object SDClienteINFORMACOES: TStringField
      FieldName = 'INFORMACOES'
      Size = 10000
    end
    object SDClienteEMAILPRIMARIO: TStringField
      FieldName = 'EMAILPRIMARIO'
      Size = 1024
    end
    object SDClienteCREDITO: TFMTBCDField
      FieldName = 'CREDITO'
      Precision = 18
      Size = 2
    end
    object SDClienteISENTO: TStringField
      FieldName = 'ISENTO'
      Size = 1
    end
    object SDClienteCONDICAO: TFMTBCDField
      FieldName = 'CONDICAO'
      Precision = 18
      Size = 0
    end
    object SDClientePARCELAS: TFMTBCDField
      FieldName = 'PARCELAS'
      Precision = 18
      Size = 0
    end
    object SDClienteINTERVALO: TFMTBCDField
      FieldName = 'INTERVALO'
      Precision = 18
      Size = 0
    end
    object SDClienteDATACADASTRO: TDateField
      FieldName = 'DATACADASTRO'
    end
    object SDClienteDATAATUALIZACAO: TDateField
      FieldName = 'DATAATUALIZACAO'
    end
    object SDClienteULTIMACOMPRA: TDateField
      FieldName = 'ULTIMACOMPRA'
    end
    object SDClienteUSUARIOCADASTRO: TStringField
      FieldName = 'USUARIOCADASTRO'
      Size = 10
    end
    object SDClienteUSUARIOATUALIZACAO: TStringField
      FieldName = 'USUARIOATUALIZACAO'
      Size = 10
    end
    object SDClienteENDERECOENTREGA: TStringField
      FieldName = 'ENDERECOENTREGA'
      Size = 50
    end
    object SDClienteOBSERVACAOCLIENTE: TStringField
      FieldName = 'OBSERVACAOCLIENTE'
      Size = 1024
    end
    object SDClienteLIBERADODOCUMENTOVENCIDO: TStringField
      FieldName = 'LIBERADODOCUMENTOVENCIDO'
      Size = 1
    end
    object SDClienteOBSERVACAOVENDA: TStringField
      FieldName = 'OBSERVACAOVENDA'
      Size = 100
    end
    object SDClienteDESCONTO: TFMTBCDField
      FieldName = 'DESCONTO'
      Precision = 18
      Size = 2
    end
    object SDClienteVENDEDOR: TFMTBCDField
      FieldName = 'VENDEDOR'
      Precision = 18
      Size = 0
    end
    object SDClienteOBSERVACAOTRAVAMENTO: TStringField
      FieldName = 'OBSERVACAOTRAVAMENTO'
      Size = 100
    end
    object SDClienteTRAVAMENTO: TStringField
      FieldName = 'TRAVAMENTO'
      Size = 1
    end
    object SDClienteLIBERADOUMAFATURA: TStringField
      FieldName = 'LIBERADOUMAFATURA'
      Size = 1
    end
    object SDClienteATIVO: TStringField
      FieldName = 'ATIVO'
      Size = 1
    end
    object SDClienteORGAOPUBLICO: TStringField
      FieldName = 'ORGAOPUBLICO'
      Size = 1
    end
    object SDClienteNCM: TStringField
      FieldName = 'NCM'
      Size = 1
    end
    object SDClientePASTAXML: TStringField
      FieldName = 'PASTAXML'
      Size = 100
    end
    object SDClienteDIACOBRANCA: TFMTBCDField
      FieldName = 'DIACOBRANCA'
      Precision = 18
      Size = 0
    end
    object SDClienteOBSERVACAODIACOBRANCA: TStringField
      FieldName = 'OBSERVACAODIACOBRANCA'
      Size = 50
    end
    object SDClienteEMAILFINANCEIRO: TStringField
      FieldName = 'EMAILFINANCEIRO'
      Size = 1024
    end
    object SDClienteNAOCONTRIBUINTE: TStringField
      FieldName = 'NAOCONTRIBUINTE'
      Size = 1
    end
    object SDClienteCONTRIBUINTEISENTO: TStringField
      FieldName = 'CONTRIBUINTEISENTO'
      Size = 1
    end
    object SDClienteCONTATO: TStringField
      FieldName = 'CONTATO'
      Size = 50
    end
    object SDClienteFATURAR: TStringField
      FieldName = 'FATURAR'
      Size = 1
    end
    object SDClienteCOBRADOR: TFMTBCDField
      FieldName = 'COBRADOR'
      Precision = 18
      Size = 0
    end
    object SDClienteCODCTC: TFMTBCDField
      FieldName = 'CODCTC'
      Precision = 18
      Size = 0
    end
    object SDClienteIMPRIMEPRECOLIQUIDO: TStringField
      FieldName = 'IMPRIMEPRECOLIQUIDO'
      Size = 1
    end
    object SDClienteNUMEROPEDIDOITEM: TStringField
      FieldName = 'NUMEROPEDIDOITEM'
      Size = 1
    end
    object SDClienteINSCRICAOMUNICIPAL: TStringField
      FieldName = 'INSCRICAOMUNICIPAL'
    end
    object SDClienteSIMPLESNACIONAL: TStringField
      FieldName = 'SIMPLESNACIONAL'
      Size = 1
    end
    object SDClienteALIQUOTAISS: TFMTBCDField
      FieldName = 'ALIQUOTAISS'
      Precision = 18
      Size = 2
    end
    object SDClienteRETENCAOISS: TStringField
      FieldName = 'RETENCAOISS'
      Size = 1
    end
    object SDClienteLISTASTATUS: TStringField
      FieldName = 'LISTASTATUS'
      Size = 50
    end
    object SDClienteCIDADE_DESCRICAO: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADE_DESCRICAO'
      LookupDataSet = SDCidade
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'DESCRICAO'
      KeyFields = 'CIDADE'
      Size = 255
      Lookup = True
    end
    object SDClienteCIDADE_IBGE: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADE_IBGE'
      LookupDataSet = SDCidade
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'IBGE'
      KeyFields = 'CIDADE'
      Size = 255
      Lookup = True
    end
    object SDClienteCIDADE_PAIS: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADE_PAIS'
      LookupDataSet = SDCidade
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'PAIS'
      KeyFields = 'CIDADE'
      Size = 255
      Lookup = True
    end
    object SDClienteCIDADE_UF: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADE_UF'
      LookupDataSet = SDCidade
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'UF'
      KeyFields = 'CIDADE'
      Size = 255
      Lookup = True
    end
    object SDClienteCIDADC_IBGE: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADC_IBGE'
      LookupDataSet = SDCidadC
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'IBGE'
      KeyFields = 'CIDADE'
      Size = 255
      Lookup = True
    end
    object SDClienteCIDADC_DESCRICAO: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADC_DESCRICAO'
      LookupDataSet = SDCidadC
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'DESCRICAO'
      KeyFields = 'CIDADECOBRANCA'
      Size = 255
      Lookup = True
    end
  end
  object SDCidade: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'CIDADE'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 251
    Top = 540
  end
  object SDCidadC: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'CIDADE'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 247
    Top = 588
  end
  object SDLocacao: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 383
    Top = 536
    object SDLocacaoLOCACAO: TFMTBCDField
      FieldName = 'LOCACAO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      Precision = 18
      Size = 0
    end
    object SDLocacaoEMISSAODATA: TDateField
      FieldName = 'EMISSAODATA'
    end
    object SDLocacaoEMISSAOHORA: TTimeField
      FieldName = 'EMISSAOHORA'
    end
    object SDLocacaoDEVOLUCAODATA: TDateField
      FieldName = 'DEVOLUCAODATA'
    end
    object SDLocacaoDEVOLUCAOHORA: TTimeField
      FieldName = 'DEVOLUCAOHORA'
    end
    object SDLocacaoSAIDADATA: TDateField
      FieldName = 'SAIDADATA'
    end
    object SDLocacaoSAIDAHORA: TTimeField
      FieldName = 'SAIDAHORA'
    end
    object SDLocacaoFINALIZADODATA: TDateField
      FieldName = 'FINALIZADODATA'
    end
    object SDLocacaoFINALIZADOHORA: TTimeField
      FieldName = 'FINALIZADOHORA'
    end
    object SDLocacaoVALOREQUIPAMENTO: TFMTBCDField
      FieldName = 'VALOREQUIPAMENTO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoVALORLOCACAO: TFMTBCDField
      FieldName = 'VALORLOCACAO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoVALORADICIONAL: TFMTBCDField
      FieldName = 'VALORADICIONAL'
      Precision = 18
      Size = 2
    end
    object SDLocacaoVALORADIANTAMENTO: TFMTBCDField
      FieldName = 'VALORADIANTAMENTO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoTIPOALUGUEL: TStringField
      FieldName = 'TIPOALUGUEL'
      Size = 1
    end
    object SDLocacaoOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 10000
    end
    object SDLocacaoFECHAMENTODATA: TDateField
      FieldName = 'FECHAMENTODATA'
    end
    object SDLocacaoFECHAMENTOHORA: TTimeField
      FieldName = 'FECHAMENTOHORA'
    end
    object SDLocacaoUSUARIO: TStringField
      FieldName = 'USUARIO'
      Size = 10
    end
    object SDLocacaoSTATUS: TFMTBCDField
      FieldName = 'STATUS'
      Precision = 18
      Size = 0
    end
    object SDLocacaoVALORDESCONTO: TFMTBCDField
      FieldName = 'VALORDESCONTO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoOBRA: TStringField
      FieldName = 'OBRA'
      Size = 10000
    end
    object SDLocacaoCANCELADA: TStringField
      FieldName = 'CANCELADA'
      Size = 1
    end
    object SDLocacaoULTIMACOBRANCAMENSAL: TDateField
      FieldName = 'ULTIMACOBRANCAMENSAL'
    end
  end
  object SDAux: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 604
    Top = 376
  end
  object MySQLBatchExecute1: TMySQLBatchExecute
    Database = Dmod.MyDB
    Delimiter = ';'
    Left = 504
    Top = 292
  end
  object SDLocacaoItens: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 387
    Top = 588
    object SDLocacaoItensLOCACAO: TFMTBCDField
      FieldName = 'LOCACAO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensSEQUENCIA: TFMTBCDField
      FieldName = 'SEQUENCIA'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensPRODUTO: TFMTBCDField
      FieldName = 'PRODUTO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensQUANTIDADEPRODUTO: TFMTBCDField
      FieldName = 'QUANTIDADEPRODUTO'
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensVALORPRODUTO: TFMTBCDField
      FieldName = 'VALORPRODUTO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoItensQUANTIDADELOCACAO: TFMTBCDField
      FieldName = 'QUANTIDADELOCACAO'
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensVALORLOCACAO: TFMTBCDField
      FieldName = 'VALORLOCACAO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoItensQUANTIDADEDEVOLUCAO: TFMTBCDField
      FieldName = 'QUANTIDADEDEVOLUCAO'
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensTROCA: TStringField
      FieldName = 'TROCA'
      Size = 1
    end
    object SDLocacaoItensCATEGORIA: TStringField
      FieldName = 'CATEGORIA'
      Size = 1
    end
    object SDLocacaoItensCOBRANCALOCACAO: TStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 50
    end
    object SDLocacaoItensCOBRANCACONSUMO: TFMTBCDField
      FieldName = 'COBRANCACONSUMO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoItensCOBRANCAREALIZADAVENDA: TStringField
      FieldName = 'COBRANCAREALIZADAVENDA'
      Size = 1
    end
    object SDLocacaoItensDATAINICIALCOBRANCA: TDateField
      FieldName = 'DATAINICIALCOBRANCA'
    end
    object SDLocacaoItensHORAINICIALCOBRANCA: TTimeField
      FieldName = 'HORAINICIALCOBRANCA'
    end
    object SDLocacaoItensDATASAIDA: TDateField
      FieldName = 'DATASAIDA'
    end
    object SDLocacaoItensHORASAIDA: TTimeField
      FieldName = 'HORASAIDA'
    end
  end
  object SDStatusCliente: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 463
    Top = 484
    object SDStatusClienteSTATUS: TFMTBCDField
      FieldName = 'STATUS'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDStatusClienteDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
  end
  object SDClienteStatus: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 467
    Top = 532
    object SDClienteStatusCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDClienteStatusSTATUS: TFMTBCDField
      FieldName = 'STATUS'
      Required = True
      Precision = 18
      Size = 0
    end
  end
  object SDProduto: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = 1
    DataSet.Params = <>
    Params = <>
    Left = 555
    Top = 484
    object SDProdutoPRODUTO: TFMTBCDField
      FieldName = 'PRODUTO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDProdutoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 200
    end
    object SDProdutoMAQ_BASE: TStringField
      FieldName = 'MAQ_BASE'
    end
    object SDProdutoMARCA: TStringField
      FieldName = 'MARCA'
      Size = 15
    end
    object SDProdutoIMPORTADO: TStringField
      FieldName = 'IMPORTADO'
      Size = 1
    end
    object SDProdutoGRUPO: TFMTBCDField
      FieldName = 'GRUPO'
      Precision = 18
      Size = 0
    end
    object SDProdutoMARGEM: TFMTBCDField
      FieldName = 'MARGEM'
      Precision = 18
      Size = 2
    end
    object SDProdutoUNIDADE: TStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object SDProdutoCUSTO: TFMTBCDField
      FieldName = 'CUSTO'
      Precision = 18
      Size = 4
    end
    object SDProdutoESTOQUE: TFMTBCDField
      FieldName = 'ESTOQUE'
      Precision = 18
      Size = 6
    end
    object SDProdutoPRECOVENDA: TFMTBCDField
      FieldName = 'PRECOVENDA'
      Precision = 18
      Size = 2
    end
    object SDProdutoATIVO: TStringField
      FieldName = 'ATIVO'
      Size = 1
    end
    object SDProdutoMARGEMSUBSTITUICAO: TFMTBCDField
      FieldName = 'MARGEMSUBSTITUICAO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPRODUTODECIMAL: TStringField
      FieldName = 'PRODUTODECIMAL'
      Size = 1
    end
    object SDProdutoDUPLICA: TStringField
      FieldName = 'DUPLICA'
      Size = 1
    end
    object SDProdutoPRECOPROMOCAO: TFMTBCDField
      FieldName = 'PRECOPROMOCAO'
      Precision = 18
      Size = 2
    end
    object SDProdutoDATAPROMOCAO: TDateField
      FieldName = 'DATAPROMOCAO'
    end
    object SDProdutoCLASSIFICACAOFISCAL: TFMTBCDField
      FieldName = 'CLASSIFICACAOFISCAL'
      Precision = 18
      Size = 0
    end
    object SDProdutoULTIMOFORNECEDOR: TFMTBCDField
      FieldName = 'ULTIMOFORNECEDOR'
      Precision = 18
      Size = 0
    end
    object SDProdutoULTIMANOTA: TStringField
      FieldName = 'ULTIMANOTA'
      Size = 10
    end
    object SDProdutoULTIMAQUANTIDADE: TFMTBCDField
      FieldName = 'ULTIMAQUANTIDADE'
      Precision = 18
      Size = 6
    end
    object SDProdutoULTIMADATA: TDateField
      FieldName = 'ULTIMADATA'
    end
    object SDProdutoPULTIMOFORNECEDOR: TFMTBCDField
      FieldName = 'PULTIMOFORNECEDOR'
      Precision = 18
      Size = 0
    end
    object SDProdutoPULTIMANOTA: TStringField
      FieldName = 'PULTIMANOTA'
      Size = 10
    end
    object SDProdutoPULTIMAQUANTIDADE: TFMTBCDField
      FieldName = 'PULTIMAQUANTIDADE'
      Precision = 18
      Size = 6
    end
    object SDProdutoPULTIMADATA: TDateField
      FieldName = 'PULTIMADATA'
    end
    object SDProdutoPULTIMOCUSTO: TFMTBCDField
      FieldName = 'PULTIMOCUSTO'
      Precision = 18
      Size = 4
    end
    object SDProdutoPULTIMOPRECOVENDA: TFMTBCDField
      FieldName = 'PULTIMOPRECOVENDA'
      Precision = 18
      Size = 2
    end
    object SDProdutoDATAMVTOESTOQUE: TSQLTimeStampField
      FieldName = 'DATAMVTOESTOQUE'
    end
    object SDProdutoMENSAGEM: TStringField
      FieldName = 'MENSAGEM'
      Size = 60
    end
    object SDProdutoNCM: TStringField
      FieldName = 'NCM'
      Size = 8
    end
    object SDProdutoULTIMAATUALIZACAO: TSQLTimeStampField
      FieldName = 'ULTIMAATUALIZACAO'
    end
    object SDProdutoULTIMOVALOR: TFMTBCDField
      FieldName = 'ULTIMOVALOR'
      Precision = 18
      Size = 6
    end
    object SDProdutoPULTIMOVALOR: TFMTBCDField
      FieldName = 'PULTIMOVALOR'
      Precision = 18
      Size = 6
    end
    object SDProdutoLINHA: TStringField
      FieldName = 'LINHA'
      Size = 3
    end
    object SDProdutoESTOQUEEXTERNO: TFMTBCDField
      FieldName = 'ESTOQUEEXTERNO'
      Precision = 18
      Size = 3
    end
    object SDProdutoDESCONTO: TFMTBCDField
      FieldName = 'DESCONTO'
      Precision = 18
      Size = 0
    end
    object SDProdutoBOCACAIXA: TFMTBCDField
      FieldName = 'BOCACAIXA'
      Precision = 18
      Size = 2
    end
    object SDProdutoSOMACUSTO: TFMTBCDField
      FieldName = 'SOMACUSTO'
      Precision = 18
      Size = 2
    end
    object SDProdutoTIPOITEM: TFMTBCDField
      FieldName = 'TIPOITEM'
      Precision = 18
      Size = 0
    end
    object SDProdutoGENERO: TFMTBCDField
      FieldName = 'GENERO'
      Precision = 18
      Size = 0
    end
    object SDProdutoMODELO: TFMTBCDField
      FieldName = 'MODELO'
      Precision = 18
      Size = 0
    end
    object SDProdutoSERIE: TStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object SDProdutoREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
    end
    object SDProdutoPESOMAQUINA: TFMTBCDField
      FieldName = 'PESOMAQUINA'
      Precision = 18
      Size = 3
    end
    object SDProdutoCODIGOBARRAS: TStringField
      FieldName = 'CODIGOBARRAS'
    end
    object SDProdutoCODIGOFABRICA: TStringField
      FieldName = 'CODIGOFABRICA'
      Size = 50
    end
    object SDProdutoUSUARIOCADASTRO: TStringField
      FieldName = 'USUARIOCADASTRO'
      Size = 10
    end
    object SDProdutoUSUARIOALTERACAO: TStringField
      FieldName = 'USUARIOALTERACAO'
      Size = 10
    end
    object SDProdutoDATAHORACADASTRO: TSQLTimeStampField
      FieldName = 'DATAHORACADASTRO'
    end
    object SDProdutoCLA_TRI: TStringField
      FieldName = 'CLA_TRI'
      Size = 3
    end
    object SDProdutoPERCIPI: TFMTBCDField
      FieldName = 'PERCIPI'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCFRETEPROPRIO: TFMTBCDField
      FieldName = 'PERCFRETEPROPRIO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCFRETETERCEIRO: TFMTBCDField
      FieldName = 'PERCFRETETERCEIRO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCDESPESAS: TFMTBCDField
      FieldName = 'PERCDESPESAS'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCFINANCEIRO: TFMTBCDField
      FieldName = 'PERCFINANCEIRO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCPISCOFINSCREDITO: TFMTBCDField
      FieldName = 'PERCPISCOFINSCREDITO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCICMORIGEM: TFMTBCDField
      FieldName = 'PERCICMORIGEM'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCICMDESTINO: TFMTBCDField
      FieldName = 'PERCICMDESTINO'
      Precision = 18
      Size = 2
    end
    object SDProdutoTABELAFORNECEDOR: TFMTBCDField
      FieldName = 'TABELAFORNECEDOR'
      Precision = 18
      Size = 4
    end
    object SDProdutoCUSTOREPOSICAO: TFMTBCDField
      FieldName = 'CUSTOREPOSICAO'
      Precision = 18
      Size = 4
    end
    object SDProdutoPULTIMOPERCFRETEPROPRIO: TFMTBCDField
      FieldName = 'PULTIMOPERCFRETEPROPRIO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCFRETETERCEIRO: TFMTBCDField
      FieldName = 'PULTIMOPERCFRETETERCEIRO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCDESPESAS: TFMTBCDField
      FieldName = 'PULTIMOPERCDESPESAS'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCFINANCEIRO: TFMTBCDField
      FieldName = 'PULTIMOPERCFINANCEIRO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCPISCOFINSCREDITO: TFMTBCDField
      FieldName = 'PULTIMOPERCPISCOFINSCREDITO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCICMORIGEM: TFMTBCDField
      FieldName = 'PULTIMOPERCICMORIGEM'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCICMDESTINO: TFMTBCDField
      FieldName = 'PULTIMOPERCICMDESTINO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOCUSTOREPOSICAO: TFMTBCDField
      FieldName = 'PULTIMOCUSTOREPOSICAO'
      Precision = 18
      Size = 4
    end
    object SDProdutoPULTIMOPERCIPI: TFMTBCDField
      FieldName = 'PULTIMOPERCIPI'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMAMARGEMSUBSTITUICAO: TFMTBCDField
      FieldName = 'PULTIMAMARGEMSUBSTITUICAO'
      Precision = 18
      Size = 4
    end
    object SDProdutoPULTIMATABELAFORNECEDOR: TFMTBCDField
      FieldName = 'PULTIMATABELAFORNECEDOR'
      Precision = 18
      Size = 4
    end
    object SDProdutoPULTIMAMARGEM: TFMTBCDField
      FieldName = 'PULTIMAMARGEM'
      Precision = 18
      Size = 2
    end
    object SDProdutoQUANTIDADEEMBALAGEMCOMPRA: TFMTBCDField
      FieldName = 'QUANTIDADEEMBALAGEMCOMPRA'
      Precision = 18
      Size = 0
    end
    object SDProdutoSERVICO: TStringField
      FieldName = 'SERVICO'
      Size = 1
    end
    object SDProdutoLOCALIZACAO: TStringField
      FieldName = 'LOCALIZACAO'
    end
    object SDProdutoMULTIPLOVENDA: TFMTBCDField
      FieldName = 'MULTIPLOVENDA'
      Precision = 18
      Size = 0
    end
    object SDProdutoDESCRICAOETIQUETA: TStringField
      FieldName = 'DESCRICAOETIQUETA'
      Size = 100
    end
    object SDProdutoFICHATECNICA: TStringField
      FieldName = 'FICHATECNICA'
      Size = 10000
    end
    object SDProdutoIMAGEM: TMemoField
      FieldName = 'IMAGEM'
      BlobType = ftMemo
      Size = 1
    end
    object SDProdutoTEMFICHATECNICA: TStringField
      FieldName = 'TEMFICHATECNICA'
      Size = 1
    end
    object SDProdutoTEMIMAGEM: TStringField
      FieldName = 'TEMIMAGEM'
      Size = 1
    end
    object SDProdutoULTIMAATUALIZACAOIMAGEM: TDateField
      FieldName = 'ULTIMAATUALIZACAOIMAGEM'
    end
    object SDProdutoPRODUTONOVO: TFMTBCDField
      FieldName = 'PRODUTONOVO'
      Precision = 18
      Size = 0
    end
    object SDProdutoPATRIMONIO: TStringField
      FieldName = 'PATRIMONIO'
      Size = 50
    end
    object SDProdutoCATEGORIA: TStringField
      FieldName = 'CATEGORIA'
      Size = 10
    end
    object SDProdutoVALORDIARIA: TFMTBCDField
      FieldName = 'VALORDIARIA'
      Precision = 18
      Size = 2
    end
    object SDProdutoVALORSEMANAL: TFMTBCDField
      FieldName = 'VALORSEMANAL'
      Precision = 18
      Size = 2
    end
    object SDProdutoVALORQUINZENAL: TFMTBCDField
      FieldName = 'VALORQUINZENAL'
      Precision = 18
      Size = 2
    end
    object SDProdutoVALORMENSAL: TFMTBCDField
      FieldName = 'VALORMENSAL'
      Precision = 18
      Size = 2
    end
    object SDProdutoPRECOFDS: TFMTBCDField
      FieldName = 'PRECOFDS'
      Precision = 18
      Size = 2
    end
    object SDProdutoDIASCARENCIA: TFMTBCDField
      FieldName = 'DIASCARENCIA'
      Precision = 18
      Size = 0
    end
    object SDProdutoREDUTORDIARIA: TFMTBCDField
      FieldName = 'REDUTORDIARIA'
      Precision = 18
      Size = 2
    end
    object SDProdutoCOBRANCACONSUMO: TFMTBCDField
      FieldName = 'COBRANCACONSUMO'
      Precision = 18
      Size = 2
    end
  end
  object SDGrupo: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 552
    Top = 536
    object SDGrupoGRUPO: TFMTBCDField
      FieldName = 'GRUPO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDGrupoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 40
    end
    object SDGrupoATUALIZA: TStringField
      FieldName = 'ATUALIZA'
      Size = 1
    end
    object SDGrupoULTIMAATUALIZACAO: TSQLTimeStampField
      FieldName = 'ULTIMAATUALIZACAO'
    end
    object SDGrupoMENSAGEM: TStringField
      FieldName = 'MENSAGEM'
      Size = 1
    end
  end
  object SDLinha: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 
      'SELECT FIRST 500  lin.Descricao, prd.Grupo, '#13#10' prd.Linha, COUNT(' +
      'prd.Produto) ITENS'#13#10' FROM produto prd'#13#10' LEFT JOIN linha lin ON (' +
      'lin.Linha=prd.Linha)'#13#10' GROUP BY prd.Linha, prd.Grupo, lin.Descri' +
      'cao'#13#10' ORDER BY prd.Linha, prd.Grupo'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 467
    Top = 584
    object SDLinhaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 50
    end
    object SDLinhaGRUPO: TFMTBCDField
      FieldName = 'GRUPO'
      Precision = 18
      Size = 0
    end
    object SDLinhaLINHA: TStringField
      FieldName = 'LINHA'
      Size = 3
    end
    object SDLinhaITENS: TIntegerField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object SDMarca: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 551
    Top = 588
    object SDMarcaMARCA: TStringField
      FieldName = 'MARCA'
      Required = True
      Size = 15
    end
    object SDMarcaULTIMAATUALIZACAO: TSQLTimeStampField
      FieldName = 'ULTIMAATUALIZACAO'
    end
  end
  object QrGraGX: TMySQLQuery
    Database = Dmod.MyDB
    Left = 531
    Top = 388
  end
  object SDGraGX: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 
      'SELECT PRODUTO, ACESSORIO  '#13#10'FROM produtoacessorio '#13#10'WHERE PRODU' +
      'TO=2877 '#13#10'OR ACESSORIO=2877'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 643
    Top = 532
    object SDGraGXPRODUTO: TFMTBCDField
      FieldName = 'PRODUTO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDGraGXACESSORIO: TFMTBCDField
      FieldName = 'ACESSORIO'
      Required = True
      Precision = 18
      Size = 0
    end
  end
  object SDProdutoAcessorio: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'PRODUTOACESSORIO'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 643
    Top = 480
    object SDProdutoAcessorioPRODUTO: TFMTBCDField
      FieldName = 'PRODUTO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDProdutoAcessorioACESSORIO: TFMTBCDField
      FieldName = 'ACESSORIO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDProdutoAcessorioQUANTIDADE: TFMTBCDField
      FieldName = 'QUANTIDADE'
      Precision = 18
      Size = 0
    end
  end
  object QrItem: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraGruX, "gragxpatr" TABELA, '
      'Aplicacao '
      'FROM gragxpatr '
      'WHERE GraGruX=10  '
      ' '
      'UNION '
      ' '
      'SELECT GraGruX, "gragxoutr" TABELA, '
      'Aplicacao '
      'FROM gragxoutr '
      'WHERE GraGruX=10 ')
    Left = 643
    Top = 584
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrItemTABELA: TWideStringField
      FieldName = 'TABELA'
      Required = True
      Size = 9
    end
    object QrItemAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Required = True
    end
  end
  object QrGraGruY: TMySQLQuery
    Database = FbDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _PRODUTO_ALL_; '
      'CREATE TABLE _PRODUTO_ALL_ '
      ' '
      'SELECT "S" GGY_KIND, Ativo, DESCRICAO,  '
      'PRODUTO, Linha, Categoria,  '
      'Referencia, Patrimonio '
      'FROM PRODUTO '
      'WHERE Categoria = "L" '
      'AND Patrimonio = "" '
      ' '
      'UNION '
      ' '
      'SELECT "P" GGY_KIND, Ativo, DESCRICAO,  '
      'PRODUTO, Linha, Categoria,  '
      'Referencia, Patrimonio '
      'FROM PRODUTO '
      'WHERE Categoria = "L" '
      'AND Patrimonio <> "" '
      ' '
      '; '
      ' '
      'DROP TABLE IF EXISTS _PRODUTO_ACE_; '
      'CREATE TABLE _PRODUTO_ACE_ '
      ' '
      'SELECT DISTINCT ACESSORIO '
      'FROM PRODUTOACESSORIO '
      '; '
      ' '
      'DROP TABLE IF EXISTS PRODUTOS_GGY; '
      'CREATE TABLE PRODUTOS_GGY '
      'SELECT IF(ace.ACESSORIO > 0, "A", pal.GGY_KIND) TipoProd,  '
      'pal.*  '
      'FROM _PRODUTO_ALL_ pal '
      'LEFT JOIN PRODUTOACESSORIO ace ON ace.ACESSORIO=pal.PRODUTO '
      ' '
      '; '
      'SELECT * FROM PRODUTOS_GGY'
      'ORDER BY GGY_KIND DESC, DESCRICAO '
      ';')
    Left = 823
    Top = 416
    object QrGraGruYTipoProd: TWideStringField
      FieldName = 'TipoProd'
      Required = True
      Size = 1
    end
    object QrGraGruYGGY_KIND: TWideStringField
      FieldName = 'GGY_KIND'
      Required = True
      Size = 1
    end
    object QrGraGruYAtivo: TWideStringField
      FieldName = 'Ativo'
      Size = 1
    end
    object QrGraGruYDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 200
    end
    object QrGraGruYPRODUTO: TLargeintField
      FieldName = 'PRODUTO'
      Required = True
    end
    object QrGraGruYLinha: TWideStringField
      FieldName = 'Linha'
      Size = 3
    end
    object QrGraGruYCategoria: TWideStringField
      FieldName = 'Categoria'
      Size = 10
    end
    object QrGraGruYReferencia: TWideStringField
      FieldName = 'Referencia'
    end
    object QrGraGruYPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 50
    end
  end
  object FbDB: TMySQLDatabase
    DatabaseName = 'zzz_fb_db_ant'
    DesignOptions = []
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'Host=127.0.0.1'
      'DatabaseName=zzz_fb_db_ant')
    DatasetOptions = []
    Left = 820
    Top = 480
  end
  object QrLocados: TMySQLQuery
    Database = FbDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _LOCACAOES_ABERTAS_; '
      'CREATE TABLE _LOCACAOES_ABERTAS_ '
      '      SELECT DISTINCT LOCACAO '
      '      FROM LOCACAO '
      '      WHERE FECHAMENTODATA < "1900-01-01" '
      '      AND STATUS<3; '
      'SELECT PRODUTO,  '
      '(QUANTIDADEPRODUTO-QUANTIDADEDEVOLUCAO) QtdLocado '
      'FROM LOCACAOITENS its '
      'WHERE CATEGORIA="L" '
      '/*AND PRODUTO  = 37*/ '
      'AND QUANTIDADEDEVOLUCAO<QUANTIDADEPRODUTO '
      'AND LOCACAO IN ( '
      '  SELECT LOCACAO FROM _LOCACAOES_ABERTAS_ '
      ') '
      'GROUP BY PRODUTO '
      'ORDER BY PRODUTO; ')
    Left = 823
    Top = 552
    object QrLocadosPRODUTO: TLargeintField
      FieldName = 'PRODUTO'
      Required = True
    end
    object QrLocadosQtdLocado: TLargeintField
      FieldName = 'QtdLocado'
    end
  end
  object QrSeqIL: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGruY, Its.*  '
      'FROM zzz_fb_db_ant.LOCACAOITENS its '
      'LEFT JOIN toolrent_tisolin.GraGruX ggx  '
      '  ON ggx.Controle=its.PRODUTO '
      '/*WHERE its.CATEGORIA = "L"*/ '
      'ORDER BY ggx.GraGruY, its.LOCACAO, its.SEQUENCIA ')
    Left = 935
    Top = 676
    object QrSeqILGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrSeqILLOCACAO: TLargeintField
      FieldName = 'LOCACAO'
      Required = True
    end
    object QrSeqILSEQUENCIA: TLargeintField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrSeqILPRODUTO: TLargeintField
      FieldName = 'PRODUTO'
      Required = True
    end
    object QrSeqILQUANTIDADEPRODUTO: TLargeintField
      FieldName = 'QUANTIDADEPRODUTO'
    end
    object QrSeqILVALORPRODUTO: TFloatField
      FieldName = 'VALORPRODUTO'
    end
    object QrSeqILQUANTIDADELOCACAO: TLargeintField
      FieldName = 'QUANTIDADELOCACAO'
    end
    object QrSeqILVALORLOCACAO: TFloatField
      FieldName = 'VALORLOCACAO'
    end
    object QrSeqILQUANTIDADEDEVOLUCAO: TLargeintField
      FieldName = 'QUANTIDADEDEVOLUCAO'
    end
    object QrSeqILTROCA: TWideStringField
      FieldName = 'TROCA'
      Size = 1
    end
    object QrSeqILCATEGORIA: TWideStringField
      FieldName = 'CATEGORIA'
      Size = 1
    end
    object QrSeqILCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 50
    end
    object QrSeqILCOBRANCACONSUMO: TFloatField
      FieldName = 'COBRANCACONSUMO'
    end
    object QrSeqILCOBRANCAREALIZADAVENDA: TWideStringField
      FieldName = 'COBRANCAREALIZADAVENDA'
      Size = 1
    end
    object QrSeqILDATAINICIALCOBRANCA: TDateField
      FieldName = 'DATAINICIALCOBRANCA'
    end
    object QrSeqILHORAINICIALCOBRANCA: TTimeField
      FieldName = 'HORAINICIALCOBRANCA'
    end
    object QrSeqILDATASAIDA: TDateField
      FieldName = 'DATASAIDA'
    end
    object QrSeqILHORASAIDA: TTimeField
      FieldName = 'HORASAIDA'
    end
  end
end
