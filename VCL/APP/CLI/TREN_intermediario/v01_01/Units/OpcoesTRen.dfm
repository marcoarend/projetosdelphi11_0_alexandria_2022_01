object FmOpcoesTRen: TFmOpcoesTRen
  Left = 339
  Top = 185
  Caption = 'FER-OPCAO-002 :: Op'#231#245'es Espec'#237'ficas do Aplicativo'
  ClientHeight = 601
  ClientWidth = 594
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 594
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 546
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 498
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 594
    Height = 439
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 594
      Height = 439
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 594
        Height = 439
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Geral'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 586
            Height = 61
            Align = alTop
            TabOrder = 0
            object Label7: TLabel
              Left = 12
              Top = 12
              Width = 43
              Height = 13
              Caption = 'Contrato:'
            end
            object EdDefContrat: TdmkEditCB
              Left = 12
              Top = 28
              Width = 36
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBDefContrat
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBDefContrat: TdmkDBLookupComboBox
              Left = 51
              Top = 28
              Width = 529
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCartaG
              TabOrder = 1
              dmkEditCB = EdDefContrat
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 61
            Width = 586
            Height = 120
            Align = alTop
            Caption = ' Filtro de patrim'#244'nio relativo '#224' loca'#231#227'o:  '
            TabOrder = 1
            object RGGraNivPatr: TdmkRadioGroup
              Left = 2
              Top = 15
              Width = 582
              Height = 57
              Align = alTop
              Caption = ' Nivel: '
              Columns = 4
              ItemIndex = 0
              Items.Strings = (
                'Indefinido'
                'Nivel 1'
                'Nivel 2'
                'Nivel 3'
                'Nivel 4'
                'Nivel 5'
                'Tipo'
                'Sem filtro')
              TabOrder = 0
              OnClick = RGGraNivPatrClick
              QryCampo = 'GraNivPatr'
              UpdCampo = 'GraNivPatr'
              UpdType = utYes
              OldValor = 0
            end
            object Panel5: TPanel
              Left = 2
              Top = 72
              Width = 582
              Height = 46
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label1: TLabel
                Left = 8
                Top = 4
                Width = 67
                Height = 13
                Caption = 'Item do N'#237'vel:'
              end
              object EdGraCodPatr: TdmkEditCB
                Left = 8
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'GraCodPatr'
                UpdCampo = 'GraCodPatr'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBGraCodPatr
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBGraCodPatr: TdmkDBLookupComboBox
                Left = 64
                Top = 20
                Width = 514
                Height = 21
                KeyField = 'CODNIV'
                ListField = 'Nome'
                ListSource = DsPatr
                TabOrder = 1
                dmkEditCB = EdGraCodPatr
                QryCampo = 'GraCodPatr'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 181
            Width = 586
            Height = 120
            Align = alTop
            Caption = ' Filtro de materiais relativo '#224' loca'#231#227'o:  '
            TabOrder = 2
            object RGGraNivOutr: TdmkRadioGroup
              Left = 2
              Top = 15
              Width = 582
              Height = 57
              Align = alTop
              Caption = ' Nivel: '
              Columns = 4
              ItemIndex = 0
              Items.Strings = (
                'Indefinido'
                'Nivel 1'
                'Nivel 2'
                'Nivel 3'
                'Nivel 4'
                'Nivel 5'
                'Tipo'
                'Sem filtro')
              TabOrder = 0
              OnClick = RGGraNivOutrClick
              QryCampo = 'GraNivPatr'
              UpdCampo = 'GraNivPatr'
              UpdType = utYes
              OldValor = 0
            end
            object Panel6: TPanel
              Left = 2
              Top = 72
              Width = 582
              Height = 46
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label2: TLabel
                Left = 8
                Top = 4
                Width = 67
                Height = 13
                Caption = 'Item do N'#237'vel:'
              end
              object EdGraCodOutr: TdmkEditCB
                Left = 8
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'GraCodPatr'
                UpdCampo = 'GraCodPatr'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBGraCodOutr
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBGraCodOutr: TdmkDBLookupComboBox
                Left = 64
                Top = 20
                Width = 514
                Height = 21
                KeyField = 'CODNIV'
                ListField = 'Nome'
                ListSource = DsOutr
                TabOrder = 1
                dmkEditCB = EdGraCodOutr
                QryCampo = 'GraCodOutr'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 0
            Top = 301
            Width = 586
            Height = 75
            Align = alTop
            Caption = 'Relat'#243'rios'
            TabOrder = 3
            object Label3: TLabel
              Left = 12
              Top = 20
              Width = 72
              Height = 13
              Caption = 'Slogan rodap'#233':'
            end
            object EdSloganFoot: TdmkEdit
              Left = 12
              Top = 39
              Width = 569
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'SloganFoot'
              UpdCampo = 'SloganFoot'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Produtos'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label4: TLabel
            Left = 8
            Top = 4
            Width = 258
            Height = 13
            Caption = 'Tipo de grupo de produto utilizado para os patrim'#244'nios:'
          end
          object Label5: TLabel
            Left = 8
            Top = 52
            Width = 246
            Height = 13
            Caption = 'Tipo de grupo de produto utilizado para os materiasi:'
          end
          object EdTipCodPatr: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'TipCodPatr'
            UpdCampo = 'TipCodPatr'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBTipCodPatr
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBTipCodPatr: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 514
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsTipCodPatr
            TabOrder = 1
            dmkEditCB = EdTipCodPatr
            QryCampo = 'TipCodPatr'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdTipCodOutr: TdmkEditCB
            Left = 8
            Top = 68
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'TipCodOutr'
            UpdCampo = 'TipCodOutr'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBTipCodOutr
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBTipCodOutr: TdmkDBLookupComboBox
            Left = 64
            Top = 68
            Width = 514
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsTipCodOutr
            TabOrder = 3
            dmkEditCB = EdTipCodOutr
            QryCampo = 'TipCodOutr'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object RGCasasProd: TdmkRadioGroup
            Left = 8
            Top = 100
            Width = 570
            Height = 45
            Caption = ' Casas decimais nos pre'#231'os dos produtos: '
            Columns = 8
            ItemIndex = 0
            Items.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7')
            TabOrder = 4
            UpdType = utYes
            OldValor = 0
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Loca'#231#227'o '
          ImageIndex = 2
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 586
            Height = 411
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label36: TLabel
              Left = 6
              Top = 97
              Width = 266
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Hora limite de devolu'#231#227'o ap'#243's fim de semana e feriados:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label6: TLabel
              Left = 6
              Top = 121
              Width = 198
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Arredondar hor'#225'ro de devolu'#231#227'o a cada : '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label8: TLabel
              Left = 258
              Top = 121
              Width = 42
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'minutos. '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label9: TLabel
              Left = 6
              Top = 149
              Width = 125
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Hor'#225'rio de funcionamento:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label10: TLabel
              Left = 202
              Top = 149
              Width = 11
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = #224's'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object RGFormaCobrLoca: TdmkRadioGroup
              Left = 4
              Top = 4
              Width = 573
              Height = 57
              Caption = ' Forma de cobran'#231'a da loca'#231#227'o: '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'N'#227'o definido'
                'Na devolu'#231#227'o'
                'Na loca'#231#227'o')
              TabOrder = 0
              UpdType = utYes
              OldValor = 0
            end
            object CkBloPrdSPer: TdmkCheckBox
              Left = 6
              Top = 68
              Width = 311
              Height = 17
              Caption = 'Bloquear loca'#231#227'o de produtos sem permiss'#227'o de loca'#231#227'o'
              TabOrder = 1
              QryCampo = 'BloPrdSPer'
              UpdCampo = 'BloPrdSPer'
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object EdHrLimPosDiaNaoUtil: TdmkEdit
              Left = 278
              Top = 92
              Width = 60
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfLong
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'HoraEmi'
              UpdCampo = 'HoraIni'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdLocArredMinut: TdmkEdit
              Left = 204
              Top = 116
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '1'
              ValMax = '60'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '30'
              QryCampo = 'LocArredMinut'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 30
              ValWarn = False
            end
            object EdLocArredHrIniIni: TdmkEdit
              Left = 134
              Top = 144
              Width = 60
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfLong
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'HoraEmi'
              UpdCampo = 'HoraIni'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdLocArredHrIniFim: TdmkEdit
              Left = 222
              Top = 144
              Width = 60
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfLong
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'HoraEmi'
              UpdCampo = 'HoraIni'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object CkPermLocSemEstq: TdmkCheckBox
              Left = 6
              Top = 168
              Width = 311
              Height = 17
              Caption = 'Permitir loca'#231#227'o de produtos sem estoque.'
              TabOrder = 6
              QryCampo = 'PermLocSemEstq'
              UpdCampo = 'BloPrdSPer'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 487
    Width = 594
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 590
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 531
    Width = 594
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 448
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 446
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrContratos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contratos'
      'ORDER BY Nome')
    Left = 292
    Top = 64
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCartaG: TDataSource
    DataSet = QrContratos
    Left = 320
    Top = 64
  end
  object QrPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Nivel1 CODNIV'
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 296
    Top = 132
    object QrPatrNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrPatrCODNIV: TIntegerField
      FieldName = 'CODNIV'
    end
  end
  object DsPatr: TDataSource
    DataSet = QrPatr
    Left = 324
    Top = 132
  end
  object QrOutr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Nivel1 CODNIV'
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 176
    Top = 308
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object IntegerField1: TIntegerField
      FieldName = 'CODNIV'
    end
  end
  object DsOutr: TDataSource
    DataSet = QrOutr
    Left = 204
    Top = 308
  end
  object QrTipCodPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Codigo'
      'FROM prdgruptip'
      'ORDER BY Nome')
    Left = 387
    Top = 64
    object QrTipCodPatrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTipCodPatrNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsTipCodPatr: TDataSource
    DataSet = QrTipCodPatr
    Left = 415
    Top = 64
  end
  object QrTipCodOutr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Codigo'
      'FROM prdgruptip'
      'ORDER BY Nome')
    Left = 388
    Top = 136
    object QrTipCodOutrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTipCodOutrNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsTipCodOutr: TDataSource
    DataSet = QrTipCodOutr
    Left = 416
    Top = 136
  end
end
