unit UnAppPF;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, SHDocVw,
  UnInternalConsts2, ComCtrls, dmkGeral, mySQLDBTables, UnDmkEnums;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function AcaoEspecificaDeApp(Servico: String; CliInt, EntCliInt: Integer;
               Query: TmySQLQuery = nil; Query2: TmySQLQuery = nil): Boolean;
    //
    procedure Html_ConfigarImagem(WebBrowser: TWebBrowser);
    procedure Html_ConfigarUrl(WebBrowser: TWebBrowser);
    procedure Html_ConfigarVideo(WebBrowser: TWebBrowser);
    //
    procedure CadastroMateriaPrima(GraGruX: Integer; NewNome: String);
    procedure CadastroProdutoAcabado(GraGruX: Integer; NewNome: String);
    procedure CadastroServicoNFe(GraGruX: Integer; NewNome: String);
    procedure CadastroSubProduto(GraGruX: Integer; NewNome: String);
    procedure CadastroUsoEConsumo(GraGruX: Integer; NewNome: String);
    procedure CadastroUsoPermanente(GraGruX: Integer; NewNome: String);
    procedure CadastroInsumo(GraGruX: Integer; NewNome: String);
    //
    function  ObtemNomeTabelaGraGruY(GraGruY: Integer): String;
    function  CorrigeGraGruYdeGraGruX(GraGruY, GraGruX: Integer): Boolean;
    procedure MostraFormXXXxxXxxGraGruY(GraGruY, GraGruX: Integer; Edita:
              Boolean; Qry: TmySQLQuery(*; MultiplosGGX: array of Integer*));
    function  GGXCadGetForcaCor(GraGruY: Integer): Boolean;
    function  GGXCadGetForcaTam(GraGruY: Integer): Boolean;
    procedure CorrigeReduzidosDuplicadosDeInsumo(CorrigeGraTamIts: Boolean);
    procedure VerificaCadastroVSArtigoIncompleta();
    procedure VerificaGraGruYsNaoConfig();
    procedure VerificaCadastroXxArtigoIncompleta();
    procedure ConfiguraGGXsDeGGY(GraGruY: Integer; iNiveis1: array of Integer);
    procedure CalculaEstoqueGraGXPatr(GraGruX: Integer);
  end;

var
  AppPF: TUnAppPF;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UnBloqGerl_Jan,
  AppListas,
  //UnTX_Jan,
  UMySQLModule;


{ TUnAppPF }

{ TUnAppPF }

function TUnAppPF.AcaoEspecificaDeApp(Servico: String; CliInt,
  EntCliInt: Integer; Query, Query2: TmySQLQuery): Boolean;
begin
  if (Servico = 'ArreFut') and (Query <> nil) and (Query2 <> nil) then
  begin
    UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, CliInt, EntCliInt, nil, Query, Query2);
    //
    Result := True;
  end else
  if (Servico = 'LctGer2') and (Query <> nil) and (Query2 = nil) then
  begin
    UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, CliInt, EntCliInt, nil, Query, nil);
    //
    Result := True;
  end else
    Result := True;
end;

procedure TUnAppPF.CadastroInsumo(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroMateriaPrima(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroProdutoAcabado(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroServicoNFe(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroSubProduto(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroUsoEConsumo(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroUsoPermanente(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CalculaEstoqueGraGXPatr(GraGruX: Integer);
begin
{
DROP TABLE IF EXISTS _LOCACAOES_ABERTAS_;
CREATE TABLE _LOCACAOES_ABERTAS_
      SELECT DISTINCT Codigo
      FROM locccon
      WHERE DtHrBxa < "1900-01-01"
      AND STATUS<3;

DROP TABLE IF EXISTS _LOCACOES_ITS_ABERTAS_;
CREATE TABLE _LOCACOES_ITS_ABERTAS_
SELECT "PRI" TABELA, SUM((QtdeLocacao-QtdeDevolucao)) QtdLocado
FROM loccpatpri pri
WHERE GraGruX=6
AND QtdeDevolucao<QtdeLocacao
AND Codigo IN (
  SELECT Codigo FROM _LOCACAOES_ABERTAS_
)

UNION

SELECT "SEC" TABELA, SUM((QtdeLocacao-QtdeDevolucao))QtdLocado
FROM loccpatsec sec
WHERE GraGruX=6
AND QtdeDevolucao<QtdeLocacao
AND Codigo IN (
  SELECT Codigo FROM _LOCACAOES_ABERTAS_
)
;
SELECT SUM(QtdLocado) Itens
FROM _LOCACOES_ITS_ABERTAS_
}
end;

procedure TUnAppPF.ConfiguraGGXsDeGGY(GraGruY: Integer;
  iNiveis1: array of Integer);
const
  sProcName = 'TUnAppPF.ConfiguraGGXsDeGGY()';
var
  GraGruX, I: Integer;
  Tabela, sNiveis1: String;
  Qry: TmySQLQuery;
  Continua: Boolean;
begin
  Geral.MB_Info('Ver com a Dermatek: ' + sProcName);
{
  Qry := TmySQLQuery.Create(Dmod);
  try
    sNiveis1 := MyObjects.CordaDeArrayInt(iNiveis1);
    //if Nivel1 <> 0 then
    if sNiveis1 <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM gragrux ',
      //'WHERE GraGru1=' + Geral.FF0(sNiveis1),
      'WHERE GraGru1 IN (' + sNiveis1 + ')',
      '']);
      Qry.First;
      GraGruX := Qry.FieldByName('Controle').AsInteger;
      Tabela := AppPF.ObtemNomeTabelaGraGruY(GraGruY);
      if Tabela <> '' then
      begin
        //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False, [
        Qry.First;
        while not Qry.Eof do
        begin
          UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, Tabela, False, [
          ], [
          'GraGruX'], ['Ativo'], [
          ], [
          Qry.FieldByName('Controle').AsInteger], [1], True);
          //
          Qry.Next;
        end;
        case GraGruY of
          CO_GraGruY_1024_....
          CO_GraGruY_2048_....
          ...
          CO_GraGruY_6144_...:
          begin
            Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
            'GraGruY'], [
            'Controle'], [
            GraGruY], [
            GraGruX], True);
          end;
          //CO_GraGruY_5120_TXWetEnd,
          begin
            Grade_PF.CorrigeGraGruYdeGraGruX();
            Continua := True;
          end;
          else
          Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
          ' n�o est� implementado em ' + sProcName);
        end;
        if Continua (*and (not JaTemN1)*) then
        begin
          MostraFormXXXxxXxxGraGruY(GraGruY, GraGruX, True, Qry(*, MultiplosGGX*));
        end;
      end;
    end else
      Geral.MB_Erro('N�o foi poss�vel localizar o(s) produto(s) ' + sNiveis1 +
      ' para complementar os dados do seu cadastro!');
  finally
    Qry.Free;
  end;
}
end;

function TUnAppPF.CorrigeGraGruYdeGraGruX(GraGruY, GraGruX: Integer): Boolean;
const
  sProcName = 'TUnAppPF.CorrigeGraGruYdeGraGruX()';
begin
  Result := False;
(*
  case GraGruY of
    CO_GraGruY_0512_TXSubPrd,
    CO_GraGruY_0683_TXPSPPro,
    CO_GraGruY_0853_TXPSPEnd,
    CO_GraGruY_1024_TXNatCad,
    CO_GraGruY_1195_TXNatPDA,
    CO_GraGruY_1365_TXProCal,
    CO_GraGruY_1536_TXCouCal,
    CO_GraGruY_1621_TXCouDTA,
    CO_GraGruY_1707_TXProCur,
    CO_GraGruY_1877_TXCouCur,
    CO_GraGruY_2048_TXRibCad,
    CO_GraGruY_3072_TXRibCla,
    CO_GraGruY_4096_TXRibOpe:
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
      'GraGruY'], [
      'Controle'], [
      GraGruY], [
      GraGruX], True);
    end;
    CO_GraGruY_5120_TXWetEnd,
    CO_GraGruY_6144_TXFinCla:
    begin
      Grade_PF.CorrigeGraGruYdeGraGruX();
      Result := True;
    end;
    else
*)
      Geral.MB_Erro('Grupo de estoque n�o implementado em ' + sProcName);
//  end;
end;

procedure TUnAppPF.CorrigeReduzidosDuplicadosDeInsumo(
  CorrigeGraTamIts: Boolean);
begin
  // Apenas compatibilidade por enquanto...
end;

function TUnAppPF.GGXCadGetForcaCor(GraGruY: Integer): Boolean;
begin
(*
  Result :=
    (GraGruY = CO_GraGruY_6144_TXFinCla)
    or
    (GraGruY = CO_GraGruY_5120_TXWetEnd);
*)
  Result := True;
end;

function TUnAppPF.GGXCadGetForcaTam(GraGruY: Integer): Boolean;
begin
  Result := GGXCadGetForcaCor(GraGruY);
end;

procedure TUnAppPF.Html_ConfigarImagem(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarUrl(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarVideo(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.MostraFormXXXxxXxxGraGruY(GraGruY, GraGruX: Integer;
  Edita: Boolean; Qry: TmySQLQuery);
const
  sProcName = 'TUnAppPF.MostraFormXXXxxXxxGraGruY()';
begin
{
  case GraGruY of
(*
    CO_GraGruY_0512_TXSubPrd: VS_PF.MostraFormVSSubPrd(GraGruX, True);
    CO_GraGruY_0683_TXPSPPro: VS_PF.MostraFormVSPSPPro(GraGruX, True);
    CO_GraGruY_0853_TXPSPEnd: VS_PF.MostraFormVSPSPEnd(GraGruX, True, Qry);
*)
    CO_GraGruY_1024_TXCadNat: TX_Jan.MostraFormTXCadNat(GraGruX, True, Qry);
(*
    CO_GraGruY_1195_TXNatPDA: VS_PF.MostraFormVSNatPDA(GraGruX, True);
    CO_GraGruY_1365_TXProCal: VS_PF.MostraFormVSProCal(GraGruX, True);
    CO_GraGruY_1536_TXCouCal: VS_PF.MostraFormVSCouCal(GraGruX, True);
    CO_GraGruY_1621_TXCouDTA: VS_PF.MostraFormVSCouDTA(GraGruX, True);
    CO_GraGruY_1707_TXProCur: VS_PF.MostraFormVSProCur(GraGruX, True);
    CO_GraGruY_1877_TXCouCur: VS_PF.MostraFormVSCouCur(GraGruX, True);
*)
    CO_GraGruY_2048_TXCadOpe: TX_Jan.MostraFormTXCadOpe(GraGruX, True, Qry);
(*
    CO_GraGruY_3072_TXRibCla: VS_PF.MostraFormVSRibCla(GraGruX, True, Qry);
    CO_GraGruY_4096_TXRibOpe: VS_PF.MostraFormVSRibOpe(GraGruX, True);
    CO_GraGruY_5120_TXWetEnd: VS_PF.MostraFormVSWetEnd(GraGruX, True, Qry(*, MultiplosGGX*));
*)
    CO_GraGruY_6144_TXCadFcc: TX_Jan.MostraFormTXCadFcc(GraGruX, True, Qry(*, MultiplosGGX*));
    else
}
    Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
    ' n�o est� implementado em ' + sProcName);
  //end;
end;

function TUnAppPF.ObtemNomeTabelaGraGruY(GraGruY: Integer): String;
const
  sProcName = 'TUnAppPF.ObtemNomeTabelaGraGruY()';
begin
  Geral.MB_Info('Ver com a Dermatek: ' + sProcName);
{
(*
  case GraGruY of
    -1: Result := '';
(*
    CO_GraGruY_0512_TXSubPrd: Result := LowerCase('TXSubPrd');
    CO_GraGruY_0683_TXPSPPro: Result := LowerCase('TXPSPPro');
    CO_GraGruY_0853_TXPSPEnd: Result := LowerCase('TXPSPEnd');
*)
    CO_GraGruY_1024_TXCadNat: Result := LowerCase('TXCadNat');
(*
    CO_GraGruY_1195_TXNatPDA: Result := LowerCase('TXNatpda');
    CO_GraGruY_1365_TXProCal: Result := LowerCase('TXProCal');
    CO_GraGruY_1536_TXCouCal: Result := LowerCase('TXCouCal');
    CO_GraGruY_1621_TXCouDTA: Result := LowerCase('TXCouDTA');
    CO_GraGruY_1707_TXProCur: Result := LowerCase('TXProCur');
    CO_GraGruY_1877_TXCouCur: Result := LowerCase('TXCouCur');
*)
    CO_GraGruY_2048_TXCadOpe: Result := LowerCase('TXCadOpe');
(*
    CO_GraGruY_3072_TXRibCla: Result := LowerCase('TXRibCla');
    CO_GraGruY_4096_TXRibOpe: Result := LowerCase('TXRibOpe');
    CO_GraGruY_5120_TXWetEnd: Result := LowerCase('TXWetEnd');
*)
    CO_GraGruY_6144_TXCadFcc: Result := LowerCase('TXCadFcc');
    else
    begin
      Result := '';
      Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
      ' n�o est� implementado em ' + sProcName);
    end;
  end;
}
end;

procedure TUnAppPF.VerificaCadastroVSArtigoIncompleta;
const
  sProcName = 'TUnAppPF.VerificaCadastroVSArtigoIncompleta()';
{
var
  Qry: TmySQLQuery;
}
begin
  Geral.MB_Info('Ver necessidade de implementar ' + sProcName);
{
  if Dmod = nil then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggx.Controle, gg1.Nome, ggc.GraGruX, ggc.CouNiv1, ',
      'co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2, ',
      'ggc.MediaMinKg, ggc.MediaMaxKg  ',
      'FROM gragrux ggx  ',
      'LEFT JOIN gragruxcou ggc ON ggc.GraGruX=ggx.Controle ',
      'LEFT JOIN couniv1 co1 ON co1.Codigo=ggc.CouNiv1 ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE ggx.GraGruY<>0 ',
      'AND ggc.GraGruX IS NULL ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Existem artigos com cadastro incompleto!');
      //
      if DBCheck.CriaFm(TFmGraGruYIncompleto, FmGraGruYIncompleto, afmoNegarComAviso) then
      begin
        FmGraGruYIncompleto.ReopenIncompleto(Qry);
        FmGraGruYIncompleto.ShowModal;
        FmGraGruYIncompleto.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
}
end;

procedure TUnAppPF.VerificaCadastroXxArtigoIncompleta;
const
  sProcName = 'TUnAppPF.VerificaCadastroXxArtigoIncompleta()';
{
var
  Qry: TmySQLQuery;
}
begin
  Geral.MB_Info('Ver necessidade de implementar ' + sProcName);
{
  if Dmod = nil then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggx.Controle, gg1.Nome, ggc.GraGruX, ggc.CouNiv1, ',
      'co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2, ',
      'ggc.MediaMinKg, ggc.MediaMaxKg  ',
      'FROM gragrux ggx  ',
      'LEFT JOIN gragruxcou ggc ON ggc.GraGruX=ggx.Controle ',
      'LEFT JOIN couniv1 co1 ON co1.Codigo=ggc.CouNiv1 ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE ggx.GraGruY<>0 ',
      'AND ggc.GraGruX IS NULL ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Existem artigos com cadastro incompleto!');
      //
      if DBCheck.CriaFm(TFmGraGruYIncompleto, FmGraGruYIncompleto, afmoNegarComAviso) then
      begin
        FmGraGruYIncompleto.ReopenIncompleto(Qry);
        FmGraGruYIncompleto.ShowModal;
        FmGraGruYIncompleto.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
}
end;

procedure TUnAppPF.VerificaGraGruYsNaoConfig;
const
  sProcName = 'TUnAppPF.VerificaGraGruYsNaoConfig()';
var
  Nome, TitNiv1, TitNiv2, TitNiv3, TitNiv4, TitNiv5: String;
  Codigo, CodUsu, MadeBy, Fracio, Gradeado, TipPrd, NivCad, FaixaIni, FaixaFim,
  Customizav, ImpedeCad, LstPrcFisc, Tipo_Item, Genero, GraTabApp: Integer;
  PerComissF, PerComissR: Double;
  //
  function GeraPrdGrupTip(): Integer;
  begin
    Result := 0;
    Codigo := UMyMod.BuscaEmLivreY_Def('prdgruptip', 'Codigo', stIns, 0);
    CodUsu := Codigo;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
    'CodUsu', 'Nome', 'MadeBy',
    'Fracio', 'Gradeado', 'TipPrd',
    'NivCad', 'FaixaIni', 'FaixaFim',
    'TitNiv1', 'TitNiv2', 'TitNiv3',
    'TitNiv4', 'TitNiv5', 'Customizav',
    'ImpedeCad', 'LstPrcFisc', 'PerComissF',
    'PerComissR', 'Tipo_Item', 'Genero',
    'GraTabApp'], [
    'Codigo'], [
    CodUsu, Nome, MadeBy,
    Fracio, Gradeado, TipPrd,
    NivCad, FaixaIni, FaixaFim,
    TitNiv1, TitNiv2, TitNiv3,
    TitNiv4, TitNiv5, Customizav,
    ImpedeCad, LstPrcFisc, PerComissF,
    PerComissR, Tipo_Item, Genero,
    GraTabApp], [
    Codigo], True) then
      Result := Codigo
  end;
var
  Qry: TmySQLQuery;
  GraGruY, PrdGrupTip: Integer;
  Inclui: Boolean;
begin
////////////////////////////////////////////////////////////////////////////////
   EXIT;  // Deprecado!!! Tentar eliminar campo PrdGrupTip da tabela GraGruY
////////////////////////////////////////////////////////////////////////////////
{
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM gragruy ',
    'WHERE PrdGrupTip=0 ',
    'AND Codigo <> 0 ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(Qry.RecordCount) +
      ' Grupos de Estoque sem Grupo de Produto definido!' + sLineBreak +
      'Deseja que o(s) Grupo(s) de Produto sejam criado(s) e atrelados aos respectivos Grupos de Estoque?')
      = ID_YES then
      begin
        TitNiv2        := '';
        TitNiv3        := '';
        TitNiv4        := '';
        TitNiv5        := '';
        NivCad         := 1;
        FaixaIni       := 0;
        FaixaFim       := 0;
        Customizav     := 0;
        ImpedeCad      := 1;
        LstPrcFisc     := 5;
        PerComissF     := 0;
        PerComissR     := 0;
        Tipo_Item      := 0;
        Genero         := 0;
        GraTabApp      := 0;
        //
        Qry.First;
        while not Qry.Eof do
        begin
          GraGruY := Qry.FieldByName('Codigo').AsInteger;
          Inclui  := True;
          case GraGruY of
            (*
            CO_GraGruY_0512_TXSubPrd:
            begin
              Nome           := CO_TXT_GraGruY_0512_TXSubPrd;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 5;  // subproduto
            end;
            CO_GraGruY_0683_TXPSPPro:
            begin
              Nome           := CO_TXT_GraGruY_0683_TXPSPPro;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_0853_TXPSPEnd:
            begin
              Nome           := CO_TXT_GraGruY_0853_TXPSPEnd;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;*)
            CO_GraGruY_1024_TXCadNat:
            begin
              Nome           := CO_TXT_GraGruY_1024_TXCadNat;
              MadeBy         := 2;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 2;
              Tipo_Item      := 1; // materia-prima
            end;
            (*
            CO_GraGruY_1195_TXNatPDA:
            begin
              Nome           := CO_TXT_GraGruY_1195_TXNatPDA;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1365_TXProCal:
            begin
              Nome           := CO_TXT_GraGruY_1365_TXProCal;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_1536_TXCouCal:
            begin
              Nome           := CO_TXT_GraGruY_1536_TXCouCal;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1621_TXCouDTA:
            begin
              Nome           := CO_TXT_GraGruY_1621_TXCouDTA;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1707_TXProCur:
            begin
              Nome           := CO_TXT_GraGruY_1707_TXProCur;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_1877_TXCouCur:
            begin
              Nome           := CO_TXT_GraGruY_1877_TXCouCur;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;*)
            CO_GraGruY_2048_TXCadOpe:
            begin
              Nome           := CO_TXT_GraGruY_2048_TXCadOpe;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            (*
            CO_GraGruY_3072_TXRibCla:
            begin
              Nome           := CO_TXT_GraGruY_3072_TXRibCla;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            CO_GraGruY_4096_TXRibOpe:
            begin
              Nome           := CO_TXT_GraGruY_4096_TXRibOpe;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_5120_TXWetEnd:
            begin
              Nome           := CO_TXT_GraGruY_5120_TXWetEnd;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 1;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_6144_TXFinCla:
            begin
              Nome           := CO_TXT_GraGruY_6144_TXFinCla;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 1;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;*)
            else
            begin
              Geral.MB_Erro(
              'GraGruY ' + Geral.FF0(GraGruY) + ' n�o definido em "' + sProcName + '"');
              Inclui := False;
            end;
          end;
          if Inclui then
          begin
            TitNiv1 := Nome;
            PrdGrupTip := GeraPrdGrupTip();
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragruy', False, [
            'PrdGrupTip'], [
            'Codigo'], [
            PrdGrupTip], [
            GraGruY], True);
          end;
          Qry.Next;
        end;
      end;
      Qry.First;
    end;
  finally
    Qry.Free;
  end;
}
end;

end.
