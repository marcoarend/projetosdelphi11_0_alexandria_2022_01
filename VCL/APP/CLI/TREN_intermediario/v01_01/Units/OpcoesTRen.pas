unit OpcoesTRen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkCheckBox, UnDmkEnums;

type
  TFmOpcoesTRen = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrContratos: TmySQLQuery;
    QrContratosCodigo: TIntegerField;
    QrContratosNome: TWideStringField;
    DsCartaG: TDataSource;
    QrPatr: TmySQLQuery;
    QrPatrNome: TWideStringField;
    QrPatrCODNIV: TIntegerField;
    DsPatr: TDataSource;
    QrOutr: TmySQLQuery;
    StringField1: TWideStringField;
    IntegerField1: TIntegerField;
    DsOutr: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    EdDefContrat: TdmkEditCB;
    CBDefContrat: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    RGGraNivPatr: TdmkRadioGroup;
    Panel5: TPanel;
    Label1: TLabel;
    EdGraCodPatr: TdmkEditCB;
    CBGraCodPatr: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    RGGraNivOutr: TdmkRadioGroup;
    Panel6: TPanel;
    Label2: TLabel;
    EdGraCodOutr: TdmkEditCB;
    CBGraCodOutr: TdmkDBLookupComboBox;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    EdSloganFoot: TdmkEdit;
    TabSheet2: TTabSheet;
    Label4: TLabel;
    EdTipCodPatr: TdmkEditCB;
    CBTipCodPatr: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTipCodOutr: TdmkEditCB;
    CBTipCodOutr: TdmkDBLookupComboBox;
    QrTipCodPatr: TmySQLQuery;
    DsTipCodPatr: TDataSource;
    QrTipCodOutr: TmySQLQuery;
    DsTipCodOutr: TDataSource;
    QrTipCodPatrNome: TWideStringField;
    QrTipCodPatrCodigo: TIntegerField;
    QrTipCodOutrCodigo: TIntegerField;
    QrTipCodOutrNome: TWideStringField;
    RGCasasProd: TdmkRadioGroup;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    RGFormaCobrLoca: TdmkRadioGroup;
    CkBloPrdSPer: TdmkCheckBox;
    Label36: TLabel;
    EdHrLimPosDiaNaoUtil: TdmkEdit;
    Label6: TLabel;
    EdLocArredMinut: TdmkEdit;
    Label8: TLabel;
    Label9: TLabel;
    EdLocArredHrIniIni: TdmkEdit;
    Label10: TLabel;
    EdLocArredHrIniFim: TdmkEdit;
    CkPermLocSemEstq: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGGraNivPatrClick(Sender: TObject);
    procedure RGGraNivOutrClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmOpcoesTRen: TFmOpcoesTRen;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral;

{$R *.DFM}

procedure TFmOpcoesTRen.BtOKClick(Sender: TObject);
const
  Codigo = 1;
var
  DefContrat, GraNivPatr, GraCodPatr, GraNivOutr, GraCodOutr, TipCodPatr,
  TipCodOutr, FormaCobrLoca, LocArredMinut, PermLocSemEstq: Integer;
  SloganFoot, HrLimPosDiaNaoUtil, LocArredHrIniIni, LocArredHrIniFim: String;
begin
  TipCodPatr := EdTipCodPatr.ValueVariant;
  TipCodOutr := EdTipCodOutr.ValueVariant;
  DefContrat := EdDefContrat.ValueVariant;
  GraNivPatr := RGGraNivPatr.ItemIndex;
  GraCodPatr := EdGraCodPatr.ValueVariant;
  GraNivOutr := RGGraNivOutr.ItemIndex;
  GraCodOutr := EdGraCodOutr.ValueVariant;
  FormaCobrLoca      := RGFormaCobrLoca.ItemIndex;
  HrLimPosDiaNaoUtil := EdHrLimPosDiaNaoUtil.Text;
  LocArredMinut      := EdLocArredMinut.ValueVariant;
  LocArredHrIniIni   := EdLocArredHrIniIni.Text;
  LocArredHrIniFim   := EdLocArredHrIniFim.Text;
  PermLocSemEstq     := Geral.BoolToInt(CkPermLocSemEstq.Checked);
  //
  if MyObjects.FIC(FormaCobrLoca < 1, RGFormaCobrLoca,
  'Informe a forma de cobran�a da loca��o!') then
  begin
    PageControl1.ActivePageIndex := 2;
    RGFormaCobrLoca.SetFocus;
    Exit;
  end;
  //
 if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoestren', False, [
  'DefContrat', 'GraNivPatr', 'GraCodPatr',
  'GraNivOutr', 'GraCodOutr', 'BloPrdSPer',
  'TipCodPatr', 'TipCodOutr', 'FormaCobrLoca',
  'HrLimPosDiaNaoUtil', 'LocArredMinut',
  'LocArredHrIniIni', 'LocArredHrIniFim',
  'PermLocSemEstq'], [
  'Codigo'], [
  DefContrat, GraNivPatr, GraCodPatr,
  GraNivOutr, GraCodOutr, CkBloPrdSPer.Checked,
  TipCodPatr, TipCodOutr, FormaCobrLoca,
  HrLimPosDiaNaoUtil, LocArredMinut,
  LocArredHrIniIni, LocArredHrIniFim,
  PermLocSemEstq], [
  Codigo], True) then
  begin
    SloganFoot := EdSloganFoot.Text;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoesgerl', False, [
    'SloganFoot'], ['Codigo'], [SloganFoot], [Codigo], False) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'controle', False, [
        'CasasProd'], [], [RGCasasProd.ItemIndex], [], False) then
      begin
        DModG.ReopenOpcoesGerl();
        Dmod.ReopenOpcoesTRen();
        Close;
      end;
    end;
  end;
end;

procedure TFmOpcoesTRen.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesTRen.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesTRen.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  //
  DModG.ReopenOpcoesGerl();
  Dmod.ReopenOpcoesTRen();
  Dmod.ReopenControle;
  //
  EdDefContrat.ValueVariant := Dmod.QrOpcoesTRenDefContrat.Value;
  CBDefContrat.KeyValue     := Dmod.QrOpcoesTRenDefContrat.Value;
  RGGraNivPatr.ItemIndex    := Dmod.QrOpcoesTRenGraNivPatr.Value;
  EdGraCodPatr.ValueVariant := Dmod.QrOpcoesTRenGraCodPatr.Value;
  RGGraNivOutr.ItemIndex    := Dmod.QrOpcoesTRenGraNivOutr.Value;
  EdGraCodOutr.ValueVariant := Dmod.QrOpcoesTRenGraCodOutr.Value;
  CkBloPrdSPer.Checked      := Geral.IntToBool(Dmod.QrOpcoesTRenBloPrdSPer.Value);
  //
  EdTipCodPatr.ValueVariant := Dmod.QrOpcoesTRenTipCodPatr.Value;
  CBTipCodPatr.KeyValue     := Dmod.QrOpcoesTRenTipCodPatr.Value;
  EdTipCodOutr.ValueVariant := Dmod.QrOpcoesTRenTipCodOutr.Value;
  CBTipCodOutr.KeyValue     := Dmod.QrOpcoesTRenTipCodOutr.Value;
  //
  RGCasasProd.ItemIndex     := Dmod.QrControleCasasProd.Value;
  //
  RGFormaCobrLoca.ItemIndex := Dmod.QrOpcoesTRenFormaCobrLoca.Value;
  //
  EdHrLimPosDiaNaoUtil.ValueVariant := Dmod.QrOpcoesTRenHrLimPosDiaNaoUtil.Value;
  EdLocArredMinut.ValueVariant      := Dmod.QrOpcoesTRenLocArredMinut.Value;
  EdLocArredHrIniIni.ValueVariant   := Dmod.QrOpcoesTRenLocArredHrIniIni.Value;
  EdLocArredHrIniFim.ValueVariant   := Dmod.QrOpcoesTRenLocArredHrIniFim.Value;
  CkPermLocSemEstq.Checked          := Geral.IntToBool(Dmod.QrOpcoesTRenPermLocSemEstq.Value);
  //
  {
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartaG, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM cartag ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  }
  UnDmkDAC_PF.AbreQuery(QrTipCodPatr, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTipCodOutr, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM contratos ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  //
  EdSloganFoot.ValueVariant := DModG.QrOpcoesGerl.FieldByName('SloganFoot').AsString;
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmOpcoesTRen.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesTRen.RGGraNivOutrClick(Sender: TObject);
var
  FldCod, Tabela: String;
begin
  QrOutr.Close;
  EdGraCodOutr.ValueVariant := 0;
  CBGraCodOutr.KeyValue := 0;
  FldCod := '';
  Tabela := '';
  case RGGraNivOutr.ItemIndex of
    0: ; // nada
    1: begin FldCod := 'Nivel1'; Tabela := 'gragru1';    end;
    2: begin FldCod := 'Nivel2'; Tabela := 'gragru2';    end;
    3: begin FldCod := 'Nivel3'; Tabela := 'gragru3';    end;
    4: begin FldCod := 'Nivel4'; Tabela := 'gragru4';    end;
    5: begin FldCod := 'Nivel5'; Tabela := 'gragru5';    end;
    6: begin FldCod := 'Codigo'; Tabela := 'prdgruptip'; end;
    7: ; // nada
    else
    begin
      Geral.MensagemBox('N�vel n�o implementado!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  if Tabela <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrOutr, Dmod.MyDB, [
    'SELECT Nome, ' + FldCod + ' CODNIV ',
    'FROM ' + Tabela,
    'ORDER BY Nome ',
    '']);
  end;
end;

procedure TFmOpcoesTRen.RGGraNivPatrClick(Sender: TObject);
var
  FldCod, Tabela: String;
begin
  QrPatr.Close;
  EdGraCodPatr.ValueVariant := 0;
  CBGraCodPatr.KeyValue := 0;
  FldCod := '';
  Tabela := '';
  case RGGraNivPatr.ItemIndex of
    0: ; // nada
    1: begin FldCod := 'Nivel1'; Tabela := 'gragru1';    end;
    2: begin FldCod := 'Nivel2'; Tabela := 'gragru2';    end;
    3: begin FldCod := 'Nivel3'; Tabela := 'gragru3';    end;
    4: begin FldCod := 'Nivel4'; Tabela := 'gragru4';    end;
    5: begin FldCod := 'Nivel5'; Tabela := 'gragru5';    end;
    6: begin FldCod := 'Codigo'; Tabela := 'prdgruptip'; end;
    7: ; // nada
    else
    begin
      Geral.MensagemBox('N�vel n�o implementado!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  if Tabela <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPatr, Dmod.MyDB, [
    'SELECT Nome, ' + FldCod + ' CODNIV ',
    'FROM ' + Tabela,
    'ORDER BY Nome ',
    '']);
  end;
end;

end.
