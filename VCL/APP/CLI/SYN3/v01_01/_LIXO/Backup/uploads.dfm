object FmUploads: TFmUploads
  Left = 321
  Top = 159
  Caption = 'WEB-UPLOA-001 :: Uploads de arquivos'
  ClientHeight = 594
  ClientWidth = 814
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 92
    Width = 814
    Height = 483
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 168
    ExplicitTop = 72
    ExplicitHeight = 499
    object DBGrid1: TDBGrid
      Left = 9
      Top = 57
      Width = 723
      Height = 270
      BorderStyle = bsNone
      DataSource = DsUploads
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 190
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Arquivo'
          Width = 133
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NCONDOM'
          Title.Caption = 'Condom'#237'nio'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DIRNOME'
          Title.Caption = 'Tipo'
          Width = 77
          Visible = True
        end>
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 814
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 6
        Top = 1
        Width = 60
        Height = 13
        Caption = 'Condom'#237'nio:'
      end
      object EdCondPesq: TdmkEditCB
        Left = 6
        Top = 16
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBCondPesq
        IgnoraDBLookupComboBox = False
      end
      object CBCondPesq: TdmkDBLookupComboBox
        Left = 66
        Top = 16
        Width = 639
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NCONDOM'
        ListSource = DsNomeCond
        TabOrder = 1
        dmkEditCB = EdCondPesq
        UpdType = utYes
      end
      object BitBtn1: TBitBtn
        Tag = 18
        Left = 707
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Reabre'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BitBtn1Click
        NumGlyphs = 2
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 413
      Width = 814
      Height = 70
      Align = alBottom
      TabOrder = 2
      ExplicitTop = 429
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 810
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 666
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 11
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
          NumGlyphs = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 129
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
          NumGlyphs = 2
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 253
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtExcluiClick
          NumGlyphs = 2
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 92
    Width = 814
    Height = 483
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 244
    ExplicitTop = 128
    object Label9: TLabel
      Left = 16
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label10: TLabel
      Left = 16
      Top = 52
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object LaCond: TLabel
      Left = 16
      Top = 94
      Width = 60
      Height = 13
      Caption = 'Condom'#237'nio:'
    end
    object Label1: TLabel
      Left = 16
      Top = 138
      Width = 70
      Height = 13
      Caption = 'Diret'#243'rio WEB:'
    end
    object Label3: TLabel
      Left = 15
      Top = 182
      Width = 261
      Height = 13
      Caption = 'Caminho do arquivo a ser enviado para o servidor [F4]: '
    end
    object EdCodigo: TdmkEdit
      Left = 16
      Top = 24
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdCond: TdmkEditCB
      Left = 16
      Top = 110
      Width = 57
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCond
      IgnoraDBLookupComboBox = False
    end
    object CBCond: TdmkDBLookupComboBox
      Left = 76
      Top = 110
      Width = 325
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NCONDOM'
      ListSource = DsNomeCond
      TabOrder = 3
      dmkEditCB = EdCond
      UpdType = utYes
    end
    object EdDirWeb: TdmkEditCB
      Left = 16
      Top = 154
      Width = 57
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBDirWeb
      IgnoraDBLookupComboBox = False
    end
    object CBDirWeb: TdmkDBLookupComboBox
      Left = 76
      Top = 154
      Width = 125
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsNomeDir
      TabOrder = 5
      dmkEditCB = EdDirWeb
      UpdType = utYes
    end
    object EdNome: TdmkEdit
      Left = 15
      Top = 69
      Width = 385
      Height = 21
      MaxLength = 30
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdArquivo: TdmkEdit
      Left = 15
      Top = 198
      Width = 385
      Height = 21
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnKeyDown = EdArquivoKeyDown
    end
    object pbProgress: TProgressBar
      Left = 208
      Top = 157
      Width = 150
      Height = 15
      Position = 50
      TabOrder = 7
      Visible = False
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 413
      Width = 814
      Height = 70
      Align = alBottom
      TabOrder = 8
      ExplicitTop = 429
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 810
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel3: TPanel
          Left = 666
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 15
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
            NumGlyphs = 2
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
          NumGlyphs = 2
        end
      end
    end
  end
  object sbStatus: TStatusBar
    Left = 0
    Top = 575
    Width = 814
    Height = 19
    Panels = <
      item
        Text = 'Offline'
        Width = 100
      end
      item
        Width = 150
      end
      item
        Width = 50
      end>
    ExplicitTop = 547
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 814
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 766
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 718
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 247
        Height = 32
        Caption = 'Uploads de arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 247
        Height = 32
        Caption = 'Uploads de arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 247
        Height = 32
        Caption = 'Uploads de arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 814
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 810
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrNomeCond: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      
        'SELECT con.Codigo, IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NC' +
        'ONDOM'
      'FROM cond con'
      'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente'
      'WHERE con.Codigo > 0'
      'AND rec.Cliente1 = '#39'V'#39
      'ORDER BY NCONDOM')
    Left = 545
    Top = 13
    object QrNomeCondCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrNomeCondNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Size = 100
    end
  end
  object DsNomeCond: TDataSource
    DataSet = QrNomeCond
    Left = 574
    Top = 13
  end
  object QrUploads: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT upl.*, dir.Pasta NDIRWEB, dir.Nome DIRNOME,'
      'IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM'
      'FROM uploads upl'
      'LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb'
      'LEFT JOIN cond con ON con.Codigo=upl.Cond'
      'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente'
      'ORDER BY NCONDOM, upl.DirWeb, upl.Nome')
    Left = 488
    Top = 13
    object QrUploadsCodigo: TAutoIncField
      FieldName = 'Codigo'
      Origin = 'uploads.Codigo'
    end
    object QrUploadsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'uploads.Nome'
      Size = 32
    end
    object QrUploadsArquivo: TWideStringField
      FieldName = 'Arquivo'
      Origin = 'uploads.Arquivo'
      Size = 32
    end
    object QrUploadsCond: TIntegerField
      FieldName = 'Cond'
      Origin = 'uploads.Cond'
    end
    object QrUploadsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'uploads.Lk'
    end
    object QrUploadsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'uploads.DataCad'
    end
    object QrUploadsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'uploads.DataAlt'
    end
    object QrUploadsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'uploads.UserCad'
    end
    object QrUploadsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'uploads.UserAlt'
    end
    object QrUploadsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'uploads.AlterWeb'
    end
    object QrUploadsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'uploads.Ativo'
      Required = True
    end
    object QrUploadsDirWeb: TIntegerField
      FieldName = 'DirWeb'
      Origin = 'uploads.DirWeb'
      Required = True
    end
    object QrUploadsNDIRWEB: TWideStringField
      FieldName = 'NDIRWEB'
      Origin = 'dirweb.Pasta'
      Size = 32
    end
    object QrUploadsDIRNOME: TWideStringField
      FieldName = 'DIRNOME'
      Origin = 'dirweb.Nome'
      Size = 32
    end
    object QrUploadsNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Origin = 'NCONDOM'
      Size = 100
    end
  end
  object DsUploads: TDataSource
    DataSet = QrUploads
    Left = 516
    Top = 13
  end
  object QrNomeDir: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome, Pasta'
      'FROM dirweb'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 601
    Top = 13
    object QrNomeDirCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrNomeDirNome: TWideStringField
      FieldName = 'Nome'
      Size = 32
    end
    object QrNomeDirPasta: TWideStringField
      FieldName = 'Pasta'
      Size = 32
    end
  end
  object DsNomeDir: TDataSource
    DataSet = QrNomeDir
    Left = 630
    Top = 13
  end
  object OpenDialog1: TOpenDialog
    Filter = 'FDFs (*.pdf)|*.pdf'
    Left = 156
    Top = 29
  end
  object PMExclui: TPopupMenu
    Left = 288
    Top = 320
    object Excluiosdadosetambmoarquivo1: TMenuItem
      Caption = '&Exclui os dados e tamb'#233'm o arquivo'
      OnClick = Excluiosdadosetambmoarquivo1Click
    end
    object Exluisomenteosdados1: TMenuItem
      Caption = 'E&xlui somente os dados'
      OnClick = Exluisomenteosdados1Click
    end
  end
  object QrCondGri: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT upl.*, dir.Pasta NDIRWEB, dir.Nome DIRNOME,'
      'IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM'
      'FROM uploads upl'
      'LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb'
      'LEFT JOIN cond con ON con.Codigo=upl.Cond'
      'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente'
      'ORDER BY NCONDOM, upl.DirWeb, upl.Nome')
    Left = 749
    Top = 57
    object QrCondGriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondGriNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCondGriArquivo: TWideStringField
      FieldName = 'Arquivo'
      Required = True
      Size = 32
    end
    object QrCondGriCond: TIntegerField
      FieldName = 'Cond'
      Required = True
    end
    object QrCondGriLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCondGriDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCondGriDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCondGriUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCondGriUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCondGriAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCondGriAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrCondGriDirWeb: TIntegerField
      FieldName = 'DirWeb'
      Required = True
    end
    object QrCondGriNDIRWEB: TWideStringField
      FieldName = 'NDIRWEB'
      Size = 32
    end
    object QrCondGriDIRNOME: TWideStringField
      FieldName = 'DIRNOME'
      Size = 32
    end
    object QrCondGriNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Size = 100
    end
  end
  object DsCondGri: TDataSource
    DataSet = QrCondGri
    Left = 777
    Top = 57
  end
  object IdFTP1: TIdFTP
    OnStatus = IdFTP1Status
    OnDisconnected = IdFTP1Disconnected
    OnWork = IdFTP1Work
    OnWorkBegin = IdFTP1WorkBegin
    OnWorkEnd = IdFTP1WorkEnd
    AutoLogin = True
    Passive = True
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    OnAfterClientLogin = IdFTP1AfterClientLogin
    Left = 544
    Top = 144
  end
end
