object FmSyndic_MLA: TFmSyndic_MLA
  Left = 399
  Top = 261
  Caption = 'Syndic'
  ClientHeight = 179
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 408
    Height = 74
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 78
      Top = 24
      Width = 29
      Height = 13
      Caption = 'Login:'
    end
    object Label2: TLabel
      Left = 221
      Top = 24
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object Label3: TLabel
      Left = 8
      Top = 24
      Width = 43
      Height = 13
      Caption = 'Terminal:'
    end
    object LaSenhas: TLabel
      Left = 78
      Top = 61
      Width = 80
      Height = 13
      Cursor = crHandPoint
      Caption = 'Gerenciar Senha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaSenhasClick
      OnMouseEnter = LaSenhasMouseEnter
      OnMouseLeave = LaSenhasMouseLeave
    end
    object LaConexao: TLabel
      Left = 264
      Top = 61
      Width = 96
      Height = 13
      Cursor = crHandPoint
      Caption = 'Gerenciar Conex'#245'es'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaConexaoClick
      OnMouseEnter = LaConexaoMouseEnter
      OnMouseLeave = LaConexaoMouseLeave
    end
    object EdLogin: TEdit
      Left = 78
      Top = 39
      Width = 139
      Height = 22
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 0
      OnKeyDown = EdLoginKeyDown
    end
    object EdSenha: TEdit
      Left = 221
      Top = 39
      Width = 137
      Height = 22
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 1
      OnKeyDown = EdSenhaKeyDown
    end
    object EdTerminal: TEdit
      Left = 8
      Top = 39
      Width = 64
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 2
      OnKeyDown = EdLoginKeyDown
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 132
    Width = 408
    Height = 47
    Align = alBottom
    TabOrder = 1
    object BtEntra: TBitBtn
      Tag = 14
      Left = 10
      Top = 4
      Width = 118
      Height = 39
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtEntraClick
    end
    object BtSair: TBitBtn
      Tag = 15
      Left = 281
      Top = 4
      Width = 117
      Height = 39
      Caption = '&Fechar'
      TabOrder = 1
      OnClick = BtSairClick
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 74
    Width = 408
    Height = 58
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 404
      Height = 43
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 26
        Width = 405
        Height = 16
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 128
    Top = 8
    object Close1: TMenuItem
      Caption = '&Close'
      OnClick = Close1Click
    end
  end
end
