unit CondSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Grids,
  DBGrids, dmkDBGrid, dmkEdit, dmkDBLookupComboBox, dmkEditCB, UnInternalConsts,
  dmkGeral, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmCondSel = class(TForm)
    Panel3: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel1: TPanel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Panel4: TPanel;
    EdNome: TdmkEdit;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    RGAtivo: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdNomeChange(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RGAtivoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEmpresas();
  public
    { Public declarations }
    FCond, FEnti: Integer;
  end;

  var
  FmCondSel: TFmCondSel;

implementation

uses Module, Principal, UMySQLModule, ModuleGeral, UnMyObjects;

{$R *.DFM}

procedure TFmCondSel.BtSaidaClick(Sender: TObject);
begin
  FCond := 0;
  FEnti := 0;
  Close;
end;

procedure TFmCondSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondSel.ReopenEmpresas();
var
  SQL, SQLAtivos: String;
begin
  SQLAtivos := 'AND eci.Ativo <> -1';
  //
  case RGAtivo.ItemIndex of
    1: SQLAtivos := SQLAtivos + ' AND eci.Ativo = ' + Geral.FF0(1);
    2: SQLAtivos := SQLAtivos + ' AND eci.Ativo = ' + Geral.FF0(0);
  end;
  //
  SQL := 'AND IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE "%' + EdNome.Text + '%"';
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa, SQL + SQLAtivos);
end;

procedure TFmCondSel.RGAtivoClick(Sender: TObject);
begin
  ReopenEmpresas();
end;

procedure TFmCondSel.EdNomeChange(Sender: TObject);
begin
  ReopenEmpresas();
end;

procedure TFmCondSel.dmkDBGrid1DblClick(Sender: TObject);
begin
  if DModG.QrEmpresas.RecordCount > 0 then
  begin
    EdEmpresa.ValueVariant := DModG.QrEmpresasFilial.Value;
    CBEmpresa.KeyValue := DModG.QrEmpresasFilial.Value;
    BtOKClick(Self);
  end;
end;

procedure TFmCondSel.BtOKClick(Sender: TObject);
begin
  FCond := Geral.IMV(EdEmpresa.Text);
  FEnti := DModG.QrEmpresasCodigo.Value;
  FmPrincipal.DefineVarsCliInt(EdEmpresa.ValueVariant);
  //
  Close;
end;

procedure TFmCondSel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
end;

procedure TFmCondSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  RGAtivo.ItemIndex := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  ReopenEmpresas();
end;

end.
