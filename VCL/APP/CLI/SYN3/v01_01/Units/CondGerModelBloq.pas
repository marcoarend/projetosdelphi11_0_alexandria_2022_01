unit CondGerModelBloq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, Db, mySQLDbTables, dmkGeral, dmkImage, UnInternalConsts,
  UnDmkEnums;

type
  TFmCondGerModelBloq = class(TForm)
    RGModelBloq: TRadioGroup;
    GBConfig: TGroupBox;
    EdConfig: TdmkEditCB;
    CBConfig: TdmkDBLookupComboBox;
    QrConfigBol: TmySQLQuery;
    DsConfigBol: TDataSource;
    QrConfigBolCodigo: TIntegerField;
    QrConfigBolNome: TWideStringField;
    QrConfigBolColunas: TSmallintField;
    Panel1: TPanel;
    CkAgrupaMensal: TCheckBox;
    RGCompe: TRadioGroup;
    GroupBox1: TGroupBox;
    ST_Condominio: TStaticText;
    StaticText1: TStaticText;
    StaticText3: TStaticText;
    STUH_Propriet: TStaticText;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    SpeedButton2: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGModelBloqClick(Sender: TObject);
    procedure EdConfigChange(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaBotaoOK();
  public
    { Public declarations }
    FImprime: Boolean;
  end;

  var
  FmCondGerModelBloq: TFmCondGerModelBloq;

implementation

uses Principal, UnMyObjects, Module, UMySQLModule, ModuleBloq, UnBloquetos,
  UnBloquetos_Jan;

{$R *.DFM}

procedure TFmCondGerModelBloq.BtSaidaClick(Sender: TObject);
begin
  FImprime := False;
  Close;
end;

procedure TFmCondGerModelBloq.EdConfigChange(Sender: TObject);
begin
  HabilitaBotaoOK();
end;

procedure TFmCondGerModelBloq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerModelBloq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerModelBloq.HabilitaBotaoOK();
var
  Enab: Boolean;
begin
  Enab := DmBloq.ValidaModeloBoleto(RGModelBloq, RGCompe, EdConfig.ValueVariant);
  //
  BtOK.Enabled := Enab;
end;

procedure TFmCondGerModelBloq.BtOKClick(Sender: TObject);
var
  Modelo: String;
begin
  Modelo := RGModelBloq.Items[RGModelBloq.ItemIndex][1];
  if (Modelo = 'E') and (RGCompe.ItemIndex < 1) then
  begin
    Geral.MensagemBox('O modelo "E" exige a quantidade de vias de ' +
    'ficha de compensação!', 'Aviso', MB_OK+MB_ICONWARNING);
    RGCompe.SetFocus;
    Exit;
  end else begin
    FImprime := True;
    Close;
  end;
end;

procedure TFmCondGerModelBloq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FImprime := False;
  UBloquetos.PreencheModelosBloq(RGModelBloq);
  QrConfigBol.Open;
end;

procedure TFmCondGerModelBloq.RGModelBloqClick(Sender: TObject);
var
  Compe, ModelBol: Boolean;
begin
  DmBloq.ConfiguraCompeConfigModelBol(RGModelBloq, Compe, ModelBol);
  //
  GBConfig.Visible := ModelBol;
  RGCompe.Visible  := Compe;
  //
  HabilitaBotaoOK();
end;

procedure TFmCondGerModelBloq.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UBloquetos_Jan.MostraConfigBol(EdConfig.ValueVariant);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.AbreQuery(QrConfigBol, DMod.MyDB);
    //
    EdConfig.ValueVariant := VAR_CADASTRO;
    CBConfig.KeyValue     := VAR_CADASTRO;
    EdConfig.SetFocus;
  end;
end;

end.
