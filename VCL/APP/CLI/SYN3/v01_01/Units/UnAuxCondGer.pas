unit UnAuxCondGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, ComCtrls, dmkDBGrid, MyDBCheck, DBCtrls, UnDmkProcFunc,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, dmkEditDateTimePicker, TypInfo,
  stdctrls, UnMyVCLref, dmkGeral, dmkLabel, dmkRadioGroup, dmkMemo, dmkdbEdit,
  dmkCheckBox, DBGrids, UnMLAGeral, UnDmkEnums;

type
  TUnAuxCondGer = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure IncluiintensbasedearrecadaoNovo(SoListaProvisoes: Boolean);
  end;
var
  AuxCondGer: TUnAuxCondGer;

implementation

uses Module, UCreate, ModuleCond, CondGer, UMySQLModule, GetValor, ArreBaA,
ModuleGeral, UnMyObjects;


procedure TUnAuxCondGer.IncluiintensbasedearrecadaoNovo(SoListaProvisoes: Boolean);
var
  Pode, Aded, MesI, MesF, MesT, AnoT, a, b, c: Word;
  Adiciona, Seq, ArreInt: Integer;
  Texto, ComplTxt, Titulo: String;
  Txa, ValB, Val1, Val2, Val3, ValCalc, FatorUH, Fator_u, Fator_t, PercXtra: Double;
  ResVar: Variant;
  IncluiArrec: Boolean;
begin
  if not SoListaProvisoes then
  begin
    if Geral.MB_Pergunta('� aconselhavel incluir estes itens ' +
      'somente ap�s definido toda provis�o or�amentaria, todas leituras, e ' +
      'todas arrecada��es extras, pois ela afeta os valores aqui definidos. ' +
      sLineBreak + 'Confirma a inclus�o dos itens?') <> ID_YES
    then
      Exit;
  end;
  Screen.Cursor := crHourGlass;
  Pode := 0;
  Aded := 0;
  Seq  := 0;
  Val3 := 0;
  //
  FmCondGer.FArreBaA    := UCriar.RecriaTempTableNovo(ntrttArreBaA, DmodG.QrUpdPID1, False);
  FmCondGer.FArreBaAUni := UCriar.RecriaTempTableNovo(ntrttArreBaAUni, DmodG.QrUpdPID1, False);
  {
  UCriar.Recria Tabela Local('ArreBAA', 1);
  UCriar.Recria Tabela Local('ArreBAAUni', 1);
  }
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FmCondGer.FArreBaA + ' SET ');
  DmodG.QrUpdPID1.SQL.Add('Conta=:P0, ArreBac=:P1, ArreBaI=:P2, Valor=:P3, ');
  DmodG.QrUpdPID1.SQL.Add('Texto=:P4, Adiciona=:P5, Seq=:P6, CNAB_Cfg=:P7');
  //
  DmodG.QrUpdPID2.Close;
  DmodG.QrUpdPID2.SQL.Clear;
  DmodG.QrUpdPID2.SQL.Add('INSERT INTO ' + FmCondGer.FArreBaAUni + ' SET ');
  DmodG.QrUpdPID2.SQL.Add('Seq=:P0, Apto=:P1, Unidade=:P2, Propriet=:P3, ');
  DmodG.QrUpdPID2.SQL.Add('NomePropriet=:P4, Valor=:P5, Adiciona=:P6, ');
  DmodG.QrUpdPID2.SQL.Add('NaoArreSobre=:P7, Calculo=:P8, Cotas=:P9, ');
  DmodG.QrUpdPID2.SQL.Add('DescriCota=:P10, TextoCota=:P11, ');
  DmodG.QrUpdPID2.SQL.Add('ArreBac=:P12, ArreBaI=:P13, NaoArreRisco=:P14');
  //
  DmCond.QrNIA1.Close;
  DmCond.QrNIA1.SQL.Clear;
  DmCond.QrNIA1.SQL.Add('SELECT abc.Codigo, abc.Nome, abc.Conta,');
  DmCond.QrNIA1.SQL.Add('abi.Valor, abi.SitCobr, abi.Parcelas, abi.ParcPerI,');
  DmCond.QrNIA1.SQL.Add('abi.ParcPerF, abi.InfoParc, abi.Texto, abi.Controle,');
  DmCond.QrNIA1.SQL.Add('abi.Fator,abi.Percent, abi.DeQuem, abi.DoQue,');
  DmCond.QrNIA1.SQL.Add('abi.ComoCobra, abi.NaoArreSobre,');
  DmCond.QrNIA1.SQL.Add('abi.Calculo, abi.Arredonda, abi.ListaBaU,');
  DmCond.QrNIA1.SQL.Add('abi.DescriCota, abi.ValFracDef, abi.ValFracAsk,');
  DmCond.QrNIA1.SQL.Add('abi.Partilha, abi.NaoArreRisco, abi.CNAB_Cfg');
  DmCond.QrNIA1.SQL.Add('FROM arrebac abc');
  DmCond.QrNIA1.SQL.Add('LEFT JOIN arrebai abi ON abi.Codigo=Abc.Codigo');
  DmCond.QrNIA1.SQL.Add('WHERE abi.Cond=:P0');
  DmCond.QrNIA1.SQL.Add('AND abc.Codigo NOT IN');
  DmCond.QrNIA1.SQL.Add('(');
  DmCond.QrNIA1.SQL.Add('  SELECT Codigo');
  DmCond.QrNIA1.SQL.Add('  FROM ' + DmCond.FTabAriA);
  DmCond.QrNIA1.SQL.Add('  WHERE Codigo=:P1');
  DmCond.QrNIA1.SQL.Add(')');
  DmCond.QrNIA1.SQL.Add('ORDER BY DestCobra');
  DmCond.QrNIA1.Params[00].AsInteger := DmCond.QrCondCodigo.Value;
  DmCond.QrNIA1.Params[01].AsInteger := FmCondGer.QrPrevCodigo.Value;
  UMyMod.AbreQuery(DmCond.QrNIA1, Dmod.MyDB);
  //

(* //  N�o est� sendo usado!! O cliente reclama que n�o funciona!!!
  if not SoListaProvisoes then
  begin
    DCond.QrNID.Close;
    DCond.QrNID.Params[00].AsInteger := DmCond.QrCondCodigo.Value;
    DCond.QrNID.Params[01].AsInteger := FmCondGer.QrPrevCodigo.Value;
    DCond.QrNID. O p e n ;
    //
  end;
*)
  FmCondGer.ProgressBar2.Visible := True;
  FmCondGer.ProgressBar2.Max := DmCond.QrNIA1.RecordCount;

  // Cobran�as Sobre provis�es.
  while not DmCond.QrNIA1.Eof do
  begin
    if SoListaProvisoes then
    begin
      IncluiArrec :=
        (DmCond.QrNIA1Fator.Value = 1) and (DmCond.QrNIA1Doque.Value = 0);
    end else IncluiArrec := True;
    if IncluiArrec then
    begin
      case DmCond.QrNIA1SitCobr.Value of
        0: ; // N�o cobrar
        1:   // Cobran�a Cont�nua (Anual)
        begin
          MesI := DmCond.QrNIA1ParcPerI.Value;
          MesF := DmCond.QrNIA1ParcPerF.Value;
          dmkPF.PeriodoDecode(FmCondGer.QrPrevPeriodo.Value, AnoT, MesT);
          //
          if (MesI > 0) and (MesF > 0) and (MesI < 13) and (MesF < 13) then
          begin
            if (MesF > MesI) then
            begin
              if (MesF >= MesT)  and (MesI <= MesT) then
              inc(Pode, 1);
            end else
            if (MesF < MesI) then
            begin
              a := MesI;
              b := MesT;
              if MesF > MesT then b := b + 12;
              c := MesF + 12;
              if (c >= b) and (a <= b) then
                inc(Pode, 1);
            end;
          end;
        end;
        2:  // Cobran�a Programada
        begin
          MesI := DmCond.QrNIA1ParcPerI.Value;
          MesF := DmCond.QrNIA1ParcPerF.Value;
          MesT := FmCondGer.QrPrevPeriodo.Value;
          //
          if MesI > MesF then
          begin
            if MesF > MesT then MesT := MesT + 12;
            MesF := MesF + 12;
          end;
          if (MesF >= MesT)  and (MesI <= MesT) then
            inc(Pode, 1);
        end;
      end;

      ////////////////////////////////////////////////////////////////////////////

      if Aded < Pode then
      begin
        ValB := 0;
        inc(Aded, 1);
        Adiciona := 1;
        if Trim(DmCond.QrNIA1Texto.Value) <> '' then
          Texto := DmCond.QrNIA1Texto.Value
        else
          Texto := DmCond.QrNIA1Nome.Value;
        (*
        if Texto = 'TAXA ADM. COBRAN�A - PR� COND�MINO' then
          ShowMessage(Texto);
        *)
        if DmCond.QrNIA1InfoParc.Value = 1 then
        begin
          a := 0;
          b := 0;
          c := 0;
          case DmCond.QrNIA1SitCobr.Value of
            1: // Cobran�a Cont�nua (Anual)
            begin
              dmkPF.PeriodoDecode(FmCondGer.QrPrevPeriodo.Value, AnoT, MesT);
              a := DmCond.QrNIA1ParcPerI.Value;
              c := DmCond.QrNIA1ParcPerF.Value;
              if c < a then Inc(c, 12);
              b := MesT;
              if b < a then Inc(b, 12);
            end;
            2: // Cobran�a Programada
            begin
              a := DmCond.QrNIA1ParcPerI.Value;
              b := FmCondGer.QrPrevPeriodo.Value;
              c := DmCond.QrNIA1ParcPerF.Value;
            end;
          end;
          Texto := FormatFloat('00', b-a+1) + '/' + FormatFloat('00', c-a+1) +
            ' - ' + Texto;
        end;
        FmCondGer.ProgressBar2.Position := FmCondGer.ProgressBar2.Position + 1;
        FmCondGer.Update;
        Application.ProcessMessages;
        //
        DmCond.QrAptos.Close;
        DmCond.QrAptos.SQL.Clear;
        if DmCond.QrNIA1DeQuem.Value in ([0,1]) then
        begin
          // De todos  e selecionados
          DmCond.QrAptos.SQL.Add('SELECT cdi.Conta, cdi.Unidade, cdi.Propriet, CASE WHEN ');
          DmCond.QrAptos.SQL.Add('ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEPROPRIET,');
          DmCond.QrAptos.SQL.Add('FracaoIdeal, Moradores');
          DmCond.QrAptos.SQL.Add('FROM condimov cdi');
          DmCond.QrAptos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet');
          DmCond.QrAptos.SQL.Add('WHERE cdi.Codigo=:P0');
          DmCond.QrAptos.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
        end;
        if DmCond.QrNIA1DeQuem.Value = 0 then
        begin
          // De todos Habilitados
          DmCond.QrAptos.SQL.Add('AND cdi.SitImv=1');
        end else if DmCond.QrNIA1DeQuem.Value = 1 then
        begin
          // Selecionados
          DmCond.QrAptos.SQL.Add('AND cdi.Conta IN (');
          DmCond.QrAptos.SQL.Add('  SELECT Apto FROM arrebau');
          (*
          DmCond.QrAptos.SQL.Add('  WHERE Codigo=:P1');
          DmCond.QrAptos.SQL.Add('  AND Controle=:P2)');
          DmCond.QrAptos.Params[1].AsInteger := DmCond.QrNIA1Codigo.Value;
          DmCond.QrAptos.Params[2].AsInteger := DmCond.QrNIA1Controle.Value;
          *)
          DmCond.QrAptos.SQL.Add('  WHERE Controle=:P1)');
          if DmCond.QrNIA1ListaBaU.Value <> 0 then
            DmCond.QrAptos.Params[1].AsInteger := DmCond.QrNIA1ListaBau.Value
          else
            DmCond.QrAptos.Params[1].AsInteger := DmCond.QrNIA1Controle.Value;
        end;
        DmCond.QrAptos.SQL.Add('ORDER BY cdi.Unidade');
        UMyMod.AbreQuery(DmCond.QrAptos, Dmod.MyDB, 'TUnAuxCondGer.IncluiintensbasedearrecadaoNovo()');
        Inc(Seq, 1);
        //
        FmCondGer.ProgressBar3.Visible  := True;
        FmCondGer.ProgressBar3.Position := 0;
        FmCondGer.ProgressBar3.Max := DmCond.QrAptos.RecordCount;
        //
        //  ArreBaIFator para ValB
        case DmCond.QrNIA1Fator.Value of
          // Fator = Valor
          0: ValB := DmCond.QrNIA1Valor.Value;
          // 1: Fator = Percentual
          1:
          begin
            if DmCond.QrAptos.RecordCount = 0 then ValB := 0 else
            begin
              ValCalc := 0;
              case DmCond.QrNIA1DoQue.Value of
                //  Sobre Provis�es
                0: ValCalc := FmCondGer.QrPrevGastos.Value;
                //  Sobre Consumos
                1: ValCalc := 0;//QrSumCTVALOR.Value;
                //  Sobre ambos
                2: ValCalc := FmCondGer.QrPrevGastos.Value;// + QrSumCTVALOR.Value;
                //  Valor a definir / definido
                3:
                begin
                  if DmCond.QrNIA1ValFracAsk.Value = 1 then
                  begin
                    // Parei aqui
                    if DmCond.QrNIA1Texto.Value <> '' then
                      Titulo := '(' + DmCond.QrNIA1Texto.Value + ')'
                    else
                      Titulo := '';
                    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
                      DmCond.QrNIA1ValFracDef.Value, 2, 0, '', '', True,
                      'XXX-XXXXX-998 :: ' + DmCond.QrNIA1Nome.Value,
                      'Informe o valor total: ' + Titulo, 0, ResVar)
                    then
                      ValB := ResVar
                    else
                      ValB := 0;
                  end else ValB := DmCond.QrNIA1ValFracDef.Value;
                end;
                4:
                begin
                  // Parei aqui!!!
                  // Precisa Fazer ArreBaI
                end;
                else ValCalc := 0;
              end;
              if DmCond.QrNIA1DoQue.Value <> 3 then
              begin
                if DmCond.QrNIA1ComoCobra.Value = 0 then
                //  Calcular o percentual sobre o valor cobrado.
                //n�o dividir por apto aqui! ????
                //  ValB := (DmCond.QrNIA1Percent.Value * ValCalc) / DmCond.QrAptos.RecordCount
                  ValB := DmCond.QrNIA1Percent.Value * ValCalc / 100
                else
                begin
                //  Incluir o pr�prio valor gerado no c�lculo.
                  if DmCond.QrNIA1Percent.Value = 0 then ValB := 0 else
                  begin
                    PercXtra := DmCond.QrNIA1Percent.Value;
                    PercXtra := PercXtra + (PercXtra * PercXtra / 100);
                    // ... nem dividir por apto aqui! ????
                    //  ValB := ValCalc / (1 - (DmCond.QrNIA1Percent.Value/100)) / DmCond.QrAptos.RecordCount;
                    ValB := PercXtra * ValCalc / 100;
                    //ValB := ValCalc / (1 - (PercXtra/100));
                    //ShowMessage(FloatToStr(ValB));
                  end;
                end;
              end;
            end;
            // n�o precisa ainda ??
            //if ValB > 0 then ValB := Int(ValB + 0.5) / 100;
          end;
          else begin
            ValB := 0;
            Geral.MensagemBox(
              'Fator n�o definido no cadastro de arrecada��es base!',
              'Avise a DERMATEK!', MB_OK+MB_ICONWARNING);
          end;
        end;
        // FIM ArreBaIFator para ValB

        //Total das fra��es ideais ou moradores na partilha
        case DmCond.QrNIA1Partilha.Value of
          1:
          begin
            DmCond.QrTFracao.Close;
            DmCond.QrTFracao.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
            UMyMod.AbreQuery(DmCond.QrTFracao, Dmod.MyDB);
          end;
          2:
          begin
            DmCond.QrTMoradores.Close;
            DmCond.QrTMoradores.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
            UMyMod.AbreQuery(DmCond.QrTMoradores, Dmod.MyDB);
          end;
        end;
        //calcula % por condomino aqui ?
        DmCond.QrAptos.First;
        while not DmCond.QrAptos.Eof do
        begin
          // calcular aqui o Val1 por apto conforme o campo "ArreBaI.Calculo"
          Val1    := 0;
          Fator_u := 0;
          case DmCond.QrNIA1Fator.Value of
            // Fator: Valor
            0: Val1 := ValB;
            // Fator: Percentual
            1:
            begin
              case DmCond.QrNIA1DeQuem.Value of
                // Todos
                0:
                begin
                  // Partilha
                  case DmCond.QrNIA1Partilha.Value of
                    // Equitativa
                    0: FatorUH := 1 / DmCond.QrAptos.RecordCount;
                    // Fra��o ideal
                    1:
                    begin
                      if DmCond.QrTFracaoTOTAL.Value = 0 then FatorUH := 0 else
                      FatorUH := DmCond.QrAptosFracaoIdeal.Value / DmCond.QrTFracaoTOTAL.Value;
                    end;
                    // Moradores
                    2:
                    begin
                      if DmCond.QrTMoradoresTOTAL.Value = 0 then FatorUH := 0 else
                      FatorUH := DmCond.QrAptosMoradores.Value / DmCond.QrTMoradoresTOTAL.Value;
                    end
                    else begin
                      FatorUH := 0;
                      Geral.MensagemBox('Partilha n�o definida!', 'ERRO',
                      MB_OK+MB_ICONERROR);
                    end;
                  end;
                  Val1 := ValB * FatorUH;
                end;
                // Selecionados
                1:
                begin
                  case DmCond.QrNIA1Calculo.Value of
                    0: FatorUH := 1 / DmCond.QrAptos.RecordCount;
                    1: FatorUH := DmCond.GetPercAptUni(DmCond.QrNIA1Controle.Value,
                       DmCond.QrNIA1ListaBaU.Value, DmCond.QrAptosConta.Value,
                       DmCond.QrNIA1Nome.Value) / 100;
                    2:
                    begin
                      Fator_u := DmCond.GetPercAptUni(DmCond.QrNIA1Controle.Value,
                        DmCond.QrNIA1ListaBaU.Value, DmCond.QrAptosConta.Value,
                        DmCond.QrNIA1Nome.Value);
                      Fator_t := DmCond.GetPercAptAll(DmCond.QrNIA1Controle.Value,
                      DmCond.QrNIA1ListaBaU.Value);
                      if Fator_t <> 0 then
                        FatorUH := Fator_u / Fator_t
                      else
                        FatorUH := 0;
                      // se for cotas ...
                      // N�o pode ser aqui!
                      (*
                      if DmCond.QrNIA1Calculo.Value = 2 then
                      begin
                        Texto := Texto + ' ' +
                          FloatToStr(Fator_u) + DmCond.QrNIA1DescriCota.Value
                      end;
                      *)
                      //
                    end;
                    else begin
                      FatorUH := 0;
                      Geral.MensagemBox('C�lculo n�o definido!', 'ERRO',
                      MB_OK+MB_ICONERROR);
                    end;
                  end;
                  Val1 := ValB * FatorUH;
                end;
              end;
            end;
            else begin
              Val1 := 0;
              (*
              J� avisado acima!!
              Geral.MensagemBox(
                'Fator n�o definido no cadastro de arrecada��es base!',
                'Avise a DERMATEK!', MB_OK+MB_ICONWARNING);
              *)
            end;
          end;
          //
          // Arredonda valor conforme configurado no ArreBaI
          if DmCond.QrNIA1Arredonda.Value > 0 then
          begin
            ArreInt := Trunc((Val1 + (DmCond.QrNIA1Arredonda.Value * 0.99)) /
              DmCond.QrNIA1Arredonda.Value);
            Val1 := ArreInt * DmCond.QrNIA1Arredonda.Value;
          end else Val1 := Trunc(Val1 * 100) / 100;
          //

          FmCondGer.ProgressBar3.Position := FmCondGer.ProgressBar3.Position + 1;
          Val2 := 0;
          // Quando for cobrar sobre consumos ou Consumos + Provis�es
          if DmCond.QrNIA1DoQue.Value > 0 then
          begin
            DmCond.QrConsApt.Close;
            DmCond.QrConsApt.SQL.Clear;
            DmCond.QrConsApt.SQL.Add('SELECT SUM(cni.Valor) Valor');
            DmCond.QrConsApt.SQL.Add('FROM cons cns');
            DmCond.QrConsApt.SQL.Add('LEFT JOIN ' + DmCond.FTabCnsA + ' cni ON cni.Codigo=cns.Codigo');
            DmCond.QrConsApt.SQL.Add('LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo');
            DmCond.QrConsApt.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=cni.Apto');
            DmCond.QrConsApt.SQL.Add('WHERE cni.Cond=:P0');
            DmCond.QrConsApt.SQL.Add('AND cnp.Cond=:P1');
            DmCond.QrConsApt.SQL.Add('AND cni.Periodo=:P2');
            DmCond.QrConsApt.SQL.Add('AND cdi.Conta=:P3');
            DmCond.QrConsApt.Params[00].AsInteger := DmCond.QrCondCodigo.Value;
            DmCond.QrConsApt.Params[01].AsInteger := DmCond.QrCondCodigo.Value;
            DmCond.QrConsApt.Params[02].AsInteger := FmCondGer.QrPrevPeriodo.Value;
            DmCond.QrConsApt.Params[03].AsInteger := DmCond.QrAptosConta.Value;
            UMyMod.AbreQuery(DmCond.QrConsApt, Dmod.MyDB);
            case DmCond.QrNIA1DoQue.Value of
              // Sobre Provis�es
              0: ValCalc := 0;//QrPrevGastos.Value;
              // Sobre Consumos
              1: ValCalc := DmCond.QrConsAptValor.Value;
              // Sobre Ambos
              //2: ValCalc := (*QrPrevGastos.Value +*) DmCond.QrConsAptValor.Value;
              2: ValCalc := DmCond.QrConsAptValor.Value;
              // Valor para c�lculo
              // N�o Calcula Val2 para "Valor para c�lculo" ?
              3: ValCalc := 0;
              // Sobre as pr�prias arrecada��es rec�m geradas
              // Taxa de Administra��o de Cobran�a (Seguro de recebimento)
              4:
              begin
                DmCond.QrSumBaAUni.Close;
                DmCond.QrSumBaAUni.Params[0].AsInteger := DmCond.QrAptosConta.Value;
                UMyMod.AbreQuery(DmCond.QrSumBaAUni, DModG.MyPID_DB);
                //
                DmCond.QrSumArreUH.Close;
                DmCond.QrSumArreUH.SQL.Clear;
                DmCond.QrSumArreUH.SQL.Add('SELECT SUM(Valor) Valor');
                DmCond.QrSumArreUH.SQL.Add('FROM ' + DmCond.FTabAriA);
                DmCond.QrSumArreUH.SQL.Add('WHERE NaoArreRisco=0');
                DmCond.QrSumArreUH.SQL.Add('AND Codigo=:P0');
                DmCond.QrSumArreUH.SQL.Add('AND Apto=:P1');
                DmCond.QrSumArreUH.Params[00].AsInteger := FmCondGer.QrPrevCodigo.Value;
                DmCond.QrSumArreUH.Params[01].AsInteger := DmCond.QrAptosConta.Value;
                UMyMod.AbreQuery(DmCond.QrSumArreUH, Dmod.MyDB);
                //
                ValCalc := DmCond.QrSumBaAUniValor.Value +
                  DmCond.QrSumArreUHValor.Value + DmCond.QrConsAptValor.Value;
              end;
              else ValCalc := 0;
            end;
            if DmCond.QrNIA1ComoCobra.Value = 0 then
            //  Calcular o percentual sobre o valor cobrado.
              Val2 := (DmCond.QrNIA1Percent.Value * ValCalc) / 100
            else
            begin
            //  Incluir o pr�prio valor gerado no c�lculo.
              if DmCond.QrNIA1Percent.Value = 0 then
                Val2 := 0
              else
              begin
                //Val2 := (ValCalc / (1 - (DmCond.QrNIA1Percent.Value/100))) - ValCalc;
                Txa := Trunc(((ValCalc * DmCond.QrNIA1Percent.Value / 100) + 0.01) * 100) / 100;
                Val2 := (ValCalc + Txa) * DmCond.QrNIA1Percent.Value / 100;
              end;
            end;
          end;
          //
          // Arredonda valor conforme configurado no ArreBaI
          if (Val2 >= 0.01) and (DmCond.QrNIA1Arredonda.Value > 0) then
          begin
            ArreInt := Trunc((Val2 + (DmCond.QrNIA1Arredonda.Value * 0.99)) /
              DmCond.QrNIA1Arredonda.Value);
            Val2 := ArreInt * DmCond.QrNIA1Arredonda.Value;
          end else Val2 := Trunc(Val2 * 100) / 100;
          //
          if (Fator_u > 0) and (DmCond.QrNIA1Calculo.Value = 2)
          and (DmCond.QrNIA1DescriCota.Value <> '') then
            ComplTxt := DmCond.QrNIA1DescriCota.Value + ': ' + FloatToStr(Fator_u)
          else
            ComplTxt := '';
          DmodG.QrUpdPID2.Params[00].AsInteger := Seq;
          DmodG.QrUpdPID2.Params[01].AsInteger := DmCond.QrAptosConta.Value;
          DmodG.QrUpdPID2.Params[02].AsString  := DmCond.QrAptosUnidade.Value;
          DmodG.QrUpdPID2.Params[03].AsInteger := DmCond.QrAptosPropriet.Value;
          DmodG.QrUpdPID2.Params[04].AsString  := DmCond.QrAptosNOMEPROPRIET.Value;
          DmodG.QrUpdPID2.Params[05].AsFloat   := Val1+Val2;
          DmodG.QrUpdPID2.Params[06].AsInteger := Adiciona;
          DmodG.QrUpdPID2.Params[07].AsInteger := DmCond.QrNIA1NaoArreSobre.Value;
          DmodG.QrUpdPID2.Params[08].AsInteger := DmCond.QrNIA1Calculo.Value;
          DmodG.QrUpdPID2.Params[09].AsFloat   := Fator_u;
          DmodG.QrUpdPID2.Params[10].AsString  := DmCond.QrNIA1DescriCota.Value;
          DmodG.QrUpdPID2.Params[11].AsString  := ComplTxt;
          DmodG.QrUpdPID2.Params[12].AsInteger := DmCond.QrNIA1Codigo.Value;   // ArreBaC
          DmodG.QrUpdPID2.Params[13].AsInteger := DmCond.QrNIA1Controle.Value; // ArreBaI
          DmodG.QrUpdPID2.Params[14].AsInteger := DmCond.QrNIA1NaoArreRisco.Value;
          UMyMod.ExecutaQuery(DmodG.QrUpdPID2);
          //
          Val3 := Val3 + Val1 + Val2;
          DmCond.QrAptos.Next;
        end;
        DmodG.QrUpdPID1.Params[00].AsInteger := DmCond.QrNIA1Conta.Value;
        DmodG.QrUpdPID1.Params[01].AsInteger := DmCond.QrNIA1Codigo.Value;
        DmodG.QrUpdPID1.Params[02].AsInteger := DmCond.QrNIA1Controle.Value;
        DmodG.QrUpdPID1.Params[03].AsFloat   := Val3;
        DmodG.QrUpdPID1.Params[04].AsString  := Texto;
        DmodG.QrUpdPID1.Params[05].AsInteger := Adiciona;
        DmodG.QrUpdPID1.Params[06].AsInteger := Seq;
        DModG.QrUpdPID1.Params[07].AsInteger := DmCond.QrNIA1CNAB_Cfg.Value;
        DmodG.QrUpdPID1.ExecSQL;
        Val3 := 0;
      end;

      ////////////////////////////////////////////////////////////////////////////
    end;
    DmCond.QrNIA1.Next;
  end;






(* //  N�o est� sendo usado!! O cliente reclama que n�o funciona!!!
  // Cobran�as Sobre arrecada��es
  // criado para calcular taxa condominial de administradora de riscos
  if not SoListaProvisoes then
  begin
    while not DmCond.QrNID.Eof do
    begin
      case DmCond.QrNIDSitCobr.Value of
        0: ; // nada
        1:
        begin
          MesI := DmCond.QrNIDParcPerI.Value;
          MesF := DmCond.QrNIDParcPerF.Value;
          dmkPF.PeriodoDecode(FmCondGer.QrPrevPeriodo.Value, AnoT, MesT);
          //
          if (MesI > 0) and (MesF > 0) and (MesI < 13) and (MesF < 13) then
          begin
            if (MesF > MesI) then
            begin
              if (MesF >= MesT)  and (MesI <= MesT) then
              inc(Pode, 1);
            end else
            if (MesF < MesI) then
            begin
              a := MesI;
              b := MesT;
              if MesF > MesT then b := b + 12;
              c := MesF + 12;
              if (c >= b) and (a <= b) then
                inc(Pode, 1);
            end;
          end;
        end;
        2:
        begin
          MesI := DmCond.QrNIDParcPerI.Value;
          MesF := DmCond.QrNIDParcPerF.Value;
          MesT := FmCondGer.QrPrevPeriodo.Value;
          //
          if MesI > MesF then
          begin
            if MesF > MesT then MesT := MesT + 12;
            MesF := MesF + 12;
          end;
          if (MesF >= MesT)  and (MesI <= MesT) then
            inc(Pode, 1);
        end;
      end;

      ////////////////////////////////////////////////////////////////////////////

      if Aded < Pode then
      begin
        inc(Aded, 1);
        Adiciona := 1;
        if Trim(DmCond.QrNIDTexto.Value) <> '' then
          Texto := DmCond.QrNIDTexto.Value
        else
          Texto := DmCond.QrNIDNome.Value;
        if DmCond.QrNIDInfoParc.Value = 1 then
        begin
          a := 0;
          b := 0;
          c := 0;
          case DmCond.QrNIDSitCobr.Value of
            1:
            begin
              dmkPF.PeriodoDecode(FmCondGer.QrPrevPeriodo.Value, AnoT, MesT);
              a := DmCond.QrNIDParcPerI.Value;
              c := DmCond.QrNIDParcPerF.Value;
              if c < a then Inc(c, 12);
              b := MesT;
              if b < a then Inc(b, 12);
            end;
            2:
            begin
              a := DmCond.QrNIDParcPerI.Value;
              b := FmCondGer.QrPrevPeriodo.Value;
              c := DmCond.QrNIDParcPerF.Value;
            end;
          end;
          Texto := FormatFloat('00', b-a+1) + '/' + FormatFloat('00', c-a+1) +
            ' - ' + Texto;
        end;
        FmCondGer.ProgressBar2.Position := FmCondGer.ProgressBar2.Position + 1;
        FmCondGer.Update;
        Application.ProcessMessages;
        //
        Val1 := 0;
        Inc(Seq, 1);
        FmCondGer.QrJaArr.Close;
        FmCondGer.QrJaArr.O p e n;
        FmCondGer.QrJaArr.First;
        while not FmCondGer.QrJaArr.Eof do
        begin
          FmCondGer.ProgressBar3.Position := FmCondGer.ProgressBar3.Position + 1;
          Val2 := 0;
          if DmCond.QrNIDDoQue.Value > 0 then
          begin
            DmCond.QrConsApt.Close;
SELECT SUM(cni.Valor) Valor 
FROM cons cns
LEFT JOIN cons+its  cni ON cni.Codigo=cns.Codigo
LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo
LEFT JOIN condimov cdi ON cdi.Conta=cni.Apto
WHERE cni.Cond=:P0
AND cnp.Cond=:P1
AND cni.Periodo=:P2
AND cdi.Conta=:P3
            DmCond.QrConsApt.Params[00].AsInteger := DmCond.QrCondCodigo.Value;
            DmCond.QrConsApt.Params[01].AsInteger := DmCond.QrCondCodigo.Value;
            DmCond.QrConsApt.Params[02].AsInteger := FmCondGer.QrPrevPeriodo.Value;
            DmCond.QrConsApt.Params[03].AsInteger := FmCondGer.QrJaArrApto.Value;
            DmCond.QrConsApt.O p e n;
            case DmCond.QrNIDDoQue.Value of
              0: ValCalc := 0;//FmCondGer.QrJaArrValor.Value;
              1: ValCalc := DmCond.QrConsAptValor.Value;
              //2: ValCalc := FmCondGer.QrJaArrValor.Value + DmCond.QrConsAptValor.Value;
              2: ValCalc := DmCond.QrConsAptValor.Value;
              else ValCalc := 0;
            end;
            ValCalc := ValCalc + FmCondGer.QrJaArrValor.Value;
            if DmCond.QrNIDComoCobra.Value = 0 then
              Val2 := (DmCond.QrNIDPercent.Value * ValCalc)
            else
            begin
              if DmCond.QrNIDPercent.Value = 0 then Val2 := 0 else
              Val2 := (ValCalc / (1 - (DmCond.QrNIDPercent.Value/100))) - ValCalc;
            end;
          end;
          DmodG.QrUpdPID2.Params[00].AsInteger := Seq;
          DmodG.QrUpdPID2.Params[01].AsInteger := FmCondGer.QrJaArrApto.Value;
          DmodG.QrUpdPID2.Params[02].AsString  := FmCondGer.QrJaArrUnidade.Value;
          DmodG.QrUpdPID2.Params[03].AsInteger := FmCondGer.QrJaArrPropriet.Value;
          DmodG.QrUpdPID2.Params[04].AsString  := FmCondGer.QrJaArrNOMEPROPRIET.Value;
          DmodG.QrUpdPID2.Params[05].AsFloat   := Val1+Val2;
          DmodG.QrUpdPID2.Params[06].AsInteger := Adiciona;
          DmodG.QrUpdPID2.Params[07].AsInteger := 0;//DmCond.QrNIDNaoArreSobre.Value;
          //Precisa?
          DmodG.QrUpdPID2.Params[08].AsInteger := FmCondGer.QrJaArrCalculo.Value;
          DmodG.QrUpdPID2.Params[09].AsFloat   := FmCondGer.QrJaArrCotas.Value;
          DmodG.QrUpdPID2.Params[10].AsString  := FmCondGer.QrJaArrDescriCota.Value;
          DmodG.QrUpdPID2.Params[11].AsString  := FmCondGer.QrJaArrTextoCota.Value;
          ///
          DmodG.QrUpdPID2.ExecSQL;
          //
          Val3 := Val3 + Val1 + Val2;
          FmCondGer.QrJaArr.Next;
        end;
        DmodG.QrUpdPID1.Params[00].AsInteger := DmCond.QrNIDConta.Value;
        DmodG.QrUpdPID1.Params[01].AsInteger := DmCond.QrNIDCodigo.Value;
        DmodG.QrUpdPID1.Params[02].AsInteger := DmCond.QrNIDControle.Value;
        DmodG.QrUpdPID1.Params[03].AsFloat   := Val3;
        DmodG.QrUpdPID1.Params[04].AsString  := Texto;
        DmodG.QrUpdPID1.Params[05].AsInteger := Adiciona;
        DmodG.QrUpdPID1.Params[06].AsInteger := Seq;
        DmodG.QrUpdPID1.ExecSQL;
        Val3 := 0;
      end;

      ////////////////////////////////////////////////////////////////////////////

      DmCond.QrNID.Next;
    end;
  end;
*)



  FmCondGer.ProgressBar2.Visible := False;
  FmCondGer.ProgressBar3.Visible := False;
  Screen.Cursor := crDefault;
  if not SoListaProvisoes then
  begin
    case Pode of
      0: Texto := 'N�o foi localizado nenhum item base de or�amento aplic�vel';
      1: Texto := 'Foi localizado 1 (um) item base de or�amento aplic�vel';
      else Texto := 'Foram localizados '+IntToStr(Pode)+' itens base de or�amento aplic�veis';
    end;
    Texto := Texto + ' de ' + IntToStr(DmCond.QrNIA1.RecordCount);
    case DmCond.QrNIA1.RecordCount of
      0: Texto := 'N�o h� itens base de or�amento cadastrados para este cliente!';
      1: Texto := Texto + ' pesquisado!';
      else Texto := Texto + ' pesquisados!';
    end;
    Geral.MensagemBox(Texto, 'Mensagem', MB_OK+MB_ICONINFORMATION);
    //
    if (Pode > 0) then
    begin
      Application.CreateForm(TFmArreBaA, FmArreBaA);
      FmArreBaA.FNomeCliente := DmCond.QrCondNOMECLI.Value;
      FmArreBaA.FCodiCliente := DmCond.QrCondCliente.Value;
      FmArreBaA.FCond        := DmCond.QrCondCodigo.Value;
      FmArreBaA.ShowModal;
      FmArreBaA.Destroy;
    end;
  end;
end;

end.

