object Fmbloqparcpesq: TFmbloqparcpesq
  Left = 339
  Top = 185
  Caption = 'BLQ-REPAR-004 :: Pesquisa de Reparcelamentos'
  ClientHeight = 713
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 545
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 348
      Height = 545
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitHeight = 482
      object dmkDBGridDAC1: TdmkDBGridDAC
        Left = 0
        Top = 20
        Width = 348
        Height = 525
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'CodCliInt'
            Title.Caption = 'Condom.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECOND'
            Title.Caption = 'Nome do condom'#237'nio'
            Width = 260
            Visible = True
          end>
        Color = clWindow
        DataSource = DsCond
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        EditForceNextYear = False
        FiltraDmk = True
        Columns = <
          item
            Expanded = False
            FieldName = 'CodCliInt'
            Title.Caption = 'Condom.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECOND'
            Title.Caption = 'Nome do condom'#237'nio'
            Width = 260
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 348
        Height = 20
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 1
        ExplicitTop = 1
        ExplicitWidth = 346
        object CkConfirmad: TCheckBox
          Left = 8
          Top = 0
          Width = 197
          Height = 17
          Caption = 'Somente n'#227'o confirmados.'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = CkConfirmadClick
        end
      end
    end
    object dmkDBGridDAC2: TdmkDBGridDAC
      Left = 348
      Top = 0
      Width = 658
      Height = 545
      Align = alLeft
      Columns = <
        item
          Expanded = False
          FieldName = 'CodigoEnt'
          Title.Caption = 'Entidade'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPROP'
          Title.Caption = 'Propriet'#225'rio'
          Width = 260
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 70
          Visible = True
        end>
      Color = clWindow
      DataSource = DsProp
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      EditForceNextYear = False
      FiltraDmk = True
      Columns = <
        item
          Expanded = False
          FieldName = 'CodigoEnt'
          Title.Caption = 'Entidade'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPROP'
          Title.Caption = 'Propriet'#225'rio'
          Width = 260
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 70
          Visible = True
        end>
    end
    object dmkDBGridDAC3: TdmkDBGridDAC
      Left = 769
      Top = 0
      Width = 239
      Height = 545
      Align = alRight
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Parcelamento'
          Width = 77
          Visible = True
        end>
      Color = clWindow
      DataSource = DsParc
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = dmkDBGridDAC3DblClick
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Parcelamento'
          Width = 77
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 370
        Height = 32
        Caption = 'Pesquisa de Reparcelamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 370
        Height = 32
        Caption = 'Pesquisa de Reparcelamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 370
        Height = 32
        Caption = 'Pesquisa de Reparcelamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 599
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 643
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
    end
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCondBeforeClose
    AfterScroll = QrCondAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT eci.CodCliInt, eci.CodEnti, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECOND'
      'FROM bloqparc blp'
      'LEFT JOIN entidades cli ON cli.Codigo=blp.CodCliEnt'
      'LEFT JOIN enticliint eci ON eci.CodEnti=cli.Codigo'
      'WHERE eci.TipoTabLct=0'
      'ORDER BY NOMECOND')
    Left = 4
    Top = 12
    object QrCondCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
      Origin = 'enticliint.CodCliInt'
    end
    object QrCondCodEnti: TIntegerField
      FieldName = 'CodEnti'
      Origin = 'enticliint.CodEnti'
    end
    object QrCondNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Origin = 'NOMECOND'
      Size = 100
    end
  end
  object DsCond: TDataSource
    DataSet = QrCond
    Left = 32
    Top = 12
  end
  object QrProp: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPropBeforeClose
    AfterScroll = QrPropAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT blp.CodigoEnt, blp.CodigoEsp,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP,'
      'cim.Unidade'
      'FROM bloqparc blp'
      'LEFT JOIN condimov cim ON cim.Conta=blp.CodigoEsp'
      'LEFT JOIN entidades ent ON ent.Codigo=blp.CodigoEnt'
      'LEFT JOIN enticliint eci ON eci.CodEnti=blp.CodCliEnt'
      'WHERE eci.TipoTabLct=0'
      'AND cim.Codigo=:P0'
      'ORDER BY NOMEPROP')
    Left = 60
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPropCodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
    end
    object QrPropNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrPropUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPropCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
    end
  end
  object DsProp: TDataSource
    DataSet = QrProp
    Left = 88
    Top = 12
  end
  object QrParc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT blp.Codigo'
      'FROM bloqparc blp'
      'LEFT JOIN condimov cim ON cim.Conta=blp.CodigoEsp'
      'LEFT JOIN entidades ent ON ent.Codigo=blp.CodigoEnt'
      'LEFT JOIN enticliint eci ON eci.CodEnti=blp.CodCliEnt'
      'WHERE eci.TipoTabLct=0'
      'AND cim.Codigo=:P0'
      'AND cim.Propriet=:P1'
      'AND cim.Conta=:P2'
      'ORDER BY blp.Codigo DESC')
    Left = 116
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrParcCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
  end
  object DsParc: TDataSource
    DataSet = QrParc
    Left = 144
    Top = 12
  end
end
