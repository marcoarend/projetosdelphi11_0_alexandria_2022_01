object FmCondGri: TFmCondGri
  Left = 368
  Top = 194
  Caption = 'GER-PROPR-002 :: Grupos de Unidades Habitacionais'
  ClientHeight = 471
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelItens: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 375
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Panel8: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 196
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 52
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label7: TLabel
          Left = 16
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = EdDBCodigo
        end
        object Label8: TLabel
          Left = 68
          Top = 8
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = EdDBNome
        end
        object EdDBCodigo: TDBEdit
          Left = 16
          Top = 24
          Width = 48
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsCondGri
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object EdDBNome: TDBEdit
          Left = 68
          Top = 24
          Width = 705
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsCondGri
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
      end
      object Panel10: TPanel
        Left = 0
        Top = 52
        Width = 792
        Height = 92
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label11: TLabel
          Left = 16
          Top = 4
          Width = 60
          Height = 13
          Caption = 'Condom'#237'nio:'
        end
        object Label12: TLabel
          Left = 16
          Top = 44
          Width = 140
          Height = 13
          Caption = 'Propriet'#225'rio [F4 mostra todos]:'
        end
        object Label13: TLabel
          Left = 620
          Top = 44
          Width = 103
          Height = 13
          Caption = 'Unidade habitacional:'
        end
        object EdEmpresa: TdmkEditCB
          Left = 16
          Top = 20
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 72
          Top = 20
          Width = 701
          Height = 21
          Color = clWhite
          KeyField = 'CodCond'
          ListField = 'NOMECOND'
          ListSource = DsEntiCond
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPropriet: TdmkEditCB
          Left = 16
          Top = 60
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdProprietChange
          OnKeyDown = EdProprietKeyDown
          DBLookupComboBox = CBPropriet
          IgnoraDBLookupComboBox = False
        end
        object EdCondImov: TdmkEditCB
          Left = 620
          Top = 60
          Width = 53
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCondImov
          IgnoraDBLookupComboBox = False
        end
        object CBCondImov: TdmkDBLookupComboBox
          Left = 676
          Top = 60
          Width = 100
          Height = 21
          Color = clWhite
          KeyField = 'Conta'
          ListField = 'Unidade'
          ListSource = DsCondImov
          TabOrder = 5
          OnKeyDown = CBCondImovKeyDown
          dmkEditCB = EdCondImov
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBPropriet: TdmkDBLookupComboBox
          Left = 72
          Top = 60
          Width = 545
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEPROP'
          ListSource = DsPropriet
          TabOrder = 3
          OnKeyDown = CBProprietKeyDown
          dmkEditCB = EdPropriet
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 305
      Width = 792
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel12: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn2Click
          end
        end
        object BitBtn1: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 375
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 196
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 68
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 500
        Top = 8
        Width = 62
        Height = 13
        Caption = 'Usu'#225'rio web:'
      end
      object Label4: TLabel
        Left = 624
        Top = 8
        Width = 57
        Height = 13
        Caption = 'Senha web:'
      end
      object Label14: TLabel
        Left = 16
        Top = 53
        Width = 259
        Height = 13
        Caption = 'E-mail (para que o usu'#225'rio possa recuperar sua senha):'
      end
      object SpeedButton5: TSpeedButton
        Left = 475
        Top = 111
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label16: TLabel
        Left = 16
        Top = 95
        Width = 111
        Height = 13
        Caption = 'Perfil de senha s'#237'ndico:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 48
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 24
        Width = 428
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdUsername: TdmkEdit
        Left = 500
        Top = 24
        Width = 120
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPassword: TdmkEdit
        Left = 624
        Top = 24
        Width = 120
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkAtivo: TdmkCheckBox
        Left = 16
        Top = 139
        Width = 55
        Height = 17
        Caption = 'Ativo'
        TabOrder = 7
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdEMail: TdmkEdit
        Left = 16
        Top = 69
        Width = 480
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'EMail'
        UpdCampo = 'EMail'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBPerfil: TdmkDBLookupComboBox
        Left = 72
        Top = 111
        Width = 397
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsWPerfil
        TabOrder = 6
        dmkEditCB = EdPerfil
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPerfil: TdmkEditCB
        Left = 15
        Top = 111
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPerfil
        IgnoraDBLookupComboBox = False
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 305
      Width = 792
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel11: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 375
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 120
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 68
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 540
        Top = 8
        Width = 62
        Height = 13
        Caption = 'Usu'#225'rio web:'
      end
      object Label6: TLabel
        Left = 664
        Top = 8
        Width = 57
        Height = 13
        Caption = 'Senha web:'
      end
      object Label15: TLabel
        Left = 16
        Top = 50
        Width = 259
        Height = 13
        Caption = 'E-mail (para que o usu'#225'rio possa recuperar sua senha):'
        FocusControl = DBEdit3
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 48
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCondGri
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 68
        Top = 24
        Width = 469
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsCondGri
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 540
        Top = 24
        Width = 120
        Height = 21
        DataField = 'Username'
        DataSource = DsCondGri
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 664
        Top = 24
        Width = 120
        Height = 21
        DataField = 'Password'
        DataSource = DsCondGri
        TabOrder = 3
      end
      object DBCheckBox2: TDBCheckBox
        Left = 16
        Top = 90
        Width = 55
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsCondGri
        TabOrder = 5
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 66
        Width = 521
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Email'
        DataSource = DsCondGri
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 0
      Top = 191
      Width = 792
      Height = 120
      Align = alBottom
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECOND'
          Title.Caption = 'Condom'#237'nio'
          Width = 400
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UNIDADE'
          Title.Caption = 'U.H.'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPROP'
          Title.Caption = 'Propriet'#225'rio'
          Width = 283
          Visible = True
        end>
      Color = clWindow
      DataSource = DsCondGriImv
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECOND'
          Title.Caption = 'Condom'#237'nio'
          Width = 400
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UNIDADE'
          Title.Caption = 'U.H.'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPROP'
          Title.Caption = 'Propriet'#225'rio'
          Width = 283
          Visible = True
        end>
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 311
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 95
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtGrupo: TBitBtn
          Tag = 280
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtGrupoClick
        end
        object BtUH: TBitBtn
          Tag = 10021
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&UH'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtUHClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 1
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 420
        Height = 32
        Caption = 'Grupos de Unidades Habitacionais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 420
        Height = 32
        Caption = 'Grupos de Unidades Habitacionais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 420
        Height = 32
        Caption = 'Grupos de Unidades Habitacionais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 2
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsCondGri: TDataSource
    DataSet = QrCondGri
    Left = 244
    Top = 261
  end
  object QrCondGri: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCondGriBeforeOpen
    AfterOpen = QrCondGriAfterOpen
    AfterScroll = QrCondGriAfterScroll
    SQL.Strings = (
      'SELECT * FROM condgri'
      'WHERE Codigo > 0')
    Left = 216
    Top = 261
    object QrCondGriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondGriNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCondGriUsername: TWideStringField
      FieldName = 'Username'
      Size = 32
    end
    object QrCondGriPassword: TWideStringField
      FieldName = 'Password'
      Size = 32
    end
    object QrCondGriLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCondGriDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCondGriDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCondGriUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCondGriUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCondGriAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCondGriAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCondGriEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
  end
  object PMGrupo: TPopupMenu
    OnPopup = PMGrupoPopup
    Left = 340
    Top = 256
    object Crianovogrupo1: TMenuItem
      Caption = '&Cria novo grupo'
      OnClick = Crianovogrupo1Click
    end
    object Alteragrupoatual1: TMenuItem
      Caption = '&Altera grupo atual'
      OnClick = Alteragrupoatual1Click
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 685
    Top = 269
  end
  object QrCondImov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cim.Conta, cim.Unidade'
      'FROM condimov cim'
      'WHERE cim.Codigo<>0'
      'AND cim.Propriet<>0'
      'ORDER BY cim.Unidade'
      '')
    Left = 657
    Top = 269
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
  end
  object DsPropriet: TDataSource
    DataSet = QrPropriet
    Left = 629
    Top = 269
  end
  object QrPropriet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP '
      'FROM entidades ent'
      'LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet'
      'WHERE ent.Cliente2="V"'
      'AND cim.Codigo<>0'
      'ORDER BY NOMEPROP')
    Left = 601
    Top = 269
    object QrProprietCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrProprietNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Origin = 'NOMEPROP'
      Required = True
      Size = 100
    end
  end
  object DsEntiCond: TDataSource
    DataSet = QrEntiCond
    Left = 573
    Top = 269
  end
  object QrEntiCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cnd.Codigo CodCond, ent.Codigo CodEnti,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECOND '
      'FROM cond cnd '
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMECOND'
      '')
    Left = 545
    Top = 269
    object QrEntiCondCodCond: TIntegerField
      FieldName = 'CodCond'
      Origin = 'cond.Codigo'
    end
    object QrEntiCondCodEnti: TIntegerField
      FieldName = 'CodEnti'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEntiCondNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Origin = 'NOMECOND'
      Required = True
      Size = 100
    end
  end
  object PMUH: TPopupMenu
    OnPopup = PMUHPopup
    Left = 452
    Top = 260
    object AdicionaUHaogrupo1: TMenuItem
      Caption = '&Adiciona UH ao grupo'
      OnClick = AdicionaUHaogrupo1Click
    end
    object RetiraUHselecionadadogrupo1: TMenuItem
      Caption = '&Retira UH selecionada do grupo'
      OnClick = RetiraUHselecionadadogrupo1Click
    end
  end
  object QrCondGriImv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(cii.Propr=0,"T O D O S", '
      '  IF(pro.Tipo=0, pro.RazaoSocial, pro.Nome)) NOMEPROP,'
      'IF(cii.Cond=0,"T O D O S", '
      '  IF(con.Tipo=0, con.RazaoSocial, con.Nome)) NOMECOND,'
      'IF(cii.Apto=0,"TODAS", '
      '  imv.Unidade) UNIDADE, cii.* '
      'FROM condgriimv cii'
      'LEFT JOIN condimov  imv ON imv.Conta=cii.Apto'
      'LEFT JOIN cond      cnd ON cnd.Codigo=cii.Cond'
      'LEFT JOIN entidades con ON con.Codigo=cnd.Cliente'
      'LEFT JOIN entidades pro ON pro.Codigo=cii.Propr'
      'WHERE cii.Codigo=:P0')
    Left = 86
    Top = 290
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondGriImvNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrCondGriImvNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
    object QrCondGriImvUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object QrCondGriImvCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCondGriImvControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCondGriImvCond: TIntegerField
      FieldName = 'Cond'
      Required = True
    end
    object QrCondGriImvApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrCondGriImvPropr: TIntegerField
      FieldName = 'Propr'
      Required = True
    end
    object QrCondGriImvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCondGriImvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCondGriImvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCondGriImvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCondGriImvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCondGriImvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCondGriImvAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsCondGriImv: TDataSource
    DataSet = QrCondGriImv
    Left = 114
    Top = 290
  end
  object DsWPerfil: TDataSource
    DataSet = QrWPerfil
    Left = 728
    Top = 336
  end
  object QrWPerfil: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 700
    Top = 336
    object QrWPerfilCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWPerfilNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
