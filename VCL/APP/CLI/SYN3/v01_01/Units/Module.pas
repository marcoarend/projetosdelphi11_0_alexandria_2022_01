unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, UnMLAGeral, UMySQLModule, mySQLDbTables,
  UnGOTOy, Winsock, MySQLBatch, frxClass, frxDBSet, comctrls, DBCtrls,
  UnFinanceiro, dmkGeral, StdCtrls, ExtCtrls, ABSMain, DBGrids, TypInfo,
  dmkRadioGroup, dmkDBGrid, dmkDBGridDAC, DmkDAC_PF, UnDmkEnums, UnGrl_Vars;

type
  TDmod = class(TDataModule)
    QrFields: TMySQLQuery;
    QrMaster: TMySQLQuery;
    QrLivreY_D: TmySQLQuery;
    QrRecCountX: TMySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrUser: TMySQLQuery;
    QrSelX: TMySQLQuery;
    QrSelXLk: TIntegerField;
    QrSB: TMySQLQuery;
    QrSBCodigo: TIntegerField;
    QrSBNome: TWideStringField;
    DsSB: TDataSource;
    QrSB2: TMySQLQuery;
    QrSB2Codigo: TFloatField;
    QrSB2Nome: TWideStringField;
    DsSB2: TDataSource;
    QrSB3: TMySQLQuery;
    QrSB3Codigo: TDateField;
    QrSB3Nome: TWideStringField;
    DsSB3: TDataSource;
    QrDuplicIntX: TMySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrDuplicStrX: TMySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrInsLogX: TMySQLQuery;
    QrDelLogX: TMySQLQuery;
    QrDataBalY: TmySQLQuery;
    QrDataBalYData: TDateField;
    QrSenha: TMySQLQuery;
    QrSenhaLogin: TWideStringField;
    QrSenhaNumero: TIntegerField;
    QrSenhaSenha: TWideStringField;
    QrSenhaPerfil: TIntegerField;
    QrSenhaLk: TIntegerField;
    QrUserLogin: TWideStringField;
    QrMaster2: TMySQLQuery;
    QrProduto: TMySQLQuery;
    QrVendas: TMySQLQuery;
    QrEntrada: TMySQLQuery;
    QrEntradaPecas: TFloatField;
    QrEntradaValor: TFloatField;
    QrVendasPecas: TFloatField;
    QrVendasValor: TFloatField;
    QrUpdM: TmySQLQuery;
    QrUpdU: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrUpdY: TmySQLQuery;
    QrLivreY: TmySQLQuery;
    QrLivreYCodigo: TIntegerField;
    QrMaster2Em: TWideStringField;
    QrMaster2CNPJ: TWideStringField;
    QrMaster2Monitorar: TSmallintField;
    QrMaster2Distorcao: TIntegerField;
    QrMaster2DataI: TDateField;
    QrMaster2DataF: TDateField;
    QrMaster2Hoje: TDateField;
    QrMaster2Hora: TTimeField;
    QrMaster2MasLogin: TWideStringField;
    QrMaster2MasSenha: TWideStringField;
    QrMaster2MasAtivar: TWideStringField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrLocDataY: TmySQLQuery;
    QrLocDataYRecord: TDateField;
    QrLivreY_DCodigo: TLargeintField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    MyDB: TmySQLDatabase;
    MyLocDatabase: TmySQLDatabase;
    QrSB4: TmySQLQuery;
    DsSB4: TDataSource;
    QrSB4Codigo: TWideStringField;
    QrSB4Nome: TWideStringField;
    QrPriorNext: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrSoma1: TmySQLQuery;
    QrSoma1TOTAL: TFloatField;
    QrSoma1Qtde: TFloatField;
    QrEstoque: TmySQLQuery;
    QrEstoqueTipo: TSmallintField;
    QrEstoqueQtde: TFloatField;
    QrEstoqueValorCus: TFloatField;
    QrPeriodoBal: TmySQLQuery;
    QrPeriodoBalPeriodo: TIntegerField;
    QrMin: TmySQLQuery;
    QrMinPeriodo: TIntegerField;
    QrBalancosIts: TmySQLQuery;
    QrBalancosItsEstqQ: TFloatField;
    QrBalancosItsEstqV: TFloatField;
    QrEstqperiodo: TmySQLQuery;
    QrBalancosItsEstqQ_G: TFloatField;
    QrBalancosItsEstqV_G: TFloatField;
    QrEstqperiodoTipo: TSmallintField;
    QrEstqperiodoQtde: TFloatField;
    QrEstqperiodoValorCus: TFloatField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrProdutoCodigo: TIntegerField;
    QrProdutoControla: TWideStringField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QlLocal: TMySQLBatchExecute;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrEndereco: TmySQLQuery;
    QrEnderecoCodigo: TIntegerField;
    QrEnderecoCadastro: TDateField;
    QrEnderecoNOMEDONO: TWideStringField;
    QrEnderecoCNPJ_CPF: TWideStringField;
    QrEnderecoIE_RG: TWideStringField;
    QrEnderecoNIRE_: TWideStringField;
    QrEnderecoRUA: TWideStringField;
    QrEnderecoNUMERO: TLargeintField;
    QrEnderecoCOMPL: TWideStringField;
    QrEnderecoBAIRRO: TWideStringField;
    QrEnderecoCIDADE: TWideStringField;
    QrEnderecoNOMELOGRAD: TWideStringField;
    QrEnderecoNOMEUF: TWideStringField;
    QrEnderecoPais: TWideStringField;
    QrEnderecoLograd: TLargeintField;
    QrEnderecoTipo: TSmallintField;
    QrEnderecoCEP: TLargeintField;
    QrEnderecoTE1: TWideStringField;
    QrEnderecoFAX: TWideStringField;
    QrEnderecoENatal: TDateField;
    QrEnderecoPNatal: TDateField;
    QrEnderecoECEP_TXT: TWideStringField;
    QrEnderecoNUMERO_TXT: TWideStringField;
    QrEnderecoE_ALL: TWideStringField;
    QrEnderecoCNPJ_TXT: TWideStringField;
    QrEnderecoFAX_TXT: TWideStringField;
    QrEnderecoTE1_TXT: TWideStringField;
    QrEnderecoNATAL_TXT: TWideStringField;
    QrEnderecoRespons1: TWideStringField;
    QrAgoraAGORA: TDateTimeField;
    QrBoss: TmySQLQuery;
    QrBossMasSenha: TWideStringField;
    QrBossMasLogin: TWideStringField;
    QrBossEm: TWideStringField;
    QrBossCNPJ: TWideStringField;
    QrBossMasZero: TWideStringField;
    QrBSit: TmySQLQuery;
    QrBSitSitSenha: TSmallintField;
    QrSenhas: TmySQLQuery;
    QrSenhasSenhaNew: TWideStringField;
    QrSenhasSenhaOld: TWideStringField;
    QrSenhaslogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasPerfil: TIntegerField;
    QrSSit: TmySQLQuery;
    QrSSitSitSenha: TSmallintField;
    QrMaster2Limite: TSmallintField;
    QrMaster2Licenca: TWideStringField;
    QrTerminaisLicenca: TWideStringField;
    QrControle: TmySQLQuery;
    QrSenhasFuncionario: TIntegerField;
    QrLocCtrl: TmySQLQuery;
    QrLocCtrlCartConcilia: TIntegerField;
    QrUpd2: TmySQLQuery;
    frxDsControle: TfrxDBDataset;
    QrDono: TmySQLQuery;
    QrDonoCodigo: TIntegerField;
    QrDonoCadastro: TDateField;
    QrDonoNOMEDONO: TWideStringField;
    QrDonoCNPJ_CPF: TWideStringField;
    QrDonoIE_RG: TWideStringField;
    QrDonoNIRE_: TWideStringField;
    QrDonoRUA: TWideStringField;
    QrDonoCOMPL: TWideStringField;
    QrDonoBAIRRO: TWideStringField;
    QrDonoCIDADE: TWideStringField;
    QrDonoNOMELOGRAD: TWideStringField;
    QrDonoNOMEUF: TWideStringField;
    QrDonoPais: TWideStringField;
    QrDonoTipo: TSmallintField;
    QrDonoTE1: TWideStringField;
    QrDonoFAX: TWideStringField;
    QrDonoENatal: TDateField;
    QrDonoPNatal: TDateField;
    QrDonoECEP_TXT: TWideStringField;
    QrDonoNUMERO_TXT: TWideStringField;
    QrDonoCNPJ_TXT: TWideStringField;
    QrDonoFAX_TXT: TWideStringField;
    QrDonoTE1_TXT: TWideStringField;
    QrDonoNATAL_TXT: TWideStringField;
    QrDonoE_LNR: TWideStringField;
    QrDonoE_CUC: TWideStringField;
    QrDonoEContato: TWideStringField;
    QrDonoECel: TWideStringField;
    QrDonoCEL_TXT: TWideStringField;
    QrDonoE_LN2: TWideStringField;
    QrDonoAPELIDO: TWideStringField;
    QrDonoNUMERO: TFloatField;
    frxDsDono: TfrxDBDataset;
    QrUpdZ: TmySQLQuery;
    frxDsEndereco: TfrxDBDataset;
    QrWeb: TmySQLQuery;
    MyDBn: TmySQLDatabase;
    QrUpdN: TmySQLQuery;
    frxDsMaster: TfrxDBDataset;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterSolicitaSenha: TSmallintField;
    QrDonoRespons1: TWideStringField;
    QrDonoRespons2: TWideStringField;
    QrMasterEm: TWideStringField;
    QrUpdW: TmySQLQuery;
    QrRecebiBloq: TmySQLQuery;
    QrRecebiBloqApto: TIntegerField;
    QrRecebiBloqProtoco: TIntegerField;
    QrRecebiBloqBloqueto: TIntegerField;
    QrRecebiBloqEmeio: TIntegerField;
    QrRecebiBloqEnvios: TIntegerField;
    QrRecebiBloqDataHora: TDateTimeField;
    QrRecebiBloqAlterWeb: TSmallintField;
    QrRecebiBloqAtivo: TSmallintField;
    QrRecebiBloqSincro: TSmallintField;
    DsRecebiBloq: TDataSource;
    QrEmeioProt: TmySQLQuery;
    QrEmeioProtCodigo: TIntegerField;
    QrEmeioProtNome: TWideStringField;
    DsEmeioProt: TDataSource;
    QrEmeioProtPak: TmySQLQuery;
    DsEmeioProtPak: TDataSource;
    QrEmeioProtPakIts: TmySQLQuery;
    DsEmeioProtPakIts: TDataSource;
    QrEmeioProtPakItsConta: TIntegerField;
    QrEmeioProtPakItsDataE: TDateField;
    QrEmeioProtPakItsCliente: TIntegerField;
    QrEmeioProtPakItsDepto: TIntegerField;
    QrEmeioProtPakItsDocum: TFloatField;
    QrEmeioProtPakControle: TIntegerField;
    QrEmeioProtPakMez: TIntegerField;
    QrEmeioProtPakItsNOMECLI: TWideStringField;
    QrEmeioProtPakItsUnidade: TWideStringField;
    QrEmeioProtPakItsDATAE_TXT: TWideStringField;
    QrEmeioProtPakNOMEMEZ: TWideStringField;
    QrEmeioProtTot: TmySQLQuery;
    DsEmeioProtTot: TDataSource;
    QrEmeioProtTotITENS: TLargeintField;
    QrEmeioProtITENS: TLargeintField;
    QrEmeioProtPakITENS: TLargeintField;
    QrCondEmeios: TmySQLQuery;
    QrCondEmeiosCodigo: TIntegerField;
    QrCondEmeiosControle: TIntegerField;
    QrCondEmeiosConta: TIntegerField;
    QrCondEmeiosItem: TIntegerField;
    QrCondEmeiosPronome: TWideStringField;
    QrCondEmeiosNome: TWideStringField;
    QrCondEmeiosEMeio: TWideStringField;
    QrCondEmeiosNOMECOND: TWideStringField;
    QrPPIMail: TmySQLQuery;
    QrPPIMailPROTOCOLO: TIntegerField;
    QrPPIMailLOTE: TIntegerField;
    QrPPIMailDataE: TDateField;
    QrPPIMailDATAE_TXT: TWideStringField;
    QrPPIMailDATAD_TXT: TWideStringField;
    QrPPIMailCancelado: TIntegerField;
    QrPPIMailMotivo: TIntegerField;
    QrPPIMailID_Cod1: TIntegerField;
    QrPPIMailID_Cod2: TIntegerField;
    QrPPIMailID_Cod3: TIntegerField;
    QrPPIMailID_Cod4: TIntegerField;
    QrPPIMailCliInt: TIntegerField;
    QrPPIMailCliente: TIntegerField;
    QrPPIMailPeriodo: TIntegerField;
    QrPPIMailDef_Sender: TIntegerField;
    QrPPIMailDELIVER: TWideStringField;
    QrPPIMailDocum: TFloatField;
    QrPPIMailPreEmeio: TIntegerField;
    QrPPIMailDataD: TDateTimeField;
    QrPPIMailTAREFA_COD: TIntegerField;
    QrPPIMailTAREFA_NOM: TWideStringField;
    QrEmeioProtPakItsID_Cod1: TIntegerField;
    QrEmeioProtPakItsID_Cod2: TIntegerField;
    QrEmeioProtPakItsID_Cod3: TIntegerField;
    QrEmeioProtPakItsID_Cod4: TIntegerField;
    QrBolArr: TmySQLQuery;
    QrBolArrValor: TFloatField;
    QrBolArrApto: TIntegerField;
    QrBolArrBOLAPTO: TWideStringField;
    QrBolArrBoleto: TFloatField;
    QrBolLei: TmySQLQuery;
    QrBolLeiValor: TFloatField;
    QrBolLeiApto: TIntegerField;
    QrBolLeiBOLAPTO: TWideStringField;
    QrBolLeiBoleto: TFloatField;
    QrBolMail: TmySQLQuery;
    QrBolMailPROTOCOD: TIntegerField;
    QrBolMailApto: TIntegerField;
    QrBolMailPropriet: TIntegerField;
    QrBolMailVencto: TDateField;
    QrBolMailUnidade: TWideStringField;
    QrBolMailNOMEPROPRIET: TWideStringField;
    QrBolMailBOLAPTO: TWideStringField;
    QrBolMailBoleto: TFloatField;
    QrBolMailSUB_ARR: TFloatField;
    QrBolMailSUB_LEI: TFloatField;
    QrBolMailSUB_TOT: TFloatField;
    QrDonoEMail: TWideStringField;
    QrSenhasIP_Default: TWideStringField;
    QrProtPg: TmySQLQuery;
    QrProtPgRegistros: TLargeintField;
    QrEmeioProtPakItsVencto: TDateField;
    QrProtVct: TmySQLQuery;
    QrEmeioProtPakItsLancto: TIntegerField;
    QrReenv: TmySQLQuery;
    QrReenvNOMECLI: TWideStringField;
    QrReenvUnidade: TWideStringField;
    QrReenvDATAE_TXT: TWideStringField;
    QrReenvCodigo: TIntegerField;
    QrReenvControle: TIntegerField;
    QrReenvConta: TIntegerField;
    QrReenvDataE: TDateField;
    QrReenvDataD: TDateTimeField;
    QrReenvRetorna: TSmallintField;
    QrReenvCancelado: TIntegerField;
    QrReenvMotivo: TIntegerField;
    QrReenvLink_ID: TIntegerField;
    QrReenvCliInt: TIntegerField;
    QrReenvCliente: TIntegerField;
    QrReenvFornece: TIntegerField;
    QrReenvPeriodo: TIntegerField;
    QrReenvLancto: TIntegerField;
    QrReenvDocum: TFloatField;
    QrReenvDepto: TIntegerField;
    QrReenvID_Cod1: TIntegerField;
    QrReenvID_Cod2: TIntegerField;
    QrReenvID_Cod3: TIntegerField;
    QrReenvID_Cod4: TIntegerField;
    QrReenvCedente: TIntegerField;
    QrReenvVencto: TDateField;
    QrReenvValor: TFloatField;
    QrReenvMoraDiaVal: TFloatField;
    QrReenvMultaVal: TFloatField;
    QrReenvComoConf: TSmallintField;
    QrReenvSerieCH: TWideStringField;
    QrReenvManual: TIntegerField;
    QrReenvTexto: TWideStringField;
    QrReenvApto: TIntegerField;
    QrEnderecoNO_TIPO_DOC: TWideStringField;
    QrProtoMail_: TmySQLQuery;
    QrProtoMail_Depto_Cod: TIntegerField;
    QrProtoMail_Depto_Txt: TWideStringField;
    QrProtoMail_Entid_Cod: TIntegerField;
    QrProtoMail_Entid_Txt: TWideStringField;
    QrProtoMail_Taref_Cod: TIntegerField;
    QrProtoMail_Taref_Txt: TWideStringField;
    QrProtoMail_Deliv_Cod: TIntegerField;
    QrProtoMail_Deliv_Txt: TWideStringField;
    QrProtoMail_DataE: TDateField;
    QrProtoMail_DataE_Txt: TWideStringField;
    QrProtoMail_DataD: TDateField;
    QrProtoMail_DataD_Txt: TWideStringField;
    QrProtoMail_ProtoLote: TIntegerField;
    QrProtoMail_NivelEmail: TSmallintField;
    QrProtoMail_Item: TIntegerField;
    QrProtoMail_NivelDescr: TWideStringField;
    QrProtoMail_Protocolo: TIntegerField;
    QrProtoMail_RecipEmeio: TWideStringField;
    QrProtoMail_RecipNome: TWideStringField;
    QrProtoMail_RecipProno: TWideStringField;
    QrProtoMail_RecipItem: TIntegerField;
    QrProtoMail_Bloqueto: TFloatField;
    QrProtoMail_Vencimento: TDateField;
    QrProtoMail_Valor: TFloatField;
    QrProtoMail_PreEmeio: TIntegerField;
    QrProtoMail_Condominio: TWideStringField;
    QrProtoMail_IDEmeio: TIntegerField;
    QrMasterUsaAccMngr: TSmallintField;
    QrUserTxts: TmySQLQuery;
    QrUserTxtsTam: TLargeintField;
    QrUserTxtsCodigo: TIntegerField;
    QrUserTxtsTxtSys: TWideStringField;
    QrUserTxtsTxtMeu: TWideStringField;
    QrUserTxtsOrdem: TIntegerField;
    QrDonoLograd: TFloatField;
    QrDonoCEP: TFloatField;
    QrCB4Data4: TmySQLQuery;
    QrCB4Data4Registro: TIntegerField;
    QrEmeioProtPakItsCODCOND: TIntegerField;
    QrSomaM: TmySQLQuery;
    QrSomaMValor: TFloatField;
    QrAuxN: TmySQLQuery;
    QrNotifi: TmySQLQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrEnderecoCalcFields(DataSet: TDataSet);
    procedure QrDonoCalcFields(DataSet: TDataSet);
    procedure QrControleAfterOpen(DataSet: TDataSet);
    procedure QrEmeioProtAfterScroll(DataSet: TDataSet);
    procedure QrEmeioProtBeforeClose(DataSet: TDataSet);
    procedure QrEmeioProtPakAfterScroll(DataSet: TDataSet);
    procedure QrEmeioProtPakBeforeClose(DataSet: TDataSet);
    procedure QrBolMailCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FProdDelete: Integer;
    function Privilegios(Usuario : Integer) : Boolean;
    procedure VerificaSenha(Index: Integer; Login, Senha: String);
    procedure ReabreProtoMeioOpen();
    // Reenvio de emeio
{###
    procedure ReopenBolMail(ID_Cod1, ID_Cod2, Periodo: Integer);
    procedure VerificaEntregaPorEmail(ID_Cod1, ID_Cod2, Periodo, PreEmeio:
              Integer; PB1: TProgressBar);
}
    procedure EmailBloqueto(PB: TProgressBar; PrevCodigo: Integer;
              Antigo: Boolean);
    //
    procedure InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
              DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
              CliInt: Integer; Campos: String; DescrSubstit: Boolean);
    procedure VerificaReenvioEmailAvisoRecebBloqueto(PreEmeio: Integer;
              PB1: TProgressBar);
    function  InsereItem_ProtoMail(
              RecipEMeio, RecipNome, RecipProno: String;
              RecipItem: Integer;
              Boleto: Double;
              Apto: Integer; Unidade: String;
              Propriet: Integer;
              NOMEPROPRIET: String;
              PROTOCOD: Integer;
              TAREFA: String;
              DEF_SENDER: Integer;
              DELIVER: String;
              DATAE: TDateTime; DATAE_TXT: String;
              DATAD: TDateTime; DATAD_TXT: String;
              LOTE, PROTOCOLO, Nivel, Item: Integer;
              Vencimento: TDateTime; Valor: Double;
              Condominio: String; PreEmeio, IDEmeio: Integer): Boolean;
    function  TabelasQueNaoQueroCriar(): String;
    procedure ReopenControle();
    function  ReopenCB4Data4(Empresa, Tabela: Integer): Boolean;
    function  ObtemProximoCB4Data4Registro(Empresa, Tabela: Integer): Integer;
    function ObtemSindicoCond(CliInt: Integer): String;
    procedure AtzListaMailProt_WebDwn(LaAviso1, LaAviso2: TLabel; PB: TProgressBar);
    procedure AtzListaMailProt_LocPgt(LaAviso1, LaAviso2: TLabel; PB: TProgressBar; Memo: TMemo);
    procedure AtzListaMailProt_WebSin(LaAviso1, LaAviso2: TLabel; PB: TProgressBar; Memo: TMemo);
    procedure AtzListaMailProt_WebCln(LaAviso1, LaAviso2: TLabel; PB: TProgressBar; Memo: TMemo);
    procedure ReopenOpcoesSynd();
    procedure Teste_ListaCondominios(QrCond: TmySQLQuery; Memo: TMemo;
              QrCondCodigo: TIntegerField; QrCondNO_COND: TWideStringField);
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
    function  ObtemDiretorioFTPBalancete(): Integer;
    function  ValidaUsuarioWeb(const Usuario: String; var Tipo, Codigo: Integer): Boolean;
    function  LocalizaUsuarioWeb(Query: TmySQLQuery; DataBase: TmySQLDatabase;
              Usuario: String; var Tipo, ID: Integer): Boolean;
    procedure ReopenParamsEspecificos(Empresa: Integer);
  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;

implementation

uses Principal, SenhaBoss, Servidor, VerifiDB, MyDBCheck, VerifiDBTerceiros,
  InstallMySQL41, Syndic_MLA, MyListas, VerifiDBi, UCreate, EmailBloqueto,
  ModuleGeral, UnMyObjects, ModuleFin, UnLic_Dmk, UnDmkWeb;

{$R *.DFM}

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  Versao, VerZero, Resp: Integer;
  BD: String;
  Verifica, VerificaDBTerc: Boolean;
begin
  VerificaDBTerc := False;
  //
  MyDB.LoginPrompt := False;
  ZZDB.LoginPrompt := False;
  MyLocDatabase.LoginPrompt := False;
  //
  if MyDB.Connected then
  begin
    MyDB.Disconnect;
    Geral.MensagemBox('MyDB est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  end;
  if MyLocDataBase.Connected then
  begin
    MyLocDataBase.Disconnect;
    Geral.MensagemBox('MyLocDataBase est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  end;
  if MyDBn.Connected then
  begin
    MyDBn.Disconnect;
    Geral.MensagemBox('MyDBn est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  end;
  if ZZDB.Connected then
  begin
    ZZDB.Disconnect;
    Geral.MensagemBox('ZZDB est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  end;
  //
  VAR_BDSENHA := 'wkljweryhvbirt';
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  if not GOTOy.OMySQLEstaInstalado(
  FmSyndic_MLA.LaAviso1, FmSyndic_MLA.LaAviso2, FmSyndic_MLA.ProgressBar1) then
  begin
    //raise EAbort.Create('');
    //
    MyObjects.Informa2(FmSyndic_MLA.LaAviso1, FmSyndic_MLA.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    FmSyndic_MLA.BtEntra.Visible := False;
    //
    Exit;
  end;
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  ZZDB.UserPassword := VAR_BDSENHA;
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE user SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then ShowMessage('Banco de dados teste n�o se conecta!');
    end;
  end;
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  //VAR_SQLy := TStringList.Create;
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  //MAR_SQLy := TStringList.Create;
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MensagemBox('M�quina cliente sem rede.', 'Erro', MB_OK+MB_ICONERROR);
        //Application.Terminate;
      end;
    end;
  except
    Geral.MensagemBox('Teste', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
{
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host := VAR_IP;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
}
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql',  VAR_IP,
  VAR_PORTA,  VAR_SQLUSER, VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True,
  (*Conecta*)False);
  //
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  QrAux.Open;
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  //
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MensagemBox('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?', 'Banco de Dados',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      //
      if MyDB.Connected then
        MyDB.Disconnect;
      MyDB.DatabaseName := TMeuDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MensagemBox('O aplicativo ser� encerrado!', 'Banco de Dados',
      MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
  (*
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  QrAuxL.Open;
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MensagemBox('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?',
      'Banco de Dados Local', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MensagemBox('O aplicativo ser� encerrado!',
      'Banco de Dados Local', MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  *)
  //
  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  Verifica := False;
  if Versao < CO_Versao then Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Geral.MensagemBox('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?',
    'Aus�ncia de Informa��o de Vers�o no Registro',
    MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;

  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  //FmPrincipal.VerificaTerminal;
  //MLAGeral.ConfiguracoesIniciais(0, MyDB.DatabaseName);
  // 2011-10-07
  //MLAGeral.ConfiguracoesIniciais(1, Application.Name);
  MyList.ConfiguracoesIniciais(1, Application.Name);
  // Fim 2011-10-07


  if Verifica then
  begin
    VAR_VLOCAL := True;
    if not VAR_VERIFI_DB_CANCEL then
    begin
      Application.CreateForm(TFmVerifiDB, FmVerifiDB);
      with FmVerifiDb do
      begin
        BtSair.Enabled := False;
        FVerifi := True;
        ShowModal;
        FVerifi := False;
        Destroy;
      end;
    end;
  end;
  //
  try
    QrMaster.Open;
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      if not VAR_VERIFI_DB_CANCEL then
      begin
        Application.CreateForm(TFmVerifiDB, FmVerifiDB);
        with FmVerifiDb do
        begin
          BtSair.Enabled := False;
          FVerifi := True;
          ShowModal;
          FVerifi := False;
          Destroy;
          //
          VerificaDBTerc := True;
        end;
      end;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  // Conex�o na base de dados na url
  // S� pode ser ap�s abrir a tabela controle!
  // ver se est� ativado s� ent�o fazer !
  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados geral', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;

  //

  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados financeiro', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;

  if VerificaDBTerc then
  begin
    Application.CreateForm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros);
    FmVerifiDBTerceiros.FVerifi := True;
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.FVerifi := False;
    FmVerifiDBTerceiros.Destroy;
  end;

  try
    if DModG.QrOpcoesGerl.Active = False then
      DModG.ReopenOpcoesGerl;
    //
    if DmodG.QrOpcoesGerl.FieldByName('Web_MySQL').AsInteger = 2 then
    begin
      if DmkWeb.ConexaoRemota(MyDBn, DModG.QrOpcoesGerl, 2) then
      begin
        MyDBn.Connect;
        if MyDBn.Connected and Verifica then
        begin
          if not VAR_VERIFI_DB_CANCEL then
          begin
            Application.CreateForm(TFmVerifiDBi, FmVerifiDBi);
            with FmVerifiDbi do
            begin
              BtSair.Enabled := False;
              FVerifi := True;
              ShowModal;
              FVerifi := False;
              Destroy;
            end;
          end;
        end;
      end;
    end;
  except
    Geral.MensagemBox('N�o foi poss�vel se conectar � base de dados remota!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  //
  //if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  Lic_Dmk.LiberaUso5;
  //
  FmPrincipal.AtualizaTextoBtAcessoRapido();
  //
  VAR_DB := MyDB.DatabaseName;
end;

procedure TDmod.VerificaSenha(Index: Integer; Login, Senha: String);
begin
  if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
  else
  begin
    if Index = 7 then
    begin
(*      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      QrSenha.Open;
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        QrPerfis.Open;
        if QrPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else MessageBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;                      *)
    end else ShowMessage('Em constru��o');
  end;
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  ShowMessage('CNPJ n�o definido ou Empresa n�o definida. '+Chr(13)+Chr(10)+
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value+' # '+
  QrMasterCNPJ.Value;
  //
  QrControle.Close;
  QrControle.Open;
  //
  if QrControle.FieldByName('SoMaiusculas').AsString = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControle.FieldByName('UFPadrao').AsInteger;
  VAR_CIDADEPADRAO    := QrControle.FieldByName('Cidade').AsString;
  VAR_TRAVACIDADE     := QrControle.FieldByName('TravaCidade').AsInteger;
  VAR_MOEDA           := QrControle.FieldByName('Moeda').AsString;
  //
  VAR_CARTVEN         := QrControle.FieldByName('CartVen').AsInteger;
  VAR_CARTCOM         := QrControle.FieldByName('CartCom').AsInteger;
  VAR_CARTRES         := QrControle.FieldByName('CartReS').AsInteger;
  VAR_CARTDES         := QrControle.FieldByName('CartDeS').AsInteger;
  VAR_CARTREG         := QrControle.FieldByName('CartReG').AsInteger;
  VAR_CARTDEG         := QrControle.FieldByName('CartDeG').AsInteger;
  VAR_CARTCOE         := QrControle.FieldByName('CartCoE').AsInteger;
  VAR_CARTCOC         := QrControle.FieldByName('CartCoC').AsInteger;
  VAR_CARTEMD         := QrControle.FieldByName('CartEmD').AsInteger;
  VAR_CARTEMA         := QrControle.FieldByName('CartEmA').AsInteger;
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControle.FieldByName('PaperTop').AsInteger;
  VAR_PAPERLEF        := QrControle.FieldByName('PaperLef').AsInteger;
  VAR_PAPERWID        := QrControle.FieldByName('PaperWid').AsInteger;
  VAR_PAPERHEI        := QrControle.FieldByName('PaperHei').AsInteger;
  VAR_PAPERFCL        := QrControle.FieldByName('PaperFcl').AsInteger;
  //
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
  {
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value := Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
  }
end;

procedure TDmod.AtzListaMailProt_WebSin(LaAviso1, LaAviso2: TLabel; PB: TProgressBar; Memo: TMemo);
const
  Txt1 = 'Atualizando protocolo na Web. Protocolo n� ';
var
  Qry: TmySQLQuery;
  A, B, C, Sincro, Apto, Protoco, Emeio: Integer;
  Bloqueto: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  Qry.Database := MyDB;
  //
  PB.Position := 0;
  PB.Max := QrRecebiBloq.RecordCount;
  QrRecebiBloq.First;
    A := 0;
    B := 0;
    C := 0;
  while not QrRecebiBloq.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True, Txt1 + Geral.FF0(QrRecebiBloqProtoco.Value));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ComoConf ',
    'FROM protpakits ',
    'WHERE Conta=' + Geral.FF0(QrRecebiBloqProtoco.Value),
    'AND Depto=' + Geral.FF0(QrRecebiBloqApto.Value),
    'AND Docum=' + Geral.FFI(QrRecebiBloqBloqueto.Value),
    'AND ID_Cod3=' + Geral.FF0(QrRecebiBloqEmeio.Value),
    '']);
    case Qry.RecordCount of
      0: A := A + 1;
      1: B := B + 1;
      else C := C + 1;
    end;
    if (Qry.RecordCount = 1) and (Qry.FieldByName('ComoConf').AsInteger <> 0) then
    begin
      Sincro := Qry.FieldByName('ComoConf').AsInteger;
      Apto     := QrRecebiBloqApto.Value;
      Protoco  := QrRecebiBloqProtoco.Value;
      Bloqueto := QrRecebiBloqBloqueto.Value;
      Emeio    := QrRecebiBloqEmeio.Value;
      UMyMod.SQLInsUpd(Dmod.QrWeb, stUpd, 'recebibloq', False, [
      'Sincro'], [
      'Apto', 'Protoco', 'Bloqueto', 'Emeio'], [
      Sincro], [
      Apto, Protoco, Bloqueto, Emeio], False);
    end;
    //
    QrRecebiBloq.Next;
  end;
  Qry.Close;
  Qry.Free;
  Memo.Lines.Add(
  '=============' + #13#10 +
  'recebibloq <> protpakits 0 reg. = ' + IntToStr(A) + #13#10 +
  'recebibloq <> protpakits 1 reg. = ' + IntToStr(B) + #13#10 +
  'recebibloq <> protpakits N reg. = ' + IntToStr(C));

end;

procedure TDmod.AtzListaMailProt_LocPgt(LaAviso1, LaAviso2: TLabel;
PB: TProgressBar; Memo: TMemo);
var
  Pagos, Velhos, A, B: Integer;
  Txt1, Txt2, Txt3, Txt4, TabLctA: String;
begin
  // N�o usar! Usu�rio pode abrir forms!
  //Screen.Cursor := crHourGlass;
  try
    Txt1 := 'Verificando pagos ou caducados (' +
      Geral.FF0(QrEmeioProtTotITENS.Value) + ' itens)...';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, Txt1);
    QrUpd.SQL.Clear;
    QrUpd.SQL.Add('UPDATE protpakits SET AlterWeb=1, ');
    QrUpd.SQL.Add('ComoConf=3, DataD=:P0 '); // ComoConf=3 => Pago sem avisar que recebeu
    QrUpd.SQL.Add('WHERE Conta=:P1');
    //
    QrUpdM.SQL.Clear;
    QrUpdM.SQL.Add('UPDATE protpakits SET AlterWeb=1, ');
    QrUpdM.SQL.Add('ComoConf=4, DataD=:P0 '); // ComoConf=4 => Caducado ...
    QrUpdM.SQL.Add('WHERE Conta=:P1');        // ... cfe dias na tab Controle > DdAutConfMail
    //
    Pagos  := 0;
    Velhos := 0;
    //
    PB.Position := 0;
    PB.Max := QrEmeioProtTotITENS.Value;
    //
    QrEmeioProt.First;
    while not QrEmeioProt.Eof do
    begin
      Txt2 := ' [Tarefa: ' + Geral.FFN(QrEmeioProtCodigo.Value, 4) + ']';
      QrEmeioProtPak.First;
      while not QrEmeioProtPak.Eof do
      begin
        Txt3 := ' [Lote: ' + Geral.FFN(QrEmeioProtPakControle.Value, 6) + ']';
        QrEmeioProtPakIts.First;
        while not QrEmeioProtPakIts.Eof do
        begin
          Txt4 := ' [Protocolo: ' + Geral.FFN(QrEmeioProtPakItsConta.Value, 9) + ']';
          PB.Position := PB.Position + 1;
          PB.Update;
          {
          QrProtPg.Close;
          QrProtPg.Params[00].AsInteger := QrEmeioProtPakMez.Value;
          QrProtPg.Params[01].AsInteger := QrEmeioProtPakItsCliente.Value;
          QrProtPg.Params[02].AsInteger := QrEmeioProtPakItsDepto.Value;
          QrProtPg.Params[03].AsFloat   := QrEmeioProtPakItsDocum.Value;
          QrProtPg.Open;
          }
          if QrEmeioProtPakItsID_Cod2.Value <> 0 then
          begin
            TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrEmeioProtPakItsCODCOND.Value);
            UnDmkDAC_PF.AbreMySQLQuery0(QrProtPg, Dmod.MyDB, [
            'SELECT COUNT(*) Registros ',
            'FROM ' + TabLctA,
            'WHERE Tipo=2 ',
            'AND FatID >= 600 ',
            'AND Sit = 0 ',
            'AND Compensado <= 1 ',
            'AND Mez=' + Geral.FF0(QrEmeioProtPakMez.Value),
            'AND Cliente=' + Geral.FF0(QrEmeioProtPakItsCliente.Value),
            'AND Depto=' + Geral.FF0(QrEmeioProtPakItsDepto.Value),
            'AND Documento=' + Geral.FFI(QrEmeioProtPakItsDocum.Value),
            '']);
            //
            if QrProtPgRegistros.Value = 0 then
            begin
              Pagos := Pagos + 1;
              //
              QrUpd.Params[00].AsString  := Geral.FDT(Date, 1);
              QrUpd.Params[01].AsInteger := QrEmeioProtPakItsConta.Value;
              QrUpd.ExecSQL;
            end else begin
              // eliminar antigos - que j� faz tempo que foram enviados
              A := Trunc(QrEmeioProtPakItsDataE.Value);
              B := Trunc(Date - QrControle.FieldByName('DdAutConfMail').AsInteger);
              if A < B then
              begin
                Velhos := Velhos + 1;
                //
                QrUpdM.Params[00].AsString  := Geral.FDT(Date, 1);
                QrUpdM.Params[01].AsInteger := QrEmeioProtPakItsConta.Value;
                QrUpdM.ExecSQL;
              end;
              MyObjects.Informa2(LaAviso1, LaAviso2, True, Txt1 + Txt2 + Txt3 + Txt4 +
              ' [Pagos: ' + FormatFloat('0', Pagos) +
              ' - Caducados: ' + IntToStr(Velhos) + ']');
            end;
          end else Memo.Lines.Add('Condom�nio n�o definido! ProtPakIts.Conta = ' + Geral.FF0(QrEmeioProtPakItsConta.Value));
          //
          QrEmeioProtPakIts.Next;
        end;
        QrEmeioProtPak.Next;
      end;
      QrEmeioProt.Next;
    end;
    //
    Memo.Lines.Add('=============');
    if Pagos > 0 then
      Memo.Lines.Add(IntToStr(Pagos) + ' bloquetos foram pagos sem ' +
      'sua confirma��o de recebimento por e-mail!');
    //
    if Velhos > 0 then
      Memo.Lines.Add(IntToStr(Velhos) + ' bloquetos foram considerados ' +
      'como recebidos pois j� venceram a mais de ' + IntToStr(
      QrControle.FieldByName('DdAutConfMail').AsInteger) +' dias!');
    //
  finally
    ReabreProtoMeioOpen();
    //Screen.Cursor := crDefault;
  end;
end;

procedure TDmod.AtzListaMailProt_WebCln(LaAviso1, LaAviso2: TLabel;
  PB: TProgressBar; Memo: TMemo);
const
  Txt1 = 'Verificando protocolos orf�os na WEB!';
var
  Qry: TmySQLQuery;
  Preservados, Anulados, (*A, B, C,*) Sincro, Apto, Protoco, Emeio: Integer;
  Bloqueto: Double;
begin
  Preservados := 0;
  Anulados := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, Txt1);
  //
  Qry := TmySQLQuery.Create(Dmod);
  Qry.Database := MyDBn;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, MyDBn, [
  'SELECT rec.Apto, rec.Protoco, rec.Bloqueto, rec.Emeio, ',
  'ppi.Codigo, ppi.Controle, ppi.Conta, ppi.Depto, ppi.Docum, ppi.ID_Cod3 ',
  'FROM recebibloq rec ',
  'LEFT JOIN ' + TMeuDB + '.protpakits ppi ON ppi.Conta=rec.Protoco ',
  'WHERE rec.Sincro=0 ',
  'AND rec.Protoco < ',
  '  ( ',
  '  SELECT MAX(Conta) ',
  '  FROM ' + TMeuDB + '.protpakits ',
  '  ) ',
  'ORDER BY rec.Protoco ',
  '']);
  PB.Position := 0;
  PB.Max := Qry.RecordCount;
  Qry.First;
  while not Qry.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True, Txt1 + Geral.FF0(QrRecebiBloqProtoco.Value));
    if (Qry.FieldByName('Codigo').AsInteger = 0)
    or (Qry.FieldByName('Apto').AsInteger <> Qry.FieldByName('Depto').AsInteger)
    or (Trunc(Qry.FieldByName('Bloqueto').AsFloat) <> Trunc(Qry.FieldByName('Docum').AsFloat))
    or (Qry.FieldByName('Emeio').AsInteger <> Qry.FieldByName('ID_Cod3').AsInteger) then
    begin
      Anulados := Anulados + 1;
      MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True, Txt1 +
      'Anulando protocolo ' + Geral.FF0(Qry.FieldByName('Protoco').AsInteger));
      //
      Sincro   := -1;
      Apto     := Qry.FieldByName('Apto').AsInteger;
      Protoco  := Qry.FieldByName('Protoco').AsInteger;
      Bloqueto := Qry.FieldByName('Bloqueto').AsFloat;
      Emeio    := Qry.FieldByName('Emeio').AsInteger;
      UMyMod.SQLInsUpd(Dmod.QrWeb, stUpd, 'recebibloq', False, [
      'Sincro'], [
      'Apto', 'Protoco', 'Bloqueto', 'Emeio'], [
      Sincro], [
      Apto, Protoco, Bloqueto, Emeio], False);
    end else
      Preservados := Preservados + 1;
    //
    Qry.Next;
  end;
  //
  Qry.Close;
  Qry.Free;
  Memo.Lines.Add(
  '=============' + #13#10 +
  'recebibloq anulados = ' + IntToStr(Anulados) + #13#10 +
  'recebibloq preservados = ' + IntToStr(Preservados));
end;

procedure TDmod.AtzListaMailProt_WebDwn(LaAviso1, LaAviso2: TLabel;
PB: TProgressBar);
const
  Txt1 = 'Verificando emails confirmados...';
var
  DataD, Conta: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, Txt1);
  if DmkWeb.ConexaoRemota(MyDBn, DModG.QrOpcoesGerl, 1) then
  begin
    // N�o usar! Usu�rio pode abrir forms!
    //Screen.Cursor := crHourGlass;
    try
    UnDmkDAC_PF.AbreQuery(QrRecebiBloq, MyDBn);
    //
    if QrRecebiBloq.RecordCount > 0 then
    begin
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE protpakits SET AlterWeb=1, ');
      QrUpd.SQL.Add('ComoConf=2, DataD=:P0 '); // ComoConf=2 => emeio
      QrUpd.SQL.Add('WHERE DataD <= "1900-01-01"');
      QrUpd.SQL.Add('AND Conta=:P1');
      //
      PB.Position := 0;
      PB.Max := QrRecebiBloq.RecordCount;
      QrRecebiBloq.First;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, Txt1 +
        'Novos itens confirmados por emeio:' +
        IntToStr(QrRecebiBloq.RecordCount));
      while not QrRecebiBloq.Eof do
      begin
        PB.Position := PB.Position + 1;
        PB.Update;
        Application.ProcessMessages;
        //
        DataD := Geral.FDT(QrRecebiBloqDataHora.Value, 105);
        Conta := FormatFloat('0', QrRecebiBloqProtoco.Value);
        QrUpd.Params[00].AsString  := DataD;
        QrUpd.Params[01].AsInteger := QrRecebiBloqProtoco.Value;
        QrUpd.ExecSQL;
        //
        QrRecebiBloq.Next;
      end;
    end;
    ReabreProtoMeioOpen();
    finally
      //Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  VAR_SQLx.Free;
  VAR_SQL1.Free;
  VAR_SQL2.Free;
  VAR_SQLa.Free;
  //
  //MAR_SQLy.Free;
  MAR_SQLx.Free;
  MAR_SQL1.Free;
  MAR_SQL2.Free;
  MAR_SQLa.Free;
  //
  WSACleanup;
end;

function TDmod.ObtemDiretorioFTPBalancete: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWeb, Dmod.MyDBn, [
  'SELECT DirWebBalancete ',
  'FROM wcontrol ',
  'WHERE Codigo=1',
  '']);
  if QrWeb.RecordCount > 0 then
    Result := QrWeb.FieldByName('DirWebBalancete').AsInteger
  else
    Result := 0;
end;

function TDmod.ObtemProximoCB4Data4Registro(Empresa, Tabela: Integer): Integer;
begin
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('LOCK TABLES cb4data4 WRITE');
  QrAux.ExecSQL;
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SELECT MAX(Registro) Registro FROM cb4data4');
  //QrAux.SQL.Add('WHERE Empresa=' + Geral.FF0(Empresa));
  //QrAux.SQL.Add('AND Tabela=' + Geral.FF0(Tabela));
  QrAux.Open;
  //
  Result := QrAux.FieldByName('Registro').AsInteger;
  Result := Result + 1;
  //
  QrAux.SQL.Clear;
  QrAux.SQL.Add('UPDATE cb4data4 SET Registro=' + Geral.FF0(Result));
  QrAux.SQL.Add('WHERE Empresa=' + Geral.FF0(Empresa));
  QrAux.SQL.Add('AND Tabela=' + Geral.FF0(Tabela));
  QrAux.ExecSQL;
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('UNLOCK TABLES ');
  QrAux.ExecSQL;
end;

function TDmod.ObtemSindicoCond(CliInt: Integer): String;
var
  EntiCargSind: Integer;
begin
  ReopenControle;
  //
  EntiCargSind := Dmod.QrControle.FieldByName('EntiCargSind').AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT Nome ',
  'FROM entirespon ',
  'WHERE Codigo=' + Geral.FF0(CliInt),
  'AND Cargo=' + Geral.FF0(EntiCargSind),
  'LIMIT 1 ',
  '']);
  if QrAux.RecordCount > 0 then
    Result := QrAux.FieldByName('Nome').AsString
  else
    Result := '';
end;

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  FM_MASTER := 'F';
  Dmod.QrPerfis.Params[0].AsInteger := Usuario;
  Dmod.QrPerfis.Open;
  if Dmod.QrPerfis.RecordCount > 0 then
  begin
    Result := True;
    //FM_EQUIPAMENTOS             := Dmod.QrPerfisEquipamentos.Value;
    //FM_MARCAS          := Dmod.QrPerfisMarcas.Value;
    //FM_MODELOS                := Dmod.QrPerfisModelos.Value;
    //FM_SERVICOS            := Dmod.QrPerfisServicos.Value;
    //FM_PRODUTOS            := Dmod.QrPerfisProdutos.Value;
  end;
  Dmod.QrPerfis.Close;
end;

procedure TDmod.QrEnderecoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrEnderecoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrEnderecoRua.Value, QrEnderecoNumero.Value, False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' CEP ' + Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  //
  QrEnderecoECEP_TXT.Value := Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then
  begin
    Natal := QrEnderecoENatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEnderecoPNatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CPF';
  end;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDmod.QrDonoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrDonoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoTe1.Value);
  QrDonoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoFax.Value);
  QrDonoCEL_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoECel.Value);
  QrDonoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrDonoCNPJ_CPF.Value);
  //
  QrDonoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrDonoRua.Value, Trunc(QrDonoNumero.Value), False);
  QrDonoE_LNR.Value := QrDonoNOMELOGRAD.Value;
  if Trim(QrDonoE_LNR.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ';
  QrDonoE_LNR.Value := QrDonoE_LNR.Value + QrDonoRua.Value;
  if Trim(QrDonoRua.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ', ' + QrDonoNUMERO_TXT.Value;
  if Trim(QrDonoCompl.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ' + QrDonoCompl.Value;
  if Trim(QrDonoBairro.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' - ' + QrDonoBairro.Value;
  //
  QrDonoE_LN2.Value := '';
  if Trim(QrDonoCidade.Value) <>  '' then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + ' - '+QrDonoCIDADE.Value;
  QrDonoE_LN2.Value := QrDonoE_LN2.Value + ' - '+QrDonoNOMEUF.Value;
  if QrDonoCEP.Value > 0 then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + '  CEP ' + Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  QrDonoE_CUC.Value := QrDonoE_LNR.Value + QrDonoE_LN2.Value;
  //
  QrDonoECEP_TXT.Value := Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  if QrDonoTipo.Value = 0 then Natal := QrDonoENatal.Value
  else Natal := QrDonoPNatal.Value;
  if Natal < 2 then QrDonoNATAL_TXT.Value := ''
  else QrDonoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDmod.QrBolMailCalcFields(DataSet: TDataSet);
begin
  QrBolMailSUB_TOT.Value :=
  QrBolMailSUB_ARR.Value +
  QrBolMailSUB_LEI.Value;
  //
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  QrDono.Close;
  QrDono.Open;
  //
  VAR_MEULOGOPATH := QrControle.FieldByName('MeuLogoPath').AsString;
  if FileExists(VAR_MEULOGOPATH) then
    VAR_MEULOGOEXISTE := True
  else
    VAR_MEULOGOEXISTE := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrUserTxts, MyDB, [
  'SELECT LENGTH(TxtSys) Tam, ut.* ',
  'FROM usertxts ut',
  'WHERE TxtSys <> TxtMeu',
  'ORDER BY ut.Ordem, Tam Desc',
  '']);
  //
  FmPrincipal.ReCaptionComponentesDeForm(FmPrincipal);
end;


{        saldos
SELECT car.Codigo Carteira, car.Nome NOMECART,
IF(car.Tipo <> 2, car.Inicial, 0) INICIAL,
IF(car.Tipo <> 2, SUM(lan.Credito-lan.Debito), 0) ATU_S,
IF(car.Tipo <> 2, 0,
  SUM(CASE WHEN (lan.Credito-lan.Debito+lan.Pago > 0)
  AND (lan.Sit<2)
  THEN lan.Credito-lan.Debito+lan.Pago ELSE 0 END)) FUT_C,
IF(car.Tipo <> 2, 0,
  SUM(CASE WHEN (lan.Credito-lan.Debito+lan.Pago < 0)
  AND (lan.Sit<2)
  THEN lan.Credito-lan.Debito+lan.Pago ELSE 0 END)) FUT_D,
IF(car.Tipo <> 2, 0,
  SUM(IF(lan.Sit=0, (lan.Credito-lan.Debito),
  IF(lan.Sit=1, (lan.Credito-lan.Debito+lan.Pago), 0)))) FUT_S,
IF(ca2.Codigo=0, "", ca2.Nome) NOMEBANCO
FROM carteiras car
LEFT JOIN lan�tos lan   ON car.Codigo=lan.Carteira
LEFT JOIN carteiras ca2 ON ca2.Codigo=car.Banco
WHERE car.ForneceI=1
GROUP BY car.Codigo
ORDER BY NOMECART}

procedure TDmod.ReabreProtoMeioOpen();
begin
  Screen.Cursor := crHourGlass;
  //
  QrEmeioProtTot.Close;
  QrEmeioProtTot.Open;
  //
  QrEmeioProt.Close;
  QrEmeioProt.Open;
  //
  Screen.Cursor := crDefault;
end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
var
  Saldo: Double;
begin
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM Carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM Lanctos');
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM Lanctos');
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE Carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
(*  if Localiza = 3 then
  begin
    if Dmod.QrLanctos.State in [dsBrowse] then
    if (Dmod.QrLanctosTipo.Value = Tipo)
    and (Dmod.QrLanctosCarteira.Value = Carteira) then
    Localiza := 1;
    Dmod.DefParams;
  end;
  if Localiza = 1 then GOTOx.LocalizaCodigo(Carteira, Carteira);*)
end;

function TDmod.ReopenCB4Data4(Empresa, Tabela: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCB4Data4, Dmod.MyDB, [
  'SELECT Registro ',
  'FROM cb4data4 ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND Tabela=' + Geral.FF0(Tabela),
  '']);
  //
  Result := QrCB4Data4.RecordCount > 0;
end;

procedure TDmod.ReopenControle;
begin
  QrControle.Close;
  QrControle.Open;
end;

procedure TDmod.ReopenOpcoesSynd();
begin
  //UMyMod.AbreQuery(QrOpcoesSynd, MyDB);
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  //Compatibilidade
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  Result := Lowercase('"' + LAN_CTOS + '"');
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
end;

procedure TDmod.Teste_ListaCondominios(QrCond: TmySQLQuery; Memo: TMemo;
  QrCondCodigo: TIntegerField; QrCondNO_COND: TWideStringField);
begin
  Memo.Lines.Clear;
  UnDmkDAC_PF.AbreQuery(QrCond, Dmod.MyDB);
  QrCond.First;
  while not QrCond.Eof do
  begin
    Memo.Lines.Add(Geral.FFN(QrCondCodigo.Value, 4) + ' - ' +
      QrCondNO_COND.Value);
    //
    QrCond.Next;
  end;
end;

procedure TDmod.QrEmeioProtAfterScroll(DataSet: TDataSet);
begin
  QrEmeioProtPak.Close;
  QrEmeioProtPak.Params[0].AsInteger := QrEmeioProtCodigo.Value;
  QrEmeioProtPak.Open;
end;

procedure TDmod.QrEmeioProtBeforeClose(DataSet: TDataSet);
begin
  QrEmeioProtPak.Close;
end;

procedure TDmod.QrEmeioProtPakAfterScroll(DataSet: TDataSet);
begin
  QrEmeioProtPakIts.Close;
  QrEmeioProtPakIts.Params[0].AsInteger := QrEmeioProtPakControle.Value;
  QrEmeioProtPakIts.Open;
end;

procedure TDmod.QrEmeioProtPakBeforeClose(DataSet: TDataSet);
begin
  QrEmeioProtPakIts.Close;
end;

function TDmod.InsereItem_ProtoMail(
      RecipEMeio, RecipNome, RecipProno: String;
      RecipItem: Integer;
      Boleto: Double;
      Apto: Integer; Unidade: String;
      Propriet: Integer;
      NOMEPROPRIET: String;
      PROTOCOD: Integer;
      TAREFA: String;
      DEF_SENDER: Integer;
      DELIVER: String;
      DATAE: TDateTime; DATAE_TXT: String;
      DATAD: TDateTime; DATAD_TXT: String;
      LOTE, PROTOCOLO, Nivel, Item: Integer;
      Vencimento: TDateTime; Valor: Double;
      Condominio: String; PreEmeio, IDEmeio: Integer): Boolean;
const
  GetNivel: array [0..4] of String = ('Sem emeio', 'Sem protocolo',
  'N�o enviado', 'Aguardando retorno', 'Retornado');
begin
  try
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'protomail', False, [
      'RecipEMeio', 'RecipNome', 'RecipProno', 'RecipItem',
      'Bloqueto',
      'Depto_Cod', 'Depto_Txt', 'Entid_Cod', 'Entid_Txt',
      'Taref_Cod', 'Taref_Txt', 'Deliv_Cod', 'Deliv_Txt',
      'DataE', 'DataE_Txt', 'DataD', 'DataD_Txt',
      'ProtoLote', 'Protocolo',
      'NivelEmail', 'NivelDescr',
      'Vencimento', 'Valor',
      'Condominio', 'PreEmeio',
      'IDEmeio'
    ], ['Item'], [
      RecipEMeio, RecipNome, RecipProno, RecipItem,
      Boleto,
      Apto, Unidade, Propriet, NOMEPROPRIET,
      PROTOCOD, TAREFA, DEF_SENDER, DELIVER,
      DATAE, DATAE_TXT, DATAD, DATAD_TXT,
      LOTE,PROTOCOLO, Nivel, GetNivel[Nivel],
      Geral.FDT(Vencimento, 1), Valor,
      Condominio, PreEmeio,
      IDEmeio
    ], [Item], False);
    Result := True;
  except
    //Result := False;
    raise;
  end;
end;

{###
procedure TDmod.VerificaEntregaPorEmail(ID_Cod1, ID_Cod2, Periodo, PreEmeio: Integer;
PB1: TProgressBar);
var
  Nivel, Item, PE: Integer;
begin
  Item := 0;
  Screen.Cursor := crHourGlass;
  //FVeriEmeio := True;
  //
  ReopenBolMail(ID_Cod1, ID_Cod2, Periodo);
  if QrBolMail.RecordCount > 0 then
  begin
    if PB1 <> nil then
    begin
      PB1.Position := 0;
      PB1.Max := QrBolMail.RecordCount;
    end;
    Application.ProcessMessages;
    //
    UCriar.RecriaTempTable('ProtoMail', DmodG.QrUpdPid1, False);
    QrBolMail.First;
    while not QrBolMail.Eof do
    begin
      if PB1 <> nil then
      begin
        PB1.Position := PB1.Position + 1;
        PB1.Update;
      end;
      Application.ProcessMessages;
      QrCondEmeios.Close;
      QrCondEmeios.Params[0].AsInteger := QrBolMailApto.Value;
      QrCondEmeios.Open;
      if QrCondEmeios.RecordCount > 0 then
      begin
        QrCondEmeios.First;
        while not QrCondEmeios.Eof do
        begin
          QrPPIMail.Close;
          QrPPIMail.Params[00].AsInteger := ID_Cod1;
          QrPPIMail.Params[01].AsInteger := ID_Cod2;
          QrPPIMail.Params[02].AsInteger := QrCondEmeiosItem.Value;
          QrPPIMail.Open;
          //
          if PreEmeio > 0 then
            PE := PreEmeio
          else
            PE := QrPPIMailPreEmeio.Value;
          //
          Nivel := MLAGeral.BoolToIntDeVarios(
          [
            QrPPIMailDataD.Value > 0,
            QrPPIMailDataE.Value > 0,
            QrPPIMailPROTOCOLO.Value > 0
          ], [4,3,2], 1);
          //
          Inc(Item, 1);
          InsereItem_ProtoMail(
            // EMeio
            QrCondEmeiosEMeio.Value,
            QrCondEmeiosNome.Value,
            QrCondEmeiosPronome.Value,
            // ID do cadastro do emeio
            QrCondEmeiosItem.Value,

            //Bloqueto
            QrBolMailBoleto.Value,
            //Apto
            QrBolMailApto.Value, QrBolMailUnidade.Value,
            //Proprietario
            QrBolMailPropriet.Value, QrBolMailNOMEPROPRIET.Value,
            // Tarefa
            QrPPIMailTAREFA_COD.Value,
            QrPPIMailTAREFA_NOM.Value,
            // Deliver
            QrPPIMailDef_Sender.Value,
            QrPPIMailDELIVER.Value,
            // Data entrega
            QrPPIMailDataE.Value, QrPPIMailDATAE_TXT.Value,
            // Data devolu��o
            QrPPIMailDataD.Value, QrPPIMailDATAD_TXT.Value,
            // Lote e protocolo
            QrPPIMailLOTE.Value, QrPPIMailPROTOCOLO.Value,
            // N�vel, ID
            Nivel, Item,
            // Vencto, valor
            QrBolMailVencto.Value, QrBolMailSUB_TOT.Value,
            // Sauda��o e pr�-emeio
            QrCondEmeiosNOMECOND.Value, PE,
            // ID do emeio (c�digo do cadastro > campo Item da tabela CondEmeios)
            QrCondEmeiosItem.Value);
          QrCondEmeios.Next;
        end;
      end else begin
        Inc(Item, 1);
        InsereItem_ProtoMail(
            // EMeio
            '', '', '', 0,
            //Bloqueto
            QrBolMailBoleto.Value,
            //Apto
            QrBolMailApto.Value, QrBolMailUnidade.Value,
            //Proprietario
            QrBolMailPropriet.Value, QrBolMailNOMEPROPRIET.Value,
            // Tarefa
            QrPPIMailTAREFA_COD.Value,
            QrPPIMailTAREFA_NOM.Value,
            // Deliver
            QrPPIMailDef_Sender.Value,
            QrPPIMailDELIVER.Value,
            // Data entrega
            QrPPIMailDATAE.Value, QrPPIMailDATAE_TXT.Value,
            // Data devolu��o
            QrPPIMailDATAD.Value, QrPPIMailDATAD_TXT.Value,
            // Lote e protocolo
            QrPPIMailLOTE.Value, QrPPIMailPROTOCOLO.Value,
            // N�vel, ID
            0, Item,
            // Vencto, valor
            QrBolMailVencto.Value, QrBolMailSUB_TOT.Value,
            // Sauda��o e pr�-emeio
            '', PreEmeio, 0);
      end;
      QrBolMail.Next;
    end;
    //LaProtoMail.Caption := '';
  end else
  begin
    UCriar.RecriaTempTable('ProtoMail', DmodG.QrUpdPid1, False);
    //LaProtoMail.Caption := 'Nenhuma UH est� cadastrada para entrega por emeio!';
    Geral.MensagemBox('Nenhuma UH est� cadastrada para entrega por emeio!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  //PnProtoMail.Visible := True;
  if PB1 <> nil then
  begin
    PB1.Max             := 0;
    PB1.Position        := 0;
  end;
  //
  //ReopenProtoMails;
  Screen.Cursor := crDefault;
end;
}

function TDmod.ValidaUsuarioWeb(const Usuario: String; var Tipo,
  Codigo: Integer): Boolean;
begin
  Tipo   := 0;
  Codigo := 0;
  Result := True;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAuxN, MyDBn, [
    'SELECT 3 Tipo, Codigo ',
    'FROM condgri ',
    'WHERE UserName="' + Usuario + '"',
    ' ',
    'UNION ',
    ' ',
    'SELECT Tipo, User_ID Codigo ',
    'FROM users ',
    'WHERE UserName="' + Usuario + '"',
    ' ',
    'UNION ',
    ' ',
    'SELECT 7 Tipo, User_ID Codigo ',
    'FROM wclients ',
    'WHERE UserName="' + Usuario + '"',
    '']);
  if QrAuxN.RecordCount > 0 then
  begin
    Tipo   := QrAuxN.FieldByName('Tipo').AsInteger;
    Codigo := QrAuxN.FieldByName('Codigo').AsInteger;
    Result := False;
  end;
  QrAuxN.Close;
end;

procedure TDmod.VerificaReenvioEmailAvisoRecebBloqueto(PreEmeio: Integer;
PB1: TProgressBar);
var
  Nivel, Item, PE: Integer;
begin
  Item := 0;
  Screen.Cursor := crHourGlass;
  QrReenv.Close;
  QrReenv.Open;
  //
  if QrReenv.RecordCount > 0 then
  begin
    if PB1 <> nil then
    begin
      PB1.Position := 0;
      PB1.Max := QrReenv.RecordCount;
    end;
    Application.ProcessMessages;
    //
    UCriar.RecriaTempTable('ProtoMail', DmodG.QrUpdPid1, False);
    QrReenv.First;
    while not QrReenv.Eof do
    begin
      if PB1 <> nil then
      begin
        PB1.Position := PB1.Position + 1;
        PB1.Update;
      end;
      Application.ProcessMessages;
      QrCondEmeios.Close;
      QrCondEmeios.Params[0].AsInteger := QrReenvApto.Value;
      QrCondEmeios.Open;
      if QrCondEmeios.RecordCount > 0 then
      begin
        QrCondEmeios.First;
        while not QrCondEmeios.Eof do
        begin
          QrPPIMail.Close;
          QrPPIMail.Params[00].AsInteger := QrReenvID_Cod1.Value;  //ID_Cod1; // Prev
          QrPPIMail.Params[01].AsInteger := QrReenvID_Cod2.Value;  //ID_Cod2; // Cond
          QrPPIMail.Params[02].AsInteger := QrCondEmeiosItem.Value;
          QrPPIMail.Open;
          //
          if PreEmeio > 0 then
            PE := PreEmeio
          else
            PE := QrPPIMailPreEmeio.Value;
          //
          Nivel := MLAGeral.BoolToIntDeVarios(
          [
            QrPPIMailDataD.Value > 0,
            QrPPIMailDataE.Value > 0,
            QrPPIMailPROTOCOLO.Value > 0
          ], [4,3,2], 1);
          //
          Inc(Item, 1);
          InsereItem_ProtoMail(
            // EMeio
            QrCondEmeiosEMeio.Value,
            QrCondEmeiosNome.Value,
            QrCondEmeiosPronome.Value,
            // ID do cadastro do emeio
            QrCondEmeiosItem.Value,

            //Bloqueto
            QrReenvDocum.Value,
            //Apto
            QrReenvApto.Value, QrReenvUnidade.Value,
            //Proprietario
            QrReenvCliente.Value, QrReenvNOMECLI.Value,
            // Tarefa
            QrPPIMailTAREFA_COD.Value,
            QrPPIMailTAREFA_NOM.Value,
            // Deliver
            QrPPIMailDef_Sender.Value,
            QrPPIMailDELIVER.Value,
            // Data entrega
            QrPPIMailDataE.Value, QrPPIMailDATAE_TXT.Value,
            // Data devolu��o
            QrPPIMailDataD.Value, QrPPIMailDATAD_TXT.Value,
            // Lote e protocolo
            QrPPIMailLOTE.Value, QrPPIMailPROTOCOLO.Value,
            // N�vel, ID
            Nivel, Item,
            // Vencto, valor
            QrReenvVencto.Value, QrReenvValor.Value(*zerado*),
            // Sauda��o e pr�-emeio
            QrCondEmeiosNOMECOND.Value, PE,
            // ID do emeio (c�digo do cadastro > campo Item da tabela CondEmeios)
            QrCondEmeiosItem.Value);
          QrCondEmeios.Next;
        end;
      end else begin
        Inc(Item, 1);
        InsereItem_ProtoMail(
            // EMeio
            '', '', '', 0,
            //Bloqueto
            QrReenvDocum.Value,
            //Apto
            QrReenvApto.Value, QrReenvUnidade.Value,
            //Proprietario
            QrReenvCliente.Value, QrReenvNOMECLI.Value,
            // Tarefa
            QrPPIMailTAREFA_COD.Value,
            QrPPIMailTAREFA_NOM.Value,
            // Deliver
            QrPPIMailDef_Sender.Value,
            QrPPIMailDELIVER.Value,
            // Data entrega
            QrPPIMailDATAE.Value, QrPPIMailDATAE_TXT.Value,
            // Data devolu��o
            QrPPIMailDATAD.Value, QrPPIMailDATAD_TXT.Value,
            // Lote e protocolo
            QrPPIMailLOTE.Value, QrPPIMailPROTOCOLO.Value,
            // N�vel, ID
            0, Item,
            // Vencto, valor
            QrReenvVencto.Value, QrReenvValor.Value,
            // Sauda��o e pr�-emeio
            '', PreEmeio, 0);
      end;
      QrReenv.Next;
    end;
    //LaProtoMail.Caption := '';
  end else
  begin
    UCriar.RecriaTempTable('ProtoMail', DmodG.QrUpdPid1, False);
    //LaProtoMail.Caption := 'Nenhuma UH est� cadastrada para entrega por emeio!';
    Geral.MensagemBox('Nenhuma UH foi localizada!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  //PnProtoMail.Visible := True;
  if PB1 <> nil then
  begin
    PB1.Max             := 0;
    PB1.Position        := 0;
  end;
  //
  //ReopenProtoMails;
  Screen.Cursor := crDefault;
end;

{###
procedure TDmod.ReopenBolMail(ID_Cod1, ID_Cod2, Periodo: Integer);
begin
(*
SELECT DISTINCT cdi.Andar, cdi.Codigo, cdi.Controle, ari.Boleto,
ari.Apto, ari.Propriet, ari.Vencto,
cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial
ELSE ent.Nome END NOMEPROPRIET, cdi.Protocolo PROTOCOD,
CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO
FROM arre+its ari
LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto
LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet
LEFT JOIN protocolos ptc ON ptc.Codigo=cdi.Protocolo
WHERE ari.Boleto > 0
AND ptc.Tipo=2
AND ari.Codigo=:P0

UNION

SELECT DISTINCT cdi.Andar, cdi.Codigo, cdi.Controle, cni.Boleto,
cni.Apto, cni.Propriet, cni.Vencto,
cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial
ELSE ent.Nome END NOMEPROPRIET, cdi.Protocolo PROTOCOD,
CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO
FROM cons+its  cni
LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto
LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet
LEFT JOIN protocolos ptc ON ptc.Codigo=cdi.Protocolo
WHERE cni.Boleto > 0
AND ptc.Tipo=2
AND cni.Cond=:P1
AND cni.Periodo=:P2

ORDER BY Andar, Unidade, Boleto
*)
  QrBolMail.Close;
  QrBolMail.SQL.Clear;
  QrBolMail.SQL.Add('SELECT DISTINCT cdi.Andar, ari.Boleto, ari.Apto, ari.Propriet, ari.Vencto,');
  QrBolMail.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrBolMail.SQL.Add('ELSE ent.Nome END NOMEPROPRIET, cdi.Protocolo PROTOCOD, ');
  QrBolMail.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO');
  QrBolMail.SQL.Add('FROM ' + TabAriA + ' ari');
  QrBolMail.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
  QrBolMail.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
  QrBolMail.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=cdi.Protocolo');
  QrBolMail.SQL.Add('WHERE ari.Boleto > 0');
  QrBolMail.SQL.Add('AND ptc.Tipo=2');
  QrBolMail.SQL.Add('AND ari.Codigo=:P0');
  QrBolMail.SQL.Add('');
  QrBolMail.SQL.Add('UNION');
  QrBolMail.SQL.Add('');
  QrBolMail.SQL.Add('SELECT DISTINCT cdi.Andar, cni.Boleto, cni.Apto, cni.Propriet, cni.Vencto,');
  QrBolMail.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrBolMail.SQL.Add('ELSE ent.Nome END NOMEPROPRIET, cdi.Protocolo PROTOCOD, ');
  QrBolMail.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO');
  QrBolMail.SQL.Add('FROM ' + TabCnsA + '  cni');
  QrBolMail.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
  QrBolMail.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
  QrBolMail.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=cdi.Protocolo');
  QrBolMail.SQL.Add('WHERE cni.Boleto > 0');
  QrBolMail.SQL.Add('AND ptc.Tipo=2');
  QrBolMail.SQL.Add('AND cni.Cond=:P1');
  QrBolMail.SQL.Add('AND cni.Periodo=:P2');
  QrBolMail.SQL.Add('');
  QrBolMail.SQL.Add('ORDER BY Andar, Unidade, Boleto');
  QrBolMail.SQL.Add('');
  //
  QrBolMail.Params[00].AsInteger := ID_Cod1;
  QrBolMail.Params[01].AsInteger := ID_Cod2;
  QrBolMail.Params[02].AsInteger := Periodo;
  QrBolMail.Open;
  //
end;
}

procedure TDmod.EmailBloqueto(PB: TProgressBar; PrevCodigo: Integer;
Antigo: Boolean);
var
  Ordem: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    //FSdoOldNew :=
    UCriar.RecriaTempTable('UnidCond', DmodG.QrUpdPID1, False);
    //
    DModG.QrProtoMail.Close;
    DModG.QrProtoMail.Database := DModG.MyPID_DB;
    DModG.QrProtoMail.Open;
    if PB <> nil then
    begin
      PB.Position := 0;
      PB.Max := DModG.QrProtoMail.RecordCount;
    end;
    DModG.QrProtoMail.First;
    while not DModG.QrProtoMail.Eof do
    begin
      if PB <> nil then
      begin
        PB.Position := PB.Position + 1;
        PB.Update;
      end;
      Application.ProcessMessages;
      //
      Ordem := DModG.QrProtoMail.RecNo;
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'unidcond', False, [
        'Apto', 'Unidade',
        'Proprie', 'Selecio',
        'Entidad', 'Protoco',
        'Bloquet',
        'Data1',
        'Data2',
        'RecipItem', 'RecipNome',
        'RecipEMeio', 'RecipProno',
        'Vencimento', 'Valor',
        'Condominio', 'PreEmeio',
        'IDEmeio'
      ], ['Ordem'], [
        DModG.QrProtoMailDepto_Cod.Value, DModG.QrProtoMailDepto_Txt.Value,
        DModG.QrProtoMailEntid_Txt.Value, MLAGeral.BoolToInt(DModG.QrProtoMailDATAE.Value = 0),
        DModG.QrProtoMailEntid_Cod.Value, DModG.QrProtoMailProtocolo.Value,
        DModG.QrProtoMailBloqueto.Value,
        Geral.FDT(DModG.QrProtoMailDataE.Value, 1),
        Geral.FDT(DModG.QrProtoMailDataD.Value, 1),
        DModG.QrProtoMailRecipItem.Value, DModG.QrProtoMailRecipNome.Value,
        DModG.QrProtoMailRecipEmeio.Value, DModG.QrProtoMailRecipProno.Value,
        Geral.FDT(DModG.QrProtoMailVencimento.Value, 1), DModG.QrProtoMailValor.Value,
        DModG.QrProtoMailCondominio.Value, DModG.QrProtoMailPreEmeio.Value,
        DModG.QrProtoMailIDEmeio.Value
      ], [Ordem], False);
      DModG.QrProtoMail.Next;
    end;
    Application.CreateForm(TFmEmailBloqueto, FmEmailBloqueto);
    FmEmailBloqueto.FPrevCodigo := PrevCodigo;
    //FmEmailBloqueto.FPermiteAnexarBloqueto := PermiteAnexarBloqueto;
  finally
    Screen.Cursor := crDefault;
  end;
  FmEmailBloqueto.ShowModal;
  FmEmailBloqueto.Destroy;
end;

procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
CliInt: Integer; Campos: String; DescrSubstit: Boolean);
begin
  Geral.MensagemBox('Este balancete n�o possui dados industriais ' +
  'adicionais neste aplicativo!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

function TDmod.LocalizaUsuarioWeb(Query: TmySQLQuery; DataBase: TmySQLDatabase;
  Usuario: String; var Tipo, ID: Integer): Boolean;
begin
  Result := False;
  //
  //UH
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
    'SELECT User_ID Codigo ',
    'FROM users ',
    'WHERE Username="' + Usuario + '"',
    'AND Tipo = 1',
    '']);
  if Query.RecordCount = 0 then
  begin
    //S�ndico
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT User_ID Codigo ',
      'FROM wclients ',
      'WHERE Username="' + Usuario + '"',
      '']);
    if Query.RecordCount = 0 then
    begin
      //Grupo
      UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
        'SELECT Codigo ',
        'FROM condgri ',
        'WHERE Username="' + Usuario + '"',
        '']);
      if Query.RecordCount = 0 then
      begin
        //Administrador
        UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
          'SELECT User_ID Codigo ',
          'FROM users ',
          'WHERE Username="' + Usuario + '"',
          'AND Tipo = 9',
          '']);
        if Query.RecordCount = 0 then
        begin
          ID     := 0;
          Tipo   := 0;
          Result := False;
        end else
        begin
        ID     := Query.FieldByName('Codigo').AsInteger;
        Tipo   := 9;
        Result := True;
        end;
      end else
      begin
        ID     := Query.FieldByName('Codigo').AsInteger;
        Tipo   := 3;
        Result := True;
      end;
    end else
    begin
      ID     := Query.FieldByName('Codigo').AsInteger;
      Tipo   := 7;
      Result := True;
    end;
  end else
  begin
    ID     := Query.FieldByName('Codigo').AsInteger;
    Tipo   := 1;
    Result := True;
  end;
end;

{
SQL do QrBolArr :
SELECT ari.Boleto, ari.Apto, SUM(ari.Valor) Valor,
CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO
FROM arre+its ari
WHERE ari.Codigo=:P0
GROUP BY ari.Boleto, ari.Apto

SQL do QrBolLei:
SELECT cni.Boleto, cni.Apto, SUM(cni.Valor) Valor,
CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO
FROM cons+its  cni 
WHERE cni.Cond=:P0
AND cni.Periodo=:P1
GROUP BY cni.Boleto, cni.Apto

}







{
object QrControleTxtCnd_10: TWideStringField
  FieldName = 'TxtCnd_10'
  Origin = 'controle.TxtCnd_10'
  Size = 10
end
object QrControleTxtCnd_05: TWideStringField
  FieldName = 'TxtCnd_05'
  Origin = 'controle.TxtCnd_05'
  Size = 5
end
object QrControleTxtUsu_10: TWideStringField
  FieldName = 'TxtUsu_10'
  Origin = 'controle.TxtUsu_10'
  Size = 10
end
object QrControleTxtPrp_15: TWideStringField
  FieldName = 'TxtPrp_15'
  Origin = 'controle.TxtPrp_15'
  Size = 15
end
object QrControleTxtRis_25: TWideStringField
  FieldName = 'TxtRis_25'
  Origin = 'controle.TxtRis_25'
  Size = 25
end
object QrControleTxtImb_15: TWideStringField
  FieldName = 'TxtImb_15'
  Origin = 'controle.TxtImb_15'
  Size = 15
end
object QrControleTxtMor_15: TWideStringField
  FieldName = 'TxtMor_15'
  Origin = 'controle.TxtMor_15'
  Size = 15
end
object QrControleTxtUHs_20: TWideStringField
  FieldName = 'TxtUHs_20'
  Origin = 'controle.TxtUHs_20'
end
object QrControleTxtUHs_10: TWideStringField
  FieldName = 'TxtUHs_10'
  Origin = 'controle.TxtUHs_10'
  Size = 10
end
object QrControleTxtUHs_12: TWideStringField
  FieldName = 'TxtUHs_12'
  Origin = 'controle.TxtUHs_12'
  Size = 10
end
object QrControleTxtUHs_03: TWideStringField
  FieldName = 'TxtUHs_03'
  Origin = 'controle.TxtUHs_03'
  Size = 3
end
object QrControleTxtUHs_05: TWideStringField
  FieldName = 'TxtUHs_05'
  Origin = 'controle.TxtUHs_05'
  Size = 3
end
object QrControleTxtBlc_10: TWideStringField
  FieldName = 'TxtBlc_10'
  Origin = 'controle.TxtBlc_10'
  Size = 10
end
object QrControleTxtBlc_11: TWideStringField
  FieldName = 'TxtBlc_11'
  Origin = 'controle.TxtBlc_11'
  Size = 10
end
object QrControleTxtNiv_10: TWideStringField
  FieldName = 'TxtNiv_10'
  Origin = 'controle.TxtNiv_10'
  Size = 10
end
object QrControleTxtNiv_11: TWideStringField
  FieldName = 'TxtNiv_11'
  Origin = 'controle.TxtNiv_11'
  Size = 10
end
object QrControleTxtSin_10: TWideStringField
  FieldName = 'TxtSin_10'
  Origin = 'controle.TxtSin_10'
  Size = 10
end
object QrControleTxtMor_20: TWideStringField
  FieldName = 'TxtMor_20'
end
}
end.

