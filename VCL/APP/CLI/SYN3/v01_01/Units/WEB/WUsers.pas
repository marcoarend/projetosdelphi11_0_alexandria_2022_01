unit WUsers;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DBCtrls, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, UnInternalConsts,
  dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmWUsers = class(TForm)
    Panel1: TPanel;
    QrEntiCond: TmySQLQuery;
    QrEntiCondCodCond: TIntegerField;
    QrEntiCondCodEnti: TIntegerField;
    QrEntiCondNOMECOND: TWideStringField;
    DsEntiCond: TDataSource;
    QrPropriet: TmySQLQuery;
    QrProprietCodigo: TIntegerField;
    QrProprietNOMEPROP: TWideStringField;
    DsPropriet: TDataSource;
    QrCondImov: TmySQLQuery;
    QrCondImovConta: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    DsCondImov: TDataSource;
    Panel11: TPanel;
    PnPesq1: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdPropriet: TdmkEditCB;
    CBPropriet: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdCondImov: TdmkEditCB;
    CBCondImov: TdmkDBLookupComboBox;
    RGTipoUser: TRadioGroup;
    QrPesq1: TmySQLQuery;
    QrPesq1NO_Condom: TWideStringField;
    QrPesq1NO_Pessoa: TWideStringField;
    QrPesq1Unidade: TWideStringField;
    QrPesq1User_ID: TAutoIncField;
    QrPesq1CodCliEsp: TIntegerField;
    QrPesq1CodigoEsp: TIntegerField;
    QrPesq1Username: TWideStringField;
    QrPesq1Password: TWideStringField;
    QrPesq1LoginID: TWideStringField;
    QrPesq1LastAcess: TDateTimeField;
    QrPesq1Tipo: TSmallintField;
    DsPesq1: TDataSource;
    DBGrid1: TDBGrid;
    QrPesq1NO_Tipo: TWideStringField;
    QrErrSenhas1: TmySQLQuery;
    QrErrSenhas1User_ID: TIntegerField;
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtPesq: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    BtAbrir: TBitBtn;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    ImgWEB: TdmkImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdProprietChange(Sender: TObject);
    procedure EdProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCondImovChange(Sender: TObject);
    procedure CBCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGTipoUserClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure QrPesq1AfterOpen(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure CBProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrPesq1BeforeClose(DataSet: TDataSet);
    procedure BtAbrirClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPropriet(Todos: Boolean);
    procedure ReopenCondImov();
    procedure PesquisaEm_users();
    //procedure PesquisaEm_wclients();
  public
    { Public declarations }
    FCond, FUser_ID: Integer;
    procedure LocCod(Atual, Codigo: Integer);
  end;

  var
  FmWUsers: TFmWUsers;

implementation

uses dmkGeral, ModuleCond, Module, ModuleGeral, UnMyObjects, UnDmkWeb;

{$R *.DFM}

procedure TFmWUsers.BtAbrirClick(Sender: TObject);
begin
  FCond    := QrPesq1CodigoEsp.Value;
  FUser_ID := QrPesq1User_ID.Value;
  Close;
end;

procedure TFmWUsers.BtExcluiClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreQuery(QrErrSenhas1, Dmod.MyDBn);
    //
    if QrErrSenhas1.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(QrErrSenhas1.RecordCount) +
        ' senhas web na qual mudou o ' +
        DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) + ' da ' +
        DModG.ReCaptionTexto(VAR_U_H) + '.' + sLineBreak +
        '� aconselhav�l excluir estas senhas!' + sLineBreak +
        'Deseja excluir estas senhas?') = ID_YES then
      begin
        Screen.Cursor := crHourGlass;
        try
          Dmod.QrUpdN.SQL.Clear;
          Dmod.QrUpdN.SQL.Add('DELETE FROM users WHERE ');
          Dmod.QrUpdN.SQL.Add('User_ID=:P0');
          //
          QrErrSenhas1.First;
          while not QrErrSenhas1.Eof do
          begin
            Dmod.QrUpdN.Params[00].AsInteger := QrErrSenhas1User_ID.Value;
            Dmod.QrUpdN.ExecSQL;
            //
            QrErrSenhas1.Next;
          end;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end else
      Geral.MensagemBox('N�o existem senhas web na qual mudou o ' +
      DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) + ' da ' +
      DModG.ReCaptionTexto(VAR_U_H) + '.',
      'Mensagem', MB_OK+MB_ICONINFORMATION);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWUsers.BtPesqClick(Sender: TObject);
begin
  case RGTipoUser.ItemIndex of
    0,1: PesquisaEm_users();
    //1: PesquisaEm_wclients();
    else Geral.MensagemBox('Tipo de usu�rio sem implementa��o de pesquisa!',
    'Erro', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmWUsers.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWUsers.CBCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_DELETE then
  begin
    EdCondImov.ValueVariant := 0;
    CBCondImov.KeyValue     := Null;
  end;
end;

procedure TFmWUsers.CBProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmWUsers.EdCondImovChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
end;

procedure TFmWUsers.EdEmpresaChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  if EdEmpresa.ValueVariant <> 0 then
  begin
    ReopenPropriet(False);
    ReopenCondImov();
  end else
  begin
    QrPropriet.Close;
    QrCondImov.Close;
  end;
end;

procedure TFmWUsers.EdProprietChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  ReopenCondImov;
end;

procedure TFmWUsers.EdProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmWUsers.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWUsers.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCond           := 0;
  FUser_ID        := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrEntiCond, Dmod.MyDB);
  //
  BtExclui.Visible := False;
  BtAbrir.Enabled  := False;
end;

procedure TFmWUsers.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWUsers.LocCod(Atual, Codigo: Integer);
begin
  //Compatibilidade
end;

procedure TFmWUsers.PesquisaEm_users();
var
  Cond, Prop, Apto: Integer;
begin
  Cond := EdEmpresa.ValueVariant;
  Prop := EdPropriet.ValueVariant;
  Apto := EdCondImov.ValueVariant;
  //
  QrPesq1.Close;
  QrPesq1.Database := Dmod.MyDBn;
  QrPesq1.SQL.Clear;
  QrPesq1.SQL.Add('SELECT ');
  QrPesq1.SQL.Add('IF(con.Tipo=0,con.RazaoSocial,con.Nome) NO_Condom,');
  QrPesq1.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_Pessoa,');
  QrPesq1.SQL.Add('imv.Unidade, CASE WHEN usr.Tipo=1 THEN "Morador" ');
  QrPesq1.SQL.Add('WHEN usr.Tipo=9 THEN "Admin" ELSE "TESTE" END NO_Tipo,');
  QrPesq1.SQL.Add('usr.*');
  QrPesq1.SQL.Add('FROM users usr');
  QrPesq1.SQL.Add('LEFT JOIN condimov imv ON imv.Conta = usr.CodCliEsp');
  QrPesq1.SQL.Add('LEFT JOIN cond cnd ON cnd.Codigo = usr.CodigoEsp');
  QrPesq1.SQL.Add('LEFT JOIN entidades con ON con.Codigo=cnd.Cliente');
  QrPesq1.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=imv.Propriet');
  QrPesq1.SQL.Add('WHERE usr.User_ID>0'); // n�o mostrar -1
  //
  if Cond <> 0 then
    QrPesq1.SQL.Add('AND usr.Tipo=' + Geral.FF0(RGTipoUser.ItemIndex));
  if Cond <> 0 then
    QrPesq1.SQL.Add('AND usr.CodCliEsp=' + Geral.FF0(Cond));
  if Prop <> 0 then
    QrPesq1.SQL.Add('AND imv.Propriet=' + Geral.FF0(Prop));
  if Apto <> 0 then
    QrPesq1.SQL.Add('AND usr.CodigoEsp=' + Geral.FF0(Apto));
  //
  QrPesq1.Open;
end;

procedure TFmWUsers.QrPesq1AfterOpen(DataSet: TDataSet);
begin
  PnPesq1.Visible := True;
  BtAbrir.Enabled := QrPesq1.RecordCount > 0;
end;

procedure TFmWUsers.QrPesq1BeforeClose(DataSet: TDataSet);
begin
  BtAbrir.Enabled  := False;
end;

procedure TFmWUsers.ReopenCondImov();
var
  Cond, Propriet: Integer;
begin
  EdCondImov.Text := '';
  CBCondImov.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  Propriet := Geral.IMV(EdPropriet.Text);
  QrCondImov.Close;
  QrCondImov.Database := Dmod.MyDB;
  QrCondImov.SQL.Clear;
  QrCondImov.SQL.Add('SELECT cim.Conta, cim.Unidade');
  QrCondImov.SQL.Add('FROM condimov cim');
  QrCondImov.SQL.Add('WHERE cim.Codigo<>0');
  QrCondImov.SQL.Add('AND cim.Propriet<>0');
  QrCondImov.SQL.Add('');
  if Cond <> 0 then
    QrCondImov.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  if Propriet <> 0 then
    QrCondImov.SQL.Add('AND cim.Propriet=' + IntToStr(Propriet));
  QrCondImov.SQL.Add('');
  QrCondImov.SQL.Add('ORDER BY cim.Unidade');
  QrCondImov.Open;
end;

procedure TFmWUsers.ReopenPropriet(Todos: Boolean);
var
  Cond: Integer;
begin
  EdPropriet.Text := '';
  CBPropriet.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  QrPropriet.Close;
  QrPropriet.Database := Dmod.MyDB;
  QrPropriet.SQL.Clear;
  QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
  QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
  QrPropriet.SQL.Add('FROM entidades ent');
  QrPropriet.SQL.Add('LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet');
  QrPropriet.SQL.Add('WHERE ent.Cliente2="V"');
  QrPropriet.SQL.Add('');
  if (Cond <> 0) and not Todos then
    QrPropriet.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  QrPropriet.SQL.Add('');
  QrPropriet.SQL.Add('ORDER BY NOMEPROP');
  QrPropriet.Open;
end;

procedure TFmWUsers.RGTipoUserClick(Sender: TObject);
begin
  BtPesq.Enabled := RGTipoUser.ItemIndex > 0;
end;

end.
