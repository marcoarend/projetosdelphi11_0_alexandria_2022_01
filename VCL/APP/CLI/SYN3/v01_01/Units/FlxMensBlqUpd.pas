unit FlxMensBlqUpd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkPermissoes, UnDmkEnums;

type
  TFmFlxMensBlqUpd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    LaDataHora: TLabel;
    LaTexto: TLabel;
    EdDataHora: TdmkEdit;
    EdObserv: TEdit;
    Panel6: TPanel;
    Label1: TLabel;
    LaMes: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdAnoMes: TdmkEdit;
    RGEtapa: TRadioGroup;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFlxMensBlqUpd: TFmFlxMensBlqUpd;

implementation

uses Module, UnMyObjects, ModuleGeral, UMySQLModule;

{$R *.DFM}

procedure TFmFlxMensBlqUpd.BtOKClick(Sender: TObject);
var
  field_User, field_Data,
  DataHora, Observ: String;
  AnoMes, Codigo: Integer;
begin
  Codigo := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Codigo=0, EdEmpresa, 'Informe a empresa!') then Exit;
  AnoMes := Geral.DTAM(EdAnoMes.ValueVariant);
  if MyObjects.FIC(AnoMes < 2, EdAnoMes, 'Informe o m�s') then Exit;
  //
  if Trim(EdObserv.Text) <> '' then
  begin
    Observ   := EdObserv.Text;
    DataHora := '0000-00-00 00:00:00';
  end else begin
    Observ   := '';
    DataHora := Geral.FDT(EdDataHora.ValueVariant, 9);
  end;
  //
  case RGEtapa.ItemIndex of
    1:
    begin
      field_User := 'Folha_User';
      field_Data := 'Folha_Data';
    end;
    2:                                               
    begin                                            
      field_User := 'LeiAr_User';                         
      field_Data := 'LeiAr_Data';
    end;
    3:
    begin
      field_User := 'Fecha_User';
      field_Data := 'Fecha_Data';
    end;
    4:
    begin
      field_User := 'Risco_User';
      field_Data := 'Risco_Data';
    end;
    5:
    begin
      field_User := 'Print_User';
      field_Data := 'Print_Data';
    end;
    6:
    begin
      field_User := 'Proto_User';
      field_Data := 'Proto_Data';
    end;
    7:
    begin
      field_User := 'Relat_User';
      field_Data := 'Relat_Data';
    end;
    8:
    begin
      field_User := 'EMail_User';
      field_Data := 'EMail_Data';
    end;
    9:
    begin
      field_User := 'Porta_User';
      field_Data := 'Porta_Data';
    end;
    10:
    begin
      field_User := 'Postl_User';
      field_Data := 'Postl_Data';
    end;
    else
    begin
      field_User := '?????_User';
      field_Data := '?????_Data';
    end;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'flxbloq', False, [
    'Observ', field_user, field_Data
  ], [
    'AnoMes', 'Codigo'
  ], [
    Observ, VAR_USUARIO, DataHora
  ], [
    AnoMes, Codigo], True) then
  Close;
end;

procedure TFmFlxMensBlqUpd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFlxMensBlqUpd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFlxMensBlqUpd.FormCreate(Sender: TObject);
begin
  EdDataHora.ValueVariant := DModG.ObtemAgora();
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmFlxMensBlqUpd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
