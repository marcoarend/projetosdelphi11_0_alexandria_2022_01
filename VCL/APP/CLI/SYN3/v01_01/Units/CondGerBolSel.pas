unit CondGerBolSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral,
  dmkImage, UnDmkEnums;

type
  TFmCondGerBolSel = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FBoleto: Double;
    FProtocolo: Integer;
    FData: TDateTime;
  end;

  var
  FmCondGerBolSel: TFmCondGerBolSel;

implementation

{$R *.DFM}

uses ModuleBloq, UnMyObjects;

procedure TFmCondGerBolSel.BtSaidaClick(Sender: TObject);
begin
  FProtocolo := 0;
  FBoleto    := 0;
  FData      := 0;
  Close;
end;

procedure TFmCondGerBolSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerBolSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerBolSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FProtocolo := 0;
  FBoleto    := 0;
  FData      := 0;
end;

procedure TFmCondGerBolSel.DBGrid1DblClick(Sender: TObject);
begin
  FProtocolo := DmBloq.QrBolSelPROTOCOLO.Value;
  FBoleto    := DmBloq.QrBolSelBoleto.Value;
  FData      := DmBloq.QrBolSelVencto.Value;
  Close;
end;

procedure TFmCondGerBolSel.BtOKClick(Sender: TObject);
begin
  FProtocolo := DmBloq.QrBolSelPROTOCOLO.Value;
  FBoleto    := DmBloq.QrBolSelBoleto.Value;
  FData      := DmBloq.QrBolSelVencto.Value;
  Close;
end;

end.
