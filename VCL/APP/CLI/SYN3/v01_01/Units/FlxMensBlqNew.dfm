object FmFlxMensBlqNew: TFmFlxMensBlqNew
  Left = 339
  Top = 185
  Caption = 'FLX-BALAN-005 :: Novo Per'#237'odo de Fluxo de Bloquetos'
  ClientHeight = 482
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 440
        Height = 32
        Caption = 'Novo Per'#237'odo de Fluxo de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 440
        Height = 32
        Caption = 'Novo Per'#237'odo de Fluxo de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 440
        Height = 32
        Caption = 'Novo Per'#237'odo de Fluxo de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 296
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 296
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 296
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 61
          Align = alTop
          TabOrder = 0
          object Label32: TLabel
            Left = 16
            Top = 7
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object LaAnoI: TLabel
            Left = 192
            Top = 7
            Width = 22
            Height = 13
            Caption = 'Ano:'
          end
          object CBMes: TComboBox
            Left = 17
            Top = 24
            Width = 172
            Height = 21
            Style = csDropDownList
            Color = clWhite
            DropDownCount = 12
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object CBAno: TComboBox
            Left = 191
            Top = 24
            Width = 78
            Height = 21
            Style = csDropDownList
            Color = clWhite
            DropDownCount = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
          object BtTodos: TBitBtn
            Tag = 127
            Left = 794
            Top = 8
            Width = 90
            Height = 40
            Caption = '&Todos'
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BtTodosClick
          end
          object BtNenhum: TBitBtn
            Tag = 128
            Left = 890
            Top = 8
            Width = 90
            Height = 40
            Caption = '&Nenhum'
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BtNenhumClick
          end
          object BitBtn1: TBitBtn
            Left = 440
            Top = 8
            Width = 90
            Height = 40
            Caption = '&Dias padr'#245'es**'
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BitBtn1Click
          end
          object PB1: TProgressBar
            Left = 536
            Top = 24
            Width = 150
            Height = 17
            TabOrder = 5
          end
        end
        object DBGrid1: TdmkDBGridDAC
          Left = 2
          Top = 76
          Width = 1004
          Height = 218
          SQLFieldsToChange.Strings = (
            'Ativo')
          SQLIndexesOnUpdate.Strings = (
            'Codigo')
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'FlxB_Ordem'
              Title.Caption = 'Ordem'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'OK'
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Condom.'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CND'
              Title.Caption = 'Nome do condom'#237'nio'
              Width = 258
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Folha'
              Title.Caption = '*Folha'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_LeiAr'
              Title.Caption = '*Leit./arrec.'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Fecha'
              Title.Caption = '*Fechamento'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Risco'
              Title.Caption = '*Adm. riscos'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Print'
              Title.Caption = '*Impress'#227'o'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Proto'
              Title.Caption = '*Protocolo'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Relat'
              Title.Caption = '*Relat'#243'rios'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_EMail'
              Title.Caption = '*Email'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Porta'
              Title.Caption = '*Portaria'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Postl'
              Title.Caption = '*Postal'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DiaVencto'
              Title.Caption = 'Vct bol'
              Width = 48
              Visible = True
            end>
          Color = clWindow
          DataSource = DsFlxCond
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          SQLTable = '_flx_cond_blq_'
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'FlxB_Ordem'
              Title.Caption = 'Ordem'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'OK'
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Condom.'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CND'
              Title.Caption = 'Nome do condom'#237'nio'
              Width = 258
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Folha'
              Title.Caption = '*Folha'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_LeiAr'
              Title.Caption = '*Leit./arrec.'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Fecha'
              Title.Caption = '*Fechamento'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Risco'
              Title.Caption = '*Adm. riscos'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Print'
              Title.Caption = '*Impress'#227'o'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Proto'
              Title.Caption = '*Protocolo'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Relat'
              Title.Caption = '*Relat'#243'rios'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_EMail'
              Title.Caption = '*Email'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Porta'
              Title.Caption = '*Portaria'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FlxB_Postl'
              Title.Caption = '*Postal'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DiaVencto'
              Title.Caption = 'Vct bol'
              Width = 48
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 344
    Width = 1008
    Height = 68
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 51
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1A: TLabel
        Left = 13
        Top = 2
        Width = 244
        Height = 16
        Caption = '* Dia do m'#234's limite para t'#233'rmino da tarefa.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2A: TLabel
        Left = 12
        Top = 1
        Width = 244
        Height = 16
        Caption = '* Dia do m'#234's limite para t'#233'rmino da tarefa.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso1B: TLabel
        Left = 13
        Top = 26
        Width = 503
        Height = 16
        Caption = 
          '** Define os dias limites pelo padr'#227'o para os condom'#237'nios que ai' +
          'nda n'#227'o tem defini'#231#227'o.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2B: TLabel
        Left = 12
        Top = 25
        Width = 503
        Height = 16
        Caption = 
          '** Define os dias limites pelo padr'#227'o para os condom'#237'nios que ai' +
          'nda n'#227'o tem defini'#231#227'o.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 412
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(AnoMes) AnoMes'
      'FROM flxbloq')
    Left = 552
    Top = 8
    object QrPesqAnoMes: TIntegerField
      FieldName = 'AnoMes'
      Required = True
    end
  end
  object QrCtrl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT'
      'FlxB_Folha, FlxB_LeiAr, '
      'FlxB_Fecha, FlxB_Risco, FlxB_Print, '
      'FlxB_Proto, FlxB_Relat, FlxB_EMail, '
      'FlxB_Porta, FlxB_Postl'
      'FROM Controle')
    Left = 580
    Top = 8
    object QrCtrlFlxB_Folha: TSmallintField
      FieldName = 'FlxB_Folha'
    end
    object QrCtrlFlxB_LeiAr: TSmallintField
      FieldName = 'FlxB_LeiAr'
    end
    object QrCtrlFlxB_Fecha: TSmallintField
      FieldName = 'FlxB_Fecha'
    end
    object QrCtrlFlxB_Risco: TSmallintField
      FieldName = 'FlxB_Risco'
    end
    object QrCtrlFlxB_Print: TSmallintField
      FieldName = 'FlxB_Print'
    end
    object QrCtrlFlxB_Proto: TSmallintField
      FieldName = 'FlxB_Proto'
    end
    object QrCtrlFlxB_Relat: TSmallintField
      FieldName = 'FlxB_Relat'
    end
    object QrCtrlFlxB_EMail: TSmallintField
      FieldName = 'FlxB_EMail'
    end
    object QrCtrlFlxB_Porta: TSmallintField
      FieldName = 'FlxB_Porta'
    end
    object QrCtrlFlxB_Postl: TSmallintField
      FieldName = 'FlxB_Postl'
    end
  end
  object QrCndNDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cnd.Codigo'
      'FROM cond cnd'
      'WHERE cnd.FlxB_Folha=0 AND'
      'cnd.FlxB_LeiAr = 0 AND cnd.FlxB_Fecha = 0 AND'
      'cnd.FlxB_Risco = 0 AND cnd.FlxB_Print = 0 AND'
      'cnd.FlxB_Proto = 0 AND cnd.FlxB_Relat = 0 AND'
      'cnd.FlxB_EMail = 0 AND cnd.FlxB_Porta = 0 AND'
      'cnd.FlxB_Postl = 0')
    Left = 608
    Top = 8
    object QrCndNDefCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLocJa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM flxbloq'
      'WHERE Codigo=:P0'
      'AND AnoMes=:P1')
    Left = 636
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object TbFlxCond: TmySQLTable
    Database = DModG.MyPID_DB
    SortFieldNames = 'FlxB_Ordem, Codigo'
    DefaultIndex = False
    TableName = '_flx_cond_blq_'
    Left = 128
    Top = 236
    object TbFlxCondCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbFlxCondAtivo: TSmallintField
      FieldName = 'Ativo'
      ReadOnly = True
      MaxValue = 1
    end
    object TbFlxCondNO_CND: TWideStringField
      FieldName = 'NO_CND'
      ReadOnly = True
      Size = 100
    end
    object TbFlxCondFlxB_Ordem: TIntegerField
      FieldName = 'FlxB_Ordem'
    end
    object TbFlxCondFlxB_Folha: TSmallintField
      FieldName = 'FlxB_Folha'
      DisplayFormat = '0;-0; '
    end
    object TbFlxCondFlxB_LeiAr: TSmallintField
      FieldName = 'FlxB_LeiAr'
      DisplayFormat = '0;-0; '
    end
    object TbFlxCondFlxB_Fecha: TSmallintField
      FieldName = 'FlxB_Fecha'
      DisplayFormat = '0;-0; '
    end
    object TbFlxCondFlxB_Risco: TSmallintField
      FieldName = 'FlxB_Risco'
      DisplayFormat = '0;-0; '
    end
    object TbFlxCondFlxB_Print: TSmallintField
      FieldName = 'FlxB_Print'
      DisplayFormat = '0;-0; '
    end
    object TbFlxCondFlxB_Proto: TSmallintField
      FieldName = 'FlxB_Proto'
      DisplayFormat = '0;-0; '
    end
    object TbFlxCondFlxB_Relat: TSmallintField
      FieldName = 'FlxB_Relat'
      DisplayFormat = '0;-0; '
    end
    object TbFlxCondFlxB_EMail: TSmallintField
      FieldName = 'FlxB_EMail'
      DisplayFormat = '0;-0; '
    end
    object TbFlxCondFlxB_Porta: TSmallintField
      FieldName = 'FlxB_Porta'
      DisplayFormat = '0;-0; '
    end
    object TbFlxCondFlxB_Postl: TSmallintField
      FieldName = 'FlxB_Postl'
      DisplayFormat = '0;-0; '
    end
    object TbFlxCondDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
  end
  object DsFlxCond: TDataSource
    DataSet = TbFlxCond
    Left = 156
    Top = 236
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 184
    Top = 236
  end
end
