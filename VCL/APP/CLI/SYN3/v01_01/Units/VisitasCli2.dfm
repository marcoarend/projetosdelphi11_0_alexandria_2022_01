object FmVisitasCli2: TFmVisitasCli2
  Left = 339
  Top = 185
  Caption = 'WEB-VISIT-001 :: Controle de usu'#225'rios web'
  ClientHeight = 629
  ClientWidth = 964
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 964
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 916
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 868
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 309
        Height = 32
        Caption = 'Controle de usu'#225'rios web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 309
        Height = 32
        Caption = 'Controle de usu'#225'rios web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 309
        Height = 32
        Caption = 'Controle de usu'#225'rios web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 964
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 964
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 964
        Height = 467
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 812
        object PnPesquisa: TPanel
          Left = 2
          Top = 15
          Width = 960
          Height = 220
          Align = alTop
          TabOrder = 0
          ExplicitWidth = 808
          object dmkCkCond: TdmkLabel
            Left = 7
            Top = 4
            Width = 144
            Height = 13
            Caption = 'Condom'#237'nio [F4 mostra todos]:'
            UpdType = utYes
            SQLType = stNil
          end
          object dmkCkPropriet: TdmkLabel
            Left = 7
            Top = 44
            Width = 126
            Height = 13
            Caption = 'Morador [F4 mostra todos]:'
            UpdType = utYes
            SQLType = stNil
          end
          object dmkCKUH: TdmkLabel
            Left = 454
            Top = 43
            Width = 103
            Height = 13
            Caption = 'Unidade habitacional:'
            UpdType = utYes
            SQLType = stNil
          end
          object dmkLabel1: TdmkLabel
            Left = 7
            Top = 84
            Width = 246
            Height = 13
            Caption = 'Grupo de Unidades Habitacionais [F4 mostra todos]:'
            UpdType = utYes
            SQLType = stNil
          end
          object EdCond: TdmkEditCB
            Left = 7
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdCondChange
            DBLookupComboBox = CBCond
            IgnoraDBLookupComboBox = False
          end
          object CBCond: TdmkDBLookupComboBox
            Left = 62
            Top = 20
            Width = 555
            Height = 21
            KeyField = 'Codigo'
            ListField = 'COND'
            ListSource = DsCond
            TabOrder = 1
            dmkEditCB = EdCond
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPropriet: TdmkEditCB
            Left = 7
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdProprietChange
            DBLookupComboBox = CBPropriet
            IgnoraDBLookupComboBox = False
          end
          object CBPropriet: TdmkDBLookupComboBox
            Left = 62
            Top = 60
            Width = 386
            Height = 21
            KeyField = 'Propriet'
            ListField = 'PROP'
            ListSource = DsPropriet
            TabOrder = 3
            dmkEditCB = EdPropriet
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdUH: TdmkEditCB
            Left = 454
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBUH
            IgnoraDBLookupComboBox = False
          end
          object CBUH: TdmkDBLookupComboBox
            Left = 510
            Top = 60
            Width = 107
            Height = 21
            KeyField = 'Conta'
            ListField = 'Unidade'
            ListSource = DsUHs
            TabOrder = 5
            dmkEditCB = EdUH
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CBGrupo: TdmkDBLookupComboBox
            Left = 62
            Top = 100
            Width = 555
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCondGri
            TabOrder = 7
            dmkEditCB = EdGrupo
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdGrupo: TdmkEditCB
            Left = 7
            Top = 100
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBGrupo
            IgnoraDBLookupComboBox = False
          end
          object BtPesquisa: TBitBtn
            Tag = 22
            Left = 7
            Top = 170
            Width = 120
            Height = 40
            Caption = '&Pesquisa'
            NumGlyphs = 2
            TabOrder = 14
            OnClick = BtPesquisaClick
          end
          object GroupBox2: TGroupBox
            Left = 625
            Top = 4
            Width = 160
            Height = 77
            Caption = ' Pesquisar po usu'#225'rios do tipo '
            TabOrder = 13
            object CkTipoUHs: TCheckBox
              Left = 10
              Top = 19
              Width = 110
              Height = 17
              Caption = 'Cond'#244'mino'
              TabOrder = 0
            end
            object CkTipoSindico: TCheckBox
              Left = 10
              Top = 35
              Width = 110
              Height = 17
              Caption = 'S'#237'ndico'
              TabOrder = 1
            end
            object CkTipoGrupo: TCheckBox
              Left = 10
              Top = 51
              Width = 110
              Height = 17
              Caption = 'Grupo de UH'#39's'
              TabOrder = 2
            end
          end
          object RGTipoPesq: TRadioGroup
            Left = 201
            Top = 127
            Width = 200
            Height = 37
            Caption = ' Tipo de pesquisa '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              #218'ltimo acesso'
              'Hist'#243'rico')
            TabOrder = 12
          end
          object CkIniDta: TCheckBox
            Left = 7
            Top = 126
            Width = 90
            Height = 17
            Caption = 'Data inicial:'
            TabOrder = 8
          end
          object TPIniDta: TDateTimePicker
            Left = 7
            Top = 143
            Width = 90
            Height = 21
            CalColors.TextColor = clMenuText
            Date = 37636.777157974500000000
            Time = 37636.777157974500000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 9
          end
          object TPFimDta: TDateTimePicker
            Left = 105
            Top = 143
            Width = 90
            Height = 21
            Date = 37636.777203761600000000
            Time = 37636.777203761600000000
            TabOrder = 11
          end
          object CkFimDta: TCheckBox
            Left = 105
            Top = 126
            Width = 90
            Height = 17
            Caption = 'Data final:'
            TabOrder = 10
          end
        end
        object DBGPesq: TdmkDBGrid
          Left = 2
          Top = 235
          Width = 960
          Height = 230
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'NivelLogin'
              Title.Caption = 'N'#237've login'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CondGrupo'
              Title.Caption = 'Condom'#237'nio / Grupo'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UH'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Propriet'
              Title.Caption = 'Propriet'#225'rio'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / Hora'
              Width = 115
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ACESSOS'
              Title.Caption = 'Acessos'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IP'
              Width = 115
              Visible = True
            end>
          Color = clWindow
          DataSource = DsPesq
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NivelLogin'
              Title.Caption = 'N'#237've login'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CondGrupo'
              Title.Caption = 'Condom'#237'nio / Grupo'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UH'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Propriet'
              Title.Caption = 'Propriet'#225'rio'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / Hora'
              Width = 115
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ACESSOS'
              Title.Caption = 'Acessos'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IP'
              Width = 115
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 964
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 960
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 964
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 818
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 816
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtImprime: TBitBtn
        Tag = 5
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
    end
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT con.Codigo,  IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) C' +
        'OND'
      'FROM cond con'
      'LEFT JOIN entidades ent ON ent.Codigo = con.Cliente'
      'ORDER BY COND')
    Left = 556
    Top = 11
    object QrCondCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cond.Codigo'
    end
    object QrCondCOND: TWideStringField
      FieldName = 'COND'
      Size = 100
    end
  end
  object DsCond: TDataSource
    DataSet = QrCond
    Left = 584
    Top = 11
  end
  object QrPropriet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT imv.Propriet, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) P' +
        'ROP'
      'FROM condimov imv'
      'LEFT JOIN entidades ent ON ent.Codigo = imv.Propriet'
      'WHERE imv.Codigo=:P0'
      'ORDER BY PROP')
    Left = 612
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object a: TWideStringField
      FieldName = 'PROP'
      Origin = 'PROP'
      Size = 100
    end
    object QrProprietPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'condimov.Propriet'
      Required = True
    end
  end
  object DsPropriet: TDataSource
    DataSet = QrPropriet
    Left = 640
    Top = 11
  end
  object QrUHs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imv.Conta, imv.Unidade'
      'FROM condimov imv'
      'WHERE imv.Codigo=:P0')
    Left = 668
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUHsUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrUHsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
  end
  object DsUHs: TDataSource
    DataSet = QrUHs
    Left = 696
    Top = 11
  end
  object QrCondGri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM condgri'
      'ORDER BY Nome')
    Left = 500
    Top = 11
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cond.Codigo'
    end
    object QrCondGriNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCondGri: TDataSource
    DataSet = QrCondGri
    Left = 528
    Top = 11
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    DataSource = DsUHs
    SQL.Strings = (
      'SELECT *'
      'FROM condgri'
      'ORDER BY Nome')
    Left = 308
    Top = 323
    object QrPesqNivelLogin: TWideStringField
      FieldName = 'NivelLogin'
      Size = 12
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrPesqTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPesqCondGrupo: TWideStringField
      FieldName = 'CondGrupo'
      Size = 100
    end
    object QrPesqUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrPesqPropriet: TWideStringField
      FieldName = 'Propriet'
      Size = 100
    end
    object QrPesqDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPesqIP: TWideStringField
      FieldName = 'IP'
      Size = 64
    end
    object QrPesqACESSOS: TFloatField
      FieldName = 'ACESSOS'
    end
    object QrPesqCondGrupoCod: TIntegerField
      FieldName = 'CondGrupoCod'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 336
    Top = 323
  end
  object frxWEB_VISIT_001_001: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 41437.799783206020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 332
    Top = 384
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape2: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 593.386210000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 487.559370000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle de acessos WEB')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 699.213050000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 13.228346460000000000
        Top = 117.165430000000000000
        Width = 699.213050000000000000
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Width = 166.299197950000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Condom'#237'nio / Grupo')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 468.661720000000000000
          Width = 86.929133860000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 204.094620000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UH')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 260.787570000000000000
          Width = 207.874015750000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Propriet'#225'rio')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 555.590910000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Acessos')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 612.283860000000000000
          Width = 86.929133860000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IP')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Top = 257.008040000000000000
        Width = 699.213050000000000000
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 317.480520000000000000
        Width = 699.213050000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo13: TfrxMemoView
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 427.086890000000000000
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 196.535560000000000000
        Width = 699.213050000000000000
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 37.795300000000000000
          Width = 166.299197950000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'CondGrupo'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."CondGrupo"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 468.661720000000000000
          Width = 86.929133860000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataHora'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."DataHora"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 204.094620000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'UH'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."UH"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 260.787570000000000000
          Width = 207.874052360000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Propriet'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."Propriet"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 555.590910000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'ACESSOS'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."ACESSOS"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 612.283860000000000000
          Width = 86.929133860000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'IP'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."IP"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'CondGrupoCod'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."CondGrupoCod"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 18.897637800000000000
        Top = 154.960730000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsPesq."NivelLogin"'
        object Memo4: TfrxMemoView
          Width = 699.212927950000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#237'vel login: [frxDsPesq."NivelLogin"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Top = 234.330860000000000000
        Width = 699.213050000000000000
      end
    end
  end
  object frxDsPesq: TfrxDBDataset
    UserName = 'frxDsPesq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NivelLogin=NivelLogin'
      'Controle=Controle'
      'Usuario=Usuario'
      'Tipo=Tipo'
      'CondGrupo=CondGrupo'
      'UH=UH'
      'Propriet=Propriet'
      'DataHora=DataHora'
      'IP=IP'
      'ACESSOS=ACESSOS'
      'CondGrupoCod=CondGrupoCod')
    DataSet = QrPesq
    BCDToCurrency = False
    Left = 364
    Top = 323
  end
end
