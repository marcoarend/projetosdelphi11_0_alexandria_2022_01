unit FlxMensBalNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGridDAC, dmkPermissoes, UnDmkEnums;

type
  TFmFlxMensBalNew = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1A: TLabel;
    LaAviso2A: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    LaAviso1B: TLabel;
    LaAviso2B: TLabel;
    QrPesq: TmySQLQuery;
    QrPesqAnoMes: TIntegerField;
    QrCtrl: TmySQLQuery;
    QrCtrlFlxM_Conci: TSmallintField;
    QrCtrlFlxM_Docum: TSmallintField;
    QrCtrlFlxM_Contb: TSmallintField;
    QrCtrlFlxM_Anali: TSmallintField;
    QrCtrlFlxM_Encad: TSmallintField;
    QrCtrlFlxM_Entrg: TSmallintField;
    QrCndNDef: TmySQLQuery;
    QrCndNDefCodigo: TIntegerField;
    QrLocJa: TmySQLQuery;
    TbFlxCond: TmySQLTable;
    TbFlxCondCodigo: TIntegerField;
    TbFlxCondAtivo: TSmallintField;
    TbFlxCondNO_CND: TWideStringField;
    TbFlxCondFlxM_Ordem: TIntegerField;
    TbFlxCondFlxM_Conci: TSmallintField;
    TbFlxCondFlxM_Docum: TSmallintField;
    TbFlxCondFlxM_Contb: TSmallintField;
    TbFlxCondFlxM_Anali: TSmallintField;
    TbFlxCondFlxM_Encad: TSmallintField;
    TbFlxCondFlxM_Entrg: TSmallintField;
    DsFlxCond: TDataSource;
    Panel5: TPanel;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BitBtn1: TBitBtn;
    PB1: TProgressBar;
    DBGrid1: TdmkDBGridDAC;
    dmkPermissoes1: TdmkPermissoes;
    TbFlxCondFlxM_Web: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFlxCond(Cond: Integer);
    procedure AtualizaTodos(Ativa: Integer);
  public
    { Public declarations }
  end;

  var
  FmFlxMensBalNew: TFmFlxMensBalNew;

implementation

uses Module, UnMyObjects, ModuleGeral, FlxMensBalViw, Cond, UMySQLModule;

{$R *.DFM}

procedure TFmFlxMensBalNew.AtualizaTodos(Ativa: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE _FLX_COND_BAL_ SET Ativo=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Ativa;
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenFlxCond(TbFlxCondCodigo.Value);
end;

procedure TFmFlxMensBalNew.BitBtn1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    QrCndNDef.Close;
    QrCndNDef.Open;
    //
    QrCtrl.Close;
    QrCtrl.Open;
    //
    PB1.Position := 0;
    PB1.Max := QrCndNDef.RecordCount;
    while not QrCndNDef.Eof do
    begin
      if TbFlxCond.Locate('Codigo', QrCndNDefCodigo.Value, []) then
      begin
        PB1.Position := PB1.Position + 1;
        //
        TbFlxCond.Edit;
        TbFlxCondFlxM_Conci.Value := QrCtrlFlxM_Conci.Value;
        TbFlxCondFlxM_Docum.Value := QrCtrlFlxM_Docum.Value;
        TbFlxCondFlxM_Contb.Value := QrCtrlFlxM_Contb.Value;
        TbFlxCondFlxM_Anali.Value := QrCtrlFlxM_Anali.Value;
        TbFlxCondFlxM_Encad.Value := QrCtrlFlxM_Encad.Value;
        TbFlxCondFlxM_Entrg.Value := QrCtrlFlxM_Entrg.Value;
        TbFlxCond.Post;
      end;
      //
      QrCndNDef.Next;
    end;
  finally
    PB1.Position := 0;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFlxMensBalNew.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmFlxMensBalNew.BtOKClick(Sender: TObject);
  procedure IncluiCondominio(AnoMes, Codigo, Ano, Mes: Integer);
    function CriaData(Ano, Mes, Dia: Integer): TDateTime;
    begin
      if Dia = 0 then
        Result := 0
      else
        Result := EncodeDate(Ano, Mes, 1) + Dia - 1;
    end;
  var
    Ordem: Integer;
    Conci_DLim,
    Docum_DLim,
    Contb_DLim,
    Anali_DLim,
    Encad_DLim,
    Entrg_DLim,
    Web01_DLim,
    Abert_Data: TDateTime;
    Abert_User: Integer;
  begin
    Ordem      := TbFlxCondFlxM_Ordem.Value;
    Conci_DLim := CriaData(Ano, Mes, TbFlxCondFlxM_Conci.Value);
    Docum_DLim := CriaData(Ano, Mes, TbFlxCondFlxM_Docum.Value);
    Contb_DLim := CriaData(Ano, Mes, TbFlxCondFlxM_Contb.Value);
    Anali_DLim := CriaData(Ano, Mes, TbFlxCondFlxM_Anali.Value);
    Encad_DLim := CriaData(Ano, Mes, TbFlxCondFlxM_Encad.Value);
    Entrg_DLim := CriaData(Ano, Mes, TbFlxCondFlxM_Entrg.Value);
    Web01_DLim := CriaData(Ano, Mes, TbFlxCondFlxM_Web.Value);
    //
    Abert_User := VAR_USUARIO;
    Abert_Data := DModG.ObtemAgora();
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'flxmens', False, [
    'Ordem', 'Abert_User', 'Abert_Data',
    'Conci_DLim',
    'Docum_DLim',
    'Contb_DLim',
    'Anali_DLim',
    'Encad_DLim',
    'Entrg_DLim',
    'Web01_DLim'
    ], [
    'AnoMes', 'Codigo'], [
    Ordem, Abert_User, Abert_Data,
    Conci_DLim,
    Docum_DLim,
    Contb_DLim,
    Anali_DLim,
    Encad_DLim,
    Entrg_DLim,
    Web01_DLim
    ], [
    AnoMes, Codigo], True) then
  end;
var
  Ano, Mes, AnoMes: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Ano    := Geral.IMV(CBAno.Text);
    Mes    := CBMes.Itemindex + 1;
    //
    if (Ano < 2)  or (Mes < 1) then
    begin
      Application.MessageBox('Informe ano e m�s corretamente!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end else begin
      AnoMes := (Ano * 100) + Mes;
      //
      TbFlxCond.First;
      while not TbFlxCond.Eof do
      begin
        if TbFlxCondFlxM_Conci.Value +
           TbFlxCondFlxM_Docum.Value +
           TbFlxCondFlxM_Contb.Value +
           TbFlxCondFlxM_Anali.Value +
           TbFlxCondFlxM_Encad.Value +
           TbFlxCondFlxM_Entrg.Value > 0 then
        begin
          QrLocJa.Close;
          QrLocJa.Params[00].AsInteger := TbFlxCondCodigo.Value;
          QrLocJa.Params[01].AsInteger := AnoMes;
          QrLocJa.Open;
          //
          if QrLocJa.RecordCount = 0 then
          begin
            if TbFlxCondAtivo.Value = 1 then
              IncluiCondominio(AnoMes, TbFlxCondCodigo.Value, Ano, Mes);
          end;
        end;
        //
        TbFlxCond.Next;
      end;
    end;
    FmFlxMensBalViw.ReopenFlx(AnoMes, 0);
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFlxMensBalNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFlxMensBalNew.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmFlxMensBalNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFlxMensBalNew.FormCreate(Sender: TObject);
var
  i: Integer;
  //
  Ano, Mes: Word;
begin
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  //////////////////////////////////////////////////////////////////////////
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Max(AnoMes) AnoMes');
  QrPesq.SQL.Add('FROM flxmens');
  QrPesq.Open;
  if QrPesqAnoMes.Value > 0 then
  begin
    MLAGeral.MesEAnoDePeriodoShort(QrPesqAnoMes.Value, Mes, Ano);
    if Mes = 12 then
    begin
      Mes := 1;
      Ano := Ano + 1;
    end else begin
      Mes := Mes + 1;
    end;
    for i := 0 to CBAno.Items.Count do
    begin
      if CBAno.Items[i] = IntToStr(Ano) then
      begin
        CBAno.ItemIndex := i;
        Break;
      end;
    end;
    CBMes.ItemIndex := Mes -1;
  end;
  //
  TbFlxCond.Close;
  TbFlxCond.Database := DModG.MyPID_DB;
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS _FLX_COND_BAL_;');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.SQL.Add('CREATE TABLE _FLX_COND_BAL_');
  DModG.QrUpdPID1.SQL.Add('SELECT cnd.Codigo, cnd.Ativo,');
  DModG.QrUpdPID1.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CND,');
  DModG.QrUpdPID1.SQL.Add('cnd.FlxM_Ordem, cnd.FlxM_Conci,');
  DModG.QrUpdPID1.SQL.Add('cnd.FlxM_Docum, cnd.FlxM_Contb, cnd.FlxM_Anali,');
  DModG.QrUpdPID1.SQL.Add('cnd.FlxM_Encad, cnd.FlxM_Entrg, cnd.FlxM_Web ');
  DModG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.cond cnd');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=cnd.Cliente');
  DModG.QrUpdPID1.SQL.Add('WHERE cnd.Ativo=1;');
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenFlxCond(0);
end;

procedure TFmFlxMensBalNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1A, LaAviso2A, LaAviso1B, LaAviso2B], Caption, False, taCenter, 2, 10, 20);
end;

procedure TFmFlxMensBalNew.ReopenFlxCond(Cond: Integer);
begin
  TbFlxCond.Close;
  TbFlxCond.Open;
  //
  TbFlxCond.Locate('Codigo', Cond, []);
end;

end.
