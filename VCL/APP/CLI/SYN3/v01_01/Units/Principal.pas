// ini Delphi 28 Alexandria
//{$I dmk.inc}
// fim Delphi 28 Alexandria


unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, UMySQLModule, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*)
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, Mask, DBCtrls, Tabs, dmkGeral, dmkDBGrid, UnLic_Dmk,
  DockTabSet,
  //AdvToolBar, AdvGlowButton, AdvMenus, AdvToolBarStylers,
  //AdvShapeButton, AdvPreviewMenu, AdvOfficeHint,
  dmkEdit, UnMyObjects, dmkDBLookupComboBox, dmkEditCB, dmkEditDateTimePicker,
  ImgList, dmkRadioGroup, dmkDBGridDAC, Clipbrd, frxClass, DmkDAC_PF, (*Codebase,*)
  ActiveX, AxSms_TLB, // TLB da ActiveExperts > http://www.activexperts.com
  UnDmkProcFunc, dmkLed, OleCtrls, SHDocVw, UnDmkEnums, Vcl.Imaging.pngimage,
  ShellApi, dmkPageControl, UnitNotificacoes, UnitNotificacoesEdit, UnGrl_Vars;

type
  TMember = record
    Name : string;
    eMail : string;
    Posts : Cardinal;
  end;
  TcpCalc = (cpJurosMes, cpMulta);
  TFmPrincipal = class(TForm)
    OpenPictureDialog1: TOpenPictureDialog;
    Timer1: TTimer;
    PMProdImp: TPopupMenu;
    Estoque1: TMenuItem;
    Histrico1: TMenuItem;
    Vendas1: TMenuItem;
    PMGeral_: TPopupMenu;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    DsCarteiras: TDataSource;
    OpenDialog1: TOpenDialog;
    AdvPMSkin: TPopupMenu;
    Escolher2: TMenuItem;
    Padro2: TMenuItem;
    AdvPMImagem: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    Estilo2: TMenuItem;
    Centralizado1: TMenuItem;
    ManterProporo1: TMenuItem;
    ManterAltura1: TMenuItem;
    ManterLargura1: TMenuItem;
    Nenhum1: TMenuItem;
    Espichar1: TMenuItem;
    Repetido1: TMenuItem;
    AdvPMMenuCor: TPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvToolBarOfficeStyler1: TPanel;
    AdvPreviewMenu1: TPopupMenu;
    Memo3: TMemo;
    Automatico1: TMenuItem;
    AdvPMTextos: TPopupMenu;
    Montagemdecontratos2: TMenuItem;
    rechosdecontrato2: TMenuItem;
    Desativar1: TMenuItem;
    PageControl1: TdmkPageControl;
    TabSheet1: TTabSheet;
    QrDupl: TmySQLQuery;
    QrDuplITENS: TLargeintField;
    QrDuplConta: TIntegerField;
    QrDuplNOMECONTA: TWideStringField;
    QrDupi: TmySQLQuery;
    QrDupiCodigo: TIntegerField;
    QrDupiNome: TWideStringField;
    DsDupl: TDataSource;
    DsDupi: TDataSource;
    StatusBar: TStatusBar;
    Panel1: TPanel;
    LaAviso: TLabel;
    PB1: TProgressBar;
    N2: TMenuItem;
    extos1: TMenuItem;
    AdvPMVerificaBD: TPopupMenu;
    VerificaBDServidor1: TMenuItem;
    VerificaBDWeb1: TMenuItem;
    APMExtratos: TPopupMenu;
    PagarReceber1: TMenuItem;
    Movimento1: TMenuItem;
    ResultadosMensais1: TMenuItem;
    APMPesquisas: TPopupMenu;
    PorNveldoPLanodeContas1: TMenuItem;
    ContasControladas1: TMenuItem;
    ContasSazonais1: TMenuItem;
    APMSaldos: TPopupMenu;
    Futuros1: TMenuItem;
    Em1: TMenuItem;
    PMGeral: TPopupMenu;
    UnicaEmpresa1: TMenuItem;
    MultiplasEmpresas1: TMenuItem;
    MovimentoPlanodecontas1: TMenuItem;
    Timer2: TTimer;
    QrCond2: TmySQLQuery;
    QrCond2NOMECLI: TWideStringField;
    QrCond2Codigo: TIntegerField;
    QrCond2Cliente: TIntegerField;
    DsCond2: TDataSource;
    AdvPMRetornoCNAB: TPopupMenu;
    Bancos1: TMenuItem;
    QrLoc: TmySQLQuery;
    QrLoc2: TmySQLQuery;
    Modelo20031: TMenuItem;
    Modelo20121: TMenuItem;
    TySuporte: TTrayIcon;
    TmSuporte: TTimer;
    VerificaBDGlobal1: TMenuItem;
    TextosdeSMS1: TMenuItem;
    ConfiguraesdeEnviodeMensagens1: TMenuItem;
    GB_Cartas: TGroupBox;
    Panel19: TPanel;
    Shape1: TShape;
    LaCartas1A: TLabel;
    LaCartas1B: TLabel;
    LaCartas1C: TLabel;
    Panel20: TPanel;
    BtImprimeCartas: TBitBtn;
    QrCond: TmySQLQuery;
    QrCondCodigo: TIntegerField;
    QrCondNO_COND: TWideStringField;
    APMInflIdx: TPopupMenu;
    IGPDIFGV1: TMenuItem;
    INPCIBGE1: TMenuItem;
    N4: TMenuItem;
    Composiesdendices1: TMenuItem;
    AdvToolBarPagerNovo: TdmkPageControl;
    AdvToolBarPager21: TTabSheet;
    AdvToolBarPager22: TTabSheet;
    AdvToolBarPager23: TTabSheet;
    AdvPage5: TTabSheet;
    AdvPage6: TTabSheet;
    AdvPage7: TTabSheet;
    AdvPage8: TTabSheet;
    AdvPage9: TTabSheet;
    ATBEmp1: TPanel;
    AGBAcesRapid: TBitBtn;
    AGBGerCond: TBitBtn;
    AGBCondCad1: TBitBtn;
    ATBConsumidor: TPanel;
    AGBPesqPag: TBitBtn;
    AGBGerProp: TBitBtn;
    AGBAnalBloq: TBitBtn;
    AdvToolBar19: TPanel;
    AGBRetCNAB: TBitBtn;
    AdvToolBar27: TPanel;
    AGBLivre1_6_1: TBitBtn;
    AGBLivre1_6_2: TBitBtn;
    AGBLivre1_6_3: TBitBtn;
    AdvToolBar24: TPanel;
    AGBDirCNAB: TBitBtn;
    AGBCNABCampos: TBitBtn;
    AdvToolBar13: TPanel;
    AGBFinEncerMes: TBitBtn;
    AdvToolBar5: TPanel;
    AGBFinCart: TBitBtn;
    AdvGlowButton19: TBitBtn;
    AGBPlanRelCab: TBitBtn;
    AdvToolBar7: TPanel;
    AGBFinLista: TBitBtn;
    AGBFinSaldos: TBitBtn;
    AdvGlowButton53: TBitBtn;
    AdvToolBar6: TPanel;
    AGBFinNiv: TBitBtn;
    AGBFinCta: TBitBtn;
    AGBSubGru: TBitBtn;
    AGBFinGru: TBitBtn;
    AGBFinCjt: TBitBtn;
    AGBFinPlano: TBitBtn;
    AGBAgrExtrCta: TBitBtn;
    AGBLinkConcBco: TBitBtn;
    AGBSumValCta: TBitBtn;
    AdvToolBar20: TPanel;
    AGBNFSe_Edit: TBitBtn;
    AGBNFSeLRpsC: TBitBtn;
    AGBNFSe_RPSPesq: TBitBtn;
    AGBNFSe_NFSePesq: TBitBtn;
    AGBNFSeFatCab: TBitBtn;
    AdvToolBar30: TPanel;
    AGBNFSeSrvCad: TBitBtn;
    AGBNFSeMenCab: TBitBtn;
    AdvToolBar31: TPanel;
    AGBTabsSPEDEFD: TBitBtn;
    AGBLaySPEDEFD: TBitBtn;
    AGB_DTB_BACEN: TBitBtn;
    AGBListServ: TBitBtn;
    AGBErrosAlertas: TBitBtn;
    AdvGlowButton5: TBitBtn;
    AdvToolBar15: TPanel;
    BtBalancete: TBitBtn;
    BtReceDesp: TBitBtn;
    AdvGlowButton24: TBitBtn;
    AdvGlowButton88: TBitBtn;
    AdvGlowButton89: TBitBtn;
    AdvGlowButton90: TBitBtn;
    AdvGlowButton94: TBitBtn;
    AdvGlowButton95: TBitBtn;
    AGBExtratos: TBitBtn;
    AGBPesquisas: TBitBtn;
    AGBSaldos: TBitBtn;
    AdvGlowButton1: TBitBtn;
    AdvGlowButton7: TBitBtn;
    AdvToolBar29: TPanel;
    AGBListUHs: TBitBtn;
    AdvGlowButton92: TBitBtn;
    AdvGlowButton93: TBitBtn;
    GBMoradores: TBitBtn;
    AdvToolBar12: TPanel;
    AGBCustomFR3: TBitBtn;
    AGBFrx3Imp: TBitBtn;
    AGBDesignMode: TBitBtn;
    AdvToolBar9: TPanel;
    AGMBBackup: TBitBtn;
    AdvGlowButton55: TBitBtn;
    AdvGlowMenuButton5: TBitBtn;
    AdvToolBar23: TPanel;
    AdvGlowButton41: TBitBtn;
    AdvGlowButton42: TBitBtn;
    AdvToolBar3: TPanel;
    AdvGlowButton82: TBitBtn;
    AdvGlowButton83: TBitBtn;
    AdvGlowButton84: TBitBtn;
    AdvGlowButton3: TBitBtn;
    AdvGlowButton26: TBitBtn;
    AdvGlowButton30: TBitBtn;
    AdvToolBar14: TPanel;
    AdvGlowButton4: TBitBtn;
    AdvToolBar16: TPanel;
    AdvGlowButton8: TBitBtn;
    AdvPMAtualiz: TPopupMenu;
    Migraentidadesparaoformatonovo1: TMenuItem;
    AdvToolBar18: TPanel;
    AdvPMVisitasWEB: TPopupMenu;
    Controledevisitas1: TMenuItem;
    Controledevisitas21: TMenuItem;
    AdvPage1: TTabSheet;
    AdvToolBar10: TPanel;
    AdvGlowButton17: TBitBtn;
    AdvGlowButton16: TBitBtn;
    AdvGlowButton58: TBitBtn;
    AGBFiliais: TBitBtn;
    AdvGlowButton9: TBitBtn;
    AdvGlowButton10: TBitBtn;
    AdvToolBar11: TPanel;
    AdvGlowMenuButton3: TBitBtn;
    AdvGlowMenuButton4: TBitBtn;
    AGBTheme: TBitBtn;
    AGBGrupUHs2: TBitBtn;
    BalloonHint1: TBalloonHint;
    AdvGlowButton12: TBitBtn;
    AdvToolBar17: TPanel;
    AdvGlowMenuButton1: TBitBtn;
    AdvPMCadastros: TPopupMenu;
    Perfis2: TMenuItem;
    JanelasWEB1: TMenuItem;
    AdvToolBar33: TPanel;
    AdvGlowButton13: TBitBtn;
    AdvGlowButton21: TBitBtn;
    AdvGlowButton87: TBitBtn;
    AdvGlowButton97: TBitBtn;
    AdvGlowButton20: TBitBtn;
    Avisos1: TMenuItem;
    AdvToolBar34: TPanel;
    AGBRepaLoc: TBitBtn;
    AGBBloqParc: TBitBtn;
    N6: TMenuItem;
    Consultaprotocolos1: TMenuItem;
    N5: TMenuItem;
    AdvToolBar35: TPanel;
    AdvGlowButton6: TBitBtn;
    AdvGlowButton25: TBitBtn;
    AdvPage2: TTabSheet;
    AdvToolBar8: TPanel;
    AdvGlowButton11: TBitBtn;
    AdvPage3: TTabSheet;
    AdvToolBar36: TPanel;
    AdvToolBar37: TPanel;
    AdvGlowButton15: TBitBtn;
    AdvGlowButton18: TBitBtn;
    PageControl4: TPageControl;
    TabSheet6: TTabSheet;
    ShapeFill: TShape;
    RgSitCobr: TRadioGroup;
    ImgDescanso: TImage;
    TabSheet15: TTabSheet;
    PageControl3: TPageControl;
    TabSheet10: TTabSheet;
    Panel11: TPanel;
    Panel10: TPanel;
    Label6: TLabel;
    BtAtzListaMailProt: TBitBtn;
    DBEdEmeioProtTot: TDBEdit;
    BtReenvia: TBitBtn;
    DBGEmeioProt: TdmkDBGrid;
    DBGEmeioProtPak: TdmkDBGrid;
    DBGEmeioProtPakIts: TdmkDBGrid;
    MeAvisos: TMemo;
    Panel18: TPanel;
    LaAviso5: TLabel;
    LaAviso4: TLabel;
    PB2: TProgressBar;
    TabSheet12: TTabSheet;
    Panel14: TPanel;
    GradePTK: TdmkDBGrid;
    Panel17: TPanel;
    Label11: TLabel;
    RGProtoFiltro: TRadioGroup;
    EdEmpresa: TdmkEdit;
    DBEdit2: TDBEdit;
    Panel15: TPanel;
    Panel16: TPanel;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    BitBtn19: TBitBtn;
    GradePPI: TDBGrid;
    TabSheet13: TTabSheet;
    GradePKA: TDBGrid;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    BtRefresh: TBitBtn;
    TabSheet3: TTabSheet;
    Panel4: TPanel;
    ProgressBar1: TProgressBar;
    MeTbInuteis: TMemo;
    Memo1: TMemo;
    Memo2: TMemo;
    TabSheet4: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Button6: TButton;
    RGCNAB: TRadioGroup;
    Panel8: TPanel;
    Button7: TButton;
    Memo4: TMemo;
    TabSheet7: TTabSheet;
    Panel9: TPanel;
    DBGrid3: TDBGrid;
    DBGrid4: TDBGrid;
    TabSheet8: TTabSheet;
    Panel13: TPanel;
    BtMenu: TBitBtn;
    DBGSemMez: TdmkDBGrid;
    TabSheet11: TTabSheet;
    Panel12: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdComando: TEdit;
    EdResposta: TEdit;
    MeResposta: TdmkEdit;
    BtComando: TButton;
    BtRefrPrto: TButton;
    BtNegrito: TButton;
    BtNormal: TButton;
    TabSheet9: TTabSheet;
    Memo6: TMemo;
    Panel21: TPanel;
    Button3: TButton;
    AdvToolBar1: TPanel;
    AGBArreBase: TBitBtn;
    AGBProvBase: TBitBtn;
    AGBConsLeit: TBitBtn;
    AGBEntiCad: TBitBtn;
    AdvToolBar25: TPanel;
    AGBProtocolos2: TBitBtn;
    AGBMotNaoEntr: TBitBtn;
    AGBOcorEntr: TBitBtn;
    AGBConfImpBlq: TBitBtn;
    AdvToolBar4: TPanel;
    AGBTxtEMail: TBitBtn;
    AGBBancos: TBitBtn;
    AGBAnota: TBitBtn;
    AGBTextos: TBitBtn;
    AGBFeriados: TBitBtn;
    AGBStatus: TBitBtn;
    AGBInflIdx: TBitBtn;
    AGBFolhaFunci: TBitBtn;
    AdvToolBar26: TPanel;
    AGBDiaEven: TBitBtn;
    AGBDiaGer: TBitBtn;
    AGBDiaAss: TBitBtn;
    AGBHistCons: TBitBtn;
    AdvToolBar22: TPanel;
    AGBImpBal: TBitBtn;
    AdvGlowButton14: TBitBtn;
    AdvToolBar28: TPanel;
    AGBFlxEdit: TBitBtn;
    AGBFlxPanBlq: TBitBtn;
    AGBFlxPanBal: TBitBtn;
    AdvToolBar2: TPanel;
    AGBExpCobr: TBitBtn;
    AGBProtocolos1: TBitBtn;
    AGBRemConfig: TBitBtn;
    AdvGlowButton2: TBitBtn;
    AdvToolBar21: TPanel;
    AdvGlowButton22: TBitBtn;
    AdvGlowButton23: TBitBtn;
    AdvGlowButton27: TBitBtn;
    TmVersao: TTimer;
    AdvGlowButton57: TBitBtn;
    TimerIdle: TTimer;
    AdvGlowButton167: TBitBtn;
    Panel5: TPanel;
    AdvShapeButton1: TSpeedButton;
    AdvToolBarButton6: TBitBtn;
    AdvToolBarButton5: TBitBtn;
    AdvToolBarButton7: TBitBtn;
    AdvToolBarButton1: TBitBtn;
    AdvGlowMenuButton2: TBitBtn;
    AdvToolBarButton8: TBitBtn;
    AdvToolBarButton2: TBitBtn;
    AdvToolBarButton10: TBitBtn;
    AdvToolBarButton12: TBitBtn;
    LaAviso2: TLabel;
    LaAviso3: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SBCadEntidadesClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Saldodecontas1Click(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure Escolher2Click(Sender: TObject);
    procedure Padro2Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure AGBEntiCadClick(Sender: TObject);
    procedure AGBFinPlanoClick(Sender: TObject);
    procedure AGBFinCjtClick(Sender: TObject);
    procedure AGBFinGruClick(Sender: TObject);
    procedure AGBSubGruClick(Sender: TObject);
    procedure AGBFinCtaClick(Sender: TObject);
    procedure AGBFinNivClick(Sender: TObject);
    procedure AGBFinListaClick(Sender: TObject);
    procedure AGBFinSaldosClick(Sender: TObject);
    procedure AGBFinCartClick(Sender: TObject);
    procedure AdvGlowButton16Click(Sender: TObject);
    procedure AdvGlowButton58Click(Sender: TObject);
    procedure AGMBBackupClick(Sender: TObject);
    procedure AGBTxtEMailClick(Sender: TObject);
    procedure AGBBancosClick(Sender: TObject);
    procedure AGBAnotaClick(Sender: TObject);
    procedure Montagemdecontratos2Click(Sender: TObject);
    procedure rechosdecontrato2Click(Sender: TObject);
    procedure Desativar1Click(Sender: TObject);
    procedure CkPTK_AbertosClick(Sender: TObject);
    procedure GradePPICellClick(Column: TColumn);
    procedure GradePPIDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure GradePKACellClick(Column: TColumn);
    procedure GradePKADrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtRefreshClick(Sender: TObject);
    procedure BtComandoClick(Sender: TObject);
    procedure BtRefrPrtoClick(Sender: TObject);
    procedure BtNegritoClick(Sender: TObject);
    procedure BtNormalClick(Sender: TObject);
    procedure QrDuplAfterScroll(DataSet: TDataSet);
    procedure AGBAcesRapidClick(Sender: TObject);
    procedure AGBGerCondClick(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure AGBFinEncerMesClick(Sender: TObject);
    procedure BtBalanceteClick(Sender: TObject);
    procedure BtReceDespClick(Sender: TObject);
    procedure AdvGlowButton20Click(Sender: TObject);
    procedure AdvGlowButton21Click(Sender: TObject);
    procedure AGBBloqParcClick(Sender: TObject);
    procedure AdvGlowButton26Click(Sender: TObject);
    procedure AGBProtocolos1Click(Sender: TObject);
    procedure RGProtoFiltroClick(Sender: TObject);
    procedure AGBGerPropClick(Sender: TObject);
    procedure AGBRetCNABClick(Sender: TObject);
    procedure AGBArreBaseClick(Sender: TObject);
    procedure AGBProvBaseClick(Sender: TObject);
    procedure AGBConsLeitClick(Sender: TObject);
    procedure AGBImpBalClick(Sender: TObject);
    procedure AGBHistConsClick(Sender: TObject);
    procedure AdvGlowButton24Click(Sender: TObject);
    procedure AGBRepaLocClick(Sender: TObject);
    procedure AdvGlowButton42Click(Sender: TObject);
    procedure AdvGlowButton41Click(Sender: TObject);
    procedure AGBAgrExtrCtaClick(Sender: TObject);
    procedure AGBLinkConcBcoClick(Sender: TObject);
    procedure AGBFeriadosClick(Sender: TObject);
    procedure AGBDirCNABClick(Sender: TObject);
    procedure AGBRemConfigClick(Sender: TObject);
    procedure AGBCNABCamposClick(Sender: TObject);
    procedure AGBProtocolos2Click(Sender: TObject);
    procedure AGBMotNaoEntrClick(Sender: TObject);
    procedure AGBOcorEntrClick(Sender: TObject);
    procedure AGBConfImpBlqClick(Sender: TObject);
    procedure extos1Click(Sender: TObject);
    procedure AGBDiaGerClick(Sender: TObject);
    procedure AGBDiaAssClick(Sender: TObject);
    procedure AGBFolhaFunciClick(Sender: TObject);
    procedure VerificaBDServidor1Click(Sender: TObject);
    procedure VerificaBDWeb1Click(Sender: TObject);
    procedure AdvGlowButton82Click(Sender: TObject);
    procedure AdvGlowButton83Click(Sender: TObject);
    procedure AGBAnalBloqClick(Sender: TObject);
    procedure AdvGlowButton88Click(Sender: TObject);
    procedure AdvGlowButton89Click(Sender: TObject);
    procedure AdvGlowButton90Click(Sender: TObject);
    procedure AGBListUHsClick(Sender: TObject);
    procedure AdvGlowButton95Click(Sender: TObject);
    procedure AdvGlowButton94Click(Sender: TObject);
    procedure AdvGlowButton93Click(Sender: TObject);
    procedure AdvGlowButton92Click(Sender: TObject);
    procedure AdvGlowButton30Click(Sender: TObject);
    procedure GBMoradoresClick(Sender: TObject);
    procedure AGBStatusClick(Sender: TObject);
    procedure AGBExpCobrClick(Sender: TObject);
    procedure ResultadosMensais1Click(Sender: TObject);
    procedure Movimento1Click(Sender: TObject);
    procedure ContasControladas1Click(Sender: TObject);
    procedure ContasSazonais1Click(Sender: TObject);
    procedure Futuros1Click(Sender: TObject);
    procedure Em1Click(Sender: TObject);
    procedure AGBThemeClick(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure UnicaEmpresa1Click(Sender: TObject);
    procedure MultiplasEmpresas1Click(Sender: TObject);
    procedure MovimentoPlanodecontas1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure EdEmpresaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdEmpresaChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AGBPlanRelCabClick(Sender: TObject);
    procedure AGBGrupUHs2Click(Sender: TObject);
    procedure AGBFlxEditClick(Sender: TObject);
    procedure AGBFlxPanBlqClick(Sender: TObject);
    procedure AGBFlxPanBalClick(Sender: TObject);
    procedure Bancos1Click(Sender: TObject);
    procedure AGBDesignModeClick(Sender: TObject);
    procedure AGBCustomFR3Click(Sender: TObject);
    procedure AGBFrx3ImpClick(Sender: TObject);
    procedure AdvGlowButton53Click(Sender: TObject);
    procedure Modelo20031Click(Sender: TObject);
    procedure Modelo20121Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AdvGlowButton84Click(Sender: TObject);
    procedure AdvGlowButton55Click(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure AdvGlowButton4Click(Sender: TObject);
    procedure AGBNFSe_EditClick(Sender: TObject);
    procedure AGBNFSe_NFSePesqClick(Sender: TObject);
    procedure AGBNFSe_RPSPesqClick(Sender: TObject);
    procedure AGBNFSeFatCabClick(Sender: TObject);
    procedure AGBNFSeLRpsCClick(Sender: TObject);
    procedure AGBNFSeSrvCadClick(Sender: TObject);
    procedure AGBNFSeMenCabClick(Sender: TObject);
    procedure AGBTabsSPEDEFDClick(Sender: TObject);
    procedure AGBLaySPEDEFDClick(Sender: TObject);
    procedure AGB_DTB_BACENClick(Sender: TObject);
    procedure AGBListServClick(Sender: TObject);
    procedure AdvGlowButton5Click(Sender: TObject);
    procedure AGBErrosAlertasClick(Sender: TObject);
    procedure VerificaBDGlobal1Click(Sender: TObject);
    procedure AGBFiliaisClick(Sender: TObject);
    procedure AdvGlowButton7Click(Sender: TObject);
    procedure BtReenviaClick(Sender: TObject);
    procedure BtAtzListaMailProtClick(Sender: TObject);
    procedure TextosdeSMS1Click(Sender: TObject);
    procedure ConfiguraesdeEnviodeMensagens1Click(Sender: TObject);
    procedure BtImprimeCartasClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure AGBLivre1_6_1Click(Sender: TObject);
    procedure AGBLivre1_6_2Click(Sender: TObject);
    procedure AGBSumValCtaClick(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure IGPDIFGV1Click(Sender: TObject);
    procedure INPCIBGE1Click(Sender: TObject);
    procedure Composiesdendices1Click(Sender: TObject);
    procedure AdvToolBarButton6Click(Sender: TObject);
    procedure AdvToolBarButton5Click(Sender: TObject);
    procedure AdvToolBarButton7Click(Sender: TObject);
    procedure AdvToolBarButton1Click(Sender: TObject);
    procedure AdvToolBarButton8Click(Sender: TObject);
    procedure AdvGlowButton87Click(Sender: TObject);
    procedure AdvGlowButton97Click(Sender: TObject);
    procedure Migraentidadesparaoformatonovo1Click(Sender: TObject);
    procedure Controledevisitas1Click(Sender: TObject);
    procedure Controledevisitas21Click(Sender: TObject);
    procedure AdvGlowButton25Click(Sender: TObject);
    procedure AGBPesqPagClick(Sender: TObject);
    procedure AdvToolBarButton2Click(Sender: TObject);
    procedure AdvToolBarButton10Click(Sender: TObject);
    procedure AdvGlowButton12Click(Sender: TObject);
    procedure Perfis2Click(Sender: TObject);
    procedure JanelasWEB1Click(Sender: TObject);
    procedure AdvGlowButton13Click(Sender: TObject);
    procedure Avisos1Click(Sender: TObject);
    procedure Consultaprotocolos1Click(Sender: TObject);
    procedure AdvToolBarButton12Click(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AdvGlowButton18Click(Sender: TObject);
    procedure AdvGlowButton15Click(Sender: TObject);
    procedure AGBDiaEvenClick(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AdvGlowButton23Click(Sender: TObject);
    procedure AdvGlowButton22Click(Sender: TObject);
    procedure AdvGlowButton27Click(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure AdvGlowButton57Click(Sender: TObject);
    procedure sd1FormSkin(Sender: TObject; aName: string; var DoSkin: Boolean);
    procedure TimerIdleTimer(Sender: TObject);
    procedure AdvGlowButton167Click(Sender: TObject);
  private
    { Private declarations }
    FALiberar: Boolean;
    procedure CadastroDeAnot;
    //
    procedure MostraOpcoes;
    procedure MostraOpcoesSyndic;
    procedure MostraLogoff;
    procedure MostraMatriz();
    procedure SkinMenu(Index: integer);
    //
    procedure VerificaOrelhas();
    procedure DefineCondominio(Periodo: Integer; Gerencia: Boolean);
    procedure MostraFiliais();
    procedure MostraFormIGPDI_FGV();
    procedure MostraFormINPC_IBGE();
    procedure MostraUploads(Condominio: Integer; Arquivo, Descricao: String;
              ExcluiArqOrigem: Boolean);
    procedure SincronizarTabelasWeb();
    procedure MostraLctGer2();
  public
    { Public declarations }
    FLinModErr: Integer;
    FIncI: Integer;
    FBMPDescanso: TBitmap;
    FJaAbriuFmCondGer: Boolean;
    FAtualizouFavoritos: Boolean;
    FCliInt_, FEntInt: Integer;
    FTipoNovoEnti: Integer;
    FDatabase1A, FDatabase1B, FDatabase2A, FDatabase2B: TmySQLDatabase;
    FDuplicata, FCheque, FVerArqSCX, FCliInt: Integer;
    FImagemDescanso: String;
    FImportPath: String;
    FContasMesEmiss: Integer;
    FTemModuloWEB, FDmodCriado: Boolean;

    procedure AcoesIniciaisDoAplicativo();
    function  AdicionaMinutosSQL(HI, HF: TTime): Boolean;
    procedure AtualizaTextoBtAcessoRapido();
    procedure CalculaCambioAlterado(MyProgress: TProgressBar);
    procedure DefineVarsCliInt(CliInt: Integer);
    procedure HistoricoConsumo();
    procedure MostraFmCondGer(CondAnterior: Boolean; Periodo: Integer);
    procedure MostraWClients2(Codigo: Integer);
    procedure MostraCondGri(Codigo: Integer);
    function  PreparaMinutosSQL(): Boolean;
    procedure RetornoCNAB();
    procedure RetornoCNABBancos();
    procedure ShowHint(Sender: TObject);
    procedure MyOnHint(Sender: TObject);
    procedure SelecionaImagemdefundo;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid; Entidade: Integer;
              Aba: Boolean = False);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade1: TStringGrid);
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    procedure AjustaEstiloImagem(Index: integer);
    procedure CadastroDeProtocolos(Lote: Integer; Codigo: Integer = 0);
    procedure CadastroDeCondominio(Codigo: Integer; CondBloco: Integer = 0;
                CondImov: Integer = 0; Aba1: Integer = 0; Aba2: Integer = 0;
                Aba3: Integer = 0);
    procedure CadastroDeFeriados();
    procedure VerificaEmissoesDeContasMensais();

    // Cashier
    function CartaoDeFatura: Integer;
    function CompensacaoDeFatura: String;
    function AcaoEspecificaDeApp(Servico: String): Boolean;
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    function VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;

    procedure MostraFormPreEmail();
    procedure MostraFormTxtSMS();
    procedure MostraFormCarta_App();
    procedure MostraFormDiarCEMCad();
    procedure MostraFormImportSalCfgCab(Codigo: Integer);
    //
    function  MigraEspecificos(CliInt: Integer): Boolean;
    procedure FormImprimeImoveis(Condominio: Integer);
    //
    procedure ReparcelamentosdeBloquetos_Local(Codigo: Integer);
    procedure ReCaptionComponentesDeForm(Form: TForm);
    procedure MostraBloqParcAviso(Parcelamentos: Integer);
    procedure MostraBloqParc(Codigo: Integer);
    procedure MostraLctFisico(Entidade, Empresa, Carteira: Integer);
    procedure InadimplenciaNew(Empresa: Integer);
    procedure MostraCfgInfl(Codigo: Integer);
    procedure MostraCNAB_Cfg(Codigo: Integer);
    //
    function  TentaDefinirDiretorio(const Empresa: Integer; const Pasta:
              String; var Dir: String): Boolean;
    //
    function  ConverteWPerfilToWPerfilSyndic(Perfil: Integer): Integer;
    function  ConverteWPerfilSyndicToWPerfil(Perfil: Integer): Integer;
    //Notifica��es
    function  NotificacoesVerifica(QueryNotifi: TmySQLQuery; DataHora: TDateTime): Boolean;
    procedure NotificacoesJanelas(TipoNotifi: TNotifi; DataHora: TDateTime);
    function  FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;
  end;
var
  FmPrincipal: TFmPrincipal;
  // Clipboard
  NextInChain  : THandle;
  DelphiGuide  : TMember;
  OurFormat    : Integer;
  MemberHandle : THandle;

implementation

uses Cond, Module, Entidades, VerifiDB, MyGlyfs, MyDBCheck, Syndic_MLA, Opcoes,
  MyListas, MyVCLSkin, ChConfigCab, UCreate, Backup3, UnMyPrinters, PertoChek,
  UnFinanceiro, UnFinanceiroJan, CalcPercent, Bancos, Cartas, CartaG,
  ModuleGeral, Entidade2, Matriz, Anotacoes, PreEmail, OpcoesSyndic, InfoSeq,
  Feriados, ModuleCond, CondSel, ModuleLct2, CondGer, ModuleBloq, ModuleFin,
  CondImovImp, ModuleAtencoes, BloqParc, WSincro2, bloqparcDel, UnProtocoUnit,
  CertidaoNeg, Reparc, CNAB_Cfg, PesqCond, Promissoria, Duplicata1, FpFunci,
  CNAB_Dir, CNAB_Fld, CNAB_Ret2a, LeituraImp, Inadimp2, UnDiario_Jan,
  BloqAnalisa, LeDataArq, CalcMod10e11, CondGerCarne, Maladireta, Imprime,
  VisitasCli, CondFichaCad, Status, FavoritosG, Produsys2, ContasConfEmisAll,
  LinkRankSkin, BloqParcAviso, CondGri, FlxMensSel, FlxMensBlqViw, FlxMensBalViw,
  CNAB_RetBco, CustomFR3, CustomFR3Imp, CodBarraRead, UnDmkWeb, UnDmkWeb_Jan,
  UnBloquetosCond, UnBloquetos_Jan, ServicoManager, CNAE21Cad, NFeLoadTabs,
  NFSe_PF_0000, DMKpnfsConversao, NFSe_PF_0201, NFSe_LoadXML_0201, SPED_EFD_Tabelas,
  VerifiDBTerceiros, ParamsEmp, PrevImp, TxtSMS, CfgCadLista, DiarCEMCad,
  CobrarGer, ModCobranca, SMSCommands, SMSRecebe, ImportSalCfgCab, UnBloqGerl_Jan,
  VerifiBloquetosValor, CondGerArreFut, IGPDI, INPC, CfgInfl, VisitasCli2,
  UnEntities, WOpcoes, DirWeb, Uploads, About, WClients2, UnWUsersJan, UnFixBugs,
  UnGrl_Geral, UnDmkWeb2_Jan;

const
  FAltLin = CO_POLEGADA / 2.16;

{$R *.DFM}

function TFmPrincipal.FixBugAtualizacoesApp(FixBug: Integer;
  FixBugStr: String): Boolean;
begin
  Result := True;
  (* Marcar quando for criar uma fun��o de atualiza��o
  try
    if FixBug = 0 then
      Atualiza��o1()
    else if FixBug = 1 then
      Atualiza��o2()
    else if FixBug = 2 then
      Atualiza��o3()
    else
      Result := False;
  except
    Result := False;
  end;
  *)
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
var
  Txt_CO, Txt_FI: String;
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente;
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  //
  Txt_CO := Geral.VersaoTxt2006(CO_VERSAO);
  Txt_FI := Geral.FileVerInfo(Application.ExeName, 3 (*Versao*));
  if Txt_CO <> Txt_FI then
    Geral.MB_Aviso('Vers�o difere do arquivo!' + #13#10 +
    'Vers�o configurada: ' + Txt_CO + #13#10 +
    'Vers�o do arquivo : ' + Txt_FI);
  if not FALiberar then Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
  //MemberPointer  : ^TMember;
begin
  FAtualizouFavoritos := False;
  //
  (*
  AdvToolBarPager22.TabVisible := False;
  AdvToolBarPager21.TabVisible := False;
  TabSheet15.TabVisible        := False;
  TabSheet2.TabVisible         := False;
  TabSheet3.TabVisible         := False;
  TabSheet4.TabVisible         := False;
  TabSheet7.TabVisible         := False;
  TabSheet8.TabVisible         := False;
  TabSheet11.TabVisible        := False;
  TabSheet9.TabVisible         := False;
  *)
  //
  // Clipboard
(*  ????
  begin
    NextInChain:=SetClipboardViewer(Handle);

    DelphiGuide.Name  := 'Zarko Gajic';
    DelphiGuide.eMail := 'delphi.guide@about.com';
    DelphiGuide.Posts := 15;

    OurFormat:=RegisterClipboardFormat('CF_TMember');

    if OpenClipboard(Handle) then
    begin
      EmptyClipboard;
      MemberHandle := GlobalAlloc(GMEM_DDESHARE or GMEM_MOVEABLE, SizeOf(TMember));
{      MemberPointer := GlobalLock(MemberHandle);
      MemberPointer^.Name  := DelphiGuide.Name;
      MemberPointer^.eMail := DelphiGuide.eMail;
      MemberPointer^.Posts := DelphiGuide.Posts;
}
      GlobalUnLock(MemberHandle);
      SetClipboardData(OurFormat,MemberHandle);
      CloseClipboard();
    end;
  end;
*)
  FIncI := 0;
  //
  VAR_TIPO_TAB_LCT := 1;
  FDmodCriado := False;
  // Deixar invis�vel
  AlphaBlendValue := 0;
  AlphaBlend := True;
  //
  PageControl1.ActivePageIndex := 0;
  AdvToolBarPagerNovo.ActivePageIndex := 0;
  //
  VAR_TYPE_LOG := ttlCliIntLog;
  //
  VAR_USA_TAG_BITBTN := True;
  VAR_MULTIPLAS_TAB_LCT := True;
  VAR_SERVIDOR3 := 3;
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  Application.OnMessage := MyObjects.FormMsg;
  FImagemDescanso := Geral.ReadAppKeyCU(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg');
  if FileExists(FImagemDescanso) then
  begin
   MyObjects.CarregaImagemEmTImage(ImgDescanso, FImagemDescanso, FBMPDescanso, clBtnFace); //SD1.Colors[csButtonShadow]);
   //AjustaEstiloImagem(ImgPrincipal.Tag);
  end;
  //
{ Alexandria
  MenuStyle := Geral.ReadAppKeyCU('MenuStyle', Application.Title,
    ktInteger, Integer(bsOffice2007Luna));
  SkinMenu(MenuStyle);
}
  //
  VAR_CAD_POPUP := PMGeral;
  //MLAGeral.CopiaItensDeMenu(PMGeral, Cadastros1, FmPrincipal);
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  AjustaEstiloImagem(-1);
  //
  Application.OnHint      := ShowHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  //2017-07-19 => N�o ativar por causa da c�mera Application.OnIdle := AppIdle;
  TimerIdle.Interval := VAR_TIMERIDLEITERVAL;
  TimerIdle.Enabled  := True;

  //:://:://:://:://::

  FmPrincipal.FDatabase1A := nil;
  FmPrincipal.FDatabase1B := nil;
  FmPrincipal.FDatabase2A := nil;
  FmPrincipal.FDatabase2B := nil;

  VAR_KIND_DEPTO := kdUH;
  VAR_NOME_TAB_CLIINT := 'cond';
  FJaAbriuFmCondGer   := False;
  VAR_NaoReabrirLct   := False;
  VAR_FP_EMPRESA := 'Cliente1="V"';
  VAR_FP_FUNCION := '(Fornece2="V" OR Fornece4="V")';
  VAR_STIMPCHEQUE   := StatusBar.Panels[13];

  //////////////////////////////////////////////////////////////////////////////
  VAR_FL_DataIni := Date - Geral.ReadAppKeyCU('Dias', Application.Title,
    ktInteger, 60);
  VAR_FL_DataFim := Date;
  //////////////////////////////////////////////////////////////////////////////
  DBGSemMez.FieldsCalcToOrder.Clear;
  DBGSemMez.FieldsCalcToOrder.Add('NOMESIT=Sit,Vencimento');
  DBGSemMez.FieldsCalcToOrder.Add('SERIE_CHEQUE=SerieCH,Documento');
  DBGSemMez.FieldsCalcToOrder.Add('COMPENSADO_TXT=Compensado');
  DBGSemMez.FieldsCalcToOrder.Add('MENSAL=Mez');
  //
  TmSuporte.Enabled := False;
  //
  TabSheet9.TabVisible := False; //Pertochek
  TabSheet8.TabVisible := False; //Pendencias Grupo UHs
  TabSheet7.TabVisible := False; //CNAB
  TabSheet4.TabVisible := False; //Outros
  //
  AdvToolBarPagerNovo.Visible := not AdvToolBarPagerNovo.Visible;
  //MyObjects.MaximizaAdvToolBarPager(AdvToolBarPagerNovo, deftfTrue);
end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
  // Clipboard
  GlobalFree(MemberHandle);
  ChangeClipboardChain(Handle, NextInChain);
end;

procedure TFmPrincipal.FormImprimeImoveis(Condominio: Integer);
begin
  if DBCheck.CriaFm(TFmCondImovImp, FmCondImovImp, afmoNegarComAviso) then
  begin
    if Condominio <> 0 then
    begin
      FmCondImovImp.EdEmpresa.ValueVariant := Condominio;
      FmCondImovImp.CBEmpresa.KeyValue     := Condominio;
    end;
    FmCondImovImp.ShowModal;
    FmCondImovImp.Destroy;
  end;
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  if OpenPictureDialog1.Execute then
  begin
    MyObjects.CarregaImagemEmTImage(
      ImgDescanso, OpenpictureDialog1.FileName, FBMPDescanso, clBtnFace); //Sd1.Colors[csButtonShadow]);
    Geral.WriteAppKeyCU('ImagemFundo', Application.Title, OpenPictureDialog1.FileName, ktString);
    //AjustaEstiloImagem(ImgPrincipal.Tag);
  end;
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if VAR_IMPRECHEQUE = 1 then
    PertoChekP.DesabilitaPertoCheck;
    (*DmDados.TbMaster.Open;
    DmDados.TbMaster.Edit;
    if Trunc(Date) > DmDados.TbMasterLA.Value then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value +
      (Trunc(Date) - Trunc(DmDados.TbMasterLA.Value));
    if Trunc(Date) < DmDados.TbMasterLA.Value then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 3;
    if (Trunc(Date) = DmDados.TbMasterLA.Value) and
       (Time < DmDados.TbMasterLH.Value)  then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 1;
    DmDados.TbMasterLA.Value := Trunc(Date);
    DmDados.TbMasterLH.Value := Time;
    DmDados.TbMaster.Post;
    DmDados.TbMaster.Close;
    FmSkin.Close;
    FmSkin.Destroy;*)
  if DModG <> nil then
    DModG.MostraBackup3(False);
  Application.Terminate;
end;

function TFmPrincipal.PreparaMinutosSQL(): Boolean;
//var
//  i: Integer;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM Ocupacao');
    Dmod.QrUpdL.ExecSQL;
    ///
(*    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO Ocupacao SET Min=:P0');
    for i := 1 to 1440 do
    begin
      Dmod.QrUpdL.Params[0].AsInteger := I;
      Dmod.QrUpdL.ExecSQL;
    end;*)
  except
    Result := False
  end;
end;

procedure TFmPrincipal.QrDuplAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDupi, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM prevbac ',
  'WHERE Conta=' + Geral.FF0(QrDuplConta.Value),
  '']);
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO Ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := I;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal.AGBFinNivClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano;
end;

procedure TFmPrincipal.AGBFinCtaClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.AGBSubGruClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.AGBSumValCtaClick(Sender: TObject);
begin
  MostraFormImportSalCfgCab(0);
end;

procedure TFmPrincipal.AGBFinGruClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.AGBFinCjtClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.AGBFinPlanoClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.AdvGlowButton11Click(Sender: TObject);
begin
  MostraLctGer2();
end;

procedure TFmPrincipal.AdvGlowButton12Click(Sender: TObject);
begin
  MostraWClients2(0);
end;

procedure TFmPrincipal.AdvGlowButton13Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWOpcoes, FmWOpcoes, afmoNegarComAviso) then
  begin
    FmWOpcoes.ShowModal;
    FmWOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton15Click(Sender: TObject);
begin
  CadastroDeCondominio(0);
end;

procedure TFmPrincipal.AdvGlowButton167Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal.AdvGlowButton16Click(Sender: TObject);
begin
  MostraOpcoesSyndic();
end;

procedure TFmPrincipal.AdvGlowButton17Click(Sender: TObject);
begin
  MostraOpcoes;
end;

procedure TFmPrincipal.AdvGlowButton18Click(Sender: TObject);
begin
  UBloquetos_Jan.MostraCondGer2(True, 0, 0, 0, PageControl1, AdvToolBarPagerNovo);
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
begin
  FinanceiroJan.CadastroIndiPag;
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
begin
  FinanceiroJan.DemonstrativoDeReceitasEDespesasFree(0);
end;

procedure TFmPrincipal.AGBCustomFR3Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCustomFR3, FmCustomFR3, afmoNegarComAviso) then
  begin
    FmCustomFR3.ShowModal;
    FmCustomFR3.Destroy;
  end;
end;

procedure TFmPrincipal.AGBEntiCadClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.AGBErrosAlertasClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormTsErrosAlertasCab();
end;

procedure TFmPrincipal.AdvGlowButton20Click(Sender: TObject);
begin
  MostraBloqParc(0);
end;

procedure TFmPrincipal.SincronizarTabelasWeb();
begin
  DmkWeb2_Jan.MostraWSincro(CO_DMKID_APP, CO_Versao,
    DModG.QrMasterHabilModulos.Value, DmodG.QrWebParams, Dmod.MyDB, Dmod.MyDBn,
    [], [], VAR_VERIFI_DB_CANCEL);
end;

procedure TFmPrincipal.AdvGlowButton21Click(Sender: TObject);
begin
  SincronizarTabelasWeb();
end;

procedure TFmPrincipal.AdvGlowButton22Click(Sender: TObject);
begin
  CadastroDeCondominio(0);
end;

procedure TFmPrincipal.AdvGlowButton23Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.AGBBancosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

procedure TFmPrincipal.AGBAnotaClick(Sender: TObject);
begin
  CadastroDeAnot;
end;

procedure TFmPrincipal.AdvGlowButton24Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCertidaoNeg, FmCertidaoNeg, afmoNegarComAviso) then
  begin
    FmCertidaoNeg.ShowModal;
    FmCertidaoNeg.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton25Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton26Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmBloqParcDel, FmBloqParcDel, afmoSoBoss) then
  begin
    FmBloqParcDel.ShowModal;
    FmBloqParcDel.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton27Click(Sender: TObject);
begin
  MostraLctGer2();
end;

procedure TFmPrincipal.AdvGlowButton2Click(Sender: TObject);
begin
  UBloqGerl_Jan.MostraBloCNAB_Ret();
end;

procedure TFmPrincipal.AGBBloqParcClick(Sender: TObject);
begin
  ReparcelamentosdeBloquetos_Local(0);
end;

procedure TFmPrincipal.AGBGerPropClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPesqCond, FmPesqCond, afmoNegarComAviso) then
  begin
    FmPesqCond.ShowModal;
    FmPesqCond.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFiliaisClick(Sender: TObject);
begin
  MostraFiliais();
end;

procedure TFmPrincipal.AdvGlowButton30Click(Sender: TObject);
begin
{
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if DBCheck.CriaFm(TFmCondMudaCod, FmCondMudaCod, afmoSoBoss) then
  begin
    FmCondMudaCod.ShowModal;
    FmCondMudaCod.Destroy;
  end;
}
end;

procedure TFmPrincipal.AdvGlowButton3Click(Sender: TObject);
begin
  FinanceiroJan.MostraLctDelLanctoz;
end;

procedure TFmPrincipal.AGBProtocolos1Click(Sender: TObject);
begin
  CadastroDeProtocolos(0);
end;

procedure TFmPrincipal.AGBRetCNABClick(Sender: TObject);
begin
  RetornoCNAB();
end;

procedure TFmPrincipal.AGBArreBaseClick(Sender: TObject);
begin
  UBloquetos_Jan.CadastroDeArrecadacoes(0, 0);
end;

procedure TFmPrincipal.AGBProvBaseClick(Sender: TObject);
begin
  UBloquetos_Jan.CadastroDeProvisoes(0, 0);
end;

procedure TFmPrincipal.AGBConsLeitClick(Sender: TObject);
begin
  UBloquetos_Jan.MostraCons(0);
end;

procedure TFmPrincipal.AGBImpBalClick(Sender: TObject);
begin
  FinanceiroJan.MostraCashBal(0);
end;

procedure TFmPrincipal.MostraFormINPC_IBGE();
begin
  if DBCheck.CriaFm(TFmINPC, FmINPC, afmoNegarComAviso) then
  begin
    FmINPC.ShowModal;
    FmINPC.Destroy;
  end;
end;

procedure TFmPrincipal.AGBHistConsClick(Sender: TObject);
begin
  HistoricoConsumo();
end;

procedure TFmPrincipal.AGBGrupUHs2Click(Sender: TObject);
begin
  MostraCondGri(0);
end;

procedure TFmPrincipal.AGBNFSeFatCabClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeFatCab(False, nil, nil);
end;

procedure TFmPrincipal.AGBNFSeLRpsCClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeLRpsC_0201(0);
end;

procedure TFmPrincipal.AGBNFSeMenCabClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeMenCab(False, nil, nil);
end;

procedure TFmPrincipal.AGBNFSeSrvCadClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeSrvCad();
end;

procedure TFmPrincipal.AGBNFSe_EditClick(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;
  Valor         = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
var
  Prestador: Integer;
  Serie: String;
  Numero: Integer;
begin
  Prestador := DmodG.QrFiliLogFilial.Value;
  //
  UnNFSe_PF_0201.MostraFormNFSe(SQLType, Prestador, Tomador, Intermediario,
    MeuServico, ItemListSrv, Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico,
    nil, Valor, Serie, Numero, nil);
end;

procedure TFmPrincipal.AGBNFSe_NFSePesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(False, nil, nil);
end;

procedure TFmPrincipal.AGBNFSe_RPSPesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_RPSPesq(False, nil, nil);
end;

procedure TFmPrincipal.AGBRepaLocClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmReparc, FmReparc, afmoNegarComAviso) then
  begin
    FmReparc.ShowModal;
    FmReparc.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton41Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmPromissoria, FmPromissoria, afmoNegarComAviso) then
  begin
    FmPromissoria.ShowModal;
    FmPromissoria.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton42Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmDuplicata1, FmDuplicata1, afmoNegarComAviso) then
  begin
    FmDuplicata1.ShowModal;
    FmDuplicata1.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton4Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AGBAnalBloqClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmBloqAnalisa, FmBloqAnalisa, afmoNegarComAviso) then
  begin
    FmBloqAnalisa.ShowModal;
    FmBloqAnalisa.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFeriadosClick(Sender: TObject);
begin
  CadastroDeFeriados();
end;

procedure TFmPrincipal.CadastrodeFeriados();
begin
  if DBCheck.CriaFm(TFmFeriados, FmFeriados, afmoNegarComAviso) then
  begin
    FmFeriados.ShowModal;
    FmFeriados.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFinListaClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmPrincipal.AGBConfImpBlqClick(Sender: TObject);
begin
  UBloquetos_Jan.MostraConfigBol(0);
end;

procedure TFmPrincipal.AdvGlowButton53Click(Sender: TObject);
begin
  FinanceiroJan.MostraRestricaoDeContas;
end;

procedure TFmPrincipal.AdvGlowButton55Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  if DBCheck.CriaFm(TFmServicoManager, FmServicoManager, afmoNegarComAviso) then
  begin
    FmServicoManager.FCB4_DB := Dmod.QrControle.FieldByName('CB4_DB').AsString;
    FmServicoManager.ShowModal;
    FmServicoManager.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton57Click(Sender: TObject);
begin
  UnNotificacoes.MostraFmNotificacoes(PageControl1, AdvToolBarPagerNovo);
end;

procedure TFmPrincipal.AdvGlowButton58Click(Sender: TObject);
begin
  MostraMatriz();
end;

procedure TFmPrincipal.AdvGlowButton5Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAE21Cad, FmCNAE21Cad, afmoNegarComAviso) then
  begin
    FmCNAE21Cad.ShowModal;
    FmCNAE21Cad.Destroy;
  end;
end;

procedure TFmPrincipal.AGBStatusClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStatus, FmStatus, afmoNegarComAviso) then
  begin
    FmStatus.ShowModal;
    FmStatus.Destroy;
  end;
end;

procedure TFmPrincipal.MostraMatriz();
begin
  // 2011-09-20 de afmoSoAdmin para afmoSoBoss
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFinSaldosClick(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas(0);
end;

procedure TFmPrincipal.AGBFlxEditClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFlxMensSel, FmFlxMensSel, afmoNegarComAviso) then
  begin
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    FmFlxMensSel.ShowModal;
    FmFlxMensSel.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFlxPanBalClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFlxMensBalViw, FmFlxMensBalViw, afmoNegarComAviso) then
  begin
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    FmFlxMensBalViw.ShowModal;
    FmFlxMensBalViw.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFlxPanBlqClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFlxMensBlqViw, FmFlxMensBlqViw, afmoNegarComAviso) then
  begin
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    FmFlxMensBlqViw.ShowModal;
    FmFlxMensBlqViw.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFolhaFunciClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFpFunci, FmFpFunci, afmoNegarComAviso) then
  begin
    FmFpFunci.ShowModal;
    FmFpFunci.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFrx3ImpClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCustomFR3Imp, FmCustomFR3Imp, afmoNegarComAviso) then
  begin
    FmCustomFR3Imp.ShowModal;
    FmCustomFR3Imp.Destroy;
  end;
end;

procedure TFmPrincipal.AGBLaySPEDEFDClick(Sender: TObject);
begin
//
end;

procedure TFmPrincipal.AGBLinkConcBcoClick(Sender: TObject);
begin
  FinanceiroJan.CadastroContasLnk;
end;

procedure TFmPrincipal.AGBLivre1_6_1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSMSCommands, FmSMSCommands, afmoNegarComAviso) then
  begin
    FmSMSCommands.ShowModal;
    FmSMSCommands.Destroy;
  end;
end;

procedure TFmPrincipal.AGBLivre1_6_2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSMSRecebe, FmSMSRecebe, afmoLiberado) then
  begin
    FmSMSRecebe.ShowModal;
    FmSMSRecebe.Destroy;
  end;
end;

procedure TFmPrincipal.AGBAgrExtrCtaClick(Sender: TObject);
begin
  FinanceiroJan.CadastroContasAgr;
end;

procedure TFmPrincipal.AGBDirCNABClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_Dir, FmCNAB_Dir, afmoNegarComAviso) then
  begin
    FmCNAB_Dir.ShowModal;
    FmCNAB_Dir.Destroy;
  end;
end;

procedure TFmPrincipal.AGBRemConfigClick(Sender: TObject);
begin
  MostraCNAB_Cfg(0);
end;

procedure TFmPrincipal.MostraCNAB_Cfg(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCNAB_Cfg.LocCod(Codigo, Codigo);
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
end;

procedure TFmPrincipal.AGBCNABCamposClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_Fld, FmCNAB_Fld, afmoNegarComAviso) then
  begin
    FmCNAB_Fld.ShowModal;
    FmCNAB_Fld.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvGlowButton7Click(Sender: TObject);
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmPrevImp, FmPrevImp, afmoNegarComAviso) then
  begin
    FmPrevImp.ShowModal;
    FmPrevImp.Destroy;
  end;
end;

procedure TFmPrincipal.AGBProtocolos2Click(Sender: TObject);
begin
  CadastroDeProtocolos(0);
end;

procedure TFmPrincipal.AGBMotNaoEntrClick(Sender: TObject);
begin
  ProtocoUnit.MostraFormProtocoMot;
end;

procedure TFmPrincipal.AGBOcorEntrClick(Sender: TObject);
begin
  ProtocoUnit.MostraFormProtocoOco;
end;

procedure TFmPrincipal.AGBDiaGerClick(Sender: TObject);
begin
  UDiario_Jan.MostraDiarioGer2();
end;

procedure TFmPrincipal.AGBDesignModeClick(Sender: TObject);
begin
  MyObjects.frxNextDesign();
end;

procedure TFmPrincipal.AGBDiaAssClick(Sender: TObject);
begin
  UDiario_Jan.MostraDiarioAss;
end;

procedure TFmPrincipal.AGBDiaEvenClick(Sender: TObject);
begin
  UDiario_Jan.MostraDiarioAdd();
end;

procedure TFmPrincipal.AGBFinEncerMesClick(Sender: TObject);
begin
  FinanceiroJan.MostraLctEncerraMes();
end;

procedure TFmPrincipal.AdvGlowButton82Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLeDataArq, FmLeDataArq, afmoNegarComAviso) then
  begin
    FmLeDataArq.ShowModal;
    FmLeDataArq.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton83Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCalcMod10e11, FmCalcMod10e11, afmoNegarComAviso) then
  begin
    FmCalcMod10e11.ShowModal;
    FmCalcMod10e11.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton84Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmObtemFoto0, FmObtemFoto0, afmoNegarComAviso) then
  begin
    FmObtemFoto0.ShowModal;
    FmObtemFoto0.Destroy;
  end;
}
end;

procedure TFmPrincipal.AdvGlowButton87Click(Sender: TObject);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
  begin
    if DBCheck.CriaFm(TFmDirWeb, FmDirWeb, afmoNegarComAviso) then
    begin
      FmDirWeb.ShowModal;
      FmDirWeb.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.AdvGlowButton88Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCertidaoNeg, FmCertidaoNeg, afmoNegarComAviso) then
  begin
    FmCertidaoNeg.ShowModal;
    FmCertidaoNeg.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton89Click(Sender: TObject);
begin
  //DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmCondGerCarne, FmCondGerCarne, afmoNegarComAviso) then
  begin
    FmCondGerCarne.FCond := 0;
    FmCondGerCarne.ShowModal;
    FmCondGerCarne.Destroy;
    try
      // Reabrir tabelas terce�rias para evitar incorre��es
      DmBloq.ReopenBoletos('');
    except
      ;// nada
    end;
  end;
end;

procedure TFmPrincipal.AGBTabsSPEDEFDClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPED_EFD_Tabelas, FmSPED_EFD_Tabelas, afmoNegarComAviso) then
  begin
    FmSPED_EFD_Tabelas.ShowModal;
    FmSPED_EFD_Tabelas.Destroy;
  end;
end;

procedure TFmPrincipal.AGBThemeClick(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.AGBTxtEMailClick(Sender: TObject);
begin
  MostraFormPreEmail();
end;

procedure TFmPrincipal.AGB_DTB_BACENClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeLoadTabs, FmNFeLoadTabs, afmoNegarComAviso) then
  begin
   FmNFeLoadTabs.ShowModal;
   FmNFeLoadTabs.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton90Click(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmPrincipal.AGBListServClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormListServ();
end;

procedure TFmPrincipal.AGBListUHsClick(Sender: TObject);
begin
  FormImprimeImoveis(0);
end;

procedure TFmPrincipal.AdvGlowButton92Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton93Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmImprime, FmImprime, afmoNegarComAviso) then
  begin
    FmImprime.ShowModal;
    FmImprime.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton94Click(Sender: TObject);
begin
  FinanceiroJan.MostraRelChAbertos(0);
end;

procedure TFmPrincipal.AdvGlowButton95Click(Sender: TObject);
begin
  FinanceiroJan.MostraCashPreCta(0);
end;

procedure TFmPrincipal.AdvGlowButton97Click(Sender: TObject);
begin
  MostraUploads(0, '', '', False);
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiBloquetosValor, FmVerifiBloquetosValor, afmoNegarComAviso) then
  begin
    FmVerifiBloquetosValor.ShowModal;
    FmVerifiBloquetosValor.Destroy;
  end;
end;

procedure TFmPrincipal.IGPDIFGV1Click(Sender: TObject);
begin
  MostraFormIGPDI_FGV();
end;

procedure TFmPrincipal.AGBFinCartClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCarteiras(0);
end;

procedure TFmPrincipal.AdvToolBarButton10Click(Sender: TObject);
begin
  SincronizarTabelasWeb();
end;

procedure TFmPrincipal.AdvToolBarButton12Click(Sender: TObject);
begin
  AdvToolBarPagerNovo.Visible := not AdvToolBarPagerNovo.Visible;
  //MyObjects.MaximizaAdvToolBarPager(AdvToolBarPagerNovo, deftfInverse);
end;

procedure TFmPrincipal.AdvToolBarButton1Click(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AdvToolBarButton2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCodBarraRead, FmCodBarraRead, afmoLiberado) then
  begin
    FmCodBarraRead.ShowModal;
    FmCodBarraRead.Destroy;
  end;
end;

procedure TFmPrincipal.AdvToolBarButton5Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvToolBarButton6Click(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.AdvToolBarButton7Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    DModG.CriaFavoritos(AdvToolBarPagerNovo, LaAviso3, LaAviso2, AGBAcesRapid, FmPrincipal);
  end;
end;

procedure TFmPrincipal.AdvToolBarButton8Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AGMBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.SBCadEntidadesClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.sd1FormSkin(Sender: TObject; aName: string;
  var DoSkin: Boolean);
begin
  if aName = VAR_FORMTDI_NAME then
    DoSkin := False;
end;

procedure TFmPrincipal.Escolher2Click(Sender: TObject);
begin
(*
  MeuVCLSkin.VCLSkinEscolhe(FmMyGlyfs.Dialog1, FmPrincipal.sd1);
*)
end;

procedure TFmPrincipal.extos1Click(Sender: TObject);
begin
  MostraFormCarta_App();
end;

function TFmPrincipal.TentaDefinirDiretorio(const Empresa: Integer; const
  Pasta: String; var Dir: String): Boolean;
  procedure Msg(Txt: String);
  begin
    Geral.MensagemBox(Txt, 'ERRO', MB_OK+MB_ICONERROR);
  end;
var
  ImgIPv4, ImgSDir, ArqDir, ArqImg, IniDir,
  (*Res, Titulo,*) FullArq: String;
  //I, F: Integer;
begin
  Result := False;
  //
  ImgIPv4 :=  dmkPF.IgnoraLocalhost(Dmod.QrControle.FieldByName('CB4_Host').AsString);
  ImgSDir := dmkPF.VeSeEhDirDmk(Dmod.QrControle.FieldByName('CB4_DB').AsString, Pasta, False);
  //
  ArqDir  := 'Emp_' + Geral.Substitui(Geral.FFN(Empresa, 4), '-', '_');
  ArqImg  := 'teste.txt';
  //
  if ImgIPv4 <> '' then
  begin
    ImgIPv4 :=  '\\' + ImgIPv4 + '\';
    ImgSDir := Copy(ImgSDir, 4);
  end;
  IniDir := ImgIPv4 + ImgSDir;
  //
  if DirectoryExists(IniDir) then
  begin
    Result := True;
    FullArq := Geral.Substitui(IniDir + '\' + ArqDir + '\' + ArqImg, '\\', '\');
    while pos('\\', FullArq) > 0 do
      FullArq := Geral.Substitui(IniDir + '\' + ArqDir, '\\', '\');
    if FullArq[1] = '\' then
      FullArq := '\' + FullArq;
    if FileExists(FullArq) then
    begin
      try
        DeleteFile(FullArq);
      except
        Msg('N�o foi poss�vel excluir arquivo teste!');
      end;
    end;
    try
      Dir := ExtractFileDir(FullArq);
      ForceDirectories(Dir);
      Geral.SalvaTextoEmArquivo(FullArq, 'Teste', True);
      Result := True;
    except
        Msg('N�o foi poss�vel salvar arquivo teste:' + #13#10 + FullArq);
    end;
  end else
  begin
    Msg(
    'O diret�rio raiz obrigat�rio definido nas op��es espec�ficas n�o foi localizado!' +
    #13#10 + IniDir);
  end;
end;

procedure TFmPrincipal.TextosdeSMS1Click(Sender: TObject);
begin
  MostraFormTxtSMS();
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmSyndic_MLA.Show;
  Enabled := False;
  FmSyndic_MLA.Refresh;
  FmSyndic_MLA.EdSenha.Text := FmSyndic_MLA.EdSenha.Text+'*';
  FmSyndic_MLA.EdSenha.Refresh;
  FmSyndic_MLA.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
    Application.CreateForm(TDmBloq, DmBloq);
    Application.CreateForm(TFmCondGer, FmCondGer);
    FDmodCriado := True;
    ReCaptionComponentesDeForm(FmPrincipal);
    AdvToolBarPagerNovo.Visible := True;
    // Tornar vis�vel
    Timer2.Enabled := True;
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmSyndic_MLA.EdSenha.Text := FmSyndic_MLA.EdSenha.Text+'*';
  FmSyndic_MLA.EdSenha.Refresh;
  FmSyndic_MLA.ReloadSkin;
  FmSyndic_MLA.EdLogin.Text := '';
  FmSyndic_MLA.EdLogin.PasswordChar := 'l';
  FmSyndic_MLA.EdSenha.Text := '';
  FmSyndic_MLA.EdSenha.Refresh;
  FmSyndic_MLA.EdLogin.ReadOnly := False;
  FmSyndic_MLA.EdSenha.ReadOnly := False;
  FmSyndic_MLA.EdLogin.SetFocus;
  //FmSyndic_MLA.ReloadSkin;
  FmSyndic_MLA.Refresh;
  try
    // Parei Aqui quando atualizar tudo
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE status');
    Dmod.QrUpd.SQL.Add('SET Nome=Descri');
    Dmod.QrUpd.SQL.Add('WHERE Nome="?"');
    Dmod.QrUpd.ExecSQL;
  except

  end;
end;

procedure TFmPrincipal.Timer2Timer(Sender: TObject);
begin
  if AlphaBlendValue < 255 then
    AlphaBlendValue := AlphaBlendValue + 1
  else begin
    Timer2.Enabled := False;
    AlphaBlend := False;
  end;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
begin
  if DModG <> nil then
    DmodG.ExecutaPing(FmSyndic_MLA, [Dmod.MyDB, Dmod.MyDBn, DModG.MyPID_DB, DModG.AllID_DB]);
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  //MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
end;

procedure TFmPrincipal.Futuros1Click(Sender: TObject);
begin
  FEntInt := 0;
  DefineCondominio(0, False);
  if FEntInt <> 0 then
    DModFin.ImprimeSaldos(FEntInt);
end;

procedure TFmPrincipal.MostraLctFisico(Entidade, Empresa, Carteira: Integer);
(*
var
  FotoDir: String;
  //
*)
begin
{$IFDEF DELPHI12_UP}
   // XE2 Ver o que fazer!
{$ELSE}
{
  if not TentaDefinirDiretorio(Empresa, '\Fotos\', FotoDir) then
    Exit;
  if DBCheck.CriaFm(TFmObtemFoto2, FmObtemFoto2, afmoLiberado) then
  begin
    FmObtemFoto2.FFotoDir := FotoDir;
    FmObtemFoto2.FEmpresa := Empresa;
    FmObtemFoto2.FEntidade := Entidade;
    FmObtemFoto2.FCarteira := Carteira;
    FmObtemFoto2.FTabela := CO_TAB_INDX_CB4_FOTODOCU;
    FmObtemFoto2.TimerCB4.Enabled := True;
    //FmObtemFoto2.Show;
    FmObtemFoto2.ShowModal;
    FmObtemFoto2.Destroy;
  end;
}
{$ENDIF}
end;

procedure TFmPrincipal.MostraLctGer2;
var
  DmLctX: TDataModule;
begin
  if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
  begin
    TDmLct2(DmLctX).GerenciaEmpresa(PageControl1, AdvToolBarPagerNovo, True, False);
  end;
end;

procedure TFmPrincipal.MostraLogoff;
begin
  FmPrincipal.Enabled := False;
  //
  FmSyndic_MLA.Show;
  FmSyndic_MLA.EdLogin.Text   := '';
  FmSyndic_MLA.EdSenha.Text   := '';
  FmSyndic_MLA.EdLogin.SetFocus;
end;

procedure TFmPrincipal.MostraOpcoes;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraOpcoesSyndic;
begin
  if DBCheck.CriaFm(TFmOpcoesSyndic, FmOpcoesSyndic, afmoNegarComAviso) then
  begin
    FmOpcoesSyndic.ShowModal;
    FmOpcoesSyndic.Destroy;
  end;
end;

procedure TFmPrincipal.MostraUploads(Condominio: Integer; Arquivo,
  Descricao: String; ExcluiArqOrigem: Boolean);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
  begin
    if DBCheck.CriaFm(TFmUploads, FmUploads, afmoNegarComAviso) then
    begin
      (*
      FmUploads.FCondominio      := Condominio;
      FmUploads.FDiretorio       := Dmod.ObtemDiretorioFTPBalancete;
      FmUploads.FArquivo         := Arquivo;
      FmUploads.FDescricao       := Descricao;
      FmUploads.FExcluiArqOrigem := ExcluiArqOrigem;
      //
      *)
      FmUploads.ShowModal;
      FmUploads.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.MostraFormCarta_App;
begin
  if DBCheck.CriaFm(TFmCartas, FmCartas, afmoNegarComAviso) then
  begin
    FmCartas.FTipo := 6;
    FmCartas.ShowModal;
    FmCartas.Destroy;
    Dmod.QrControle.Close;
    Dmod.QrControle.Open;
  end;
end;

procedure TFmPrincipal.MostraFormDiarCEMCad();
begin
  if DBCheck.CriaFm(TFmDiarCEMCad, FmDiarCEMCad, afmoNegarComAviso) then
  begin
    FmDiarCEMCad.ShowModal;
    FmDiarCEMCad.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormIGPDI_FGV;
begin
  if DBCheck.CriaFm(TFmIGPDI, FmIGPDI, afmoNegarComAviso) then
  begin
    FmIGPDI.ShowModal;
    FmIGPDI.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormImportSalCfgCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmImportSalCfgCab, FmImportSalCfgCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmImportSalCfgCab.LocCod(Codigo, Codigo);
    FmImportSalCfgCab.ShowModal;
    FmImportSalCfgCab.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormPreEmail();
begin
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
  end;
end;

procedure TFmPrincipal.Movimento1Click(Sender: TObject);
begin
  FinanceiroJan.MostraMovimento(0);
end;

procedure TFmPrincipal.MovimentoPlanodecontas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraMovimentoPlanodeContas(0);
end;

procedure TFmPrincipal.MultiplasEmpresas1Click(Sender: TObject);
begin
  FinanceiroJan.ExtratosFinanceirosMultiplasEmpresas();
end;

procedure TFmPrincipal.MyOnHint(Sender: TObject);
begin
  {
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
  }
end;

procedure TFmPrincipal.NotificacoesJanelas(TipoNotifi: TNotifi;
  DataHora: TDateTime);
begin
  //Colocar notifica��es espec�ficas aqui
end;

function TFmPrincipal.NotificacoesVerifica(QueryNotifi: TmySQLQuery;
  DataHora: TDateTime): Boolean;
begin
  Result := True;
  //Colocar notifica��es espec�ficas aqui
end;

procedure TFmPrincipal.EdEmpresaChange(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCond2, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI, ',
  'con.Codigo, con.Cliente ',
  'FROM cond con ',
  'LEFT JOIN entidades ent ON ent.Codigo=con.Cliente ',
  'WHERE Con.Codigo = ' + Geral.FF0(EdEmpresa.ValueVariant),
  'ORDER BY NOMECLI',
  '']);
{
  QrCond2.Close;
  QrCond2.Params[0].AsInteger := EdEmpresa.ValueVariant;
  QrCond2.Open;
}
  if EdEmpresa.ValueVariant <> 0 then
    DModG.FPTK_Def_Client := QrCond2Cliente.Value
  else
    DModG.FPTK_Def_Client := 0;
  DModG.ReopenPTK();
end;

procedure TFmPrincipal.EdEmpresaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Cond: Integer;
begin
  if Key = VK_F4 then
    DmCond.DefineCond(Cond, EdEmpresa, nil);
end;

procedure TFmPrincipal.Em1Click(Sender: TObject);
begin
  FinanceiroJan.MostraSaldos(0);
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

function TFmPrincipal.AcaoEspecificaDeApp(Servico: String): Boolean;
var
  Ano, Mes, Dia: Word;
  i, Cond: Integer;
begin
  if Servico = 'ArreFut' then
  begin
    Result := True;
    //
    if not DModG.DadosRetDeEntidade(FmCNAB_Ret2a.QrLeiEntidade.Value, Cond) then Exit;
    Application.CreateForm(TFmCondGerArreFut, FmCondGerArreFut);
    FmCondGerArreFut.FCliInt := Cond;
    FmCondGerArreFut.ImgTipo.SQLType := stIns;
    FmCondGerArreFut.QrDeptos.Close;
    FmCondGerArreFut.QrDeptos.Params[0].AsInteger := Cond;
    FmCondGerArreFut.QrDeptos.Open;
    DecodeDate(Date, Ano, Mes, Dia);
    if FmCNAB_Ret2a.QrLei.RecordCount > 0 then
    begin
      with FmCondGerArreFut do
      begin
        EdDepto.Text := IntToStr(FmCNAB_Ret2a.QrLeiItensApto.Value);
        CBDepto.KeyValue := FmCNAB_Ret2a.QrLeiItensApto.Value;
        if Dmod.QrControle.FieldByName('CNABCtaJur').AsInteger > 0 then
        begin
          EdConta.Text := IntToStr(Dmod.QrControle.FieldByName('CNABCtaJur').AsInteger);
          CBConta.KeyValue := Dmod.QrControle.FieldByName('CNABCtaJur').AsInteger;
        end else ;
        EdValor.Text := Geral.FFT(FmCNAB_Ret2a.QrLeiDJM.Value, 2, siPositivo);
        EdDescricao.Text := 'Diferen�a de juros ' + FmCNAB_Ret2a.QrLeiItensMez_TXT.Value +
          ' (boleto ' + FormatFloat('00000', FmCNAB_Ret2a.QrLeiIDNum.Value)+')';
        FmCondGerArreFut.CkContinuar.Visible := False;
        FmCondGerArreFut.CkContinuar.Checked := False;
        for i := 0 to FmCondGerArreFut.CBAno.Items.Count - 1 do
        begin
          if Geral.IMV(FmCondGerArreFut.CBAno.Items[i]) = Ano then
            FmCondGerArreFut.CBAno.ItemIndex := i;
        end;
        FmCondGerArreFut.CBMes.ItemIndex := Mes-1;
      end;
    end;
    FmCondGerArreFut.CkContinuar.Visible := True;
    FmCondGerArreFut.CkContinuar.Checked := True;
    //
    FmCondGerArreFut.ShowModal;
    FmCondGerArreFut.Destroy;
  end
  else Result := True;
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; Aba: Boolean = False);
begin
 // Nada
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade1: TStringGrid);
begin
   // Compatibilidade
end;

procedure TFmPrincipal.ReparcelamentosdeBloquetos_Local(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmbloqparc, Fmbloqparc, afmoNegarComAviso) then
  begin
    FDatabase1A := Dmod.MyDB;
    FDatabase1B := DModG.MyPID_DB;
    FDatabase2A := nil;
    FDatabase2B := nil;
    Fmbloqparc.FParcWEB := False;
    Fmbloqparc.DefineDatabaseTabelas();
    Fmbloqparc.FParcWEB := False;
    Fmbloqparc.LocCod(Codigo, Codigo);
    Fmbloqparc.ShowModal;
    Fmbloqparc.Destroy;
  end;
end;

procedure TFmPrincipal.ResultadosMensais1Click(Sender: TObject);
begin
  FinanceiroJan.MostraResultadosMensais();
end;

procedure TFmPrincipal.Padro2Click(Sender: TObject);
(*
var
  Cam: String;
*)
begin
(*
  Cam := Application.Title+'\VCLSkins';
  MLAGeral.DelAppKey(Cam, HKEY_CURRENT_USER);
  MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
*)
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKeyCU('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.Perfis2Click(Sender: TObject);
begin
  UWUsersJan.MostraWPerfis(0);
end;

procedure TFmPrincipal.MostraWClients2(Codigo: Integer);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
  begin
    if DBCheck.CriaFm(TFmWClients2, FmWClients2, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmWClients2.LocCod(Codigo, Codigo);
      FmWClients2.ShowModal;
      FmWClients2.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
//  MLAGeral.ReabrirtabelasFormAtivo;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm(*; Memo: TMemo*));
  function VerificaSeSubstitui(var Texto: String): Boolean;
    function SubstituiuCampo(const Campo: String; var Texto: String; const
    NovoTexto: String): Boolean;
    var
      //Txt,
      Fld: String;
    begin
      Result := True;
      //Txt := Texto;
      if pos(Campo, Texto) > 0 then
        Texto := Geral.Substitui(Texto, Campo, NovoTexto)
      else
      begin
        //Txt := AnsiLowerCase(Texto);
        Fld := AnsiLowerCase(Campo);
        if pos(Fld, Texto)  > 0 then
          Texto := Geral.Substitui(Texto, Fld, AnsiLowercase(NovoTexto))
        else
        begin
          //Txt := AnsiUpperCase(Texto);
          Fld := AnsiUpperCase(Campo);
          if pos(Fld, Texto) > 0 then
            Texto := Geral.Substitui(Texto, Fld, AnsiUppercase(NovoTexto))
          else
            Result := False;
        end;
      end;
      //if Result then
        //Texto := Txt;
    end;
  begin
    Result := True;
    if not FDmodCriado then
      Exit;
    //
    if Dmod.QrUserTxts.Active then
    begin
      Dmod.QrUserTxts.First;
      while not Dmod.QrUserTxts.Eof do
      begin
        SubstituiuCampo(Dmod.QrUserTxtsTxtSys.Value, Texto, Dmod.QrUserTxtsTxtMeu.Value);
        //
        Dmod.QrUserTxts.Next;
      end;
    end else
      Result := False;
  end;

var
  PropInfo: PPropInfo;
  Texto: String;
  Trocar: Boolean;
  I, J: Integer;
begin
  with Form do
  begin
    Texto := Form.Caption;
    Trocar := VerificaSeSubstitui(Texto);
    //
    if Trocar then
      Form.Caption := Texto;
    //
    for I := 0 to ComponentCount - 1 do
    begin
      PropInfo := GetPropInfo(TComponent(Components[I]).ClassInfo, 'Caption',
      [tkChar, tkString, tkWChar, tkLString, tkWString]);
      if PropInfo <> nil then
      begin
        Texto := GetStrProp(TComponent(Components[i]), PropInfo);
        Trocar := VerificaSeSubstitui(Texto);
        //
        try
          if Trocar then
            SetPropValue(TComponent(Components[i]), 'Caption', Texto);
        except
          Geral.MensagemBox('ERRO ao substituir texto!' + #13#10 +
          'Rotina: "ReCaptionComponentesDeForm"' + #13#10 +
          'Classe: ' + TComponent(Components[I]).ClassName,
          'ERRO', MB_OK+MB_ICONERROR);
          //GetPropInfo()
          //ShowMessage(PropInfo.Name);
        end;
      end;
      if ((TComponent(Components[i]) is TRadioGroup))
      or ((TComponent(Components[i]) is TdmkRadioGroup)) then
      begin
        for J := 0 to TRadioGroup(TComponent(Components[i])).Items.Count - 1 do
        begin
          Texto := TRadioGroup(TComponent(Components[i])).Items[J];
          Trocar := VerificaSeSubstitui(Texto);
          //
          if Trocar then
            TRadioGroup(TComponent(Components[i])).Items[J] := Texto;
        end;
      end;
      if (TComponent(Components[i]) is TDBRadioGroup) then
      begin
        for J := 0 to TDBRadioGroup(TComponent(Components[i])).Items.Count - 1 do
        begin
          Texto := TDBRadioGroup(TComponent(Components[i])).Items[J];
          Trocar := VerificaSeSubstitui(Texto);
          //
          if Trocar then
            TDBRadioGroup(TComponent(Components[i])).Items[J] := Texto;
        end;
      end;
      if ((TComponent(Components[i]) is TDBGrid))
      or ((TComponent(Components[i]) is TdmkDBGrid))
      or ((TComponent(Components[i]) is TdmkDBGridDAC)) then
      begin
        for J := 0 to TDBGrid(TComponent(Components[i])).Columns.Count - 1 do
        begin
          Texto := TDBGrid(TComponent(Components[i])).Columns[J].Title.Caption;
          Trocar := VerificaSeSubstitui(Texto);
          //
          if Trocar then
            TDBGrid(TComponent(Components[i])).Columns[J].Title.Caption := Texto;
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.rechosdecontrato2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCartas, FmCartas, afmoNegarComAviso) then
  begin
    FmCartas.FTipo := 7;
    FmCartas.ShowModal;
    FmCartas.Destroy;
    Dmod.QrControle.Close;
    Dmod.QrControle.Open;
  end;
end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.AjustaEstiloImagem(Index: integer);
var
 Indice: Integer;
begin
  if Index >= 0 then Indice := Index else
  Indice := Geral.ReadAppKeyCU('Estio_ImgPrincipal', Application.Title,
    ktInteger, 1);
  //MyObjects.DisplayBitmap(FBMPDescanso, ImgDescanso, SD1.Colors[csButtonShadow]);
  if FileExists(FImagemDescanso) then
  begin
   MyObjects.CarregaImagemEmTImage(ImgDescanso, FImagemDescanso, FBMPDescanso, clBtnFace); //SD1.Colors[csButtonShadow]);
   //AjustaEstiloImagem(ImgPrincipal.Tag);
  end;

{@@@ ver exemplo AspectRatio.dproj
  case Indice of
    00: ImgPrincipal.Style := sbAutosize;
    01: ImgPrincipal.Style := sbCenter;
    02: ImgPrincipal.Style := sbKeepAspRatio;
    03: ImgPrincipal.Style := sbKeepHeight;
    04: ImgPrincipal.Style := sbKeepWidth;
    05: ImgPrincipal.Style := sbNone;
    06: ImgPrincipal.Style := sbStretch;
    07: ImgPrincipal.Style := sbTile;
    else ImgPrincipal.Style := sbCenter;
  end;
}
  ImgDescanso.Tag        := Indice;
  Automatico1.Checked    := Indice = 00;
  Centralizado1.Checked  := Indice = 01;
  ManterProporo1.Checked := Indice = 02;
  ManterAltura1.Checked  := Indice = 03;
  ManterLargura1.Checked := Indice = 04;
  Nenhum1.Checked        := Indice = 05;
  Espichar1.Checked      := Indice = 06;
  Repetido1.Checked      := Indice = 07;
  //
  ImgDescanso.Invalidate;
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.MenuItem2Click(Sender: TObject);
begin
  Geral.WriteAppKeyCU('ImagemFundo', Application.Title, '', ktString);
  Geral.MensagemBox(
  '� necess�rio reexecutar o aplicativo para que a altera��o tenha efeito!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmPrincipal.Migraentidadesparaoformatonovo1Click(Sender: TObject);
begin
  (*
  if DModG.QrCtrlGeralAtualizouEntidades.AsInteger = 0 then
  begin
    if Geral.MB_Pergunta('Deseja atualizar a entiades agora?' + sLineBreak +
      'Este processo dermorar� aproximadamente 60 minutos' + sLineBreak +
      'n�o poder� ser pausado ' + sLineBreak +
      'e durante este processo o aplicativo n�o poder� ser utilizado neste computador!' +
      sLineBreak + 'Confirma a atualiza��o?') = ID_YES then
    begin
      Entities.AtualizaEntidadesParaEntidade2(PB1, Dmod.MyDB, DModG.AllID_DB);
    end;
  end else
    Geral.MB_Aviso('Migra��o j� realizada!');
    *)
end;

function TFmPrincipal.MigraEspecificos(CliInt: Integer): Boolean;
var
  A1, A2, B1, B2, A3, B3: Integer;
  CI_TXT, Campos, TabAriA, TabCnsA, TabPrvA, TabPriA: String;
  QtdeFlds: Integer;
begin
  CI_TXT  := FormatFloat('0', CliInt);
{
  TabAriA := 'ari' + FormatFloat('0000', CliInt) + ttA;
  TabCnsA := 'cns' + FormatFloat('0000', CliInt) + ttA;
  TabPriA := 'pri' + FormatFloat('0000', CliInt) + ttA;
  TabPrvA := 'prv' + FormatFloat('0000', CliInt) + ttA;
}
  TabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt);
  TabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CliInt);
  TabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, CliInt);
  TabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, CliInt);

  //////////////////////////////////////////////////////////////////////////////
  ///  P R E V I T S
  //////////////////////////////////////////////////////////////////////////////

  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT COUNT(pri.Codigo) Itens');
  Dmod.QrAux.SQL.Add('FROM ' + TAB_PRI + ' pri');
  Dmod.QrAux.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=pri.Codigo');
  Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
  Dmod.QrAux.Open;
  A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
  //
  if A1 > 0 then
  begin
    Screen.Cursor := crHourGlass;
    //
    try
      Dmod.QrUpd.SQL.Clear;
      {
      Dmod.QrUpd.SQL.Add('LOCK TABLES ' +
        TabPrvA + ' WRITE, ' +
        TabPriA + ' WRITE, ' +
        TAB_PRI + ' WRITE, ' +
        TAB_PRV + ' WRITE, master WRITE');
      Dmod.QrUpd.ExecSQL;
      }
      //
      try
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabPriA);
        Dmod.QrAux.Open;
        B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
        //
        Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabPriA, 'pri.', QtdeFlds);
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ' + TabPriA);
        Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
        Dmod.QrUpd.SQL.Add('FROM ' + TAB_PRI + ' pri');
        Dmod.QrUpd.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=pri.Codigo');
        Dmod.QrUpd.SQL.Add('WHERE prv.Cond=' + CI_TXT);
        //dmkPF.LeMeuTexto(Dmod.QrUpd.SQL.Text);

        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabPriA);
        Dmod.QrAux.Open;
        B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
        //
        B3 := B2 - B1;
        if B3 > 0 then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE pri');
          Dmod.QrUpd.SQL.Add('FROM ' + TAB_PRI + ' pri, ' + TAB_PRV + ' prv');
          Dmod.QrUpd.SQL.Add('WHERE pri.Codigo=prv.Codigo');
          Dmod.QrUpd.SQL.Add('AND prv.Cond=' + CI_TXT);
          Dmod.QrUpd.ExecSQL;
          //
          // Parei aqui!
          //fazer update na tabela carteira (entidade?) que esta foi migrada
        end;
        //
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT COUNT(pri.Codigo) Itens');
        Dmod.QrAux.SQL.Add('FROM ' + TAB_PRI + ' pri');
        Dmod.QrAux.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=pri.Codigo');
        Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
        Dmod.QrAux.Open;
        A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
        //
      finally
      {
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
        Dmod.QrUpd.ExecSQL;
      }
      end;

      A3 := A1 - A2;
      if A3 <> B3 then
      begin
        Result := False;
      end else
      begin
        Result := True;
      end;
      Screen.Cursor := crDefault;
   finally
      Screen.Cursor := crDefault;
    end;
  end else Result := True;

  if Result then
  begin

    //////////////////////////////////////////////////////////////////////////////
    ///  C O N S I T S
    //////////////////////////////////////////////////////////////////////////////

    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT COUNT(cns.Codigo) Itens');
    Dmod.QrAux.SQL.Add('FROM ' + TAB_CNS + ' cns');
    Dmod.QrAux.SQL.Add('WHERE cns.Cond=' + CI_TXT);
    Dmod.QrAux.Open;
    A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
    //
    if A1 > 0 then
    begin
      Screen.Cursor := crHourGlass;
      //
      try
        Dmod.QrUpd.SQL.Clear;
        {
        Dmod.QrUpd.SQL.Add('LOCK TABLES ' +
          TabPrvA + ' WRITE, ' +
          TabPriA + ' WRITE, ' +
          TAB_CNS + ' WRITE, ' +
          TAB_PRV + ' WRITE, master WRITE');
        Dmod.QrUpd.ExecSQL;
        }
        //
        try
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabCnsA);
          Dmod.QrAux.Open;
          B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabCnsA, '', QtdeFlds);
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO ' + TabCnsA);
          Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
          Dmod.QrUpd.SQL.Add('FROM ' + TAB_CNS + ' cns');
          Dmod.QrUpd.SQL.Add('WHERE cns.Cond=' + CI_TXT);
          //dmkPF.LeMeuTexto(Dmod.QrUpd.SQL.Text);

          Dmod.QrUpd.ExecSQL;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabCnsA);
          Dmod.QrAux.Open;
          B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          B3 := B2 - B1;
          if B3 > 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('DELETE cns');
            Dmod.QrUpd.SQL.Add('FROM ' + TAB_CNS + ' cns');
            Dmod.QrUpd.SQL.Add('WHERE cns.Cond=' + CI_TXT);
            Dmod.QrUpd.ExecSQL;
          end;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(cns.Codigo) Itens');
          Dmod.QrAux.SQL.Add('FROM ' + TAB_CNS + ' cns');
          Dmod.QrAux.SQL.Add('WHERE cns.Cond=' + CI_TXT);
          Dmod.QrAux.Open;
          A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
        finally
        {
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
          Dmod.QrUpd.ExecSQL;
        }
        end;

        A3 := A1 - A2;
        if A3 <> B3 then
        begin
          Result := False;
        end else
        begin
          Result := True;
        end;
        Screen.Cursor := crDefault;
     finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;

  if Result then
  begin

    //////////////////////////////////////////////////////////////////////////////
    ///  A R R E I T S
    //////////////////////////////////////////////////////////////////////////////

    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT COUNT(Ari.Codigo) Itens');
    Dmod.QrAux.SQL.Add('FROM ' + TAB_ARI + ' Ari');
    Dmod.QrAux.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=Ari.Codigo');
    Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
    Dmod.QrAux.Open;
    A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
    //
    if A1 > 0 then
    begin
      Screen.Cursor := crHourGlass;
      //
      try
        Dmod.QrUpd.SQL.Clear;
        {
        Dmod.QrUpd.SQL.Add('LOCK TABLES ' +
          TabPrvA + ' WRITE, ' +
          TabAriA + ' WRITE, ' +
          TAB_ARI + ' WRITE, ' +
          TAB_PRV + ' WRITE, master WRITE');
        Dmod.QrUpd.ExecSQL;
        }
        //
        try
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabAriA);
          Dmod.QrAux.Open;
          B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabAriA, 'Ari.', QtdeFlds);
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO ' + TabAriA);
          Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
          Dmod.QrUpd.SQL.Add('FROM ' + TAB_ARI + ' Ari');
          Dmod.QrUpd.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=Ari.Codigo');
          Dmod.QrUpd.SQL.Add('WHERE prv.Cond=' + CI_TXT);
          //dmkPF.LeMeuTexto(Dmod.QrUpd.SQL.Text);

          Dmod.QrUpd.ExecSQL;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabAriA);
          Dmod.QrAux.Open;
          B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          B3 := B2 - B1;
          if B3 > 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('DELETE Ari');
            Dmod.QrUpd.SQL.Add('FROM ' + TAB_ARI + ' Ari, ' + TAB_PRV + ' prv');
            Dmod.QrUpd.SQL.Add('WHERE Ari.Codigo=prv.Codigo');
            Dmod.QrUpd.SQL.Add('AND prv.Cond=' + CI_TXT);
            Dmod.QrUpd.ExecSQL;
            //
            // Parei aqui!
            //fazer update na tabela carteira (entidade?) que esta foi migrada
          end;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Ari.Codigo) Itens');
          Dmod.QrAux.SQL.Add('FROM ' + TAB_ARI + ' Ari');
          Dmod.QrAux.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=Ari.Codigo');
          Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
          Dmod.QrAux.Open;
          A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
        finally
        {
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
          Dmod.QrUpd.ExecSQL;
        }
        end;

        A3 := A1 - A2;
        if A3 <> B3 then
        begin
          Result := False;
        end else
        begin
          Result := True;
        end;
        Screen.Cursor := crDefault;
     finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;

  //

  if Result then
  begin

    //////////////////////////////////////////////////////////////////////////////
    ///  P R E V
    //////////////////////////////////////////////////////////////////////////////

    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT COUNT(prv.Codigo) Itens');
    Dmod.QrAux.SQL.Add('FROM ' + TAB_PRV + ' prv');
    Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
    Dmod.QrAux.Open;
    A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
    //
    if A1 > 0 then
    begin
      Screen.Cursor := crHourGlass;
      //
      try
        Dmod.QrUpd.SQL.Clear;
        {
        Dmod.QrUpd.SQL.Add('LOCK TABLES ' +
          TabPrvA + ' WRITE, ' +
          TAB_PRV + ' WRITE, master WRITE');
        Dmod.QrUpd.ExecSQL;
        }
        //
        try
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabPrvA);
          Dmod.QrAux.Open;
          B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabPrvA, '', QtdeFlds);
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO ' + TabPrvA);
          Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
          Dmod.QrUpd.SQL.Add('FROM ' + TAB_PRV + ' prv');
          Dmod.QrUpd.SQL.Add('WHERE prv.Cond=' + CI_TXT);
          //dmkPF.LeMeuTexto(Dmod.QrUpd.SQL.Text);

          Dmod.QrUpd.ExecSQL;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabPrvA);
          Dmod.QrAux.Open;
          B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          B3 := B2 - B1;
          if B3 > 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('DELETE Prv');
            Dmod.QrUpd.SQL.Add('FROM ' + TAB_PRV + ' prv');
            Dmod.QrUpd.SQL.Add('WHERE prv.Cond=' + CI_TXT);
            Dmod.QrUpd.ExecSQL;
          end;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(prv.Codigo) Itens');
          Dmod.QrAux.SQL.Add('FROM ' + TAB_PRV + ' prv');
          Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
          Dmod.QrAux.Open;
          A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
        finally
        {
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
          Dmod.QrUpd.ExecSQL;
        }
        end;

        A3 := A1 - A2;
        if A3 <> B3 then
        begin
          Result := False;
        end else
        begin
          Result := True;
        end;
        Screen.Cursor := crDefault;
     finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmPrincipal.Modelo20031Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaPorNveldoPLanodeContas(0);
end;

procedure TFmPrincipal.Modelo20121Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaPorNveldoPLanodeContas2(0);
end;

procedure TFmPrincipal.Montagemdecontratos2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCartaG, FmCartaG, afmoNegarComAviso) then
  begin
    FmCartaG.FTipo := 4;
    FmCartaG.ShowModal;
    FmCartaG.Destroy;
  end;
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
{var
  Indice : Integer;}
begin
  {if Index >= 0 then
    Indice := Index
  else
    Indice := Geral.ReadAppKeyCU('MenuStyle', Application.Title,
      ktInteger, 1);}
{
  case Index of
    0: AdvToolBarOfficeStyler1.Style := bsOffice2003Blue;
    1: AdvToolBarOfficeStyler1.Style := bsOffice2003Classic;
    2: AdvToolBarOfficeStyler1.Style := bsOffice2003Olive;
    3: AdvToolBarOfficeStyler1.Style := bsOffice2003Silver;
    4: AdvToolBarOfficeStyler1.Style := bsOffice2007Luna;
    5: AdvToolBarOfficeStyler1.Style := bsOffice2007Obsidian;
    6: AdvToolBarOfficeStyler1.Style := bsOffice2007Silver;
    7: AdvToolBarOfficeStyler1.Style := bsOfficeXP;
    8: AdvToolBarOfficeStyler1.Style := bsWhidbeyStyle;
    9: AdvToolBarOfficeStyler1.Style := bsWindowsXP;
  end;
}
end;

procedure TFmPrincipal.CadastroDeProtocolos(Lote: Integer; Codigo: Integer = 0);
begin
  ProtocoUnit.MostraFormProtocolos(Lote, Codigo);
end;

procedure TFmPrincipal.CadastroDeCondominio(Codigo: Integer; CondBloco: Integer = 0;
  CondImov: Integer = 0; Aba1: Integer = 0; Aba2: Integer = 0; Aba3: Integer = 0);
begin
  if DBCheck.CriaFm(TFmCond, FmCond, afmoNegarComAviso) then
  begin
    if Codigo <> 0  then
      FmCond.LocCod(Codigo, Codigo);
    if CondBloco <> 0 then
      FmCond.ReopenCondBloco(CondBloco);
    if CondImov <> 0 then
      FmCond.ReopenCondImov(CondImov);
    FmCond.PageControl1.ActivePageIndex := Aba1;
    FmCond.PageControl7.ActivePageIndex := Aba2;
    FmCond.PageControl2.ActivePageIndex := Aba3;
    FmCond.ShowModal;
    FmCond.Destroy;
  end;
end;

procedure TFmPrincipal.MostraBloqParc(Codigo: Integer);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
  begin
    {N�o tem mais nenhum cliente usando o Syndic 
    Geral.MensagemBox(
    'ATEN��O! Edite ou crie novos lan�amentos apenas nos condom�nios j� migrados!',
    'AVISO IMPORTANTE', MB_OK+MB_ICONWARNING);
    }
    //
    Screen.Cursor := crHourGlass;
    try
      Application.CreateForm(TFmbloqparc, Fmbloqparc);
    finally
      Screen.Cursor := crDefault;
    end;
    FDatabase1A := nil;
    FDatabase1B := nil;
    FDatabase2A := Dmod.MyDBn;
    FDatabase2B := Dmod.MyDBn;
    Fmbloqparc.FParcWEB := True;
    Fmbloqparc.DefineDatabaseTabelas();
    Fmbloqparc.FParcWEB := True;
    Fmbloqparc.LocCod(Codigo, Codigo);
    Fmbloqparc.ShowModal;
    Fmbloqparc.Destroy;
  end;
end;

procedure TFmPrincipal.MostraBloqParcAviso(Parcelamentos: Integer);
begin
  if Parcelamentos > 0 then
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
    begin
      if DBCheck.CriaFm(TFmBloqParcAviso, FmBloqParcAviso, afmoNegarComAviso) then
      begin
        FmBloqParcAviso.FParcelamentos := Parcelamentos;
        FmBloqParcAviso.ShowModal;
        FmBloqParcAviso.Destroy;
      end;
    end;
  end;
end;

procedure TFmPrincipal.MostraCondGri(Codigo: Integer);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
  begin
    if DBCheck.CriaFm(TFmCondGri, FmCondGri, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmCondGri.LocCod(Codigo, Codigo);
      FmCondGri.ShowModal;
      FmCondGri.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.MostraFiliais;
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFmCondGer(CondAnterior: Boolean; Periodo: Integer);
const
  LocCart = 0;
var
  I: Integer;
begin
  //
  if DmodFin.EntidadeHabilitadadaParaFinanceiroNovo(FEntInt, True, True) <> sfnNovoFin then Exit;
  //if DmCond.AssistenteNaoHabilitadadoParaOCondominio(FEntInt, True) then
    //Exit;
  //
  Screen.Cursor := crHourGlass;
  DmCond.DefTabsEmpresa(FCliInt_);
  // Deve ser depois do DmCond.DefTabsEmpresa!
  DmAtencoes.ExecutaPesquisas();
  MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Fechando tabelas do condom�nio anterior');
  //
  if not CondAnterior or not FJaAbriuFmCondGer then
  begin
    for I := 0 to FmCondGer.ComponentCount - 1 do
    begin
      if FmCondGer.Components[I] is TmySQLQuery then
        TmySQLQuery(FmCondGer.Components[I]).Close;
    end;
    for I := 0 to DmCond.ComponentCount - 1 do
    begin
      if DmCond.Components[I] is TmySQLQuery then
        TmySQLQuery(DmCond.Components[I]).Close;
    end;
    for I := 0 to DmLct2.ComponentCount - 1 do
    begin
      if DmLct2.Components[I] is TmySQLQuery then
        TmySQLQuery(DmLct2.Components[I]).Close;
    end;
    for I := 0 to DmBloq.ComponentCount - 1 do
    begin
      if DmBloq.Components[I] is TmySQLQuery then
        TmySQLQuery(DmBloq.Components[I]).Close;
    end;
    DmCond.ReopenQrCond(FCliInt_);
    if Periodo > 0 then
    begin
      FmCondGer.LocPeriodo(Periodo, Periodo);
      FmCondGer.PageControl1.ActivePageIndex := 3;
      //F_EntInt := DCond.QrCondCliente.Value;
    end;
    DmLct2.QrCrt.Close;
    DmLct2.QrLct.Close;
  end;
  FmCondGer.QrPropriet.Close;
  FmCondGer.QrPropriet.Open;
  FmCondGer.QrBancos.Close;
  FmCondGer.QrBancos.Open;
  DModG.DefineDataMinima(FEntInt);
  // Novo Financeiro
  VAR_LETRA_LCT := 'a';
  DModG.EmpresaAtual_SetaCodigos(FCliInt_, tecCliInt, False);
  //DmLct2.FechaQueries();
  DModG.Def_EM_ABD(TMeuDB, FEntInt, FCliInt_, DmCond.FDtEncer, DmCond.FDtMorto,
      DmCond.FTabLctA, DmCond.FTabLctB, DmCond.FTabLctD);
  DmLct2.FTabLctA   := DmCond.FTabLctA;
  DmLct2.FTPDataIni := FmCondGer.TPDataIni;
  DmLct2.FTPDataFim := FmCondGer.TPDataFim;
  MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Reabrindo financeiro');
  //
  DmLct2.ReabreCarteiras(LocCart, DmLct2.QrCrt, DmLct2.QrCrtSum, 'TFmPrincipal.MostraFmCondGer()');
  //
  Screen.Cursor := crDefault;
  //
  FmCondGer.Show;
  MyObjects.Informa2(LaAviso3, LaAviso2, False, '...');
  // Evitar erro de desenho?
  LaAviso2.Visible := False;
  LaAviso3.Visible := False;
  //
  Application.OnHint := ShowHint;
end;

procedure TFmPrincipal.MostraFormTxtSMS();
begin
  if DBCheck.CriaFm(TFmTxtSMS, FmTxtSMS, afmoNegarComAviso) then
  begin
    FmTxtSMS.ShowModal;
    FmTxtSMS.Destroy;
  end;
end;

procedure TFmPrincipal.InadimplenciaNew(Empresa: Integer);
begin
  if DBCheck.CriaFm(TFmInadimp2, FmInadimp2, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmInadimp2.EdEmpresa.ValueVariant := Empresa;
      FmInadimp2.CBEmpresa.KeyValue     := Empresa;
    end;
    FmInadimp2.ShowModal;
    FmInadimp2.Destroy;
  end;
end;

procedure TFmPrincipal.INPCIBGE1Click(Sender: TObject);
begin
  MostraFormINPC_IBGE();
end;

procedure TFmPrincipal.JanelasWEB1Click(Sender: TObject);
begin
  UWUsersJan.MostraWPerfJan(True);
end;

procedure TFmPrincipal.DefineCondominio(Periodo: Integer; Gerencia: Boolean);
begin
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmCondSel, FmCondSel);
    case DModG.QrEmpresas.RecordCount of
      0:
      begin
        Geral.MensagemBox('N�o h� condom�nio cadastrado ou liberado!', 'Aviso',
         MB_OK+MB_ICONWARNING);
        DefineVarsCliInt(0);
      end;
      1:
      begin
        DefineVarsCliInt(DModG.QrEmpresasFilial.Value);//FmCondSel.QrCondCodCliInt.Value);
      end else begin
        Screen.Cursor := crDefault;
        FmCondSel.ShowModal;
        DefineVarsCliInt(FmCondSel.FCond);
      end;
    end;
    FmCondSel.Destroy;
    Application.ProcessMessages;
    if FCliInt_ <> 0 then
    if Gerencia then
      MostraFmCondGer(False, Periodo)
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.DefineVarsCliInt(CliInt: Integer);
begin
{   ANTES:
    DmodG.QrCliIntLog.SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE CliInt=:P0')
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrCliIntLog, DmodG.QrCliIntLog.DataBase, [
  'SELECT DISTINCT eci.CodEnti',
  'FROM enticliint eci',
  'LEFT JOIN senhas snh ON snh.Funcionario=eci.AccManager',
  'LEFT JOIN senhasits sei ON eci.CodEnti=sei.Empresa',
  'WHERE eci.CodCliInt=' + FormatFloat('0', CliInt),
  'AND ',
  '(',
  '  eci.AccManager=0',
  '  OR ',
  '  snh.Numero=' + FormatFloat('0', VAR_USUARIO),
  '  OR',
  '  sei.Numero=' + FormatFloat('0', VAR_USUARIO),
  '  OR',
     FormatFloat('0', VAR_USUARIO) + '<0',
  ')',
  '']);
  //
  FEntInt := DmodG.QrCliIntLogCodEnti.Value;
  if FEntInt = 0 then
    FCliInt_ := 0
  else
    FCliInt_ := CliInt;
 //
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntLogCodEnti.Value);
  if VAR_LIB_EMPRESAS = '' then VAR_LIB_EMPRESAS := '-100000000';

  //2012-01-12 Desmarcado por gerar erro em SQLs que buscam dados de v�rios condom�nios
  //VAR_LIB_FILIAIS  := '';
  //Fim 2012-01-12 Desmarcado por gerar erro em SQLs que buscam dados de v�rios condom�nios

  DModG.DefineDataMinima(FEntInt);
  //VAR LCT_ENCERRADO := DModG.OBtemDataUltimoEncerramentoFinanceiro(FEntInt);
  //
  DmLct2.QrCrt.Close;
  DmLct2.QrLct.Close;
  //
end;

procedure TFmPrincipal.Desativar1Click(Sender: TObject);
(*
var
  Cam: String;
*)
begin
(*
  Cam := Application.Title+'\VCLSkins';
  Geral.WriteAppKeyCU('VCLSkins', Application.Title, False, ktBoolean);
  MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
*)
end;

procedure TFmPrincipal.GradePKACellClick(Column: TColumn);
var
  DataHora, DataSai, DataRec, DataRet: TDateTime;
  Saiu, Recebeu, Retornou, Conta: Integer;
begin
  if (Column.FieldName = 'Saiu') and (DModG.QrPKASaiu.Value = 0) then
  begin
    DataHora := Now();
    if Geral.MensagemBox('Confirma a sa�da do protocolo ' + IntToStr(
    DModG.QrPKAConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
    ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) =
    ID_YES then
    begin
      Saiu := VAR_USUARIO;
      DataSai := DataHora;
      Conta := DModG.QrPKAConta.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
      'Saiu', 'DataSai'], ['Conta'], [Saiu, DataSai], [Conta], True) then
      DModG.ReopenPTK();
    end;
  end;
  //
  if (Column.FieldName = 'Recebeu') and (DModG.QrPKARecebeu.Value = 0) then
  begin
    if DModG.QrPKASaiu.Value = 0 then
    begin
      Geral.MensagemBox('� necess�rio informar a sa�da antes do recebimento!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else
    begin
      DataHora := Now();
      if Geral.MensagemBox('Confirma o recebimento do protocolo ' + IntToStr(
      DModG.QrPKAConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
      ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) =
      ID_YES then
      begin
        Recebeu := VAR_USUARIO;
        DataRec := DataHora;
        Conta := DModG.QrPKAConta.Value;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Recebeu', 'DataRec'], ['Conta'], [Recebeu, DataRec], [Conta], True) then
        DModG.ReopenPTK();
      end;
    end;
  end;
  //
  if (Column.FieldName = 'Retornou') and (DModG.QrPKARetornou.Value = 0) then
  begin
    if DModG.QrPKARecebeu.Value = 0 then
    begin
      Geral.MensagemBox('� necess�rio informar o recebimento antes do retorno!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else
    begin
      DataHora := Now();
      if Geral.MensagemBox('Confirma o retorno do protocolo ' + IntToStr(
      DModG.QrPKAConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
      ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) =
      ID_YES then
      begin
        Retornou := VAR_USUARIO;
        DataRet := DataHora;
        Conta := DModG.QrPKAConta.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Retornou', 'DataRet'], ['Conta'], [Retornou, DataRet], [Conta], True) then
        begin
          DModG.FPKA_Next := UMyMod.ProximoRegistro(DModG.QrPKA, 'Conta',
            DModG.QrPKAConta.Value);
          DModG.ReopenPTK();
        end;  
      end;
    end;
  end;
  //
end;

procedure TFmPrincipal.GradePKADrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'NO_RETORNA') then
  begin
    if DModG.QrPKARetorna.Value = 1 then
    begin
      Txt := clWindow;
      Bak := clRed;
    end else begin
      Txt := clWindow;
      Bak := clBlue;
    end;
    MyObjects.DesenhaTextoEmDBGrid(GradePKA, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
  if Column.FieldName = 'Saiu' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePKA), Rect, 1, DModG.QrPKASaiu.Value);
  if Column.FieldName = 'Recebeu' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePKA), Rect, 1, DModG.QrPKARecebeu.Value);
  if Column.FieldName = 'Retornou' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePKA), Rect, 1, DModG.QrPKARetornou.Value);
end;

procedure TFmPrincipal.GradePPICellClick(Column: TColumn);
var
  DataHora, DataSai, DataRec, DataRet: TDateTime;
  Saiu, Recebeu, Retornou, Conta: Integer;
begin
  if (Column.FieldName = 'Saiu') and (DModG.QrPPISaiu.Value = 0) then
  begin
    DataHora := Now();
    if Geral.MensagemBox('Confirma a sa�da do protocolo ' + IntToStr(
    DModG.QrPPIConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
    ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) =
    ID_YES then
    begin
      Saiu := VAR_USUARIO;
      DataSai := DataHora;
      Conta := DModG.QrPPIConta.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
      'Saiu', 'DataSai'], ['Conta'], [Saiu, DataSai], [Conta], True) then
      DModG.ReopenPTK();
    end;
  end;
  //
  if (Column.FieldName = 'Recebeu') and (DModG.QrPPIRecebeu.Value = 0) then
  begin
    if DModG.QrPPISaiu.Value = 0 then
    begin
      Geral.MensagemBox('� necess�rio informar a sa�da antes do recebimento!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else
    begin
      DataHora := Now();
      if Geral.MensagemBox('Confirma o recebimento do protocolo ' + IntToStr(
      DModG.QrPPIConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
      ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) =
      ID_YES then
      begin
        Recebeu := VAR_USUARIO;
        DataRec := DataHora;
        Conta := DModG.QrPPIConta.Value;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Recebeu', 'DataRec'], ['Conta'], [Recebeu, DataRec], [Conta], True) then
        DModG.ReopenPTK();
      end;
    end;
  end;
  //
  if (Column.FieldName = 'Retornou') and (DModG.QrPPIRetornou.Value = 0) then
  begin
    if DModG.QrPPIRecebeu.Value = 0 then
    begin
      Geral.MensagemBox('� necess�rio informar o recebimento antes do retorno!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else
    begin
      DataHora := Now();
      if Geral.MensagemBox('Confirma o retorno do protocolo ' + IntToStr(
      DModG.QrPPIConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
      ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) =
      ID_YES then
      begin
        Retornou := VAR_USUARIO;
        DataRet := DataHora;
        Conta := DModG.QrPPIConta.Value;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Retornou', 'DataRet'], ['Conta'], [Retornou, DataRet], [Conta], True) then
        DModG.ReopenPTK();
      end;
    end;
  end;
  //
end;

procedure TFmPrincipal.GradePPIDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'NO_RETORNA') then
  begin
    if DModG.QrPPIRetorna.Value = 1 then
    begin
      Txt := clWindow;
      Bak := clRed;
    end else begin
      Txt := clWindow;
      Bak := clBlue;
    end;
    MyObjects.DesenhaTextoEmDBGrid(GradePPI, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
  if Column.FieldName = 'Saiu' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePPI), Rect, 1, DModG.QrPPISaiu.Value);
  if Column.FieldName = 'Recebeu' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePPI), Rect, 1, DModG.QrPPIRecebeu.Value);
  if Column.FieldName = 'Retornou' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePPI), Rect, 1, DModG.QrPPIRetornou.Value);
end;

procedure TFmPrincipal.HistoricoConsumo();
begin
  if DBCheck.CriaFm(TFmLeituraImp, FmLeituraImp, afmoNegarComAviso) then
  begin
    FmLeituraImp.ShowModal;
    FmLeituraImp.Destroy;
  end;
end;

procedure TFmPrincipal.GBMoradoresClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondFichaCad, FmCondFichaCad, afmoNegarComAviso) then
  begin
    FmCondFichaCad.ShowModal;
    FmCondFichaCad.Destroy;
  end;
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
  procedure AvisaSetarOpcoes;
  var
    Caminho: String;
  begin
    Caminho := 'Dermatek\Syndic\Opcoes';
    if not Geral.ReadAppKeyLM('JaSetado', Caminho, ktBoolean, False) then
    begin
      if Geral.MensagemBox('O aplicativo detectou que as op��es do '+
      'aplicativo n�o foram configuradas ainda. Deseja configur�-las agora?',
      'Aviso de Op��es', MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) =
      ID_YES then MostraOpcoesSyndic();
    end;
  end;
  procedure NovoItem(var Seq: Integer; const Item: integer; const Local, Acao: String);
  begin
    Inc(Seq, 1);
    DModG.QrUpdPID1.Params[00].AsInteger := Seq;
    DModG.QrUpdPID1.Params[01].AsInteger := Item;  // Mostra Janela
    DModG.QrUpdPID1.Params[02].AsString  := Local;
    DModG.QrUpdPID1.Params[03].AsString  := Acao;
    DModG.QrUpdPID1.ExecSQL;
  end;
const
  InfoSeq = 6;
var
  i, U: integer;
  //IP_Default
  Retorno: String;
  Continuar: Boolean;
begin
  try
    if ZZTerminate then
      Exit;
    Application.ProcessMessages;
    Screen.Cursor := crHourGlass;
    if DModG <> nil then
    begin
      // Precisa ser antes!
      DModG.ConectaBDLocalViaServidor();
      MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Criando banco de dados pessoal');
      DModG.MyPID_DB_Cria();
      //
      //Verifica se m�dulo WEB est� dispon�vel
      DmodG.ReopenMaster();      
      if Pos('WEB', DModG.QrMasterHabilModulos.Value) > 0 then
        FTemModuloWEB := True
      else
        FTemModuloWEB := False;
      //
      if Dmod.QrControle.FieldByName('InfoSeq').AsInteger < InfoSeq then
      begin
        UCriar.RecriaTempTable('InfoSeq', DModG.QrUpdPID1, False);
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('INSERT INTO infoseq SET ');
        DModG.QrUpdPID1.SQL.Add('Linha=:P0, Item=:P1, Local=:P2, Acao=:P3');
        DModG.QrUpdPID1.SQL.Add('');
        Application.CreateForm(TFmInfoSeq, FmInfoSeq);
        i := 0;
        if Dmod.QrControle.FieldByName('InfoSeq').AsInteger < 1 then NovoItem(i, 1,
          'Ferramentas > Op��es > Espec�ficas: Impress�es',
          'Impressora de cheques');
        if Dmod.QrControle.FieldByName('InfoSeq').AsInteger < 1 then NovoItem(i, 2,
          'Gerencia Clientes: Bloqueto: Atraso pagamento',
          '% Multa e % de juros');
        if Dmod.QrControle.FieldByName('InfoSeq').AsInteger < 2 then NovoItem(i, 1,
          'Ferramentas > Op��es > Espec�ficas: Impress�es',
          'Bloqueto Condominial');
        if Dmod.QrControle.FieldByName('InfoSeq').AsInteger < 3 then NovoItem(i, 2,
          'Gerencia Clientes: Ag�ncia/C�digo cedente bloqueto:',
          'Informar a ag�ncia / c�digo cedente exatamente como consta no boleto. ' +
          'Caso n�o tenha, solicitar ao banco correspondente');
        if Dmod.QrControle.FieldByName('InfoSeq').AsInteger < 4 then NovoItem(i, 2,
          'Gerencia Clientes: Orelha > Parecer do conselho; ' +
          'Bot�o: Condom. > Parecer do conselho',
          'Editar o texto do parecer do conselho fiscal do condom�nio.');
        if Dmod.QrControle.FieldByName('InfoSeq').AsInteger < 5 then NovoItem(i, 3,
          'Ferramentas > Op��es > Diversos: "Orelha" Cobran�a (CNAB); ',
          'Informar a conta de Juros de mora p�s pagamento a menor');
        if Dmod.QrControle.FieldByName('InfoSeq').AsInteger < 6 then NovoItem(i, 3,
          'Ferramentas > Op��es > Diversos: "Orelha" Cobran�a (CNAB); ',
          'Informar a conta de tarifa de cobran�a de bloqueto.');
        if i > 0 then
        begin
          UnDmkDAC_PF.AbreQuery(FmInfoSeq.QrInfoSeq, DModG.MyPID_DB);
          //
          FmInfoSeq.BtMostra.Visible := False;
          FmInfoSeq.FTmpTable        := 'infoseq';
          FmInfoSeq.ShowModal;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE controle SET InfoSeq=:P0');
          Dmod.QrUpd.Params[0].AsInteger := InfoSeq;
          Dmod.QrUpd.ExecSQL;
          //
          Dmod.QrControle.Close;
          Dmod.QrControle.Open;
        end;
        FmInfoSeq.Destroy;
      end;
      //
      // Local
      VAR_IMPCHEQUE       := Geral.ReadAppKeyCU('ImpCheque', 'Dermatek', ktInteger, 0);
      VAR_IMPCHEQUE_PORTA := Geral.ReadAppKeyCU('ImpCheque_Porta', 'Dermatek', ktString, 'COM2');
      // Servidor
      VAR_IMPCH_IP        := Geral.ReadAppKeyCU('ImpreCh_IP', 'Dermatek', ktString, '127.0.0.1');
      VAR_IMPCHPORTA      := Geral.ReadAppKeyCU('ImpreChPorta', 'Dermatek', ktInteger, 9520);
      //
      MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Verificando impressora de cheque');
      MyPrinters.VerificaImpressoraDeCheque(Retorno);
      if Retorno <> '' then
        Geral.MensagemBox(Retorno, 'Aviso', MB_OK+MB_ICONWARNING);
      //
      MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Verificando feriados futuros');
      UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados);
      //
      VerificaOrelhas;
      //
      MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Conectando ao banco de dados local');
      //DModG.ConectaBDLocalViaServidor();
      //MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Criando banco de dados pessoal');
      //DModG.MyPID_DB_Cria();
      MyObjects.Informa2(LaAviso3, LaAviso2, True, '');
      //
      MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Setando fontes de dados');
      DBGEmeioProt.DataSource       := Dmod.DsEmeioProt;
      DBGEmeioProtPak.DataSource    := Dmod.DsEmeioProtPak;
      DBGEmeioProtPakIts.DataSource := Dmod.DsEmeioProtPakIts;
      DBEdEmeioProtTot.DataSource   := Dmod.DsEmeioProtTot;
      //
      DmCond.QrSumBaAUni.Close;
      DmCond.QrSumBaAUni.Database := DmodG.MyPID_DB;
      //
      if not FAtualizouFavoritos then
      begin
        MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Criando favoritos');
        DModG.CriaFavoritos(AdvToolBarPagerNovo, LaAviso3, LaAviso2, AGBAcesRapid, FmPrincipal);
        //
        FAtualizouFavoritos := True;
      end;
      //
      MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Conferindo emiss�es de contas mensais');
      FContasMesEmiss := Geral.ReadAppKeyCU('ContasMesEmiss_Config', Application.Title, ktInteger, 1);
      case FContasMesEmiss of
        0: Continuar := False;
        1:
        begin
          U := Trunc(Date());
          Continuar := Geral.ReadAppKeyCU('ContasMesEmiss_Last', Application.Title,
            ktInteger, 0) <> U;
          Geral.WriteAppKeyCU('ContasMesEmiss_Last', Application.Title, U, ktInteger);
        end
        else Continuar := True;
      end;
      if Continuar then
        VerificaEmissoesDeContasMensais();
      //
    { TODO : Retirar no futuro junto com o campo "Assistente" da tabela "cond" }
      // Tempor�rio!!! 2011-09-20
      MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Sincronizando Assistente x AccManager');
      DmCond.VerificaAccManager(saDeNaoZeradoParaZerado);
      // Fim Tempor�rio!!! 2011-09-20
      //
      //
      DModG.VerificaCartasAImprimir(
        GB_Cartas, LaCartas1A, LaCartas1B, Lacartas1C);
      DModG.AtualizaEntiConEnt();       
      //
      UFixBugs.MostraFixBugs(['']);
      //
      DModG.ReopenControle;
      (*
      if (DModG.QrControle.FieldByName('AtualizouCamposWEB').AsInteger = 0) and
        (Grl_Geral.LiberaModulo(CO_DMKID_APP, Grl_Geral.ObtemSiglaModulo(mdlappWEB),
        DModG.QrMasterHabilModulos.Value))
      then
        DModG.AtualizaCamposWEB(DModG.QrOpcoesGerl, Dmod.QrUpd, Dmod.MyDB);
      *)
      //
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      TmVersao.Enabled := True;
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
    end;
  finally
    MyObjects.Informa2(LaAviso3, LaAviso2, False, '');
    Screen.Cursor := crDefault;
    //
    TmSuporte.Enabled := True;
  end;
end;

procedure TFmPrincipal.FormResize(Sender: TObject);
begin
  AjustaEstiloImagem(ImgDescanso.Tag);
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  Result := 0;//FmFaturas.QrFaturasCodigo.Value;
end;

procedure TFmPrincipal.CkPTK_AbertosClick(Sender: TObject);
begin
  DModG.FFiltroPPI := RGProtoFiltro.ItemIndex;
  DModG.ReopenPTK();
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  Result := '';//FormatDateTime(VAR_FORMATDATE, FmFaturas.QrFaturasData.Value);
end;

procedure TFmPrincipal.Composiesdendices1Click(Sender: TObject);
begin
  MostraCfgInfl(0);
end;

procedure TFmPrincipal.MostraCfgInfl(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCfgInfl, FmCfgInfl, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCfgInfl.LocCod(Codigo, Codigo);
    FmCfgInfl.ShowModal;
    FmCfgInfl.Destroy;
  end;
end;

procedure TFmPrincipal.ConfiguraesdeEnviodeMensagens1Click(Sender: TObject);
begin
  MostraFormDiarCEMCad();
end;

procedure TFmPrincipal.Consultaprotocolos1Click(Sender: TObject);
var
  Msg, Link: String;
begin
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg) then
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
    begin
      Link := 'http://syndinet.dermatek.net.br/' +
                DModG.QrOpcoesGerl.FieldByName('WebId').AsString + '.php?page=proimpbol';
      //
      ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
    end;
  end else
    Geral.MB_Aviso(Msg);
end;

procedure TFmPrincipal.ContasControladas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaContasControladas(0);
end;

procedure TFmPrincipal.ContasSazonais1Click(Sender: TObject);
begin
  FinanceiroJan.MostraContasSazonais;
end;

procedure TFmPrincipal.Controledevisitas1Click(Sender: TObject);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
  begin
    if DBCheck.CriaFm(TFmVisitasCli2, FmVisitasCli2, afmoNegarComAviso) then
    begin
      FmVisitasCli2.ShowModal;
      FmVisitasCli2.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.Controledevisitas21Click(Sender: TObject);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
  begin
    if DBCheck.CriaFm(TFmVisitasCli, FmVisitasCli, afmoNegarComAviso) then
    begin
      FmVisitasCli.ShowModal;
      FmVisitasCli.Destroy;
    end;
  end;
end;

function TFmPrincipal.ConverteWPerfilToWPerfilSyndic(Perfil: Integer): Integer;
begin
  case Perfil of
      0: Result := 9;   //Administrador
      1: Result := 3;   //Grupo
      2: Result := 7;   //S�ndico
      3: Result := 1;   //Cond�mino
    else Result := -99; //N�o localizado
  end;
end;

function TFmPrincipal.ConverteWPerfilSyndicToWPerfil(Perfil: Integer): Integer;
begin
  case Perfil of
      9: Result := 0;   //Administrador
      3: Result := 1;   //Grupo
      7: Result := 2;   //S�ndico
      1: Result := 3;   //Cond�mino
    else Result := -99; //N�o localizado
  end;
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmPrincipal.Saldodecontas1Click(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas(0);
end;

procedure TFmPrincipal.VerificaBDGlobal1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
  begin
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.Destroy;
  end;
end;

procedure TFmPrincipal.VerificaBDServidor1Click(Sender: TObject);
begin
  if not VAR_VERIFI_DB_CANCEL then
  begin
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
end;

procedure TFmPrincipal.VerificaBDWeb1Click(Sender: TObject);
begin
  DmkWeb_Jan.MostraVerifiDBi(DmodG.QrOpcoesGerl, Dmod.MyDBn, VAR_VERIFI_DB_CANCEL);
end;

procedure TFmPrincipal.VerificaEmissoesDeContasMensais();
begin
  if FindWindow('TFmContasConfEmisAll', nil) = 0 then
    MyObjects.CriaForm_AcessoTotal(TFmContasConfEmisAll, FmContasConfEmisAll);
  FmContasConfEmisAll.Show;
  FmContasConfEmisAll.VerificaEmissoes();
  //FmContasConfEmisAll.Destroy;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao, VersaoSyncobra: Integer;
  Arq, AplDir: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'Syndi3', 'Syndi3',
    Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO, CO_DMKID_APP,
    DModG.ObtemAgora(), Memo3, dtExec, Versao, Arq, False, ApenasVerifica, BalloonHint1);
  //
  if Result then
  begin
    if DModG.EhOServidor then //Deve rodar apenas no servidor
    begin
      AplDir := ExtractFilePath(Application.ExeName);
      if FileExists(AplDir + 'Syncobra.exe') then
      begin
        VersaoSyncobra := Geral.VersaoInt2006(Geral.FileVerInfo(AplDir +
          'Syncobra.exe', 3 (*Versao*)));
        //
        DmkWeb.VerificaAtualizacaoVersao2(True, True, 'Syncobra', 'Syncobra',
          Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), VersaoSyncobra, 30,
          DModG.ObtemAgora(), Memo1, dtExecAux, Versao, Arq, False);
      end;
    end;
  end;
end;

procedure TFmPrincipal.VerificaOrelhas();
{
var
  Select: Integer;
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Select := 0;
    //
    MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Verificando protocolos');
    GradePTK.DataSource := DModG.DsPTK;
    GradePPI.DataSource := DModG.DsPPI;
    DModG.ReopenPTK();
    if DModG.QrPTK.RecordCount > 0 then
    begin
      if Select = 0 then
        PageControl1.ActivePageIndex := 1;
    end;
    MyObjects.Informa2(LaAviso3, LaAviso2, False, '');
  finally
    Screen.Cursor := MyCursor;
  end;
}
var
  Mostra: Boolean;
  Select: Integer;
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Select := 0;
    //
    MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Verificando provis�es base');
    Mostra := False;
    UnDmkDAC_PF.AbreMySQLQuery0(QrDupl, Dmod.MyDB, [
    'SELECT COUNT(pbc.Codigo) ITENS, pbc.Conta, ',
    'con.Nome NOMECONTA ',
    'FROM prevbac pbc ',
    'LEFT JOIN contas con ON con.Codigo=pbc.Conta ',
    'GROUP BY pbc.Conta ',
    'ORDER BY ITENS DESC, pbc.Conta ',
    '']);
    //
    if QrDupl.RecordCount > 0 then
    begin
      if QrDuplItens.Value > 1 then
      begin
        Mostra := True;
        PageControl1.ActivePageIndex := 2;
        PageControl2.ActivePageIndex := 0;
        Select := 2;
      end;
    end;
    if Select = 0 then
    begin
      PageControl1.ActivePageIndex := 0;
      PageControl2.ActivePageIndex := 0;
      Select := 3;
    end;
    Panel2.Visible := Mostra;
    //
    MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Verificando protocolos');
    // em caso de logoff
    EdEmpresa.ValueVariant := 0;
    //
    GradePTK.DataSource := DModG.DsPTK;
    GradePPI.DataSource := DModG.DsPPI;
    DModG.ReopenPTK();
    if (DModG.QrPTK.State <> dsInactive)
    and (DModG.QrPTK.RecordCount > 0) then
    begin
      if Select = 0 then
      begin
        PageControl1.ActivePageIndex := 1;
        PageControl3.ActivePageIndex := 1;
        //Select := -1;
      end;
    end;
    MyObjects.Informa2(LaAviso3, LaAviso2, False, '');
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    AdvToolBarButton8, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(AdvToolBarButton5, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

procedure TFmPrincipal.UnicaEmpresa1Click(Sender: TObject);
begin
  FinanceiroJan.ExtratosFinanceirosEmpresaUnica();
end;

procedure TFmPrincipal.BtMenuClick(Sender: TObject);
begin
{### buscar do syndic PMMenu}
end;

procedure TFmPrincipal.Bancos1Click(Sender: TObject);
begin
  RetornoCNABBancos();
end;

procedure TFmPrincipal.BitBtn17Click(Sender: TObject);
var
  DataHora, DataSai: TDateTime;
  Saiu, Conta: Integer;
begin
  DataHora := Now();
  if Geral.MensagemBox('Confirma a sa�da de todos protocolos do lote ' + IntToStr(
  DModG.QrPTKLote.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
  ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    Saiu := VAR_USUARIO;
    DataSai := DataHora;
    DModG.QrPPI.First;
    while not DModG.QrPPI.Eof do
    begin
      if DModG.QrPPISaiu.Value = 0 then
      begin
        Conta := DModG.QrPPIConta.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Saiu', 'DataSai'], ['Conta'], [Saiu, DataSai], [Conta], True);
      end;
      DModG.QrPPI.Next;
      //
    end;
    DModG.ReopenPTK();
  end;
end;

procedure TFmPrincipal.BitBtn18Click(Sender: TObject);
var
  DataHora,DataRec: TDateTime;
  Recebeu, Conta: Integer;
begin
  DataHora := Now();
  if Geral.MensagemBox('Confirma o recebimento de todos protocolos do lote ' + IntToStr(
  DModG.QrPTKLote.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
  ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    Recebeu := VAR_USUARIO;
    DataRec := DataHora;
    DModG.QrPPI.First;
    while not DModG.QrPPI.Eof do
    begin
      if (DModG.QrPPISaiu.Value <> 0) and (DModG.QrPPIRecebeu.Value = 0) then
      begin
        Conta := DModG.QrPPIConta.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Recebeu', 'DataRec'], ['Conta'], [Recebeu, DataRec], [Conta], True);
      end;
      DModG.QrPPI.Next;
      //
    end;
    DModG.ReopenPTK();
  end;
end;

procedure TFmPrincipal.BitBtn19Click(Sender: TObject);
var
  DataHora,DataRet: TDateTime;
  Retornou, Conta: Integer;
begin
  DataHora := Now();
  if Geral.MensagemBox('Confirma o recebimento de todos protocolos do lote ' + IntToStr(
  DModG.QrPTKLote.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
  ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    Retornou := VAR_USUARIO;
    DataRet := DataHora;
    DModG.QrPPI.First;
    while not DModG.QrPPI.Eof do
    begin
      if (DModG.QrPPIRecebeu.Value <> 0) and (DModG.QrPPIRetorna.Value = 1)
      and (DModG.QrPPIRetornou.Value = 0) then
      begin
        Conta := DModG.QrPPIConta.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Retornou', 'DataRet'], ['Conta'], [Retornou, DataRet], [Conta], True);
      end;
      DModG.QrPPI.Next;
      //
    end;
    DModG.ReopenPTK();
  end;
end;

procedure TFmPrincipal.BtReceDespClick(Sender: TObject);
begin
  FinanceiroJan.MostraReceDesp(0);
end;

procedure TFmPrincipal.BtReenviaClick(Sender: TObject);
var
  PreEmeio: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    PreEmeio := DMod.QrControle.FieldByName('PreMailReenv').AsInteger;
    if PreEmeio > 0 then
    begin
      Dmod.VerificaReenvioEmailAvisoRecebBloqueto(PreEmeio, PB2);
      Dmod.EmailBloqueto(PB2(*, False*), 0, False);
      //
    end else Geral.MB_Aviso('Pr�-emeio n�o definido nas op��es ' +
    'espec�ficas do aplicativo! Envio abortado!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.AGBAcesRapidClick(Sender: TObject);
var
  Periodo, CliInt: Integer;
begin
  if FCliInt_ = 0 then
  begin
    CliInt := Geral.ReadAppKeyCU('Condominio2', Application.Title,
      ktInteger, 0);
    DefineVarsCliInt(CliInt);
  end else
    DModG.DefineDataMinima(FEntInt);
  //
  Periodo := Geral.ReadAppKeyCU('Periodo', Application.Title,
    ktInteger, 0);
  //
  if FCliInt_ <> 0 then
     MostraFmCondGer(True, Periodo)
  else
    DefineCondominio(Periodo, True);
end;

procedure TFmPrincipal.BtAtzListaMailProtClick(Sender: TObject);
begin
  try
    Dmod.QrEmeioProt.DisableControls;
    Dmod.QrEmeioProtPak.DisableControls;
    Dmod.QrEmeioProtPakIts.DisableControls;
    //
    Dmod.AtzListaMailProt_WebDwn(LaAviso4, LaAviso5, PB2);
    Dmod.AtzListaMailProt_LocPgt(LaAviso4, LaAviso5, PB2, MeAvisos);
    Dmod.AtzListaMailProt_WebSin(LaAviso4, LaAviso5, PB2, MeAvisos);
    Dmod.AtzListaMailProt_WebCln(LaAviso4, LaAviso5, PB2, MeAvisos);
  finally
    Dmod.QrEmeioProt.EnableControls;
    Dmod.QrEmeioProtPak.EnableControls;
    Dmod.QrEmeioProtPakIts.EnableControls;
    MyObjects.Informa2(LaAviso4, LaAviso5, False, 'Atualizado em ' + Geral.FDT(Now(), 0));
  end;
end;

procedure TFmPrincipal.BtBalanceteClick(Sender: TObject);
begin
  FinanceiroJan.MostraCashBal(0);
end;

procedure TFmPrincipal.BtComandoClick(Sender: TObject);
var
  Retorno: String;
begin
  EdResposta.Text := '';
  MeResposta.Text := '';
  EdResposta.Text := PertoChekP.EnviaPertoChek2(EdComando.Text, Retorno);
  MeResposta.Text := Retorno;
end;

procedure TFmPrincipal.BtImprimeCartasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCobrarGer, FmCobrarGer, afmoNegarComAviso) then
  begin
    FmCobrarGer.ShowModal;
    FmCobrarGer.Destroy;
  end;
end;

procedure TFmPrincipal.AGBExpCobrClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmProdusys2, FmProdusys2, afmoNegarComAviso) then
  begin
    FmProdusys2.ShowModal;
    FmProdusys2.Destroy;
  end;
end;

procedure TFmPrincipal.AGBGerCondClick(Sender: TObject);
begin
  DefineCondominio(0, True);
end;

procedure TFmPrincipal.BtNegritoClick(Sender: TObject);
var
  Retorno: String;
begin
  EdResposta.Text := PertoChekP.EnviaPertoChek2(',1000', Retorno);
  MeResposta.Text := Retorno;
end;

procedure TFmPrincipal.BtNormalClick(Sender: TObject);
var
  Retorno: String;
begin
  EdResposta.Text := PertoChekP.EnviaPertoChek2(',0000', Retorno);
  MeResposta.Text := Retorno;
end;

procedure TFmPrincipal.AGBPesqPagClick(Sender: TObject);
begin
  InadimplenciaNew(0);
end;

procedure TFmPrincipal.AGBPlanRelCabClick(Sender: TObject);
begin
  FinanceiroJan.MostraPlanRelCab;
end;

procedure TFmPrincipal.BtRefreshClick(Sender: TObject);
begin
  VerificaOrelhas();
end;

procedure TFmPrincipal.BtRefrPrtoClick(Sender: TObject);
var
  Retorno: String;
begin
  PertoChekP.DesabilitaPertoCheck;
  PertoChekP.HabilitaPertoChek(Retorno);
  if Retorno <> '' then
    Geral.MensagemBox(Retorno, 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmPrincipal.Button3Click(Sender: TObject);
begin
  Dmod.Teste_ListaCondominios(QrCond, Memo6, QrCondCodigo, QrCondNO_COND);
end;

procedure TFmPrincipal.AtualizaTextoBtAcessoRapido();
var
  Texto, Cond, MesAno: String;
  CliInt, Periodo: Integer;
begin
  Texto := 'Acesso'#13#10'R�pido';
  CliInt := Geral.ReadAppKeyCU('Condominio2', Application.Title,
      ktInteger, 0);
  if CliInt > 0 then
  begin
    Periodo := Geral.ReadAppKeyCU('Periodo', Application.Title,
      ktInteger, 0);
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Sigla FROM cond ');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrAux.Params[0].AsInteger := CliInt;
    Dmod.QrAux.Open;
    //
    Cond := Trim(Dmod.QrAux.FieldByName('Sigla').AsString);
//    if (Cond = '') or (Cond = '?') then Cond := 'Cond=' + IntToStr(CliInt);
    if (Cond = '') or (Cond = '?') then Cond := 'N� ' + IntToStr(CliInt);
    if Periodo > 0 then
      MesAno := Geral.FDT(Geral.PeriodoToDate(Periodo, 1, False), 5)
    else
      MesAno := '--/--';
    Texto := Cond + #13#10 + Uppercase(MesAno);
  end;
  AGBAcesRapid.Caption := Texto;
end;

procedure TFmPrincipal.Avisos1Click(Sender: TObject);
var
  Msg, Link: String;
begin
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg) then
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
    begin
      Link := 'http://syndinet.dermatek.net.br/' +
                DModG.QrOpcoesGerl.FieldByName('WebId').AsString + '.php?page=avisos';
      //
      ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
    end;
  end else
    Geral.MB_Aviso(Msg);
end;

procedure TFmPrincipal.RetornoCNAB();
begin
  if DBCheck.CriaFm(TFmCNAB_Ret2a, FmCNAB_Ret2a, afmoNegarComAviso) then
  begin
    FmCNAB_Ret2a.ShowModal;
    FmCNAB_Ret2a.Destroy;
  end;
end;

procedure TFmPrincipal.RetornoCNABBancos();
begin
  if DBCheck.CriaFm(TFmCNAB_RetBco, FmCNAB_RetBco, afmoNegarComAviso) then
  begin
    FmCNAB_RetBco.ShowModal;
    FmCNAB_RetBco.Destroy;
  end;
end;

procedure TFmPrincipal.RGProtoFiltroClick(Sender: TObject);
begin
  DModG.FFiltroPPI := RGProtoFiltro.ItemIndex;
  DModG.ReopenPTK();
end;

procedure TFmPrincipal.CalculaCambioAlterado(MyProgress: TProgressBar);
//var
  //Valor: Double;
begin
  // Compatibilidade. Ver adiante
  (*QrPQx.Close;
  QrPQx.Open;
  if QrPQx.RecordCount > 0 then
  begin
    if MyProgress <> nil then
    begin
      MyProgress.Position := 0;
      MyProgress.Visible := True;
      MyProgress.Max := QrPQx.RecordCount;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE PQ SET Valor=:P0 WHERE Codigo=:P1');
    while not QrPQx.Eof do
    begin
      if MyProgress <> nil then
      begin
        MyProgress.Position := MyProgress.Position + 1;
        MyProgress.Update;
      end;
      Valor := MLAGeral.CalculaValorPQ(QrPQxReais.Value, QrPQxDolar.Value,
      QrPQxEuro.Value, QrPQxPreco.Value, QrPQxICMS.Value, QrPQxRICMS.VAlue,
      QrPQxRICMSF.Value, QrPQxIPI.VAlue, QrPQxRIPI.VAlue, QrPQxFrete.VAlue,
      QrPQxCFin.VAlue, Dmod.QrCambiosDolar.Value, Dmod.QrCambiosEuro.Value,
      QrPQxMoeda.Value, 0);
      //
      Dmod.QrUpd.Params[0].AsFloat   := Valor;
      Dmod.QrUpd.Params[1].AsInteger := QrPQxCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      QrPQx.Next;
    end;
  end;
  QrPQx.Close;*)
end;

procedure TFmPrincipal.CadastroDeAnot;
begin
  if DBCheck.CriaFm(TFmAnotacoes, FmAnotacoes, afmoNegarComAviso) then
  begin
    FmAnotacoes.ShowModal;
    FmAnotacoes.Destroy;
  end;
end;

//ver duplicidade no prev!

{ Parei Aqui!
TODO LIST

Ver se na duplica��o est� copiando o CtrlQuitPg e/ou ID_Pgto
}

{
verifica��o das tabelas web!
testar Reparcelamento! Para isso precisa fazer sincroniza��o!
}

end.
