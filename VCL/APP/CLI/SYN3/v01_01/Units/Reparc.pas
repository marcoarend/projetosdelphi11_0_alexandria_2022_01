unit Reparc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DBCtrls, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, Mask, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmReparc = class(TForm)
    Panel1: TPanel;
    QrPropriet: TmySQLQuery;
    QrProprietCodigo: TIntegerField;
    QrProprietNOMEPROP: TWideStringField;
    DsPropriet: TDataSource;
    QrCondImov: TmySQLQuery;
    QrCondImovConta: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    DsCondImov: TDataSource;
    Panel11: TPanel;
    PnPesq1: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdPropriet: TdmkEditCB;
    CBPropriet: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdCondImov: TdmkEditCB;
    CBCondImov: TdmkDBLookupComboBox;
    QrPesq1: TmySQLQuery;
    DsReparcI2: TDataSource;
    DBGrid1: TDBGrid;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    TPPriVct: TdmkEditDateTimePicker;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    TPDtaIni: TdmkEditDateTimePicker;
    Label5: TLabel;
    TPDtaFim: TdmkEditDateTimePicker;
    QrReparcI2: TmySQLQuery;
    QrReparcI2Credito: TFloatField;
    QrReparcI2Multa: TFloatField;
    QrReparcI2MoraDia: TFloatField;
    QrReparcI2Cliente: TIntegerField;
    QrReparcI2Depto: TIntegerField;
    QrReparcI2Mez: TIntegerField;
    QrReparcI2Vencimento: TDateField;
    QrReparcI2DataCad: TDateField;
    QrReparcI2FatNum: TFloatField;
    QrReparcI2Vencto: TDateField;
    QrReparcI2TxaJur: TFloatField;
    QrReparcI2TxaMul: TFloatField;
    QrReparcI2ValBol: TFloatField;
    QrReparcI2ValJur: TFloatField;
    QrReparcI2ValMul: TFloatField;
    QrReparcI2ValPgt: TFloatField;
    QrReparcI2Controle: TIntegerField;
    QrPesq2: TmySQLQuery;
    QrPesq2Credito: TFloatField;
    QrPesq2Multa: TFloatField;
    QrPesq2MoraDia: TFloatField;
    QrPesq2Cliente: TIntegerField;
    QrPesq2Depto: TIntegerField;
    QrPesq2Mez: TIntegerField;
    QrPesq2Vencimento: TDateField;
    QrPesq2DataCad: TDateField;
    QrPesq2FatNum: TFloatField;
    QrPesq2Vencto: TDateField;
    QrPesq2TxaJur: TFloatField;
    QrPesq2TxaMul: TFloatField;
    QrPesq2ValBol: TFloatField;
    QrPesq2ValJur: TFloatField;
    QrPesq2ValMul: TFloatField;
    QrPesq2ValPgt: TFloatField;
    QrPesq2Controle: TIntegerField;
    Qrwcli: TmySQLQuery;
    Dswcli: TDataSource;
    GBwcli: TGroupBox;
    QrwcliParc_ValMin: TFloatField;
    QrwcliParc_QtdMax: TIntegerField;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    QrSumI: TmySQLQuery;
    DsSumI: TDataSource;
    QrSumIValBol: TFloatField;
    QrSumIValJur: TFloatField;
    QrSumIValMul: TFloatField;
    QrSumIValPgt: TFloatField;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    DBEdit3: TDBEdit;
    Label10: TLabel;
    DBEdit4: TDBEdit;
    Label11: TLabel;
    DBEdit5: TDBEdit;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    QrReparcP2: TmySQLQuery;
    DsReparcP2: TDataSource;
    DBGrid2: TDBGrid;
    QrReparcP2TotParc: TIntegerField;
    QrParcelasP: TmySQLQuery;
    DsParcelasP: TDataSource;
    QrParcelasPNumParc: TIntegerField;
    QrParcelasPVencto: TDateField;
    QrParcelasPValor: TFloatField;
    DBGrid3: TDBGrid;
    QrReparcP2TotalVal: TFloatField;
    QrTP: TmySQLQuery;
    QrTPValor: TFloatField;
    QrCond: TmySQLQuery;
    QrCondPercJuros: TFloatField;
    QrCondPercMulta: TFloatField;
    EdPercMulta: TdmkEdit;
    Label14: TLabel;
    EdPercJuros: TdmkEdit;
    Label15: TLabel;
    Panel4: TPanel;
    QrLct: TmySQLQuery;
    DsLct: TDataSource;
    DBGrid4: TDBGrid;
    QrLctCredito: TFloatField;
    QrLctMulta: TFloatField;
    QrLctMoraDia: TFloatField;
    QrLctCliente: TIntegerField;
    QrLctDepto: TIntegerField;
    QrLctMez: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctDataCad: TDateField;
    QrLctFatNum: TFloatField;
    QrLctVencto: TDateField;
    QrLctTxaJur: TFloatField;
    QrLctTxaMul: TFloatField;
    QrLctValBol: TFloatField;
    QrLctControle: TIntegerField;
    QrLctSub: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtPesq: TBitBtn;
    BtParcela: TBitBtn;
    QrLoc: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdProprietChange(Sender: TObject);
    procedure EdProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCondImovChange(Sender: TObject);
    procedure CBCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtPesqClick(Sender: TObject);
    procedure QrReparcI2AfterOpen(DataSet: TDataSet);
    procedure QrReparcI2BeforeClose(DataSet: TDataSet);
    procedure QrReparcP2BeforeClose(DataSet: TDataSet);
    procedure QrReparcP2AfterScroll(DataSet: TDataSet);
    procedure QrReparcP2CalcFields(DataSet: TDataSet);
    procedure TPDtaIniChange(Sender: TObject);
    procedure TPDtaFimChange(Sender: TObject);
    procedure TPPriVctChange(Sender: TObject);
    procedure BtParcelaClick(Sender: TObject);
    procedure QrParcelasPBeforeClose(DataSet: TDataSet);
    procedure QrParcelasPAfterOpen(DataSet: TDataSet);
    procedure TPDtaIniClick(Sender: TObject);
    procedure TPDtaFimClick(Sender: TObject);
    procedure EdPercMultaExit(Sender: TObject);
    procedure EdPercJurosExit(Sender: TObject);
    procedure CBProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FTabLctA, FReparcI2, FReparcP2: String;
    procedure ReopenPropriet(Todos: Boolean);
    procedure ReopenCondImov();
    procedure HabilitaBtPesq();
    procedure Reopenwcli();
    procedure FechaPesquisa();
    procedure Parcelamento_parcelar();
    procedure ReopenCond();
    function  ObtemCNAB_CfgDeCond(CodCliEnt, CodCliEsp: Integer): Integer;
  public
    { Public declarations }
  end;

  var
  FmReparc: TFmReparc;

implementation

uses dmkGeral, UMySQLModule, ModuleGeral, UCreate, Module, MyDBCheck, Principal,
  UnInternalConsts, UnMyObjects, UnFinanceiro;

{$R *.DFM}

procedure TFmReparc.BtPesqClick(Sender: TObject);
var
  Hoje, Amanha, Vcto: TDateTime;
  ValJur, ValMul, ValBol, ValPgt: Double;
  //
  DtaIni, DtaFim: String;
  TxaJur, TxaMul, Juros,
  Meses, (*QtdMese,*) VlrMeses, FatNum: Double;
  QtdItens, Cond, Prop, Apto, Controle: Integer;
  //
  Vencto: String;
  QtdMax, I, J, TotParc, NumParc: Integer;
  Valor, JurosVal, MultaVal, MedMeses, NewMeses: Double;
  ArrDatas: array of TDateTime;
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '1. Excluindo dados da tabela tempor�ria "' +
      FReparcI2 + '"');
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add(DELETE_FROM + FReparcI2);
    DModG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '2. Excluindo dados da tabela tempor�ria "' +
      FReparcP2 + '"');
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add(DELETE_FROM + FReparcP2);
    DModG.QrUpdPID1.ExecSQL;
    //QtdItens := 0;
    //Meses := 0;
    //QtdMese := 0;
    //VlrMeses := 0;
    //
    //Cond := EdEmpresa.ValueVariant;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '3. Inserindo dados na tabela tempor�ria "' +
      FReparcP2 + '"');
    //
    Prop := EdPropriet.ValueVariant;
    Apto := EdCondImov.ValueVariant;
    DtaIni := Geral.FDT(TPDtaIni.Date, 1);
    DtaFim := Geral.FDT(TPDtaFim.Date, 1);
    QrPesq1.Close;
    QrPesq1.SQL.Clear;
    QrPesq1.SQL.Add('INSERT INTO ' + FReparcI2);
    QrPesq1.SQL.Add('SELECT SUM(lan.Credito) Credito, lan.Multa,  lan.MoraDia,');
    QrPesq1.SQL.Add('lan.Cliente, lan.Depto, lan.Mez, lan.Vencimento, lan.DataCad,');
    QrPesq1.SQL.Add('FatNum, lan.Vencimento Vencto, lan.MoraDia TxaJur,');
    QrPesq1.SQL.Add('lan.Multa TxaMul, SUM(lan.Credito) ValBol,');
    QrPesq1.SQL.Add('0.00 ValJur, 0.00 ValMul, 0.00 ValPgt, lan.Controle');
    QrPesq1.SQL.Add('FROM ' +  FTabLctA + ' lan');
    QrPesq1.SQL.Add('WHERE FatID in (600,601)');
    QrPesq1.SQL.Add('AND Reparcel=0');
    QrPesq1.SQL.Add('AND Tipo=2');
    QrPesq1.SQL.Add('AND FatNum>0');
    QrPesq1.SQL.Add('AND lan.Compensado < 2');
    QrPesq1.SQL.Add('AND lan.Vencimento BETWEEN :P0 AND :P1');
    QrPesq1.SQL.Add('AND lan.ForneceI = :P2');
    QrPesq1.SQL.Add('AND lan.Depto = :P3');
    QrPesq1.SQL.Add('GROUP BY lan.Cliente, lan.Depto, lan.Mez, lan.FatNum');
    QrPesq1.SQL.Add('ORDER BY lan.Vencimento');
    QrPesq1.Params[00].AsString  := DtaIni;
    QrPesq1.Params[01].AsString  := DtaFim;
    QrPesq1.Params[02].AsInteger := Prop;
    QrPesq1.Params[03].AsInteger := Apto;
    //dmkPF.LeMeuTexto(QrPesq1.SQL.Text);
    QrPesq1.ExecSQL;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '4. Obtendo dados na tabela de lan�amentos');
    //
    QrLct.Close;
    QrLct.SQL.Clear;
    QrLct.SQL.Add('SELECT lan.Credito, lan.Multa,  lan.MoraDia,');
    QrLct.SQL.Add('lan.Cliente, lan.Depto, lan.Mez, lan.Vencimento, lan.DataCad,');
    QrLct.SQL.Add('FatNum, lan.Vencimento Vencto, lan.MoraDia TxaJur,');
    QrLct.SQL.Add('lan.Multa TxaMul, lan.Credito ValBol,');
    QrLct.SQL.Add('lan.Controle, lan.Sub');
    QrLct.SQL.Add('FROM ' + FTabLctA + ' lan');
    QrLct.SQL.Add('WHERE FatID in (600,601)');
    QrLct.SQL.Add('AND Reparcel=0');
    QrLct.SQL.Add('AND Tipo=2');
    QrLct.SQL.Add('AND FatNum>0');
    QrLct.SQL.Add('AND lan.Compensado < 2');
    QrLct.SQL.Add('AND lan.Vencimento BETWEEN :P0 AND :P1');
    QrLct.SQL.Add('AND lan.ForneceI = :P2');
    QrLct.SQL.Add('AND lan.Depto = :P3');
    QrLct.SQL.Add('ORDER BY lan.Vencimento');
    QrLct.Params[00].AsString  := DtaIni;
    QrLct.Params[01].AsString  := DtaFim;
    QrLct.Params[02].AsInteger := Prop;
    QrLct.Params[03].AsInteger := Apto;
    QrLct.Open;
    // C�lculo de parcelas 1 (inadimpl�ncia)
  (*
          $hoje = DbModule_TO_DAYS('');
  if ($FmSyndicNet->CurrentUserLogin->Requerido6 > $hoje)
  {
    $Amanha = $FmSyndicNet->CurrentUserLogin->Requerido6;
  } else {
    $Amanha = $hoje + 1;
  }
          $Amanha = CalculaDataDeposito2($Amanha);
  *)
    Hoje   := Int(TPPriVct.Date);
    Amanha := UMyMod.CalculaDataDeposito(Hoje);
  (*
          for( $i=1; $i <= GetDBModule()->QrVia2->RecordCount; $i++ )
          {
  *)
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '5. Abrindo a tabela tempor�ria "' +
      FReparcI2 + '"');
    //
    QrPesq2.Close;
    QrPesq2.Open;
    QrPesq2.First;
    while not QrPesq2.Eof do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, '6. Atualizando valores na tabela tempor�ria "' +
        FReparcI2 + '" ' + FormatFloat('0', QrPesq2.RecNo) + ' de ' +
        FormatFloat('0', QrPesq2.RecordCount));
    //
  (*
            print('  <tr>');
            $ValJur = 0;
            $ValMul = 0;
            $ValBol = GetDBModule()->QrVia2->ValBol;
            $ValPgt = GetDBModule()->QrVia2->ValBol;
            //
            $vcto = DBModule_TO_DAYS(GetDBModule()->QrVia2->Vencto);
  *)
      ValJur := 0;
      ValMul := 0;
      ValBol := QrPesq2ValBol.Value;
      ValPgt := QrPesq2ValBol.Value;
      //
      Vcto := QrPesq2Vencto.Value;
  (*
            if ($vcto + 1 < $hoje) // + 1 dia (dia seguinte)
            {
              $vcto = CalculaDataDeposito2($vcto);
              if ($vcto + 1 < $hoje)
              {
  *)
      if (Vcto + 1 < Hoje) then
      begin
        Vcto := UMyMod.CalculaDataDeposito(Vcto);
        if (Vcto + 1 < Hoje) then
        begin
  (*
                $QtdItens += 1;
                $Meses = ($Amanha - $vcto) / 30;
                $QtdMese += $Meses;
                $VlrMeses += ( $Meses * $ValBol );
                //
                $TxaJur = GetDBModule()->QrVia2->TxaJur;
                if( $TxaJur == 0 ) {$TxaJur = 1;}
                //
                $TxaMul = GetDBModule()->QrVia2->TxaMul;
                if( $TxaMul == 0 ) {$TxaMul = 2;}
                //
                $Juros = $Meses * $TxaJur;
                $ValJur = round($Juros  * $ValBol / 100, 2);
                $ValMul = round($TxaMul * $ValBol / 100, 2);
                //
                $ValPgt = $ValBol + $ValJur + $ValMul;
              }
            }
  *)
          //QtdItens := QtdItens + 1;
          //Meses := (Amanha - Vcto) / 30; ERRADO!!!
          Meses := DModG.MesesEntreDatas(Vcto, Amanha);
          //QtdMese := QtdMese + Meses; ????
          //VlrMeses := (Meses * ValBol);
          //
          {
          TxaJur :=  QrPesq2TxaJur.Value;
          if TxaJur = 0 then TxaJur := 1;
          //
          }
          TxaJur := EdPercJuros.ValueVariant;
          {
          TxaMul :=  QrPesq2TxaMul.Value;
          if TxaMul = 0 then TxaMul := 2;
          }
          TxaMul := EdPercMulta.ValueVariant;
          //
          Juros := Meses * TxaJur;
          ValJur := Round(Juros * ValBol) / 100;
          ValMul := Round(TxaMul * ValBol) / 100;
          //
          ValPgt := ValBol + ValJur + ValMul;
        end;
      end;
      //FatNum   := QrPesq2FatNum.Value;
      Controle := QrPesq2Controle.Value;
      //if
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, 'ReparcI2', False, [
      'ValJur', 'ValMul', 'ValPgt'], ['Controle'], [
       ValJur,   ValMul,   ValPgt] , [Controle], False);

  (*
            $CodigoEnt = $FmSyndicNet->CurrentUserLogin->UserCodigoEsp;

            $MyLink = '<a target="_blank" href="index.php?page=sit_user_via2&CodigoEnt='. $CodigoEnt .'&bloq=' .
            GetDBModule()->QrVia2->FatNum .
            '" title="Imprime o bloqueto n� ' .
            GetDBModule()->QrVia2->FatNum . '"><img src="imgs/seta.gif" border="0" />' .
            /*GetDBModule()->QrVia2->FatNum .*/ 'imprimir</a>  ';
            print('<td style="color:#333399">' . $MyLink . '</td>');
            print('<td style="color:#333399">' . GetDBModule()->QrVia2->Vencimento . '</td>');
            print('<td style="color:#333399;text-align:right;padding-right:3px">' . FFT($ValBol, 2) . '</td>');
            print('<td style="color:#993366;text-align:right;padding-right:3px">' . FFT($ValMul, 2) . '</td>   ');
            print('<td style="color:#993366;text-align:right;padding-right:3px">' . FFT($ValJur, 2) . '</td>   ');
            print('<td style="color:#D00000;font-weight:bold;text-align:right;padding-right:3px">'   . FFT($ValPgt, 2) . '</td>  ');
            print('  </tr>                                                                    ');

  *)
  (*
            $VlrOrigi += $ValBol;
            $VlrMulta += $ValMul;
            $VlrJuros += $ValJur;
            $VlrTotal += $ValPgt;
            //
            GetDBModule()->QrVia2->Next();
          }
  *)
      (*
      VlrOrigi := VlrOrigi + ValBol;
      VlrMulta := VlrMulta + ValMul;
      VlrJuros := VlrOrigi + ValJur;
      VlrTotal := VlrOrigi + ValPgt;
      *)
      //
      QrPesq2.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '7. Reabrindo a tabela tempor�ria "' +
      FReparcI2 + '"');
    //
    QrReparcI2.Close;
    QrReparcI2.Open;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '8. Reabrindo soma da tabela tempor�ria "' +
      FReparcI2 + '"');
    //
    QrSumI.Close;
    QrSumI.Open;
  (*        print('
             </table>
             <table cellpadding="0" cellspacing="0" border="0" class="tabela2">
               <tr>
                 <td rowspan="2" colspan="2" width="190px" class="tabela_tit" style="font-size:18px">TOTAIS</td>
                 <td colspan="4" class="tabela_tit" style="border-right:none">Valores em Reais</td>
               </tr>
               <tr>
                 <td width="85px" class="tabela_tit">Original</td>
                 <td width="85px" class="tabela_tit">Multa</td>
                 <td width="85px" class="tabela_tit">Juros</td>
                 <td width="85px" class="tabela_tit" style="border-right:none">A PAGAR&sup1;</td>
               </tr>');
          print('  <tr>');
          print('<td>&nbsp;</td>');
          print('<td>&nbsp;</td>');
          print('<td style="color:#333399;text-align:right;padding-right:3px">' . FFT($VlrOrigi, 2) . '</td>');
          print('<td style="color:#993366;text-align:right;padding-right:3px">' . FFT($VlrMulta, 2) . '</td>   ');
          print('<td style="color:#993366;text-align:right;padding-right:3px">' . FFT($VlrJuros, 2) . '</td>   ');
          print('<td style="color:#D00000;font-weight:bold;text-align:right;padding-right:3px">' . FFT($VlrTotal, 2) . '</td>  ');
          print('  </tr>                                                                    ');
          print('
                    <tr>
                      <td colspan="6" width:"530px" class="tabela_textofim">');

          print('�: Valor calculado para vencimento em ' . DBModule_FROM_DAYS($Amanha, 2) . '.</td>');
          print('
                    </tr>
                  </table>
                </td>
                <td width="10">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
          ');
        } else
        // N�o h� bloquetos em atraso
        print('
          <link href="css/user.css" rel="stylesheet" type="text/css" />
          <div class="aviso">
            <img align="left" src="imgs/alerta.jpg"/>
            <p class="aviso_texto">N�o h� bloquetos em atraso</p>
            <p class="aviso_texto">para o per�odo selecionado!</p>
          </div>
        ');
      }


    }

  *)

  //////////////////////////////////////////////////////////////////////////////
  /// P A R C E L A S //////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////

(*
      if( $VlrTotal > 0 )
      {
        print('<div>');
        // N�o calcular j� est� embutido
        //$MedMeses = $VlrMeses / $VlrOrigi;
        /*
        print('Meses      : ' . $QtdMeses  . '<br/>' .
        'Val. Meses : ' . $VlrMeses  . '<br/>' .
        'Qtd. Itens : ' . $QtdItens  . '<br/>' .
        'M�dia meses : ' . $MedMeses . '<br/>');
        */
        GetDBModule()->Qrwcli->Close();
        GetDBModule()->Qrwcli->SQL =
        'SELECT * FROM wclients ' .
        'WHERE CodCliEsp="' .
          $FmSyndicNet->CurrentUserLogin->UserCodCliEsp . '"';
        GetDBModule()->Qrwcli->Open();
        //print(GetDBModule()->Qrwcli->SQL . '<br/>');
        if( GetDBModule()->Qrwcli->RecordCount > 0 )
        {
          /* Parei Aqui */
          /*
          print(
          'C�digo Cond: ' . $FmSyndicNet->CurrentUserLogin->UserCodCliEsp . '<br/>' .
          'C�digo Enti: ' . $FmSyndicNet->CurrentUserLogin->UserCodCliEnt . '<br/>' .
          'Parc_ValMin: ' . GetDBModule()->Qrwcli->Parc_ValMin . '<br/>' .
          'Parc_QtdMax: ' . GetDBModule()->Qrwcli->Parc_QtdMax . '<br/>');
          */
          //
          try {
            $DataIni2 = $FmSyndicNet->CurrentUserLogin->PesqDataIni;
            if( $DataIni2 <> '' )
            {
              $DataIni1 = ValidaDataSimples($DataIni2, False);
            }
            $DataFim2 = $FmSyndicNet->CurrentUserLogin->PesqDataFim;
            if( $DataFim2 <> '' )
            {
              $DataFim1 = ValidaDataSimples($DataFim2, False);
            }
          } catch (Exception $e) {
            $DataIni1 = '';
            $DataFim1 = '';
          }
          //
          print('
<head>
<script src="js/SpryCollapsiblePanel.js" type="text/javascript"></script>
<link href="css/user.css" rel="stylesheet" type="text/css" />
</head>
          ');
          if( $FmSyndicNet->CurrentUserLogin->SubPage1 == 'inad')
          {
            $loginid = $FmSyndicNet->CurrentUserLogin->ObtemLoginID();
            //
            $Dados = $FmSyndicNet->CurrentUserLogin->UserCodigoEnt . '#' .
            $FmSyndicNet->CurrentUserLogin->UserCodigoEsp . '#' .
            $DataIni . '#' .
            $DataFim . '#' .
            '1' . '#'.
            number_format($VlrTotal, 2, '.', '') . '#';
            $MyLink = PWDExEncode($Dados, $FSecurityString, 0);
            $FmSyndicNet->LB1->AddItem(/*$FmSyndicNet->LB1->Count . ' = ' . */$Dados);
            $FmSyndicNet->LB1->AddItem(/*$FmSyndicNet->LB1->Count . ' = ' . */$MyLink);
            $AllLink = ' <a href="index.php?page=sit_user_acei&loguser=' .
            $loginid . '&parcsel=' . $MyLink  .
            '"><img border="0" src="imgs/seta.gif"/> Aceito o parcelamento</a>';
            print('
<table cellpadding="0" cellspacing="0" border="0" class="parcel_tit">
  <tr>
    <td><h2>Parcele aqui</h2></td>
  </tr>
</table>

<div id="CollapsiblePanel1" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0"><h4><img src="imgs/setas.gif"/> Parcela �nica</h4></div>
  <div class="CollapsiblePanelContent">
    <script type="text/javascript">
    <!--
    var CollapsiblePanel1 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel1");
    //-->
    </script>
    <table cellpadding="0" width="160" cellspacing="0" border="0" id="inadparc">
      <tr>
        <td colspan="2" id="inadparctit" width="160" height="20">' .  $AllLink .
       '</td>
      </tr>
      <tr>
        <td width="80" height="20" id="inadparcsubtit1">Vencimento</td>
        <td width="80" id="inadparcsubtit2">Valor</td>
      </tr>'
            );
          print('
      <tr>
        <td class="parc_text1">' . DbModule_FROM_DAYS($Amanha, 2) . '</td>
        <td class="parc_text2">' . number_format($VlrTotal, 2, ',', '.') . '</td>
      </tr>
    </table>
  </div>
</div>
          ');
          } else
          {
            $loginid = '';
            $Dados   = '';
            $MyLink  = '';
            if( $ParcelaSel == 1)
            {
              $FmSyndicNet->LBParc->AddItem(DbModule_FROM_DAYS($Amanha, 1) . '#' .
                number_format($VlrTotal, 2, '.', ''));
              //
              $AllLink = '<a>Parcelamento</a>';
              print('
  <table cellpadding="0" width="140" cellspacing="0" border="0" class="parc">
    <tr>
      <td colspan="2" class="parc_tit" height="20">' .  $AllLink .
     '</td>
    </tr>
    <tr>
      <td width="100" height="20" class="parc_subtit">Total de parcelas</td>
      <td class="parc_text1">'. $ParcelaSel .'</td>
    </tr>
  </table>
  <table cellpadding="0" width="140" cellspacing="0" border="0" class="parc">
    <tr>
      <td class="parc_subtit">Vencimento</td>
      <td class="parc_subtit">Valor</td>
    </tr>'
              );
          print('
    <tr>
      <td class="parc_text1">' . DbModule_FROM_DAYS($Amanha, 2) . '</td>
      <td class="parc_text2">' . number_format($VlrTotal, 2, ',', '.') . '</td>
    </tr>
  </table>
          ');
            }
          }

*)
  if QrSumIValPgt.Value > 0 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '9. Obtendo dados do condom�nio!');
    // Ter certeza que � do condom�nio certo
    Reopenwcli();
    //
    // Parcela �nica
    TotParc := 1;
    NumParc := 1;
    Vencto  := Geral.FDT(Amanha, 1);
    Valor   := QrSumIValPgt.Value;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '10. Atualizando totais reajustados: 1 de ?');
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ReparcP2', False, [
    'Vencto', 'Valor'], ['TotParc', 'NumParc'], [
    Vencto, Valor], [TotParc, NumParc], False);
    //
    //MedMeses := 0;
    TxaJur   := EdPercJuros.ValueVariant;
    TxaMul   := EdPercMulta.ValueVariant;
    {
    if QrwcliParc_ValMin.Value > 0 then
      QtdMax := Trunc(Valor / QrwcliParc_ValMin.Value);
    if QrwcliParc_QtdMax.Value < QtdMax then
    }
      QtdMax := QrwcliParc_QtdMax.Value;
    //
    for I := 2 to QtdMax do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, '11. Atualizando totais reajustados: ' +
        FormatFloat('0', I) + ' de ' + FormatFloat('0', QtdMax));
      SetLength(ArrDatas, I);
      TotParc  := I;
      Valor    := 0;
      NewMeses := DModG.MediaMesesReparcelamento3(Hoje, i, ArrDatas);
      QrReparcI2.First;
      while not QrReparcI2.Eof do
      begin
        MedMeses := NewMeses - DModG.MesesEntreDatas(0, QrReparcI2Vencimento.Value);
        JurosVal := QrReparcI2ValBol.Value  * MedMeses * TxaJur / 100;
        MultaVal := QrReparcI2ValBol.Value             * TxaMul / 100;
        Valor    := Valor + QrReparcI2ValBol.Value + JurosVal + MultaVal;
        //
        QrReparcI2.Next;
      end;
      Valor := (Round(Valor / I * 100)) / 100;
      if Valor < QrwcliParc_ValMin.Value then Break;
      for J := 0 to I - 1 do
      begin
        //ShowMessage(Geral.FDT(ArrDatas[J], 2));
        // Parcela �nica
        NumParc := J + 1;
        Vencto  := Geral.FDT(ArrDatas[J], 1);
        UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ReparcP2', False, [
        'Vencto', 'Valor'], ['TotParc', 'NumParc'], [
        Vencto, Valor], [TotParc, NumParc], False);
        //
      end;
    end;
  end;  
(*
          for( $i = 2; $i <= GetDBModule()->Qrwcli->Parc_QtdMax; $i++ )
          {
            if( ($FmSyndicNet->CurrentUserLogin->SubPage1 == 'inad') or
            ( $ParcelaSel ==  $i) )
            {
              //$NewMeses = MediaMesesReparcelamento(($Amanha - 719528) * 86400, $MedMeses, $i);
              $NewMeses = MediaMesesReparcelamento2($Amanha, $MedMeses, $i);
              /* Parei Aqui */
              // Parei Aqui
              //Valor parcela = (media meses * Taxa mora mes * valor original) +
              //multa 2% * valor original + juros calculados at� o dia seguinte +
              //Valor original;
              //$TxaJur = 1;//GetDBModule()->QrVia2->TxaJur;

              if( DbModule_ReopenQrCond(
                $FmSyndicNet->CurrentUserLogin->UserCodCliEsp) <> True )
              {
                mensagem_alerta1( 'N�o foi poss�vel obter dados do condom�nio!' );
                exit;
              }
              $TxaJur = GetDBModule()->QrCond->PercJuros;
              $TxaMul = GetDBModule()->QrCond->PercMulta;
              //
              $VlrParc = (($NewMeses[0] * $TxaJur * $VlrOrigi / 100) +
              $VlrOrigi + $VlrMulta + $VlrJuros) / $i;
              if( $VlrParc > GetDBModule()->Qrwcli->Parc_ValMin )
              {
                $Dados = $FmSyndicNet->CurrentUserLogin->UserCodigoEnt . '#' .
                $FmSyndicNet->CurrentUserLogin->UserCodigoEsp . '#' .
                $DataIni . '#' .
                $DataFim . '#' .
                $i . '#'.
                number_format($VlrTotal, 2, '.', '') . '#';
                if( $FmSyndicNet->CurrentUserLogin->SubPage1 == 'inad')
                {
                  $MyLink = PWDExEncode($Dados, $FSecurityString, 0);
                  $FmSyndicNet->LB1->AddItem(/*$FmSyndicNet->LB1->Count . ' = ' . */$Dados);
                  $FmSyndicNet->LB1->AddItem(/*$FmSyndicNet->LB1->Count . ' = ' . */$MyLink);
                  $w += 1;
                  $wclsp = 'CollapsiblePanel' . $w;
                  print('
<div id="' . $wclsp . '" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0"><h4><img src="imgs/setas.gif"/> ' . $i . ' Parcelas</h4></div>
  <div class="CollapsiblePanelContent">
    <script type="text/javascript">
    <!--
    var ' . $wclsp . ' = new Spry.Widget.CollapsiblePanel("' . $wclsp . '");
    //-->
    </script>
    <table cellpadding="0" width="160" cellspacing="0" border="0" id="inadparc">
      <tr>
        <td height="20" width="160" colspan="2" id="inadparctit"><a href="' .
        'index.php?page=sit_user_acei&loguser=' . $loginid . '&parcsel=' . $MyLink  .
        '"><img border="0" src="imgs/seta.gif"/> Aceito o parcelamento</a></td>
      </tr>
      <tr>
        <td width="80" height="20" id="inadparcsubtit1">Vencimento</td>
        <td width="80" id="inadparcsubtit2">Valor</td>
      </tr>
                  ');

                } else {
                  print('
<div>
  <div>
    <table cellpadding="0" width="140" cellspacing="0" border="0" class="parc">
      <tr>
        <td colspan="2" class="parc_tit" height="20"><a>Parcelamento</a></td>
      </tr>
      <tr>
        <td width="100" height="20" class="parc_subtit">Total de parcelas</td>
        <td class="parc_text1">'. $ParcelaSel .'</td>
      </tr>
    </table>
    <table cellpadding="0" width="140" cellspacing="0" border="0" class="parc">
      <tr>
        <td width="90" height="20" class="parc_subtit">Vencimento</td>
        <td class="parc_subtit">Valor</td>
      </tr>
                  ');

                }

                for( $j=1; $j <= $i; $j++ )
                {
                  $FmSyndicNet->LBParc->AddItem(DBModule_FROM_DAYS($NewMeses[$j], 1) . '#' .
                      number_format($VlrParc, 2, '.', ''));
                  print('
    <tr>
      <td class="parc_text1">' . DBModule_FROM_DAYS($NewMeses[$j], 2)  . '</td>
      <td class="parc_text2">' . number_format($VlrParc, 2, ',', '.') . '</td>
    </tr>
                  ');
                }
                print('
    </table>
  </div>
</div>
                ');
              }
            }
            //print('Parcelas   : ' . $i . ' Valor parcela: '. $VlrParc / $i .
            //'  M�dia meses: ' . $NewMeses . '<br/>');
            print('
              </div>
            ');
          }
        }
      }

      // btEnviarClick
      if( $FmSyndicNet->CurrentUserLogin->Requerido8 == '190912' )
      {
        //$FmSyndicNet->CurrentUserLogin->Requerido8 = '';
        //redirect_query('page=sit_user_prep');
        //mensagem_alerta1('AGUARDE....');
        //parcelamento_parcelar();
      }

    }
  }
*)
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'XX. Reabrindo tabela tempor�ria "' +
      FReparcP2 + '"');
    //
    QrReparcP2.Close;
    QrReparcP2.Open;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReparc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReparc.CBCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_DELETE then
  begin
    EdCondImov.ValueVariant := 0;
    CBCondImov.KeyValue     := Null;
  end;
end;

procedure TFmReparc.CBProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      ReopenPropriet(True);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmReparc.EdCondImovChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  HabilitaBtPesq();
  FechaPesquisa();
end;

procedure TFmReparc.EdEmpresaChange(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    FTabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, DModG.QrEmpresasFilial.Value);
    PnPesq1.Visible := False;
    ReopenPropriet(False);
    ReopenCondImov();
    Reopenwcli();
    ReopenCond();
    //
    HabilitaBtPesq();
    FechaPesquisa();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReparc.EdPercJurosExit(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmReparc.EdPercMultaExit(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmReparc.EdProprietChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  ReopenCondImov;
  HabilitaBtPesq();
  FechaPesquisa();
end;

procedure TFmReparc.EdProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      ReopenPropriet(True);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmReparc.FechaPesquisa();
begin
  QrReparcI2.Close;
end;

procedure TFmReparc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmReparc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  TPDtaIni.Date := 0;
  TPDtaFim.Date := Date;
  //
  TPPriVct.Date := Date + 1;
  TPPriVct.MinDate := Date;
  //
  QrPropriet.Open;
  QrCondImov.Open;
  //
  QrTP.Database          := DModG.MyPID_DB;
  QrParcelasP.Database   := DModG.MyPID_DB;
  QrSumI.Database        := DModG.MyPID_DB;
  QrPesq1.Database       := DModG.MyPID_DB;
  QrPesq2.Database       := DModG.MyPID_DB;
  QrReparcI2.DataBase    := DModG.MyPID_DB;
  QrReparcP2.DataBase    := DModG.MyPID_DB;
  FReparcI2 := UCriar.RecriaTempTable('ReparcI2', DmodG.QrUpdPID1, False);
  FReparcP2 := UCriar.RecriaTempTable('ReparcP2', DmodG.QrUpdPID1, False);
end;

procedure TFmReparc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReparc.HabilitaBtPesq();
begin
  BtPesq.Enabled :=
    (EdEmpresa.ValueVariant <> 0) and
    (EdPropriet.ValueVariant <> 0) and
    (EdCondImov.ValueVariant <> 0);
end;

function TFmReparc.ObtemCNAB_CfgDeCond(CodCliEnt, CodCliEsp: Integer): Integer;
const
  Aviso  = '...';
  Titulo = 'Configura��o do boleto';
  Prompt = 'Informe a configura��o do boleto:';
  Campo  = 'Descricao';
var
  CNAB_Cfg: Variant;
begin
  Result := -1;
  //
  if MyObjects.FIC(CodCliEsp = 0, nil, 'Defina o condom�nio!') then Exit;
  if MyObjects.FIC(CodCliEnt = 0, nil, 'Defina o condom�nio!') then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT UsaCNAB_Cfg ',
    'FROM cond ',
    'WHERE Codigo=' + Geral.FF0(CodCliEsp),
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    if QrLoc.FieldByName('UsaCNAB_Cfg').AsInteger = 1 then //True
    begin
      CNAB_Cfg := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
        'SELECT Codigo, Nome ' + Campo,
        'FROM cnab_cfg ',
        'WHERE Ativo = 1 ',
        'AND Cedente=' + Geral.FF0(CodCliEnt),
        'ORDER BY ' + Campo,
        ''], DMod.MyDB, False);
      //
      if CNAB_Cfg <> Null then
        Result := CNAB_Cfg;
    end else
      Result := 0;
  end;
end;

{
procedure TFmReparc.PesquisaEm_users();
var
  Cond, Prop, Apto: Integer;
begin
  Cond := EdEmpresa.ValueVariant;
  Prop := EdPropriet.ValueVariant;
  Apto := EdCondImov.ValueVariant;
  //
  QrPesq1.Close;
  QrPesq1.SQL.Clear;
  QrPesq1.SQL.Add('SELECT ');
  QrPesq1.SQL.Add('IF(con.Tipo=0,con.RazaoSocial,con.Nome) NO_Condom,');
  QrPesq1.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_Pessoa,');
  QrPesq1.SQL.Add('cim.Unidade, CASE WHEN usr.Tipo=1 THEN "Morador" ');
  QrPesq1.SQL.Add('WHEN usr.Tipo=9 THEN "Admin" ELSE "TESTE" END NO_Tipo,');
  QrPesq1.SQL.Add('usr.*');
  QrPesq1.SQL.Add('FROM users usr');
  QrPesq1.SQL.Add('LEFT JOIN entidades con ON con.Codigo=usr.CodCliEnt');
  QrPesq1.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=usr.CodigoEnt');
  QrPesq1.SQL.Add('LEFT JOIN condimov cim ON cim.Conta=usr.CodigoEsp');
  QrPesq1.SQL.Add('WHERE usr.User_ID>0'); // n�o mostrar -1
  //
  if Cond <> 0 then
    QrPesq1.SQL.Add('AND usr.Tipo=' + FormatFloat('0', RGTipoUser.ItemIndex));
  if Cond <> 0 then
    QrPesq1.SQL.Add('AND usr.CodCliEsp=' + FormatFloat('0', Cond));
  if Prop <> 0 then
    QrPesq1.SQL.Add('AND usr.CodigoEnt=' + FormatFloat('0', Prop));
  if Apto <> 0 then
    QrPesq1.SQL.Add('AND usr.CodigoEsp=' + FormatFloat('0', Apto));
  //
  QrPesq1.Open;
end;
}
procedure TFmReparc.QrParcelasPAfterOpen(DataSet: TDataSet);
begin
  BtParcela.Enabled := QrParcelasP.RecordCount > 0;
end;

procedure TFmReparc.QrParcelasPBeforeClose(DataSet: TDataSet);
begin
  BtParcela.Enabled := False;
end;

procedure TFmReparc.QrReparcI2AfterOpen(DataSet: TDataSet);
begin
  PnPesq1.Visible := True;
end;

procedure TFmReparc.QrReparcI2BeforeClose(DataSet: TDataSet);
begin
  QrSumI.Close;
  QrReparcP2.Close;
end;

procedure TFmReparc.QrReparcP2AfterScroll(DataSet: TDataSet);
begin
  QrParcelasP.Close;
  QrParcelasP.Params[00].AsInteger := QrReparcP2TotParc.Value;
  QrParcelasP.Open;
end;

procedure TFmReparc.QrReparcP2BeforeClose(DataSet: TDataSet);
begin
  QrParcelasP.Close;
end;

procedure TFmReparc.QrReparcP2CalcFields(DataSet: TDataSet);
begin
  QrTP.Close;
  QrTP.Params[0].AsInteger := QrReparcP2TotParc.Value;
  QrTP.Open;
  QrReparcP2TotalVal.Value := QrTPValor.Value;
end;

procedure TFmReparc.ReopenCond();
begin
  QrCond.Close;
  QrCond.Params[0].AsInteger := EdEmpresa.ValueVariant;
  QrCond.Open;
  //
  EdPercMulta.ValueVariant := QrCondPercMulta.Value;
  EdPercJuros.ValueVariant := QrCondPercJuros.Value;
end;

procedure TFmReparc.ReopenCondImov();
var
  Cond, Propriet: Integer;
begin
  EdCondImov.Text := '';
  CBCondImov.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  Propriet := Geral.IMV(EdPropriet.Text);
  //Obrigar a preencher o condom�nio para n�o ficar lento
  if (Cond <> 0) or (Propriet <> 0) then
  begin
    QrCondImov.Close;
    QrCondImov.SQL.Clear;
    QrCondImov.SQL.Add('SELECT cim.Conta, cim.Unidade');
    QrCondImov.SQL.Add('FROM condimov cim');
    QrCondImov.SQL.Add('WHERE cim.Codigo<>0');
    QrCondImov.SQL.Add('AND cim.Propriet<>0');
    QrCondImov.SQL.Add('');
    if Cond <> 0 then
      QrCondImov.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
    if Propriet <> 0 then
      QrCondImov.SQL.Add('AND cim.Propriet=' + IntToStr(Propriet));
    QrCondImov.SQL.Add('');
    QrCondImov.SQL.Add('ORDER BY cim.Unidade');
    QrCondImov.Open;
  end else
    QrCondImov.Close;
end;

procedure TFmReparc.ReopenPropriet(Todos: Boolean);
var
  Cond: Integer;
begin
  EdPropriet.Text := '';
  CBPropriet.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  //
  if (not Todos) and (Cond = 0) then
  begin
    QrPropriet.Close;
    Exit;
  end;  
  QrPropriet.Close;
  QrPropriet.SQL.Clear;
  QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
  QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
  QrPropriet.SQL.Add('FROM entidades ent');
  QrPropriet.SQL.Add('LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet');
  QrPropriet.SQL.Add('WHERE ent.Cliente2="V"');
  QrPropriet.SQL.Add('');
  if (Cond <> 0) and not Todos then
    QrPropriet.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  QrPropriet.SQL.Add('');
  QrPropriet.SQL.Add('ORDER BY NOMEPROP');
  QrPropriet.Open;
end;

procedure TFmReparc.Reopenwcli();
begin
  Qrwcli.Close;
  Qrwcli.Params[0].AsInteger := EdEmpresa.ValueVariant;
  Qrwcli.Open;
end;

procedure TFmReparc.TPDtaFimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmReparc.TPDtaFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmReparc.TPDtaIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmReparc.TPDtaIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmReparc.TPPriVctChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmReparc.BtParcelaClick(Sender: TObject);
begin
  Parcelamento_parcelar();
end;

//function parcelamento_parcelar()
procedure TFmReparc.Parcelamento_parcelar();
(*
{
  global $User_parc;
  global $FmSyndicNet;
  $TxaJur = GetDBModule()->QrCond->PercJuros;
  $TxaMul = GetDBModule()->QrCond->PercMulta;
  GetDBModule()->Database1->Execute(
    'INSERT INTO bloqparc SET ' .
    ' loginID="'. $FmSyndicNet->CurrentUserLogin->Requerido0 . '", ' .
    ' autentica="'. $FmSyndicNet->CurrentUserLogin->Requerido9 . '", ' .
    ' CodCliEnt="'. $FmSyndicNet->CurrentUserLogin->UserCodCliEnt . '", ' .
    ' CodCliEsp="'. $FmSyndicNet->CurrentUserLogin->UserCodCliEsp . '", ' .
    ' CodigoEnt="'. $FmSyndicNet->CurrentUserLogin->UserCodigoEnt . '", ' .
    ' CodigoEsp="'. $FmSyndicNet->CurrentUserLogin->UserCodigoEsp . '", ' .
    ' TxaJur="'. GetDBModule()->QrCond->PercJuros . '", ' .
    ' TxaMul="'. GetDBModule()->QrCond->PercMulta . '", ' .
    ' DataP="'. FDT_Now() . '" '
  );
  $CODIGO = mysql_insert_id();
*)
const
  MaxFatNum = 1000000;
var
  DataP, loginID, autentica: WideString;
  VlrOrigi, VlrMulta, VlrJuros, VlrTotal, TxaJur, TxaMul: Double;
  CodCliEsp, CodCliEnt, CodigoEnt, CodigoEsp, Novo, Codigo: Integer;
  //
  //VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
  FatNum, ValBol, VlrDesco: Double;
  Vencimento: String;
  Parcela, Lancto, Controle, Sub, Reparcel, CNAB_Cfg: Integer;
begin
  if Geral.MensagemBox('Confirma o reparcelamento dos d�bitos desta pesquisa?' +
  sLineBreak + 'Valor a reparcelar:  ' + FormatFloat('#,###,###,##0.00', QrSumIValBol.Value) +
  sLineBreak + 'Total reparcelado:  ' + FormatFloat('#,###,###,##0.00', QrReparcP2TotalVal.Value) +
  sLineBreak + 'Total de parcelas:  ' + FormatFloat('0', QrReparcP2TotParc.Value) +
  sLineBreak + 'Valor da parcela:  ' + FormatFloat('#,###,###,##0.00', QrParcelasPValor.Value),
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
  //
  //if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  CodCliEnt := DModG.QrEmpresasCodigo.Value;
  CodCliEsp := EdEmpresa.ValueVariant;
  CNAB_Cfg  := ObtemCNAB_CfgDeCond(CodCliEnt, CodCliEsp);
  //
  if MyObjects.FIC(CNAB_Cfg = -1, nil, 'Configura��o do boleto n�o definida!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    loginID   := '';
    autentica := '';
    CodigoEsp := EdCondImov.ValueVariant;
    CodCliEnt := DModG.QrEmpresasCodigo.Value; //QrEntiCondCodEnti.Value;
    CodigoEnt := EdPropriet.ValueVariant;
    CodigoEsp := EdCondImov.ValueVariant;
    VlrOrigi  := QrSumIValBol.Value;
    VlrTotal  := QrReparcP2TotalVal.Value;
    VlrMulta  := QrSumIValMul.Value;
    VlrJuros  := VlrTotal - VlrOrigi - VlrMulta;
    DataP     := Geral.FDT(Now(), 9);
    Novo      := 1;
    TxaJur    := EdPercJuros.ValueVariant;
    TxaMul    := EdPercMulta.ValueVariant;
    //
    if not DModG.BuscaProximoCodigoInt_Negativo('bloqparc', 'Codigo', '', 0, '',
    Codigo) then Exit;

    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'bloqparc', False, [
    'loginID', 'autentica', 'CodCliEsp',
    'CodCliEnt', 'CodigoEnt', 'CodigoEsp',
    'VlrOrigi', 'VlrMulta', 'VlrJuros',
    'VlrTotal', 'DataP', 'Novo',
    'TxaJur', 'TxaMul'],
    ['Codigo'],
    [loginID, autentica, CodCliEsp,
    CodCliEnt, CodigoEnt, CodigoEsp,
    VlrOrigi, VlrMulta, VlrJuros,
    VlrTotal, DataP, Novo,
    TxaJur, TxaMul],
    [Codigo], True) then Exit;

(*
  $j = $FmSyndicNet->LBParc->Count;
  for( $i = 0; $i < $j; $i++)
  {
    $ParcelaNum = '"'. ($i + 1) . '"';
    $TamLinParc = strlen($FmSyndicNet->LBParc->Items[$i]);
    if( $TamLinParc > 11 )
    {
      $VENCTO = substr($FmSyndicNet->LBParc->Items[$i], 0, 10);
      $VALOR  = substr($FmSyndicNet->LBParc->Items[$i], 11, $TamLinParc-10);
      GetDBModule()->Database1->Execute(
        'INSERT INTO bloqparcpar SET Codigo = "' . $CODIGO . '", ' .
        ' Parcela=' . $ParcelaNum . ', ' .
        ' ValBol="' . $VALOR . '", ' .
        ' Vencimento="' . $VENCTO  . '"'
      );
      $CONTROLE = mysql_insert_id();
      $SEQUENCIAL = 990000 + fmod($CONTROLE, 10000);
      GetDBModule()->Database1->Execute(
        'UPDATE bloqparcpar SET FatNum = "' . $SEQUENCIAL . '" ' .
        ' WHERE Controle="' . $CONTROLE . '"'
      );
    }
  }
*)
    QrParcelasP.Last;
    while not QrParcelasP.Bof do
    begin
      Parcela    := QrParcelasPNumParc.Value;
      VlrOrigi   := 0;
      VlrMulta   := 0;
      VlrJuros   := 0;
      ValBol     := QrParcelasPValor.Value;
      Vencimento := Geral.FDT(QrParcelasPVencto.Value, 1);
      Lancto     := 0;
      VlrTotal   := 0;
      VlrDesco   := 0;
      //
      if not DModG.BuscaProximoCodigoInt_Negativo('bloqparcpar', 'Controle', '', 0, '',
        Controle) then Exit;
      FatNum     := MaxFatNum + Controle;
      while FatNum < 0 do
        FatNum := FatNum + MaxFatNum;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'bloqparcpar', False, [
      'Codigo', 'Parcela', 'FatNum',
      'VlrOrigi', 'VlrMulta', 'VlrJuros',
      'ValBol', 'Vencimento', 'Lancto',
      'VlrTotal', 'VlrDesco'], [
      'Controle'], [
      Codigo, Parcela, FatNum,
      VlrOrigi, VlrMulta, VlrJuros,
      ValBol, Vencimento, Lancto,
      VlrTotal, VlrDesco], [
      Controle], True) then
        QrParcelasP.Prior;
    end;
(*
  $j = GetDBModule()->QrVia2->RecordCount;
  GetDBModule()->QrVia2->First();
  for( $i = 0; $i < $j; $i++)
  {
    GetDBModule()->Database1->Execute(
      ' UPDATE lct SET Reparcel = "' . $CODIGO . '" ' .
      ' WHERE Cliente="' . GetDBModule()->QrVia2->Cliente . '"' .
      ' AND Depto="' . GetDBModule()->QrVia2->Depto . '"' .
      ' AND Mez="' . GetDBModule()->QrVia2->Mez . '"' .
      ' AND FatNum="' . GetDBModule()->QrVia2->FatNum . '"' .
      ' AND Reparcel="0"'
    );
    GetDBModule()->QrVia2->Next();
  }
*)
    QrLct.First;
    while not QrLct.Eof do
    begin
      Reparcel := Codigo;
      Controle := QrLctControle.Value;
      Sub      := QrLctSub.Value;
      //
{
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FTabLctA, False, [
      'Reparcel'], ['Controle'], [Reparcel], [Controle], True) then
      ' UPDATE lct SET Reparcel = "' . $CODIGO . '" ' .
      ' WHERE Cliente="' . GetDBModule()->QrVia2->Cliente . '"' .
      ' AND Depto="' . GetDBModule()->QrVia2->Depto . '"' .
      ' AND Mez="' . GetDBModule()->QrVia2->Mez . '"' .
      ' AND FatNum="' . GetDBModule()->QrVia2->FatNum . '"' .
      ' AND Reparcel="0"'
      Cliente := EdPropriet.ValueVariant;
      Depto   := EdCondImov.ValueVariant;
      Mez     := QrLctMez.Value;
      FatNum  := QrLctFatNum.Value;
}
      if UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Reparcel'], ['Controle', 'Sub'],
      [Reparcel], [Controle, Sub], True, '', FTablctA) then
      QrLct.Next;
    end;
    //
    QrLct.Close;
    //
    FmPrincipal.ReparcelamentosdeBloquetos_Local(Codigo);
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
