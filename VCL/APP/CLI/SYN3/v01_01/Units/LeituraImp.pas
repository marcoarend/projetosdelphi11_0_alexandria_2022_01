unit LeituraImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, DB, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnMyObjects, Grids, DBGrids,
  dmkDBGrid, frxClass, frxDBSet, ComCtrls, dmkGeral, dmkImage, UnDmkEnums;

type
  TMaxQ = 00..11;
  TFmLeituraImp = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrLis: TmySQLQuery;
    QrLisCodigo: TIntegerField;
    QrLisNome: TWideStringField;
    QrLisPreco: TFloatField;
    QrLisCasas: TSmallintField;
    QrLisUnidLei: TWideStringField;
    QrLisUnidImp: TWideStringField;
    QrLisUnidFat: TFloatField;
    QrCons: TmySQLQuery;
    DsCons: TDataSource;
    QrConsCodigo: TIntegerField;
    QrConsNome: TWideStringField;
    QrConsPreco: TFloatField;
    QrConsCasas: TSmallintField;
    QrConsUnidImp: TWideStringField;
    QrConsUnidLei: TWideStringField;
    QrConsUnidFat: TFloatField;
    QrConsAtivo: TSmallintField;
    Panel4: TPanel;
    DBGrid1: TdmkDBGrid;
    Panel5: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QrUnidCond: TmySQLQuery;
    QrUnidCondApto: TIntegerField;
    QrUnidCondUnidade: TWideStringField;
    QrUnidCondProprie: TWideStringField;
    QrUnidCondSelecio: TSmallintField;
    QrUnidCondEntidad: TIntegerField;
    DsUnidCond: TDataSource;
    DBGrid2: TdmkDBGrid;
    QrConsUHs: TmySQLQuery;
    DsConsUHs: TDataSource;
    QrConsUHsCodigo: TIntegerField;
    QrConsUHsNO_Bloco: TWideStringField;
    QrConsUHsUnidade: TWideStringField;
    QrConsUHsPeriodo: TIntegerField;
    QrConsUHsMedAnt: TFloatField;
    QrConsUHsMedAtu: TFloatField;
    QrConsUHsCarencia: TFloatField;
    QrConsUHsConsumo: TFloatField;
    QrConsUHsPreco: TFloatField;
    QrConsUHsValor: TFloatField;
    QrConsUHsBoleto: TFloatField;
    QrConsUHsControle: TIntegerField;
    QrConsUHsLancto: TIntegerField;
    QrConsUHsPERIODO_TXT: TWideStringField;
    Splitter1: TSplitter;
    QrConsUHsNO_Leitura: TWideStringField;
    QrConsUHsMedAnt_TXT: TWideStringField;
    QrConsUHsMedAtu_TXT: TWideStringField;
    QrConsUHsCasas: TSmallintField;
    QrConsUHsCarencia_TXT: TWideStringField;
    QrConsUHsConsumo_TXT: TWideStringField;
    frxCUHs: TfrxReport;
    frxDsCUHs: TfrxDBDataset;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid3: TDBGrid;
    Panel7: TPanel;
    Label2: TLabel;
    EdMesesA: TdmkEdit;
    QrConsUHsCONSUMO2: TFloatField;
    QrConsUHsCONSUMO2_TXT: TWideStringField;
    QrConsUHsUnidLei: TWideStringField;
    QrConsUHsUnidImp: TWideStringField;
    QrConsUHsUnidFat: TFloatField;
    QrConsUHsApto: TIntegerField;
    QrConsUHsOrdem: TIntegerField;
    QrConsUHsAndar: TIntegerField;
    QrConsUHsPropriet: TIntegerField;
    QrConsUHsNO_PROPRIET: TWideStringField;
    QrUnidCon2: TmySQLQuery;
    TabSheet2: TTabSheet;
    DsUnidCon2: TDataSource;
    frxUC2: TfrxReport;
    frxDsUC2: TfrxDBDataset;
    QrUnidCon2BlocOrd: TIntegerField;
    QrUnidCon2Andar: TIntegerField;
    QrUnidCon2Apto: TIntegerField;
    QrUnidCon2Entidad: TIntegerField;
    QrUnidCon2BlocNom: TWideStringField;
    QrUnidCon2Unidade: TWideStringField;
    QrUnidCon2Proprie: TWideStringField;
    QrUnidCon2Quant01: TFloatField;
    QrUnidCon2Quant02: TFloatField;
    QrUnidCon2Quant03: TFloatField;
    QrUnidCon2Quant04: TFloatField;
    QrUnidCon2Quant05: TFloatField;
    QrUnidCon2Quant06: TFloatField;
    QrUnidCon2Quant07: TFloatField;
    QrUnidCon2Quant08: TFloatField;
    QrUnidCon2Quant09: TFloatField;
    QrUnidCon2Quant10: TFloatField;
    QrUnidCon2Quant11: TFloatField;
    QrUnidCon2Quant12: TFloatField;
    QrUnidCon2Ativo: TSmallintField;
    QrUnidCon2LeitCod: TIntegerField;
    QrUnidCon2LeitNom: TWideStringField;
    QrUnidCon2UnidImp: TWideStringField;
    QrUnidCon2Casas: TSmallintField;
    QrUnidCon2Q01_TXT: TWideStringField;
    QrUnidCon2Q02_TXT: TWideStringField;
    QrUnidCon2Q03_TXT: TWideStringField;
    QrUnidCon2Q04_TXT: TWideStringField;
    QrUnidCon2Q05_TXT: TWideStringField;
    QrUnidCon2Q06_TXT: TWideStringField;
    QrUnidCon2Q07_TXT: TWideStringField;
    QrUnidCon2Q08_TXT: TWideStringField;
    QrUnidCon2Q09_TXT: TWideStringField;
    QrUnidCon2Q10_TXT: TWideStringField;
    QrUnidCon2Q11_TXT: TWideStringField;
    QrUnidCon2Q12_TXT: TWideStringField;
    CBMes: TComboBox;
    CBAno: TComboBox;
    LaMes: TLabel;
    DBGrid4: TDBGrid;
    frxReport1: TfrxReport;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdEmpresaEnter(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrConsUHsCalcFields(DataSet: TDataSet);
    procedure frxCUHsGetValue(const VarName: string; var Value: Variant);
    procedure EdMesesAChange(Sender: TObject);
    procedure CBMesChange(Sender: TObject);
    procedure CBAnoChange(Sender: TObject);
    procedure QrUnidCon2CalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FCond: Integer;
    FCons: String;
    FQtde: array[TMaxQ] of Double;
    procedure ReopenUnidCond(Apto: Integer);
    procedure ReopenCons(Codigo: Integer);
    procedure ReCriaCons();
    //procedure ReopenConsIts();
    procedure AtualizaTodosCons(Ativo: Integer);
    procedure AtualizaTodosAptos(Ativo: Integer);

  public
    { Public declarations }
    //FPaginar: Boolean;
    FPeriodoIni,
    FPeriodoFim,
    FClienteInicial,
    FPeriodoInicial: Integer;
    FConfiguInicial: Boolean;
  end;

  var
  FmLeituraImp: TFmLeituraImp;

implementation

uses CondSel, ModuleGeral, Module, UCreate, UMySQLModule, ModuleCond, MyVCLSkin,
UnInternalConsts;

{$R *.DFM}

procedure TFmLeituraImp.AtualizaTodosAptos(Ativo: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE UnidCond SET Selecio=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Ativo;
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenUnidCond(QrUnidCondApto.Value);
end;

procedure TFmLeituraImp.AtualizaTodosCons(Ativo: Integer);
begin
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE cons SET Ativo=:P0');
  DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
  DmodG.QrUpdPID1.ExecSQL;
  //
  ReopenCons(QrConsCodigo.Value);
end;

procedure TFmLeituraImp.BitBtn1Click(Sender: TObject);
begin
  AtualizaTodosAptos(0);
end;

procedure TFmLeituraImp.BitBtn2Click(Sender: TObject);
begin
  AtualizaTodosAptos(1);
end;

procedure TFmLeituraImp.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodosCons(0);
end;

procedure TFmLeituraImp.BtImprimeClick(Sender: TObject);
  procedure ReopenConsUHs(Cond: Integer);
  begin
    QrConsUHs.Close;
    QrConsUHs.SQL.Clear;
    QrConsUHs.SQL.Add('SELECT cns.Nome NO_Leitura, cni.Codigo, cdb.Ordem,');
    QrConsUHs.SQL.Add('cdb.Descri NO_Bloco, cdi.Andar, cdi.Propriet,');
    QrConsUHs.SQL.Add('cdi.Unidade, cni.Periodo, cni.MedAnt,');
    QrConsUHs.SQL.Add('cni.MedAtu, cni.Carencia, cni.Consumo, cni.Preco,');
    QrConsUHs.SQL.Add('cni.Valor, cni.Boleto, cni.Controle, cni.Lancto,');
    QrConsUHs.SQL.Add('cni.Casas, cni.UnidLei, cni.UnidImp, cni.UnidFat,');
    QrConsUHs.SQL.Add('cni.Apto, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)');
    QrConsUHs.SQL.Add('NO_PROPRIET');
    QrConsUHs.SQL.Add('FROM cons cns');
    QrConsUHs.SQL.Add('LEFT JOIN consits  cni ON cni.Codigo=cns.Codigo');
    QrConsUHs.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=cni.Apto');
    QrConsUHs.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
    QrConsUHs.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet');
    QrConsUHs.SQL.Add('');
    QrConsUHs.SQL.Add('WHERE cni.Cond=:P0');
    QrConsUHs.SQL.Add('AND cni.Periodo BETWEEN :P1 AND :P2');
    QrConsUHs.SQL.Add('AND cni.Codigo in (');
    QrConsUHs.SQL.Add('  SELECT cs.Codigo');
    QrConsUHs.SQL.Add('  FROM ' + VAR_MyPID_DB_NOME + '.cons cs');
    QrConsUHs.SQL.Add('  WHERE cs.Ativo=1');
    QrConsUHs.SQL.Add(')');
    QrConsUHs.SQL.Add('AND cdi.Conta IN (');
    QrConsUHs.SQL.Add('  SELECT uc.Apto');
    QrConsUHs.SQL.Add('  FROM ' + VAR_MyPID_DB_NOME + '.UnidCond uc');
    QrConsUHs.SQL.Add('  WHERE uc.Selecio=1');
    QrConsUHs.SQL.Add(')');
    QrConsUHs.SQL.Add('ORDER BY NO_Leitura, cdb.Ordem, ');
    QrConsUHs.SQL.Add('cdi.Unidade, cni.Periodo');
    //
    QrConsUHs.Params[00].AsInteger := Cond;
    QrConsUHs.Params[01].AsInteger := FPeriodoIni;
    QrConsUHs.Params[02].AsInteger := FPeriodoFim;
    //
    UMyMod.AbreQuery(QrConsUHs, Dmod.MyDB, 'TFmLeituraImp.BtOKClick()');
  end;
  //
  procedure InserirItem(LeitCod, BlocOrd, Andar, Apto, Entidad, Casas: Integer;
  LeitNom, BlocNom, Unidade, Proprie, UnidImp: String);
  var
    I: Integer;
  begin
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'unidcon2', True, [
    'LeitCod', 'LeitNom', 'UnidImp',
    'BlocOrd', 'Andar', 'Apto',
    'Entidad', 'BlocNom', 'Unidade',
    'Proprie', 'Quant01', 'Quant02',
    'Quant03', 'Quant04', 'Quant05',
    'Quant06', 'Quant07', 'Quant08',
    'Quant09', 'Quant10', 'Quant11',
    'Quant12', 'Casas'], [
    ], [
    LeitCod, LeitNom, UnidImp,
    BlocOrd, Andar, Apto,
    Entidad, BlocNom, Unidade,
    Proprie, FQtde[00], FQtde[01],
    FQtde[02], FQtde[03], FQtde[04],
    FQtde[05], FQtde[06], FQtde[07],
    FQtde[08], FQtde[09], FQtde[10],
    FQtde[11], Casas], [
    ], False);
    for I := 0 to High(TMaxQ) do
      FQtde[I] := 0;
  end;
var
  LeitNom, BlocNom, Unidade, Proprie, UnidImp: String;
  LeitCod, BlocOrd, Andar, Apto, Entidad, Cond, PeriX, I, Casas: Integer;
begin
  Cond        := EdEmpresa.ValueVariant;
  FPeriodoFim := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
  case PageControl1.ActivePageIndex of
    0:
    begin
      FPeriodoIni := FPeriodoFim - EdMesesA.ValueVariant - 1;
      ReopenConsUHs(Cond);
      //
      MyObjects.frxMostra(frxCUHs, 'Consumos de UHs - Anal�tico');
    end;
    1:
    begin
      UCriar.RecriaTempTable('UnidCon2', DmodG.QrUpdPID1, False);
      FPeriodoIni := FPeriodoFim - 11;
      ReopenConsUHs(Cond);
      //
      Casas    := 0;
      LeitCod  := 0;
      BlocOrd  := 0;
      Andar    := 0;
      Apto     := 0;
      Entidad  := 0;
      BlocNom  := '';
      Unidade  := '';
      Proprie  := '';
      for I := 0 to High(TMaxQ) do
        FQtde[I] := 0;
      QrConsUHs.First;
      while not QrConsUHs.Eof do
      begin
        if Apto <> QrConsUHsApto.Value then
        begin
          if Apto <> 0 then
            InserirItem(LeitCod, BlocOrd, Andar, Apto, Entidad, Casas,
              LeitNom, BlocNom, Unidade, Proprie, UnidImp);
          LeitCod  := QrConsUHsCodigo.Value;
          BlocOrd  := QrConsUHsOrdem.Value;
          Andar    := QrConsUHsAndar.Value;
          Apto     := QrConsUHsApto.Value;
          LeitNom  := QrConsUHsNO_Leitura.Value;
          Entidad  := QrConsUHsPropriet.Value;
          BlocNom  := QrConsUHsNO_Bloco.Value;
          Unidade  := QrConsUHsUnidade.Value;
          Proprie  := QrConsUHsNO_PROPRIET.Value;
          UnidImp  := QrConsUHsUnidImp.Value;
          Casas    := QrConsUHsCasas.Value;
        end;
        //
        PeriX := QrConsUHsPeriodo.Value - FPeriodoIni + 1;
        FQtde[PeriX-1] := QrConsUHsCONSUMO2.Value;
        //
        if QrConsUHs.RecordCount = QrConsUHs.RecNo then
          InserirItem(LeitCod, BlocOrd, Andar, Apto, Entidad, Casas,
            LeitNom, BlocNom, Unidade, Proprie, UnidImp);
        //
        QrConsUHs.Next;
      end;
      //
      UMyMod.AbreQuery(QrUnidCon2, DModG.MyPID_DB, 'TFmLeituraImp.BtImprimeClick()');
      //
      MyObjects.frxMostra(frxUC2, 'Consumos de UHs - Sint�tico');
    end;
  end;
end;

procedure TFmLeituraImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLeituraImp.BtTodosClick(Sender: TObject);
begin
  AtualizaTodosCons(1);
end;

procedure TFmLeituraImp.CBAnoChange(Sender: TObject);
begin
  ReCriaCons();
end;

procedure TFmLeituraImp.CBMesChange(Sender: TObject);
begin
  ReCriaCons();
end;

procedure TFmLeituraImp.DBGrid1CellClick(Column: TColumn);
var
  Ativo, Cons: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Ativo := QrConsAtivo.Value;
    if Ativo = 0 then Ativo := 1 else Ativo := 0;
    Cons := QrConsCodigo.Value;
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE cons SET Ativo=:P0');
    DmodG.QrUpdPID1.SQL.Add('WHERE Codigo=:P1');
    DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
    DmodG.QrUpdPID1.Params[01].AsInteger := Cons;
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenCons(Cons);
  end;
end;

procedure TFmLeituraImp.DBGrid2CellClick(Column: TColumn);
var
  Status, Apto: Integer;
begin
  if Column.FieldName = 'Selecio' then
  begin
    Status := QrUnidCondSelecio.Value;
    if Status = 0 then Status := 1 else Status := 0;
    Apto := QrUnidCondApto.Value;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE UnidCond SET Selecio=:P0');
    DModG.QrUpdPID1.SQL.Add('WHERE Apto=:P1');
    DModG.QrUpdPID1.Params[00].AsInteger := Status;
    DModG.QrUpdPID1.Params[01].AsInteger := Apto;
    DModG.QrUpdPID1.ExecSQL;
    //
    ReopenUnidCond(Apto);
  end;
end;

procedure TFmLeituraImp.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  //if Column.FieldName = 'Selecio' then
    //MeuVCLSkin.DrawGrid(DBGrid2, Rect, 1, QrUnidCondSelecio.Value);
end;

procedure TFmLeituraImp.EdEmpresaChange(Sender: TObject);
begin
  if not EdEmpresa.Focused then
  begin
    FCond := EdEmpresa.ValueVariant;
    ReCriaCons();
  end;
end;

procedure TFmLeituraImp.EdEmpresaEnter(Sender: TObject);
begin
  FCond := EdEmpresa.ValueVariant;
end;

procedure TFmLeituraImp.EdEmpresaExit(Sender: TObject);
begin
  if FCond <> EdEmpresa.ValueVariant then
  begin
    FCond := EdEmpresa.ValueVariant;
    ReCriaCons();
  end;
end;

procedure TFmLeituraImp.EdMesesAChange(Sender: TObject);
begin
  ReCriaCons();
end;

procedure TFmLeituraImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLeituraImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FClienteInicial := 0;
  FPeriodoInicial := -1000;
  FConfiguInicial := False;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  QrCons.Database     := DModG.MyPID_DB;
  QrUnidCon2.Database := DModG.MyPID_DB;
end;

procedure TFmLeituraImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLeituraImp.frxCUHsGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'PERIODO_TXT') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoIni, 1, False), 14) +
    ' at� ' + Geral.FDT(Geral.PeriodoToDate(FPeriodoFim, 1, False), 14)
  {
  else
  if AnsiCompareText(VarName, 'VARF_GRANDEZA1') = 0 then
    Value := ? DCond.QrConsUnidLei.Value
  else
  if AnsiCompareText(VarName, 'VARF_GRANDEZA2') = 0 then
    Value := ? DCond.QrConsUnidImp.Value
  else
  if AnsiCompareText(VarName, 'VARF_GRANDEZA3') = 0 then
    Value := ? DCond.QrConsUnidImp.Value
  }
  else
  if AnsiCompareText(VarName, 'MES_01') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 11, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_02') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 10, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_03') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 09, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_04') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 08, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_05') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 07, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_06') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 06, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_07') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 05, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_08') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 04, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_09') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 03, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_10') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 02, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_11') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 01, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'MES_12') = 0 then
    Value := Geral.FDT(Geral.PeriodoToDate(FPeriodoFim - 00, 1, False), 14)
  else
  if AnsiCompareText(VarName, 'VAR_NOME_CLI') = 0 then
    Value := CBEmpresa.Text
end;

procedure TFmLeituraImp.QrConsUHsCalcFields(DataSet: TDataSet);
begin
  QrConsUHsPERIODO_TXT.Value := Geral.FDT(
    Geral.PeriodoToDate(QrConsUHsPeriodo.Value, 1, False), 14);
  QrConsUHsMedAnt_TXT.Value := Geral.FFT_Null(
    QrConsUHsMedAnt.Value, QrConsUHsCasas.Value, siNegativo);
  QrConsUHsMedAtu_TXT.Value := Geral.FFT_Null(
    QrConsUHsMedAtu.Value, QrConsUHsCasas.Value, siNegativo);
  QrConsUHsCarencia_TXT.Value := Geral.FFT_Null(
    QrConsUHsCarencia.Value, QrConsUHsCasas.Value, siNegativo);
  QrConsUHsConsumo_TXT.Value := Geral.FFT_Null(
    QrConsUHsConsumo.Value, QrConsUHsCasas.Value, siNegativo);
  QrConsUHsCONSUMO2.Value := QrConsUHsConsumo.Value * QrConsUHsUnidFat.Value;
  QrConsUHsCONSUMO2_TXT.Value := Geral.FFT_Null(
    QrConsUHsCONSUMO2.Value, QrConsUHsCasas.Value, siNegativo);
end;

procedure TFmLeituraImp.QrUnidCon2CalcFields(DataSet: TDataSet);
  function FF(D: Double; C: Integer): String;
  begin
    Result := Geral.FFT(D, C, siNegativo);
  end;
var
  C: Integer;
begin
  C := QrUnidCon2Casas.Value;
  QrUnidCon2Q01_TXT.Value := FF(QrUnidCon2Quant01.Value, C);
  QrUnidCon2Q02_TXT.Value := FF(QrUnidCon2Quant02.Value, C);
  QrUnidCon2Q03_TXT.Value := FF(QrUnidCon2Quant03.Value, C);
  QrUnidCon2Q04_TXT.Value := FF(QrUnidCon2Quant04.Value, C);
  QrUnidCon2Q05_TXT.Value := FF(QrUnidCon2Quant05.Value, C);
  QrUnidCon2Q06_TXT.Value := FF(QrUnidCon2Quant06.Value, C);
  QrUnidCon2Q07_TXT.Value := FF(QrUnidCon2Quant07.Value, C);
  QrUnidCon2Q08_TXT.Value := FF(QrUnidCon2Quant08.Value, C);
  QrUnidCon2Q09_TXT.Value := FF(QrUnidCon2Quant09.Value, C);
  QrUnidCon2Q10_TXT.Value := FF(QrUnidCon2Quant10.Value, C);
  QrUnidCon2Q11_TXT.Value := FF(QrUnidCon2Quant11.Value, C);
  QrUnidCon2Q12_TXT.Value := FF(QrUnidCon2Quant12.Value, C);
end;

procedure TFmLeituraImp.ReCriaCons();
var
  DB: String;
begin
  FCons := UCriar.RecriaTempTable('Cons', DmodG.QrUpdPID1, False);
  {
  FPeriodoFim := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
  FPeriodoIni := FPeriodoFim - EdMesesA.ValueVariant -1;
  }
  DB := Dmod.MyDB.DatabaseName;
  //
  DModG.QrUpdPID1.Close;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FCons);
  DModG.QrUpdPID1.SQL.Add('SELECT DISTINCT cns.Codigo, cns.Nome, cnp.Preco,');
  DModG.QrUpdPID1.SQL.Add('cnp.Casas, cnp.UnidLei, cnp.UnidImp, cnp.UnidFat, ');
  DModG.QrUpdPID1.SQL.Add('1 Ativo');
  DModG.QrUpdPID1.SQL.Add('FROM ' + DB + '.consprc cnp');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + DB + '.cons    cns ON cns.Codigo=cnp.Codigo');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + DB + '.consits cni ON cni.Codigo=cns.Codigo');
  DModG.QrUpdPID1.SQL.Add('WHERE cni.Cond=:P0');
  DModG.QrUpdPID1.SQL.Add('AND cnp.Cond=:P1');
  //DModG.QrUpdPID1.SQL.Add('AND cni.Periodo BETWEEN :P2 AND :P3');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.Params[00].AsInteger := FCond;
  DModG.QrUpdPID1.Params[01].AsInteger := FCond;
  {
  DModG.QrUpdPID1.Params[02].AsInteger := FPeriodoIni;
  DModG.QrUpdPID1.Params[03].AsInteger := FPeriodoFim;
  }
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  ReopenCons(0);
  //
  UCriar.RecriaTempTable('UnidCond', DmodG.QrUpdPID1, False);

  DmCond.QrAptos.Close;
  DmCond.QrAptos.SQL.Clear;
  // De todos  e selecionados
  DmCond.QrAptos.SQL.Add('SELECT cdi.Conta, cdi.Unidade, cdi.Propriet, CASE WHEN ');
  DmCond.QrAptos.SQL.Add('ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEPROPRIET, ');
  DmCond.QrAptos.SQL.Add('FracaoIdeal, Moradores');
  DmCond.QrAptos.SQL.Add('FROM condimov cdi');
  DmCond.QrAptos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet');
  DmCond.QrAptos.SQL.Add('WHERE cdi.Codigo=:P0');
  DmCond.QrAptos.Params[0].AsInteger := FCond;
  //
  UMyMod.AbreQuery(DmCond.QrAptos, Dmod.MyDB);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO unidcond SET ');
  DModG.QrUpdPID1.SQL.Add('Apto=:P0, Unidade=:P1, Proprie=:P2, Selecio=:P3, ');
  DModG.QrUpdPID1.SQL.Add('Entidad=:P4');
  while not DmCond.QrAptos.Eof do
  begin
    DModG.QrUpdPID1.Params[00].AsInteger := DmCond.QrAptosConta.Value;
    DModG.QrUpdPID1.Params[01].AsString  := DmCond.QrAptosUnidade.Value;
    DModG.QrUpdPID1.Params[02].AsString  := DmCond.QrAptosNOMEPROPRIET.Value;
    DModG.QrUpdPID1.Params[03].AsInteger := 1;
    DModG.QrUpdPID1.Params[04].AsInteger := DmCond.QrAptosPropriet.Value;
    DModG.QrUpdPID1.ExecSQL;
    //
    DmCond.QrAptos.Next;
  end;
  //
  ReopenUnidCond(0);
  //
  BtImprime.Enabled := True;
end;

procedure TFmLeituraImp.ReopenCons(Codigo: Integer);
begin
  QrCons.Close;
  UmyMod.AbreQuery(QrCons, DModG.MyPID_DB, 'TFmLeituraImp.ReopenCons()');
  //
  QrCons.Locate('Codigo', Codigo, []);
end;

{
procedure TFmLeituraImp.ReopenConsIts;
begin
  (*
  QrConsIts.Close;
  QrConsIts.Params[00].AsInteger := FCond;
  QrConsIts.Params[01].AsInteger := FCond;
  QrConsIts.Params[02].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  QrConsIts.Params[03].AsInteger := QrConsCodigo.Value;
  QrConsIts.Open;
  *)
  //
  //QrConsIts.Locate('Controle', Controle, []);
end;
}

procedure TFmLeituraImp.ReopenUnidCond(Apto: Integer);
begin
  QrUnidCond.Close;
  QrUnidCond.Database := DModG.MyPID_DB;
  QrUnidCond.Open;
  QrUnidCond.Locate('Apto', Apto, []);
end;

procedure TFmLeituraImp.SpeedButton1Click(Sender: TObject);
  procedure DefineCond(Cond: Integer);
  begin
    EdEmpresa.ValueVariant := Cond;
    CBEmpresa.KeyValue     := Cond;
  end;
begin
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmCondSel, FmCondSel);
    case DModG.QrEmpresas.RecordCount of
      0: Geral.MensagemBox('N�o h� condom�nio cadastrado ou liberado!', 'Aviso',
         MB_OK+MB_ICONWARNING);
      1:
      begin
        DefineCond(DmCond.QrCondCodigo.Value);
      end else begin
        Screen.Cursor := crDefault;
        FmCondSel.ShowModal;
        DefineCond(FmCondSel.FCond);
      end;
    end;
    FmCondSel.Destroy;
    Application.ProcessMessages;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
