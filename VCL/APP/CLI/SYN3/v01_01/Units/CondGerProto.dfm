object FmCondGerProto: TFmCondGerProto
  Left = 383
  Top = 180
  Caption = 'GER-CONDM-010 :: Protocolos de Entrega de Bloqueto'
  ClientHeight = 541
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 379
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1008
      Height = 379
      ActivePage = TabSheet1
      Align = alClient
      TabHeight = 25
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'UHs cadastrados para entrega do documento'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 100
          Width = 1000
          Height = 10
          Cursor = crVSplit
          Align = alTop
          ExplicitLeft = -2
          ExplicitTop = 94
        end
        object DBG_CD1: TDBGrid
          Left = 0
          Top = 110
          Width = 1000
          Height = 234
          Align = alClient
          DataSource = DsCD1
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Boleto'
              Title.Caption = 'Bloqueto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'U.H.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROPRIET'
              Title.Caption = 'Propriet'#225'rio'
              Width = 215
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PROTOCOLO'
              Title.Caption = 'Protocolo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TAREFA'
              Title.Caption = 'Tarefa'
              Width = 388
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DELIVER'
              Title.Caption = 'Respons'#225'vel pela tarefa'
              Width = 121
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOTE'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALOR_PROT'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VENCTO_PROT'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MULTAVAL'
              Title.Caption = 'Multa'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MORADIAVAL'
              Title.Caption = 'Mora / Dia'
              Width = 56
              Visible = True
            end>
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 100
          Align = alTop
          DataSource = DsProt1
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 888
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'UHs cadastrados para entrega por e-mail'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter2: TSplitter
          Left = 0
          Top = 100
          Width = 1000
          Height = 10
          Cursor = crVSplit
          Align = alTop
          ExplicitLeft = -20
          ExplicitTop = 92
        end
        object PnProtoMail: TPanel
          Left = 0
          Top = 158
          Width = 1000
          Height = 186
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 186
            Align = alClient
            Caption = 'Panel3'
            TabOrder = 0
            object DBG_CE1: TdmkDBGrid
              Left = 1
              Top = 1
              Width = 998
              Height = 184
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NivelDescr'
                  Title.Caption = 'Status'
                  Width = 99
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ProtoLote'
                  Title.Caption = 'Lote'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Protocolo'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Bloqueto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Depto_Txt'
                  Title.Caption = 'UH'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Entid_Txt'
                  Title.Caption = 'Propriet'#225'rio'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataE_Txt'
                  Title.Caption = 'Envio'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataD_Txt'
                  Title.Caption = 'Retorno'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RecipEmeio'
                  Title.Caption = 'E-mail'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RecipProno'
                  Title.Caption = 'Pronome'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RecipNome'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Taref_Txt'
                  Title.Caption = 'Tarefa'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Deliv_Txt'
                  Title.Caption = 'Entregador'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsProtoMail
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBG_CE1DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NivelDescr'
                  Title.Caption = 'Status'
                  Width = 99
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ProtoLote'
                  Title.Caption = 'Lote'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Protocolo'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Bloqueto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Depto_Txt'
                  Title.Caption = 'UH'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Entid_Txt'
                  Title.Caption = 'Propriet'#225'rio'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataE_Txt'
                  Title.Caption = 'Envio'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataD_Txt'
                  Title.Caption = 'Retorno'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RecipEmeio'
                  Title.Caption = 'E-mail'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RecipProno'
                  Title.Caption = 'Pronome'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RecipNome'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Taref_Txt'
                  Title.Caption = 'Tarefa'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Deliv_Txt'
                  Title.Caption = 'Entregador'
                  Visible = True
                end>
            end
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 110
          Width = 1000
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object LaProtoMail: TLabel
            Left = 100
            Top = 8
            Width = 202
            Height = 13
            Caption = 'Aguarde... verificando protocolos...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object BtEmeio: TBitBtn
            Tag = 244
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Caption = '&E-mail'
            TabOrder = 0
            OnClick = BtEmeioClick
          end
          object PB1: TProgressBar
            Left = 100
            Top = 24
            Width = 669
            Height = 17
            TabOrder = 1
          end
        end
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 100
          Align = alTop
          DataSource = DsProt1
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 888
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'UHs cadastrados para cobran'#231'a registrada'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter3: TSplitter
          Left = 0
          Top = 100
          Width = 1000
          Height = 10
          Cursor = crVSplit
          Align = alTop
        end
        object DBG_CR1: TDBGrid
          Left = 0
          Top = 110
          Width = 1000
          Height = 234
          Align = alClient
          DataSource = DsCR1
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Boleto'
              Title.Caption = 'Bloqueto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'U.H.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROPRIET'
              Title.Caption = 'Propriet'#225'rio'
              Width = 215
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PROTOCOLO'
              Title.Caption = 'Protocolo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TAREFA'
              Title.Caption = 'Tarefa'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DELIVER'
              Title.Caption = 'Respons'#225'vel pela tarefa'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOTE'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALOR_PROT'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VENCTO_PROT'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MULTAVAL'
              Title.Caption = 'Multa'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MORADIAVAL'
              Title.Caption = 'Mora / dia'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUB_TOT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencto'
              Visible = True
            end>
        end
        object DBGrid3: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 100
          Align = alTop
          DataSource = DsProt1
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 888
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 422
        Height = 32
        Caption = 'Protocolos de Entrega de Bloqueto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 422
        Height = 32
        Caption = 'Protocolos de Entrega de Bloqueto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 422
        Height = 32
        Caption = 'Protocolos de Entrega de Bloqueto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 427
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 471
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtProtocolo: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Protocolo'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtProtocoloClick
      end
    end
  end
  object QrPPICir1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.Conta PROTOCOLO, ppi.Controle LOTE, ppi.DataE, '
      
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DATAE_TX' +
        'T,'
      
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DATAD_TX' +
        'T,'
      'ppi.DataD, ppi.Cancelado, ppi.Motivo, ppi.ID_Cod1, ppi.ID_Cod2, '
      'ppi.ID_Cod3, ppi.ID_Cod4, ppi.CliInt, ppi.Cliente, ppi.Periodo,'
      'ppi.Docum, ptc.Nome TAREFA, ptc.Def_Sender,'
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER,'
      'ppi.Valor, ppi.Vencto, ppi.MoraDiaVal, ppi.MultaVal'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN entidades  snd ON snd.Codigo=ptc.Def_Sender'
      'WHERE ppi.Link_ID=1'
      'AND ppi.ID_Cod1=:P0'
      'AND ppi.ID_Cod2=:P1'
      'AND ppi.ID_Cod3=:P2')
    Left = 224
    Top = 124
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPPICir1PROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'protpakits.Conta'
      Required = True
    end
    object QrPPICir1DataE: TDateField
      FieldName = 'DataE'
      Origin = 'protpakits.DataE'
      Required = True
    end
    object QrPPICir1DataD: TDateField
      FieldName = 'DataD'
      Origin = 'protpakits.DataD'
      Required = True
    end
    object QrPPICir1Cancelado: TIntegerField
      FieldName = 'Cancelado'
      Origin = 'protpakits.Cancelado'
      Required = True
    end
    object QrPPICir1Motivo: TIntegerField
      FieldName = 'Motivo'
      Origin = 'protpakits.Motivo'
      Required = True
    end
    object QrPPICir1ID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Origin = 'protpakits.ID_Cod1'
      Required = True
    end
    object QrPPICir1ID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Origin = 'protpakits.ID_Cod2'
      Required = True
    end
    object QrPPICir1ID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Origin = 'protpakits.ID_Cod3'
      Required = True
    end
    object QrPPICir1ID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Origin = 'protpakits.ID_Cod4'
      Required = True
    end
    object QrPPICir1TAREFA: TWideStringField
      FieldName = 'TAREFA'
      Origin = 'protocolos.Nome'
      Size = 100
    end
    object QrPPICir1DELIVER: TWideStringField
      FieldName = 'DELIVER'
      Size = 100
    end
    object QrPPICir1LOTE: TIntegerField
      FieldName = 'LOTE'
      Origin = 'protpakits.Controle'
      Required = True
    end
    object QrPPICir1CliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'protpakits.CliInt'
      Required = True
    end
    object QrPPICir1Cliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'protpakits.Cliente'
      Required = True
    end
    object QrPPICir1Periodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'protpakits.Periodo'
      Required = True
    end
    object QrPPICir1DATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 10
    end
    object QrPPICir1DATAD_TXT: TWideStringField
      FieldName = 'DATAD_TXT'
      Size = 10
    end
    object QrPPICir1Def_Sender: TIntegerField
      FieldName = 'Def_Sender'
      Origin = 'protocolos.Def_Sender'
    end
    object QrPPICir1Valor: TFloatField
      FieldName = 'Valor'
      Origin = 'protpakits.Valor'
      Required = True
    end
    object QrPPICir1Vencto: TDateField
      FieldName = 'Vencto'
      Origin = 'protpakits.Vencto'
      Required = True
    end
    object QrPPICir1MoraDiaVal: TFloatField
      FieldName = 'MoraDiaVal'
      Required = True
    end
    object QrPPICir1MultaVal: TFloatField
      FieldName = 'MultaVal'
      Required = True
    end
    object QrPPICir1Docum: TFloatField
      FieldName = 'Docum'
      Required = True
    end
  end
  object PMProtocolo: TPopupMenu
    OnPopup = PMProtocoloPopup
    Left = 49
    Top = 409
    object Gera1: TMenuItem
      Caption = '&Gera protocolo'
      object Todosabertos1: TMenuItem
        Caption = '&Todos abertos'
        OnClick = Todosabertos1Click
      end
      object Abertosseleciondos1: TMenuItem
        Caption = '&Abertos seleciondos'
        OnClick = Abertosseleciondos1Click
      end
      object Atuaseaberto1: TMenuItem
        Caption = '&Atual (se aberto)'
        OnClick = Atuaseaberto1Click
      end
    end
    object Desfazprotocolo1: TMenuItem
      Caption = '&Desfaz protocolo'
      OnClick = Desfazprotocolo1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Localizaprotocolo1: TMenuItem
      Caption = '&Localiza lote'
      OnClick = Localizaprotocolo1Click
    end
  end
  object DsBolMail: TDataSource
    DataSet = QrBolMail
    Left = 460
    Top = 264
  end
  object QrBolMail: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrBolMailBeforeOpen
    OnCalcFields = QrBolMailCalcFields
    Left = 432
    Top = 264
    object QrBolMailApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolMailPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrBolMailVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrBolMailUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrBolMailNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrBolMailBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolMailBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
    object QrBolMailSUB_ARR: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_ARR'
      LookupDataSet = QrBolArr
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBolMailSUB_LEI: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_LEI'
      LookupDataSet = QrBolLei
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBolMailSUB_TOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOT'
      Calculated = True
    end
    object QrBolMailPPI_BOLETO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'PPI_BOLETO'
      LookupDataSet = QrPPI_
      LookupKeyFields = 'Docum'
      LookupResultField = 'Docum'
      KeyFields = 'Boleto'
      Lookup = True
    end
    object QrBolMailPROTOCOD: TFloatField
      FieldName = 'PROTOCOD'
    end
  end
  object QrProtoMail: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM protomail'
      'ORDER BY NivelEmail')
    Left = 621
    Top = 13
    object QrProtoMailDepto_Cod: TIntegerField
      FieldName = 'Depto_Cod'
      Origin = 'protomail.Depto_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailDepto_Txt: TWideStringField
      FieldName = 'Depto_Txt'
      Origin = 'protomail.Depto_Txt'
      Size = 100
    end
    object QrProtoMailEntid_Cod: TIntegerField
      FieldName = 'Entid_Cod'
      Origin = 'protomail.Entid_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailEntid_Txt: TWideStringField
      FieldName = 'Entid_Txt'
      Origin = 'protomail.Entid_Txt'
      Size = 100
    end
    object QrProtoMailTaref_Cod: TIntegerField
      FieldName = 'Taref_Cod'
      Origin = 'protomail.Taref_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailTaref_Txt: TWideStringField
      FieldName = 'Taref_Txt'
      Origin = 'protomail.Taref_Txt'
      Size = 100
    end
    object QrProtoMailDeliv_Cod: TIntegerField
      FieldName = 'Deliv_Cod'
      Origin = 'protomail.Deliv_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailDeliv_Txt: TWideStringField
      FieldName = 'Deliv_Txt'
      Origin = 'protomail.Deliv_Txt'
      Size = 100
    end
    object QrProtoMailDataE: TDateField
      FieldName = 'DataE'
      Origin = 'protomail.DataE'
    end
    object QrProtoMailDataE_Txt: TWideStringField
      FieldName = 'DataE_Txt'
      Origin = 'protomail.DataE_Txt'
      Size = 30
    end
    object QrProtoMailDataD: TDateField
      FieldName = 'DataD'
      Origin = 'protomail.DataD'
    end
    object QrProtoMailDataD_Txt: TWideStringField
      FieldName = 'DataD_Txt'
      Origin = 'protomail.DataD_Txt'
      Size = 30
    end
    object QrProtoMailProtoLote: TIntegerField
      FieldName = 'ProtoLote'
      Origin = 'protomail.ProtoLote'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailNivelEmail: TSmallintField
      FieldName = 'NivelEmail'
      Origin = 'protomail.NivelEmail'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailItem: TIntegerField
      FieldName = 'Item'
      Origin = 'protomail.Item'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailNivelDescr: TWideStringField
      FieldName = 'NivelDescr'
      Origin = 'protomail.NivelDescr'
      Size = 50
    end
    object QrProtoMailProtocolo: TIntegerField
      FieldName = 'Protocolo'
      Origin = 'protomail.Protocolo'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailRecipEmeio: TWideStringField
      FieldName = 'RecipEmeio'
      Size = 100
    end
    object QrProtoMailRecipNome: TWideStringField
      FieldName = 'RecipNome'
      Size = 100
    end
    object QrProtoMailRecipProno: TWideStringField
      FieldName = 'RecipProno'
    end
    object QrProtoMailRecipItem: TIntegerField
      FieldName = 'RecipItem'
    end
    object QrProtoMailBloqueto: TFloatField
      FieldName = 'Bloqueto'
    end
    object QrProtoMailVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrProtoMailValor: TFloatField
      FieldName = 'Valor'
    end
    object QrProtoMailPreEmeio: TIntegerField
      FieldName = 'PreEmeio'
    end
    object QrProtoMailCondominio: TWideStringField
      FieldName = 'Condominio'
      Size = 100
    end
    object QrProtoMailIDEmeio: TIntegerField
      FieldName = 'IDEmeio'
    end
  end
  object DsProtoMail: TDataSource
    DataSet = QrProtoMail
    Left = 649
    Top = 13
  end
  object QrCondEmeios: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECOND, cem.*' +
        ' '
      'FROM condemeios cem'
      'LEFT JOIN cond cnd ON cnd.Codigo=cem.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE cem.Conta=:P0')
    Left = 232
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondEmeiosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondEmeiosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCondEmeiosConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCondEmeiosItem: TIntegerField
      FieldName = 'Item'
    end
    object QrCondEmeiosPronome: TWideStringField
      FieldName = 'Pronome'
    end
    object QrCondEmeiosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCondEmeiosEMeio: TWideStringField
      FieldName = 'EMeio'
      Size = 100
    end
    object QrCondEmeiosNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
  end
  object QrPPIMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.Conta PROTOCOLO, ppi.Controle LOTE, ppi.DataE, '
      
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DATAE_TX' +
        'T,'
      
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DATAD_TX' +
        'T,'
      'ppi.DataD, ppi.Cancelado, ppi.Motivo, ppi.ID_Cod1, ppi.ID_Cod2, '
      'ppi.ID_Cod3, ppi.ID_Cod4, ppi.CliInt, ppi.Cliente, ppi.Periodo,'
      
        'ppi.Docum, ptc.Codigo TAREFA_COD, ptc.Nome TAREFA_NOM, ptc.Def_S' +
        'ender,'
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ptc.PreEmeio'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN entidades  snd ON snd.Codigo=ptc.Def_Sender'
      'WHERE ppi.Link_ID=1'
      'AND ppi.ID_Cod1=:P0'
      'AND ppi.ID_Cod2=:P1'
      'AND ppi.ID_Cod3=:P2')
    Left = 488
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPPIMailPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Required = True
    end
    object QrPPIMailLOTE: TIntegerField
      FieldName = 'LOTE'
      Required = True
    end
    object QrPPIMailDataE: TDateField
      FieldName = 'DataE'
      Required = True
    end
    object QrPPIMailDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 10
    end
    object QrPPIMailDATAD_TXT: TWideStringField
      FieldName = 'DATAD_TXT'
      Size = 10
    end
    object QrPPIMailCancelado: TIntegerField
      FieldName = 'Cancelado'
      Required = True
    end
    object QrPPIMailMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrPPIMailID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Required = True
    end
    object QrPPIMailID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Required = True
    end
    object QrPPIMailID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Required = True
    end
    object QrPPIMailID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Required = True
    end
    object QrPPIMailCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPPIMailCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPPIMailPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrPPIMailDef_Sender: TIntegerField
      FieldName = 'Def_Sender'
    end
    object QrPPIMailDELIVER: TWideStringField
      FieldName = 'DELIVER'
      Size = 100
    end
    object QrPPIMailDocum: TFloatField
      FieldName = 'Docum'
      Required = True
    end
    object QrPPIMailPreEmeio: TIntegerField
      FieldName = 'PreEmeio'
    end
    object QrPPIMailDataD: TDateTimeField
      FieldName = 'DataD'
      Required = True
    end
    object QrPPIMailTAREFA_COD: TIntegerField
      FieldName = 'TAREFA_COD'
      Required = True
    end
    object QrPPIMailTAREFA_NOM: TWideStringField
      FieldName = 'TAREFA_NOM'
      Size = 100
    end
  end
  object QrCD1: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCD1CalcFields
    Left = 168
    Top = 124
    object QrCD1SUB_ARR: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_ARR'
      LookupDataSet = QrBolArr
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrCD1SUB_LEI: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_LEI'
      LookupDataSet = QrBolLei
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrCD1SUB_TOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCD1Apto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrCD1Propriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrCD1Vencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCD1Unidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrCD1NOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrCD1BOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrCD1PROTOCOLO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'PROTOCOLO'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'PROTOCOLO'
      KeyFields = 'Boleto'
      DisplayFormat = '000000;-000000; '
      Lookup = True
    end
    object QrCD1TAREFA: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TAREFA'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'TAREFA'
      KeyFields = 'Boleto'
      Size = 50
      Lookup = True
    end
    object QrCD1DELIVER: TWideStringField
      FieldKind = fkLookup
      FieldName = 'DELIVER'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'DELIVER'
      KeyFields = 'Boleto'
      Size = 100
      Lookup = True
    end
    object QrCD1LOTE: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LOTE'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'LOTE'
      KeyFields = 'Boleto'
      DisplayFormat = '000000;-000000; '
      Lookup = True
    end
    object QrCD1VENCTO_PROT: TDateField
      FieldKind = fkLookup
      FieldName = 'VENCTO_PROT'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'Vencto'
      KeyFields = 'Boleto'
      DisplayFormat = 'dd/mm/yy'
      Lookup = True
    end
    object QrCD1VALOR_PROT: TFloatField
      FieldKind = fkLookup
      FieldName = 'VALOR_PROT'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'Valor'
      KeyFields = 'Boleto'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrCD1MULTAVAL: TFloatField
      FieldKind = fkLookup
      FieldName = 'MULTAVAL'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'MultaVal'
      KeyFields = 'Boleto'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrCD1MORADIAVAL: TFloatField
      FieldKind = fkLookup
      FieldName = 'MORADIAVAL'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'MoraDiaVal'
      KeyFields = 'Boleto'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrCD1Boleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
    object QrCD1PROTOCOD: TFloatField
      FieldName = 'PROTOCOD'
    end
  end
  object DsCD1: TDataSource
    DataSet = QrCD1
    Left = 196
    Top = 124
  end
  object QrCR1: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCR1CalcFields
    Left = 88
    Top = 236
    object QrCR1SUB_ARR: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_ARR'
      LookupDataSet = QrBolArr
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrCR1SUB_LEI: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_LEI'
      LookupDataSet = QrBolLei
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrCR1SUB_TOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCR1Apto: TIntegerField
      FieldName = 'Apto'
      Origin = 'Apto'
      Required = True
    end
    object QrCR1Propriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'Propriet'
      Required = True
    end
    object QrCR1Vencto: TDateField
      FieldName = 'Vencto'
      Origin = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCR1Unidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'Unidade'
      Size = 10
    end
    object QrCR1NOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Origin = 'NOMEPROPRIET'
      Size = 100
    end
    object QrCR1BOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrCR1PROTOCOLO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'PROTOCOLO'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'PROTOCOLO'
      KeyFields = 'Boleto'
      DisplayFormat = '000000;-000000; '
      Lookup = True
    end
    object QrCR1TAREFA: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TAREFA'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'TAREFA'
      KeyFields = 'Boleto'
      Size = 50
      Lookup = True
    end
    object QrCR1DELIVER: TWideStringField
      FieldKind = fkLookup
      FieldName = 'DELIVER'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'DELIVER'
      KeyFields = 'Boleto'
      Size = 100
      Lookup = True
    end
    object QrCR1LOTE: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LOTE'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'LOTE'
      KeyFields = 'Boleto'
      DisplayFormat = '000000;-000000; '
      Lookup = True
    end
    object QrCR1VALOR_PROT: TFloatField
      FieldKind = fkLookup
      FieldName = 'VALOR_PROT'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'Valor'
      KeyFields = 'Boleto'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrCR1VENCTO_PROT: TDateField
      FieldKind = fkLookup
      FieldName = 'VENCTO_PROT'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'Vencto'
      KeyFields = 'Boleto'
      DisplayFormat = 'dd/mm/yy'
      Lookup = True
    end
    object QrCR1MORADIAVAL: TFloatField
      FieldKind = fkLookup
      FieldName = 'MORADIAVAL'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'MoraDiaVal'
      KeyFields = 'Boleto'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrCR1MULTAVAL: TFloatField
      FieldKind = fkLookup
      FieldName = 'MULTAVAL'
      LookupDataSet = QrPPICir1
      LookupKeyFields = 'Docum'
      LookupResultField = 'MultaVal'
      KeyFields = 'Boleto'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrCR1Boleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
    object QrCR1PROTOCOD: TFloatField
      FieldName = 'PROTOCOD'
    end
  end
  object DsCR1: TDataSource
    DataSet = QrCR1
    Left = 116
    Top = 236
  end
  object QrProtos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ppi.Controle LOTE'
      ''
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      ''
      'WHERE ppi.Link_ID=1'
      'AND ppi.ID_Cod1=:P0'
      'AND ppi.ID_Cod2=:P1'
      'AND ptc.Tipo=:P2')
    Left = 260
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrProtosLOTE: TIntegerField
      FieldName = 'LOTE'
      Required = True
    end
  end
  object DsProtos: TDataSource
    DataSet = QrProtos
    Left = 288
    Top = 240
  end
  object QrBolLei: TmySQLQuery
    Database = Dmod.MyDB
    Left = 521
    Top = 125
    object QrBolLeiValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
    object QrBolLeiApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolLeiBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolLeiBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrBolArr: TmySQLQuery
    Database = Dmod.MyDB
    Left = 493
    Top = 125
    object QrBolArrValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
    object QrBolArrApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolArrBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolArrBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrProt1: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrProt1BeforeClose
    AfterScroll = QrProt1AfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM protocolos'
      'WHERE Tipo=1 '
      'AND def_client=:P0'
      '')
    Left = 16
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProt1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProt1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsProt1: TDataSource
    DataSet = QrProt1
    Left = 44
    Top = 312
  end
  object QrPPI_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.Docum'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN entidades  snd ON snd.Codigo=ptc.Def_Sender'
      'WHERE ppi.Link_ID=1'
      'AND ppi.ID_Cod1=:P0'
      'AND ppi.ID_Cod2=:P1'
      'AND ppi.Periodo=:P2')
    Left = 144
    Top = 236
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPPI_Docum: TFloatField
      FieldName = 'Docum'
    end
  end
end
