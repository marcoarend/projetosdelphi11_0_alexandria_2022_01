unit GeraCNAB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Db, mySQLDbTables, Grids,
  DBGrids, dmkGeral, dmkEdit, dmkEditDateTimePicker, dmkImage, UnDmkEnums;

type
  TFmGeraCNAB = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    TPDataD: TdmkEditDateTimePicker;
    TPDataC: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label9: TLabel;
    EdMulta: TdmkEdit;
    Label10: TLabel;
    EdJuros: TdmkEdit;
    Label3: TLabel;
    TPDataV: TdmkEditDateTimePicker;
    QrDir: TmySQLQuery;
    QrDirCodigo: TIntegerField;
    QrDirNome: TWideStringField;
    QrDirEnvio: TSmallintField;
    QrDirCliInt: TIntegerField;
    QrDirLk: TIntegerField;
    QrDirDataCad: TDateField;
    QrDirDataAlt: TDateField;
    QrDirUserCad: TIntegerField;
    QrDirUserAlt: TIntegerField;
    QrDirCarteira: TIntegerField;
    QrBoletos: TmySQLQuery;
    DsBoletos: TDataSource;
    Panel4: TPanel;
    DBGradeS: TDBGrid;
    Memo1: TMemo;
    Splitter1: TSplitter;
    QrBoletosApto: TIntegerField;
    QrBoletosPropriet: TIntegerField;
    QrBoletosVencto: TDateField;
    QrBoletosUnidade: TWideStringField;
    QrBoletosNOMEPROPRIET: TWideStringField;
    QrBoletosBOLAPTO: TWideStringField;
    QrBolArr: TmySQLQuery;
    QrBolLei: TmySQLQuery;
    QrBolArrApto: TIntegerField;
    QrBolArrValor: TFloatField;
    QrBolArrBOLAPTO: TWideStringField;
    QrBolLeiApto: TIntegerField;
    QrBolLeiValor: TFloatField;
    QrBolLeiBOLAPTO: TWideStringField;
    QrBoletosSUB_ARR: TFloatField;
    QrBoletosSUB_LEI: TFloatField;
    QrBoletosSUB_TOT: TFloatField;
    QrBoletosVENCTO_TXT: TWideStringField;
    QrBoletosInstante: TFloatField;
    Panel18: TPanel;
    Label5: TLabel;
    LaCIS: TLabel;
    LaSVIS: TLabel;
    Label25: TLabel;
    QrBolArrBoleto: TFloatField;
    QrBolLeiBoleto: TFloatField;
    QrBoletosBoleto: TFloatField;
    QrBoletosBLOQUETO: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    Label4: TLabel;
    EdArq: TEdit;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdMultaExit(Sender: TObject);
    procedure EdJurosExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TPDataDChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure QrBoletosAfterOpen(DataSet: TDataSet);
    procedure DBGradeSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenBoletos;
  public
    { Public declarations }
    FTabAriA, FTabCnsA: String;
  end;

  var
  FmGeraCNAB: TFmGeraCNAB;

implementation

uses Module, ModuleGeral, UnMyObjects;

{$R *.DFM}

procedure TFmGeraCNAB.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGeraCNAB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmGeraCNAB.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGeraCNAB.BtOKClick(Sender: TObject);
var
  i, n: Integer;
  Linha, Txt: String;
  Multa, Juros, ValJ, ValM, ValP: Double;
begin
  Memo1.Lines.Clear;
  Juros := (Geral.DMV(EdJuros.Text) / 30);
  i := Trunc(TPDataD.Date) - Trunc(TPDataV.Date);
  if i > 0 then
  begin
    Juros := Juros * i;
    Multa := Geral.DMV(EdMulta.Text);
  end else begin
    Juros := 0;
    Multa := 0;
  end;
  Linha := '02RETORNO';
  Linha := Linha + '01COBRANCA';
  Linha := Linha + '       ';
  Linha := Linha + '3392879869327000154';
  Linha := Linha + '                               ';
  Linha := Linha + '748BANSICREDI     ';
  //
  Linha := Linha + FormatDateTime('yyyymmdd', TPDataD.Date);
  Linha := Linha + '        ';
  Linha := Linha + '0000168';
  while Length(Linha) < 390 do
    Linha := Linha + ' ';
  Linha := Linha + '1.00000001';
  Memo1.Lines.Add(Linha);
  //
  n := 1;
  (*with FmCondGer.DBGradeS.DataSource.DataSet do
  for i:= 0 to FmCondGer.DBGradeS.SelectedRows.Count-1 do
  begin
    GotoBook mark(point er(FmCondGer.DBGradeS.SelectedRows.Items[i]));*)
  QrBoletos.Close;
  QrBoletos.SQL.Clear;
  QrBoletos.SQL.Add('SELECT DISTINCT ari.Boleto, ari.Apto, ari.Propriet, ari.Vencto,');
  QrBoletos.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrBoletos.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
  QrBoletos.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO, cnb.Instante');
  QrBoletos.SQL.Add('FROM ' + FTabAriA + ' ari');
  QrBoletos.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
  QrBoletos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
  QrBoletos.SQL.Add('LEFT JOIN locsyndic.cnab_1 cnb ON ari.Boleto=cnb.Boleto');
  QrBoletos.SQL.Add('WHERE cnb.Boleto IS NOT NULL');
  QrBoletos.SQL.Add('');
  QrBoletos.SQL.Add('UNION');
  QrBoletos.SQL.Add('');
  QrBoletos.SQL.Add('SELECT DISTINCT cni.Boleto, cni.Apto, cni.Propriet, cni.Vencto,');
  QrBoletos.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrBoletos.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
  QrBoletos.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO, cnb.Instante');
  QrBoletos.SQL.Add('FROM ' + FTabCnsA + ' cni');
  QrBoletos.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
  QrBoletos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
  QrBoletos.SQL.Add('LEFT JOIN locsyndic.cnab_1 cnb  ON cni.Boleto=cnb.Boleto');
  QrBoletos.SQL.Add('WHERE cnb.Boleto IS NOT NULL');
  QrBoletos.SQL.Add('');
  QrBoletos.SQL.Add('ORDER BY Instante');
  QrBoletos.Open;
  while not QrBoletos.Eof do
  begin
    //
    Txt := FormatFloat('00000000', QrBoletosBLOQUETO.Value);
    Txt := Txt + Geral.Modulo11_2a9_Back(Txt, 0);
    //Num := QrBoletosBLOQUETO.Value * 10 + MLAGeral.Modulo11_2a9_Back(Txt, 0);
    ValJ := Int(QrBoletosSUB_TOT.Value * Juros);
    ValM := Int(QrBoletosSUB_TOT.Value * Multa);
    ValP := (QrBoletosSUB_TOT.Value * 100) + ValJ + ValM;
    //
    Linha := '1            C0000000000                       ';
    Linha := Linha + Txt;
    Linha := Linha + '                                                    06';
    Linha := Linha + FormatDateTime('ddmmyy', TPDataD.Date);
    Linha := Linha + '          COMPE                     ';
    Linha := Linha + FormatFloat('0000000000000', QrBoletosSUB_TOT.Value * 100);
    Linha := Linha + '                                                              ';
    Linha := Linha + '00000000000000000000000000';
    Linha := Linha + FormatFloat('0000000000000', ValP);
    Linha := Linha + FormatFloat('0000000000000', ValM);
    Linha := Linha + FormatFloat('0000000000000', ValJ);
    Linha := Linha + '                          H5        ';
    Linha := Linha + FormatDateTime('yyyymmdd', TPDataC.Date);
    Linha := Linha + '                                                          ';
    n := n + 1;
    Linha := Linha + FormatFloat('000000', n);
    //
    Memo1.Lines.Add(Linha);
    //
    Linha := '1            C0000000000                       ';
    Linha := Linha + Txt;
    Linha := Linha + '                                                    28';
    Linha := Linha + FormatDateTime('ddmmyy', TPDataD.Date);
    Linha := Linha + '          COMPE                     ';
    Linha := Linha + FormatFloat('0000000000000', 1.6 * 100);
    Linha := Linha + '                                                              ';
    Linha := Linha + '00000000000000000000000000';
    Linha := Linha + FormatFloat('0000000000000', ValP);
    Linha := Linha + FormatFloat('0000000000000', ValM);
    Linha := Linha + FormatFloat('0000000000000', ValJ);
    Linha := Linha + '                          B3        ';
    Linha := Linha + FormatDateTime('yyyymmdd', TPDataD.Date);
    Linha := Linha + '                                                          ';
    n := n + 1;
    Linha := Linha + FormatFloat('000000', n);
    //
    Memo1.Lines.Add(Linha);
    QrBoletos.Next;
  end;
  Linha := '9274833928';
  while Length(Linha) < 394 do
    Linha := Linha + ' ';
  n := n + 1;
  Linha := Linha + FormatFloat('000000', n);
  //
  Memo1.Lines.Add(Linha);
end;

procedure TFmGeraCNAB.EdMultaExit(Sender: TObject);
begin
  EdMulta.Text := Geral.TFT(EdMulta.Text, 2, siPositivo);
end;

procedure TFmGeraCNAB.EdJurosExit(Sender: TObject);
begin
  EdJuros.Text := Geral.TFT(EdJuros.Text, 2, siPositivo);
end;

procedure TFmGeraCNAB.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrBolArr.Close;
  QrBolArr.Database := DModG.MyPID_DB;
  //
  QrBolLei.Close;
  QrBolLei.Database := DModG.MyPID_DB;
  //
  TPDataV.Date := Date;
  TPDataD.Date := Date;
  TPDataC.Date := Date;
  //
  QrDir.Close;
  QrDir.Open;
  EdArq.Text := QrDirNome.Value;
  ReopenBoletos;
end;

procedure TFmGeraCNAB.ReopenBoletos();
begin
  QrBoletos.Close;
  QrBolArr.Close;
  QrBolLei.Close;
  //
  QrBolArr.SQL.Clear;
  QrBolArr.SQL.Add('SELECT ari.Boleto, ari.Apto, SUM(ari.Valor) Valor,');
  QrBolArr.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO');
  QrBolArr.SQL.Add('FROM cnab_1 cnb');
  QrBolArr.SQL.Add('LEFT JOIN syndic.' + FTabAriA + ' ari ON ari.Boleto=cnb.Boleto');
  QrBolArr.SQL.Add('GROUP BY ari.Boleto, ari.Apto');
  QrBolArr.Open;
  //
  QrBolLei.SQL.Clear;
  QrBolLei.SQL.Add('SELECT cni.Boleto, cni.Apto, SUM(cni.Valor) Valor,');
  QrBolLei.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO');
  QrBolLei.SQL.Add('FROM cnab_1 cnb');
  QrBolLei.SQL.Add('LEFT JOIN syndic.' + FTabCnsA
   + ' cni ON cni.Boleto=cnb.Boleto');
  QrBolLei.SQL.Add('GROUP BY cni.Boleto, cni.Apto');
  QrBolLei.Open;
  //
  QrBoletos.Open;
end;

procedure TFmGeraCNAB.TPDataDChange(Sender: TObject);
begin
  EdArq.Text := QrDirNome.Value + '\' +
    FormatDateTime('yyyymmdd', TPDataD.Date) + '.CRT';
end;

procedure TFmGeraCNAB.BitBtn1Click(Sender: TObject);
begin
  MLAGeral.ExportaMemoToFile(Memo1, EdArq.Text, False, True, True);
end;

procedure TFmGeraCNAB.QrBoletosCalcFields(DataSet: TDataSet);
begin
  QrBoletosSUB_TOT.Value :=
  QrBoletosSUB_ARR.Value +
  QrBoletosSUB_LEI.Value;
  //
  if QrBoletosBoleto.Value = 0 then
    QrBoletosBLOQUETO.Value := -1
  else
    QrBoletosBLOQUETO.Value := QrBoletosBoleto.Value;
  //
  QrBoletosVENCTO_TXT.Value := Geral.FDT(QrBoletosVencto.Value, 3);
end;

procedure TFmGeraCNAB.QrBoletosAfterOpen(DataSet: TDataSet);
var
  Valor: Double;
begin
  QrBoletos.DisableControls;
  Valor := 0;
  while not QrBoletos.Eof do
  begin
    Valor := Valor + QrBoletosSUB_TOT.Value;
    QrBoletos.Next;
  end;
  LaCIS.Caption := IntToStr(QrBoletos.RecordCount);
  LaSVIS.Caption := Geral.FFT(Valor, 2, siNegativo);
  QrBoletos.First;
  QrBoletos.EnableControls;
end;

procedure TFmGeraCNAB.DBGradeSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_DELETE) and (shift = [ssCtrl]) then
  begin
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('DELETE FROM cnab_1 WHERE Boleto=:P0');
    DModG.QrUpdPID1.Params[0].AsFloat  := QrBoletosBoleto.Value;
    DModG.QrUpdPID1.ExecSQL;
    //
    ReopenBoletos;
  end;
end;

end.

