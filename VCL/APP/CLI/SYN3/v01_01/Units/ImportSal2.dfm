object FmImportSal2: TFmImportSal2
  Left = 339
  Top = 185
  Caption = 'SAL-IMP02-001 :: Carrega Lan'#231'amentos de Sal'#225'rios - Modelo 2'
  ClientHeight = 725
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 526
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnConfig: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 178
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 1241
        Height = 74
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 5
          Top = 20
          Width = 319
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Caminho do arquivo com os pagamentos de sal'#225'rios:'
        end
        object SpeedButton1: TSpeedButton
          Left = 1201
          Top = 39
          Width = 29
          Height = 27
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object EdPath: TEdit
          Left = 5
          Top = 39
          Width = 1193
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 0
          OnChange = EdPathChange
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 74
        Width = 1241
        Height = 104
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object LaEmpresa: TLabel
          Left = 10
          Top = 15
          Width = 58
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Empresa:'
        end
        object Label11: TLabel
          Left = 10
          Top = 44
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Carteira'
        end
        object Label12: TLabel
          Left = 10
          Top = 74
          Width = 83
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Configura'#231#227'o:'
        end
        object Label13: TLabel
          Left = 729
          Top = 2
          Width = 124
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data do lan'#231'amento:'
        end
        object LaMes: TLabel
          Left = 862
          Top = 2
          Width = 34
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'M'#234's*:'
        end
        object LaVencimento: TLabel
          Left = 932
          Top = 2
          Width = 100
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Vencimento [F6]:'
        end
        object Label14: TLabel
          Left = 729
          Top = 49
          Width = 272
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o + nome do funcion'#225'rio + descri'#231#227'o:'
        end
        object SbConfig: TSpeedButton
          Left = 689
          Top = 69
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbConfigClick
        end
        object EdEmpresa: TdmkEditCB
          Left = 94
          Top = 10
          Width = 68
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 162
          Top = 10
          Width = 552
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCarteira: TdmkEditCB
          Left = 94
          Top = 39
          Width = 68
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCarteiraChange
          DBLookupComboBox = CBCarteira
          IgnoraDBLookupComboBox = False
        end
        object CBCarteira: TdmkDBLookupComboBox
          Left = 162
          Top = 39
          Width = 552
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCarteiras
          TabOrder = 3
          dmkEditCB = EdCarteira
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdConfig: TdmkEditCB
          Left = 94
          Top = 69
          Width = 68
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdConfigChange
          DBLookupComboBox = CBConfig
          IgnoraDBLookupComboBox = False
        end
        object CBConfig: TdmkDBLookupComboBox
          Left = 162
          Top = 69
          Width = 524
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsImportSalCfgCab
          TabOrder = 5
          dmkEditCB = EdConfig
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object TPData: TdmkEditDateTimePicker
          Left = 729
          Top = 22
          Width = 130
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 41433.655720300900000000
          Time = 41433.655720300900000000
          TabOrder = 6
          OnChange = EdPathChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'Data'
          UpdType = utYes
        end
        object EdMes: TdmkEdit
          Left = 862
          Top = 22
          Width = 66
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taCenter
          TabOrder = 7
          FormatType = dmktfMesAno
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfShort
          QryCampo = 'Mez'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = Null
          ValWarn = False
          OnChange = EdPathChange
        end
        object TPVencimento: TdmkEditDateTimePicker
          Left = 932
          Top = 22
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 41433.672523148100000000
          Time = 41433.672523148100000000
          TabOrder = 8
          OnChange = EdPathChange
          OnKeyDown = TPVencimentoKeyDown
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'Vencimento'
          UpdType = utYes
        end
        object EdDescriA: TdmkEdit
          Left = 729
          Top = 69
          Width = 188
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdPathChange
        end
        object EdDescriB: TdmkEdit
          Left = 921
          Top = 69
          Width = 149
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdPathChange
        end
        object GroupBox6: TGroupBox
          Left = 1078
          Top = 1
          Width = 154
          Height = 106
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '                                   '
          TabOrder = 11
          object LaDoc: TLabel
            Left = 10
            Top = 42
            Width = 129
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'S'#233'rie  e docum. (CH) :'
            Enabled = False
          end
          object EdSerieCH1: TdmkEdit
            Left = 10
            Top = 62
            Width = 50
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            CharCase = ecUpperCase
            Enabled = False
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SerieCH'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdPathChange
          end
          object EdDocumento1: TdmkEdit
            Left = 63
            Top = 62
            Width = 76
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            QryCampo = 'Documento'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdPathChange
          end
          object CkAtribCHManu: TCheckBox
            Left = 15
            Top = 0
            Width = 129
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Atrib. CH manual:'
            Enabled = False
            TabOrder = 0
            OnClick = CkAtribCHManuClick
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 178
      Width = 1241
      Height = 348
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Arquivo carregado '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 1231
          Height = 313
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          WordWrap = False
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Pr'#233'-lan'#231'amentos financeiros '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1231
          Height = 313
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsLct
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGrid1CellClick
          OnDrawColumnCell = DBGrid1DrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Ok'
              Width = 25
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Genero'
              Title.Caption = 'Conta'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CONTA'
              Title.Caption = 'Descri'#231#227'o da conta'
              Width = 167
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 354
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SerieCH'
              Title.Caption = 'S'#233'rie Ch.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Title.Caption = 'N'#186' Ch.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's/Ano'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sit'
              ReadOnly = True
              Width = 20
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 643
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Carrega Lan'#231'amentos de Sal'#225'rios - Modelo 2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 643
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Carrega Lan'#231'amentos de Sal'#225'rios - Modelo 2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 643
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Carrega Lan'#231'amentos de Sal'#225'rios - Modelo 2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 585
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 1236
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 353
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'M'#234's*: Deixe vazio quando informado no arquivo.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 353
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'M'#234's*: Deixe vazio quando informado no arquivo.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 639
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1061
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 18
      Width = 1059
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPreGera: TBitBtn
        Tag = 241
        Left = 25
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Pr'#233'-gera'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPreGeraClick
      end
      object BtConfirma: TBitBtn
        Tag = 10035
        Left = 330
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BitBtn2: TBitBtn
        Tag = 11
        Left = 177
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtPreGeraClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 482
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Todos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 634
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Nenhum'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtNenhumClick
      end
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fpf.Codigo, fpf.Chapa'
      'FROM fpfunci fpf'
      'LEFT JOIN entidades ent ON ent.Codigo=fpf.Codigo'
      'WHERE fpf.Empresa=:P0'
      'AND fpf.Registro=:P1'
      'AND ent.CPF=:P2')
    Left = 292
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciChapa: TWideStringField
      FieldName = 'Chapa'
      Size = 50
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, '
      'Tipo, ExigeNumCheque, ForneceI, TipoDoc'
      'FROM carteiras '
      'WHERE ForneceI=:P0'
      'ORDER BY Nome')
    Left = 528
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 556
    Top = 220
  end
  object QrImportSalCfgCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrImportSalCfgCabBeforeClose
    AfterScroll = QrImportSalCfgCabAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM importsalcfgcab'
      'ORDER BY Nome')
    Left = 528
    Top = 268
    object QrImportSalCfgCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImportSalCfgCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrImportSalCfgCabEmpCNPJIni: TIntegerField
      FieldName = 'EmpCNPJIni'
    end
    object QrImportSalCfgCabEmpCNPJTam: TIntegerField
      FieldName = 'EmpCNPJTam'
    end
    object QrImportSalCfgCabFunCPFIni: TIntegerField
      FieldName = 'FunCPFIni'
    end
    object QrImportSalCfgCabFunCPFTam: TIntegerField
      FieldName = 'FunCPFTam'
    end
    object QrImportSalCfgCabFunCodIni: TIntegerField
      FieldName = 'FunCodIni'
    end
    object QrImportSalCfgCabFunCodTam: TIntegerField
      FieldName = 'FunCodTam'
    end
    object QrImportSalCfgCabFunNomIni: TIntegerField
      FieldName = 'FunNomIni'
    end
    object QrImportSalCfgCabFunNomTam: TIntegerField
      FieldName = 'FunNomTam'
    end
    object QrImportSalCfgCabCtaCodIni: TIntegerField
      FieldName = 'CtaCodIni'
    end
    object QrImportSalCfgCabCtaCodTam: TIntegerField
      FieldName = 'CtaCodTam'
    end
    object QrImportSalCfgCabMesRefIni: TIntegerField
      FieldName = 'MesRefIni'
    end
    object QrImportSalCfgCabMesRefTam: TIntegerField
      FieldName = 'MesRefTam'
    end
    object QrImportSalCfgCabAnoRefIni: TIntegerField
      FieldName = 'AnoRefIni'
    end
    object QrImportSalCfgCabAnoRefTam: TIntegerField
      FieldName = 'AnoRefTam'
    end
    object QrImportSalCfgCabValLiqIni: TIntegerField
      FieldName = 'ValLiqIni'
    end
    object QrImportSalCfgCabValLiqTam: TIntegerField
      FieldName = 'ValLiqTam'
    end
    object QrImportSalCfgCabSeparador: TWideStringField
      FieldName = 'Separador'
      Size = 1
    end
    object QrImportSalCfgCabUsaSepara: TSmallintField
      FieldName = 'UsaSepara'
    end
    object QrImportSalCfgCabFloatSep: TWideStringField
      FieldName = 'FloatSep'
      Size = 1
    end
  end
  object DsImportSalCfgCab: TDataSource
    DataSet = QrImportSalCfgCab
    Left = 556
    Top = 268
  end
  object DsLct: TDataSource
    DataSet = TbLct
    Left = 136
    Top = 272
  end
  object QrEnti1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM entidades'
      'WHERE CPF=:P0'
      'ORDER BY Codigo')
    Left = 320
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnti1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrEnti2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM entidades'
      'WHERE Nome=:P0'
      'ORDER BY Codigo')
    Left = 348
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnti2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object TbLct: TmySQLTable
    Database = DModG.MyPID_DB
    AfterOpen = TbLctAfterOpen
    BeforeClose = TbLctBeforeClose
    OnCalcFields = TbLctCalcFields
    TableName = '_lct_'
    Left = 108
    Top = 272
    object TbLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TbLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object TbLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object TbLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object TbLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object TbLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object TbLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object TbLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000'
    end
    object TbLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TbLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object TbLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object TbLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object TbLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object TbLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object TbLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object TbLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object TbLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object TbLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object TbLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object TbLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object TbLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object TbLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object TbLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object TbLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object TbLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object TbLctUnidade: TIntegerField
      FieldName = 'Unidade'
    end
    object TbLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object TbLctNO_CONTA: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_CONTA'
      LookupDataSet = QrContas
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Genero'
      Size = 50
      Lookup = True
    end
    object TbLctMENSAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object TbLctAtivo: TIntegerField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object QrImportSalCfgCta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Nome NO_CTA, scc.* '
      'FROM importsalcfgcta scc'
      'LEFT JOIN contas cta ON cta.Codigo=scc.Genero '
      'WHERE scc.Codigo =:P0'
      'ORDER BY Referencia')
    Left = 528
    Top = 313
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImportSalCfgCtaNO_CTA: TWideStringField
      FieldName = 'NO_CTA'
      Size = 50
    end
    object QrImportSalCfgCtaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImportSalCfgCtaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrImportSalCfgCtaGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrImportSalCfgCtaReferencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 60
    end
    object QrImportSalCfgCtaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrImportSalCfgCtaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrImportSalCfgCtaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrImportSalCfgCtaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrImportSalCfgCtaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrImportSalCfgCtaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrImportSalCfgCtaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsImportSalCfgCta: TDataSource
    DataSet = QrImportSalCfgCta
    Left = 556
    Top = 313
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 108
    Top = 320
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
