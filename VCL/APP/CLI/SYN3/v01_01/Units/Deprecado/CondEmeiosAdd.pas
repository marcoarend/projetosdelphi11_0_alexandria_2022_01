unit CondEmeiosAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkDBGridDAC, Db,
  mySQLDbTables, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmCondEmeiosAdd = class(TForm)
    Panel1: TPanel;
    dmkDBGridDAC1: TdmkDBGridDAC;
    QrEmeios: TmySQLQuery;
    DsEmeios: TDataSource;
    QrEmeiosEntiCod: TIntegerField;
    QrEmeiosExtrCod: TIntegerField;
    QrEmeiosAtivo: TSmallintField;
    QrEmeiosEmeio: TWideStringField;
    QrEmeiosEntiNom: TWideStringField;
    QrEmeiosExtrNom: TWideStringField;
    QrEmeiosTipoStr: TWideStringField;
    QrEmeiosItem: TIntegerField;
    QrEmeiosExtrInt1: TIntegerField;
    QrEmeiosExtrInt2: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    BtOK: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaTodos(Status: Integer);
  public
    { Public declarations }
  end;

  var
  FmCondEmeiosAdd: TFmCondEmeiosAdd;

implementation

uses Module, UMySQLModule, UnFinanceiro, Cond, ModuleGeral, UnMyObjects;

{$R *.DFM}

procedure TFmCondEmeiosAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondEmeiosAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondEmeiosAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondEmeiosAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  //
  QrEmeios.Close;
  QrEmeios.Database := DModG.MyPID_DB;
  UmyMod.AbreQuery(QrEmeios, DModG.MyPID_DB, 'TFmCondEmeiosAdd.FormCreate()');
end;

procedure TFmCondEmeiosAdd.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmCondEmeiosAdd.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmCondEmeiosAdd.AtualizaTodos(Status: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE emeios SET Ativo=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Status;
  DModG.QrUpdPID1.ExecSQL;
  //
  UmyMod.AbreQuery(QrEmeios, DModG.MyPID_DB, 'TFmCondEmeiosAdd.AtualizaTodos()');
end;

procedure TFmCondEmeiosAdd.BtOKClick(Sender: TObject);
var
  Item: Integer;
begin
  QrEmeios.First;
  while not QrEmeios.Eof do
  begin
    if QrEmeiosAtivo.Value = 1 then
    begin
      Item := UMyMod.BuscaEmLivreY_Def('condemeios', 'Item', stIns, 0);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'condemeios', False, [
        'Codigo', 'Controle', 'Conta',
        'Pronome', 'Nome', 'Emeio'
      ], ['Item'], [
        QrEmeiosExtrInt1.Value, QrEmeiosExtrInt2.Value, QrEmeiosExtrCod.Value,
        '', QrEmeiosEntiNom.Value, QrEmeiosEmeio.Value
      ], [Item], True);
    end;  
    QrEmeios.Next;
  end;
  FmCond.LocCod(FmCond.QrCondCodigo.Value, FmCond.QrCondCodigo.Value);
  Close;
end;

end.
