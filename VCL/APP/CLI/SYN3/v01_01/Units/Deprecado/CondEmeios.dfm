object FmCondEmeios: TFmCondEmeios
  Left = 506
  Top = 223
  Caption = 'CAD-CONDO-010 :: Cadastro de E-mail de UH'
  ClientHeight = 412
  ClientWidth = 436
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 169
    Width = 436
    Height = 129
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 84
      Height = 13
      Caption = 'Pronome pessoal:'
    end
    object Label2: TLabel
      Left = 8
      Top = 48
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object Label3: TLabel
      Left = 8
      Top = 88
      Width = 31
      Height = 13
      Caption = 'E-mail:'
    end
    object Label4: TLabel
      Left = 108
      Top = 28
      Width = 188
      Height = 13
      Caption = 'Exemplo: Sr. ou Senhor ou Sra. ou Srta.'
    end
    object EdSaudacao: TdmkEdit
      Left = 8
      Top = 24
      Width = 89
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Pronome'
      UpdCampo = 'Pronome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 8
      Top = 64
      Width = 389
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdEmeio: TdmkEdit
      Left = 8
      Top = 104
      Width = 389
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Emeio'
      UpdCampo = 'EMeio'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 436
    Height = 121
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 1
    object Label5: TLabel
      Left = 8
      Top = 4
      Width = 60
      Height = 13
      Caption = 'Condom'#237'nio:'
    end
    object Label6: TLabel
      Left = 8
      Top = 44
      Width = 30
      Height = 13
      Caption = 'Bloco:'
    end
    object Label7: TLabel
      Left = 8
      Top = 84
      Width = 103
      Height = 13
      Caption = 'Unidade habitacional:'
    end
    object Label8: TLabel
      Left = 332
      Top = 84
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object dmkDBEdit1: TdmkDBEdit
      Left = 8
      Top = 20
      Width = 65
      Height = 21
      DataField = 'Codigo'
      DataSource = FmCond.DsCond
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object dmkDBEdit2: TdmkDBEdit
      Left = 8
      Top = 60
      Width = 65
      Height = 21
      DataField = 'Controle'
      DataSource = FmCond.DsCondBloco
      TabOrder = 1
      UpdCampo = 'Controle'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object dmkDBEdit3: TdmkDBEdit
      Left = 8
      Top = 100
      Width = 65
      Height = 21
      DataField = 'Conta'
      DataSource = FmCond.DsCondImov
      TabOrder = 2
      UpdCampo = 'Conta'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdit1: TDBEdit
      Left = 76
      Top = 20
      Width = 321
      Height = 21
      DataField = 'NCONDOM'
      DataSource = FmCond.DsCond
      TabOrder = 3
    end
    object DBEdit2: TDBEdit
      Left = 76
      Top = 60
      Width = 321
      Height = 21
      DataField = 'Descri'
      DataSource = FmCond.DsCondBloco
      TabOrder = 4
    end
    object DBEdit3: TDBEdit
      Left = 76
      Top = 100
      Width = 253
      Height = 21
      DataField = 'Unidade'
      DataSource = FmCond.DsCondImov
      TabOrder = 5
    end
    object dmkEdit1: TdmkEdit
      Left = 332
      Top = 100
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      QryCampo = 'Item'
      UpdCampo = 'Item'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 436
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 388
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 340
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 317
        Height = 32
        Caption = 'Cadastro de E-mail de UH'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 32
        Caption = 'Cadastro de E-mail de UH'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 317
        Height = 32
        Caption = 'Cadastro de E-mail de UH'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 298
    Width = 436
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 432
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 342
    Width = 436
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 290
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 288
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
