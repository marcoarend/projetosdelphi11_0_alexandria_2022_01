object FmCondLocEmail: TFmCondLocEmail
  Left = 339
  Top = 185
  Caption = 'CAD-CONDO-008 :: Localiza e-mails - Cond'#244'mino'
  ClientHeight = 492
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 357
        Height = 32
        Caption = 'Localiza e-mails - Cond'#244'mino'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 357
        Height = 32
        Caption = 'Localiza e-mails - Cond'#244'mino'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 357
        Height = 32
        Caption = 'Localiza e-mails - Cond'#244'mino'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 330
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 55
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label8: TLabel
            Left = 8
            Top = 8
            Width = 31
            Height = 13
            Caption = 'E-mail:'
          end
          object EdEMail: TdmkEdit
            Left = 8
            Top = 24
            Width = 509
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'EMail'
            UpdCampo = 'EMail'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object BtPesquisa: TBitBtn
            Tag = 22
            Left = 523
            Top = 9
            Width = 120
            Height = 40
            Caption = '&Pesquisa'
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtPesquisaClick
          end
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 2
          Top = 70
          Width = 1004
          Height = 258
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Pronome'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EMeio'
              Title.Caption = 'E-mail'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UH'
              Title.Caption = 'U.H.'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBLOCO'
              Title.Caption = 'Bloco'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECOND'
              Title.Caption = 'Condom'#237'nio'
              Width = 300
              Visible = True
            end>
          Color = clWindow
          DataSource = DsEmail
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Pronome'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EMeio'
              Title.Caption = 'E-mail'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UH'
              Title.Caption = 'U.H.'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBLOCO'
              Title.Caption = 'Bloco'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECOND'
              Title.Caption = 'Condom'#237'nio'
              Width = 300
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtLocaliza: TBitBtn
        Tag = 40
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Localiza'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtLocalizaClick
      end
    end
  end
  object QrEmail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECOND, '
      'blo.Descri NOMEBLOCO, imv.Unidade UH, eme.* '
      'FROM condemeios eme'
      'LEFT JOIN cond con ON con.Codigo = eme.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo  = con.Cliente'
      'LEFT JOIN condbloco blo ON blo.Controle = eme.Controle'
      'LEFT JOIN condimov imv ON imv.Conta = eme.Conta'
      'WHERE EMeio=:P0'
      'ORDER BY NOMECOND, NOMEBLOCO, UH, Nome')
    Left = 301
    Top = 265
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmailNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
    object QrEmailNOMEBLOCO: TWideStringField
      FieldName = 'NOMEBLOCO'
      Size = 100
    end
    object QrEmailUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrEmailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmailControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmailConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrEmailItem: TIntegerField
      FieldName = 'Item'
    end
    object QrEmailPronome: TWideStringField
      FieldName = 'Pronome'
    end
    object QrEmailNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEmailEMeio: TWideStringField
      FieldName = 'EMeio'
      Size = 100
    end
    object QrEmailLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmailDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmailDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmailUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmailUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmailAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmailAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEmail: TDataSource
    DataSet = QrEmail
    Left = 329
    Top = 265
  end
end
