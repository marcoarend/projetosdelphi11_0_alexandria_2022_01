object Fmbloqparc: TFmbloqparc
  Left = 344
  Top = 168
  Align = alClient
  Caption = 'BLQ-REPAR-001 :: Parcelamentos de Bloquetos'
  ClientHeight = 587
  ClientWidth = 791
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  ExplicitWidth = 640
  ExplicitHeight = 480
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 51
    Width = 791
    Height = 536
    ActivePage = TabSheet1
    Align = alClient
    TabHeight = 20
    TabOrder = 0
    ExplicitWidth = 993
    ExplicitHeight = 693
    object TabSheet1: TTabSheet
      Caption = ' Parcelamento selecionado'
      object PnVisual: TPanel
        Left = 0
        Top = 0
        Width = 783
        Height = 506
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        ExplicitWidth = 986
        ExplicitHeight = 665
        object PainelDados: TPanel
          Left = 0
          Top = 0
          Width = 783
          Height = 506
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          ExplicitWidth = 986
          ExplicitHeight = 665
          object PainelData: TPanel
            Left = 0
            Top = 0
            Width = 783
            Height = 162
            Align = alTop
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 0
            ExplicitWidth = 986
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 781
              Height = 162
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label5: TLabel
                Left = 524
                Top = 118
                Width = 73
                Height = 13
                Caption = 'ID de conex'#227'o:'
              end
              object Label17: TLabel
                Left = 445
                Top = 118
                Width = 70
                Height = 13
                Caption = 'Valor parcelas:'
                FocusControl = DBEdit8
              end
              object Label13: TLabel
                Left = 378
                Top = 118
                Width = 52
                Height = 13
                Caption = 'Valor juros:'
                FocusControl = DBEdit7
              end
              object Label18: TLabel
                Left = 283
                Top = 118
                Width = 93
                Height = 13
                Caption = 'Taxa juros (%/m'#234's):'
                FocusControl = DBEdit9
              end
              object Label12: TLabel
                Left = 217
                Top = 118
                Width = 55
                Height = 13
                Caption = 'Valor multa:'
                FocusControl = DBEdit6
              end
              object Label19: TLabel
                Left = 150
                Top = 118
                Width = 39
                Height = 13
                Caption = '% multa:'
                FocusControl = DBEdit10
              end
              object Label8: TLabel
                Left = 67
                Top = 118
                Width = 77
                Height = 13
                Caption = 'Valor parcelado:'
                FocusControl = DBEdit4
              end
              object Label7: TLabel
                Left = 8
                Top = 118
                Width = 54
                Height = 13
                Caption = 'Data Parc.:'
                FocusControl = DBEdit3
              end
              object Label6: TLabel
                Left = 8
                Top = 63
                Width = 148
                Height = 13
                Caption = 'Autentica'#231#227'o do parcelamento:'
                FocusControl = DBMemo1
              end
              object Label1: TLabel
                Left = 8
                Top = 24
                Width = 36
                Height = 13
                Caption = 'C'#243'digo:'
                FocusControl = DBEdCodigo
              end
              object Label4: TLabel
                Left = 59
                Top = 24
                Width = 68
                Height = 13
                Caption = 'Unidade (UH):'
                FocusControl = DBEdit2
              end
              object Label2: TLabel
                Left = 134
                Top = 24
                Width = 56
                Height = 13
                Caption = 'Propriet'#225'rio:'
                FocusControl = DBEdNome
              end
              object Label11: TLabel
                Left = 496
                Top = 24
                Width = 33
                Height = 13
                Caption = 'Status:'
                FocusControl = DBEdit5
              end
              object DBEdit1: TDBEdit
                Left = 524
                Top = 134
                Width = 249
                Height = 21
                DataField = 'loginID'
                DataSource = Dsbloqparc
                TabOrder = 0
              end
              object DBEdit8: TDBEdit
                Left = 445
                Top = 134
                Width = 76
                Height = 21
                DataField = 'VlrTotal'
                DataSource = Dsbloqparc
                TabOrder = 1
              end
              object DBEdit7: TDBEdit
                Left = 378
                Top = 134
                Width = 63
                Height = 21
                DataField = 'VlrJuros'
                DataSource = Dsbloqparc
                TabOrder = 2
              end
              object DBEdit9: TDBEdit
                Left = 283
                Top = 134
                Width = 92
                Height = 21
                DataField = 'TxaJur'
                DataSource = Dsbloqparc
                TabOrder = 3
              end
              object DBEdit6: TDBEdit
                Left = 217
                Top = 134
                Width = 63
                Height = 21
                DataField = 'VlrMulta'
                DataSource = Dsbloqparc
                TabOrder = 4
              end
              object DBEdit10: TDBEdit
                Left = 150
                Top = 134
                Width = 63
                Height = 21
                DataField = 'TxaMul'
                DataSource = Dsbloqparc
                TabOrder = 5
              end
              object DBEdit4: TDBEdit
                Left = 67
                Top = 134
                Width = 79
                Height = 21
                DataField = 'VlrOrigi'
                DataSource = Dsbloqparc
                TabOrder = 6
              end
              object DBEdit3: TDBEdit
                Left = 8
                Top = 134
                Width = 55
                Height = 21
                DataField = 'DataP'
                DataSource = Dsbloqparc
                TabOrder = 7
              end
              object DBMemo1: TDBMemo
                Left = 8
                Top = 78
                Width = 765
                Height = 37
                DataField = 'autentica'
                DataSource = Dsbloqparc
                TabOrder = 8
              end
              object DBEdCodigo: TDBEdit
                Left = 8
                Top = 39
                Width = 47
                Height = 21
                Hint = 'N'#186' do banco'
                TabStop = False
                DataField = 'Codigo'
                DataSource = Dsbloqparc
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 8281908
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 9
              end
              object DBEdit2: TDBEdit
                Left = 59
                Top = 39
                Width = 72
                Height = 21
                DataField = 'Unidade'
                DataSource = Dsbloqparc
                TabOrder = 10
              end
              object DBEdNome: TDBEdit
                Left = 134
                Top = 39
                Width = 360
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'NOMEPROP'
                DataSource = Dsbloqparc
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 11
              end
              object DBEdit5: TDBEdit
                Left = 496
                Top = 39
                Width = 277
                Height = 21
                DataField = 'STATUS'
                DataSource = Dsbloqparc
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 4227327
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 12
              end
              object DBEdit111: TDBEdit
                Left = 8
                Top = 2
                Width = 765
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'NOMECOND'
                DataSource = Dsbloqparc
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 13
              end
            end
            object RichEdit1: TRichEdit
              Left = 781
              Top = 0
              Width = 204
              Height = 162
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
          end
          object PainelGrid: TPanel
            Left = 0
            Top = 162
            Width = 783
            Height = 180
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitWidth = 986
            object PageControl2: TPageControl
              Left = 0
              Top = 0
              Width = 783
              Height = 180
              ActivePage = TabSheet2
              Align = alClient
              TabHeight = 20
              TabOrder = 0
              ExplicitWidth = 1233
              object TabSheet2: TTabSheet
                Caption = 'Parcelas'
                object Panel4: TPanel
                  Left = 0
                  Top = 0
                  Width = 336
                  Height = 150
                  Align = alLeft
                  ParentBackground = False
                  TabOrder = 0
                  ExplicitHeight = 153
                  object DBGbloqparcpar: TDBGrid
                    Left = 1
                    Top = 48
                    Width = 333
                    Height = 98
                    Align = alClient
                    DataSource = Dsbloqparcpar
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Parcela'
                        Width = 32
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Vencimento'
                        Title.Caption = 'Vencto'
                        Width = 45
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'FatNum'
                        Title.Caption = 'Bloqueto'
                        Width = 59
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ValBol'
                        Title.Caption = 'Valor'
                        Width = 58
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Lancto'
                        Title.Caption = 'Lan'#231'amento'
                        Width = 62
                        Visible = True
                      end>
                  end
                  object Panel6: TPanel
                    Left = 1
                    Top = 1
                    Width = 333
                    Height = 47
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 1
                    object BtEdita: TBitBtn
                      Tag = 11
                      Left = 4
                      Top = 4
                      Width = 89
                      Height = 39
                      Cursor = crHandPoint
                      Caption = '&Altera'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtEditaClick
                    end
                    object CkZerado: TCheckBox
                      Left = 101
                      Top = 4
                      Width = 95
                      Height = 17
                      Caption = 'Sem val/vcto.'
                      TabOrder = 1
                      Visible = False
                    end
                    object CkAntigo: TCheckBox
                      Left = 101
                      Top = 22
                      Width = 95
                      Height = 16
                      Caption = 'Bloq deste form.'
                      Checked = True
                      State = cbChecked
                      TabOrder = 2
                      Visible = False
                    end
                    object CkDesign: TCheckBox
                      Left = 199
                      Top = 22
                      Width = 95
                      Height = 16
                      Caption = 'Design'
                      TabOrder = 3
                      Visible = False
                    end
                  end
                end
                object Panel9: TPanel
                  Left = 336
                  Top = 0
                  Width = 439
                  Height = 150
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  ExplicitWidth = 644
                  ExplicitHeight = 153
                  object PageControl4: TPageControl
                    Left = 0
                    Top = 0
                    Width = 439
                    Height = 150
                    ActivePage = TabSheet3
                    Align = alClient
                    TabHeight = 20
                    TabOrder = 0
                    ExplicitWidth = 889
                    object TabSheet3: TTabSheet
                      Caption = 'Parcelas originais'
                      object DBGrid2: TDBGrid
                        Left = 0
                        Top = 47
                        Width = 633
                        Height = 65
                        Align = alClient
                        DataSource = Dsbloqparcits
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Seq'
                            Width = 23
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Tipo'
                            Title.Caption = 'T'
                            Width = 10
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'PercIni'
                            Title.Caption = '% Ini'
                            Width = 32
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'PercFim'
                            Title.Caption = '% Fim'
                            Width = 32
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VlrOrigi'
                            Title.Caption = '$ original'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VlrMulta'
                            Title.Caption = '$ multa'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VlrJuros'
                            Title.Caption = '$ juros'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VlrSomas'
                            Title.Caption = '$ somas'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VlrAjust'
                            Title.Caption = '$ ajuste 1'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VlrJuro2'
                            Title.Caption = '$ ajuste 2'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VlrMult2'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VlrTotal'
                            Title.Caption = '$ total'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VlrLanct'
                            Title.Caption = '$ pago'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'BloqOrigi'
                            Title.Caption = 'Bloq. origi.'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Controle'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'SParcA'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'SParcD'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Dias'
                            Width = 38
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'CtrlOrigi'
                            Title.Caption = 'Lan'#231'to'
                            Width = 59
                            Visible = True
                          end>
                      end
                      object Panel15: TPanel
                        Left = 0
                        Top = 0
                        Width = 633
                        Height = 47
                        Align = alTop
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 1
                        object BtReconfReparc: TBitBtn
                          Tag = 10117
                          Left = 4
                          Top = 4
                          Width = 190
                          Height = 39
                          Cursor = crHandPoint
                          Caption = '&Reconfirma reparcelamento'
                          Enabled = False
                          NumGlyphs = 2
                          ParentShowHint = False
                          ShowHint = True
                          TabOrder = 0
                          OnClick = BtReconfReparcClick
                        end
                      end
                    end
                    object TabSheet9: TTabSheet
                      Caption = 'Ratifica'#231#227'o de pagamentos'
                      ImageIndex = 2
                      object DBGrid4: TDBGrid
                        Left = 0
                        Top = 47
                        Width = 633
                        Height = 65
                        Align = alClient
                        DataSource = DsIncorr
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Controle'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Data'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Descricao'
                            Title.Caption = 'Descri'#231#227'o'
                            Width = 394
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Credito'
                            Title.Caption = 'Cr'#233'dito'
                            Width = 59
                            Visible = True
                          end>
                      end
                      object Panel10: TPanel
                        Left = 0
                        Top = 0
                        Width = 633
                        Height = 47
                        Align = alTop
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 1
                        object BtRatificaQuit: TBitBtn
                          Tag = 10116
                          Left = 2
                          Top = 2
                          Width = 147
                          Height = 40
                          Caption = '&Ratifica quita'#231#227'o'
                          Enabled = False
                          TabOrder = 0
                          OnClick = BtRatificaQuitClick
                        end
                      end
                    end
                    object TabSheet4: TTabSheet
                      Caption = 'Pagamentos ratificados'
                      ImageIndex = 1
                      object Panel11: TPanel
                        Left = 0
                        Top = 0
                        Width = 111
                        Height = 112
                        Align = alLeft
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        object GroupBox1: TGroupBox
                          Left = 0
                          Top = 48
                          Width = 111
                          Height = 64
                          Align = alClient
                          Caption = ' Totais pagto: '
                          TabOrder = 0
                          object Label31: TLabel
                            Left = 4
                            Top = 16
                            Width = 66
                            Height = 13
                            Caption = 'Pago parcela:'
                            FocusControl = DBEdit11
                          end
                          object Label32: TLabel
                            Left = 4
                            Top = 55
                            Width = 67
                            Height = 13
                            Caption = 'Multa parcela:'
                            FocusControl = DBEdit12
                          end
                          object Label33: TLabel
                            Left = 4
                            Top = 94
                            Width = 66
                            Height = 13
                            Caption = 'Juros parcela:'
                            FocusControl = DBEdit13
                          end
                          object DBEdit11: TDBEdit
                            Left = 4
                            Top = 31
                            Width = 95
                            Height = 21
                            DataField = 'Credito'
                            DataSource = DsBPP_ToP
                            TabOrder = 0
                          end
                          object DBEdit12: TDBEdit
                            Left = 4
                            Top = 71
                            Width = 95
                            Height = 21
                            DataField = 'MultaVal'
                            DataSource = DsBPP_ToP
                            TabOrder = 1
                          end
                          object DBEdit13: TDBEdit
                            Left = 4
                            Top = 110
                            Width = 71
                            Height = 21
                            DataField = 'MoraVal'
                            DataSource = DsBPP_ToP
                            TabOrder = 2
                          end
                        end
                        object PnRecorrige: TPanel
                          Left = 0
                          Top = 0
                          Width = 111
                          Height = 48
                          Align = alTop
                          BevelOuter = bvNone
                          TabOrder = 1
                          object BtReCorrige: TBitBtn
                            Tag = 10114
                            Left = 3
                            Top = 4
                            Width = 104
                            Height = 39
                            Cursor = crHandPoint
                            Caption = 'Recorr&ige'
                            Enabled = False
                            NumGlyphs = 2
                            ParentShowHint = False
                            ShowHint = True
                            TabOrder = 0
                            OnClick = BtReCorrigeClick
                          end
                        end
                      end
                      object Panel13: TPanel
                        Left = 111
                        Top = 0
                        Width = 522
                        Height = 112
                        Align = alClient
                        BevelOuter = bvNone
                        TabOrder = 1
                        object DBGrid3: TDBGrid
                          Left = 0
                          Top = 44
                          Width = 522
                          Height = 68
                          Align = alClient
                          DataSource = DsBPP_Pgt
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'Controle'
                              Title.Caption = 'Lan'#231'to'
                              Width = 45
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Credito'
                              Title.Caption = 'Cr'#233'dito'
                              Width = 59
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Data'
                              Width = 45
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NOMECONTA'
                              Title.Caption = 'Conta'
                              Width = 160
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Descricao'
                              Title.Caption = 'Descri'#231#227'o'
                              Width = 154
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'MultaVal'
                              Title.Caption = '$ multa'
                              Width = 59
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'MoraVal'
                              Title.Caption = '$ juros'
                              Width = 59
                              Visible = True
                            end>
                        end
                        object PnAtz1: TPanel
                          Left = 0
                          Top = 0
                          Width = 522
                          Height = 44
                          Align = alTop
                          BevelOuter = bvNone
                          ParentBackground = False
                          TabOrder = 1
                          Visible = False
                          object LaAtz1: TLabel
                            Left = 24
                            Top = 4
                            Width = 154
                            Height = 13
                            Caption = 'Aguarde...   Atualizando....'
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clRed
                            Font.Height = -12
                            Font.Name = 'MS Sans Serif'
                            Font.Style = [fsBold]
                            ParentFont = False
                          end
                          object PBAtz1: TProgressBar
                            Left = 24
                            Top = 20
                            Width = 410
                            Height = 17
                            TabOrder = 0
                          end
                        end
                      end
                    end
                    object TabSheet10: TTabSheet
                      Caption = ' Resultado da recorre'#231#227'o '
                      ImageIndex = 3
                      object DBGrid5: TDBGrid
                        Left = 0
                        Top = 52
                        Width = 633
                        Height = 60
                        Align = alClient
                        DataSource = DataSource1
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Ordem'
                            Width = 29
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Origi'
                            Title.Caption = 'Original'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Dias'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Fator'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'ValDif'
                            Title.Caption = 'Diferen'#231'a'
                            Width = 80
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'ValItem'
                            Title.Caption = 'Valor item'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'RestaDif'
                            Width = 59
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'RestaFat'
                            Width = 59
                            Visible = True
                          end>
                      end
                      object Panel14: TPanel
                        Left = 0
                        Top = 0
                        Width = 633
                        Height = 52
                        Align = alTop
                        BevelOuter = bvNone
                        TabOrder = 1
                        object DBGrid6: TDBGrid
                          Left = 0
                          Top = 0
                          Width = 633
                          Height = 52
                          Align = alClient
                          DataSource = DataSource2
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                        end
                      end
                    end
                  end
                end
              end
              object TabSheet5: TTabSheet
                Caption = 'Bloquetos originais'
                ImageIndex = 1
                object dmkDBGrid1: TdmkDBGrid
                  Left = 0
                  Top = 0
                  Width = 977
                  Height = 146
                  Align = alClient
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Data'
                      Width = 59
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'BLOQUETO'
                      Width = 62
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Credito'
                      Title.Caption = 'Cr'#233'dito'
                      Width = 59
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Vencimento'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Descricao'
                      Title.Caption = 'Descri'#231#227'o'
                      Width = 336
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Width = 59
                      Visible = True
                    end>
                  Color = clWindow
                  DataSource = Dslct
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Data'
                      Width = 59
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'BLOQUETO'
                      Width = 62
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Credito'
                      Title.Caption = 'Cr'#233'dito'
                      Width = 59
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Vencimento'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Descricao'
                      Title.Caption = 'Descri'#231#227'o'
                      Width = 336
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Width = 59
                      Visible = True
                    end>
                end
              end
            end
          end
          object PnCalc: TPanel
            Left = 0
            Top = 342
            Width = 783
            Height = 156
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            Visible = False
            ExplicitWidth = 986
            object PageControl3: TPageControl
              Left = 0
              Top = 0
              Width = 783
              Height = 156
              ActivePage = TabSheet7
              Align = alClient
              TabHeight = 20
              TabOrder = 0
              ExplicitWidth = 1233
              object TabSheet7: TTabSheet
                Caption = 'Log de quebra de lan'#231'amentos'
                object MeLog: TMemo
                  Left = 0
                  Top = 0
                  Width = 977
                  Height = 120
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                end
              end
              object TabSheet8: TTabSheet
                Caption = 'C'#225'lculo de juros (contra prova)'
                ImageIndex = 1
                object PnQuebra: TPanel
                  Left = 0
                  Top = 0
                  Width = 775
                  Height = 52
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  Visible = False
                  ExplicitWidth = 980
                  object Label23: TLabel
                    Left = 8
                    Top = 4
                    Width = 19
                    Height = 13
                    Caption = 'At'#233':'
                  end
                  object Label24: TLabel
                    Left = 166
                    Top = 4
                    Width = 38
                    Height = 13
                    Caption = 'Original:'
                  end
                  object Label25: TLabel
                    Left = 248
                    Top = 4
                    Width = 28
                    Height = 13
                    Caption = 'Juros:'
                  end
                  object Label26: TLabel
                    Left = 331
                    Top = 4
                    Width = 29
                    Height = 13
                    Caption = 'Multa:'
                  end
                  object Label27: TLabel
                    Left = 414
                    Top = 4
                    Width = 27
                    Height = 13
                    Caption = 'Total:'
                  end
                  object Label28: TLabel
                    Left = 496
                    Top = 4
                    Width = 39
                    Height = 13
                    Caption = 'Parcela:'
                  end
                  object Label29: TLabel
                    Left = 737
                    Top = 4
                    Width = 105
                    Height = 13
                    Caption = 'Soma (Dias * Original):'
                  end
                  object Label30: TLabel
                    Left = 850
                    Top = 4
                    Width = 93
                    Height = 13
                    Caption = 'M'#233'dia MUL / Valor:'
                  end
                  object Edit4: TEdit
                    Left = 8
                    Top = 20
                    Width = 154
                    Height = 21
                    TabOrder = 0
                  end
                  object dmkEdit1: TdmkEdit
                    Left = 166
                    Top = 20
                    Width = 78
                    Height = 20
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit2: TdmkEdit
                    Left = 248
                    Top = 20
                    Width = 79
                    Height = 20
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit3: TdmkEdit
                    Left = 331
                    Top = 20
                    Width = 79
                    Height = 20
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit4: TdmkEdit
                    Left = 414
                    Top = 20
                    Width = 78
                    Height = 20
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit5: TdmkEdit
                    Left = 496
                    Top = 20
                    Width = 79
                    Height = 20
                    Alignment = taRightJustify
                    TabOrder = 5
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object CkManual: TCheckBox
                    Left = 579
                    Top = 4
                    Width = 68
                    Height = 17
                    Caption = 'Manual:'
                    TabOrder = 6
                  end
                  object dmkEdit6: TdmkEdit
                    Left = 579
                    Top = 20
                    Width = 79
                    Height = 20
                    Alignment = taRightJustify
                    TabOrder = 7
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit10: TdmkEdit
                    Left = 737
                    Top = 20
                    Width = 109
                    Height = 20
                    Alignment = taRightJustify
                    TabOrder = 8
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 4
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,0000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit11: TdmkEdit
                    Left = 850
                    Top = 20
                    Width = 92
                    Height = 20
                    Alignment = taRightJustify
                    TabOrder = 9
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
                object PageControl5: TPageControl
                  Left = 0
                  Top = 52
                  Width = 775
                  Height = 74
                  ActivePage = TabSheet6
                  Align = alClient
                  TabHeight = 20
                  TabOrder = 1
                  ExplicitWidth = 1225
                  object TabSheet6: TTabSheet
                    Caption = ' A '
                    object Splitter1: TSplitter
                      Left = 859
                      Top = 0
                      Height = 34
                    end
                    object GradeA: TStringGrid
                      Left = 0
                      Top = 0
                      Width = 859
                      Height = 34
                      Align = alLeft
                      DefaultColWidth = 51
                      DefaultRowHeight = 14
                      RowCount = 2
                      TabOrder = 0
                      OnDrawCell = GradeADrawCell
                    end
                    object Panel8: TPanel
                      Left = 862
                      Top = 0
                      Width = 107
                      Height = 34
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 1
                      object Label20: TLabel
                        Left = 8
                        Top = 4
                        Width = 85
                        Height = 13
                        Caption = 'Total Dias * Valor:'
                      end
                      object Label21: TLabel
                        Left = 8
                        Top = 43
                        Width = 93
                        Height = 13
                        Caption = 'M'#233'dia MUL / Valor:'
                      end
                      object Label22: TLabel
                        Left = 8
                        Top = 82
                        Width = 90
                        Height = 13
                        Caption = 'M'#233'dia MUL / Dias:'
                      end
                      object dmkEdit7: TdmkEdit
                        Left = 8
                        Top = 20
                        Width = 118
                        Height = 20
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfDouble
                        MskType = fmtNone
                        DecimalSize = 2
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0,00'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0.000000000000000000
                        ValWarn = False
                      end
                      object dmkEdit8: TdmkEdit
                        Left = 8
                        Top = 59
                        Width = 118
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfDouble
                        MskType = fmtNone
                        DecimalSize = 2
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0,00'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0.000000000000000000
                        ValWarn = False
                      end
                      object dmkEdit9: TdmkEdit
                        Left = 8
                        Top = 98
                        Width = 118
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 2
                        FormatType = dmktfDouble
                        MskType = fmtNone
                        DecimalSize = 2
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0,00'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0.000000000000000000
                        ValWarn = False
                      end
                    end
                  end
                  object TabSheet11: TTabSheet
                    Caption = ' B '
                    ImageIndex = 1
                    object GradeB: TStringGrid
                      Left = 0
                      Top = 0
                      Width = 969
                      Height = 34
                      Align = alClient
                      DefaultColWidth = 51
                      DefaultRowHeight = 14
                      RowCount = 2
                      TabOrder = 0
                      OnDrawCell = GradeBDrawCell
                    end
                  end
                end
              end
            end
          end
          object GBCntrl: TGroupBox
            Left = 0
            Top = 443
            Width = 783
            Height = 63
            Align = alBottom
            TabOrder = 3
            ExplicitTop = 602
            ExplicitWidth = 986
            object Panel5: TPanel
              Left = 2
              Top = 14
              Width = 169
              Height = 47
              Align = alLeft
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 1
              object SpeedButton4: TBitBtn
                Tag = 4
                Left = 126
                Top = 4
                Width = 40
                Height = 39
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = SpeedButton4Click
              end
              object SpeedButton3: TBitBtn
                Tag = 3
                Left = 86
                Top = 4
                Width = 40
                Height = 39
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = SpeedButton3Click
              end
              object SpeedButton2: TBitBtn
                Tag = 2
                Left = 47
                Top = 4
                Width = 39
                Height = 39
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = SpeedButton2Click
              end
              object SpeedButton1: TBitBtn
                Tag = 1
                Left = 8
                Top = 4
                Width = 39
                Height = 39
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
                OnClick = SpeedButton1Click
              end
            end
            object LaRegistro: TStaticText
              Left = 171
              Top = 14
              Width = 30
              Height = 17
              Align = alClient
              BevelInner = bvLowered
              BevelKind = bkFlat
              Caption = '[N]: 0'
              TabOrder = 2
            end
            object Panel3: TPanel
              Left = 388
              Top = 14
              Width = 594
              Height = 47
              Align = alRight
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object Panel2: TPanel
                Left = 464
                Top = 0
                Width = 130
                Height = 46
                Align = alRight
                Alignment = taRightJustify
                BevelOuter = bvNone
                TabOrder = 0
                object BtSaida: TBitBtn
                  Tag = 13
                  Left = 4
                  Top = 4
                  Width = 118
                  Height = 39
                  Cursor = crHandPoint
                  Caption = '&Sa'#237'da'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaidaClick
                end
              end
              object BtRatificaParc: TBitBtn
                Tag = 10115
                Left = 4
                Top = 4
                Width = 148
                Height = 39
                Cursor = crHandPoint
                Caption = '&Ratifica parcelamento'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                Visible = False
                OnClick = BtRatificaParcClick
              end
              object BtExclui: TBitBtn
                Tag = 12
                Left = 158
                Top = 4
                Width = 148
                Height = 39
                Cursor = crHandPoint
                Caption = '&Exclui parcelamento'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtExcluiClick
              end
            end
          end
          object GBAvisos1: TGroupBox
            Left = 0
            Top = 388
            Width = 783
            Height = 55
            Align = alBottom
            Caption = ' Avisos: '
            TabOrder = 4
            ExplicitTop = 547
            ExplicitWidth = 986
            object Panel17: TPanel
              Left = 2
              Top = 14
              Width = 980
              Height = 39
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object LaAviso1: TLabel
                Left = 13
                Top = 2
                Width = 12
                Height = 16
                Caption = '...'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clSilver
                Font.Height = -14
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object LaAviso2: TLabel
                Left = 12
                Top = 1
                Width = 12
                Height = 16
                Caption = '...'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -14
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object PBAtz: TProgressBar
                Left = 0
                Top = 22
                Width = 981
                Height = 16
                Align = alBottom
                TabOrder = 0
              end
            end
          end
        end
      end
    end
    object TabSheet12: TTabSheet
      Caption = ' Pesquisa reparcelamento '
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 783
        Height = 506
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 986
        ExplicitHeight = 665
        object Panel16: TPanel
          Left = 0
          Top = 0
          Width = 783
          Height = 166
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 986
          object Label3: TLabel
            Left = 8
            Top = 4
            Width = 60
            Height = 13
            Caption = 'Condom'#237'nio:'
          end
          object Label9: TLabel
            Left = 8
            Top = 43
            Width = 140
            Height = 13
            Caption = 'Propriet'#225'rio [F4 mostra todos]:'
          end
          object Label10: TLabel
            Left = 386
            Top = 43
            Width = 103
            Height = 13
            Caption = 'Unidade habitacional:'
          end
          object Label14: TLabel
            Left = 8
            Top = 82
            Width = 91
            Height = 13
            Caption = 'N'#186' reparcelamento:'
          end
          object Label15: TLabel
            Left = 106
            Top = 82
            Width = 59
            Height = 13
            Caption = 'N'#186' bloqueto:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 20
            Width = 43
            Height = 20
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdPropriet: TdmkEditCB
            Left = 8
            Top = 59
            Width = 43
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdProprietChange
            OnKeyDown = EdProprietKeyDown
            DBLookupComboBox = CBPropriet
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBPropriet: TdmkDBLookupComboBox
            Left = 55
            Top = 59
            Width = 328
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEPROP'
            ListSource = DsPropriet
            TabOrder = 3
            dmkEditCB = EdPropriet
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 55
            Top = 20
            Width = 471
            Height = 21
            Color = clWhite
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCondImov: TdmkEditCB
            Left = 386
            Top = 59
            Width = 44
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCondImovChange
            DBLookupComboBox = CBCondImov
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCondImov: TdmkDBLookupComboBox
            Left = 434
            Top = 59
            Width = 92
            Height = 21
            Color = clWhite
            KeyField = 'Conta'
            ListField = 'Unidade'
            ListSource = DsCondImov
            TabOrder = 5
            dmkEditCB = EdCondImov
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdReparcel: TdmkEdit
            Left = 8
            Top = 98
            Width = 95
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdReparcelChange
          end
          object EdFatNum: TdmkEdit
            Left = 106
            Top = 98
            Width = 96
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFatNumChange
          end
          object BtReabre: TBitBtn
            Tag = 18
            Left = 276
            Top = 122
            Width = 170
            Height = 40
            Caption = 'Reabre pesquisa'
            NumGlyphs = 2
            TabOrder = 11
            OnClick = BtReabreClick
          end
          object CkSoAbertos: TCheckBox
            Left = 209
            Top = 102
            Width = 317
            Height = 17
            Caption = 'Mostrar somente parcelas em aberto (pode mais demorado).'
            TabOrder = 8
          end
          object GBVencimento: TGroupBox
            Left = 686
            Top = 14
            Width = 142
            Height = 105
            Caption = ' Per'#237'odo de vencimento: '
            TabOrder = 10
            object CkIniVct: TCheckBox
              Left = 12
              Top = 14
              Width = 110
              Height = 16
              Caption = 'Data inicial:'
              TabOrder = 0
              OnClick = CkIniParClick
            end
            object TPIniVct: TDateTimePicker
              Left = 12
              Top = 31
              Width = 110
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.000000000000000000
              Time = 0.777157974502188200
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              OnClick = CkIniParClick
            end
            object CkFimVct: TCheckBox
              Left = 12
              Top = 57
              Width = 110
              Height = 17
              Caption = 'Data final:'
              TabOrder = 2
              OnClick = CkIniParClick
            end
            object TPFimVct: TDateTimePicker
              Left = 12
              Top = 75
              Width = 110
              Height = 21
              Date = 37636.000000000000000000
              Time = 0.777203761601413100
              TabOrder = 3
              OnClick = CkIniParClick
            end
          end
          object GBParcelamento: TGroupBox
            Left = 537
            Top = 14
            Width = 141
            Height = 105
            Caption = ' Per'#237'odo de parcelamento: '
            TabOrder = 9
            object CkIniPar: TCheckBox
              Left = 12
              Top = 14
              Width = 110
              Height = 16
              Caption = 'Data inicial:'
              TabOrder = 0
              OnClick = CkIniParClick
            end
            object TPIniPar: TDateTimePicker
              Left = 12
              Top = 31
              Width = 110
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.000000000000000000
              Time = 0.777157974502188200
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              OnClick = CkIniParClick
            end
            object CkFimPar: TCheckBox
              Left = 12
              Top = 57
              Width = 110
              Height = 17
              Caption = 'Data final:'
              TabOrder = 2
              OnClick = CkIniParClick
            end
            object TPFimPar: TDateTimePicker
              Left = 12
              Top = 75
              Width = 110
              Height = 21
              Date = 37636.000000000000000000
              Time = 0.777203761601413100
              TabOrder = 3
              OnClick = CkIniParClick
            end
          end
        end
        object PnPesq1: TPanel
          Left = 0
          Top = 166
          Width = 783
          Height = 340
          Align = alClient
          BevelOuter = bvLowered
          TabOrder = 1
          Visible = False
          ExplicitWidth = 986
          ExplicitHeight = 499
          object DBGPesq_Dados: TDBGrid
            Left = 1
            Top = 37
            Width = 983
            Height = 455
            Align = alClient
            BorderStyle = bsNone
            DataSource = DsPesq
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGPesq_DadosDblClick
            OnMouseUp = DBGPesq_DadosMouseUp
            Columns = <
              item
                Expanded = False
                FieldName = 'CodCliEnt'
                Title.Caption = 'Entidade'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodigoEsp'
                Title.Caption = 'C'#243'digo'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_EMP'
                Title.Caption = 'Nome'
                Width = 154
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodCliEsp'
                Title.Caption = 'C'#243'digo'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Unidade'
                Title.Caption = 'UH'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodigoEnt'
                Title.Caption = 'Entidade'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PES'
                Title.Caption = 'Propriet'#225'rio'
                Width = 154
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataP'
                Title.Caption = 'Data'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Parcela'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatNum'
                Title.Caption = 'Bloqueto'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'BPP_VlrTotal'
                Title.Caption = 'Valor'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'BPP_VlrDesco'
                Title.Caption = 'Desconto'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Lancto'
                Title.Caption = 'Lan'#231'amento'
                Width = 62
                Visible = True
              end>
          end
          object DBGPesq_Titul: TDBGrid
            Left = 1
            Top = 1
            Width = 983
            Height = 36
            Align = alTop
            BorderStyle = bsNone
            Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                Title.Alignment = taCenter
                Title.Caption = 'Condom'#237'nio'
                Width = 258
                Visible = True
              end
              item
                Expanded = False
                Title.Alignment = taCenter
                Title.Caption = 'Unidade habitacional'
                Width = 116
                Visible = True
              end
              item
                Expanded = False
                Title.Alignment = taCenter
                Title.Caption = 'Propriet'#225'rio'
                Width = 206
                Visible = True
              end
              item
                Expanded = False
                Title.Alignment = taCenter
                Title.Caption = 'Reparcelamento'
                Width = 87
                Visible = True
              end
              item
                Expanded = False
                Title.Alignment = taCenter
                Title.Caption = 'Parcela'
                Width = 333
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 791
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 993
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 51
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 213
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 213
      Top = 0
      Width = 733
      Height = 51
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 328
        Height = 31
        Caption = 'Parcelamentos de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 328
        Height = 31
        Caption = 'Parcelamentos de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 328
        Height = 31
        Caption = 'Parcelamentos de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Dsbloqparc: TDataSource
    DataSet = Qrbloqparc
    Left = 920
    Top = 21
  end
  object Qrbloqparc: TMySQLQuery
    Database = DModG.RV_CEP_DB
    BeforeOpen = QrbloqparcBeforeOpen
    AfterOpen = QrbloqparcAfterOpen
    BeforeClose = QrbloqparcBeforeClose
    AfterScroll = QrbloqparcAfterScroll
    OnCalcFields = QrbloqparcCalcFields
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECOND,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP,'
      'cim.Unidade, blp.* '
      'FROM bloqparc blp'
      'LEFT JOIN condimov cim ON cim.Conta=blp.CodigoEsp'
      'LEFT JOIN entidades ent ON ent.Codigo=blp.CodigoEnt'
      'LEFT JOIN entidades cli ON cli.Codigo=blp.CodCliEnt'
      'WHERE blp.Codigo>0')
    Left = 892
    Top = 21
    object QrbloqparcNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrbloqparcUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Size = 10
    end
    object QrbloqparcCodigo: TAutoIncField
      FieldName = 'Codigo'
      Origin = 'bloqparc.Codigo'
    end
    object QrbloqparcVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      Origin = 'bloqparc.VlrOrigi'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrbloqparcVlrMulta: TFloatField
      FieldName = 'VlrMulta'
      Origin = 'bloqparc.VlrMulta'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrbloqparcVlrJuros: TFloatField
      FieldName = 'VlrJuros'
      Origin = 'bloqparc.VlrJuros'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrbloqparcVlrTotal: TFloatField
      FieldName = 'VlrTotal'
      Origin = 'bloqparc.VlrTotal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrbloqparcDataP: TDateTimeField
      FieldName = 'DataP'
      Origin = 'bloqparc.DataP'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrbloqparcLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'bloqparc.Lk'
    end
    object QrbloqparcDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'bloqparc.DataCad'
    end
    object QrbloqparcDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'bloqparc.DataAlt'
    end
    object QrbloqparcUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'bloqparc.UserCad'
    end
    object QrbloqparcUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'bloqparc.UserAlt'
    end
    object QrbloqparcAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'bloqparc.AlterWeb'
      Required = True
    end
    object QrbloqparcloginID: TWideStringField
      FieldName = 'loginID'
      Origin = 'bloqparc.loginID'
      Required = True
      Size = 32
    end
    object Qrbloqparcautentica: TWideMemoField
      FieldName = 'autentica'
      Origin = 'bloqparc.autentica'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrbloqparcNovo: TSmallintField
      FieldName = 'Novo'
      Origin = 'bloqparc.Novo'
      Required = True
    end
    object QrbloqparcCodCliEsp: TIntegerField
      FieldName = 'CodCliEsp'
      Origin = 'bloqparc.CodCliEsp'
      Required = True
    end
    object QrbloqparcCodCliEnt: TIntegerField
      FieldName = 'CodCliEnt'
      Origin = 'bloqparc.CodCliEnt'
      Required = True
    end
    object QrbloqparcCodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
      Origin = 'bloqparc.CodigoEnt'
      Required = True
    end
    object QrbloqparcCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
      Origin = 'bloqparc.CodigoEsp'
      Required = True
    end
    object QrbloqparcTxaJur: TFloatField
      FieldName = 'TxaJur'
      Origin = 'bloqparc.TxaJur'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrbloqparcTxaMul: TFloatField
      FieldName = 'TxaMul'
      Origin = 'bloqparc.TxaMul'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrbloqparcSTATUS: TWideStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'STATUS'
      Size = 50
      Calculated = True
    end
    object QrbloqparcNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
    object QrbloqparcCond: TIntegerField
      FieldName = 'Cond'
    end
    object QrbloqparcCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
  end
  object Qrlct: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrlctCalcFields
    Left = 264
    Top = 12
    object QrlctData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrlctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrlctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrlctVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrlctMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrlctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrlctMES_TXT: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MES_TXT'
      Size = 7
      Calculated = True
    end
    object QrlctSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrlctReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrlctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrlctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrlctBLOQUETO: TFloatField
      FieldName = 'BLOQUETO'
    end
  end
  object Dslct: TDataSource
    DataSet = Qrlct
    Left = 292
    Top = 12
  end
  object Qrbloqparcpar: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterOpen = QrbloqparcparAfterOpen
    BeforeClose = QrbloqparcparBeforeClose
    AfterScroll = QrbloqparcparAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM bloqparcpar'
      'WHERE Codigo=:P0'
      'ORDER BY Vencimento')
    Left = 892
    Top = 49
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrbloqparcparCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrbloqparcparControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrbloqparcparParcela: TIntegerField
      FieldName = 'Parcela'
    end
    object QrbloqparcparVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      DisplayFormat = '#,###,##0.00'
    end
    object QrbloqparcparVlrMulta: TFloatField
      FieldName = 'VlrMulta'
      DisplayFormat = '#,###,##0.00'
    end
    object QrbloqparcparVlrJuros: TFloatField
      FieldName = 'VlrJuros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrbloqparcparLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrbloqparcparDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrbloqparcparDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrbloqparcparUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrbloqparcparUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrbloqparcparAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrbloqparcparValBol: TFloatField
      FieldName = 'ValBol'
      DisplayFormat = '#,###,##0.00'
    end
    object QrbloqparcparVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrbloqparcparFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrbloqparcparAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrbloqparcparVlrTotal: TFloatField
      FieldName = 'VlrTotal'
    end
    object QrbloqparcparLancto: TIntegerField
      FieldName = 'Lancto'
    end
  end
  object Dsbloqparcpar: TDataSource
    DataSet = Qrbloqparcpar
    Left = 920
    Top = 49
  end
  object QrPesqlan: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrlctCalcFields
    Left = 320
    Top = 12
    object QrPesqlanData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPesqlanControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPesqlanDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqlanVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPesqlanMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPesqlanCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPesqlanSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrPesqlanReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrPesqlanMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrPesqlanMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPesqlanBLOQUETO: TFloatField
      FieldName = 'BLOQUETO'
    end
  end
  object frxDsBloqParcPar: TfrxDBDataset
    UserName = 'frxDsBloqParcPar'
    CloseDataSource = False
    DataSet = Qrbloqparcpar
    BCDToCurrency = False
    
    Left = 948
    Top = 49
  end
  object frxDsLcto: TfrxDBDataset
    UserName = 'frxDsLcto'
    CloseDataSource = False
    DataSet = Qrlct
    BCDToCurrency = False
    
    Left = 236
    Top = 12
  end
  object frxDsBloqParc: TfrxDBDataset
    UserName = 'frxDsBloqParc'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEPROP=NOMEPROP'
      'Unidade=Unidade'
      'Codigo=Codigo'
      'VlrOrigi=VlrOrigi'
      'VlrMulta=VlrMulta'
      'VlrJuros=VlrJuros'
      'VlrTotal=VlrTotal'
      'DataP=DataP'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'loginID=loginID'
      'autentica=autentica'
      'Novo=Novo'
      'CodCliEsp=CodCliEsp'
      'CodCliEnt=CodCliEnt'
      'CodigoEnt=CodigoEnt'
      'CodigoEsp=CodigoEsp'
      'TxaJur=TxaJur'
      'TxaMul=TxaMul'
      'STATUS=STATUS'
      'NOMECOND=NOMECOND'
      'Cond=Cond'
      'CNAB_Cfg=CNAB_Cfg')
    DataSet = Qrbloqparc
    BCDToCurrency = False
    
    Left = 948
    Top = 21
  end
  object Qrbloqparcits: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM bloqparcits'
      'WHERE Codigo=:P0'
      'AND Controle=:P1')
    Left = 892
    Top = 77
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrbloqparcitsCodigo: TIntegerField
      FieldName = 'Codigo'
      DisplayFormat = '000000'
    end
    object QrbloqparcitsControle: TIntegerField
      FieldName = 'Controle'
      DisplayFormat = '000000'
    end
    object QrbloqparcitsConta: TAutoIncField
      FieldName = 'Conta'
      DisplayFormat = '000000'
    end
    object QrbloqparcitsSeq: TIntegerField
      FieldName = 'Seq'
      DisplayFormat = '00'
    end
    object QrbloqparcitsTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QrbloqparcitsCtrlOrigi: TIntegerField
      FieldName = 'CtrlOrigi'
      DisplayFormat = '000000'
    end
    object QrbloqparcitsBloqOrigi: TIntegerField
      FieldName = 'BloqOrigi'
      DisplayFormat = '000000'
    end
    object QrbloqparcitsPercIni: TFloatField
      FieldName = 'PercIni'
      DisplayFormat = '0.00'
    end
    object QrbloqparcitsPercFim: TFloatField
      FieldName = 'PercFim'
      DisplayFormat = '0.00'
    end
    object QrbloqparcitsVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsVlrMulta: TFloatField
      FieldName = 'VlrMulta'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsVlrJuros: TFloatField
      FieldName = 'VlrJuros'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsVlrTotal: TFloatField
      FieldName = 'VlrTotal'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsVlrAjust: TFloatField
      FieldName = 'VlrAjust'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsVlrSomas: TFloatField
      FieldName = 'VlrSomas'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsVlrJuro2: TFloatField
      FieldName = 'VlrJuro2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsVlrMult2: TFloatField
      FieldName = 'VlrMult2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsVlrLanct: TFloatField
      FieldName = 'VlrLanct'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsSParcA: TFloatField
      FieldName = 'SParcA'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsSParcD: TFloatField
      FieldName = 'SParcD'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrbloqparcitsDias: TIntegerField
      FieldName = 'Dias'
      DisplayFormat = '0000'
    end
    object QrbloqparcitsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrbloqparcitsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrbloqparcitsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrbloqparcitsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrbloqparcitsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrbloqparcitsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrbloqparcitsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrbloqparcitsParcela: TIntegerField
      FieldName = 'Parcela'
    end
  end
  object Dsbloqparcits: TDataSource
    DataSet = Qrbloqparcits
    Left = 920
    Top = 77
  end
  object frxDsBPI: TfrxDBDataset
    UserName = 'frxDsBPI'
    CloseDataSource = False
    DataSet = QrBPI
    BCDToCurrency = False
    
    Left = 948
    Top = 77
  end
  object QrBPI: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM bloqparcits'
      'WHERE Codigo=:P0')
    Left = 976
    Top = 77
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBPICodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'bloqparcits.Codigo'
    end
    object QrBPIControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'bloqparcits.Controle'
    end
    object QrBPIConta: TAutoIncField
      FieldName = 'Conta'
      Origin = 'bloqparcits.Conta'
    end
    object QrBPISeq: TIntegerField
      FieldName = 'Seq'
      Origin = 'bloqparcits.Seq'
    end
    object QrBPITipo: TWideStringField
      FieldName = 'Tipo'
      Origin = 'bloqparcits.Tipo'
      Size = 1
    end
    object QrBPICtrlOrigi: TIntegerField
      FieldName = 'CtrlOrigi'
      Origin = 'bloqparcits.CtrlOrigi'
    end
    object QrBPIBloqOrigi: TIntegerField
      FieldName = 'BloqOrigi'
      Origin = 'bloqparcits.BloqOrigi'
    end
    object QrBPIPercIni: TFloatField
      FieldName = 'PercIni'
      Origin = 'bloqparcits.PercIni'
    end
    object QrBPIPercFim: TFloatField
      FieldName = 'PercFim'
      Origin = 'bloqparcits.PercFim'
    end
    object QrBPIVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      Origin = 'bloqparcits.VlrOrigi'
    end
    object QrBPIVlrMulta: TFloatField
      FieldName = 'VlrMulta'
      Origin = 'bloqparcits.VlrMulta'
    end
    object QrBPIVlrJuros: TFloatField
      FieldName = 'VlrJuros'
      Origin = 'bloqparcits.VlrJuros'
    end
    object QrBPIVlrTotal: TFloatField
      FieldName = 'VlrTotal'
      Origin = 'bloqparcits.VlrTotal'
    end
    object QrBPIVlrAjust: TFloatField
      FieldName = 'VlrAjust'
      Origin = 'bloqparcits.VlrAjust'
    end
    object QrBPIVlrSomas: TFloatField
      FieldName = 'VlrSomas'
      Origin = 'bloqparcits.VlrSomas'
    end
    object QrBPISParcA: TFloatField
      FieldName = 'SParcA'
      Origin = 'bloqparcits.SParcA'
    end
    object QrBPISParcD: TFloatField
      FieldName = 'SParcD'
      Origin = 'bloqparcits.SParcD'
    end
    object QrBPIDias: TIntegerField
      FieldName = 'Dias'
      Origin = 'bloqparcits.Dias'
    end
    object QrBPILk: TIntegerField
      FieldName = 'Lk'
      Origin = 'bloqparcits.Lk'
    end
    object QrBPIDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'bloqparcits.DataCad'
    end
    object QrBPIDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'bloqparcits.DataAlt'
    end
    object QrBPIUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'bloqparcits.UserCad'
    end
    object QrBPIUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'bloqparcits.UserAlt'
    end
    object QrBPIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'bloqparcits.AlterWeb'
    end
    object QrBPIAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'bloqparcits.Ativo'
    end
    object QrBPIParcela: TIntegerField
      FieldName = 'Parcela'
      Origin = 'bloqparcits.Parcela'
    end
  end
  object QrStatus: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Novo'
      'FROM bloqparc'
      'WHERE Codigo=:P0')
    Left = 902
    Top = 106
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStatusNovo: TSmallintField
      FieldName = 'Novo'
    end
  end
  object QrSomas: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(VlrOrigi) VlrOrigi, SUM(VlrMulta) VlrMulta,'
      'SUM(VlrJuros+VlrAjust) VlrJuros, SUM(VlrTotal) VlrTotal'
      'FROM bloqparcits'
      'WHERE Codigo=:P0')
    Left = 73
    Top = 225
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomasVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
    end
    object QrSomasVlrMulta: TFloatField
      FieldName = 'VlrMulta'
    end
    object QrSomasVlrJuros: TFloatField
      FieldName = 'VlrJuros'
    end
    object QrSomasVlrTotal: TFloatField
      FieldName = 'VlrTotal'
    end
  end
  object QrSomaT: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 101
    Top = 225
  end
  object QrBloqorig: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT BloqOrigi, SUM(VlrOrigi) VlrOrigi, '
      'SUM(VlrMulta) VlrMulta, SUM(VlrJuros+VlrAjust) VlrJuros,'
      'SUM(VlrTotal) VlrTotal'
      'FROM bloqparcits'
      'WHERE Codigo=:P0'
      ''
      'GROUP BY BloqOrigi')
    Left = 977
    Top = 105
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBloqorigBloqOrigi: TIntegerField
      FieldName = 'BloqOrigi'
    end
    object QrBloqorigVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
    end
    object QrBloqorigVlrMulta: TFloatField
      FieldName = 'VlrMulta'
    end
    object QrBloqorigVlrJuros: TFloatField
      FieldName = 'VlrJuros'
    end
    object QrBloqorigVlrTotal: TFloatField
      FieldName = 'VlrTotal'
    end
  end
  object frxDsBloqlan: TfrxDBDataset
    UserName = 'frxDsBloqlan'
    CloseDataSource = False
    DataSet = QrBloqlan
    BCDToCurrency = False
    
    Left = 949
    Top = 133
  end
  object QrBloqlan: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 977
    Top = 133
    object QrBloqlanCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrBloqlanVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrBloqlanVLRORIG: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRORIG'
      LookupDataSet = QrBloqorig
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrOrigi'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrBloqlanVLRMULTA: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRMULTA'
      LookupDataSet = QrBloqorig
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrMulta'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrBloqlanVLRJUROS: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRJUROS'
      LookupDataSet = QrBloqorig
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrJuros'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrBloqlanVLRTOTAL: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRTOTAL'
      LookupDataSet = QrBloqorig
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrTotal'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrBloqlanFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrSomaBPI: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT '
      'SUM(VlrSomas) VlrSomas, '
      'SUM(VlrTotal) VlrTotal,'
      'SUM(VlrJuros) VlrJuros'
      'FROM bloqparcits'
      'WHERE Codigo=:P0'
      '')
    Left = 81
    Top = 346
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaBPIVlrSomas: TFloatField
      FieldName = 'VlrSomas'
    end
    object QrSomaBPIVlrJuros: TFloatField
      FieldName = 'VlrJuros'
    end
    object QrSomaBPIVlrTotal: TFloatField
      FieldName = 'VlrTotal'
    end
  end
  object QrSomaBPP: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(ValBol) ValBol'
      'FROM bloqparcpar'
      'WHERE Codigo=:P0')
    Left = 109
    Top = 346
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaBPPValBol: TFloatField
      FieldName = 'ValBol'
    end
  end
  object QrIts: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Conta'
      'FROM bloqparcits'
      'WHERE Codigo=:P0'
      'ORDER BY VlrJuros DESC')
    Left = 137
    Top = 346
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsConta: TAutoIncField
      FieldName = 'Conta'
    end
  end
  object QrBPP_Pgt: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 921
    Top = 161
    object QrBPP_PgtControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000'
    end
    object QrBPP_PgtData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBPP_PgtNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrBPP_PgtGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBPP_PgtDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBPP_PgtCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBPP_PgtMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBPP_PgtMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsBPP_Pgt: TDataSource
    DataSet = QrBPP_Pgt
    Left = 477
    Top = 441
  end
  object QrIncorr: TMySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrIncorrAfterOpen
    BeforeClose = QrIncorrBeforeClose
    Left = 949
    Top = 189
    object QrIncorrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIncorrData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrIncorrDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrIncorrCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrIncorrTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrIncorrCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrIncorrSit: TIntegerField
      FieldName = 'Sit'
    end
  end
  object DsIncorr: TDataSource
    DataSet = QrIncorr
    Left = 977
    Top = 189
  end
  object QrBPP_ToP: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 949
    Top = 161
    object QrBPP_ToPCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBPP_ToPMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBPP_ToPMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsBPP_ToP: TDataSource
    DataSet = QrBPP_ToP
    Left = 977
    Top = 161
  end
  object QrBPI2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 828
    Top = 52
    object QrBPI2CTRL_LAN: TIntegerField
      FieldName = 'CTRL_LAN'
    end
    object QrBPI2SUB_LAN: TSmallintField
      FieldName = 'SUB_LAN'
    end
    object QrBPI2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBPI2Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBPI2VlrTotal: TFloatField
      FieldName = 'VlrTotal'
      Required = True
    end
    object QrBPI2MultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrBPI2CTRL_REP: TIntegerField
      FieldName = 'CTRL_REP'
      Required = True
    end
    object QrBPI2Dias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrBPI2VlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      Required = True
    end
    object QrBPI2Fator: TFloatField
      FieldName = 'Fator'
      Required = True
    end
  end
  object QrIncorr2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 800
    Top = 52
    object QrIncorr2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIncorr2Data: TDateField
      FieldName = 'Data'
    end
    object QrIncorr2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrIncorr2Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrIncorr2Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrIncorr2Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrIncorr2Sit: TIntegerField
      FieldName = 'Sit'
    end
    object QrIncorr2Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrIncorr2Compensado: TDateField
      FieldName = 'Compensado'
    end
  end
  object Query: TABSQuery
    CurrentVersion = '7.93 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 696
    Top = 340
    object QueryOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QueryOrigi: TFloatField
      FieldName = 'Origi'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QueryDias: TFloatField
      FieldName = 'Dias'
    end
    object QueryFator: TFloatField
      FieldName = 'Fator'
    end
    object QueryValDif: TFloatField
      FieldName = 'ValDif'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QueryValItem: TFloatField
      FieldName = 'ValItem'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QueryAtivo: TIntegerField
      FieldName = 'Ativo'
    end
    object QueryRestaFat: TFloatField
      FieldName = 'RestaFat'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QueryRestaDif: TFloatField
      FieldName = 'RestaDif'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 724
    Top = 340
  end
  object QrSum: TABSQuery
    CurrentVersion = '7.93 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 696
    Top = 368
  end
  object DataSource2: TDataSource
    DataSet = QrSum
    Left = 724
    Top = 368
  end
  object QrPropriet: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP '
      'FROM entidades ent'
      'LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet'
      'WHERE ent.Cliente2="V"'
      'AND cim.Codigo<>0'
      'ORDER BY NOMEPROP')
    Left = 705
    Top = 17
    object QrProprietCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrProprietNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Origin = 'NOMEPROP'
      Required = True
      Size = 100
    end
  end
  object DsPropriet: TDataSource
    DataSet = QrPropriet
    Left = 733
    Top = 17
  end
  object QrCondImov: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT cim.Conta, cim.Unidade'
      'FROM condimov cim'
      'WHERE cim.Codigo<>0'
      'AND cim.Propriet<>0'
      'ORDER BY cim.Unidade'
      '')
    Left = 761
    Top = 17
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 789
    Top = 17
  end
  object QrPesq: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 12
    Top = 200
    object QrPesqVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqLancto: TIntegerField
      FieldName = 'Lancto'
      Required = True
      DisplayFormat = '000000'
    end
    object QrPesqBPP_VlrTotal: TFloatField
      FieldName = 'BPP_VlrTotal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqBPP_VlrDesco: TFloatField
      FieldName = 'BPP_VlrDesco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrPesqNO_PES: TWideStringField
      FieldName = 'NO_PES'
      Size = 100
    end
    object QrPesqUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPesqCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrPesqloginID: TWideStringField
      FieldName = 'loginID'
      Size = 32
    end
    object QrPesqautentica: TWideMemoField
      FieldName = 'autentica'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPesqCodCliEsp: TIntegerField
      FieldName = 'CodCliEsp'
    end
    object QrPesqCodCliEnt: TIntegerField
      FieldName = 'CodCliEnt'
    end
    object QrPesqCodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
    end
    object QrPesqCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
    end
    object QrPesqVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqVlrMulta: TFloatField
      FieldName = 'VlrMulta'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqVlrJuros: TFloatField
      FieldName = 'VlrJuros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqVlrTotal: TFloatField
      FieldName = 'VlrTotal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqDataP: TDateTimeField
      FieldName = 'DataP'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqNovo: TSmallintField
      FieldName = 'Novo'
    end
    object QrPesqTxaJur: TFloatField
      FieldName = 'TxaJur'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqTxaMul: TFloatField
      FieldName = 'TxaMul'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrPesqParcela: TIntegerField
      FieldName = 'Parcela'
      Required = True
      DisplayFormat = '0'
    end
    object QrPesqFatNum: TFloatField
      FieldName = 'FatNum'
      Required = True
      DisplayFormat = '000000'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 40
    Top = 200
  end
  object frxParcelamento: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39563.538129155100000000
    ReportOptions.LastChange = 39563.538129155100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 348
    Top = 12
    Datasets = <
      item
        DataSet = frxDsBloqlan
        DataSetName = 'frxDsBloqlan'
      end
      item
        DataSet = frxDsBloqParc
        DataSetName = 'frxDsBloqParc'
      end
      item
        DataSet = frxDsBloqParcPar
        DataSetName = 'frxDsBloqParcPar'
      end
      item
      end
      item
        DataSet = frxDsBPI
        DataSetName = 'frxDsBPI'
      end
      item
      end
      item
        DataSet = DmCond.frxDsCond
        DataSetName = 'frxDsCond'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end
      item
        DataSet = frxDsLcto
        DataSetName = 'frxDsLcto'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 102.047310000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PARCELAMENTO DE D'#201'BITOS N'#186' [frxDsBloqParc."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBloqParc."NOMECOND"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'UH: [frxDsBloqParc."Unidade"]   Propriet'#225'rio: [frxDsBloqParc."NO' +
              'MEPROP"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Data parcelamento: [frxDsBloqParc."DataP"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Subreport1: TfrxSubreport
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Page = frxParcelamento.Page6
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Subreport2: TfrxSubreport
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Page = frxParcelamento.Page5
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 385.512060000000000000
        Width = 680.315400000000000000
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Subreport3: TfrxSubreport
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Page = frxParcelamento.Page7
        end
      end
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Subreport4: TfrxSubreport
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Page = frxParcelamento.Page2
        end
      end
    end
    object Page6: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 60.472480000000000000
        Width = 680.315400000000000000
        DataSet = frxDsBloqParcPar
        DataSetName = 'frxDsBloqParcPar'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976402360000000000
          Width = 75.590551180000000000
          Height = 18.897637795275590000
          DataSet = frxDsBloqParcPar
          DataSetName = 'frxDsBloqParcPar'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000;-000000; '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39',<frxDsBloqParcPar."FatNum">)]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 75.590551180000000000
          Height = 18.897637795275590000
          DataSet = frxDsBloqParcPar
          DataSetName = 'frxDsBloqParcPar'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000;-000000; '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39',<frxDsBloqParcPar."Parcela">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566953540000000000
          Width = 75.590551180000000000
          Height = 18.897637795275590000
          DataField = 'ValBol'
          DataSet = frxDsBloqParcPar
          DataSetName = 'frxDsBloqParcPar'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBloqParcPar."ValBol"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385851180000000000
          Width = 75.590551180000000000
          Height = 18.897637795275590000
          DataField = 'Vencimento'
          DataSet = frxDsBloqParcPar
          DataSetName = 'frxDsBloqParcPar'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBloqParcPar."Vencimento"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsBloqParcPar."Codigo"'
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976402360000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Bloqueto')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Parcela')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 264.566953540000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385851180000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 102.047310000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DataSet = frxDsBloqParcPar
          DataSetName = 'frxDsBloqParcPar'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBloqParcPar."ValBol">)]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          DataSet = frxDsBloqParcPar
          DataSetName = 'frxDsBloqParcPar'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[COUNT(MasterData1)] parcelas')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 49.133880240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLcto."Reparcel"'
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385948820000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Bloqueto')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976402360000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Valor original')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 15.118120000000000000
          Width = 529.134151180000000000
          Height = 18.897640240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Bloquetos que foram parcelados')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Base c'#225'lculo*')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '$ Multa*')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '$ Juros*')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '$ Atualizado*')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 49.133890000000000000
        Top = 132.283550000000000000
        Width = 680.315400000000000000
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 151.181151180000000000
          Height = 17.007874015748030000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBloqlan."Credito">)]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567197640000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBloqlan."VLRORIG">)]*')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157797640000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBloqlan."VLRMULTA">)]*')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748397640000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBloqlan."VLRJUROS">)]*')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338997640000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBloqlan."VLRTOTAL">)]*')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 529.134200000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            
              '* Valores podem ter diferen'#231'as, pois cada bloqueto pode ter v'#225'ri' +
              'os lan'#231'amentos, e cada lan'#231'amento '#233' calculado individualmente e ' +
              'arredondado na segunda casa decimal.')
          ParentFont = False
        end
      end
      object MasterData8: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874020000000000
        Top = 90.708720000000000000
        Width = 680.315400000000000000
        DataSet = frxDsBloqlan
        DataSetName = 'frxDsBloqlan'
        RowCount = 0
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385948820000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'FatNum'
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBloqlan."FatNum"]')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976402360000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'Credito'
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBloqlan."Credito"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'Vencimento'
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBloqlan."Vencimento"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'VLRORIG'
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBloqlan."VLRORIG"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'VLRMULTA'
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBloqlan."VLRMULTA"]')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'VLRJUROS'
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBloqlan."VLRJUROS"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'VLRTOTAL'
          DataSet = frxDsBloqlan
          DataSetName = 'frxDsBloqlan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBloqlan."VLRTOTAL"]')
          ParentFont = False
        end
      end
    end
    object Page5: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 49.133880240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLcto."Reparcel"'
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771751180000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590648820000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Bloqueto')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724702360000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181297640000000000
          Top = 34.015770000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362448820000000000
          Top = 34.015770000000010000
          Width = 302.362351180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118120000000000000
          Width = 680.315351180000000000
          Height = 18.897640240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lan'#231'amentos que foram parcelados')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 132.283550000000000000
        Width = 680.315400000000000000
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLcto."Credito">)]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Width = 604.724751180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874020000000000
        Top = 90.708720000000000000
        Width = 680.315400000000000000
        DataSet = frxDsLcto
        DataSetName = 'frxDsLcto'
        RowCount = 0
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771751180000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'Vencimento'
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLcto."Vencimento"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590648820000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'BLOQUETO'
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLcto."BLOQUETO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724702360000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'Credito'
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLcto."Credito"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'Data'
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLcto."Data"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181297640000000000
          Width = 75.590551180000000000
          Height = 17.007874015748030000
          DataField = 'Controle'
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLcto."Controle"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362448820000000000
          Width = 302.362351180000000000
          Height = 17.007874015748030000
          DataField = 'Descricao'
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLcto."Descricao"]')
          ParentFont = False
        end
      end
    end
    object Page7: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 10.204724410000000000
        Top = 86.929190000000000000
        Width = 680.315400000000000000
        DataSet = frxDsBPI
        DataSetName = 'frxDsBPI'
        RowCount = 0
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897698820000000000
          Width = 11.338541180000000000
          Height = 10.204724409448800000
          DataField = 'Tipo'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBPI."Tipo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Width = 18.897601180000000000
          Height = 10.204724409448800000
          DataField = 'Seq'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBPI."Seq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 26.456692913385800000
          Height = 10.204724410000000000
          DataField = 'PercIni'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBPI."PercIni"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Width = 26.456692910000000000
          Height = 10.204724410000000000
          DataField = 'PercFim'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBPI."PercFim"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataField = 'VlrOrigi'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBPI."VlrOrigi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960778820000000000
          Width = 45.354330710000000000
          Height = 10.204724410000000000
          DataField = 'VlrMulta'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBPI."VlrMulta"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315138820000000000
          Width = 45.354330710000000000
          Height = 10.204724410000000000
          DataField = 'VlrJuros'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBPI."VlrJuros"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669498820000000000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataField = 'VlrTotal'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBPI."VlrTotal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803388820000000000
          Width = 37.795275590000000000
          Height = 10.204724410000000000
          DataField = 'VlrAjust'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBPI."VlrAjust"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598688820000000000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBPI."VlrSomas"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Width = 41.574803150000000000
          Height = 10.204724410000000000
          DataField = 'Controle'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBPI."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 41.574781180000000000
          Height = 10.204724410000000000
          DataField = 'BloqOrigi'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBPI."BloqOrigi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882238820000000000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataField = 'SParcA'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBPI."SParcA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016128820000000000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataField = 'SParcD'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBPI."SParcD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Width = 45.354311180000000000
          Height = 10.204724410000000000
          DataField = 'Codigo'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBPI."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 45.354311180000000000
          Height = 10.204724410000000000
          DataField = 'CtrlOrigi'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBPI."CtrlOrigi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Width = 26.456661180000000000
          Height = 10.204724410000000000
          DataField = 'Dias'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBPI."Dias"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236240000000000000
          Width = 22.677131180000000000
          Height = 10.204724409448800000
          DataField = 'Parcela'
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBPI."Parcela"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 44.220494410000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsBPI."Codigo"'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826791180000000000
          Top = 34.015770000000010000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Original')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897698820000000000
          Top = 34.015770000000010000
          Width = 11.338541180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bloqueto')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015770000000010000
          Width = 18.897601180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913517640000000000
          Top = 34.015770000000010000
          Width = 26.456692913385800000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% ini')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315351180000000000
          Height = 15.118110240000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Atrelamento de lan'#231'amentos')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 34.015770000000010000
          Width = 26.456692910000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Fim')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 34.015770000000010000
          Width = 45.354330710000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Multa')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 34.015770000000010000
          Width = 45.354330710000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Juros')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 34.015770000000010000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ TOTAL')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 34.015770000000010000
          Width = 37.795275590000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Ajuste')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 34.015770000000010000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ SOMAS')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Top = 34.015770000000010000
          Width = 41.574803150000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ctrl Parc.')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 34.015770000000010000
          Width = 41.574781180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bloqueto')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 34.015770000000010000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ S.Antes')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 34.015770000000010000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ S.Depois')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Top = 34.015770000000010000
          Width = 45.354311180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reparcelamento')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 34.015770000000010000
          Width = 45.354311180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Top = 34.015770000000010000
          Width = 26.456661180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dias')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 34.015770000000010000
          Width = 22.677131180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Parc.')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 120.944960000000000000
        Width = 680.315400000000000000
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Width = 298.582821180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Width = 105.826791180000000000
          Height = 10.204724410000000000
          DataSet = frxDsLcto
          DataSetName = 'frxDsLcto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBPI."VlrOrigi">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960778820000000000
          Width = 45.354311180000000000
          Height = 10.204724410000000000
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBPI."VlrMulta">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315138820000000000
          Width = 45.354311180000000000
          Height = 10.204724410000000000
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBPI."VlrJuros">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669498820000000000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBPI."VlrTotal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803388820000000000
          Width = 37.795251180000000000
          Height = 10.204724410000000000
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBPI."VlrAjust">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598688820000000000
          Width = 49.133841180000000000
          Height = 10.204724410000000000
          DataSet = frxDsBPI
          DataSetName = 'frxDsBPI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBPI."VlrSomas">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxCondE2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 39342.853423206000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoBancoExiste> = True then'
      '  begin'
      '      Picture_Bco.LoadFromFile(<LogoBancoPath>);'
      '      Memo_001.Visible := False;'
      '      Pictur2_Bco.LoadFromFile(<LogoBancoPath>);'
      '      Mem2_001.Visible := False;'
      '  end else begin'
      '    Memo_001.Visible := True;'
      '    Mem2_001.Visible := True;'
      '  end;              '
      '  //if <MeuLogoBolExiste> = True then '
      '    //Picture3.LoadFromFile( <frxDsConfigBol."MeuLogoArq">);'
      'end.')
    OnGetValue = frxCondE2GetValue
    Left = 804
    Top = 84
    Datasets = <
      item
        DataSet = frxDsBloqlan
        DataSetName = 'frxDsBloqlan'
      end
      item
        DataSet = frxDsBloqParc
        DataSetName = 'frxDsBloqParc'
      end
      item
        DataSet = frxDsBloqParcPar
        DataSetName = 'frxDsBloqParcPar'
      end
      item
        DataSet = frxDsBoletos
        DataSetName = 'frxDsBoletos'
      end
      item
        DataSet = frxDsBPI
        DataSetName = 'frxDsBPI'
      end
      item
      end
      item
        DataSet = DmCond.frxDsCond
        DataSetName = 'frxDsCond'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end
      item
        DataSet = frxDsLcto
        DataSetName = 'frxDsLcto'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 1107.402290000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        DataSet = frxDsBoletos
        DataSetName = 'frxDsBoletos'
        RowCount = 0
        object Memo129: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 896.504330000000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO3]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 840.567322130000000000
          Width = 137.952755910000000000
          Height = 17.385826770000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletos."SUB_TOT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Pictur2_Bco: TfrxPictureView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 710.551640000000000000
          Width = 173.858380000000000000
          Height = 41.574830000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 761.197243390000000000
          Width = 137.952755910000000000
          Height = 23.055118110000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletos."Vencto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 831.496600000000000000
          Width = 137.952755910000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(=) Valor do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 752.126470000000000000
          Width = 137.952755910000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line13: TfrxLineView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 710.551640000000000000
          Width = 789.921770000000000000
          Color = clBlack
          ArrowLength = 100
          ArrowWidth = 20
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 699.213050000000000000
          Width = 162.519790000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'AUTENTICA'#199#195'O MEC'#194'NICA')
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          AllowVectorExport = True
          Left = 90.708661420000000000
          Top = 1050.709054410000000000
          Width = 405.000000000000000000
          Height = 49.133858270000000000
          BarType = bcCode_2_5_interleaved
          Expression = '<VARF_CODIGOBARRAS>'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          TestLine = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 3.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ColorBar = clBlack
        end
        object Line14: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 752.126470000000000000
          Width = 687.874015750000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line15: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 784.252359060000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line16: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 808.819420000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line17: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 831.496600000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line18: TfrxLineView
          AllowVectorExport = True
          Left = 226.771653540000000000
          Top = 729.449290000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line19: TfrxLineView
          AllowVectorExport = True
          Left = 294.803152050000000000
          Top = 729.449290000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line20: TfrxLineView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 752.126470000000000000
          Height = 230.551330000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line21: TfrxLineView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 808.819420000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line22: TfrxLineView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 808.819420000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line23: TfrxLineView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Top = 808.819420000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line24: TfrxLineView
          AllowVectorExport = True
          Left = 228.661417320000000000
          Top = 831.496600000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line25: TfrxLineView
          AllowVectorExport = True
          Left = 160.629921260000000000
          Top = 808.819420000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line26: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 857.953310000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line27: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 982.677555910000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line28: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 1043.149996850000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 1.500000000000000000
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 729.449290000000000000
          Width = 68.031540000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAX]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 729.449290000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_LINHADIGITAVEL]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 752.126470000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Local de Pagamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 786.142240000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cedente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 808.819420000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Top = 763.465060000000000000
          Width = 544.252320000000100000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."LocalPag"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 831.496600000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Uso do Banco')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 808.819420000000000000
          Width = 64.252010000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#250'mero do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 808.819420000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Esp'#233'cie do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 808.819420000000000000
          Width = 26.456710000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Aceite')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 808.819420000000000000
          Width = 68.031540000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data do Processamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 831.496600000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 831.496600000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Esp'#233'cie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 831.496600000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 831.496600000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 840.945274880000000000
          Width = 7.559060000000000000
          Height = 8.314960630000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'X')
          ParentFont = False
          WordWrap = False
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 786.142240000000000000
          Width = 136.063080000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Ag'#234'ncia/C'#243'digo Cedente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 808.819420000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Nosso N'#250'mero')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 857.953310000000000000
          Width = 136.063080000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(-) Desconto/Abatimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 884.410020000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(-) Outras Dedu'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 910.866730000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(+) Mora/Multa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 933.543910000000000000
          Width = 64.252010000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(+) Outros Acr'#233'scimos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 960.000620000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(=) Valor Cobrado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line29: TfrxLineView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 882.520075590000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line30: TfrxLineView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 907.087004720000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line31: TfrxLineView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 931.653933860000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line32: TfrxLineView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 956.220862990000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 1046.929810000000000000
          Width = 151.181200000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 1028.032160000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'C'#243'digo de Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 858.709054410000000000
          Width = 132.283550000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Top = 793.701300000000000000
          Width = 544.252320000000100000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."NOMECED"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 817.890156770000000000
          Width = 102.047310000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 817.890156770000000000
          Width = 128.504020000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBoletos."Boleto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 817.890156770000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."EspecieDoc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 817.890156770000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."ACEITETIT_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 817.890156770000000000
          Width = 166.299320000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 840.567322130000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."CART_IMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 840.567322130000000000
          Width = 56.692950000000010000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."EspecieVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 817.890156770000000000
          Width = 136.063080000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_NossoNumero]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 867.779920550000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO1]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 882.142125280000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO2]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo130: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 910.866730000000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO4]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo131: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Top = 925.228739450000000000
          Width = 544.252320000000100000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO5]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo132: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 939.590944170000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO6]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo133: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 953.953148900000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO7]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 968.315353620000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO8]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 793.701300000000000000
          Width = 136.063080000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_AGCodCed]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Mem2_001: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 721.890230000000000000
          Width = 173.858380000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."NOMEBANCO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 393.826840000000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO3]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 337.889832130000000000
          Width = 137.952755910000000000
          Height = 17.385826770000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletos."SUB_TOT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Picture_Bco: TfrxPictureView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 207.874150000000000000
          Width = 173.858380000000000000
          Height = 41.574830000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 258.519753390000000000
          Width = 137.952755910000000000
          Height = 23.055118110000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletos."Vencto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 328.819110000000000000
          Width = 137.952755910000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(=) Valor do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 249.448980000000000000
          Width = 137.952755910000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 207.874150000000000000
          Width = 789.921770000000000000
          Color = clBlack
          ArrowLength = 100
          ArrowWidth = 20
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 192.756030000000000000
          Width = 162.519790000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'SEGUNDA VIA')
          ParentFont = False
        end
        object BarCode2: TfrxBarCodeView
          AllowVectorExport = True
          Left = 90.708661420000000000
          Top = 548.031564410000000000
          Width = 405.000000000000000000
          Height = 49.133858270000000000
          BarType = bcCode_2_5_interleaved
          Expression = '<VARF_CODIGOBARRAS>'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          TestLine = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 3.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ColorBar = clBlack
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 249.448980000000000000
          Width = 687.874015750000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 281.574869060000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 306.141930000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 328.819110000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line6: TfrxLineView
          AllowVectorExport = True
          Left = 226.771653540000000000
          Top = 226.771800000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line7: TfrxLineView
          AllowVectorExport = True
          Left = 294.803152050000000000
          Top = 226.771800000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line8: TfrxLineView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 249.448980000000000000
          Height = 230.551330000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 306.141930000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line10: TfrxLineView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 306.141930000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line11: TfrxLineView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Top = 306.141930000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line12: TfrxLineView
          AllowVectorExport = True
          Left = 228.661417320000000000
          Top = 328.819110000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line33: TfrxLineView
          AllowVectorExport = True
          Left = 160.629921260000000000
          Top = 306.141930000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line34: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 355.275820000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line35: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 480.000065910000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line36: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 540.472506849999900000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 1.500000000000000000
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 226.771800000000000000
          Width = 68.031540000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAX]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 226.771800000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_LINHADIGITAVEL]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 249.448980000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Local de Pagamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 283.464750000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cedente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 306.141930000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Top = 260.787570000000000000
          Width = 544.252320000000100000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."LocalPag"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 328.819110000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Uso do Banco')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 306.141930000000000000
          Width = 64.252010000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#250'mero do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 306.141930000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Esp'#233'cie do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 306.141930000000000000
          Width = 26.456710000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Aceite')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 306.141930000000000000
          Width = 68.031540000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data do Processamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 328.819110000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 328.819110000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Esp'#233'cie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 328.819110000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 328.819110000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 338.267784880000000000
          Width = 7.559060000000000000
          Height = 8.314960630000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'X')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 283.464750000000000000
          Width = 136.063080000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Ag'#234'ncia/C'#243'digo Cedente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 306.141930000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Nosso N'#250'mero')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 355.275820000000000000
          Width = 136.063080000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(-) Desconto/Abatimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 381.732530000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(-) Outras Dedu'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 408.189240000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(+) Mora/Multa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 430.866420000000000000
          Width = 64.252010000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(+) Outros Acr'#233'scimos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 457.323130000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '(=) Valor Cobrado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line37: TfrxLineView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 379.842585590000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line38: TfrxLineView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 404.409514720000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line39: TfrxLineView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 428.976443860000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line40: TfrxLineView
          AllowVectorExport = True
          Left = 602.834645670000000000
          Top = 453.543372990000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 544.252320000000100000
          Width = 151.181200000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 356.031564410000000000
          Width = 132.283550000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Top = 291.023810000000000000
          Width = 544.252320000000100000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."NOMECED"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 315.212666770000000000
          Width = 102.047310000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 315.212666770000000000
          Width = 128.504020000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBoletos."Boleto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 315.212666770000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."EspecieDoc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 315.212666770000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."ACEITETIT_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 315.212666770000000000
          Width = 166.299320000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 337.889832130000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."CART_IMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 337.889832130000000000
          Width = 56.692950000000010000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."EspecieVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 315.212666770000000000
          Width = 136.063080000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_NossoNumero]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 365.102430550000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO1]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 379.464635280000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO2]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 408.189240000000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO4]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 422.551249450000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO5]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 436.913454170000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO6]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 451.275658900000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO7]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 465.637863620000000000
          Width = 544.252320000000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO8]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 291.023810000000000000
          Width = 136.063080000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_AGCodCed]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo_001: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 219.212740000000000000
          Width = 173.858380000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."NOMEBANCO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 1033.700782520000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Sacador / Avalista')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 1012.914040000000000000
          Width = 449.764070000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsInquilino."E_ALL"]')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 997.795920000000000000
          Width = 449.764070000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsInquilino."PROPRI_E_MORADOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 1033.700782520000000000
          Width = 393.071120000000000000
          Height = 9.070866140000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."NOMESAC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_049: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 982.677800000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_050: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 982.677800000000000000
          Width = 309.921460000000000000
          Height = 15.118112680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCond."NOMECLI"]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo_055: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 982.677800000000000000
          Width = 128.504020000000000000
          Height = 30.236232680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade: [frxDsBoletos."Unidade"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 525.354670000000100000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'C'#243'digo de Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 510.236549999999900000
          Width = 449.764070000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsInquilino."E_ALL"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 495.118430000000000000
          Width = 449.764070000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsInquilino."PROPRI_E_MORADOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 480.000310000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 480.000310000000000000
          Width = 309.921460000000000000
          Height = 15.118112680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCond."NOMECLI"]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 480.000310000000000000
          Width = 128.504020000000000000
          Height = 30.236232680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade: [frxDsBoletos."Unidade"]')
          ParentFont = False
          WordWrap = False
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 41.574830000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line41: TfrxLineView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 60.472480000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PARCELAMENTO DE D'#201'BITOS N'#186' [frxDsBloqParc."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 60.472480000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 60.472480000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 83.149660000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBloqParc."NOMECOND"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 120.944960000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'UH: [frxDsBloqParc."Unidade"]   Propriet'#225'rio: [frxDsBloqParc."NO' +
              'MEPROP"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Data parcelamento: [frxDsBloqParc."DataP"]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 147.401670000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 158.740260000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Parcela [frxDsBoletos."Parcela"] / [VARF_PARCELAS]')
          ParentFont = False
        end
      end
    end
  end
  object QrBoletos: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrBoletosCalcFields
    SQL.Strings = (
      'SELECT ValBol SUB_TOT, Vencimento Vencto, '
      'FatNum Boleto, Parcela'
      'FROM bloqparcpar'
      'WHERE Codigo=:P0'
      'ORDER BY Vencimento')
    Left = 832
    Top = 84
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBoletosSUB_TOT: TFloatField
      FieldName = 'SUB_TOT'
    end
    object QrBoletosVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrBoletosBoleto: TFloatField
      FieldName = 'Boleto'
    end
    object QrBoletosUnidade: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Unidade'
      Size = 50
      Calculated = True
    end
    object QrBoletosBLOQUETO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'BLOQUETO'
      Calculated = True
    end
    object QrBoletosParcela: TIntegerField
      FieldName = 'Parcela'
    end
  end
  object frxDsBoletos: TfrxDBDataset
    UserName = 'frxDsBoletos'
    CloseDataSource = False
    DataSet = QrBoletos
    BCDToCurrency = False
    
    Left = 860
    Top = 84
  end
  object PMImprime: TPopupMenu
    Left = 8
    Top = 12
    object Reparcelamento1: TMenuItem
      Caption = '&Reparcelamento'
      OnClick = Reparcelamento1Click
    end
    object Bloqueto1: TMenuItem
      Caption = '&Bloqueto(s)'
      OnClick = Bloqueto1Click
    end
  end
  object QrLocLct: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 372
    Top = 96
    object QrLocLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLocLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLocLctAtrelado: TIntegerField
      FieldName = 'Atrelado'
    end
  end
  object QrPesqL: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 80
    Top = 380
    object QrPesqLControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqLData: TDateField
      FieldName = 'Data'
    end
    object QrPesqLTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPesqLCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPesqLSub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object QrCNAB_Cfg_B: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      
        'IF(ent.Tipo=0, ent.EUF, ent.PUF) + 0.000 UF, ufs.Nome NOMEUF, bc' +
        's.DVB,'
      'bcs.Nome NOMEBANCO, car.Ativo CART_ATIVO,'
      'car.TipoDoc CAR_TIPODOC, con.*,'
      'IF(ced.Tipo=0, ced.RazaoSocial, ced.Nome) NOMECED,'
      'IF(sac.Tipo=0, sac.RazaoSocial, sac.Nome) NOMESAC,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCNUM'
      'FROM cond con'
      'LEFT JOIN entidades ced ON ced.Codigo=con.Cedente'
      'LEFT JOIN entidades sac ON sac.Codigo=con.SacadAvali'
      'LEFT JOIN entidades ent ON ent.Codigo=con.Cliente'
      'LEFT JOIN bancos   bcs ON bcs.Codigo=con.Banco'
      'LEFT JOIN carteiras car ON car.Codigo=con.CartEmiss'
      
        'LEFT JOIN ufs   ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PU' +
        'F)'
      'WHERE con.Codigo=:P0')
    Left = 208
    Top = 492
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_Cfg_BNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCNAB_Cfg_BLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Size = 127
    end
    object QrCNAB_Cfg_BNOMECED: TWideStringField
      FieldName = 'NOMECED'
      Size = 100
    end
    object QrCNAB_Cfg_BEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
    end
    object QrCNAB_Cfg_BACEITETIT_TXT: TWideStringField
      FieldName = 'ACEITETIT_TXT'
      Size = 1
    end
    object QrCNAB_Cfg_BCART_IMP: TWideStringField
      FieldName = 'CART_IMP'
      Size = 10
    end
    object QrCNAB_Cfg_BEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Size = 5
    end
    object QrCNAB_Cfg_BCedBanco: TIntegerField
      FieldName = 'CedBanco'
    end
    object QrCNAB_Cfg_BAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Size = 40
    end
    object QrCNAB_Cfg_BCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
    end
    object QrCNAB_Cfg_BCedPosto: TIntegerField
      FieldName = 'CedPosto'
    end
    object QrCNAB_Cfg_BCedConta: TWideStringField
      FieldName = 'CedConta'
      Size = 30
    end
    object QrCNAB_Cfg_BCartNum: TWideStringField
      FieldName = 'CartNum'
      Size = 3
    end
    object QrCNAB_Cfg_BIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Size = 2
    end
    object QrCNAB_Cfg_BCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
    end
    object QrCNAB_Cfg_BTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrCNAB_Cfg_BCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrCNAB_Cfg_BCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCNAB_Cfg_BModalCobr: TIntegerField
      FieldName = 'ModalCobr'
    end
    object QrCNAB_Cfg_BMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrCNAB_Cfg_BJurosPerc: TFloatField
      FieldName = 'JurosPerc'
    end
    object QrCNAB_Cfg_BTexto01: TWideStringField
      FieldName = 'Texto01'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto02: TWideStringField
      FieldName = 'Texto02'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto03: TWideStringField
      FieldName = 'Texto03'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto04: TWideStringField
      FieldName = 'Texto04'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto05: TWideStringField
      FieldName = 'Texto05'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto06: TWideStringField
      FieldName = 'Texto06'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto07: TWideStringField
      FieldName = 'Texto07'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto08: TWideStringField
      FieldName = 'Texto08'
      Size = 100
    end
    object QrCNAB_Cfg_BCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Size = 1
    end
    object QrCNAB_Cfg_BCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Size = 1
    end
    object QrCNAB_Cfg_BOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Size = 3
    end
    object QrCNAB_Cfg_BLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrCNAB_Cfg_BNOMESAC: TWideStringField
      FieldName = 'NOMESAC'
      Size = 100
    end
    object QrCNAB_Cfg_BCorreio: TWideStringField
      FieldName = 'Correio'
      Size = 1
    end
    object QrCNAB_Cfg_BCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrCNAB_Cfg_BCedente: TIntegerField
      FieldName = 'Cedente'
    end
    object QrCNAB_Cfg_BCAR_TIPODOC: TIntegerField
      FieldName = 'CAR_TIPODOC'
    end
    object QrCNAB_Cfg_BCART_ATIVO: TIntegerField
      FieldName = 'CART_ATIVO'
    end
    object QrCNAB_Cfg_BNosNumFxaU: TFloatField
      FieldName = 'NosNumFxaU'
    end
    object QrCNAB_Cfg_BNosNumFxaI: TFloatField
      FieldName = 'NosNumFxaI'
    end
    object QrCNAB_Cfg_BNosNumFxaF: TFloatField
      FieldName = 'NosNumFxaF'
    end
    object QrCNAB_Cfg_BDVB: TWideStringField
      FieldName = 'DVB'
      Size = 1
    end
    object QrCNAB_Cfg_BCodigo: TFloatField
      FieldName = 'Codigo'
    end
    object QrCNAB_Cfg_BNPrinBc: TWideStringField
      FieldName = 'NPrinBc'
      Size = 30
    end
    object QrCNAB_Cfg_BCorresCto: TWideStringField
      FieldName = 'CorresCto'
      Size = 30
    end
    object QrCNAB_Cfg_BCorresAge: TIntegerField
      FieldName = 'CorresAge'
    end
    object QrCNAB_Cfg_BCorresBco: TIntegerField
      FieldName = 'CorresBco'
    end
  end
  object frxDsCNAB_Cfg_B: TfrxDBDataset
    UserName = 'frxDsCNAB_Cfg_B'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEBANCO=NOMEBANCO'
      'LocalPag=LocalPag'
      'NOMECED=NOMECED'
      'EspecieDoc=EspecieDoc'
      'ACEITETIT_TXT=ACEITETIT_TXT'
      'CART_IMP=CART_IMP'
      'EspecieVal=EspecieVal'
      'CedBanco=CedBanco'
      'AgContaCed=AgContaCed'
      'CedAgencia=CedAgencia'
      'CedPosto=CedPosto'
      'CedConta=CedConta'
      'CartNum=CartNum'
      'IDCobranca=IDCobranca'
      'CodEmprBco=CodEmprBco'
      'TipoCobranca=TipoCobranca'
      'CNAB=CNAB'
      'CtaCooper=CtaCooper'
      'ModalCobr=ModalCobr'
      'MultaPerc=MultaPerc'
      'JurosPerc=JurosPerc'
      'Texto01=Texto01'
      'Texto02=Texto02'
      'Texto03=Texto03'
      'Texto04=Texto04'
      'Texto05=Texto05'
      'Texto06=Texto06'
      'Texto07=Texto07'
      'Texto08=Texto08'
      'CedDAC_C=CedDAC_C'
      'CedDAC_A=CedDAC_A'
      'OperCodi=OperCodi'
      'LayoutRem=LayoutRem'
      'NOMESAC=NOMESAC'
      'Correio=Correio'
      'CartEmiss=CartEmiss'
      'Cedente=Cedente'
      'CAR_TIPODOC=CAR_TIPODOC'
      'CART_ATIVO=CART_ATIVO'
      'NosNumFxaU=NosNumFxaU'
      'NosNumFxaI=NosNumFxaI'
      'NosNumFxaF=NosNumFxaF'
      'DVB=DVB'
      'Codigo=Codigo'
      'NPrinBc=NPrinBc'
      'CorresCto=CorresCto'
      'CorresAge=CorresAge'
      'CorresBco=CorresBco')
    DataSet = QrCNAB_Cfg_B
    BCDToCurrency = False
    
    Left = 316
    Top = 492
  end
  object QrLoc: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 520
    Top = 516
  end
end
