object FmBloqParcDel: TFmBloqParcDel
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-031 :: Limpeza de Pacelamentos Exclu'#237'dos'
  ClientHeight = 645
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 483
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 479
    object PageControl1: TPageControl
      Left = 0
      Top = 246
      Width = 1008
      Height = 237
      ActivePage = TabSheet1
      Align = alBottom
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 241
      ExplicitWidth = 1006
      object TabSheet1: TTabSheet
        Caption = ' Prov'#225'veis bloquetos de parcelamento n'#227'o exclu'#237'dos (Local)'
        ExplicitWidth = 998
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 209
          Align = alClient
          DataSource = DsLctsL
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'UH'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Propriet'#225'rio'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CLIINT'
              Title.Caption = 'Condom'#237'nio'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Bloqueto'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Carteira'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CART'
              Title.Caption = 'Nome carteira'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Prov'#225'veis bloquetos de parcelamento n'#227'o exclu'#237'dos (web)'
        ImageIndex = 1
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 209
          Align = alClient
          DataSource = DsLctsN
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'UH'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Propriet'#225'rio'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CLIINT'
              Title.Caption = 'Condom'#237'nio'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Bloqueto'
              Width = 68
              Visible = True
            end>
        end
      end
    end
    object Panel4: TPanel
      Left = 440
      Top = 0
      Width = 568
      Height = 246
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 441
      ExplicitTop = 1
      ExplicitWidth = 566
      ExplicitHeight = 240
      object GroupBox2: TGroupBox
        Left = 12
        Top = 4
        Width = 329
        Height = 225
        Caption = ' Rastreamento de inconsist'#234'ncias: '
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 325
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label9: TLabel
            Left = 8
            Top = 4
            Width = 82
            Height = 13
            Caption = 'Parcelam. inicial: '
          end
          object Label10: TLabel
            Left = 92
            Top = 4
            Width = 75
            Height = 13
            Caption = 'Parcelam. final: '
          end
          object EdParcIni: TdmkEdit
            Left = 8
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdParcFim: TdmkEdit
            Left = 92
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object BitBtn1: TBitBtn
            Tag = 22
            Left = 184
            Top = 2
            Width = 120
            Height = 40
            Caption = '&Rastrear'
            TabOrder = 2
            OnClick = BitBtn1Click
            NumGlyphs = 2
          end
        end
        object DBGrid3: TDBGrid
          Left = 2
          Top = 61
          Width = 325
          Height = 162
          Align = alClient
          DataSource = DsCad
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid3DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end>
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 440
      Height = 246
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitHeight = 240
      object GroupBox1: TGroupBox
        Left = 4
        Top = 56
        Width = 429
        Height = 173
        Caption = ' Registros localizados (do parcelamento pesquisado): '
        TabOrder = 0
        object Label2: TLabel
          Left = 20
          Top = 40
          Width = 136
          Height = 13
          Caption = 'Cabe'#231'alho de parcelamento:'
        end
        object Label3: TLabel
          Left = 20
          Top = 64
          Width = 85
          Height = 13
          Caption = 'Parcelas geradas:'
        end
        object Label4: TLabel
          Left = 20
          Top = 88
          Width = 136
          Height = 13
          Caption = 'Itens de c'#225'lculo de parcelas:'
        end
        object Label5: TLabel
          Left = 20
          Top = 112
          Width = 202
          Height = 13
          Caption = 'Bloquetos de parcelamento n'#227'o exclu'#237'dos:'
        end
        object Label6: TLabel
          Left = 256
          Top = 20
          Width = 26
          Height = 13
          Caption = 'Web:'
        end
        object Label7: TLabel
          Left = 344
          Top = 20
          Width = 29
          Height = 13
          Caption = 'Local:'
        end
        object Label8: TLabel
          Left = 20
          Top = 136
          Width = 192
          Height = 13
          Caption = 'Lan'#231'amentos originais n'#227'o desatrelados:'
        end
        object EdRepaN: TdmkEdit
          Left = 229
          Top = 36
          Width = 80
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdRepaNChange
        end
        object EdRepaL: TdmkEdit
          Left = 317
          Top = 36
          Width = 80
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdRepaNChange
        end
        object EdParcN: TdmkEdit
          Left = 229
          Top = 60
          Width = 80
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdRepaNChange
        end
        object EdParcL: TdmkEdit
          Left = 317
          Top = 60
          Width = 80
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdRepaNChange
        end
        object EdItnsN: TdmkEdit
          Left = 229
          Top = 84
          Width = 80
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdRepaNChange
        end
        object EdItnsL: TdmkEdit
          Left = 317
          Top = 84
          Width = 80
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdRepaNChange
        end
        object EdLctsN: TdmkEdit
          Left = 229
          Top = 108
          Width = 80
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdRepaNChange
        end
        object EdLctsL: TdmkEdit
          Left = 317
          Top = 108
          Width = 80
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdRepaNChange
        end
        object EdAtreN: TdmkEdit
          Left = 229
          Top = 132
          Width = 80
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdRepaNChange
        end
        object EdAtreL: TdmkEdit
          Left = 317
          Top = 132
          Width = 80
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdRepaNChange
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 440
        Height = 52
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 1
        ExplicitTop = 1
        ExplicitWidth = 438
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 71
          Height = 13
          Caption = 'Parcelamento: '
        end
        object EdBloqParc: TdmkEdit
          Left = 80
          Top = 12
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdBloqParcChange
        end
        object BtPesq: TBitBtn
          Tag = 22
          Left = 164
          Top = 5
          Width = 120
          Height = 40
          Caption = '&Pesquisar'
          TabOrder = 1
          OnClick = BtPesqClick
          NumGlyphs = 2
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 441
        Height = 32
        Caption = 'Limpeza de Pacelamentos Exclu'#237'dos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 441
        Height = 32
        Caption = 'Limpeza de Pacelamentos Exclu'#237'dos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 441
        Height = 32
        Caption = 'Limpeza de Pacelamentos Exclu'#237'dos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 531
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 575
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
      object BtCorrige: TBitBtn
        Tag = 11
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Corrigir'
        Enabled = False
        TabOrder = 1
        OnClick = BtCorrigeClick
        NumGlyphs = 2
      end
    end
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM bloqparc'
      'WHERE Codigo=:P0')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1Codigo: TAutoIncField
      FieldName = 'Codigo'
    end
  end
  object QrRepaN: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT *'
      'FROM bloqparc'
      'WHERE Codigo=:P0')
    Left = 424
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrRepaL: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM bloqparc'
      'WHERE Codigo=:P0')
    Left = 484
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrParcN: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT *'
      'FROM bloqparcpar'
      'WHERE Codigo=:P0')
    Left = 424
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrParcL: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM bloqparcpar'
      'WHERE Codigo=:P0')
    Left = 484
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrItnsN: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT *'
      'FROM bloqparcits'
      'WHERE Codigo=:P0')
    Left = 424
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrItnsL: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM bloqparcits'
      'WHERE Codigo=:P0')
    Left = 484
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrLctsN: TmySQLQuery
    Database = Dmod.MyDBn
    AfterOpen = QrLctsNAfterOpen
    SQL.Strings = (
      'SELECT ci.Unidade UH,'
      'IF(la.Cliente>0, '
      '  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),'
      '  IF (la.Fornecedor>0,'
      
        '    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO' +
        ','
      'IF(em.Tipo=0, em.RazaoSocial, em.Nome) NO_CLIINT,'
      ' la.Data, la.Controle, la.FatNum, la.Descricao,'
      'la.Carteira, ca.Nome NO_CART'
      'FROM lct la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN entidades em ON em.Codigo=la.CliInt'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN condimov  ci ON ci.Conta=la.Depto'
      'WHERE la.Genero=-10 '
      'AND la.FatID=610'
      'AND la.Descricao LIKE "%(Reparc. ?xxxxx?)%"')
    Left = 424
    Top = 216
    object QrLctsNControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctsNUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLctsNNOMERELACIONADO: TWideStringField
      FieldName = 'NOMERELACIONADO'
      Size = 100
    end
    object QrLctsNNO_CLIINT: TWideStringField
      FieldName = 'NO_CLIINT'
      Size = 100
    end
    object QrLctsNData: TDateField
      FieldName = 'Data'
    end
    object QrLctsNFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctsNDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctsNCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctsNNO_CART: TWideStringField
      FieldName = 'NO_CART'
      Size = 100
    end
  end
  object QrLctsL: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLctsLAfterOpen
    SQL.Strings = (
      'SELECT ci.Unidade UH,'
      'IF(la.Cliente>0, '
      '  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),'
      '  IF (la.Fornecedor>0,'
      
        '    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO' +
        ','
      'IF(em.Tipo=0, em.RazaoSocial, em.Nome) NO_CLIINT,'
      ' la.Data, la.Controle, la.FatNum, la.Descricao,'
      'la.Carteira, ca.Nome NO_CART'
      'FROM lct la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN entidades em ON em.Codigo=la.CliInt'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN condimov  ci ON ci.Conta=la.Depto'
      'WHERE la.Genero=-10 '
      'AND la.FatID=610'
      'AND la.Descricao LIKE "%(Reparc. ?xxxxx?)%"')
    Left = 484
    Top = 216
    object QrLctsLControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctsLUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLctsLNOMERELACIONADO: TWideStringField
      FieldName = 'NOMERELACIONADO'
      Size = 100
    end
    object QrLctsLNO_CLIINT: TWideStringField
      FieldName = 'NO_CLIINT'
      Size = 100
    end
    object QrLctsLData: TDateField
      FieldName = 'Data'
    end
    object QrLctsLFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctsLDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctsLCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctsLNO_CART: TWideStringField
      FieldName = 'NO_CART'
      Size = 100
    end
  end
  object DsLctsL: TDataSource
    DataSet = QrLctsL
    Left = 512
    Top = 216
  end
  object DsLctsN: TDataSource
    DataSet = QrLctsN
    Left = 452
    Top = 216
  end
  object QrAtreN: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Controle'
      'FROM lct'
      'WHERE Reparcel=:P0')
    Left = 424
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtreNControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrAtreL: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM lct'
      'WHERE Reparcel=:P0')
    Left = 484
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtreLControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrCad: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM cad_0'
      '')
    Left = 724
    Top = 28
    object QrCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCad: TDataSource
    DataSet = QrCad
    Left = 752
    Top = 28
  end
  object QrPesq2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodCliEsp, CodCliEnt'
      'FROM bloqparc'
      'WHERE Codigo=:P0')
    Left = 36
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Codigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrPesq2CodCliEsp: TIntegerField
      FieldName = 'CodCliEsp'
    end
    object QrPesq2CodCliEnt: TIntegerField
      FieldName = 'CodCliEnt'
    end
  end
end
