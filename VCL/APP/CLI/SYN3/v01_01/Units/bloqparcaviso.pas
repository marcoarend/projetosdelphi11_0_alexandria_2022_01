unit bloqparcaviso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmBloqParcAviso = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtLocalizar: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    DBGErrRepa: TDBGrid;
    QrParc: TmySQLQuery;
    DsParc: TDataSource;
    QrParcCodigo: TAutoIncField;
    QrParcUnidade: TWideStringField;
    QrParcNOMECOND: TWideStringField;
    QrParcNOMEPROP: TWideStringField;
    QrParcTipoTabLct: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtLocalizarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FParcelamentos: Integer;
  end;

  var
  FmBloqParcAviso: TFmBloqParcAviso;

implementation

uses Module, UnMyObjects, Principal;

{$R *.DFM}

procedure TFmBloqParcAviso.BtLocalizarClick(Sender: TObject);
begin
  if QrParcTipoTabLct.Value = 1 then
  begin
    //if QrParcCodigo.Value < 0 then
    if QrParcCodigo.Value > 0 then
      FmPrincipal.MostraBloqParc(QrParcCodigo.Value) //Web
    else
      FmPrincipal.ReparcelamentosdeBloquetos_Local(QrParcCodigo.Value); //Local
    //
    QrParc.Close;
    QrParc.Open;
  end else
  begin
    Geral.MensagemBox('O reparcelamento selecionado foi criado em uma vers�o anterior do aplicativo!' +
      #13#10 + 'Finalize o reparcelamento n� ' + FormatFloat('0', QrParcCodigo.Value) +
      ' no seu aplicativo de origem!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmBloqParcAviso.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloqParcAviso.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmBloqParcAviso.FormCreate(Sender: TObject);
begin
  QrParc.Database := Dmod.MyDBn;
end;

procedure TFmBloqParcAviso.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloqParcAviso.FormShow(Sender: TObject);
var
  Msg: String;
begin
  case FParcelamentos of
    0: Msg := '';// Nada
    1: Msg := 'H� um novo parcelamento de d�bitos no site.' + #13#10 +
      ' � necess�rio process�-lo antes de sincronizar!';
    else Msg := 'Existem ' + FormatFloat('0', FParcelamentos) + 
      ' novos parcelamentos de d�bitos no site.' + #13#10 +
      '� recomend�vel process�-los antes de sincronizar!';
  end;
  //
  if Length(Msg) > 0 then
  begin
    LaAviso1.Caption := Msg;
    LaAviso2.Caption := Msg;
  end;
  //
  QrParc.Close;
  QrParc.Open;
end;

end.
