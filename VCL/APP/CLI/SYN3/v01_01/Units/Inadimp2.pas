unit Inadimp2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Grids,
  DBGrids, dmkDBGrid, Mask, ComCtrls, frxClass, frxDBSet, Variants, Menus,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UMySQLModule, dmkGeral,
  dmkEditDateTimePicker, UnDmkProcFunc, dmkImage, DmkDAC_PF, dmkDBGridZTO,
  UnDmkEnums;

type
  TFmInadimp2 = class(TForm)
    Panel1: TPanel;
    QrPropriet: TmySQLQuery;
    DsPropriet: TDataSource;
    QrProprietCodigo: TIntegerField;
    QrProprietNOMEPROP: TWideStringField;
    QrCondImov: TmySQLQuery;
    DsCondImov: TDataSource;
    PnPesq1: TPanel;
    DBGInadimp2: TdmkDBGridZTO;
    QrCondImovConta: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    PnPesq2: TPanel;
    dmkDBGrid1: TdmkDBGridZTO;
    Splitter1: TSplitter;
    QrPesq4: TmySQLQuery;
    DsPesq4: TDataSource;
    QrTot3: TmySQLQuery;
    DsTot3: TDataSource;
    QrPesq4Unidade: TWideStringField;
    QrPesq4Data: TDateField;
    QrPesq4PAGO: TFloatField;
    QrPesq4SALDO: TFloatField;
    QrPesq4Mez: TIntegerField;
    QrPesq4Vencimento: TDateField;
    QrPesq4Compensado: TDateField;
    QrPesq4Controle: TIntegerField;
    QrPesq4Descricao: TWideStringField;
    QrPesq4NOMEEMPCOND: TWideStringField;
    QrPesq4NOMEPRPIMOV: TWideStringField;
    QrTot3VALOR: TFloatField;
    QrTot3PAGO: TFloatField;
    QrTot3SALDO: TFloatField;
    Panel3: TPanel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    EdMezIni: TEdit;
    EdMezFim: TEdit;
    CkNaoMensais: TCheckBox;
    GBEmissao: TGroupBox;
    CkIniDta: TCheckBox;
    TPIniDta: TDateTimePicker;
    CkFimDta: TCheckBox;
    TPFimDta: TDateTimePicker;
    GBVencto: TGroupBox;
    CkIniVct: TCheckBox;
    TPIniVct: TDateTimePicker;
    CkFimVct: TCheckBox;
    TPFimVct: TDateTimePicker;
    CkFimPgt: TCheckBox;
    TPFimPgt: TDateTimePicker;
    Panel5: TPanel;
    Panel6: TPanel;
    Memo1: TMemo;
    Panel7: TPanel;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    frxPend_s0i0: TfrxReport;
    frxDsPesq3: TfrxDBDataset;
    RGGrupos: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    QrCondGri: TmySQLQuery;
    QrCondGriCodigo: TIntegerField;
    QrCondGriNome: TWideStringField;
    DsCondGri: TDataSource;
    PCAgrupamentos: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    QrCondGriImv: TmySQLQuery;
    QrCondGriImvNOMEPROP: TWideStringField;
    QrCondGriImvNOMECOND: TWideStringField;
    QrCondGriImvUNIDADE: TWideStringField;
    QrCondGriImvCodigo: TIntegerField;
    QrCondGriImvControle: TIntegerField;
    QrCondGriImvCond: TIntegerField;
    QrCondGriImvApto: TIntegerField;
    QrCondGriImvPropr: TIntegerField;
    QrCondGriImvLk: TIntegerField;
    QrCondGriImvDataCad: TDateField;
    QrCondGriImvDataAlt: TDateField;
    QrCondGriImvUserCad: TIntegerField;
    QrCondGriImvUserAlt: TIntegerField;
    QrCondGriImvAlterWeb: TSmallintField;
    QrCondGriImvAtivo: TSmallintField;
    DsCondGriImv: TDataSource;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Panel8: TPanel;
    EdCondGri: TdmkEditCB;
    CBCondGri: TDMKDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrPesq4Juros: TFloatField;
    QrPesq4Multa: TFloatField;
    QrPesq4TOTAL: TFloatField;
    QrPesq4FatNum: TFloatField;
    Panel9: TPanel;
    RGSomas: TRadioGroup;
    RGImpressao: TRadioGroup;
    Panel10: TPanel;
    RGDocumentos: TRadioGroup;
    RGSituacao: TRadioGroup;
    QrPesq4PEND_VAL: TFloatField;
    QrPesq4MEZ_TXT: TWideStringField;
    QrPesq4VCTO_TXT: TWideStringField;
    QrPesq4CREDITO: TFloatField;
    PMMulJur: TPopupMenu;
    AlterapercentualdeMulta1: TMenuItem;
    AlterapercentualdeJurosmensais1: TMenuItem;
    QrPesq4CliInt: TIntegerField;
    Panel11: TPanel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdPropriet: TdmkEditCB;
    CBPropriet: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdCondImov: TdmkEditCB;
    CBCondImov: TdmkDBLookupComboBox;
    QrPesq4Propriet: TIntegerField;
    QrPesq4Apto: TIntegerField;
    QrPesq4Imobiliaria: TIntegerField;
    QrPesq4Procurador: TIntegerField;
    QrPesq4Empresa: TIntegerField;
    QrPesq4Usuario: TIntegerField;
    PMImprime: TPopupMenu;
    Pendnciasdecondminos1: TMenuItem;
    Segundaviadebloquetos1: TMenuItem;
    frxCondE2: TfrxReport;
    QrIts2: TmySQLQuery;
    DsIts2: TDataSource;
    QrIts2Genero: TIntegerField;
    QrIts2Descricao: TWideStringField;
    QrIts2Credito: TFloatField;
    QrPesq4Juridico: TSmallintField;
    QrPesq4Juridico_TXT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel12: TPanel;
    BtPesquisa: TBitBtn;
    BtImprime: TBitBtn;
    BtCertidao: TBitBtn;
    BtMulJur: TBitBtn;
    BtDiarioAdd: TBitBtn;
    BtReparc: TBitBtn;
    QrPesq4Juridico_DESCRI: TWideStringField;
    QrBloqInad: TmySQLQuery;
    QrBloqInadEmpresa: TIntegerField;
    QrBloqInadPropriet: TIntegerField;
    QrBloqInadUnidade: TWideStringField;
    QrBloqInadApto: TIntegerField;
    QrBloqInadImobiliaria: TIntegerField;
    QrBloqInadProcurador: TIntegerField;
    QrBloqInadUsuario: TIntegerField;
    QrBloqInadData: TDateField;
    QrBloqInadCliInt: TIntegerField;
    QrBloqInadCREDITO: TFloatField;
    QrBloqInadPAGO: TFloatField;
    QrBloqInadJuros: TFloatField;
    QrBloqInadMulta: TFloatField;
    QrBloqInadTOTAL: TFloatField;
    QrBloqInadSALDO: TFloatField;
    QrBloqInadPEND_VAL: TFloatField;
    QrBloqInadDescricao: TWideStringField;
    QrBloqInadMez: TIntegerField;
    QrBloqInadMEZ_TXT: TWideStringField;
    QrBloqInadVencimento: TDateField;
    QrBloqInadCompensado: TDateField;
    QrBloqInadControle: TIntegerField;
    QrBloqInadFatNum: TFloatField;
    QrBloqInadVCTO_TXT: TWideStringField;
    QrBloqInadNOMEEMPCOND: TWideStringField;
    QrBloqInadNOMEPRPIMOV: TWideStringField;
    QrBloqInadAtivo: TSmallintField;
    QrBloqInadNewVencto: TDateField;
    BtCobranca: TBitBtn;
    QrIts2Controle: TIntegerField;
    QrInadimp2: TmySQLQuery;
    QrInadimp2Empresa: TIntegerField;
    QrInadimp2ForneceI: TIntegerField;
    QrInadimp2Apto: TIntegerField;
    QrInadimp2Mez: TIntegerField;
    QrInadimp2Vencimento: TDateField;
    QrInadimp2Propriet: TIntegerField;
    QrInadimp2Unidade: TWideStringField;
    QrInadimp2Imobiliaria: TIntegerField;
    QrInadimp2Procurador: TIntegerField;
    QrInadimp2Usuario: TIntegerField;
    QrInadimp2Juridico: TSmallintField;
    QrInadimp2Data: TDateField;
    QrInadimp2CliInt: TIntegerField;
    QrInadimp2CREDITO: TFloatField;
    QrInadimp2PAGO: TFloatField;
    QrInadimp2Juros: TFloatField;
    QrInadimp2Multa: TFloatField;
    QrInadimp2TOTAL: TFloatField;
    QrInadimp2SALDO: TFloatField;
    QrInadimp2PEND_VAL: TFloatField;
    QrInadimp2Descricao: TWideStringField;
    QrInadimp2MEZ_TXT: TWideStringField;
    QrInadimp2Compensado: TDateField;
    QrInadimp2Controle: TIntegerField;
    QrInadimp2VCTO_TXT: TWideStringField;
    QrInadimp2NOMEEMPCOND: TWideStringField;
    QrInadimp2NOMEPRPIMOV: TWideStringField;
    QrInadimp2NewVencto: TDateField;
    QrInadimp2Infl1Indice: TWideStringField;
    QrInadimp2Infl1PeriI: TIntegerField;
    QrInadimp2Infl1PeriF: TIntegerField;
    QrInadimp2Infl1FatorI: TFloatField;
    QrInadimp2Infl1FatorF: TFloatField;
    QrInadimp2Infl1ValorT: TFloatField;
    QrInadimp2Infl1Percen: TFloatField;
    QrInadimp2Infl1ValorS: TFloatField;
    QrInadimp2Infl2Indice: TWideStringField;
    QrInadimp2Infl2PeriI: TIntegerField;
    QrInadimp2Infl2PeriF: TIntegerField;
    QrInadimp2Infl2FatorI: TFloatField;
    QrInadimp2Infl2FatorF: TFloatField;
    QrInadimp2Infl2ValorT: TFloatField;
    QrInadimp2Infl2Percen: TFloatField;
    QrInadimp2Infl2ValorS: TFloatField;
    QrInadimp2InflTValorS: TFloatField;
    QrInadimp2Ativo: TSmallintField;
    DsInadimp2: TDataSource;
    QrInadimp2Juridico_TXT: TWideStringField;
    QrInadimp2Juridico_DESCRI: TWideStringField;
    QrCfgInfl: TmySQLQuery;
    QrCfgInflCfgInfl: TIntegerField;
    QrCfgInflIndic1: TSmallintField;
    QrCfgInflPercent1: TFloatField;
    QrCfgInflMesAnt1: TSmallintField;
    QrCfgInflIndic2: TSmallintField;
    QrCfgInflPercent2: TFloatField;
    QrCfgInflMesAnt2: TSmallintField;
    QrCfgInflNome: TWideStringField;
    QrIndice: TmySQLQuery;
    QrIndiceFator: TFloatField;
    PB1: TProgressBar;
    QrPesq4CNAB_Cfg: TIntegerField;
    QrPesq4FatID: TIntegerField;
    QrBloqInadFatID: TIntegerField;
    QrInadimp2FatNum: TFloatField;
    QrInadimp2FatID: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdProprietChange(Sender: TObject);
    procedure EdCondImovChange(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure EdMezIniExit(Sender: TObject);
    procedure EdMezIniKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMezFimExit(Sender: TObject);
    procedure EdMezFimKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrTot3CalcFields(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxPend_s0i0GetValue(const VarName: String;
      var Value: Variant);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure RGOrdem3Click(Sender: TObject);
    procedure RGOrdem4Click(Sender: TObject);
    procedure EdCondGriChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtCertidaoClick(Sender: TObject);
    procedure BtMulJurClick(Sender: TObject);
    procedure AlterapercentualdeJurosmensais1Click(Sender: TObject);
    procedure AlterapercentualdeMulta1Click(Sender: TObject);
    procedure BtDiarioAddClick(Sender: TObject);
    procedure DBGInadimp2DblClick(Sender: TObject);
    procedure BtReparcClick(Sender: TObject);
    procedure Pendnciasdecondminos1Click(Sender: TObject);
    procedure Segundaviadebloquetos1Click(Sender: TObject);
    procedure QrPesq4CalcFields(DataSet: TDataSet);
    procedure EdEmpresaExit(Sender: TObject);
    procedure EdEmpresaEnter(Sender: TObject);
    procedure BtCobrancaClick(Sender: TObject);
    procedure EdCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrInadimp2AfterScroll(DataSet: TDataSet);
    procedure QrInadimp2BeforeClose(DataSet: TDataSet);
    procedure QrInadimp2CalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FTabLctA, FTabAriA, FTabCnsA: String;
    FDtaPgt: String;
    FInadim2: String;
    FTemDtP: Boolean;
    FCondAnt: Integer;
    function  AtualizaCorrecaoMonetariaDoItemAtual(
              Nome1: String; Indic1, MesAnt1, PerioF1: Integer;
              FatorF1, Percen1: Double;
              Nome2: String; Indic2, MesAnt2, PerioF2: Integer;
              FatorF2, Percen2: Double): Boolean;
    procedure AvisoTotal(Aviso: String);
    procedure ReopenPropriet(Todos: Boolean);
    procedure ReopenCondImov();
    procedure ComplementaQuery3(Query: TMySQLQuery);
    procedure ReopenCondGriImv(Controle: Integer);
    procedure SetaEImprimeFrx(Frx: TfrxReport; Titulo: String);
    procedure MostraDiarioAdd();
    procedure ConfiguraOutroCondominio();
    procedure RefreshEmpresaSelecionada();
    procedure DefineCondImovPeloCodigo(Key: Integer);
    function  DefineFatoresInflacionarios(const Data: TDateTime;
              const Indice1, Indice2, MesAnt1, MesAnt2: Integer;
              var PerioX1, PerioX2: Integer;
              var FatorX1, FatorX2: Double): Boolean;
  public
    { Public declarations }
  end;

  var
  FmInadimp2: TFmInadimp2;

implementation

uses Module, Principal, CondGri, UnInternalConsts, MyDBCheck, CertidaoNeg,
  DiarioAdd, Reparc, UnFinanceiro, CreateApp, ModuleGeral, InadimpBloq,
  UnMyObjects, ModuleCond, CobrarGer, ModCobranca, GetValor, InflIdx_Tabs;

{$R *.DFM}

var
  HEADERS: array of String;

const
  Ordens: array[0..5] of String = ('Vencimento', 'NOMEEMPCOND', 'NOMEPRPIMOV', 'Unidade', 'Mez', 'Data');
  _Labels_: array[0..5] of String = ('Vencimento', 'NOMEEMPCOND', 'NOMEPRPIMOV', 'Unidade', 'MEZ_TXT', 'Data');
                                     
procedure TFmInadimp2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmInadimp2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmInadimp2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmInadimp2.EdEmpresaChange(Sender: TObject);
begin
  if not EdEmpresa.Focused then
    ConfiguraOutroCondominio();
end;

procedure TFmInadimp2.EdEmpresaEnter(Sender: TObject);
begin
  FCondAnt := EdEmpresa.ValueVariant;
end;

procedure TFmInadimp2.EdEmpresaExit(Sender: TObject);
begin
  if FCondAnt <> EdEmpresa.ValueVariant then
  begin
    FCondAnt := EdEmpresa.ValueVariant;
    ConfiguraOutroCondominio();
  end;
end;

procedure TFmInadimp2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FCondAnt := 0;
  SetLength(Headers, 6);
  HEADERS[0] := 'Vencimento';
  HEADERS[1] := DModG.ReCaptionTexto(VAR_C_O_N_D_O_M_I_N_I_O);
  HEADERS[2] := DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O);
  HEADERS[3] := DModG.ReCaptionTexto(VAR_U_H_LONGO);
  HEADERS[4] := 'Compet�ncia';
  HEADERS[5] := 'Emiss�o';
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  UMyMod.AbreQuery(QrPropriet, Dmod.MyDB);
  UMyMod.AbreQuery(QrCondImov, Dmod.MyDB);
  UMyMod.AbreQuery(QrCondGri, Dmod.MyDB);
  //
  TPIniDta.Date := Date;
  TPFimDta.Date := Date;
  //
  TPIniVct.Date := Date;
  TPFimVct.Date := Date;
  //
  TPFimPgt.Date := Date;
  //
  PCAgrupamentos.ActivePageIndex := 0;
  //
  CkFimPgt.Visible  := True;
  TPFimPgt.Visible  := True;
  //
  CkIniVct.Checked := False;
  CkIniDta.Checked := False;
  CkFimDta.Checked := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'D� um duplo clique na grade para adicionar um evento ao di�rio!');
end;

procedure TFmInadimp2.EdProprietChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
  ReopenCondImov;
end;

procedure TFmInadimp2.EdCondImovChange(Sender: TObject);
begin
  PnPesq1.Visible   := False;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp2.EdCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  DefineCondImovPeloCodigo(Key);
end;

procedure TFmInadimp2.ReopenPropriet(Todos: Boolean);
var
  Cond: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    EdPropriet.Text := '';
    CBPropriet.KeyValue := Null;
    //
    Cond := EdEmpresa.ValueVariant;
    //
    if (Cond <> 0) or Todos then
    begin
      QrPropriet.Close;
      QrPropriet.SQL.Clear;
      QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
      QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
      QrPropriet.SQL.Add('FROM entidades ent');
      QrPropriet.SQL.Add('LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet');
      QrPropriet.SQL.Add('WHERE ent.Cliente2="V"');
      QrPropriet.SQL.Add('');
      //
      if (Cond <> 0) and not Todos then
        QrPropriet.SQL.Add('AND cim.Codigo=' + Geral.FF0(Cond));
      //
      QrPropriet.SQL.Add('');
      QrPropriet.SQL.Add('ORDER BY NOMEPROP');
      UMyMod.AbreQuery(QrPropriet, Dmod.MyDB);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmInadimp2.ReopenCondImov();
var
  Cond, Propriet: Integer;
begin
  EdCondImov.Text := '';
  CBCondImov.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  Propriet := Geral.IMV(EdPropriet.Text);
  QrCondImov.Close;
  QrCondImov.SQL.Clear;
  QrCondImov.SQL.Add('SELECT cim.Conta, cim.Unidade');
  QrCondImov.SQL.Add('FROM condimov cim');
  QrCondImov.SQL.Add('WHERE cim.Codigo<>0');
  QrCondImov.SQL.Add('AND cim.Propriet<>0');
  QrCondImov.SQL.Add('');
  if Cond <> 0 then
    QrCondImov.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  if Propriet <> 0 then
    QrCondImov.SQL.Add('AND cim.Propriet=' + IntToStr(Propriet));
  QrCondImov.SQL.Add('');
  QrCondImov.SQL.Add('ORDER BY cim.Unidade');
  //
  UMyMod.AbreQuery(QrCondImov, Dmod.MyDB);
end;

procedure TFmInadimp2.EdMezIniExit(Sender: TObject);
begin
  EdMezIni.Text := MLAGeral.TST(EdMezIni.Text, False);
end;

procedure TFmInadimp2.EdMezIniKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Periodo: Integer;
begin
  if key in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
  begin
    if (EdMezIni.Text = '') or (key=VK_F5) then
      Periodo := Geral.Periodo2000(Date)
    else begin
      Periodo := dmkPF.MensalToPeriodo(EdMezIni.Text);
      if key=VK_DOWN then Periodo := Periodo -1;
      if key=VK_UP   then Periodo := Periodo +1;
      if key=VK_F4   then Periodo := Periodo -1;
      if key=VK_F5   then Periodo := Periodo   ;
    end;
    EdMezIni.Text := dmkPF.PeriodoToMensal(Periodo);
  end;
end;

procedure TFmInadimp2.EdMezFimExit(Sender: TObject);
begin
  EdMezFim.Text := MLAGeral.TST(EdMezFim.Text, False);
end;

procedure TFmInadimp2.EdMezFimKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Periodo: Integer;
begin
  if key in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
  begin
    if (EdMezFim.Text = '') or (key=VK_F5) then
      Periodo := Geral.Periodo2000(Date)
    else begin
      Periodo := dmkPF.MensalToPeriodo(EdMezFim.Text);
      if key=VK_DOWN then Periodo := Periodo -1;
      if key=VK_UP   then Periodo := Periodo +1;
      if key=VK_F4   then Periodo := Periodo -1;
      if key=VK_F5   then Periodo := Periodo   ;
    end;
    EdMezFim.Text := dmkPF.PeriodoToMensal(Periodo);
  end;
end;

procedure TFmInadimp2.BtPesquisaClick(Sender: TObject);
const
  sCorrMonet = 'Pesquisando boletos.';
var
  CondCod, CodEnt, Propriet, CondImov, PeriodoAtual, PeriodoFina1,
  PeriodoFina2: Integer;
  FatorF1, FatorF2: Double;
  Nome1, Nome2, sMax: String;
  Indic1, MesAnt1, Indic2, MesAnt2, PerioF1, PerioF2: Integer;
  Percen1, Percen2: Double;
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
  'Informe o Condom�nio!') then
    Exit;
  Screen.Cursor := crHourGlass;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando boletos');
  DBGInadimp2.DataSource := nil;
  Memo1.Lines.Clear;
  //
  FInadim2  := UnCreateApp.RecriaTempTableNovo(ntrtt_Inadim2, DmodG.QrUpdPID1, False);
  QrPesq4.Close;
  CondCod  := Geral.IMV(EdEmpresa.Text);
  CodEnt   := DModG.QrEmpresasCodigo.Value;
  Propriet := Geral.IMV(EdPropriet.Text);
  CondImov := Geral.IMV(EdCondImov.Text);
  //
  QrPesq4.SQL.Text := 'INSERT INTO ' + VAR_MyPID_DB_NOME + '.' + FInadim2 + sLineBreak +
  DmModCobranca.Pesquisa4_InadimpDeCond(FTabLctA,
  CondCod, CodEnt, Propriet, CondImov,
  RGDocumentos.ItemIndex, PCAgrupamentos.ActivePageIndex,
  QrCondGriImv,
  EdMezIni.Text, EdMezFim.Text,
  CkNaoMensais.Checked,
  TPIniDta.Date, TPFimDta.Date, CkIniDta.Checked, CkFimDta.Checked, GBEmissao.Visible,
  TPIniVct.Date, TPFimVct.Date, CkIniVct.Checked, CkFimVct.Checked, GBVencto.Visible,
  CkFimPgt.Checked,
  TPFimPgt.Date,
  RGSomas.ItemIndex, RGSituacao.ItemIndex,
  RGOrdem1.ItemIndex, RGOrdem2.ItemIndex, RGOrdem3.ItemIndex, RGOrdem4.ItemIndex);
  //
  UnDmkDAC_PF.ExecutaQuery(QrPesq4, DModG.MyPID_DB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrInadimp2, DModG.MyPID_DB, [
  'SELECT * FROM ' + FInadim2,
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando boletos.');

  //  Atualizar corre��o monet�ria
  UnDmkDAC_PF.AbreMySQLQuery0(QrCfgInfl, Dmod.MyDB, [
  'SELECT cnd.CfgInfl, ',
  'nfl.Indic1, nfl.Percent1, nfl.MesAnt1,',
  'nfl.Indic2, nfl.Percent2, nfl.MesAnt2,',
  'nfl.Nome',
  'FROM cond cnd',
  'LEFT JOIN cfginfl nfl ON nfl.Codigo=cnd.CfgInfl',
  'WHERE cnd.Codigo=' + Geral.FF0(CondCod),
  '']);
  if QrCfgInflCfgInfl.Value <> 0 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, sCorrMonet);
    //
    if not DefineFatoresInflacionarios(Date,
    QrCfgInflIndic1.Value, QrCfgInflIndic2.Value,
    QrCfgInflMesAnt1.Value, QrCfgInflMesAnt2.Value,
    PerioF1, PerioF2, FatorF1, FatorF2) then
      Exit;
    //
    Nome1 := sLista_INDICE_INFLACIO[QrCfgInflIndic1.Value];
    Nome2 := sLista_INDICE_INFLACIO[QrCfgInflIndic2.Value];

    Indic1  := QrCfgInflIndic1.Value;
    MesAnt1 := QrCfgInflMesAnt1.Value;
    Percen1 := QrCfgInflPercent1.Value;

    Indic2  := QrCfgInflIndic2.Value;
    MesAnt2 := QrCfgInflMesAnt2.Value;
    Percen2 := QrCfgInflPercent2.Value;

    if QrCfgInflCfgInfl.Value > 0 then
    begin
      PB1.Position := 0;
      PB1.Max := QrInadimp2.RecordCount;
      sMax := ' de ' + Geral.FF0(QrInadimp2.RecordCount);
      QrInadimp2.First;
      while not QrInadimp2.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, sCorrMonet +
        ' Item ' + Geral.FF0(QrInadimp2.RecNo) + sMax);
        //
        if not AtualizaCorrecaoMonetariaDoItemAtual(
        Nome1, Indic1, MesAnt1, PerioF1, FatorF1, Percen1,
        Nome2, Indic2, MesAnt2, PerioF2, FatorF2, Percen2) then
          Exit;
        //
        QrInadimp2.Next;
      end;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrInadimp2, DModG.MyPID_DB, [
    'SELECT * FROM ' + FInadim2,
    '']);
  end;  // Fim corre��o monet�ria!
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Finalizando');
  BtImprime.Enabled := QrInadimp2.RecordCount > 0;
  BtMulJur.Enabled := QrInadimp2.RecordCount > 0;
  //
  if PnPesq1 <> nil then
    PnPesq1.Visible := True;
  //
  DBGInadimp2.DataSource := DsInadimp2;
  DBEdit1.DataSource := DsTot3;
  DBEdit2.DataSource := DsTot3;
  DBEdit3.DataSource := DsTot3;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  PB1.Position := 0;
  Screen.Cursor := crDefault;
end;

procedure TFmInadimp2.BtReparcClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmReparc, FmReparc, afmoNegarComAviso) then
  begin
    FmReparc.EdEmpresa.ValueVariant  := EdEmpresa.ValueVariant;
    FmReparc.CBEmpresa.KeyValue      := CBEmpresa.KeyValue;
    FmReparc.EdPropriet.ValueVariant := QrInadimp2Propriet.Value;
    FmReparc.CBPropriet.KeyValue     := QrInadimp2Propriet.Value;
    FmReparc.EdCondImov.ValueVariant := EdCondImov.ValueVariant;
    FmReparc.CBCondImov.KeyValue     := CBCondImov.KeyValue;
    //
    FmReparc.ShowModal;
    FmReparc.Destroy;

    //
    //  Evitar erro caso empresa mudou!
    RefreshEmpresaSelecionada();
  end;
end;

{
procedure TFmInadimp.ComplementaQuery1(Query: TMySQLQuery);
var
  CondCod, CondEnt, Propriet, Apto: Integer;
begin
  CondCod := Geral.IMV(EdEmpresa.Text);
  if CondCod <> 0 then CondEnt := QrEmpresasCodEnti.Value else CondEnt := 0;
  Propriet := Geral.IMV(EdPropriet.Text);
  Apto := Geral.IMV(EdCondImov.Text);
  //
  if CondEnt > 0 then
    Query.SQL.Add('AND car.ForneceI=' + IntToStr(CondEnt));
  if Propriet > 0 then
    Query.SQL.Add('AND lan.ForneceI=' + IntToStr(Propriet));
  if Apto > 0 then
    Query.SQL.Add('AND lan.Depto=' + IntToStr(Apto));


  Query.SQL.Add(dmkPF.SQL_Mensal('lan.Mez', EdMezIni.Text, EdMezFim.Text,
    CkNaoMensais.Checked));

  Query.SQL.Add(dmkPF.SQL_Periodo('AND lan.Data ', TPIniDta.Date,
    TPFimDta.Date, CkIniDta.Checked, CkFimDta.Checked));

  Query.SQL.Add(dmkPF.SQL_Periodo('AND lan.Vencimento ', TPIniVct.Date,
    TPFimVct.Date, CkIniVct.Checked, CkFimVct.Checked));


  Query.SQL.Add('');
  Query.SQL.Add('');


end;
}

procedure TFmInadimp2.ComplementaQuery3(Query: TMySQLQuery);
var
  C1, P1, A1, CondCod, CondEnt, Propriet, Apto: Integer;
  Txt, Lig: String;
begin
  Query.SQL.Add('FROM ' + FTabLctA + ' lan');
  Query.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  Query.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
  Query.SQL.Add('LEFT JOIN entidades emp ON emp.Codigo=car.ForneceI');
  Query.SQL.Add('LEFT JOIN entidades prp ON prp.Codigo=lan.ForneceI');
  Query.SQL.Add('');
  //
  //Query.SQL.Add('WHERE lan.FatID in (' + FmPrincipal.GetFatIDs + ')');
  case RGDocumentos.ItemIndex of
    0: Query.SQL.Add('WHERE lan.FatID in (600,601)');
    1: Query.SQL.Add('WHERE lan.FatID in (610)');
    2: Query.SQL.Add('WHERE lan.FatID in (600,601,610)');
  end;
  //
  // Reparcelados n�o s�o d�bitos
  Query.SQL.Add('AND lan.Reparcel=0');
  Query.SQL.Add('AND lan.Tipo=2');
  Query.SQL.Add('');

  //

  if PCAgrupamentos.ActivePageIndex = 0 then
  begin
    CondCod := Geral.IMV(EdEmpresa.Text);
    if CondCod <> 0 then
      CondEnt := DModG.QrEmpresasCodigo.Value
    else
      CondEnt := 0;
    Propriet := Geral.IMV(EdPropriet.Text);
    Apto := Geral.IMV(EdCondImov.Text);
    //
    if CondEnt > 0 then
      Query.SQL.Add('AND car.ForneceI=' + IntToStr(CondEnt));
    if Propriet > 0 then
      Query.SQL.Add('AND lan.ForneceI=' + IntToStr(Propriet));
    if Apto > 0 then
      Query.SQL.Add('AND lan.Depto=' + IntToStr(Apto));
  end else if (QrCondGriImv.State = dsBrowse) and (QrCondGriImv.RecordCount > 0) then begin
    Txt := 'AND (';
    QrCondGriImv.First;
    while not QrCondGriImv.Eof do
    begin
      if QrCondGriImv.RecNo > 1 then
        Txt := Txt + Chr(13) + Chr(10) + '  OR' + Chr(13) + Chr(10);
      //
      C1 := QrCondGriImvCond.Value;
      P1 := QrCondGriImvPropr.Value;
      A1 := QrCondGriImvApto.Value;
      Lig := '';
      if C1 <> 0 then
      begin
        Txt := Txt + 'imv.Codigo=' + IntToStr(C1);
        Lig := ' AND ';
      end;
      if P1 <> 0 then
      begin
        Txt := Txt + Lig + 'prp.Codigo=' + IntToStr(P1);
        Lig := ' AND ';
      end;
      if A1 <> 0 then
      begin
        Txt := Txt + Lig + 'imv.Conta=' + IntToStr(A1);
        //Lig := ' AND ';
      end;
      //
      Txt := Txt + Chr(13) + Chr(10);
      QrCondGriImv.Next;
    end;
    Txt := Txt + ')';
  end;
  Query.SQL.Add(Txt);
  //

  Query.SQL.Add(dmkPF.SQL_Mensal('lan.Mez', EdMezIni.Text, EdMezFim.Text,
    CkNaoMensais.Checked));

  Query.SQL.Add(dmkPF.SQL_Periodo('AND lan.Data ', TPIniDta.Date,
    TPFimDta.Date,
    CkIniDta.Checked and GBEmissao.Visible,
    CkFimDta.Checked and GBEmissao.Visible));

  Query.SQL.Add(dmkPF.SQL_Periodo('AND lan.Vencimento ', TPIniVct.Date,
    TPFimVct.Date,
    CkIniVct.Checked and GBVencto.Visible,
    CkFimVct.Checked and GBVencto.Visible));

  (* Data que deseja ver os d�bitos ou hoje se n�o definido*)
  if FTemDtP then
    Query.SQL.Add(
    'AND (' +
    '  (lan.Compensado > "' + FDtaPgt + '")' +
    '  OR (lan.Compensado < 2)' +
    '  )')
  else
    Query.SQL.Add(
    'AND ' +
    '   (lan.Compensado < 2)' +
    '  ');


  Query.SQL.Add('');
  Query.SQL.Add('');


end;

procedure TFmInadimp2.ConfiguraOutroCondominio();
begin
  FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, DModG.QrEmpresasFilial.Value);
  FTabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, DModG.QrEmpresasFilial.Value);
  FTabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, DModG.QrEmpresasFilial.Value);
  //
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
  ReopenPropriet(False);
  ReopenCondImov();
end;

procedure TFmInadimp2.DefineCondImovPeloCodigo(Key: Integer);
var
  CondImov: Variant;
begin
  if Key = VK_F4 then
  begin
    CondImov := 0;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, CondImov, 0, 0,
    '', '', True, 'Reduzido', 'Informe o Reduzido: ', 0, CondImov) then
    begin
      if QrCondImov.Locate('Conta', CondImov, []) then
      begin
        EdCondImov.ValueVariant := CondImov;
        CBCondImov.KeyValue := CondImov;
      end else
        Geral.MB_Aviso('O im�vel c�digo ' + Geral.FF0(CondImov) +
        ' n�o existe ou n�o pertence ao condom�nio selecionado!');
    end;
  end else
  if key = VK_DELETE then
  begin
    EdCondImov.Text := '';
    CBCondImov.KeyValue := Null;
  end;
end;

function TFmInadimp2.DefineFatoresInflacionarios(const Data: TDateTime;
  const Indice1, Indice2, MesAnt1, MesAnt2: Integer;
  var PerioX1, PerioX2: Integer;
  var FatorX1, FatorX2: Double): Boolean;
  function DefineFatorX(const IndiceX: Integer; var PeriodoX: Integer; var FatorX: Double): Boolean;
  var
    Mes, Aviso: String;
  begin
    Result := False;
    QrIndice.Close;
    //
    case IndiceX of
      {0}CO_INDICE_INFLACIO_COD_NENHUM: ; // Nada
      {1}CO_INDICE_INFLACIO_COD_INPC:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrIndice, Dmod.MyDB, [
        'SELECT INPC_D Fator ',
        'FROM inpc ',
        'WHERE AnoMes=' + Geral.FF0(PeriodoX),
        '']);
      end;
      {2}CO_INDICE_INFLACIO_COD_IGPDI:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrIndice, Dmod.MyDB, [
        'SELECT IGPDI_D Fator ',
        'FROM igpdi ',
        'WHERE AnoMes=' + Geral.FF0(PeriodoX),
        '']);
      end
      else
      begin
        AvisoTotal('�ndice inflacion�rio n�o implementado: ' +
          Geral.FF0(QrCfgInflIndic1.Value));
        //
        Exit;
      end;
    end;
    if (QrIndice.State = dsInactive) or (QrIndice.RecordCount = 0) then
    begin
      Mes := Geral.FDT(Geral.PeriodoToDate(PeriodoX, 1, False), 17);
      AvisoTotal('Per�odo sem fator definido!' + sLineBreak +
      'Per�odo: ' + Mes + sLineBreak +
      '�ndice: ' + sLista_INDICE_INFLACIO[IndiceX]);
    end else
    begin
      FatorX := QrIndiceFator.Value;
      Result := FatorX > 0;
      if not Result then
        AvisoTotal('Fator n�o pode ser igual ou inferir a zero!' + sLineBreak +
        'Per�odo: ' + Mes + sLineBreak +
        '�ndice: ' + sLista_INDICE_INFLACIO[IndiceX]);
    end;
  end;
var
  PeriodoAtual: Integer;
begin
  Result := False;
  PeriodoAtual := Geral.Periodo2000(Data);
  PerioX1 := PeriodoAtual - MesAnt1;
  PerioX2 := PeriodoAtual - MesAnt2;
  //
  Result := DefineFatorX(Indice1, PerioX1, FatorX1);
  if Result then
    Result := DefineFatorX(Indice2, PerioX2, FatorX2);
end;

procedure TFmInadimp2.DBGInadimp2DblClick(Sender: TObject);
begin
  MostraDiarioAdd();
end;

procedure TFmInadimp2.Pendnciasdecondminos1Click(Sender: TObject);
begin
  SetaEImprimeFrx(frxPend_s0i0, 'Pend�ncias de cond�minos');
end;

procedure TFmInadimp2.CBCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  DefineCondImovPeloCodigo(Key);
end;

procedure TFmInadimp2.QrTot3CalcFields(DataSet: TDataSet);
begin
  QrTot3SALDO.Value := QrTot3VALOR.Value - QrTot3PAGO.Value;
end;

procedure TFmInadimp2.AlterapercentualdeJurosmensais1Click(Sender: TObject);
var
  PercNum: Double;
  PercTxt: String;
begin
  PercTxt := '1,00';
  if InputQuery('Novo Percentual de Juros Mensais',
  'Informe o novo percentual de juros mensais', PercTxt) then
  begin
    PercNum := Geral.DMV(PercTxt);
    PercTxt := Geral.FFT(PercNum, 2, siPositivo);
    if Geral.MensagemBox('Confirma a altera��o do percentual de juros ' +
    'mensais de todos itens pesquisados para ' + PercTxt + '%?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        QrInadimp2.First;
        while not QrInadimp2.Eof do
        begin
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
            'MoraDia'], ['CliInt', 'FatNum', 'Data'], [PercNum], [
            QrInadimp2CliInt.Value, QrInadimp2FatNum.Value,
            Geral.FDT(QrInadimp2Data.Value, 1)], True, '', FTabLctA);
          //
          QrInadimp2.Next;
        end;
        QrInadimp2.Close;
        //UMyMod.AbreQuery(QrInadimp2, Dmod.MyDB);
      finally
        Screen.Cursor := crDefault;
      end;
      BtPesquisaClick(Sender);
    end;
  end;
end;

procedure TFmInadimp2.AlterapercentualdeMulta1Click(Sender: TObject);
var
  PercNum: Double;
  PercTxt: String;
begin
  PercTxt := '2,00';
  if InputQuery('Novo Percentual de Multa',
  'Informe o novo percentual multa:', PercTxt) then
  begin
    PercNum := Geral.DMV(PercTxt);
    PercTxt := Geral.FFT(PercNum, 2, siPositivo);
    if Geral.MensagemBox('Confirma a altera��o do percentual de ' +
    'multa de todos itens pesquisados para ' + PercTxt + '%?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        QrInadimp2.First;
        while not QrInadimp2.Eof do
        begin
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
            'MoraDia'], ['CliInt', 'FatNum', 'Data'], [PercNum], [
            QrInadimp2CliInt.Value, QrInadimp2FatNum.Value,
            Geral.FDT(QrInadimp2Data.Value, 1)], True, '', FTabLctA);
          //
          QrInadimp2.Next;
        end;
        QrInadimp2.Close;
        //UMyMod.AbreQuery(QrInadimp2, Dmod.MyDB);
      finally
        Screen.Cursor := crDefault;
        BtPesquisaClick(Sender);
      end;
    end;
  end;
end;

function TFmInadimp2.AtualizaCorrecaoMonetariaDoItemAtual(
Nome1: String; Indic1, MesAnt1, PerioF1: Integer; FatorF1, Percen1: Double;
Nome2: String; Indic2, MesAnt2, PerioF2: Integer; FatorF2, Percen2: Double):
Boolean;
var
  Infl1Indice, Infl2Indice: String;
  PeriodoAtual,
  Infl1PeriI, Infl1PeriF, Infl2PeriI, Infl2PeriF: Integer;
  Infl1FatorI, Infl1FatorF, Infl1ValorT, Infl1Percen, Infl1ValorS,
  Infl2FatorI, Infl2FatorF, Infl2ValorT, Infl2Percen, Infl2ValorS,
  InflTValorS: Double;
  //
  Data: TDateTime;
  PerioI1, PerioI2: Integer;
  FatorI1, FatorI2,
  ValorT1, ValorS1,
  ValorT2, ValorS2,
  FatNum, CREDITO, MULTA, JUROS, TOTAL, PAGO, SALDO, PEND_VAL: Double;
  Empresa, ForneceI, Apto: Integer;
  Vencimento: String;
begin
  Result := False;
  //
  Infl1Indice    := '';
  Infl1PeriI     := 0;
  Infl1PeriF     := 0;
  Infl1FatorI    := 0.000;
  Infl1FatorF    := 0.000;
  Infl1ValorT    := 0.000;
  Infl1Percen    := 0.000000;
  Infl1ValorS    := 0.000;

  Infl2Indice    := '';
  Infl2PeriI     := 0;
  Infl2PeriF     := 0;
  Infl2FatorI    := 0.00;;
  Infl2FatorF    := 0.000;
  Infl2ValorT    := 0.000;
  Infl2Percen    := 0.000000;
  Infl2ValorS    := 0.000;

  InflTValorS    := 0.00;

  PeriodoAtual := Geral.Periodo2000(Date);

  Data := QrInadimp2Vencimento.Value;
  //
  if not DefineFatoresInflacionarios(Data, Indic1, Indic2, MesAnt1, MesAnt2,
  PerioI1, PerioI2, FatorI1, FatorI2) then
    Exit;
  //
  CREDITO := QrInadimp2CREDITO.Value;
  //
  if Indic1 <> 0 then
  begin
    ValorT1 := ((Credito / FatorI1) * FatorF1) - Credito;
    ValorS1 := ValorT1 * Percen1 / 100;
    //
    Infl1Indice    := Nome1;
    Infl1PeriI     := PerioI1;
    Infl1PeriF     := PerioF1;
    Infl1FatorI    := FatorI1;
    Infl1FatorF    := FatorF1;
    Infl1ValorT    := ValorT1;
    Infl1Percen    := Percen1;
    Infl1ValorS    := ValorS1;
  end;
  if Indic1 <> 0 then
  begin
    ValorT2 := ((Credito / FatorI2) * FatorF2) - Credito;
    ValorS2 := ValorT2 * Percen2 / 100;
    //
    Infl2Indice    := Nome2;
    Infl2PeriI     := PerioI2;
    Infl2PeriF     := PerioF2;
    Infl2FatorI    := FatorI2;
    Infl2FatorF    := FatorF2;
    Infl2ValorT    := ValorT2;
    Infl2Percen    := Percen2;
    Infl2ValorS    := ValorS2;
  end;
  InflTValorS := ValorS1 + ValorS2;
  //
  MULTA := QrInadimp2Multa.Value;
  JUROS := QrInadimp2Juros.Value;
  PAGO  := QrInadimp2PAGO.Value;
  //
  TOTAL := InflTValorS + (CREDITO + JUROS + MULTA);
  //
  SALDO := TOTAL - PAGO;
  if SALDO < 0 then
    PEND_VAL := 0
  else
    PEND_VAL := SALDO;
  //
  Empresa    := QrInadimp2Empresa.Value;
  ForneceI   := QrInadimp2ForneceI.Value;
  Apto       := QrInadimp2Apto.Value;
  Vencimento := Geral.FDT(QrInadimp2Vencimento.Value, 1);
  FatNum     := QrInadimp2FatNum.Value;
  //
  Result := UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FInadim2, False, [
  'TOTAL', 'SALDO', 'PEND_VAL',
  'Infl1Indice', 'Infl1PeriI', 'Infl1PeriF',
  'Infl1FatorI', 'Infl1FatorF', 'Infl1ValorT',
  'Infl1Percen', 'Infl1ValorS', 'Infl2Indice',
  'Infl2PeriI', 'Infl2PeriF', 'Infl2FatorI',
  'Infl2FatorF', 'Infl2ValorT', 'Infl2Percen',
  'Infl2ValorS', 'InflTValorS'], [
  'Empresa', 'ForneceI', 'Apto', 'Vencimento', 'FatNum'], [
  TOTAL, SALDO, PEND_VAL,
  Infl1Indice, Infl1PeriI, Infl1PeriF,
  Infl1FatorI, Infl1FatorF, Infl1ValorT,
  Infl1Percen, Infl1ValorS, Infl2Indice,
  Infl2PeriI, Infl2PeriF, Infl2FatorI,
  Infl2FatorF, Infl2ValorT, Infl2Percen,
  Infl2ValorS, InflTValorS], [
  Empresa, ForneceI, Apto, Vencimento, FatNum], False);
end;

procedure TFmInadimp2.AvisoTotal(Aviso: String);
begin
  Geral.MB_Erro(Aviso);
  MyObjects.Informa2(LAAviso1, LaAviso2, False,
    Geral.Substitui(Aviso, sLineBreak, ' - '));
  //
  Screen.Cursor := crDefault;
end;

procedure TFmInadimp2.BtCertidaoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCertidaoNeg, FmCertidaoNeg, afmoNegarComAviso) then
  begin
    //
    FmCertidaoNeg.EdEmpresa.ValueVariant := EdEmpresa.ValueVariant;
    FmCertidaoNeg.CBEmpresa.KeyValue := CBEmpresa.KeyValue;
    //
    FmCertidaoNeg.EdPropriet.ValueVariant := EdPropriet.ValueVariant;
    FmCertidaoNeg.CBPropriet.KeyValue := CBPropriet.KeyValue;
    //
    FmCertidaoNeg.EdCondImov.ValueVariant := EdCondImov.ValueVariant;
    FmCertidaoNeg.CBCondImov.KeyValue := CBCondImov.KeyValue;
    //
    FmCertidaoNeg.ShowModal;
    FmCertidaoNeg.Destroy;
  end;
end;

procedure TFmInadimp2.BtCobrancaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCobrarGer, FmCobrarGer, afmoNegarComAviso) then
  begin
    FmCobrarGer.ShowModal;
    FmCobrarGer.Destroy;
    //
    //  Evitar erro caso empresa mudou!
    RefreshEmpresaSelecionada();
  end;
end;

procedure TFmInadimp2.BtDiarioAddClick(Sender: TObject);
begin
  MostraDiarioAdd();
end;

procedure TFmInadimp2.BtMulJurClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMulJur, BtMulJur);
end;

procedure TFmInadimp2.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmInadimp2.Segundaviadebloquetos1Click(Sender: TObject);
var
  NivelJuridico: Integer;
  Quando: String;
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
  'Informe o condom�nio e pesquise novamente!') then
    Exit;
  //
  // 2012-05-16  - Impedir impress�o de protestados e com a��o de cobran�a
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando impedimentos de impress�o');
  UnCreateApp.RecriaTempTableNovo(ntrtt_Inadimp, DModG.QrUpdPID1, False, 1, 'BloqInad');
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  // 2014-08-27 Ini
  (*
  Dmod.QrAux.SQL.Add(DELETE_FROM + VAR_MyPID_DB_NOME + '.bloqinad;');
  Dmod.QrAux.SQL.Add('INSERT INTO ' + VAR_MyPID_DB_NOME + '.bloqinad ');
  *)
  Dmod.QrAux.SQL.Add(DELETE_FROM + VAR_MyPID_DB_NOME + '.' + FInadim2 + '; ');
  // 2014-08-27 Fim
  Dmod.QrAux.SQL.Add(QrPesq4.SQL.Text);
  Dmod.QrAux.Params := QrPesq4.Params;
  Dmod.QrAux.ExecSQL;
  //
  QrBloqInad.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(QrBloqInad, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FInadim2,
  'WHERE Juridico > 0 ',
  '']);
  if QrBloqInad.RecordCount > 0 then
  begin
    NivelJuridico := MyObjects.SelRadioGroup('Situa��o Jur�dica Detectada!',
    'Mostrar os boletos pesquisados dos cond�minos:', ['Sem restri��o',
    'Sem restri��o ou n�o vencidos (precisa de SENHA)',
    'TODOS (precisa de SENHA)'], 1);
    if NivelJuridico < 0 then
      Exit;
    if NivelJuridico > 0 then
      if not DBCheck.LiberaPelaSenhaBoss('SITUA��O JUR�DICA EXIGE SENHA!') then Exit;
  end else
    NivelJuridico := 0;
  //
{   N�VEL JUR�DICO
0 - Sem restri��o',
1 - Sem restri��o ou n�o vencidos (precisa de SENHA)',
2 - TODOS (precisa de SENHA)']
}
  case NivelJuridico of
    0: Quando := 'WHERE Juridico=0';
    1: Quando := 'WHERE Juridico=0 OR Vencimento >= SYSDATE()';
    else Quando := '';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrBloqInad, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FInadim2,
  Quando,
  '']);
  if QrBloqInad.RecordCount > 0 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    if DBCheck.CriaFm(TFmInadimpBloq, FmInadimpBloq, afmoNegarComAviso) then
    begin
      FmInadimpBloq.FTabLctA       := FTabLctA;
      FmInadimpBloq.FTabAriA       := FTabAriA;
      FmInadimpBloq.FTabCnsA       := FTabCnsA;
      FmInadimpBloq.FTabBloqInad   := FInadim2;
      FmInadimpBloq.FNivelJuridico := NivelJuridico;
      FmInadimpBloq.ShowModal;
      FmInadimpBloq.Destroy;
    end;
  end else
    Geral.MensagemBox('Nenhum bloqueto passou pelo filtro jur�dico!',
    'Aviso', MB_OK+MB_ICONWARNING);
  // FIM 2012-05-16
end;

procedure TFmInadimp2.SetaEImprimeFrx(Frx: TfrxReport; Titulo: String);
  function Footer(Indice: Integer): String;
  begin
    Result := 'Sub-total ' + Headers[Indice];
  end;
begin
  frx.Variables['LogoAdmiExiste']  := FileExists(Dmod.QrControle.FieldByName('MyLogo').AsString);
  frx.Variables['LogoAdmiCaminho'] := QuotedStr(Dmod.QrControle.FieldByName('MyLogo').AsString);
  frx.Variables['VARF_MAXGRUPO'] := RGGrupos.ItemIndex;

  frx.Variables['VARF_GRUPO1']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR1']   := ''''+Headers[RGOrdem1.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem1.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR1']   := ''''+Footer(RGOrdem1.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  frx.Variables['VARF_GRUPO2']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem2.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR2']   := ''''+Headers[RGOrdem2.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem2.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR2']   := ''''+Footer(RGOrdem2.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem2.ItemIndex]+'"]''';

  frx.Variables['VARF_GRUPO3']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem3.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR3']   := ''''+Headers[RGOrdem3.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem3.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR3']   := ''''+Footer(RGOrdem1.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  frx.Variables['VARF_GRUPO4']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem4.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR4']   := ''''+Headers[RGOrdem4.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem4.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR4']   := ''''+Footer(RGOrdem1.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  case RGSomas.ItemIndex of
    0: frx.Variables['VARF_ID_VAL'] := '''[frxDsPesq3."Controle"]''';
    1: frx.Variables['VARF_ID_VAL'] := '''[frxDsPesq3."FatNum"]''';
    else frx.Variables['VARF_ID_VAL'] := '';
  end;
  frx.Variables['VARF_DATA']  := Now();

  MyObjects.frxDefineDataSets(frx, [
    DModG.frxDsDono,
    frxDsPesq3
  ]);
  MyObjects.frxMostra(frx, Titulo);
end;

procedure TFmInadimp2.frxPend_s0i0GetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO_VCT') = 0 then
    Value := dmkPF.PeriodoImp2(TPIniVct.Date, TPFimVct.Date,
      CkIniVct.Checked, CkFimVct.Checked, 'Per�odo de vencimento: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_PERIODO_DTA') = 0 then
    Value := dmkPF.PeriodoImp2(TPIniDta.Date, TPFimDta.Date,
      CkIniDta.Checked, CkFimDta.Checked, 'Per�odo de emiss�o: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_PERIODO_PSQ') = 0 then
    Value := dmkPF.PeriodoImp2(0, TPFimPgt.Date,
      False, CkFimPgt.Checked, 'Data da pesquisa: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_PERIODO_CPT') = 0 then
    Value := dmkPF.PeriodoImpMezAno(EdMezIni.Text, EdMezFim.Text,
      'Per�odo de compet�ncia: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_ID_TIT') = 0 then
  begin
    case RGSomas.ItemIndex of
      0: Value := 'Controle';
      1: Value := 'Bloqueto';
      else Value := '';
    end;
  end
  else if AnsiCompareText(VarName, 'VARF_TITULO') = 0 then
  begin
    case RGSituacao.ItemIndex of
      0: Value := 'TODOS VALORES ABERTOS DE COND�MINOS';
      1: Value := 'PEND�NCIAS DE COND�MINOS';
      else Value := RGSituacao.Items[RGSituacao.ItemIndex];
    end;
  end
  else begin
    case PCAgrupamentos.ActivePageIndex of
      0:
      begin
        if AnsiCompareText(VarName, 'VARF_EMPRESA') = 0 then
          Value := dmkPF.ParValueCodTxt('Cliente: ', CBEmpresa.Text, Null)
        else if AnsiCompareText(VarName, 'VARF_UH') = 0 then
          Value := dmkPF.ParValueCodTxt(DModG.ReCaptionTexto(VAR_U_H) + ': ', CBCondImov.Text, Null)
        else if AnsiCompareText(VarName, 'VARF_CONDOMINO') = 0 then
          Value := dmkPF.ParValueCodTxt(DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) + ': ', CBPropriet.Text, Null)
      end;
      1:
      begin
        case RGSomas.ItemIndex of
          0:
          begin
            if AnsiCompareText(VarName, 'VARF_EMPRESA') = 0 then
              Value := dmkPF.ParValueCodTxt('Cliente: ', CBEmpresa.Text, Null)
            else if AnsiCompareText(VarName, 'VARF_UH') = 0 then
              Value := dmkPF.ParValueCodTxt(DModG.ReCaptionTexto(VAR_U_H) + ': ', CBCondImov.Text, Null)
            else if AnsiCompareText(VarName, 'VARF_CONDOMINO') = 0 then
              Value := dmkPF.ParValueCodTxt(DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) + ': ', CBPropriet.Text, Null)
          end;
          1:
          begin
            if AnsiCompareText(VarName, 'VARF_EMPRESA') = 0 then
              Value := dmkPF.ParValueCodTxt('Grupo de pesquisa: ', CBCondGri.Text, Null)
            else if AnsiCompareText(VarName, 'VARF_UH') = 0 then
              Value := ''
            else if AnsiCompareText(VarName, 'VARF_CONDOMINO') = 0 then
              Value := ''
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmInadimp2.MostraDiarioAdd();
const
  UsaDataHora = False;
  Data        = 0;
  Hora        = 0;
  DiarioAss   = 0;
  Texto       = '';
var
  CliInt, Entidade1, Entidade2, Depto: Integer;
begin
  if DBCheck.CriaFm(TFmDiarioAdd, FmDiarioAdd, afmoNegarComAviso) then
  begin
    FmDiarioAdd.FChamou        := -1;
    FmDiarioAdd.ImgTipo.SQLType := stIns;
    //
    CliInt    := QrPesq4Empresa.Value;
    Entidade1 := QrPesq4Propriet.Value;
    Entidade2 := QrPesq4Usuario.Value;
    Depto     := QrPesq4Apto.Value;
    //
    FmDiarioAdd.PreencheDados(UsaDataHora, Data, Hora, DiarioAss, CliInt,
      Depto, Entidade1, Entidade2, Texto);
    if QrPesq4Imobiliaria.Value > 0 then
    begin
      FmDiarioAdd.FProcurador := QrPesq4Imobiliaria.Value;
      FmDiarioAdd.FNomeTipoPRC := 'Imobili�ria';
    end else
    if QrPesq4Procurador.Value > 0 then
    begin
      FmDiarioAdd.FProcurador := QrPesq4Procurador.Value;
      FmDiarioAdd.FNomeTipoPRC := 'Procurador';
    end else FmDiarioAdd.FProcurador := 0;
    FmDiarioAdd.ShowModal;
    FmDiarioAdd.Destroy;
  end;
end;

procedure TFmInadimp2.RGOrdem1Click(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp2.RGOrdem2Click(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp2.RGOrdem3Click(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp2.RGOrdem4Click(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp2.EdCondGriChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
  ReopenCondGriImv(0);
end;

procedure TFmInadimp2.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCondGri, FmCondGri, afmoNegarComAviso) then
  begin
    FmCondGri.ShowModal;
    FmCondGri.Destroy;
  end;
  QrCondGri.Close;
  //
  UMyMod.AbreQuery(QrCondGri, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
  begin
    EdCondGri.Text := IntToStr(VAR_CADASTRO);
    CBCondGri.KeyValue := VAR_CADASTRO;
  end;
end;

procedure TFmInadimp2.EdProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmInadimp2.CBProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

// Evitar erro caso empresa mudou!
procedure TFmInadimp2.RefreshEmpresaSelecionada();
var
  Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  EdEmpresa.ValueVariant := 0;
  CBEmpresa.KeyValue := Null;
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue := Empresa;
end;

procedure TFmInadimp2.ReopenCondGriImv(Controle: Integer);
begin
  QrCondGriImv.Close;
  QrCondGriImv.Params[0].AsInteger := QrCondGriCodigo.Value;
  //
  UMyMod.AbreQuery(QrCondGriImv, Dmod.MyDB);
end;

procedure TFmInadimp2.QrInadimp2AfterScroll(DataSet: TDataSet);
var
  Depto, FatNum, Mez, Vencto: String;
begin
  Depto  := FormatFloat('0', QrInadimp2Apto.Value);
  FatNum := FormatFloat('0', QrInadimp2FatNum.Value);
  Mez    := FormatFloat('0', QrInadimp2Mez.Value);
  Vencto := Geral.FDT(QrInadimp2Vencimento.Value, 1);
  //
  QrIts2.Close;
  QrIts2.SQL.Clear;
  QrIts2.SQL.Add('SELECT lct.Genero, lct.Descricao, lct.Credito, lct.Controle ');
  QrIts2.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrIts2.SQL.Add('WHERE lct.Depto=' + Depto);
  QrIts2.SQL.Add('AND lct.FatNum=' + FatNum);
  QrIts2.SQL.Add('AND lct.Mez=' + Mez);
  QrIts2.SQL.Add('AND lct.Vencimento="' + Vencto + '"');
  UMyMod.AbreQuery(QrIts2, Dmod.MyDB);
end;

procedure TFmInadimp2.QrInadimp2BeforeClose(DataSet: TDataSet);
begin
  QrIts2.Close;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp2.QrInadimp2CalcFields(DataSet: TDataSet);
begin
  QrInadimp2Juridico_TXT.Value := dmkPF.DefineJuricoSigla(QrInadimp2Juridico.Value);
  QrInadimp2Juridico_DESCRI.Value := dmkPF.DefineJuricoSigla(QrInadimp2Juridico.Value);
end;

procedure TFmInadimp2.QrPesq4CalcFields(DataSet: TDataSet);
begin
  QrPesq4Juridico_TXT.Value := dmkPF.DefineJuricoSigla(QrPesq4Juridico.Value);
  QrPesq4Juridico_DESCRI.Value := dmkPF.DefineJuricoSigla(QrPesq4Juridico.Value);
end;

end.

