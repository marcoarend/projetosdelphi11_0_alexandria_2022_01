object FmEmailBloqueto: TFmEmailBloqueto
  Left = 366
  Top = 193
  Caption = 'GER-CONDM-028 :: Envio de Bloquetos por Emeio'
  ClientHeight = 609
  ClientWidth = 841
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 841
    Height = 447
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TdmkDBGrid
      Left = 0
      Top = 0
      Width = 841
      Height = 227
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Selecio'
          ReadOnly = True
          Title.Caption = 'Sel'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Protoco'
          Title.Caption = 'Protocolo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bloquet'
          Title.Caption = 'Bloqueto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RecipProno'
          Title.Caption = 'Pronome'
          Width = 49
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RecipNome'
          Title.Caption = 'Nome destinat'#225'rio'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RecipEmeio'
          Title.Caption = 'Emeio destinat'#225'rio'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTA1_TXT'
          Title.Caption = 'Entrega'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTA2_TXT'
          Title.Caption = 'Retorno'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PreEmeio'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsUnidCond
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Selecio'
          ReadOnly = True
          Title.Caption = 'Sel'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Protoco'
          Title.Caption = 'Protocolo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bloquet'
          Title.Caption = 'Bloqueto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RecipProno'
          Title.Caption = 'Pronome'
          Width = 49
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RecipNome'
          Title.Caption = 'Nome destinat'#225'rio'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RecipEmeio'
          Title.Caption = 'Emeio destinat'#225'rio'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTA1_TXT'
          Title.Caption = 'Entrega'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTA2_TXT'
          Title.Caption = 'Retorno'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PreEmeio'
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 400
      Width = 841
      Height = 47
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object PB1: TProgressBar
        Left = 4
        Top = 24
        Width = 829
        Height = 17
        TabOrder = 0
      end
      object StaticText1: TStaticText
        Left = 4
        Top = 4
        Width = 46
        Height = 17
        BorderStyle = sbsSunken
        Caption = '  Status: '
        TabOrder = 1
      end
      object STStatus: TStaticText
        Left = 52
        Top = 4
        Width = 781
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 
          '  Clique em "Envia" ap'#243's selecionar os bloquetos a serem enviado' +
          's.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 359
      Width = 841
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object RGFormato: TRadioGroup
        Left = 0
        Top = 0
        Width = 164
        Height = 41
        Align = alLeft
        Caption = ' Formato do arquivo de envio: '
        ItemIndex = 0
        Items.Strings = (
          'PDF')
        TabOrder = 0
      end
      object CkRecriarImagens: TCheckBox
        Left = 532
        Top = 15
        Width = 157
        Height = 17
        Caption = 'For'#231'ar recria'#231#227'o de imagens.'
        TabOrder = 1
      end
      object RGIndividuais: TRadioGroup
        Left = 164
        Top = 0
        Width = 164
        Height = 41
        Align = alLeft
        Caption = ' Modelos individuais: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 2
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 227
      Width = 841
      Height = 132
      Align = alBottom
      Caption = 'Panel3'
      TabOrder = 3
      object PageControl1: TPageControl
        Left = 1
        Top = 1
        Width = 839
        Height = 130
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = ' Mensagem texto '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGMsg: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 237
            Height = 102
            Align = alLeft
            Columns = <
              item
                Expanded = False
                FieldName = 'Ordem'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ativo'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 53
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Nome'
                Width = 90
                Visible = True
              end>
            Color = clWindow
            DataSource = DModG.DsPreEmMsg
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Ordem'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ativo'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 53
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Nome'
                Width = 90
                Visible = True
              end>
          end
          object MeMsg: TDBMemo
            Left = 237
            Top = 0
            Width = 594
            Height = 102
            Align = alClient
            DataField = 'Texto'
            DataSource = DModG.DsPreEmMsg
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Mensagem gerada'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object memEMailSource: TMemo
            Left = 0
            Top = 0
            Width = 831
            Height = 102
            Align = alClient
            TabOrder = 0
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'antigo Texto plano '
          ImageIndex = 2
          object edMessageText: TMemo
            Left = 0
            Top = 0
            Width = 831
            Height = 102
            Align = alClient
            TabOrder = 0
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Codifica'#231#227'o de imagens'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object MeEncode: TMemo
            Left = 0
            Top = 0
            Width = 831
            Height = 102
            Align = alClient
            TabOrder = 0
          end
        end
        object TabSheet5: TTabSheet
          Caption = ' Itens n'#227'o carregados '
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object dmkDBGrid2: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 831
            Height = 102
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Protoco'
                Title.Caption = 'Protocolo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bloquet'
                Title.Caption = 'Bloqueto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Unidade'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RecipProno'
                Title.Caption = 'Pronome'
                Width = 49
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RecipNome'
                Title.Caption = 'Nome destinat'#225'rio'
                Width = 210
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RecipEmeio'
                Title.Caption = 'Emeio destinat'#225'rio'
                Width = 228
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA1_TXT'
                Title.Caption = 'Entrega'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA2_TXT'
                Title.Caption = 'Retorno'
                Width = 56
                Visible = True
              end>
            Color = clWindow
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGrid1CellClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Protoco'
                Title.Caption = 'Protocolo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bloquet'
                Title.Caption = 'Bloqueto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Unidade'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RecipProno'
                Title.Caption = 'Pronome'
                Width = 49
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RecipNome'
                Title.Caption = 'Nome destinat'#225'rio'
                Width = 210
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RecipEmeio'
                Title.Caption = 'Emeio destinat'#225'rio'
                Width = 228
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA1_TXT'
                Title.Caption = 'Entrega'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA2_TXT'
                Title.Caption = 'Retorno'
                Width = 56
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 841
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 793
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 745
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 365
        Height = 32
        Caption = 'Envio de Bloquetos por Emeio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 365
        Height = 32
        Caption = 'Envio de Bloquetos por Emeio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 365
        Height = 32
        Caption = 'Envio de Bloquetos por Emeio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 841
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 837
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 539
    Width = 841
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 837
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 693
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Envia'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 242
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 366
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtNenhumClick
      end
    end
  end
  object QrUnidCond: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeOpen = QrUnidCondBeforeOpen
    AfterScroll = QrUnidCondAfterScroll
    SQL.Strings = (
      'SELECT CONCAT_WS("-", uc.Bloquet, uc.Apto) BOLAPTO,'
      'IF(uc.Data1=0,"", DATE_FORMAT(uc.Data1, "%d/%m/%Y")) DTA1_TXT,'
      'IF(uc.Data2=0,"", DATE_FORMAT(uc.Data2, "%d/%m/%Y")) DTA2_TXT,'
      'uc.*'
      'FROM unidcond uc'
      '/*'
      'WHERE uc.Protoco > 0'
      'AND uc.RecipEmeio <> '#39#39
      '*/'
      'ORDER BY uc.Unidade')
    Left = 364
    Top = 148
    object QrUnidCondBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Size = 23
    end
    object QrUnidCondDTA1_TXT: TWideStringField
      FieldName = 'DTA1_TXT'
      Size = 10
    end
    object QrUnidCondDTA2_TXT: TWideStringField
      FieldName = 'DTA2_TXT'
      Size = 10
    end
    object QrUnidCondApto: TIntegerField
      FieldName = 'Apto'
      Origin = 'unidcond.Apto'
    end
    object QrUnidCondEntidad: TIntegerField
      FieldName = 'Entidad'
      Origin = 'unidcond.Entidad'
    end
    object QrUnidCondProtoco: TIntegerField
      FieldName = 'Protoco'
      Origin = 'unidcond.Protoco'
    end
    object QrUnidCondBloquet: TIntegerField
      FieldName = 'Bloquet'
      Origin = 'unidcond.Bloquet'
    end
    object QrUnidCondUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'unidcond.Unidade'
      Size = 10
    end
    object QrUnidCondProprie: TWideStringField
      FieldName = 'Proprie'
      Origin = 'unidcond.Proprie'
      Size = 100
    end
    object QrUnidCondValor: TFloatField
      FieldName = 'Valor'
      Origin = 'unidcond.Valor'
    end
    object QrUnidCondData1: TDateField
      FieldName = 'Data1'
      Origin = 'unidcond.Data1'
    end
    object QrUnidCondData2: TDateField
      FieldName = 'Data2'
      Origin = 'unidcond.Data2'
    end
    object QrUnidCondTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'unidcond.Texto'
      Size = 100
    end
    object QrUnidCondRecipNome: TWideStringField
      FieldName = 'RecipNome'
      Origin = 'unidcond.RecipNome'
      Size = 100
    end
    object QrUnidCondRecipEmeio: TWideStringField
      FieldName = 'RecipEmeio'
      Origin = 'unidcond.RecipEmeio'
      Size = 100
    end
    object QrUnidCondOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'unidcond.Ordem'
    end
    object QrUnidCondSelecio: TSmallintField
      FieldName = 'Selecio'
      Origin = 'unidcond.Selecio'
      MaxValue = 1
    end
    object QrUnidCondAndar: TIntegerField
      FieldName = 'Andar'
      Origin = 'unidcond.Andar'
    end
    object QrUnidCondRecipItem: TIntegerField
      FieldName = 'RecipItem'
      Origin = 'unidcond.RecipItem'
    end
    object QrUnidCondRecipProno: TWideStringField
      FieldName = 'RecipProno'
      Origin = 'unidcond.RecipProno'
    end
    object QrUnidCondVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'unidcond.Vencimento'
    end
    object QrUnidCondPreEmeio: TIntegerField
      FieldName = 'PreEmeio'
      Origin = 'unidcond.PreEmeio'
    end
    object QrUnidCondCondominio: TWideStringField
      FieldName = 'Condominio'
      Origin = 'unidcond.Condominio'
      Size = 255
    end
    object QrUnidCondIDEmeio: TIntegerField
      FieldName = 'IDEmeio'
    end
  end
  object DsUnidCond: TDataSource
    DataSet = QrUnidCond
    Left = 392
    Top = 148
  end
  object QrSel: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT uc.Selecio'
      'FROM unidcond uc'
      'WHERE Selecio>0'
      'ORDER BY uc.Unidade')
    Left = 420
    Top = 148
    object QrSelSelecio: TSmallintField
      FieldName = 'Selecio'
    end
  end
  object frxPDFExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'Dermatek'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 448
    Top = 148
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'MIME'
    BccList = <
      item
      end>
    CCList = <>
    Encoding = meMIME
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 476
    Top = 148
  end
  object IdSMTP1: TIdSMTP
    IOHandler = IdSSL1
    SASLMechanisms = <>
    Left = 504
    Top = 148
  end
  object QrNCs: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM unidcond'
      'WHERE Selecio=1'
      'AND (Protoco=0'
      '     OR PreEmeio=0'
      '     OR RecipEmeio="")')
    Left = 588
    Top = 148
    object QrNCsItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object IdSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = ':25'
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv2
    SSLOptions.SSLVersions = [sslvSSLv2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 532
    Top = 148
  end
  object IMAP: TIdIMAP4
    SASLMechanisms = <>
    MilliSecsToWaitToClearBuffer = 10
    Left = 560
    Top = 148
  end
end
