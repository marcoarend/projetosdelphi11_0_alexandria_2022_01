unit ArreBaCAddUni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit,
  dmkDBLookupComboBox, dmkEditCB, dmkLabel, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmArreBaCAddUni = class(TForm)
    PainelDados: TPanel;
    DsAptos: TDataSource;
    QrAptos: TmySQLQuery;
    Panel1: TPanel;
    dmkEdCBApto: TdmkEditCB;
    dmkCBApto: TdmkDBLookupComboBox;
    Label1: TLabel;
    dmkEdPercent: TdmkEdit;
    dmkLabel1: TdmkLabel;
    QrAptosConta: TIntegerField;
    QrAptosUnidade: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmArreBaCAddUni: TFmArreBaCAddUni;

implementation

uses Module, ArreBaC, UnFinanceiro, UnMyObjects;

{$R *.DFM}

procedure TFmArreBaCAddUni.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmArreBaCAddUni.BtConfirmaClick(Sender: TObject);
var
  Codigo, Controle, Conta, Apto: Integer;
begin
  Apto := dmkEdCBApto.ValueVariant;
  if Apto <> 0 then
  begin
    Codigo   := FmArreBaC.QrArreBaCCodigo.Value;
    Controle := FmArreBaC.QrArreBaIControle.Value;
    Conta    := UMyMod.BuscaEmLivreY_Def('ArreBaU', 'Conta',
      ImgTipo.SQLType, FmArreBaC.QrArreBaUConta.Value);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ArreBaU', False, [
      'Codigo', 'Controle', 'Apto', 'Percent'
    ], ['Conta'], [
      Codigo, Controle, Apto, dmkEdPercent.ValueVariant
    ], [Conta], True) then
    begin
      FmArreBaC.ReopenQrArreBaU(Conta);
      Close;
    end;  
  end else Geral.MensagemBox('Defina a unidade habitacional!', 'Aviso',
  MB_OK+MB_ICONWARNING);
end;

procedure TFmArreBaCAddUni.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmArreBaCAddUni.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmArreBaCAddUni.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

end.
