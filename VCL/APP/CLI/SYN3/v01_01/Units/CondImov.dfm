object FmCondImov: TFmCondImov
  Left = 339
  Top = 185
  ActiveControl = EdCodigo
  Caption = 'CAD-CONDO-004 :: Im'#243'veis'
  ClientHeight = 836
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 691
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label12: TLabel
      Left = 192
      Top = 5
      Width = 71
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID Unidade:'
    end
    object Label1: TLabel
      Left = 103
      Top = 6
      Width = 54
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID Bloco:'
    end
    object Label45: TLabel
      Left = 437
      Top = 7
      Width = 40
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Status:'
    end
    object Label61: TLabel
      Left = 348
      Top = 7
      Width = 55
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Unidade:'
    end
    object Label62: TLabel
      Left = 283
      Top = 7
      Width = 39
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Andar:'
    end
    object SpeedButton7: TSpeedButton
      Left = 576
      Top = 25
      Width = 26
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton7Click
    end
    object Label53: TLabel
      Left = 15
      Top = 110
      Width = 73
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Propriet'#225'rio:'
    end
    object SpeedButton5: TSpeedButton
      Left = 576
      Top = 130
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton5Click
    end
    object SpeedButton6: TSpeedButton
      Left = 576
      Top = 185
      Width = 26
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton6Click
    end
    object Label44: TLabel
      Left = 15
      Top = 164
      Width = 52
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Inquilino:'
    end
    object Label2: TLabel
      Left = 15
      Top = 6
      Width = 75
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Condom'#237'nio:'
    end
    object Label65: TLabel
      Left = 16
      Top = 218
      Width = 53
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#244'njuge:'
    end
    object SpeedButton10: TSpeedButton
      Left = 577
      Top = 239
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton10Click
    end
    object Label203: TLabel
      Left = 625
      Top = 335
      Width = 70
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Procurador:'
    end
    object Label32: TLabel
      Left = 15
      Top = 60
      Width = 87
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Qtd Garagens:'
    end
    object Label190: TLabel
      Left = 108
      Top = 59
      Width = 74
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Moradores*:'
    end
    object Label194: TLabel
      Left = 182
      Top = 59
      Width = 87
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Fra'#231#227'o ideal *:'
    end
    object Label3: TLabel
      Left = 15
      Top = 448
      Width = 85
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Observa'#231#245'es:'
    end
    object Label196: TLabel
      Left = 354
      Top = 670
      Width = 471
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 
        '*: Utilizado somente para rateio de consumo! N'#227'o '#233' utilizado em ' +
        'arrecada'#231#245'es!'
    end
    object Label4: TLabel
      Left = 822
      Top = 670
      Width = 372
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = #185': Dias corridos ap'#243's vencimento do boleto (0  para n'#227'o emitir).'
    end
    object SpeedButton1: TSpeedButton
      Left = 1196
      Top = 354
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object EdCodigo: TdmkEdit
      Left = 192
      Top = 25
      Width = 87
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Conta'
      UpdCampo = 'Conta'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object dmkDBEdCodigo: TdmkDBEdit
      Left = 103
      Top = 25
      Width = 87
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Controle'
      DataSource = FmCond.DsCondBloco
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object EdAndar: TdmkEdit
      Left = 283
      Top = 25
      Width = 60
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Andar'
      UpdCampo = 'Andar'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdUnidade: TdmkEdit
      Left = 348
      Top = 25
      Width = 86
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      MaxLength = 10
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Unidade'
      UpdCampo = 'Unidade'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdStatus: TdmkEditCB
      Left = 438
      Top = 25
      Width = 35
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Status'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStatus
      IgnoraDBLookupComboBox = False
    end
    object CBStatus: TdmkDBLookupComboBox
      Left = 478
      Top = 25
      Width = 94
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsStatus
      TabOrder = 6
      dmkEditCB = EdStatus
      QryCampo = 'Status'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdPropriet: TdmkEditCB
      Left = 15
      Top = 130
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Propriet'
      UpdCampo = 'Propriet'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPropriet
      IgnoraDBLookupComboBox = False
    end
    object CBPropriet: TdmkDBLookupComboBox
      Left = 84
      Top = 130
      Width = 487
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsImovPro
      TabOrder = 12
      dmkEditCB = EdPropriet
      QryCampo = 'Propriet'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CBInquilino: TdmkDBLookupComboBox
      Left = 84
      Top = 185
      Width = 487
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsImovInq
      TabOrder = 14
      dmkEditCB = EdInquilino
      QryCampo = 'Usuario'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdInquilino: TdmkEditCB
      Left = 15
      Top = 185
      Width = 69
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Usuario'
      UpdCampo = 'Usuario'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBInquilino
      IgnoraDBLookupComboBox = False
    end
    object dmkDBEdit1: TdmkDBEdit
      Left = 15
      Top = 25
      Width = 86
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Codigo'
      DataSource = FmCond.DsCond
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object CkContinuar: TCheckBox
      Left = 16
      Top = 665
      Width = 138
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Continuar inserindo.'
      TabOrder = 26
    end
    object RGddVctEsp: TdmkRadioGroup
      Left = 610
      Top = 4
      Width = 602
      Height = 91
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 
        ' Dia do m'#234's do vencimento da quota condominial (quando diferente' +
        ' do padr'#227'o do condom'#237'nio): '
      Columns = 10
      ItemIndex = 0
      Items.Strings = (
        '00'
        '01'
        '02'
        '03'
        '04'
        '05'
        '06'
        '07'
        '08'
        '09'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28')
      TabOrder = 19
      QryCampo = 'ddVctEsp'
      UpdCampo = 'ddVctEsp'
      UpdType = utYes
      OldValor = 0
    end
    object EdConjuge: TdmkEditCB
      Left = 15
      Top = 239
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Conjuge'
      UpdCampo = 'Conjuge'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBConjuge
      IgnoraDBLookupComboBox = False
    end
    object CBConjuge: TdmkDBLookupComboBox
      Left = 84
      Top = 239
      Width = 487
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsConjuges
      TabOrder = 16
      dmkEditCB = EdConjuge
      QryCampo = 'Conjuge'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdProcurador: TdmkEditCB
      Left = 625
      Top = 354
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 21
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Procurador'
      UpdCampo = 'Procurador'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBProcurador
      IgnoraDBLookupComboBox = False
    end
    object CBProcurador: TdmkDBLookupComboBox
      Left = 694
      Top = 354
      Width = 499
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'NOME_ENT'
      ListSource = DsProcuradores
      TabOrder = 22
      dmkEditCB = EdProcurador
      QryCampo = 'Procurador'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object MeObserv: TdmkMemo
      Left = 15
      Top = 468
      Width = 592
      Height = 193
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 25
      QryCampo = 'Observ'
      UpdCampo = 'ObServ'
      UpdType = utYes
    end
    object EdQtdGaragem: TdmkEdit
      Left = 15
      Top = 79
      Width = 88
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'QtdGaragem'
      UpdCampo = 'QtdGaragem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMoradores: TdmkEdit
      Left = 108
      Top = 79
      Width = 66
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Moradores'
      UpdCampo = 'Moradores'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdFracaoIdeal: TdmkEdit
      Left = 182
      Top = 79
      Width = 120
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'FracaoIdeal'
      UpdCampo = 'FracaoIdeal'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object CkUmAUm: TCheckBox
      Left = 158
      Top = 665
      Width = 193
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Aborta altera'#231#227'o "um a um".'
      TabOrder = 27
      Visible = False
    end
    object RGJuridico: TdmkRadioGroup
      Left = 625
      Top = 384
      Width = 597
      Height = 91
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Status Jur'#237'dico: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Sem restri'#231#245'es'
        'Processo de protesto'
        'Protestado'
        'Com a'#231#227'o de cobran'#231'a'
        'Protestado e a'#231#227'o de cobran'#231'a')
      TabOrder = 23
      QryCampo = 'Juridico'
      UpdCampo = 'Juridico'
      UpdType = utYes
      OldValor = 0
    end
    object CkSitImv: TdmkCheckBox
      Left = 315
      Top = 84
      Width = 234
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Rateia itens base de arrecada'#231#227'o.'
      TabOrder = 10
      QryCampo = 'SitImv'
      UpdCampo = 'SitImv'
      UpdType = utYes
      ValCheck = '1'
      ValUncheck = '0'
      OldValor = #0
    end
    object GroupBox1: TGroupBox
      Left = 15
      Top = 266
      Width = 592
      Height = 109
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Imobili'#225'ria: '
      TabOrder = 17
      object Panel3: TPanel
        Left = 2
        Top = 18
        Width = 588
        Height = 89
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object SpeedButton11: TSpeedButton
          Left = 559
          Top = 4
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton11Click
        end
        object Label128: TLabel
          Left = 11
          Top = 34
          Width = 49
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Contato:'
        end
        object Label127: TLabel
          Left = 410
          Top = 34
          Width = 57
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Telefone:'
        end
        object EdImobiliaria: TdmkEditCB
          Left = 10
          Top = 4
          Width = 70
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Imobiliaria'
          UpdCampo = 'Imobiliaria'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBImobiliaria
          IgnoraDBLookupComboBox = False
        end
        object CBImobiliaria: TdmkDBLookupComboBox
          Left = 80
          Top = 4
          Width = 474
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'RazaoSocial'
          ListSource = DsImobiliarias
          TabOrder = 1
          dmkEditCB = EdImobiliaria
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object DBImov_Cont: TDBEdit
          Left = 10
          Top = 55
          Width = 392
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'EContato'
          DataSource = DsImobiliarias
          Enabled = False
          TabOrder = 2
        end
        object DBImov_ContTel: TDBEdit
          Left = 410
          Top = 55
          Width = 144
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'IMOBTEL_TXT'
          DataSource = DsImobiliarias
          Enabled = False
          TabOrder = 3
        end
      end
    end
    object GroupBox3: TGroupBox
      Left = 15
      Top = 384
      Width = 592
      Height = 60
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Configura'#231#227'o de envio de mensagens por inadimpl'#234'ncia: '
      TabOrder = 18
      object Panel6: TPanel
        Left = 2
        Top = 18
        Width = 588
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object SBInadCEMCad: TSpeedButton
          Left = 559
          Top = 4
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SBInadCEMCadClick
        end
        object EdInadCEMCad: TdmkEditCB
          Left = 10
          Top = 4
          Width = 56
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'InadCEMCad'
          UpdCampo = 'InadCEMCad'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBInadCEMCad
          IgnoraDBLookupComboBox = False
        end
        object CBInadCEMCad: TdmkDBLookupComboBox
          Left = 66
          Top = 4
          Width = 488
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsInadCEMCad
          TabOrder = 1
          dmkEditCB = EdInadCEMCad
          QryCampo = 'InadCEMCad'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 610
      Top = 98
      Width = 622
      Height = 233
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Endere'#231'o de envio do bloqueto condominial: '
      TabOrder = 20
      object Label201: TLabel
        Left = 15
        Top = 73
        Width = 258
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Terceiro a receber o bloqueto condominial:'
      end
      object Label202: TLabel
        Left = 15
        Top = 124
        Width = 253
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Endere'#231'o para tipo igual a "Descrito aqui":'
      end
      object SpeedButton16: TSpeedButton
        Left = 586
        Top = 95
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton16Click
      end
      object RGBloqEndTip: TdmkRadioGroup
        Left = 2
        Top = 18
        Width = 618
        Height = 51
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Caption = ' Quem vai receber: '
        Columns = 5
        ItemIndex = 0
        Items.Strings = (
          'Autom'#225'tico'
          'Morador'
          'Propriet'#225'rio'
          'Terceiro'
          'Descrito aqui')
        TabOrder = 0
        QryCampo = 'BloqEndTip'
        UpdCampo = 'BloqEndTip'
        UpdType = utYes
        OldValor = 0
      end
      object EdBloqEndEnt: TdmkEditCB
        Left = 15
        Top = 95
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'BloqEndEnt'
        UpdCampo = 'BloqEndEnt'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBBloqEndEnt
        IgnoraDBLookupComboBox = False
      end
      object CBBloqEndEnt: TdmkDBLookupComboBox
        Left = 89
        Top = 95
        Width = 493
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsBloqEndEnt
        TabOrder = 2
        OnClick = CBBloqEndEntClick
        dmkEditCB = EdBloqEndEnt
        QryCampo = 'BloqEndEnt'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEnderNome: TdmkEdit
        Left = 15
        Top = 146
        Width = 595
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'EnderNome'
        UpdCampo = 'EnderNome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEnderLin1: TdmkEdit
        Left = 15
        Top = 172
        Width = 595
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'EnderLin1'
        UpdCampo = 'EnderLin1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEnderLin2: TdmkEdit
        Left = 15
        Top = 198
        Width = 595
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'EnderLin2'
        UpdCampo = 'EnderLin2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GroupBox5: TGroupBox
      Left = 610
      Top = 478
      Width = 622
      Height = 183
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Celular (cadastro de entidades "A") para envio de SMS: '
      TabOrder = 24
      object Label6: TLabel
        Left = 15
        Top = 73
        Width = 158
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Terceiro a receber o SMS:'
      end
      object Label5: TLabel
        Left = 15
        Top = 124
        Width = 373
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#250'mero do celular e...    Nome da pessoa para "Descrito aqui":'
      end
      object SBSMSCelEnti: TSpeedButton
        Left = 586
        Top = 95
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SBSMSCelEntiClick
      end
      object RGSMSCelTipo: TdmkRadioGroup
        Left = 15
        Top = 20
        Width = 597
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Quem vai receber: '
        Columns = 5
        ItemIndex = 2
        Items.Strings = (
          'Autom'#225'tico'
          'Morador'
          'Propriet'#225'rio'
          'Terceiro'
          'Descrito aqui')
        TabOrder = 0
        QryCampo = 'SMSCelTipo'
        UpdCampo = 'SMSCelTipo'
        UpdType = utYes
        OldValor = 0
      end
      object EdSMSCelEnti: TdmkEditCB
        Left = 15
        Top = 95
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SMSCelEnti'
        UpdCampo = 'SMSCelEnti'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSMSCelEnti
        IgnoraDBLookupComboBox = False
      end
      object CBSMSCelEnti: TdmkDBLookupComboBox
        Left = 89
        Top = 95
        Width = 493
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSMSCelEnti
        TabOrder = 2
        dmkEditCB = EdSMSCelEnti
        QryCampo = 'SMSCelEnti'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSMSCelNumr: TdmkEdit
        Left = 15
        Top = 146
        Width = 138
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtTelLongo
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'SMSCelNumr'
        UpdCampo = 'SMSCelNumr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSMSCelNome: TdmkEdit
        Left = 158
        Top = 146
        Width = 454
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'SMSCelNome'
        UpdCampo = 'SMSCelNome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 247
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 109
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Im'#243'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 109
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Im'#243'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 109
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Im'#243'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 306
      Top = 0
      Width = 876
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 18
        Width = 872
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 16
          Top = 2
          Width = 150
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 15
          Top = 1
          Width = 150
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 750
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 1060
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrStatus: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM status'
      'ORDER BY Nome')
    Left = 12
    Top = 12
    object QrStatusCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'status.Codigo'
    end
    object QrStatusCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStatusNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsStatus: TDataSource
    DataSet = QrStatus
    Left = 40
    Top = 12
  end
  object QrImovPro: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CASE WHEN Tipo=0 '
      'THEN RazaoSocial ELSE Nome END Nome'
      'FROM Entidades'
      'WHERE Ativo = 1 '
      'AND Cliente2='#39'V'#39
      'ORDER BY Nome')
    Left = 96
    Top = 12
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'entidades.CodUsu'
    end
  end
  object DsImovPro: TDataSource
    DataSet = QrImovPro
    Left = 124
    Top = 12
  end
  object QrImovInq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CASE WHEN Tipo=0 '
      'THEN RazaoSocial ELSE Nome END Nome'
      'FROM Entidades'
      'WHERE Ativo = 1'
      'AND (Cliente3 = '#39'V'#39' OR Cliente4 = '#39'V'#39')'
      'ORDER BY Nome')
    Left = 152
    Top = 12
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrImovInqCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'entidades.CodUsu'
    end
  end
  object DsImovInq: TDataSource
    DataSet = QrImovInq
    Left = 180
    Top = 12
  end
  object QrConjuges: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 '
      'THEN RazaoSocial ELSE Nome END NOME'
      'FROM entidades'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 208
    Top = 12
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Origin = 'entidades.Nome'
      Size = 100
    end
  end
  object DsConjuges: TDataSource
    DataSet = QrConjuges
    Left = 236
    Top = 12
  end
  object QrImobiliarias: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, RazaoSocial, EContato, ETe1'
      'FROM entidades'
      'WHERE Ativo = 1'
      'AND (Fornece1 = '#39'V'#39'  OR Terceiro = '#39'V'#39')'
      'ORDER BY RazaoSocial')
    Left = 264
    Top = 12
    object QrImobiliariasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImobiliariasRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 100
    end
    object QrImobiliariasEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrImobiliariasETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrImobiliariasIMOBTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IMOBTEL_TXT'
      Calculated = True
    end
  end
  object DsImobiliarias: TDataSource
    DataSet = QrImobiliarias
    Left = 292
    Top = 12
  end
  object DsBloqEndEnt: TDataSource
    DataSet = QrBloqEndEnt
    Left = 348
    Top = 12
  end
  object QrProcuradores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME_ENT'
      'FROM entidades'
      'ORDER BY NOME_ENT')
    Left = 376
    Top = 12
    object QrProcuradoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProcuradoresNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Required = True
      Size = 100
    end
  end
  object DsProcuradores: TDataSource
    DataSet = QrProcuradores
    Left = 404
    Top = 12
  end
  object QrBloqEndEnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 '
      'THEN RazaoSocial ELSE Nome END NOME'
      'FROM entidades'
      'WHERE Terceiro = '#39'V'#39
      'ORDER BY Nome')
    Left = 320
    Top = 12
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField5: TWideStringField
      FieldName = 'Nome'
      Origin = 'entidades.Nome'
      Size = 100
    end
  end
  object VUStatus: TdmkValUsu
    dmkEditCB = EdStatus
    QryCampo = 'Status'
    UpdCampo = 'Status'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 68
    Top = 12
  end
  object QrInadCEMCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM DiarCEMCad'
      'ORDER BY Nome')
    Left = 844
    Top = 416
    object QrInadCEMCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInadCEMCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsInadCEMCad: TDataSource
    DataSet = QrInadCEMCad
    Left = 872
    Top = 416
  end
  object QrSMSCelEnti: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 '
      'THEN RazaoSocial ELSE Nome END NOME'
      'FROM entidades'
      'WHERE Terceiro = '#39'V'#39
      'ORDER BY Nome')
    Left = 432
    Top = 12
    object QrSMSCelEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSMSCelEntiNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
  end
  object DsSMSCelEnti: TDataSource
    DataSet = QrSMSCelEnti
    Left = 460
    Top = 12
  end
end
