unit CondGerImpArre;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Variants,Grids, DB, ABSMain,
  DBGrids, dmkDBGrid, DBCtrls, mySQLDbTables, ComCtrls, frxClass, frxDBSet,
  dmkGeral, UnDmkProcFunc, dmkImage, UnDmkEnums, UnDmkABS_PF;

type
  THackDBGrid = class(TDBGrid);
  TFmCondGerImpArre = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdTitulo: TEdit;
    QrPesq: TmySQLQuery;
    QrPesqNivel1: TIntegerField;
    QrPesqCodUsu: TIntegerField;
    QrPesqNome: TWideStringField;
    DataSource1: TDataSource;
    Query: TABSQuery;
    DBGrid1: TdmkDBGrid;
    QrPesqPreco: TFloatField;
    PB2: TProgressBar;
    QueryConta: TIntegerField;
    QueryArreBaI: TIntegerField;
    QueryVALOR: TFloatField;
    QueryAtivo: TSmallintField;
    QueryTEXTO_IMP: TWideStringField;
    StaticText1: TStaticText;
    frxListaA: TfrxReport;
    QrIAINOMEPROPRIET: TWideStringField;
    QrIAIUnidade: TWideStringField;
    QrIAIApto: TIntegerField;
    QrIAIValor: TFloatField;
    QrIAIPropriet: TIntegerField;
    QrIAIControle: TIntegerField;
    QrIAIDeQuem: TSmallintField;
    QrIAICalculo: TSmallintField;
    QrIAIPartilha: TSmallintField;
    QrIAIPercent: TFloatField;
    QrIAIMoradores: TIntegerField;
    QrIAIFracaoIdeal: TFloatField;
    QrIAIArreBaI: TIntegerField;
    QrIAIDescriCota: TWideStringField;
    QrIAIFATOR_COBRADO: TFloatField;
    QrIAITITULO_FATOR: TWideStringField;
    QueryCodigo: TIntegerField;
    PB1: TProgressBar;
    frxDsIAI: TfrxDBDataset;
    QrIAI: TABSQuery;
    frxDsCond: TfrxDBDataset;
    frxDsCNS: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure frxListaAGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    procedure AtivaItensQuery(Ativo: Integer);
    procedure ReacertaFrxDataSets();

  public
    { Public declarations }
    FCadastrar: Boolean;
  end;

  var
  FmCondGerImpArre: TFmCondGerImpArre;

implementation

uses Module, UMySQLModule, ModuleCond, CondGer, ModuleBloq, UnMyObjects, ModuleGeral;

{$R *.DFM}

const
  FTextoPadrao =
  'Clique duplo no item de arrecada��o iguala o t�tulo ao texto da arrecada��o';

procedure TFmCondGerImpArre.AtivaItensQuery(Ativo: Integer);
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('UPDATE cgia SET Ativo=' + dmkPF.FFP(Ativo, 0) + '; ');
  Query.SQL.Add('SELECT * FROM cgia; ');
  DmkABS_PF.AbreQuery(Query);
end;

procedure TFmCondGerImpArre.BtNenhumClick(Sender: TObject);
begin
  AtivaItensQuery(0);
end;

procedure TFmCondGerImpArre.BtOKClick(Sender: TObject);
var
  Lin: String;
  Codigo: Variant;
begin
  if Trim(EdTitulo.Text) = '' then
  begin
    Geral.MensagemBox('Informe o "T�tulo do relat�rio"!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    Query.Filter   := 'Ativo = 1';
    Query.Filtered := True;

    if Query.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max := Query.RecordCount;
      QrIAI.Close;
      QrIAI.SQL.Clear;
      QrIAI.SQL.Add('DROP TABLE IAI; ');
      QrIAI.SQL.Add('CREATE TABLE IAI (');
      QrIAI.SQL.Add('  NOMEPROPRIET  varchar(100)      ,');
      QrIAI.SQL.Add('  Unidade       varchar(10)       ,');
      QrIAI.SQL.Add('  Apto          integer           ,');
      QrIAI.SQL.Add('  Valor         float             ,');
      QrIAI.SQL.Add('  Propriet      integer           ,');
      QrIAI.SQL.Add('  Controle      integer           ,');
      QrIAI.SQL.Add('  DeQuem        smallint          ,');
      QrIAI.SQL.Add('  Calculo       smallint          ,');
      QrIAI.SQL.Add('  Partilha      smallint          ,');
      QrIAI.SQL.Add('  Percent       float             ,');
      QrIAI.SQL.Add('  Moradores     integer           ,');
      QrIAI.SQL.Add('  FracaoIdeal   float             ,');
      QrIAI.SQL.Add('  ArreBaI       integer           ,');
      QrIAI.SQL.Add('  DescriCota    varchar(10)       ,');
      QrIAI.SQL.Add('  FATOR_COBRADO float             ,');
      QrIAI.SQL.Add('  TITULO_FATOR  varchar(30)       ,');
      QrIAI.SQL.Add('  Ativo         smallint           ');
      QrIAI.SQL.Add(');');

      Lin := 'INSERT INTO iai (NOMEPROPRIET,Unidade,Apto,Valor,Propriet,' +
             'Controle,DeQuem,Calculo,Partilha,Percent,Moradores,FracaoIdeal,' +
             'ArreBaI,DescriCota,FATOR_COBRADO,TITULO_FATOR,Ativo) VALUES (';

      Query.First;
      while not Query.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        Update;
        Application.ProcessMessages;
        if QueryCodigo.Value = 0 then
          Codigo := Null
        else
          Codigo := QueryCodigo.Value;
        //
        if DmBloq.QrListaA.Locate('Conta;ArreBaI;Codigo', VarArrayOf([
          QueryConta.Value, QueryArreBaI.Value, Codigo]), []) then
        begin
          DmBloq.QrListaAIts.First;
          PB2.Position := 0;
          PB2.Max := DmBloq.QrListaAIts.RecordCount;
          while not DmBloq.QrListaAIts.Eof do
          begin
            PB2.Position := PB2.Position + 1;
            Update;
            Application.ProcessMessages;
            QrIAI.SQL.Add(Lin + //'(' +
              '"' + DmBloq.QrListaAItsNOMEPROPRIET.Value + '",' +
              '"' + DmBloq.QrListaAItsUnidade.Value + '",' +
              dmkPF.FFP(DmBloq.QrListaAItsApto.Value, 0) + ',' +
              dmkPF.FFP(DmBloq.QrListaAItsValor.Value, 2) + ',' +
              dmkPF.FFP(DmBloq.QrListaAItsPropriet.Value, 0) + ',' +
              dmkPF.FFP(DmBloq.QrListaAItsControle.Value, 0) + ',' +
              dmkPF.FFP(DmBloq.QrListaAItsDeQuem.Value, 0) + ',' +
              dmkPF.FFP(DmBloq.QrListaAItsCalculo.Value, 0) + ',' +
              dmkPF.FFP(DmBloq.QrListaAItsPartilha.Value, 0) + ',' +
              dmkPF.FFP(DmBloq.QrListaAItsPercent.Value, 6) + ',' +
              dmkPF.FFP(DmBloq.QrListaAItsMoradores.Value, 0) + ',' +
              dmkPF.FFP(DmBloq.QrListaAItsFracaoIdeal.Value, 6) + ',' +
              dmkPF.FFP(DmBloq.QrListaAItsArreBaI.Value, 0) + ',' +
              '"' + DmBloq.QrListaAItsDescriCota.Value + '",' +
              dmkPF.FFP(DmBloq.QrListaAItsFATOR_COBRADO.Value, 0) + ',' +
              '"' + DmBloq.QrListaAItsTITULO_FATOR.Value + '",' +
            '0);');
            DmBloq.QrListaAIts.Next;
          end;
          //
        end else
        begin
          Geral.MensagemBox('O grupo "' +
          QueryTEXTO_IMP.Value + '" n�o foi lozalizado!', 'ERRO',
          MB_OK+MB_ICONERROR);
        end;
        Query.Next;
      end;
      //
      QrIAI.SQL.Add('SELECT * FROM iai;');
      DmkABS_PF.AbreQuery(QrIAI);
      ReacertaFrxDataSets();
      MyObjects.frxMostra(frxListaA, EdTitulo.Text);
    end else begin
      Geral.MensagemBox('Nenhum item foi selecionado!', 'Avido',
      MB_OK+MB_ICONWARNING);
    end;
  finally
    Screen.Cursor := crDefault;
    Query.Filtered := False;
    PB1.Position := 0;
    PB2.Position := 0;
  end;
end;

procedure TFmCondGerImpArre.BtSaidaClick(Sender: TObject);
begin
  FCadastrar := False;
  Close;
end;

procedure TFmCondGerImpArre.BtTudoClick(Sender: TObject);
begin
  AtivaItensQuery(1);
end;

procedure TFmCondGerImpArre.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if Query.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    Query.Edit;
    Query.FieldByName('Ativo').Value := Status;
    Query.Post;
  end;
end;

procedure TFmCondGerImpArre.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = 'Preco' then
    DBGrid1.Options := DBGrid1.Options + [dgEditing] else
    DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmCondGerImpArre.DBGrid1ColExit(Sender: TObject);
begin
  DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmCondGerImpArre.DBGrid1DblClick(Sender: TObject);
begin
  EdTitulo.Text := QueryTEXTO_IMP.Value;
end;

procedure TFmCondGerImpArre.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerImpArre.FormCreate(Sender: TObject);
var
  Lin: String;
begin
  ImgTipo.SQLType := stLok;
  //
  { TODO 1 -oMarcelo -cBUG : Testar este relat�rio e arrumar os DataSets }
  FCadastrar := False;
  DmBloq.ReopenListaA;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE CGIA; ');
  Query.SQL.Add('CREATE TABLE CGIA (');
  Query.SQL.Add('  Conta     integer      ,');
  Query.SQL.Add('  ArreBaI   integer      ,');
  Query.SQL.Add('  Codigo    integer      ,');
  Query.SQL.Add('  TEXTO_IMP varchar(50)  ,');
  Query.SQL.Add('  VALOR     float        ,');
  Query.SQL.Add('  Ativo     smallint      ');
  Query.SQL.Add(');');
  //
  Lin := 'INSERT INTO cgia (Conta,ArreBaI,Codigo,TEXTO_IMP,Valor,Ativo) Values (';
  DmBloq.QrListaA.First;
  while not DmBloq.QrListaA.Eof do
  begin
    Query.SQL.Add(Lin + //'(' +
      dmkPF.FFP(DmBloq.QrListaAConta.Value, 0) + ',' +
      dmkPF.FFP(DmBloq.QrListaAArreBaI.Value, 0) + ',' +
      dmkPF.FFP(DmBloq.QrListaACodigo.Value, 0) + ',' +
      '"' + DmBloq.QrListaATEXTO_IMP.Value + '",' +
      dmkPF.FFP(DmBloq.QrListaAVALOR.Value , 2) + ',' +
    '0);');
    //
    DmBloq.QrListaA.Next;
  end;
  //
  Query.SQL.Add('SELECT * FROM cgia');
  DmkABS_PF.AbreQuery(Query);
end;

procedure TFmCondGerImpArre.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerImpArre.frxListaAGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULO' then
    Value := EdTitulo.Text;
end;

procedure TFmCondGerImpArre.ReacertaFrxDataSets();
begin
  frxDsCond.DataSet := DmCond.QrCond;
  frxListaA.DataSets.Add(frxDsCond);
  frxListaA.EnabledDataSets.Add(frxDsCond);
  frxDsCNS.DataSet  := DmCond.QrCNS;
  //
  MyObjects.frxDefineDataSets(frxListaA, [
    DModG.frxDsDono,
    frxDsCond,
    FmCondGer.frxDsPrev,
    frxDsIAI
    ], False);
end;

end.

