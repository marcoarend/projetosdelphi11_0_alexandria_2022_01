unit CondImov;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkRadioGroup, Mask, dmkDBEdit,
  dmkValUsu, DB, mySQLDbTables, dmkGeral, dmkCheckBox, Variants, dmkMemo,
  dmkImage, UnDmkEnums;

type
  TFmCondImov = class(TForm)
    Panel1: TPanel;
    Label12: TLabel;
    EdCodigo: TdmkEdit;
    dmkDBEdCodigo: TdmkDBEdit;
    Label1: TLabel;
    QrStatus: TmySQLQuery;
    QrStatusCodigo: TIntegerField;
    DsStatus: TDataSource;
    QrImovPro: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    DsImovPro: TDataSource;
    EdAndar: TdmkEdit;
    EdUnidade: TdmkEdit;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    Label45: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    SpeedButton7: TSpeedButton;
    EdPropriet: TdmkEditCB;
    Label53: TLabel;
    CBPropriet: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    CBInquilino: TdmkDBLookupComboBox;
    EdInquilino: TdmkEditCB;
    Label44: TLabel;
    QrImovInq: TmySQLQuery;
    IntegerField4: TIntegerField;
    StringField4: TWideStringField;
    DsImovInq: TDataSource;
    dmkDBEdit1: TdmkDBEdit;
    Label2: TLabel;
    QrImovInqCodUsu: TIntegerField;
    CkContinuar: TCheckBox;
    RGddVctEsp: TdmkRadioGroup;
    QrConjuges: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField2: TWideStringField;
    DsConjuges: TDataSource;
    Label65: TLabel;
    EdConjuge: TdmkEditCB;
    CBConjuge: TdmkDBLookupComboBox;
    SpeedButton10: TSpeedButton;
    QrImobiliarias: TmySQLQuery;
    QrImobiliariasCodigo: TIntegerField;
    QrImobiliariasRazaoSocial: TWideStringField;
    QrImobiliariasEContato: TWideStringField;
    QrImobiliariasETe1: TWideStringField;
    QrImobiliariasIMOBTEL_TXT: TWideStringField;
    DsImobiliarias: TDataSource;
    DsBloqEndEnt: TDataSource;
    QrProcuradores: TmySQLQuery;
    QrProcuradoresCodigo: TIntegerField;
    QrProcuradoresNOME_ENT: TWideStringField;
    DsProcuradores: TDataSource;
    QrBloqEndEnt: TmySQLQuery;
    IntegerField5: TIntegerField;
    StringField5: TWideStringField;
    Label203: TLabel;
    EdProcurador: TdmkEditCB;
    CBProcurador: TdmkDBLookupComboBox;
    MeObserv: TdmkMemo;
    Label32: TLabel;
    EdQtdGaragem: TdmkEdit;
    Label190: TLabel;
    EdMoradores: TdmkEdit;
    EdFracaoIdeal: TdmkEdit;
    Label194: TLabel;
    Label3: TLabel;
    Label196: TLabel;
    QrStatusCodUsu: TIntegerField;
    QrStatusNome: TWideStringField;
    VUStatus: TdmkValUsu;
    CkUmAUm: TCheckBox;
    RGJuridico: TdmkRadioGroup;
    CkSitImv: TdmkCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    EdImobiliaria: TdmkEditCB;
    CBImobiliaria: TdmkDBLookupComboBox;
    SpeedButton11: TSpeedButton;
    Label128: TLabel;
    DBImov_Cont: TDBEdit;
    Label127: TLabel;
    DBImov_ContTel: TDBEdit;
    Label4: TLabel;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    SBInadCEMCad: TSpeedButton;
    EdInadCEMCad: TdmkEditCB;
    CBInadCEMCad: TdmkDBLookupComboBox;
    QrInadCEMCad: TmySQLQuery;
    DsInadCEMCad: TDataSource;
    QrInadCEMCadCodigo: TIntegerField;
    QrInadCEMCadNome: TWideStringField;
    GroupBox4: TGroupBox;
    RGBloqEndTip: TdmkRadioGroup;
    Label201: TLabel;
    EdBloqEndEnt: TdmkEditCB;
    CBBloqEndEnt: TdmkDBLookupComboBox;
    Label202: TLabel;
    EdEnderNome: TdmkEdit;
    EdEnderLin1: TdmkEdit;
    EdEnderLin2: TdmkEdit;
    SpeedButton16: TSpeedButton;
    GroupBox5: TGroupBox;
    RGSMSCelTipo: TdmkRadioGroup;
    Label6: TLabel;
    EdSMSCelEnti: TdmkEditCB;
    CBSMSCelEnti: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdSMSCelNumr: TdmkEdit;
    EdSMSCelNome: TdmkEdit;
    SBSMSCelEnti: TSpeedButton;
    QrSMSCelEnti: TmySQLQuery;
    DsSMSCelEnti: TDataSource;
    QrSMSCelEntiCodigo: TIntegerField;
    QrSMSCelEntiNOME: TWideStringField;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure SpeedButton16Click(Sender: TObject);
    procedure SBInadCEMCadClick(Sender: TObject);
    procedure CBBloqEndEntClick(Sender: TObject);
    procedure SBSMSCelEntiClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    FPropriet: Integer;
  public
    { Public declarations }
  end;

  var
  FmCondImov: TFmCondImov;

implementation

uses Principal, UnInternalConsts, UMySQLModule, Cond, Module, DmkDAC_PF,
MyDBCheck, Status, ModuleGeral, ModuleCond, UnMyObjects;

{$R *.DFM}

procedure TFmCondImov.BtOKClick(Sender: TObject);
var
  Conta, Codigo, Controle, Propriet: Integer;
  UH: String;
begin
  if RGddVctEsp.Itemindex = -1 then
    RGddVctEsp.Itemindex := 0;
  //  
  Propriet := EdPropriet.ValueVariant;
  UH       := EdUnidade.ValueVariant;
  //
  if Propriet = 0 then
  begin
    Geral.MensagemBox('"' + Label53.Caption + '" n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdPropriet.SetFocus;
    Exit;
  end;
  if Length(UH) = 0 then
  begin
    Geral.MensagemBox('"' + Label61.Caption + '"  n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdUnidade.SetFocus;
    Exit;
  end;
  //
  Codigo   := FmCond.QrCondCodigo.Value;
  Controle := FmCond.QrCondBlocoControle.Value;
  Conta    := UMyMod.BuscaEmLivreY_Def('condimov', 'Conta', ImgTipo.SQLType,
    FmCond.QrCondImovConta.Value);
  if UMyMod.ExecSQLInsUpdFm(FmCondImov, ImgTipo.SQLType, 'condimov', Conta,
    Dmod.QrUpd) then
  begin
    DmCond.AtualizaSomaFracaoApto(FmCond.QrCondBlocoControle.Value, FmCond.QrCondCodigo.Value);
    FmCond.LocCod(Codigo, Codigo);
    FmCond.ReopenCondBloco(Controle);
    FmCond.ReopenCondImov(Conta);
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
      ImgTipo.SQLType           := stIns;
      EdUnidade.ValueVariant   := '';
      EdStatus.ValueVariant    := 0;
      CBStatus.KeyValue        := Null;
      EdPropriet.ValueVariant  := 0;
      CBPropriet.KeyValue      := Null;
      EdInquilino.ValueVariant := 0;
      CBInquilino.KeyValue     := Null;
      EdAndar.SetFocus;
    end else
    begin
      //FmCond.CkUmAUm.Checked := CkUmAUm.Checked;
      Close;
    end;
  end;
end;

procedure TFmCondImov.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondImov.CBBloqEndEntClick(Sender: TObject);
begin
   // UnDmkDAC_PF.AbreQuery(QrConjuges, Dmod.MyDB);
end;

procedure TFmCondImov.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  EdAndar.SetFocus;
end;

procedure TFmCondImov.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrStatus, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrImovPro, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrImovInq, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrConjuges, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrImobiliarias, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBloqEndEnt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcuradores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrInadCEMCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSMSCelEnti, Dmod.MyDB);
  //CkUmAUm.Visible := FmCond.PnUmAUm.Visible;
  CkUmAUm.Checked := CkUmAUm.Checked;
end;

procedure TFmCondImov.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondImov.FormShow(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := ImgTipo.SQLType = stIns;
  //
  CkContinuar.Visible := Enab;
  CkContinuar.Checked := Enab;
end;

procedure TFmCondImov.SBInadCEMCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.MostraFormDiarCEMCad();
  //
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrInadCEMCad, Dmod.MyDB);
    UMyMod.SetaCodigoPesquisado(EdInadCEMCad, CBInadCEMCad, QrInadCEMCad, VAR_CADASTRO);
    EdInadCEMCad.SetFocus;
  end;
end;

procedure TFmCondImov.SBSMSCelEntiClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdSMSCelEnti.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrSMSCelEnti, Dmod.MyDB);
    UMyMod.SetaCodigoPesquisado(EdSMSCelEnti, CBSMSCelEnti, QrSMSCelEnti, VAR_CADASTRO);
    EdSMSCelEnti.SetFocus;
  end;
end;

procedure TFmCondImov.SpeedButton10Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdConjuge.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrConjuges, Dmod.MyDB);
    UMyMod.SetaCodigoPesquisado(EdConjuge, CBConjuge, QrConjuges, VAR_CADASTRO);
    EdConjuge.SetFocus;
  end;
end;

procedure TFmCondImov.SpeedButton11Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdImobiliaria.ValueVariant, fmcadEntidade2,
    fmcadEntidade2, False, False, nil, nil, False, uetFornece1);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrImobiliarias, Dmod.MyDB);
    UMyMod.SetaCodigoPesquisado(EdImobiliaria, CBImobiliaria, QrImobiliarias, VAR_CADASTRO);
    EdImobiliaria.SetFocus;
  end;
end;

procedure TFmCondImov.SpeedButton16Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdBloqEndEnt.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrBloqEndEnt, Dmod.MyDB);
    UMyMod.SetaCodigoPesquisado(EdBloqEndEnt, CBBloqEndEnt, QrBloqEndEnt, VAR_CADASTRO);
    EdBloqEndEnt.SetFocus;
  end;
end;

procedure TFmCondImov.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdProcurador.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrProcuradores, Dmod.MyDB);
    UMyMod.SetaCodigoPesquisado(EdProcurador, CBProcurador, QrProcuradores, VAR_CADASTRO);
    EdProcurador.SetFocus;
  end;
end;

procedure TFmCondImov.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdPropriet.ValueVariant, fmcadEntidade2,
    fmcadEntidade2, False, False, nil, nil, False, uetCliente2);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrImovPro, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdPropriet, CBPropriet, QrImovPro, VAR_CADASTRO);
    EdPropriet.SetFocus;
  end;
end;

procedure TFmCondImov.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdInquilino.ValueVariant, fmcadEntidade2,
    fmcadEntidade2, False, False, nil, nil, False, uetCliente4);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrImovInq, Dmod.MyDB);
    UMyMod.SetaCodigoPesquisado(EdInquilino, CBInquilino, QrImovInq, VAR_CADASTRO);
    EdInquilino.SetFocus;
  end;
end;

procedure TFmCondImov.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmStatus, FmStatus, afmoNegarComAviso) then
  begin
    FmStatus.ShowModal;
    FmStatus.Destroy;
  end;
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrStatus, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdStatus, CBStatus, QrStatus, VAR_CADASTRO);
    EdStatus.SetFocus;
  end;
end;

end.
