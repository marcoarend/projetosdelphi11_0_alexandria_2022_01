object DmAtencoes: TDmAtencoes
  OldCreateOrder = False
  Height = 633
  Width = 935
  object QrErrRepa: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrErrRepaBeforeClose
    AfterScroll = QrErrRepaAfterScroll
    SQL.Strings = (
      '')
    Left = 36
    object QrErrRepaData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrRepaTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrErrRepaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000'
    end
    object QrErrRepaSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrErrRepaDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrErrRepaCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrErrRepaCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrRepaDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000'
    end
    object QrErrRepaVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrRepaID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
      DisplayFormat = '000000'
    end
    object QrErrRepaCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrErrRepaCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrErrRepaDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrErrRepaMES: TWideStringField
      FieldName = 'MES'
      Required = True
      Size = 7
    end
    object QrErrRepaNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
    object QrErrRepaNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrErrRepaCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrErrRepaGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrErrRepaAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrErrRepaQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrErrRepaNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrErrRepaDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrErrRepaSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrErrRepaFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrErrRepaFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrErrRepaFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrErrRepaID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrErrRepaFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrErrRepaEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrErrRepaBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrErrRepaAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrErrRepaContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrErrRepaCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrErrRepaLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrErrRepaCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrErrRepaLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrErrRepaOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrErrRepaLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrErrRepaPago: TFloatField
      FieldName = 'Pago'
    end
    object QrErrRepaMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrErrRepaFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrErrRepaForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrErrRepaMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrErrRepaMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrErrRepaProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrErrRepaDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrErrRepaCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrErrRepaNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrErrRepaVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrErrRepaAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrErrRepaICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrErrRepaICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrErrRepaDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrErrRepaDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrErrRepaDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrErrRepaDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrErrRepaNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrErrRepaAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrErrRepaExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrErrRepaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrErrRepaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrErrRepaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrErrRepaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrErrRepaSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrErrRepaDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrErrRepaMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrErrRepaMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrErrRepaCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrErrRepaTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrErrRepaReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrErrRepaID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrErrRepaAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrErrRepaPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrErrRepaPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrErrRepaUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrErrRepaFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsErrRepa: TDataSource
    DataSet = QrErrRepa
    Left = 116
  end
  object DsErrRepaIts: TDataSource
    DataSet = QrErrRepaIts
    Left = 116
    Top = 48
  end
  object QrErrRepaIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrErrRepaItsBeforeClose
    Left = 36
    Top = 48
    object QrErrRepaItsData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrErrRepaItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrErrRepaItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrErrRepaItsVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrErrRepaItsCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrErrRepaItsMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrErrRepaItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrErrRepaItsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrErrRepaItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrErrRepaItsForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrErrRepaItsMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrErrRepaItsDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrErrRepaItsVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      Required = True
    end
    object QrErrRepaItsVlrMulta: TFloatField
      FieldName = 'VlrMulta'
      Required = True
    end
    object QrErrRepaItsVlrJuros: TFloatField
      FieldName = 'VlrJuros'
      Required = True
    end
    object QrErrRepaItsVlrTotal: TFloatField
      FieldName = 'VlrTotal'
      Required = True
    end
    object QrErrRepaItsMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrErrRepaItsMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrErrRepaItsVlrAjust: TFloatField
      FieldName = 'VlrAjust'
      Required = True
    end
    object QrErrRepaItsCTRL_LAN: TIntegerField
      FieldName = 'CTRL_LAN'
      Required = True
    end
    object QrErrRepaItsCTRL_REP: TIntegerField
      FieldName = 'CTRL_REP'
      Required = True
    end
    object QrErrRepaItsCONTA_REP: TAutoIncField
      FieldName = 'CONTA_REP'
    end
    object QrErrRepaItsFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrErrRepaOri: TmySQLQuery
    Database = Dmod.MyDB
    Left = 36
    Top = 96
    object QrErrRepaOriAtrelado: TIntegerField
      FieldName = 'Atrelado'
    end
  end
  object QrErrRepaIt2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT lan.Data, lan.Controle CTRL_LAN, lan.Descricao, lan.Gener' +
        'o,'
      'lan.Vencimento, lan.Credito, lan.Mez, lan.FatNum, lan.FatID,'
      'lan.CliInt, lan.Cliente, lan.ForneceI, lan.Multa, lan.Depto,'
      'lan.MultaVal, lan.MoraVal, bpi.VlrAjust, bpi.Controle CTRL_REP,'
      'bpi.VlrOrigi, bpi.VlrMulta, bpi.VlrJuros, bpi.VlrTotal, '
      'bpi.Conta CONTA_REP'
      'FROM bloqparcits bpi'
      'LEFT JOIN VAR LCT lan ON lan.Controle=bpi.CtrlOrigi'
      'WHERE bpi.Controle=:P0'
      'ORDER BY bpi.VlrTotal ASC')
    Left = 116
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrErrRepaIt2Data: TDateField
      FieldName = 'Data'
    end
    object QrErrRepaIt2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrErrRepaIt2Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrErrRepaIt2Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrErrRepaIt2Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrErrRepaIt2Mez: TIntegerField
      FieldName = 'Mez'
    end
    object QrErrRepaIt2FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrErrRepaIt2CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrErrRepaIt2Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrErrRepaIt2ForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrErrRepaIt2Multa: TFloatField
      FieldName = 'Multa'
    end
    object QrErrRepaIt2Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrErrRepaIt2MultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrErrRepaIt2MoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrErrRepaIt2VlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      Required = True
    end
    object QrErrRepaIt2VlrMulta: TFloatField
      FieldName = 'VlrMulta'
      Required = True
    end
    object QrErrRepaIt2VlrJuros: TFloatField
      FieldName = 'VlrJuros'
      Required = True
    end
    object QrErrRepaIt2VlrTotal: TFloatField
      FieldName = 'VlrTotal'
      Required = True
    end
    object QrErrRepaIt2VlrAjust: TFloatField
      FieldName = 'VlrAjust'
      Required = True
    end
    object QrErrRepaIt2CTRL_LAN: TIntegerField
      FieldName = 'CTRL_LAN'
    end
    object QrErrRepaIt2CTRL_REP: TIntegerField
      FieldName = 'CTRL_REP'
      Required = True
    end
    object QrErrRepaIt2CONTA_REP: TIntegerField
      FieldName = 'CONTA_REP'
      Required = True
    end
    object QrErrRepaIt2FatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsErrRepaSum: TDataSource
    DataSet = QrErrRepaSum
    Left = 116
    Top = 144
  end
  object QrErrRepaSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ' '
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 36
    Top = 144
    object QrErrRepaSumCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrErrRepaSumMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrErrRepaSumMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrErrRepaSumVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
    end
    object QrErrRepaSumVlrMulta: TFloatField
      FieldName = 'VlrMulta'
    end
    object QrErrRepaSumVlrJuros: TFloatField
      FieldName = 'VlrJuros'
    end
    object QrErrRepaSumVlrTotal: TFloatField
      FieldName = 'VlrTotal'
    end
    object QrErrRepaSumVlrAjust: TFloatField
      FieldName = 'VlrAjust'
    end
  end
  object QrIncorr: TmySQLQuery
    Database = Dmod.MyDB
    Left = 36
    Top = 192
    object QrIncorrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIncorrData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrIncorrDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrIncorrCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrIncorrTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrIncorrCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrIncorrSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrIncorrVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrIncorrCompensado: TDateField
      FieldName = 'Compensado'
    end
  end
  object QrBPI2: TmySQLQuery
    Database = Dmod.MyDB
    Left = 116
    Top = 240
    object QrBPI2CTRL_LAN: TIntegerField
      FieldName = 'CTRL_LAN'
    end
    object QrBPI2SUB_LAN: TSmallintField
      FieldName = 'SUB_LAN'
    end
    object QrBPI2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBPI2Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBPI2VlrTotal: TFloatField
      FieldName = 'VlrTotal'
      Required = True
    end
    object QrBPI2MultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrBPI2CTRL_REP: TIntegerField
      FieldName = 'CTRL_REP'
      Required = True
    end
    object QrBPI2Dias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrBPI2VlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      Required = True
    end
    object QrBPI2Fator: TFloatField
      FieldName = 'Fator'
      Required = True
    end
  end
  object QrIncorr2_: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 36
    Top = 240
    object QrIncorr2_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIncorr2_Data: TDateField
      FieldName = 'Data'
    end
    object QrIncorr2_Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrIncorr2_Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrIncorr2_Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrIncorr2_Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrIncorr2_Sit: TIntegerField
      FieldName = 'Sit'
    end
    object QrIncorr2_Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrIncorr2_Compensado: TDateField
      FieldName = 'Compensado'
    end
  end
  object Query: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 36
    Top = 288
    object QueryOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QueryOrigi: TFloatField
      FieldName = 'Origi'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QueryDias: TFloatField
      FieldName = 'Dias'
    end
    object QueryFator: TFloatField
      FieldName = 'Fator'
    end
    object QueryValDif: TFloatField
      FieldName = 'ValDif'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QueryValItem: TFloatField
      FieldName = 'ValItem'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QueryAtivo: TIntegerField
      FieldName = 'Ativo'
    end
    object QueryRestaFat: TFloatField
      FieldName = 'RestaFat'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QueryRestaDif: TFloatField
      FieldName = 'RestaDif'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 116
    Top = 288
  end
  object DataSource2: TDataSource
    DataSet = QrSum
    Left = 116
    Top = 336
  end
  object QrSum: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 36
    Top = 336
  end
  object QrSemMez: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 192
    object QrSemMezData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSemMezTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrSemMezCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrSemMezAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrSemMezGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrSemMezDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
    object QrSemMezNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrSemMezDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSemMezCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSemMezCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSemMezDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrSemMezSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrSemMezVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSemMezLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSemMezFatID: TIntegerField
      FieldName = 'FatID'
      DisplayFormat = '000;-000; '
    end
    object QrSemMezFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrSemMezCONTA: TIntegerField
      FieldName = 'CONTA'
    end
    object QrSemMezNOMECONTA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONTA'
      FixedChar = True
      Size = 128
    end
    object QrSemMezNOMEEMPRESA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEEMPRESA'
      Size = 128
    end
    object QrSemMezNOMESUBGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMESUBGRUPO'
      Size = 128
    end
    object QrSemMezNOMEGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEGRUPO'
      Size = 128
    end
    object QrSemMezNOMECONJUNTO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONJUNTO'
      Size = 128
    end
    object QrSemMezNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrSemMezAno: TFloatField
      FieldName = 'Ano'
    end
    object QrSemMezMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrSemMezMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrSemMezBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrSemMezLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrSemMezFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrSemMezSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrSemMezCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrSemMezLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrSemMezPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSemMezSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSemMezID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrSemMezMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrSemMezFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrSemMezcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrSemMezMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrSemMezNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrSemMezNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrSemMezTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrSemMezNOMERELACIONADO: TWideStringField
      FieldName = 'NOMERELACIONADO'
      Size = 50
    end
    object QrSemMezOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrSemMezLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrSemMezMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrSemMezATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrSemMezJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSemMezDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrSemMezNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrSemMezVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrSemMezAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrSemMezMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrSemMezProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrSemMezDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSemMezDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSemMezUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrSemMezUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrSemMezControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrSemMezID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrSemMezCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrSemMezFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrSemMezICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSemMezICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSemMezDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrSemMezCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrSemMezCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrSemMezDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrSemMezDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrSemMezPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrSemMezForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrSemMezQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrSemMezEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrSemMezContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrSemMezCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrSemMezDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrSemMezDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrSemMezUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrSemMezNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrSemMezAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrSemMezExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrSemMezSerieCH: TWideStringField
      DisplayWidth = 10
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrSemMezSERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Calculated = True
    end
    object QrSemMezUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrSemMezDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrSemMezNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrSemMezMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSemMezMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSemMezCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrSemMezBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrSemMezAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrSemMezConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrSemMezTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrSemMezAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrSemMezReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrSemMezID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrSemMezAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrSemMezAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrSemMezFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrSemMezProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrSemMezPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrSemMezPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrSemMezAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsSemMez: TDataSource
    DataSet = QrSemMez
    Left = 272
  end
end
