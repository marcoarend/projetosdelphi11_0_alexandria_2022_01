unit Principal;

interface

uses
  Windows, Forms, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr,
  Dialogs, Registry, ExtCtrls, dmkGeral;

type
  TFmPrincipal = class(TService)
    Timer1: TTimer;
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceAfterUninstall(Sender: TService);
    procedure ServiceBeforeInstall(Sender: TService);
    procedure ServiceBeforeUninstall(Sender: TService);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceDestroy(Sender: TObject);
    procedure ServiceExecute(Sender: TService);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceShutdown(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    FCount: Integer;
    procedure doSaveLog(Msg: String);
    procedure ReativaTimer();
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
    procedure ReCaptionComponentesDeForm(Form: TForm);
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses ModCobranca;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  FmPrincipal.Controller(CtrlCode);
end;

procedure TFmPrincipal.doSaveLog(Msg: String);
const
  Log = 'C:\Dermatek\Cobranca\Log.log';
var
  loLista: TStringList;
begin
  try
    // cria uma lista de strings para armazenar o conte�do em log
    loLista := TStringList.Create;
    try
      // se o log ja existe carrega ele
      if FileExists(Log) then
        loLista.LoadFromFile(Log);
      // adiciona a nova string ao log
      loLista.Add(TimeToStr(Now()) + ': ' + Msg);
    except
      on e: exception do
        loLista.Add(TimeToStr(Now()) + ': Erro ' + E.Message);
    end;
  finally
    // atualiza o log
    loLista.SaveToFile(Log);
    // libera a lista
    loLista.Free;
  end;
end;

function TFmPrincipal.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TFmPrincipal.ReativaTimer();
var
  Intervalo: Integer;
begin
  Intervalo := Geral.ReadAppKey('Intervalo', 'ECobranca', ktInteger, 300000,
    HKEY_LOCAL_MACHINE);
  //
  Timer1.Enabled := False;
  Timer1.Interval := Intervalo;
  Timer1.Enabled := True;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

procedure TFmPrincipal.ServiceAfterInstall(Sender: TService);
var
  Descricao: TRegistry;
begin
  doSaveLog('ServiceAfterInstall');
  //
  try
    Descricao := TRegistry.Create(KEY_READ or KEY_WRITE);
    Descricao.RootKey := HKEY_LOCAL_MACHINE;

    if (Descricao.OpenKey('\SYSTEM\CurrentControlSet\Services\' + Name, False)) then
    begin
      Descricao.WriteString('Description', 'Cobran�a autom�tica de inadimplentes');
      Descricao.CloseKey;
    end;
  finally
    FreeAndNil(Descricao);
  end;
end;

procedure TFmPrincipal.ServiceAfterUninstall(Sender: TService);
begin
  doSaveLog('ServiceAfterUninstall');
end;

procedure TFmPrincipal.ServiceBeforeInstall(Sender: TService);
begin
  doSaveLog('ServiceBeforeInstall');
end;

procedure TFmPrincipal.ServiceBeforeUninstall(Sender: TService);
begin
  doSaveLog('ServiceBeforeUninstall');
  Timer1.Enabled := False;
end;

procedure TFmPrincipal.ServiceContinue(Sender: TService; var Continued: Boolean);
begin
  doSaveLog('ServiceContinue');
  ReativaTimer();
end;

procedure TFmPrincipal.ServiceCreate(Sender: TObject);
begin
  doSaveLog('ServiceCreate');
  ReativaTimer();
end;

procedure TFmPrincipal.ServiceDestroy(Sender: TObject);
begin
  doSaveLog('ServiceDestroy');
end;

procedure TFmPrincipal.ServiceExecute(Sender: TService);
begin
  doSaveLog('ServiceExecute');
  //
  while not self.Terminated do
    ServiceThread.ProcessRequests(True);
(*
procedure TMyServiceThread.Execute;
const
  SecBetweenRuns = 10;
var
  Count: Integer;
begin
  { Place thread code here }
  while not Terminated do  // loop around until we should stop
  begin
    Inc(Count);
    if Count >= SecBetweenRuns then
    begin
      Count := 0;

      { place your service code here }
      { this is where the action happens }
      SomeProcedureInAnotherUnit;

    end;
    Sleep(1000);
  end;
end;
*)
end;

procedure TFmPrincipal.ServicePause(Sender: TService; var Paused: Boolean);
begin
  doSaveLog('ServicePause');
  Timer1.Enabled := False;
end;

procedure TFmPrincipal.ServiceShutdown(Sender: TService);
begin
  doSaveLog('ServiceShutdown');
  Timer1.Enabled := False;
end;

procedure TFmPrincipal.ServiceStart(Sender: TService; var Started: Boolean);
begin
  doSaveLog('ServiceStart');
  ReativaTimer();
end;

procedure TFmPrincipal.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  doSaveLog('ServiceStop');
  Timer1.Enabled := False;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  FCount := FCount + 1;
  doSaveLog('Timer1Timer:' + Geral.FF0(FCount));
  //
  DmModCobranca.VerificaInadimplencia();
end;

end.
