unit Module;

interface

uses
  SysUtils, Classes, DB, mySQLDbTables, dmkGeral;

type
  TDmod = class(TDataModule)
    MyDB: TmySQLDatabase;
    QrUpd: TmySQLQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Dmod: TDmod;

implementation

uses UnInternalConsts;

{$R *.dfm}

procedure TDmod.DataModuleCreate(Sender: TObject);
begin
  MyDB.LoginPrompt := False;

  TMeuDB := 'syndic';
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  VAR_BDSENHA := 'wkljweryhvbirt';
  VAR_IP := '127.0.0.1';
  VAR_SQLUSER := 'root';
  //
  MyDB.UserName     := VAR_SQLUSER;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host         := VAR_IP;
  MyDB.Port         := VAR_PORTA;

  //


{
  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Geral.MensagemBox('Impossível criar Modulo de dados Geral', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
}
end;

end.
