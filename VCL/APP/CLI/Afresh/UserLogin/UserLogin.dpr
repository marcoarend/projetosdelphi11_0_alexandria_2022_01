program UserLogin;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  Windows,
  ShellApi,
  System.SysUtils;

function ConnectAs(const lpszUsername, lpszPassword: string): Boolean;
var
  hToken: THandle;
begin
  Result := LogonUser(PChar(lpszUsername), nil, PChar(lpszPassword), LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, hToken);
  if Result then
    Result := ImpersonateLoggedOnUser(hToken)
  else
    RaiseLastOSError;
end;

procedure Executa();
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    sei.lpVerb := 'runas'; // Run as admin
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := HWND_CmdShow; //SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      InstApp  := sei.hInstApp;
      //HProcesso := sei.hProcess;
    end;
  end;
var
  Executavel, Pasta, Parametros: String;
  _HWND: HWND;
begin
  //Executavel     := 'TesteOutroApp.exe';
  Executavel     := 'Project1.exe';
  Pasta          := ExtractFilePath('C:\_Compilers\ProjetosDelphi10_2_2_Tokyo\VCL\APP\CLI\Afresh\UserLogin\Win32\Debug\');
  //Pasta          := 'C:\Executaveis\19.0\EarTug\';
  Parametros     := '';
  // _HWND := nil;
  RunAsAdmin(0, Executavel, Pasta, Parametros, SW_SHOWNORMAL);
end;

////////////////////////////////////////////////////////////////////////////////

var
  Admin, Password: String;
begin
  Admin    := 'angeloni';
  Password := 'eletro26';
  try
   ConnectAs(Admin, Password);
   //do something here

   Executa();
      Writeln('Teste');


   //terminates the impersonation
   RevertToSelf;

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
  readln;
end.

