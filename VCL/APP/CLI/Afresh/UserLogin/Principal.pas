unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, ShellApi, Vcl.ExtCtrls;

type
  TForm2 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    function ConnectAs(const lpszUsername, lpszPassword: string): Boolean;
    procedure Executa();
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
var
  Admin, Password: String;
begin
  Admin    := 'angeloni';
  Password := 'eletro26';
  try
   ConnectAs(Admin, Password);
   //do something here

   Executa();
   Memo1.Lines.Add('Executado!');


   //terminates the impersonation
   RevertToSelf;

  except
    on E: Exception do
     Memo1.Lines.Add(E.ClassName + ': ' + E.Message);
  end;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  Timer1.Enabled := True;
end;

function TForm2.ConnectAs(const lpszUsername, lpszPassword: string): Boolean;
var
  hToken: THandle;
begin
  Result := LogonUser(PChar(lpszUsername), nil, PChar(lpszPassword), LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, hToken);
  if Result then
    Result := ImpersonateLoggedOnUser(hToken)
  else
    RaiseLastOSError;
end;

procedure TForm2.Executa;
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    //sei.lpVerb := 'runas'; // Run as admin
    sei.lpVerb := '';
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := HWND_CmdShow; //SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      InstApp  := sei.hInstApp;
      //HProcesso := sei.hProcess;
    end;
  end;
var
  Executavel, Pasta, Parametros: String;
  _HWND: HWND;
begin
{
  //////////////////////////////////////////////////////////////////////////////
  //Executavel     := 'TesteOutroApp.exe';
  Executavel     := 'Project1.exe';
  Pasta          := ExtractFilePath('C:\_Compilers\ProjetosDelphi10_2_2_Tokyo\VCL\APP\CLI\Afresh\UserLogin\Win32\Debug\');
  //Pasta          := 'C:\Executaveis\19.0\EarTug\';
  Parametros     := '';
  // _HWND := nil;
  //////////////////////////////////////////////////////////////////////////////
}
  //"C:\Users\angeloni\AppData\Local\Uniface\Uniface Anywhere Client\Client\ua-client.exe" -h www30.bhan.com.br -u NAYR -p 660286 -c -a "Virtual Age"
  //"C:\Program Files (x86)\Uniface\Uniface Anywhere Client\Client\ua-client.exe" -h www30.bhan.com.br -u NAYR -p 660286 -c -a "Virtual Age"
  Executavel     := 'ua-client.exe';
  Pasta          := 'C:\Program Files (x86)\Uniface\Uniface Anywhere Client\Client\';
  //Pasta          := 'C:\Users\angeloni\AppData\Local\Uniface\Uniface Anywhere Client\Client\';
  //Pasta          := 'C:\Executaveis\19.0\EarTug\';
  Parametros     := ' -h www30.bhan.com.br -u NAYR -p 660286 -c -a "Virtual Age"';
  RunAsAdmin(0, Executavel, Pasta, Parametros, SW_SHOWNORMAL);
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  Executa();
end;

end.
