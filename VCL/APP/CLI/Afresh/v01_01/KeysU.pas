unit KeysU;

interface

procedure PressKey(Key: Char);
procedure ReleaseKey(Key: Char);
procedure SendKeys(const Keys: String);

const
  SnapShotWholeScreen: Boolean = False;

implementation

uses
  WinTypes, WinProcs, Forms, SysUtils;

const
  KeyEventF_KeyDown = 0;
{$ifndef WIN32}
  KeyEventF_KeyUp = $80; {It changes to 2 in Win32}

procedure Keybd_Event; far; external 'USER' index 289;

procedure PostVirtualKeyEvent(vk: Word; fUp: Boolean);
var
  AXReg, BXReg: WordRec;
const
  ButtonUp: array[Boolean] of Byte = (KeyEventF_KeyDown, KeyEventF_KeyUp);
begin
  AXReg.Hi := ButtonUp[fUp];
  AXReg.Lo := vk;
  BXReg.Hi := 0; { not an extended scan code }
  { Special processing for the PrintScreen key. }
  { If scan code is set to 1 it copies entire }
  { screen. If set to 0 it copies active window. }
  if vk = vk_SnapShot then
    BXReg.Lo := Byte(SnapShotWholeScreen)
  else
    BXReg.Lo := MapVirtualKey(vk, 0);
  asm
    mov ax, AXReg
    mov bx, BXReg
    call Keybd_Event
  end;
end;
{$else}
procedure PostVirtualKeyEvent(vk: Word; fUp: Boolean);
var
  ScanCode: Byte;
const
  ButtonUp: array[Boolean] of Byte = (KeyEventF_KeyDown, KeyEventF_KeyUp);
begin
  if vk = vk_SnapShot then
    { Special processing for the PrintScreen key. }
    { If scan code is set to 1 it copies entire }
    { screen. If set to 0 it copies active window. }
    ScanCode := Byte(SnapShotWholeScreen)
  else
    ScanCode := MapVirtualKey(vk, 0);
  Keybd_Event(vk, ScanCode, ButtonUp[fUp], 0);
end;
{$endif}

procedure PressKey(Key: Char);
begin
  PostVirtualKeyEvent(Ord(Key), False)
end;

procedure ReleaseKey(Key: Char);
begin
  PostVirtualKeyEvent(Ord(Key), True)
end;

procedure SendKeys(const Keys: String);
var
  Loop: Byte;
begin
  for Loop := 1 to Length(Keys) do
  begin
    PostVirtualKeyEvent(Ord(Keys[Loop]), False); { Press key }
    PostVirtualKeyEvent(Ord(Keys[Loop]), True);  { Release key }
  end;
  { Let the keys be processed }
  Application.ProcessMessages;
end;


(*

////////////// Exemplo 2

// Captura tela para o clipboard
  SendKeys(Chr(vk_SnapShot));


////////////// Exemplo 1

var
  Wnd: HWnd;

  Wnd := FindWindow('TAppBuilder', nil);
  if Wnd = 0 then Exit;
  { If Delphi is minimised, this statement may have no visible effect }
  BringWindowToTop(Wnd);
  { Delphi 1 has four About box gang screens, Delphi 2 has three }
  { Delphi 3 has three and Delphi 4 has four. The fourth one }
  { (Delphi 1 only) only works on >=256 colour screen drivers }
  SendKeys(Chr(vk_Menu)+'HA'); { Invoke the About box: Help | About }
  PressKey(Char(vk_Menu)); { Hold down Alt key }
  case edtGangScreen.Value of
    1: SendKeys('DEVELOPERS');
    2: SendKeys('TEAM');
    3: SendKeys('VERSION');
    4: if ScreenHas256ColoursOrMore then
         SendKeys('AND')
       else
         MessageDlg(
           'Alt+AND (Delphi 1 only) requires at least a 256-colour driver',
           mtInformation, [mbOk], 0);
    5: SendKeys('QUALITY');
    6: SendKeys('CHUCK');
  end;
  ReleaseKey(Char(vk_Menu));  { Release Alt key }


/////////////////////

  { The intention here is to enter the string: }
  {   Copyright: � }
  { into the edit control. This requires some planning }
  { to get the mixed case, as well as the colon character }

  { Give focus to edit }
  Edit1.SetFocus;
  { Make sure Caps Lock is off }
  if Odd(GetKeyState(vk_Capital)) then
    SendKeys(Chr(vk_Capital));
  { Hold down Shift key, press C, then release Shift }
  PressKey(Chr(vk_Shift));
  SendKeys('C');
  ReleaseKey(Chr(vk_Shift));
  { Press more keys (which will be lower case) }
  SendKeys('OPYRIGHT');
  { Hold down Shift key, press ;, then release Shift }
  PressKey(Chr(vk_Shift));
  SendKeys(#$BA);
  ReleaseKey(Chr(vk_Shift));
  { Send a space character }
  SendKeys(Chr(vk_Space));
  { Do Alt+0169 on number pad }
  PressKey(Chr(vk_Menu));
  SendKeys(Chr(vk_Insert) + Chr(vk_End) + Chr(vk_Right) + Chr(vk_Prior));
  ReleaseKey(Chr(vk_Menu))

*)

end.
