unit UnAppMainPF;


interface

uses Winapi.Windows, Winapi.Messages, Vcl.Forms, Winapi.ShellApi, Winapi.ShlObj,
  System.SysUtils, Vcl.Controls,
  DCPcrypt2, DCPblockciphers, DCPtwofish, DCPsha1, System.JSON, System.IOUtils,
  System.Classes,
  //
  Data.DB, Datasnap.DBClient, // TClientDataSet
  UnGrl_Vars;

type
  TUnAppMainPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Tabela_Carrega(const DCP_twofish: TDCP_twofish;
              const TbPassos: TClientDataSet;
              const TbPassosTpAcao: TIntegerField;
              const TbPassosCodigo: TIntegerField;
              const TbPassosOrdem: TIntegerField;
              const TbPassosNome: TStringField;
              const TbPassosSegundosA: TFloatField;
              const TbPassosSegundosD: TFloatField;
              const TbPassosMousePosX: TIntegerField;
              const TbPassosTexto: TStringField;
              const TbPassosMousePosY: TIntegerField;
              const TbPassosMouseEvent: TIntegerField;
              const TbPassosQtdClkSmlt: TIntegerField;
              const TbPassosKeyCode: TIntegerField;
              const TbPassosKeyEvent: TIntegerField;
              const TbPassosShiftState: TIntegerField;
              const TbPassosTipoTecla: TIntegerField;
              const TbPassosTeclEsp: TIntegerField;
              const TbPassosLetra: TStringField;
              var Nome, Pasta, Executavel, Parametros: String;
              var MonitSohProprioProces, ParaEtapaOnDeactive, ExecAsAdmin,
                  LogInMemo, ExeOnIni: Boolean;
              var SegWaitOnOSReinit, SegWaitScanMonit: Integer;
              var EstadoJanela, MaiorCodigo: Integer);
    procedure CriaEcarregaTabela(
              const DCP_twofish: TDCP_twofish;
              const TbPassos: TClientDataSet;
              const TbPassosTpAcao: TIntegerField;
              const TbPassosCodigo: TIntegerField;
              const TbPassosOrdem: TIntegerField;
              const TbPassosNome: TStringField;
              const TbPassosSegundosA: TFloatField;
              const TbPassosSegundosD: TFloatField;
              const TbPassosMousePosX: TIntegerField;
              const TbPassosTexto: TStringField;
              const TbPassosMousePosY: TIntegerField;
              const TbPassosMouseEvent: TIntegerField;
              const TbPassosQtdClkSmlt: TIntegerField;
              const TbPassosKeyCode: TIntegerField;
              const TbPassosKeyEvent: TIntegerField;
              const TbPassosShiftState: TIntegerField;
              const TbPassosTipoTecla: TIntegerField;
              const TbPassosTeclEsp: TIntegerField;
              const TbPassosLetra: TStringField;
              var Nome, Pasta, Executavel, Parametros: String;
              var MonitSohProprioProces, ParaEtapaOnDeactive, ExecAsAdmin,
                  LogInMemo, ExeOnIni: Boolean;
              var SegWaitOnOSReinit, SegWaitScanMonit: Integer;
              var HWNDShow, MaiorCodigo: Integer);
    function  ExecutaPassosRobot(const Form: TForm;
              const ExecAsAdmin: Boolean;
              const TbPassos: TClientDataSet;
              const TbPassosTpAcao: TIntegerField;
              const TbPassosCodigo: TIntegerField;
              const TbPassosOrdem: TIntegerField;
              const TbPassosNome: TStringField;
              const TbPassosSegundosA: TFloatField;
              const TbPassosSegundosD: TFloatField;
              const TbPassosMousePosX: TIntegerField;
              const TbPassosTexto: TStringField;
              const TbPassosMousePosY: TIntegerField;
              const TbPassosMouseEvent: TIntegerField;
              const TbPassosQtdClkSmlt: TIntegerField;
              const TbPassosKeyCode: TIntegerField;
              const TbPassosKeyEvent: TIntegerField;
              const TbPassosShiftState: TIntegerField;
              const TbPassosTipoTecla: TIntegerField;
              const TbPassosTeclEsp: TIntegerField;
              const TbPassosLetra: TStringField;
              const Pasta, Executavel, Parametros: String;
              const EstadoJanela: Integer;
              const OrdemStop, CodigoStop: Integer;
              var HProcesso: Integer): Boolean;
  end;

var
  AppMainPF: TUnAppMainPF;


implementation

uses dmkGeral, UnLicVendaApp_Dmk, LicDados, UnDmkProcFunc, MyListas, UnMyJSON,
  UnAppPF, UnAppEnums, UnMyVclEvents, UnApp_Consts, Principal;

{ TUnAppMainPF }

const
  FChave = 'SohJesusSalva';

procedure TUnAppMainPF.CriaEcarregaTabela(
  const DCP_twofish: TDCP_twofish;
  const TbPassos: TClientDataSet;
  const TbPassosTpAcao: TIntegerField;
  const TbPassosCodigo: TIntegerField;
  const TbPassosOrdem: TIntegerField;
  const TbPassosNome: TStringField;
  const TbPassosSegundosA: TFloatField;
  const TbPassosSegundosD: TFloatField;
  const TbPassosMousePosX: TIntegerField;
  const TbPassosTexto: TStringField;
  const TbPassosMousePosY: TIntegerField;
  const TbPassosMouseEvent: TIntegerField;
  const TbPassosQtdClkSmlt: TIntegerField;
  const TbPassosKeyCode: TIntegerField;
  const TbPassosKeyEvent: TIntegerField;
  const TbPassosShiftState: TIntegerField;
  const TbPassosTipoTecla: TIntegerField;
  const TbPassosTeclEsp: TIntegerField;
  const TbPassosLetra: TStringField;
  var Nome, Pasta, Executavel, Parametros: String;
  var MonitSohProprioProces, ParaEtapaOnDeactive, ExecAsAdmin, LogInMemo,
      ExeOnIni: Boolean;
  var SegWaitOnOSReinit, SegWaitScanMonit: Integer;
  var HWNDShow, MaiorCodigo: Integer);
//var
  //Dir: String;
begin
  //Dir := CO_DIR_RAIZ_DMK + '\Afresh\Data\';
  ForceDirectories(CO_DIR_APP_DATA);
  //FDBCriptArqName := Dir + 'TarefaUnica.dmkafr';
  //////////////////////////////////////////////////////////////////////////////
  TbPassos.FieldDefs.Add('Codigo',         ftInteger,  0, True);
  TbPassos.FieldDefs.Add('Ordem',          ftInteger,  0, True);
  TbPassos.FieldDefs.Add('Nome',           ftString, 100, True);
  //TbPassos.FieldDefs.Add('Data', ftDate,    0, False);
  TbPassos.FieldDefs.Add('SegundosA',      ftFloat,    0, False);
  TbPassos.FieldDefs.Add('SegundosD',      ftFloat,    0, False);
  TbPassos.FieldDefs.Add('TpAcao',         ftInteger,  0, False);
  //
  TbPassos.FieldDefs.Add('MousePosX',      ftInteger,  0, False);
  TbPassos.FieldDefs.Add('MousePosY',      ftInteger,  0, False);
  TbPassos.FieldDefs.Add('MouseEvent',     ftInteger,  0, False);
  TbPassos.FieldDefs.Add('QtdClkSmlt',     ftInteger,  0, False);
  //
  TbPassos.FieldDefs.Add('KeyCode',        ftInteger,  0, False);
  TbPassos.FieldDefs.Add('KeyEvent',       ftInteger,  0, False);
  //
  TbPassos.FieldDefs.Add('Texto',          ftString, 255, False);
  //
  TbPassos.FieldDefs.Add('ShiftState',     ftInteger,  0, False);
  TbPassos.FieldDefs.Add('TipoTecla',      ftInteger,  0, False);
  TbPassos.FieldDefs.Add('TeclEsp',        ftInteger,  0, False);
  TbPassos.FieldDefs.Add('Letra',          ftString,   2, False);
  //
  //
  TbPassos.CreateDataSet;
  TbPassos.IndexFieldNames := 'Ordem;Codigo';
  //////////////////////////////////////////////////////////////////////////////
  Tabela_Carrega(DCP_twofish,
  TbPassos,
  TbPassosTpAcao,
  TbPassosCodigo,
  TbPassosOrdem,
  TbPassosNome,
  TbPassosSegundosA,
  TbPassosSegundosD,
  TbPassosMousePosX,
  TbPassosTexto,
  TbPassosMousePosY,
  TbPassosMouseEvent,
  TbPassosQtdClkSmlt,
  TbPassosKeyCode,
  TbPassosKeyEvent,
  TbPassosShiftState,
  TbPassosTipoTecla,
  TbPassosTeclEsp,
  TbPassosLetra,
  Nome, Pasta, Executavel, Parametros, MonitSohProprioProces,
  ParaEtapaOnDeactive, ExecAsAdmin, LogInMemo, ExeOnIni, SegWaitOnOSReinit,
  SegWaitScanMonit,
  HWNDShow, MaiorCodigo);
end;

function TUnAppMainPF.ExecutaPassosRobot(const Form: TForm;
  const ExecAsAdmin: Boolean;
  const TbPassos: TClientDataSet;
  const TbPassosTpAcao: TIntegerField;
  const TbPassosCodigo: TIntegerField;
  const TbPassosOrdem: TIntegerField;
  const TbPassosNome: TStringField;
  const TbPassosSegundosA: TFloatField;
  const TbPassosSegundosD: TFloatField;
  const TbPassosMousePosX: TIntegerField;
  const TbPassosTexto: TStringField;
  const TbPassosMousePosY: TIntegerField;
  const TbPassosMouseEvent: TIntegerField;
  const TbPassosQtdClkSmlt: TIntegerField;
  const TbPassosKeyCode: TIntegerField;
  const TbPassosKeyEvent: TIntegerField;
  const TbPassosShiftState: TIntegerField;
  const TbPassosTipoTecla: TIntegerField;
  const TbPassosTeclEsp: TIntegerField;
  const TbPassosLetra: TStringField;
  const Pasta, Executavel, Parametros: String;
  const EstadoJanela: Integer;
  const OrdemStop, CodigoStop: Integer;
  var HProcesso: Integer): Boolean;
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    sei.lpVerb := 'runas'; // Run as admin
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := HWND_CmdShow; //SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      InstApp  := sei.hInstApp;
      HProcesso := sei.hProcess;
    end;
  end;
var
  FileName, Folder: string;
  WaitUntilTerminated, WaitUntilIdle(*, RunMinimized*): Boolean;
  ErrorCode: Integer;
  OK: Boolean;
  //EstadoJanela: Integer;
  HWND_CmdShow: Integer;
  //
  Keys: array of Word;
  Shift: TShiftState;
  SpecialKey: Boolean;
  Retorno: Integer;
var
  Path: string;
  ExecuteResult, MouseClickEventId: Integer;
  Continua: Boolean;
begin
  Result := False;
  //FileName  := EdPasta.Text + EdExecutavel.Text;
  FileName  := Pasta + Executavel;
  //Params    := EdParametros.Text; // '-p'; // can be empty
  Folder    := ''; //EdPasta.Text; // if empty function will extract path from FileName
  //ErrorCode := ;
  //
  WaitUntilTerminated := False;
  WaitUntilIdle       := True;//True;
  HWND_CmdShow        := AppPF.EstadoJanelaToHWND_CmdShow(EstadoJanela);

  if FmPrincipal.FBtAtivado = False then Exit;

  if not ExecAsAdmin then
  begin
    //Funcionando local e Server! *)
    Retorno := ShellExecute(Application.Handle, 'open', PChar(Executavel),
      PChar(Parametros), PChar(Pasta), HWND_CmdShow);
    Continua := Retorno > 32;
  end else
  begin
    Continua :=  RunAsAdmin(Form.Handle, Executavel, Pasta, Parametros, HWND_CmdShow);
  end;
  //
  if Continua then
  begin
    //Exit; //////////////////////////////////////////////////////////////////////
    TbPassos.First;
    while not TbPassos.Eof do
    begin
      if FmPrincipal.FBtAtivado = False then
      begin
        TbPassos.Last;
        Exit;
      end;
      AppPF.EsperaEprocessaMensagensWindows(TbPassosSegundosA.Value);
      //
      case TTpAcaoInputEvent(TbPassosTpAcao.Value) of
        TTpAcaoInputEvent.taieMouse: // 1
        begin
          MouseClickEventId := TbPassosMouseEvent.Value;
          if TbPassosQtdClkSmlt.Value = 2 then
            MouseClickEventId := MouseClickEventId + CO_MaxSigleMouseClickSouce;
          MyVclEvents.SimulaMouseClickXY(
            TbPassosMousePosX.Value, TbPassosMousePosY.Value,
            TMyMouseClickSouce(MouseClickEventId));
        end;
        TTpAcaoInputEvent.taieKeysComand: // 2
        begin
          Shift := [];
          if Geral.IntInConjunto(1, TbPassosShiftState.Value) then
            Shift := Shift + [ssShift];
          if Geral.IntInConjunto(2, TbPassosShiftState.Value) then
            Shift := Shift + [ssAlt];
          if Geral.IntInConjunto(4, TbPassosShiftState.Value) then
            Shift := Shift + [ssCtrl];
          //
          case TbPassosTipoTecla.Value of
            // Normal
            1: Keys := [Ord(TbPassosLetra.Value[1])];
            // Especial
            2: Keys := [TbPassosTeclEsp.Value];
          end;
          SpecialKey := False;
          MyVclEvents.SimulaKeyBoardPress_0_PostKeyEx32Arr(Keys, Shift, SpecialKey);
        end;
        TTpAcaoInputEvent.taieTexto: // 3
        begin
          MyVclEvents.SimulaDigitacaoDeTexto(TbPassosTexto.Value);
          //MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(VK_TAB, [], False);
        end;
      end;
      //
      AppPF.EsperaEprocessaMensagensWindows(TbPassosSegundosD.Value);
      //
      if (OrdemStop <= TbPassosOrdem.Value) and
      (CodigoStop <= TbPassosCodigo.Value) then
        TbPassos.Last
      else
        TbPassos.Next;
    end;
  end;
  //
  Result := True;
end;


procedure TUnAppMainPF.Tabela_Carrega(const DCP_twofish: TDCP_twofish;
  const TbPassos: TClientDataSet;
  const TbPassosTpAcao: TIntegerField;
  const TbPassosCodigo: TIntegerField;
  const TbPassosOrdem: TIntegerField;
  const TbPassosNome: TStringField;
  const TbPassosSegundosA: TFloatField;
  const TbPassosSegundosD: TFloatField;
  const TbPassosMousePosX: TIntegerField;
  const TbPassosTexto: TStringField;
  const TbPassosMousePosY: TIntegerField;
  const TbPassosMouseEvent: TIntegerField;
  const TbPassosQtdClkSmlt: TIntegerField;
  const TbPassosKeyCode: TIntegerField;
  const TbPassosKeyEvent: TIntegerField;
  const TbPassosShiftState: TIntegerField;
  const TbPassosTipoTecla: TIntegerField;
  const TbPassosTeclEsp: TIntegerField;
  const TbPassosLetra: TStringField;
  var Nome, Pasta, Executavel, Parametros: String;
  var MonitSohProprioProces, ParaEtapaOnDeactive, ExecAsAdmin, LogInMemo,
      ExeOnIni: Boolean;
  var SegWaitOnOSReinit, SegWaitScanMonit: Integer;
  var EstadoJanela, MaiorCodigo: Integer);
  //
  function DeCryptJSON(Crypt: string; twofish: TDCP_twofish): string;
  begin
    twofish.InitStr(FChave, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
    Result := twofish.DecryptString(Crypt); // Descriptografa o JSON.
    twofish.Burn();
  end;
var
  jsonObj, jSubObj: TJSONObject;
  ja: TJSONArray;
  jv: TJSONValue;
  i: Integer;
  Texto: TStringList;
var
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
  //
  jsArray: TJSONArray;
  jsonObject: TJSONObject;
  Campo, Valor: String;
  N: Integer;
  //
  Item: TJSONObject;
  Txt, TxtCript: String;
begin
  //if not FileExists(FDBArqName) then Exit;
  if not FileExists(CO_DBCriptArqName) then Exit;
  Texto := TStringList.Create;
  try
    Txt := TFile.ReadAllText(CO_DBCriptArqName);
    Txt := DeCryptJSON(Txt, DCP_twofish);
    //
    jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Txt), 0) as TJSONObject;
    //jsonObj := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(Txt), 0) as TJSONObject;
    if jsonObj = nil then
      jsonObj := TJSONObject.ParseJSONValue(TEncoding.ANSI.GetBytes(Txt), 0) as TJSONObject;
      if jsonObj = nil then
        Exit;

    //jv := jsonObj.Get('Nome').JsonValue;
    Nome := MyJSON.JObjValStr(jsonObj, 'Nome');
    Pasta := MyJSON.JObjValStr(jsonObj, 'Pasta');
    Executavel := MyJSON.JObjValStr(jsonObj, 'Executavel');
    Parametros := MyJSON.JObjValStr(jsonObj, 'Parametros');
    EstadoJanela :=
      AppPF.HWND_CmdShowToEstadoJanela(MyJSON.JObjValInt(jsonObj, 'HWNDShow'));
    //
    MonitSohProprioProces := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'MonitSohProprioProces'));
    ParaEtapaOnDeactive   := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ParaEtapaOnDeactive'));
    ExecAsAdmin           := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ExecAsAdmin'));
    LogInMemo             := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'LogInMemo'));
    ExeOnIni              := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ExeOnIni'));
    SegWaitOnOSReinit     := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'SegWaitOnOSReinit'));
    SegWaitScanMonit      := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'SegWaitScanMonit'));

    jv := jsonObj.Get('TarefasIts').JsonValue;
    ja := jv as TJSONArray;


    TbPassos.DisableControls;
    for i := 0 to ja.Size - 1 do
    begin
      Item := ja.Get(i) as TJSONObject;

      TbPassos.Append;
      TbPassosCodigo.Value      := MyJSON.JObjValInt(Item, 'Codigo');
      TbPassosOrdem.Value       := MyJSON.JObjValInt(Item, 'Ordem');
      TbPassosNome.Value        := MyJSON.JObjValStr(Item, 'Nome');

      TbPassosSegundosA.Value   := MyJSON.JObjValFlu(Item, 'SegundosA');
      TbPassosSegundosD.Value   := MyJSON.JObjValFlu(Item, 'SegundosD');

      TbPassosTpAcao.Value      := MyJSON.JObjValInt(Item, 'TpAcao');
      TbPassosMousePosX.Value   := MyJSON.JObjValInt(Item, 'MousePosX');
      TbPassosMousePosY.Value   := MyJSON.JObjValInt(Item, 'MousePosY');
      TbPassosMouseEvent.Value  := MyJSON.JObjValInt(Item, 'MouseEvent');
      TbPassosQtdClkSmlt.Value  := MyJSON.JObjValInt(Item, 'QtdClkSmlt');

      TbPassosKeyCode.Value     := MyJSON.JObjValInt(Item, 'KeyCode');
      TbPassosKeyEvent.Value    := MyJSON.JObjValInt(Item, 'KeyEvent');

      TbPassosTexto.Value       := MyJSON.JObjValStr(Item, 'Texto');

      TbPassosShiftState.Value  := MyJSON.JObjValInt(Item, 'ShiftState');
      TbPassosTipoTecla.Value   := MyJSON.JObjValInt(Item, 'TipoTecla');
      TbPassosTeclEsp.Value     := MyJSON.JObjValInt(Item, 'TeclEsp');
      TbPassosLetra.Value       := MyJSON.JObjValStr(Item, 'Letra');

      TbPassos.Post;

      if TbPassosCodigo.Value > MaiorCodigo then
        MaiorCodigo := TbPassosCodigo.Value;
    end;
    TbPassos.EnableControls;

  finally
    Texto.Free;
  end;
end;

end.

