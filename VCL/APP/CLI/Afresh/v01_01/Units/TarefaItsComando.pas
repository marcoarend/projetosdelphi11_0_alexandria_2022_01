unit TarefaItsComando;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, dmkEdit,
  Vcl.Buttons, dmkRadioGroup, dmkCheckGroup;

type
  TFmTarefaItsComando = class(TForm)
    Panel1: TPanel;
    Memo1: TMemo;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    EdOrdem: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    EdSegundosA: TdmkEdit;
    EdSegundosD: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    CGShiftState: TdmkCheckGroup;
    RGTipoTecla: TdmkRadioGroup;
    CBTeclEsp: TComboBox;
    LaTeclEsp: TLabel;
    EdLetra: TdmkEdit;
    LaLetra: TLabel;
    Label6: TLabel;
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGTipoTeclaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmTarefaItsComando: TFmTarefaItsComando;

implementation

uses dmkGeral, UnAppEnums, UnAppPF, Principal, UnMyVclEvents, Tarefa;

{$R *.dfm}

procedure TFmTarefaItsComando.BtConfirmaClick(Sender: TObject);
var
  Codigo, Ordem, TpAcao, ShiftState, TipoTecla, TeclEsp: Integer;
  Nome, Letra: String;
  SegundosA, SegundosD: Double;
begin
  TpAcao     := Integer(TTpAcaoInputEvent.taieKeysComand); // 2
  Codigo     := EdCodigo.ValueVariant;
  Ordem      := EdOrdem.ValueVariant - 1;
  Nome       := EdNome.ValueVariant;
  SegundosA  := EdSegundosA.ValueVariant;
  SegundosD  := EdSegundosD.ValueVariant;
  ShiftState := CGShiftState.Value;
  TipoTecla  := RGTipoTecla.ItemIndex;
  TeclEsp    := MyVclEvents.ObtemCodigoDeTeclaEspecial(CBTeclEsp.Items[CBTeclEsp.ItemIndex]);
  Letra      := EdLetra.Text;
  //
  if Codigo = 0 then
  begin
    FmTarefa.FMaiorCodigo := FmTarefa.FMaiorCodigo + 1;
    Codigo := FmTarefa.FMaiorCodigo;
    FmTarefa.MemTable.Append;
  end else
  begin
    FmTarefa.MemTable.Edit;
  end;
  FmTarefa.MemTableCodigo.Value     := Codigo;
  FmTarefa.MemTableNome.Value       := Nome;
  FmTarefa.MemTableOrdem.Value      := Ordem;
  FmTarefa.MemTableTpAcao.Value     := TpAcao;

  FmTarefa.MemTableShiftState.Value := ShiftState;
  FmTarefa.MemTableTipoTecla.Value  := TipoTecla;
  FmTarefa.MemTableTeclEsp.Value    := TeclEsp;
  FmTarefa.MemTableLetra.Value      := Letra;

  FmTarefa.MemTableSegundosA.Value  := SegundosA;
  FmTarefa.MemTableSegundosD.Value  := SegundosD;
  //
  FmTarefa.MemTable.Post;
  //
  FmTarefa.Reordena();
  FmTarefa.Salva();
  //
  Close;
end;

procedure TFmTarefaItsComando.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTarefaItsComando.FormCreate(Sender: TObject);
begin
  CGShiftState.Value := 0;
end;

procedure TFmTarefaItsComando.RGTipoTeclaClick(Sender: TObject);
begin
  LaTeclEsp.Enabled := RGTipoTecla.ItemIndex = 2;
  CBTeclEsp.Enabled := RGTipoTecla.ItemIndex = 2;
  LaLetra.Enabled   := RGTipoTecla.ItemIndex = 1;
  EdLetra.Enabled   := RGTipoTecla.ItemIndex = 1;
end;

(*
  VK_CANCEL = vkCancel; {3}
  VK_BACK = vkBack; {8}
  VK_TAB = vkTab; {9}
  VK_CLEAR = vkClear; {12}
  VK_RETURN = vkReturn; {13}
  VK_SHIFT = vkShift; { $10, 16}
  VK_CONTROL = vkControl; {17}
  VK_MENU = vkMenu; {18}
  VK_PAUSE = vkPause; {19}
  VK_CAPITAL = vkCapital; {20}
  VK_ESCAPE = vkEscape; {27}
  VK_SPACE = vkSpace; { $20}
  VK_PRIOR = vkPrior; {33}
  VK_NEXT = vkNext; {34}
  VK_END = vkEnd; {35}
  VK_HOME = vkHome; {35}
  VK_LEFT = vkLeft; {37}
  VK_UP = vkUp; {38}
  VK_RIGHT = vkRight; {39}
  VK_DOWN = vkDown; {40}
  VK_SELECT = vkSelect; {41}
  VK_PRINT = vkPrint; {42}
  VK_EXECUTE = vkExecute; {43}
  VK_SNAPSHOT = vkSnapShot; {44}
  VK_INSERT = vkInsert; {45}
  VK_DELETE = vkDelete; {46}
  VK_HELP = vkHelp; {47}
  VK_LWIN = vkLWin; {91}
  VK_RWIN = vkRWin; {92}
  VK_APPS = vkApps; {93}
  VK_SLEEP = vkSleep; {95}
  VK_NUMPAD0 = vkNumpad0; {96}
  VK_NUMPAD1 = vkNumpad1; {97}
  VK_NUMPAD2 = vkNumpad2; {98}
  VK_NUMPAD3 = vkNumpad3; {99}
  VK_NUMPAD4 = vkNumpad4; {100}
  VK_NUMPAD5 = vkNumpad5; {101}
  VK_NUMPAD6 = vkNumpad6; {102}
  VK_NUMPAD7 = vkNumpad7; {103}
  VK_NUMPAD8 = vkNumpad8; {104}
  VK_NUMPAD9 = vkNumpad9; {105}
  VK_MULTIPLY = vkMultiply; {106}
  VK_ADD = vkAdd; {107}
  VK_SEPARATOR = vkSeparator; {108}
  VK_SUBTRACT = vkSubtract; {109}
  VK_DECIMAL = vkDecimal; {110}
  VK_DIVIDE = vkDivide; {111}
  VK_F1 = vkF1; {112}
  VK_F2 = vkF2; {113}
  VK_F3 = vkF3; {114}
  VK_F4 = vkF4; {115}
  VK_F5 = vkF5; {116}
  VK_F6 = vkF6; {117}
  VK_F7 = vkF7; {118}
  VK_F8 = vkF8; {119}
  VK_F9 = vkF9; {120}
  VK_F10 = vkF10; {121}
  VK_F11 = vkF11; {122}
  VK_F12 = vkF12; {123}
  VK_F13 = vkF13; {124}
  VK_F14 = vkF14; {125}
  VK_F15 = vkF15; {126}
  VK_F16 = vkF16; {127}
  VK_F17 = vkF17; {128}
  VK_F18 = vkF18; {129}
  VK_F19 = vkF19; {130}
  VK_F20 = vkF20; {131}
  VK_F21 = vkF21; {132}
  VK_F22 = vkF22; {133}
  VK_F23 = vkF23; {134}
  VK_F24 = vkF24; {135}
  VK_NUMLOCK = vkNumLock; {144}
  VK_SCROLL = vkScroll; {145}
  VK_LSHIFT = vkLShift; {160}
  VK_RSHIFT = vkRShift; {161}
  VK_LCONTROL = vkLControl; {162}
  VK_RCONTROL = vkRControl; {163}
  VK_LMENU = vkLMenu; {163}
  VK_RMENU = vkRMenu; {165}
  VK_BROWSER_BACK = 166;
  VK_BROWSER_FORWARD = 167;
  VK_BROWSER_REFRESH = 168;
  VK_BROWSER_STOP = 169;
  VK_BROWSER_SEARCH = 170;
  VK_BROWSER_FAVORITES = 171;
  VK_BROWSER_HOME = 172;
  VK_VOLUME_MUTE = 173;
  VK_VOLUME_DOWN = 174;
  VK_VOLUME_UP = 175;
  VK_MEDIA_NEXT_TRACK = 176;
  VK_MEDIA_PREV_TRACK = 177;
  VK_MEDIA_STOP = 178;
  VK_MEDIA_PLAY_PAUSE = 179;
  VK_LAUNCH_MAIL = 180;
  VK_LAUNCH_MEDIA_SELECT = 181;
  VK_LAUNCH_APP1 = 182;
  VK_LAUNCH_APP2 = 183;
  VK_PLAY = vkPlay; {250}
  VK_ZOOM = vkZoom; {251}
*)

end.
