object FmOpcoesApp: TFmOpcoesApp
  Left = 0
  Top = 0
  Caption = 'Dados do Licenciado'
  ClientHeight = 190
  ClientWidth = 315
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 315
    Height = 133
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 24
      Width = 58
      Height = 13
      Caption = 'CPF / CNPJ:'
    end
    object Label2: TLabel
      Left = 148
      Top = 24
      Width = 40
      Height = 13
      Caption = 'Posi'#231#227'o:'
    end
    object Label3: TLabel
      Left = 192
      Top = 24
      Width = 57
      Height = 13
      Caption = 'C'#243'digo PIN:'
    end
    object Label4: TLabel
      Left = 24
      Top = 64
      Width = 174
      Height = 13
      Caption = 'Minha senha para entrar neste App:'
    end
    object EdCPF_CNPJ: TdmkEdit
      Left = 24
      Top = 40
      Width = 120
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtCPFJ
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPosicao: TdmkEdit
      Left = 148
      Top = 40
      Width = 41
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCodigoPIN: TdmkEdit
      Left = 192
      Top = 40
      Width = 97
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdAppUserPwd: TdmkEdit
      Left = 24
      Top = 80
      Width = 265
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CkExigeSenhaLoginApp: TdmkCheckBox
      Left = 24
      Top = 108
      Width = 269
      Height = 17
      Caption = 'Exigir senha (se cadastrada) para logar no Afresh. '
      TabOrder = 4
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 133
    Width = 315
    Height = 57
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Left = 8
      Top = 8
      Width = 120
      Height = 40
      Caption = 'OK'
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtDesiste: TBitBtn
      Left = 184
      Top = 8
      Width = 120
      Height = 40
      Caption = 'Desiste'
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
end
