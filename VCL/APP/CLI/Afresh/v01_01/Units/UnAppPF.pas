unit UnAppPF;

interface

uses Winapi.Windows, Winapi.Messages, Vcl.Forms, Winapi.ShellApi, Winapi.ShlObj,
  System.SysUtils, Vcl.Controls, UnInternalConsts,
  DCPcrypt2, DCPblockciphers, DCPtwofish, DCPsha1, System.JSON, System.IOUtils,
  System.Classes, UnGrl_Vars, UnApp_Vars, Vcl.StdCtrls;
type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  LiberaAppPelaSenha(AppUserPwd: String): Boolean;
    function  ObtemDadosOpcoesAppDeArquivo(const Arquivo: String; const DCP_twofish:
              TDCP_twofish): Boolean;
    function  DeCryptJSON(Crypt: string; twofish: TDCP_twofish): string;
    function  EnCryptJSON(JSON: string; twofish: TDCP_twofish): string;
    //
    function  MouseBotaoEventToWindowsMessage(MouseBotaoEvent: Integer): Integer;
    function  WindowsMessageToMouseBotaoEvent(WindowsMessage: Integer): Integer;

    function  GetSpecialFolderPath(CSIDLFolder: Integer): string;

    function  ObtemNomeArquivoOpcoes(): String;
    function  ObtemNomeArquivoTarefa(): String;

    procedure MostraFormOpcoes();
    procedure MostraFormAbout();

    function UpTime(): string;

  end;

var
  AppPF: TUnAppPF;


implementation

uses dmkGeral, UnLicVendaApp_Dmk, UnDmkProcFunc, MyListas, UnMyJSON,
  LicDados, Dmk_Afresh, OpcoesApp, UnMyVclEvents, About;

{ TUnAppPF }

const
  CO_CHAVE = 'SohJesusSalva';

function TUnAppPF.DeCryptJSON(Crypt: string; twofish: TDCP_twofish): string;
begin
  twofish.InitStr(CO_CHAVE, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.DecryptString(Crypt); // Descriptografa o JSON.
  twofish.Burn();
end;

function TUnAppPF.EnCryptJSON(JSON: string; twofish: TDCP_twofish): string;
begin
  twofish.InitStr(CO_CHAVE, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.EncryptString(JSON); // Criptografa o JSON.
  twofish.Burn();
end;

function TUnAppPF.GetSpecialFolderPath(CSIDLFolder: Integer): string;
var
  FilePath: array [0..MAX_PATH] of char;
begin
  SHGetFolderPath(0, CSIDLFolder, 0, 0, FilePath);
  Result := FilePath;
end;

function TUnAppPF.LiberaAppPelaSenha(AppUserPwd: String): Boolean;
begin
  Application.CreateForm(TFmDmk_Afresh, FmDmk_Afresh);
  FmDmk_Afresh.ShowModal;
  Result := FmDmk_Afresh.FResult;
  FmDmk_Afresh.Destroy;
end;

procedure TUnAppPF.MostraFormAbout();
begin
  Application.CreateForm(TFmAbout, FmAbout);
(*
  FmAbout.ProductName := Application.Title;
  FmAbout.Version.Caption := 'Vers�o: ' + VersaoTxt(CO_VERSAO);
object ProductName: TLabel
*)
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TUnAppPF.MostraFormOPcoes();
var
  Continua: Boolean;
begin
  if VAR_SENHA_BOSS <> EmptyStr then
    Continua := AppPF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
  else
    Continua := True;
  if Continua then
  begin
    Application.CreateForm(TFmOpcoesApp, FmOpcoesApp);
    FmOpcoesApp.EdCPF_CNPJ.ValueVariant      := VAR_CPF_CNPJ;
    FmOpcoesApp.EdPosicao.ValueVariant       := VAR_Posicao;
    FmOpcoesApp.EdCodigoPIN.ValueVariant     := VAR_CodigoPIN;
    FmOpcoesApp.EdAppUserPwd.ValueVariant    := VAR_SENHA_BOSS;
    FmOpcoesApp.CkExigeSenhaLoginApp.Checked := VAR_ExigeSenhaLoginApp;
    FmOpcoesApp.ShowModal;
    FmOpcoesApp.Destroy;
  end;
end;

function TUnAppPF.MouseBotaoEventToWindowsMessage(
  MouseBotaoEvent: Integer): Integer;
const
  sProcFunc = 'TUnAppPF.MouseBotaoEventToWindowsMessage()';
begin
  case MouseBotaoEvent of
    //Indefinido
    //Esquerdo
    1: Result := wm_lbuttondown;
    //Direto
    2: Result := wm_rbuttondown;
    //Centro
    3: Result := wm_mbuttondown;
    //
    else
    begin
      Result := -1;
      Geral.MB_Erro('Evento de mouse n�o implementado!' + sLineBreak + sProcFunc);
    end;
  end;
end;

function TUnAppPF.ObtemDadosOpcoesAppDeArquivo(const Arquivo: String;
  const DCP_twofish: TDCP_twofish): Boolean;
var
  Texto: TStringList;
  Txt: String;
  jsonObj: TJSONObject;
begin
  Result := False;
  //if not FileExists(FDBArqName) then Exit;
  if not FileExists(Arquivo) then
  begin
    AppPF.MostraFormOpcoes();
    Halt(0);
  end else
  begin
    Texto := TStringList.Create;
    try
      Txt := TFile.ReadAllText(Arquivo);
      Txt := DeCryptJSON(Txt, DCP_twofish);
      //
      jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Txt), 0) as TJSONObject;
      if jsonObj = nil then
        jsonObj := TJSONObject.ParseJSONValue(TEncoding.ANSI.GetBytes(Txt), 0) as TJSONObject;
        if jsonObj = nil then
          Exit;
      //
      VAR_CPF_CNPJ           := MyJSON.JObjValStr(jsonObj, 'CPF_CNPJ');
      VAR_SENHA_BOSS         := MyJSON.JObjValStr(jsonObj, 'SENHA_BOSS');
      VAR_Posicao            := MyJSON.JObjValInt(jsonObj, 'Posicao');
      VAR_CodigoPIN          := MyJSON.JObjValStr(jsonObj, 'CodigoPIN');
      VAR_ExigeSenhaLoginApp := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ExigeSenhaLoginApp'));
      //
      Result := True;
    finally
      Texto.Free;
    end;
  end;
end;

function TUnAppPF.ObtemNomeArquivoOpcoes(): String;
var
  Pasta: String;
begin
  Result := '';
  Pasta    := AppPF.GetSpecialFolderPath(CSIDL_COMMON_DOCUMENTS);
  Pasta    := IncludeTrailingPathDelimiter(Pasta) + 'Dermatek\Afresh\';
  //
  Result := Pasta + 'Afresh.dmkopt';
end;

function TUnAppPF.ObtemNomeArquivoTarefa(): String;
var
  Pasta: String;
begin
  Result := '';
  Pasta := CO_DIR_RAIZ_DMK + '\Afresh\Data\';
  ForceDirectories(Pasta);
  //FDBArqName := Pasta + 'TarefaUnica.json';
  Result := Pasta + 'TarefaUnica.dmkafr';
end;

function TUnAppPF.UpTime(): string;
const
  ticksperday: integer = 1000 * 60 * 60 * 24;
  ticksperhour: integer = 1000 * 60 * 60;
  ticksperminute: integer = 1000 * 60;
  tickspersecond: integer = 1000;

var
  t: longword;
  d, h, m, s: integer;

begin
  t := GetTickCount;

  d := t div ticksperday;
  dec(t, d * ticksperday);

  h := t div ticksperhour;
  dec(t, h * ticksperhour);

  m := t div ticksperminute;
  dec(t, m * ticksperminute);

  s := t div tickspersecond;

  Result := IntToStr(d) + ' dias ' + IntToStr(h) + ' horas ' + IntToStr(m)
    + ' minutos ' + IntToStr(s) + ' segundos';
end;

function TUnAppPF.WindowsMessageToMouseBotaoEvent(
  WindowsMessage: Integer): Integer;
const
  sProcFunc = 'TUnAppPF.WindowsMessageToMouseBotaoEvent()';
begin
  case WindowsMessage of
    //Indefinido
    //Esquerdo
    wm_lbuttondown: Result := 1;
    //Direto
    wm_rbuttondown: Result := 2;
    //Centro
    wm_mbuttondown: Result := 3;
    //
    else
    begin
      Result := -1;
      Geral.MB_Erro('Mensagem do windows n�o implementada!' + sLineBreak + sProcFunc);
    end;
  end;
end;

end.
