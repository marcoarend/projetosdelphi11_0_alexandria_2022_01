unit DadosApp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, dmkEdit,
  Vcl.Grids, Vcl.Buttons, Data.DB, Vcl.DBGrids, Datasnap.DBClient,
  System.JSON, System.IOUtils, Vcl.Menus;
(*
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.Controls.Presentation, FMX.StdCtrls,
  System.JSON, FMX.ScrollBox, FMX.Memo, UnGrl_Vars;
*)

type
  TFmDadosApp = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdNome: TdmkEdit;
    Panel2: TPanel;
    Label2: TLabel;
    EdLinCom: TdmkEdit;
    Label3: TLabel;
    EdProcesso: TdmkEdit;
    Panel3: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    BtItens: TBitBtn;
    PMItens: TPopupMenu;
    IncluiClickDeMouse1: TMenuItem;
    IncluiDigitaoDeTexto1: TMenuItem;
    IncluiComandoTabEnterEtc1: TMenuItem;
    MemTable: TClientDataSet;
    MemTableCodigo: TIntegerField;
    MemTableNome: TStringField;
    MemTableOrdem: TIntegerField;
    MemTableTpAcao: TIntegerField;
    MemTableMousePosX: TIntegerField;
    MemTableMousePosY: TIntegerField;
    MemTableMouseEvent: TIntegerField;
    MemTableKeyCode: TIntegerField;
    MemTableKeyEvent: TIntegerField;
    MemTableTexto: TStringField;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure IncluiClickDeMouse1Click(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure Tabela_Carrega();
  public
    { Public declarations }
    FDBArqName: String;
    FMaiorCodigo: Integer;
    //
  end;

var
  FmDadosApp: TFmDadosApp;

implementation

uses
  UnInternalConsts, UnMyJson, UnMyObjects, dmkGeral, TarefaItsMouse;

{$R *.dfm}

procedure TFmDadosApp.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmDadosApp.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmDadosApp.BtOKClick(Sender: TObject);
{
var
  i: Integer;
  JSONTarefas: TJSONArray;
  TarefaCab, TarefaIts: TJSONObject;
begin
  JSONTarefas := TJSONArray.Create;
  try
    for i := Low(Cars) to High(Cars) do
    for i := Low(Cars) to High(Cars) do
    begin
      Car := TJSONObject.Create;
      JSONTarefas.AddElement(Car);
      Car.AddPair('manufacturer',
          Cars[i][TCarInfo.Manufacturer]);
      Car.AddPair('name', Cars[i][TCarInfo.Name]);
      Price := TJSONObject.Create;
      Car.AddPair('price', Price);
      Price.AddPair('value',
         TJSONNumber.Create(
               Cars[i][TCarInfo.Price].ToInteger));
      Price.AddPair('currency',
         Cars[i][TCarInfo.Currency]);
    end;
    JSON := JSONTarefas.ToString;
    Memo1.Text := JSON;
  finally
    JSONTarefas.Free;
  end;
end;
}
var
  i: Integer;
  lJsonObj: TJSONObject;
  JSONColor: TJSONObject;
  JSONArray : TJSONArray;
  Nome, LinCom, Processo: String;
begin
  lJsonObj := TJSONObject.Create;
  Nome     := EdNome.Text;
  LinCom   := EdLinCom.Text;
  Processo := EdProcesso.Text;
  //for i := 0 to StrGrdCsv.ColCount do
  //begin
    lJsonObj.AddPair('Nome', Nome);
    lJsonObj.AddPair('LinCom', LinCom);
    lJsonObj.AddPair('Processo', EdProcesso.Text);
    lJsonObj.AddPair('Itens', Geral.FF0(MemTable.RecordCount));
  //end;
  //
  JSONArray := TJSONArray.Create();
  MemTable.First;
  while not MemTable.Eof do
  begin
    JSONColor := TJSONObject.Create();
    //
    JSONColor.AddPair('Codigo',      Geral.FF0(MemTableCodigo.Value));
    JSONColor.AddPair('Nome',        MemTableNome.Value);
    JSONColor.AddPair('Ordem',       Geral.FF0(MemTableOrdem.Value));
    JSONColor.AddPair('TpAcao',      Geral.FF0(MemTableTpAcao.Value));
    JSONColor.AddPair('MousePosX',   Geral.FF0(MemTableMousePosX.Value));
    JSONColor.AddPair('MousePosY',   Geral.FF0(MemTableMousePosY.Value));
    JSONColor.AddPair('MouseEvent',  Geral.FF0(MemTableMouseEvent.Value));
    JSONColor.AddPair('KeyCode',     Geral.FF0(MemTableKeyCode.Value));
    JSONColor.AddPair('KeyEvent',    Geral.FF0(MemTableKeyEvent.Value));
    JSONColor.AddPair('Texto',       MemTableTexto.Value);
    JSONArray.Add(JSONColor);
    //
    MemTable.Next;
  end;
  lJsonObj.AddPair('TarefasIts', JSONArray);

(*
procedure TJSONValueDemoResource1.Get(const AContext: TendpointContext;
	const ARequest: TEndpointRequest; const AResponse: TEndpointResponse);
var
  JSONColor : TJSONObject;
  JSONArray : TJSONArray;
  JSONObject : TJSONObject;
  JSONValue : TJSONValue;
begin
  // create some JSON objects
  JSONColor := TJSONObject.Create();
  JSONColor.AddPair('name', 'red');
  JSONColor.AddPair('hex', '#ff0000');
  JSONColor.AddPair('name', 'blue');
  JSONColor.AddPair('hex', '#0000FF');
  JSONArray := TJSONArray.Create();
  JSONArray.Add(JSONColor);
  JSONObject := TJSONObject.Create();
  JSONObject.AddPair('colors', JSONArray);
  JSONValue := TJSONObject.Create();
  JSONValue := TJSONObject.ParseJSONValue(JSONObject.ToJSON);
  AResponse.building.SetValue(
    TJSONString.Create(
      JSONColor.ToJSON
      + ','
      + JSONObject.ToJSON
      + ',{"name":"' + JSONValue.GetValue<string>('colors[0].name')
      + '","hex":"'+ JSONValue.GetValue<string>('colors[0].hex')
      + '"}'
  ), True);
end;
*)



  TFile.WriteAllText(FDBArqName, lJsonObj.ToString, TEncoding.ANSI);
  //btnsave.Caption := 'Load Data';
  Close;
end;

procedure TFmDadosApp.Button1Click(Sender: TObject);
var
  i: word;
begin
  MemTable.DisableControls;
  for i := 1 to 20 do
  begin
    MemTable.Append;
    MemTable.FieldByName('Codigo').AsInteger       := i;
    MemTable.FieldByName('Ordem').AsInteger        := i;
    MemTable.FieldByName('Nome').AsString    := 'Codigo_' + IntToStr(i);
    //MemTable.FieldByName('Created').AsDateTime := Date();
    //MemTable.FieldByName('Volume').AsFloat     := Random(10000);
    MemTable.Post;
  end;
  MemTable.EnableControls;
end;


procedure TFmDadosApp.Button2Click(Sender: TObject);
begin
  MemTable.IndexFieldNames := 'Nome';
end;

procedure TFmDadosApp.FormCreate(Sender: TObject);
var
  Dir: String;
begin
  FMaiorCodigo := 0;
  Dir := CO_DIR_RAIZ_DMK + '\Afresh\Data\';
  ForceDirectories(Dir);
  FDBArqName := Dir + 'TarefaUnica.json';

  //////////////////////////////////////////////////////////////////////////////
  MemTable.FieldDefs.Add('Codigo',         ftInteger,  0, True);
  MemTable.FieldDefs.Add('Ordem',          ftInteger,  0, True);
  MemTable.FieldDefs.Add('Nome',           ftString, 100, True);
  //MemTable.FieldDefs.Add('Data', ftDate,    0, False);
  //MemTable.FieldDefs.Add('Volume',  ftFloat,   0, False);
  MemTable.FieldDefs.Add('TpAcao',         ftInteger,  0, False);
  //
  MemTable.FieldDefs.Add('MousePosX',      ftInteger,  0, False);
  MemTable.FieldDefs.Add('MousePosY',      ftInteger,  0, False);
  MemTable.FieldDefs.Add('MouseEvent',     ftInteger,  0, False);
  //
  MemTable.FieldDefs.Add('KeyCode',        ftInteger,  0, False);
  MemTable.FieldDefs.Add('KeyEvent',       ftInteger,  0, False);
  //
  MemTable.FieldDefs.Add('Texto',          ftString, 255, False);
  //
  MemTable.CreateDataSet;
  //////////////////////////////////////////////////////////////////////////////
  Tabela_Carrega();
end;

procedure TFmDadosApp.IncluiClickDeMouse1Click(Sender: TObject);
begin
  Application.CreateForm(TFmTarefaItsMouse, FmTarefaItsMouse);
  FmTarefaItsMouse.EdOrdem.ValueVariant := MemTableCodigo.Value + 1;
  FmTarefaItsMouse.ShowModal;
  FmTarefaItsMouse.Destroy;
end;

procedure TFmDadosApp.Tabela_Carrega();
var
  jsonObj, jSubObj: TJSONObject;
  ja: TJSONArray;
  jv: TJSONValue;
  i: Integer;
  Texto: TStringList;
var
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
  //
  jsArray: TJSONArray;
  jsonObject: TJSONObject;
  Campo, Valor: String;
  N: Integer;
  //
  Item: TJSONObject;
  Codigo, Nome: String;
  Txt: String;
begin
  if not FileExists(FDBArqName) then Exit;
  Texto := TStringList.Create;
  try
    //Texto.LoadFromFile(FDBArqName);
    Txt := TFile.ReadAllText(FDBArqName);

    //jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Texto.Text), 0) as TJSONObject;
    //jsonObj := TJSONObject.ParseJSONValue(TEncoding.ANSI.GetBytes(Texto.Text), 0) as TJSONObject;
    jsonObj := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(Txt), 0) as TJSONObject;
    //jv := jsonObj.Get('Nome').JsonValue;
    EdNome.ValueVariant := MyJSON.JObjValStr(jsonObj, 'Nome');
    EdLinCom.ValueVariant := MyJSON.JObjValStr(jsonObj, 'LinCom');
    EdProcesso.ValueVariant := MyJSON.JObjValStr(jsonObj, 'Processo');
    //
    jv := jsonObj.Get('TarefasIts').JsonValue;
    ja := jv as TJSONArray;


    MemTable.DisableControls;
    for i := 0 to ja.Size - 1 do
    begin
      Item := ja.Get(i) as TJSONObject;

      MemTable.Append;
      MemTableCodigo.Value      := MyJSON.JObjValInt(Item, 'Codigo');
      MemTableOrdem.Value       := MyJSON.JObjValInt(Item, 'Ordem');
      MemTableNome.Value        := MyJSON.JObjValStr(Item, 'Nome');

      MemTableTpAcao.Value      := MyJSON.JObjValInt(Item, 'TpAcao');
      MemTableMousePosX.Value   := MyJSON.JObjValInt(Item, 'MousePosX');
      MemTableMousePosY.Value   := MyJSON.JObjValInt(Item, 'MousePosY');
      MemTableMouseEvent.Value  := MyJSON.JObjValInt(Item, 'MouseEvent');

      MemTableKeyCode.Value     := MyJSON.JObjValInt(Item, 'KeyCode');
      MemTableKeyEvent.Value    := MyJSON.JObjValInt(Item, 'KeyEvent');

      MemTableTexto.Value       := MyJSON.JObjValStr(Item, 'Texto');

      MemTable.Post;

      if MemTableCodigo.Value > FMaiorCodigo then
        FMaiorCodigo := MemTableCodigo.Value;
    end;
    MemTable.EnableControls;

  finally
    Texto.Free;
  end;
end;

end.
