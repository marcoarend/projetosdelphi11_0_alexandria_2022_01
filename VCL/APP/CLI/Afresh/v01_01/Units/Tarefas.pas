unit Tarefas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, mySQLDbTables, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums;

type
  TFmTarefas = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCadastro_SimplesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCadastro_SimplesBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmTarefas: TFmTarefas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTarefas.LocCod(Atual, Codigo: Integer);
begin
(*&¨%
  DefParams;
  GOTOy.LC(Atual, Codigo);
*)
end;

procedure TFmTarefas.Va(Para: TVaiPara);
begin
(*&¨%$
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCadastro_SimplesCodigo.Value, LaRegistro.Caption[2]);
*)
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTarefas.DefParams;
begin
(*&¨%$
  VAR_GOTOTABELA := 'cadastro_simples';
  VAR_GOTOMYSQLTABLE := QrCadastro_Simples;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cadastro_simples');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
*)
end;

procedure TFmTarefas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTarefas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTarefas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmTarefas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTarefas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTarefas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTarefas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTarefas.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTarefas.BtAlteraClick(Sender: TObject);
begin
(*&¨%$
  if (QrCadastro_Simples.State <> dsInactive) and (QrCadastro_Simples.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCadastro_Simples, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'cadastro_simples');
  end;
*)
end;

procedure TFmTarefas.BtSaidaClick(Sender: TObject);
begin
(*&¨%$
  VAR_CADASTRO := QrCadastro_SimplesCodigo.Value;
  Close;
*)
end;

procedure TFmTarefas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
(*&¨%$
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cadastro_simples', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
*)
end;

procedure TFmTarefas.BtIncluiClick(Sender: TObject);
begin
(*&¨%$#
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCadastro_Simples, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'cadastro_simples');
*)
end;

procedure TFmTarefas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmTarefas.SbNumeroClick(Sender: TObject);
begin
(*&¨%$
  LaRegistro.Caption := GOTOy.Codigo(QrCadastro_SimplesCodigo.Value, LaRegistro.Caption);
*)
end;

procedure TFmTarefas.SbNomeClick(Sender: TObject);
begin
(*&¨%$
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
*)
end;

procedure TFmTarefas.SbNovoClick(Sender: TObject);
begin
(*&¨%
  LaRegistro.Caption := GOTOy.CodUsu(QrCadastro_SimplesCodigo.Value, LaRegistro.Caption);
*)
end;

procedure TFmTarefas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
(*&¨%
  Action := GOTOy.Fechar(ImgTipo.SQLType);
*)
end;

procedure TFmTarefas.QrCadastro_SimplesAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTarefas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTarefas.SbQueryClick(Sender: TObject);
begin
(*&¨%$
  LocCod(QrCadastro_SimplesCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadastro_simples', Dmod.MyDB, CO_VAZIO));
*)
end;

procedure TFmTarefas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTarefas.QrCadastro_SimplesBeforeOpen(DataSet: TDataSet);
begin
(*&¨%$
  QrCadastro_SimplesCodigo.DisplayFormat := FFormatFloat;
*)
end;

end.

