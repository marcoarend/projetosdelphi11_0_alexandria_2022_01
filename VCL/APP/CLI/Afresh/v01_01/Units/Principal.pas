unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Registry, Data.DB, Vcl.Grids, Vcl.DBGrids,
(*
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, CheckLst, ComCtrls, ExtCtrls, Spin, Menus, ShellApi,
  UnInternalConsts, MySQLTools, Db, mySQLDbTables, Grids, DBGrids,
  MySQLDump, MySQLBatch, dmkGeral, UnDmkSystem, SHFolder, dmkEdit, UnDmkProcFunc,
  UnDmkEnums, UnGrl_Vars;
  *)
  Winapi.ShlObj, Winapi.KnownFolders, DCPcrypt2, DCPblockciphers, DCPtwofish,
  Datasnap.DBClient, Vcl.ComCtrls, dmkEdit;
type
  TFmPrincipal = class(TForm)
    TrayIcon1: TTrayIcon;
    PopupMenu1: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    N1: TMenuItem;
    Inicializao1: TMenuItem;
    Executarnainicializao1: TMenuItem;
    NOexecutarnainicializao1: TMenuItem;
    TmOculta: TTimer;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtAtiva: TBitBtn;
    Button1: TButton;
    DCP_twofish1: TDCP_twofish;
    BtCon: TBitBtn;
    MemTable: TClientDataSet;
    MemTableTpAcao: TIntegerField;
    MemTableCodigo: TIntegerField;
    MemTableOrdem: TIntegerField;
    MemTableNome: TStringField;
    MemTableSegundosA: TFloatField;
    MemTableSegundosD: TFloatField;
    MemTableMousePosX: TIntegerField;
    MemTableTexto: TStringField;
    MemTableMousePosY: TIntegerField;
    MemTableMouseEvent: TIntegerField;
    MemTableQtdClkSmlt: TIntegerField;
    MemTableKeyCode: TIntegerField;
    MemTableKeyEvent: TIntegerField;
    MemTableShiftState: TIntegerField;
    MemTableTipoTecla: TIntegerField;
    MemTableTeclEsp: TIntegerField;
    MemTableLetra: TStringField;
    DataSource1: TDataSource;
    BtOpcoesApp: TBitBtn;
    TmMonitora: TTimer;
    Label1: TLabel;
    EdTempoAtividade: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    BtVisualizaItens: TBitBtn;
    DBGEtapas: TDBGrid;
    MeLog: TMemo;
    Button2: TButton;
    BtSobre: TBitBtn;
    Button3: TButton;
    procedure TrayIcon1Click(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure TmOcultaTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtConClick(Sender: TObject);
    procedure BtAtivaClick(Sender: TObject);
    procedure BtOpcoesAppClick(Sender: TObject);
    procedure TmMonitoraTimer(Sender: TObject);
    procedure BtVisualizaItensClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BtSobreClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
    //FHides: Integer;
    FShowed, FAtivou: Boolean;
    //FHideFirst: Boolean;
    FNome, FPasta, FExecutavel, FParametros: String;
    FMonitSohProprioProces,
    FParaEtapaOnDeactive, FExecAsAdmin, FLogInMemo, FExeOnIni: Boolean;
    FSegWaitOnOSReinit, FSegWaitScanMonit,
    FEstadoJanela, FMaiorCodigo, FHProcesso: Integer;
    //
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
    procedure AtivaTarefa(Aguarda, Executa: Boolean);
    procedure ConnectAs();
  public
    { Public declarations }
    FBtAtivado: Boolean;
    //
  end;

var
  FmPrincipal: TFmPrincipal;

const
  CO_Titulo = 'Reiniciador de Aplicativos';

implementation

{$R *.dfm}

uses dmkGeral, UnMyObjects, Tarefa, UnDmkProcFunc, Teste, UnLicVendaApp_Dmk,
  UnMyVclEvents, ShellApi, UnAppPF, UnAppMainPF, OpcoesApp, UnApp_Vars;
(*
UnAppJan, MyDBCheck, UnOVS_ProjGroupVars, UnProjGroup_Jan,
  Module, ModuleGeral, UnProjGroup_PF;
*)

procedure TFmPrincipal.AtivaTarefa(Aguarda, Executa: Boolean);
  procedure ExecutaEspera();
    function T(): Integer;
    begin
     Result := GetTickCount div 1000;
    end;
  var
    SegWait, Espera: Integer;
  begin
    SegWait:= FSegWaitOnOSReinit;
    if T() < SegWait then
    begin
      while T() <  SegWait do
      begin
        if FBtAtivado = False then Exit;
        //
        EdTempoAtividade.Text := AppPF.UpTime();
        Application.ProcessMessages();
        Sleep(1000);
      end;
    end else
    begin
      //SegWait := 2; // testar
      SegWait := 400; // 400 * 50 = 20000 (20 segundos)
      Espera  := 0;
      while Espera < SegWait do
      begin
        if FBtAtivado = False then Exit;
        //
        Application.ProcessMessages();
        Sleep(50);
        Espera := Espera + 1;
      end;
    end;
    Application.ProcessMessages();
  end;
var
  OrdemStop, CodigoStop: Integer;
  Agora: TDateTime;
begin
(*
  BtAtiva.Enabled := False;
  try
*)
    if (FBtAtivado = False) or (Executa) then
    begin
      //////////// Verifica se processo est� ativo! ////////////////////////////////
      // Procura pelo hProcess conhecido caso exigido pelo usu�rio desta forma!
      if FMonitSohProprioProces  then
      begin
        if MyVclEvents.FindProcessesByHProcess(FhProcesso) then Exit;  // achou!
      end else
      begin
        // Localiza pelo nome!
        if MyVclEvents.FindProcessesByName(FExecutavel) >= 0 then Exit; //Achou!
      end;

      BtAtiva.Caption := '&Desativa';
      FBtAtivado := True;
      //
      OrdemStop  := High(Integer);
      CodigoStop := High(Integer);
      //
      Application.ProcessMessages();
      if Aguarda then
        ExecutaEspera();
      //
      if FBtATivado = False then Exit;
      //
      Agora := Now();
      if AppMainPF.ExecutaPassosRobot((*Form*)Self,
      FExecAsAdmin,
      MemTable,
      MemTableTpAcao,
      MemTableCodigo,
      MemTableOrdem,
      MemTableNome,
      MemTableSegundosA,
      MemTableSegundosD,
      MemTableMousePosX,
      MemTableTexto,
      MemTableMousePosY,
      MemTableMouseEvent,
      MemTableQtdClkSmlt,
      MemTableKeyCode,
      MemTableKeyEvent,
      MemTableShiftState,
      MemTableTipoTecla,
      MemTableTeclEsp,
      MemTableLetra,
      FPasta,
      FExecutavel,
      FParametros,
      FEstadoJanela,
      OrdemStop, CodigoStop,
      FHProcesso) then
        AppPF.InfoLog(MeLog, Agora, 'Processo restaurado.')
      else
        AppPF.InfoLog(MeLog, Agora, 'N�o foi poss�vel resaurar o processo!.')
    end else
    begin
      FBtAtivado := False;
      BtAtiva.Caption := '&Ativa';
      //
    end;
(*
  finally
    BtAtiva.Enabled := True;
  end;
*)
end;

procedure TFmPrincipal.BtVisualizaItensClick(Sender: TObject);
var
  Continua: Boolean;
begin
  if DBGEtapas.Visible then
  begin
    DBGEtapas.Visible := False;
    BtVisualizaItens.Caption := 'Visualizar itens';
  end else
  begin
    if VAR_SENHA_BOSS <> EmptyStr then
      Continua := AppPF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
    else
      Continua := True;
    if Continua then
    begin
      DBGEtapas.Visible := True;
      BtVisualizaItens.Caption := 'Ocultar itens';
    end;
  end;
end;

procedure TFmPrincipal.BtAtivaClick(Sender: TObject);
begin
  AtivaTarefa(False, not FBtAtivado);
end;

procedure TFmPrincipal.BtConClick(Sender: TObject);
var
  Continua: Boolean;
begin
  if VAR_SENHA_BOSS <> EmptyStr then
    Continua := AppPF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
  else
    Continua := True;
  if Continua then
  begin
    Application.CreateForm(TFmTarefa, FmTarefa);
    FmTarefa.ShowModal;
    FmTarefa.Destroy;
  end;
end;

procedure TFmPrincipal.BtOpcoesAppClick(Sender: TObject);
begin
  AppPF.MostraFormOpcoes();
end;

procedure TFmPrincipal.BtSaidaClick(Sender: TObject);
begin
  TrayIcon1.Visible := True;
  Application.MainFormOnTaskBar := FShowed;
  FmPrincipal.Hide;
  //FmWetBlueMLA_BK.WindowState :=  wsMinimized;
  Application.MainFormOnTaskBar := FShowed;

end;

procedure TFmPrincipal.BtSobreClick(Sender: TObject);
begin
  AppPF.MostraFormABOUT();
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
  Application.CreateForm(TFmTeste, FmTeste);
  FmTeste.ShowModal;
  FmTeste.Destroy;
end;

procedure TFmPrincipal.Button2Click(Sender: TObject);
const
  CPFCNPJs: array[0..2] of String = ('03.143.014/0001-52', '02.582.267/0001-60', '96.734.892/0001-23');
var
  I, J: Integer;
  Texto, CPFCNPJ, CodigoPIN: String;
begin
  for I := 0 to 9 do
  begin
    Texto := '';
    for J := 0 to 2 do
    begin
      CPFCNPJ := CPFCNPJs[J];
      LicVendaApp_Dmk.ObtemCodigoPinDeCnpj(CPFCNPJ, I + 1, CodigoPIN);
      //
      Texto := Texto + ' [' + CPFCNPJ + ']  [' + CodigoPIN + ']';
    end;
    MeLog.Lines.Add(Texto);
  end;
end;

procedure TFmPrincipal.Button3Click(Sender: TObject);
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    sei.lpVerb := 'runas'; // Run as admin
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := HWND_CmdShow; //SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      InstApp  := sei.hInstApp;
      //HProcesso := sei.hProcess;
    end;
  end;
var
  Executavel, Pasta, Parametros: String;
begin
  Executavel     := 'Sniffer.exe';
  //Executavel     := 'Ativador.exe';
  Pasta          := 'C:\Executaveis\19.0\EarTug\';
  Parametros     := '';
  RunAsAdmin(FmPrincipal.Handle, Executavel, Pasta, Parametros, SW_SHOWNORMAL);
end;

function TFmPrincipal.ConnectAs: Boolean;
var
  hToken: THandle;
begin
  Result := LogonUser(PChar(lpszUsername), nil, PChar(lpszPassword), LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, hToken);
  if Result then
    Result := ImpersonateLoggedOnUser(hToken)
  else
    RaiseLastOSError;
end;


/ procedure TForm2.Executa;
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    //sei.lpVerb := 'runas'; // Run as admin
    sei.lpVerb := '';
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := HWND_CmdShow; //SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      InstApp  := sei.hInstApp;
      //HProcesso := sei.hProcess;
    end;
  end;
var
  Executavel, Pasta, Parametros: String;
  _HWND: HWND;
begin
{
  //////////////////////////////////////////////////////////////////////////////
  //Executavel     := 'TesteOutroApp.exe';
  Executavel     := 'Project1.exe';
  Pasta          := ExtractFilePath('C:\_Compilers\ProjetosDelphi10_2_2_Tokyo\VCL\APP\CLI\Afresh\UserLogin\Win32\Debug\');
  //Pasta          := 'C:\Executaveis\19.0\EarTug\';
  Parametros     := '';
  // _HWND := nil;
  //////////////////////////////////////////////////////////////////////////////
}
  //"C:\Users\angeloni\AppData\Local\Uniface\Uniface Anywhere Client\Client\ua-client.exe" -h www30.bhan.com.br -u NAYR -p 660286 -c -a "Virtual Age"
  //"C:\Program Files (x86)\Uniface\Uniface Anywhere Client\Client\ua-client.exe" -h www30.bhan.com.br -u NAYR -p 660286 -c -a "Virtual Age"
  Executavel     := 'ua-client.exe';
  Pasta          := 'C:\Program Files (x86)\Uniface\Uniface Anywhere Client\Client\';
  //Pasta          := 'C:\Users\angeloni\AppData\Local\Uniface\Uniface Anywhere Client\Client\';
  //Pasta          := 'C:\Executaveis\19.0\EarTug\';
  Parametros     := ' -h www30.bhan.com.br -u NAYR -p 660286 -c -a "Virtual Age"';
  RunAsAdmin(0, Executavel, Pasta, Parametros, SW_SHOWNORMAL);
end;



end;

procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo,
  Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.Executarnainicializao1Click(Sender: TObject);
begin
// UnAppPF.GetSpecialFolderPath(CSIDL_STARTUP) =
//C:\Users\angeloni\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup

//Make Vista launch UAC restricted programs at startup with Task Scheduler
//https://www.techrepublic.com/blog/windows-and-office/make-vista-launch-uac-restricted-programs-at-startup-with-task-scheduler/


  ExecutaNaInicializacao(True, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
var
  Continua: Boolean;
begin
  MyObjects.CorIniComponente;
  Continua := False;
  //
  if FAtivou = False then
  begin
    FAtivou := True;
    if AppPF.ObtemDadosOpcoesAppDeArquivo(
      AppPF.ObtemNomeArquivoOpcoes(), DCP_twofish1) then
    begin
      if AppPF.LiberaUsoVendaApp1(DCP_twofish1(*, FApp User Pwd, FExe On Ini*)) then
      begin
        // Liberar tudo por aqui!
        if (VAR_SENHA_BOSS <> EmptyStr) and (VAR_ExigeSenhaLoginApp) then
          Continua := AppPF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
        else
          Continua := True;
      end else
      begin
        Geral.MB_Info('N�O Liberado!');
        Halt(0);
      end;
    end;
  end else
  begin
    Continua := True;
  end;
  //
  if Continua then
  begin
    AppMainPF.CriaEcarregaTabela(
    FmPrincipal.DCP_twofish1,
    MemTable,
    MemTableTpAcao,
    MemTableCodigo,
    MemTableOrdem,
    MemTableNome,
    MemTableSegundosA,
    MemTableSegundosD,
    MemTableMousePosX,
    MemTableTexto,
    MemTableMousePosY,
    MemTableMouseEvent,
    MemTableQtdClkSmlt,
    MemTableKeyCode,
    MemTableKeyEvent,
    MemTableShiftState,
    MemTableTipoTecla,
    MemTableTeclEsp,
    MemTableLetra,
    FNome, FPasta, FExecutavel, FParametros, FMonitSohProprioProces,
    FParaEtapaOnDeactive, FExecAsAdmin, FLogInMemo, FExeOnIni,
    FSegWaitOnOSReinit, FSegWaitScanMonit,
    FEstadoJanela, FMaiorCodigo);
    TmMonitora.Interval := FSegWaitScanMonit * 1000;
    TmOculta.Enabled := True;
    //if ????? then
    begin
      TrayIcon1.Visible := True;
      //
    end;
  end;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TrayIcon1 <> nil then
    TrayIcon1.Visible := False;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  FBtAtivado  := False;
  FExeOnIni   := False;
  FHProcesso  := 0;
(*
  TMeuDB          := 'overseer';
  VAR_RUN_AS_SVC  := True;
  VAR_BDSENHA     := EmptyStr;
  FHides          := 0;
*)
  FShowed         := True;
  FAtivou         := False;
  FNome           := '';
  FPasta          := '';
  FExecutavel     := '';
  FParametros     := '';
  FEstadoJanela   := -1;
  //
(*
  VAR_TYPE_LOG :=  TTypeLog.ttlCliIntUni;
*)
  Geral.DefineFormatacoes;
  dmkPF.ConfigIniApp(0);
  Application.OnMessage := MyObjects.FormMsg;
  //Application.OnIdle    := AppIdle;
  //
end;

procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

procedure TFmPrincipal.NOexecutarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(False, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.PopupMenu1Popup(Sender: TObject);
var
  Valor: String;
begin
  Valor := Geral.ReadAppKeyCU(CO_Titulo, 'Microsoft\Windows\CurrentVersion\Run', ktString, '');
  //
  if Valor <> '' then
  begin
    Executarnainicializao1.Checked   := True;
    NOexecutarnainicializao1.Checked := False;
  end else
  begin
    Executarnainicializao1.Checked   := False;
    NOexecutarnainicializao1.Checked := True;
  end;
  Mostrar1.Enabled := not FmPrincipal.Visible;
end;

procedure TFmPrincipal.TmMonitoraTimer(Sender: TObject);
begin
  AtivaTarefa(False, True);
  EdTempoAtividade.Text := AppPF.UpTime();
end;

procedure TFmPrincipal.TmOcultaTimer(Sender: TObject);
begin
  TmOculta.Enabled := False;
  BtSaidaClick(Self);
  if (MemTable.State <> dsInactive)  and (FExeOnIni) then
  begin
    AtivaTarefa(True, True);
    //TmMonitora.Interval := FSegWaitScanMonit * 1000;
    TmMonitora.Enabled := True;
  end;
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;


//https://stackoverflow.com/questions/17064672/programmatical-log-in-by-providing-credentials

{
You can impersonate a logged on user to access the data folder, using the LogonUser, ImpersonateLoggedOnUser and RevertToSelf functions.
Try this sample

{$APPTYPE CONSOLE}
{
uses
  Windows,
  SysUtils;

function ConnectAs(const lpszUsername, lpszPassword: string): Boolean;
var
  hToken       : THandle;
begin
  Result := LogonUser(PChar(lpszUsername), nil, PChar(lpszPassword), LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, hToken);
  if Result then
    Result := ImpersonateLoggedOnUser(hToken)
  else
  RaiseLastOSError;
end;

begin
  try
   ConnectAs('Admin','Password');
   //do something here


   //terminates the impersonation
   RevertToSelf;

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
  readln;
end.
}

end.
