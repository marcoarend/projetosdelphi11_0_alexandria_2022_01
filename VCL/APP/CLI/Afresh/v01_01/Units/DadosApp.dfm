object FmDadosApp: TFmDadosApp
  Left = 0
  Top = 0
  Caption = 'Configura'#231#227'o de ....'
  ClientHeight = 353
  ClientWidth = 556
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 556
    Height = 133
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 114
      Height = 13
      Caption = 'Nome da configura'#231#227'o :'
    end
    object Label2: TLabel
      Left = 12
      Top = 48
      Width = 219
      Height = 13
      Caption = 'Linha de comando de reinicia'#231#227'o do processo:'
    end
    object Label3: TLabel
      Left = 12
      Top = 88
      Width = 241
      Height = 13
      Caption = 'Nome do processo na lista de tarefas do windows:'
    end
    object EdNome: TdmkEdit
      Left = 12
      Top = 24
      Width = 529
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdLinCom: TdmkEdit
      Left = 12
      Top = 64
      Width = 529
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdProcesso: TdmkEdit
      Left = 12
      Top = 104
      Width = 529
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 133
    Width = 556
    Height = 220
    Align = alClient
    TabOrder = 1
    object Panel3: TPanel
      Left = 1
      Top = 164
      Width = 554
      Height = 55
      Align = alBottom
      TabOrder = 0
      object BtOK: TBitBtn
        Left = 8
        Top = 8
        Width = 120
        Height = 40
        Caption = 'Salvar'
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Left = 420
        Top = 8
        Width = 120
        Height = 40
        Caption = 'Desiste'
        TabOrder = 1
        OnClick = BitBtn1Click
      end
      object Button1: TButton
        Left = 264
        Top = 16
        Width = 75
        Height = 25
        Caption = 'Add'
        Enabled = False
        TabOrder = 2
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 340
        Top = 16
        Width = 75
        Height = 25
        Caption = 'Indice'
        Enabled = False
        TabOrder = 3
        OnClick = Button2Click
      end
      object BtItens: TBitBtn
        Left = 128
        Top = 8
        Width = 120
        Height = 40
        Caption = 'Itens'
        TabOrder = 4
        OnClick = BtItensClick
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 554
      Height = 163
      Align = alClient
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
  end
  object DataSource1: TDataSource
    DataSet = MemTable
    Left = 360
    Top = 209
  end
  object PMItens: TPopupMenu
    Left = 200
    Top = 217
    object IncluiClickDeMouse1: TMenuItem
      Caption = 'Inclui click de mouse'
      OnClick = IncluiClickDeMouse1Click
    end
    object IncluiDigitaoDeTexto1: TMenuItem
      Caption = 'Inclui digita'#231#227'o de texto'
    end
    object IncluiComandoTabEnterEtc1: TMenuItem
      Caption = 'Inclui comando (Tab, Enter, etc...)'
    end
  end
  object MemTable: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 360
    Top = 161
    object MemTableCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object MemTableNome: TStringField
      FieldName = 'Nome'
      Size = 100
    end
    object MemTableOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object MemTableTpAcao: TIntegerField
      FieldName = 'TpAcao'
    end
    object MemTableMousePosX: TIntegerField
      FieldName = 'MousePosX'
    end
    object MemTableMousePosY: TIntegerField
      FieldName = 'MousePosY'
    end
    object MemTableMouseEvent: TIntegerField
      FieldName = 'MouseEvent'
    end
    object MemTableKeyCode: TIntegerField
      FieldName = 'KeyCode'
    end
    object MemTableKeyEvent: TIntegerField
      FieldName = 'KeyEvent'
    end
    object MemTableTexto: TStringField
      FieldName = 'Texto'
      Size = 255
    end
  end
end
