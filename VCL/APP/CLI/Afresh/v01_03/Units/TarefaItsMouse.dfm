object FmTarefaItsMouse: TFmTarefaItsMouse
  Left = 0
  Top = 0
  Caption = 'FmTarefaItsMouse'
  ClientHeight = 306
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 306
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 20
      Width = 37
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 76
      Top = 20
      Width = 36
      Height = 13
      Caption = 'Ordem:'
    end
    object Label3: TLabel
      Left = 144
      Top = 20
      Width = 96
      Height = 13
      Caption = 'Descri'#231#227'o do passo:'
    end
    object Label4: TLabel
      Left = 12
      Top = 192
      Width = 277
      Height = 13
      Caption = 'Tempo de espera (em segundos) antes do click do mouse:'
    end
    object Label5: TLabel
      Left = 12
      Top = 216
      Width = 267
      Height = 13
      Caption = 'Tempo de espera (em segundos) ap'#243's o click do mouse:'
    end
    object Memo1: TMemo
      Left = 400
      Top = 186
      Width = 229
      Height = 59
      Lines.Strings = (
        'Memo1')
      TabOrder = 7
      Visible = False
    end
    object EdCodigo: TdmkEdit
      Left = 8
      Top = 36
      Width = 65
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdOrdem: TdmkEdit
      Left = 76
      Top = 36
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 144
      Top = 36
      Width = 481
      Height = 21
      MaxLength = 255
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 243
      Width = 635
      Height = 63
      Align = alBottom
      TabOrder = 6
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel2: TPanel
        Left = 496
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 60
      Width = 621
      Height = 123
      Caption = ' Dados do click: '
      TabOrder = 3
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 617
        Height = 106
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LabelX: TLabel
          Left = 144
          Top = 8
          Width = 10
          Height = 13
          Caption = 'X:'
        end
        object LabelY: TLabel
          Left = 212
          Top = 8
          Width = 10
          Height = 13
          Caption = 'Y:'
        end
        object BtCapturar: TBitBtn
          Left = 4
          Top = 8
          Width = 137
          Height = 40
          Caption = 'Iniciar Captura'
          TabOrder = 0
          OnClick = BtCapturarClick
        end
        object EdMousePosX: TdmkEdit
          Left = 144
          Top = 24
          Width = 65
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
          ValWarn = False
        end
        object EdMousePosY: TdmkEdit
          Left = 212
          Top = 24
          Width = 65
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
          ValWarn = False
        end
        object RGBotao: TdmkRadioGroup
          Left = 268
          Top = 52
          Width = 345
          Height = 50
          Caption = ' Bot'#227'o do mouse: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'Esquerdo'
            'Direto'
            'Centro')
          TabOrder = 4
          UpdType = utYes
          OldValor = 0
        end
        object RGQtdClkSmlt: TdmkRadioGroup
          Left = 4
          Top = 52
          Width = 261
          Height = 50
          Caption = ' Click do mouse: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'Simples'
            'Duplo')
          TabOrder = 3
          UpdType = utYes
          OldValor = 0
        end
      end
    end
    object EdSegundosA: TdmkEdit
      Left = 296
      Top = 188
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdSegundosD: TdmkEdit
      Left = 296
      Top = 212
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
end
