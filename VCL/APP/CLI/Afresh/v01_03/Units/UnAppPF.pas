unit UnAppPF;

interface

uses Winapi.Windows, Winapi.Messages, Vcl.Forms, Winapi.ShellApi, Winapi.ShlObj,
  System.SysUtils, Vcl.Controls, UnInternalConsts,
  DCPcrypt2, DCPblockciphers, DCPtwofish, DCPsha1, System.JSON, System.IOUtils,
  System.Classes, UnGrl_Vars, UnApp_Vars, Vcl.StdCtrls;
type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ConnectAs(lpszUsername, lpszPassword: String): Boolean;
    function  LiberaUsoVendaApp1(const DCP_twofish: TDCP_twofish(*; var
              App User Pwd, Exe On Ini: String*)): Boolean;
    function  ObtemDadosLicencaDeArquivo(const Arquivo: String; const DCP_twofish:
              TDCP_twofish; var AtualizadoEm, SerialHD, CPUID, SerialKey:
              String): Boolean;
    //
    procedure EsperaEprocessaMensagensWindows(Segundos: Double);
    function  MouseBotaoEventToWindowsMessage(MouseBotaoEvent: Integer): Integer;
    function  WindowsMessageToMouseBotaoEvent(WindowsMessage: Integer): Integer;

    function  EstadoJanelaToHWND_CmdShow(EstadoJanela: Integer): Integer;
    function  HWND_CmdShowToEstadoJanela(HWND_CmdShow: Integer): Integer;

    function  ObtemNomeArquivoTarefa(): String;
    function  ObtemNomeArquivoLicenca(var Arquivo, Pasta: String): Boolean;

    procedure MostraFormAbout();

    function UpTime(): string;

    procedure InfoLog(Memo: TMemo; DataHora: TDateTime; Msg: String);

  end;

var
  AppPF: TUnAppPF;


implementation

uses dmkGeral, UnLicVendaApp_Dmk, UnDmkProcFunc, MyListas, UnMyJSON,
  LicDados, Dmk_Afresh, OpcoesApp, UnMyVclEvents, About,
  UnProjGroup_PF, UnProjGroup_Vars;

{ TUnAppPF }

procedure TUnAppPF.EsperaEprocessaMensagensWindows(Segundos: Double);
begin
  Application.ProcessMessages;
  if Segundos >= 0.001 then
  begin
    Sleep(Trunc(Segundos * 1000));
    Application.ProcessMessages;
  end;
end;

function TUnAppPF.EstadoJanelaToHWND_CmdShow(EstadoJanela: Integer): Integer;
begin
  case EstadoJanela of
    //Padr�o do app
    0: Result := SW_SHOW;
    //Normal
    1: Result := SW_NORMAL;
    //Maximizada
    2: Result := SW_MAXIMIZE;
    //Minimizada
    3: Result := SW_MINIMIZE;
    //Oculta
    4: Result := SW_HIDE;
  end;
end;

function TUnAppPF.ConnectAs(lpszUsername, lpszPassword: String): Boolean;
var
  hToken: THandle;
begin
  Result := LogonUser(PChar(lpszUsername), nil, PChar(lpszPassword), LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, hToken);
  if Result then
    Result := ImpersonateLoggedOnUser(hToken)
  else
    RaiseLastOSError;

(*
  procedure TForm2.Executa;
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    //sei.lpVerb := 'runas'; // Run as admin
    sei.lpVerb := '';
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := HWND_CmdShow; //SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      InstApp  := sei.hInstApp;
      //HProcesso := sei.hProcess;
    end;
  end;
var
  Executavel, Pasta, Parametros: String;
  _HWND: HWND;
begin
{
  //////////////////////////////////////////////////////////////////////////////
  //Executavel     := 'TesteOutroApp.exe';
  Executavel     := 'Project1.exe';
  Pasta          := ExtractFilePath('C:\_Compilers\ProjetosDelphi10_2_2_Tokyo\VCL\APP\CLI\Afresh\UserLogin\Win32\Debug\');
  //Pasta          := 'C:\Executaveis\19.0\EarTug\';
  Parametros     := '';
  // _HWND := nil;
  //////////////////////////////////////////////////////////////////////////////
}
  //"C:\Users\angeloni\AppData\Local\Uniface\Uniface Anywhere Client\Client\ua-client.exe" -h www30.bhan.com.br -u NAYR -p 660286 -c -a "Virtual Age"
  //"C:\Program Files (x86)\Uniface\Uniface Anywhere Client\Client\ua-client.exe" -h www30.bhan.com.br -u NAYR -p 660286 -c -a "Virtual Age"
  Executavel     := 'ua-client.exe';
  Pasta          := 'C:\Program Files (x86)\Uniface\Uniface Anywhere Client\Client\';
  //Pasta          := 'C:\Users\angeloni\AppData\Local\Uniface\Uniface Anywhere Client\Client\';
  //Pasta          := 'C:\Executaveis\19.0\EarTug\';
  Parametros     := ' -h www30.bhan.com.br -u NAYR -p 660286 -c -a "Virtual Age"';
  RunAsAdmin(0, Executavel, Pasta, Parametros, SW_SHOWNORMAL);
end;
*)
end;

function TUnAppPF.HWND_CmdShowToEstadoJanela(HWND_CmdShow: Integer): Integer;
begin
  case HWND_CmdShow of
    //Padr�o do app
    SW_SHOW       : Result := 0;
    //Normal
    SW_NORMAL     : Result := 1;
    //Maximizada
    SW_MAXIMIZE   : Result := 2;
    //Minimizada
    SW_MINIMIZE   : Result := 3;
    //Oculta
    SW_HIDE       : Result := 4;
  end;
end;

procedure TUnAppPF.InfoLog(Memo: TMemo; DataHora: TDateTime; Msg: String);
begin
  Memo.Lines.Add(Geral.FDT(DataHora, 0) + '  ' + Msg);
end;

function TUnAppPF.LiberaUsoVendaApp1(const DCP_twofish: TDCP_twofish(*; var
  App User Pwd, Exe On Ini: String*)): Boolean;

  function MostraFormLicDados(var CPF_CNPJ, AtualizadoEm, SerialHD, CPUID,
  SerialKey(*, App User Pwd, Exe On Ini*): String): Boolean;
  begin
    Application.CreateForm(TFmLicDados, FmLicDados);
    FmLicDados.EdCPF_CNPJ.ValueVariant  := VAR_CPF_CNPJ;
    FmLicDados.EdPosicao.ValueVariant   := VAR_Posicao;
    FmLicDados.EdCodigoPIN.ValueVariant := VAR_CodigoPIN;
    FmLicDados.ShowModal;
    CPF_CNPJ     := VAR_CPF_CNPJ;
    AtualizadoEm := FmLicDados.FAtualizadoEm;
    SerialHD     := FmLicDados.FSerialHD;
    CPUID        := FmLicDados.FCPUID;
    SerialKey    := FmLicDados.FSerialKey;
    //App User Pwd   := FmLicDados.FApp User Pwd;
    //Exe On Ini     := FmLicDados.F Exe On Ini;
    //
    Result       := FmLicDados.FLiberado;
    //
    FmLicDados.Destroy;
  end;
  //
  function GeraESalvaJSON(Dir, DirEArquivo, CPF_CNPJ, AtualizadoEm, SerialHD,
  CPUID, SerialKey(*, App User Pwd, Exe On Ini*): String): Boolean;
  var
    i: Integer;
    lJsonObj: TJSONObject;
    JSONColor: TJSONObject;
    JSONArray : TJSONArray;
    Nome, Pasta, Executavel, Parametros: String;
    EstadoJanela: Integer;
    TxtCript: String;
  begin
    Result := False;
    Screen.Cursor := crHourGlass;
    try
      lJsonObj     := TJSONObject.Create;
      //
      lJsonObj.AddPair('CPF_CNPJ', Geral.JsonText(CPF_CNPJ));
      lJsonObj.AddPair('SerialHD', Geral.JsonText(SerialHD));
      lJsonObj.AddPair('CPUID', Geral.JsonText(CPUID));
      lJsonObj.AddPair('SerialKey', Geral.JsonText(SerialKey));
      lJsonObj.AddPair('AtualizadoEm', Geral.JsonText(AtualizadoEm));
      //lJsonObj.AddPair('App User Pwd', Geral.JsonText(AppUserPwd));
      //lJsonObj.AddPair('Exe On Ini', Geral.JsonText(ExeOnIni));
      //
      TxtCript := ProjGroup_PF.EnCryptJSON(lJsonObj.ToString, DCP_twofish);
      ForceDirectories(Dir);
      TFile.WriteAllText(DirEArquivo, TxtCript, TEncoding.ANSI);
      //
      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

//https://forums.embarcadero.com/thread.jspa?threadID=117722
var
  Arq_AtualizadoEm, Arq_SerialHD, Arq_CPUID, Arq_SerialKey,
  Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey,
  Arquivo, Pasta: String;
  Liberado: Boolean;
  Salvar: Boolean;
  Agora, UltAtz: TDateTime;
begin
  Liberado := False;
  Salvar   := False;
  ObtemNomeArquivoLicenca(Arquivo, Pasta);
  if not FileExists(Arquivo) then
  begin
    Liberado := MostraFormLicDados(VAR_CPF_CNPJ,
      Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey(*, App User Pwd, Exe On Ini*));
    Salvar := Liberado;
  end else
  begin
    // Abrir arquivo aqui!
    ObtemDadosLicencaDeArquivo(Arquivo, DCP_twofish, Arq_AtualizadoEm,
    Arq_SerialHD, Arq_CPUID, Arq_SerialKey (*App User Pwd, Exe On Ini,*) );

    Maq_SerialKey := DmkPF.CalculaValSerialKey_VendaApp(VAR_CPF_CNPJ, Maq_SerialHD, Maq_CPUID);
    if Arq_SerialKey <> Maq_SerialKey then
    begin
      //App User Pwd := '';
      //Exe On Ini   := '0';
      Liberado := MostraFormLicDados(VAR_CPF_CNPJ,
        Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey(*, App User Pwd, Exe On Ini*));
      Salvar := Liberado;
    end else
    begin
      //UltAtz := Geral.ValidaDataBR(Arq_AtualizadoEm, (*PermiteZero*) True,
      //(*ForceNextYear*)False, (*MostraMsg*)True);
      UltAtz := Geral.ValidaDataHoraSQL(Arq_AtualizadoEm);
      //
      if LicVendaApp_Dmk.ObtemDataHoraWeb(Agora, 2000) then
      begin
        if Agora - UltAtz > 7 then // mais de uma semana
        begin
          if LicVendaApp_Dmk.LiberaUso(VAR_CPF_CNPJ, VAR_CodigoPIN, CO_DMKID_APP, CO_VERSAO,
          Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey) then
          begin
            Liberado := True;
            Salvar   := True;
          end else
           Halt(0);
          Liberado := True;
        end else
          Liberado := True;
      end else
      begin
        // N�o tem internet?
        Liberado := True;
      end;
    end;
  end;
  if Salvar then
  begin
    //if Exe On Ini = '' then
      //Exe On Ini := '0';
    GeraESalvaJSON(Pasta, Arquivo, VAR_CPF_CNPJ, Maq_AtualizadoEm, Maq_SerialHD,
      Maq_CPUID, Maq_SerialKey(*, App User Pwd, Exe On Ini*));
  end;
  Result := Liberado;
end;

procedure TUnAppPF.MostraFormAbout();
begin
  Application.CreateForm(TFmAbout, FmAbout);
(*
  FmAbout.ProductName := Application.Title;
  FmAbout.Version.Caption := 'Vers�o: ' + VersaoTxt(CO_VERSAO);
object ProductName: TLabel
*)
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

function TUnAppPF.MouseBotaoEventToWindowsMessage(
  MouseBotaoEvent: Integer): Integer;
const
  sProcFunc = 'TUnAppPF.MouseBotaoEventToWindowsMessage()';
begin
  case MouseBotaoEvent of
    //Indefinido
    //Esquerdo
    1: Result := wm_lbuttondown;
    //Direto
    2: Result := wm_rbuttondown;
    //Centro
    3: Result := wm_mbuttondown;
    //
    else
    begin
      Result := -1;
      Geral.MB_Erro('Evento de mouse n�o implementado!' + sLineBreak + sProcFunc);
    end;
  end;
end;

function TUnAppPF.ObtemDadosLicencaDeArquivo(const Arquivo: String; const DCP_twofish:
  TDCP_twofish; var AtualizadoEm, SerialHD, CPUID, SerialKey: String): Boolean;
var
  Texto: TStringList;
var
  jsonObj, jSubObj: TJSONObject;
  ja: TJSONArray;
  jv: TJSONValue;
  i: Integer;
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
  //
  jsArray: TJSONArray;
  jsonObject: TJSONObject;
  Campo, Valor: String;
  N: Integer;
  //
  Item: TJSONObject;
  Txt, TxtCript: String;
  EstadoJanela: Integer;
  DtUltimaAtz: TDateTime;
  Liberado: Boolean;
begin
  //if not FileExists(FDBArqName) then Exit;
  if not FileExists(Arquivo) then Exit;
  Texto := TStringList.Create;
  try
    Txt := TFile.ReadAllText(Arquivo);
    Txt := ProjGroup_PF.DeCryptJSON(Txt, DCP_twofish);
    //
    jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Txt), 0) as TJSONObject;
    if jsonObj = nil then
      jsonObj := TJSONObject.ParseJSONValue(TEncoding.ANSI.GetBytes(Txt), 0) as TJSONObject;
      if jsonObj = nil then
        Exit;
    VAR_CPF_CNPJ  := MyJSON.JObjValStr(jsonObj, 'CPF_CNPJ');
    SerialHD      := MyJSON.JObjValStr(jsonObj, 'SerialHD');
    CPUID         := MyJSON.JObjValStr(jsonObj, 'CPUID');
    SerialKey     := MyJSON.JObjValStr(jsonObj, 'SerialKey');
    AtualizadoEm  := MyJSON.JObjValStr(jsonObj, 'AtualizadoEm');
    //App User Pwd    := MyJSON.JObjValStr(jsonObj, 'App User Pwd');
    //Exe On Ini      := MyJSON.JObjValStr(jsonObj, 'Exe On Ini');
    //if Exe On Ini <> '1' then
      //Exe On Ini := '0';
    DtUltimaAtz   := Geral.ValidaDataHoraSQL(AtualizadoEm);
  finally
    Texto.Free;
  end;
end;

function TUnAppPF.ObtemNomeArquivoLicenca(var Arquivo, Pasta: String): Boolean;
begin
  Arquivo := '';
  Pasta := ProjGroup_PF.GetSpecialFolderPath(CSIDL_COMMON_DOCUMENTS);
  Pasta := IncludeTrailingPathDelimiter(Pasta) + 'Dermatek\Afresh\';
  Arquivo := Pasta + 'Afresh.dmklic';
  Result := True;
end;

function TUnAppPF.ObtemNomeArquivoTarefa(): String;
var
  Pasta: String;
begin
  Result := '';
  Pasta := CO_DIR_RAIZ_DMK + '\Afresh\Data\';
  ForceDirectories(Pasta);
  //FDBArqName := Pasta + 'TarefaUnica.json';
  Result := Pasta + 'TarefaUnica.dmkafr';
end;

function TUnAppPF.UpTime(): string;
const
  ticksperday: integer = 1000 * 60 * 60 * 24;
  ticksperhour: integer = 1000 * 60 * 60;
  ticksperminute: integer = 1000 * 60;
  tickspersecond: integer = 1000;

var
  t: longword;
  d, h, m, s: integer;

begin
  t := GetTickCount;

  d := t div ticksperday;
  dec(t, d * ticksperday);

  h := t div ticksperhour;
  dec(t, h * ticksperhour);

  m := t div ticksperminute;
  dec(t, m * ticksperminute);

  s := t div tickspersecond;

  Result := IntToStr(d) + ' dias ' + IntToStr(h) + ' horas ' + IntToStr(m)
    + ' minutos ' + IntToStr(s) + ' segundos';
end;

function TUnAppPF.WindowsMessageToMouseBotaoEvent(
  WindowsMessage: Integer): Integer;
const
  sProcFunc = 'TUnAppPF.WindowsMessageToMouseBotaoEvent()';
begin
  case WindowsMessage of
    //Indefinido
    //Esquerdo
    wm_lbuttondown: Result := 1;
    //Direto
    wm_rbuttondown: Result := 2;
    //Centro
    wm_mbuttondown: Result := 3;
    //
    else
    begin
      Result := -1;
      Geral.MB_Erro('Mensagem do windows n�o implementada!' + sLineBreak + sProcFunc);
    end;
  end;
end;

end.
