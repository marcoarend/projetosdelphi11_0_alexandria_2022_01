unit Tarefa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, dmkEdit,
  Vcl.Grids, Vcl.Buttons, Data.DB, Vcl.DBGrids, Datasnap.DBClient,
  System.JSON, System.IOUtils, Vcl.Menus, dmkDBGridZTO, UnDmkProcFunc,
  DCPcrypt2, DCPblockciphers, DCPtwofish, DCPsha1, ShellApi, dmkCheckBox;
(*
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.Controls.Presentation, FMX.StdCtrls,
  System.JSON, FMX.ScrollBox, FMX.Memo, UnGrl_Vars;
*)

type
  TFmTarefa = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdNome: TdmkEdit;
    Panel2: TPanel;
    Label2: TLabel;
    EdExecutavel: TdmkEdit;
    Label3: TLabel;
    EdPasta: TdmkEdit;
    Panel3: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    DataSource1: TDataSource;
    BtItens: TBitBtn;
    PMItens: TPopupMenu;
    IncluiClickDeMouse1: TMenuItem;
    IncluiDigitaoDeTexto1: TMenuItem;
    IncluiComandoTabEnterEtc1: TMenuItem;
    MemTable: TClientDataSet;
    N1: TMenuItem;
    AlteraEtapaSelecionada1: TMenuItem;
    ExcluiEtapaSelecionada1: TMenuItem;
    DBGrid1: TDBGrid;
    RGEstadoJanela: TRadioGroup;
    Label4: TLabel;
    EdParametros: TdmkEdit;
    IncluiEtapaNova1: TMenuItem;
    MemTableTpAcao: TIntegerField;
    MemTableCodigo: TIntegerField;
    MemTableOrdem: TIntegerField;
    MemTableNome: TStringField;
    MemTableSegundosA: TFloatField;
    MemTableSegundosD: TFloatField;
    MemTableMousePosX: TIntegerField;
    MemTableTexto: TStringField;
    MemTableMousePosY: TIntegerField;
    MemTableMouseEvent: TIntegerField;
    MemTableQtdClkSmlt: TIntegerField;
    MemTableKeyCode: TIntegerField;
    MemTableKeyEvent: TIntegerField;
    MemTableShiftState: TIntegerField;
    MemTableTipoTecla: TIntegerField;
    MemTableTeclEsp: TIntegerField;
    MemTableLetra: TStringField;
    Button4: TButton;
    Edit1: TEdit;
    EdHProcesso: TdmkEdit;
    Kill: TButton;
    BtTestar: TBitBtn;
    PMTestar: TPopupMenu;
    odospassos1: TMenuItem;
    Atopassoselecionado1: TMenuItem;
    CkExeOnIni: TdmkCheckBox;
    CkMonitSohProprioProces: TdmkCheckBox;
    CkParaEtapaOnDeactive: TdmkCheckBox;
    EdSegWaitOnOSReinit: TdmkEdit;
    Label5: TLabel;
    CkExecAsAdmin: TdmkCheckBox;
    CkLogInMemo: TdmkCheckBox;
    EdSegWaitScanMonit: TdmkEdit;
    LaSegWaitOnOSReinit: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure IncluiClickDeMouse1Click(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure AlteraEtapaSelecionada1Click(Sender: TObject);
    procedure ExcluiEtapaSelecionada1Click(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure IncluiDigitaoDeTexto1Click(Sender: TObject);
    procedure IncluiComandoTabEnterEtc1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button4Click(Sender: TObject);
    procedure KillClick(Sender: TObject);
    procedure BtTestarClick(Sender: TObject);
    procedure odospassos1Click(Sender: TObject);
    procedure Atopassoselecionado1Click(Sender: TObject);
    procedure CkExeOnIniClick(Sender: TObject);
  private
    { Private declarations }
    //procedure Tabela_Carrega();
    procedure TestarPassos(Todos: Boolean);
  public
    { Public declarations }
    FDBArqName: String;
    FMaiorCodigo: Integer;
    //
    procedure Reordena();
    function  Salva(): Boolean;
  end;

var
  FmTarefa: TFmTarefa;

implementation

uses
  UnInternalConsts, UnMyJson, UnMyObjects, dmkGeral, UnDmkEnums, UnAppEnums,
  UnAppPF,
  UnMyVclEvents,
  UnMyProcesses,
  UnAppMainPF, UnProjGroup_PF,
  TarefaItsMouse, TarefaItsTexto, TarefaItsComando, Principal;

{$R *.dfm}

//const
  //FChave = 'SohJesusSalva';

procedure TFmTarefa.AlteraEtapaSelecionada1Click(Sender: TObject);
var
  TeclaEspecial: String;
  I: Integer;
begin
  if  MemTable.RecordCount = 0 then Exit;
  //
  case TTpAcaoInputEvent(MemTableTpAcao.Value) of
    TTpAcaoInputEvent.taieMouse: // 1
    begin
      Application.CreateForm(TFmTarefaItsMouse, FmTarefaItsMouse);
      //
      FmTarefaItsMouse.EdCodigo.ValueVariant      := MemTableCodigo.Value;
      FmTarefaItsMouse.EdOrdem.ValueVariant       := MemTableOrdem.Value;
      FmTarefaItsMouse.EdMousePosX.ValueVariant   := MemTableMousePosX.Value;
      FmTarefaItsMouse.EdMousePosY.ValueVariant   := MemTableMousePosY.Value;
      FmTarefaItsMouse.EdNome.ValueVariant        := MemTableNome.Value;
      FmTarefaItsMouse.EdSegundosA.ValueVariant   := MemTableSegundosA.Value;
      FmTarefaItsMouse.EdSegundosD.ValueVariant   := MemTableSegundosD.Value;
      FmTarefaItsMouse.RGBotao.ItemIndex          :=
        AppPF.WindowsMessageToMouseBotaoEvent(MemTableMouseEvent.Value);
      FmTarefaItsMouse.RGQtdClkSmlt.ItemIndex     := MemTableQtdClkSmlt.Value;

      //
      FmTarefaItsMouse.ShowModal;
      FmTarefaItsMouse.Destroy;
    end;
    TTpAcaoInputEvent.taieKeysComand: // 2
    begin
      Application.CreateForm(TFmTarefaItsComando, FmTarefaItsComando);
      //
      FmTarefaItsComando.EdCodigo.ValueVariant      := MemTableCodigo.Value;
      FmTarefaItsComando.EdOrdem.ValueVariant       := MemTableOrdem.Value;
      FmTarefaItsComando.EdNome.ValueVariant        := MemTableNome.Value;
      FmTarefaItsComando.EdSegundosA.ValueVariant   := MemTableSegundosA.Value;
      FmTarefaItsComando.EdSegundosD.ValueVariant   := MemTableSegundosD.Value;
      //
      FmTarefaItsComando.CGShiftState.Value         := MemTableShiftState.Value;
      FmTarefaItsComando.RGTipoTecla.ItemIndex      := MemTableTipoTecla.Value;
      FmTarefaItsComando.EdLetra.ValueVariant       := MemTableLetra.Value;
      //
      TeclaEspecial := MyVclEvents.ObtemTeclaEspecialDoCodigo(MemTableTeclEsp.Value);
      for I := 0 to FmTarefaItsComando.CBTeclEsp.Items.Count do
      begin
        if Uppercase(FmTarefaItsComando.CBTeclEsp.Items[I]) = Uppercase(TeclaEspecial) then
          FmTarefaItsComando.CBTeclEsp.ItemIndex := I;
      end;
      //
      FmTarefaItsComando.ShowModal;
      FmTarefaItsComando.Destroy;
    end;
    TTpAcaoInputEvent.taieTexto: // 3
    begin
      Application.CreateForm(TFmTarefaItsTexto, FmTarefaItsTexto);
      //
      FmTarefaItsTexto.EdCodigo.ValueVariant      := MemTableCodigo.Value;
      FmTarefaItsTexto.EdOrdem.ValueVariant       := MemTableOrdem.Value;
      FmTarefaItsTexto.EdTexto.ValueVariant       := MemTableTexto.Value;
      FmTarefaItsTexto.EdNome.ValueVariant        := MemTableNome.Value;
      FmTarefaItsTexto.EdSegundosA.ValueVariant   := MemTableSegundosA.Value;
      FmTarefaItsTexto.EdSegundosD.ValueVariant   := MemTableSegundosD.Value;
      //
      FmTarefaItsTexto.ShowModal;
      FmTarefaItsTexto.Destroy;
    end;
  end;
end;

procedure TFmTarefa.Atopassoselecionado1Click(Sender: TObject);
begin
  TestarPassos(False);
end;

procedure TFmTarefa.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmTarefa.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmTarefa.BtOKClick(Sender: TObject);
begin
  if Salva() then
    Close;
end;

procedure TFmTarefa.BtTestarClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTestar, BtTestar);
end;

procedure TFmTarefa.Button4Click(Sender: TObject);
var
  hProcess: Integer;
begin
  hProcess := EdHProcesso.ValueVariant;
  if hProcess <> 0 then
  begin
    if MyProcesses.FindProcessesByHProcess(hProcess) then
      Edit1.Text := 'Sim'
    else
      Edit1.Text := 'N�o'
  end else
      Edit1.Text := '???'
end;

procedure TFmTarefa.CkExeOnIniClick(Sender: TObject);
begin
  EdSegWaitOnOSReinit.Enabled := CkExeOnIni.Checked;
  LaSegWaitOnOSReinit.Enabled := CkExeOnIni.Checked;
end;

procedure TFmTarefa.DBGrid1TitleClick(Column: TColumn);
var
  indice: string;
  existe: boolean;
  clientdataset_idx: tclientdataset;
begin
  clientdataset_idx := TClientDataset(Column.Grid.DataSource.DataSet);

  if clientdataset_idx.IndexFieldNames = Column.FieldName then
  begin
    indice := AnsiUpperCase(Column.FieldName + '_INV');

    try
      clientdataset_idx.IndexDefs.Find(indice);
      existe := true;
    except
      existe := false;
    end;

    if not existe then
    begin
      with clientdataset_idx.IndexDefs.AddIndexDef do
      begin
        Name := indice;
        Fields := Column.FieldName;
        Options := [ixDescending];
      end;
    end;
    clientdataset_idx.IndexName := indice;
  end
  else
     clientdataset_idx.IndexFieldNames := Column.FieldName;
end;

procedure TFmTarefa.ExcluiEtapaSelecionada1Click(Sender: TObject);
begin
  if  MemTable.RecordCount = 0 then Exit;
  //
  if Geral.MB_Pergunta('Deseja excluir a etapa ' + Geral.FF0(MemTableCodigo.Value) +
  sLineBreak + '"' + MemTableNome.Value + '"?') = ID_YES then
  begin
    MemTable.Delete;
    Reordena();
  end;
end;

procedure TFmTarefa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Geral.MB_Pergunta('Deseja salvar as altera��es?') = ID_YES then
    Salva();
end;

procedure TFmTarefa.FormCreate(Sender: TObject);
var
  //Dir: String;
  Nome, Pasta, Executavel, Parametros: String;
  MonitSohProprioProces, ParaEtapaOnDeactive, ExecAsAdmin, LogInMemo, ExeOnIni: Boolean;
  SegWaitOnOSReinit, SegWaitScanMonit: Integer;
  EstadoJanela: Integer;
begin
  FMaiorCodigo := 0;
(*
  /
  Dir := CO_DIR_RAIZ_DMK + '\Afresh\Data\';
  ForceDirectories(Dir);
  FDBArqName := Dir + 'TarefaUnica.json';
  FDBCriptArqName := Dir + 'TarefaUnica.dmkafr';
  //////////////////////////////////////////////////////////////////////////////
  MemTable.FieldDefs.Add('Codigo',         ftInteger,  0, True);
  MemTable.FieldDefs.Add('Ordem',          ftInteger,  0, True);
  MemTable.FieldDefs.Add('Nome',           ftString, 100, True);
  //MemTable.FieldDefs.Add('Data', ftDate,    0, False);
  MemTable.FieldDefs.Add('SegundosA',      ftFloat,    0, False);
  MemTable.FieldDefs.Add('SegundosD',      ftFloat,    0, False);
  MemTable.FieldDefs.Add('TpAcao',         ftInteger,  0, False);
  //
  MemTable.FieldDefs.Add('MousePosX',      ftInteger,  0, False);
  MemTable.FieldDefs.Add('MousePosY',      ftInteger,  0, False);
  MemTable.FieldDefs.Add('MouseEvent',     ftInteger,  0, False);
  MemTable.FieldDefs.Add('QtdClkSmlt',     ftInteger,  0, False);
  //
  MemTable.FieldDefs.Add('KeyCode',        ftInteger,  0, False);
  MemTable.FieldDefs.Add('KeyEvent',       ftInteger,  0, False);
  //
  MemTable.FieldDefs.Add('Texto',          ftString, 255, False);
  //
  MemTable.FieldDefs.Add('ShiftState',     ftInteger,  0, False);
  MemTable.FieldDefs.Add('TipoTecla',      ftInteger,  0, False);
  MemTable.FieldDefs.Add('TeclEsp',        ftInteger,  0, False);
  MemTable.FieldDefs.Add('Letra',          ftString,   2, False);
  //
  //
  MemTable.CreateDataSet;
  MemTable.IndexFieldNames := 'Ordem;Codigo';
  //////////////////////////////////////////////////////////////////////////////
  Tabela_Carrega();
*)
  AppMainPF.CriaEcarregaTabela(
  FmPrincipal.DCP_twofish1,
  MemTable,
  MemTableTpAcao,
  MemTableCodigo,
  MemTableOrdem,
  MemTableNome,
  MemTableSegundosA,
  MemTableSegundosD,
  MemTableMousePosX,
  MemTableTexto,
  MemTableMousePosY,
  MemTableMouseEvent,
  MemTableQtdClkSmlt,
  MemTableKeyCode,
  MemTableKeyEvent,
  MemTableShiftState,
  MemTableTipoTecla,
  MemTableTeclEsp,
  MemTableLetra,
  Nome, Pasta, Executavel, Parametros, MonitSohProprioProces,
  ParaEtapaOnDeactive, ExecAsAdmin, LogInMemo, ExeOnIni,
  SegWaitOnOSReinit, SegWaitScanMonit,
  EstadoJanela, FMaiorCodigo);
  //
  EdNome.ValueVariant       := Nome;
  EdPasta.ValueVariant      := Pasta;
  EdExecutavel.ValueVariant := Executavel;
  EdParametros.ValueVariant := Parametros;
  RGEstadoJanela.ItemIndex  := EstadoJanela;

  CkMonitSohProprioProces.Checked  := MonitSohProprioProces;
  CkParaEtapaOnDeactive.Checked    := ParaEtapaOnDeactive;
  CkExecAsAdmin.Checked            := ExecAsAdmin;
  CkLogInMemo.Checked              := LogInMemo;
  CkExeOnIni.Checked               := ExeOnIni;
  EdSegWaitOnOSReinit.ValueVariant := SegWaitOnOSReinit;
  EdSegWaitScanMonit.ValueVariant  := SegWaitScanMonit;
end;

procedure TFmTarefa.IncluiClickDeMouse1Click(Sender: TObject);
begin
  Application.CreateForm(TFmTarefaItsMouse, FmTarefaItsMouse);
  FmTarefaItsMouse.EdOrdem.ValueVariant := MemTableCodigo.Value + 1;
  FmTarefaItsMouse.ShowModal;
  FmTarefaItsMouse.Destroy;
end;

procedure TFmTarefa.IncluiComandoTabEnterEtc1Click(Sender: TObject);
begin
  Application.CreateForm(TFmTarefaItsComando, FmTarefaItsComando);
  FmTarefaItsComando.EdOrdem.ValueVariant := MemTableCodigo.Value + 1;
  FmTarefaItsComando.ShowModal;
  FmTarefaItsComando.Destroy;
end;

procedure TFmTarefa.IncluiDigitaoDeTexto1Click(Sender: TObject);
begin
  Application.CreateForm(TFmTarefaItsTexto, FmTarefaItsTexto);
  FmTarefaItsTexto.EdOrdem.ValueVariant := MemTableCodigo.Value + 1;
  FmTarefaItsTexto.ShowModal;
  FmTarefaItsTexto.Destroy;
end;

procedure TFmTarefa.KillClick(Sender: TObject);
  function KillProcess (aProcessId : Cardinal) : boolean;
  var
    hProcess : integer;
  begin
    hProcess:= OpenProcess(PROCESS_ALL_ACCESS, TRUE, aProcessId);
    Result:= False;
    //
    if (hProcess <>0 ) then
    begin
      Result:= TerminateProcess(hProcess, 0);
      exit;
    end;
  end;
  //
begin
  if EdExecutavel.Text <> '' then
  begin
    if not KillProcess(MyProcesses.FindProcessesByName(EdExecutavel.Text)) then
      ShowMessage('Processo n�o finalizado.')
    else
      ShowMessage('Processo finalizado com sucesso!.');
  end;
end;

procedure TFmTarefa.odospassos1Click(Sender: TObject);
begin
  TestarPassos(True);
end;

procedure TFmTarefa.Reordena;
var
  Codigo, I: Integer;
  Codigos: array of Integer;
begin
  if MemTable.RecordCount > 0 then
  begin
    Codigo := MemTableCodigo.Value;
    MemTable.First;
    SetLength(Codigos, MemTable.RecordCount);
    while not MemTable.Eof do
    begin
      Codigos[MemTable.RecNo -1] := MemTableCodigo.Value;
      //
      MemTable.Next;
    end;
    MemTable.IndexDefs.Clear;
    MemTable.IndexFieldNames := '';
    MemTable.IndexName       := '';
    MemTable.First;
    //
    for I := Low(Codigos) to High(Codigos) do
    begin
      Codigo := Codigos[I];
      MemTable.Locate('Codigo', Codigo, []);
      MemTable.Edit;
      MemTableOrdem.Value := I + 1;
      MemTable.Post;
    end;
    //
    MemTable.IndexFieldNames := 'Ordem;Codigo';
    MemTable.Locate('Codigo', Codigo, []);
  end;
end;

function TFmTarefa.Salva(): Boolean;
var
  i: Integer;
  lJsonObj: TJSONObject;
  JSONColor: TJSONObject;
  JSONArray : TJSONArray;
  Nome, Pasta, Executavel, Parametros: String;
  EstadoJanela: Integer;
  TxtCript: String;
  MonitSohProprioProces, ParaEtapaOnDeactive, ExecAsAdmin, LogInMemo, ExeOnIni: String;
  SegWaitOnOSReinit, SegWaitScanMonit: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    lJsonObj     := TJSONObject.Create;
    //
    Nome         := EdNome.Text;
    Pasta        := EdPasta.Text;
    Executavel   := EdExecutavel.Text;
    Parametros   := EdParametros.Text;
    //LinCom       := EdLinCom.Text;
    //Processo     := EdProcesso.Text;
    EstadoJanela := AppPF.EstadoJanelaToHWND_CmdShow(RGEstadoJanela.ItemIndex);


    MonitSohProprioProces := Geral.BoolToNumStr(CkMonitSohProprioProces.Checked);
    ParaEtapaOnDeactive   := Geral.BoolToNumStr(CkParaEtapaOnDeactive.Checked);
    ExecAsAdmin           := Geral.BoolToNumStr(CkExecAsAdmin.Checked);
    LogInMemo             := Geral.BoolToNumStr(CkLogInMemo.Checked);
    ExeOnIni              := Geral.BoolToNumStr(CkExeOnIni.Checked);
    SegWaitOnOSReinit     := EdSegWaitOnOSReinit.ValueVariant;
    SegWaitScanMonit      := EdSegWaitScanMonit.ValueVariant;

    lJsonObj.AddPair('Nome', Geral.JsonText(Nome));
    lJsonObj.AddPair('Pasta', Geral.JsonText(Pasta));
    lJsonObj.AddPair('Executavel', Geral.JsonText(Executavel));
    lJsonObj.AddPair('Parametros', Geral.JsonText(Parametros));
    lJsonObj.AddPair('HWNDShow', Geral.FF0(EstadoJanela));
    lJsonObj.AddPair('Itens', Geral.FF0(MemTable.RecordCount));
    //
    lJsonObj.AddPair('MonitSohProprioProces', Geral.JsonText(MonitSohProprioProces));
    lJsonObj.AddPair('ParaEtapaOnDeactive',   Geral.JsonText(ParaEtapaOnDeactive  ));
    lJsonObj.AddPair('ExecAsAdmin',           Geral.JsonText(ExecAsAdmin          ));
    lJsonObj.AddPair('LogInMemo',             Geral.JsonText(LogInMemo            ));
    lJsonObj.AddPair('ExeOnIni',              Geral.JsonText(ExeOnIni             ));
    lJsonObj.AddPair('SegWaitOnOSReinit',     Geral.FF0(SegWaitOnOSReinit    ));
    lJsonObj.AddPair('SegWaitScanMonit',      Geral.FF0(SegWaitScanMonit     ));
    //
    JSONArray := TJSONArray.Create();
    MemTable.First;
    while not MemTable.Eof do
    begin
      JSONColor := TJSONObject.Create();
      //
      JSONColor.AddPair('Codigo',      Geral.FF0(MemTableCodigo.Value));
      JSONColor.AddPair('Nome',        Geral.JsonText(MemTableNome.Value));
      JSONColor.AddPair('Ordem',       Geral.FF0(MemTableOrdem.Value));
      JSONColor.AddPair('SegundosA',   Geral.FFT_Dot(MemTableSegundosA.Value, 3, siPositivo));
      JSONColor.AddPair('SegundosD',   Geral.FFT_Dot(MemTableSegundosD.Value, 3, siPositivo));
      JSONColor.AddPair('TpAcao',      Geral.FF0(MemTableTpAcao.Value));
      JSONColor.AddPair('MousePosX',   Geral.FF0(MemTableMousePosX.Value));
      JSONColor.AddPair('MousePosY',   Geral.FF0(MemTableMousePosY.Value));
      JSONColor.AddPair('MouseEvent',  Geral.FF0(MemTableMouseEvent.Value));
      JSONColor.AddPair('QtdClkSmlt',  Geral.FF0(MemTableQtdClkSmlt.Value));
      JSONColor.AddPair('KeyCode',     Geral.FF0(MemTableKeyCode.Value));
      JSONColor.AddPair('KeyEvent',    Geral.FF0(MemTableKeyEvent.Value));
      JSONColor.AddPair('Texto',       Geral.JsonText(MemTableTexto.Value));
      JSONColor.AddPair('ShiftState',  Geral.FF0(MemTableShiftState.Value));
      JSONColor.AddPair('TipoTecla',   Geral.FF0(MemTableTipoTecla.Value));
      JSONColor.AddPair('TeclEsp',     Geral.FF0(MemTableTeclEsp.Value));
      JSONColor.AddPair('Letra',       Geral.JsonText(MemTableLetra.Value));

      JSONArray.Add(JSONColor);
      //
      MemTable.Next;
    end;
    lJsonObj.AddPair('TarefasIts', JSONArray);
    //
    TxtCript := ProjGroup_PF.EnCryptJSON(lJsonObj.ToString, FmPrincipal.DCP_twofish1);
    TFile.WriteAllText(AppPF.ObtemNomeArquivoTarefa(), TxtCript, TEncoding.ANSI);

    //btnsave.Caption := 'Load Data';
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTarefa.TestarPassos(Todos: Boolean);
var
  HProcesso, OrdemStop, CodigoStop: Integer;
begin
  if Todos then
  begin
    OrdemStop  := High(Integer);
    CodigoStop := High(Integer);
  end else
  begin
    OrdemStop  := MemTableOrdem.Value;
    CodigoStop := MemTableCodigo.Value;
  end;
  AppMainPF.ExecutaPassosRobot((*Form*)Self,
  CkExecAsAdmin.Checked,
  MemTable,
  MemTableTpAcao,
  MemTableCodigo,
  MemTableOrdem,
  MemTableNome,
  MemTableSegundosA,
  MemTableSegundosD,
  MemTableMousePosX,
  MemTableTexto,
  MemTableMousePosY,
  MemTableMouseEvent,
  MemTableQtdClkSmlt,
  MemTableKeyCode,
  MemTableKeyEvent,
  MemTableShiftState,
  MemTableTipoTecla,
  MemTableTeclEsp,
  MemTableLetra,
  EdPasta.Text,
  EdExecutavel.Text,
  EdParametros.Text,
  RGEstadoJanela.ItemIndex,
  OrdemStop, CodigoStop,
  HProcesso);
  //
  EdHProcesso.ValueVariant := HProcesso;
end;

end.
