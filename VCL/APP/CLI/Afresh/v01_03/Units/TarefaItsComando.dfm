object FmTarefaItsComando: TFmTarefaItsComando
  Left = 0
  Top = 0
  Caption = 'Etapa de Digita'#231#227'o de Texto'
  ClientHeight = 266
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 266
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 20
      Width = 37
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 76
      Top = 20
      Width = 36
      Height = 13
      Caption = 'Ordem:'
    end
    object Label3: TLabel
      Left = 144
      Top = 20
      Width = 96
      Height = 13
      Caption = 'Descri'#231#227'o do passo:'
    end
    object Label4: TLabel
      Left = 12
      Top = 120
      Width = 277
      Height = 13
      Caption = 'Tempo de espera (em segundos) antes do click do mouse:'
    end
    object Label5: TLabel
      Left = 12
      Top = 144
      Width = 267
      Height = 13
      Caption = 'Tempo de espera (em segundos) ap'#243's o click do mouse:'
    end
    object LaTeclEsp: TLabel
      Left = 524
      Top = 72
      Width = 73
      Height = 13
      Caption = 'Tecla especial: '
      Enabled = False
    end
    object LaLetra: TLabel
      Left = 452
      Top = 72
      Width = 64
      Height = 13
      Caption = 'Tecla normal:'
      Enabled = False
    end
    object Label6: TLabel
      Left = 16
      Top = 184
      Width = 322
      Height = 13
      Caption = 'https://www.pcworld.com/article/164234/keyboard_shortcuts.html'
    end
    object Memo1: TMemo
      Left = 400
      Top = 114
      Width = 229
      Height = 59
      Lines.Strings = (
        'Memo1')
      TabOrder = 9
      Visible = False
    end
    object EdCodigo: TdmkEdit
      Left = 8
      Top = 36
      Width = 65
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdOrdem: TdmkEdit
      Left = 76
      Top = 36
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 144
      Top = 36
      Width = 481
      Height = 21
      MaxLength = 255
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 203
      Width = 635
      Height = 63
      Align = alBottom
      TabOrder = 10
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel2: TPanel
        Left = 496
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object EdSegundosA: TdmkEdit
      Left = 296
      Top = 116
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdSegundosD: TdmkEdit
      Left = 296
      Top = 140
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object CGShiftState: TdmkCheckGroup
      Left = 8
      Top = 64
      Width = 213
      Height = 45
      Caption = ' Teclas pressionadas durante a digita'#231#227'o: '
      Columns = 3
      Items.Strings = (
        'Shift'
        'Alt'
        'Ctrl')
      TabOrder = 3
      UpdType = utYes
      Value = 0
      OldValor = 0
    end
    object RGTipoTecla: TdmkRadioGroup
      Left = 228
      Top = 64
      Width = 217
      Height = 45
      Caption = ' Tipo de tecla: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Indefinido'
        'Normal'
        'Especial')
      TabOrder = 4
      OnClick = RGTipoTeclaClick
      UpdType = utYes
      OldValor = 0
    end
    object CBTeclEsp: TComboBox
      Left = 524
      Top = 88
      Width = 101
      Height = 21
      Enabled = False
      TabOrder = 6
      Items.Strings = (
        'CANCEL'
        'BACK'
        'TAB'
        'CLEAR'
        'RETURN'
        'SHIFT'
        'CONTROL'
        'MENU'
        'PAUSE'
        'CAPITAL'
        'ESCAPE'
        'SPACE'
        'PRIOR'
        'NEXT'
        'END'
        'HOME'
        'LEFT'
        'UP'
        'RIGHT'
        'DOWN'
        'SELECT'
        'PRINT'
        'EXECUTE'
        'SNAPSHOT'
        'INSERT'
        'DELETE'
        'HELP'
        'LWIN'
        'RWIN'
        'APPS'
        'SLEEP'
        'NUMPAD0'
        'NUMPAD1'
        'NUMPAD2'
        'NUMPAD3'
        'NUMPAD4'
        'NUMPAD5'
        'NUMPAD6'
        'NUMPAD7'
        'NUMPAD8'
        'NUMPAD9'
        'MULTIPLY'
        'ADD'
        'SEPARATOR'
        'SUBTRACT'
        'DECIMAL'
        'DIVIDE'
        'F1'
        'F2'
        'F3'
        'F4'
        'F5'
        'F6'
        'F7'
        'F8'
        'F9'
        'F10'
        'F11'
        'F12'
        'F13'
        'F14'
        'F15'
        'F16'
        'F17'
        'F18'
        'F19'
        'F20'
        'F21'
        'F22'
        'F23'
        'F24'
        'NUMLOCK'
        'SCROLL'
        'LSHIFT'
        'RSHIFT'
        'LCONTROL'
        'RCONTROL'
        'LMENU'
        'RMENU'
        'BROWSER_BACK'
        'BROWSER_FORWARD'
        'BROWSER_REFRESH'
        'BROWSER_STOP'
        'BROWSER_SEARCH'
        'BROWSER_FAVORITES'
        'BROWSER_HOME'
        'VOLUME_MUTE'
        'VOLUME_DOWN'
        'VOLUME_UP'
        'MEDIA_NEXT_TRACK'
        'MEDIA_PREV_TRACK'
        'MEDIA_STOP'
        'MEDIA_PLAY_PAUSE'
        'LAUNCH_MAIL'
        'LAUNCH_MEDIA_SELECT'
        'LAUNCH_APP1'
        'LAUNCH_APP2'
        'PLAY'
        'ZOOM')
    end
    object EdLetra: TdmkEdit
      Left = 452
      Top = 88
      Width = 69
      Height = 21
      Enabled = False
      MaxLength = 1
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
end
