object FmLicDados: TFmLicDados
  Left = 0
  Top = 0
  Caption = 'Dados do Licenciado'
  ClientHeight = 125
  ClientWidth = 391
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 391
    Height = 68
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 24
      Width = 58
      Height = 13
      Caption = 'CPF / CNPJ:'
    end
    object Label2: TLabel
      Left = 148
      Top = 24
      Width = 40
      Height = 13
      Caption = 'Posi'#231#227'o:'
    end
    object Label3: TLabel
      Left = 192
      Top = 24
      Width = 57
      Height = 13
      Caption = 'C'#243'digo PIN:'
    end
    object EdCPF_CNPJ: TdmkEdit
      Left = 24
      Top = 40
      Width = 120
      Height = 21
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtCPFJ
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPosicao: TdmkEdit
      Left = 148
      Top = 40
      Width = 41
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCodigoPIN: TdmkEdit
      Left = 192
      Top = 40
      Width = 73
      Height = 21
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object Button1: TButton
      Left = 272
      Top = 36
      Width = 113
      Height = 25
      Caption = 'GeraCodigoPin'
      Enabled = False
      TabOrder = 3
      Visible = False
      OnClick = Button1Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 68
    Width = 391
    Height = 57
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Left = 8
      Top = 8
      Width = 120
      Height = 40
      Caption = 'Ativar licen'#231'a'
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtDesiste: TBitBtn
      Left = 260
      Top = 8
      Width = 120
      Height = 40
      Caption = 'Desiste'
      TabOrder = 1
      OnClick = BtDesisteClick
    end
    object BtOpcoesApp: TBitBtn
      Tag = 13
      Left = 135
      Top = 8
      Width = 120
      Height = 40
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Op'#231#245'es'
      NumGlyphs = 2
      TabOrder = 2
    end
  end
end
