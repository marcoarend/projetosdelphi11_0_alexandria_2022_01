unit LicDados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, System.DateUtils,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, dmkEdit, Vcl.ExtCtrls,
  Vcl.ComCtrls, dmkEditDateTimePicker, Vcl.Buttons, IdBaseComponent,
  IdComponent, IdUDPBase, IdUDPClient, IdSNTP, dmkCheckBox;

type
  TFmLicDados = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    EdCPF_CNPJ: TdmkEdit;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    EdPosicao: TdmkEdit;
    Label2: TLabel;
    EdCodigoPIN: TdmkEdit;
    Label3: TLabel;
    Button1: TButton;
    BtOpcoesApp: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FLiberado: Boolean;
    FAtualizadoEm: String;
    FSerialHD: String;
    FCPUID: String;
    FSerialKey: String;

  end;

var
  FmLicDados: TFmLicDados;

implementation

uses UnLicVendaApp_Dmk, dmkGeral, MyListas, UnGrl_Vars, SntpSend, UnProjGroup_Vars;

{$R *.dfm}

procedure TFmLicDados.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLicDados.BtOKClick(Sender: TObject);
var
  CodAplic, Versao: Integer;
  CodigoPin: String;
begin
  VAR_CPF_CNPJ := Geral.SoNumero_TT(EdCPF_CNPJ.ValueVariant);
  if LicVendaApp_Dmk.ObtemCodigoPinDeCnpj(
    EdCPF_CNPJ.Text, // CNPJ do cliente
    EdPosicao.ValueVariant, // Posi��o de 1 a 10. Come�ar com 1 e ir incrementando conforme o cliente solicita
    CodigoPIN) then // CodigoPin - ser� fornecido ao cliente via WhatsApp, email ou outro meio seguro
  begin
    if CodigoPin <> EdCodigoPIN.Text then
    begin
      Geral.MB_Info('C�digo PIN inv�lido!');
      Exit;
    end;
  end else Exit;
  //
  CodAplic  := CO_DMKID_APP;
  Versao    := CO_VERSAO;
  //DataVenda := TPDataVenda.Date;
  //
  if LicVendaApp_Dmk.LiberaUso(VAR_CPF_CNPJ, CodigoPIN, CodAplic, Versao, FAtualizadoEm, FSerialHD,
  FCPUID, FSerialKey) then
  begin
    FLiberado := True;
    Close;
  end else
   Halt(0);
end;

procedure TFmLicDados.Button1Click(Sender: TObject);
var
  CodigoPin: String;
begin
  if LicVendaApp_Dmk.ObtemCodigoPinDeCnpj(
    EdCPF_CNPJ.Text, // CNPJ do cliente
    EdPosicao.ValueVariant, // Posi��o de 1 a 10. Come�ar com 1 e ir incrementando conforme o cliente solicita
    CodigoPIN) then // CodigoPin - ser� fornecido ao cliente via WhatsApp, email ou outro meio seguro
  Geral.MB_Info('C�digo PIN da Posi��o ' + Geral.FF0(EdPosicao.ValueVariant) +
  ' do CNPJ ' + EdCPF_CNPJ.Text + ': ' + sLineBreak + sLineBreak + CodigoPin);
end;

procedure TFmLicDados.FormCreate(Sender: TObject);
begin
  FLiberado     := False;
  FAtualizadoEm := EmptyStr;
end;

end.
