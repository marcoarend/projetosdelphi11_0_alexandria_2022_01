unit UnProjGroup_Consts;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, mysqlDBTables, UnMyLinguas,
  UnInternalConsts, dmkGeral, UnDmkProcFunc,
  UnDmkEnums;

type
  TUnProjGroup_Consts = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
  end;

const
  CO_DIR_APP_DATA  = CO_DIR_RAIZ_DMK + '\Afresh\Data\';
  CO_DBArqName     = CO_DIR_APP_DATA + 'TarefaUnica.json';
  CO_DBCriptArqName = CO_DIR_APP_DATA + 'TarefaUnica.dmkafr';
  //
var
  ProjGroup_Consts: TUnProjGroup_Consts;

implementation

end.
