unit FormTeste;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TFmFormTeste = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmFormTeste: TFmFormTeste;

implementation

{$R *.dfm}

procedure TFmFormTeste.Button1Click(Sender: TObject);
begin
  ShowMessage('Usuario: ' + Edit1.Text + sLineBreak +
  'Senha:' + Edit2.Text);
end;

end.
