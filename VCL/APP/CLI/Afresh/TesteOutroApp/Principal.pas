unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TFmPrincipal = class(TForm)
    Button1: TButton;
    procedure FormClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses FormTeste;

{$R *.dfm}

procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
  Application.CreateForm(TFmFormTeste, FmFormTeste);
  FmFormTeste.ShowModal;
  FmFormTeste.Destroy;
end;

procedure TFmPrincipal.FormClick(Sender: TObject);
var
  Pt: TPoint;
begin
  GetCursorPos(Pt);
  //ShowMessage(Application.Name + '.TFmPrincipal.FormClick() >>  X = ' +
  //IntToStr(Pt.X) + '  Y = ' + IntToStr(Pt.Y));
end;

end.
