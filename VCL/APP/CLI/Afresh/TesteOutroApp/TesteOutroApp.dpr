program TesteOutroApp;

uses
  Vcl.Forms,
  Principal in 'Principal.pas' {FmPrincipal},
  FormTeste in 'FormTeste.pas' {FmFormTeste};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Name := 'TesteOutroApp';
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmFormTeste, FmFormTeste);
  Application.Run;
end.
