program Volver;

uses
  Vcl.Forms,
  Principal in 'Principal.pas' {FmPrincipal},
  UnProjGroup_PF in '..\Units\UnProjGroup_PF.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnLicVendaApp_Dmk in '..\..\..\..\..\UTL\_UNT\v01_01\UnLicVendaApp_Dmk.pas',
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnGrl_DmkREST in '..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_DmkREST.pas',
  UnDmkHTML2 in '..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  WBFuncs in '..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  UnMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  Windows_FMX_ProcFunc in '..\..\..\..\..\..\FMX\UTL\_UNT\v01_01\Windows\Windows_FMX_ProcFunc.pas',
  MyListas in '..\Units\MyListas.pas',
  UnMyJSON in '..\..\..\..\..\UTL\JSON\UnMyJSON.pas',
  uLkJSON in '..\..\..\..\..\UTL\JSON\uLkJSON.pas',
  UnDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  LicDados in '..\Units\LicDados.pas' {FmLicDados},
  Dmk_Afresh in '..\Units\Dmk_Afresh.pas' {FmDmk_Afresh},
  OpcoesApp in '..\Units\OpcoesApp.pas' {FmOpcoesApp},
  UnMyVclEvents in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyVclEvents.pas',
  UnProjGroup_Vars in '..\Projeto\UnProjGroup_Vars.pas',
  UnRobotPF in '..\ProjGroup\UnRobotPF.pas',
  UnProjGroup_Consts in '..\ProjGroup\UnProjGroup_Consts.pas',
  UnProjGroupEnums in '..\ProjGroup\UnProjGroupEnums.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
