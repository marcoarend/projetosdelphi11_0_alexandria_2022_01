unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  Datasnap.DBClient, DCPcrypt2, DCPblockciphers, DCPtwofish, dmkGeral,
  UnProjGroup_Vars, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFmPrincipal = class(TForm)
    MemTable: TClientDataSet;
    MemTableTpAcao: TIntegerField;
    MemTableCodigo: TIntegerField;
    MemTableOrdem: TIntegerField;
    MemTableNome: TStringField;
    MemTableSegundosA: TFloatField;
    MemTableSegundosD: TFloatField;
    MemTableMousePosX: TIntegerField;
    MemTableTexto: TStringField;
    MemTableMousePosY: TIntegerField;
    MemTableMouseEvent: TIntegerField;
    MemTableQtdClkSmlt: TIntegerField;
    MemTableKeyCode: TIntegerField;
    MemTableKeyEvent: TIntegerField;
    MemTableShiftState: TIntegerField;
    MemTableTipoTecla: TIntegerField;
    MemTableTeclEsp: TIntegerField;
    MemTableLetra: TStringField;
    DCP_twofish1: TDCP_twofish;
    MeLog: TMemo;
    TmExecuta: TTimer;
    TrayIcon1: TTrayIcon;
    TmTrayIcon: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure TmExecutaTimer(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TmTrayIconTimer(Sender: TObject);
  private
    { Private declarations }
    procedure CarregaEAtiva();
    //
    procedure LogaCarregaEATiva();
    function  ConnectAs(const lpszUsername, lpszPassword: string): Boolean;
    //
    procedure AtivaTarefa();
    function CarregaTarefa(): Boolean;
    //
    procedure ExibeTrayIcon();
  public
    { Public declarations }
    FShowed, FBtAtivado: Boolean;
    //
    FNome, FPasta, FExecutavel, FParametros: String;
    FMonitSohProprioProces,
    FParaEtapaOnDeactive, FExecAsAdmin, FLogInMemo, FExeOnIni: Boolean;
    FSegWaitOnOSReinit, FSegWaitScanMonit,
    FEstadoJanela, FMaiorCodigo, FHProcesso: Integer;
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

{$R *.dfm}

uses
  UnProjGroup_PF, UnRobotPF, UnMyVclEvents;

procedure TFmPrincipal.AtivaTarefa();
{
  procedure ExecutaEspera();
    function T(): Integer;
    begin
     Result := GetTickCount div 1000;
    end;
  var
    SegWait, Espera: Integer;
  begin
    SegWait:= FSegWaitOnOSReinit;
    if T() < SegWait then
    begin
      while T() <  SegWait do
      begin
        if FBtAtivado = False then Exit;
        //
        EdTempoAtividade.Text := AppPF.UpTime();
        Application.ProcessMessages();
        Sleep(1000);
      end;
    end else
    begin
      //SegWait := 2; // testar
      SegWait := 400; // 400 * 50 = 20000 (20 segundos)
      Espera  := 0;
      while Espera < SegWait do
      begin
        if FBtAtivado = False then Exit;
        //
        Application.ProcessMessages();
        Sleep(50);
        Espera := Espera + 1;
      end;
    end;
    Application.ProcessMessages();
  end;
}
var
  OrdemStop, CodigoStop: Integer;
  Agora: TDateTime;
begin
(*&�%$#@!
  if (FBtAtivado = False) or (Executa) then
  begin
    //////////// Verifica se processo est� ativo! ////////////////////////////////
    // Procura pelo hProcess conhecido caso exigido pelo usu�rio desta forma!
    if FMonitSohProprioProces  then
    begin
      if MyVclEvents.FindProcessesByHProcess(FhProcesso) then Exit;  // achou!
    end else
    begin
      // Localiza pelo nome!
*)
      if MyVclEvents.FindProcessesByName(FExecutavel) >= 0 then Exit; //Achou!
(*&�%$#@!
    end;

    BtAtiva.Caption := '&Desativa';
    FBtAtivado := True;
    //
    OrdemStop  := High(Integer);
    CodigoStop := High(Integer);
    //
    Application.ProcessMessages();
    if Aguarda then
      ExecutaEspera();
    //
    if FBtATivado = False then Exit;
    //
*)
    Agora := Now();
    if RobotPF.ExecutaPassosRobot((*Form*)Self,
    FExecAsAdmin,
    MemTable,
    MemTableTpAcao,
    MemTableCodigo,
    MemTableOrdem,
    MemTableNome,
    MemTableSegundosA,
    MemTableSegundosD,
    MemTableMousePosX,
    MemTableTexto,
    MemTableMousePosY,
    MemTableMouseEvent,
    MemTableQtdClkSmlt,
    MemTableKeyCode,
    MemTableKeyEvent,
    MemTableShiftState,
    MemTableTipoTecla,
    MemTableTeclEsp,
    MemTableLetra,
    FPasta,
    FExecutavel,
    FParametros,
    FEstadoJanela,
    OrdemStop, CodigoStop,
    FHProcesso) then
      ProjGroup_PF.InfoLog(MeLog, Agora, 'Processo restaurado.')
    else
      ProjGroup_PF.InfoLog(MeLog, Agora, 'N�o foi poss�vel restaurar o processo!')
    (*&�%$#@!
  end else
  begin
    FBtAtivado := False;
    BtAtiva.Caption := '&Ativa';
    //
  end;
    *)
end;

procedure TFmPrincipal.CarregaEAtiva();
begin
  TmExecuta.Enabled := False;
  //
  FHProcesso := 0;
  FBtAtivado := True;
  if CarregaTarefa() then
    AtivaTarefa();
end;

function TFmPrincipal.CarregaTarefa(): Boolean;
var
  Continua: Boolean;
begin
  Result := False;
  (*&�%$#@!
  MyObjects.CorIniComponente;
  *)
  Continua := False;
  //
  (*&�%$#@!
  if FAtivou = False then
  begin
    FAtivou := True;
  *)




    Continua := True;
////////////////////////////////////////////////////////////////////////////////
{
    if ProjGroup_PF.ObtemDadosOpcoesAppDeArquivo(
      ProjGroup_PF.ObtemNomeArquivoOpcoes(), DCP_twofish1) then
    begin
      if ProjGroup_PF.LiberaUsoVendaApp1(DCP_twofish1(*, FApp User Pwd, FExe On Ini*)) then
      begin
        // Liberar tudo por aqui!
        if (VAR_SENHA_BOSS <> EmptyStr) and (VAR_ExigeSenhaLoginApp) then
          Continua := ProjGroup_PF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
        else
          Continua := True;
      end else
      begin
        Geral.MB_Info('N�O Liberado!');
        Halt(0);
      end;
    end;
  (*&�%$#@!
  end else
  begin
    Continua := True;
  end;
  *)
}
////////////////////////////////////////////////////////////////////////////////
///



  //
  if Continua then
  begin
    RobotPF.CriaEcarregaTabela(
    FmPrincipal.DCP_twofish1,
    MemTable,
    MemTableTpAcao,
    MemTableCodigo,
    MemTableOrdem,
    MemTableNome,
    MemTableSegundosA,
    MemTableSegundosD,
    MemTableMousePosX,
    MemTableTexto,
    MemTableMousePosY,
    MemTableMouseEvent,
    MemTableQtdClkSmlt,
    MemTableKeyCode,
    MemTableKeyEvent,
    MemTableShiftState,
    MemTableTipoTecla,
    MemTableTeclEsp,
    MemTableLetra,
    FNome, FPasta, FExecutavel, FParametros, FMonitSohProprioProces,
    FParaEtapaOnDeactive, FExecAsAdmin, FLogInMemo, FExeOnIni,
    FSegWaitOnOSReinit, FSegWaitScanMonit,
    FEstadoJanela, FMaiorCodigo);
(*&�%$#@!
    TmMonitora.Interval := FSegWaitScanMonit * 1000;
    TmOculta.Enabled := True;
    //if ????? then
    begin
      TrayIcon1.Visible := True;
      //
    end;
*)
    Result := True;
  end;
end;

function TFmPrincipal.ConnectAs(const lpszUsername,
  lpszPassword: string): Boolean;
var
  hToken: THandle;
begin
  Result := LogonUser(PChar(lpszUsername), nil, PChar(lpszPassword), LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, hToken);
  if Result then
    Result := ImpersonateLoggedOnUser(hToken)
  else
    RaiseLastOSError;
end;

procedure TFmPrincipal.ExibeTrayIcon;
begin
  TrayIcon1.Visible := True;
  Application.MainFormOnTaskBar := FShowed;
  FmPrincipal.Hide;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TrayIcon1 <> nil then
    TrayIcon1.Visible := False;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  FShowed        := True;
  //CarregaEATiva();
  TmExecuta.Enabled := True;
end;

procedure TFmPrincipal.LogaCarregaEATiva();
var
  Admin, Password: String;
begin
  //
  Admin    := 'Dermatek';
  Password := 'N4yr2019@';
(*
  Admin    := 'Teste';
  Password := '2002';
*)
  try
   ConnectAs(Admin, Password);
   //do something here

   CarregaEATiva();
   MeLog.Lines.Add('Executado!');


   //terminates the impersonation
   RevertToSelf;

  except
    on E: Exception do
     MeLog.Lines.Add(E.ClassName + ': ' + E.Message);
  end;
end;

procedure TFmPrincipal.TmExecutaTimer(Sender: TObject);
begin
  //CarregaEAtiva();
  LogaCarregaEATiva();
end;

procedure TFmPrincipal.TmTrayIconTimer(Sender: TObject);
begin
  TmTrayIcon.Enabled := False;
  //
  ExibeTrayIcon();
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

end.
