unit TarefaItsTexto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, dmkEdit,
  Vcl.Buttons, dmkRadioGroup;

type
  TFmTarefaItsTexto = class(TForm)
    Panel1: TPanel;
    Memo1: TMemo;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    EdOrdem: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    EdSegundosA: TdmkEdit;
    EdSegundosD: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdTexto: TdmkEdit;
    Label6: TLabel;
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmTarefaItsTexto: TFmTarefaItsTexto;

implementation

uses dmkGeral, UnAppEnums, UnAppPF, Principal, Tarefa;

{$R *.dfm}

procedure TFmTarefaItsTexto.BtConfirmaClick(Sender: TObject);
var
  Codigo, Ordem, TpAcao: Integer;
  Nome, Texto: String;
  SegundosA, SegundosD: Double;
begin
  TpAcao     := Integer(TTpAcaoInputEvent.taieTexto); // 3
  Codigo     := EdCodigo.ValueVariant;
  Ordem      := EdOrdem.ValueVariant - 1;
  Texto      := EdTexto.ValueVariant;
  Nome       := EdNome.ValueVariant;
  SegundosA  := EdSegundosA.ValueVariant;
  SegundosD  := EdSegundosD.ValueVariant;
  //
  if Codigo = 0 then
  begin
    FmTarefa.FMaiorCodigo := FmTarefa.FMaiorCodigo + 1;
    Codigo := FmTarefa.FMaiorCodigo;
    FmTarefa.MemTable.Append;
  end else
  begin
    FmTarefa.MemTable.Edit;
  end;
  FmTarefa.MemTableCodigo.Value     := Codigo;
  FmTarefa.MemTableNome.Value       := Nome;
  FmTarefa.MemTableOrdem.Value      := Ordem;
  FmTarefa.MemTableTpAcao.Value     := TpAcao;

  FmTarefa.MemTableTexto.Value      := Texto;

  FmTarefa.MemTableSegundosA.Value  := SegundosA;
  FmTarefa.MemTableSegundosD.Value  := SegundosD;
  //
  FmTarefa.MemTable.Post;
  Application.ProcessMessages;
  //FmTarefa.MemTable.Refresh;
  //
  FmTarefa.Reordena();
  FmTarefa.Salva();
  //
  Close;
end;

procedure TFmTarefaItsTexto.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

end.
