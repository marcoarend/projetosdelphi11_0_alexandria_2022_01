
{***********************************************************************************************************************************}
{                                                                                                                                   }
{                                                         XML Data Binding                                                          }
{                                                                                                                                   }
{         Generated on: 10/02/2020 12:49:08                                                                                         }
{       Generated from: C:\_Compilers\ProjetosDelphi10_2_2_Tokyo\VCL\APP\CLI\Afresh\v01_01\Units\Schemas\Layout_Tarefas_v0.01.xsd   }
{   Settings stored in: C:\_Compilers\ProjetosDelphi10_2_2_Tokyo\VCL\APP\CLI\Afresh\v01_01\Units\Schemas\Layout_Tarefas_v0.01.xdb   }
{                                                                                                                                   }
{***********************************************************************************************************************************}

unit Layout_Tarefas_v001;

interface

uses Xml.xmldom, Xml.XMLDoc, Xml.XMLIntf;

type

{ Forward Decls }

  IXMLTTarefas = interface;
  IXMLTTarefas_Tarefas = interface;
  IXMLTTarefa = interface;

{ IXMLTTarefas }

  IXMLTTarefas = interface(IXMLNode)
    ['{B838B3C2-9F0F-4165-9719-4C979EFB58AD}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_Tarefas: IXMLTTarefas_Tarefas;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property Tarefas: IXMLTTarefas_Tarefas read Get_Tarefas;
  end;

{ IXMLTTarefas_Tarefas }

  IXMLTTarefas_Tarefas = interface(IXMLNode)
    ['{EDB5CFA3-D49D-441D-AC39-69B73F702F1A}']
    { Property Accessors }
    function Get_Cabecalho: IXMLTTarefa;
    { Methods & Properties }
    property Cabecalho: IXMLTTarefa read Get_Cabecalho;
  end;

{ IXMLTTarefa }

  IXMLTTarefa = interface(IXMLNode)
    ['{5A499590-C6A6-4276-9511-58B35CED8ED4}']
    { Property Accessors }
    function Get_Codigo: UnicodeString;
    function Get_Nome: UnicodeString;
    function Get_LinCom: UnicodeString;
    function Get_NomExec: UnicodeString;
    function Get_MyID: UnicodeString;
    procedure Set_Codigo(Value: UnicodeString);
    procedure Set_Nome(Value: UnicodeString);
    procedure Set_LinCom(Value: UnicodeString);
    procedure Set_NomExec(Value: UnicodeString);
    procedure Set_MyID(Value: UnicodeString);
    { Methods & Properties }
    property Codigo: UnicodeString read Get_Codigo write Set_Codigo;
    property Nome: UnicodeString read Get_Nome write Set_Nome;
    property LinCom: UnicodeString read Get_LinCom write Set_LinCom;
    property NomExec: UnicodeString read Get_NomExec write Set_NomExec;
    property MyID: UnicodeString read Get_MyID write Set_MyID;
  end;

{ Forward Decls }

  TXMLTTarefas = class;
  TXMLTTarefas_Tarefas = class;
  TXMLTTarefa = class;

{ TXMLTTarefas }

  TXMLTTarefas = class(TXMLNode, IXMLTTarefas)
  protected
    { IXMLTTarefas }
    function Get_Versao: UnicodeString;
    function Get_Tarefas: IXMLTTarefas_Tarefas;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTTarefas_Tarefas }

  TXMLTTarefas_Tarefas = class(TXMLNode, IXMLTTarefas_Tarefas)
  protected
    { IXMLTTarefas_Tarefas }
    function Get_Cabecalho: IXMLTTarefa;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTTarefa }

  TXMLTTarefa = class(TXMLNode, IXMLTTarefa)
  protected
    { IXMLTTarefa }
    function Get_Codigo: UnicodeString;
    function Get_Nome: UnicodeString;
    function Get_LinCom: UnicodeString;
    function Get_NomExec: UnicodeString;
    function Get_MyID: UnicodeString;
    procedure Set_Codigo(Value: UnicodeString);
    procedure Set_Nome(Value: UnicodeString);
    procedure Set_LinCom(Value: UnicodeString);
    procedure Set_NomExec(Value: UnicodeString);
    procedure Set_MyID(Value: UnicodeString);
  end;

{ Global Functions }

function GetTarefas(Doc: IXMLDocument): IXMLTTarefas;
function LoadTarefas(const FileName: string): IXMLTTarefas;
function NewTarefas: IXMLTTarefas;

const
  TargetNamespace = 'http://www.dermatek.com.br/www/apps/Afresh';

implementation

uses Xml.xmlutil;

{ Global Functions }

function GetTarefas(Doc: IXMLDocument): IXMLTTarefas;
begin
  Result := Doc.GetDocBinding('Tarefas', TXMLTTarefas, TargetNamespace) as IXMLTTarefas;
end;

function LoadTarefas(const FileName: string): IXMLTTarefas;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Tarefas', TXMLTTarefas, TargetNamespace) as IXMLTTarefas;
end;

function NewTarefas: IXMLTTarefas;
begin
  Result := NewXMLDocument.GetDocBinding('Tarefas', TXMLTTarefas, TargetNamespace) as IXMLTTarefas;
end;

{ TXMLTTarefas }

procedure TXMLTTarefas.AfterConstruction;
begin
  RegisterChildNode('Tarefas', TXMLTTarefas_Tarefas);
  inherited;
end;

function TXMLTTarefas.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTTarefas.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTTarefas.Get_Tarefas: IXMLTTarefas_Tarefas;
begin
  Result := ChildNodes['Tarefas'] as IXMLTTarefas_Tarefas;
end;

{ TXMLTTarefas_Tarefas }

procedure TXMLTTarefas_Tarefas.AfterConstruction;
begin
  RegisterChildNode('Cabecalho', TXMLTTarefa);
  inherited;
end;

function TXMLTTarefas_Tarefas.Get_Cabecalho: IXMLTTarefa;
begin
  Result := ChildNodes['Cabecalho'] as IXMLTTarefa;
end;

{ TXMLTTarefa }

function TXMLTTarefa.Get_Codigo: UnicodeString;
begin
  Result := ChildNodes['Codigo'].Text;
end;

procedure TXMLTTarefa.Set_Codigo(Value: UnicodeString);
begin
  ChildNodes['Codigo'].NodeValue := Value;
end;

function TXMLTTarefa.Get_Nome: UnicodeString;
begin
  Result := ChildNodes['Nome'].Text;
end;

procedure TXMLTTarefa.Set_Nome(Value: UnicodeString);
begin
  ChildNodes['Nome'].NodeValue := Value;
end;

function TXMLTTarefa.Get_LinCom: UnicodeString;
begin
  Result := ChildNodes['LinCom'].Text;
end;

procedure TXMLTTarefa.Set_LinCom(Value: UnicodeString);
begin
  ChildNodes['LinCom'].NodeValue := Value;
end;

function TXMLTTarefa.Get_NomExec: UnicodeString;
begin
  Result := ChildNodes['NomExec'].Text;
end;

procedure TXMLTTarefa.Set_NomExec(Value: UnicodeString);
begin
  ChildNodes['NomExec'].NodeValue := Value;
end;

function TXMLTTarefa.Get_MyID: UnicodeString;
begin
  Result := ChildNodes['MyID'].Text;
end;

procedure TXMLTTarefa.Set_MyID(Value: UnicodeString);
begin
  ChildNodes['MyID'].NodeValue := Value;
end;

end.