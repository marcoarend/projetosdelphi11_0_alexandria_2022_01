unit Teste;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, dmkEdit,
  Vcl.ExtCtrls;

type
  TFmTeste = class(TForm)
    Timer1: TTimer;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    EdY1: TdmkEdit;
    EdX1: TdmkEdit;
    Label3: TLabel;
    EdX2: TdmkEdit;
    EdY2: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdTexto1: TdmkEdit;
    Button1: TButton;
    EdTexto2: TdmkEdit;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Label6: TLabel;
    EdIni: TdmkEdit;
    Label7: TLabel;
    EdFim: TdmkEdit;
    CkShift: TCheckBox;
    Timer2: TTimer;
    Memo2: TMemo;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Timer1Timer(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private declarations }
    FTypyng: Boolean;

  public
    { Public declarations }
  end;

var
  FmTeste: TFmTeste;

implementation

uses
  UnMyVclEvents, dmkGeral, KeysU;

{$R *.dfm}

procedure TFmTeste.BitBtn1Click(Sender: TObject);
begin
  MyVclEvents.SimulaMouseClickXY(960, 540, TMyMouseClickSouce.mmcsLeft);
end;

procedure TFmTeste.Button1Click(Sender: TObject);
var
  I: Integer;
  Texto: String;
  s: String;
  o, v0, v1: Word;
  us, s0: Boolean;
begin
  //Memo1.Lines.Clear;
  Texto := EdTexto1.ValueVariant;
  //EdTexto2.SetFocus;
  for I := 1 to Length(Texto) do
  begin
    v0 := 0;
    V1 := 1;
    //Memo1.Lines.Add(IntToStr(I) + ' - ');
    s := Texto[I];
    if s = ' ' then begin v1 := 32; us := False; end else
    if s = '(' then begin v1 := 57; us := True; end else
    if s = '*' then begin v1 := 56; us := True; end else
    if s = '&' then begin v1 := 55; us := True; end else
    if s = '�' then begin v1 := 54; us := True; end else
    if s = '%' then begin v1 := 53; us := True; end else  // Acentua!
    if s = '$' then begin v1 := 52; us := True; end else
    if s = '#' then begin v1 := 51; us := True; end else
    if s = '@' then begin v1 := 50; us := True; end else
    if s = '!' then begin v1 := 49; us := True; end else
    if s = ')' then begin v1 := 48; us := True; end else
    // Num Pad
    if s = '/' then begin v1 := 111; us := False; end else
    if s = '.' then begin v1 := 110; us := False; end else
    if s = '-' then begin v1 := 109; us := False; end else
    //if s = ' ' then begin v1 := 108; us := False; end else
    if s = '+' then begin v1 := 107; us := False; end else
    if s = '*' then begin v1 := 106; us := False; end else


(*    // NumLock -> 144
112: Coloca(' [F1] ');
113: Coloca(' [F2] ');
114: Coloca(' [F3] ');
115: Coloca(' [F4] ');
116: Coloca(' [F5] ');
117: Coloca(' [F6] ');
118: Coloca(' [F7] ');
119: Coloca(' [F8] ');
120: Coloca(' [F9] ');
121: Coloca(' [F10] ');
122: Coloca(' [F11] ');
123: Coloca(' [F12] ');
144: Coloca(' [NUMLCK] ');
*)

    if s = '.' then begin v1 := 194; us := True; end else
    if s = '/' then begin v1 := 193; us := True; end else
    if s = '''' then begin v1 := 192; us := True; end else
    if s = ';' then begin v1 := 191; us := False; end else
    if s = '.' then begin v1 := 190; us := False; end else
    if s = '-' then begin v1 := 189; us := False; end else
    if s = ',' then begin v1 := 188; us := False; end else
    if s = '=' then begin v1 := 187; us := False; end else
    if s = '�' then begin v1 := 186; us := False; end else

    if s = '.' then begin v1 := 194; us := True; end else
    if s = '?' then begin v1 := 193; us := True; end else
    if s = '"' then begin v1 := 192; us := True; end else
    if s = ':' then begin v1 := 191; us := True; end else
    if s = '>' then begin v1 := 190; us := True; end else
    if s = '_' then begin v1 := 189; us := True; end else
    if s = '<' then begin v1 := 188; us := True; end else
    if s = '+' then begin v1 := 187; us := True; end else
    if s = '�' then begin v1 := 186; us := True; end else

    //219 + Shift = crase
    if (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�') then
    begin
      v0 := 219;
      s0 := True;
      if (s = '�') then begin v1 := 65; us := False; end else
      if (s = '�') then begin v1 := 69; us := False; end else
      if (s = '�') then begin v1 := 73; us := False; end else
      if (s = '�') then begin v1 := 79; us := False; end else
      if (s = '�') then begin v1 := 85; us := False; end else
      if (s = '�') then begin v1 := 65; us := True; end else
      if (s = '�') then begin v1 := 69; us := True; end else
      if (s = '�') then begin v1 := 73; us := True; end else
      if (s = '�') then begin v1 := 79; us := True; end else
      if (s = '�') then begin v1 := 85; us := True; end;
    end
    else
    //219 = acento agudo
    if (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�') then
    begin
      v0 := 219;
      s0 := False;
      if (s = '�') then begin v1 := 65; us := False; end else
      if (s = '�') then begin v1 := 69; us := False; end else
      if (s = '�') then begin v1 := 73; us := False; end else
      if (s = '�') then begin v1 := 79; us := False; end else
      if (s = '�') then begin v1 := 85; us := False; end else
      if (s = '�') then begin v1 := 65; us := True; end else
      if (s = '�') then begin v1 := 69; us := True; end else
      if (s = '�') then begin v1 := 73; us := True; end else
      if (s = '�') then begin v1 := 79; us := True; end else
      if (s = '�') then begin v1 := 85; us := True; end;
    end
    else
    //222 + Shift = ^
    if (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�') then
    begin
      v0 := 222;
      s0 := True;
      if (s = '�') then begin v1 := 65; us := False; end else
      if (s = '�') then begin v1 := 69; us := False; end else
      if (s = '�') then begin v1 := 73; us := False; end else
      if (s = '�') then begin v1 := 79; us := False; end else
      if (s = '�') then begin v1 := 85; us := False; end else
      if (s = '�') then begin v1 := 65; us := True; end else
      if (s = '�') then begin v1 := 69; us := True; end else
      if (s = '�') then begin v1 := 73; us := True; end else
      if (s = '�') then begin v1 := 79; us := True; end else
      if (s = '�') then begin v1 := 85; us := True; end;
    end
    else
    //222  =
    if (s = '�')
    or (s = '�')
    or (s = '�')
    or (s = '�') then
    begin
      v0 := 222;
      s0 := False;
      if (s = '�') then begin v1 := 65; us := False; end else
      if (s = '�') then begin v1 := 79; us := False; end else
      if (s = '�') then begin v1 := 65; us := True; end else
      if (s = '�') then begin v1 := 79; us := True; end;
    end
    else
    if s = '[' then begin v1 := 221; us := False; end else
    if s = ']' then begin v1 := 220; us := False; end else
    if s = '{' then begin v1 := 221; us := True; end else
    if s = '}' then begin v1 := 220; us := True; end else


    if s = '\' then begin v1 := 226; us := False; end else
    if s = '|' then begin v1 := 226; us := True; end else


    //if s = '' then begin v1 := ; us := True; end else
    begin
      o := Ord(s[1]); //Ord(s);
      case o of
        // 96..105
        48..57  : begin v1 := o + 48; us := False; end; // 96.. (0 a 9)
        65..90  : begin v1 := o + 00; us :=  True; end; // MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(o, [ssShift], False);  // Uppercase
        97..122 : begin v1 := o - 32; us :=  False; end; // MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(o-32, [], False);     // Lowercase
      end;
    end;
    if v0 > 0 then
    begin
      if s0 then
        MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(v0, [ssShift], False)
      else
        MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(v0, [], False);
    end;
    //
    if us then
      MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(v1, [ssShift], False)
    else
      MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(v1, [], False);
    //
    Sleep(100);
    Application.ProcessMessages();
    //Memo1.Text := Memo1.Text + sLineBreak;
  end;
end;

procedure TFmTeste.Button2Click(Sender: TObject);
const
  NumPads: array [0..9] of Word = (VK_NUMPAD0, VK_NUMPAD1, VK_NUMPAD2, VK_NUMPAD3, VK_NUMPAD4, VK_NUMPAD5, VK_NUMPAD6, VK_NUMPAD7, VK_NUMPAD8, VK_NUMPAD9);
var
  I, n1, n2, n3: Integer;
  Texto, Nums: String;
  k1, k2, k3: Word;
begin
  EdTexto2.SetFocus;
  Texto := EdTexto1.ValueVariant;
  for I := 1 to Length(Texto) do
  begin
    Nums := IntToStr(Ord(Texto[I]));
    while Length(Nums) < 3 do
      Nums := '0' + Nums;
    n1 := StrToInt(Nums[1]);
    n2 := StrToInt(Nums[2]);
    n3 := StrToInt(Nums[3]);
    //
    k1 := NumPads[n1];
    k2 := NumPads[n2];
    k3 := NumPads[n3];
    //MyVclEvents.SimulaKeyBoardPress_0_PostKeyEx32Arr([VK_NUMPAD1, VK_NUMPAD6, VK_NUMPAD7],[ssAlt], False);
    MyVclEvents.SimulaKeyBoardPress_0_PostKeyEx32Arr([k1, k2, k3],[ssAlt], False);
    Sleep(80);
    Application.ProcessMessages;
  end;
end;

procedure TFmTeste.Button3Click(Sender: TObject);
{
function VKeytoWideString (Key : Word) : WideString;
var
  WBuff         : array [0..255] of WideChar;
  KeyboardState : TKeyboardState;
  UResult       : Integer;
begin
  Result := '';
  GetKeyBoardState (KeyboardState);
  ZeroMemory(@WBuff[0], SizeOf(WBuff));
  UResult := ToUnicode(key, MapVirtualKey(key, 0), KeyboardState, WBuff, Length(WBuff), 0);
  if UResult > 0 then
    SetString(Result, WBuff, UResult)
  else if UResult = -1 then
    Result := WBuff;
end;
}
var
    I: Integer;
begin
  FmTeste.WindowState := wsMaximized;
  Memo1.Setfocus;
  Memo1.Lines.Clear;
  for I := EdIni.ValueVariant to EdFIm.ValueVariant do
  begin
    Memo1.Text :=  ' - ' + IntToStr(I) + Memo1.Text;
    //MyVclEvents.SimulaMouseClickXY(350, 1005, TMyMouseClickSouce.mmcsLeft);
    if CkShift.Checked then
      MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(I, [ssShift], False)
    else
      MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(I, [], False);
    Application.ProcessMessages;
    //sleep(1000);
    Memo1.Text := sLineBreak + Memo1.Text;
    //sleep(1000);
  end;
end;

procedure TFmTeste.Button4Click(Sender: TObject);
begin
  MyVclEvents.SimulaMouseClickXY(640, 340, TMyMouseClickSouce.mmcsLeft);
  Application.ProcessMessages;
  Sleep(300);
  Button1Click(Self);
end;

procedure TFmTeste.FormClick(Sender: TObject);
begin
//  ShowMessage('Click!');
end;

procedure TFmTeste.FormCreate(Sender: TObject);
begin
  FTypyng := False;
end;

procedure TFmTeste.FormKeyPress(Sender: TObject; var Key: Char);
var
  X, Y, I: Integer;
  Texto: WideString;
  s: WideChar;
  o: Word;
begin
{
  if FTypyng then Exit;

  X := EdX2.ValueVariant;
  Y := EdX2.ValueVariant;                       12
  //
  if (Key = 'c') or (Key = 'C') then
  begin
    FTypyng := True;

    MyVclEvents.SimulaMouseClickXY(X, Y, TMyMouseClickSouce.mmcsLeft);
    //SendKeys('Teste');
    Texto := EdTexto1.ValueVariant;
    for I := 1 to Length(Texto) do
    begin
      s := Texto[I];
      o := Ord(s);
      keybd_event(o, 0, 0, 0);
      Sleep(100);
      Application.ProcessMessages();
      //MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(o, [], False);
    end;
    FTypyng := False;
  end;
  if (Key = 'e') or (Key = 'E') then
  begin
    //MyVclEvents.SimulaKeyBoardPress_1_PostKeyEx32(Ord('D'), [], False);
    SendKeys('Dasdfg');
  end;
}
end;

procedure TFmTeste.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  EdX1.ValueVariant := Geral.FF0(X);
  EdY1.ValueVariant := Geral.FF0(Y);
end;

procedure TFmTeste.Timer1Timer(Sender: TObject);
var
  P: TPoint;
begin
  GetCursorPos(P);
  EdX2.ValueVariant := Geral.FF0(P.X);
  EdY2.ValueVariant := Geral.FF0(P.Y);
end;

procedure TFmTeste.Timer2Timer(Sender: TObject);
var
  keyloop, KeyResult : Integer;
begin
  keyloop := 0;
  repeat
    KeyResult := GetAsyncKeyState(keyloop);
    if KeyResult = -32767 then
      Memo2.Text := Memo2.Text + IntToStr(KeyLoop);
(*
    begin
      case keyloop of
      8: Coloca(' [BACKSPACE] ');
      9: Coloca(' [TAB] ');
      12: Coloca(' [ALT] ');
      13: Coloca(' [ENTER] ');
      16: Coloca(' [SHIFT] ');
      17: Coloca(' [CONTROL] ');
      18: Coloca(' [ALT] ');
      20: Coloca(' [CAPS LOCK] ');
      21: Coloca(' [PAGE UP] ');
      27: Coloca(' [ESC] ');
      33: Coloca(' [PAGE UP] ');
      34: Coloca(' [PAGE DOWN] ');
      35: Coloca(' [END] ');
      36: Coloca(' [HOME] ');
      37: Coloca(' [SETA ESQUERDA] ');
      38: Coloca(' [SETA ACIMA] ');
      39: Coloca(' [SETA DIREITA] ');
      40: Coloca(' [SETA ABAIXO] ');
      45: Coloca(' [INSERT] ');
      46: Coloca(' [DEL] '); 91: Coloca(' [WIN ESQUERDA] '); 92: Coloca(' [WIN DIREITA] '); 93: Coloca(' [MENU POP-UP] ');
      96: Coloca('0'); 97: Coloca('1'); 98: Coloca('2'); 99: Coloca('3'); 100: Coloca('4'); 101: Coloca('5'); 102: Coloca('6');
      103: Coloca('7'); 104: Coloca('8'); 105: Coloca('9'); 106: Coloca(' [NUM *] '); 107: Coloca(' [NUM +] '); 109: Coloca(' [NUM -] ');
      110: Coloca(' [NUM SEP. DECIMAL] '); 111: Coloca(' [NUM /] '); 112: Coloca(' [F1] '); 113: Coloca(' [F2] '); 114: Coloca(' [F3] ');
      115: Coloca(' [F4] '); 116: Coloca(' [F5] '); 117: Coloca(' [F6] '); 118: Coloca(' [F7] '); 119: Coloca(' [F8] '); 120: Coloca(' [F9] ');
      121: Coloca(' [F10] '); 122: Coloca(' [F11] '); 123: Coloca(' [F12] '); 144: Coloca(' [NUM LOCK] '); 186: Coloca('�'); 187: Coloca('=');
      188: Coloca(','); 189: Coloca('-'); 190: Coloca('.'); 191: Coloca(';'); 192: Coloca(' [AP�STROFO] ');
      193: Coloca('/'); 194: Coloca(' [NUM PONTO] '); 219: Coloca('�'); 220: Coloca(']'); 221: Coloca('['); 222: Coloca('~');
      226: Coloca('\'); else if (KeyLoop >= 65) and (keyloop <= 90) then Coloca(Chr(keyloop)); if (keyloop >= 32) and (keyloop <= 63) then Coloca(Chr(keyloop)); //numpad keycodes if (keyloop >= 96) and (keyloop <= 110) then Coloca(Chr(keyloop)); end; end; //case; inc(keyloop); until keyloop = 255; end;  until ;
*)
     inc(keyloop);
  until keyloop = 255;
end;

end.
