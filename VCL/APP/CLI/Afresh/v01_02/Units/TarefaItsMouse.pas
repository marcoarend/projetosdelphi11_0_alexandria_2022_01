unit TarefaItsMouse;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, dmkEdit,
  Vcl.Buttons, dmkRadioGroup;

type
  TFmTarefaItsMouse = class(TForm)
    Panel1: TPanel;
    Memo1: TMemo;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    EdOrdem: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    BtCapturar: TBitBtn;
    EdMousePosX: TdmkEdit;
    LabelX: TLabel;
    LabelY: TLabel;
    EdMousePosY: TdmkEdit;
    RGBotao: TdmkRadioGroup;
    EdSegundosA: TdmkEdit;
    EdSegundosD: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    RGQtdClkSmlt: TdmkRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtCapturarClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
  private
    { Private declarations }
    FCaptura: Boolean;
  public
    { Public declarations }
  end;

  MouseLLHookStruct = record
    pt          : TPoint;
    mouseData   : cardinal;
    flags       : cardinal;
    time        : cardinal;
    dwExtraInfo : cardinal;
  end;
    function LowLevelMouseHookProc(nCode, wParam, lParam : integer) : integer; stdcall;

var
  FmTarefaItsMouse: TFmTarefaItsMouse;
  mHook : cardinal;

implementation

uses dmkGeral, UnAppEnums, UnAppPF, UnMyObjects, Principal, Tarefa;

{$R *.dfm}

//function TFmTarefaItsMouse.LowLevelMouseHookProc(nCode, wParam,
function LowLevelMouseHookProc(nCode, wParam,
  lParam: integer): integer;
// possible wParam values: WM_LBUTTONDOWN, WM_LBUTTONUP, WM_MOUSEMOVE, WM_MOUSEWHEEL, WM_RBUTTONDOWN, WM_RBUTTONUP
var
  info : ^MouseLLHookStruct absolute lParam;
begin
  result := CallNextHookEx(mHook, nCode, wParam, lParam);
  with info^ do begin
    FmTarefaItsMouse.LabelX.Caption := 'X:  ' + IntToStr(pt.X);
    FmTarefaItsMouse.LabelY.Caption := 'Y:  ' + IntToStr(pt.Y);
    //
    case wParam of
      wm_lbuttondown : FmTarefaItsMouse.Memo1.Lines.Append(format('pressed left button (%d, %d)'    , [pt.x, pt.y]));
      wm_lbuttonup   : FmTarefaItsMouse.Memo1.Lines.Append(format('released left button (%d, %d)'   , [pt.x, pt.y]));
      wm_mbuttondown : FmTarefaItsMouse.Memo1.Lines.Append(format('pressed middle button (%d, %d)'  , [pt.x, pt.y]));
      wm_mbuttonup   : FmTarefaItsMouse.Memo1.Lines.Append(format('released middle button (%d, %d)' , [pt.x, pt.y]));
      wm_rbuttondown : FmTarefaItsMouse.Memo1.Lines.Append(format('pressed right button (%d, %d)'   , [pt.x, pt.y]));
      wm_rbuttonup   : FmTarefaItsMouse.Memo1.Lines.Append(format('released right button (%d, %d)'  , [pt.x, pt.y]));
      wm_mousewheel  : begin
        if smallInt(mouseData shr 16) > 0
        then FmTarefaItsMouse.Memo1.Lines.Append('scrolled wheel (up)')
        else FmTarefaItsMouse.Memo1.Lines.Append('scrolled wheel (down)');
      end;
    end;
    if FmTarefaItsMouse.FCaptura then
    begin
      if (wParam = wm_lbuttondown)
      or (wParam = wm_rbuttondown)
      or (wParam = wm_mbuttondown) then
      begin
        FmPrincipal.WindowState      := wsNormal;
        FmTarefa.WindowState       := wsNormal;
        FmTarefaItsMouse.WindowState := wsNormal;
        FmTarefaItsMouse.EdMousePosX.ValueVariant := pt.X;
        FmTarefaItsMouse.EdMousePosY.ValueVariant := pt.Y;
        //
        if (wParam = wm_lbuttondown) then
          FmTarefaItsMouse.RGBotao.ItemIndex := 1
        else
        if (wParam = wm_rbuttondown) then
          FmTarefaItsMouse.RGBotao.ItemIndex := 2
        else
        if (wParam = wm_mbuttondown) then
          FmTarefaItsMouse.RGBotao.ItemIndex := 3;
        //
        FmTarefaItsMouse.FCaptura := False;
      end;
    end;
  end;
end;

procedure TFmTarefaItsMouse.BtCapturarClick(Sender: TObject);
begin
  FmPrincipal.WindowState      := wsMinimized;
  FmTarefa.WindowState         := wsMinimized;
  FmTarefaItsMouse.WindowState := wsMinimized;
  FCaptura := True;
end;

procedure TFmTarefaItsMouse.BtConfirmaClick(Sender: TObject);
var
  Codigo, Ordem, TpAcao, MousePosX, MousePosY, MouseEvent, QtdClkSmlt: Integer;
  Nome: String;
  SegundosA, SegundosD: Double;
begin
  TpAcao     := Integer(TTpAcaoInputEvent.taieMouse); // 1
  Codigo     := EdCodigo.ValueVariant;
  Ordem      := EdOrdem.ValueVariant - 1;
  MousePosX  := EdMousePosX.ValueVariant;
  MousePosY  := EdMousePosY.ValueVariant;
  Nome       := EdNome.ValueVariant;
  SegundosA  := EdSegundosA.ValueVariant;
  SegundosD  := EdSegundosD.ValueVariant;
(*
  case RGBotao.ItemIndex of
    //Indefinido
    //Esquerdo
    1: MouseEvent := wm_lbuttondown;
    //Direto
    2: MouseEvent := wm_rbuttondown;
    //Centro
    3: MouseEvent := wm_mbuttondown;
  end;
*)
  MouseEvent := AppPF.MouseBotaoEventToWindowsMessage(RGBotao.ItemIndex);
  QtdClkSmlt := RGQtdClkSmlt.ItemIndex;
  //
  if MyObjects.FIC(RGQtdClkSmlt.ItemIndex = 0, RGQtdClkSmlt,
    'Informe o click do mouse!') then Exit;
  if MyObjects.FIC(RGBotao.ItemIndex = 0, RGBotao,
    'Informe o bot�o do mouse!') then Exit;
  if Codigo = 0 then
  begin
    FmTarefa.FMaiorCodigo := FmTarefa.FMaiorCodigo + 1;
    Codigo := FmTarefa.FMaiorCodigo;
    FmTarefa.MemTable.Append;
  end else
  begin
    FmTarefa.MemTable.Edit;
  end;
  FmTarefa.MemTableCodigo.Value     := Codigo;
  FmTarefa.MemTableNome.Value       := Nome;
  FmTarefa.MemTableOrdem.Value      := Ordem;
  FmTarefa.MemTableTpAcao.Value     := TpAcao;

  FmTarefa.MemTableMousePosX.Value  := MousePosX;
  FmTarefa.MemTableMousePosY.Value  := MousePosY;
  FmTarefa.MemTableMouseEvent.Value := MouseEvent;
  FmTarefa.MemTableQtdClkSmlt.Value := QtdClkSmlt;

  FmTarefa.MemTableSegundosA.Value  := SegundosA;
  FmTarefa.MemTableSegundosD.Value  := SegundosD;
  //
  FmTarefa.MemTable.Post;
  //
  FmTarefa.Reordena();
  FmTarefa.Salva();
  //
  Close;
end;

procedure TFmTarefaItsMouse.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTarefaItsMouse.FormCreate(Sender: TObject);
const
  WH_MOUSE_LL = 14;
begin
  mHook := SetWindowsHookEx(WH_MOUSE_LL, @LowLevelMouseHookProc, hInstance, 0);
end;

procedure TFmTarefaItsMouse.FormDestroy(Sender: TObject);
begin
  UnhookWindowsHookEx(mHook);
end;

end.
