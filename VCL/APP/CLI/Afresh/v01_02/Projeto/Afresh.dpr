program Afresh;

uses
  Vcl.Forms,
  Teste in '..\Units\Teste.pas' {FmTeste},
  dmkGeral in '..\..\..\..\..\..\dmkComp\dmkGeral.pas',
  KeysU in '..\KeysU.pas',
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  Vcl.Themes,
  Vcl.Styles,
  Tarefa in '..\Units\Tarefa.pas' {FmTarefa},
  Layout_Tarefas_v001 in '..\Units\Schemas\Layout_Tarefas_v001.pas',
  Tarefas in '..\Units\Tarefas.pas' {Form1},
  UnMyJSON in '..\..\..\..\..\UTL\JSON\UnMyJSON.pas',
  uLkJSON in '..\..\..\..\..\UTL\JSON\uLkJSON.pas',
  UnProjGroup_Vars in 'UnProjGroup_Vars.pas',
  UnAppEnums in '..\Units\UnAppEnums.pas',
  UnAppPF in '..\Units\UnAppPF.pas',
  UnLicVendaApp_Dmk in '..\..\..\..\..\UTL\_UNT\v01_01\UnLicVendaApp_Dmk.pas',
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnGrl_DmkREST in '..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_DmkREST.pas',
  UnDmkHTML2 in '..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  TarefaItsMouse in '..\Units\TarefaItsMouse.pas' {FmTarefaItsMouse},
  TarefaItsTexto in '..\Units\TarefaItsTexto.pas' {FmTarefaItsTexto},
  TarefaItsComando in '..\Units\TarefaItsComando.pas' {FmTarefaItsComando},
  WBFuncs in '..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  MyListas in '..\Units\MyListas.pas',
  LicDados in '..\Units\LicDados.pas' {FmLicDados},
  OpcoesApp in '..\Units\OpcoesApp.pas' {FmOpcoesApp},
  SntpSend in '..\..\SntpSend.pas',
  Windows_FMX_ProcFunc in '..\..\..\..\..\..\FMX\UTL\_UNT\v01_01\Windows\Windows_FMX_ProcFunc.pas',
  UnAppMainPF in '..\Units\UnAppMainPF.pas',
  Dmk_Afresh in '..\Units\Dmk_Afresh.pas' {FmDmk_Afresh},
  UnApp_Consts in '..\Units\UnApp_Consts.pas',
  UnApp_Vars in '..\Units\UnApp_Vars.pas',
  ABOUT in '..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {FmAbout},
  UnProjGroup_PF in '..\Units\UnProjGroup_PF.pas',
  UnMyVclEvents in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyVclEvents.pas',
  UnMyProcesses in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyProcesses.pas';

{$R *.res}

begin
  //DELPHI12_UP;NO_USE_MYSQLMODULE;NAO_USA_SEL_RADIO_GROUP;NAO_USA_GLYFS;NAO_USA_FRX;NAO_USA_TEXTOS;NAO_USA_DB_GERAL;NAO_USA_DB;sGetData;NAO_USA_UnitMD5;DEBUG
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Tablet Light');
  Application.Title := 'Afresh';
  Application.Name  := 'Afresh';
  if CO_VERMCW > CO_VERMLA then
    CO_VERSAO := CO_VERMCW
  else
    CO_VERSAO := CO_VERMLA;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
