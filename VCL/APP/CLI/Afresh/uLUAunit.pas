unit uLUAunit;

interface
Uses Registry, SysUtils,Windows, ShellAPI, Forms;

Type
  osRec = record
    Win32 : Byte;
    VerHI,
    VerLO   : Byte;
  end;

  TExecAsOption = (
    eoHide,
    eoWait,
    eoElevate  );

  TExecAsOptions = set of TExecAsOption;

function IsUACActive: Boolean;
function GetWindowsVersion: OsRec;
function ExecAs(Handle: HWND; const Filename, Paramaters: String; Options: TExecAsOptions): Integer;
function CheckTokenMembership(TokenHandle: THandle; SidToCheck: PSID; out IsMember: BOOL): BOOL; stdcall;
function SHTestTokenMembership(hToken: THandle; ulRID: ULONG): BOOL; stdcall;
function IsUserAdmin(): BOOL; stdcall;


implementation

function GetWindowsVersion: OsRec;
begin
   FillChar(Result,3,0);
   try
     if Win32Platform = VER_PLATFORM_WIN32s then
       Result.Win32:=VER_PLATFORM_WIN32s
     else if Win32Platform = VER_PLATFORM_WIN32_WINDOWS then
     begin
       Result.Win32:=VER_PLATFORM_WIN32_WINDOWS;
       { Ver 4    = 95
             4.1  = 98
             4.9  = Me
             >4.9 = 9x }
     end
     else if Win32Platform = VER_PLATFORM_WIN32_NT then
     begin
       Result.Win32:=VER_PLATFORM_WIN32_NT;
       Result.VerHI:=Win32MajorVersion;
       Result.VerLO:=Win32MinorVersion;
       { Ver 3.51  = NT 3.51
             4.0   = NT 4
             5.0   = Win 2000
             5.1   = XP
             5.2   = Win 2003
             6.0   = Vista
             6.1   = Windows 7
             6.2   = Windows 8
             6.3   =
             10.x   =
             }
    end
  except
  end;
end;

// UAC Aktif mi de?il mi ?
// Windows vista ve Sonras? i�in ge�erli
function IsUACActive: Boolean;
var
  Reg: TRegistry;
begin
  Result := FALSE;
  // Vista Windows 7. veya sonras?
  if CheckWin32Version(6, 0) then
  begin
    Reg := TRegistry.Create;
    try
      Reg.RootKey := HKEY_LOCAL_MACHINE;
      if Reg.OpenKeyReadOnly('SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System') then
      begin
        if (Reg.ValueExists('EnableLUA')) and (Reg.ReadBool('EnableLUA')) then
          Result := TRUE;
      end;
    finally
      FreeAndNil(Reg);
    end;
  end;
end;

//------------------------------------------------------------------------------------------
function ExecAs(Handle: HWND; const Filename, Paramaters: String; Options: TExecAsOptions): Integer;
var
  ShellExecuteInfo: TShellExecuteInfo;
  ExitCode: DWORD;
begin
  Result := -1;
  ZeroMemory(@ShellExecuteInfo, SizeOf(ShellExecuteInfo));
  ShellExecuteInfo.cbSize := SizeOf(TShellExecuteInfo);
  ShellExecuteInfo.Wnd := Handle;
  ShellExecuteInfo.fMask := SEE_MASK_NOCLOSEPROCESS;

  if (eoElevate in Options) and (IsUACActive) then
    ShellExecuteInfo.lpVerb := PChar('runas');

  ShellExecuteInfo.lpFile := PChar(Filename);

  if Paramaters <> '' then
    ShellExecuteInfo.lpParameters := PChar(Paramaters);

  // Show or hide the window
  if eoHide in Options then
    ShellExecuteInfo.nShow := SW_HIDE
  else
    ShellExecuteInfo.nShow := SW_SHOWNORMAL;

  if ShellExecuteEx(@ShellExecuteInfo) then
    Result := 0;

  if (Result = 0) and (eoWait in Options) then
  begin
    GetExitCodeProcess(ShellExecuteInfo.hProcess, ExitCode);
    while (ExitCode = STILL_ACTIVE) and  (not Application.Terminated) do
    begin
      sleep(50);
      GetExitCodeProcess(ShellExecuteInfo.hProcess, ExitCode);
    end;
    Result := ExitCode;
  end;
end;

//------------------------------------------------------------------------------------------
function GetAdvApi32Lib(): HMODULE;
const
  ModuleName = 'ADVAPI32';
const
  ModuleHandle: HMODULE = HMODULE(nil);
begin
  Result := ModuleHandle;
  if Result = HMODULE(nil) then
  begin
    Result := LoadLibrary(ModuleName);
    if Result <> HMODULE(nil) then
      ModuleHandle := Result;
  end;
end;

//------------------------------------------------------------------------------------------
function CheckTokenMembership(TokenHandle: THandle; SidToCheck: PSID;  out IsMember: BOOL): BOOL;
type
  TFNCheckTokenMembership = function(TokenHandle: THandle; SidToCheck: PSID;      out IsMember: BOOL): BOOL; stdcall;
const
  Initialized: Integer = 0;
  RealApiFunc: TFNCheckTokenMembership = nil;
type
  TAceHeader = packed record
    AceType : Byte;
    AceFlags: Byte;
    AceSize : Word;
  end;
  TAccessAllowedAce = packed record
    Header : TAceHeader;
    Mask : ACCESS_MASK;
    SidStart: DWORD;
  end;
const
  ACL_REVISION = 2;
  DesiredAccess = 1;
  GenericMapping: TGenericMapping = (
    GenericRead : STANDARD_RIGHTS_READ;
    GenericWrite : STANDARD_RIGHTS_WRITE;
    GenericExecute: STANDARD_RIGHTS_EXECUTE;
    GenericAll : STANDARD_RIGHTS_ALL  );
var
  ClientToken: THandle;
  ProcessToken: THandle;
  SecurityDescriptorSize: Cardinal;
  SecurityDescriptor: PSecurityDescriptor;
  Dacl: PACL;
  PrivilegeSetBufferSize: ULONG;
  PrivilegeSetBuffer: packed record
    PrivilegeSet: TPrivilegeSet;
    Buffer: array [0..2] of TLUIDAndAttributes;
  end;
  GrantedAccess: ACCESS_MASK;
  AccessStatus: BOOL;
begin
  if Initialized = 0 then
  begin
    RealApiFunc := TFNCheckTokenMembership(
      GetProcAddress(GetAdvApi32Lib(), 'CheckTokenMembership'));
    InterlockedIncrement(Initialized);
  end;
  if Assigned(RealApiFunc) then
    Result := RealApiFunc(TokenHandle, SidToCheck, IsMember)
  else
  begin
    Result := False;
    IsMember := False;
    ClientToken := THandle(nil);
    try
      if TokenHandle <> THandle(nil) then
        ClientToken := TokenHandle
      else if not OpenThreadToken(GetCurrentThread(), TOKEN_QUERY, False,
        ClientToken) then
      begin
        ClientToken := THandle(nil);
        if GetLastError() = ERROR_NO_TOKEN then
        begin
          if OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY or
            TOKEN_DUPLICATE, ProcessToken) then
          try
            if not DuplicateToken(ProcessToken, SecurityImpersonation,
              @ClientToken) then
            begin
              ClientToken := THandle(nil);
            end;
          finally
            CloseHandle(ProcessToken);
          end;
        end;
      end;
      if ClientToken <> THandle(nil) then
      begin
        SecurityDescriptorSize := SizeOf(TSecurityDescriptor) +
          SizeOf(TAccessAllowedAce) + SizeOf(TACL) +
          3 * GetLengthSid(SidToCheck);
        SecurityDescriptor := PSecurityDescriptor(
          LocalAlloc(LMEM_ZEROINIT, SecurityDescriptorSize));
        if SecurityDescriptor <> nil then
        try
          if InitializeSecurityDescriptor(SecurityDescriptor,
            SECURITY_DESCRIPTOR_REVISION) then
          begin
            if SetSecurityDescriptorOwner(SecurityDescriptor, SidToCheck,
              False) then
            begin
              if SetSecurityDescriptorGroup(SecurityDescriptor, SidToCheck,
                False) then
              begin
                Dacl := PACL(SecurityDescriptor);
                Inc(PSecurityDescriptor(Dacl));
                if InitializeAcl(Dacl^,
                  SecurityDescriptorSize - SizeOf(TSecurityDescriptor),
                  ACL_REVISION) then
                begin
                  if AddAccessAllowedAce(Dacl^, ACL_REVISION, DesiredAccess,
                    SidToCheck) then
                  begin
                    if SetSecurityDescriptorDacl(SecurityDescriptor, True, Dacl,
                      False) then
                    begin
                      PrivilegeSetBufferSize := SizeOf(PrivilegeSetBuffer);
                      Result := AccessCheck(SecurityDescriptor, ClientToken,
                        DesiredAccess, GenericMapping,
                        PrivilegeSetBuffer.PrivilegeSet, PrivilegeSetBufferSize,
                        GrantedAccess, AccessStatus);
                      if Result then
                        IsMember := AccessStatus and
                          (GrantedAccess = DesiredAccess);
                    end;
                  end;
                end;
              end;
            end;
          end;
        finally
          LocalFree(HLOCAL(SecurityDescriptor));
        end;
      end;
    finally
      if (ClientToken <> THandle(nil)) and
        (ClientToken <> TokenHandle) then
      begin
        CloseHandle(ClientToken);
      end;
    end;
  end;
end;

//------------------------------------------------------------------------------------------
function GetShell32Lib(): HMODULE;
const
  ModuleName = 'SHELL32';
const
  ModuleHandle: HMODULE = HMODULE(nil);
begin
  Result := ModuleHandle;
  if Result = HMODULE(nil) then
  begin
    Result := LoadLibrary(ModuleName);
    if Result <> HMODULE(nil) then
      ModuleHandle := Result;
  end;
end;

//------------------------------------------------------------------------------------------
function SHTestTokenMembership(hToken: THandle; ulRID: ULONG): BOOL; stdcall;
type
  TFNSHTestTokenMembership = function(hToken: THandle; ulRID: ULONG): BOOL; stdcall;
const
  Initialized: Integer = 0;
  RealApiFunc: TFNSHTestTokenMembership = nil;
const
  SECURITY_NT_AUTHORITY: TSIDIdentifierAuthority = (Value: (0, 0, 0, 0, 0, 5));
  SECURITY_BUILTIN_DOMAIN_RID = $00000020;
var
  SidToCheck: PSID;
begin
  if Initialized = 0 then
  begin
    RealApiFunc := TFNSHTestTokenMembership(
      GetProcAddress(GetShell32Lib(), 'SHTestTokenMembership'));
    InterlockedIncrement(Initialized);
  end;
  if Assigned(RealApiFunc) then
    Result := RealApiFunc(hToken, ulRID)
  else
  begin
    Result := AllocateAndInitializeSid(SECURITY_NT_AUTHORITY, 2,
      SECURITY_BUILTIN_DOMAIN_RID, ulRID, 0, 0, 0, 0, 0, 0, SidToCheck);
    if Result then
    try
      if not CheckTokenMembership(THandle(nil), SidToCheck, Result) then
        Result := False;
    finally
      FreeSid(SidToCheck);
    end;
  end;
end;

//------------------------------------------------------------------------------------------
function IsUserAdmin(): BOOL;
const
  DOMAIN_ALIAS_RID_ADMINS = $00000220;
type
  TFNIsUserAnAdmin = function(): BOOL; stdcall;
const
  Initialized: Integer = 0;
  RealApiFunc: TFNIsUserAnAdmin = nil;
begin
  if Initialized = 0 then
  begin
    RealApiFunc := TFNIsUserAnAdmin(
      GetProcAddress(GetShell32Lib(), 'IsUserAnAdmin'));
    InterlockedIncrement(Initialized);
  end;
  if Assigned(RealApiFunc) then
    Result := RealApiFunc()
  else
    Result := SHTestTokenMembership(THandle(nil), DOMAIN_ALIAS_RID_ADMINS);
end;

end.



(*
Uses uLUAunit;
 ....

Var _2nd_time : Boolean;
begin

  _2nd_time=Paramstr(1)='/TEKRAR';
  If (NOT IsUserAdmin()) and  (NOT _2nd_time) Then
      ExecAs(Self.Handle, ParamStr(0),'/TEKRAR',[eoElevate]);

   // Self.Handle = Caller Handele ID
   // Paramstr(0) = Executable FileName
   // Parameters
   // Run Options eoElevate, eoHide, eoWait
   ...
end;
...
...
*)
