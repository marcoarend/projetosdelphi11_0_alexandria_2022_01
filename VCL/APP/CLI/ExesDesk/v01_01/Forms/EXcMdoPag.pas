unit EXcMdoPag;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkCheckBox, Vcl.Menus;

type
  TFmEXcMdoPag = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEXcMdoPag: TMySQLQuery;
    QrEXcMdoPagCodigo: TIntegerField;
    QrEXcMdoPagNome: TWideStringField;
    DsEXcMdoPag: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    DBCheckBox2: TDBCheckBox;
    CkAtivo: TdmkCheckBox;
    QrEXcMdoPagOrdem: TIntegerField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    EdOrdem: TdmkEdit;
    Label4: TLabel;
    QrEXcMdoPagAtivo: TSmallintField;
    PMAltera: TPopupMenu;
    Dados1: TMenuItem;
    Imagem1: TMenuItem;
    QrEXcMdoPagImgB64: TWideMemoField;
    QrEXcMdoPagImgPath: TWideStringField;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    ImagemMobile1: TMenuItem;
    Panel6: TPanel;
    PnImg: TPanel;
    ImgImgB64: TImage;
    Panel7: TPanel;
    ImgPngB64: TImage;
    QrEXcMdoPagPngB64: TWideMemoField;
    EdSigla: TdmkEdit;
    Label6: TLabel;
    QrEXcMdoPagSigla: TWideStringField;
    Label8: TLabel;
    DBEdit3: TDBEdit;
    GroupBox2: TGroupBox;
    Panel8: TPanel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    GroupBox1: TGroupBox;
    Panel9: TPanel;
    EdDocHeader: TdmkEdit;
    EdItemText: TdmkEdit;
    QrEXcMdoPagDocHeader: TWideStringField;
    QrEXcMdoPagItemText: TWideStringField;
    Label11: TLabel;
    Label12: TLabel;
    QrEXcMdoPagReference: TWideStringField;
    Label14: TLabel;
    DBEdit6: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdReference: TdmkEdit;
    Label10: TLabel;
    dmkEdit1: TdmkEdit;
    RGRegrTipSeq: TdmkRadioGroup;
    EdRegrQtdDia: TdmkEdit;
    Label13: TLabel;
    RGRegrDiaSem: TdmkRadioGroup;
    QrEXcMdoPagRegrQtdDia: TIntegerField;
    QrEXcMdoPagRegrTipSeq: TIntegerField;
    QrEXcMdoPagRegrDiaSem: TSmallintField;
    dmkRadioGroup1: TDBRadioGroup;
    Label18: TLabel;
    dmkRadioGroup2: TDBRadioGroup;
    DBEdit7: TDBEdit;
    EdNoEn: TdmkEdit;
    Label19: TLabel;
    QrEXcMdoPagNoEN: TWideStringField;
    Label20: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEXcMdoPagAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEXcMdoPagBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure Dados1Click(Sender: TObject);
    procedure Imagem1Click(Sender: TObject);
    procedure QrEXcMdoPagAfterScroll(DataSet: TDataSet);
    procedure ImagemMobile1Click(Sender: TObject);
    procedure RGRegrTipSeqClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEXcMdoPag: TFmEXcMdoPag;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkImg, UnExesD_PF, MyGlyfs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEXcMdoPag.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEXcMdoPag.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEXcMdoPagCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEXcMdoPag.DefParams;
begin
  VAR_GOTOTABELA := 'excmdopag';
  VAR_GOTOMYSQLTABLE := QrEXcMdoPag;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM excmdopag');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmEXcMdoPag.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEXcMdoPag.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEXcMdoPag.RGRegrTipSeqClick(Sender: TObject);
begin
  RGRegrDiaSem.Enabled := RGRegrTipSeq.ItemIndex = 3;
end;

procedure TFmEXcMdoPag.Dados1Click(Sender: TObject);
begin
  if (QrEXcMdoPag.State <> dsInactive) and (QrEXcMdoPag.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEXcMdoPag, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'excmdopag');
  end;
end;

procedure TFmEXcMdoPag.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEXcMdoPag.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEXcMdoPag.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEXcMdoPag.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEXcMdoPag.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEXcMdoPag.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEXcMdoPag.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmEXcMdoPag.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEXcMdoPagCodigo.Value;
  Close;
end;

procedure TFmEXcMdoPag.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla, NoEN: String;
  Codigo, Ordem: Integer;
  SQLType: TSQLType;
  DocHeader, ItemText, Reference, DescrCondiPg: String;
  RegrQtdDia, RegrTipSeq, RegrDiaSem: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  NoEN           := EdNoEN.ValueVariant;
  Sigla          := EdSigla.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  DocHeader      := EdDocHeader.ValueVariant;
  ItemText       := EdItemText.ValueVariant;
  Reference      := EdReference.ValueVariant;
  RegrQtdDia     := EdRegrQtdDia.ValueVariant;
  RegrTipSeq     := RGRegrTipSeq.ItemIndex;
  RegrDiaSem     := RGRegrDiaSem.ItemIndex;

  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(Sigla) = 0, EdNome, 'Defina a sigla!') then Exit;
  //

  //
  Codigo := UMyMod.BPGS1I32('excmdopag', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'excmdopag', False, [
  'Nome', 'Ordem', 'Sigla',
  'DocHeader', 'ItemText', 'Reference',
  'RegrQtdDia', 'RegrTipSeq', 'RegrDiaSem',
  'NoEN'], [
  'Codigo'], [
  Nome, Ordem, Sigla,
  DocHeader, ItemText, Reference,
  RegrQtdDia, RegrTipSeq, RegrDiaSem,
  NoEN], [
  Codigo], True) then
  begin
    if UnDmkDAC_PF.AtivaDesativaRegistroIdx1(Dmod.MyDB, 'excmdopag', 'Codigo',
    'Ativo', Codigo, CkAtivo.Checked) then
    begin
      ImgTipo.SQLType := stLok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
      GOTOy.BotoesSb(ImgTipo.SQLType);
      //
      LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmEXcMdoPag.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'excmdopag', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEXcMdoPag.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEXcMdoPag, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'excmdopag');
end;

procedure TFmEXcMdoPag.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //PnImg.Align := alClient;
  CriaOForm;
end;

procedure TFmEXcMdoPag.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEXcMdoPagCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcMdoPag.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEXcMdoPag.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEXcMdoPagCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcMdoPag.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEXcMdoPag.QrEXcMdoPagAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEXcMdoPag.QrEXcMdoPagAfterScroll(DataSet: TDataSet);
  procedure CarregaBmp();
  var
    Bmp2: TBitmap;
    ImgB64: String;
  begin
    ImgB64 := QrEXcMdoPagImgB64.Value;
    if ImgB64 <> EmptyStr then
    begin
      Bmp2 := TBitmap.Create;
      try
        Bmp2 := DmkImg.BitmapFromBase64(ImgB64);
        ImgImgB64.Picture.Bitmap.Assign(Bmp2);
        ImgImgB64.Visible := True;
        PnDados.Visible := False;
        PnDados.Visible := True;
      finally
        Bmp2.Free;
      end;
    end else
      ImgImgB64.Visible := False;
  end;
  //
begin
  CarregaBmp();
  FmMyGlyfs.CarregaPNGDoBD(ImgPngB64, QrEXcMdoPagPngB64.Value);
end;

procedure TFmEXcMdoPag.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEXcMdoPag.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEXcMdoPagCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'excmdopag', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEXcMdoPag.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEXcMdoPag.Imagem1Click(Sender: TObject);
{
const
  Titulo = 'Defini��o Imagem de bot�o';
var
  IniDir, Arquivo, Filtro, ImgB64, ImgPath: String;
  Bmp1, Bmp2: TBitmap;
  Codigo: Integer;
begin
  IniDir  := 'C:\Dermatek\';
  Arquivo := '';
  Filtro  := 'Arquivos Bitmap|*.bmp;Arquivos JPEG|*.jpeg;Arquivos JPG|*.jpg';
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, Titulo, Filtro, [], Arquivo) then
  begin
    if FileExists(Arquivo) then
    begin
      Screen.Cursor := crHourGlass;
      try
        Bmp1 := TBitmap.Create;
        try
          Bmp1.LoadFromFile(Arquivo);
          //ImgB64 := DmkImg.Base64FromBitmap2(Bitmap);
          ImgB64 := DmkImg.Base64FromBitmap(Bmp1);
          ImgPath := Arquivo;
          Codigo := QrEXcMdoPagCodigo.Value;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'excmdopag', False, [
          'ImgB64', 'ImgPath'], [
          'Codigo'], [
          ImgB64, ImgPath], [
          Codigo], True) then
          begin
            LocCod(Codigo, Codigo);
          end;
        finally
          Bmp1.Free;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;
}
var
  Codigo: Integer;
begin
  Codigo := QrEXcMdoPagCodigo.Value;
  if ExesD_PF.CarregaImagemRegistro(Self, 'excmdopag', 'ImgB64', 'ImgPath',
  Codigo, 192, 192) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEXcMdoPag.ImagemMobile1Click(Sender: TObject);
var
  Codigo: Integer;
  PngB64, PngPath: String;
begin
  Codigo := QrEXcMdoPagCodigo.Value;
  if FmMyGlyfs.SalvaPNGDeArquivoNoBD(Self, 192, 192, PngB64, PngPath) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd,  'excmdopag', False, [
    'PngB64', 'PngPath'], [
    'Codigo'], [
     PngB64, PngPath], [
     Codigo], True) then
     begin
       LocCod(Codigo, Codigo);
     end;
  end;
end;

procedure TFmEXcMdoPag.QrEXcMdoPagBeforeOpen(DataSet: TDataSet);
begin
  QrEXcMdoPagCodigo.DisplayFormat := FFormatFloat;
end;

end.

