unit ComptncIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmComptncIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrComptncCad: TMySQLQuery;
    QrComptncCadCodigo: TIntegerField;
    QrComptncCadNome: TWideStringField;
    DsComptncCad: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label1: TLabel;
    EdComptncCad: TdmkEditCB;
    CBComptncCad: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenComptncIts(Controle: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmComptncIts: TFmComptncIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmComptncIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, ComptncCad: Integer;
  SQLType: TSQLType;
begin
  SQLType     := ImgTipo.SQLType;
  Codigo      := FCodigo;
  Controle    := EdControle.ValueVariant;
  ComptncCad  := EdComptncCad.ValueVariant;
  //
  if MyObjects.FIC(ComptncCad = 0, EdComptncCad, 'Informe a competÍncia!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('comptncits', 'Controle', '', '', tsPos, SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'comptncits', False, [
  'Codigo', 'ComptncCad'], [
  'Controle'], [
  Codigo, ComptncCad], [
  Controle], True) then
  begin
    ReopenComptncIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdControle.ValueVariant   := 0;
      EdComptncCad.ValueVariant := 0;
      CBComptncCad.KeyValue     := Null;
      //
      EdComptncCad.SetFocus;
      //
    end else Close;
  end;
end;

procedure TFmComptncIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmComptncIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmComptncIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrComptncCad, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM comptnccad',
  'ORDER BY NOme ',
  '']);
end;

procedure TFmComptncIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmComptncIts.ReopenComptncIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmComptncIts.SpeedButton1Click(Sender: TObject);
begin
{
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFm_CadSel_, Fm_CadSel_, afmoNegarComAviso) then
  begin
    Fm_CadSel_.ShowModal;
    Fm_CadSel_.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
}
end;

end.
