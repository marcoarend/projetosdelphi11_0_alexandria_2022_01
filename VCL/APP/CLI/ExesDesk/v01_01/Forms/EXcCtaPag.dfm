object FmEXcCtaPag: TFmEXcCtaPag
  Left = 368
  Top = 194
  Caption = 'EXS-CADAS-002 :: Cadastro de Contas de Pagamento'
  ClientHeight = 483
  ClientWidth = 838
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 838
    Height = 387
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 838
      Height = 149
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 93
        Height = 13
        Caption = 'Descri'#231#227'o da conta'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 100
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object Label8: TLabel
        Left = 404
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Descri'#231#227'o do grupo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 100
        Top = 100
        Width = 109
        Height = 13
        Caption = 'Conta cont'#225'bil (Grupo):'
      end
      object SbEXcGruCta: TSpeedButton
        Left = 692
        Top = 116
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbEXcGruCtaClick
      end
      object Label11: TLabel
        Left = 76
        Top = 56
        Width = 143
        Height = 13
        Caption = 'Descri'#231#227'o da conta em ingl'#234's:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 404
        Top = 56
        Width = 143
        Height = 13
        Caption = 'Descri'#231#227'o do grupo em ingl'#234's:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 325
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkAtivo: TdmkCheckBox
        Left = 720
        Top = 120
        Width = 53
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 8
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object EdOrdem: TdmkEdit
        Left = 16
        Top = 116
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNomGrupo: TdmkEdit
        Left = 404
        Top = 32
        Width = 361
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NomGrupo'
        UpdCampo = 'NomGrupo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEXcGruCta: TdmkEditCB
        Left = 100
        Top = 116
        Width = 89
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EXcGruCta'
        UpdCampo = 'EXcGruCta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEXcGruCta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEXcGruCta: TdmkDBLookupComboBox
        Left = 192
        Top = 116
        Width = 497
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEXcGruCta
        TabOrder = 7
        dmkEditCB = EdEXcGruCta
        QryCampo = 'EXcGruCta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdNoEN: TdmkEdit
        Left = 76
        Top = 72
        Width = 325
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NoEN'
        UpdCampo = 'NoEN'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNomGruEN: TdmkEdit
        Left = 404
        Top = 72
        Width = 361
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NomGruEN'
        UpdCampo = 'NomGruEN'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 324
      Width = 838
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 699
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 838
    Height = 387
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Panel6: TPanel
      Left = 776
      Top = 0
      Width = 62
      Height = 323
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object PnImg: TPanel
        Left = 0
        Top = 0
        Width = 62
        Height = 69
        Align = alTop
        BevelOuter = bvNone
        Caption = 'BMP'
        DoubleBuffered = True
        ParentDoubleBuffered = False
        TabOrder = 0
        object ImgImgB64: TImage
          Left = 4
          Top = 8
          Width = 50
          Height = 50
          Proportional = True
          Stretch = True
          Transparent = True
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 69
        Width = 62
        Height = 64
        Align = alTop
        BevelOuter = bvNone
        Caption = 'PNG'
        DoubleBuffered = True
        ParentDoubleBuffered = False
        TabOrder = 1
        object ImgPngB64: TImage
          Left = 4
          Top = 4
          Width = 50
          Height = 50
          Proportional = True
          Stretch = True
          Transparent = True
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 713
      Height = 323
      Align = alLeft
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      ExplicitLeft = 20
      ExplicitTop = 12
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Descri'#231#227'o da conta:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 16
        Top = 104
        Width = 31
        Height = 13
        Caption = 'Ordem'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 156
        Top = 104
        Width = 202
        Height = 13
        Caption = 'Caminho do arquivo da imagem carregada:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 348
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Descri'#231#227'o do grupo:'
        FocusControl = dmkDBEdit1
      end
      object Label12: TLabel
        Left = 16
        Top = 144
        Width = 109
        Height = 13
        Caption = 'Conta cont'#225'bil (Grupo):'
        FocusControl = DBEdit4
      end
      object Label14: TLabel
        Left = 76
        Top = 56
        Width = 143
        Height = 13
        Caption = 'Descri'#231#227'o da conta em ingl'#234's:'
        FocusControl = dmkDBEdit2
      end
      object Label15: TLabel
        Left = 348
        Top = 56
        Width = 143
        Height = 13
        Caption = 'Descri'#231#227'o do grupo em ingl'#234's:'
        FocusControl = dmkDBEdit3
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEXcCtaPag
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 269
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEXcCtaPag
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCheckBox2: TDBCheckBox
        Left = 648
        Top = 100
        Width = 57
        Height = 17
        Caption = 'Ativo.'
        DataField = 'Ativo'
        DataSource = DsEXcCtaPag
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 120
        Width = 134
        Height = 21
        DataField = 'Ordem'
        DataSource = DsEXcCtaPag
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 156
        Top = 120
        Width = 549
        Height = 21
        DataField = 'ImgPath'
        DataSource = DsEXcCtaPag
        TabOrder = 4
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 348
        Top = 32
        Width = 361
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NomGrupo'
        DataSource = DsEXcCtaPag
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit3: TDBEdit
        Left = 152
        Top = 160
        Width = 553
        Height = 21
        DataField = 'NO_EXcGruCta'
        DataSource = DsEXcCtaPag
        TabOrder = 6
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 160
        Width = 134
        Height = 21
        DataField = 'EXcGruCta'
        DataSource = DsEXcCtaPag
        TabOrder = 7
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 76
        Top = 72
        Width = 269
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NoEN'
        DataSource = DsEXcCtaPag
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 348
        Top = 72
        Width = 361
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NomGruEN'
        DataSource = DsEXcCtaPag
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 323
      Width = 838
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 158
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 332
        Top = 15
        Width = 504
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 371
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 838
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 790
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 574
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 427
        Height = 32
        Caption = 'Cadastro de Contas de Pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 427
        Height = 32
        Caption = 'Cadastro de Contas de Pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 427
        Height = 32
        Caption = 'Cadastro de Contas de Pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 838
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 834
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrEXcCtaPag: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrEXcCtaPagBeforeOpen
    AfterOpen = QrEXcCtaPagAfterOpen
    AfterScroll = QrEXcCtaPagAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM cadastro_simples')
    Left = 64
    Top = 64
    object QrEXcCtaPagCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEXcCtaPagNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
    object QrEXcCtaPagOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEXcCtaPagAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEXcCtaPagImgB64: TWideMemoField
      FieldName = 'ImgB64'
      BlobType = ftWideMemo
    end
    object QrEXcCtaPagImgPath: TWideStringField
      FieldName = 'ImgPath'
      Size = 511
    end
    object QrEXcCtaPagNomGrupo: TWideStringField
      FieldName = 'NomGrupo'
      Size = 60
    end
    object QrEXcCtaPagPngB64: TWideMemoField
      FieldName = 'PngB64'
      BlobType = ftWideMemo
    end
    object QrEXcCtaPagNO_EXcGruCta: TWideStringField
      FieldName = 'NO_EXcGruCta'
      Size = 60
    end
    object QrEXcCtaPagEXcGruCta: TIntegerField
      FieldName = 'EXcGruCta'
    end
    object QrEXcCtaPagNoEN: TWideStringField
      FieldName = 'NoEN'
      Size = 60
    end
    object QrEXcCtaPagNomGruEN: TWideStringField
      FieldName = 'NomGruEN'
      Size = 60
    end
  end
  object DsEXcCtaPag: TDataSource
    DataSet = QrEXcCtaPag
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object PMAltera: TPopupMenu
    Left = 340
    Top = 312
    object Dados1: TMenuItem
      Caption = '&Dados'
      OnClick = Dados1Click
    end
    object Imagem1: TMenuItem
      Caption = '&Imagem'
      OnClick = Imagem1Click
    end
    object ImagemMobilepng1: TMenuItem
      Caption = 'Imagem Mobile (png)'
      OnClick = ImagemMobilepng1Click
    end
  end
  object QrEXcGruCta: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT *'
      'FROM excgructa'
      'ORDER BY Nome'
      '')
    Left = 260
    Top = 92
    object QrEXcGruCtaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEXcGruCtaNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEXcGruCta: TDataSource
    DataSet = QrEXcGruCta
    Left = 292
    Top = 92
  end
end
