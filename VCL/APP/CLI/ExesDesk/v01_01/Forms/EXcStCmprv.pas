unit EXcStCmprv;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkCheckBox, Vcl.Menus;

type
  TFmEXcStCmprv = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEXcStCmprv: TMySQLQuery;
    QrEXcStCmprvCodigo: TIntegerField;
    QrEXcStCmprvNome: TWideStringField;
    DsEXcStCmprv: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrEXcStCmprvTipoDoc: TSmallintField;
    QrEXcStCmprvObtmDoc: TSmallintField;
    QrEXcStCmprvAtivo: TSmallintField;
    RGTipoDoc: TdmkRadioGroup;
    CkObtmDoc: TdmkCheckBox;
    CkAtivo: TdmkCheckBox;
    QrEXcStCmprvNO_TipoDoc: TWideStringField;
    DBRadioGroup1: TDBRadioGroup;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    PnImg: TPanel;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    QrEXcStCmprvImgB64: TWideMemoField;
    QrEXcStCmprvImgPath: TWideStringField;
    PMAltera: TPopupMenu;
    Dados1: TMenuItem;
    Imagem1: TMenuItem;
    ImagemMobilepng1: TMenuItem;
    Panel6: TPanel;
    Panel7: TPanel;
    ImgImgB64: TImage;
    Panel8: TPanel;
    ImgPngB64: TImage;
    QrEXcStCmprvPngB64: TWideMemoField;
    Label3: TLabel;
    Label4: TLabel;
    EdNoEN: TdmkEdit;
    QrEXcStCmprvNoEN: TWideStringField;
    QrEXcStCmprvPngPath: TWideStringField;
    dmkDBEdit1: TdmkDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEXcStCmprvAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEXcStCmprvBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure Dados1Click(Sender: TObject);
    procedure Imagem1Click(Sender: TObject);
    procedure QrEXcStCmprvAfterScroll(DataSet: TDataSet);
    procedure ImagemMobilepng1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEXcStCmprv: TFmEXcStCmprv;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnExesD_PF, MyGlyfs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEXcStCmprv.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEXcStCmprv.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEXcStCmprvCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEXcStCmprv.DefParams;
begin
  VAR_GOTOTABELA := 'excstcmprv';
  VAR_GOTOMYSQLTABLE := QrEXcStCmprv;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT scm.*, ');
  VAR_SQLx.Add('ELT(scm.TipoDoc+1, "Definitivo", "Provisório", "Pendente", "?????") NO_TipoDoc ');
  VAR_SQLx.Add('FROM excstcmprv scm ');
  VAR_SQLx.Add('WHERE scm.Codigo > 0');
  //
  VAR_SQL1.Add('AND scm.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND scm.Nome Like :P0');
  //
end;

procedure TFmEXcStCmprv.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEXcStCmprv.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEXcStCmprv.Dados1Click(Sender: TObject);
begin
  if (QrEXcStCmprv.State <> dsInactive) and (QrEXcStCmprv.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEXcStCmprv, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'excstcmprv');
  end;
end;

procedure TFmEXcStCmprv.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmEXcStCmprv.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEXcStCmprv.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEXcStCmprv.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEXcStCmprv.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEXcStCmprv.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEXcStCmprv.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmEXcStCmprv.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEXcStCmprvCodigo.Value;
  Close;
end;

procedure TFmEXcStCmprv.BtConfirmaClick(Sender: TObject);
var
  Nome, NoEN: String;
  Codigo, TipoDoc, ObtmDoc: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  NoEN           := EdNoEN.Text;
  TipoDoc        := RGTipoDoc.ItemIndex;
  ObtmDoc        := Geral.BoolToInt(CkObtmDoc.Checked);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  if MyObjects.FIC(TipoDoc < 0, RGTipoDoc, 'Defina o status do documento!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('excstcmprv', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'excstcmprv', False, [
  'Nome', 'NoEN', 'TipoDoc', 'ObtmDoc'], [
  'Codigo'], [
  Nome, NoEN, TipoDoc, ObtmDoc], [
  Codigo], True) then
  begin
    if UnDmkDAC_PF.AtivaDesativaRegistroIdx1(Dmod.MyDB, 'excstcmprv', 'Codigo',
    'Ativo', Codigo, CkAtivo.Checked) then
    begin
      ImgTipo.SQLType := stLok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
      GOTOy.BotoesSb(ImgTipo.SQLType);
      //
      LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmEXcStCmprv.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'excstcmprv', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEXcStCmprv.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEXcStCmprv, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'excstcmprv');
end;

procedure TFmEXcStCmprv.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PnImg.Align := alClient;
  CriaOForm;
end;

procedure TFmEXcStCmprv.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEXcStCmprvCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcStCmprv.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEXcStCmprv.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEXcStCmprvCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcStCmprv.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEXcStCmprv.QrEXcStCmprvAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEXcStCmprv.QrEXcStCmprvAfterScroll(DataSet: TDataSet);
begin
  ExesD_PF.MostraImagem(QrEXcStCmprvImgB64.Value, ImgImgB64, PnImg);
  FmMyGlyfs.CarregaPNGDoBD(ImgPngB64, QrEXcStCmprvPngB64.Value);
end;

procedure TFmEXcStCmprv.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEXcStCmprv.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEXcStCmprvCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'excstcmprv', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEXcStCmprv.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEXcStCmprv.Imagem1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrEXcStCmprvCodigo.Value;
  if ExesD_PF.CarregaImagemRegistro(Self, 'excstcmprv', 'ImgB64', 'ImgPath',
  Codigo, 192, 192) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEXcStCmprv.ImagemMobilepng1Click(Sender: TObject);
var
  Codigo: Integer;
  PngB64, PngPath: String;
begin
  Codigo := QrEXcStCmprvCodigo.Value;
  if FmMyGlyfs.SalvaPNGDeArquivoNoBD(Self, 192, 192, PngB64, PngPath) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd,  'excstcmprv', False, [
    'PngB64', 'PngPath'], [
    'Codigo'], [
     PngB64, PngPath], [
     Codigo], True) then
     begin
       LocCod(Codigo, Codigo);
     end;
  end;
end;

procedure TFmEXcStCmprv.QrEXcStCmprvBeforeOpen(DataSet: TDataSet);
begin
  QrEXcStCmprvCodigo.DisplayFormat := FFormatFloat;
end;

end.

