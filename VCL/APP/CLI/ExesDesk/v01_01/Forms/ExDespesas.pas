unit ExDespesas;

interface

uses
  Windows, System.UITypes, Messages, SysUtils, Classes, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, Vcl.Imaging.jpeg,
  FMX.Graphics, Soap.EncdDecd,
  VCL.Graphics;

type
  TFmExDespesas = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPeriodos: TMySQLQuery;
    DsPeriodos: TDataSource;
    PnPeriodo: TPanel;
    QrPeriodosAnoMes: TWideStringField;
    QrPeriodosAno: TIntegerField;
    QrPeriodosMes: TIntegerField;
    QrEXcMdoPag: TMySQLQuery;
    DsEXcMdoPag: TDataSource;
    QrEXcMdoPagCodigo: TIntegerField;
    QrEXcMdoPagNome: TWideStringField;
    QrEXcCtaPag: TMySQLQuery;
    QrEXcCtaPagCodigo: TIntegerField;
    QrEXcCtaPagNome: TWideStringField;
    DsEXcCtaPag: TDataSource;
    QrEXmDspDevIts: TMySQLQuery;
    DsEXmDspDevIts: TDataSource;
    PnModo: TPanel;
    PnGrids: TPanel;
    PnConta: TPanel;
    DBGConra: TDBGrid;
    DBGLanctos: TDBGrid;
    PnFiltros: TPanel;
    DBGDocs: TDBGrid;
    QrEXmDspDevFts: TMySQLQuery;
    DsEXmDspDevFts: TDataSource;
    QrEXmDspDevItsCodigo: TIntegerField;
    QrEXmDspDevItsCodInMob: TIntegerField;
    QrEXmDspDevItsCtrlInMob: TIntegerField;
    QrEXmDspDevItsAnoMes: TWideStringField;
    QrEXmDspDevItsAno: TIntegerField;
    QrEXmDspDevItsMes: TIntegerField;
    QrEXmDspDevItsEXcIdFunc: TIntegerField;
    QrEXmDspDevItsData: TDateField;
    QrEXmDspDevItsHora: TTimeField;
    QrEXmDspDevItsEXcCtaPag: TIntegerField;
    QrEXmDspDevItsEXcMdoPag: TIntegerField;
    QrEXmDspDevItsSiglaOri: TWideStringField;
    QrEXmDspDevItsCotacOri: TFloatField;
    QrEXmDspDevItsValorOri: TFloatField;
    QrEXmDspDevItsSiglaInt: TWideStringField;
    QrEXmDspDevItsValorInt: TFloatField;
    QrEXmDspDevItsSiglaDst: TWideStringField;
    QrEXmDspDevItsCotacDst: TFloatField;
    QrEXmDspDevItsValorDst: TFloatField;
    QrEXmDspDevItsAtivo: TSmallintField;
    QrEXmDspDevFtsCodigo: TIntegerField;
    QrEXmDspDevFtsCodInMob: TIntegerField;
    QrEXmDspDevFtsCtrlInMob: TIntegerField;
    QrEXmDspDevFtsIDTabela: TIntegerField;
    QrEXmDspDevFtsDataHora: TDateTimeField;
    QrEXmDspDevFtsCtrlFoto: TIntegerField;
    QrEXmDspDevFtsEXcStCmprv: TIntegerField;
    QrEXmDspDevFtsNomeArq: TWideStringField;
    QrEXmDspDevFtsNoArqSvr: TWideStringField;
    QrEXmDspDevFtsAtivo: TSmallintField;
    PnFotoENome: TPanel;
    ImgFoto: TImage;
    LaNomeFoto: TLabel;
    QrEXmDspDevBmp: TMySQLQuery;
    QrEXmDspDevBmpTxtBmp: TWideMemoField;
    Splitter1: TSplitter;
    Panel3: TPanel;
    DBGPeriodos: TDBGrid;
    DBGModos: TDBGrid;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    QrEXmDspDevFtsNO_EXcStCmprv: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPeriodosBeforeClose(DataSet: TDataSet);
    procedure QrPeriodosAfterScroll(DataSet: TDataSet);
    procedure QrEXcMdoPagAfterScroll(DataSet: TDataSet);
    procedure QrEXcMdoPagBeforeClose(DataSet: TDataSet);
    procedure QrEXcCtaPagAfterScroll(DataSet: TDataSet);
    procedure QrEXcCtaPagBeforeClose(DataSet: TDataSet);
    procedure QrEXmDspDevItsAfterScroll(DataSet: TDataSet);
    procedure QrEXmDspDevItsBeforeClose(DataSet: TDataSet);
    procedure QrEXmDspDevFtsAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
     procedure ReopenEXcMdoPag();
     procedure ReopenEXcCtaPag();
     procedure ReopenEXmDspDevIts();
     procedure ReopenEXmDspDevFts();
     //
     function  ConvertFmxBitmapToVclBitmap(b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
  public
    { Public declarations }
    FEXcIdFunc: Integer;
    //
    procedure ReopenPeriodos();
  end;

  var
  FmExDespesas: TFmExDespesas;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UnExes_ProjGroupVars, UnDmkImg, UnExesD_PF,
  UMySQLModule;

{$R *.DFM}

function BitmapFromBase64(const base64: string):FMX.Graphics.TBitmap;
var
  Input: TStringStream;
  Output: TBytesStream;
begin
  Input := TStringStream.Create(base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Soap.EncdDecd.DecodeStream(Input, Output);
      Output.Position := 0;
      Result := FMX.Graphics.TBitmap.Create;
      try
        Result.LoadFromStream(Output);
      except
        Result.Free;
        raise;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TFmExDespesas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmExDespesas.ConvertFmxBitmapToVclBitmap(
  b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
var
  data:FMX.Graphics.TBitmapData;
  i,j:Integer;
  AlphaColor: System.UITypes.TAlphaColor;
begin
  Result := VCL.Graphics.TBitmap.Create;
  Result.SetSize(b.Width, b.Height);
  if(b.Map(TMapAccess.Readwrite, data)) then
  try
    for i := 0 to data.Height-1 do
    begin
      for j := 0 to data.Width-1 do
      begin
        AlphaColor:=data.GetPixel(j,i);
        Result.Canvas.Pixels[j,i]:=
          RGB(
            TAlphaColorRec(AlphaColor).R,
            TAlphaColorRec(AlphaColor).G,
            TAlphaColorRec(AlphaColor).B
          );
      end;
    end;
  finally
    b.Unmap(data);
  end;
end;

procedure TFmExDespesas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmExDespesas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmExDespesas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmExDespesas.QrEXcCtaPagAfterScroll(DataSet: TDataSet);
begin
  ReopenEXmDspDevIts();
end;

procedure TFmExDespesas.QrEXcCtaPagBeforeClose(DataSet: TDataSet);
begin
  QrEXmDspDevIts.Close;
end;

procedure TFmExDespesas.QrEXcMdoPagAfterScroll(DataSet: TDataSet);
begin
  ReopenEXcCtaPag();
end;

procedure TFmExDespesas.QrEXcMdoPagBeforeClose(DataSet: TDataSet);
begin
  QrEXcCtaPag.Close;
end;

procedure TFmExDespesas.QrEXmDspDevFtsAfterScroll(DataSet: TDataSet);
var
  NomeArq, NoArqSvr, Base64: AnsiString;
  FMX_Bitmap: FMX.Graphics.TBitmap;
  VCL_Bitmap: VCL.Graphics.TBitmap;
  Codigo, EXcIDFunc, CtrlInMob, IdTabela, CtrlFoto: Integer;
  DataHora: TDateTime;
  SiglaOri: String;
  CriarArqLocal: Boolean;
  ValorOri: Double;
begin
  if VAR_DIR_FOTOS_DESPESAS = EmptyStr then Exit;
  CriarArqLocal := False;
  NomeArq := VAR_DIR_FOTOS_DESPESAS + QrEXmDspDevFtsNoArqSvr.Value;
  if QrEXmDspDevFtsNoArqSvr.Value <> EmptyStr then
  begin
    if FileExists(NomeArq) then
      ImgFoto.Picture.Bitmap.LoadFromFile(NomeArq)
    else
      CriarArqLocal := True;
  end else
    CriarArqLocal := True;
  if CriarArqLocal then
  begin
    if not DirectoryExists(VAR_DIR_FOTOS_DESPESAS) then
      ForceDirectories(VAR_DIR_FOTOS_DESPESAS);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevBmp, Dmod.MyDB, [
    'SELECT bmp.TxtBmp ',
    'FROM exmdspdevbmp bmp',
    'WHERE bmp.Codigo=' + Geral.FF0(QrEXmDspDevFtsCodigo.Value),
    'AND bmp.CodInMob=' + Geral.FF0(QrEXmDspDevFtsCodInMob.Value),
    'AND bmp.CtrlFoto=' + Geral.FF0(QrEXmDspDevFtsCtrlFoto.Value),
    'ORDER BY bmp.Pedaco ',
    EmptyStr]);
    //
    Base64 := '';
    QrEXmDspDevBmp.First;
    while not QrEXmDspDevBmp.Eof do
    begin
      Base64 := Base64 + QrEXmDspDevBmpTxtBmp.Value;
      //
      QrEXmDspDevBmp.Next;
    end;
    if Base64 <> EmptyStr then
    begin
      FMX_BitMap := FMX.Graphics.TBitmap.Create;
      VCL_BitMap := VCL.Graphics.TBitmap.Create;
      //
      FMX_Bitmap := BitmapFromBase64(Base64);
      //Converter aqui de FMX para VCL!
      VCL_BitMap := ConvertFmxBitmapToVclBitmap(FMX_Bitmap);
      //
      ImgFoto.Picture.Bitmap.Assign(VCL_Bitmap);
      Codigo    := QrEXmDspDevFtsCodigo.Value;
      EXcIdFunc := FExcIdFunc;
      CtrlInMob := QrEXmDspDevFtsCtrlInMob.Value;
      DataHora  := QrEXmDspDevFtsDataHora.Value;
      IdTabela  := QrEXmDspDevFtsIDTabela.Value;
      CtrlFoto  := QrEXmDspDevFtsCtrlFoto.Value;
      SiglaOri  := QrEXmDspDevItsSiglaOri.Value;
      ValorOri  := QrEXmDspDevItsValorOri.Value;
      NoArqSvr  := ExesD_PF.DefineNomeArquivoFotoDespesa(Codigo, CtrlInMob,
        EXcIdFunc, SiglaOri, ValorOri, DataHora);
      NomeArq := VAR_DIR_FOTOS_DESPESAS + NoArqSvr;
      ImgFoto.Picture.Bitmap.SaveToFile(NomeArq);
      if FileExists(NomeArq) and (QrEXmDspDevFtsNoArqSvr.Value = EmptyStr) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'exmdspdevfts', False, [
        'NoArqSvr'], [
        'Codigo', 'IdTabela', 'CtrlInMob', 'CtrlFoto'], [
        NoArqSvr], [
        Codigo, IdTabela, CtrlInMob, CtrlFoto], True) then
        begin
          // ???? Excluir dados da tabela de exmdspdevbmp?????
        end;
      end;
    end;
  end;
end;

procedure TFmExDespesas.QrEXmDspDevItsAfterScroll(DataSet: TDataSet);
begin
  ReopenEXmDspDevFts();
end;

procedure TFmExDespesas.QrEXmDspDevItsBeforeClose(DataSet: TDataSet);
begin
  QrEXmDspDevFts.Close;
  if ImgFoto.Picture <> nil then
    ImgFoto.Picture := nil;
end;

procedure TFmExDespesas.QrPeriodosAfterScroll(DataSet: TDataSet);
begin
  ReopenEXcMdoPag();
end;

procedure TFmExDespesas.QrPeriodosBeforeClose(DataSet: TDataSet);
begin
  QrEXcMdoPag.Close;
end;

procedure TFmExDespesas.ReopenEXcCtaPag();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXcCtaPag, Dmod.MyDB, [
  'SELECT DISTINCT cct.Codigo, cct.Nome',
  'FROM excctapag cct ',
  'LEFT JOIN exmdspdevits its ON cct.Codigo=its.EXcCtaPag',
  'LEFT JOIN exmdspdevlct lct ON its.Codigo=lct.Codigo',
  'WHERE lct.EXcIdFunc=' + Geral.FF0(FEXcIdFunc),
  'AND lct.Ano=' + Geral.FF0(QrPeriodosAno.Value),
  'AND lct.Mes=' + Geral.FF0(QrPeriodosMes.Value),
  'AND its.EXcMdoPag=' + Geral.FF0(QrEXcMdoPagCodigo.Value),
  'ORDER BY cct.Ordem',
  '']);
end;

procedure TFmExDespesas.ReopenEXcMdoPag();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXcMdoPag, Dmod.MyDB, [
  'SELECT DISTINCT cmp.Codigo, cmp.Nome',
  'FROM excmdopag cmp ',
  'LEFT JOIN exmdspdevits its ON cmp.Codigo=its.EXcMdoPag',
  'LEFT JOIN exmdspdevlct lct ON its.Codigo=lct.Codigo',
  'WHERE lct.EXcIdFunc=' + Geral.FF0(FEXcIdFunc),
  'AND lct.Ano=' + Geral.FF0(QrPeriodosAno.Value),
  'AND lct.Mes=' + Geral.FF0(QrPeriodosMes.Value),
  'ORDER BY cmp.Ordem',
  '']);
end;

procedure TFmExDespesas.ReopenEXmDspDevFts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevFts, Dmod.MyDB, [
  'SELECT scm.Nome NO_EXcStCmprv, fts.* ',
  'FROM exmdspdevfts fts  ',
  'LEFT JOIN excstcmprv scm ON scm.Codigo=fts.EXcStCmprv ',
  'WHERE fts.Codigo=' + Geral.FF0(QrEXmDspDevItsCodigo.Value),
  'AND fts.CodInMob=' + Geral.FF0(QrEXmDspDevItsCodInMob.Value),
  '']);
  //Geral.MB_Teste(QrEXmDspDevFts.SQL.Text);
end;

procedure TFmExDespesas.ReopenEXmDspDevIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevIts, Dmod.MyDB, [
  'SELECT its.*',
  'FROM exmdspdevits its ',
  'WHERE its.EXcIdFunc=' + Geral.FF0(FEXcIdFunc),
  'AND its.Ano=' + Geral.FF0(QrPeriodosAno.Value),
  'AND its.Mes=' + Geral.FF0(QrPeriodosMes.Value),
  'AND EXcMdoPag=' + Geral.FF0(QrEXcMdoPagCodigo.Value),
  'AND EXcCtaPag=' + Geral.FF0(QrEXcCtaPagCodigo.Value),
  '']);
end;

procedure TFmExDespesas.ReopenPeriodos();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPeriodos, Dmod.MyDB, [
  'SELECT DISTINCT AnoMes, Ano, Mes',
  'FROM exmdspdevlct',
  'WHERE EXcIdFunc=' + Geral.FF0(FEXcIdFunc),
  'ORDER BY Ano DESC, Mes DESC',
  '']);
end;

end.
