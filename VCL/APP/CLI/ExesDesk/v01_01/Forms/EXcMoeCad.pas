unit EXcMoeCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkCheckBox, Vcl.Menus;

type
  TFmEXcMoeCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEXcMoeCad: TMySQLQuery;
    QrEXcMoeCadCodigo: TIntegerField;
    QrEXcMoeCadNome: TWideStringField;
    DsEXcMoeCad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    DBCheckBox2: TDBCheckBox;
    CkAtivo: TdmkCheckBox;
    QrEXcMoeCadOrdem: TIntegerField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    EdOrdem: TdmkEdit;
    Label4: TLabel;
    QrEXcMoeCadAtivo: TSmallintField;
    PMAltera: TPopupMenu;
    Dados1: TMenuItem;
    Imagem1: TMenuItem;
    QrEXcMoeCadImgB64: TWideMemoField;
    QrEXcMoeCadImgPath: TWideStringField;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    PnImg: TPanel;
    EdSigla: TdmkEdit;
    Label6: TLabel;
    QrEXcMoeCadSigla: TWideStringField;
    ImagemMobilepng1: TMenuItem;
    QrEXcMoeCadPngB64: TWideMemoField;
    PnBotoes: TPanel;
    Panel7: TPanel;
    ImgImgB64: TImage;
    Panel8: TPanel;
    ImgPngB64: TImage;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEXcMoeCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEXcMoeCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure Dados1Click(Sender: TObject);
    procedure Imagem1Click(Sender: TObject);
    procedure QrEXcMoeCadAfterScroll(DataSet: TDataSet);
    procedure ImagemMobilepng1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEXcMoeCad: TFmEXcMoeCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkImg, UnExesD_PF, MyGlyfs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEXcMoeCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEXcMoeCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEXcMoeCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEXcMoeCad.DefParams;
begin
  VAR_GOTOTABELA := 'excmoecad';
  VAR_GOTOMYSQLTABLE := QrEXcMoeCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM excmoecad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmEXcMoeCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEXcMoeCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEXcMoeCad.Dados1Click(Sender: TObject);
begin
  if (QrEXcMoeCad.State <> dsInactive) and (QrEXcMoeCad.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEXcMoeCad, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'excmoecad');
  end;
end;

procedure TFmEXcMoeCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEXcMoeCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEXcMoeCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEXcMoeCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEXcMoeCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEXcMoeCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEXcMoeCad.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmEXcMoeCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEXcMoeCadCodigo.Value;
  Close;
end;

procedure TFmEXcMoeCad.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla: String;
  Codigo, Ordem: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Sigla          := Uppercase(EdSigla.ValueVariant);
  Ordem          := EdOrdem.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //

  //
  Codigo := UMyMod.BPGS1I32('excmoecad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'excmoecad', False, [
  'Nome', 'Sigla', 'Ordem'], [
  'Codigo'], [
  Nome, Sigla, Ordem], [
  Codigo], True) then
  begin
    if UnDmkDAC_PF.AtivaDesativaRegistroIdx1(Dmod.MyDB, 'excmoecad', 'Codigo',
    'Ativo', Codigo, CkAtivo.Checked) then
    begin
      ImgTipo.SQLType := stLok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
      GOTOy.BotoesSb(ImgTipo.SQLType);
      //
      LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmEXcMoeCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'excmoecad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEXcMoeCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEXcMoeCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'excmoecad');
end;

procedure TFmEXcMoeCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alLeft;
  PnBotoes.Align := alLeft;
  PnImg.Align := alClient;
  CriaOForm;
end;

procedure TFmEXcMoeCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEXcMoeCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcMoeCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEXcMoeCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEXcMoeCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcMoeCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEXcMoeCad.QrEXcMoeCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEXcMoeCad.QrEXcMoeCadAfterScroll(DataSet: TDataSet);
var
  Bmp2: TBitmap;
  ImgB64: String;
begin
  ImgB64 := QrExcMoeCadImgB64.Value;
  if ImgB64 <> EmptyStr then
  begin
    Bmp2 := TBitmap.Create;
    try
      Bmp2 := DmkImg.BitmapFromBase64(ImgB64);
      ImgImgB64.Picture.Bitmap.Assign(Bmp2);
      ImgImgB64.Visible := True;
      PnDados.Visible := False;
      PnDados.Visible := True;
    finally
      Bmp2.Free;
    end;
  end else
    ImgImgB64.Visible := False;
  //
  FmMyGlyfs.CarregaPNGDoBD(ImgPngB64, QrEXcMoeCadPngB64.Value);
end;

procedure TFmEXcMoeCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEXcMoeCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEXcMoeCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'excmoecad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEXcMoeCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEXcMoeCad.Imagem1Click(Sender: TObject);
{
const
  Titulo = 'Defini��o Imagem de bot�o';
var
  IniDir, Arquivo, Filtro, ImgB64, ImgPath: String;
  Bmp1, Bmp2: TBitmap;
  Codigo: Integer;
begin
  IniDir  := 'C:\Dermatek\';
  Arquivo := '';
  Filtro  := 'Arquivos Bitmap|*.bmp;Arquivos JPEG|*.jpeg;Arquivos JPG|*.jpg';
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, Titulo, Filtro, [], Arquivo) then
  begin
    if FileExists(Arquivo) then
    begin
      Screen.Cursor := crHourGlass;
      try
        Bmp1 := TBitmap.Create;
        try
          Bmp1.LoadFromFile(Arquivo);
          //ImgB64 := DmkImg.Base64FromBitmap2(Bitmap);
          ImgB64 := DmkImg.Base64FromBitmap(Bmp1);
          ImgPath := Arquivo;
          Codigo := QrEXcMoeCadCodigo.Value;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'excmoecad', False, [
          'ImgB64', 'ImgPath'], [
          'Codigo'], [
          ImgB64, ImgPath], [
          Codigo], True) then
          begin
            LocCod(Codigo, Codigo);
          end;
        finally
          Bmp1.Free;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;
}
var
  Codigo: Integer;
begin
  Codigo := QrEXcMoeCadCodigo.Value;
  if ExesD_PF.CarregaImagemRegistro(Self, 'excmoecad', 'ImgB64', 'ImgPath',
  Codigo, 192, 192) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEXcMoeCad.ImagemMobilepng1Click(Sender: TObject);
var
  Codigo: Integer;
  PngB64, PngPath: String;
begin
  Codigo := QrEXcMoeCadCodigo.Value;
  if FmMyGlyfs.SalvaPNGDeArquivoNoBD(Self, 192, 192, PngB64, PngPath) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd,  'excmoecad', False, [
    'PngB64', 'PngPath'], [
    'Codigo'], [
     PngB64, PngPath], [
     Codigo], True) then
     begin
       LocCod(Codigo, Codigo);
     end;
  end;
end;

procedure TFmEXcMoeCad.QrEXcMoeCadBeforeOpen(DataSet: TDataSet);
begin
  QrEXcMoeCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

