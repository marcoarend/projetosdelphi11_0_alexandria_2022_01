object FmLoclzCtaEnti: TFmLoclzCtaEnti
  Left = 339
  Top = 185
  Caption = 'LNG-INTLZ-101 :: Localiza'#231#227'o: Entidades x Contas de pagamento'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 576
        Height = 32
        Caption = 'Localiza'#231#227'o: Entidades x Contas de pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 576
        Height = 32
        Caption = 'Localiza'#231#227'o: Entidades x Contas de pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 576
        Height = 32
        Caption = 'Localiza'#231#227'o: Entidades x Contas de pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtAltera: TBitBtn
        Tag = 11
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera item'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAlteraClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter1: TSplitter
      Left = 209
      Top = 29
      Width = 5
      Height = 438
      ExplicitLeft = 320
      ExplicitTop = 81
      ExplicitHeight = 386
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 29
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 8
        Width = 100
        Height = 13
        Caption = 'Localiza'#231#227'o (Idioma):'
      end
      object SpeedButton1: TSpeedButton
        Left = 660
        Top = 3
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object DBEdit2: TDBEdit
        Left = 116
        Top = 4
        Width = 540
        Height = 21
        DataField = 'NO_loclzl10ncab'
        DataSource = DsCompanies
        TabOrder = 0
      end
    end
    object DBGCompanies: TDBGrid
      Left = 0
      Top = 29
      Width = 209
      Height = 438
      Align = alLeft
      DataSource = DsCompanies
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Entidade'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ENT'
          Title.Caption = 'Nome da Compania'
          Width = 140
          Visible = True
        end>
    end
    object DBGContas: TDBGrid
      Left = 214
      Top = 29
      Width = 794
      Height = 438
      Align = alClient
      DataSource = DsLoclzCtaEnti
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGContasCellClick
      OnDrawColumnCell = DBGContasDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 43
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NoCtaPadr'
          Title.Caption = 'Descri'#231#227'o'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NoEN'
          Title.Caption = 'Descri'#231#227'o em ingl'#234's'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Texto no Idioma selecionado'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomGrupo'
          Title.Caption = 'Descri'#231#227'o do Grupo'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomGruEN'
          Title.Caption = 'Descri'#231#227'o do Grupo em ingl'#234's'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TxtGruEN'
          Title.Caption = 'Descri'#231#227'o do Grupo selecionado'
          Width = 140
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrCompanies: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrCompaniesBeforeClose
    AfterScroll = QrCompaniesAfterScroll
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial,  cny.LoclzL10nCab,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'llc.Nome NO_loclzl10ncab  '
      'FROM entidades ent'
      'LEFT JOIN prmscompny   cny ON cny.Entidade=ent.Codigo'
      'LEFT JOIN loclzl10ncab llc ON llc.Codigo=cny.LoclzL10nCab '
      'WHERE ent.Codigo < -10 '
      'AND ent.Filial > 0 '
      'ORDER BY NO_ENT ')
    Left = 384
    Top = 284
    object QrCompaniesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCompaniesFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrCompaniesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 120
    end
    object QrCompaniesNO_loclzl10ncab: TWideStringField
      FieldName = 'NO_loclzl10ncab'
      Size = 60
    end
    object QrCompaniesLoclzL10nCab: TIntegerField
      FieldName = 'LoclzL10nCab'
    end
  end
  object DsCompanies: TDataSource
    DataSet = QrCompanies
    Left = 384
    Top = 340
  end
  object QrLoclzCtaEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lce.Ativo, cta.Codigo, cta.Nome NoCtaPadr, '
      'IF(lce.Ativo IS NULL, "N", IF(lce.Ativo=0, "I", "A")) SIT,'
      'cta.NoEN, cta.NomGrupo, NomGruEN, llc.Texto, llc.TxtGruEN  '
      'FROM excctapag cta '
      'LEFT JOIN loclzctaenti lce ON lce.EXcCtaPag=cta.Codigo '
      '  AND lce.Entidade=-11'
      'LEFT JOIN loclzl10ncta llc ON llc.EXcCtaPag=cta.Codigo '
      '  AND lce.Entidade=-11')
    Left = 560
    Top = 264
    object QrLoclzCtaEntiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLoclzCtaEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLoclzCtaEntiNoCtaPadr: TWideStringField
      FieldName = 'NoCtaPadr'
      Size = 60
    end
    object QrLoclzCtaEntiSIT: TWideStringField
      FieldName = 'SIT'
      Size = 1
    end
    object QrLoclzCtaEntiTexto: TWideStringField
      FieldName = 'Texto'
      Size = 60
    end
    object QrLoclzCtaEntiNoEN: TWideStringField
      FieldName = 'NoEN'
      Required = True
      Size = 60
    end
    object QrLoclzCtaEntiNomGrupo: TWideStringField
      FieldName = 'NomGrupo'
      Required = True
      Size = 60
    end
    object QrLoclzCtaEntiNomGruEN: TWideStringField
      FieldName = 'NomGruEN'
      Required = True
      Size = 60
    end
    object QrLoclzCtaEntiTxtGruEN: TWideStringField
      FieldName = 'TxtGruEN'
      Size = 60
    end
  end
  object DsLoclzCtaEnti: TDataSource
    DataSet = QrLoclzCtaEnti
    Left = 560
    Top = 312
  end
end
