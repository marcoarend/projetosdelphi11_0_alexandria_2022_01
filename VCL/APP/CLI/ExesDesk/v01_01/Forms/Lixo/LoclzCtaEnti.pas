unit LoclzCtaEnti;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, Vcl.Mask;

type
  TFmLoclzCtaEnti = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAltera: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrCompanies: TMySQLQuery;
    DsCompanies: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGCompanies: TDBGrid;
    Splitter1: TSplitter;
    DBGContas: TDBGrid;
    QrCompaniesCodigo: TIntegerField;
    QrCompaniesFilial: TIntegerField;
    QrCompaniesNO_ENT: TWideStringField;
    QrLoclzCtaEnti: TMySQLQuery;
    DsLoclzCtaEnti: TDataSource;
    QrLoclzCtaEntiAtivo: TSmallintField;
    QrLoclzCtaEntiCodigo: TIntegerField;
    QrLoclzCtaEntiNoCtaPadr: TWideStringField;
    QrLoclzCtaEntiSIT: TWideStringField;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    QrCompaniesNO_loclzl10ncab: TWideStringField;
    DBEdit2: TDBEdit;
    QrLoclzCtaEntiTexto: TWideStringField;
    QrLoclzCtaEntiNoEN: TWideStringField;
    QrLoclzCtaEntiNomGrupo: TWideStringField;
    QrLoclzCtaEntiNomGruEN: TWideStringField;
    QrLoclzCtaEntiTxtGruEN: TWideStringField;
    QrCompaniesLoclzL10nCab: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrCompaniesAfterScroll(DataSet: TDataSet);
    procedure QrCompaniesBeforeClose(DataSet: TDataSet);
    procedure DBGContasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGContasCellClick(Column: TColumn);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCompanies(Entidade: Integer);
    procedure ReopenLoclzCtaEnti(Conta: Integer);
  public
    { Public declarations }
  end;

  var
  FmLoclzCtaEnti: TFmLoclzCtaEnti;

implementation

uses UnMyObjects, Module, DmkDAC_PF, MyVCLSkin, UMySQLModule, UnExesD_Jan;

{$R *.DFM}

const FFldAtiv = 'Ativo';

procedure TFmLoclzCtaEnti.BtAlteraClick(Sender: TObject);
begin
//
end;

procedure TFmLoclzCtaEnti.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLoclzCtaEnti.DBGContasCellClick(Column: TColumn);
var
  Ativo, IDInt: Integer;
  SQLType: TSQLType;
  Entidade, EXcCtaPag: Integer;
begin
  if Column.FieldName = FFldAtiv then
  begin
    Entidade := QrCompaniesCodigo.Value;
    EXcCtaPag := QrLoclzCtaEntiCodigo.Value;
    if QrLoclzCtaEntiSIT.Value = 'N' then
    begin
      SQLType := stIns;
      Ativo := 1;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loclzctaenti', False, [], [
      'Entidade', 'EXcCtaPag'], [], [
       Entidade, EXcCtaPag], True) then
       begin
         ReopenLoclzCtaEnti(EXcCtaPag);
       end;
    end else
    begin
      if QrLoclzCtaEntiAtivo.Value = 1 then
        Ativo := 0
      else
        Ativo := 1;
      Dmod.MyDB.Execute(Geral.ATS([
      'UPDATE loclzctaenti',
      'SET Ativo=' + Geral.FF0(Ativo),
      'WHERE Entidade=' + Geral.FF0(Entidade),
      'AND EXcCtaPag=' + Geral.FF0(EXcCtaPag),
      '']));
       ReopenLoclzCtaEnti(EXcCtaPag);
    end
  end;
end;

procedure TFmLoclzCtaEnti.DBGContasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = FFldAtiv then
    MeuVCLSkin.DrawGrid(DBGContas, Rect, 1, QrLoclzCtaEntiAtivo.Value);
end;

procedure TFmLoclzCtaEnti.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLoclzCtaEnti.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenCompanies(0);
end;

procedure TFmLoclzCtaEnti.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLoclzCtaEnti.QrCompaniesAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrCompaniesLoclzl10ncab.Value <> 0; //NO_loclzl10ncab
  ReopenLoclzCtaEnti(0);
end;

procedure TFmLoclzCtaEnti.QrCompaniesBeforeClose(DataSet: TDataSet);
begin
  QrLoclzCtaEnti.Close;
end;

procedure TFmLoclzCtaEnti.ReopenCompanies(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCompanies, Dmod.MyDB, [
  'SELECT ent.Codigo, ent.Filial, cny.LoclzL10nCab, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'llc.Nome NO_loclzl10ncab   ',
  'FROM entidades ent ',
  'LEFT JOIN prmscompny   cny ON cny.Entidade=ent.Codigo ',
  'LEFT JOIN loclzl10ncab llc ON llc.Codigo=cny.LoclzL10nCab  ',
  'WHERE ent.Codigo < -10  ',
  'AND ent.Filial > 0  ',
  'ORDER BY NO_ENT  ',
  '']);
  if Entidade <> 0 then
    QrCompanies.Locate('Codigo', Entidade, []);
end;



procedure TFmLoclzCtaEnti.ReopenLoclzCtaEnti(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoclzCtaEnti, Dmod.MyDB, [
  'SELECT lce.Ativo, cta.Codigo, cta.Nome NoCtaPadr, ',
  'IF(lce.Ativo IS NULL, "N", IF(lce.Ativo=0, "I", "A")) SIT,',
  'cta.NoEN, cta.NomGrupo, NomGruEN, llc.Texto, llc.TxtGruEN  ',
  'FROM excctapag cta ',
  'LEFT JOIN loclzctaenti lce ON lce.EXcCtaPag=cta.Codigo ',
  '  AND lce.Entidade=' + Geral.FF0(QrCompaniesCodigo.Value),
  'LEFT JOIN loclzl10ncta llc ON llc.EXcCtaPag=cta.Codigo ',
  '  AND lce.Entidade=' + Geral.FF0(QrCompaniesCodigo.Value),
  '']);
  if Conta > 0 then
    QrLoclzCtaEnti.Locate('Codigo', Conta, []);
end;

procedure TFmLoclzCtaEnti.SpeedButton1Click(Sender: TObject);
begin
  ExesD_Jan.MostraFormFmPrmsCompny(QrCompaniesCodigo.Value);
  ReopenCompanies(QrCompaniesCodigo.Value);
end;

end.
