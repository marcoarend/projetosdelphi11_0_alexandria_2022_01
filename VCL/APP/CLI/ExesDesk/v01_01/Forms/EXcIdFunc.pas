unit EXcIdFunc;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkDBLookupComboBox, dmkEditCB;

type
  TFmEXcIdFunc = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEXcIdFunc: TMySQLQuery;
    QrEXcIdFuncCodigo: TIntegerField;
    QrEXcIdFuncNome: TWideStringField;
    DsEXcIdFunc: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label3: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    SbEntidade: TSpeedButton;
    Label4: TLabel;
    QrEXcIdFuncEntidade: TIntegerField;
    QrEXcIdFuncNO_ENT: TWideStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrEntidades: TMySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    DsEntidades: TDataSource;
    QrEntidadesNO_ENT: TWideStringField;
    Label32: TLabel;
    DBEdit15: TDBEdit;
    QrEXcIdFuncIDCntrCst: TIntegerField;
    Label31: TLabel;
    EdIDCntrCst: TdmkEdit;
    EdSigla: TdmkEdit;
    Label5: TLabel;
    QrEXcIdFuncSigla: TWideStringField;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    EdCenCustOth: TdmkEdit;
    Label10: TLabel;
    EdCodFornOth: TdmkEdit;
    Label8: TLabel;
    QrEXcIdFuncCodFornOth: TWideStringField;
    QrEXcIdFuncCenCustOth: TWideStringField;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    EdCompany: TdmkEdit;
    Label13: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    DBEdit6: TDBEdit;
    QrEXcIdFuncCompany: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEXcIdFuncAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEXcIdFuncBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbEntidadeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEXcIdFunc: TFmEXcIdFunc;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEXcIdFunc.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEXcIdFunc.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEXcIdFuncCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEXcIdFunc.DefParams;
begin
  VAR_GOTOTABELA := 'excidfunc';
  VAR_GOTOMYSQLTABLE := QrEXcIdFunc;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT eif.*, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT');
  VAR_SQLx.Add('FROM excidfunc eif');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=eif.Entidade');
  VAR_SQLx.Add('WHERE eif.Codigo > 0');
  //
  VAR_SQL1.Add('AND eif.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND eif.Nome Like :P0');
  //
end;

procedure TFmEXcIdFunc.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEXcIdFunc.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEXcIdFunc.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEXcIdFunc.SbEntidadeClick(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdEntidade.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrEntidades, VAR_CADASTRO, 'Codigo');
  EdEntidade.SetFocus;
end;

procedure TFmEXcIdFunc.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEXcIdFunc.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEXcIdFunc.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEXcIdFunc.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEXcIdFunc.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEXcIdFunc.BtAlteraClick(Sender: TObject);
begin
  if (QrEXcIdFunc.State <> dsInactive) and (QrEXcIdFunc.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEXcIdFunc, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'excidfunc');
  end;
end;

procedure TFmEXcIdFunc.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEXcIdFuncCodigo.Value;
  VAR_ENTIDADE := QrEXcIdFuncEntidade.Value;
  Close;
end;

procedure TFmEXcIdFunc.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla: String;
  Codigo, Entidade, IDCntrCst: Integer;
  SQLType: TSQLType;
  CenCustOth, CodFornOth, Company: String;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  Sigla          := EdSigla.Text;
  Entidade       := EdEntidade.ValueVariant;
  IDCntrCst      := EdIDCntrCst.ValueVariant;
  CenCustOth     := EdCenCustOth.ValueVariant;
  CodFornOth     := EdCodFornOth.ValueVariant;
  Company        := EdCompany.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Entidade = 0, EdNome, 'Defina uma entidade!') then Exit;
  //
  //Codigo := UMyMod.BPGS1I32('excidfunc', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'excidfunc', False, [
  'Nome', 'Sigla', 'Entidade', 'Company',
  'IDCntrCst', 'CenCustOth', 'CodFornOth'], [
  'Codigo'], [
  Nome, Sigla, Entidade, Company,
  IDCntrCst, CenCustOth, CodFornOth], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEXcIdFunc.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'excidfunc', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEXcIdFunc.BtIncluiClick(Sender: TObject);
var
  Codigo: Integer;
  CodStr: String;
begin
  CodStr := '';
  if InputQuery('Nova Identidade Funcional', 'Informe o c�digo:', CodStr) then
  begin
    Codigo := Geral.IMV(CodStr);
    if Codigo > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM excidfunc ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      if Dmod.QrAux.RecordCount = 0 then
      begin
        UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEXcIdFunc, [PnDados],
        [PnEdita], EdNome, ImgTipo, 'excidfunc');
        EdCodigo.ValueVariant := Codigo;
      end else
      begin
        Geral.MB_Aviso('C�digo j� existe!');
        LocCod(Codigo, Codigo);
      end;
    end else
      Geral.MB_Aviso('C�digo inv�lido: ' + CodStr);
  end;
end;

procedure TFmEXcIdFunc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
 'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT ',
 'FROM entidades ',
 'ORDER BY NO_ENT ',
 ' ',
  '']);
end;

procedure TFmEXcIdFunc.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEXcIdFuncCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcIdFunc.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEXcIdFunc.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEXcIdFuncCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcIdFunc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEXcIdFunc.QrEXcIdFuncAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEXcIdFunc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEXcIdFunc.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEXcIdFuncCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'excidfunc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEXcIdFunc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEXcIdFunc.QrEXcIdFuncBeforeOpen(DataSet: TDataSet);
begin
  QrEXcIdFuncCodigo.DisplayFormat := FFormatFloat;
end;

end.

