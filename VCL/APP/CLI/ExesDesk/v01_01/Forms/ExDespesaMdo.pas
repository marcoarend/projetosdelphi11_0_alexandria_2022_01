unit ExDespesaMdo;

interface

uses
  Windows, System.UITypes, Messages, SysUtils, Classes, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, Vcl.Imaging.jpeg,
  FMX.Graphics, Soap.EncdDecd,
  UnProjGroupEnums,
  VCL.Graphics, dmkEditCB, dmkDBLookupComboBox;

type
  TFmExDespesaMdo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtGerarFotos: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPeriodos: TMySQLQuery;
    DsPeriodos: TDataSource;
    PnPeriodo: TPanel;
    QrPeriodosAnoMes: TWideStringField;
    QrPeriodosAno: TIntegerField;
    QrPeriodosMes: TIntegerField;
    QrEXcMdoPag: TMySQLQuery;
    DsEXcMdoPag: TDataSource;
    QrEXcMdoPagCodigo: TIntegerField;
    QrEXcMdoPagNome: TWideStringField;
    QrEXcCtaPag: TMySQLQuery;
    QrEXcCtaPagCodigo: TIntegerField;
    QrEXcCtaPagNome: TWideStringField;
    DsEXcCtaPag: TDataSource;
    QrEXmDspDevIts: TMySQLQuery;
    DsEXmDspDevIts: TDataSource;
    PnModo: TPanel;
    PnGrids: TPanel;
    PnConta: TPanel;
    DBGConra: TDBGrid;
    DBGLanctos: TDBGrid;
    PnFiltros: TPanel;
    DBGDocs: TDBGrid;
    QrEXmDspDevFts: TMySQLQuery;
    DsEXmDspDevFts: TDataSource;
    QrEXmDspDevItsCodigo: TIntegerField;
    QrEXmDspDevItsCodInMob: TIntegerField;
    QrEXmDspDevItsCtrlInMob: TIntegerField;
    QrEXmDspDevItsAnoMes: TWideStringField;
    QrEXmDspDevItsAno: TIntegerField;
    QrEXmDspDevItsMes: TIntegerField;
    QrEXmDspDevItsEXcIdFunc: TIntegerField;
    QrEXmDspDevItsData: TDateField;
    QrEXmDspDevItsHora: TTimeField;
    QrEXmDspDevItsEXcCtaPag: TIntegerField;
    QrEXmDspDevItsEXcMdoPag: TIntegerField;
    QrEXmDspDevItsSiglaOri: TWideStringField;
    QrEXmDspDevItsCotacOri: TFloatField;
    QrEXmDspDevItsValorOri: TFloatField;
    QrEXmDspDevItsSiglaInt: TWideStringField;
    QrEXmDspDevItsValorInt: TFloatField;
    QrEXmDspDevItsSiglaDst: TWideStringField;
    QrEXmDspDevItsCotacDst: TFloatField;
    QrEXmDspDevItsValorDst: TFloatField;
    QrEXmDspDevItsAtivo: TSmallintField;
    QrEXmDspDevFtsCodigo: TIntegerField;
    QrEXmDspDevFtsCodInMob: TIntegerField;
    QrEXmDspDevFtsCtrlInMob: TIntegerField;
    QrEXmDspDevFtsIDTabela: TIntegerField;
    QrEXmDspDevFtsDataHora: TDateTimeField;
    QrEXmDspDevFtsCtrlFoto: TIntegerField;
    QrEXmDspDevFtsEXcStCmprv: TIntegerField;
    QrEXmDspDevFtsNomeArq: TWideStringField;
    QrEXmDspDevFtsNoArqSvr: TWideStringField;
    QrEXmDspDevFtsAtivo: TSmallintField;
    PnFotoENome: TPanel;
    ImgFoto: TImage;
    LaNomeFoto: TLabel;
    QrEXmDspDevBmp: TMySQLQuery;
    QrEXmDspDevBmpTxtBmp: TWideMemoField;
    Splitter1: TSplitter;
    Panel3: TPanel;
    DBGPeriodos: TDBGrid;
    DBGModos: TDBGrid;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    QrEXmDspDevFtsNO_EXcStCmprv: TWideStringField;
    Button1: TButton;
    Panel2: TPanel;
    QrEXcIDFunc: TMySQLQuery;
    DsEXcIDFunc: TDataSource;
    CBEXcIDFunc: TdmkDBLookupComboBox;
    EdEXcIDFunc: TdmkEditCB;
    Label1: TLabel;
    QrEXcIDFuncCodigo: TIntegerField;
    QrEXcIDFuncNome: TWideStringField;
    QrEXmDspDevFtsMobInstlSeq: TIntegerField;
    SbDecrAnoMes: TSpeedButton;
    SbIncAnoMes: TSpeedButton;
    EdAnoMes: TdmkEdit;
    CkAnoMes: TCheckBox;
    QrEXmDspDevItsNO_EXcIdFunc: TWideStringField;
    CkMostrarFoto: TCheckBox;
    QrEXmDspDevItsSigla_IdFunc: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPeriodosBeforeClose(DataSet: TDataSet);
    procedure QrPeriodosAfterScroll(DataSet: TDataSet);
    procedure QrEXcMdoPagAfterScroll(DataSet: TDataSet);
    procedure QrEXcMdoPagBeforeClose(DataSet: TDataSet);
    procedure QrEXcCtaPagAfterScroll(DataSet: TDataSet);
    procedure QrEXcCtaPagBeforeClose(DataSet: TDataSet);
    procedure QrEXmDspDevItsAfterScroll(DataSet: TDataSet);
    procedure QrEXmDspDevItsBeforeClose(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure BtGerarFotosClick(Sender: TObject);
    procedure QrEXmDspDevFtsAfterScroll(DataSet: TDataSet);
    procedure EdEXcIDFuncRedefinido(Sender: TObject);
    procedure SbDecrAnoMesClick(Sender: TObject);
    procedure SbIncAnoMesClick(Sender: TObject);
    procedure EdAnoMesRedefinido(Sender: TObject);
  private
    { Private declarations }
     procedure ReopenEXcMdoPag();
     procedure ReopenEXcCtaPag();
     procedure ReopenEXmDspDevIts();
     procedure ReopenEXmDspDevFts();
     //
     function  ConvertFmxBitmapToVclBitmap(b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
     //function  CriarFotoSeNecessario_BMP(var Incremento: Integer): Word;
     function  CriarFotoSeNecessario_JPG(var Incremento: Integer): Word;
     function  SQLFiltro(Alias: String): String;
  public
    { Public declarations }
    FCriando: Boolean;
    //FEXcIdFunc: Integer;
    //
    procedure ReopenPeriodos();
  end;

  var
  FmExDespesaMdo: TFmExDespesaMdo;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UnExes_ProjGroupVars, UnDmkImg, UnExesD_PF,
  UMySQLModule;

{$R *.DFM}

function BitmapFromBase64(const base64: string):FMX.Graphics.TBitmap;
var
  Input: TStringStream;
  Output: TBytesStream;
begin
  Input := TStringStream.Create(base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Soap.EncdDecd.DecodeStream(Input, Output);
      Output.Position := 0;
      Result := FMX.Graphics.TBitmap.Create;
      try
        Result.LoadFromStream(Output);
      except
        Result.Free;
        raise;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TFmExDespesaMdo.BtGerarFotosClick(Sender: TObject);
var
 sMdoPag, sCtaPag, sDsp, sFoto: String;
 QtdFotos, QtdGeradas: Integer;
begin
  QtdFotos := 0;
  QtdGeradas := 0;
  QrEXcMdoPag.First;
  while not QrEXcMdoPag.Eof do
  begin
    sMdoPag := 'Modo: ' + QrEXcMdoPagNome.Value;
    QrEXcCtaPag.First;
    while not QrEXcCtaPag.Eof do
    begin
      sCtaPag := '. Conta: ' + QrEXcCtaPagNome.Value;
      QrEXmDspDevIts.First;
      while not QrEXmDspDevIts.Eof do
      begin
        sDsp := '. Item: ' + Geral.FF0(QrEXmDspDevItsCtrlInMob.Value);
        while not QrEXmDspDevFts.Eof do
        begin
          QtdFotos := QtdFotos + 1;
          sFoto := ' Foto: ' + Geral.FF0(QrEXmDspDevFtsCtrlFoto.Value);
          //
          MyObjects.Informa2(LaAviso1, LaAviso2, True, sMdoPag + sCtaPag + sDsp + sFoto);
          CriarFotoSeNecessario_JPG(QtdGeradas);
          //
          QrEXmDspDevFts.Next;
        end;
        //
        QrEXmDspDevIts.Next;
      end;
      //
      QrEXcCtaPag.Next;
    end;
    //
    QrEXcMdoPag.Next;
  end;
  //
  Geral.MB_Aviso('Arquivos baixados e gerados agora: ' + Geral.FF0(QtdGeradas) +
  sLineBreak + 'Total de arquivos do per�odo gerados:' + Geral.FF0(QtdFotos) +
  sLineBreak);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sMdoPag + sCtaPag + sDsp + sFoto);
  CriarFotoSeNecessario_JPG(QtdGeradas);
end;

procedure TFmExDespesaMdo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmExDespesaMdo.Button1Click(Sender: TObject);
var
  Base64: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevBmp, Dmod.MyDB, [
  'SELECT bmp.TxtBmp ',
  'FROM exmdspdevbmp bmp',
  'WHERE bmp.Codigo=' + Geral.FF0(QrEXmDspDevFtsCodigo.Value),
  'AND bmp.CodInMob=' + Geral.FF0(QrEXmDspDevFtsCodInMob.Value),
  'AND bmp.CtrlFoto=' + Geral.FF0(QrEXmDspDevFtsCtrlFoto.Value),
  'AND bmp.IDTabela=' + Geral.FF0(Integer(TEXIdGrupoTabelas.exigt_00_Lancto)),
  'ORDER BY bmp.Pedaco ',
  EmptyStr]);
  //
  Base64 := '';
  QrEXmDspDevBmp.First;
  while not QrEXmDspDevBmp.Eof do
  begin
    Base64 := Base64 + QrEXmDspDevBmpTxtBmp.Value + sLineBreak;
    //
    QrEXmDspDevBmp.Next;
  end;
  Geral.MB_Info(Base64)
end;

function TFmExDespesaMdo.ConvertFmxBitmapToVclBitmap(
  b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
var
  data:FMX.Graphics.TBitmapData;
  i,j:Integer;
  AlphaColor: System.UITypes.TAlphaColor;
begin
  Result := VCL.Graphics.TBitmap.Create;
  Result.SetSize(b.Width, b.Height);
  if(b.Map(TMapAccess.Readwrite, data)) then
  try
    for i := 0 to data.Height-1 do
    begin
      for j := 0 to data.Width-1 do
      begin
        AlphaColor:=data.GetPixel(j,i);
        Result.Canvas.Pixels[j,i]:=
          RGB(
            TAlphaColorRec(AlphaColor).R,
            TAlphaColorRec(AlphaColor).G,
            TAlphaColorRec(AlphaColor).B
          );
      end;
    end;
  finally
    b.Unmap(data);
  end;
end;

{
function TFmExDespesaMdo.CriarFotoSeNecessario_BMP(var Incremento: Integer): Word;
var
  NomeArq, NoArqSvr, Base64: AnsiString;
  FMX_Bitmap: FMX.Graphics.TBitmap;
  VCL_Bitmap: VCL.Graphics.TBitmap;
  Codigo, EXcIDFunc, CtrlInMob, IdTabela, CtrlFoto, MobInstlSeq, EXcStCmprv: Integer;
  DataHora: TDateTime;
  SiglaOri: String;
  CriarArqLocal: Boolean;
  ValorOri: Double;
begin
  if VAR_DIR_FOTOS_DESPESAS = EmptyStr then Exit;
  CriarArqLocal := False;
  NomeArq := VAR_DIR_FOTOS_DESPESAS + QrEXmDspDevFtsNoArqSvr.Value;
  if QrEXmDspDevFtsNoArqSvr.Value <> EmptyStr then
  begin
    if FileExists(NomeArq) then
      //ImgFoto.Picture.Bitmap.LoadFromFile(NomeArq)
    else
      CriarArqLocal := True;
  end else
    CriarArqLocal := True;
  if CriarArqLocal then
  begin
    if not DirectoryExists(VAR_DIR_FOTOS_DESPESAS) then
      ForceDirectories(VAR_DIR_FOTOS_DESPESAS);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevBmp, Dmod.MyDB, [
    'SELECT bmp.TxtBmp ',
    'FROM exmdspdevbmp bmp',
    'WHERE bmp.Codigo=' + Geral.FF0(QrEXmDspDevFtsCodigo.Value),
    'AND bmp.CodInMob=' + Geral.FF0(QrEXmDspDevFtsCodInMob.Value),
    'AND bmp.CtrlFoto=' + Geral.FF0(QrEXmDspDevFtsCtrlFoto.Value),
    'AND bmp.IDTabela=' + Geral.FF0(Integer(TEXIdGrupoTabelas.exigt_00_Lancto)),
    'ORDER BY bmp.Pedaco ',
    EmptyStr]);
    //
    Base64 := '';
    QrEXmDspDevBmp.First;
    while not QrEXmDspDevBmp.Eof do
    begin
      Base64 := Base64 + QrEXmDspDevBmpTxtBmp.Value;
      //
      QrEXmDspDevBmp.Next;
    end;
    if Base64 <> EmptyStr then
    begin
      FMX_BitMap := FMX.Graphics.TBitmap.Create;
      VCL_BitMap := VCL.Graphics.TBitmap.Create;
      //
      FMX_Bitmap := BitmapFromBase64(Base64);
      //Converter aqui de FMX para VCL!
      VCL_BitMap := ConvertFmxBitmapToVclBitmap(FMX_Bitmap);
      //
      ImgFoto.Picture.Bitmap.Assign(VCL_Bitmap);
      Codigo    := QrEXmDspDevFtsCodigo.Value;
      EXcIdFunc := QrEXmDspDevItsEXcIdFunc.Value;
      CtrlInMob := QrEXmDspDevFtsCtrlInMob.Value;
      DataHora  := QrEXmDspDevFtsDataHora.Value;
      IdTabela  := QrEXmDspDevFtsIDTabela.Value;
      CtrlFoto  := QrEXmDspDevFtsCtrlFoto.Value;
      SiglaOri  := QrEXmDspDevItsSiglaOri.Value;
      ValorOri  := QrEXmDspDevItsValorOri.Value;
      MobInstlSeq := QrEXmDspDevFtsMobInstlSeq.Value;
      EXcStCmprv  := QrEXmDspDevFtsEXcStCmprv.Value;
      //
      NoArqSvr  := ExesD_PF.DefineNomeArquivoFotoDespesa(IDTabela, Codigo,
      MobInstlSeq, CtrlInMob, CtrlFoto, EXcIdFunc, EXcStCmprv, SiglaOri,
      ValorOri, DataHora, QrPeriodosAnoMes.Value);
      //
      NomeArq := VAR_DIR_FOTOS_DESPESAS + NoArqSvr;
      ImgFoto.Picture.Bitmap.SaveToFile(NomeArq);
      if FileExists(NomeArq) and (QrEXmDspDevFtsNoArqSvr.Value = EmptyStr) then
      begin

        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'exmdspdevfts', False, [
        'NoArqSvr'], [
        'Codigo', 'IdTabela', 'CtrlInMob', 'CtrlFoto'], [
        NoArqSvr], [
        Codigo, IdTabela, CtrlInMob, CtrlFoto], True) then
        begin
          Incremento := Incremento + 1;
          // ???? Excluir dados da tabela de exmdspdevbmp?????
        end;
      end;
    end;
  end;
end;
}


function TFmExDespesaMdo.CriarFotoSeNecessario_JPG(var Incremento: Integer): Word;
{
  procedure BmpToJpg(FileName: string);
  var
    Jpg: TJpegImage;
    Stm: TMemoryStream;
    Bmp: TBitmap;
  begin
     if FileExists(FileName) then
     begin
       Bmp := TBitmap.Create;
       Bmp.LoadFromFile(FileName);
       Jpg := TJpegImage.Create;
       Jpg.Assign(Bmp);
       Jpg.Compress;
       Stm := TMemoryStream.Create;
       Jpg.SaveToStream(Stm);
       Stm.Position := 0;
       Stm.SaveToFile(ChangeFileExt(FileName, �.jpg�));
       Stm.Free;
       Jpg.Free;
       Bmp.Free;
     end;
  end;
}
var
  NomeArq, NoArqSvr, Base64: AnsiString;
  FMX_Bitmap: FMX.Graphics.TBitmap;
  VCL_Bitmap: VCL.Graphics.TBitmap;
  Codigo, EXcIDFunc, CtrlInMob, IdTabela, CtrlFoto, MobInstlSeq, EXcStCmprv: Integer;
  DataHora: TDateTime;
  SiglaOri, Sigla_IdFunc: String;
  CriarArqLocal: Boolean;
  ValorOri: Double;
  //
  Jpg: TJpegImage;
  Stm: TMemoryStream;
begin
  if VAR_DIR_FOTOS_DESPESAS = EmptyStr then Exit;
  CriarArqLocal := False;
  NomeArq := VAR_DIR_FOTOS_DESPESAS + QrEXmDspDevFtsNoArqSvr.Value;
  if QrEXmDspDevFtsNoArqSvr.Value <> EmptyStr then
  begin
    if FileExists(NomeArq) then
    begin
      if CkMostrarFoto.Checked then
      try
        ImgFoto.Picture.Bitmap.LoadFromFile(NomeArq)
      except
        on E: Exception do
        begin
          Geral.MB_Erro('Arquivo: ' + NomeArq + sLineBreak +
          E.Message);
        end;
      end;
    end else
      CriarArqLocal := True;
  end else
    CriarArqLocal := True;
  if CriarArqLocal then
  begin
    if not DirectoryExists(VAR_DIR_FOTOS_DESPESAS) then
      ForceDirectories(VAR_DIR_FOTOS_DESPESAS);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevBmp, Dmod.MyDB, [
    'SELECT bmp.TxtBmp ',
    'FROM exmdspdevbmp bmp',
    'WHERE bmp.Codigo=' + Geral.FF0(QrEXmDspDevFtsCodigo.Value),
    'AND bmp.CodInMob=' + Geral.FF0(QrEXmDspDevFtsCodInMob.Value),
    'AND bmp.CtrlFoto=' + Geral.FF0(QrEXmDspDevFtsCtrlFoto.Value),
    'AND bmp.IDTabela=' + Geral.FF0(Integer(TEXIdGrupoTabelas.exigt_00_Lancto)),
    'ORDER BY bmp.Pedaco ',
    EmptyStr]);
    //
    Base64 := '';
    QrEXmDspDevBmp.First;
    while not QrEXmDspDevBmp.Eof do
    begin
      Base64 := Base64 + QrEXmDspDevBmpTxtBmp.Value;
      //
      QrEXmDspDevBmp.Next;
    end;
    if Base64 <> EmptyStr then
    begin
      FMX_BitMap := FMX.Graphics.TBitmap.Create;
      VCL_BitMap := VCL.Graphics.TBitmap.Create;
      //
      FMX_Bitmap := BitmapFromBase64(Base64);
      //Converter aqui de FMX para VCL!
      VCL_BitMap := ConvertFmxBitmapToVclBitmap(FMX_Bitmap);
      //
      ImgFoto.Picture.Bitmap.Assign(VCL_Bitmap);
      Codigo    := QrEXmDspDevFtsCodigo.Value;
      EXcIdFunc := QrEXmDspDevItsExcIdFunc.Value;
      CtrlInMob := QrEXmDspDevFtsCtrlInMob.Value;
      DataHora  := QrEXmDspDevFtsDataHora.Value;
      IdTabela  := QrEXmDspDevFtsIDTabela.Value;
      CtrlFoto  := QrEXmDspDevFtsCtrlFoto.Value;
      SiglaOri  := QrEXmDspDevItsSiglaOri.Value;
      ValorOri  := QrEXmDspDevItsValorOri.Value;
      MobInstlSeq := QrEXmDspDevFtsMobInstlSeq.Value;
      EXcStCmprv  := QrEXmDspDevFtsEXcStCmprv.Value;
      Sigla_IdFunc := QrEXmDspDevItsSigla_IdFunc.Value;
      //
      NoArqSvr  := ExesD_PF.DefineNomeArquivoFotoDespesa(IDTabela, Codigo,
      MobInstlSeq, CtrlInMob, CtrlFoto, EXcIdFunc, Sigla_IdFunc, EXcStCmprv, SiglaOri,
      ValorOri, DataHora, QrPeriodosAnoMes.Value);
      //
      NomeArq := VAR_DIR_FOTOS_DESPESAS + NoArqSvr;
      //ImgFoto.Picture.Bitmap.SaveToFile(NomeArq);
       Jpg := TJpegImage.Create;
       Jpg.Assign(ImgFoto.Picture.Bitmap);
       Jpg.Compress;
       Stm := TMemoryStream.Create;
       Jpg.SaveToStream(Stm);
       Stm.Position := 0;
       Stm.SaveToFile(ChangeFileExt(NomeArq, '.jpg'));
       Stm.Free;
       Jpg.Free;
      if FileExists(NomeArq) and (QrEXmDspDevFtsNoArqSvr.Value = EmptyStr) then
      begin

        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'exmdspdevfts', False, [
        'NoArqSvr'], [
        'Codigo', 'IdTabela', 'CtrlInMob', 'CtrlFoto'], [
        NoArqSvr], [
        Codigo, IdTabela, CtrlInMob, CtrlFoto], True) then
        begin
          Incremento := Incremento + 1;
          // ???? Excluir dados da tabela de exmdspdevbmp?????
        end;
      end;
    end;
  end;
end;


procedure TFmExDespesaMdo.EdAnoMesRedefinido(Sender: TObject);
begin
  ReopenPeriodos();
end;

procedure TFmExDespesaMdo.EdEXcIDFuncRedefinido(Sender: TObject);
begin
  //FEXcIDFunc := EdEXCIDFunc.ValueVariant;
  ReopenPeriodos();
  ReopenEXcMdoPag()
end;

procedure TFmExDespesaMdo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmExDespesaMdo.FormCreate(Sender: TObject);
begin
  FCriando := True;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEXcIDFunc, Dmod.MyDB);
  EdAnoMes.FormatType := dmktf_AAAA_MM;
  EdAnoMes.ValueVariant := FormatDateTime('YYYY-MM', Date - 15);
  //
  //FEXcIdFunc := EdEXcIDFunc.ValueVariant;
  //DefineAnoMes();
  FCriando := False;
  ReopenPeriodos();
end;

procedure TFmExDespesaMdo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmExDespesaMdo.QrEXcCtaPagAfterScroll(DataSet: TDataSet);
begin
  ReopenEXmDspDevIts();
end;

procedure TFmExDespesaMdo.QrEXcCtaPagBeforeClose(DataSet: TDataSet);
begin
  QrEXmDspDevIts.Close;
end;

procedure TFmExDespesaMdo.QrEXcMdoPagAfterScroll(DataSet: TDataSet);
begin
  ReopenEXcCtaPag();
end;

procedure TFmExDespesaMdo.QrEXcMdoPagBeforeClose(DataSet: TDataSet);
begin
  QrEXcCtaPag.Close;
end;

procedure TFmExDespesaMdo.QrEXmDspDevFtsAfterScroll(DataSet: TDataSet);
  procedure CarregaJPG(NomeArq: String);
  var
     jpg: TJpegImage;
     bmp: TBitmap;
  begin
     jpg := TJpegImage.Create;
     jpg.LoadFromFile(NomeArq);

     bmp := TBitmap.Create;
     bmp.Assign(jpg);

     // Image Processing
     ImgFoto.Picture.Bitmap.Assign(Bmp);


     bmp.free;
     jpg.free;
  end;
var
  NomeArq(*, NoArqSvr, Base64*): AnsiString;
(*
  FMX_Bitmap: FMX.Graphics.TBitmap;
  VCL_Bitmap: VCL.Graphics.TBitmap;
  Codigo, EXcIDFunc, CtrlInMob, IdTabela, CtrlFoto: Integer;
  DataHora: TDateTime;
  SiglaOri: String;
  CriarArqLocal: Boolean;
  ValorOri: Double;
*)
begin
  if VAR_DIR_FOTOS_DESPESAS = EmptyStr then Exit;
  //CriarArqLocal := False;
  NomeArq := VAR_DIR_FOTOS_DESPESAS + QrEXmDspDevFtsNoArqSvr.Value;
  if QrEXmDspDevFtsNoArqSvr.Value <> EmptyStr then
  begin
    if FileExists(NomeArq) then
      //ImgFoto.Picture.Bitmap.LoadFromFile(NomeArq)
      CarregaJPG(NomeArq)
  end;
end;

procedure TFmExDespesaMdo.QrEXmDspDevItsAfterScroll(DataSet: TDataSet);
begin
  ReopenEXmDspDevFts();
end;

procedure TFmExDespesaMdo.QrEXmDspDevItsBeforeClose(DataSet: TDataSet);
begin
  QrEXmDspDevFts.Close;
  if ImgFoto.Picture <> nil then
    ImgFoto.Picture := nil;
end;

procedure TFmExDespesaMdo.QrPeriodosAfterScroll(DataSet: TDataSet);
begin
  if EdEXcIdFunc.ValueVariant <> 0 then
    ReopenEXcMdoPag()

end;

procedure TFmExDespesaMdo.QrPeriodosBeforeClose(DataSet: TDataSet);
begin
  QrEXcMdoPag.Close;
end;

procedure TFmExDespesaMdo.ReopenEXcCtaPag();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXcCtaPag, Dmod.MyDB, [
  'SELECT DISTINCT cct.Codigo, cct.Nome',
  'FROM excctapag cct ',
  'LEFT JOIN exmdspdevits its ON cct.Codigo=its.EXcCtaPag',
  'LEFT JOIN exmdspdevlct lct ON its.Codigo=lct.Codigo',
{
  'WHERE lct.EXcIdFunc=' + Geral.FF0(FEXcIdFunc),
  'AND lct.Ano=' + Geral.FF0(QrPeriodosAno.Value),
  'AND lct.Mes=' + Geral.FF0(QrPeriodosMes.Value),
}
  SQLFiltro('lct.'),
  'AND its.EXcMdoPag=' + Geral.FF0(QrEXcMdoPagCodigo.Value),
  'ORDER BY cct.Ordem',
  '']);
end;

procedure TFmExDespesaMdo.ReopenEXcMdoPag();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXcMdoPag, Dmod.MyDB, [
  'SELECT DISTINCT cmp.Codigo, cmp.Nome',
  'FROM excmdopag cmp ',
  'LEFT JOIN exmdspdevits its ON cmp.Codigo=its.EXcMdoPag',
  'LEFT JOIN exmdspdevlct lct ON its.Codigo=lct.Codigo',
{
  'WHERE lct.EXcIdFunc=' + Geral.FF0(FEXcIdFunc),
  'AND lct.Ano=' + Geral.FF0(QrPeriodosAno.Value),
  'AND lct.Mes=' + Geral.FF0(QrPeriodosMes.Value),
}
  SQLFiltro('lct.'),
  'ORDER BY cmp.Ordem',
  '']);
end;

procedure TFmExDespesaMdo.ReopenEXmDspDevFts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevFts, Dmod.MyDB, [
  'SELECT scm.Nome NO_EXcStCmprv, fts.* ',
  'FROM exmdspdevfts fts  ',
  'LEFT JOIN excstcmprv scm ON scm.Codigo=fts.EXcStCmprv ',
  'WHERE fts.Codigo=' + Geral.FF0(QrEXmDspDevItsCodigo.Value),
  'AND fts.CodInMob=' + Geral.FF0(QrEXmDspDevItsCodInMob.Value),
  'AND fts.IDTabela=' + Geral.FF0(Integer(TEXIdGrupoTabelas.exigt_00_Lancto)),
  '']);
  //Geral.MB_Teste(QrEXmDspDevFts.SQL.Text);
end;

procedure TFmExDespesaMdo.ReopenEXmDspDevIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevIts, Dmod.MyDB, [
  'SELECT fnc.Nome NO_EXcIdFunc, fnc.Sigla Sigla_IdFunc, its.*',
  'FROM exmdspdevits its ',
  'LEFT JOIN excidfunc fnc ON its.EXcIDFunc=fnc.Codigo ',
{
  'WHERE its.EXcIdFunc=' + Geral.FF0(FEXcIdFunc),
  'AND its.Ano=' + Geral.FF0(QrPeriodosAno.Value),
  'AND its.Mes=' + Geral.FF0(QrPeriodosMes.Value),
}
  SQLFiltro('its.'),
  'AND EXcMdoPag=' + Geral.FF0(QrEXcMdoPagCodigo.Value),
  'AND EXcCtaPag=' + Geral.FF0(QrEXcCtaPagCodigo.Value),
  '']);
end;

procedure TFmExDespesaMdo.ReopenPeriodos();
begin
  if EdEXcIdFunc.ValueVariant <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPeriodos, Dmod.MyDB, [
    'SELECT DISTINCT AnoMes, Ano, Mes',
    'FROM exmdspdevlct',
    SQLFiltro(''),
    'ORDER BY Ano DESC, Mes DESC',
    '']);
  end else
    QrPeriodos.Close;
end;

procedure TFmExDespesaMdo.SbDecrAnoMesClick(Sender: TObject);
begin
  EdAnoMes.Text := Geral.IncrementaMes_Ano_Mes(EdAnoMes.Text, - 1);
end;

procedure TFmExDespesaMdo.SbIncAnoMesClick(Sender: TObject);
begin
  EdAnoMes.Text := Geral.IncrementaMes_Ano_Mes(EdAnoMes.Text, + 1);
end;

function TFmExDespesaMdo.SQLFiltro(Alias: String): String;
var
  SQL_EXcIdFunc, SQL_AnoMes: String;
begin
  SQL_EXcIdFunc := '';
  SQL_AnoMes    := '';
  //
  if EdEXcIdFunc.ValueVariant <> 0 then
    SQL_EXcIdFunc := 'AND ' + Alias + 'EXcIdFunc=' + Geral.FF0(EdEXcIdFunc.ValueVariant);
  if CkAnoMes.Checked and (EdAnoMes.Text <> EmptyStr) then
    SQL_AnoMes := 'AND ' + Alias + 'AnoMes="' + EdAnoMes.Text + '"';
  //
  Result :=
    'WHERE ' + Alias + 'Ativo=1' + sLineBreak +
    SQL_EXcIdFunc + sLineBreak +
    SQL_AnoMes;
end;

end.
