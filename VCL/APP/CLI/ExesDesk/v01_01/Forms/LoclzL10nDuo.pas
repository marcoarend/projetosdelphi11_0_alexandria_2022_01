unit LoclzL10nDuo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, Vcl.Mask;

type
  TFmLoclzL10nDuo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrCompanies: TMySQLQuery;
    DsCompanies: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGCompanies: TDBGrid;
    Splitter1: TSplitter;
    QrCompaniesCodigo: TIntegerField;
    QrCompaniesFilial: TIntegerField;
    QrCompaniesNO_ENT: TWideStringField;
    QrLoclzCtaEnti: TMySQLQuery;
    DsLoclzCtaEnti: TDataSource;
    QrLoclzCtaEntiAtivo: TSmallintField;
    QrLoclzCtaEntiCodigo: TIntegerField;
    QrLoclzCtaEntiNoCtaPadr: TWideStringField;
    QrLoclzCtaEntiSIT: TWideStringField;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    QrCompaniesNO_loclzl10ncab: TWideStringField;
    DBEdit2: TDBEdit;
    QrLoclzCtaEntiTexto: TWideStringField;
    QrLoclzCtaEntiNoEN: TWideStringField;
    QrLoclzCtaEntiNomGrupo: TWideStringField;
    QrLoclzCtaEntiNomGruEN: TWideStringField;
    QrLoclzCtaEntiTxtGruEN: TWideStringField;
    QrCompaniesLoclzL10nCab: TIntegerField;
    QrLoclzCtaEntiEXcCtaPag: TIntegerField;
    QrLoclzCtaEntiLoclzL10nCab: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    DBGCta: TDBGrid;
    QrLoclzMdoEnti: TMySQLQuery;
    QrLoclzMdoEntiAtivo: TSmallintField;
    QrLoclzMdoEntiCodigo: TIntegerField;
    QrLoclzMdoEntiNoMdoPadr: TWideStringField;
    QrLoclzMdoEntiSIT: TWideStringField;
    QrLoclzMdoEntiTexto: TWideStringField;
    QrLoclzMdoEntiNoEN: TWideStringField;
    QrLoclzMdoEntiEXcMdoPag: TIntegerField;
    QrLoclzMdoEntiLoclzL10nCab: TIntegerField;
    DsLoclzMdoEnti: TDataSource;
    QrLoclzCmpEnti: TMySQLQuery;
    QrLoclzCmpEntiAtivo: TSmallintField;
    QrLoclzCmpEntiCodigo: TIntegerField;
    QrLoclzCmpEntiNoCmpPadr: TWideStringField;
    QrLoclzCmpEntiSIT: TWideStringField;
    QrLoclzCmpEntiTexto: TWideStringField;
    QrLoclzCmpEntiNoEN: TWideStringField;
    QrLoclzCmpEntiEXcStCmprv: TIntegerField;
    QrLoclzCmpEntiLoclzL10nCab: TIntegerField;
    DsLoclzCmpEnti: TDataSource;
    DBGMdo: TDBGrid;
    DBGCmp: TDBGrid;
    Panel1: TPanel;
    BtAltCmp: TBitBtn;
    Panel5: TPanel;
    BtAltMdo: TBitBtn;
    Panel6: TPanel;
    BtAltCta: TBitBtn;
    QrLoclzMdoEntiSigla: TWideStringField;
    QrLoclzMdoEntiDocHeader: TWideStringField;
    QrLoclzMdoEntiItemText: TWideStringField;
    QrLoclzMdoEntiReference: TWideStringField;
    QrLoclzMdoEntiDescrCondiPg: TWideStringField;
    QrLoclzMdoEntiRegrQtdDia: TIntegerField;
    QrLoclzMdoEntiRegrTipSeq: TIntegerField;
    QrLoclzMdoEntiRegrDiaSem: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrCompaniesAfterScroll(DataSet: TDataSet);
    procedure QrCompaniesBeforeClose(DataSet: TDataSet);
    procedure DBGCtaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGCtaCellClick(Column: TColumn);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtAltCmpClick(Sender: TObject);
    procedure BtAltCtaClick(Sender: TObject);
    procedure BtAltMdoClick(Sender: TObject);
    procedure DBGMdoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGCmpDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGMdoCellClick(Column: TColumn);
    procedure DBGCmpCellClick(Column: TColumn);
  private
    { Private declarations }
    procedure ReopenCompanies(Entidade: Integer);
    function  ReopenLoclzCtaEnti(EXcCtaPag: Integer): Boolean;
    function  ReopenLoclzMdoEnti(EXcMdoPag: Integer): Boolean;
    function  ReopenLoclzCmpEnti(EXcStCmprv: Integer): Boolean;
  public
    { Public declarations }
  end;

  var
  FmLoclzL10nDuo: TFmLoclzL10nDuo;

implementation

uses UnMyObjects, Module, DmkDAC_PF, MyVCLSkin, UMySQLModule, UnExesD_Jan;

{$R *.DFM}

const FFldAtiv = 'Ativo';

procedure TFmLoclzL10nDuo.BtAltCmpClick(Sender: TObject);
var
  IdxFld1, IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, (*FldTradu2,*) Txt1ptBR,
  Txt1enUS, (*Txt2ptBR, Txt2enUS,*) TxtTradu1, (*TxtTradu2,*) TablName: String;
  IdxVal1, IdxVal2: Integer;
  //
  Continua: Boolean;
  Entidade, EXcStCmprv, Ativo: Integer;
  SQLType: TSQLType;
begin
  Continua   := False;
  Entidade   := QrCompaniesCodigo.Value;
  EXcStCmprv := QrLoclzCmpEntiCodigo.Value;
  if QrLoclzCmpEntiSIT.Value = 'N' then
  begin
    SQLType := stIns;
    Ativo := 0;
    //if
    Dmod.MyDB.Execute(Geral.ATS([
    'INSERT INTO loclzcmpenti',
    'SET Ativo=' + Geral.FF0(Ativo),
    ', Entidade=' + Geral.FF0(Entidade),
    ', EXcStCmprv=' + Geral.FF0(EXcStCmprv),
    ''])); // then
    begin
    (*if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loclzcmpenti', False, [], [
    'Entidade', 'EXcStCmprv'], [], [
     Entidade, EXcStCmprv], True) then
     begin*)
       Continua := ReopenLoclzCmpEnti(EXcStCmprv);
     end;
  end else
    Continua := True;
  //
  if Continua then
  begin
    TablName    := 'loclzl10ncmp';
    IdxFld1     := 'EXcStCmprv';
    IdxFld2     := 'LoclzL10nCab';
    Idx1Descri  := QrLoclzCmpEntiNoCmpPadr.Value;
    Idx2Descri  := QrCompaniesNO_loclzl10ncab.Value;
    FldTradu1   := 'Texto';
    //FldTradu2   := 'TxtGruEN';
    Txt1ptBR    := QrLoclzCmpEntiNoCmpPadr.Value;
    Txt1enUS    := QrLoclzCmpEntiNoEN.Value;
    //Txt2ptBR    := QrLoclzCmpEntiNomGrupo.Value;
    //Txt2enUS    := QrLoclzCmpEntiNomGruEN.Value;
    TxtTradu1   := QrLoclzCmpEntiTexto.Value;
    //TxtTradu2   := QrLoclzCmpEntiTxtGruEN.Value;
    IdxVal1     := QrLoclzCmpEntiCodigo.Value;
    IdxVal2     := QrCompaniesLoclzL10nCab.Value;
    //
    //
    ExesD_Jan.MostraFormFmLoclzL10nEd1(QrLoclzCmpEnti, TablName,
    IdxFld1, IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, (*FldTradu2,*) Txt1ptBR,
    Txt1enUS, (*Txt2ptBR, Txt2enUS,*) TxtTradu1, (*TxtTradu2,*) IdxVal1, IdxVal2);

(*    procedure TUnExesD_Jan.MostraFormFmLoclzL10nEd1(QrIts: TmySQLQuery; TableName,
  IdxFld1, IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, Txt1ptBR, Txt1enUS,
  TxtTradu1: String; IdxVal1, IdxVal2: Integer);*)
  end else;
  //
end;

procedure TFmLoclzL10nDuo.BtAltCtaClick(Sender: TObject);
var
  IdxFld1, IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, FldTradu2, Txt1ptBR,
  Txt1enUS, Txt2ptBR, Txt2enUS, TxtTradu1, TxtTradu2, TablName: String;
  IdxVal1, IdxVal2: Integer;
  //
  Continua: Boolean;
  Entidade, EXcCtaPag, Ativo: Integer;
  SQLType: TSQLType;
begin
  Continua := False;
  Entidade := QrCompaniesCodigo.Value;
  EXcCtaPag := QrLoclzCtaEntiCodigo.Value;
  if QrLoclzCtaEntiSIT.Value = 'N' then
  begin
    SQLType := stIns;
    Ativo := 0;
    //if
    Dmod.MyDB.Execute(Geral.ATS([
    'INSERT INTO loclzctaenti',
    'SET Ativo=' + Geral.FF0(Ativo),
    ', Entidade=' + Geral.FF0(Entidade),
    ', EXcCtaPag=' + Geral.FF0(EXcCtaPag),
    ''])); // then
    begin
    (*if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loclzctaenti', False, [], [
    'Entidade', 'EXcCtaPag'], [], [
     Entidade, EXcCtaPag], True) then
     begin*)
       Continua := ReopenLoclzCtaEnti(EXcCtaPag);
     end;
  end else
    Continua := True;
  //
  if Continua then
  begin
    TablName    := 'loclzl10ncta';
    IdxFld1     := 'EXcCtaPag';
    IdxFld2     := 'LoclzL10nCab';
    Idx1Descri  := QrLoclzCtaEntiNoCtaPadr.Value;
    Idx2Descri  := QrCompaniesNO_loclzl10ncab.Value;
    FldTradu1   := 'Texto';
    FldTradu2   := 'TxtGruEN';
    Txt1ptBR    := QrLoclzCtaEntiNoCtaPadr.Value;
    Txt1enUS    := QrLoclzCtaEntiNoEN.Value;
    Txt2ptBR    := QrLoclzCtaEntiNomGrupo.Value;
    Txt2enUS    := QrLoclzCtaEntiNomGruEN.Value;
    TxtTradu1   := QrLoclzCtaEntiTexto.Value;
    TxtTradu2   := QrLoclzCtaEntiTxtGruEN.Value;
    IdxVal1     := QrLoclzCtaEntiCodigo.Value;
    IdxVal2     := QrCompaniesLoclzL10nCab.Value;
    //
    //
    ExesD_Jan.MostraFormFmLoclzL10nEd2(QrLoclzCtaEnti, TablName, IdxFld1, IdxFld2, Idx1Descri,
    Idx2Descri, FldTradu1, FldTradu2, Txt1ptBR, Txt1enUS, Txt2ptBR, Txt2enUS,
    TxtTradu1, TxtTradu2, IdxVal1, IdxVal2);
  end else;
  //
end;

procedure TFmLoclzL10nDuo.BtAltMdoClick(Sender: TObject);
var
  IdxFld1, IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, (*FldTradu2,*) Txt1ptBR,
  Txt1enUS, (*Txt2ptBR, Txt2enUS,*) TxtTradu1, (*TxtTradu2,*) TablName: String;
  IdxVal1, IdxVal2: Integer;
  //
  Continua: Boolean;
  Entidade, EXcMdoPag, Ativo: Integer;
  SQLType: TSQLType;
  //
  Sigla, DocHeader, ItemText, Reference, DescrCondiPg: String;
  RegrQtdDia, RegrTipSeq, RegrDiaSem: Integer;
begin
  Continua := False;
  Entidade := QrCompaniesCodigo.Value;
  EXcMdoPag := QrLoclzMdoEntiCodigo.Value;
  if QrLoclzMdoEntiSIT.Value = 'N' then
  begin
    SQLType := stIns;
    Ativo := 0;
    //if
    Dmod.MyDB.Execute(Geral.ATS([
    'INSERT INTO loclzmdoenti',
    'SET Ativo=' + Geral.FF0(Ativo),
    ', Entidade=' + Geral.FF0(Entidade),
    ', EXcMdoPag=' + Geral.FF0(EXcMdoPag),
    ''])); // then
    begin
    (*if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loclzmdoenti', False, [], [
    'Entidade', 'EXcMdoPag'], [], [
     Entidade, EXcMdoPag], True) then
     begin*)
       Continua := ReopenLoclzMdoEnti(EXcMdoPag);
     end;
  end else
    Continua := True;
  //
  if Continua then
  begin
    TablName    := 'loclzl10nmdo';
    IdxFld1     := 'EXcMdoPag';
    IdxFld2     := 'LoclzL10nCab';
    Idx1Descri  := QrLoclzMdoEntiNoMdoPadr.Value;
    Idx2Descri  := QrCompaniesNO_loclzl10ncab.Value;
    FldTradu1   := 'Texto';
    //FldTradu2   := 'TxtGruEN';
    Txt1ptBR    := QrLoclzMdoEntiNoMdoPadr.Value;
    Txt1enUS    := QrLoclzMdoEntiNoEN.Value;
    //Txt2ptBR    := QrLoclzMdoEntiNomGrupo.Value;
    //Txt2enUS    := QrLoclzMdoEntiNomGruEN.Value;
    TxtTradu1   := QrLoclzMdoEntiTexto.Value;
    //TxtTradu2   := QrLoclzMdoEntiTxtGruEN.Value;
    IdxVal1     := QrLoclzMdoEntiCodigo.Value;
    IdxVal2     := QrCompaniesLoclzL10nCab.Value;
    //
    Sigla         := QrLoclzMdoEntiSigla     .Value;
    DocHeader     := QrLoclzMdoEntiDocHeader .Value;
    ItemText      := QrLoclzMdoEntiItemText  .Value;
    Reference     := QrLoclzMdoEntiReference .Value;
    DescrCondiPg  := QrLoclzMdoEntiDescrCondiPg.Value;
    RegrQtdDia    := QrLoclzMdoEntiRegrQtdDia.Value;
    RegrTipSeq    := QrLoclzMdoEntiRegrTipSeq.Value;
    RegrDiaSem    := QrLoclzMdoEntiRegrDiaSem.Value;
    //
{*.*    Sigla,
    DocHeader,
    ItemText,
    Reference,
    DescrCondiPg,
    RegrQtdDia,
    RegrTipSeq,
    RegrDiaSem,
    //

    ExesD_Jan.MostraFormFmLoclzL10nEd3(QrLoclzMdoEnti, TablName,
    IdxFld1, IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, (*FldTradu2,*) Txt1ptBR,
    Txt1enUS, (*Txt2ptBR, Txt2enUS,*) TxtTradu1, (*TxtTradu2,*) IdxVal1, IdxVal2);
    }
(*    procedure TUnExesD_Jan.MostraFormFmLoclzL10nEd1(QrIts: TmySQLQuery; TableName,
  IdxFld1, IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, Txt1ptBR, Txt1enUS,
  TxtTradu1: String; IdxVal1, IdxVal2: Integer);*)
    ExesD_Jan.MostraFormFmLoclzL10nEd3(QrLoclzMdoEnti, TablName,
    IdxFld1, IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, Txt1ptBR, Txt1enUS,
    TxtTradu1, IdxVal1, IdxVal2, Sigla, DocHeader, ItemText, Reference,
    DescrCondiPg, RegrQtdDia, RegrTipSeq, RegrDiaSem);
  end else;
  //
end;

procedure TFmLoclzL10nDuo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLoclzL10nDuo.DBGCmpCellClick(Column: TColumn);
var
  Ativo, IDInt: Integer;
  SQLType: TSQLType;
  Entidade, EXcStCmprv: Integer;
begin
  if Column.FieldName = FFldAtiv then
  begin
    Entidade := QrCompaniesCodigo.Value;
    EXcStCmprv := QrLoclzCmpEntiCodigo.Value;
    if QrLoclzCmpEntiSIT.Value = 'N' then
    begin
      SQLType := stIns;
      Ativo := 1;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loclzcmpenti', False, [], [
      'Entidade', 'EXcStCmprv'], [], [
       Entidade, EXcStCmprv], True) then
       begin
         ReopenLoclzCmpEnti(EXcStCmprv);
       end;
    end else
    begin
      if QrLoclzCmpEntiAtivo.Value = 1 then
        Ativo := 0
      else
        Ativo := 1;
      Dmod.MyDB.Execute(Geral.ATS([
      'UPDATE loclzcmpenti',
      'SET Ativo=' + Geral.FF0(Ativo),
      'WHERE Entidade=' + Geral.FF0(Entidade),
      'AND EXcStCmprv=' + Geral.FF0(EXcStCmprv),
      '']));
       ReopenLoclzCmpEnti(EXcStCmprv);
    end
  end;
end;

procedure TFmLoclzL10nDuo.DBGCmpDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = FFldAtiv then
    MeuVCLSkin.DrawGrid(DBGCmp, Rect, 1, QrLoclzCmpEntiAtivo.Value);
end;

procedure TFmLoclzL10nDuo.DBGCtaCellClick(Column: TColumn);
var
  Ativo, IDInt: Integer;
  SQLType: TSQLType;
  Entidade, EXcCtaPag: Integer;
begin
  if Column.FieldName = FFldAtiv then
  begin
    Entidade := QrCompaniesCodigo.Value;
    EXcCtaPag := QrLoclzCtaEntiCodigo.Value;
    if QrLoclzCtaEntiSIT.Value = 'N' then
    begin
      SQLType := stIns;
      Ativo := 1;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loclzctaenti', False, [], [
      'Entidade', 'EXcCtaPag'], [], [
       Entidade, EXcCtaPag], True) then
       begin
         ReopenLoclzCtaEnti(EXcCtaPag);
       end;
    end else
    begin
      if QrLoclzCtaEntiAtivo.Value = 1 then
        Ativo := 0
      else
        Ativo := 1;
      Dmod.MyDB.Execute(Geral.ATS([
      'UPDATE loclzctaenti',
      'SET Ativo=' + Geral.FF0(Ativo),
      'WHERE Entidade=' + Geral.FF0(Entidade),
      'AND EXcCtaPag=' + Geral.FF0(EXcCtaPag),
      '']));
       ReopenLoclzCtaEnti(EXcCtaPag);
    end
  end;
end;

procedure TFmLoclzL10nDuo.DBGCtaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = FFldAtiv then
    MeuVCLSkin.DrawGrid(DBGCta, Rect, 1, QrLoclzCtaEntiAtivo.Value);
end;

procedure TFmLoclzL10nDuo.DBGMdoCellClick(Column: TColumn);
var
  Ativo, IDInt: Integer;
  SQLType: TSQLType;
  Entidade, EXcMdoPag: Integer;
begin
  if Column.FieldName = FFldAtiv then
  begin
    Entidade := QrCompaniesCodigo.Value;
    EXcMdoPag := QrLoclzMdoEntiCodigo.Value;
    if QrLoclzMdoEntiSIT.Value = 'N' then
    begin
      SQLType := stIns;
      Ativo := 1;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loclzmdoenti', False, [], [
      'Entidade', 'EXcMdoPag'], [], [
       Entidade, EXcMdoPag], True) then
       begin
         ReopenLoclzMdoEnti(EXcMdoPag);
       end;
    end else
    begin
      if QrLoclzMdoEntiAtivo.Value = 1 then
        Ativo := 0
      else
        Ativo := 1;
      Dmod.MyDB.Execute(Geral.ATS([
      'UPDATE loclzmdoenti',
      'SET Ativo=' + Geral.FF0(Ativo),
      'WHERE Entidade=' + Geral.FF0(Entidade),
      'AND EXcMdoPag=' + Geral.FF0(EXcMdoPag),
      '']));
       ReopenLoclzMdoEnti(EXcMdoPag);
    end
  end;
end;

procedure TFmLoclzL10nDuo.DBGMdoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = FFldAtiv then
    MeuVCLSkin.DrawGrid(DBGMdo, Rect, 1, QrLoclzMdoEntiAtivo.Value);
end;

procedure TFmLoclzL10nDuo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLoclzL10nDuo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  //
  ReopenCompanies(0);
end;

procedure TFmLoclzL10nDuo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLoclzL10nDuo.QrCompaniesAfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  //Habilita := QrCompaniesLoclzl10ncab.Value <> 0; //NO_loclzl10ncab
  Habilita := QrCompaniesCodigo.Value <= -10; //NO_loclzl10ncab
  BtAltCta.Enabled := Habilita;
  BtAltMdo.Enabled := Habilita;
  BtAltCmp.Enabled := Habilita;
  ReopenLoclzCtaEnti(0);
  ReopenLoclzMdoEnti(0);
  ReopenLoclzCmpEnti(0);
end;

procedure TFmLoclzL10nDuo.QrCompaniesBeforeClose(DataSet: TDataSet);
begin
  QrLoclzCtaEnti.Close;
  QrLoclzMdoEnti.Close;
  QrLoclzCmpEnti.Close;
end;

procedure TFmLoclzL10nDuo.ReopenCompanies(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCompanies, Dmod.MyDB, [
  'SELECT ent.Codigo, ent.Filial, cny.LoclzL10nCab, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'llc.Nome NO_loclzl10ncab   ',
  'FROM entidades ent ',
  'LEFT JOIN prmscompny   cny ON cny.Entidade=ent.Codigo ',
  'LEFT JOIN loclzl10ncab llc ON llc.Codigo=cny.LoclzL10nCab  ',
  'WHERE ent.Codigo <= -10  ',   // Filial para quem baixar exes sem haver l�ngua implementada!
  'AND ent.Filial >= 0  ',       // Filial para quem baixar exes sem haver l�ngua implementada!
  'ORDER BY NO_ENT  ',
  '']);
  if Entidade <> 0 then
    QrCompanies.Locate('Codigo', Entidade, []);
end;



function TFmLoclzL10nDuo.ReopenLoclzCmpEnti(EXcStCmprv: Integer): Boolean;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoclzCmpEnti, Dmod.MyDB, [
  'SELECT lce.Ativo, cmp.Codigo, cmp.Nome NoCmpPadr, ',
  'IF(lce.Ativo IS NULL, "N", IF(lce.Ativo=0, "I", "A")) SIT,',
  'cmp.NoEN, llc.Texto, llc.EXcStCmprv, llc.LoclzL10nCab ',
  'FROM excstcmprv cmp ',
  'LEFT JOIN loclzcmpenti lce ON lce.EXcStCmprv=cmp.Codigo ',
  '  AND lce.Entidade=' + Geral.FF0(QrCompaniesCodigo.Value),
  'LEFT JOIN loclzl10ncmp llc ON llc.EXcStCmprv=cmp.Codigo ',
  '  AND llc.LoclzL10nCab=' + Geral.FF0(QrCompaniesLoclzL10nCab.Value),
  '']);
  if EXcStCmprv > 0 then
    Result := QrLoclzCmpEnti.Locate('Codigo', EXcStCmprv, [])
  else
    Result := True;
end;

function TFmLoclzL10nDuo.ReopenLoclzCtaEnti(EXcCtaPag: Integer): Boolean;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoclzCtaEnti, Dmod.MyDB, [
  'SELECT lce.Ativo, cta.Codigo, cta.Nome NoCtaPadr, ',
  'IF(lce.Ativo IS NULL, "N", IF(lce.Ativo=0, "I", "A")) SIT,',
  'cta.NoEN, cta.NomGrupo, NomGruEN, llc.Texto, llc.TxtGruEN, ',
  'llc.EXcCtaPag, llc.LoclzL10nCab ',
  'FROM excctapag cta ',
  'LEFT JOIN loclzctaenti lce ON lce.EXcCtaPag=cta.Codigo ',
  '  AND lce.Entidade=' + Geral.FF0(QrCompaniesCodigo.Value),
  'LEFT JOIN loclzl10ncta llc ON llc.EXcCtaPag=cta.Codigo ',
  '  AND llc.LoclzL10nCab=' + Geral.FF0(QrCompaniesLoclzL10nCab.Value),
  '']);
  if EXcCtaPag > 0 then
    Result := QrLoclzCtaEnti.Locate('Codigo', EXcCtaPag, [])
  else
    Result := True;
  //
  //Geral.MB_Teste(QrLoclzCtaEnti.SQL.Text);
end;

function TFmLoclzL10nDuo.ReopenLoclzMdoEnti(EXcMdoPag: Integer): Boolean;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoclzMdoEnti, Dmod.MyDB, [
  'SELECT lce.Ativo, mdo.Codigo, mdo.Nome NoMdoPadr, ',
  'IF(lce.Ativo IS NULL, "N", IF(lce.Ativo=0, "I", "A")) SIT,',
  'mdo.NoEN, llc.Texto, llc.EXcMdoPag, llc.LoclzL10nCab, ',
  'llc.Sigla, llc.DocHeader, llc.ItemText, llc.Reference, llc.DescrCondiPg, ',
  'llc.RegrQtdDia, llc.RegrTipSeq, llc.RegrDiaSem ',
  'FROM excmdopag mdo ',
  'LEFT JOIN loclzmdoenti lce ON lce.EXcMdoPag=mdo.Codigo ',
  '  AND lce.Entidade=' + Geral.FF0(QrCompaniesCodigo.Value),
  'LEFT JOIN loclzl10nmdo llc ON llc.EXcMdoPag=mdo.Codigo ',
  '  AND llc.LoclzL10nCab=' + Geral.FF0(QrCompaniesLoclzL10nCab.Value),
  '']);
  if EXcMdoPag > 0 then
    Result := QrLoclzMdoEnti.Locate('Codigo', EXcMdoPag, [])
  else
    Result := True;
end;

procedure TFmLoclzL10nDuo.SpeedButton1Click(Sender: TObject);
begin
  ExesD_Jan.MostraFormFmPrmsCompny(QrCompaniesCodigo.Value);
  ReopenCompanies(QrCompaniesCodigo.Value);
end;

end.
