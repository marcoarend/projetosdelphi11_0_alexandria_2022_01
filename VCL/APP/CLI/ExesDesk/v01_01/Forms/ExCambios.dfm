object FmExCambios: TFmExCambios
  Left = 339
  Top = 185
  Caption = 'EXS-GEREN-002 :: Gerenciamento de C'#226'mbios'
  ClientHeight = 645
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1168
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 338
        Height = 32
        Caption = 'Gerenciamento de C'#226'mbios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 338
        Height = 32
        Caption = 'Gerenciamento de C'#226'mbios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 338
        Height = 32
        Caption = 'Gerenciamento de C'#226'mbios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 531
    Width = 1264
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1260
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 575
    Width = 1264
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 1118
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1116
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
  object PnPeriodo: TPanel
    Left = 0
    Top = 48
    Width = 1264
    Height = 483
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter3: TSplitter
      Left = 828
      Top = 0
      Width = 5
      Height = 483
    end
    object PnModo: TPanel
      Left = 0
      Top = 0
      Width = 828
      Height = 483
      Align = alLeft
      TabOrder = 0
      object PnGrids: TPanel
        Left = 1
        Top = 1
        Width = 826
        Height = 481
        Align = alClient
        TabOrder = 0
        object PnFiltros: TPanel
          Left = 1
          Top = 1
          Width = 824
          Height = 479
          Align = alClient
          TabOrder = 0
          object Panel3: TPanel
            Left = 1
            Top = 1
            Width = 822
            Height = 477
            Align = alClient
            TabOrder = 0
            object DBGPeriodos: TDBGrid
              Left = 1
              Top = 1
              Width = 148
              Height = 475
              Align = alLeft
              DataSource = DsPeriodos
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AnoMes'
                  Width = 109
                  Visible = True
                end>
            end
            object DBGCambios: TDBGrid
              Left = 149
              Top = 1
              Width = 672
              Height = 475
              Align = alClient
              DataSource = DsEXmDspDevMCb
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
    end
    object PnFotoENome: TPanel
      Left = 833
      Top = 0
      Width = 431
      Height = 483
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object ImgFoto: TImage
        Left = 0
        Top = 13
        Width = 431
        Height = 470
        Align = alClient
        Picture.Data = {
          0A544A504547496D61676577020000FFD8FFE000104A46494600010101006000
          600000FFDB004300020101020101020202020202020203050303030303060404
          0305070607070706070708090B0908080A0807070A0D0A0A0B0C0C0C0C07090E
          0F0D0C0E0B0C0C0CFFDB004301020202030303060303060C0807080C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0CFFC00011080002000203012200021101031101
          FFC4001F0000010501010101010100000000000000000102030405060708090A
          0BFFC400B5100002010303020403050504040000017D01020300041105122131
          410613516107227114328191A1082342B1C11552D1F02433627282090A161718
          191A25262728292A3435363738393A434445464748494A535455565758595A63
          6465666768696A737475767778797A838485868788898A92939495969798999A
          A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
          D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
          01010101010101010000000000000102030405060708090A0BFFC400B5110002
          0102040403040705040400010277000102031104052131061241510761711322
          328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
          292A35363738393A434445464748494A535455565758595A636465666768696A
          737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
          A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
          E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00FE7F
          E8A28A00FFD9}
        Proportional = True
        ExplicitLeft = 76
        ExplicitWidth = 359
        ExplicitHeight = 454
      end
      object LaNomeFoto: TLabel
        Left = 0
        Top = 0
        Width = 431
        Height = 13
        Align = alTop
        Caption = '...'
        ExplicitWidth = 9
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrPeriodos: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrPeriodosBeforeClose
    AfterScroll = QrPeriodosAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT AnoMes, Ano, Mes'
      'FROM exmdspdevlct'
      'ORDER BY Ano DESC, Mes DESC')
    Left = 384
    Top = 284
    object QrPeriodosAnoMes: TWideStringField
      FieldName = 'AnoMes'
      Size = 7
    end
    object QrPeriodosAno: TIntegerField
      FieldName = 'Ano'
    end
    object QrPeriodosMes: TIntegerField
      FieldName = 'Mes'
    end
  end
  object DsPeriodos: TDataSource
    DataSet = QrPeriodos
    Left = 384
    Top = 340
  end
  object QrEXmDspDevMCb: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEXmDspDevMCbBeforeClose
    AfterScroll = QrEXmDspDevMCbAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT *'
      'FROM exmdspdevmcb')
    Left = 488
    Top = 292
    object QrEXmDspDevMCbCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEXmDspDevMCbCodInMob: TIntegerField
      FieldName = 'CodInMob'
      Required = True
    end
    object QrEXmDspDevMCbDeviceSI: TIntegerField
      FieldName = 'DeviceSI'
      Required = True
    end
    object QrEXmDspDevMCbDeviceID: TWideStringField
      FieldName = 'DeviceID'
      Size = 60
    end
    object QrEXmDspDevMCbRandmStr: TWideStringField
      FieldName = 'RandmStr'
      Required = True
      Size = 32
    end
    object QrEXmDspDevMCbAnoMes: TWideStringField
      FieldName = 'AnoMes'
      Required = True
      Size = 7
    end
    object QrEXmDspDevMCbAno: TIntegerField
      FieldName = 'Ano'
      Required = True
    end
    object QrEXmDspDevMCbMes: TIntegerField
      FieldName = 'Mes'
      Required = True
    end
    object QrEXmDspDevMCbEXcIdFunc: TIntegerField
      FieldName = 'EXcIdFunc'
      Required = True
    end
    object QrEXmDspDevMCbIDCntrCst: TIntegerField
      FieldName = 'IDCntrCst'
      Required = True
    end
    object QrEXmDspDevMCbData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrEXmDspDevMCbHora: TTimeField
      FieldName = 'Hora'
      Required = True
    end
    object QrEXmDspDevMCbSiglaOri: TWideStringField
      FieldName = 'SiglaOri'
      Required = True
      Size = 10
    end
    object QrEXmDspDevMCbCotacInf: TFloatField
      FieldName = 'CotacInf'
      Required = True
    end
    object QrEXmDspDevMCbCotacOri: TFloatField
      FieldName = 'CotacOri'
      Required = True
    end
    object QrEXmDspDevMCbValorOri: TFloatField
      FieldName = 'ValorOri'
      Required = True
    end
    object QrEXmDspDevMCbSiglaDst: TWideStringField
      FieldName = 'SiglaDst'
      Required = True
      Size = 10
    end
    object QrEXmDspDevMCbCotacDst: TFloatField
      FieldName = 'CotacDst'
      Required = True
    end
    object QrEXmDspDevMCbValorDst: TFloatField
      FieldName = 'ValorDst'
      Required = True
    end
    object QrEXmDspDevMCbObserva: TWideStringField
      FieldName = 'Observa'
      Size = 511
    end
    object QrEXmDspDevMCbQtdFotos: TIntegerField
      FieldName = 'QtdFotos'
      Required = True
    end
    object QrEXmDspDevMCbObsrvFinal: TWideStringField
      FieldName = 'ObsrvFinal'
      Size = 511
    end
    object QrEXmDspDevMCbEncerrado: TDateTimeField
      FieldName = 'Encerrado'
      Required = True
    end
    object QrEXmDspDevMCbDtHrUpIni: TDateTimeField
      FieldName = 'DtHrUpIni'
      Required = True
    end
    object QrEXmDspDevMCbDtHrUpFim: TDateTimeField
      FieldName = 'DtHrUpFim'
      Required = True
    end
    object QrEXmDspDevMCbEnviadoWeb: TSmallintField
      FieldName = 'EnviadoWeb'
      Required = True
    end
  end
  object DsEXmDspDevMCb: TDataSource
    DataSet = QrEXmDspDevMCb
    Left = 488
    Top = 348
  end
  object QrEXmDspDevFts: TMySQLQuery
    Database = Dmod.ZZDB
    AfterScroll = QrEXmDspDevFtsAfterScroll
    SQL.Strings = (
      'SELECT scm.Nome NO_EXcStCmprv, fts.* '
      'FROM exmdspdevfts fts  '
      'LEFT JOIN excstcmprv scm ON scm.Codigo=fts.EXcStCmprv '
      'WHERE fts.Codigo>0 '
      'AND fts.CodInMob=12 ')
    Left = 788
    Top = 296
    object QrEXmDspDevFtsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEXmDspDevFtsCodInMob: TIntegerField
      FieldName = 'CodInMob'
      Required = True
    end
    object QrEXmDspDevFtsCtrlInMob: TIntegerField
      FieldName = 'CtrlInMob'
      Required = True
    end
    object QrEXmDspDevFtsIDTabela: TIntegerField
      FieldName = 'IDTabela'
      Required = True
    end
    object QrEXmDspDevFtsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrEXmDspDevFtsCtrlFoto: TIntegerField
      FieldName = 'CtrlFoto'
      Required = True
    end
    object QrEXmDspDevFtsEXcStCmprv: TIntegerField
      FieldName = 'EXcStCmprv'
      Required = True
    end
    object QrEXmDspDevFtsNomeArq: TWideStringField
      FieldName = 'NomeArq'
      Size = 255
    end
    object QrEXmDspDevFtsNoArqSvr: TWideStringField
      FieldName = 'NoArqSvr'
      Size = 255
    end
    object QrEXmDspDevFtsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEXmDspDevFtsNO_EXcStCmprv: TWideStringField
      FieldName = 'NO_EXcStCmprv'
      Size = 60
    end
    object QrEXmDspDevFtsMobInstlSeq: TIntegerField
      FieldName = 'MobInstlSeq'
    end
    object QrEXmDspDevFtsSigla_IdFunc: TWideStringField
      FieldName = 'Sigla_IdFunc'
      Size = 15
    end
  end
  object DsEXmDspDevFts: TDataSource
    DataSet = QrEXmDspDevFts
    Left = 788
    Top = 352
  end
  object QrEXmDspDevBmp: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 584
    Top = 413
    object QrEXmDspDevBmpTxtBmp: TWideMemoField
      FieldName = 'TxtBmp'
      BlobType = ftWideMemo
    end
  end
end
