unit ComptncPrf;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmComptncPrf = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrComptncPrf: TMySQLQuery;
    DsComptncPrf: TDataSource;
    QrComptncIts: TMySQLQuery;
    DsComptncIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrComptncPrfCodigo: TIntegerField;
    QrComptncPrfNome: TWideStringField;
    QrComptncItsCodigo: TIntegerField;
    QrComptncItsControle: TIntegerField;
    QrComptncItsComptncCad: TIntegerField;
    QrComptncItsNO_ComptncCad: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrComptncPrfAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrComptncPrfBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrComptncPrfAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrComptncPrfBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenComptncIts(Controle: Integer);

  end;

var
  FmComptncPrf: TFmComptncPrf;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ComptncIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmComptncPrf.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmComptncPrf.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmComptncIts, FmComptncIts, afmoNegarComAviso) then
  begin
    FmComptncIts.ImgTipo.SQLType := SQLType;
    FmComptncIts.FQrCab := QrComptncPrf;
    FmComptncIts.FDsCab := DsComptncPrf;
    FmComptncIts.FQrIts := QrComptncIts;
    FmComptncIts.FCodigo := QrComptncPrfCodigo.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmComptncIts.EdControle.ValueVariant := QrComptncItsControle.Value;
      //
      FmComptncIts.EdComptncCad.ValueVariant := QrComptncItsComptncCad.Value;
      FmComptncIts.CBComptncCad.KeyValue     := QrComptncItsComptncCad.Value;
    end;
    FmComptncIts.ShowModal;
    FmComptncIts.Destroy;
  end;
end;

procedure TFmComptncPrf.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrComptncPrf);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrComptncPrf, QrComptncIts);
end;

procedure TFmComptncPrf.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrComptncPrf);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrComptncIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrComptncIts);
end;

procedure TFmComptncPrf.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrComptncPrfCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmComptncPrf.DefParams;
begin
  VAR_GOTOTABELA := 'comptncprf';
  VAR_GOTOMYSQLTABLE := QrComptncPrf;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM comptncprf');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmComptncPrf.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmComptncPrf.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmComptncPrf.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmComptncPrf.QueryPrincipalAfterOpen;
begin
end;

procedure TFmComptncPrf.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrComptncItsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrComptncIts,
      QrComptncItsControle, QrComptncItsControle.Value);
    ReopenComptncIts(Controle);
  end;
}
end;

procedure TFmComptncPrf.ReopenComptncIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrComptncIts, Dmod.MyDB, [
  'SELECT coc.Nome NO_ComptncCad, coi.*',
  'FROM comptncits coi',
  'LEFT JOIN comptnccad coc ON coc.Codigo=coi.ComptncCad',
  'WHERE coi.Codigo=' + Geral.FF0(QrComptncPrfCodigo.Value),
  '']);
  //
  QrComptncIts.Locate('Controle', Controle, []);
end;


procedure TFmComptncPrf.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmComptncPrf.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmComptncPrf.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmComptncPrf.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmComptncPrf.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmComptncPrf.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmComptncPrf.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrComptncPrfCodigo.Value;
  Close;
end;

procedure TFmComptncPrf.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmComptncPrf.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrComptncPrf, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'comptncprf');
end;

procedure TFmComptncPrf.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('comptncprf', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'comptncprf', False, [
  'Nome'], [
  'Codigo'], [
  Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmComptncPrf.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'comptncprf', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'comptncprf', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmComptncPrf.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmComptncPrf.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmComptncPrf.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmComptncPrf.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrComptncPrfCodigo.Value, LaRegistro.Caption);
end;

procedure TFmComptncPrf.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmComptncPrf.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrComptncPrfCodigo.Value, LaRegistro.Caption);
end;

procedure TFmComptncPrf.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmComptncPrf.QrComptncPrfAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmComptncPrf.QrComptncPrfAfterScroll(DataSet: TDataSet);
begin
  ReopenComptncIts(0);
end;

procedure TFmComptncPrf.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrComptncPrfCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmComptncPrf.SbQueryClick(Sender: TObject);
begin
  LocCod(QrComptncPrfCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'comptncprf', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmComptncPrf.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmComptncPrf.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrComptncPrf, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'comptncprf');
end;

procedure TFmComptncPrf.QrComptncPrfBeforeClose(
  DataSet: TDataSet);
begin
  QrComptncIts.Close;
end;

procedure TFmComptncPrf.QrComptncPrfBeforeOpen(DataSet: TDataSet);
begin
  QrComptncPrfCodigo.DisplayFormat := FFormatFloat;
end;

end.

