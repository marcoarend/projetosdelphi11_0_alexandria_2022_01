unit EXcGruCta;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkDBLookupComboBox, dmkEditCB, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO;

type
  TFmEXcGruCta = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEXcGruCta: TMySQLQuery;
    QrEXcGruCtaCodigo: TIntegerField;
    QrEXcGruCtaNome: TWideStringField;
    DsEXcGruCta: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrEXcGruIts: TMySQLQuery;
    QrEXcGruItsCodigo: TIntegerField;
    DsEXcGruIts: TDataSource;
    QrEXcGruItsNome: TWideStringField;
    DBGEXcGruIts: TdmkDBGridZTO;
    EdOrdem: TdmkEdit;
    Label4: TLabel;
    QrEXcGruCtaOrdem: TIntegerField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEXcGruCtaAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEXcGruCtaBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrEXcGruCtaAfterScroll(DataSet: TDataSet);
    procedure QrEXcGruCtaBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenEXcGruIts();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEXcGruCta: TFmEXcGruCta;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEXcGruCta.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEXcGruCta.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEXcGruCtaCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEXcGruCta.DefParams;
begin
  VAR_GOTOTABELA := 'excgructa';
  VAR_GOTOMYSQLTABLE := QrEXcGruCta;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT egc.*');
  VAR_SQLx.Add('FROM excgructa egc');
//  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=egc.Entidade');
  VAR_SQLx.Add('WHERE egc.Codigo > 0');
  //
  VAR_SQL1.Add('AND egc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND egc.Nome Like :P0');
  //
end;

procedure TFmEXcGruCta.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEXcGruCta.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEXcGruCta.ReopenEXcGruIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXcGruIts, Dmod.MyDB, [
  'SELECT Codigo, Nome  ',
  'FROM excctapag',
  'WHERE excgructa=' + Geral.FF0(QrEXcGruCtaCodigo.Value),
  'ORDER BY Nome ',
  '']);
end;

procedure TFmEXcGruCta.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEXcGruCta.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEXcGruCta.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEXcGruCta.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEXcGruCta.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEXcGruCta.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEXcGruCta.BtAlteraClick(Sender: TObject);
begin
  if (QrEXcGruCta.State <> dsInactive) and (QrEXcGruCta.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEXcGruCta, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'excgructa');
  end;
end;

procedure TFmEXcGruCta.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEXcGruCtaCodigo.Value;
  Close;
end;

procedure TFmEXcGruCta.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, Ordem: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
(*
  Entidade       := EdEntidade.ValueVariant;
  IDCntrCst      := EdIDCntrCst.ValueVariant;
*)
  Ordem          := EdOrdem.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
(*
  if MyObjects.FIC(Entidade = 0, EdNome, 'Defina uma entidade!') then Exit;
*)
  //
  //Codigo := UMyMod.BPGS1I32('excgructa', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'excgructa', False, [
  'Nome', 'Ordem'], [
  'Codigo'], [
  Nome, Ordem], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEXcGruCta.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'excgructa', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEXcGruCta.BtIncluiClick(Sender: TObject);
var
  Codigo: Integer;
  CodStr: String;
begin
  CodStr := '';
  if InputQuery('Nova Conta Cont�bil', 'Informe o c�digo:', CodStr) then
  begin
    Codigo := Geral.IMV(CodStr);
    if Codigo > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM excgructa ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      if Dmod.QrAux.RecordCount = 0 then
      begin
        UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEXcGruCta, [PnDados],
        [PnEdita], EdNome, ImgTipo, 'excgructa');
        EdCodigo.ValueVariant := Codigo;
      end else
      begin
        Geral.MB_Aviso('C�digo j� existe!');
        LocCod(Codigo, Codigo);
      end;
    end else
      Geral.MB_Aviso('C�digo inv�lido: ' + CodStr);
  end;
end;

procedure TFmEXcGruCta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //GBDados.Align := alClient;
  DBGEXcGruIts.Align := alClient;
  CriaOForm;
end;

procedure TFmEXcGruCta.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEXcGruCtaCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcGruCta.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEXcGruCta.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEXcGruCtaCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcGruCta.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEXcGruCta.QrEXcGruCtaAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEXcGruCta.QrEXcGruCtaAfterScroll(DataSet: TDataSet);
begin
  ReopenEXcGruIts();
end;

procedure TFmEXcGruCta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEXcGruCta.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEXcGruCtaCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'excgructa', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEXcGruCta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEXcGruCta.QrEXcGruCtaBeforeClose(DataSet: TDataSet);
begin
  QrEXcGruIts.Close;
end;

procedure TFmEXcGruCta.QrEXcGruCtaBeforeOpen(DataSet: TDataSet);
begin
  QrEXcGruCtaCodigo.DisplayFormat := FFormatFloat;
end;

end.

