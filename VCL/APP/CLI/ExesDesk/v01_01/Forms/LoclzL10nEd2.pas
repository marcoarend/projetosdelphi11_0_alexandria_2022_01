unit LoclzL10nEd2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmLoclzL10nEd2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label7: TLabel;
    EdTxt1ptBR: TdmkEdit;
    Panel5: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    EdID1: TdmkEdit;
    EdID1Descri: TdmkEdit;
    Label4: TLabel;
    EdID2Descri: TdmkEdit;
    EdID2: TdmkEdit;
    Label5: TLabel;
    EdTxt1enUS: TdmkEdit;
    Label1: TLabel;
    EdTxtTradu1: TdmkEdit;
    Label6: TLabel;
    Label8: TLabel;
    EdTxt2ptBR: TdmkEdit;
    Label9: TLabel;
    EdTxt2enUS: TdmkEdit;
    Label10: TLabel;
    EdTxtTradu2: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrIts(ID1, ID2: Integer);
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrIts: TmySQLQuery;
    FIdxFld1, FIdxFld2, FFldTradu1, FFldTradu2, FTableName: String;
    FIdxVal1, FIdxVal2: Integer;
  end;

  var
  FmLoclzL10nEd2: TFmLoclzL10nEd2;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmLoclzL10nEd2.BtOKClick(Sender: TObject);
var
  TxtTradu1, TxtTradu2: String;
  ID1, ID2: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  ID1            := EdID1.ValueVariant;
  ID2            := EdID2.ValueVariant;
  TxtTradu1      := EdTxtTradu1.Text;
  TxtTradu2      := EdTxtTradu2.Text;
  //
  //ou > ? := UMyMod.BPGS1I32('loclzl10ncta', 'EXcCtaPag', 'LoclzL10nCab', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTableName, False, [
  FFldTradu1, FFldTradu2], [
  FIdxFld1, FIdxFld2], [
  TxtTradu1, TxtTradu2], [
  ID1, ID2], True) then
  begin
    ReopenQrIts(ID1, ID2);
(*
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else*) Close;
  end;
end;

procedure TFmLoclzL10nEd2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLoclzL10nEd2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLoclzL10nEd2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //UnDmkDAC_PF.AbreQuery(Qr_Sel_, Dmod.MyDB?);
end;

procedure TFmLoclzL10nEd2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLoclzL10nEd2.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
end;

procedure TFmLoclzL10nEd2.ReopenQrIts(ID1, ID2: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    try
      FQrIts.Locate(FIdxFld1 + ';' + FIdxFld2, VarArrayOf([ID1, ID2]), []);
    except
      //
    end;
  end;
end;

end.
