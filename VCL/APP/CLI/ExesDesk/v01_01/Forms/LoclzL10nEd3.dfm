object FmLoclzL10nEd3: TFmLoclzL10nEd3
  Left = 339
  Top = 185
  Caption = 'LNG-INTLZ-006 :: Localiza'#231#227'o: Textos em outras l'#237'nguas (3)'
  ClientHeight = 452
  ClientWidth = 774
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 774
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 726
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 678
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 500
        Height = 32
        Caption = 'Localiza'#231#227'o: Textos em outras l'#237'nguas (3)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 500
        Height = 32
        Caption = 'Localiza'#231#227'o: Textos em outras l'#237'nguas (3)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 500
        Height = 32
        Caption = 'Localiza'#231#227'o: Textos em outras l'#237'nguas (3)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 338
    Width = 774
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 770
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 382
    Width = 774
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 628
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 626
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 97
    Width = 774
    Height = 241
    Align = alClient
    TabOrder = 0
    object Label7: TLabel
      Left = 12
      Top = 4
      Width = 98
      Height = 13
      Caption = 'Descri'#231#227'o em pt-BR:'
    end
    object Label1: TLabel
      Left = 304
      Top = 4
      Width = 101
      Height = 13
      Caption = 'Descri'#231#227'o em en-US:'
    end
    object Label6: TLabel
      Left = 12
      Top = 44
      Width = 113
      Height = 13
      Caption = 'Tradu'#231#227'o da descri'#231#227'o:'
    end
    object Label8: TLabel
      Left = 304
      Top = 44
      Width = 26
      Height = 13
      Caption = 'Sigla:'
      Color = clBtnFace
      ParentColor = False
    end
    object EdTxt1ptBR: TdmkEdit
      Left = 12
      Top = 20
      Width = 289
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdTxt1enUS: TdmkEdit
      Left = 304
      Top = 20
      Width = 289
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdTxtTradu1: TdmkEdit
      Left = 12
      Top = 60
      Width = 289
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdSigla: TdmkEdit
      Left = 304
      Top = 60
      Width = 90
      Height = 21
      MaxLength = 10
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Sigla'
      UpdCampo = 'Sigla'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object GroupBox1: TGroupBox
      Left = 12
      Top = 84
      Width = 753
      Height = 149
      Caption = ' C'#243'digos de app terceiros: (exporta'#231#227'o de dados)'
      TabOrder = 4
      object Panel9: TPanel
        Left = 2
        Top = 15
        Width = 749
        Height = 132
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label15: TLabel
          Left = 8
          Top = 8
          Width = 53
          Height = 13
          Caption = 'Reference:'
        end
        object Label16: TLabel
          Left = 168
          Top = 8
          Width = 61
          Height = 13
          Caption = 'Doc Header:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label17: TLabel
          Left = 8
          Top = 48
          Width = 43
          Height = 13
          Caption = 'Item text:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label10: TLabel
          Left = 8
          Top = 88
          Width = 114
          Height = 13
          Caption = 'Condi'#231#227'o de reembolso:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
          Visible = False
        end
        object Label13: TLabel
          Left = 496
          Top = 88
          Width = 28
          Height = 13
          Caption = 'Doas:'
          Color = clBtnFace
          ParentColor = False
        end
        object EdDocHeader: TdmkEdit
          Left = 168
          Top = 24
          Width = 245
          Height = 21
          MaxLength = 25
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'DocHeader'
          UpdCampo = 'DocHeader'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdItemText: TdmkEdit
          Left = 8
          Top = 64
          Width = 481
          Height = 21
          MaxLength = 50
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ItemText'
          UpdCampo = 'ItemText'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdReference: TdmkEdit
          Left = 8
          Top = 24
          Width = 153
          Height = 21
          MaxLength = 16
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Reference'
          UpdCampo = 'Reference'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdDescrCondiPg: TdmkEdit
          Left = 8
          Top = 104
          Width = 481
          Height = 21
          Enabled = False
          MaxLength = 50
          TabOrder = 3
          Visible = False
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'DescrCondiPg'
          UpdCampo = 'DescrCondiPg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGRegrTipSeq: TdmkRadioGroup
          Left = 492
          Top = 0
          Width = 105
          Height = 89
          Caption = ' Tipo de prazo: '
          ItemIndex = 0
          Items.Strings = (
            'N'#227'o informar'
            'Dia corrido'
            'Dia '#250'til'
            'Semana corrida')
          TabOrder = 4
          OnClick = RGRegrTipSeqClick
          QryCampo = 'RegrTipSeq'
          UpdCampo = 'RegrTipSeq'
          UpdType = utYes
          OldValor = 0
        end
        object EdRegrQtdDia: TdmkEdit
          Left = 496
          Top = 104
          Width = 61
          Height = 21
          Alignment = taRightJustify
          MaxLength = 25
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '1'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          QryCampo = 'RegrQtdDia'
          UpdCampo = 'EdRegrQtdDia'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
        object RGRegrDiaSem: TdmkRadioGroup
          Left = 596
          Top = 0
          Width = 145
          Height = 125
          Caption = ' Dia da semana: '
          Columns = 2
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'N/D'
            'Domingo'
            'Segunda'
            'Ter'#231'a'
            'Quarta'
            'Quinta'
            'Sexta'
            'S'#225'bado')
          TabOrder = 6
          QryCampo = 'RegrDiaSem'
          UpdCampo = 'RegrDiaSem'
          UpdType = utYes
          OldValor = 0
        end
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 48
    Width = 774
    Height = 49
    Align = alTop
    Enabled = False
    TabOrder = 4
    object Label2: TLabel
      Left = 8
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label3: TLabel
      Left = 92
      Top = 4
      Width = 50
      Height = 13
      Caption = 'Compania:'
    end
    object Label4: TLabel
      Left = 468
      Top = 4
      Width = 34
      Height = 13
      Caption = 'Idioma:'
    end
    object Label5: TLabel
      Left = 384
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object EdID1: TdmkEdit
      Left = 8
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdID1Descri: TdmkEdit
      Left = 92
      Top = 20
      Width = 289
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdID2Descri: TdmkEdit
      Left = 468
      Top = 20
      Width = 289
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdID2: TdmkEdit
      Left = 384
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
end
