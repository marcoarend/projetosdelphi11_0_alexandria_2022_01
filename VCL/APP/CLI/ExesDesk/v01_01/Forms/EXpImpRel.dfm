object FmEXpImpRel: TFmEXpImpRel
  Left = 339
  Top = 185
  Caption = 'EXS-RELAT-001 :: Relat'#243'rios de Presta'#231#227'o de Contas'
  ClientHeight = 421
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 424
        Height = 32
        Caption = 'Relat'#243'rios de Presta'#231#227'o de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 424
        Height = 32
        Caption = 'Relat'#243'rios de Presta'#231#227'o de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 424
        Height = 32
        Caption = 'Relat'#243'rios de Presta'#231#227'o de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 307
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 351
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 259
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaPrompt1: TLabel
        Left = 12
        Top = 0
        Width = 78
        Height = 13
        Caption = 'Centro de custo:'
      end
      object SbDecrementaAnpMes: TSpeedButton
        Left = 688
        Top = 15
        Width = 23
        Height = 23
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = SbDecrementaAnpMesClick
      end
      object SbIncrementaAnpMes: TSpeedButton
        Left = 788
        Top = 15
        Width = 23
        Height = 23
        Caption = '+'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = SbIncrementaAnpMesClick
      end
      object EdIDCntrCst: TdmkEditCB
        Left = 16
        Top = 16
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBIDCntrCst
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBIDCntrCst: TdmkDBLookupComboBox
        Left = 80
        Top = 16
        Width = 601
        Height = 21
        KeyField = 'IDCntrCst'
        ListField = 'Nome'
        ListSource = DsEXcIDFunc
        TabOrder = 1
        dmkEditCB = EdIDCntrCst
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdAnoMes: TdmkEdit
        Left = 712
        Top = 16
        Width = 75
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 4
    Top = 7
  end
  object QrEXcIDFunc: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM stqalocits')
    Left = 572
    Top = 4
    object QrEXcIDFuncIDCntrCst: TIntegerField
      FieldName = 'IDCntrCst'
    end
    object QrEXcIDFuncNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEXcIDFunc: TDataSource
    DataSet = QrEXcIDFunc
    Left = 572
    Top = 52
  end
  object QrSumCtb: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '  SELECT ecp.EXcGruCta, egc.Nome NO_EXcGruCta,'
      '  its.EXcMdoPag, emp.Nome NO_EXcMdoPag,'
      '  IF(SiglaDst <> "", SiglaDst, SiglaOri) Sigla,'
      
        '  SUM(IF(EXcMdoPag=1, IF(SiglaDst <> "", ValorDst, ValorOri), 0)' +
        ') ValMdo001,'
      
        '  SUM(IF(EXcMdoPag=2, IF(SiglaDst <> "", ValorDst, ValorOri), 0)' +
        ') ValMdo002,'
      
        '  SUM(IF(EXcMdoPag=3, IF(SiglaDst <> "", ValorDst, ValorOri), 0)' +
        ') ValMdo003,'
      '  SUM(IF(SiglaDst <> "", ValorDst, ValorOri)) Valor'
      '  FROM exmdspdevits its'
      '  LEFT JOIN EXcCtaPag ecp ON ecp.Codigo=its.EXcCtaPag'
      '  LEFT JOIN EXcGruCta egc ON egc.Codigo=ecp.EXcGruCta'
      '  LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag'
      '  WHERE its.AnoMes="2021-07"'
      '  AND IDCntrCst=18522'
      '  GROUP BY egc.Codigo, Sigla'
      '  /*ORDER BY egc.Ordem*/')
    Left = 196
    Top = 76
    object QrSumCtbEXcGruCta: TIntegerField
      FieldName = 'EXcGruCta'
    end
    object QrSumCtbNO_EXcGruCta: TWideStringField
      FieldName = 'NO_EXcGruCta'
      Size = 60
    end
    object QrSumCtbEXcMdoPag: TIntegerField
      FieldName = 'EXcMdoPag'
      Required = True
    end
    object QrSumCtbNO_EXcMdoPag: TWideStringField
      FieldName = 'NO_EXcMdoPag'
      Size = 60
    end
    object QrSumCtbSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 10
    end
    object QrSumCtbValMdo001: TFloatField
      FieldName = 'ValMdo001'
    end
    object QrSumCtbValMdo002: TFloatField
      FieldName = 'ValMdo002'
    end
    object QrSumCtbValMdo003: TFloatField
      FieldName = 'ValMdo003'
    end
    object QrSumCtbValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxEXS_RELAT_001_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41690.713452719910000000
    ReportOptions.LastChange = 41690.713452719910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxEXS_RELAT_001_AGetValue
    Left = 440
    Top = 228
    Datasets = <
      item
        DataSet = frxDsItsCta
        DataSetName = 'frxDsItsCta'
      end
      item
        DataSet = frxDsItsCtb
        DataSetName = 'frxDsItsCtb'
      end
      item
        DataSet = frxDsLanctos
        DataSetName = 'frxDsLanctos'
      end
      item
        DataSet = frxDsSumCta
        DataSetName = 'frxDsSumCta'
      end
      item
        DataSet = frxDsSumCtb
        DataSetName = 'frxDsSumCtb'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370130000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line_PH_01: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE DESPESAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 41.574830000000000000
          Width = 570.709030000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_CENTRO_CUSTO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 60.472480000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 90.708656540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Centro de Custo:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 1152.756650000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 18.897637800000000000
        ParentFont = False
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSumCtb
        DataSetName = 'frxDsSumCtb'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692950000000000000
          Height = 18.897637800000000000
          DataField = 'EXcGruCta'
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsSumCtb."EXcGruCta"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 321.260050000000000000
          Height = 18.897637800000000000
          DataField = 'NO_EXcGruCta'
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsSumCtb."NO_EXcGruCta"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataField = 'ValMdo001'
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumCtb."ValMdo001"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataField = 'ValMdo002'
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumCtb."ValMdo002"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataField = 'ValMdo003'
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumCtb."ValMdo003"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataField = 'Valor'
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumCtb."Valor"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.692950000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 37.795300000000000000
          Width = 321.260050000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta cont'#225'bil')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 37.795300000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_A]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 37.795300000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_B]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 37.795300000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_C]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 37.795300000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 680.315400000000000000
          Height = 26.456697800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Contas Cont'#225'beis - Sint'#233'tico')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSumCtb."ValMdo001">,MD1)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSumCtb."ValMdo002">,MD1)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSumCtb."ValMdo003">,MD1)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSumCtb."Valor">,MD1)]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 18.897637800000000000
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
        end
      end
      object MD2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 15.118110240000000000
        ParentFont = False
        Top = 400.630180000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSumCta
        DataSetName = 'frxDsSumCta'
        RowCount = 0
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'EXcGruCta'
          DataSet = frxDsSumCta
          DataSetName = 'frxDsSumCta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumCta."EXcGruCta"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 181.417440000000000000
          Height = 15.118110240000000000
          DataField = 'NO_EXcGruCta'
          DataSet = frxDsSumCta
          DataSetName = 'frxDsSumCta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsSumCta."NO_EXcGruCta"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValMdo001'
          DataSet = frxDsSumCta
          DataSetName = 'frxDsSumCta'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumCta."ValMdo001"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValMdo002'
          DataSet = frxDsSumCta
          DataSetName = 'frxDsSumCta'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumCta."ValMdo002"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValMdo003'
          DataSet = frxDsSumCta
          DataSetName = 'frxDsSumCta'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumCta."ValMdo003"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsSumCta
          DataSetName = 'frxDsSumCta'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumCta."Valor"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          DataField = 'EXcCtaPag'
          DataSet = frxDsSumCta
          DataSetName = 'frxDsSumCta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSumCta."EXcCtaPag"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          DataField = 'NO_EXcCtaPag'
          DataSet = frxDsSumCta
          DataSetName = 'frxDsSumCta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsSumCta."NO_EXcCtaPag"]')
          ParentFont = False
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913420000000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 680.315400000000000000
          Height = 26.456697800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Contas de Despesas - Sint'#233'tico')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 37.795300000000000000
          Width = 181.417440000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta cont'#225'bil')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_A]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_B]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_C]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 37.795300000000000000
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 37.795300000000000000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta de Despesa')
          ParentFont = False
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSumCta."ValMdo001">,MD2)]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSumCta."ValMdo002">,MD2)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSumCta."ValMdo003">,MD2)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSumCta."Valor">,MD2)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsSumCtb
          DataSetName = 'frxDsSumCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
        end
      end
      object MD3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 18.897637800000000000
        ParentFont = False
        Top = 600.945270000000000000
        Width = 680.315400000000000000
        DataSet = frxDsItsCtb
        DataSetName = 'frxDsItsCtb'
        RowCount = 0
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataField = 'ValMdo001'
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsCtb."ValMdo001"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataField = 'ValMdo002'
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsCtb."ValMdo002"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataField = 'ValMdo003'
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsCtb."ValMdo003"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataField = 'Valor'
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsCtb."Valor"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913390710000000000
          Height = 18.897637800000000000
          DataField = 'Data'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCta."Data"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 41.574805590000000000
          Height = 18.897637800000000000
          DataField = 'Hora'
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = 'hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCtb."Hora"]')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 283.464725590000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCtb."Observa"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.692950000000000000
        Top = 480.000310000000000000
        Width = 680.315400000000000000
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 37.795300000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_A]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 37.795300000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_B]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 37.795300000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_C]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 37.795300000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 680.315400000000000000
          Height = 26.456697800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Contas Cont'#225'beis - Anal'#237'tico')
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 37.795300000000000000
          Width = 283.464725590000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
      end
      object Footer3: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 684.094930000000000000
        Width = 680.315400000000000000
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCtb."ValMdo001">,MD3)]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCtb."ValMdo002">,MD3)]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCtb."ValMdo003">,MD3)]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCtb."Valor">,MD3)]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
        end
      end
      object MD4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 15.118110240000000000
        ParentFont = False
        Top = 842.835190000000000000
        Width = 680.315400000000000000
        DataSet = frxDsItsCta
        DataSetName = 'frxDsItsCta'
        RowCount = 0
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'EXcGruCta'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCta."EXcGruCta"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'NO_EXcGruCta'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCta."NO_EXcGruCta"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValMdo001'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsCta."ValMdo001"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValMdo002'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsCta."ValMdo002"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValMdo003'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsCta."ValMdo003"]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsCta."Valor"]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795275590551180000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCta."Data"]')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'Hora'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          DisplayFormat.FormatStr = 'hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCta."Hora"]')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Width = 241.889846770000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCta."Observa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header4: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913420000000000000
        Top = 729.449290000000000000
        Width = 680.315400000000000000
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 680.315400000000000000
          Height = 26.456697800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Contas de Despesas - Anal'#237'tico')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Top = 37.795300000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 37.795300000000000000
          Width = 113.385826771653500000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta cont'#225'bil')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_A]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_B]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_C]')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 37.795300000000000000
          Width = 241.889920000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 37.795275590551180000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 37.795300000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
        end
      end
      object Footer4: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 918.425790000000000000
        Width = 680.315400000000000000
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCta."ValMdo001">,MD4)]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCta."ValMdo002">,MD4)]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCta."ValMdo003">,MD4)]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCta."Valor">,MD4)]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 559.370440000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsItsCtb."NO_EXcGruCta"'
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692950000000000000
          Height = 18.897637800000000000
          DataField = 'EXcGruCta'
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsItsCtb."EXcGruCta"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 623.622450000000000000
          Height = 18.897637800000000000
          DataField = 'NO_EXcGruCta'
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCtb."NO_EXcGruCta"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 642.520100000000000000
        Width = 680.315400000000000000
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCtb."ValMdo001">,MD3)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCtb."ValMdo002">,MD3)]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCtb."ValMdo003">,MD3)]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 75.590526770000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCtb."Valor">,MD3)]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 283.464627950000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCtb."EXcGruCta"] - [frxDsItsCtb."NO_EXcGruCta"] ')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo132: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 18.897637800000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 805.039890000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsItsCta."EXcCtaPag"'
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = -0.000029290000000000
          Width = 45.354335590000000000
          Height = 15.118110240000000000
          DataField = 'EXcCtaPag'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCta."EXcCtaPag"]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 638.740570000000000000
          Height = 15.118110240000000000
          DataField = 'NO_EXcCtaPag'
          DataSet = frxDsItsCta
          DataSetName = 'frxDsItsCta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItsCta."NO_EXcCtaPag"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 880.630490000000000000
        Width = 680.315400000000000000
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCta."ValMdo001">,MD4)]')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCta."ValMdo002">,MD4)]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCta."ValMdo003">,MD4)]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsCta."Valor">,MD4)]')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [frxDsItsCta."NO_EXcCtaPag"] ')
          ParentFont = False
        end
      end
      object MD5: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 15.118110240000000000
        ParentFont = False
        Top = 1035.591220000000000000
        Width = 680.315400000000000000
        DataSet = frxDsLanctos
        DataSetName = 'frxDsLanctos'
        RowCount = 0
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'EXcGruCta'
          DataSet = frxDsLanctos
          DataSetName = 'frxDsLanctos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLanctos."EXcGruCta"]')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 136.063080000000000000
          Height = 15.118110240000000000
          DataField = 'NO_EXcGruCta'
          DataSet = frxDsLanctos
          DataSetName = 'frxDsLanctos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLanctos."NO_EXcGruCta"]')
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValMdo001'
          DataSet = frxDsLanctos
          DataSetName = 'frxDsLanctos'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLanctos."ValMdo001"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValMdo002'
          DataSet = frxDsLanctos
          DataSetName = 'frxDsLanctos'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLanctos."ValMdo002"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'ValMdo003'
          DataSet = frxDsLanctos
          DataSetName = 'frxDsLanctos'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLanctos."ValMdo003"]')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsLanctos
          DataSetName = 'frxDsLanctos'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLanctos."Valor"]')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsLanctos
          DataSetName = 'frxDsLanctos'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLanctos."Data"]')
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Hora'
          DataSet = frxDsLanctos
          DataSetName = 'frxDsLanctos'
          DisplayFormat.FormatStr = 'hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLanctos."Hora"]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          DataField = 'EXcCtaPag'
          DataSet = frxDsLanctos
          DataSetName = 'frxDsLanctos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLanctos."EXcCtaPag"]')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 158.740260000000000000
          Height = 15.118110240000000000
          DataField = 'NO_EXcCtaPag'
          DataSet = frxDsLanctos
          DataSetName = 'frxDsLanctos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLanctos."NO_EXcCtaPag"]')
          ParentFont = False
        end
      end
      object Header5: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913420000000000000
        Top = 960.000620000000000000
        Width = 680.315400000000000000
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 680.315400000000000000
          Height = 26.456697800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Extrato Cronol'#243'gico das Despesas')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 37.795300000000000000
          Width = 136.063080000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta cont'#225'bil')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_A]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_B]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CAMPO_C]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 37.795300000000000000
          Width = 30.236210710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 37.795300000000000000
          Width = 158.740260000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta de Despesa')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
        end
      end
      object Footer5: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 1073.386520000000000000
        Width = 680.315400000000000000
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLanctos."ValMdo001">,MD5)]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLanctos."ValMdo002">,MD5)]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLanctos."ValMdo003">,MD5)]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLanctos."Valor">,MD5)]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsCtb
          DataSetName = 'frxDsItsCtb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
        end
      end
    end
  end
  object frxDsSumCtb: TfrxDBDataset
    UserName = 'frxDsSumCtb'
    CloseDataSource = False
    FieldAliases.Strings = (
      'EXcGruCta=EXcGruCta'
      'NO_EXcGruCta=NO_EXcGruCta'
      'EXcMdoPag=EXcMdoPag'
      'NO_EXcMdoPag=NO_EXcMdoPag'
      'Sigla=Sigla'
      'ValMdo001=ValMdo001'
      'ValMdo002=ValMdo002'
      'ValMdo003=ValMdo003'
      'Valor=Valor')
    DataSet = QrSumCtb
    BCDToCurrency = False
    DataSetOptions = []
    Left = 196
    Top = 124
  end
  object QrSumCta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ecp.EXcGruCta, egc.Nome NO_EXcGruCta,  '
      'its.EXcCtaPag, ecp.Nome NO_EXcCtaPag,  '
      'its.EXcMdoPag, emp.Nome NO_EXcMdoPag, '
      'IF(SiglaDst <> "", SiglaDst, SiglaOri) Sigla, '
      
        'SUM(IF(EXcMdoPag=1, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ' +
        'ValMdo001, '
      
        'SUM(IF(EXcMdoPag=2, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ' +
        'ValMdo002, '
      
        'SUM(IF(EXcMdoPag=3, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ' +
        'ValMdo003, '
      'SUM(IF(SiglaDst <> "", ValorDst, ValorOri)) Valor '
      'FROM exmdspdevits its '
      'LEFT JOIN EXcCtaPag ecp ON ecp.Codigo=its.EXcCtaPag '
      'LEFT JOIN EXcGruCta egc ON egc.Codigo=ecp.EXcGruCta '
      'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag  '
      'WHERE its.AnoMes="2021-07" '
      'AND IDCntrCst=18522'
      'GROUP BY egc.Codigo, ecp.Codigo, Sigla '
      'ORDER BY egc.Ordem, NO_EXcGruCta, ecp.Ordem, NO_EXcCtaPag ')
    Left = 288
    Top = 76
    object QrSumCtaEXcGruCta: TIntegerField
      FieldName = 'EXcGruCta'
    end
    object QrSumCtaNO_EXcGruCta: TWideStringField
      FieldName = 'NO_EXcGruCta'
      Size = 60
    end
    object QrSumCtaEXcCtaPag: TIntegerField
      FieldName = 'EXcCtaPag'
      Required = True
    end
    object QrSumCtaNO_EXcCtaPag: TWideStringField
      FieldName = 'NO_EXcCtaPag'
      Size = 60
    end
    object QrSumCtaEXcMdoPag: TIntegerField
      FieldName = 'EXcMdoPag'
      Required = True
    end
    object QrSumCtaNO_EXcMdoPag: TWideStringField
      FieldName = 'NO_EXcMdoPag'
      Size = 60
    end
    object QrSumCtaSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 10
    end
    object QrSumCtaValMdo001: TFloatField
      FieldName = 'ValMdo001'
    end
    object QrSumCtaValMdo002: TFloatField
      FieldName = 'ValMdo002'
    end
    object QrSumCtaValMdo003: TFloatField
      FieldName = 'ValMdo003'
    end
    object QrSumCtaValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxDsSumCta: TfrxDBDataset
    UserName = 'frxDsSumCta'
    CloseDataSource = False
    FieldAliases.Strings = (
      'EXcGruCta=EXcGruCta'
      'NO_EXcGruCta=NO_EXcGruCta'
      'EXcCtaPag=EXcCtaPag'
      'NO_EXcCtaPag=NO_EXcCtaPag'
      'EXcMdoPag=EXcMdoPag'
      'NO_EXcMdoPag=NO_EXcMdoPag'
      'Sigla=Sigla'
      'ValMdo001=ValMdo001'
      'ValMdo002=ValMdo002'
      'ValMdo003=ValMdo003'
      'Valor=Valor')
    DataSet = QrSumCta
    BCDToCurrency = False
    DataSetOptions = []
    Left = 288
    Top = 124
  end
  object QrItsCtb: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ecp.EXcGruCta, egc.Nome NO_EXcGruCta,  '
      'its.EXcMdoPag, emp.Nome NO_EXcMdoPag, '
      'IF(SiglaDst <> "", SiglaDst, SiglaOri) Sigla, '
      
        '(IF(EXcMdoPag=1, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) Val' +
        'Mdo001, '
      
        '(IF(EXcMdoPag=2, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) Val' +
        'Mdo002, '
      
        '(IF(EXcMdoPag=3, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) Val' +
        'Mdo003, '
      '(IF(SiglaDst <> "", ValorDst, ValorOri)) Valor '
      'FROM exmdspdevits its '
      'LEFT JOIN EXcCtaPag ecp ON ecp.Codigo=its.EXcCtaPag '
      'LEFT JOIN EXcGruCta egc ON egc.Codigo=ecp.EXcGruCta '
      'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag  '
      'WHERE its.AnoMes="2021-07" '
      'AND IDCntrCst=18522'
      ''
      'ORDER BY egc.Ordem, egc.Nome ')
    Left = 196
    Top = 172
    object QrItsCtbEXcGruCta: TIntegerField
      FieldName = 'EXcGruCta'
    end
    object QrItsCtbNO_EXcGruCta: TWideStringField
      FieldName = 'NO_EXcGruCta'
      Size = 60
    end
    object QrItsCtbEXcMdoPag: TIntegerField
      FieldName = 'EXcMdoPag'
      Required = True
    end
    object QrItsCtbNO_EXcMdoPag: TWideStringField
      FieldName = 'NO_EXcMdoPag'
      Size = 60
    end
    object QrItsCtbSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 10
    end
    object QrItsCtbValMdo001: TFloatField
      FieldName = 'ValMdo001'
    end
    object QrItsCtbValMdo002: TFloatField
      FieldName = 'ValMdo002'
    end
    object QrItsCtbValMdo003: TFloatField
      FieldName = 'ValMdo003'
    end
    object QrItsCtbData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrItsCtbHora: TTimeField
      FieldName = 'Hora'
      Required = True
    end
    object QrItsCtbPartConta: TFloatField
      FieldName = 'PartConta'
      Required = True
    end
    object QrItsCtbPartOutra: TFloatField
      FieldName = 'PartOutra'
      Required = True
    end
    object QrItsCtbValor: TFloatField
      FieldName = 'Valor'
    end
    object QrItsCtbObserva: TWideStringField
      FieldName = 'Observa'
      Size = 511
    end
  end
  object frxDsItsCtb: TfrxDBDataset
    UserName = 'frxDsItsCtb'
    CloseDataSource = False
    FieldAliases.Strings = (
      'EXcGruCta=EXcGruCta'
      'NO_EXcGruCta=NO_EXcGruCta'
      'EXcMdoPag=EXcMdoPag'
      'NO_EXcMdoPag=NO_EXcMdoPag'
      'Sigla=Sigla'
      'ValMdo001=ValMdo001'
      'ValMdo002=ValMdo002'
      'ValMdo003=ValMdo003'
      'Data=Data'
      'Hora=Hora'
      'PartConta=PartConta'
      'PartOutra=PartOutra'
      'Valor=Valor'
      'Observa=Observa')
    DataSet = QrItsCtb
    BCDToCurrency = False
    DataSetOptions = []
    Left = 196
    Top = 220
  end
  object QrItsCta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ecp.EXcGruCta, egc.Nome NO_EXcGruCta,  '
      'its.EXcCtaPag, ecp.Nome NO_EXcCtaPag,  '
      'its.EXcMdoPag, emp.Nome NO_EXcMdoPag, '
      'IF(SiglaDst <> "", SiglaDst, SiglaOri) Sigla, '
      
        '(IF(EXcMdoPag=1, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) Val' +
        'Mdo001, '
      
        '(IF(EXcMdoPag=2, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) Val' +
        'Mdo002, '
      
        '(IF(EXcMdoPag=3, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) Val' +
        'Mdo003, '
      '(IF(SiglaDst <> "", ValorDst, ValorOri)) Valor '
      'FROM exmdspdevits its '
      'LEFT JOIN EXcCtaPag ecp ON ecp.Codigo=its.EXcCtaPag '
      'LEFT JOIN EXcGruCta egc ON egc.Codigo=ecp.EXcGruCta '
      'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag  '
      'WHERE its.AnoMes="2021-07" '
      'AND IDCntrCst=18522'
      ''
      'ORDER BY egc.Ordem, NO_EXcGruCta, ecp.Ordem, NO_EXcCtaPag ')
    Left = 288
    Top = 172
    object QrItsCtaEXcGruCta: TIntegerField
      FieldName = 'EXcGruCta'
    end
    object QrItsCtaNO_EXcGruCta: TWideStringField
      FieldName = 'NO_EXcGruCta'
      Size = 60
    end
    object QrItsCtaEXcCtaPag: TIntegerField
      FieldName = 'EXcCtaPag'
      Required = True
    end
    object QrItsCtaNO_EXcCtaPag: TWideStringField
      FieldName = 'NO_EXcCtaPag'
      Size = 60
    end
    object QrItsCtaEXcMdoPag: TIntegerField
      FieldName = 'EXcMdoPag'
      Required = True
    end
    object QrItsCtaNO_EXcMdoPag: TWideStringField
      FieldName = 'NO_EXcMdoPag'
      Size = 60
    end
    object QrItsCtaSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 10
    end
    object QrItsCtaValMdo001: TFloatField
      FieldName = 'ValMdo001'
    end
    object QrItsCtaValMdo002: TFloatField
      FieldName = 'ValMdo002'
    end
    object QrItsCtaValMdo003: TFloatField
      FieldName = 'ValMdo003'
    end
    object QrItsCtaData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrItsCtaHora: TTimeField
      FieldName = 'Hora'
      Required = True
    end
    object QrItsCtaPartConta: TFloatField
      FieldName = 'PartConta'
      Required = True
    end
    object QrItsCtaPartOutra: TFloatField
      FieldName = 'PartOutra'
      Required = True
    end
    object QrItsCtaValor: TFloatField
      FieldName = 'Valor'
    end
    object QrItsCtaObserva: TWideStringField
      FieldName = 'Observa'
      Size = 511
    end
  end
  object frxDsItsCta: TfrxDBDataset
    UserName = 'frxDsItsCta'
    CloseDataSource = False
    FieldAliases.Strings = (
      'EXcGruCta=EXcGruCta'
      'NO_EXcGruCta=NO_EXcGruCta'
      'EXcCtaPag=EXcCtaPag'
      'NO_EXcCtaPag=NO_EXcCtaPag'
      'EXcMdoPag=EXcMdoPag'
      'NO_EXcMdoPag=NO_EXcMdoPag'
      'Sigla=Sigla'
      'ValMdo001=ValMdo001'
      'ValMdo002=ValMdo002'
      'ValMdo003=ValMdo003'
      'Data=Data'
      'Hora=Hora'
      'PartConta=PartConta'
      'PartOutra=PartOutra'
      'Valor=Valor'
      'Observa=Observa')
    DataSet = QrItsCta
    BCDToCurrency = False
    DataSetOptions = []
    Left = 288
    Top = 220
  end
  object QrLanctos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT its.Data, its.Hora, PartConta, PartOutra, '
      'ecp.EXcGruCta, egc.Nome NO_EXcGruCta,  '
      'its.EXcCtaPag, ecp.Nome NO_EXcCtaPag,  '
      'its.EXcMdoPag, emp.Nome NO_EXcMdoPag, '
      'IF(SiglaDst <> "", SiglaDst, SiglaOri) Sigla, '
      
        '(IF(EXcMdoPag=1, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) Val' +
        'Mdo001, '
      
        '(IF(EXcMdoPag=2, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) Val' +
        'Mdo002, '
      
        '(IF(EXcMdoPag=3, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) Val' +
        'Mdo003, '
      '(IF(SiglaDst <> "", ValorDst, ValorOri)) Valor '
      'FROM exmdspdevits its '
      'LEFT JOIN EXcCtaPag ecp ON ecp.Codigo=its.EXcCtaPag '
      'LEFT JOIN EXcGruCta egc ON egc.Codigo=ecp.EXcGruCta '
      'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag  '
      'WHERE its.AnoMes="2021-07" '
      'AND IDCntrCst=18522'
      ''
      'ORDER BY its.Data, its.Hora, egc.Ordem, egc.Nome ')
    Left = 384
    Top = 76
    object QrLanctosData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrLanctosHora: TTimeField
      FieldName = 'Hora'
      Required = True
    end
    object QrLanctosPartConta: TFloatField
      FieldName = 'PartConta'
      Required = True
    end
    object QrLanctosPartOutra: TFloatField
      FieldName = 'PartOutra'
      Required = True
    end
    object QrLanctosEXcGruCta: TIntegerField
      FieldName = 'EXcGruCta'
    end
    object QrLanctosNO_EXcGruCta: TWideStringField
      FieldName = 'NO_EXcGruCta'
      Size = 60
    end
    object QrLanctosNO_EXcMdoPag: TWideStringField
      FieldName = 'NO_EXcMdoPag'
      Size = 60
    end
    object QrLanctosSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 10
    end
    object QrLanctosValMdo001: TFloatField
      FieldName = 'ValMdo001'
      Required = True
    end
    object QrLanctosValMdo002: TFloatField
      FieldName = 'ValMdo002'
      Required = True
    end
    object QrLanctosValMdo003: TFloatField
      FieldName = 'ValMdo003'
      Required = True
    end
    object QrLanctosValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrLanctosEXcCtaPag: TIntegerField
      FieldName = 'EXcCtaPag'
      Required = True
    end
    object QrLanctosNO_EXcCtaPag: TWideStringField
      FieldName = 'NO_EXcCtaPag'
      Size = 60
    end
    object QrLanctosEXcMdoPag: TIntegerField
      FieldName = 'EXcMdoPag'
      Required = True
    end
  end
  object frxDsLanctos: TfrxDBDataset
    UserName = 'frxDsLanctos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Hora=Hora'
      'PartConta=PartConta'
      'PartOutra=PartOutra'
      'EXcGruCta=EXcGruCta'
      'NO_EXcGruCta=NO_EXcGruCta'
      'NO_EXcMdoPag=NO_EXcMdoPag'
      'Sigla=Sigla'
      'ValMdo001=ValMdo001'
      'ValMdo002=ValMdo002'
      'ValMdo003=ValMdo003'
      'Valor=Valor'
      'EXcCtaPag=EXcCtaPag'
      'NO_EXcCtaPag=NO_EXcCtaPag'
      'EXcMdoPag=EXcMdoPag')
    DataSet = QrLanctos
    BCDToCurrency = False
    DataSetOptions = []
    Left = 384
    Top = 124
  end
end
