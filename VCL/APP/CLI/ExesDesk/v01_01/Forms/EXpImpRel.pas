unit EXpImpRel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, frxClass, frxDBSet;

type
  TFmEXpImpRel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrEXcIDFunc: TMySQLQuery;
    DsEXcIDFunc: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    QrEXcIDFuncIDCntrCst: TIntegerField;
    QrEXcIDFuncNome: TWideStringField;
    LaPrompt1: TLabel;
    EdIDCntrCst: TdmkEditCB;
    CBIDCntrCst: TdmkDBLookupComboBox;
    EdAnoMes: TdmkEdit;
    SbDecrementaAnpMes: TSpeedButton;
    SbIncrementaAnpMes: TSpeedButton;
    QrSumCtb: TMySQLQuery;
    QrSumCtbEXcGruCta: TIntegerField;
    QrSumCtbNO_EXcGruCta: TWideStringField;
    QrSumCtbEXcMdoPag: TIntegerField;
    QrSumCtbNO_EXcMdoPag: TWideStringField;
    QrSumCtbSigla: TWideStringField;
    QrSumCtbValMdo001: TFloatField;
    QrSumCtbValMdo002: TFloatField;
    QrSumCtbValMdo003: TFloatField;
    QrSumCtbValor: TFloatField;
    frxEXS_RELAT_001_A: TfrxReport;
    frxDsSumCtb: TfrxDBDataset;
    QrSumCta: TMySQLQuery;
    frxDsSumCta: TfrxDBDataset;
    QrSumCtaEXcGruCta: TIntegerField;
    QrSumCtaNO_EXcGruCta: TWideStringField;
    QrSumCtaEXcCtaPag: TIntegerField;
    QrSumCtaNO_EXcCtaPag: TWideStringField;
    QrSumCtaEXcMdoPag: TIntegerField;
    QrSumCtaNO_EXcMdoPag: TWideStringField;
    QrSumCtaSigla: TWideStringField;
    QrSumCtaValMdo001: TFloatField;
    QrSumCtaValMdo002: TFloatField;
    QrSumCtaValMdo003: TFloatField;
    QrSumCtaValor: TFloatField;
    QrItsCtb: TMySQLQuery;
    frxDsItsCtb: TfrxDBDataset;
    QrItsCta: TMySQLQuery;
    frxDsItsCta: TfrxDBDataset;
    QrItsCtbEXcGruCta: TIntegerField;
    QrItsCtbNO_EXcGruCta: TWideStringField;
    QrItsCtbEXcMdoPag: TIntegerField;
    QrItsCtbNO_EXcMdoPag: TWideStringField;
    QrItsCtbSigla: TWideStringField;
    QrItsCtbValMdo001: TFloatField;
    QrItsCtbValMdo002: TFloatField;
    QrItsCtbValMdo003: TFloatField;
    QrItsCtbValor: TFloatField;
    QrItsCtaEXcGruCta: TIntegerField;
    QrItsCtaNO_EXcGruCta: TWideStringField;
    QrItsCtaEXcCtaPag: TIntegerField;
    QrItsCtaNO_EXcCtaPag: TWideStringField;
    QrItsCtaEXcMdoPag: TIntegerField;
    QrItsCtaNO_EXcMdoPag: TWideStringField;
    QrItsCtaSigla: TWideStringField;
    QrItsCtaValMdo001: TFloatField;
    QrItsCtaValMdo002: TFloatField;
    QrItsCtaValMdo003: TFloatField;
    QrItsCtaValor: TFloatField;
    QrLanctos: TMySQLQuery;
    QrLanctosData: TDateField;
    QrLanctosHora: TTimeField;
    QrLanctosPartConta: TFloatField;
    QrLanctosPartOutra: TFloatField;
    QrLanctosEXcGruCta: TIntegerField;
    QrLanctosNO_EXcGruCta: TWideStringField;
    QrLanctosNO_EXcMdoPag: TWideStringField;
    QrLanctosSigla: TWideStringField;
    QrLanctosValMdo001: TFloatField;
    QrLanctosValMdo002: TFloatField;
    QrLanctosValMdo003: TFloatField;
    QrLanctosValor: TFloatField;
    QrItsCtbData: TDateField;
    QrItsCtbHora: TTimeField;
    QrItsCtbPartConta: TFloatField;
    QrItsCtbPartOutra: TFloatField;
    QrItsCtaData: TDateField;
    QrItsCtaHora: TTimeField;
    QrItsCtaPartConta: TFloatField;
    QrItsCtaPartOutra: TFloatField;
    frxDsLanctos: TfrxDBDataset;
    QrLanctosEXcCtaPag: TIntegerField;
    QrLanctosNO_EXcCtaPag: TWideStringField;
    QrLanctosEXcMdoPag: TIntegerField;
    QrItsCtbObserva: TWideStringField;
    QrItsCtaObserva: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbDecrementaAnpMesClick(Sender: TObject);
    procedure SbIncrementaAnpMesClick(Sender: TObject);
    procedure frxEXS_RELAT_001_AGetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    ArrMdoCod: Array of Integer;
    ArrMdoSgl: Array of String;
    //
    procedure ReopenEXcIDFunc(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmEXpImpRel: TFmEXpImpRel;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnExesD_PF;

{$R *.DFM}

procedure TFmEXpImpRel.BtOKClick(Sender: TObject);
var
  IDCntrCst: Integer;
  SQL_IDCntrCst: String;
begin
  SQL_IDCntrCst := '';
  IDCntrCst := EdIDCntrCst.ValueVariant;
  if IDCntrCst <> 0 then
    SQL_IDCntrCst := 'AND its.IDCntrCst=' + Geral.FF0(IDCntrCst);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumCtb, Dmod.MyDB, [
  'SELECT ecp.EXcGruCta, egc.Nome NO_EXcGruCta,  ',
  'its.EXcMdoPag, emp.Nome NO_EXcMdoPag, ',
  'IF(SiglaDst <> "", SiglaDst, SiglaOri) Sigla, ',
  'SUM(IF(EXcMdoPag=' + Geral.FF0(ArrMdoCod[0]) + ', IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ValMdo001, ',
  'SUM(IF(EXcMdoPag=' + Geral.FF0(ArrMdoCod[1]) + ', IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ValMdo002, ',
  'SUM(IF(EXcMdoPag=' + Geral.FF0(ArrMdoCod[2]) + ', IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ValMdo003, ',
  'SUM(IF(SiglaDst <> "", ValorDst, ValorOri)) Valor ',
  'FROM exmdspdevits its ',
  'LEFT JOIN excctapag ecp ON ecp.Codigo=its.EXcCtaPag ',
  'LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta ',
  'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag  ',
  'WHERE its.AnoMes="' + EdAnoMes.Text + '" ',
  SQL_IDCntrCst,
  'GROUP BY egc.Codigo, Sigla ',
  'ORDER BY egc.Ordem, egc.Nome ',
  '']);

  // Contas
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumCta, Dmod.MyDB, [
  'SELECT ecp.EXcGruCta, egc.Nome NO_EXcGruCta,  ',
  'its.EXcCtaPag, ecp.Nome NO_EXcCtaPag,  ',
  'its.EXcMdoPag, emp.Nome NO_EXcMdoPag, ',
  'IF(SiglaDst <> "", SiglaDst, SiglaOri) Sigla, ',
  'SUM(IF(EXcMdoPag=1, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ValMdo001, ',
  'SUM(IF(EXcMdoPag=2, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ValMdo002, ',
  'SUM(IF(EXcMdoPag=3, IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ValMdo003, ',
  'SUM(IF(SiglaDst <> "", ValorDst, ValorOri)) Valor ',
  'FROM exmdspdevits its ',
  'LEFT JOIN excctapag ecp ON ecp.Codigo=its.EXcCtaPag ',
  'LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta ',
  'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag  ',
  'WHERE its.AnoMes="' + EdAnoMes.Text + '" ',
  SQL_IDCntrCst,
  'GROUP BY egc.Codigo, ecp.Codigo, Sigla ',
  'ORDER BY egc.Ordem, NO_EXcGruCta, ecp.Ordem, NO_EXcCtaPag ',
  '']);
  //
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsCtb, Dmod.MyDB, [
  'SELECT lct.Observa, ',
  'its.Data, its.Hora, PartConta, PartOutra, ',
  'ecp.EXcGruCta, egc.Nome NO_EXcGruCta,   ',
  'its.EXcMdoPag, emp.Nome NO_EXcMdoPag,  ',
  'IF(its.SiglaDst <> "", its.SiglaDst, its.SiglaOri) SIGLA,  ',
  '(IF(its.EXcMdoPag=' + Geral.FF0(ArrMdoCod[0]) + ', IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri), 0)) ValMdo001,  ',
  '(IF(its.EXcMdoPag=' + Geral.FF0(ArrMdoCod[1]) + ', IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri), 0)) ValMdo002,  ',
  '(IF(its.EXcMdoPag=' + Geral.FF0(ArrMdoCod[2]) + ', IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri), 0)) ValMdo003,  ',
  '(IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri)) VALOR ',
  'FROM exmdspdevits its ',
  'LEFT JOIN exmdspdevlct lct  ',
  '  ON lct.CodInMob=its.CodInMob ',
  '  AND lct.IDCntrCst=its.IDCntrCst ',
  '  AND lct.AnoMes=its.AnoMes ',
  'LEFT JOIN excctapag ecp ON ecp.Codigo=its.EXcCtaPag  ',
  'LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta  ',
  'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag   ',
  'WHERE its.AnoMes="' + EdAnoMes.Text + '" ',
  SQL_IDCntrCst,
  'ORDER BY egc.Ordem, egc.Nome, its.Data, its.Hora ',
  ' ',
  '']);
  (*)
  'SELECT its.Data, its.Hora, PartConta, PartOutra, ',
  'ecp.EXcGruCta, egc.Nome NO_EXcGruCta,  ',
  'its.EXcMdoPag, emp.Nome NO_EXcMdoPag, ',
  'IF(SiglaDst <> "", SiglaDst, SiglaOri) Sigla, ',
  '(IF(EXcMdoPag=' + Geral.FF0(ArrMdoCod[0]) + ', IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ValMdo001, ',
  '(IF(EXcMdoPag=' + Geral.FF0(ArrMdoCod[1]) + ', IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ValMdo002, ',
  '(IF(EXcMdoPag=' + Geral.FF0(ArrMdoCod[2]) + ', IF(SiglaDst <> "", ValorDst, ValorOri), 0)) ValMdo003, ',
  '(IF(SiglaDst <> "", ValorDst, ValorOri)) Valor ',
  'FROM exmdspdevits its ',
  'LEFT JOIN excctapag ecp ON ecp.Codigo=its.EXcCtaPag ',
  'LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta ',
  'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag  ',
  'WHERE its.AnoMes="' + EdAnoMes.Text + '" ',
  SQL_IDCntrCst,
  'ORDER BY egc.Ordem, egc.Nome, its.Data, its.Hora ',
  '']);
*)
  Geral.MB_Teste(QrItsCtb.SQL.Text);

  // Contas
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsCta, Dmod.MyDB, [
  'SELECT lct.Observa, ',
  'its.Data, its.Hora, its.PartConta, its.PartOutra, ',
  'ecp.EXcGruCta, egc.Nome NO_EXcGruCta,  ',
  'its.EXcCtaPag, ecp.Nome NO_EXcCtaPag,  ',
  'its.EXcMdoPag, emp.Nome NO_EXcMdoPag, ',
  'IF(its.SiglaDst <> "", its.SiglaDst, its.SiglaOri) Sigla, ',
  '(IF(its.EXcMdoPag=1, IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri), 0)) ValMdo001, ',
  '(IF(its.EXcMdoPag=2, IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri), 0)) ValMdo002, ',
  '(IF(its.EXcMdoPag=3, IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri), 0)) ValMdo003, ',
  '(IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri)) Valor ',
  'FROM exmdspdevits its ',
  'LEFT JOIN exmdspdevlct lct  ',
  '  ON lct.CodInMob=its.CodInMob ',
  '  AND lct.IDCntrCst=its.IDCntrCst ',
  '  AND lct.AnoMes=its.AnoMes ',
  'LEFT JOIN excctapag ecp ON ecp.Codigo=its.EXcCtaPag ',
  'LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta ',
  'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag  ',
  'WHERE its.AnoMes="' + EdAnoMes.Text + '" ',
  SQL_IDCntrCst,
  'ORDER BY egc.Ordem, NO_EXcGruCta, its.Data, its.Hora ',
  '']);
  //Geral.MB_Teste(QrItsCta.SQL.Text);
  //

  UnDmkDAC_PF.AbreMySQLQuery0(QrLanctos, Dmod.MyDB, [
  'SELECT its.Data, its.Hora, PartConta, PartOutra, ',
  'ecp.EXcGruCta, egc.Nome NO_EXcGruCta,  ',
  'its.EXcCtaPag, ecp.Nome NO_EXcCtaPag,  ',
  'its.EXcMdoPag, emp.Nome NO_EXcMdoPag, ',
  'IF(its.SiglaDst <> "", its.SiglaDst, its.SiglaOri) Sigla, ',
  '(IF(its.EXcMdoPag=1, IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri), 0)) ValMdo001, ',
  '(IF(its.EXcMdoPag=2, IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri), 0)) ValMdo002, ',
  '(IF(its.EXcMdoPag=3, IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri), 0)) ValMdo003, ',
  '(IF(its.SiglaDst <> "", its.ValorDst, its.ValorOri)) Valor ',
  'FROM exmdspdevits its ',
  'LEFT JOIN excctapag ecp ON ecp.Codigo=its.EXcCtaPag ',
  'LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta ',
  'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag  ',
  'WHERE its.AnoMes="' + EdAnoMes.Text + '" ',
  SQL_IDCntrCst,
  '',
  'ORDER BY its.Data, its.Hora, egc.Ordem, egc.Nome ',
  '']);
  //Geral.MB_Teste(QrLanctos.SQL.Text);

   MyObjects.frxDefineDataSets(frxEXS_RELAT_001_A, [
    frxDsSumCtb,
    frxDsSumCta,
    frxDsItsCtb,
    frxDsItsCta,
    frxDsLanctos
  ]);
  //
  MyObjects.frxMostra(frxEXS_RELAT_001_A, 'Relatório de Despesas');
end;

procedure TFmEXpImpRel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEXpImpRel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEXpImpRel.FormCreate(Sender: TObject);
var
  Ano, Mes, Dia, Itens: Word;
  AnoMes: String;
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenEXcIDFunc(0);
  //
  DecodeDate(Now()-21, Ano, Mes, Dia);
  AnoMes := ExesD_PF.FormataAnoMes_Num(Ano, Mes);
  EdAnoMes.Text := AnoMes;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Codigo, Sigla',
  'FROM excmdopag',
  //'WHERE Codigo > 1000',
  'ORDER BY Ordem, Codigo',
  '']);
  SetLength(ArrMdoCod, Dmod.QrAux.RecordCount);
  SetLength(ArrMdoSgl, Dmod.QrAux.RecordCount);
  Dmod.QrAux.First;
  while not Dmod.QrAux.Eof do
  begin
    ArrMdoCod[Dmod.QrAux.RecNo-1] := Dmod.QrAux.FieldByName('Codigo').AsInteger;
    ArrMdoSgl[Dmod.QrAux.RecNo-1] := Dmod.QrAux.FieldByName('Sigla').AsString;
    //
    Dmod.QrAux.Next;
  end;
  Itens := Length(ArrMdoCod);
  while Itens < 3 do
  begin
    Itens := Itens + 1;
    SetLength(ArrMdoCod, Itens);
    SetLength(ArrMdoSgl, Itens);
    ArrMdoCod[Itens - 1] := 0;
    ArrMdoSgl[Itens - 1] := '???';
  end;
end;

procedure TFmEXpImpRel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEXpImpRel.frxEXS_RELAT_001_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_NO_EMPRESA' then
    Value := Dmod.QrMasterEm.Value
  else
  if VarName = 'VARF_PERIODO' then
    Value := EdAnoMes.Text
  else
  if VarName = 'VARF_CAMPO_A' then
    Value := ArrMdoSgl[0]
  else
  if VarName = 'VARF_CAMPO_B' then
    Value := ArrMdoSgl[1]
  else
  if VarName = 'VARF_CAMPO_C' then
    Value := ArrMdoSgl[2]
  else
  if VarName = 'VARF_CENTRO_CUSTO' then
  begin
    Value := 'Qualquer';
    if EdIDCntrCst.ValueVariant > 0 then
      Value := Geral.FF0(EdIDCntrCst.ValueVariant) + ' - ' + CBIDCntrCst.Text
  end
  else
  //
end;

procedure TFmEXpImpRel.ReopenEXcIDFunc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXcIDFunc, Dmod.MyDB, [
  'SELECT IDCntrCst, Nome  ',
  'FROM excidfunc ',
  'WHERE IDCntrCst <> 0 ',
  'ORDER BY Nome ',  '']);
end;

procedure TFmEXpImpRel.SbDecrementaAnpMesClick(Sender: TObject);
begin
  EdAnoMes.Text := ExesD_PF.IncrementaAnoMes(EdAnoMes.Text, -1);
end;

procedure TFmEXpImpRel.SbIncrementaAnpMesClick(Sender: TObject);
begin
  EdAnoMes.Text := ExesD_PF.IncrementaAnoMes(EdAnoMes.Text, +1);
end;

end.
