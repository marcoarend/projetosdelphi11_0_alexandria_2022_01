unit LoclzL10nIds;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmLoclzL10nIds = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    EdLocID: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenLoclzL10nIds(LocID: String);
  public
    { Public declarations }
    FCodigo: Integer;
    FQrIts: TmySQLQuery;
    FOldLocID: String;
  end;

  var
  FmLoclzL10nIds: TFmLoclzL10nIds;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmLoclzL10nIds.BtOKClick(Sender: TObject);
var
  LocID: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  LocID          := EdLocID.ValueVariant;
  //
  if MyObjects.FIC(LocID = '', EdLocID, 'Informe a L�ngua - Pa�s!') then
    Exit;
  //ou > ? := UMyMod.BPGS1I32('loclzl10nids', 'LocID', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if UMyMod.SQLInsUpdAlterIdx(Dmod.QrUpd, SQLType, 'loclzl10nids', False, [
  ], ['Codigo', 'LocID'], ['Codigo', 'LocID'], [], [Codigo, LocID], [Codigo, FOldLocID], True) then
  begin
    ReopenLoclzL10nIds(LocID);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdLocID.ValueVariant     := '';
      EdLocID.SetFocus;
    end else Close;
  end;
end;

procedure TFmLoclzL10nIds.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLoclzL10nIds.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLoclzL10nIds.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmLoclzL10nIds.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLoclzL10nIds.ReopenLoclzL10nIds(LocID: String);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if LocID <> EmptyStr then
      FQrIts.Locate('LocID', LocID, []);
  end;
end;

end.
