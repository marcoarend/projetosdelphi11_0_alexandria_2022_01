unit LoclzL10nEd3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmLoclzL10nEd3 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label7: TLabel;
    EdTxt1ptBR: TdmkEdit;
    Panel5: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    EdID1: TdmkEdit;
    EdID1Descri: TdmkEdit;
    Label4: TLabel;
    EdID2Descri: TdmkEdit;
    EdID2: TdmkEdit;
    Label5: TLabel;
    EdTxt1enUS: TdmkEdit;
    Label1: TLabel;
    EdTxtTradu1: TdmkEdit;
    Label6: TLabel;
    Label8: TLabel;
    EdSigla: TdmkEdit;
    GroupBox1: TGroupBox;
    Panel9: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    EdDocHeader: TdmkEdit;
    EdItemText: TdmkEdit;
    EdReference: TdmkEdit;
    EdDescrCondiPg: TdmkEdit;
    RGRegrTipSeq: TdmkRadioGroup;
    EdRegrQtdDia: TdmkEdit;
    RGRegrDiaSem: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGRegrTipSeqClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrIts(ID1, ID2: Integer);
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrIts: TmySQLQuery;
    FIdxFld1, FIdxFld2, FFldTradu1, FTableName: String;
    FIdxVal1, FIdxVal2: Integer;
  end;

  var
  FmLoclzL10nEd3: TFmLoclzL10nEd3;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmLoclzL10nEd3.BtOKClick(Sender: TObject);
var
  TxtTradu1: String;
  ID1, ID2: Integer;
  SQLType: TSQLType;
var
  Sigla, DocHeader, ItemText, Reference, DescrCondiPg: String;
  RegrQtdDia, RegrTipSeq, RegrDiaSem: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  ID1            := EdID1.ValueVariant;
  ID2            := EdID2.ValueVariant;
  TxtTradu1      := EdTxtTradu1.Text;
  //
  Sigla          := EdSigla.ValueVariant;
  //Ordem          := EdOrdem.ValueVariant;
  DocHeader      := EdDocHeader.ValueVariant;
  ItemText       := EdItemText.ValueVariant;
  Reference      := EdReference.ValueVariant;
  RegrQtdDia     := EdRegrQtdDia.ValueVariant;
  RegrTipSeq     := RGRegrTipSeq.ItemIndex;
  RegrDiaSem     := RGRegrDiaSem.ItemIndex;

  DescrCondiPg   := EdDescrCondiPg.ValueVariant;

  //
  //ou > ? := UMyMod.BPGS1I32('loclzl10ncta', 'EXcCtaPag', 'LoclzL10nCab', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTableName, False, [
  FFldTradu1, 'Sigla', 'DocHeader',
  'ItemText', 'Reference', 'DescrCondiPg',
  'RegrQtdDia', 'RegrTipSeq', 'RegrDiaSem'], [
  FIdxFld1, FIdxFld2], [
  TxtTradu1, Sigla, DocHeader,
  ItemText, Reference, DescrCondiPg,
  RegrQtdDia, RegrTipSeq, RegrDiaSem], [
  ID1, ID2], True) then
  begin
    ReopenQrIts(ID1, ID2);
(*
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else*) Close;
  end;
end;

procedure TFmLoclzL10nEd3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLoclzL10nEd3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLoclzL10nEd3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //UnDmkDAC_PF.AbreQuery(Qr_Sel_, Dmod.MyDB?);
end;

procedure TFmLoclzL10nEd3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLoclzL10nEd3.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
end;

procedure TFmLoclzL10nEd3.ReopenQrIts(ID1, ID2: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    try
      FQrIts.Locate(FIdxFld1 + ';' + FIdxFld2, VarArrayOf([ID1, ID2]), []);
    except
      //
    end;
  end;
end;

procedure TFmLoclzL10nEd3.RGRegrTipSeqClick(Sender: TObject);
begin
  RGRegrDiaSem.Enabled := RGRegrTipSeq.ItemIndex = 3;
end;

end.
