unit ExportaSAP1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO,
  ComObj;  //Excel

type
  TFmExportaSAP1 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrAnal: TMySQLQuery;
    Panel2: TPanel;
    Panel3: TPanel;
    QrSint: TMySQLQuery;
    Panel5: TPanel;
    SbDecrAnoMes: TSpeedButton;
    SbIncAnoMes: TSpeedButton;
    EdAnoMes: TdmkEdit;
    CkAnoMes: TCheckBox;
    DBEXcIdFuns: TDBGrid;
    QrEXcIDFunc: TMySQLQuery;
    QrEXcIDFuncCodigo: TIntegerField;
    QrEXcIDFuncNome: TWideStringField;
    DsEXcIDFunc: TDataSource;
    QrEXcIDFuncSIT: TWideStringField;
    EdCompany: TdmkEdit;
    Label1: TLabel;
    QrAnalCompany: TWideStringField;
    QrAnalPostDate: TDateTimeField;
    QrAnalDocDate: TDateTimeField;
    QrAnalDocType: TWideStringField;
    QrAnalReference: TWideStringField;
    QrAnalNO_Fun: TWideStringField;
    QrAnalDocHeader: TWideStringField;
    QrAnalGLAccount: TIntegerField;
    QrAnalNO_GLAccount: TWideStringField;
    QrAnalItemText: TWideStringField;
    QrAnalVendor: TWideStringField;
    QrAnalCenCustOth: TWideStringField;
    QrAnalValorOri: TFloatField;
    QrAnalSiglaOri: TWideStringField;
    QrSintCompany: TWideStringField;
    QrSintNO_Fun: TWideStringField;
    QrSintPostDate: TDateTimeField;
    QrSintDocDate: TDateTimeField;
    QrSintDocType: TWideStringField;
    QrSintReference: TWideStringField;
    QrSintDocHeader: TWideStringField;
    QrSintItemText: TWideStringField;
    QrSintVendor: TWideStringField;
    QrSintCenCustOth: TWideStringField;
    QrSintValorOri: TFloatField;
    QrSintSiglaOri: TWideStringField;
    SGDocs: TStringGrid;
    BtGera: TBitBtn;
    EdBRL_USD: TdmkEdit;
    Label2: TLabel;
    BtSalva: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    SGExporta: TStringGrid;
    QrSintEXcMdoPag: TIntegerField;
    QrSintRegrQtdDia: TIntegerField;
    QrSintRegrTipSeq: TIntegerField;
    QrSintRegrDiaSem: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdAnoMesChange(Sender: TObject);
    procedure EdCompanyChange(Sender: TObject);
    procedure QrSintAfterScroll(DataSet: TDataSet);
    procedure QrSintBeforeClose(DataSet: TDataSet);
    procedure BtGeraClick(Sender: TObject);
    procedure EdAnoMesRedefinido(Sender: TObject);
    procedure BtSalvaClick(Sender: TObject);
  private
    { Private declarations }
    FAnoInt, FMesInt: Integer;
    FAnoStr, FMesStr, Fmmm: String;
    //
    function  ReopenDespesasSint(Corda: String): Boolean;
    procedure ReopenDespesasAnal();
    procedure ReopenEXcIdFunc();
    procedure ConfiguraGrade();
    function  FDP(Data: TDateTime): String;
    function  FFA(Valor: Double): String;
    function  SubsTags(Texto: String): String;
    function  ObtemBlindDate(): String;
  public
    { Public declarations }
  end;

  var
  FmExportaSAP1: TFmExportaSAP1;

implementation

uses UnMyObjects, Module, DmkDAC_PF, dmkExcel, UMySQLModule;

{$R *.DFM}


const
  CO_SeparadorAmount = ',';
  //
  COL_NUMLINH = 00;
  COL_Company = 01;
  COL_PostDat = 02;
  COL_DocDate = 03;
  COL_DocType = 04;
  COL_Referen = 05;
  COL_DocHead = 06;
  COL_G_L_Acc = 07;
  COL_ItemTex = 08;
  COL_AcctTyp = 09;
  COL_Custome = 10;
  COL_Vendor  = 11;
  COL_ProfitC = 12;
  COL_Transac = 13;
  COL_CostCen = 14;
  COL_OrderNu = 15;
  COL_Ammount = 16;
  COL_BlineDa = 17;
  //COL_Currenc = 18;
  //COL_Exchang = 19;

procedure TFmExportaSAP1.BtOKClick(Sender: TObject);
var
  Corda: String;
begin
  if DBEXcIdFuns.SelectedRows.Count > 0 then
  begin
    //Corda := MyObjects.CordaDeQuery(QrEXcIdFunc, 'Codigo');
    Corda := MyObjects.CordaDeDBGridSelectedRows(DBEXcIdFuns,
      QrEXcIdFunc, 'Codigo', True);
    //
    ReopenDespesasSint(Corda);
  end else
    Geral.MB_Aviso('Nenhum colaborador foi selecionado!');
end;

procedure TFmExportaSAP1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmExportaSAP1.BtSalvaClick(Sender: TObject);
const
  sNome = 'ZFI_POSTDOC';
var
  coluna, linha: integer;
  excel: variant;
  valor: string;
  //
  Col, Row: Integer;
  Dir, FileName: string;
begin
  Dir := 'C:\TFL\DespesasViagem\';
  FileName := Dir + sNome + '_' + FormatDateTime('yyyymmdd_hhnnss', Now()) + '.xlsx';
  try
    ForceDirectories(Dir);
  except
    Geral.MB_Erro('N�o foi poss�vel criaro diret�rio: ' + sLineBreak + Dir);
  end;
  UnDmkExcel.StringGridToExcelSheet(SGExporta, sNome, FileName, True);
  Geral.MB_Info('Arquivo salvo: ' + sLineBreak + FileName);

  ///////////////////////////////////////////////////////////////////
  Exit;
  ///////////////////////////////////////////////////////////////////

  try
    excel:=CreateOleObject('Excel.Application');
    excel.Workbooks.add(1);
  except
    Geral.MB_Erro('Vers�o do MS-Excel Incompat�vel');
    Exit;
  end;
  try
  for Col := 0 to SGExporta.RowCount - 1 do
  begin
    for Row := 0 to SGExporta.ColCount - 1 do
    begin
      excel.cells [Row + 2, Col + 2] := SGExporta.Cells[Col, Row];
    end;
  end;
  excel.columns.AutoFit;
  excel.visible:=true;
  except
    Geral.MB_Erro('Aconteceu um erro desconhecido durante o preenchimento da planilha Excel');
  end;
end;

procedure TFmExportaSAP1.BtGeraClick(Sender: TObject);
var
  Linha, Coluna: Integer;
  BRL_USD: Double;
begin
(*
  BRL_USD := EdBRL_USD.ValueVariant;
  if BRL_USD < 0.0000000001 then
  begin
    Geral.MB_Avisa('Informe o valor da taxa de c�mbio!');
    Exit;
  end;
  //
*)
  MyObjects.LimpaGrade(SGDocs, 1, 1, True);
  Linha := 1;
  QrSint.First;
  while not QrSint.Eof do
  begin
    if QrSint.RecNo > 1 then
    begin
      Linha := Linha + 1;
      SGDocs.RowCount := Linha + 1;
    end;
    //
    SGDocs.Cells[COL_NUMLINH, Linha] := Geral.FF0(Linha); //'Itm';
    SGDocs.Cells[COL_Company, Linha] := QrSintCompany.Value; //'Company (4)';
    SGDocs.Cells[COL_PostDat, Linha] := FDP(QrSintPostDate.Value); //'Post date';
    SGDocs.Cells[COL_DocDate, Linha] := FDP(QrSintDocDate.Value); //'Doc date';
    SGDocs.Cells[COL_DocType, Linha] := QrSintDocType.Value; //'Doc.type (2)';
    SGDocs.Cells[COL_Referen, Linha] := Copy(SubsTags(QrSintReference.Value), 1, 16); //'Reference (16)';
    SGDocs.Cells[COL_DocHead, Linha] := Copy(SubsTags(QrSintDocHeader.Value), 1, 25); //'Doc header text (25)';
    SGDocs.Cells[COL_G_L_Acc, Linha] := ''; //QrSintGLAccount.Value; //'G/L account (10)';
    SGDocs.Cells[COL_ItemTex, Linha] := Copy(SubsTags(QrSintItemText.Value), 1, 50); //'Item Text (50)';
    SGDocs.Cells[COL_AcctTyp, Linha] := ''; //'Acct type (1)';
    SGDocs.Cells[COL_Custome, Linha] := ''; //'Customer (10)';
    SGDocs.Cells[COL_Vendor , Linha] := QrSintVendor.Value; //'Vendor (10)';
    SGDocs.Cells[COL_ProfitC, Linha] := ''; //'Profit center (10)';
    SGDocs.Cells[COL_Transac, Linha] := ''; //'Transaction type (3)';
    SGDocs.Cells[COL_CostCen, Linha] := ''; //QrSintCenCustOth.Value; //'Cost center (10)';
    SGDocs.Cells[COL_OrderNu, Linha] := ''; //'Order number (12)';
    SGDocs.Cells[COL_Ammount, Linha] := FFA(-1 * QrSintValorOri.Value); //'Ammount';
    SGDocs.Cells[COL_BlineDa, Linha] := ObtemBlindDate(); //'Bline date';
    //SGDocs.Cells[COL_Currenc, Linha] := QrSintSiglaOri.Value; //'Currency';
    //SGDocs.Cells[COL_Exchang, Linha] := QrSintSiglaOri.Value; //'Currency';
    //
    while not QrAnal.Eof do
    begin
      Linha := Linha + 1;
      SGDocs.RowCount := Linha + 1;
      //
      SGDocs.Cells[COL_NUMLINH, Linha] := Geral.FF0(Linha); //'Itm';
      SGDocs.Cells[COL_Company, Linha] := QrAnalCompany.Value; //'Company (4)';
      SGDocs.Cells[COL_PostDat, Linha] := FDP(QrAnalPostDate.Value); //'Post date';
      SGDocs.Cells[COL_DocDate, Linha] := FDP(QrAnalDocDate.Value); //'Doc date';
      SGDocs.Cells[COL_DocType, Linha] := QrAnalDocType.Value; //'Doc.type (2)';
      SGDocs.Cells[COL_Referen, Linha] := Copy(SubsTags(QrAnalReference.Value), 1, 16); //'Reference (16)';
      SGDocs.Cells[COL_DocHead, Linha] := SubsTags(QrAnalDocHeader.Value); //'Doc header text (25)';
      SGDocs.Cells[COL_G_L_Acc, Linha] := Copy(Geral.FF0(QrAnalGLAccount.Value), 1, 25); //'G/L account (10)';
      SGDocs.Cells[COL_ItemTex, Linha] := Copy(SubsTags(QrAnalItemText.Value), 1, 50); //'Item Text (50)';
      SGDocs.Cells[COL_AcctTyp, Linha] := ''; //'Acct type (1)';
      SGDocs.Cells[COL_Custome, Linha] := ''; //'Customer (10)';
      SGDocs.Cells[COL_Vendor , Linha] := ''; //QrAnalVendor.Value; //'Vendor (10)';
      SGDocs.Cells[COL_ProfitC, Linha] := ''; //'Profit center (10)';
      SGDocs.Cells[COL_Transac, Linha] := ''; //'Transaction type (3)';
      SGDocs.Cells[COL_CostCen, Linha] := QrAnalCenCustOth.Value; //'Cost center (10)';
      SGDocs.Cells[COL_OrderNu, Linha] := ''; //'Order number (12)';
      SGDocs.Cells[COL_Ammount, Linha] := FFA(QrAnalValorOri.Value); //'Ammount';
      SGDocs.Cells[COL_BlineDa, Linha] := ''; //'Bline date';
      //SGDocs.Cells[COL_Currenc, Linha] := QrAnalSiglaOri.Value; //'Currency';
      //SGDocs.Cells[COL_Exchang, Linha] := QrAnalSiglaOri.Value; //'Currency';
      //
      //
      QrAnal.Next;
    end;
    //
    QrSint.Next;
  end;
  //
  SGExporta.RowCount := SGDocs.RowCount;
  for Linha := 0 to  SGDocs.RowCount - 1 do
  begin
    for Coluna := 0 to  SGDocs.ColCount - 2 do
    begin
      SGExporta.Cells[Coluna, Linha] := SGDocs.Cells[Coluna + 1, Linha];
    end;
  end;
end;

procedure TFmExportaSAP1.ConfiguraGrade();
begin
  SGDocs.Cells[COL_NUMLINH, 00] := 'Itm';
  SGDocs.Cells[COL_Company, 00] := 'Company (4)';
  SGDocs.Cells[COL_PostDat, 00] := 'Post date';
  SGDocs.Cells[COL_DocDate, 00] := 'Doc date';
  SGDocs.Cells[COL_DocType, 00] := 'Doc.type (2)';
  SGDocs.Cells[COL_Referen, 00] := 'Reference (16)';
  SGDocs.Cells[COL_DocHead, 00] := 'Doc header text (25)';
  SGDocs.Cells[COL_G_L_Acc, 00] := 'G/L account (10)';
  SGDocs.Cells[COL_ItemTex, 00] := 'Item Text (50)';
  SGDocs.Cells[COL_AcctTyp, 00] := 'Acct type (1)';
  SGDocs.Cells[COL_Custome, 00] := 'Customer (10)';
  SGDocs.Cells[COL_Vendor , 00] := 'Vendor (10)';
  SGDocs.Cells[COL_ProfitC, 00] := 'Profit center (10)';
  SGDocs.Cells[COL_Transac, 00] := 'Transaction type (3)';
  SGDocs.Cells[COL_CostCen, 00] := 'Cost center (10)';
  SGDocs.Cells[COL_OrderNu, 00] := 'Order number (12)';
  SGDocs.Cells[COL_Ammount, 00] := 'Ammount';
  SGDocs.Cells[COL_BlineDa, 00] := 'Bline date';
  //SGDocs.Cells[COL_Currenc, 00] := 'Currency';
  //
  SGExporta.Cells[COL_Company - 1, 00] := 'Company (4)';
  SGExporta.Cells[COL_PostDat - 1, 00] := 'Post date';
  SGExporta.Cells[COL_DocDate - 1, 00] := 'Doc date';
  SGExporta.Cells[COL_DocType - 1, 00] := 'Doc.type (2)';
  SGExporta.Cells[COL_Referen - 1, 00] := 'Reference (16)';
  SGExporta.Cells[COL_DocHead - 1, 00] := 'Doc header text (25)';
  SGExporta.Cells[COL_G_L_Acc - 1, 00] := 'G/L account (10)';
  SGExporta.Cells[COL_ItemTex - 1, 00] := 'Item Text (50)';
  SGExporta.Cells[COL_AcctTyp - 1, 00] := 'Acct type (1)';
  SGExporta.Cells[COL_Custome - 1, 00] := 'Customer (10)';
  SGExporta.Cells[COL_Vendor  - 1, 00] := 'Vendor (10)';
  SGExporta.Cells[COL_ProfitC - 1, 00] := 'Profit center (10)';
  SGExporta.Cells[COL_Transac - 1, 00] := 'Transaction type (3)';
  SGExporta.Cells[COL_CostCen - 1, 00] := 'Cost center (10)';
  SGExporta.Cells[COL_OrderNu - 1, 00] := 'Order number (12)';
  SGExporta.Cells[COL_Ammount - 1, 00] := 'Ammount';
  SGExporta.Cells[COL_BlineDa - 1, 00] := 'Bline date';
  //SGExporta.Cells[COL_Currenc, 00] := 'Currency';
  //
  SGDocs.ColWidths[COL_NUMLINH] := 24;
  SGDocs.ColWidths[COL_Company] := 79;
  SGDocs.ColWidths[COL_PostDat] := 71;
  SGDocs.ColWidths[COL_DocDate] := 71;
  SGDocs.ColWidths[COL_DocType] := 76;
  SGDocs.ColWidths[COL_Referen] := 90;
  SGDocs.ColWidths[COL_DocHead] := 101;
  SGDocs.ColWidths[COL_G_L_Acc] := 102;
  SGDocs.ColWidths[COL_ItemTex] := 186; //86;
  SGDocs.ColWidths[COL_AcctTyp] := 78;
  SGDocs.ColWidths[COL_Custome] := 87;
  SGDocs.ColWidths[COL_Vendor ] := 73;
  SGDocs.ColWidths[COL_ProfitC] := 72;
  SGDocs.ColWidths[COL_Transac] := 76;
  SGDocs.ColWidths[COL_CostCen] := 98;
  SGDocs.ColWidths[COL_OrderNu] := 112;
  SGDocs.ColWidths[COL_Ammount] := 69;
  SGDocs.ColWidths[COL_BlineDa] := 71;
  //SGDocs.ColWidths[COL_Currenc] := 40;
  //
  SGExporta.ColWidths[COL_Company - 1] := 79;
  SGExporta.ColWidths[COL_PostDat - 1] := 71;
  SGExporta.ColWidths[COL_DocDate - 1] := 71;
  SGExporta.ColWidths[COL_DocType - 1] := 76;
  SGExporta.ColWidths[COL_Referen - 1] := 90;
  SGExporta.ColWidths[COL_DocHead - 1] := 101;
  SGExporta.ColWidths[COL_G_L_Acc - 1] := 102;
  SGExporta.ColWidths[COL_ItemTex - 1] := 186; //86;
  SGExporta.ColWidths[COL_AcctTyp - 1] := 78;
  SGExporta.ColWidths[COL_Custome - 1] := 87;
  SGExporta.ColWidths[COL_Vendor  - 1] := 73;
  SGExporta.ColWidths[COL_ProfitC - 1] := 72;
  SGExporta.ColWidths[COL_Transac - 1] := 76;
  SGExporta.ColWidths[COL_CostCen - 1] := 98;
  SGExporta.ColWidths[COL_OrderNu - 1] := 112;
  SGExporta.ColWidths[COL_Ammount - 1] := 69;
  SGExporta.ColWidths[COL_BlineDa - 1] := 71;
  //SGExporta.ColWidths[COL_Currenc] := 40;
  //
end;

procedure TFmExportaSAP1.EdAnoMesChange(Sender: TObject);
begin
  //
  ReopenEXcIdFunc();
end;

procedure TFmExportaSAP1.EdAnoMesRedefinido(Sender: TObject);
var
  Ano_Mes: String;
begin
  Ano_Mes := EdAnoMes.Text;
  //
  FAnoStr := Copy(Ano_Mes, 1, 4);
  FMesStr := Copy(Ano_Mes, 6);
  //
  FAnoInt := Geral.IMV(FAnoStr);
  FMesInt := Geral.IMV(FMesStr);
  //
  Fmmm := FormatDateTime('mmm', EncodeDate(FAnoInt, FMesInt, 1));
  //
end;

procedure TFmExportaSAP1.EdCompanyChange(Sender: TObject);
begin
  ReopenEXcIdFunc();
end;

function TFmExportaSAP1.FDP(Data: TDateTime): String;
(*
var
  iAno, iMes, iDia: Word;
  sAno, sMes, sDia: String;
*)
begin
(*
  DecodeDate(Data, iAno, iMes, iDia);
  sDia := Geral.FF0(iDia);
  sMes := Geral.FF0(iMes);
  sAno := Geral.FF0(iAno);
  //
  while Length(sDia) < 2 do
    sDia := '0' + sDia;
  while Length(sMes) < 2 do
    sMes := '0' + sMes;
  while Length(sAno) < 4 do
    sAno := '0' + sAno;
  //Result := Geral.FF0(Ano) + '.' + Geral.FF0(Mes) + '.' + Geral.FF0(Dia);
  Result := sDia + '.' + sMes + '.' + sAno;
*)
  Result := FormatDateTime('dd.mm.yyyy', Data);
end;

function TFmExportaSAP1.FFA(Valor: Double): String;
var
  //I, F: String;
  T: Integer;
begin
  if CO_SeparadorAmount = ',' then
  begin
    Result := Geral.FFT(Valor, 2, siPositivo);
    Result := StringReplace(Result, '.', '', []);
  end else
    Result := Geral.FFT_Dot(Valor, 2, siPositivo);
  //
  T := Length(Result);
  while Result[T] in (['0', CO_SeparadorAmount]) do
    Result := Copy(Result, 1, T - 1);
  //
  if Valor < 0 then
    Result := Result + '-';
end;

procedure TFmExportaSAP1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmExportaSAP1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ConfiguraGrade();
  //
  EdAnoMes.FormatType := dmktf_AAAA_MM;
  EdAnoMes.ValueVariant := FormatDateTime('YYYY-MM', Date - 15);
  //
end;

procedure TFmExportaSAP1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmExportaSAP1.ObtemBlindDate: String;
var
  Dia: Integer;
  Data: TDateTime;
  Res: TDateTime;
  DiaSemTxt: String;
  Dom, Seg, Ter, Qua, Qui, Sex, Sab, Feriado: Word;
begin
  Dom := 1;
  Seg := 0;
  Ter := 0;
  Qua := 0;
  Qui := 0;
  Sex := 0;
  Sab := 2;
  Feriado := 1;

  case QrSintRegrTipSeq.Value of
    //Dia corrido
    0:
    begin
      Result := '';
      Exit;
    end;
    //Dia �til
    1:
    begin
      Dia := QrSintRegrQtdDia.Value;
      Data := EncodeDate(FAnoInt, FMesInt, Dia);
      Data := IncMonth(Data, 1);
    end;
    2:
    begin
      Dia := QrSintRegrQtdDia.Value;
      Data := EncodeDate(FAnoInt, FMesInt, Dia);
      Data := IncMonth(Data, 1);
      UMyMod.ProximoDiaUtil(Data, Dom, Seg, Ter, Qua, Qui, Sex, Sab, Feriado, Res, DiaSemTxt);
      Data := Res;
    end;
    //Semana corrida
    3:
    begin
      Dia := 1;
      Data := EncodeDate(FAnoInt, FMesInt, Dia);
      Data := IncMonth(Data, 1);
      while DayOfWeek(Data) <> QrSintRegrDiaSem.Value do
        Data := Data + 1;
      Data := Data + ((QrSintRegrQtdDia.Value - 1) * 7);
    end;
  end;
  //
  Result := FDP(Data);
end;

procedure TFmExportaSAP1.QrSintAfterScroll(DataSet: TDataSet);
begin
  ReopenDespesasAnal();
end;

procedure TFmExportaSAP1.QrSintBeforeClose(DataSet: TDataSet);
begin
  QrAnal.Close;
end;

procedure TFmExportaSAP1.ReopenDespesasAnal();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAnal, Dmod.MyDB, [
  'SELECT eif.Company, SYSDATE() PostDate, ',
  'SYSDATE() DocDate,  "KI" DocType, emp.Reference, ',
  'eif.Nome NO_Fun, emp.DocHeader, egc.Codigo GLAccount, ',
  'egc.Nome NO_GLAccount, ',
  'emp.ItemText, eif.CodFornOth Vendor, ',
  'eif.CenCustOth, SUM(its.ValorOri) ValorOri, ',
  'its.SiglaOri ',
  ' ',
  'FROM exmdspdevits its ',
  'LEFT JOIN exmdspdevlct lct ',
  '  ON lct.CodInMob=its.CodInMob ',
  '  AND lct.IDCntrCst=its.IDCntrCst ',
  '  AND lct.AnoMes=its.AnoMes ',
  'LEFT JOIN excctapag ecp ON ecp.Codigo=its.EXcCtaPag ',
  'LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta ',
  'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag ',
  'LEFT JOIN excidfunc eif ON eif.Codigo=its.EXcIdFunc ',
  'WHERE its.AnoMes="' + EdAnoMes.Text + '" ',
  'AND eif.CodFornOth="' + QrSintVendor.Value + '"',
  'AND emp.Codigo=' + Geral.FF0(QrSintEXcMdoPag.Value),
  'GROUP BY eif.Company, Vendor, ',
  '  emp.Reference, GLAccount, SiglaOri ',
  '']);
end;

function TFmExportaSAP1.ReopenDespesasSint(Corda: String): Boolean;
begin
  if Corda <> EmptyStr then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSint, Dmod.MyDB, [
    'SELECT eif.Company, eif.Nome NO_Fun, SYSDATE() PostDate,  ',
    'SYSDATE() DocDate, "KI" DocType, emp.Reference,  ',
    'emp.DocHeader, emp.Codigo EXcMdoPag, ',
    'emp.ItemText, eif.CodFornOth Vendor, ',
    'eif.CenCustOth, SUM(its.ValorOri) ValorOri, ',
    'its.SiglaOri, emp.RegrQtdDia, ',
    'emp.RegrTipSeq, emp.RegrDiaSem ',
    '  ',
    'FROM exmdspdevits its  ',
    'LEFT JOIN exmdspdevlct lct   ',
    '  ON lct.CodInMob=its.CodInMob  ',
    '  AND lct.IDCntrCst=its.IDCntrCst  ',
    '  AND lct.AnoMes=its.AnoMes  ',
    'LEFT JOIN excctapag ecp ON ecp.Codigo=its.EXcCtaPag   ',
    'LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta   ',
    'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag    ',
    'LEFT JOIN excidfunc eif ON eif.Codigo=its.EXcIdFunc ',
    'WHERE its.AnoMes="' + EdAnoMes.Text + '" ',
    'AND Company="' + EdCompany.Text + '"',
    'AND eif.Codigo IN (' + Corda + ') ',
    'GROUP BY eif.Company, Vendor,  ',
    '  emp.Reference, SiglaOri ',
    '']);
  end else
    QrSint.Close;
end;

procedure TFmExportaSAP1.ReopenEXcIdFunc();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXcIdFunc, Dmod.MyDB, [
  'SELECT eif.Codigo, eif.Nome, ',
  'IF(es1.AnoMes IS NULL, "", "OK") SIT ',
  'FROM excidfunc eif ',
  'LEFT JOIN exportasap es1 ON es1.EXcIdFunc=eif.Codigo  ',
  '  AND es1.AnoMes="' + EdAnoMes.Text + '" ',
  'WHERE eif.Company = "' + EdCompany.Text + '"',
  'ORDER BY eif.Nome ',
  '']);
end;

function TFmExportaSAP1.SubsTags(Texto: String): String;
var
  AnoMes, MesAno: String;
begin
  Result := Texto;
  Result := StringReplace(Result, '[ANOMES]', FAnoStr + FMesStr, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '[MESANO]', Uppercase(Fmmm) + '/' + FAnoStr, [rfReplaceAll, rfIgnoreCase]);
end;

end.
