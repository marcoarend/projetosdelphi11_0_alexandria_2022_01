object FmExportaSAP1: TFmExportaSAP1
  Left = 339
  Top = 185
  Caption = 'EXS-EXSAP-001 :: Exporta para SAP Modelo 1'
  ClientHeight = 629
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1168
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 335
        Height = 32
        Caption = 'Exporta para SAP Modelo 1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 335
        Height = 32
        Caption = 'Exporta para SAP Modelo 1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 335
        Height = 32
        Caption = 'Exporta para SAP Modelo 1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1264
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 1118
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1116
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Abre'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtGera: TBitBtn
        Tag = 14
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Gera'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtGeraClick
      end
      object BtSalva: TBitBtn
        Tag = 14
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Salva'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtSalvaClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1264
    Height = 511
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1264
      Height = 57
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 336
        Height = 57
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitHeight = 141
        object SbDecrAnoMes: TSpeedButton
          Left = 8
          Top = 24
          Width = 21
          Height = 21
        end
        object SbIncAnoMes: TSpeedButton
          Left = 112
          Top = 24
          Width = 21
          Height = 21
        end
        object Label1: TLabel
          Left = 140
          Top = 8
          Width = 23
          Height = 13
          Caption = 'Filial:'
        end
        object Label2: TLabel
          Left = 180
          Top = 8
          Width = 131
          Height = 13
          Caption = 'Taxa de c'#226'mbio BRL/USD:'
          Enabled = False
        end
        object EdAnoMes: TdmkEdit
          Left = 32
          Top = 24
          Width = 78
          Height = 21
          Alignment = taCenter
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '2000-01'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '2000-01'
          ValWarn = False
          OnChange = EdAnoMesChange
          OnRedefinido = EdAnoMesRedefinido
        end
        object CkAnoMes: TCheckBox
          Left = 8
          Top = 4
          Width = 97
          Height = 17
          Caption = 'Ano - M'#234's:'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object EdCompany: TdmkEdit
          Left = 140
          Top = 24
          Width = 37
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0041'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0041'
          ValWarn = False
          OnChange = EdCompanyChange
        end
        object EdBRL_USD: TdmkEdit
          Left = 180
          Top = 24
          Width = 133
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 10
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GBAvisos1: TGroupBox
        Left = 336
        Top = 0
        Width = 928
        Height = 57
        Align = alClient
        Caption = ' Avisos: '
        TabOrder = 1
        ExplicitHeight = 141
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 924
          Height = 23
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 107
          object LaAviso1: TLabel
            Left = 13
            Top = 2
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 12
            Top = 1
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
        object PB1: TProgressBar
          Left = 2
          Top = 38
          Width = 924
          Height = 17
          Align = alBottom
          TabOrder = 1
          ExplicitTop = 122
        end
      end
    end
    object DBEXcIdFuns: TDBGrid
      Left = 0
      Top = 57
      Width = 309
      Height = 247
      Align = alLeft
      DataSource = DsEXcIDFunc
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 195
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIT'
          Title.Caption = 'OK'
          Visible = True
        end>
    end
    object SGDocs: TStringGrid
      Left = 309
      Top = 57
      Width = 955
      Height = 247
      Align = alClient
      ColCount = 18
      DefaultColWidth = 24
      DefaultRowHeight = 21
      RowCount = 2
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      ExplicitTop = 141
      ExplicitHeight = 163
      ColWidths = (
        24
        79
        71
        71
        76
        90
        101
        102
        86
        78
        87
        73
        72
        76
        98
        112
        69
        71)
    end
    object SGExporta: TStringGrid
      Left = 0
      Top = 304
      Width = 1264
      Height = 207
      Align = alBottom
      ColCount = 17
      DefaultColWidth = 24
      DefaultRowHeight = 21
      FixedCols = 0
      RowCount = 2
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Visible = False
      ColWidths = (
        24
        79
        71
        71
        76
        90
        101
        102
        86
        78
        87
        73
        72
        76
        98
        112
        69)
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrAnal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eif.Company, SYSDATE() PostDate, '
      'SYSDATE() DocDate,  "KI" DocType, emp.Reference, '
      'eif.Nome NO_Fun, emp.DocHeader, egc.Codigo GLAccount, '
      'egc.Nome NO_GLAccount, '
      'emp.ItemText, eif.CodFornOth Vendor, '
      'eif.CenCustOth, SUM(its.ValorOri) ValorOri, '
      'its.SiglaOri '
      ' '
      'FROM exmdspdevits its '
      'LEFT JOIN exmdspdevlct lct '
      '  ON lct.CodInMob=its.CodInMob '
      '  AND lct.IDCntrCst=its.IDCntrCst '
      '  AND lct.AnoMes=its.AnoMes '
      'LEFT JOIN excctapag ecp ON ecp.Codigo=its.EXcCtaPag '
      'LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta '
      'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag '
      'LEFT JOIN excidfunc eif ON eif.Codigo=its.EXcIdFunc '
      'WHERE its.AnoMes="2022-03" '
      'AND eif.CodFornOth=519373 '
      'GROUP BY eif.Company, Vendor, '
      '  emp.Reference, GLAccount, SiglaOri ')
    Left = 136
    Top = 264
    object QrAnalCompany: TWideStringField
      FieldName = 'Company'
      Size = 4
    end
    object QrAnalPostDate: TDateTimeField
      FieldName = 'PostDate'
      Required = True
    end
    object QrAnalDocDate: TDateTimeField
      FieldName = 'DocDate'
      Required = True
    end
    object QrAnalDocType: TWideStringField
      FieldName = 'DocType'
      Required = True
      Size = 2
    end
    object QrAnalReference: TWideStringField
      FieldName = 'Reference'
      Size = 16
    end
    object QrAnalNO_Fun: TWideStringField
      FieldName = 'NO_Fun'
      Size = 60
    end
    object QrAnalDocHeader: TWideStringField
      FieldName = 'DocHeader'
      Size = 25
    end
    object QrAnalGLAccount: TIntegerField
      FieldName = 'GLAccount'
    end
    object QrAnalNO_GLAccount: TWideStringField
      FieldName = 'NO_GLAccount'
      Size = 60
    end
    object QrAnalItemText: TWideStringField
      FieldName = 'ItemText'
      Size = 50
    end
    object QrAnalVendor: TWideStringField
      FieldName = 'Vendor'
      Size = 60
    end
    object QrAnalCenCustOth: TWideStringField
      FieldName = 'CenCustOth'
      Size = 60
    end
    object QrAnalValorOri: TFloatField
      FieldName = 'ValorOri'
    end
    object QrAnalSiglaOri: TWideStringField
      FieldName = 'SiglaOri'
      Required = True
      Size = 10
    end
  end
  object QrSint: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSintBeforeClose
    AfterScroll = QrSintAfterScroll
    SQL.Strings = (
      'SELECT eif.Company, eif.Nome NO_Fun, SYSDATE() PostDate,  '
      'SYSDATE() DocDate, "KI" DocType, emp.Reference,  '
      'emp.DocHeader, '
      'emp.ItemText, eif.CodFornOth Vendor, '
      'eif.CenCustOth, SUM(its.ValorOri) ValorOri, '
      'its.SiglaOri '
      '  '
      'FROM exmdspdevits its  '
      'LEFT JOIN exmdspdevlct lct   '
      '  ON lct.CodInMob=its.CodInMob  '
      '  AND lct.IDCntrCst=its.IDCntrCst  '
      '  AND lct.AnoMes=its.AnoMes  '
      'LEFT JOIN excctapag ecp ON ecp.Codigo=its.EXcCtaPag   '
      'LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta   '
      'LEFT JOIN excmdopag emp ON emp.Codigo=its.EXcMdoPag    '
      'LEFT JOIN excidfunc eif ON eif.Codigo=its.EXcIdFunc '
      'WHERE its.AnoMes="2022-03" '
      'AND Company="0041"'
      ' '
      'GROUP BY eif.Company, Vendor,  '
      '  emp.Reference, SiglaOri ')
    Left = 136
    Top = 208
    object QrSintCompany: TWideStringField
      FieldName = 'Company'
      Size = 4
    end
    object QrSintNO_Fun: TWideStringField
      FieldName = 'NO_Fun'
      Size = 60
    end
    object QrSintPostDate: TDateTimeField
      FieldName = 'PostDate'
      Required = True
    end
    object QrSintDocDate: TDateTimeField
      FieldName = 'DocDate'
      Required = True
    end
    object QrSintDocType: TWideStringField
      FieldName = 'DocType'
      Required = True
      Size = 2
    end
    object QrSintReference: TWideStringField
      FieldName = 'Reference'
      Size = 16
    end
    object QrSintDocHeader: TWideStringField
      FieldName = 'DocHeader'
      Size = 25
    end
    object QrSintItemText: TWideStringField
      FieldName = 'ItemText'
      Size = 50
    end
    object QrSintVendor: TWideStringField
      FieldName = 'Vendor'
      Size = 60
    end
    object QrSintCenCustOth: TWideStringField
      FieldName = 'CenCustOth'
      Size = 60
    end
    object QrSintValorOri: TFloatField
      FieldName = 'ValorOri'
    end
    object QrSintSiglaOri: TWideStringField
      FieldName = 'SiglaOri'
      Required = True
      Size = 10
    end
    object QrSintEXcMdoPag: TIntegerField
      FieldName = 'EXcMdoPag'
    end
    object QrSintRegrQtdDia: TIntegerField
      FieldName = 'RegrQtdDia'
    end
    object QrSintRegrTipSeq: TIntegerField
      FieldName = 'RegrTipSeq'
    end
    object QrSintRegrDiaSem: TSmallintField
      FieldName = 'RegrDiaSem'
    end
  end
  object QrEXcIDFunc: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM excidfunc'
      'ORDER BY Nome')
    Left = 32
    Top = 152
    object QrEXcIDFuncCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEXcIDFuncNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
      Transliterate = False
    end
    object QrEXcIDFuncSIT: TWideStringField
      FieldName = 'SIT'
      Size = 2
    end
  end
  object DsEXcIDFunc: TDataSource
    DataSet = QrEXcIDFunc
    Left = 28
    Top = 208
  end
end
