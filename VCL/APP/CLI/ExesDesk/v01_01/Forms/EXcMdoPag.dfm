object FmEXcMdoPag: TFmEXcMdoPag
  Left = 368
  Top = 194
  Caption = 'EXS-CADAS-001 :: Cadastro de Modos de Pagamento'
  ClientHeight = 423
  ClientWidth = 774
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 774
    Height = 327
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 713
      Height = 263
      Align = alLeft
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 31
        Height = 13
        Caption = 'Ordem'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 156
        Top = 56
        Width = 202
        Height = 13
        Caption = 'Caminho do arquivo da imagem carregada:'
        FocusControl = DBEdit2
      end
      object Label8: TLabel
        Left = 576
        Top = 16
        Width = 23
        Height = 13
        Caption = 'Sigla'
        FocusControl = DBEdit3
      end
      object Label20: TLabel
        Left = 324
        Top = 16
        Width = 98
        Height = 13
        Caption = 'Descri'#231#227'o em ingl'#234's:'
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEXcMdoPag
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 245
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEXcMdoPag
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCheckBox2: TDBCheckBox
        Left = 652
        Top = 56
        Width = 57
        Height = 17
        Caption = 'Ativo.'
        DataField = 'Ativo'
        DataSource = DsEXcMdoPag
        TabOrder = 3
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 134
        Height = 21
        DataField = 'Ordem'
        DataSource = DsEXcMdoPag
        TabOrder = 4
      end
      object DBEdit2: TDBEdit
        Left = 156
        Top = 72
        Width = 549
        Height = 21
        DataField = 'ImgPath'
        DataSource = DsEXcMdoPag
        TabOrder = 5
      end
      object DBEdit3: TDBEdit
        Left = 576
        Top = 32
        Width = 134
        Height = 21
        DataField = 'Sigla'
        DataSource = DsEXcMdoPag
        TabOrder = 2
      end
      object GroupBox2: TGroupBox
        Left = 16
        Top = 100
        Width = 685
        Height = 145
        Caption = ' C'#243'idigos de app terceiros: (exporta'#231#227'o de dados)'
        TabOrder = 6
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 681
          Height = 128
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label11: TLabel
            Left = 168
            Top = 8
            Width = 61
            Height = 13
            Caption = 'Doc Header:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label12: TLabel
            Left = 8
            Top = 48
            Width = 43
            Height = 13
            Caption = 'Item text:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label14: TLabel
            Left = 8
            Top = 8
            Width = 50
            Height = 13
            Caption = 'Reference'
            FocusControl = DBEdit6
          end
          object Label18: TLabel
            Left = 424
            Top = 85
            Width = 24
            Height = 13
            Caption = 'Dias:'
            Color = clBtnFace
            ParentColor = False
          end
          object DBEdit4: TDBEdit
            Left = 8
            Top = 64
            Width = 405
            Height = 21
            DataField = 'ItemText'
            DataSource = DsEXcMdoPag
            TabOrder = 0
          end
          object DBEdit5: TDBEdit
            Left = 166
            Top = 24
            Width = 233
            Height = 21
            DataField = 'DocHeader'
            DataSource = DsEXcMdoPag
            TabOrder = 1
          end
          object DBEdit6: TDBEdit
            Left = 8
            Top = 24
            Width = 152
            Height = 21
            DataField = 'Reference'
            DataSource = DsEXcMdoPag
            TabOrder = 2
          end
          object dmkRadioGroup1: TDBRadioGroup
            Left = 420
            Top = -3
            Width = 113
            Height = 89
            Caption = ' Tipo de prazo: '
            DataField = 'RegrTipSeq'
            DataSource = DsEXcMdoPag
            Items.Strings = (
              'N'#227'o informar'
              'Dia corrido'
              'Dia '#250'til'
              'Semana corrida')
            TabOrder = 3
            Values.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9')
          end
          object dmkRadioGroup2: TDBRadioGroup
            Left = 536
            Top = -3
            Width = 141
            Height = 125
            Caption = ' Dia da semana: '
            Columns = 2
            DataField = 'RegrDiaSem'
            DataSource = DsEXcMdoPag
            Items.Strings = (
              'N/D'
              'Domingo'
              'Segunda'
              'Ter'#231'a'
              'Quarta'
              'Quinta'
              'Sexta'
              'S'#225'bado')
            TabOrder = 4
            Values.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9')
          end
          object DBEdit7: TDBEdit
            Left = 424
            Top = 100
            Width = 61
            Height = 21
            DataField = 'RegrQtdDia'
            DataSource = DsEXcMdoPag
            TabOrder = 5
          end
        end
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 324
        Top = 32
        Width = 245
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NoEN'
        DataSource = DsEXcMdoPag
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object Panel6: TPanel
      Left = 713
      Top = 0
      Width = 61
      Height = 263
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object PnImg: TPanel
        Left = 0
        Top = 0
        Width = 61
        Height = 69
        Align = alTop
        BevelOuter = bvNone
        Caption = 'BMP'
        DoubleBuffered = True
        ParentDoubleBuffered = False
        TabOrder = 0
        object ImgImgB64: TImage
          Left = 4
          Top = 8
          Width = 50
          Height = 50
          Proportional = True
          Stretch = True
          Transparent = True
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 69
        Width = 61
        Height = 64
        Align = alTop
        BevelOuter = bvNone
        Caption = 'PNG'
        DoubleBuffered = True
        ParentDoubleBuffered = False
        TabOrder = 1
        object ImgPngB64: TImage
          Left = 4
          Top = 4
          Width = 50
          Height = 50
          Proportional = True
          Stretch = True
          Transparent = True
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 263
      Width = 774
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 94
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 268
        Top = 15
        Width = 504
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 371
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 774
    Height = 327
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 774
      Height = 265
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      ExplicitLeft = 260
      ExplicitTop = -24
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 56
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object Label6: TLabel
        Left = 100
        Top = 56
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label19: TLabel
        Left = 428
        Top = 16
        Width = 98
        Height = 13
        Caption = 'Descri'#231#227'o em ingl'#234's:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 349
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkAtivo: TdmkCheckBox
        Left = 720
        Top = 56
        Width = 53
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 4
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object EdOrdem: TdmkEdit
        Left = 16
        Top = 72
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSigla: TdmkEdit
        Left = 100
        Top = 72
        Width = 90
        Height = 21
        MaxLength = 10
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sigla'
        UpdCampo = 'Sigla'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 100
        Width = 753
        Height = 149
        Caption = ' C'#243'digos de app terceiros: (exporta'#231#227'o de dados)'
        TabOrder = 6
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 749
          Height = 132
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label15: TLabel
            Left = 8
            Top = 8
            Width = 53
            Height = 13
            Caption = 'Reference:'
            FocusControl = DBEdit6
          end
          object Label16: TLabel
            Left = 168
            Top = 8
            Width = 61
            Height = 13
            Caption = 'Doc Header:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label17: TLabel
            Left = 8
            Top = 48
            Width = 43
            Height = 13
            Caption = 'Item text:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label10: TLabel
            Left = 8
            Top = 88
            Width = 114
            Height = 13
            Caption = 'Condi'#231#227'o de reembolso:'
            Color = clBtnFace
            ParentColor = False
            Visible = False
          end
          object Label13: TLabel
            Left = 496
            Top = 88
            Width = 24
            Height = 13
            Caption = 'Dias:'
            Color = clBtnFace
            ParentColor = False
          end
          object EdDocHeader: TdmkEdit
            Left = 168
            Top = 24
            Width = 245
            Height = 21
            MaxLength = 25
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'DocHeader'
            UpdCampo = 'DocHeader'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdItemText: TdmkEdit
            Left = 8
            Top = 64
            Width = 481
            Height = 21
            MaxLength = 50
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'ItemText'
            UpdCampo = 'ItemText'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdReference: TdmkEdit
            Left = 8
            Top = 24
            Width = 153
            Height = 21
            MaxLength = 16
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Reference'
            UpdCampo = 'Reference'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object dmkEdit1: TdmkEdit
            Left = 8
            Top = 104
            Width = 481
            Height = 21
            MaxLength = 50
            TabOrder = 3
            Visible = False
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'ItemText'
            UpdCampo = 'ItemText'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object RGRegrTipSeq: TdmkRadioGroup
            Left = 492
            Top = 0
            Width = 105
            Height = 89
            Caption = ' Tipo de prazo: '
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o informar'
              'Dia corrido'
              'Dia '#250'til'
              'Semana corrida')
            TabOrder = 4
            OnClick = RGRegrTipSeqClick
            QryCampo = 'RegrTipSeq'
            UpdCampo = 'RegrTipSeq'
            UpdType = utYes
            OldValor = 0
          end
          object EdRegrQtdDia: TdmkEdit
            Left = 496
            Top = 104
            Width = 61
            Height = 21
            Alignment = taRightJustify
            MaxLength = 25
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '1'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            QryCampo = 'RegrQtdDia'
            UpdCampo = 'EdRegrQtdDia'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object RGRegrDiaSem: TdmkRadioGroup
            Left = 596
            Top = 0
            Width = 145
            Height = 125
            Caption = ' Dia da semana: '
            Columns = 2
            Enabled = False
            ItemIndex = 0
            Items.Strings = (
              'N/D'
              'Domingo'
              'Segunda'
              'Ter'#231'a'
              'Quarta'
              'Quinta'
              'Sexta'
              'S'#225'bado')
            TabOrder = 6
            QryCampo = 'RegrDiaSem'
            UpdCampo = 'RegrDiaSem'
            UpdType = utYes
            OldValor = 0
          end
        end
      end
      object EdNoEn: TdmkEdit
        Left = 428
        Top = 32
        Width = 337
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NoEN'
        UpdCampo = 'NoEN'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 264
      Width = 774
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 635
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 774
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 726
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 510
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 422
        Height = 32
        Caption = 'Cadastro de Modos de Pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 422
        Height = 32
        Caption = 'Cadastro de Modos de Pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 422
        Height = 32
        Caption = 'Cadastro de Modos de Pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 774
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 770
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrEXcMdoPag: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrEXcMdoPagBeforeOpen
    AfterOpen = QrEXcMdoPagAfterOpen
    AfterScroll = QrEXcMdoPagAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM cadastro_simples')
    Left = 64
    Top = 64
    object QrEXcMdoPagCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEXcMdoPagNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
    object QrEXcMdoPagOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEXcMdoPagAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEXcMdoPagImgB64: TWideMemoField
      FieldName = 'ImgB64'
      BlobType = ftWideMemo
    end
    object QrEXcMdoPagImgPath: TWideStringField
      FieldName = 'ImgPath'
      Size = 511
    end
    object QrEXcMdoPagPngB64: TWideMemoField
      FieldName = 'PngB64'
      BlobType = ftWideMemo
    end
    object QrEXcMdoPagSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 10
    end
    object QrEXcMdoPagDocHeader: TWideStringField
      FieldName = 'DocHeader'
      Size = 25
    end
    object QrEXcMdoPagItemText: TWideStringField
      FieldName = 'ItemText'
      Size = 50
    end
    object QrEXcMdoPagReference: TWideStringField
      FieldName = 'Reference'
      Size = 16
    end
    object QrEXcMdoPagRegrQtdDia: TIntegerField
      FieldName = 'RegrQtdDia'
    end
    object QrEXcMdoPagRegrTipSeq: TIntegerField
      FieldName = 'RegrTipSeq'
    end
    object QrEXcMdoPagRegrDiaSem: TSmallintField
      FieldName = 'RegrDiaSem'
    end
    object QrEXcMdoPagNoEN: TWideStringField
      FieldName = 'NoEN'
      Size = 60
    end
  end
  object DsEXcMdoPag: TDataSource
    DataSet = QrEXcMdoPag
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object PMAltera: TPopupMenu
    Left = 404
    Top = 172
    object Dados1: TMenuItem
      Caption = '&Dados'
      OnClick = Dados1Click
    end
    object Imagem1: TMenuItem
      Caption = '&Imagem Desktop'
      OnClick = Imagem1Click
    end
    object ImagemMobile1: TMenuItem
      Caption = 'Imagem Mobile (png)'
      OnClick = ImagemMobile1Click
    end
  end
end
