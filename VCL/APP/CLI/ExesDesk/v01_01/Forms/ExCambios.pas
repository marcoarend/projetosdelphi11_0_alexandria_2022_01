unit ExCambios;

interface

uses
  Windows, System.UITypes, Messages, SysUtils, Classes, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, Vcl.Imaging.jpeg,
  FMX.Graphics, Soap.EncdDecd,
  UnProjGroupEnums,
  VCL.Graphics;

type
  TFmExCambios = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPeriodos: TMySQLQuery;
    DsPeriodos: TDataSource;
    PnPeriodo: TPanel;
    QrPeriodosAnoMes: TWideStringField;
    QrPeriodosAno: TIntegerField;
    QrPeriodosMes: TIntegerField;
    QrEXmDspDevMCb: TMySQLQuery;
    DsEXmDspDevMCb: TDataSource;
    PnModo: TPanel;
    QrEXmDspDevFts: TMySQLQuery;
    DsEXmDspDevFts: TDataSource;
    QrEXmDspDevFtsCodigo: TIntegerField;
    QrEXmDspDevFtsCodInMob: TIntegerField;
    QrEXmDspDevFtsCtrlInMob: TIntegerField;
    QrEXmDspDevFtsIDTabela: TIntegerField;
    QrEXmDspDevFtsDataHora: TDateTimeField;
    QrEXmDspDevFtsCtrlFoto: TIntegerField;
    QrEXmDspDevFtsEXcStCmprv: TIntegerField;
    QrEXmDspDevFtsNomeArq: TWideStringField;
    QrEXmDspDevFtsNoArqSvr: TWideStringField;
    QrEXmDspDevFtsAtivo: TSmallintField;
    PnFotoENome: TPanel;
    ImgFoto: TImage;
    LaNomeFoto: TLabel;
    QrEXmDspDevBmp: TMySQLQuery;
    QrEXmDspDevBmpTxtBmp: TWideMemoField;
    Splitter3: TSplitter;
    QrEXmDspDevFtsNO_EXcStCmprv: TWideStringField;
    PnGrids: TPanel;
    PnFiltros: TPanel;
    Panel3: TPanel;
    DBGPeriodos: TDBGrid;
    DBGCambios: TDBGrid;
    QrEXmDspDevMCbCodigo: TIntegerField;
    QrEXmDspDevMCbCodInMob: TIntegerField;
    QrEXmDspDevMCbDeviceSI: TIntegerField;
    QrEXmDspDevMCbDeviceID: TWideStringField;
    QrEXmDspDevMCbRandmStr: TWideStringField;
    QrEXmDspDevMCbAnoMes: TWideStringField;
    QrEXmDspDevMCbAno: TIntegerField;
    QrEXmDspDevMCbMes: TIntegerField;
    QrEXmDspDevMCbEXcIdFunc: TIntegerField;
    QrEXmDspDevMCbIDCntrCst: TIntegerField;
    QrEXmDspDevMCbData: TDateField;
    QrEXmDspDevMCbHora: TTimeField;
    QrEXmDspDevMCbSiglaOri: TWideStringField;
    QrEXmDspDevMCbCotacInf: TFloatField;
    QrEXmDspDevMCbCotacOri: TFloatField;
    QrEXmDspDevMCbValorOri: TFloatField;
    QrEXmDspDevMCbSiglaDst: TWideStringField;
    QrEXmDspDevMCbCotacDst: TFloatField;
    QrEXmDspDevMCbValorDst: TFloatField;
    QrEXmDspDevMCbObserva: TWideStringField;
    QrEXmDspDevMCbQtdFotos: TIntegerField;
    QrEXmDspDevMCbObsrvFinal: TWideStringField;
    QrEXmDspDevMCbEncerrado: TDateTimeField;
    QrEXmDspDevMCbDtHrUpIni: TDateTimeField;
    QrEXmDspDevMCbDtHrUpFim: TDateTimeField;
    QrEXmDspDevMCbEnviadoWeb: TSmallintField;
    QrEXmDspDevFtsMobInstlSeq: TIntegerField;
    QrEXmDspDevFtsSigla_IdFunc: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPeriodosBeforeClose(DataSet: TDataSet);
    procedure QrPeriodosAfterScroll(DataSet: TDataSet);
    procedure QrEXmDspDevItsAfterScroll(DataSet: TDataSet);
    procedure QrEXmDspDevItsBeforeClose(DataSet: TDataSet);
    procedure QrEXmDspDevFtsAfterScroll(DataSet: TDataSet);
    procedure QrEXmDspDevMCbBeforeClose(DataSet: TDataSet);
    procedure QrEXmDspDevMCbAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
     procedure ReopenEXmDspDevMCb();
     procedure ReopenEXmDspDevFts();
     //
     function  ConvertFmxBitmapToVclBitmap(b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
  public
    { Public declarations }
    FEXcIdFunc: Integer;
    //
    procedure ReopenPeriodos();
  end;

  var
  FmExCambios: TFmExCambios;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UnExes_ProjGroupVars, UnDmkImg, UnExesD_PF,
  UMySQLModule;

{$R *.DFM}

function BitmapFromBase64(const base64: string):FMX.Graphics.TBitmap;
var
  Input: TStringStream;
  Output: TBytesStream;
begin
  Input := TStringStream.Create(base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Soap.EncdDecd.DecodeStream(Input, Output);
      Output.Position := 0;
      Result := FMX.Graphics.TBitmap.Create;
      try
        Result.LoadFromStream(Output);
      except
        Result.Free;
        raise;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TFmExCambios.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmExCambios.ConvertFmxBitmapToVclBitmap(
  b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
var
  data:FMX.Graphics.TBitmapData;
  i,j:Integer;
  AlphaColor: System.UITypes.TAlphaColor;
begin
  Result := VCL.Graphics.TBitmap.Create;
  Result.SetSize(b.Width, b.Height);
  if(b.Map(TMapAccess.Readwrite, data)) then
  try
    for i := 0 to data.Height-1 do
    begin
      for j := 0 to data.Width-1 do
      begin
        AlphaColor:=data.GetPixel(j,i);
        Result.Canvas.Pixels[j,i]:=
          RGB(
            TAlphaColorRec(AlphaColor).R,
            TAlphaColorRec(AlphaColor).G,
            TAlphaColorRec(AlphaColor).B
          );
      end;
    end;
  finally
    b.Unmap(data);
  end;
end;

procedure TFmExCambios.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmExCambios.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmExCambios.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmExCambios.QrEXmDspDevFtsAfterScroll(DataSet: TDataSet);
var
  NomeArq, NoArqSvr, Base64: AnsiString;
  FMX_Bitmap: FMX.Graphics.TBitmap;
  VCL_Bitmap: VCL.Graphics.TBitmap;
  Codigo, EXcIDFunc, CtrlInMob, IdTabela, CtrlFoto, MobInstlSeq, EXcStCmprv: Integer;
  DataHora: TDateTime;
  SiglaOri, Sigla_IdFunc: String;
  CriarArqLocal: Boolean;
  ValorOri: Double;
begin
  if VAR_DIR_FOTOS_DESPESAS = EmptyStr then Exit;
  CriarArqLocal := False;
  NomeArq := VAR_DIR_FOTOS_DESPESAS + QrEXmDspDevFtsNoArqSvr.Value;
  if QrEXmDspDevFtsNoArqSvr.Value <> EmptyStr then
  begin
    if FileExists(NomeArq) then
      ImgFoto.Picture.Bitmap.LoadFromFile(NomeArq)
    else
      CriarArqLocal := True;
  end else
    CriarArqLocal := True;
  if CriarArqLocal then
  begin
    if not DirectoryExists(VAR_DIR_FOTOS_DESPESAS) then
      ForceDirectories(VAR_DIR_FOTOS_DESPESAS);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevBmp, Dmod.MyDB, [
    'SELECT bmp.TxtBmp ',
    'FROM exmdspdevbmp bmp',
    'WHERE bmp.Codigo=' + Geral.FF0(QrEXmDspDevFtsCodigo.Value),
    'AND bmp.CodInMob=' + Geral.FF0(QrEXmDspDevFtsCodInMob.Value),
    'AND bmp.CtrlFoto=' + Geral.FF0(QrEXmDspDevFtsCtrlFoto.Value),
    'AND bmp.IDTabela=' + Geral.FF0(Integer(TEXIdGrupoTabelas.exigt_01_Cotacao)),
    'ORDER BY bmp.Pedaco ',
    EmptyStr]);
    //
    Base64 := '';
    QrEXmDspDevBmp.First;
    while not QrEXmDspDevBmp.Eof do
    begin
      Base64 := Base64 + QrEXmDspDevBmpTxtBmp.Value;
      //
      QrEXmDspDevBmp.Next;
    end;
    if Base64 <> EmptyStr then
    begin
      FMX_BitMap := FMX.Graphics.TBitmap.Create;
      VCL_BitMap := VCL.Graphics.TBitmap.Create;
      //
      FMX_Bitmap := BitmapFromBase64(Base64);
      //Converter aqui de FMX para VCL!
      VCL_BitMap := ConvertFmxBitmapToVclBitmap(FMX_Bitmap);
      //
      ImgFoto.Picture.Bitmap.Assign(VCL_Bitmap);
      Codigo    := QrEXmDspDevFtsCodigo.Value;
      EXcIdFunc := FExcIdFunc;
      CtrlInMob := QrEXmDspDevFtsCtrlInMob.Value;
      DataHora  := QrEXmDspDevFtsDataHora.Value;
      IdTabela  := QrEXmDspDevFtsIDTabela.Value;
      CtrlFoto  := QrEXmDspDevFtsCtrlFoto.Value;
      SiglaOri  := QrEXmDspDevMCbSiglaOri.Value;
      ValorOri  := QrEXmDspDevMcbValorOri.Value;
      MobInstlSeq := QrEXmDspDevFtsMobInstlSeq.Value;
      EXcStCmprv  := QrEXmDspDevFtsEXcStCmprv.Value;
      Sigla_IdFunc := QrEXmDspDevFtsSigla_IdFunc.Value;
      //
      NoArqSvr  := ExesD_PF.DefineNomeArquivoFotoDespesa(IDTabela, Codigo,
      MobInstlSeq, CtrlInMob, CtrlFoto, EXcIdFunc, Sigla_IdFunc, EXcStCmprv, SiglaOri,
      ValorOri, DataHora, QrPeriodosAnoMes.Value);
      //
      NomeArq := VAR_DIR_FOTOS_DESPESAS + NoArqSvr;
      ImgFoto.Picture.Bitmap.SaveToFile(NomeArq);
      if FileExists(NomeArq) and (QrEXmDspDevFtsNoArqSvr.Value = EmptyStr) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'exmdspdevfts', False, [
        'NoArqSvr'], [
        'Codigo', 'IdTabela', 'CtrlInMob', 'CtrlFoto'], [
        NoArqSvr], [
        Codigo, IdTabela, CtrlInMob, CtrlFoto], True) then
        begin
          // ???? Excluir dados da tabela de exmdspdevbmp?????
        end;
      end;
    end;
  end;
end;

procedure TFmExCambios.QrEXmDspDevItsAfterScroll(DataSet: TDataSet);
begin
  ReopenEXmDspDevFts();
end;

procedure TFmExCambios.QrEXmDspDevItsBeforeClose(DataSet: TDataSet);
begin
  QrEXmDspDevFts.Close;
  if ImgFoto.Picture <> nil then
    ImgFoto.Picture := nil;
end;

procedure TFmExCambios.QrEXmDspDevMCbAfterScroll(DataSet: TDataSet);
begin
  ReopenEXmDspDevFts();
end;

procedure TFmExCambios.QrEXmDspDevMCbBeforeClose(DataSet: TDataSet);
begin
  QrEXmDspDevFts.Close;
end;

procedure TFmExCambios.QrPeriodosAfterScroll(DataSet: TDataSet);
begin
  ReopenEXmDspDevMCb();
end;

procedure TFmExCambios.QrPeriodosBeforeClose(DataSet: TDataSet);
begin
  QrEXmDspDevMCb.Close
end;

procedure TFmExCambios.ReopenEXmDspDevMCb();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevMCb, Dmod.MyDB, [
  'SELECT mcb.* ',
  'FROM exmdspdevmcb mcb ',
  'WHERE mcb.EXcIdFunc=' + Geral.FF0(FEXcIdFunc),
  'AND mcb.Ano=' + Geral.FF0(QrPeriodosAno.Value),
  'AND mcb.Mes=' + Geral.FF0(QrPeriodosMes.Value),
  '']);
end;

procedure TFmExCambios.ReopenEXmDspDevFts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXmDspDevFts, Dmod.MyDB, [
  'SELECT scm.Nome NO_EXcStCmprv, ',
  'fnc.Nome NO_EXcIdFunc, fnc.Sigla Sigla_IdFunc, ',
  'fts.* ',
  'FROM exmdspdevfts fts  ',
  'LEFT JOIN excstcmprv scm ON scm.Codigo=fts.EXcStCmprv ',
  'LEFT JOIN excidfunc fnc ON fts.EXcIDFunc=fnc.Codigo ',
  'WHERE fts.Codigo=' + Geral.FF0(QrEXmDspDevMCbCodigo.Value),
  'AND fts.CodInMob=' + Geral.FF0(QrEXmDspDevMCbCodInMob.Value),
  'AND fts.IDTabela=' + Geral.FF0(Integer(TEXIdGrupoTabelas.exigt_01_Cotacao)),
  '']);
  //Geral.MB_Teste(QrEXmDspDevFts.SQL.Text);
end;

procedure TFmExCambios.ReopenPeriodos();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPeriodos, Dmod.MyDB, [
  'SELECT DISTINCT AnoMes, Ano, Mes',
  'FROM exmdspdevlct',
  'WHERE EXcIdFunc=' + Geral.FF0(FEXcIdFunc),
  'ORDER BY Ano DESC, Mes DESC',
  '']);
end;

end.
