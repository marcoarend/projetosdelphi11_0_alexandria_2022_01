object FmExDespesaMdo: TFmExDespesaMdo
  Left = 339
  Top = 185
  Caption = 
    'EXS-GEREN-001 :: Gerenciamento de Presta'#231#227'o de Contas por Modo e' +
    ' Conta'
  ClientHeight = 645
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1168
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 713
        Height = 32
        Caption = 'Gerenciamento de Presta'#231#227'o de Contas por Modo e Conta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 713
        Height = 32
        Caption = 'Gerenciamento de Presta'#231#227'o de Contas por Modo e Conta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 713
        Height = 32
        Caption = 'Gerenciamento de Presta'#231#227'o de Contas por Modo e Conta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 531
    Width = 1264
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1260
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 575
    Width = 1264
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 1118
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1116
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtGerarFotos: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Gera fotos'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtGerarFotosClick
      end
      object Button1: TButton
        Left = 636
        Top = 16
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 1
        Visible = False
        OnClick = Button1Click
      end
      object CkMostrarFoto: TCheckBox
        Left = 140
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Mostrar Foto'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
    end
  end
  object PnPeriodo: TPanel
    Left = 0
    Top = 48
    Width = 1264
    Height = 483
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter3: TSplitter
      Left = 828
      Top = 0
      Width = 5
      Height = 483
    end
    object PnModo: TPanel
      Left = 0
      Top = 0
      Width = 828
      Height = 483
      Align = alLeft
      TabOrder = 0
      object PnGrids: TPanel
        Left = 1
        Top = 1
        Width = 826
        Height = 481
        Align = alClient
        TabOrder = 0
        object Splitter2: TSplitter
          Left = 1
          Top = 241
          Width = 824
          Height = 5
          Cursor = crVSplit
          Align = alTop
        end
        object PnConta: TPanel
          Left = 1
          Top = 246
          Width = 824
          Height = 234
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object DBGConra: TDBGrid
            Left = 0
            Top = 0
            Width = 280
            Height = 234
            Align = alLeft
            DataSource = DsEXcCtaPag
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o da conta'
                Visible = True
              end>
          end
          object DBGLanctos: TDBGrid
            Left = 280
            Top = 0
            Width = 544
            Height = 234
            Align = alClient
            DataSource = DsEXmDspDevIts
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Data'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SiglaOri'
                Title.Caption = 'Sigla Ori'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorOri'
                Title.Caption = 'Valor Ori'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_EXcIdFunc'
                Title.Caption = 'Colaborador'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ano'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Mes'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodInMob'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CtrlInMob'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'EXcCtaPag'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'EXcIdFunc'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'EXcMdoPag'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SiglaDst'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorDst'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AnoMes'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CotacDst'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CotacOri'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SiglaInt'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorInt'
                Visible = True
              end>
          end
        end
        object PnFiltros: TPanel
          Left = 1
          Top = 1
          Width = 824
          Height = 240
          Align = alTop
          TabOrder = 1
          object Splitter1: TSplitter
            Left = 1
            Top = 101
            Width = 822
            Height = 5
            Cursor = crVSplit
            Align = alTop
            ExplicitTop = 105
          end
          object DBGDocs: TDBGrid
            Left = 1
            Top = 106
            Width = 822
            Height = 133
            Align = alClient
            DataSource = DsEXmDspDevFts
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CtrlFoto'
                Title.Caption = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data/hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_EXcStCmprv'
                Title.Caption = 'Situa'#231#227'o do doc.'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NomeArq'
                Title.Caption = 'Nome do arquivo'
                Width = 491
                Visible = True
              end>
          end
          object Panel3: TPanel
            Left = 1
            Top = 1
            Width = 822
            Height = 100
            Align = alTop
            TabOrder = 1
            object DBGPeriodos: TDBGrid
              Left = 337
              Top = 1
              Width = 148
              Height = 98
              Align = alLeft
              DataSource = DsPeriodos
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'AnoMes'
                  Width = 109
                  Visible = True
                end>
            end
            object DBGModos: TDBGrid
              Left = 485
              Top = 1
              Width = 336
              Height = 98
              Align = alClient
              DataSource = DsEXcMdoPag
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 233
                  Visible = True
                end>
            end
            object Panel2: TPanel
              Left = 1
              Top = 1
              Width = 336
              Height = 98
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label1: TLabel
                Left = 8
                Top = 4
                Width = 60
                Height = 13
                Caption = 'Colaborador:'
              end
              object SbDecrAnoMes: TSpeedButton
                Left = 8
                Top = 68
                Width = 21
                Height = 21
                OnClick = SbDecrAnoMesClick
              end
              object SbIncAnoMes: TSpeedButton
                Left = 112
                Top = 68
                Width = 21
                Height = 21
                OnClick = SbIncAnoMesClick
              end
              object CBEXcIDFunc: TdmkDBLookupComboBox
                Left = 64
                Top = 20
                Width = 265
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEXcIDFunc
                TabOrder = 0
                dmkEditCB = EdEXcIDFunc
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdAnoMes: TdmkEdit
                Left = 32
                Top = 68
                Width = 78
                Height = 21
                Alignment = taCenter
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '2000-01'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '2000-01'
                ValWarn = False
                OnRedefinido = EdAnoMesRedefinido
              end
              object CkAnoMes: TCheckBox
                Left = 8
                Top = 48
                Width = 97
                Height = 17
                Caption = 'Ano - M'#234's:'
                Checked = True
                State = cbChecked
                TabOrder = 2
              end
            end
          end
        end
      end
    end
    object PnFotoENome: TPanel
      Left = 833
      Top = 0
      Width = 431
      Height = 483
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object ImgFoto: TImage
        Left = 0
        Top = 13
        Width = 431
        Height = 470
        Align = alClient
        Picture.Data = {
          0A544A504547496D61676577020000FFD8FFE000104A46494600010101006000
          600000FFDB004300020101020101020202020202020203050303030303060404
          0305070607070706070708090B0908080A0807070A0D0A0A0B0C0C0C0C07090E
          0F0D0C0E0B0C0C0CFFDB004301020202030303060303060C0807080C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0CFFC00011080002000203012200021101031101
          FFC4001F0000010501010101010100000000000000000102030405060708090A
          0BFFC400B5100002010303020403050504040000017D01020300041105122131
          410613516107227114328191A1082342B1C11552D1F02433627282090A161718
          191A25262728292A3435363738393A434445464748494A535455565758595A63
          6465666768696A737475767778797A838485868788898A92939495969798999A
          A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
          D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
          01010101010101010000000000000102030405060708090A0BFFC400B5110002
          0102040403040705040400010277000102031104052131061241510761711322
          328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
          292A35363738393A434445464748494A535455565758595A636465666768696A
          737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
          A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
          E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00FE7F
          E8A28A00FFD9}
        Proportional = True
        ExplicitLeft = 76
        ExplicitWidth = 359
        ExplicitHeight = 454
      end
      object LaNomeFoto: TLabel
        Left = 0
        Top = 0
        Width = 431
        Height = 13
        Align = alTop
        Caption = '...'
        ExplicitWidth = 9
      end
    end
  end
  object EdEXcIDFunc: TdmkEditCB
    Left = 12
    Top = 72
    Width = 56
    Height = 21
    Alignment = taRightJustify
    TabOrder = 4
    FormatType = dmktfInteger
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ValMin = '-2147483647'
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    Texto = '0'
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = 0
    ValWarn = False
    OnRedefinido = EdEXcIDFuncRedefinido
    DBLookupComboBox = CBEXcIDFunc
    IgnoraDBLookupComboBox = False
    AutoSetIfOnlyOneReg = setregOnlyManual
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrPeriodos: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrPeriodosBeforeClose
    AfterScroll = QrPeriodosAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT AnoMes, Ano, Mes'
      'FROM exmdspdevlct'
      'ORDER BY Ano DESC, Mes DESC')
    Left = 384
    Top = 284
    object QrPeriodosAnoMes: TWideStringField
      FieldName = 'AnoMes'
      Size = 7
    end
    object QrPeriodosAno: TIntegerField
      FieldName = 'Ano'
    end
    object QrPeriodosMes: TIntegerField
      FieldName = 'Mes'
    end
  end
  object DsPeriodos: TDataSource
    DataSet = QrPeriodos
    Left = 384
    Top = 340
  end
  object QrEXcMdoPag: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrEXcMdoPagBeforeClose
    AfterScroll = QrEXcMdoPagAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT AnoMes, Ano, Mes'
      'FROM exmdspdevlct'
      'ORDER BY Ano DESC, Mes DESC')
    Left = 488
    Top = 292
    object QrEXcMdoPagCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEXcMdoPagNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEXcMdoPag: TDataSource
    DataSet = QrEXcMdoPag
    Left = 488
    Top = 348
  end
  object QrEXcCtaPag: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrEXcCtaPagBeforeClose
    AfterScroll = QrEXcCtaPagAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT AnoMes, Ano, Mes'
      'FROM exmdspdevlct'
      'ORDER BY Ano DESC, Mes DESC')
    Left = 588
    Top = 292
    object QrEXcCtaPagCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEXcCtaPagNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEXcCtaPag: TDataSource
    DataSet = QrEXcCtaPag
    Left = 588
    Top = 348
  end
  object QrEXmDspDevIts: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrEXmDspDevItsBeforeClose
    AfterScroll = QrEXmDspDevItsAfterScroll
    SQL.Strings = (
      'SELECT its.*'
      'FROM exmdspdevits its '
      'WHERE its.EXcIdFunc=9032'
      'AND its.Ano=2021 '
      'AND its.Mes=3 '
      'AND EXcMdoPag>0 '
      'AND EXcCtaPag>0')
    Left = 682
    Top = 293
    object QrEXmDspDevItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEXmDspDevItsCodInMob: TIntegerField
      FieldName = 'CodInMob'
      Required = True
    end
    object QrEXmDspDevItsCtrlInMob: TIntegerField
      FieldName = 'CtrlInMob'
      Required = True
    end
    object QrEXmDspDevItsAnoMes: TWideStringField
      FieldName = 'AnoMes'
      Required = True
      Size = 7
    end
    object QrEXmDspDevItsAno: TIntegerField
      FieldName = 'Ano'
      Required = True
    end
    object QrEXmDspDevItsMes: TIntegerField
      FieldName = 'Mes'
      Required = True
    end
    object QrEXmDspDevItsEXcIdFunc: TIntegerField
      FieldName = 'EXcIdFunc'
      Required = True
    end
    object QrEXmDspDevItsData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEXmDspDevItsHora: TTimeField
      FieldName = 'Hora'
      Required = True
      DisplayFormat = 'hh:nn:ss'
    end
    object QrEXmDspDevItsEXcCtaPag: TIntegerField
      FieldName = 'EXcCtaPag'
      Required = True
    end
    object QrEXmDspDevItsEXcMdoPag: TIntegerField
      FieldName = 'EXcMdoPag'
      Required = True
    end
    object QrEXmDspDevItsSiglaOri: TWideStringField
      FieldName = 'SiglaOri'
      Required = True
      Size = 10
    end
    object QrEXmDspDevItsCotacOri: TFloatField
      FieldName = 'CotacOri'
      Required = True
    end
    object QrEXmDspDevItsValorOri: TFloatField
      FieldName = 'ValorOri'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEXmDspDevItsSiglaInt: TWideStringField
      FieldName = 'SiglaInt'
      Required = True
      Size = 10
    end
    object QrEXmDspDevItsValorInt: TFloatField
      FieldName = 'ValorInt'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEXmDspDevItsSiglaDst: TWideStringField
      FieldName = 'SiglaDst'
      Required = True
      Size = 10
    end
    object QrEXmDspDevItsCotacDst: TFloatField
      FieldName = 'CotacDst'
      Required = True
    end
    object QrEXmDspDevItsValorDst: TFloatField
      FieldName = 'ValorDst'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEXmDspDevItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEXmDspDevItsNO_EXcIdFunc: TWideStringField
      FieldName = 'NO_EXcIdFunc'
      Size = 60
    end
    object QrEXmDspDevItsSigla_IdFunc: TWideStringField
      FieldName = 'Sigla_IdFunc'
      Size = 15
    end
  end
  object DsEXmDspDevIts: TDataSource
    DataSet = QrEXmDspDevIts
    Left = 684
    Top = 344
  end
  object QrEXmDspDevFts: TMySQLQuery
    Database = Dmod.ZZDB
    AfterScroll = QrEXmDspDevFtsAfterScroll
    SQL.Strings = (
      'SELECT scm.Nome NO_EXcStCmprv, fts.* '
      'FROM exmdspdevfts fts  '
      'LEFT JOIN excstcmprv scm ON scm.Codigo=fts.EXcStCmprv '
      'WHERE fts.Codigo>0 '
      'AND fts.CodInMob=12 ')
    Left = 788
    Top = 296
    object QrEXmDspDevFtsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEXmDspDevFtsCodInMob: TIntegerField
      FieldName = 'CodInMob'
      Required = True
    end
    object QrEXmDspDevFtsCtrlInMob: TIntegerField
      FieldName = 'CtrlInMob'
      Required = True
    end
    object QrEXmDspDevFtsIDTabela: TIntegerField
      FieldName = 'IDTabela'
      Required = True
    end
    object QrEXmDspDevFtsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrEXmDspDevFtsCtrlFoto: TIntegerField
      FieldName = 'CtrlFoto'
      Required = True
    end
    object QrEXmDspDevFtsEXcStCmprv: TIntegerField
      FieldName = 'EXcStCmprv'
      Required = True
    end
    object QrEXmDspDevFtsNomeArq: TWideStringField
      FieldName = 'NomeArq'
      Size = 255
    end
    object QrEXmDspDevFtsNoArqSvr: TWideStringField
      FieldName = 'NoArqSvr'
      Size = 255
    end
    object QrEXmDspDevFtsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEXmDspDevFtsNO_EXcStCmprv: TWideStringField
      FieldName = 'NO_EXcStCmprv'
      Size = 60
    end
    object QrEXmDspDevFtsMobInstlSeq: TIntegerField
      FieldName = 'MobInstlSeq'
    end
  end
  object DsEXmDspDevFts: TDataSource
    DataSet = QrEXmDspDevFts
    Left = 788
    Top = 352
  end
  object QrEXmDspDevBmp: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 584
    Top = 413
    object QrEXmDspDevBmpTxtBmp: TWideMemoField
      FieldName = 'TxtBmp'
      BlobType = ftWideMemo
    end
  end
  object QrEXcIDFunc: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrPeriodosBeforeClose
    AfterScroll = QrPeriodosAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM excidfunc'
      'ORDER BY Nome')
    Left = 56
    Top = 180
    object QrEXcIDFuncCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEXcIDFuncNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
      Transliterate = False
    end
  end
  object DsEXcIDFunc: TDataSource
    DataSet = QrEXcIDFunc
    Left = 56
    Top = 236
  end
end
