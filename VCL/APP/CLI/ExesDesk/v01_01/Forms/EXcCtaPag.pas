unit EXcCtaPag;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkCheckBox, Vcl.Menus, dmkDBLookupComboBox, dmkEditCB;

type
  TFmEXcCtaPag = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEXcCtaPag: TMySQLQuery;
    QrEXcCtaPagCodigo: TIntegerField;
    QrEXcCtaPagNome: TWideStringField;
    DsEXcCtaPag: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    DBCheckBox2: TDBCheckBox;
    CkAtivo: TdmkCheckBox;
    QrEXcCtaPagOrdem: TIntegerField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    EdOrdem: TdmkEdit;
    Label4: TLabel;
    QrEXcCtaPagAtivo: TSmallintField;
    PMAltera: TPopupMenu;
    Dados1: TMenuItem;
    Imagem1: TMenuItem;
    QrEXcCtaPagImgB64: TWideMemoField;
    QrEXcCtaPagImgPath: TWideStringField;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label6: TLabel;
    QrEXcCtaPagNomGrupo: TWideStringField;
    EdNomGrupo: TdmkEdit;
    Label8: TLabel;
    ImagemMobilepng1: TMenuItem;
    Panel6: TPanel;
    PnImg: TPanel;
    ImgImgB64: TImage;
    Panel7: TPanel;
    ImgPngB64: TImage;
    QrEXcCtaPagPngB64: TWideMemoField;
    QrEXcGruCta: TMySQLQuery;
    QrEXcGruCtaCodigo: TIntegerField;
    QrEXcGruCtaNome: TWideStringField;
    DsEXcGruCta: TDataSource;
    Label10: TLabel;
    EdEXcGruCta: TdmkEditCB;
    CBEXcGruCta: TdmkDBLookupComboBox;
    SbEXcGruCta: TSpeedButton;
    QrEXcCtaPagNO_EXcGruCta: TWideStringField;
    DBEdit3: TDBEdit;
    QrEXcCtaPagEXcGruCta: TIntegerField;
    Label12: TLabel;
    DBEdit4: TDBEdit;
    Label11: TLabel;
    EdNoEN: TdmkEdit;
    EdNomGruEN: TdmkEdit;
    Label13: TLabel;
    Label14: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    Label15: TLabel;
    QrEXcCtaPagNoEN: TWideStringField;
    QrEXcCtaPagNomGruEN: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEXcCtaPagAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEXcCtaPagBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure Dados1Click(Sender: TObject);
    procedure Imagem1Click(Sender: TObject);
    procedure QrEXcCtaPagAfterScroll(DataSet: TDataSet);
    procedure ImagemMobilepng1Click(Sender: TObject);
    procedure SbEXcGruCtaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEXcCtaPag: TFmEXcCtaPag;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkImg, UnExesD_PF, MyGlyfs, UnExesD_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEXcCtaPag.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEXcCtaPag.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEXcCtaPagCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEXcCtaPag.DefParams;
begin
  VAR_GOTOTABELA := 'excctapag';
  VAR_GOTOMYSQLTABLE := QrEXcCtaPag;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ecp.*, egc.Nome NO_EXcGruCta ');
  VAR_SQLx.Add('FROM excctapag ecp');
  VAR_SQLx.Add('LEFT JOIN excgructa egc ON egc.Codigo=ecp.EXcGruCta');
  VAR_SQLx.Add('WHERE ecp.Codigo <> 0 ');
  //
  VAR_SQL1.Add('AND ecp.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ecp.Nome Like :P0');
  //
end;

procedure TFmEXcCtaPag.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEXcCtaPag.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEXcCtaPag.Dados1Click(Sender: TObject);
begin
  if (QrEXcCtaPag.State <> dsInactive) and (QrEXcCtaPag.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEXcCtaPag, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'excctapag');
  end;
end;

procedure TFmEXcCtaPag.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEXcCtaPag.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEXcCtaPag.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEXcCtaPag.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEXcCtaPag.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEXcCtaPag.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEXcCtaPag.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmEXcCtaPag.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEXcCtaPagCodigo.Value;
  Close;
end;

procedure TFmEXcCtaPag.BtConfirmaClick(Sender: TObject);
var
  Nome, NomGrupo, NoEN, NomGruEN: String;
  Codigo, Ordem, EXcGruCta: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  NomGrupo       := EdNomGrupo.ValueVariant;
  NoEN           := EdNoEN.ValueVariant;
  NomGruEN       := EdNomGruEN.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  EXcGruCta      := EdEXcGruCta.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if MyObjects.FIC(EXcGruCta = 0, EdNome, 'Defina a conta cont�bil!') then Exit;
  //

  //
  Codigo := UMyMod.BPGS1I32('excctapag', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'excctapag', False, [
  'Nome', 'NomGrupo', 'Ordem',
  'EXcGruCta', 'NoEN', 'NomGruEN'], [
  'Codigo'], [
  Nome, NomGrupo, Ordem,
  EXcGruCta, NoEN, NomGruEN], [
  Codigo], True) then
  begin
    if UnDmkDAC_PF.AtivaDesativaRegistroIdx1(Dmod.MyDB, 'excctapag', 'Codigo',
    'Ativo', Codigo, CkAtivo.Checked) then
    begin
      ImgTipo.SQLType := stLok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
      GOTOy.BotoesSb(ImgTipo.SQLType);
      //
      LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmEXcCtaPag.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'excctapag', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEXcCtaPag.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEXcCtaPag, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'excctapag');
end;

procedure TFmEXcCtaPag.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PnImg.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrEXcGruCta, Dmod.MyDB);
end;

procedure TFmEXcCtaPag.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEXcCtaPagCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcCtaPag.SbEXcGruCtaClick(Sender: TObject);
begin
  ExesD_Jan.MostraFormEXcGruCta(0);
  //
  UMyMod.AbreQuery(QrEXcGruCta, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdEXcGruCta, CBEXcGruCta, QrEXcGruCta, VAR_CADASTRO, 'Codigo');
  EdEXcGruCta.SetFocus;
end;

procedure TFmEXcCtaPag.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEXcCtaPag.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEXcCtaPagCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcCtaPag.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEXcCtaPag.QrEXcCtaPagAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEXcCtaPag.QrEXcCtaPagAfterScroll(DataSet: TDataSet);
var
  Bmp2: TBitmap;
  ImgB64: String;
begin
  ImgB64 := QrExcCtaPagImgB64.Value;
  if ImgB64 <> EmptyStr then
  begin
    Bmp2 := TBitmap.Create;
    try
      Bmp2 := DmkImg.BitmapFromBase64(ImgB64);
      ImgImgB64.Picture.Bitmap.Assign(Bmp2);
      ImgImgB64.Visible := True;
    finally
      Bmp2.Free;
    end;
  end else
    ImgImgB64.Visible := False;
    //
  FmMyGlyfs.CarregaPNGDoBD(ImgPngB64, QrEXcCtaPagPngB64.Value);
end;

procedure TFmEXcCtaPag.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEXcCtaPag.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEXcCtaPagCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'excctapag', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEXcCtaPag.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEXcCtaPag.Imagem1Click(Sender: TObject);
{
const
  Titulo = 'Defini��o Imagem de bot�o';
var
  IniDir, Arquivo, Filtro, ImgB64, ImgPath: String;
  Bmp1, Bmp2: TBitmap;
  Codigo: Integer;
begin
  IniDir  := 'C:\Dermatek\';
  Arquivo := '';
  Filtro  := 'Arquivos Bitmap|*.bmp;Arquivos JPEG|*.jpeg;Arquivos JPG|*.jpg;Arquivos PNG|*.png';
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, Titulo, Filtro, [], Arquivo) then
  begin
    if FileExists(Arquivo) then
    begin
      Screen.Cursor := crHourGlass;
      try
        Bmp1 := TBitmap.Create;
        try
          Bmp1.LoadFromFile(Arquivo);
          //ImgB64 := DmkImg.Base64FromBitmap2(Bitmap);
          ImgB64 := DmkImg.Base64FromBitmap(Bmp1);
          ImgPath := Arquivo;
          Codigo := QrEXcCtaPagCodigo.Value;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'excctapag', False, [
          'ImgB64', 'ImgPath'], [
          'Codigo'], [
          ImgB64, ImgPath], [
          Codigo], True) then
          begin
            LocCod(Codigo, Codigo);
          end;
        finally
          Bmp1.Free;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;
}
var
  Codigo: Integer;
begin
  Codigo := QrEXcCtaPagCodigo.Value;
  if ExesD_PF.CarregaImagemRegistro(Self, 'excctapag', 'ImgB64', 'ImgPath',
  Codigo, 192, 192) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEXcCtaPag.ImagemMobilepng1Click(Sender: TObject);
var
  Codigo: Integer;
  PngB64, PngPath: String;
begin
  Codigo := QrEXcCtaPagCodigo.Value;
  if FmMyGlyfs.SalvaPNGDeArquivoNoBD(Self, 192, 192, PngB64, PngPath) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd,  'excctapag', False, [
    'PngB64', 'PngPath'], [
    'Codigo'], [
     PngB64, PngPath], [
     Codigo], True) then
     begin
       LocCod(Codigo, Codigo);
     end;
  end;
end;

procedure TFmEXcCtaPag.QrEXcCtaPagBeforeOpen(DataSet: TDataSet);
begin
  QrEXcCtaPagCodigo.DisplayFormat := FFormatFloat;
end;

end.

