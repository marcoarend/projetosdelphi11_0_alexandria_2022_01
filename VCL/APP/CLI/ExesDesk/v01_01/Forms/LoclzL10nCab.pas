unit LoclzL10nCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmLoclzL10nCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrLoclzL10nCab: TMySQLQuery;
    DsLoclzL10nCab: TDataSource;
    QrLoclzL10nIds: TMySQLQuery;
    DsLoclzL10nIds: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrLoclzL10nCabCodigo: TIntegerField;
    QrLoclzL10nCabNome: TWideStringField;
    QrLoclzL10nIdsCodigo: TIntegerField;
    QrLoclzL10nIdsLocID: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrLoclzL10nCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrLoclzL10nCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrLoclzL10nCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrLoclzL10nCabBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraLoclzL10nIds(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenLoclzL10nIds(LocID: String);

  end;

var
  FmLoclzL10nCab: TFmLoclzL10nCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, LoclzL10nIds;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmLoclzL10nCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmLoclzL10nCab.MostraLoclzL10nIds(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmLoclzL10nIds, FmLoclzL10nIds, afmoNegarComAviso) then
  begin
    FmLoclzL10nIds.ImgTipo.SQLType := SQLType;
    //FmLoclzL10nIds.FQrCab := QrLoclzL10nCab;
    //FmLoclzL10nIds.FDsCab := DsLoclzL10nCab;
    FmLoclzL10nIds.FQrIts := QrLoclzL10nIds;
    FmLoclzL10nIds.FCodigo := QrLoclzL10nCabCodigo.Value;
    FmLoclzL10nIds.EdCodigo.ValueVariant := QrLoclzL10nCabCodigo.Value;
    FmLoclzL10nIds.EdNome.ValueVariant := QrLoclzL10nCabNome.Value;
    if SQLType = stIns then
    begin
      //
    end else
    begin
      FmLoclzL10nIds.FOldLocID            := QrLoclzL10nIdsLocID.Value;
      FmLoclzL10nIds.EdLocID.ValueVariant := QrLoclzL10nIdsLocID.Value;
    end;
    FmLoclzL10nIds.ShowModal;
    FmLoclzL10nIds.Destroy;
  end;
end;

procedure TFmLoclzL10nCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrLoclzL10nCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrLoclzL10nCab, QrLoclzL10nIds);
end;

procedure TFmLoclzL10nCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrLoclzL10nCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrLoclzL10nIds);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrLoclzL10nIds);
end;

procedure TFmLoclzL10nCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrLoclzL10nCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmLoclzL10nCab.DefParams;
begin
  VAR_GOTOTABELA := 'loclzl10ncab ';
  VAR_GOTOMYSQLTABLE := QrLoclzL10nCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM loclzl10ncab ');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmLoclzL10nCab.ItsAltera1Click(Sender: TObject);
begin
  MostraLoclzL10nIds(stUpd);
end;

procedure TFmLoclzL10nCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmLoclzL10nCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmLoclzL10nCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmLoclzL10nCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'LoclzL10nIds', 'Controle', QrLoclzL10nIdsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrLoclzL10nIds,
      QrLoclzL10nIdsControle, QrLoclzL10nIdsControle.Value);
    ReopenLoclzL10nIds(LocID);
  end;
}
end;

procedure TFmLoclzL10nCab.ReopenLoclzL10nIds(LocID: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoclzL10nIds, Dmod.MyDB, [
  'SELECT * ',
  'FROM loclzl10nids ',
  'WHERE Codigo=' + Geral.FF0(QrLoclzL10nCabCodigo.Value),
  '']);
  //
  QrLoclzL10nIds.Locate('LocID', LocID, []);
end;


procedure TFmLoclzL10nCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmLoclzL10nCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmLoclzL10nCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmLoclzL10nCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmLoclzL10nCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmLoclzL10nCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLoclzL10nCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrLoclzL10nCabCodigo.Value;
  Close;
end;

procedure TFmLoclzL10nCab.ItsInclui1Click(Sender: TObject);
begin
  MostraLoclzL10nIds(stIns);
end;

procedure TFmLoclzL10nCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrLoclzL10nCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'loclzl10ncab ');
end;

procedure TFmLoclzL10nCab.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('loclzl10ncab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loclzl10ncab', False, [
  'Nome'], [
  'Codigo'], [
  Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmLoclzL10nCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'loclzl10ncab ', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'loclzl10ncab ', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmLoclzL10nCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmLoclzL10nCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmLoclzL10nCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmLoclzL10nCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrLoclzL10nCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmLoclzL10nCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmLoclzL10nCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrLoclzL10nCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmLoclzL10nCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmLoclzL10nCab.QrLoclzL10nCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmLoclzL10nCab.QrLoclzL10nCabAfterScroll(DataSet: TDataSet);
begin
  ReopenLoclzL10nIds('');
end;

procedure TFmLoclzL10nCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrLoclzL10nCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmLoclzL10nCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrLoclzL10nCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'loclzl10ncab ', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmLoclzL10nCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLoclzL10nCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrLoclzL10nCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'loclzl10ncab ');
end;

procedure TFmLoclzL10nCab.QrLoclzL10nCabBeforeClose(
  DataSet: TDataSet);
begin
  QrLoclzL10nIds.Close;
end;

procedure TFmLoclzL10nCab.QrLoclzL10nCabBeforeOpen(DataSet: TDataSet);
begin
  QrLoclzL10nCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

