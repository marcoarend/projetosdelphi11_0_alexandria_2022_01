unit UnAppEnums;

interface

uses  System.SysUtils, System.Types;

//const

type

  TUndCtrleProd = (ucprdIndefinido=0, ucprdNaoAplic=1, ucprdMinutos=2,
                   ucprdDias=3, ucprdPecas=4);



  TUnAppEnums = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
  end;
var
  AppEnums: TUnAppEnums;

const
  //
  CO_TXT_ucprdIndefinido                  = 'Indefinido';
  CO_TXT_ucprdNaoAplic                    = 'N�o aplic�vel';
  CO_TXT_ucprdMinutos                     = 'Minutos';
  CO_TXT_ucprdDias                        = 'Dias';
  CO_TXT_ucprdPecas                       = 'Pe�as';
  MaxUndCtrleProd = Integer(High(TUndCtrleProd));
  sUndCtrleProd: array[0..MaxUndCtrleProd] of string = (
    CO_TXT_ucprdIndefinido     , // 0
    CO_TXT_ucprdNaoAplic       , // 1
    CO_TXT_ucprdMinutos        , // 2
    CO_TXT_ucprdDias           ,  // 3
    CO_TXT_ucprdPecas            // 4
  );

  CO_TXT_codhistMercadorias            = 'Venda de Mercadorias';
  CO_TXT_codhistServicos               = 'Venda de Servi�os';
  CO_TXT_codhistFrete                  = 'Frete';
  CO_TXT_codhistOutros                 = 'Outros';

  MaxCodHist = 3;//Integer(High(TManejoLca));
  sCodHistTextos: array[0..MaxCodHist] of string  = (
  CO_TXT_codhistMercadorias            ,
  CO_TXT_codhistServicos               ,
  CO_TXT_codhistFrete                  ,
  CO_TXT_codhistOutros                 //,
  );

  CO_SIGLA_codhistMercadorias            = 'VM';
  CO_SIGLA_codhistServicos               = 'SV';
  CO_SIGLA_codhistFrete                  = 'FR';
  CO_SIGLA_codhistOutros                 = 'OU';

  sCodHistSiglas: array[0..MaxCodHist] of string  = (
  CO_SIGLA_codhistMercadorias  ,
  CO_SIGLA_codhistServicos               ,
  CO_SIGLA_codhistFrete                  ,
  CO_SIGLA_codhistOutros                 //,
  );




implementation


end.
