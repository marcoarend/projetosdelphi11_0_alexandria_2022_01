unit AppListas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, UnMyLinguas, UnInternalConsts, dmkGeral, DB,
  mySQLDbTables, UnDmkProcFunc, UnDmkEnums, UnAppEnums;

const
  //
  MaxAplicacaoChekLstCab = 0;
  sListaAplicacaoChekLstCab: array[0..MaxAplicacaoChekLstCab] of String = (
  '? ? ?');
const

  CO_CONTROLE_ESTOQUE_GRAGRUY = '-999999999'; // Nenhum por enquanto!

  CO_RecImpApresenta_000_Txt = 'Pesos';
  CO_RecImpApresenta_001_Txt = 'Custos';
  CO_RecImpApresenta_002_Txt = 'Pre�os';
(*
  CO_RecImpApresenta_003_Txt = 'TFL 1';
  CO_RecImpApresenta_004_Txt = '2';
  CO_RecImpApresenta_005_Txt = '3';
  CO_RecImpApresenta_006_Txt = 'BMZ 1';
  CO_RecImpApresenta_007_Txt = 'Pes 1';
  CO_RecImpApresenta_008_Txt = 'Custos';
  CO_RecImpApresenta_009_Txt = 'BMZ 1>Pes 1';
  CO_RecImpApresenta_010_Txt = 'COT 1';
  CO_RecImpApresenta_011_Txt = 'BMZ 2 (Mini)';
  CO_RecImpApresenta_012_Txt = 'IDEAL 1';
*)

  MaxRecImpApresenta = 2;//12;
  sListaRecImpApresenta: array[0..MaxRecImpApresenta] of string = (
                                                   CO_RecImpApresenta_000_Txt,
                                                   CO_RecImpApresenta_001_Txt,
                                                   CO_RecImpApresenta_002_Txt(*,
                                                   CO_RecImpApresenta_003_Txt,
                                                   CO_RecImpApresenta_004_Txt,
                                                   CO_RecImpApresenta_005_Txt,
                                                   CO_RecImpApresenta_006_Txt,
                                                   CO_RecImpApresenta_007_Txt,
                                                   CO_RecImpApresenta_008_Txt,
                                                   CO_RecImpApresenta_009_Txt,
                                                   CO_RecImpApresenta_010_Txt,
                                                   CO_RecImpApresenta_011_Txt,
                                                   CO_RecImpApresenta_012_Txt*)
                                                    );
  CO_TipoCadPQ_000_Txt_NaoDefinido     =  'N�o definido';
  CO_TipoCadPQ_001_Txt_AmostraDePQ     =  'Amostra';
  CO_TipoCadPQ_002_Txt_ProdutoQuimico  =  'Insumo';
  CO_TipoCadPQ_003_Txt_Texto           =  'Texto';
  CO_TipoCadPQ_004_Txt_Material        =  'Material';
  CO_TipoCadPQ_005_Txt_Outros          =  'Outros';
  CO_TipoCadPQ_006_Txt_Servico         =  'Servi�o';
  CO_TipoCadPQ_007_Txt_MatUsoImediato  =  'Material de uso imediato';
  CO_TipoCadPQ_008_Txt_Embalagem       =  'Embalagem';
  CO_TipoCadPQ_009_Txt_Indefinido      =  '-Indefinido-';
  CO_TipoCadPQ_010_Txt_InsumoModif     =  'Insumo modificado';
  CO_TipoCadPQ_011_Txt_     =  '????';
  CO_TipoCadPQ_000_Cod_NaoDefinido     =  0;
  CO_TipoCadPQ_001_Cod_AmostraDePQ     =  1;
  CO_TipoCadPQ_002_Cod_ProdutoQuimico  =  2;
  CO_TipoCadPQ_003_Cod_Texto           =  3;
  CO_TipoCadPQ_004_Cod_Material        =  4;
  CO_TipoCadPQ_005_Cod_Outros          =  5;
  CO_TipoCadPQ_006_Cod_Servico         =  6;
  CO_TipoCadPQ_007_Cod_MatUsoImediato  =  7;
  CO_TipoCadPQ_008_Cod_Embalagem       =  8;
  CO_TipoCadPQ_009_Cod_Indefinido      =  9;
  CO_TipoCadPQ_010_Cod_InsumoModif     =  10;
  CO_TipoCadPQ_011_Cod_     =  11;
  MaxTipoCadPQ = 10;
  sListaTipoCadPQ: array[0..MaxTipoCadPQ] of string = (
                                       CO_TipoCadPQ_000_Txt_NaoDefinido      ,
                                       CO_TipoCadPQ_001_Txt_AmostraDePQ      ,
                                       CO_TipoCadPQ_002_Txt_ProdutoQuimico   ,
                                       CO_TipoCadPQ_003_Txt_Texto            ,
                                       CO_TipoCadPQ_004_Txt_Material         ,
                                       CO_TipoCadPQ_005_Txt_Outros           ,
                                       CO_TipoCadPQ_006_Txt_Servico          ,
                                       CO_TipoCadPQ_007_Txt_MatUsoImediato   ,
                                       CO_TipoCadPQ_008_Txt_Embalagem        ,
                                       CO_TipoCadPQ_009_Txt_Indefinido       ,
                                       CO_TipoCadPQ_010_Txt_InsumoModif
                                       //CO_TipoCadPQ_011_Txt_
                                  );
  // Tabelas GraGruY
  CO_GraGruY_1024_TXCadNat = 1024;
  (*CO_GraGruY_1195_TXNatPDA = 1195;
  CO_GraGruY_1365_TXProCal = 1365;
  CO_GraGruY_1536_TXCouCal = 1536;
  CO_GraGruY_1621_TXCouDTA = 1621;
  CO_GraGruY_1707_TXProCur = 1707;
  CO_GraGruY_1877_TXCouCur = 1877;*)
  CO_GraGruY_2048_TXCadInd = 2048;
  (*CO_GraGruY_3072_TXRibCla = 3072;*)
  CO_GraGruY_4096_TXCadInt = 4096;
  (*CO_GraGruY_5120_TXWetEnd = 5120;*)
  CO_GraGruY_6144_TXCadFcc = 6144;
  (*CO_GraGruY_0512_TXSubPrd = 0512;
  CO_GraGruY_0683_TXPSPPro = 0683;
  CO_GraGruY_0853_TXPSPEnd = 0853;*)
  //
  CO_TXT_GraGruY_1024_TXCadNat = 'Mat�ria-prima';
  (*CO_TXT_GraGruY_1195_TXNatPDA = 'Mat�ria-prima PDA';
  CO_TXT_GraGruY_1365_TXProCal = 'Couro em Caleiro';
  CO_TXT_GraGruY_1536_TXCouCal = 'Couro Caleirado';
  CO_TXT_GraGruY_1621_TXCouDTA = 'Couro Caleirado DTA';
  CO_TXT_GraGruY_1707_TXProCur = 'Couro em Curtimento';
  CO_TXT_GraGruY_1877_TXCouCur = 'Couro Curtido';*)
  CO_TXT_GraGruY_2048_TXCadInd = 'Produto em confec��o';
  (*CO_TXT_GraGruY_3072_TXRibCla = 'Artigo de Ribeira Classificado';*)
  CO_TXT_GraGruY_4096_TXCadInt = 'Produto intermedi�rio';
(*
  CO_TXT_GraGruY_5120_TXWetEnd = 'Produto Semi em Processo';
*)
  CO_TXT_GraGruY_6144_TXCadFcc = 'Produto Acabado';
(*
  CO_TXT_GraGruY_0512_TXSubPrd = 'Subproduto In Natura';
  CO_TXT_GraGruY_0683_TXPSPPro = 'Subproduto em Processo';
  CO_TXT_GraGruY_0853_TXPSPEnd = 'Subproduto Processado';*)
  MaxGraGruY_TX = 3; //14;
  sListaGraGruY_TX: array[0..MaxGraGruY_TX] of string = (
    CO_TXT_GraGruY_1024_TXCadNat,
    (*CO_TXT_GraGruY_1195_TXNatPDA,
    CO_TXT_GraGruY_1365_TXProCal,
    CO_TXT_GraGruY_1536_TXCouCal,
    CO_TXT_GraGruY_1621_TXCouDTA,
    CO_TXT_GraGruY_1707_TXProCur,
    CO_TXT_GraGruY_1877_TXCouCur,*)
    CO_TXT_GraGruY_2048_TXCadInd,
    (*CO_TXT_GraGruY_3072_TXRibCla,*)
    CO_TXT_GraGruY_4096_TXCadInt,
    (*CO_TXT_GraGruY_5120_TXWetEnd,*)
    CO_TXT_GraGruY_6144_TXCadFcc
    (*CO_TXT_GraGruY_0512_TXSubPrd,
    CO_TXT_GraGruY_0683_TXPSPPro,
    CO_TXT_GraGruY_0853_TXPSPEnd*));
  sCodeGraGruY_TX: array[0..MaxGraGruY_TX] of integer = (
    CO_GraGruY_1024_TXCadNat,
    (*CO_GraGruY_1195_TXNatPDA,
    CO_GraGruY_1365_TXProCal,
    CO_GraGruY_1536_TXCouCal,
    CO_GraGruY_1621_TXCouDTA,
    CO_GraGruY_1707_TXProCur,
    CO_GraGruY_1877_TXCouCur,*)
    CO_GraGruY_2048_TXCadInd,
    (*CO_GraGruY_3072_TXRibCla,*)
    CO_GraGruY_4096_TXCadInt,
    (*CO_GraGruY_5120_TXWetEnd,*)
    CO_GraGruY_6144_TXCadFcc
    (*CO_GraGruY_0512_TXSubPrd,
    CO_GraGruY_0683_TXPSPPro,
    CO_GraGruY_0853_TXPSPEnd*)
    );
  //
  CO_GraGruY_ALL_TX = '1024, 2048, 4096, 5120, 6144';
  CO_GraGruY_PlC_TX = '2048, 3072, 5120, 6144'; //
{
  CO_GraGruY_Dvl_TX = '2048, 3072, 5120, 6144'; // Devolucao de cliente
  CO_GraGruY_Rtb_TX = '2048, 3072, 5120, 6144'; // Retrabalho para cliente
  CO_GraGruY_Ped_TX = '2048, 3072, 5120, 6144'; // Pedido de Venda
  //
}
  CO_TXT_emidAjuste        = '[ND]';
  CO_TXT_emidCompra        = 'Entrada';
  CO_TXT_emidVenda         = 'Sa�da';
  CO_TXT_emidReclas        = 'Reclasse';
  CO_TXT_emidBaixa         = 'Baixa';
  CO_TXT_emidIndsWE        = 'INATIVO';
  CO_TXT_emidIndsVS        = 'Curtido';
  CO_TXT_emidClassArtVSUni = 'Classe Unit.';
  CO_TXT_emidReclasVSUni   = 'Reclasse Unit.';
  CO_TXT_emidForcado       = 'For�ado';
  CO_TXT_emidSemOrigem     = 'Sem Origem';
  CO_TXT_emidEmOperacao    = 'Em Opera��o';
  CO_TXT_emidResiduoReclas = 'Residual';
  CO_TXT_emidInventario    = 'Ajuste';
  CO_TXT_emidClassArtVSMul = 'Classe Mult.';
  CO_TXT_emidPreReclasse   = 'Pr� reclasse';
  CO_TXT_emidEntradaPlC    = 'Compra de Classificado';
  CO_TXT_emidExtraBxa      = 'Baixa extra';
  CO_TXT_emidSaldoAnterior = 'Saldo anterior';
  CO_TXT_emidEmProcWE      = 'Em processo WE';
  CO_TXT_emidFinished      = 'Acabado';
  CO_TXT_emidDevolucao     = 'Devolu��o';
  CO_TXT_emidRetrabalho    = 'Retrabalho';
  CO_TXT_emidGeraSubProd   = 'Sub Produto';
  CO_TXT_emidReclasVSMul   = 'Reclasse Mul.';
  CO_TXT_emidTransfLoc     = 'Transfer. Local';
  CO_TXT_emidEmProcCal     = 'Em proc. caleiro';
  CO_TXT_emidEmProcCur     = 'Em proc. curtimento';
  CO_TXT_emidDesclasse     = 'Desclassifica��o';
  CO_TXT_emidCaleado       = 'Caleado';
  CO_TXT_emidRibPDA        = 'Couro PDA';
  CO_TXT_emidRibDTA        = 'Couro DTA';
  CO_TXT_emidEmProcSP      = 'Subproduto em proc.';
  CO_TXT_emidEmReprRM      = 'Reprocesso / reparo';
  CO_TXT_emidCurtido       = 'Curtido';
  CO_TXT_emidMixInsum      = 'Dilu./Mist. insumos';
  CO_TXT_emidInnSemCob     = 'Entrada sem Cobertura';
  CO_TXT_emidOutSemCob     = 'Sa�da sem Cobertura';
  CO_TXT_emidIndstrlzc     = 'Industrializa��o';

{
  MaxEstqMovimID = Integer(High(TEstqMovimID));
  sEstqMovimID: array[0..MaxEstqMovimID] of string = (
    CO_TXT_emidAjuste        , // 0
    CO_TXT_emidCompra        , // 1
    CO_TXT_emidVenda         , // 2
    CO_TXT_emidReclas        , // 3
    CO_TXT_emidBaixa         , // 4
    CO_TXT_emidIndsWE        , // 5
    CO_TXT_emidIndsVS        , // 6
    CO_TXT_emidClassArtVSUni , // 7
    CO_TXT_emidReclasVSUni   , // 8
    CO_TXT_emidForcado       , // 9
    CO_TXT_emidSemOrigem     , // 10
    CO_TXT_emidEmOperacao    , // 11
    CO_TXT_emidResiduoReclas , // 12
    CO_TXT_emidInventario    , // 13
    CO_TXT_emidClassArtVSMul , // 14
    CO_TXT_emidPreReclasse   , // 15
    CO_TXT_emidEntradaPlC    , // 16
    CO_TXT_emidExtraBxa      , // 17
    CO_TXT_emidSaldoAnterior , // 18
    CO_TXT_emidEmProcWE      , // 19
    CO_TXT_emidFinished      , // 20
    CO_TXT_emidDevolucao     , // 21
    CO_TXT_emidRetrabalho    , // 22
    CO_TXT_emidGeraSubProd   , // 23
    CO_TXT_emidReclasVSMul   , // 24
    CO_TXT_emidTransfLoc     , // 25
    CO_TXT_emidEmProcCal     , // 26
    CO_TXT_emidEmProcCur     , // 27
    CO_TXT_emidDesclasse     , // 28
    CO_TXT_emidCaleado       , // 29
    CO_TXT_emidRibPDA        , // 30
    CO_TXT_emidRibDTA        , // 31
    CO_TXT_emidEmProcSP      , // 32
    CO_TXT_emidEmReprRM      , // 33
    CO_TXT_emidCurtido       , // 34
    CO_TXT_emidMixInsum      , // 35
    CO_TXT_emidInnSemCob     , // 36
    CO_TXT_emidOutSemCob     , // 37
    CO_TXT_emidIndstrlzc      // 38
  );
  CO_TXT_FRENDLY_emidAjuste        = '[ND]';
  CO_TXT_FRENDLY_emidCompra        = 'Compra de mat�ria-prima';
  CO_TXT_FRENDLY_emidVenda         = 'Venda';
  CO_TXT_FRENDLY_emidReclas        = 'Reclasse';
  CO_TXT_FRENDLY_emidBaixa         = 'Baixa';
  CO_TXT_FRENDLY_emidIndsWE        = 'INATIVO';
  CO_TXT_FRENDLY_emidIndsVS        = 'Gera��o de Artigo';
  CO_TXT_FRENDLY_emidClassArtVSUni = 'Calssifica��o couro a couro';
  CO_TXT_FRENDLY_emidReclasVSUni   = 'Reclassifica��o unit�ria';
  CO_TXT_FRENDLY_emidForcado       = 'Baixa For�ada';
  CO_TXT_FRENDLY_emidSemOrigem     = 'Sem Origem';
  CO_TXT_FRENDLY_emidEmOperacao    = 'Em Opera��o';
  CO_TXT_FRENDLY_emidResiduoReclas = 'Residual de Classe/Reclasse';
  CO_TXT_FRENDLY_emidInventario    = 'Ajuste em Invent�rio';
  CO_TXT_FRENDLY_emidClassArtVSMul = 'Classe M�ltipla';
  CO_TXT_FRENDLY_emidPreReclasse   = 'Pr� Reclasse (Aglomera��o)';
  CO_TXT_FRENDLY_emidEntradaPlC    = 'Compra de Classificado';
  CO_TXT_FRENDLY_emidExtraBxa      = 'Baixa Extra(vio)';
  CO_TXT_FRENDLY_emidSaldoAnterior = 'Saldo anterior';
  CO_TXT_FRENDLY_emidEmProcWE      = 'Semi Acabado em Processo';
  CO_TXT_FRENDLY_emidFinished      = 'Couro Acabado';
  CO_TXT_FRENDLY_emidDevolucao     = 'Devolu��o';
  CO_TXT_FRENDLY_emidRetrabalho    = 'Retrabalho';
  CO_TXT_FRENDLY_emidGeraSubProd   = 'Gera��o de Sub Produto';
  CO_TXT_FRENDLY_emidReclasVSMul   = 'Reclassifica��o M�ltipla';
  CO_TXT_FRENDLY_emidTransfLoc     = 'Transfer�ncia de Local';
  CO_TXT_FRENDLY_emidEmProcCal     = 'Em processo de caleiro';
  CO_TXT_FRENDLY_emidEmProcCur     = 'Em processo de curtimento';
  CO_TXT_FRENDLY_emidDesclasse     = 'Desclassifica��o';
  CO_TXT_FRENDLY_emidCaleado       = 'Couro caleado';
  CO_TXT_FRENDLY_emidRibPDA        = 'Couro pr�-descarnado e aparado';
  CO_TXT_FRENDLY_emidRibDTA        = 'Couro tripa (div./lam./integr.)';
  CO_TXT_FRENDLY_emidEmProcSP      = 'Processamento de subproduto';
  CO_TXT_FRENDLY_emidEmReprRM      = 'Reprocesso / reparo de material';
  CO_TXT_FRENDLY_emidCurtido       = 'Couro curtido';
  CO_TXT_FRENDLY_emidMixInsum      = 'Dilui��o / Mistura de insumos ';
  CO_TXT_FRENDLY_emidInnSemCob     = 'Entrada sem Cobertura';
  CO_TXT_FRENDLY_emidOutSemCob     = 'Sa�da sem Cobertura';
  CO_TXT_FRENDLY_emidIndstrlzc     = 'Industrializa��o';

  sEstqMovimID_FRENDLY: array[0..MaxEstqMovimID] of string = (
    CO_TXT_FRENDLY_emidAjuste        , // 0
    CO_TXT_FRENDLY_emidCompra        , // 1
    CO_TXT_FRENDLY_emidVenda         , // 2
    CO_TXT_FRENDLY_emidReclas        , // 3
    CO_TXT_FRENDLY_emidBaixa         , // 4
    CO_TXT_FRENDLY_emidIndstrlzc     , // 5
    CO_TXT_FRENDLY_emidIndsVS        , // 6
    CO_TXT_FRENDLY_emidClassArtVSUni , // 7
    CO_TXT_FRENDLY_emidReclasVSUni   , // 8
    CO_TXT_FRENDLY_emidForcado       , // 9
    CO_TXT_FRENDLY_emidSemOrigem     , // 10
    CO_TXT_FRENDLY_emidEmOperacao    , // 11
    CO_TXT_FRENDLY_emidResiduoReclas , // 12
    CO_TXT_FRENDLY_emidInventario    , // 13
    CO_TXT_FRENDLY_emidClassArtVSMul , // 14
    CO_TXT_FRENDLY_emidPreReclasse   , // 15
    CO_TXT_FRENDLY_emidEntradaPlC    , // 16
    CO_TXT_FRENDLY_emidExtraBxa      , // 17
    CO_TXT_FRENDLY_emidSaldoAnterior , // 18
    CO_TXT_FRENDLY_emidEmProcWE      , // 19
    CO_TXT_FRENDLY_emidFinished      , // 20
    CO_TXT_FRENDLY_emidDevolucao     , // 21
    CO_TXT_FRENDLY_emidRetrabalho    , // 22
    CO_TXT_FRENDLY_emidGeraSubProd   , // 23
    CO_TXT_FRENDLY_emidReclasVSMul   , // 24
    CO_TXT_FRENDLY_emidTransfLoc     , // 25
    CO_TXT_FRENDLY_emidEmProcCal     , // 26
    CO_TXT_FRENDLY_emidEmProcCur     , // 27
    CO_TXT_FRENDLY_emidDesclasse     , // 28
    CO_TXT_FRENDLY_emidCaleado       , // 29
    CO_TXT_FRENDLY_emidRibPDA        , // 30
    CO_TXT_FRENDLY_emidRibDTA        , // 31
    CO_TXT_FRENDLY_emidEmProcSP      , // 32
    CO_TXT_FRENDLY_emidEmReprRM      , // 33
    CO_TXT_FRENDLY_emidCurtido       , // 34
    CO_TXT_FRENDLY_emidMixInsum      , // 35
    CO_TXT_FRENDLY_emidInnSemCob     , // 36
    CO_TXT_FRENDLY_emidOutSemCob     , // 37
    CO_TXT_FRENDLY_emidIndstrlzc      // 38*)
  );
  CO_TXT_LONG_emidAjuste        = '[N�o Definido]';
  CO_TXT_LONG_emidCompra        = 'Compra';
  CO_TXT_LONG_emidVenda         = 'Venda';
  CO_TXT_LONG_emidReclas        = 'Reclassifica��o';
  CO_TXT_LONG_emidBaixa         = 'Baixa do estoque';
  CO_TXT_LONG_emidIndsWE        = 'Acabado';
  CO_TXT_LONG_emidIndsVS        = 'Artigo de Ribeira';
  CO_TXT_LONG_emidClassArtVSUni = 'Artigo de Ribeira Classificado Unit.';
  CO_TXT_LONG_emidReclasVSUni   = 'Artigo de Ribeira Reclassificado Unit.';
  CO_TXT_LONG_emidForcado       = 'Baixa For�ada';
  CO_TXT_LONG_emidSemOrigem     = '[Sem Origem!]';
  CO_TXT_LONG_emidEmOperacao    = 'Ordem de Opera��o';
  CO_TXT_LONG_emidResiduoReclas = 'Res�duo de reclassifica��o';
  CO_TXT_LONG_emidInventario    = 'Ajuste de Invent�rio';
  CO_TXT_LONG_emidClassArtVSMul = 'Artigo de Ribeira Classificado Mult.';
  CO_TXT_LONG_emidPreReclasse   = 'Prepara��o para reclassifica��o';
  CO_TXT_LONG_emidEntradaPlC    = 'Compra de classificado';
  CO_TXT_LONG_emidExtraBxa      = 'Baixa extra(vio)';
  CO_TXT_LONG_emidSaldoAnterior = 'Saldo anterior';
  CO_TXT_LONG_emidEmProcWE      = 'Em processo wet end';
  CO_TXT_LONG_emidFinished      = 'Acabado e classificado';
  CO_TXT_LONG_emidDevolucao     = 'Devolu��o de couro vendido';
  CO_TXT_LONG_emidRetrabalho    = 'Retrabalho de couro vendido';
  CO_TXT_LONG_emidGeraSubProd   = 'Gera��o de Sub Produto';
  CO_TXT_LONG_emidReclasVSMul   = 'Artigo de Ribeira Reclassificado Mult.';
  CO_TXT_LONG_emidTransfLoc     = 'Transfer�ncia de Local';
  CO_TXT_LONG_emidEmProcCal     = 'Em processo de caleiro';
  CO_TXT_LONG_emidEmProcCur     = 'Em processo de curtimento';
  CO_TXT_LONG_emidDesclasse     = 'Desclassifica��o de wet blue';
  CO_TXT_LONG_emidCaleado       = 'Couro caleado';
  CO_TXT_LONG_emidRibPDA        = 'Couro pr�-descarnado e aparado';
  CO_TXT_LONG_emidRibDTA        = 'Couro tripa (dividido ou laminado ou integral)';
  CO_TXT_LONG_emidEmProcSP      = 'Processamento de subproduto';
  CO_TXT_LONG_emidEmReprRM      = 'Reprocesso / reparo de material';
  CO_TXT_LONG_emidCurtido       = 'Couro curtido';
  CO_TXT_LONG_emidMixInsum      = 'Dilui��o / mistura de insumos';
  CO_TXT_LONG_emidInnSemCob     = 'Entrada sem Cobertura';
  CO_TXT_LONG_emidOutSemCob     = 'Sa�da sem Cobertura';
  CO_TXT_LONG_emidIndstrlzc     = 'Industrializa��o';

  MaxEstqMovimIDLong = Integer(High(TEstqMovimID));
  sEstqMovimIDLong: array[0..MaxEstqMovimID] of string = (
    CO_TXT_LONG_emidAjuste        , // 0
    CO_TXT_LONG_emidCompra        , // 1
    CO_TXT_LONG_emidVenda         , // 2
    CO_TXT_LONG_emidReclas        , // 3
    CO_TXT_LONG_emidBaixa         , // 4
    CO_TXT_LONG_emidIndsWE        , // 5
    CO_TXT_LONG_emidIndsVS        , // 6
    CO_TXT_LONG_emidClassArtVSUni , // 7
    CO_TXT_LONG_emidReclasVSUni   , // 8
    CO_TXT_LONG_emidForcado       , // 9
    CO_TXT_LONG_emidSemOrigem     , // 10
    CO_TXT_LONG_emidEmOperacao    , // 11
    CO_TXT_LONG_emidResiduoReclas , // 12
    CO_TXT_LONG_emidInventario    , // 13
    CO_TXT_LONG_emidClassArtVSMul , // 14
    CO_TXT_LONG_emidPreReclasse   , // 15
    CO_TXT_LONG_emidEntradaPlC    , // 16
    CO_TXT_LONG_emidExtraBxa      , // 17
    CO_TXT_LONG_emidSaldoAnterior , // 18
    CO_TXT_LONG_emidEmProcWE      , // 19
    CO_TXT_LONG_emidFinished      , // 20
    CO_TXT_LONG_emidDevolucao     , // 21
    CO_TXT_LONG_emidRetrabalho    , // 22
    CO_TXT_LONG_emidGeraSubProd   , // 23
    CO_TXT_LONG_emidReclasVSMul   , // 24
    CO_TXT_LONG_emidTransfLoc     , // 25
    CO_TXT_LONG_emidEmProcCal     , // 26
    CO_TXT_LONG_emidEmProcCur     , // 27
    CO_TXT_LONG_emidDesclasse     , // 28
    CO_TXT_LONG_emidCaleado       , // 29
    CO_TXT_LONG_emidRibPDA        , // 30
    CO_TXT_LONG_emidRibDTA        , // 31
    CO_TXT_LONG_emidEmProcSP      , // 32
    CO_TXT_LONG_emidEmReprRM      , // 33
    CO_TXT_LONG_emidCurtido       , // 34
    CO_TXT_LONG_emidMixInsum      , // 35
    CO_TXT_LONG_emidInnSemCob     , // 36
    CO_TXT_LONG_emidOutSemCob     , // 37
    CO_TXT_LONG_emidIndstrlzc      // 38
  );
  CO_TXT_ESC_emidAjuste        = '[N�o Definido]';
  CO_TXT_ESC_emidCompra        = 'Compra de couro verde / salgado';
  CO_TXT_ESC_emidVenda         = 'zzz??????';
  CO_TXT_ESC_emidReclas        = 'zzz??????';
  CO_TXT_ESC_emidBaixa         = 'zzz??????';
  CO_TXT_ESC_emidIndsWE        = 'zzz??????';
  CO_TXT_ESC_emidIndsVS        = 'Couro curtido e n�o classificado';
  CO_TXT_ESC_emidClassArtVSUni = 'Couro curtido Classificado Unit.';
  CO_TXT_ESC_emidReclasVSUni   = 'Couro curtido Reclassificado Unit.';
  CO_TXT_ESC_emidForcado       = 'zzz??????';
  CO_TXT_ESC_emidSemOrigem     = '[Sem Origem!]';
  CO_TXT_ESC_emidEmOperacao    = 'Couro Operado';
  CO_TXT_ESC_emidResiduoReclas = 'Res�duo de reclassifica��o';
  CO_TXT_ESC_emidInventario    = 'Ajuste de Invent�rio';
  CO_TXT_ESC_emidClassArtVSMul = 'Couro curtido Classificado Mult.';
  CO_TXT_ESC_emidPreReclasse   = 'zzz??????';
  CO_TXT_ESC_emidEntradaPlC    = 'Compra de couro curtido';
  CO_TXT_ESC_emidExtraBxa      = 'zzz??????';
  CO_TXT_ESC_emidSaldoAnterior = 'zzz??????';
  CO_TXT_ESC_emidEmProcWE      = 'zzz??????';
  CO_TXT_ESC_emidFinished      = 'Semi/Acabado pronto';
  CO_TXT_ESC_emidDevolucao     = 'Devolu��o definitiva de couro vendido';
  CO_TXT_ESC_emidRetrabalho    = 'Devolu��o de couro vendido para retrabalho';
  CO_TXT_ESC_emidGeraSubProd   = 'Gera��o de Sub Produto';
  CO_TXT_ESC_emidReclasVSMul   = 'Couro curtido Reclassificado Mult.';
  CO_TXT_ESC_emidTransfLoc     = 'Transfer�ncia de Local';
  CO_TXT_ESC_emidEmProcCal     = 'zzz??????';
  CO_TXT_ESC_emidEmProcCur     = 'zzz??????';
  CO_TXT_ESC_emidDesclasse     = 'zzz??????';
  CO_TXT_ESC_emidCaleado       = 'zzz??????';//'Couro caleado';
  CO_TXT_ESC_emidRibPDA        = 'zzz??????';//'Couro pr�-descarnado e aparado';
  CO_TXT_ESC_emidRibDTA        = 'zzz??????';//'Couro tripa (dividido ou laminado ou integral)';
  CO_TXT_ESC_emidEmProcSP      = 'zzz??????';//'Processamento de subproduto';
  CO_TXT_ESC_emidEmReprRM      = 'zzz??????';//'Reprocesso / reparo de material';
  CO_TXT_ESC_emidCurtido       = 'zzz??????';//'Couro curtido';
  CO_TXT_ESC_emidMixInsum      = 'zzz??????';//'Dilui��o / mistura de insumos';
  CO_TXT_ESC_emidInnSemCob     = 'Entrada sem origem conhecida';
  CO_TXT_ESC_emidOutSemCob     = 'zzz??????';
  CO_TXT_ESC_emidIndstrlzc     = 'Industrializa��o';

  MaxEstqMovimIDESC = Integer(High(TEstqMovimID));
  sEstqMovimIDESC: array[0..MaxEstqMovimID] of string = (
    CO_TXT_ESC_emidAjuste        , // 0
    CO_TXT_ESC_emidCompra        , // 1
    CO_TXT_ESC_emidVenda         , // 2
    CO_TXT_ESC_emidReclas        , // 3
    CO_TXT_ESC_emidBaixa         , // 4
    CO_TXT_ESC_emidIndsWE        , // 5
    CO_TXT_ESC_emidIndsVS        , // 6
    CO_TXT_ESC_emidClassArtVSUni , // 7
    CO_TXT_ESC_emidReclasVSUni   , // 8
    CO_TXT_ESC_emidForcado       , // 9
    CO_TXT_ESC_emidSemOrigem     , // 10
    CO_TXT_ESC_emidEmOperacao    , // 11
    CO_TXT_ESC_emidResiduoReclas , // 12
    CO_TXT_ESC_emidInventario    , // 13
    CO_TXT_ESC_emidClassArtVSMul , // 14
    CO_TXT_ESC_emidPreReclasse   , // 15
    CO_TXT_ESC_emidEntradaPlC    , // 16
    CO_TXT_ESC_emidExtraBxa      , // 17
    CO_TXT_ESC_emidSaldoAnterior , // 18
    CO_TXT_ESC_emidEmProcWE      , // 19
    CO_TXT_ESC_emidFinished      , // 20
    CO_TXT_ESC_emidDevolucao     , // 21
    CO_TXT_ESC_emidRetrabalho    , // 22
    CO_TXT_ESC_emidGeraSubProd   , // 23
    CO_TXT_ESC_emidReclasVSMul   , // 24
    CO_TXT_ESC_emidTransfLoc     , // 25
    CO_TXT_ESC_emidEmProcCal     , // 26
    CO_TXT_ESC_emidEmProcCur     , // 27
    CO_TXT_ESC_emidDesclasse     , // 28
    CO_TXT_ESC_emidCaleado       , // 29
    CO_TXT_ESC_emidRibPDA        , // 30
    CO_TXT_ESC_emidRibDTA        , // 31
    CO_TXT_ESC_emidEmProcSP      , // 32
    CO_TXT_ESC_emidEmReprRM      , // 33
    CO_TXT_ESC_emidCurtido       , // 34
    CO_TXT_ESC_emidMixInsum      , // 35
    CO_TXT_ESC_emidInnSemCob     , // 36
    CO_TXT_ESC_emidOutSemCob     , // 37
    CO_TXT_ESC_emidIndstrlzc      // 38
  );
  MaxEstqMovimIDTipo = Integer(High(TEstqMovimID));
  iEstqMovimID: array[0..MaxEstqMovimID] of integer = (
    (*emidAjuste         0: Tipo :=*) 0,  // 0 = Terceiro = ???
    (*emidCompra         1: Tipo :=*) 1,  // 1 = Terceiro = Procedencia
    (*emidVenda          2: Tipo :=*) 2,  // 2 = Terceiro = Cliente
    (*emidReclasWE       3: Tipo :=*) 1,
    (*emidBaixa          4: Tipo :=*) 1,
    (*emidIndsWE         5: Tipo :=*) 1,
    (*emidIndsVS         6: Tipo :=*) 1,
    (*emidClassArtVSUni  7: Tipo :=*) 2,
    (*emidReclasVS       8: Tipo :=*) 2,
    (*emidForcado        9: Tipo :=*) 1,
    (*emidSemOrigem      10: Tipo :=*) 0, // ???
    (*emidEmOperacao     11: Tipo :=*) 2, // ???
    (*emidResiduoReclas  12: Tipo :=*) 0,
    (*emidInventario     13: Tipo :=*) 0,
    (*emidClassArtVSMul  14: Tipo :=*) 2,
    (*emidPreReclasse    15: Tipo :=*) 1,
    (*emidEntradaPlC     16: Tipo :=*) 1,
    (*emidExtraBxa       17: Tipo :=*) 0,
    (*emidSaldoAnterior  18: Tipo :=*) 0,
    (*emidProcWE         19: Tipo :=*) 0,
    (*emidFinished       20: Tipo :=*) 0,
    (*emidDevolucao      21: Tipo :=*) 0,
    (*emidRetrabalho     22: Tipo :=*) 0,
    (*emidGeraSubProd    23: Tipo :=*) 1,
    (*emidReclasVS       24: Tipo :=*) 2,
    (*emidTransfLoc      25: Tipo :=*) 2,
    (*emidEmProcCur      26: Tipo :=*) 0,
    (*emidEmProcCal      27: Tipo :=*) 0,
    (*emidDesclasse      28: Tipo :=*) 2,
    (*emidCaleado        28: Tipo :=*) 0,
    (*emidRibPDA         30: Tipo :=*) 0,
    (*emidRibDTA         31: Tipo :=*) 0,
    (*emidEmProcSP       32: Tipo :=*) 0,
    (*emidEmReprRM       33: Tipo :=*) 0,
    (*emidCurtido        34: Tipo :=*) 0,
    (*emidMixIsum        35: Tipo :=*) 0,
    (*emidInnSemCob      36: Tipo :=*) 0,
    (*emidOutSemCob      37: Tipo :=*) 0,
    (*emidIndstrlzc      38: Tipo :=*) 0
 );
  CO_TXT_eminSemNiv          = 'Sem n�vel';
  CO_TXT_eminSorcClass       = 'Origem classifica��o';
  CO_TXT_eminDestClass       = 'Destino classifica��o';
  CO_TXT_eminSorcInds        = ''; //'Origem gera��o de artigo';
  CO_TXT_eminDestInds        = ''; //'Destino gera��o de artigo';
  CO_TXT_eminSorcReclass     = ''; //'Origem reclassifica��o';
  CO_TXT_eminDestReclass     = ''; //'Destino reclassifica��o';
  CO_TXT_eminSorcOper        = ''; //'Origem opera��o (Divis�o,...)';
  CO_TXT_eminEmOperInn       = ''; //'Em opera��o (Divis�o,...)';
  CO_TXT_eminDestOper        = ''; //'Destino opera��o (Divis�o,...)';
  CO_TXT_eminEmOperBxa       = ''; //'Baixa de opera��o (Divis�o,...)';
  CO_TXT_eminSorcPreReclas   = ''; //'Baixa em pr� reclasse';
  CO_TXT_eminDestPreReclas   = ''; //'Entrada para reclasse';
  CO_TXT_eminDestCurtiVS     = ''; //'Totalizador de Artigo Gerado';
  CO_TXT_eminSorcCurtiVS     = ''; //'Item de gera��o de Artigo Gerado';
  CO_TXT_eminBaixCurtiVS     = ''; //'Item de baixa de Artigo In Natura';
  CO_TXT_eminSdoArtInNat     = ''; //'Artigo In Natura';
  CO_TXT_eminSdoArtGerado    = ''; //'Artigo Gerado';
  CO_TXT_eminSdoArtClassif   = ''; //'Artigo Classificado';
  CO_TXT_eminSdoArtEmOper    = ''; //'Artigo em Opera��o';
  CO_TXT_eminSorcWEnd        = ''; //'Origem semi acabado em processo';
  CO_TXT_eminEmWEndInn       = ''; //'Semi acabado em processo';
  CO_TXT_eminDestWEnd        = ''; //''; //'Destino semi acabado em processo';
  CO_TXT_eminEmWEndBxa       = ''; //'Baixa de semi acabado em processo';
  CO_TXT_eminSdoArtEmWEnd    = ''; //'Artigo Semi Acabado';
  CO_TXT_eminFinishSdo       = ''; //'Artigo Acabado';
  CO_TXT_eminFinishInn       = ''; //'Artigo Acabado Calssificado';
  CO_TXT_emineminSdoSubPrd   = ''; //'Sub produto';
  CO_TXT_eminSorcLocal       = 'Origem de transf. de local';
  CO_TXT_eminDestLocal       = 'Destino de transf. de local';

  CO_TXT_eminSorcCal         = ''; //'Origem caleiro em processo';
  CO_TXT_eminEmCalInn        = ''; //'Caleiro em processo';
  CO_TXT_eminDestCal         = ''; //'Destino caleiro em processo';
  CO_TXT_eminEmCalBxa        = ''; //'Baixa de caleiro em processo';
  CO_TXT_eminSdoArtEmCal     = ''; //'Artigo de caleiro';

  CO_TXT_eminSorcCur         = ''; //'Origem curtimento em processo';
  CO_TXT_eminEmCurInn        = ''; //'Curtimento em processo';
  CO_TXT_eminDestCur         = ''; //'Destino curtimento em processo';
  CO_TXT_eminEmCurBxa        = ''; //'Baixa de curtimento em processo';
  CO_TXT_eminSdoArtEmCur     = ''; //'Artigo de curtimento';

  CO_TXT_eminSorcPDA         = ''; //'Origem PDA em opera��o';
  CO_TXT_eminEmPDAInn        = ''; //'PDA em opera��o';
  CO_TXT_eminDestPDA         = ''; //'Destino PDA em opera��o';
  CO_TXT_eminEmPDABxa        = ''; //'Baixa de PDA em opera��o';
  CO_TXT_eminSdoArtEmPDA     = ''; //'Artigo de PDA';

  CO_TXT_eminSorcDTA         = ''; //'Origem DTA em opera��o';
  CO_TXT_eminEmDTAInn        = ''; //'DTA opera��o';
  CO_TXT_eminDestDTA         = ''; //'Destino DTA em opera��o';
  CO_TXT_eminEmDTABxa        = ''; //'Baixa de DTA em opera��o';
  CO_TXT_eminSdoArtEmDTA     = ''; //'Artigo de DTA';

  CO_TXT_eminSorcPSP         = ''; //'Origem PSP em processo';
  CO_TXT_eminEmPSPInn        = ''; //'PSP processo';
  CO_TXT_eminDestPSP         = ''; //'Destino PSP em processo';
  CO_TXT_eminEmPSPBxa        = ''; //'Baixa de PSP em processo';
  CO_TXT_eminSdoArtEmPSP     = ''; //'Artigo de PSP';

  CO_TXT_eminSorcRRM         = ''; //'Origem RRM em reprocesso';
  CO_TXT_eminEmRRMInn        = ''; //'RRM reprocesso';
  CO_TXT_eminDestRRM         = ''; //'Destino RRM em reprocesso';
  CO_TXT_eminEmRRMBxa        = ''; //'Baixa de RRM em reprocesso';
  CO_TXT_eminSdoArtEmRRM     = ''; //'Artigo de Reprocesso / reparo';

  CO_TXT_eminSorcMixInsum    = ''; //'Baixa de insumo em mistura';
  CO_TXT_eminDestMixInsum    = ''; //'Gera��o de insumo em mistura';

  CO_TXT_eminSorcIndzc       = 'Origem industrializa��o';
  CO_TXT_eminEmIndzcInn      = 'Em industrializa��o';
  CO_TXT_eminDestIndzc       = 'Destino industrializa��o';
  CO_TXT_eminEmIndzcBxa      = 'Baixa de industrializa��o';

  MaxEstqMovimNiv = Integer(High(TEstqMovimNiv));
  sEstqMovimNiv: array[0..MaxEstqMovimNiv] of string = (
    CO_TXT_eminSemNiv          , // 00
    CO_TXT_eminSorcClass       , // 01
    CO_TXT_eminDestClass       , // 02
    CO_TXT_eminSorcInds        , // 03
    CO_TXT_eminDestInds        , // 04
    CO_TXT_eminSorcReclass     , // 05
    CO_TXT_eminDestReclass     , // 06
    CO_TXT_eminSorcOper        , // 07
    CO_TXT_eminEmOperInn       , // 08
    CO_TXT_eminDestOper        , // 09
    CO_TXT_eminEmOperBxa       , // 10
    CO_TXT_eminSorcPreReclas   , // 11
    CO_TXT_eminDestPreReclas   , // 12
    CO_TXT_eminDestCurtiVS     , // 13
    CO_TXT_eminSorcCurtiVS     , // 14
    CO_TXT_eminBaixCurtiVS     , // 15
    CO_TXT_eminSdoArtInNat     , // 16
    CO_TXT_eminSdoArtGerado    , // 17
    CO_TXT_eminSdoArtClassif   , // 18
    CO_TXT_eminSdoArtEmOper    , // 19
    CO_TXT_eminSorcWEnd        , // 20
    CO_TXT_eminEmWEndInn       , // 21
    CO_TXT_eminDestWEnd        , // 22
    CO_TXT_eminEmWEndBxa       , // 23
    CO_TXT_eminSdoArtEmWEnd    , // 24
    CO_TXT_eminFinishSdo       , // 25
    CO_TXT_emineminSdoSubPrd   , // 26
    CO_TXT_eminSorcLocal       , // 27
    CO_TXT_eminDestLocal       , // 28

    CO_TXT_eminSorcCal         , // 29
    CO_TXT_eminEmCalInn        , // 30
    CO_TXT_eminDestCal         , // 31
    CO_TXT_eminEmCalBxa        , // 32
    CO_TXT_eminSdoArtEmCal     , // 33

    CO_TXT_eminSorcCur         , // 34
    CO_TXT_eminEmCurInn        , // 35
    CO_TXT_eminDestCur         , // 36
    CO_TXT_eminEmCurBxa        , // 37
    CO_TXT_eminSdoArtEmCur     , // 38

    CO_TXT_eminSorcPDA         , // 39
    CO_TXT_eminEmPDAInn        , // 40
    CO_TXT_eminDestPDA         , // 41
    CO_TXT_eminEmPDABxa        , // 42
    CO_TXT_eminSdoArtEmPDA     , // 43

    CO_TXT_eminSorcDTA         , // 44
    CO_TXT_eminEmDTAInn        , // 45
    CO_TXT_eminDestDTA         , // 46
    CO_TXT_eminEmDTABxa        , // 47
    CO_TXT_eminSdoArtEmDTA     , // 48

    CO_TXT_eminSorcPSP         , // 49
    CO_TXT_eminEmPSPInn        , // 50
    CO_TXT_eminDestPSP         , // 51
    CO_TXT_eminEmPSPBxa        , // 52
    CO_TXT_eminSdoArtEmPSP     , // 53

    CO_TXT_eminSorcRRM         , // 54
    CO_TXT_eminEmRRMInn        , // 55
    CO_TXT_eminDestRRM         , // 56
    CO_TXT_eminEmRRMBxa        , // 57
    CO_TXT_eminSdoArtEmRRM     , // 58

    CO_TXT_eminSorcMixInsum    , // 59
    CO_TXT_eminDestMixInsum    , // 60

    CO_TXT_eminSorcIndzc       , // 61
    CO_TXT_eminEmIndzcInn      , // 62
    CO_TXT_eminDestIndzc       , // 63
    CO_TXT_eminEmIndzcBxa       // 64
  );

(*
  CO_ALL_CODS_PRI_IDX_TUPLE  = '1,8,9,15';
  //  V S_PF.AdicionarNovosV S_emid();
  CO_ALL_CODS_CLASS_TX       = '0,7,8,11,13,14,16,20,21,22,24,28,36';
  CO_ALL_CODS_BLEND_TX       = '1,7,8,10,13,14,16,24,28';
  CO_ALL_CODS_INN_POSIT_TX   = '1,9,13,16';
  CO_ALL_CODS_INN_ENTRA_TX   = '1,13,16';
*)
  CO_ALL_CODS_BXA_POSIT_TX   = '9,17,28';
  CO_ALL_CODS_BXA_EXTRA_TX   = '9,13,17,28';
(*
  CO_ALL_CODS_INN_NF_SPED_TX = '1,16,21,22'; // Entradas no estoque com documento fiscal de compra, remessa, devolucao, etc
  CO_ALL_CODS_NO_INN_SPED_TX = '6,11,19,20,23,26,27,32,33'; // Entradas no estoque pela producao (sem doc fiscal de compra, remessa, devolucao, etc)
  CO_ALL_CODS_ESTQ_SEM_PC    = '23,32'; N�o deveria usar, pois s� tem qtde!!
  CO_ONLY_COD_IDS_CLAS_TX    = '7,14';
  CO_ONLY_COD_IDS_RECL_TX    = '8,24,28';
  *)
  CO_CODS_OPER_PROC_ID_GET   = '11,19,26,27,29,32,33,38';
(*
  CO_CODS_OPER_PROC_ID_SET   = '11,19,38';
*)
  CO_CODS_OPER_PROC_NIV_GET  = '7,20,61';
  CO_CODS_OPER_PROC_NIV_FRO  = '7,10,20,23,30,32,34,35,37,50,52,55,57,61';
(*
  CO_CODS_NIV_NEED_MO_FORNEC = '8,13,21,30,35';
  CO_CODS_NIV_NEED_MO_CLIENT = '1,2,8,9,11,12,13,14,15,20,21,22,23,27,28,29,30,34,35';
  CO_CODS_NIV_NO_BXA_AJS     = '8,21,30,35,40,45,50,55';
  //
  //CO_COUNIV2_SUBPRD          = '2,3'; // 1=Couro, 2=Raspa, 3=Subproduto
  CO_COUNIV2_SUBPRD          = '3'; // 1=Couro, 2=Raspa, 3=Subproduto
*)
  // ATT_StatPall
  CO_TXT_txspIndefinido      = 'Indefinido';
  CO_TXT_txspMontando        = 'Montando';
  CO_TXT_txspDesmontando     = 'Desmontando';
  CO_TXT_txspMontEDesmo      = 'Mont. e desmo.';
  CO_TXT_txspEncerrado       = 'Encerrado';
  CO_TXT_txspEncerMont       = 'Encer. mas Mont.';
  CO_TXT_txspEncerDesmo      = 'Encer. mas Desmo.';
  CO_TXT_txspEncerMontEDesmo = 'Encer mas mnt e desm.';
  CO_TXT_txspRemovido        = 'Removido';
  MaxTXStatPall = Integer(High(TXXStatPall));
  sTXStatPall: array[0..MaxTXStatPall] of string = (
  CO_TXT_txspIndefinido      ,
  CO_TXT_txspMontando        ,
  CO_TXT_txspDesmontando     ,
  CO_TXT_txspMontEDesmo      ,
  CO_TXT_txspEncerrado       ,
  CO_TXT_txspEncerMont       ,
  CO_TXT_txspEncerDesmo      ,
  CO_TXT_txspEncerMontEDesmo ,
  CO_TXT_txspRemovido
  );


(*
  CO_TXT_emitIndef            = 'Indefinido';
  CO_TXT_emitIME_I            = 'IME-Itm';
  CO_TXT_emitIME_C            = 'IME-Cab';
  CO_TXT_emitIME_P            = 'IME-Par';
  CO_TXT_emitPallet           = 'Pallet';
  CO_TXT_emitAvulsos          = 'Avulso';
  CO_TXT_emitAgrupTotalizador = 'APO-Tot';
  CO_TXT_emitAgrupItensNew    = 'APO-New';
  CO_TXT_emitAgrupItensBxa    = 'APO-Bxa';
  MaxEstqMovimType = Integer(High(TEstqMovimType));
  sEstqMovimType: array[0..MaxEstqMovimType] of string = (
  CO_TXT_emitIndef            ,
  CO_TXT_emitIME_I            ,
  CO_TXT_emitIME_C            ,
  CO_TXT_emitIME_P            ,
  CO_TXT_emitPallet           ,
  CO_TXT_emitAvulsos          ,
  CO_TXT_emitAgrupTotalizador ,
  CO_TXT_emitAgrupItensNew    ,
  CO_TXT_emitAgrupItensBxa
  );
*)
}
  CO_TXT_osekND               = 'Indefinido';
  CO_TXT_osekEstoque          = 'Estoque';
  CO_TXT_osekClasse           = 'Classe';
  CO_TXT_osekProducao         = 'Produ��o';
  //CO_TXT_osekDesmonte         = 'Desmonte';
  MaxOrigemSPEDEFDKnd = Integer(High(TOrigemSPEDEFDKnd));
  sOrigemSPEDEFDKnd: array[0..MaxOrigemSPEDEFDKnd] of string = (
  CO_TXT_osekND               ,
  CO_TXT_osekEstoque          ,
  CO_TXT_osekClasse           ,
  CO_TXT_osekProducao         (*,
  CO_TXT_osekDesmonte*)
  );
  //
{
  CO_TXT_ptcNaoInfo            = 'Indefinido';
  CO_TXT_ptcPecas              = 'Pe�as';
  CO_TXT_ptcPesoKg             = 'Peso kg';
  CO_TXT_ptcAreaM2             = '�rea m�';
  CO_TXT_ptcAreaP2             = '�rea ft�';
  CO_TXT_ptcTotal              = 'Valor total';
  MaxTipoCalcCouro = Integer(High(TTipoCalcCouro));
  sListaTipoCalcCouro: array[0..MaxTipoCalcCouro] of string = (
  CO_TXT_ptcNaoInfo     , // 0
  CO_TXT_ptcPecas       , // 1
  CO_TXT_ptcPesoKg      , // 2
  CO_TXT_ptcAreaM2      , // 3
  CO_TXT_ptcAreaP2      , // 4
  CO_TXT_ptcTotal        // 5
  );

  CO_TXT_vsbstdND              = 'N/D';
  CO_TXT_vsbstdIntegral        = 'Integral';
  CO_TXT_vsbstdLaminado        = 'Laminado';
  CO_TXT_vsbstdDivTripa        = 'Dividido tripa';
  CO_TXT_vsbstdDivCurtido      = 'Dividido curtido';
  CO_TXT_vsbstdRebaixado       = 'Rebaixado';
  CO_TXT_vsbstdDivSemi         = 'Dividido semi';
  CO_TXT_vsbstdRebxSemi        = 'Rebaix. em semi';
  MaxTV SBastidao = Integer(High(TV SBastidao));
  sV SBastidao: array[0..MaxTV SBastidao] of String = (
  CO_TXT_vsbstdND              , // 0
  CO_TXT_vsbstdIntegral        , // 1
  CO_TXT_vsbstdLaminado        , // 2
  CO_TXT_vsbstdDivTripa        , // 3
  CO_TXT_vsbstdDivCurtido      , // 4
  CO_TXT_vsbstdRebaixado       , // 5
  CO_TXT_vsbstdDivSemi         , // 6
  CO_TXT_vsbstdRebxSemi         // 7
  );

  CO_TXT_vsrdfnfrmtNaoInfo     = 'N/I';
  CO_TXT_vsrdfnfrmtInaltera    = 'N�o altera';
  CO_TXT_vsrdfnfrmtDivRacha    = 'Racha (divide)';
  CO_TXT_vsrdfnfrmtParteLados  = 'Parte em meios';
  CO_TXT_vsrdfnfrmtParteCabCul = 'Parte culatra e cabe�a';
  CO_TXT_vsrdfnfrmtCabCulBari  = 'Parte culatra, cabe�a e barriga';
  CO_TXT_vsrdfnfrmtGrupona     = 'Grupona (raspa)';
  CO_TXT_vsrdfnfrmtRefila      = 'Refila';
  MaxTV SRedefnFrmt = Integer(High(TV SRedefnFrmt));
  sV SRedefnFrmt: array[0..MaxTV SRedefnFrmt] of String = (
  CO_TXT_vsrdfnfrmtNaoInfo     , // 0
  CO_TXT_vsrdfnfrmtInaltera    , // 1
  CO_TXT_vsrdfnfrmtDivRacha    , // 2
  CO_TXT_vsrdfnfrmtParteLados  , // 3
  CO_TXT_vsrdfnfrmtParteCabCul , // 4
  CO_TXT_vsrdfnfrmtCabCulBari  , // 5
  CO_TXT_vsrdfnfrmtGrupona     , // 6
  CO_TXT_vsrdfnfrmtRefila       // 7
  );
//
  CO_SMIS_01000_AjusteEstoque                = 01000;
  CO_SMIS_02000_Compra                       = 02000;
  CO_SMIS_02250_EntradaPorRemessa            = 02250;
  CO_SMIS_02500_EntradaPorDevolucao          = 02500;
  CO_SMIS_02750_EntradaParaRetrabalho        = 02750;
  CO_SMIS_03000_Classificar                  = 03000;
  CO_SMIS_04000_Reclassificar                = 04000;
  CO_SMIS_05000_BaterSal                     = 05000;
  CO_SMIS_06000_PreRemolho                   = 06000;
  CO_SMIS_07000_PreDescarne                  = 07000;
  CO_SMIS_08000_Caleiro                      = 08000;
  CO_SMIS_09000_Empilhar                     = 09000;
  CO_SMIS_10000_Redescarne                   = 10000;
  CO_SMIS_11000_DivisaoTripa                 = 11000;
  CO_SMIS_12000_CompraTripa                  = 12000;
  CO_SMIS_13000_Classificar                  = 13000;
  CO_SMIS_14000_Reclassificar                = 14000;
  CO_SMIS_15000_Curtimento                   = 15000;
  CO_SMIS_16000_Empilhar                     = 16000;
  CO_SMIS_17000_Descansar                    = 17000;
  CO_SMIS_18000_Enxugar                      = 18000;
  CO_SMIS_19000_Descansar                    = 19000;
  CO_SMIS_20000_Medir                        = 20000;
  CO_SMIS_21000_Descansar                    = 21000;
  CO_SMIS_22000_Classificar                  = 22000;
  CO_SMIS_23000_Armazenar                    = 23000;
  CO_SMIS_24000_VendaCurtido                 = 24000;
  CO_SMIS_25000_CompraCurtido                = 25000;
  CO_SMIS_26000_Armazenar                    = 26000;
  CO_SMIS_27000_Medir                        = 27000;
  CO_SMIS_28000_Remolhar                     = 28000;
  CO_SMIS_29000_Empilhar                     = 29000;
  CO_SMIS_30000_Descansar                    = 30000;
  CO_SMIS_31000_Reclassificar                = 31000;
  CO_SMIS_32000_Descansar                    = 32000;
  CO_SMIS_33000_Medir                        = 33000;
  CO_SMIS_34000_Descansar                    = 34000;
  CO_SMIS_35000_DividirCurtido               = 35000;
  CO_SMIS_36000_Refilar                      = 36000;
  CO_SMIS_37000_Rebaixar                     = 37000;
  CO_SMIS_38000_Refilar                      = 38000;
  CO_SMIS_39000_WetStrech                    = 39000;
  CO_SMIS_40000_Refilar                      = 40000;
  CO_SMIS_41000_Recurtir                     = 41000;
  CO_SMIS_41500_RecurtirFase2                = 41500;
  CO_SMIS_42000_Empilhar                     = 42000;
  CO_SMIS_43000_Descansar                    = 43000;
  CO_SMIS_44000_Estirar                      = 44000;
  CO_SMIS_45000_WetStrech                    = 45000;
  CO_SMIS_46000_Aereo                        = 46000;
  CO_SMIS_47000_Pasting                      = 47000;
  CO_SMIS_48000_Vacuo                        = 48000;
  CO_SMIS_49000_Toggling                     = 49000;
  CO_SMIS_50000_TAIC                         = 50000;
  CO_SMIS_51000_CompraSemiAcabado            = 51000;
  CO_SMIS_52000_Classificar                  = 52000;
  CO_SMIS_53000_Armazenar                    = 53000;
  CO_SMIS_54000_Refilar                      = 54000;
  CO_SMIS_55000_Reclassificar                = 55000;
  CO_SMIS_56000_Reumectar                    = 56000;
  CO_SMIS_57000_Descansar                    = 57000;
  CO_SMIS_58000_Reclassificar                = 58000;
  CO_SMIS_59000_Mollissar                    = 59000;
  CO_SMIS_60000_Reclassificar                = 60000;
  CO_SMIS_61000_Armazenar                    = 61000;
  CO_SMIS_62000_Refilar                      = 62000;
  CO_SMIS_63000_Reclassificar                = 63000;
  CO_SMIS_64000_Stucco                       = 64000;
  CO_SMIS_65000_Descansar                    = 65000;
  CO_SMIS_66000_Lixar                        = 66000;
  CO_SMIS_67000_Refilar                      = 67000;
  CO_SMIS_68000_Armazenar                    = 68000;
  CO_SMIS_69000_CompraPreAcabado             = 69000;
  CO_SMIS_70000_Reclassificar                = 70000;
  //
  CO_SMIS_80000_CouroAcabado                 = 80000;
  //
  CO_SMIS_89990_Transferencia                = 89990;
  //
  CO_SMIS_99000_VENDA                        = 99000;
  CO_SMIS_99100_BaixaForcada                 = 99100;
  CO_SMIS_99200_BaixaExtravio                = 99200;


  CO_TXT_SMIS_01000_AjusteEstoque         = 'Ajuste de estoque';
  CO_TXT_SMIS_02000_Compra                = 'Entrada por compra';
  CO_TXT_SMIS_02250_EntradaPorRemessa     = 'Entrada por remessa';
  CO_TXT_SMIS_02500_EntradaPorDevolucao   = 'Entrada por devolu��o';
  CO_TXT_SMIS_02750_EntradaParaRetrabalho = 'Entrada para retrabalho';
  CO_TXT_SMIS_03000_Classificar           = 'Classificar salgado';
  CO_TXT_SMIS_04000_Reclassificar         = 'Reclassificar salgado';
  CO_TXT_SMIS_05000_BaterSal              = 'Bater sal';
  CO_TXT_SMIS_06000_PreRemolho            = 'Pr�-remolho';
  CO_TXT_SMIS_07000_PreDescarne           = 'Pr�-descarne';
  CO_TXT_SMIS_08000_Caleiro               = 'Caleiro';
  CO_TXT_SMIS_09000_Empilhar              = 'Empilhar caleirado';
  CO_TXT_SMIS_10000_Redescarne            = 'Redescarne';
  CO_TXT_SMIS_11000_DivisaoTripa          = 'Divis�o em tripa';
  CO_TXT_SMIS_12000_CompraTripa           = 'Compra de couro tripa';
  CO_TXT_SMIS_13000_Classificar           = 'Classificar tripa';
  CO_TXT_SMIS_14000_Reclassificar         = 'Reclassificar tripa';
  CO_TXT_SMIS_15000_Curtimento            = 'Curtimento';
  CO_TXT_SMIS_16000_Empilhar              = 'Empilhar curtido';
  CO_TXT_SMIS_17000_Descansar             = 'Descansar curtido';
  CO_TXT_SMIS_18000_Enxugar               = 'Enxugar';
  CO_TXT_SMIS_19000_Descansar             = 'Descansar enxugado';
  CO_TXT_SMIS_20000_Medir                 = 'Medir WB enxugado';
  CO_TXT_SMIS_21000_Descansar             = 'Descansar WB exugado e medido';
  CO_TXT_SMIS_22000_Classificar           = 'Classificar curtido';
  CO_TXT_SMIS_23000_Armazenar             = 'Armazenar curtido';
  CO_TXT_SMIS_24000_VendaCurtido          = 'Venda curtido';
  CO_TXT_SMIS_25000_CompraCurtido         = 'Compra curtido';
  CO_TXT_SMIS_26000_Armazenar             = 'Armazenar WB comprado';
  CO_TXT_SMIS_27000_Medir                 = 'Medir WB classificado ou comprado';
  CO_TXT_SMIS_28000_Remolhar              = 'Remolhar curtido';
  CO_TXT_SMIS_29000_Empilhar              = 'Empilhar curtido remolhado';
  CO_TXT_SMIS_30000_Descansar             = 'Descansar curtido remolhado';
  CO_TXT_SMIS_31000_Reclassificar         = 'Reclassificar curtido ';
  CO_TXT_SMIS_32000_Armazenar             = 'Armazenarar curtido reclassificado';
  CO_TXT_SMIS_33000_Medir                 = 'Medir curtido reclassificado';
  CO_TXT_SMIS_34000_Armazenar             = 'Armazenar curtido reclassificado';
  CO_TXT_SMIS_35000_DividirCurtido        = 'Dividir Curtido';
  CO_TXT_SMIS_36000_Refilar               = 'Refilar dividido ap�s curtido';
  CO_TXT_SMIS_37000_Rebaixar              = 'Rebaixar curtido';
  CO_TXT_SMIS_38000_Refilar               = 'Refilar rebaixe curtido';
  CO_TXT_SMIS_39000_WetStrech             = 'WetStrech dividido curtido';
  CO_TXT_SMIS_40000_Refilar               = 'Refilar antes de recurtir';
  CO_TXT_SMIS_41000_Recurtir              = 'Recurtir';
  CO_TXT_SMIS_41500_RecurtirFase2         = 'Recurtir segunda fase';
  CO_TXT_SMIS_42000_Empilhar              = 'Empilhar';
  CO_TXT_SMIS_43000_Descansar             = 'Descansar recurtido';
  CO_TXT_SMIS_44000_Estirar               = 'Estirar recurtido';
  CO_TXT_SMIS_45000_WetStrech             = 'WetStrech recurtido';
  CO_TXT_SMIS_46000_Aereo                 = 'Secar no Aereo';
  CO_TXT_SMIS_47000_Pasting               = 'Secar no Pasting';
  CO_TXT_SMIS_48000_Vacuo                 = 'Secar no Vacuo';
  CO_TXT_SMIS_49000_Toggling              = 'Secar no Toggling';
  CO_TXT_SMIS_50000_TAIC                  = 'Secar no TAIC';
  CO_TXT_SMIS_51000_CompraSemiAcabado     = 'Compra semi acabado';
  CO_TXT_SMIS_52000_Classificar           = 'Classificar crust';
  CO_TXT_SMIS_53000_Armazenar             = 'Armazenar crust';
  CO_TXT_SMIS_54000_Refilar               = 'Refilar crust 1';
  CO_TXT_SMIS_55000_Reclassificar         = 'Reclassificar crust';
  CO_TXT_SMIS_56000_Reumectar             = 'Reumectar crust';
  CO_TXT_SMIS_57000_Descansar             = 'Descansar crust reumectado';
  CO_TXT_SMIS_58000_Reclassificar         = 'Reclassificar crust reumectado ';
  CO_TXT_SMIS_59000_Mollissar             = 'Mollissar crust';
  CO_TXT_SMIS_60000_Reclassificar         = 'Reclassificar crust mollissado ';
  CO_TXT_SMIS_61000_Armazenar             = 'Armazenar crust';
  CO_TXT_SMIS_62000_Refilar               = 'Refilar crust 2';
  CO_TXT_SMIS_63000_Reclassificar         = 'Reclassificar pr�-acabado flor integral ';
  CO_TXT_SMIS_64000_Stucco                = 'Stuccar';
  CO_TXT_SMIS_65000_Descansar             = 'Descansar stuccado';
  CO_TXT_SMIS_66000_Lixar                 = 'Lixar crust';
  CO_TXT_SMIS_67000_Refilar               = 'Refilar ';
  CO_TXT_SMIS_68000_Armazenar             = 'Armazenar pr�-acabado';
  CO_TXT_SMIS_69000_CompraPreAcabado      = 'Compra pr�-acabado';
  CO_TXT_SMIS_70000_Reclassificar         = 'Reclassificar pr�-acabado lixado ';
  //
  CO_TXT_SMIS_80000_CouroAcabado          = 'Couro acabado';
  //
  CO_TXT_SMIS_89990_Transferencia         = 'Transfer�ncia de local';
  //
  CO_TXT_SMIS_99000_VENDA                 = 'Venda';
  CO_TXT_SMIS_99100_BaixaForcada          = 'Baixa for�ada';
  CO_TXT_SMIS_99200_BaixaExtravio         = 'Baixa por extravio';

  MaxV S_SMIS = 78;
  iV S_SMIS: array[0..MaxV S_SMIS] of Integer = (
CO_SMIS_01000_AjusteEstoque                ,
CO_SMIS_02000_Compra                       ,
CO_SMIS_02250_EntradaPorRemessa            ,
CO_SMIS_02500_EntradaPorDevolucao          ,
CO_SMIS_02750_EntradaParaRetrabalho        ,
CO_SMIS_03000_Classificar                  ,
CO_SMIS_04000_Reclassificar                ,
CO_SMIS_05000_BaterSal                     ,
CO_SMIS_06000_PreRemolho                   ,
CO_SMIS_07000_PreDescarne                  ,
CO_SMIS_08000_Caleiro                      ,
CO_SMIS_09000_Empilhar                     ,
CO_SMIS_10000_Redescarne                   ,
CO_SMIS_11000_DivisaoTripa                 ,
CO_SMIS_12000_CompraTripa                  ,
CO_SMIS_13000_Classificar                  ,
CO_SMIS_14000_Reclassificar                ,
CO_SMIS_15000_Curtimento                   ,
CO_SMIS_16000_Empilhar                     ,
CO_SMIS_17000_Descansar                    ,
CO_SMIS_18000_Enxugar                      ,
CO_SMIS_19000_Descansar                    ,
CO_SMIS_20000_Medir                        ,
CO_SMIS_21000_Descansar                    ,
CO_SMIS_22000_Classificar                  ,
CO_SMIS_23000_Armazenar                    ,
CO_SMIS_24000_VendaCurtido                 ,
CO_SMIS_25000_CompraCurtido                ,
CO_SMIS_26000_Armazenar                    ,
CO_SMIS_27000_Medir                        ,
CO_SMIS_28000_Remolhar                     ,
CO_SMIS_29000_Empilhar                     ,
CO_SMIS_30000_Descansar                    ,
CO_SMIS_31000_Reclassificar                ,
CO_SMIS_32000_Descansar                    ,
CO_SMIS_33000_Medir                        ,
CO_SMIS_34000_Descansar                    ,
CO_SMIS_35000_DividirCurtido               ,
CO_SMIS_36000_Refilar                      ,
CO_SMIS_37000_Rebaixar                     ,
CO_SMIS_38000_Refilar                      ,
CO_SMIS_39000_WetStrech                    ,
CO_SMIS_40000_Refilar                      ,
CO_SMIS_41000_Recurtir                     ,
CO_SMIS_41500_RecurtirFase2                ,
CO_SMIS_42000_Empilhar                     ,
CO_SMIS_43000_Descansar                    ,
CO_SMIS_44000_Estirar                      ,
CO_SMIS_45000_WetStrech                    ,
CO_SMIS_46000_Aereo                        ,
CO_SMIS_47000_Pasting                      ,
CO_SMIS_48000_Vacuo                        ,
CO_SMIS_49000_Toggling                     ,
CO_SMIS_50000_TAIC                         ,
CO_SMIS_51000_CompraSemiAcabado            ,
CO_SMIS_52000_Classificar                  ,
CO_SMIS_53000_Armazenar                    ,
CO_SMIS_54000_Refilar                      ,
CO_SMIS_55000_Reclassificar                ,
CO_SMIS_56000_Reumectar                    ,
CO_SMIS_57000_Descansar                    ,
CO_SMIS_58000_Reclassificar                ,
CO_SMIS_59000_Mollissar                    ,
CO_SMIS_60000_Reclassificar                ,
CO_SMIS_61000_Armazenar                    ,
CO_SMIS_62000_Refilar                      ,
CO_SMIS_63000_Reclassificar                ,
CO_SMIS_64000_Stucco                       ,
CO_SMIS_65000_Descansar                    ,
CO_SMIS_66000_Lixar                        ,
CO_SMIS_67000_Refilar                      ,
CO_SMIS_68000_Armazenar                    ,
CO_SMIS_69000_CompraPreAcabado             ,
CO_SMIS_70000_Reclassificar                ,
//
CO_SMIS_80000_CouroAcabado                 ,
  //
CO_SMIS_89990_Transferencia                ,
  //
CO_SMIS_99000_VENDA                        ,
CO_SMIS_99100_BaixaForcada                 ,
CO_SMIS_99200_BaixaExtravio
);

  sV S_SMIS: array[0..MaxV S_SMIS] of String = (
CO_TXT_SMIS_01000_AjusteEstoque         ,
CO_TXT_SMIS_02000_Compra                ,
CO_TXT_SMIS_02250_EntradaPorRemessa     ,
CO_TXT_SMIS_02500_EntradaPorDevolucao   ,
CO_TXT_SMIS_02750_EntradaParaRetrabalho ,
CO_TXT_SMIS_03000_Classificar           ,
CO_TXT_SMIS_04000_Reclassificar         ,
CO_TXT_SMIS_05000_BaterSal              ,
CO_TXT_SMIS_06000_PreRemolho            ,
CO_TXT_SMIS_07000_PreDescarne           ,
CO_TXT_SMIS_08000_Caleiro               ,
CO_TXT_SMIS_09000_Empilhar              ,
CO_TXT_SMIS_10000_Redescarne            ,
CO_TXT_SMIS_11000_DivisaoTripa          ,
CO_TXT_SMIS_12000_CompraTripa           ,
CO_TXT_SMIS_13000_Classificar           ,
CO_TXT_SMIS_14000_Reclassificar         ,
CO_TXT_SMIS_15000_Curtimento            ,
CO_TXT_SMIS_16000_Empilhar              ,
CO_TXT_SMIS_17000_Descansar             ,
CO_TXT_SMIS_18000_Enxugar               ,
CO_TXT_SMIS_19000_Descansar             ,
CO_TXT_SMIS_20000_Medir                 ,
CO_TXT_SMIS_21000_Descansar             ,
CO_TXT_SMIS_22000_Classificar           ,
CO_TXT_SMIS_23000_Armazenar             ,
CO_TXT_SMIS_24000_VendaCurtido          ,
CO_TXT_SMIS_25000_CompraCurtido         ,
CO_TXT_SMIS_26000_Armazenar             ,
CO_TXT_SMIS_27000_Medir                 ,
CO_TXT_SMIS_28000_Remolhar              ,
CO_TXT_SMIS_29000_Empilhar              ,
CO_TXT_SMIS_30000_Descansar             ,
CO_TXT_SMIS_31000_Reclassificar         ,
CO_TXT_SMIS_32000_Armazenar             ,
CO_TXT_SMIS_33000_Medir                 ,
CO_TXT_SMIS_34000_Armazenar             ,
CO_TXT_SMIS_35000_DividirCurtido        ,
CO_TXT_SMIS_36000_Refilar               ,
CO_TXT_SMIS_37000_Rebaixar              ,
CO_TXT_SMIS_38000_Refilar               ,
CO_TXT_SMIS_39000_WetStrech             ,
CO_TXT_SMIS_40000_Refilar               ,
CO_TXT_SMIS_41000_Recurtir              ,
CO_TXT_SMIS_41500_RecurtirFase2         ,
CO_TXT_SMIS_42000_Empilhar              ,
CO_TXT_SMIS_43000_Descansar             ,
CO_TXT_SMIS_44000_Estirar               ,
CO_TXT_SMIS_45000_WetStrech             ,
CO_TXT_SMIS_46000_Aereo                 ,
CO_TXT_SMIS_47000_Pasting               ,
CO_TXT_SMIS_48000_Vacuo                 ,
CO_TXT_SMIS_49000_Toggling              ,
CO_TXT_SMIS_50000_TAIC                  ,
CO_TXT_SMIS_51000_CompraSemiAcabado     ,
CO_TXT_SMIS_52000_Classificar           ,
CO_TXT_SMIS_53000_Armazenar             ,
CO_TXT_SMIS_54000_Refilar               ,
CO_TXT_SMIS_55000_Reclassificar         ,
CO_TXT_SMIS_56000_Reumectar             ,
CO_TXT_SMIS_57000_Descansar             ,
CO_TXT_SMIS_58000_Reclassificar         ,
CO_TXT_SMIS_59000_Mollissar             ,
CO_TXT_SMIS_60000_Reclassificar         ,
CO_TXT_SMIS_61000_Armazenar             ,
CO_TXT_SMIS_62000_Refilar               ,
CO_TXT_SMIS_63000_Reclassificar         ,
CO_TXT_SMIS_64000_Stucco                ,
CO_TXT_SMIS_65000_Descansar             ,
CO_TXT_SMIS_66000_Lixar                 ,
CO_TXT_SMIS_67000_Refilar               ,
CO_TXT_SMIS_68000_Armazenar             ,
CO_TXT_SMIS_69000_CompraPreAcabado      ,
CO_TXT_SMIS_70000_Reclassificar         ,
//
CO_TXT_SMIS_80000_CouroAcabado          ,
//
CO_TXT_SMIS_89990_Transferencia         ,
//
CO_TXT_SMIS_99000_VENDA                 ,
CO_TXT_SMIS_99100_BaixaForcada          ,
CO_TXT_SMIS_99200_BaixaExtravio
);

////////////////////////////////////////////////////////////////////////////////

  MaxV S_Rend = 7;
  CO_TXT_vsrtIndef    = 'Indef.';
  CO_TXT_vsrtAreaArea = '%';
  CO_TXT_vsrtPesoArea = 'kg/m�';
  CO_TXT_vsrtPesoPeso = '%';
  CO_TXT_vsrtAreaPeso = 'm�/kg';
  CO_TXT_vsrtPecaArea = 'p�/m�';
  CO_TXT_vsrtPecaPeso = 'p�/kg';
  CO_TXT_vsrtPecaPeca = '%';
sV S_Rend: array[0..MaxV S_Rend] of String = (
  CO_TXT_vsrtIndef    ,
  CO_TXT_vsrtAreaArea ,
  CO_TXT_vsrtPesoArea ,
  CO_TXT_vsrtPesoPeso ,
  CO_TXT_vsrtAreaPeso ,
  CO_TXT_vsrtPecaArea ,
  CO_TXT_vsrtPecaPeso ,
  CO_TXT_vsrtPecaPeca
);
}

////////////////////////////////////////////////////////////////////////////////

  Max_TXGrandeza = 3;
  CO_TXT_TXGrandezaIndf = 'ND';
  CO_TXT_TXGrandezaPeca = 'p�';
  CO_TXT_TXGrandezaArea = 'm�';
  CO_TXT_TXGrandezaPeso = 'kg';
  CO_TXT_TXGrandezaLivr = 'ND';
s_TXGrandeza: array[-1..Max_TXGrandeza] of String = (
  CO_TXT_TXGrandezaIndf,
  CO_TXT_TXGrandezaPeca,
  CO_TXT_TXGrandezaArea,
  CO_TXT_TXGrandezaPeso,
  CO_TXT_TXGrandezaLivr
);

{
  Max_GrandezaUnidMed = 7;
  CO_TXT_GrandezaUnidmedIndef    = 'Indef.';
  CO_TXT_GrandezaUnidmedPeca     = 'Pe�a';
  CO_TXT_GrandezaUnidmedAreaM2   = '�rea m�';
  CO_TXT_GrandezaUnidmedPesoKG   = 'Peso kg';
  CO_TXT_GrandezaUnidmedVolumeM3 = 'Volume m�';
  CO_TXT_GrandezaUnidmedLinearM  = 'Linear m';
  CO_TXT_GrandezaUnidmedOutros   = 'Outros???';
  CO_TXT_GrandezaUnidmedAreaP2   = '�rea ft�';
  CO_TXT_GrandezaUnidmedPesoTon  = 'Peso ton';
sGrandezaUnidMed: array[-1..Max_GrandezaUnidMed] of String = (
  CO_TXT_GrandezaUnidmedIndef,
  CO_TXT_GrandezaUnidmedPeca,
  CO_TXT_GrandezaUnidmedAreaM2,
  CO_TXT_GrandezaUnidmedPesoKG,
  CO_TXT_GrandezaUnidmedVolumeM3,
  CO_TXT_GrandezaUnidmedLinearM,
  CO_TXT_GrandezaUnidmedOutros,
  CO_TXT_GrandezaUnidmedAreaP2,
  CO_TXT_GrandezaUnidmedPesoTon
);

  Max_SPED_KPROD_SEGM = 11;
  CO_TXT_SPED_KPROD_SEGM_000_PreDesc    = 'Pr�-descarnado';
  CO_TXT_SPED_KPROD_SEGM_001_EmCaleiro  = 'Em Caleiro';
  CO_TXT_SPED_KPROD_SEGM_002_Caleado    = 'Caleado';
  CO_TXT_SPED_KPROD_SEGM_003_Tripa      = 'Couro Tripa';
  CO_TXT_SPED_KPROD_SEGM_004_EmCurt     = 'Em Curtimento';
  CO_TXT_SPED_KPROD_SEGM_005_Curtido    = 'Curtido';
  CO_TXT_SPED_KPROD_SEGM_006_GeracaoWB  = 'Gera��o de WB';
  CO_TXT_SPED_KPROD_SEGM_007_Opera��es  = 'Opera��es';
  CO_TXT_SPED_KPROD_SEGM_008_Recurt     = 'Recurtimento';
  CO_TXT_SPED_KPROD_SEGM_009_ProcSubPrd = 'Processo de subprodutos';
  CO_TXT_SPED_KPROD_SEGM_010_ReprocRepa = 'Reprocesso/reparo';
  CO_TXT_SPED_KPROD_SEGM_011_DiluiMistu = 'Dilui��o/mistura insumos';
sSPED_KPROD_SEGM: array[0..Max_SPED_KPROD_SEGM] of String = (
CO_TXT_SPED_KPROD_SEGM_000_PreDesc    ,
CO_TXT_SPED_KPROD_SEGM_001_EmCaleiro  ,
CO_TXT_SPED_KPROD_SEGM_002_Caleado    ,
CO_TXT_SPED_KPROD_SEGM_003_Tripa      ,
CO_TXT_SPED_KPROD_SEGM_004_EmCurt     ,
CO_TXT_SPED_KPROD_SEGM_005_Curtido    ,
CO_TXT_SPED_KPROD_SEGM_006_GeracaoWB  ,
CO_TXT_SPED_KPROD_SEGM_007_Opera��es  ,
CO_TXT_SPED_KPROD_SEGM_008_Recurt     ,
CO_TXT_SPED_KPROD_SEGM_009_ProcSubPrd ,
CO_TXT_SPED_KPROD_SEGM_010_ReprocRepa ,
CO_TXT_SPED_KPROD_SEGM_011_DiluiMistu
);
}
  Max_TX_IMP_ESTQ_ORD = 14;
  sMax_TX_IMP_ESTQ_ORD: array[0..Max_TX_IMP_ESTQ_ORD] of String = (
  'MP / Artigo',
  'Fornecedor',
  'Pallet',
  'IME-I',
  'Tal�o',
  'Marca',
  'Centro',
  'Local',
  'Tipo Material',
  'Est�gio Artigo',
  'NFe',
  'IME-C',
  'Movimento',
  'Cliente M.O.',
  'Fornecedor M.O.'
  );

////////////////////////////////////////////////////////////////////////////////

type
  //TTipoTroca = (trcOA, trcOsAs, trcPara);
  TUnAppListas = class(TObject)
    mySQLQuery1: TmySQLQuery;
  private
    { Private declarations }

  public
    { Public declarations }
    function  DefineNOMETipificacaoCouro(Setor, Tipificacao: Integer): String;
    procedure ConfiguraTipificacao(Setor: Variant; RGTipificacao: TRadioGroup;
              Colunas, Default: Integer);
    function  DefineTpCalcAutoCouro(Pecas, Peso, AreaM2, AreaP2: Boolean): Double;
  end;

var
  UnAppListas: TUnAppListas;

implementation

uses Module, DmkDAC_PF, ModuleGeral, UnMyObjects;

{ TUnAppListas }

procedure TUnAppListas.ConfiguraTipificacao(Setor: Variant;
  RGTipificacao: TRadioGroup; Colunas, Default: Integer);
begin
  RGTipificacao.Columns := Colunas;
  RGTipificacao.ItemIndex := Default;
  case Setor of
    -5:
    begin
      RGTipificacao.Items[0] := 'Bovino';
      RGTipificacao.Items[1] := 'Suino';
      RGTipificacao.Items[2] := 'Caprino';
      RGTipificacao.Items[3] := 'Bubalino';
      RGTipificacao.Items[4] := 'Ovino';
      RGTipificacao.Items[5] := 'Avestruz';
      RGTipificacao.Items[6] := 'Coelho';
      RGTipificacao.Items[7] := 'Jacar�';
      RGTipificacao.Items[8] := 'R�pteis';
      RGTipificacao.Items[9] := 'Outros';
    end;
    -4: // igual a -3 para impress�o de nf
    begin
      RGTipificacao.Items[00] := 'N/D';
      RGTipificacao.Items[01] := 'Peles frescas';
      RGTipificacao.Items[02] := 'Peles salgadas';
      RGTipificacao.Items[03] := 'Peles salmouradas';
      RGTipificacao.Items[04] := 'Peles Salmouradas e salgadas';
      RGTipificacao.Items[05] := 'Peles secas';
      RGTipificacao.Items[06] := 'Peles congeladas';
      RGTipificacao.Items[07] := 'Aparas';
      RGTipificacao.Items[08] := 'Outros';
      RGTipificacao.Items[09] := 'Peles frescas tratadas';
      RGTipificacao.Items[10] := 'Peles resfriadas';
      RGTipificacao.Items[11] := 'Peles tratadas e resfriadas';
    end;
    -3:
    begin
      RGTipificacao.Items[00] := 'N/D';
      RGTipificacao.Items[01] := 'Verde';
      RGTipificacao.Items[02] := 'Salgado';
      RGTipificacao.Items[03] := 'Salmourado';
      RGTipificacao.Items[04] := 'Salm.Salg.';
      RGTipificacao.Items[05] := 'Seco';
      RGTipificacao.Items[06] := 'Congelado';
      RGTipificacao.Items[07] := 'Aparas';
      RGTipificacao.Items[08] := 'Outros';
      RGTipificacao.Items[09] := 'Tratado';
      RGTipificacao.Items[10] := 'Resfriado';
      RGTipificacao.Items[11] := 'Tratado e resfriado';
    end;
    -2:
    begin
      RGTipificacao.Items[0] := 'N/D';
      RGTipificacao.Items[1] := 'Integral';
      RGTipificacao.Items[2] := 'Laminado';
      RGTipificacao.Items[3] := 'Repartido';
      RGTipificacao.Items[4] := 'Dividido';
      RGTipificacao.Items[5] := 'Rebaixado';
      RGTipificacao.Items[6] := 'Raspa';
      RGTipificacao.Items[7] := 'Outros';
      RGTipificacao.Items[8] := 'Tapete';
    end;
    -1:
    begin
      RGTipificacao.Items[0] := 'N/D';
      RGTipificacao.Items[1] := 'Rebaixado';
      RGTipificacao.Items[2] := 'Semi-acabado';
      RGTipificacao.Items[3] := 'Dividido';
      RGTipificacao.Items[4] := 'Outros';
      RGTipificacao.Items[5] := '';
      RGTipificacao.Items[6] := '';
      RGTipificacao.Items[7] := '';
      RGTipificacao.Items[8] := '';
    end;
    else
    begin
      RGTipificacao.Items[0] := '';
      RGTipificacao.Items[1] := '';
      RGTipificacao.Items[2] := '';
      RGTipificacao.Items[3] := '';
      RGTipificacao.Items[4] := '';
      RGTipificacao.Items[5] := '';
      RGTipificacao.Items[6] := '';
      RGTipificacao.Items[7] := '';
      RGTipificacao.Items[8] := '';
    end;
  end;
end;

function TUnAppListas.DefineNOMETipificacaoCouro(Setor,
  Tipificacao: Integer): String;
begin
  case Setor of
    -5:
    begin
      case Tipificacao of
        0: Result := 'Bovino';
        1: Result := 'Suino';
        2: Result := 'Caprino';
        3: Result := 'Bubalino';
        4: Result := 'Ovino';
        5: Result := 'Avestruz';
        6: Result := 'Coelho';
        7: Result := 'Jacar�';
        8: Result := 'R�pteis';
        9: Result := 'Outros';
      end;
    end;
    -4:
      case Tipificacao of
        0: Result := '';
        1: Result := '';
        2: Result := '';
        3: Result := '';
        4: Result := '';
        5: Result := '';
        6: Result := '';
        7: Result := '';
        8: Result := '';
        else Result := '*ERRO*';
      end;
    -3:
      case Tipificacao of
        0: Result := 'N/D';
        1: Result := 'Verde';
        2: Result := 'Salgado';
        3: Result := 'Salmourado';
        4: Result := 'Salm.Salg.';
        5: Result := 'Seco';
        6: Result := 'Congelado';
        7: Result := 'Aparas';
        8: Result := 'Outros';
        else Result := '*ERRO*';
      end;
    -2:
      case Tipificacao of
        0: Result := 'N/D';
        1: Result := 'Integral';
        2: Result := 'Laminado';
        3: Result := 'Repartido';
        4: Result := 'Dividido';
        5: Result := 'Rebaixado';
        6: Result := 'Raspa';
        7: Result := 'Outros';
        8: Result := 'Tapete';
        else Result := '*ERRO*';
      end;
    -1:
      case Tipificacao of
        0: Result := 'N/D';
        1: Result := 'Rebaixado';
        2: Result := 'Semi-acabado';
        3: Result := 'Dividido';
        4: Result := 'Outros';
        5: Result := '';
        6: Result := '';
        7: Result := '';
        8: Result := '';
        else Result := '*ERRO*';
      end;
    else Result := '*ERRO*SETOR*'
  end;
end;

function TUnAppListas.DefineTpCalcAutoCouro(Pecas, Peso, AreaM2,
  AreaP2: Boolean): Double;
begin
  Result := 0;
  if Pecas then
    Result := Result + 1;
  if Peso then
    Result := Result + 2;
  if AreaM2 then
    Result := Result + 4;
  if AreaP2 then
    Result := Result + 8;
end;

end.
