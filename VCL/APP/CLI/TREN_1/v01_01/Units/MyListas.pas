unit MyListas;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, UnMLAGeral, dmkGeral, UnDmkProcFunc, UnDmkEnums,
  UnProjGroup_Consts;

type
  TMyListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function CriaListaImpDOS(FImpDOS: TStringList): Boolean;
    function CriaListaUserSets(FUserSets: TStringList): Boolean;
    //
    function CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
    function CriaListaTabelas(Database: TmySQLDatabase; Lista:
             TList<TTabelas>): Boolean;
    //function CriaListaTabelas(Database: TmySQLDatabase; FTabelas: TStringList): Boolean;
    function CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
    function CriaListaIndices(TabelaBase, TabelaNome: String;
             FLIndices: TList<TIndices>): Boolean;
    function CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
             var TemControle: TTemControle): Boolean;
    function CriaListaQeiLnk(TabelaCria: String; FLQeiLnk: TList<TQeiLnk>): Boolean;
    function CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function ExcluiTab: Boolean;
    function ExcluiReg: Boolean;
    function ExcluiIdx: Boolean;
    procedure VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
              DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ConfiguracoesIniciais(UsaCoresRel: integer; AppIDtxt: String);
  end;

{ 2022-07-18
const
  CO_VERSAO = 1905011905; Vers�o c�digo recuperado Toolrent do Atlasian / Tortoise Git em 2022-07-16
  CO_VERSAO = 1908290622  Vers�o atual rodando na AAPA em 2022-07-18  visto no backup do BD da AAPA
  CO_VERNEW = 0;
  CO_SIGLA_APP = 'TREN';
  CO_DMKID_APP = 28; // 2012-07-31
  CO_GRADE_APP = False;
  CO_VLOCAL = True;
  CO_EXTRA_LCT_003 = False; // True somente para Credito2
  //
  CO_SIT_PATR_068_D_Disponivel = 68;
  CO_SIT_PATR_070_F_Furtado    = 70;
  CO_SIT_PATR_073_I_Inspecao   = 73;
  CO_SIT_PATR_076_L_Locado     = 76;
  CO_SIT_PATR_079_O_Oficina    = 79;
  CO_SIT_PATR_082_R_Reservado  = 82;
  CO_SIT_PATR_086_V_Vendido    = 86;
}
var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;
  //
  _ArrClieSets: array[01..32] of String;
  _MaxClieSets: Integer;
  _ArrFornSets: array[01..32] of String;
  _MaxFornSets: Integer;

implementation

uses MyDBCheck, Module, ModuleGeral, Geral_TbTerc, UnPerfJan_Tabs, UnEntities,
  UnFinance_Tabs, UnIBGE_DTB_Tabs, UnBloq_Tabs, UnBloqGerl_Tabs, UnCNAB_Tabs,
  UnProtocol_Tabs, UnGrade_Tabs, UnContrat_Tabs, NFe_Tabs, NFSe_Tabs, AppListas,
  NFSe_TbTerc, Mail_Tabs, UnAnotacoes_Tabs, UnEnti_Tabs, UnGFat_Tabs, UnALL_Tabs,
  UnMoedas_Tabs, UnFiscal_Tabs, UnEmpresas_Tabs, UnSrvL_Tabs, UnGraL_Tabs,
  UnEmpg_Tabs, FPMin_Tabs, UnAgenda_Tabs, UnImprime_Tabs, UnUMedi_Tabs,
  UnPraz_Tabs, SPED_EFD_Tabs;

function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista:
 TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    if Database = Dmod.MyDB then
    begin
      Bloq_Tabs.CarregaListaTabelas(Lista);
      BloqGerl_Tabs.CarregaListaTabelas(Database, Lista);
      CNAB_Tabs.CarregaListaTabelas(Database, Lista);
      Protocol_Tabs.CarregaListaTabelas(Database, Lista);
      PerfJan_Tabs.CarregaListaTabelas(Lista);
      Finance_Tabs.CarregaListaTabelas(Lista);
      Finance_Tabs.ComplementaListaComLcts(Lista);
      ////CashTb.CarregaListaTabelasCashier(Lista);
      //CashTb.ComplementaListaComLcts(Lista);
      Grade_Tabs.CarregaListaTabelas(FTabelas);
      Moedas_Tabs.CarregaListaTabelas(FTabelas);
      Empresas_Tabs.CarregaListaTabelas(FTabelas);
      Fiscal_Tabs.CarregaListaTabelas(FTabelas);
      ALL_Tabs.CarregaListaTabelas(FTabelas);
      Anotacoes_Tabs.CarregaListaTabelas(FTabelas);
      GFat_Tabs.CarregaListaTabelas(FTabelas);
      Enti_Tabs.CarregaListaTabelas(FTabelas);
      Contrat_Tabs.CarregaListaTabelas(Lista);
      NFe_Tb.CarregaListaTabelas(Database, Lista);
      UnNFSe_Tabs.CarregaListaTabelas(Database.Name, Lista);
      Mail_Tb.CarregaListaTabelas(Lista);
      SrvL_Tabs.CarregaListaTabelas(Lista);
      GraL_Tabs.CarregaListaTabelas(Lista);
      Empg_Tabs.CarregaListaTabelas(Lista);
      UnFPMin_Tabs.CarregaListaTabelas(Lista);
      Agenda_Tabs.CarregaListaTabelas(FTabelas);
      Imprime_Tabs.CarregaListaTabelas(FTabelas);
      UMedi_Tabs.CarregaListaTabelas(Database, FTabelas);
      Praz_Tabs.CarregaListaTabelas(Database, FTabelas);
      SPEDEFD_Tb.CarregaListaTabelas(Database, FTabelas);
      //
      MyLinguas.AdTbLst(Lista, False, 'controle', '');

      MyLinguas.AdTbLst(Lista, False, LowerCase('OpcoesTRen'), '');
      //
    end
    else if Database = DModG.AllID_DB then
    begin
      IBGE_DTB_Tabs.CarregaListaTabelas(FTabelas);
      //UnNFSe_TbTerc.CarregaListaTabelas(Lista);
      UnGeral_TbTerc.CarregaListaTabelas(Lista);
      NFe_Tb.CarregaListaTabelas(Database, FTabelas);
      SPEDEFD_Tb.CarregaListaTabelas(Database, FTabelas);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    //CashTb.CarregaListaTabelasLocaisCashier(Lista);
    Finance_Tabs.CarregaListaTabelasLocais(Lista);
    //FtabelasLocais.Add('');
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaQeiLnk(TabelaCria: String;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
  Result := True;
  try
(*
    if Uppercase(TabelaCria) = Uppercase('VSEntiMP') then
    begin
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'Codigo';
      FRQeiLnk.RplyTab       := '';
      FRQeiLnk.RplyCol       := '';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpUSGSrvrInc;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'MPVImpOpc';
      FRQeiLnk.RplyTab       := 'MPVImpOpc';
      FRQeiLnk.RplyCol       := 'Codigo';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpRelatUsrSrv;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      //
    //end else if Uppercase(TabelaBase) = Uppercase('?) then
    //begin
    end else
*)
    begin
      All_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Enti_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      PerfJan_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Finance_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      IBGE_DTB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      NFe_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Grade_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Mail_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UnGeral_TbTerc.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
      Imprime_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Praz_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
  	  Moedas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Fiscal_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      GFat_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      CNAB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Anotacoes_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Empresas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UMedi_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //Tributos_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
      SPEDEFD_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UnNFSe_TbTerc.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('GraGruY') then
    begin
      FListaSQL.Add('Codigo|Tabela|Nome|Ordem');
      FListaSQL.Add('0|""|"(N�o atrelado)"|0');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1024_TXCadNat) +'|"txcadnat"|"Mat�ria-prima"|1024');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_2048_TXCadOpe) +'|"txcadope"|"Artigo em confec��o"|2048');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_6144_TXCadFcc) +'|"txcadfcc"|"Artigo confeccionado"|6144');
    end else
    if Uppercase(Tabela) = Uppercase('CartaG') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('-999|"* CONTRATO ANTERIOR *"');
    end else
    if Uppercase(Tabela) = Uppercase('GraGLSitu') then
    begin
      FListaSQL.Add('Codigo|Aplicacao|Sigla|Nome');
      FListaSQL.Add(Geral.FF0(CO_SIT_PATR_068_D_Disponivel) + '|1|"D"|"Dispon�vel"');
      FListaSQL.Add(Geral.FF0(CO_SIT_PATR_070_F_Furtado   ) + '|0|"F"|"Furtado"');
      FListaSQL.Add(Geral.FF0(CO_SIT_PATR_073_I_Inspecao  ) + '|0|"I"|"Inspe��o"');
      FListaSQL.Add(Geral.FF0(CO_SIT_PATR_076_L_Locado    ) + '|0|"L"|"Locado"');
      FListaSQL.Add(Geral.FF0(CO_SIT_PATR_079_O_Oficina   ) + '|0|"O"|"Oficina"');
      FListaSQL.Add(Geral.FF0(CO_SIT_PATR_082_R_Reservado ) + '|0|"R"|"Reservado"');
      FListaSQL.Add(Geral.FF0(CO_SIT_PATR_086_V_Vendido   ) + '|0|"V"|"Vendido"');
    end else
    if Uppercase(Tabela) = Uppercase('OpcoesTRen') then
    begin
      FListaSQL.Add('Codigo|DefContrat');
      FListaSQL.Add('1|-999');
    end;
    //
    Bloq_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    BloqGerl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    CNAB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Protocol_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Finance_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    Fiscal_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Anotacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    ALL_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    GFat_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //CashTb.CarregaListaSQLCashier(Tabela, FListaSQL);
    IBGE_DTB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Grade_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    Contrat_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    NFe_Tb.CarregaListaSQL(Tabela, FListaSQL);
    UnNFSe_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Mail_Tb.CarregaListaSQL(Tabela, FListaSQL);
    SrvL_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    GraL_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Empg_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UnFPMin_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Agenda_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Imprime_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UMedi_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    Praz_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //
    SPEDEFD_Tb.CarregaListaSQL(Tabela, FListaSQL);
    UnNFSe_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
    UnGeral_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Contas') then
    begin
      (*FListaSQL.Add('-129,"Compra de mercadorias diversas"');
      FListaSQL.Add('-130,"Frete de mercadorias"');
      FListaSQL.Add('-131,"Compra de filmes"');
      FListaSQL.Add('-132,"Frete de filmes"');
      FListaSQL.Add('-133,"Inv�lido"');
      FListaSQL.Add('-134,"Venda e/ou loca��o"');*)
    end;
    Bloq_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    BloqGerl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    BloqGerl_Tabs.CarregaListaFRIndices(Tabela, FRIndices, FLIndices);
    CNAB_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Protocol_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Fiscal_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Finance_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Anotacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ALL_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    GFat_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Contrat_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    NFe_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    UnNFSe_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Mail_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    SrvL_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    GraL_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Empg_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnFPMin_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Agenda_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Imprime_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UMedi_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Praz_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    //
    UnNFSe_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    UnGeral_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    //end else
    if Uppercase(TabelaBase) = Uppercase('OpcoesTRen') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    begin
      Bloq_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      BloqGerl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      CNAB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Protocol_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      PerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Fiscal_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Empresas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Finance_Tabs.CarregaListaFRIndices(TabelaBase, TabelaNome, FRIndices, FLIndices);
      GFat_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Moedas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      ALL_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //CashTb.CarregaListaFRIndicesCashier(TabelaBase, TabelaNome, FRIndices, FLindices);
      IBGE_DTB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Anotacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Enti_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Grade_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Contrat_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      NFe_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnNFSe_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Mail_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      SrvL_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      GraL_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Empg_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnFPMin_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Agenda_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Imprime_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UMedi_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Praz_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //
      UnNFSe_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      SPEDEFD_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      UnGeral_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.ConfiguracoesIniciais(UsaCoresRel: integer;
  AppIDtxt: String);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
  if Uppercase(AppIDtxt) = 'TOOLRENT' then
  begin
    Entities.ConfiguracoesIniciaisEntidades(CO_DMKID_APP);
  end else
    Geral.MB_Aviso(
    'Database para configura��es de "CheckBox" n�o definidos!');
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
var TemControle: TTemControle): Boolean;
begin
  try
(*
    if Uppercase(Tabela) = Uppercase('controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'xxxx';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
*)
    if Uppercase(Tabela) = Uppercase('OpcoesTRen') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DefContrat';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraNivPatr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraCodPatr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraNivOutr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraCodOutr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BloPrdSPer';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipCodPatr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipCodOutr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    end else
    begin
      Bloq_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      BloqGerl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      CNAB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Protocol_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Empresas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      PerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Finance_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Moedas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Fiscal_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      GFat_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //CashTb.CarregaListaFRCamposCashier(Tabela, FRCampos, FLCampos, TemControle);
      IBGE_DTB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      ALL_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Anotacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Enti_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Grade_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Contrat_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      NFe_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnNFSe_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Mail_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      SrvL_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      GraL_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Empg_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnFPMin_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Agenda_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Imprime_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UMedi_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Praz_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //
      UnNFSe_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      SPEDEFD_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      UnGeral_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    Bloq_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    BloqGerl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    CNAB_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Protocol_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    PerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Finance_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Anotacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Empresas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Fiscal_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Moedas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enti_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    ALL_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    GFat_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //CashTb.CompletaListaFRCamposCashier(Tabela, FRCampos, FLCampos);
    Grade_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Contrat_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    NFe_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnNFSe_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Mail_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    SrvL_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    GraL_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Empg_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnFPMin_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Agenda_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Imprime_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UMedi_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Praz_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    UnNFSe_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnGeral_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.ExcluiTab: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiReg: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiIdx: Boolean;
begin
  Result := True;
end;

function TMyListas.CriaListaUserSets(FUserSets: TStringList): Boolean;
begin
  Result := True;
  try
    FUserSets.Add('Edi��o de Itens de Mercadoria;C�digos Fiscais');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Representante');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Vendedor');
    FUserSets.Add('Edi��o de Itens de Mercadoria;IPI');
    FUserSets.Add('Edi��o de Itens de Mercadoria;ICMS');
    //
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaImpDOS(FImpDOS: TStringList): Boolean;
begin
  Result := True;
  try
    FImpDOS.Add('1001;01;NF (Topo)');
    FImpDOS.Add('1002;01;Sa�da [X]');
    FImpDOS.Add('1003;01;Entrada [X]');
    FImpDOS.Add('1004;01;Data emiss�o');
    FImpDOS.Add('1005;01;Data entra/sai');
    FImpDOS.Add('1006;01;C�digo CFOP');
    FImpDOS.Add('1007;01;Descri��o CFOP');
    FImpDOS.Add('1008;01;Base c�lculo ICMS');
    FImpDOS.Add('1009;01;Valor ICMS');
    FImpDOS.Add('1010;01;Base c�lc. ICMS subst.');
    FImpDOS.Add('1011;01;Valor ICMS subst.');
    FImpDOS.Add('1012;01;Valor frete');
    FImpDOS.Add('1013;01;Valor seguro');
    FImpDOS.Add('1014;01;Outras desp. aces.');
    FImpDOS.Add('1015;01;Valor total IPI');
    FImpDOS.Add('1016;01;Valor total produtos');
    FImpDOS.Add('1017;01;Valor total servicos');
    FImpDOS.Add('1018;01;Valor total nota');
    FImpDOS.Add('1019;01;Placa ve�culo');
    FImpDOS.Add('1020;01;UF placa ve�culo');
    FImpDOS.Add('1021;01;Vol. Quantidade');
    FImpDOS.Add('1022;01;Vol. Esp�cie');
    FImpDOS.Add('1023;01;Vol. Marca');
    FImpDOS.Add('1024;01;Vol. N�mero');
    FImpDOS.Add('1025;01;Vol. kg bruto');
    FImpDOS.Add('1026;01;Vol. kg l�quido');
    FImpDOS.Add('1027;01;Dados adicionais');
    FImpDOS.Add('1028;01;Frete por conta de ...');
    FImpDOS.Add('1029;01;Desconto especial');
    FImpDOS.Add('1030;01;NF (rodap�)');
    //
    FImpDOS.Add('2001;02;Nome ou Raz�o Social');
    FImpDOS.Add('2002;02;CNPJ ou CPF');
    FImpDOS.Add('2003;02;Endere�o');
    FImpDOS.Add('2004;02;Bairro');
    FImpDOS.Add('2005;02;CEP');
    FImpDOS.Add('2006;02;Cidade');
    FImpDOS.Add('2007;02;Telefone');
    FImpDOS.Add('2008;02;UF');
    FImpDOS.Add('2009;02;I.E. ou RG');
    FImpDOS.Add('2010;02;I.E.S.T.');
    //
    FImpDOS.Add('6001;06;Nome ou Raz�o Social');
    FImpDOS.Add('6002;06;CNPJ ou CPF');
    FImpDOS.Add('6003;06;Endere�o');
    FImpDOS.Add('6004;06;Bairro');
    FImpDOS.Add('6005;06;CEP');
    FImpDOS.Add('6006;06;Cidade');
    FImpDOS.Add('6007;06;Telefone');
    FImpDOS.Add('6008;06;UF');
    FImpDOS.Add('6009;06;I.E. ou RG');
    FImpDOS.Add('6010;06;I.E.S.T.');
    //
    FImpDOS.Add('3001;03;Descri��o');
    FImpDOS.Add('3002;03;Classifica��o Fiscal');
    FImpDOS.Add('3003;03;Situa��o Tribut�ria');
    FImpDOS.Add('3004;03;Unidade');
    FImpDOS.Add('3005;03;Quantidade');
    FImpDOS.Add('3006;03;Valor unit�rio');
    FImpDOS.Add('3007;03;Valor total');
    FImpDOS.Add('3008;03;Aliquota ICMS');
    FImpDOS.Add('3009;03;Aliquota IPI');
    FImpDOS.Add('3010;03;Valor IPI');
    FImpDOS.Add('3011;03;CFOP');
    FImpDOS.Add('3012;03;Refer�ncia');
    //
    FImpDOS.Add('4001;04;Descri��o');
    FImpDOS.Add('4002;04;Classifica��o Fiscal');
    FImpDOS.Add('4003;04;Situa��o Tribut�ria');
    FImpDOS.Add('4004;04;Unidade');
    FImpDOS.Add('4005;04;Quantidade');
    FImpDOS.Add('4006;04;Valor unit�rio');
    FImpDOS.Add('4007;04;Valor total');
    FImpDOS.Add('4008;04;Aliquota ICMS');
    FImpDOS.Add('4009;04;Aliquota IPI');
    FImpDOS.Add('4010;04;Valor IPI');
    FImpDOS.Add('4011;04;CFOP');
    FImpDOS.Add('4012;04;Refer�ncia');
    //
    FImpDOS.Add('5001;05;Parcela');
    FImpDOS.Add('5002;05;Valor');
    FImpDOS.Add('5003;05;Vencimento');
    //
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  //
end;

procedure TMyListas.VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

procedure TMyListas.ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
  DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  Result := True;
  //
  // FER-OPCAO-002 :: Op��es Espec�ficas do Aplicativo
  New(FRJanelas);
  FRJanelas.ID        := 'FER-OPCAO-002';
  FRJanelas.Nome      := 'FmOpcoeTRen';
  FRJanelas.Descricao := 'Op��es Espec�ficas do Aplicativo';
  FLJanelas.Add(FRJanelas);
  //
  Bloq_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  BloqGerl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CNAB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Protocol_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  PerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Empresas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Finance_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Fiscal_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Anotacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Moedas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  ALL_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  GFat_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Enti_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  IBGE_DTB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Grade_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Contrat_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  NFe_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnNFSe_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Mail_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SrvL_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  GraL_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Empg_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnFPMin_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Agenda_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Imprime_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UMedi_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Praz_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  UnNFSe_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SPEDEFD_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnGeral_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
end;

end.
