unit UnApp_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, Variants, UnAppEnums;

type
  TUnApp_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    // Compatibilidade
    procedure MostraFormUsoEConsumo();
    procedure MostraFormMateriaPrima();

  end;

var
  App_Jan: TUnApp_Jan;


implementation

uses
  MyDBCheck, Module, CfgCadLista, UnGrade_Jan;

{ TUnApp_Jan }


procedure TUnApp_Jan.MostraFormMateriaPrima;
begin
  Grade_Jan.MostraFormGraGruY(0, 0, '');
end;

procedure TUnApp_Jan.MostraFormUsoEConsumo;
begin

end;

end.
