object FmDB_Converte_Multi: TFmDB_Converte_Multi
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Convers'#227'o de Banco de Dados'
  ClientHeight = 610
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 899
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 357
        Height = 31
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 357
        Height = 31
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 357
        Height = 31
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 993
    Height = 438
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 993
      Height = 438
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 993
        Height = 438
        Align = alClient
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 989
          Height = 421
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 0
          ExplicitTop = 18
          ExplicitHeight = 418
          object TabSheet1: TTabSheet
            Caption = 'Tabelas DBF'
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 981
              Height = 393
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitWidth = 983
              ExplicitHeight = 397
              object Panel6: TPanel
                Left = 0
                Top = 0
                Width = 981
                Height = 393
                Align = alClient
                TabOrder = 0
                ExplicitWidth = 983
                ExplicitHeight = 397
                object PageControl2: TPageControl
                  Left = 1
                  Top = 1
                  Width = 979
                  Height = 391
                  ActivePage = TabSheet2
                  Align = alClient
                  TabOrder = 0
                  ExplicitHeight = 385
                  object TabSheet2: TTabSheet
                    Caption = 'Registros'
                    object DBGrid1: TDBGrid
                      Left = 0
                      Top = 221
                      Width = 971
                      Height = 142
                      Align = alClient
                      DataSource = DsDBF
                      TabOrder = 0
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                    end
                    object Panel7: TPanel
                      Left = 0
                      Top = 0
                      Width = 971
                      Height = 221
                      Align = alTop
                      TabOrder = 1
                      object GroupBox2: TGroupBox
                        Left = 1
                        Top = 1
                        Width = 68
                        Height = 218
                        Align = alLeft
                        Caption = ' C'#243'digos ini: '
                        TabOrder = 0
                        object Panel10: TPanel
                          Left = 2
                          Top = 14
                          Width = 64
                          Height = 203
                          Align = alClient
                          BevelOuter = bvNone
                          ParentBackground = False
                          TabOrder = 0
                          object Label3: TLabel
                            Left = 2
                            Top = 4
                            Width = 35
                            Height = 13
                            Caption = 'Cliente:'
                          end
                          object Label4: TLabel
                            Left = 2
                            Top = 43
                            Width = 57
                            Height = 13
                            Caption = 'Fornecedor:'
                          end
                          object Label5: TLabel
                            Left = 2
                            Top = 82
                            Width = 45
                            Height = 13
                            Caption = 'Entidade:'
                          end
                          object EdCliCodIni: TdmkEdit
                            Left = 1
                            Top = 20
                            Width = 59
                            Height = 20
                            Alignment = taRightJustify
                            TabOrder = 0
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '10001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 10001
                            ValWarn = False
                          end
                          object EdForCodIni: TdmkEdit
                            Left = 1
                            Top = 59
                            Width = 59
                            Height = 21
                            Alignment = taRightJustify
                            TabOrder = 1
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '20001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 20001
                            ValWarn = False
                          end
                          object EdEntCodIni: TdmkEdit
                            Left = 1
                            Top = 98
                            Width = 59
                            Height = 21
                            Alignment = taRightJustify
                            TabOrder = 2
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '30001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 30001
                            ValWarn = False
                          end
                        end
                      end
                      object CGDBF: TdmkCheckGroup
                        Left = 69
                        Top = 1
                        Width = 901
                        Height = 218
                        Align = alClient
                        Caption = ' Tabelas: '
                        Columns = 2
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -12
                        Font.Name = 'Courier New'
                        Font.Style = []
                        Items.Strings = (
                          'CADBCO        20 [SIM]             (Cadastro de Carteiras)'
                          'CADCOM       275 [NAO] 2002 / 2005 (???)'
                          'CADCLI      4852 [SIM]             (Cadastro de Clientes)'
                          'CADCSI      2779 [NAO] 2002 / 2007 (??/)'
                          'CADCTR     11381 [   ] 2002 / .... (Contratos?)'
                          'CADFOR      1313 [SIM]             (Cadastro Fornecedores)'
                          'CADGRU        85 [   ]             (Grup. equip. - 4 n'#237'v.)'
                          'CADIMP         5 [   ]             (% de Impostos)'
                          'CADMAT     30758 [   ]             (Materiais [????])'
                          'CADNAT        92 [   ]             (Plano de contas???)'
                          'CADNOP        15 [NAO]             (Natureza opera'#231#227'o)'
                          'CADORC      2764 [NAO] 2002 / 2007 (Orcamento)'
                          'CADPAT       398 [   ] 1999 / .... (PATRIMONIO)'
                          'CADPED      1367 [NAO] 2002 / 2007 (Pedidos???)'
                          'CADPGO       106 [   ]             (Cond. pagto)'
                          'CADRAM        26 [   ]             (Ramos de atividade)'
                          'CADREP         0                                      '
                          'CADREQ         0                                     '
                          'CADSIT        15 [   ]             (Situa'#231#227'o???)'
                          'CADVDE         7 [   ]             (Vendedores)'
                          'CTRAUX     15731 [   ] 2002 / .... (tab aux contrato [NFe?])'
                          'PAGDUP      8013 [   ] 2002 / .... (Contas a pagar???)'
                          'RECDUP     16388 [   ] 2002 / .... (Contas a receber ???)')
                        ParentFont = False
                        TabOrder = 1
                        OnClick = CGDBFClick
                        OnDblClick = CGDBFDblClick
                        UpdType = utYes
                        Value = 0
                        OldValor = 0
                      end
                    end
                  end
                  object TabSheet3: TTabSheet
                    Caption = 'SQL'
                    ImageIndex = 1
                    object Splitter1: TSplitter
                      Left = 0
                      Top = 84
                      Width = 971
                      Height = 5
                      Cursor = crVSplit
                      Align = alTop
                      ExplicitWidth = 975
                    end
                    object Panel9: TPanel
                      Left = 0
                      Top = 0
                      Width = 971
                      Height = 84
                      Align = alTop
                      TabOrder = 0
                      ExplicitWidth = 975
                      object MeSQL1: TMemo
                        Left = 1
                        Top = 1
                        Width = 837
                        Height = 82
                        Align = alClient
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -14
                        Font.Name = 'Courier New'
                        Font.Style = []
                        Lines.Strings = (
                          'SELECT * FROM CADCLI'
                          '')
                        ParentFont = False
                        TabOrder = 0
                        OnKeyDown = MeSQL1KeyDown
                        ExplicitWidth = 841
                      end
                      object Panel8: TPanel
                        Left = 838
                        Top = 1
                        Width = 132
                        Height = 82
                        Align = alRight
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 1
                        ExplicitLeft = 842
                        object BtExecuta: TBitBtn
                          Tag = 14
                          Left = 8
                          Top = 4
                          Width = 118
                          Height = 39
                          Caption = '&Executa'
                          NumGlyphs = 2
                          TabOrder = 0
                          OnClick = BtExecutaClick
                        end
                      end
                    end
                    object DBGrid2: TDBGrid
                      Left = 0
                      Top = 89
                      Width = 971
                      Height = 274
                      Align = alClient
                      DataSource = DsPsq
                      TabOrder = 1
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                    end
                  end
                  object TabSheet4: TTabSheet
                    Caption = 'Erros Endere'#231'os'
                    ImageIndex = 2
                    object Memo1: TMemo
                      Left = 0
                      Top = 0
                      Width = 971
                      Height = 363
                      Align = alClient
                      TabOrder = 0
                    end
                  end
                  object TabSheet5: TTabSheet
                    Caption = 'Erros Loca'#231#245'es'
                    ImageIndex = 3
                    object Memo2: TMemo
                      Left = 0
                      Top = 0
                      Width = 971
                      Height = 363
                      Align = alClient
                      TabOrder = 0
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 485
    Width = 993
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 988
      Height = 41
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 24
        Width = 989
        Height = 16
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 542
    Width = 993
    Height = 68
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 849
      Top = 14
      Width = 141
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 14
      Width = 847
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 264
        Top = 4
        Width = 63
        Height = 13
        Caption = 'Registros Tb:'
      end
      object Label2: TLabel
        Left = 354
        Top = 4
        Width = 47
        Height = 13
        Caption = 'Registros:'
      end
      object Label6: TLabel
        Left = 154
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Tabela:'
      end
      object SpeedButton1: TSpeedButton
        Left = 232
        Top = 21
        Width = 23
        Height = 21
        Caption = '>'
        OnClick = SpeedButton1Click
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object EdRegistrosTb: TdmkEdit
        Left = 264
        Top = 20
        Width = 78
        Height = 20
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdRegistrosQr: TdmkEdit
        Left = 354
        Top = 20
        Width = 80
        Height = 20
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdTabela: TdmkEdit
        Left = 154
        Top = 20
        Width = 78
        Height = 20
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'CADCLI.DBF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'CADCLI.DBF'
        ValWarn = False
        OnChange = EdTabelaChange
      end
      object Button1: TButton
        Left = 551
        Top = 12
        Width = 75
        Height = 25
        Caption = 'Situa'#231#227'o'
        TabOrder = 4
        OnClick = Button1Click
      end
    end
  end
  object DsDBF: TDataSource
    Left = 496
    Top = 12
  end
  object DsCadCli: TDataSource
    Left = 556
    Top = 12
  end
  object DsPsq: TDataSource
    Left = 468
    Top = 40
  end
  object QrPsqEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE Antigo=:P0')
    Left = 496
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPsqEntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCadFor: TDataSource
    Left = 556
    Top = 40
  end
  object DsCadGru: TDataSource
    Left = 612
    Top = 12
  end
  object DataSource1: TDataSource
    Left = 612
    Top = 40
  end
  object QrExiste: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1 ID'
      'FROM gragru1'
      'WHERE Referencia=:P0')
    Left = 320
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraGruX, AGRPAT'
      'FROM gragxpatr ')
    Left = 364
    Top = 380
    object QrGraGXPatrAGRPAT: TWideStringField
      FieldName = 'AGRPAT'
      Required = True
      Size = 25
    end
    object QrGraGXPatrGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DataSource2: TDataSource
    Left = 668
    Top = 40
  end
  object DsCadVde: TDataSource
    Left = 668
    Top = 12
  end
  object QrGraFabMar: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM grafabmar'
      'WHERE Nome=:P0')
    Left = 408
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraFabMarControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSituacao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Situacao, '
      'CHAR(Situacao) SIGLA,'
      'COUNT(Situacao) ITENS'
      'FROM gragxpatr'
      'GROUP BY Situacao'
      'ORDER BY ITENS DESC')
    Left = 444
    Top = 380
    object QrSituacaoSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrSituacaoSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Required = True
      Size = 4
    end
    object QrSituacaoITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DataSource3: TDataSource
    Left = 724
    Top = 12
  end
  object DataSource4: TDataSource
    Left = 724
    Top = 40
  end
  object DataSource5: TDataSource
    Left = 780
    Top = 12
  end
  object DataSource6: TDataSource
    Left = 780
    Top = 40
  end
end
