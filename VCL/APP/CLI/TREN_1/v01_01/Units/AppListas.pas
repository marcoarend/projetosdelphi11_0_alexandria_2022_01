unit AppListas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, UnMyLinguas, UnInternalConsts, dmkGeral, DB,
  mySQLDbTables, UnDmkProcFunc, UnDmkEnums;

const
  //
  CO_TXT_pfcIndefinido       = 'Indefinido';
  CO_TXT_pfcValorTotal       = 'Valor total';
  CO_TXT_pfcValorUnitario    = 'Valor unit�rio';
  MaxPrmFatorValor = Integer(High(TPrmFatorValor));
  sPrmFatorValor: array[0..MaxPrmFatorValor] of string = (
  CO_TXT_pfcIndefinido       ,
  CO_TXT_pfcValorTotal       ,
  CO_TXT_pfcValorUnitario
  );
  //
  CO_TXT_pfrIndefinido       = 'Indefinido';
  CO_TXT_pfrValorTotal       = 'Valor total';
  CO_TXT_pfrValorPorPessoa   = 'Valor por pessoa';
  MaxPrmFormaRateio = Integer(High(TPrmFormaRateio));
  sPrmFormaRateio: array[0..MaxPrmFormaRateio] of string = (
  CO_TXT_pfrIndefinido       ,
  CO_TXT_pfrValorTotal       ,
  CO_TXT_pfrValorPorPessoa
  );
  //
  CO_TXT_slcdUnknownToInf    = 'A informar';
  CO_TXT_slcdNoGerCtaAPag    = 'N�o gerar contas a pagar';
  CO_TXT_slcdDescoFaturam    = 'Descontar do faturamento';
  CO_TXT_slcdGeraCtaAPagr    = 'Gerar contas a pagar';
  MaxSrvLCtbDsp = Integer(High(TSrvLCtbDsp));
  sSrvLCtbDsp: array[0..MaxSrvLCtbDsp] of string = (
  CO_TXT_slcdUnknownToInf   ,
  CO_TXT_slcdNoGerCtaAPag   ,
  CO_TXT_slcdDescoFaturam   ,
  CO_TXT_slcdGeraCtaAPagr
  );

    // Tabelas GraGruY
  CO_GraGruY_1024_TXCadNat = 1024;
  (*CO_GraGruY_1195_TXNatPDA = 1195;
  CO_GraGruY_1365_TXProCal = 1365;
  CO_GraGruY_1536_TXCouCal = 1536;
  CO_GraGruY_1621_TXCouDTA = 1621;
  CO_GraGruY_1707_TXProCur = 1707;
  CO_GraGruY_1877_TXCouCur = 1877;*)
  CO_GraGruY_2048_TXCadOpe = 2048;
  (*CO_GraGruY_3072_TXRibCla = 3072;
  CO_GraGruY_4096_TXRibOpe = 4096;
  (*CO_GraGruY_5120_TXWetEnd = 5120;*)
  CO_GraGruY_6144_TXCadFcc = 6144;
  (*CO_GraGruY_0512_TXSubPrd = 0512;
  CO_GraGruY_0683_TXPSPPro = 0683;
  CO_GraGruY_0853_TXPSPEnd = 0853;*)
  //
  CO_TXT_GraGruY_1024_TXCadNat = 'Mat�ria-prima';
  (*CO_TXT_GraGruY_1195_TXNatPDA = 'Mat�ria-prima PDA';
  CO_TXT_GraGruY_1365_TXProCal = 'Couro em Caleiro';
  CO_TXT_GraGruY_1536_TXCouCal = 'Couro Caleirado';
  CO_TXT_GraGruY_1621_TXCouDTA = 'Couro Caleirado DTA';
  CO_TXT_GraGruY_1707_TXProCur = 'Couro em Curtimento';
  CO_TXT_GraGruY_1877_TXCouCur = 'Couro Curtido';*)
  CO_TXT_GraGruY_2048_TXCadOpe = 'Artigo em confec��o';
  (*CO_TXT_GraGruY_3072_TXRibCla = 'Artigo de Ribeira Classificado';
  CO_TXT_GraGruY_4096_TXRibOpe = 'Artigo em Opera��o';
  CO_TXT_GraGruY_5120_TXWetEnd = 'Artigo Semi em Processo';
*)
  CO_TXT_GraGruY_6144_TXCadFcc = 'Artigo Acabado';
(*
  CO_TXT_GraGruY_0512_TXSubPrd = 'Subproduto In Natura';
  CO_TXT_GraGruY_0683_TXPSPPro = 'Subproduto em Processo';
  CO_TXT_GraGruY_0853_TXPSPEnd = 'Subproduto Processado';*)

{
  CO_TXT_            = '';
  CO_TXT_            = '';
  CO_TXT_            = '';

  Max??? = Integer(High(T???));
  s???: array[0..Max???] of string = (
  CO_TXT_,
  CO_TXT_,
  CO_TXT_
  );
}
  CO_CONTROLE_ESTOQUE_GRAGRUY = '1024, 2048, 3072';
  CO_TXT_emidAjuste        = '[ND]';
  CO_TXT_emidCompra        = 'Entrada';
  CO_TXT_emidVenda         = 'Sa�da';
  CO_TXT_emidReclas        = 'Reclasse';
  CO_TXT_emidBaixa         = 'Baixa';
  CO_TXT_emidIndstrlzc     = 'Industrializa��o';

  MaxEstqMovimID = Integer(High(TEstqMovimID));
  //sEstqMovimID: array[0..MaxEstqMovimID] of string = (
  sEstqMovimID: array[0..5] of string = (
    CO_TXT_emidAjuste        , // 0
    CO_TXT_emidCompra        , // 1
    CO_TXT_emidVenda         , // 2
    CO_TXT_emidReclas        , // 3
    CO_TXT_emidBaixa         , // 4
    CO_TXT_emidIndstrlzc      // 5
{
    CO_TXT_emidIndsVS        , // 6
    CO_TXT_emidClassArtVSUni , // 7
    CO_TXT_emidReclasVSUni   , // 8
    CO_TXT_emidForcado       , // 9
    CO_TXT_emidSemOrigem     , // 10
    CO_TXT_emidEmOperacao    , // 11
    CO_TXT_emidResiduoReclas , // 12
    CO_TXT_emidInventario    , // 13
    CO_TXT_emidClassArtVSMul , // 14
    CO_TXT_emidPreReclasse   , // 15
    CO_TXT_emidEntradaPlC    , // 16
    CO_TXT_emidExtraBxa      , // 17
    CO_TXT_emidSaldoAnterior , // 18
    CO_TXT_emidEmProcWE      , // 19
    CO_TXT_emidFinished      , // 20
    CO_TXT_emidDevolucao     , // 21
    CO_TXT_emidRetrabalho    , // 22
    CO_TXT_emidGeraSubProd   , // 23
    CO_TXT_emidReclasVSMul   , // 24
    CO_TXT_emidTransfLoc     , // 25
    CO_TXT_emidEmProcCal     , // 26
    CO_TXT_emidEmProcCur     , // 27
    CO_TXT_emidDesclasse     , // 28
    CO_TXT_emidCaleado       , // 29
    CO_TXT_emidRibPDA        , // 30
    CO_TXT_emidRibDTA        , // 31
    CO_TXT_emidEmProcSP      , // 32
    CO_TXT_emidEmReprRM      , // 33
    CO_TXT_emidCurtido       , // 34
    CO_TXT_emidMixInsum      , // 35
    CO_TXT_emidInnSemCob     , // 36
    CO_TXT_emidOutSemCob     // 37
}
  );

type
  TUnAppListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UnAppListas: TUnAppListas;

implementation

uses Module, DmkDAC_PF, ModuleGeral, UnMyObjects;

{ TUnAppListas }

end.
