unit uploads;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, WinProcs, WinTypes, Menus, Variants, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdFTP, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, UnDmkProcFunc,
  dmkImage, UnDmkEnums;

type
  TFmUploads = class(TForm)
    PainelDados: TPanel;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    LaCond: TLabel;
    EdCond: TdmkEditCB;
    CBCond: TdmkDBLookupComboBox;
    QrNomeCond: TmySQLQuery;
    DsNomeCond: TDataSource;
    QrUploads: TmySQLQuery;
    DsUploads: TDataSource;
    QrUploadsCodigo: TAutoIncField;
    QrUploadsNome: TWideStringField;
    QrUploadsArquivo: TWideStringField;
    QrUploadsCond: TIntegerField;
    QrUploadsLk: TIntegerField;
    QrUploadsDataCad: TDateField;
    QrUploadsDataAlt: TDateField;
    QrUploadsUserCad: TIntegerField;
    QrUploadsUserAlt: TIntegerField;
    QrUploadsAlterWeb: TSmallintField;
    DBGrid1: TDBGrid;
    QrNomeCondCodigo: TIntegerField;
    QrNomeCondNCONDOM: TWideStringField;
    Label1: TLabel;
    EdDirWeb: TdmkEditCB;
    CBDirWeb: TdmkDBLookupComboBox;
    EdNome: TdmkEdit;
    QrUploadsAtivo: TSmallintField;
    QrUploadsDirWeb: TIntegerField;
    QrNomeDir: TmySQLQuery;
    DsNomeDir: TDataSource;
    QrNomeDirCodigo: TAutoIncField;
    QrNomeDirNome: TWideStringField;
    QrNomeDirPasta: TWideStringField;
    OpenDialog1: TOpenDialog;
    Label3: TLabel;
    EdArquivo: TdmkEdit;
    QrUploadsNDIRWEB: TWideStringField;
    QrUploadsDIRNOME: TWideStringField;
    QrUploadsNCONDOM: TWideStringField;
    PMExclui: TPopupMenu;
    Excluiosdadosetambmoarquivo1: TMenuItem;
    Exluisomenteosdados1: TMenuItem;
    Panel4: TPanel;
    Label2: TLabel;
    EdCondPesq: TdmkEditCB;
    CBCondPesq: TdmkDBLookupComboBox;
    BitBtn1: TBitBtn;
    QrCondGri: TmySQLQuery;
    QrCondGriCodigo: TIntegerField;
    QrCondGriNome: TWideStringField;
    DsCondGri: TDataSource;
    QrCondGriArquivo: TWideStringField;
    QrCondGriCond: TIntegerField;
    QrCondGriLk: TIntegerField;
    QrCondGriDataCad: TDateField;
    QrCondGriDataAlt: TDateField;
    QrCondGriUserCad: TIntegerField;
    QrCondGriUserAlt: TIntegerField;
    QrCondGriAlterWeb: TSmallintField;
    QrCondGriAtivo: TSmallintField;
    QrCondGriDirWeb: TIntegerField;
    QrCondGriNDIRWEB: TWideStringField;
    QrCondGriDIRNOME: TWideStringField;
    QrCondGriNCONDOM: TWideStringField;
    IdFTP1: TIdFTP;
    sbStatus: TStatusBar;
    pbProgress: TProgressBar;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure IpFtpClient1FtpError(Sender: TObject; ErrorCode: Integer;
      const Error: String);
    procedure EdArquivoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Excluiosdadosetambmoarquivo1Click(Sender: TObject);
    procedure Exluisomenteosdados1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure IdFTP1AfterClientLogin(Sender: TObject);
    procedure IdFTP1Disconnected(Sender: TObject);
    procedure IdFTP1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure IdFTP1Work(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Integer);
    procedure IdFTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Integer);
    procedure IdFTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
    procedure FormDestroy(Sender: TObject);
  private
    FUploadL, FUploadR: String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   //Procedures do form
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    //procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure FTPLogin;
    procedure FTPLogout;

  public
    { Public declarations }
    procedure SetControls;
  end;

var
  FmUploads: TFmUploads;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

const
  DefCaption = 'FTP: Ftp Cliente';

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmUploads.Va(Para: TVaiPara);
begin
  DefParams;
end;
/////////////////////////////////////////////////////////////////////////////////////
procedure TFmUploads.DefParams;
begin
  VAR_GOTOTABELA := 'uploads';
  VAR_GOTOMYSQLTABLE := QrUploads;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := nil;
  VAR_GOTOMySQLDBNAME2 := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT upl.*, dir.Pasta NDIRWEB, dir.Nome DIRNOME,');
  VAR_SQLx.Add('IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM');
  VAR_SQLx.Add('FROM uploads upl');
  VAR_SQLx.Add('LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb');
  VAR_SQLx.Add('LEFT JOIN cond con ON con.Codigo=upl.Cond');
  VAR_SQLx.Add('LEFT JOIN entidades rec ON rec.Codigo=con.Cliente');
  VAR_SQLx.Add('ORDER BY NCONDOM, upl.DirWeb');
  //
  VAR_SQL1.Add('AND upl.Codigo=:P0');
  //
  VAR_SQLa.Add('AND upl.Nome Like :P0');
  //
end;

procedure TFmUploads.MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    BtConfirma.Enabled := True;
    if SQLType = stIns then
    begin
      EdCodigo.Text     := '';
      EdNome.Text       := '';
      EdArquivo.Enabled := True;
      EdArquivo.Text    := '';
      EdCond.Text       := '';
      CBCond.KeyValue   := NULL;
      EdDirWeb.Text     := '';
      CBDirWeb.Enabled  := True;
      CBDirWeb.KeyValue := NULL;
    end else begin
      if EdCondPesq.Text = '' then
      begin
        EdCodigo.Text     := IntToStr(QrUploadsCodigo.Value);
        EdNome.Text       := QrUploadsNome.Value;
        EdArquivo.Enabled := False;
        EdArquivo.Text    := QrUploadsArquivo.Value;
        EdCond.Text       := IntToStr(QrUploadsCond.Value);
        CBCond.KeyValue   := QrUploadsCond.Value;
        EdDirWeb.Text     := IntToStr(QrUploadsDirWeb.Value);
        CBDirWeb.Enabled  := False;
        CBDirWeb.KeyValue := QrUploadsDirWeb.Value;
      end else
      begin
        EdCodigo.Text     := IntToStr(QrCondGriCodigo.Value);
        EdNome.Text       := QrCondGriNome.Value;
        EdArquivo.Enabled := False;
        EdArquivo.Text    := QrCondGriArquivo.Value;
        EdCond.Text       := IntToStr(QrCondGriCond.Value);
        CBCond.KeyValue   := QrCondGriCond.Value;
        EdDirWeb.Text     := IntToStr(QrCondGriDirWeb.Value);
        CBDirWeb.Enabled  := False;
        CBDirWeb.KeyValue := QrCondGriDirWeb.Value;
      end;
      EdNome.SetFocus;
    end;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmUploads.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmUploads.AlteraRegistro;
var
  Uploads : Integer;
begin
  Uploads := QrUploadsCodigo.Value;
  if QrUploadsCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(uploads, Dmod.MyDBn, 'uploads', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(uploads, Dmod.MyDBn, 'uploads', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmUploads.IdFTP1AfterClientLogin(Sender: TObject);
begin
  SetControls;
end;

procedure TFmUploads.IdFTP1Disconnected(Sender: TObject);
begin
  SetControls;
end;

procedure TFmUploads.IdFTP1Status(ASender: TObject; const AStatus: TIdStatus;
  const AStatusText: string);
begin
  sbStatus.Panels[2].Text := AStatusText;
end;

procedure TFmUploads.IdFTP1Work(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Integer);
begin
  pbProgress.Position := AWorkCount;
end;

procedure TFmUploads.IdFTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCountMax: Integer);
begin
  pbProgress.Max := AWorkCountMax;
  pbProgress.Position := 0;
  pbProgress.Visible := true;
end;

procedure TFmUploads.IdFTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
  pbProgress.Visible := false;
end;

procedure TFmUploads.IncluiRegistro;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MostraEdicao(True, stIns, 0);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmUploads.FTPLogin;
var
  Host, Usuario, Senha: String;
begin
  Host    := Dmod.QrControleWeb_FTPh.Value;
  Usuario := Dmod.QrControleWeb_FTPu.Value;
  Senha   := Dmod.QrControleWeb_FTPs.Value;
  //
  if IdFTP1.Connected then
    begin
      IdFTP1.Disconnect;
    end
  else
    begin
      IdFTP1.Host     := Host;
      IdFTP1.Username := Usuario;
      IdFTP1.Password := Senha;
      IdFTP1.Connect;
    end;
end;

procedure TFmUploads.FTPLogout;
begin
  IdFTP1.Disconnect;
  Caption := 'Upload de arquivos';
end;

procedure TFmUploads.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmUploads.BtIncluiClick(Sender: TObject);
begin
  if IdFTP1.Connected then
    begin
      IncluiRegistro;
    end else
    begin
      Geral.MensagemBox('N�o foi posss�vel abrir uma conex�o com o servidor "'+ Dmod.QrControleWeb_FTPh.Value +'"', 'Erro', MB_OK+MB_ICONWARNING);
    end; 
end;

procedure TFmUploads.BtSaidaClick(Sender: TObject);
begin
  VAR_SERVICOG := QrUploadsCodigo.Value;
  Close;
end;

procedure TFmUploads.BtConfirmaClick(Sender: TObject);
var
  Nome, Arquivo, Pasta, Raiz, R: String;
  Cond, DirWeb : Integer;
begin
  if ImgTipo.SQLType = stIns then
    BtConfirma.Enabled := False;
  Nome    := EdNome.Text;
  Arquivo := EdArquivo.Text;
  Cond    := Geral.IMV(EdCond.Text);
  DirWeb  := Geral.IMV(EdDirWeb.Text);
  Pasta   := QrNomeDirPasta.Value;
  FUploadL := OpenDialog1.Filename;
  FUploadR := ExtractFilename(FUploadL);
  Raiz := Dmod.QrControleWeb_Raiz.Value;
  R    := '/' + Raiz + '/_local_/pdf/' + QrNomeDirPasta.Value + '/';
  //
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Cond = 0 then
  begin
    Geral.MensagemBox('Defina o condom�nio.', 'Erro', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if DirWeb = 0 then
  begin
    Geral.MensagemBox('Defina o diret�rio WEB.', 'Erro', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if ImgTipo.SQLType = stIns then
  begin
    if Length(Arquivo) = 0 then
    begin
      Geral.MensagemBox('Arquivo n�o definido.', 'Erro', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    // Upload
    if EdArquivo.Text <> '' then
    begin
      IdFTP1.ChangeDir(R);
      IdFTP1.Put(OpenDialog1.FileName, ExtractFileName(OpenDialog1.FileName));
    end;
  end;
  //
  if ImgTipo.SQLType = stIns then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrWeb, ImgTipo.SQLType, 'uploads', True,
    [
      'Nome', 'Arquivo', 'Cond', 'DirWeb'
    ], [],
    [
      Nome, FUploadR, Cond, DirWeb
    ], [], True)
    then
    begin
      QrUploads.Close;
      QrUploads.Open;
      Screen.Cursor := crDefault;
      MostraEdicao(False, stLok, 0);
    end;
  end else
  begin
    if UMyMod.SQLInsUpd(Dmod.QrWeb, ImgTipo.SQLType, 'uploads', True,
    [
      'Nome', 'Cond'
    ], ['Codigo'],
    [
      Nome, Cond
    ], [QrUploadsCodigo.Value], True)
    then
    begin
      QrUploads.Close;
      QrUploads.Open;
      Screen.Cursor := crDefault;
      MostraEdicao(False, stLok, 0);
    end;
  end
end;

procedure TFmUploads.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(False, stLok, 0);
end;

procedure TFmUploads.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrNomeCond.Open;
  QrNomeDir.Open;
  QrUploads.Open;
  DBGrid1.Align := alClient;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  CriaOForm;
  FTPLogin;
end;

procedure TFmUploads.FormDestroy(Sender: TObject);
begin
  FTPLogout;
end;

procedure TFmUploads.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmUploads.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'ServicosG', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmUploads.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUploads.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmUploads.BtExcluiClick(Sender: TObject);
begin
  if IdFTP1.Connected then
    begin
      MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
    end else
    begin
      Geral.MensagemBox('N�o foi posss�vel abrir uma conex�o com o servidor"'+ Dmod.QrControleWeb_FTPh.Value +'"', 'Erro', MB_OK+MB_ICONWARNING);
    end; 
end;

procedure TFmUploads.IpFtpClient1FtpError(Sender: TObject;
  ErrorCode: Integer; const Error: String);
begin
  MessageDlg(Error, mtError, [mbOK], 0);
  Exit;
  Screen.Cursor := crDefault;
end;

procedure TFmUploads.EdArquivoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenDialog1.Execute then
      FUploadL  := OpenDialog1.Filename;
      FUploadR := ExtractFilename(FUploadL);
      EdArquivo.Text := OpenDialog1.FileName;
  end;
end;

procedure TFmUploads.Excluiosdadosetambmoarquivo1Click(Sender: TObject);
var
  Codigo: Integer;
  Raiz, R, L, DirWeb, Nome: String;
begin
  if IdFTP1.Connected then
    begin
      if QrUploads.RecordCount > 0 then
      begin
        if EdCondPesq.Text = '' then
        begin
          DirWeb := QrUploadsNDIRWEB.Value;
          Codigo := QrUploadsCodigo.Value;
          L      := QrUploadsArquivo.Value;
          Nome   := QrUploadsNome.Value;
        end  else
        begin
          DirWeb := QrCondGriNDIRWEB.Value;
          Codigo := QrCondGriCodigo.Value;
          L      := QrCondGriArquivo.Value;
          Nome   := QrCondGriNome.Value;
        end;
        //
        Raiz   := Dmod.QrControleWeb_Raiz.Value;
        R      := Raiz + '/_local_/pdf/' + DirWeb + '/' + L;
        //
        if Geral.MensagemBox('Confirma a exclus�o do �tem "'+
        Nome+'" e tamb�m do arquivo'+
        Chr(13)+Chr(10)+'"'+L+'" hospedado no servidor web?',
        'Pergunta de exclus�o',
        MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          Screen.Cursor := crHourGlass;
          //
          //IpFtpClient1.Delete(R);
          //
          Dmod.QrWeb.SQL.Clear;
          Dmod.QrWeb.SQL.Add('DELETE FROM uploads WHERE Codigo=:P0');
          Dmod.QrWeb.Params[0].AsInteger := Codigo;
          Dmod.QrWeb.ExecSQL;
          QrUploads.Close;
          QrUploads.Open;
          QrCondGri.Close;
          QrCondGri.Open;
          Screen.Cursor := crDefault;
        end;
    end else
    begin
      Geral.MensagemBox('N�o foi posss�vel abrir uma conex�o com o servidor "'+ Dmod.QrControleWeb_FTPh.Value +'"', 'Erro', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

procedure TFmUploads.Exluisomenteosdados1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if QrUploads.RecordCount > 0 then
  begin
    Codigo := QrUploadsCodigo.Value;
    //
    if Geral.MensagemBox('Confirma a exclus�o do �tem "'+
    QrUploadsNome.Value+'"?',
    'Pergunta de exclus�o',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      //
      Dmod.QrWeb.SQL.Clear;
      Dmod.QrWeb.SQL.Add('DELETE FROM uploads WHERE Codigo=:P0');
      Dmod.QrWeb.Params[0].AsInteger := Codigo;
      Dmod.QrWeb.ExecSQL;
      QrUploads.Close;
      QrUploads.Open;
      QrCondGri.Close;
      QrCondGri.Open;            
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmUploads.BitBtn1Click(Sender: TObject);
var
  CONDOM: String;
begin
  CONDOM := EdCondPesq.Text;
  if CONDOM = '' then
  begin
    DBGrid1.DataSource := DsUploads;
  end else
  begin
    DBGrid1.DataSource := DsCondGri;
    //
    QrCondGri.Close;
    QrCondGri.SQL.Clear;
    QrCondGri.SQL.Add('SELECT upl.*, dir.Pasta NDIRWEB, dir.Nome DIRNOME,');
    QrCondGri.SQL.Add('IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM');
    QrCondGri.SQL.Add('FROM uploads upl');
    QrCondGri.SQL.Add('LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb');
    QrCondGri.SQL.Add('LEFT JOIN cond con ON con.Codigo=upl.Cond');
    QrCondGri.SQL.Add('LEFT JOIN entidades rec ON rec.Codigo=con.Cliente');
    QrCondGri.SQL.Add('WHERE upl.Cond = ' + CONDOM + '');
    QrCondGri.SQL.Add('ORDER BY NCONDOM, upl.DirWeb');
    QrCondGri.Open;
  end;
end;

procedure TFmUploads.SetControls;
begin
  if IdFTP1.Connected then
    begin
      sbStatus.Panels[0].Text := 'Conectado';
    end
  else
    begin
      sbStatus.Panels[0].Text := 'Desconectado';
    end;
end;

end.


