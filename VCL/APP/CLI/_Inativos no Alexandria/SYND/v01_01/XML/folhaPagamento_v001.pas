
{********************************************************************************************************}
{                                                                                                        }
{                                            XML Data Binding                                            }
{                                                                                                        }
{         Generated on: 24/01/2013 18:11:27                                                              }
{       Generated from: C:\Projetos\Delphi 2007\Aplicativos\Ativos\Syndi2\XML\folhaPagamento_v0.01.xsd   }
{   Settings stored in: C:\Projetos\Delphi 2007\Aplicativos\Ativos\Syndi2\XML\folhaPagamento_v0.01.xdb   }
{                                                                                                        }
{********************************************************************************************************}

unit folhaPagamento_v001;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTFolhaPagamento = interface;
  IXMLInfFolhaPagamento = interface;
  IXMLIdeFolhaPagamento = interface;
  IXMLDetFolhaPagamento = interface;
  IXMLDetFolhaPagamentoList = interface;
  IXMLCadLancamento = interface;
  IXMLFimFolhaPagamento = interface;

{ IXMLTFolhaPagamento }

  IXMLTFolhaPagamento = interface(IXMLNode)
    ['{FABDD7B3-61EA-4EA0-9CA7-C3F47439B24E}']
    { Property Accessors }
    function Get_InfFolhaPagamento: IXMLInfFolhaPagamento;
    { Methods & Properties }
    property InfFolhaPagamento: IXMLInfFolhaPagamento read Get_InfFolhaPagamento;
  end;

{ IXMLInfFolhaPagamento }

  IXMLInfFolhaPagamento = interface(IXMLNode)
    ['{C8C4C1F3-8A63-4C9C-84D1-364F1451E9F5}']
    { Property Accessors }
    function Get_Versao: WideString;
    function Get_IdeFolhaPagamento: IXMLIdeFolhaPagamento;
    function Get_DetFolhaPagamento: IXMLDetFolhaPagamentoList;
    function Get_FimFolhaPagamento: IXMLFimFolhaPagamento;
    procedure Set_Versao(Value: WideString);
    { Methods & Properties }
    property Versao: WideString read Get_Versao write Set_Versao;
    property IdeFolhaPagamento: IXMLIdeFolhaPagamento read Get_IdeFolhaPagamento;
    property DetFolhaPagamento: IXMLDetFolhaPagamentoList read Get_DetFolhaPagamento;
    property FimFolhaPagamento: IXMLFimFolhaPagamento read Get_FimFolhaPagamento;
  end;

{ IXMLIdeFolhaPagamento }

  IXMLIdeFolhaPagamento = interface(IXMLNode)
    ['{2BF00979-3082-4DEE-8246-BB0525941F4A}']
    { Property Accessors }
    function Get_Id_executavel: WideString;
    procedure Set_Id_executavel(Value: WideString);
    { Methods & Properties }
    property Id_executavel: WideString read Get_Id_executavel write Set_Id_executavel;
  end;

{ IXMLDetFolhaPagamento }

  IXMLDetFolhaPagamento = interface(IXMLNode)
    ['{F63CCF34-B2BC-40A9-A300-7099C99FA494}']
    { Property Accessors }
    function Get_Fld_Int_Ctr: WideString;
    function Get_CadLancamento: IXMLCadLancamento;
    procedure Set_Fld_Int_Ctr(Value: WideString);
    { Methods & Properties }
    property Fld_Int_Ctr: WideString read Get_Fld_Int_Ctr write Set_Fld_Int_Ctr;
    property CadLancamento: IXMLCadLancamento read Get_CadLancamento;
  end;

{ IXMLDetFolhaPagamentoList }

  IXMLDetFolhaPagamentoList = interface(IXMLNodeCollection)
    ['{966A832D-4100-4409-8E3A-490D72BC8F0F}']
    { Methods & Properties }
    function Add: IXMLDetFolhaPagamento;
    function Insert(const Index: Integer): IXMLDetFolhaPagamento;
    function Get_Item(Index: Integer): IXMLDetFolhaPagamento;
    property Items[Index: Integer]: IXMLDetFolhaPagamento read Get_Item; default;
  end;

{ IXMLCadLancamento }

  IXMLCadLancamento = interface(IXMLNode)
    ['{C037343D-C671-4315-9579-AC20A60D1704}']
    { Property Accessors }
    function Get_Fld_Str_Emp: WideString;
    function Get_Fld_Num_CNPJ: WideString;
    function Get_Fld_Str_Nom: WideString;
    function Get_Fld_Num_CPF: WideString;
    function Get_Fld_Str_Cod: WideString;
    function Get_Fld_Str_Cta: WideString;
    function Get_Fld_Flu_Val: WideString;
    function Get_Fld_Str_A_M: WideString;
    procedure Set_Fld_Str_Emp(Value: WideString);
    procedure Set_Fld_Num_CNPJ(Value: WideString);
    procedure Set_Fld_Str_Nom(Value: WideString);
    procedure Set_Fld_Num_CPF(Value: WideString);
    procedure Set_Fld_Str_Cod(Value: WideString);
    procedure Set_Fld_Str_Cta(Value: WideString);
    procedure Set_Fld_Flu_Val(Value: WideString);
    procedure Set_Fld_Str_A_M(Value: WideString);
    { Methods & Properties }
    property Fld_Str_Emp: WideString read Get_Fld_Str_Emp write Set_Fld_Str_Emp;
    property Fld_Num_CNPJ: WideString read Get_Fld_Num_CNPJ write Set_Fld_Num_CNPJ;
    property Fld_Str_Nom: WideString read Get_Fld_Str_Nom write Set_Fld_Str_Nom;
    property Fld_Num_CPF: WideString read Get_Fld_Num_CPF write Set_Fld_Num_CPF;
    property Fld_Str_Cod: WideString read Get_Fld_Str_Cod write Set_Fld_Str_Cod;
    property Fld_Str_Cta: WideString read Get_Fld_Str_Cta write Set_Fld_Str_Cta;
    property Fld_Flu_Val: WideString read Get_Fld_Flu_Val write Set_Fld_Flu_Val;
    property Fld_Str_A_M: WideString read Get_Fld_Str_A_M write Set_Fld_Str_A_M;
  end;

{ IXMLFimFolhaPagamento }

  IXMLFimFolhaPagamento = interface(IXMLNode)
    ['{45126AA8-A4AE-4387-8C85-C91F646C50F8}']
    { Property Accessors }
    function Get_QtdRegistros: WideString;
    procedure Set_QtdRegistros(Value: WideString);
    { Methods & Properties }
    property QtdRegistros: WideString read Get_QtdRegistros write Set_QtdRegistros;
  end;

{ Forward Decls }

  TXMLTFolhaPagamento = class;
  TXMLInfFolhaPagamento = class;
  TXMLIdeFolhaPagamento = class;
  TXMLDetFolhaPagamento = class;
  TXMLDetFolhaPagamentoList = class;
  TXMLCadLancamento = class;
  TXMLFimFolhaPagamento = class;

{ TXMLTFolhaPagamento }

  TXMLTFolhaPagamento = class(TXMLNode, IXMLTFolhaPagamento)
  protected
    { IXMLTFolhaPagamento }
    function Get_InfFolhaPagamento: IXMLInfFolhaPagamento;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfFolhaPagamento }

  TXMLInfFolhaPagamento = class(TXMLNode, IXMLInfFolhaPagamento)
  private
    FDetFolhaPagamento: IXMLDetFolhaPagamentoList;
  protected
    { IXMLInfFolhaPagamento }
    function Get_Versao: WideString;
    function Get_IdeFolhaPagamento: IXMLIdeFolhaPagamento;
    function Get_DetFolhaPagamento: IXMLDetFolhaPagamentoList;
    function Get_FimFolhaPagamento: IXMLFimFolhaPagamento;
    procedure Set_Versao(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIdeFolhaPagamento }

  TXMLIdeFolhaPagamento = class(TXMLNode, IXMLIdeFolhaPagamento)
  protected
    { IXMLIdeFolhaPagamento }
    function Get_Id_executavel: WideString;
    procedure Set_Id_executavel(Value: WideString);
  end;

{ TXMLDetFolhaPagamento }

  TXMLDetFolhaPagamento = class(TXMLNode, IXMLDetFolhaPagamento)
  protected
    { IXMLDetFolhaPagamento }
    function Get_Fld_Int_Ctr: WideString;
    function Get_CadLancamento: IXMLCadLancamento;
    procedure Set_Fld_Int_Ctr(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDetFolhaPagamentoList }

  TXMLDetFolhaPagamentoList = class(TXMLNodeCollection, IXMLDetFolhaPagamentoList)
  protected
    { IXMLDetFolhaPagamentoList }
    function Add: IXMLDetFolhaPagamento;
    function Insert(const Index: Integer): IXMLDetFolhaPagamento;
    function Get_Item(Index: Integer): IXMLDetFolhaPagamento;
  end;

{ TXMLCadLancamento }

  TXMLCadLancamento = class(TXMLNode, IXMLCadLancamento)
  protected
    { IXMLCadLancamento }
    function Get_Fld_Str_Emp: WideString;
    function Get_Fld_Num_CNPJ: WideString;
    function Get_Fld_Str_Nom: WideString;
    function Get_Fld_Num_CPF: WideString;
    function Get_Fld_Str_Cod: WideString;
    function Get_Fld_Str_Cta: WideString;
    function Get_Fld_Flu_Val: WideString;
    function Get_Fld_Str_A_M: WideString;
    procedure Set_Fld_Str_Emp(Value: WideString);
    procedure Set_Fld_Num_CNPJ(Value: WideString);
    procedure Set_Fld_Str_Nom(Value: WideString);
    procedure Set_Fld_Num_CPF(Value: WideString);
    procedure Set_Fld_Str_Cod(Value: WideString);
    procedure Set_Fld_Str_Cta(Value: WideString);
    procedure Set_Fld_Flu_Val(Value: WideString);
    procedure Set_Fld_Str_A_M(Value: WideString);
  end;

{ TXMLFimFolhaPagamento }

  TXMLFimFolhaPagamento = class(TXMLNode, IXMLFimFolhaPagamento)
  protected
    { IXMLFimFolhaPagamento }
    function Get_QtdRegistros: WideString;
    procedure Set_QtdRegistros(Value: WideString);
  end;

{ Global Functions }

function Getfolha(Doc: IXMLDocument): IXMLTFolhaPagamento;
function Loadfolha(const FileName: WideString): IXMLTFolhaPagamento;
function Newfolha: IXMLTFolhaPagamento;

const
  TargetNamespace = 'http://www.dermatek.com.br/apps/aux/FP';

implementation

{ Global Functions }

function Getfolha(Doc: IXMLDocument): IXMLTFolhaPagamento;
begin
  Result := Doc.GetDocBinding('folha', TXMLTFolhaPagamento, TargetNamespace) as IXMLTFolhaPagamento;
end;

function Loadfolha(const FileName: WideString): IXMLTFolhaPagamento;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('folha', TXMLTFolhaPagamento, TargetNamespace) as IXMLTFolhaPagamento;
end;

function Newfolha: IXMLTFolhaPagamento;
begin
  Result := NewXMLDocument.GetDocBinding('folha', TXMLTFolhaPagamento, TargetNamespace) as IXMLTFolhaPagamento;
end;

{ TXMLTFolhaPagamento }

procedure TXMLTFolhaPagamento.AfterConstruction;
begin
  RegisterChildNode('infFolhaPagamento', TXMLInfFolhaPagamento);
  inherited;
end;

function TXMLTFolhaPagamento.Get_InfFolhaPagamento: IXMLInfFolhaPagamento;
begin
  Result := ChildNodes['infFolhaPagamento'] as IXMLInfFolhaPagamento;
end;

{ TXMLInfFolhaPagamento }

procedure TXMLInfFolhaPagamento.AfterConstruction;
begin
  RegisterChildNode('ideFolhaPagamento', TXMLIdeFolhaPagamento);
  RegisterChildNode('detFolhaPagamento', TXMLDetFolhaPagamento);
  RegisterChildNode('fimFolhaPagamento', TXMLFimFolhaPagamento);
  FDetFolhaPagamento := CreateCollection(TXMLDetFolhaPagamentoList, IXMLDetFolhaPagamento, 'detFolhaPagamento') as IXMLDetFolhaPagamentoList;
  inherited;
end;

function TXMLInfFolhaPagamento.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLInfFolhaPagamento.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLInfFolhaPagamento.Get_IdeFolhaPagamento: IXMLIdeFolhaPagamento;
begin
  Result := ChildNodes['ideFolhaPagamento'] as IXMLIdeFolhaPagamento;
end;

function TXMLInfFolhaPagamento.Get_DetFolhaPagamento: IXMLDetFolhaPagamentoList;
begin
  Result := FDetFolhaPagamento;
end;

function TXMLInfFolhaPagamento.Get_FimFolhaPagamento: IXMLFimFolhaPagamento;
begin
  Result := ChildNodes['fimFolhaPagamento'] as IXMLFimFolhaPagamento;
end;

{ TXMLIdeFolhaPagamento }

function TXMLIdeFolhaPagamento.Get_Id_executavel: WideString;
begin
  Result := ChildNodes['id_executavel'].Text;
end;

procedure TXMLIdeFolhaPagamento.Set_Id_executavel(Value: WideString);
begin
  ChildNodes['id_executavel'].NodeValue := Value;
end;

{ TXMLDetFolhaPagamento }

procedure TXMLDetFolhaPagamento.AfterConstruction;
begin
  RegisterChildNode('CadLancamento', TXMLCadLancamento);
  inherited;
end;

function TXMLDetFolhaPagamento.Get_Fld_Int_Ctr: WideString;
begin
  Result := AttributeNodes['fld_Int_Ctr'].Text;
end;

procedure TXMLDetFolhaPagamento.Set_Fld_Int_Ctr(Value: WideString);
begin
  SetAttribute('fld_Int_Ctr', Value);
end;

function TXMLDetFolhaPagamento.Get_CadLancamento: IXMLCadLancamento;
begin
  Result := ChildNodes['CadLancamento'] as IXMLCadLancamento;
end;

{ TXMLDetFolhaPagamentoList }

function TXMLDetFolhaPagamentoList.Add: IXMLDetFolhaPagamento;
begin
  Result := AddItem(-1) as IXMLDetFolhaPagamento;
end;

function TXMLDetFolhaPagamentoList.Insert(const Index: Integer): IXMLDetFolhaPagamento;
begin
  Result := AddItem(Index) as IXMLDetFolhaPagamento;
end;
function TXMLDetFolhaPagamentoList.Get_Item(Index: Integer): IXMLDetFolhaPagamento;
begin
  Result := List[Index] as IXMLDetFolhaPagamento;
end;

{ TXMLCadLancamento }

function TXMLCadLancamento.Get_Fld_Str_Emp: WideString;
begin
  Result := ChildNodes['fld_Str_Emp'].Text;
end;

procedure TXMLCadLancamento.Set_Fld_Str_Emp(Value: WideString);
begin
  ChildNodes['fld_Str_Emp'].NodeValue := Value;
end;

function TXMLCadLancamento.Get_Fld_Num_CNPJ: WideString;
begin
  Result := ChildNodes['fld_Num_CNPJ'].Text;
end;

procedure TXMLCadLancamento.Set_Fld_Num_CNPJ(Value: WideString);
begin
  ChildNodes['fld_Num_CNPJ'].NodeValue := Value;
end;

function TXMLCadLancamento.Get_Fld_Str_Nom: WideString;
begin
  Result := ChildNodes['fld_Str_Nom'].Text;
end;

procedure TXMLCadLancamento.Set_Fld_Str_Nom(Value: WideString);
begin
  ChildNodes['fld_Str_Nom'].NodeValue := Value;
end;

function TXMLCadLancamento.Get_Fld_Num_CPF: WideString;
begin
  Result := ChildNodes['fld_Num_CPF'].Text;
end;

procedure TXMLCadLancamento.Set_Fld_Num_CPF(Value: WideString);
begin
  ChildNodes['fld_Num_CPF'].NodeValue := Value;
end;

function TXMLCadLancamento.Get_Fld_Str_Cod: WideString;
begin
  Result := ChildNodes['fld_Str_Cod'].Text;
end;

procedure TXMLCadLancamento.Set_Fld_Str_Cod(Value: WideString);
begin
  ChildNodes['fld_Str_Cod'].NodeValue := Value;
end;

function TXMLCadLancamento.Get_Fld_Str_Cta: WideString;
begin
  Result := ChildNodes['fld_Str_Cta'].Text;
end;

procedure TXMLCadLancamento.Set_Fld_Str_Cta(Value: WideString);
begin
  ChildNodes['fld_Str_Cta'].NodeValue := Value;
end;

function TXMLCadLancamento.Get_Fld_Flu_Val: WideString;
begin
  Result := ChildNodes['fld_Flu_Val'].Text;
end;

procedure TXMLCadLancamento.Set_Fld_Flu_Val(Value: WideString);
begin
  ChildNodes['fld_Flu_Val'].NodeValue := Value;
end;

function TXMLCadLancamento.Get_Fld_Str_A_M: WideString;
begin
  Result := ChildNodes['fld_Str_A_M'].Text;
end;

procedure TXMLCadLancamento.Set_Fld_Str_A_M(Value: WideString);
begin
  ChildNodes['fld_Str_A_M'].NodeValue := Value;
end;

{ TXMLFimFolhaPagamento }

function TXMLFimFolhaPagamento.Get_QtdRegistros: WideString;
begin
  Result := ChildNodes['qtdRegistros'].Text;
end;

procedure TXMLFimFolhaPagamento.Set_QtdRegistros(Value: WideString);
begin
  ChildNodes['qtdRegistros'].NodeValue := Value;
end;

end.