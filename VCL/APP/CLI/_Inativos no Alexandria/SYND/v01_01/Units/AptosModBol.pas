unit AptosModBol;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB, ABSMain, DBGrids, UnMLAGeral,
  dmkDBGrid, DBCtrls, mySQLDbTables, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkGeral, dmkImage, UnInternalConsts, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmAptosModBol = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    QrUHs: TmySQLQuery;
    DataSource1: TDataSource;
    Query: TABSQuery;
    QueryAtivo: TSmallintField;
    DBGrid1: TdmkDBGrid;
    QrUHsApto: TIntegerField;
    QrUHsUnidade: TWideStringField;
    QrUHsNOMEBLOCO: TWideStringField;
    QrUHsNOMEPROPR: TWideStringField;
    QueryApto: TIntegerField;
    QueryUnidade: TWideStringField;
    QueryNOMEBLOCO: TWideStringField;
    QueryNOMEPROPR: TWideStringField;
    QrUHsAndar: TIntegerField;
    QueryAndar: TIntegerField;
    QrConfigBol: TmySQLQuery;
    DsConfigBol: TDataSource;
    QrConfigBolCodigo: TIntegerField;
    QrConfigBolNome: TWideStringField;
    RGModelBloq: TRadioGroup;
    GBConfig: TGroupBox;
    EdConfigBol: TdmkEditCB;
    CBConfigBol: TdmkDBLookupComboBox;
    RGCompe: TRadioGroup;
    Panel4: TPanel;
    CkBalAgrMens: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    SpeedButton2: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCadastrar: Boolean;
    FTabela: String;
    FCodBol: Integer;
    procedure CriaQuery(Cond: Integer);
  end;

  var
  FmAptosModBol: TFmAptosModBol;

implementation

uses Module, UMySQLModule, Principal, UnMyObjects;

{$R *.DFM}

procedure TFmAptosModBol.BtOKClick(Sender: TObject);
var
  Codigo, ConfigBol: Integer;
begin
  ConfigBol := Geral.IMV(EdConfigBol.Text);
  {
  if ConfigBol = 0 then
  begin
    Geral.MensagemBox('Informe ');
    EdConfigBol.SetFocus;
    Exit;
  end;
  }Screen.Cursor := crHourGlass;
  //
  FCadastrar := True;
  Query.Filter   := 'Ativo=1';
  Query.Filtered := True;
  //
  if Query.RecordCount > 0 then
  begin
    Codigo := FCodBol;//QrUHs.Params[0].AsInteger;
    Query.First;
    while not Query.Eof do
    begin
      UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, FTabela, False,
      ['ModelBloq', 'ConfigBol', 'BalAgrMens', 'Compe'],
      ['Codigo', 'Apto'],
      ['ModelBloq', 'ConfigBol', 'BalAgrMens', 'Compe'],
      [RgModelBloq.ItemIndex, ConfigBol, MLAGeral.BTI(CkBalAgrMens.Checked),
      RGCompe.ItemIndex], [Codigo, QueryApto.Value], [RgModelBloq.ItemIndex,
      ConfigBol, MLAGeral.BTI(CkBalAgrMens.Checked),RGCompe.ItemIndex], True);
      Query.Next;
    end;
    //
    Close;
  end else begin
    Query.Filtered := False;
    Geral.MensagemBox('Nenhum item foi selecionado!', 'Avido',
    MB_OK+MB_ICONWARNING);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmAptosModBol.BtSaidaClick(Sender: TObject);
begin
  FCadastrar := False;
  Close;
end;

procedure TFmAptosModBol.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if Query.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    Query.Edit;
    Query.FieldByName('Ativo').Value := Status;
    Query.Post;
  end;
end;

procedure TFmAptosModBol.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmAptosModBol.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrConfigBol, DMod.MyDB);
  //
  FmPrincipal.PreencheModelosBloq(RGModelBloq);
end;

procedure TFmAptosModBol.CriaQuery(Cond: Integer);
var
  Campos: String;
begin
  FCadastrar := False;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE AptosModBol; ');
  Query.SQL.Add('CREATE TABLE AptosModBol (');
  Query.SQL.Add('  Apto      integer      ,');
  Query.SQL.Add('  NOMEBLOCO varchar(100) ,');
  Query.SQL.Add('  Andar      integer      ,');
  Query.SQL.Add('  Unidade   varchar(10)  ,');
  Query.SQL.Add('  NOMEPROPR varchar(100) ,');
  Query.SQL.Add('  Ativo     smallint      ');
  Query.SQL.Add(');');
  //
  Campos := 'INSERT INTO aptosmodbol (Apto,NOMEBLOCO,Andar,Unidade,NOMEPROPR,Ativo) Values (';
  QrUHs.Close;
  QrUHs.Params[0].AsInteger := Cond;
  QrUHs.Open;
  QrUHs.First;
  while not QrUHs.Eof do
  begin
    Query.SQL.Add(Campos +
      {00}MLAGeral.FormatABSLn(QrUHsApto.Value) +
      {01}MLAGeral.FormatABSLn(QrUHsNOMEBLOCO.Value) +
      {02}MLAGeral.FormatABSLn(QrUHsAndar.Value) +
      {03}MLAGeral.FormatABSLn(QrUHsUnidade.Value) +
      {04}MLAGeral.FormatABSLn(QrUHsNOMEPROPR.Value) +
    '0);');
    //
    QrUHs.Next;
  end;
  //
  Query.SQL.Add('SELECT * FROM aptosmodbol');
  Query.Open;
end;

procedure TFmAptosModBol.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAptosModBol.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.MostraConfigBol(EdConfigBol.ValueVariant);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.AbreQuery(QrConfigBol, DMod.MyDB);
    //
    EdConfigBol.ValueVariant := VAR_CADASTRO;
    CBConfigBol.KeyValue     := VAR_CADASTRO;
    EdConfigBol.SetFocus;
  end;
end;

end.

