unit CondEmeios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, dmkEdit, Mask, DBCtrls,
  dmkGeral, dmkDBEdit, dmkImage, UnDmkEnums;

type
  TFmCondEmeios = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdSaudacao: TdmkEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    Label3: TLabel;
    EdEmeio: TdmkEdit;
    Label4: TLabel;
    Panel3: TPanel;
    dmkDBEdit1: TdmkDBEdit;
    Label5: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label6: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dmkEdit1: TdmkEdit;
    Label8: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCondEmeios: TFmCondEmeios;

implementation

uses Module, Cond, UMySQLModule, UnMyObjects;

{$R *.DFM}

procedure TFmCondEmeios.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondEmeios.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondEmeios.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCondEmeios.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondEmeios.BtOKClick(Sender: TObject);
var
  Item: Integer;
begin
  Item := UMyMod.BuscaEmLivreY_Def('CondEmeios', 'Item', ImgTipo.SQLType,
    FmCond.QrCondEmeiosItem.Value);
  if UMyMod.ExecSQLInsUpdFm(FmCondEmeios, ImgTipo.SQLType, 'CondEmeios',
  Item, Dmod.QrUpd) then
  begin
    FmCond.ReopenCondEmeios(Item);
    Close;
  end;
end;

end.
