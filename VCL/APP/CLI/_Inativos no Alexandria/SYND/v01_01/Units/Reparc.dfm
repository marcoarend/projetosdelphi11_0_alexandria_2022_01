object FmReparc: TFmReparc
  Left = 339
  Top = 185
  Caption = 'INA-REPAR-001 :: Reparcelamento de D'#233'bitos Condominiais'
  ClientHeight = 659
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 497
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel11: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 100
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 540
        Height = 100
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 4
          Top = 4
          Width = 60
          Height = 13
          Caption = 'Condom'#237'nio:'
        end
        object Label1: TLabel
          Left = 4
          Top = 44
          Width = 140
          Height = 13
          Caption = 'Propriet'#225'rio [F4 mostra todos]:'
        end
        object Label3: TLabel
          Left = 388
          Top = 44
          Width = 103
          Height = 13
          Caption = 'Unidade habitacional:'
        end
        object EdEmpresa: TdmkEditCB
          Left = 4
          Top = 20
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 52
          Top = 20
          Width = 479
          Height = 21
          Color = clWhite
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPropriet: TdmkEditCB
          Left = 4
          Top = 60
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdProprietChange
          OnKeyDown = EdProprietKeyDown
          DBLookupComboBox = CBPropriet
          IgnoraDBLookupComboBox = False
        end
        object CBPropriet: TdmkDBLookupComboBox
          Left = 52
          Top = 60
          Width = 333
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEPROP'
          ListSource = DsPropriet
          TabOrder = 3
          OnKeyDown = CBProprietKeyDown
          dmkEditCB = EdPropriet
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCondImov: TdmkEditCB
          Left = 388
          Top = 60
          Width = 44
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCondImovChange
          DBLookupComboBox = CBCondImov
          IgnoraDBLookupComboBox = False
        end
        object CBCondImov: TdmkDBLookupComboBox
          Left = 436
          Top = 60
          Width = 95
          Height = 21
          Color = clWhite
          KeyField = 'Conta'
          ListField = 'Unidade'
          ListSource = DsCondImov
          TabOrder = 5
          OnKeyDown = CBCondImovKeyDown
          dmkEditCB = EdCondImov
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object GroupBox4: TGroupBox
        Left = 676
        Top = 0
        Width = 129
        Height = 100
        Align = alLeft
        Caption = ' Reparcelamento: '
        TabOrder = 2
        object Label8: TLabel
          Left = 8
          Top = 16
          Width = 71
          Height = 13
          Caption = '1'#186' vencimento:'
        end
        object Label14: TLabel
          Left = 8
          Top = 56
          Width = 40
          Height = 13
          Caption = '% Multa:'
        end
        object Label15: TLabel
          Left = 68
          Top = 56
          Width = 52
          Height = 13
          Caption = '% Jur/m'#234's:'
        end
        object TPPriVct: TdmkEditDateTimePicker
          Left = 8
          Top = 32
          Width = 112
          Height = 21
          Date = 40286.700736689820000000
          Time = 40286.700736689820000000
          TabOrder = 0
          OnChange = TPPriVctChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdPercMulta: TdmkEdit
          Left = 8
          Top = 72
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdPercMultaExit
        end
        object EdPercJuros: TdmkEdit
          Left = 68
          Top = 72
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdPercJurosExit
        end
      end
      object GroupBox1: TGroupBox
        Left = 540
        Top = 0
        Width = 136
        Height = 100
        Align = alLeft
        Caption = ' Per'#237'odo do vencimento: '
        TabOrder = 1
        object Label4: TLabel
          Left = 12
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label5: TLabel
          Left = 12
          Top = 56
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object TPDtaIni: TdmkEditDateTimePicker
          Left = 12
          Top = 32
          Width = 112
          Height = 21
          Date = 40286.700736689820000000
          Time = 40286.700736689820000000
          TabOrder = 0
          OnClick = TPDtaIniClick
          OnChange = TPDtaIniChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPDtaFim: TdmkEditDateTimePicker
          Left = 12
          Top = 72
          Width = 112
          Height = 21
          Date = 40286.700736689820000000
          Time = 40286.700736689820000000
          TabOrder = 1
          OnClick = TPDtaFimClick
          OnChange = TPDtaFimChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
      end
      object GBwcli: TGroupBox
        Left = 805
        Top = 0
        Width = 103
        Height = 100
        Align = alLeft
        Caption = ' Limitadores: '
        TabOrder = 3
        object Label6: TLabel
          Left = 8
          Top = 16
          Width = 83
          Height = 13
          Caption = 'Val. m'#237'n. parcela:'
          FocusControl = DBEdit1
        end
        object Label7: TLabel
          Left = 8
          Top = 56
          Width = 83
          Height = 13
          Caption = 'N'#186' m'#225'x. parcelas:'
          FocusControl = DBEdit2
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 32
          Width = 88
          Height = 21
          DataField = 'Parc_ValMin'
          DataSource = Dswcli
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 8
          Top = 72
          Width = 88
          Height = 21
          DataField = 'Parc_QtdMax'
          DataSource = Dswcli
          TabOrder = 1
        end
      end
    end
    object PnPesq1: TPanel
      Left = 0
      Top = 100
      Width = 1008
      Height = 397
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 416
        Height = 235
        Align = alLeft
        DataSource = DsReparcI2
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'FatNum'
            Title.Caption = 'Bloqueto'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValBol'
            Title.Caption = 'Original'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValMul'
            Title.Caption = '$ Multa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValJur'
            Title.Caption = '$ Juros'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValPgt'
            Title.Caption = '$ A Pagar'
            Visible = True
          end>
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 338
        Width = 1008
        Height = 59
        Align = alBottom
        Caption = ' Totais: '
        TabOrder = 1
        object Label9: TLabel
          Left = 8
          Top = 16
          Width = 47
          Height = 13
          Caption = '$ Original:'
          FocusControl = DBEdit3
        end
        object Label10: TLabel
          Left = 104
          Top = 16
          Width = 43
          Height = 13
          Caption = '$ Multas:'
          FocusControl = DBEdit4
        end
        object Label11: TLabel
          Left = 200
          Top = 16
          Width = 37
          Height = 13
          Caption = '$ Juros:'
          FocusControl = DBEdit5
        end
        object Label12: TLabel
          Left = 296
          Top = 16
          Width = 49
          Height = 13
          Caption = '$ A pagar:'
          FocusControl = DBEdit6
        end
        object DBEdit3: TDBEdit
          Left = 8
          Top = 32
          Width = 92
          Height = 21
          DataField = 'ValBol'
          DataSource = DsSumI
          TabOrder = 0
        end
        object DBEdit4: TDBEdit
          Left = 104
          Top = 32
          Width = 92
          Height = 21
          DataField = 'ValMul'
          DataSource = DsSumI
          TabOrder = 1
        end
        object DBEdit5: TDBEdit
          Left = 200
          Top = 32
          Width = 92
          Height = 21
          DataField = 'ValJur'
          DataSource = DsSumI
          TabOrder = 2
        end
        object DBEdit6: TDBEdit
          Left = 296
          Top = 32
          Width = 92
          Height = 21
          DataField = 'ValPgt'
          DataSource = DsSumI
          TabOrder = 3
        end
      end
      object DBGrid2: TDBGrid
        Left = 416
        Top = 0
        Width = 168
        Height = 235
        Align = alLeft
        DataSource = DsReparcP2
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'TotParc'
            Title.Caption = 'Parcelas'
            Width = 47
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TotalVal'
            Title.Caption = 'Valor parcelas'
            Width = 80
            Visible = True
          end>
      end
      object DBGrid3: TDBGrid
        Left = 584
        Top = 0
        Width = 424
        Height = 235
        Align = alClient
        DataSource = DsParcelasP
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NumParc'
            Title.Caption = 'Parcela'
            Width = 47
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencto'
            Title.Caption = 'Vencim.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 235
        Width = 1008
        Height = 103
        Align = alBottom
        TabOrder = 4
        object DBGrid4: TDBGrid
          Left = 1
          Top = 1
          Width = 1006
          Height = 101
          Align = alClient
          DataSource = DsLct
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 510
        Height = 32
        Caption = 'Reparcelamento de D'#233'bitos Condominiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 510
        Height = 32
        Caption = 'Reparcelamento de D'#233'bitos Condominiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 510
        Height = 32
        Caption = 'Reparcelamento de D'#233'bitos Condominiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 545
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 589
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesq: TBitBtn
        Tag = 18
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesqClick
      end
      object BtParcela: TBitBtn
        Tag = 490
        Left = 146
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Reparcela'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtParcelaClick
      end
    end
  end
  object QrPropriet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP '
      'FROM entidades ent'
      'LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet'
      'WHERE ent.Cliente2="V"'
      'AND cim.Codigo<>0'
      'ORDER BY NOMEPROP')
    Left = 69
    Top = 9
    object QrProprietCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrProprietNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Origin = 'NOMEPROP'
      Required = True
      Size = 100
    end
  end
  object DsPropriet: TDataSource
    DataSet = QrPropriet
    Left = 97
    Top = 9
  end
  object QrCondImov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cim.Conta, cim.Unidade'
      'FROM condimov cim'
      'WHERE cim.Codigo<>0'
      'AND cim.Propriet<>0'
      'ORDER BY cim.Unidade'
      '')
    Left = 125
    Top = 9
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 153
    Top = 9
  end
  object QrPesq1: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'INSERT INTO ReparcI2'
      'SELECT SUM(lan.Credito) Credito, lan.Multa,  lan.MoraDia,'
      'lan.Cliente, lan.Depto, lan.Mez, lan.Vencimento, lan.DataCad, '
      'FatNum, lan.Vencimento Vencto, lan.MoraDia TxaJur, '
      'lan.Multa TxaMul, SUM(lan.Credito) ValBol, '
      '0.00 ValJur, 0.00 ValMul, 0.00 ValPgt, lan.Controle'
      'FROM syndic.lct lan'
      'WHERE FatID in (600,601)'
      'AND Reparcel=0'
      'AND Tipo=2'
      'AND FatNum>0'
      'AND lan.Compensado < 2'
      'AND lan.Vencimento BETWEEN :P0 AND :P1'
      'AND lan.ForneceI = :P2'
      'AND lan.Depto = :P3'
      'GROUP BY lan.Cliente, lan.Depto, lan.Mez, lan.FatNum'
      'ORDER BY lan.Vencimento')
    Left = 36
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
  end
  object DsReparcI2: TDataSource
    DataSet = QrReparcI2
    Left = 164
    Top = 184
  end
  object QrReparcI2: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrReparcI2AfterOpen
    BeforeClose = QrReparcI2BeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM ReparcI2')
    Left = 136
    Top = 184
    object QrReparcI2Credito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrReparcI2Multa: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrReparcI2MoraDia: TFloatField
      FieldName = 'MoraDia'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrReparcI2Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrReparcI2Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrReparcI2Mez: TIntegerField
      FieldName = 'Mez'
    end
    object QrReparcI2Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrReparcI2DataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrReparcI2FatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrReparcI2Vencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrReparcI2TxaJur: TFloatField
      FieldName = 'TxaJur'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrReparcI2TxaMul: TFloatField
      FieldName = 'TxaMul'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrReparcI2ValBol: TFloatField
      FieldName = 'ValBol'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrReparcI2ValJur: TFloatField
      FieldName = 'ValJur'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrReparcI2ValMul: TFloatField
      FieldName = 'ValMul'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrReparcI2ValPgt: TFloatField
      FieldName = 'ValPgt'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrReparcI2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPesq2: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM ReparcI2')
    Left = 36
    Top = 188
    object QrPesq2Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrPesq2Multa: TFloatField
      FieldName = 'Multa'
    end
    object QrPesq2MoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPesq2Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesq2Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrPesq2Mez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPesq2Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrPesq2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesq2FatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPesq2Vencto: TDateField
      FieldName = 'Vencto'
    end
    object QrPesq2TxaJur: TFloatField
      FieldName = 'TxaJur'
    end
    object QrPesq2TxaMul: TFloatField
      FieldName = 'TxaMul'
    end
    object QrPesq2ValBol: TFloatField
      FieldName = 'ValBol'
    end
    object QrPesq2ValJur: TFloatField
      FieldName = 'ValJur'
    end
    object QrPesq2ValMul: TFloatField
      FieldName = 'ValMul'
    end
    object QrPesq2ValPgt: TFloatField
      FieldName = 'ValPgt'
    end
    object QrPesq2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object Qrwcli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Parc_ValMin, Parc_QtdMax '
      'FROM wclients'
      'WHERE CodCliEsp=:P0')
    Left = 68
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrwcliParc_ValMin: TFloatField
      FieldName = 'Parc_ValMin'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrwcliParc_QtdMax: TIntegerField
      FieldName = 'Parc_QtdMax'
    end
  end
  object Dswcli: TDataSource
    DataSet = Qrwcli
    Left = 96
    Top = 244
  end
  object QrSumI: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(ValBol) ValBol, SUM(ValJur) ValJur,'
      'SUM(ValMul) ValMul, SUM(ValPgt) ValPgt'
      'FROM ReparcI2')
    Left = 68
    Top = 272
    object QrSumIValBol: TFloatField
      FieldName = 'ValBol'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrSumIValJur: TFloatField
      FieldName = 'ValJur'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrSumIValMul: TFloatField
      FieldName = 'ValMul'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrSumIValPgt: TFloatField
      FieldName = 'ValPgt'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
  end
  object DsSumI: TDataSource
    DataSet = QrSumI
    Left = 96
    Top = 272
  end
  object QrReparcP2: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrReparcP2BeforeClose
    AfterScroll = QrReparcP2AfterScroll
    OnCalcFields = QrReparcP2CalcFields
    SQL.Strings = (
      'SELECT DISTINCT(TotParc) TotParc '
      'FROM ReparcP2'
      'ORDER BY TotParc')
    Left = 136
    Top = 212
    object QrReparcP2TotParc: TIntegerField
      FieldName = 'TotParc'
    end
    object QrReparcP2TotalVal: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TotalVal'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
  end
  object DsReparcP2: TDataSource
    DataSet = QrReparcP2
    Left = 164
    Top = 212
  end
  object QrParcelasP: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrParcelasPAfterOpen
    BeforeClose = QrParcelasPBeforeClose
    SQL.Strings = (
      'SELECT *'
      'FROM ReparcP2'
      'WHERE TotParc=:P0'
      'ORDER BY NumParc')
    Left = 136
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParcelasPNumParc: TIntegerField
      FieldName = 'NumParc'
      DisplayFormat = '00'
    end
    object QrParcelasPVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrParcelasPValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
  end
  object DsParcelasP: TDataSource
    DataSet = QrParcelasP
    Left = 164
    Top = 240
  end
  object QrTP: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor'
      'FROM ReparcP2'
      'WHERE TotParc=:P0'
      'ORDER BY NumParc')
    Left = 136
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTPValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PercJuros, PercMulta'
      'FROM cond'
      'WHERE Codigo=:P0')
    Left = 164
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondPercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrCondPercMulta: TFloatField
      FieldName = 'PercMulta'
    end
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Credito, lan.Multa,  lan.MoraDia,'
      'lan.Cliente, lan.Depto, lan.Mez, lan.Vencimento, lan.DataCad,'
      'FatNum, lan.Vencimento Vencto, lan.MoraDia TxaJur,'
      'lan.Multa TxaMul, lan.Credito ValBol,'
      'lan.Controle, lan.Sub'
      'FROM Syndic.lct lan'
      'WHERE FatID in (600,601)'
      'AND Reparcel=0'
      'AND Tipo=2'
      'AND FatNum>0'
      'AND lan.Compensado < 2'
      'AND lan.Vencimento BETWEEN :P0 AND :P1'
      'AND lan.ForneceI = :P0'
      'AND lan.Depto = :P1'
      'ORDER BY lan.Vencimento'
      '')
    Left = 224
    Top = 404
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrLctTxaJur: TFloatField
      FieldName = 'TxaJur'
    end
    object QrLctTxaMul: TFloatField
      FieldName = 'TxaMul'
    end
    object QrLctValBol: TFloatField
      FieldName = 'ValBol'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 252
    Top = 404
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 480
    Top = 276
  end
end
