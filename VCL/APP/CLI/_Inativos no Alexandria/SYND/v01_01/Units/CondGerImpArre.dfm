object FmCondGerImpArre: TFmCondGerImpArre
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-006 :: Impress'#227'o de Arrecada'#231#245'es'
  ClientHeight = 586
  ClientWidth = 530
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 530
    Height = 424
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 530
      Height = 45
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 4
        Width = 86
        Height = 13
        Caption = 'T'#237'tulo do relat'#243'rio:'
      end
      object EdTitulo: TEdit
        Left = 16
        Top = 20
        Width = 497
        Height = 21
        TabOrder = 0
      end
    end
    object DBGrid1: TdmkDBGrid
      Left = 0
      Top = 45
      Width = 530
      Height = 328
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TEXTO_IMP'
          Title.Caption = 'Arrecada'#231#227'o'
          Width = 323
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Title.Caption = 'Valor'
          Width = 80
          Visible = True
        end>
      Color = clWindow
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnColEnter = DBGrid1ColEnter
      OnColExit = DBGrid1ColExit
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TEXTO_IMP'
          Title.Caption = 'Arrecada'#231#227'o'
          Width = 323
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Title.Caption = 'Valor'
          Width = 80
          Visible = True
        end>
    end
    object PB2: TProgressBar
      Left = 0
      Top = 407
      Width = 530
      Height = 17
      Align = alBottom
      TabOrder = 2
    end
    object StaticText1: TStaticText
      Left = 0
      Top = 373
      Width = 530
      Height = 17
      Align = alBottom
      Alignment = taCenter
      Caption = 
        'Clique duplo no item de arrecada'#231#227'o iguala o t'#237'tulo ao texto des' +
        'ta arrecada'#231#227'o'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHotLight
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 3
    end
    object PB1: TProgressBar
      Left = 0
      Top = 390
      Width = 530
      Height = 17
      Align = alBottom
      TabOrder = 4
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 530
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 482
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 434
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 340
        Height = 32
        Caption = 'Impress'#227'o de Arrecada'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 340
        Height = 32
        Caption = 'Impress'#227'o de Arrecada'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 340
        Height = 32
        Caption = 'Impress'#227'o de Arrecada'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 472
    Width = 530
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 526
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 516
    Width = 530
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 526
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 382
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 15
          Left = 14
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object BtTudo: TBitBtn
        Tag = 127
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTudoClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 258
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtNenhumClick
      end
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.CodUsu, gg1.Nome, tpg.Preco'
      'FROM gragru1 gg1'
      
        'LEFT JOIN tabeprcgrg tpg ON tpg.Nivel1=gg1.Nivel1 AND tpg.Codigo' +
        '=:P0'
      'WHERE gg1.Nome LIKE :P1'
      'AND gg1.PrdGrupTip=1')
    Left = 68
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesqNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPesqCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPesqPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 40
    Top = 12
  end
  object Query: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    Left = 12
    Top = 12
    object QueryConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QueryArreBaI: TIntegerField
      FieldName = 'ArreBaI'
      Required = True
    end
    object QueryVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QueryTEXTO_IMP: TWideStringField
      FieldName = 'TEXTO_IMP'
      Size = 50
    end
    object QueryAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QueryCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object frxListaA: TfrxReport
    Version = '4.15'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxListaAGetValue
    Left = 4
    Top = 168
    Datasets = <
      item
        DataSet = frxDsCond
        DataSetName = 'frxDsCond'
      end
      item
        DataSet = frxDsCNS
        DataSetName = 'frxDsCNS'
      end
      item
        DataSet = Dmod.frxDsControle
        DataSetName = 'frxDsControle'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsIAI
        DataSetName = 'frxDsIAI'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 219.212740000000000000
        Width = 718.110700000000000000
        DataSet = frxDsIAI
        DataSetName = 'frxDsIAI'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 313.700990000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = frxDsIAI
          DataSetName = 'frxDsIAI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsIAI."Unidade"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Width = 313.700990000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMEPROPRIET'
          DataSet = frxDsIAI
          DataSetName = 'frxDsIAI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsIAI."NOMEPROPRIET"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 396.850650000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsIAI
          DataSetName = 'frxDsIAI'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsIAI."Valor"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 491.338900000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsIAI
          DataSetName = 'frxDsIAI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsIAI."FATOR_COBRADO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 574.488560000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'TITULO_FATOR'
          DataSet = frxDsIAI
          DataSetName = 'frxDsIAI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsIAI."TITULO_FATOR"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 139.842588030000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Width = 718.110700000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 702.992580000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Arrecada'#231#245'es')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Top = 45.354360000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCond."NOMECLI"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 64.252010000000000000
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 313.700990000000000000
          Top = 105.826840000000000000
          Width = 83.149660000000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade'
            'Habitacional')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Top = 105.826840000000000000
          Width = 313.700990000000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ''
            'Nome do Propriet'#225'rio')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 396.850650000000000000
          Top = 105.826840000000000000
          Width = 94.488188980000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor da '
            'arrecada'#231#227'o'
            ''
            '')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 491.338900000000000000
          Top = 105.826840000000000000
          Width = 83.149660000000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator'
            'Cobrado')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 574.488560000000000000
          Top = 105.826840000000000000
          Width = 143.622140000000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Descri'#231#227'o'
            'do Fator')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Top = 86.929190000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPrev."PERIODO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 37.795300000000000000
        Top = 298.582870000000000000
        Width = 718.110700000000000000
        object Memo24: TfrxMemoView
          Top = 11.338590000000010000
          Width = 313.700990000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 396.850650000000000000
          Top = 11.338590000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsIAI."Valor">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 491.338900000000000000
          Top = 11.338590000000010000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsIAI."FATOR_COBRADO">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 574.488560000000000000
          Top = 11.338590000000010000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 313.700990000000000000
          Top = 11.338590000000010000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsIAI
          DataSetName = 'frxDsIAI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[COUNT(MasterData1)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 45.354360000000000000
        Top = 359.055350000000000000
        Width = 718.110700000000000000
      end
    end
  end
  object QrIAI: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 32
    Top = 168
    object QrIAINOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrIAIUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrIAIApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrIAIValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrIAIPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrIAIControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrIAIDeQuem: TSmallintField
      FieldName = 'DeQuem'
    end
    object QrIAICalculo: TSmallintField
      FieldName = 'Calculo'
    end
    object QrIAIPartilha: TSmallintField
      FieldName = 'Partilha'
    end
    object QrIAIPercent: TFloatField
      FieldName = 'Percent'
    end
    object QrIAIMoradores: TIntegerField
      FieldName = 'Moradores'
    end
    object QrIAIFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
    end
    object QrIAIArreBaI: TIntegerField
      FieldName = 'ArreBaI'
      Required = True
    end
    object QrIAIDescriCota: TWideStringField
      FieldName = 'DescriCota'
      Size = 10
    end
    object QrIAIFATOR_COBRADO: TFloatField
      FieldName = 'FATOR_COBRADO'
    end
    object QrIAITITULO_FATOR: TWideStringField
      FieldName = 'TITULO_FATOR'
      Size = 30
    end
  end
  object frxDsIAI: TfrxDBDataset
    UserName = 'frxDsIAI'
    CloseDataSource = False
    DataSet = QrIAI
    BCDToCurrency = False
    Left = 60
    Top = 168
  end
  object frxDsCond: TfrxDBDataset
    UserName = 'frxDsCond'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECLI=NOMECLI'
      'Codigo=Codigo'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'Cliente=Cliente'
      'Observ=Observ'
      'Andares=Andares'
      'TotApt=TotApt'
      'Banco=Banco'
      'Agencia=Agencia'
      'Conta=Conta'
      'CodCedente=CodCedente'
      'DiaVencto=DiaVencto'
      'Posto=Posto'
      'BcoLogoPath=BcoLogoPath'
      'CliLogoPath=CliLogoPath'
      'LocalPag=LocalPag'
      'EspecieDoc=EspecieDoc'
      'Aceite=Aceite'
      'Carteira=Carteira'
      'EspecieVal=EspecieVal'
      'Instrucao1=Instrucao1'
      'Instrucao2=Instrucao2'
      'Instrucao3=Instrucao3'
      'Instrucao4=Instrucao4'
      'Instrucao5=Instrucao5'
      'Instrucao6=Instrucao6'
      'Instrucao7=Instrucao7'
      'Instrucao8=Instrucao8'
      'Correio=Correio'
      'DVB=DVB'
      'NOMEBANCO=NOMEBANCO'
      'CartEmiss=CartEmiss'
      'CIDADE=CIDADE'
      'UF=UF'
      'NOMEUF=NOMEUF'
      'PercMulta=PercMulta'
      'PercJuros=PercJuros'
      'AgContaCed=AgContaCed'
      'DVAgencia=DVAgencia'
      'DVConta=DVConta'
      'TxtParecer=TxtParecer'
      'TitParecer=TitParecer'
      'ModelBloq=ModelBloq'
      'SacadAvali=SacadAvali'
      'Cedente=Cedente'
      'NOMECED=NOMECED'
      'NOMESAC=NOMESAC'
      'NOMECED_IMP=NOMECED_IMP'
      'NOMESAC_IMP=NOMESAC_IMP'
      'CartConcil=CartConcil'
      'MIB=MIB'
      'IDCobranca=IDCobranca'
      'OperCodi=OperCodi'
      'MBB=MBB'
      'MRB=MRB'
      'PBB=PBB'
      'MSB=MSB'
      'PSB=PSB'
      'MPB=MPB'
      'MAB=MAB'
      'ModalCobr=ModalCobr'
      'VTCBBNITAR=VTCBBNITAR'
      'AlterWeb=AlterWeb'
      'PwdSite=PwdSite'
      'ProLaINSSr=ProLaINSSr'
      'ProLaINSSp=ProLaINSSp'
      'Ativo=Ativo'
      'ConfigBol=ConfigBol'
      'MSP=MSP'
      'CART_ATIVO=CART_ATIVO'
      'CAR_TIPODOC=CAR_TIPODOC'
      'OcultaBloq=OcultaBloq'
      'SomaFracao=SomaFracao'
      'Sigla=Sigla'
      'BalAgrMens=BalAgrMens'
      'Compe=Compe'
      'HideCompe=HideCompe')
    DataSet = DmCond.QrCond
    BCDToCurrency = False
    Left = 88
    Top = 168
  end
  object frxDsCNS: TfrxDBDataset
    UserName = 'frxDsCNS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Preco=Preco'
      'Casas=Casas'
      'UnidLei=UnidLei'
      'UnidImp=UnidImp'
      'UnidFat=UnidFat'
      'TOT_UNID1=TOT_UNID1'
      'TOT_UNID2=TOT_UNID2'
      'TOT_VALOR=TOT_VALOR'
      'TOT_UNID1_TXT=TOT_UNID1_TXT'
      'TOT_UNID2_TXT=TOT_UNID2_TXT')
    DataSet = DmCond.QrCNS
    BCDToCurrency = False
    Left = 116
    Top = 168
  end
end
