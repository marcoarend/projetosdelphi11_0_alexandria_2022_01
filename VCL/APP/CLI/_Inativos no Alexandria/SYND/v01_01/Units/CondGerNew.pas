unit CondGerNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, dmkGeral,
  dmkImage, UnDmkEnums;

type
  TFmCondGerNew = class(TForm)
    Panel1: TPanel;
    Label32: TLabel;
    CBMes: TComboBox;
    LaAnoI: TLabel;
    CBAno: TComboBox;
    QrPesq: TmySQLQuery;
    QrPesqPeriodo: TIntegerField;
    QrCond: TmySQLQuery;
    QrCondModelBloq: TSmallintField;
    QrCondConfigBol: TIntegerField;
    CkModBol: TCheckBox;
    QrCondModBol: TmySQLQuery;
    QrCondModBolApto: TIntegerField;
    QrCondModBolModelBloq: TSmallintField;
    QrCondModBolConfigBol: TIntegerField;
    QrCondModBolBalAgrMens: TSmallintField;
    QrCondModBolCompe: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FPeriodo: Integer;
  end;

  var
    FmCondGerNew: TFmCondGerNew;

implementation

uses Module, CondGer, UnMsgInt, UMySQLModule, UnInternalConsts, ModuleCond,
  UnMyObjects;

{$R *.DFM}

procedure TFmCondGerNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerNew.FormCreate(Sender: TObject);
var
  i: Integer;
  //
  Ano, Mes: Word;
begin
  ImgTipo.SQLType := stLok;
  //
  QrCond.Close;
  QrCond.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
  QrCond.Open;
  //
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  //////////////////////////////////////////////////////////////////////////
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Max(Periodo) Periodo');
  QrPesq.SQL.Add('FROM ' + DmCond.FTabPrvA);
  QrPesq.SQL.Add('WHERE Cond=:P0');
  QrPesq.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
  QrPesq.Open;
  if QrPesqPeriodo.Value > 0 then
  begin
    MLAGeral.MesEAnoDePeriodoShort(QrPesqPeriodo.Value+1, Mes, Ano);
    for i := 0 to CBAno.Items.Count do
    begin
      if CBAno.Items[i] = IntToStr(Ano) then
      begin
        CBAno.ItemIndex := i;
        Break;
      end;
    end;
    CBMes.ItemIndex := Mes -1;
  end;
end;

procedure TFmCondGerNew.BtOKClick(Sender: TObject);
var
  Codigo, Periodo, Cond: Integer;
begin
  Cond := DmCond.QrCondCodigo.Value;
  //Cliente := DmCond.QrCondCliente.Value;
  Periodo := MLAGeral.PeriodoEncode(
    Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex+1);
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Periodo');
  QrPesq.SQL.Add('FROM ' + DmCond.FTabPrvA);
  QrPesq.SQL.Add('WHERE Cond=:P0');
  QrPesq.SQL.Add('AND Periodo=:P1');
  QrPesq.Params[0].AsInteger := Cond;
  QrPesq.Params[1].AsInteger := Periodo;
  QrPesq.Open;
  if QrPesqPeriodo.Value = Periodo then
  begin
    Geral.MB_Aviso('Inclus�o de novo or�amento abortado!' + sLineBreak +
      'J� existe or�amento para este condom�nio neste per�odo!');
    Exit;
  end;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ' + DmCond.FTabPrvA + ' SET Encerrado=0, Codigo=:P0, ');
  Dmod.QrUpd.SQL.Add('Cond=:P1, Periodo=:P2, ModelBloq=:P3, ConfigBol=:P4, ');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('');
  //
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    DmCond.FTabPrvA, TAB_PRV, 'Codigo');
  Dmod.QrUpd.Params[00].AsInteger := Codigo;
  Dmod.QrUpd.Params[01].AsInteger := Cond;
  Dmod.QrUpd.Params[02].AsInteger := Periodo;
  Dmod.QrUpd.Params[03].AsInteger := QrCondModelBloq.Value;
  Dmod.QrUpd.Params[04].AsInteger := QrCondConfigBol.Value;
  //
  Dmod.QrUpd.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[06].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.ExecSQL;
  //
  //Inclui itens CondModBol
  if CkModBol.Checked then
  begin
    QrCondModBol.Close;
    QrCondModBol.Params[0].AsInteger := Cond;
    QrCondModBol.Open;
    //
    if QrCondModBol.RecordCount > 0 then
    begin
      QrCondModBol.First;
      while not QrCondModBol.Eof do
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prevmodbol', False,
          ['ModelBloq', 'ConfigBol', 'BalAgrMens', 'Compe'], ['Codigo', 'Apto'],
          [QrCondModBolModelBloq.Value, QrCondModBolConfigBol.Value,
          QrCondModBolBalAgrMens.Value, QrCondModBolCompe.Value], [Codigo,
          QrCondModBolApto.Value], True);
        //
        QrCondModBol.Next;
      end;
    end;
  end; // fim itens mod bol
  FPeriodo := Periodo;
  Close;
end;

end.

