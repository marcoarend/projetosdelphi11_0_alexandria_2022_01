object FmInadimp: TFmInadimp
  Left = 199
  Top = 170
  Caption = 'GER-PROPR-001 :: Pesquisa Pagamentos'
  ClientHeight = 632
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 470
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter2: TSplitter
      Left = 0
      Top = 304
      Width = 1008
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitLeft = 1
      ExplicitTop = 305
      ExplicitWidth = 1044
    end
    object PnPesq1: TPanel
      Left = 0
      Top = 307
      Width = 1008
      Height = 163
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object Splitter1: TSplitter
        Left = 660
        Top = 0
        Height = 163
        Align = alRight
        ExplicitLeft = 819
        ExplicitTop = 1
        ExplicitHeight = 153
      end
      object dmkDBGPesq: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 660
        Height = 163
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Unidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Juridico_TXT'
            Title.Caption = 'SJ'
            Width = 20
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 173
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CREDITO'
            Title.Caption = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MEZ_TXT'
            Title.Caption = 'M'#234's'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VCTO_TXT'
            Title.Caption = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PAGO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SALDO'
            Title.Caption = 'Saldo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Juridico_DESCRI'
            Title.Caption = 'Situa'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEPRPIMOV'
            Title.Caption = 'Propriet'#225'rio'
            Width = 228
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEEMPCOND'
            Title.Caption = 'Condom'#237'nio'
            Width = 228
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsPesq3
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = dmkDBGPesqDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Unidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Juridico_TXT'
            Title.Caption = 'SJ'
            Width = 20
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 173
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CREDITO'
            Title.Caption = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MEZ_TXT'
            Title.Caption = 'M'#234's'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VCTO_TXT'
            Title.Caption = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PAGO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SALDO'
            Title.Caption = 'Saldo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Juridico_DESCRI'
            Title.Caption = 'Situa'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEPRPIMOV'
            Title.Caption = 'Propriet'#225'rio'
            Width = 228
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEEMPCOND'
            Title.Caption = 'Condom'#237'nio'
            Width = 228
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Visible = True
          end>
      end
      object PnPesq2: TPanel
        Left = 663
        Top = 0
        Width = 345
        Height = 163
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 345
          Height = 163
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Genero'
              Title.Caption = 'Conta'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Hist'#243'rico'
              Width = 350
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 65
              Visible = True
            end>
          Color = clWindow
          DataSource = DsIts3
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Genero'
              Title.Caption = 'Conta'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Hist'#243'rico'
              Width = 350
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 65
              Visible = True
            end>
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 304
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 901
        Height = 304
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Splitter3: TSplitter
          Left = 0
          Top = 171
          Width = 901
          Height = 3
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = 1
          ExplicitTop = 170
          ExplicitWidth = 899
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 901
          Height = 171
          Align = alClient
          Caption = ' Configura'#231#227'o da pesquisa: '
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 752
            Top = 8
            Width = 141
            Height = 60
            Caption = ' Per'#237'odo de compet'#234'ncia:'
            TabOrder = 2
            object Label6: TLabel
              Left = 64
              Top = 20
              Width = 6
              Height = 13
              Caption = #224
            end
            object EdMezIni: TEdit
              Left = 8
              Top = 16
              Width = 53
              Height = 21
              TabOrder = 0
              OnExit = EdMezIniExit
              OnKeyDown = EdMezIniKeyDown
            end
            object EdMezFim: TEdit
              Left = 76
              Top = 16
              Width = 53
              Height = 21
              TabOrder = 1
              OnExit = EdMezFimExit
              OnKeyDown = EdMezFimKeyDown
            end
            object CkNaoMensais: TCheckBox
              Left = 8
              Top = 38
              Width = 121
              Height = 17
              Caption = 'Permite n'#227'o mensais.'
              TabOrder = 2
            end
          end
          object GBEmissao: TGroupBox
            Left = 549
            Top = 8
            Width = 200
            Height = 60
            Caption = ' Per'#237'odo de emiss'#227'o: '
            TabOrder = 0
            object CkIniDta: TCheckBox
              Left = 8
              Top = 14
              Width = 90
              Height = 17
              Caption = 'Data inicial:'
              TabOrder = 0
            end
            object TPIniDta: TDateTimePicker
              Left = 8
              Top = 32
              Width = 90
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.777157974500000000
              Time = 37636.777157974500000000
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object CkFimDta: TCheckBox
              Left = 100
              Top = 14
              Width = 90
              Height = 17
              Caption = 'Data final:'
              TabOrder = 2
            end
            object TPFimDta: TDateTimePicker
              Left = 100
              Top = 32
              Width = 90
              Height = 21
              Date = 37636.777203761600000000
              Time = 37636.777203761600000000
              TabOrder = 3
            end
          end
          object GBVencto: TGroupBox
            Left = 549
            Top = 73
            Width = 344
            Height = 60
            Caption = ' Per'#237'odo de vencimento: '
            TabOrder = 1
            object CkIniVct: TCheckBox
              Left = 8
              Top = 14
              Width = 90
              Height = 17
              Caption = 'Data inicial:'
              TabOrder = 0
            end
            object TPIniVct: TDateTimePicker
              Left = 8
              Top = 32
              Width = 90
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.777157974500000000
              Time = 37636.777157974500000000
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object CkFimVct: TCheckBox
              Left = 100
              Top = 14
              Width = 89
              Height = 17
              Caption = 'Data final:'
              TabOrder = 2
            end
            object TPFimVct: TDateTimePicker
              Left = 100
              Top = 32
              Width = 90
              Height = 21
              Date = 37636.777203761600000000
              Time = 37636.777203761600000000
              TabOrder = 3
            end
            object CkFimPgt: TCheckBox
              Left = 212
              Top = 14
              Width = 90
              Height = 17
              Caption = 'Data pesquisa:'
              TabOrder = 4
              Visible = False
            end
            object TPFimPgt: TDateTimePicker
              Left = 212
              Top = 32
              Width = 90
              Height = 21
              Date = 37636.777203761600000000
              Time = 37636.777203761600000000
              TabOrder = 5
              Visible = False
            end
          end
          object PCAgrupamentos: TPageControl
            Left = 2
            Top = 15
            Width = 543
            Height = 154
            ActivePage = TabSheet1
            Align = alLeft
            TabOrder = 3
            object TabSheet1: TTabSheet
              Caption = 'Filtros normais'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel11: TPanel
                Left = 0
                Top = 0
                Width = 535
                Height = 126
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label2: TLabel
                  Left = 4
                  Top = 0
                  Width = 60
                  Height = 13
                  Caption = 'Condom'#237'nio:'
                end
                object Label1: TLabel
                  Left = 4
                  Top = 40
                  Width = 140
                  Height = 13
                  Caption = 'Propriet'#225'rio [F4 mostra todos]:'
                end
                object Label3: TLabel
                  Left = 4
                  Top = 80
                  Width = 124
                  Height = 13
                  Caption = 'Unidade habitacional [F4]:'
                end
                object EdEmpresa: TdmkEditCB
                  Left = 4
                  Top = 16
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  OnChange = EdEmpresaChange
                  OnEnter = EdEmpresaEnter
                  OnExit = EdEmpresaExit
                  DBLookupComboBox = CBEmpresa
                  IgnoraDBLookupComboBox = False
                end
                object CBEmpresa: TdmkDBLookupComboBox
                  Left = 60
                  Top = 16
                  Width = 470
                  Height = 21
                  Color = clWhite
                  KeyField = 'Filial'
                  ListField = 'NOMEFILIAL'
                  ListSource = DModG.DsEmpresas
                  TabOrder = 1
                  dmkEditCB = EdEmpresa
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdPropriet: TdmkEditCB
                  Left = 4
                  Top = 56
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  OnChange = EdProprietChange
                  OnKeyDown = EdProprietKeyDown
                  DBLookupComboBox = CBPropriet
                  IgnoraDBLookupComboBox = False
                end
                object CBPropriet: TdmkDBLookupComboBox
                  Left = 60
                  Top = 56
                  Width = 470
                  Height = 21
                  Color = clWhite
                  KeyField = 'Codigo'
                  ListField = 'NOMEPROP'
                  ListSource = DsPropriet
                  TabOrder = 3
                  OnKeyDown = CBProprietKeyDown
                  dmkEditCB = EdPropriet
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdCondImov: TdmkEditCB
                  Left = 4
                  Top = 96
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  ReadOnly = True
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  OnChange = EdCondImovChange
                  OnKeyDown = EdCondImovKeyDown
                  DBLookupComboBox = CBCondImov
                  IgnoraDBLookupComboBox = False
                end
                object CBCondImov: TdmkDBLookupComboBox
                  Left = 60
                  Top = 96
                  Width = 470
                  Height = 21
                  Color = clWhite
                  KeyField = 'Conta'
                  ListField = 'Unidade'
                  ListSource = DsCondImov
                  TabOrder = 5
                  OnKeyDown = CBCondImovKeyDown
                  dmkEditCB = EdCondImov
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
              end
            end
            object TabSheet2: TTabSheet
              Caption = 'Grupo de UHs / Cond'#244'minos:'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGrid1: TDBGrid
                Left = 0
                Top = 22
                Width = 535
                Height = 104
                Align = alClient
                DataSource = DsCondGriImv
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOMECOND'
                    Title.Caption = 'Condom'#237'nio'
                    Width = 216
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPROP'
                    Title.Caption = 'Propriet'#225'rio'
                    Width = 216
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'UNIDADE'
                    Title.Caption = 'U.H.'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Cond'
                    Title.Caption = 'Cond.'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Apto'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Propr'
                    Title.Caption = 'Propr.'
                    Visible = True
                  end>
              end
              object Panel8: TPanel
                Left = 0
                Top = 0
                Width = 535
                Height = 22
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object SpeedButton1: TSpeedButton
                  Left = 510
                  Top = 0
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton1Click
                end
                object EdCondGri: TdmkEditCB
                  Left = 0
                  Top = 0
                  Width = 44
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  OnChange = EdCondGriChange
                  DBLookupComboBox = CBCondGri
                  IgnoraDBLookupComboBox = False
                end
                object CBCondGri: TdmkDBLookupComboBox
                  Left = 48
                  Top = 0
                  Width = 457
                  Height = 21
                  Color = clWhite
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsCondGri
                  TabOrder = 1
                  dmkEditCB = EdCondGri
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
              end
            end
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 174
          Width = 901
          Height = 130
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 113
            Height = 130
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 113
              Height = 130
              Align = alClient
              Caption = ' Totais da pesquisa: '
              TabOrder = 0
              object Label4: TLabel
                Left = 8
                Top = 14
                Width = 27
                Height = 13
                Caption = 'Valor:'
                FocusControl = DBEdit1
              end
              object Label5: TLabel
                Left = 8
                Top = 52
                Width = 28
                Height = 13
                Caption = 'Pago:'
                FocusControl = DBEdit2
              end
              object Label7: TLabel
                Left = 8
                Top = 90
                Width = 30
                Height = 13
                Caption = 'Saldo:'
                FocusControl = DBEdit3
              end
              object DBEdit1: TDBEdit
                Left = 8
                Top = 28
                Width = 92
                Height = 21
                DataField = 'VALOR'
                DataSource = DsTot3
                TabOrder = 0
              end
              object DBEdit2: TDBEdit
                Left = 8
                Top = 66
                Width = 92
                Height = 21
                DataField = 'PAGO'
                DataSource = DsTot3
                TabOrder = 1
              end
              object DBEdit3: TDBEdit
                Left = 8
                Top = 104
                Width = 92
                Height = 21
                DataField = 'SALDO'
                DataSource = DsTot3
                TabOrder = 2
              end
            end
          end
          object Panel7: TPanel
            Left = 113
            Top = 0
            Width = 788
            Height = 130
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object RGGrupos: TRadioGroup
              Left = 88
              Top = 0
              Width = 60
              Height = 130
              Align = alLeft
              Caption = ' Grupos: '
              ItemIndex = 1
              Items.Strings = (
                '0'
                '1'
                '2'
                '3')
              TabOrder = 1
            end
            object RGOrdem1: TRadioGroup
              Left = 148
              Top = 0
              Width = 100
              Height = 130
              Align = alLeft
              Caption = ' Ordem 1: '
              ItemIndex = 3
              Items.Strings = (
                'Vencimento'
                'Condom'#237'nio'
                'Propriet'#225'rio'
                'Unidade'
                'Compet'#234'ncia'
                'Emiss'#227'o')
              TabOrder = 2
              OnClick = RGOrdem1Click
            end
            object RGOrdem2: TRadioGroup
              Left = 248
              Top = 0
              Width = 100
              Height = 130
              Align = alLeft
              Caption = ' Ordem 2: '
              ItemIndex = 4
              Items.Strings = (
                'Vencimento'
                'Condom'#237'nio'
                'Propriet'#225'rio'
                'Unidade'
                'Compet'#234'ncia'
                'Emiss'#227'o')
              TabOrder = 3
              OnClick = RGOrdem2Click
            end
            object RGOrdem3: TRadioGroup
              Left = 348
              Top = 0
              Width = 100
              Height = 130
              Align = alLeft
              Caption = ' Ordem 3: '
              ItemIndex = 0
              Items.Strings = (
                'Vencimento'
                'Condom'#237'nio'
                'Propriet'#225'rio'
                'Unidade'
                'Compet'#234'ncia'
                'Emiss'#227'o')
              TabOrder = 4
              OnClick = RGOrdem3Click
            end
            object RGOrdem4: TRadioGroup
              Left = 448
              Top = 0
              Width = 100
              Height = 130
              Align = alLeft
              Caption = ' Ordem 4: '
              ItemIndex = 5
              Items.Strings = (
                'Vencimento'
                'Condom'#237'nio'
                'Propriet'#225'rio'
                'Unidade'
                'Compet'#234'ncia'
                'Emiss'#227'o')
              TabOrder = 5
              OnClick = RGOrdem4Click
            end
            object Panel9: TPanel
              Left = 548
              Top = 0
              Width = 88
              Height = 130
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 6
              object RGSomas: TRadioGroup
                Left = 0
                Top = 0
                Width = 88
                Height = 65
                Align = alTop
                Caption = ' Somas: '
                ItemIndex = 1
                Items.Strings = (
                  'Nenhuma'
                  'Bloquetos')
                TabOrder = 0
              end
              object RGImpressao: TRadioGroup
                Left = 0
                Top = 65
                Width = 88
                Height = 65
                Align = alClient
                Caption = ' Impress'#227'o: '
                Enabled = False
                ItemIndex = 0
                Items.Strings = (
                  'Anal'#237'tica'
                  'Sint'#233'tica')
                TabOrder = 1
              end
            end
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 88
              Height = 130
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object RGDocumentos: TRadioGroup
                Left = 0
                Top = 0
                Width = 88
                Height = 74
                Align = alTop
                Caption = ' Documentos: '
                ItemIndex = 2
                Items.Strings = (
                  'Bloquetos'
                  'Reparcel.'
                  'Ambos')
                TabOrder = 0
              end
              object RGSituacao: TRadioGroup
                Left = 0
                Top = 74
                Width = 88
                Height = 56
                Align = alClient
                Caption = ' Situa'#231#227'o: '
                ItemIndex = 1
                Items.Strings = (
                  'Abertos'
                  'Atrasados')
                TabOrder = 1
              end
            end
            object RGPesquisa: TRadioGroup
              Left = 640
              Top = 12
              Width = 129
              Height = 105
              Caption = ' Pesquisa: '
              ItemIndex = 1
              Items.Strings = (
                'Desta Janela'
                'Do Module (Novo!)')
              TabOrder = 7
            end
          end
        end
      end
      object Memo1: TMemo
        Left = 901
        Top = 0
        Width = 107
        Height = 304
        Align = alClient
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 270
        Height = 32
        Caption = 'Pesquisa Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 270
        Height = 32
        Caption = 'Pesquisa Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 270
        Height = 32
        Caption = 'Pesquisa Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 518
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 562
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtCertidao: TBitBtn
        Tag = 10113
        Left = 252
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Certid'#227'o'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtCertidaoClick
      end
      object BtMulJur: TBitBtn
        Tag = 284
        Left = 372
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Multa/Juros'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtMulJurClick
      end
      object BtDiarioAdd: TBitBtn
        Tag = 399
        Left = 492
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Di'#225'rio'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtDiarioAddClick
      end
      object BtReparc: TBitBtn
        Tag = 490
        Left = 612
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Reparc.'
        TabOrder = 5
        OnClick = BtReparcClick
      end
      object BtCobranca: TBitBtn
        Tag = 550
        Left = 732
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Cobran'#231'a'
        TabOrder = 6
        OnClick = BtCobrancaClick
      end
    end
  end
  object QrPropriet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP '
      'FROM condimov cim'
      'LEFT JOIN entidades ent ON ent.Codigo=cim.Propriet'
      'WHERE cim.Codigo<>0'
      'ORDER BY NOMEPROP'
      '')
    Left = 69
    Top = 9
    object QrProprietCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrProprietNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Origin = 'NOMEPROP'
      Required = True
      Size = 100
    end
  end
  object DsPropriet: TDataSource
    DataSet = QrPropriet
    Left = 97
    Top = 9
  end
  object QrCondImov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cim.Conta, cim.Unidade'
      'FROM condimov cim'
      'WHERE cim.Codigo<>0'
      'AND cim.Propriet<>0'
      'ORDER BY cim.Unidade'
      '')
    Left = 125
    Top = 9
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 153
    Top = 9
  end
  object QrPesq3: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesq3AfterOpen
    BeforeClose = QrPesq3BeforeClose
    AfterScroll = QrPesq3AfterScroll
    OnCalcFields = QrPesq3CalcFields
    Left = 916
    Top = 128
    object QrPesq3Unidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPesq3Data: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3CREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesq3PAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesq3SALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesq3Mez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPesq3Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPesq3Compensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPesq3Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesq3NOMEEMPCOND: TWideStringField
      FieldName = 'NOMEEMPCOND'
      Size = 100
    end
    object QrPesq3NOMEPRPIMOV: TWideStringField
      FieldName = 'NOMEPRPIMOV'
      Size = 100
    end
    object QrPesq3Juros: TFloatField
      FieldName = 'Juros'
    end
    object QrPesq3Multa: TFloatField
      FieldName = 'Multa'
    end
    object QrPesq3TOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object QrPesq3FatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPesq3PEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
    end
    object QrPesq3MEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Required = True
      Size = 7
    end
    object QrPesq3VCTO_TXT: TWideStringField
      FieldName = 'VCTO_TXT'
      Size = 10
    end
    object QrPesq3CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPesq3Propriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrPesq3Apto: TIntegerField
      FieldName = 'Apto'
    end
    object QrPesq3Imobiliaria: TIntegerField
      FieldName = 'Imobiliaria'
    end
    object QrPesq3Procurador: TIntegerField
      FieldName = 'Procurador'
    end
    object QrPesq3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPesq3Usuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrPesq3Juridico: TSmallintField
      FieldName = 'Juridico'
    end
    object QrPesq3Juridico_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Juridico_TXT'
      Size = 2
      Calculated = True
    end
    object QrPesq3Juridico_DESCRI: TWideStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'Juridico_DESCRI'
      Size = 50
      Calculated = True
    end
  end
  object DsPesq3: TDataSource
    DataSet = QrPesq3
    Left = 944
    Top = 128
  end
  object QrTot3: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTot3CalcFields
    SQL.Strings = (
      'SELECT SUM(lan.Credito) VALOR,'
      ''
      '  SUM((SELECT SUM(la3.Credito)'
      '  FROM lct la3'
      '  WHERE la3.ID_Pgto = lan.Controle'
      '  AND la3.Data <= "2007-01-31")) PAGO'
      ''
      'FROM lct lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN condimov  imv ON imv.Conta=lan.Depto'
      ''
      'WHERE FatID=600'
      ''
      'AND lan.Tipo=2'
      'AND car.ForneceI=38'
      ' AND lan.Mez > 0 '
      'AND lan.Vencimento  <= "2007-01-31"'
      'AND ( '
      '  (lan.Compensado > "2007-01-31")'
      '  OR (lan.Compensado = 0)'
      '  )'
      ''
      'ORDER BY imv.Unidade, lan.Data')
    Left = 916
    Top = 184
    object QrTot3VALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTot3PAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTot3SALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsTot3: TDataSource
    DataSet = QrTot3
    Left = 944
    Top = 184
  end
  object frxPend_s0i0: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 39141.750700231500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  PageHeader1.Visible := True;'
      'end;'
      ''
      'procedure ReportTitle1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  PageHeader1.Visible := False;'
      'end;'
      ''
      'begin'
      '  if <LogoAdmiExiste> = True then'
      '    Picture1.LoadFromFile(<LogoAdmiCaminho>);'
      ''
      
        '  if <VARF_MAXGRUPO> > 0 then GroupHeader1.Visible := True else ' +
        'GroupHeader1.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 1 then GroupHeader2.Visible := True else ' +
        'GroupHeader2.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 2 then GroupHeader3.Visible := True else ' +
        'GroupHeader3.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 3 then GroupHeader4.Visible := True else ' +
        'GroupHeader4.Visible := False;'
      ''
      
        '  if <VARF_MAXGRUPO> > 0 then GroupFooter1.Visible := True else ' +
        'GroupFooter1.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 1 then GroupFooter2.Visible := True else ' +
        'GroupFooter2.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 2 then GroupFooter3.Visible := True else ' +
        'GroupFooter3.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 3 then GroupFooter4.Visible := True else ' +
        'GroupFooter4.Visible := False;'
      ''
      '  GroupHeader1.Condition := <VARF_GRUPO1>;'
      '  Memo27.Memo.Text := <VARF_HEADR1>;'
      '  Memo35.Memo.Text := <VARF_FOOTR1>;'
      ''
      '  GroupHeader2.Condition := <VARF_GRUPO2>;'
      '  Memo28.Memo.Text := <VARF_HEADR2>;'
      '  Memo34.Memo.Text := <VARF_FOOTR2>;'
      ''
      '  GroupHeader3.Condition := <VARF_GRUPO3>;'
      '  Memo29.Memo.Text := <VARF_HEADR3>;'
      '  Memo38.Memo.Text := <VARF_FOOTR3>;'
      ''
      '  GroupHeader4.Condition := <VARF_GRUPO4>;'
      '  Memo30.Memo.Text := <VARF_HEADR4>;'
      '  Memo46.Memo.Text := <VARF_FOOTR4>;'
      ''
      '  Memo39.Memo.Text := <VARF_ID_VAL>;'
      'end.')
    OnGetValue = frxPend_s0i0GetValue
    Left = 917
    Top = 101
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPesq3
        DataSetName = 'frxDsPesq3'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 168.189076460000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        OnAfterPrint = 'ReportTitle1OnAfterPrint'
        object Picture1: TfrxPictureView
          Left = 1.220470000000000000
          Top = 46.559060000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 173.858267720000000000
          Top = 45.409400000000010000
          Width = 575.338590000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 173.858267720000000000
          Top = 60.472480000000000000
          Width = 574.488560000000000000
          Height = 18.897635350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 173.858267720000000000
          Top = 79.370130000000000000
          Width = 158.740260000000000000
          Height = 15.118105350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_UH]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Top = 113.385900000000000000
          Width = 170.928880000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 173.858380000000000000
          Top = 94.488250000000000000
          Width = 574.488560000000000000
          Height = 15.118105350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO_VCT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 173.858380000000000000
          Top = 109.606370000000000000
          Width = 574.488560000000000000
          Height = 15.118105350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO_DTA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 173.858380000000000000
          Top = 124.724490000000000000
          Width = 574.488560000000000000
          Height = 15.118105350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO_PSQ]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 173.858492280000000000
          Top = 139.842610000000000000
          Width = 574.488560000000000000
          Height = 15.118105350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO_CPT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 79.370130000000000000
          Width = 408.189240000000000000
          Height = 15.118105350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CONDOMINO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Top = 26.456710000000000000
          Width = 748.346940000000000000
          Height = 18.897635350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 75.590600000000000000
          Top = 154.960730000000000000
          Width = 113.385900000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Condom'#237'nio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 672.756340000000000000
          Top = 154.960730000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 188.976500000000000000
          Top = 154.960730000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'U.H.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 241.889920000000000000
          Top = 154.960730000000000000
          Width = 128.504020000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Propriet'#225'rio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 370.393940000000000000
          Top = 154.960730000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 423.307360000000000000
          Top = 154.960730000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Left = 566.929500000000000000
          Top = 154.960730000000000000
          Width = 52.913380940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Top = 154.960730000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 37.795300000000000000
          Top = 154.960730000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Compet.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 710.551640000000000000
          Top = 154.960730000000000000
          Width = 45.354360000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_ID_TIT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 461.102660000000000000
          Top = 154.960730000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 514.016080000000000000
          Top = 154.960730000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 619.842920000000000000
          Top = 154.960730000000000000
          Width = 52.913380940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pendente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 13.228346460000000000
        ParentFont = False
        Top = 517.795610000000000000
        Width = 755.906000000000000000
        DataSet = frxDsPesq3
        DataSetName = 'frxDsPesq3'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 75.590600000000000000
          Width = 113.385900000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMEEMPCOND'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq3."NOMEEMPCOND"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 241.889920000000000000
          Width = 128.504020000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMEPRPIMOV'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq3."NOMEPRPIMOV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 188.976500000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq3."Unidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 672.756340000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq3."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 370.393940000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Credito'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."Credito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 566.929500000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'PAGO'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."PAGO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 619.842920000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'PEND_VAL'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."PEND_VAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'VCTO_TXT'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq3."VCTO_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'MEZ_TXT'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq3."MEZ_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 710.551640000000000000
          Width = 45.354360000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '?????')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 423.307360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Multa'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."Multa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 461.102660000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Juros'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."Juros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 514.016080000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'TOTAL'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."TOTAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 69.921296460000000000
        Top = 211.653680000000000000
        Visible = False
        Width = 755.906000000000000000
        object Memo22: TfrxMemoView
          Top = 37.795299999999910000
          Width = 748.346940000000000000
          Height = 18.897635350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'PEND'#202'NCIAS DE COND'#212'MINOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 75.590600000000000000
          Top = 56.692949999999990000
          Width = 113.385900000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Condom'#237'nio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 672.756340000000000000
          Top = 56.692949999999990000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 188.976500000000000000
          Top = 56.692949999999990000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'U.H.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 241.889920000000000000
          Top = 56.692949999999990000
          Width = 128.504020000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Propriet'#225'rio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 370.393940000000000000
          Top = 56.692949999999990000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 423.307360000000000000
          Top = 56.692949999999990000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 566.929500000000000000
          Top = 56.692949999999990000
          Width = 52.913380940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Top = 56.692949999999990000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 37.795300000000000000
          Top = 56.692949999999990000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Compet.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 710.551640000000000000
          Top = 56.692949999999990000
          Width = 45.354360000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_ID_TIT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 461.102660000000000000
          Top = 56.692949999999990000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 514.016080000000000000
          Top = 56.692949999999990000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 619.842920000000000000
          Top = 56.692949999999990000
          Width = 52.913380940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pendente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 22.677180000000000000
        Top = 343.937230000000000000
        Width = 755.906000000000000000
        OnAfterPrint = 'GroupHeader1OnAfterPrint'
        Condition = 'frxDsPesq3."NOMEEMPCOND"'
        object Memo27: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779530000000022000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cliente: [frxDsPesq3."NOMEEMPCOND"]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 22.677180000000000000
        Top = 389.291590000000000000
        Width = 755.906000000000000000
        Condition = 'frxDsPesq3."NOMEPRPIMOV"'
        object Memo28: TfrxMemoView
          Left = 52.913420000000000000
          Top = 3.779530000000022000
          Width = 702.992580000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cond'#244'mino: [frxDsPesq3."NOMEPRPIMOV"]')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 434.645950000000000000
        Width = 755.906000000000000000
        Condition = 'frxDsPesq3."Unidade"'
        object Memo29: TfrxMemoView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Unidade: [frxDsPesq3."Unidade"]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 476.220780000000000000
        Width = 755.906000000000000000
        Condition = 'frxDsPesq3."Vencimento"'
        object Memo30: TfrxMemoView
          Left = 94.488250000000000000
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Vencimento: [frxDsPesq3."VCTO_TXT"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 691.653990000000000000
        Width = 755.906000000000000000
        object Memo35: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834545590000000000
          Height = 20.787406460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Sub-total Cliente: [frxDsPesq3."NOMEEMPCOND"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 370.393940000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PAGO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 619.842920000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PEND_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Multa">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 461.102660000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Juros">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 646.299630000000000000
        Width = 755.906000000000000000
        object Memo34: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834545590000000000
          Height = 20.787406460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Sub-total Cond'#244'mino: [frxDsPesq3."NOMEPRPIMOV"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 370.393940000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PAGO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 619.842920000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PEND_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Multa">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 461.102660000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Juros">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 600.945270000000000000
        Width = 755.906000000000000000
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834545590000000000
          Height = 20.787406460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Sub-total Unidade: [frxDsPesq3."Unidade"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 370.393940000000000000
          Top = 3.779529999999908000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 566.929500000000000000
          Top = 3.779529999999908000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PAGO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 619.842920000000000000
          Top = 3.779529999999908000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PEND_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 423.307360000000000000
          Top = 3.779529999999908000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Multa">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 461.102660000000000000
          Top = 3.779529999999908000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Juros">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779529999999908000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 555.590910000000000000
        Width = 755.906000000000000000
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834513860000000000
          Height = 20.787406460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Sub-total Vencimento: [frxDsPesq3."VCTO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 370.393940000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PAGO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 619.842920000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PEND_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Multa">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 461.102660000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Juros">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 56.692950000000000000
        Top = 820.158010000000000000
        Width = 755.906000000000000000
        object Memo40: TfrxMemoView
          Left = 563.149970000000000000
          Top = 3.779530000000022000
          Width = 164.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina[Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 22.677180000000000000
        Top = 774.803650000000000000
        Width = 755.906000000000000000
        object Memo41: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779530000000022000
          Width = 332.598305590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'SOMA TOTAL DA PESQUISA:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 370.393940000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PAGO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Left = 619.842920000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PEND_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Multa">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          Left = 461.102660000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Juros">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPesq3: TfrxDBDataset
    UserName = 'frxDsPesq3'
    CloseDataSource = False
    DataSet = QrPesq3
    BCDToCurrency = False
    Left = 945
    Top = 101
  end
  object QrCondGri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM condgri'
      'ORDER BY Nome')
    Left = 181
    Top = 9
    object QrCondGriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondGriNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCondGri: TDataSource
    DataSet = QrCondGri
    Left = 209
    Top = 9
  end
  object QrCondGriImv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(cii.Propr=0,"T O D O S", '
      '  IF(pro.Tipo=0, pro.RazaoSocial, pro.Nome)) NOMEPROP,'
      'IF(cii.Cond=0,"T O D O S", '
      '  IF(con.Tipo=0, con.RazaoSocial, con.Nome)) NOMECOND,'
      'IF(cii.Apto=0,"TODAS", '
      '  imv.Unidade) UNIDADE, cii.* '
      'FROM condgriimv cii'
      'LEFT JOIN condimov  imv ON imv.Conta=cii.Apto'
      'LEFT JOIN cond      cnd ON cnd.Codigo=cii.Cond'
      'LEFT JOIN entidades con ON con.Codigo=cnd.Cliente'
      'LEFT JOIN entidades pro ON pro.Codigo=cii.Propr'
      'WHERE cii.Codigo=:P0')
    Left = 237
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondGriImvNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrCondGriImvNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
    object QrCondGriImvUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object QrCondGriImvCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCondGriImvControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCondGriImvCond: TIntegerField
      FieldName = 'Cond'
      Required = True
    end
    object QrCondGriImvApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrCondGriImvPropr: TIntegerField
      FieldName = 'Propr'
      Required = True
    end
    object QrCondGriImvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCondGriImvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCondGriImvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCondGriImvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCondGriImvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCondGriImvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCondGriImvAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsCondGriImv: TDataSource
    DataSet = QrCondGriImv
    Left = 265
    Top = 9
  end
  object PMMulJur: TPopupMenu
    Left = 300
    Top = 8
    object AlterapercentualdeMulta1: TMenuItem
      Caption = 'Altera percentual de &Multa'
      OnClick = AlterapercentualdeMulta1Click
    end
    object AlterapercentualdeJurosmensais1: TMenuItem
      Caption = '&Altera percentual de &Juros mensais'
      OnClick = AlterapercentualdeJurosmensais1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 176
    Top = 480
    object Pendnciasdecondminos1: TMenuItem
      Caption = 'Pend'#234'ncias de cond'#244'minos'
      OnClick = Pendnciasdecondminos1Click
    end
    object Segundaviadebloquetos1: TMenuItem
      Caption = 'Segunda via de bloquetos'
      OnClick = Segundaviadebloquetos1Click
    end
  end
  object frxCondE2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 39342.853423206000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 920
    Top = 248
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
      end
      item
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Width = 0.100000000000000000
      object Memo129: TfrxMemoView
        Left = 56.692950000000000000
        Top = 904.063390000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO3]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo114: TfrxMemoView
        Left = 602.834645670000000000
        Top = 848.126382130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."SUB_TOT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture2: TfrxPictureView
        Left = 52.913420000000000000
        Top = 718.110700000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        ShowHint = False
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo113: TfrxMemoView
        Left = 602.834645670000000000
        Top = 768.756303390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo101: TfrxMemoView
        Left = 602.834645670000000000
        Top = 839.055660000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo85: TfrxMemoView
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line13: TfrxLineView
        Left = 3.779530000000000000
        Top = 718.110700000000000000
        Width = 789.921770000000000000
        ShowHint = False
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo81: TfrxMemoView
        Left = 578.268090000000000000
        Top = 706.772110000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'AUTENTICA'#199#195'O MEC'#194'NICA')
        ParentFont = False
      end
      object BarCode1: TfrxBarCodeView
        Left = 90.708661417322800000
        Top = 1058.268114410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        ShowHint = False
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line14: TfrxLineView
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 687.874015750000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 52.913420000000000000
        Top = 791.811419060000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 226.771653540000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 294.803152050000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Height = 230.551330000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 430.866420000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 370.393940000000000000
        Top = 816.378480000000000000
        Height = 22.677180000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line23: TfrxLineView
        Left = 291.023810000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 228.661417320000000000
        Top = 839.055660000000000000
        Height = 22.677180000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 160.629921260000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 52.913420000000000000
        Top = 865.512370000000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line27: TfrxLineView
        Left = 52.913420000000000000
        Top = 990.236615910000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line28: TfrxLineView
        Left = 52.913420000000000000
        Top = 1050.709056850000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo82: TfrxMemoView
        Left = 226.771800000000000000
        Top = 737.008350000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo83: TfrxMemoView
        Left = 294.803340000000000000
        Top = 737.008350000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo84: TfrxMemoView
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        Left = 52.913420000000000000
        Top = 793.701300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo88: TfrxMemoView
        Left = 56.692950000000000000
        Top = 771.024120000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCond."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo89: TfrxMemoView
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo90: TfrxMemoView
        Left = 166.299320000000000000
        Top = 816.378480000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo91: TfrxMemoView
        Left = 294.803340000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo92: TfrxMemoView
        Left = 374.173470000000000000
        Top = 816.378480000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo93: TfrxMemoView
        Left = 434.645950000000000000
        Top = 816.378480000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo94: TfrxMemoView
        Left = 166.299320000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo95: TfrxMemoView
        Left = 230.551330000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo96: TfrxMemoView
        Left = 294.803340000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo97: TfrxMemoView
        Left = 434.645950000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo98: TfrxMemoView
        Left = 427.086890000000000000
        Top = 848.504334880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        ShowHint = False
        Color = clWhite
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo99: TfrxMemoView
        Left = 604.724800000000000000
        Top = 793.701300000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo100: TfrxMemoView
        Left = 604.724800000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo102: TfrxMemoView
        Left = 604.724800000000000000
        Top = 865.512370000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo103: TfrxMemoView
        Left = 604.724800000000000000
        Top = 891.969080000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo104: TfrxMemoView
        Left = 604.724800000000000000
        Top = 918.425790000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo105: TfrxMemoView
        Left = 604.724800000000000000
        Top = 941.102970000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo106: TfrxMemoView
        Left = 604.724800000000000000
        Top = 967.559680000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line29: TfrxLineView
        Left = 602.834645670000000000
        Top = 890.079135590000000000
        Width = 137.952755910000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line30: TfrxLineView
        Left = 602.834645670000000000
        Top = 914.646064720000000000
        Width = 137.952755910000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line31: TfrxLineView
        Left = 602.834645670000000000
        Top = 939.212993860000000000
        Width = 137.952755910000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line32: TfrxLineView
        Left = 602.834645670000000000
        Top = 963.779922990000000000
        Width = 137.952755910000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo109: TfrxMemoView
        Left = 589.606680000000000000
        Top = 1054.488870000000000000
        Width = 151.181200000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo110: TfrxMemoView
        Left = 619.842920000000000000
        Top = 1035.591220000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo111: TfrxMemoView
        Left = 52.913420000000000000
        Top = 866.268114410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo115: TfrxMemoView
        Left = 56.692950000000000000
        Top = 801.260360000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCond."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo116: TfrxMemoView
        Left = 56.692950000000000000
        Top = 825.449216770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo117: TfrxMemoView
        Left = 162.519790000000000000
        Top = 825.449216770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."Boleto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo118: TfrxMemoView
        Left = 294.803340000000000000
        Top = 825.449216770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."EspecieDoc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo119: TfrxMemoView
        Left = 374.173470000000000000
        Top = 825.449216770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."Aceite"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo120: TfrxMemoView
        Left = 434.645950000000000000
        Top = 825.449216770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo122: TfrxMemoView
        Left = 166.299320000000000000
        Top = 848.126382130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."Carteira"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo123: TfrxMemoView
        Left = 230.551330000000000000
        Top = 848.126382130000000000
        Width = 56.692950000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."EspecieVal"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo124: TfrxMemoView
        Left = 604.724800000000000000
        Top = 825.449216770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo127: TfrxMemoView
        Left = 56.692950000000000000
        Top = 875.338980550000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO1]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo128: TfrxMemoView
        Left = 56.692950000000000000
        Top = 889.701185280000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo130: TfrxMemoView
        Left = 56.692950000000000000
        Top = 918.425790000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO4]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo131: TfrxMemoView
        Left = 56.692950000000000000
        Top = 932.787799450000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO5]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo132: TfrxMemoView
        Left = 56.692950000000000000
        Top = 947.150004170000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO6]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo133: TfrxMemoView
        Left = 56.692950000000000000
        Top = 961.512208900000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO7]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo134: TfrxMemoView
        Left = 56.692950000000000000
        Top = 975.874413620000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO8]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo121: TfrxMemoView
        Left = 604.724800000000000000
        Top = 801.260360000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_AGCodCed]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo135: TfrxMemoView
        Left = 52.913420000000000000
        Top = 729.449290000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCond."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object frxMemoView1: TfrxMemoView
        Left = 56.692950000000000000
        Top = 480.756030000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO3]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        Left = 602.834645670000000000
        Top = 424.819022130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."SUB_TOT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture1: TfrxPictureView
        Left = 52.913420000000000000
        Top = 294.803340000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        ShowHint = False
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo3: TfrxMemoView
        Left = 602.834645670000000000
        Top = 345.448943390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo4: TfrxMemoView
        Left = 602.834645670000000000
        Top = 415.748300000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo5: TfrxMemoView
        Left = 602.834645670000000000
        Top = 336.378170000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line1: TfrxLineView
        Left = 3.779530000000000000
        Top = 294.803340000000000000
        Width = 789.921770000000000000
        ShowHint = False
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo6: TfrxMemoView
        Left = 578.268090000000000000
        Top = 279.685220000000000000
        Width = 162.519790000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          'SEGUNDA VIA')
        ParentFont = False
      end
      object BarCode2: TfrxBarCodeView
        Left = 90.708661420000000000
        Top = 634.960754410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        ShowHint = False
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line2: TfrxLineView
        Left = 52.913420000000000000
        Top = 336.378170000000000000
        Width = 687.874015750000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line3: TfrxLineView
        Left = 52.913420000000000000
        Top = 368.504059060000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line4: TfrxLineView
        Left = 52.913420000000000000
        Top = 393.071120000000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line5: TfrxLineView
        Left = 52.913420000000000000
        Top = 415.748300000000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line6: TfrxLineView
        Left = 226.771653540000000000
        Top = 313.700990000000000000
        Height = 22.677180000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        Left = 294.803152050000000000
        Top = 313.700990000000000000
        Height = 22.677180000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        Left = 602.834645670000000000
        Top = 336.378170000000000000
        Height = 230.551330000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line9: TfrxLineView
        Left = 430.866420000000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line10: TfrxLineView
        Left = 370.393940000000000000
        Top = 393.071120000000000000
        Height = 22.677180000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line11: TfrxLineView
        Left = 291.023810000000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line12: TfrxLineView
        Left = 228.661417320000000000
        Top = 415.748300000000000000
        Height = 22.677180000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line33: TfrxLineView
        Left = 160.629921260000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line34: TfrxLineView
        Left = 52.913420000000000000
        Top = 442.205010000000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line35: TfrxLineView
        Left = 52.913420000000000000
        Top = 566.929255910000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line36: TfrxLineView
        Left = 52.913420000000000000
        Top = 627.401696850000000000
        Width = 687.874460000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo7: TfrxMemoView
        Left = 226.771800000000000000
        Top = 313.700990000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo8: TfrxMemoView
        Left = 294.803340000000000000
        Top = 313.700990000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo9: TfrxMemoView
        Left = 52.913420000000000000
        Top = 336.378170000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo11: TfrxMemoView
        Left = 52.913420000000000000
        Top = 370.393940000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo12: TfrxMemoView
        Left = 52.913420000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        Left = 56.692950000000000000
        Top = 347.716760000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCond."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo14: TfrxMemoView
        Left = 52.913420000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo15: TfrxMemoView
        Left = 166.299320000000000000
        Top = 393.071120000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo16: TfrxMemoView
        Left = 294.803340000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 374.173470000000000000
        Top = 393.071120000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo18: TfrxMemoView
        Left = 434.645950000000000000
        Top = 393.071120000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo19: TfrxMemoView
        Left = 166.299320000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo20: TfrxMemoView
        Left = 230.551330000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo21: TfrxMemoView
        Left = 294.803340000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo22: TfrxMemoView
        Left = 434.645950000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo23: TfrxMemoView
        Left = 427.086890000000000000
        Top = 425.196974880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        ShowHint = False
        Color = clWhite
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo24: TfrxMemoView
        Left = 604.724800000000000000
        Top = 370.393940000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo25: TfrxMemoView
        Left = 604.724800000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo26: TfrxMemoView
        Left = 604.724800000000000000
        Top = 442.205010000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo27: TfrxMemoView
        Left = 604.724800000000000000
        Top = 468.661720000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo28: TfrxMemoView
        Left = 604.724800000000000000
        Top = 495.118430000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo29: TfrxMemoView
        Left = 604.724800000000000000
        Top = 517.795610000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo30: TfrxMemoView
        Left = 604.724800000000000000
        Top = 544.252320000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line37: TfrxLineView
        Left = 602.834645670000000000
        Top = 466.771775590000000000
        Width = 137.952755910000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line38: TfrxLineView
        Left = 602.834645670000000000
        Top = 491.338704720000000000
        Width = 137.952755910000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line39: TfrxLineView
        Left = 602.834645670000000000
        Top = 515.905633860000000000
        Width = 137.952755910000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line40: TfrxLineView
        Left = 602.834645670000000000
        Top = 540.472562990000000000
        Width = 137.952755910000000000
        ShowHint = False
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo33: TfrxMemoView
        Left = 589.606680000000000000
        Top = 631.181510000000000000
        Width = 151.181200000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo35: TfrxMemoView
        Left = 52.913420000000000000
        Top = 442.960754410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo36: TfrxMemoView
        Left = 56.692950000000000000
        Top = 377.953000000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCond."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo37: TfrxMemoView
        Left = 56.692950000000000000
        Top = 402.141856770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo38: TfrxMemoView
        Left = 162.519790000000000000
        Top = 402.141856770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."Boleto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo39: TfrxMemoView
        Left = 294.803340000000000000
        Top = 402.141856770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."EspecieDoc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo40: TfrxMemoView
        Left = 374.173470000000000000
        Top = 402.141856770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."Aceite"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo41: TfrxMemoView
        Left = 434.645950000000000000
        Top = 402.141856770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo42: TfrxMemoView
        Left = 166.299320000000000000
        Top = 424.819022130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."Carteira"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo43: TfrxMemoView
        Left = 230.551330000000000000
        Top = 424.819022130000000000
        Width = 56.692950000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."EspecieVal"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo44: TfrxMemoView
        Left = 604.724800000000000000
        Top = 402.141856770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo46: TfrxMemoView
        Left = 56.692950000000000000
        Top = 452.031620550000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO1]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo47: TfrxMemoView
        Left = 56.692950000000000000
        Top = 466.393825280000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo48: TfrxMemoView
        Left = 56.692950000000000000
        Top = 495.118430000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO4]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo49: TfrxMemoView
        Left = 56.692950000000000000
        Top = 509.480439450000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO5]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo50: TfrxMemoView
        Left = 56.692950000000000000
        Top = 523.842644170000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO6]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo51: TfrxMemoView
        Left = 56.692950000000000000
        Top = 538.204848900000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO7]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo52: TfrxMemoView
        Left = 56.692950000000000000
        Top = 552.567053620000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO8]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo53: TfrxMemoView
        Left = 604.724800000000000000
        Top = 377.953000000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_AGCodCed]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo54: TfrxMemoView
        Left = 52.913420000000000000
        Top = 306.141930000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCond."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo58: TfrxMemoView
        Left = 56.692950000000000000
        Top = 37.795300000000000000
        Width = 684.094930000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          'Bloqueto em duas vias')
        ParentFont = False
      end
      object Memo108: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1041.259842520000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacador / Avalista')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo125: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1020.473100000000000000
        Width = 449.764070000000000000
        Height = 22.677180000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsInquilino."E_ALL"]')
        ParentFont = False
      end
      object Memo136: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1005.354980000000000000
        Width = 449.764070000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsInquilino."PROPRI_E_MORADOR"]')
        ParentFont = False
        WordWrap = False
      end
      object Memo10: TfrxMemoView
        Left = 226.771800000000000000
        Top = 1041.259842520000000000
        Width = 393.071120000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCond."NOMESAC_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture3: TfrxPictureView
        Left = 56.692950000000000000
        Top = 60.472480000000000000
        Width = 168.188976380000000000
        Height = 68.031496060000000000
        ShowHint = False
        HightQuality = True
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo_049: TfrxMemoView
        Left = 170.078850000000000000
        Top = 990.236860000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo_050: TfrxMemoView
        Left = 226.771800000000000000
        Top = 990.236860000000000000
        Width = 260.787401574803200000
        Height = 15.118112680000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCond."NOMECLI"]  ')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo_055: TfrxMemoView
        Left = 491.338900000000000000
        Top = 990.236860000000000000
        Width = 173.858380000000000000
        Height = 30.236232680000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          'Unidade: [frxDsBoletos."Unidade"]')
        ParentFont = False
        WordWrap = False
      end
      object Memo31: TfrxMemoView
        Left = 619.842920000000000000
        Top = 612.283860000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo32: TfrxMemoView
        Left = 170.078850000000000000
        Top = 597.165740000000000000
        Width = 449.764070000000000000
        Height = 22.677180000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsInquilino."E_ALL"]')
        ParentFont = False
      end
      object Memo34: TfrxMemoView
        Left = 170.078850000000000000
        Top = 582.047620000000000000
        Width = 449.764070000000000000
        Height = 15.118120000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsInquilino."PROPRI_E_MORADOR"]')
        ParentFont = False
        WordWrap = False
      end
      object Memo45: TfrxMemoView
        Left = 170.078850000000000000
        Top = 566.929500000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo55: TfrxMemoView
        Left = 226.771800000000000000
        Top = 566.929500000000000000
        Width = 309.921460000000000000
        Height = 15.118112680000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCond."NOMECLI"]  ')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo56: TfrxMemoView
        Left = 536.693260000000000000
        Top = 566.929500000000000000
        Width = 128.504020000000000000
        Height = 30.236232680000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          'Unidade: [frxDsBoletos."Unidade"]')
        ParentFont = False
        WordWrap = False
      end
    end
  end
  object QrIts3: TmySQLQuery
    Database = Dmod.MyDB
    Left = 916
    Top = 156
    object QrIts3Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrIts3Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrIts3Credito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrIts3Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsIts3: TDataSource
    DataSet = QrIts3
    Left = 944
    Top = 156
  end
  object QrBloqInad: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT *'
      'FROM bloqinad'
      'WHERE Juridico > 0'
      'AND Vencimento < SYSDATE()'
      '')
    Left = 368
    Top = 8
    object QrBloqInadEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrBloqInadPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrBloqInadUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrBloqInadApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrBloqInadImobiliaria: TIntegerField
      FieldName = 'Imobiliaria'
    end
    object QrBloqInadProcurador: TIntegerField
      FieldName = 'Procurador'
    end
    object QrBloqInadUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrBloqInadData: TDateField
      FieldName = 'Data'
    end
    object QrBloqInadCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrBloqInadCREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBloqInadPAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBloqInadJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBloqInadMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBloqInadTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBloqInadSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBloqInadPEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBloqInadDescricao: TWideStringField
      DisplayWidth = 100
      FieldName = 'Descricao'
      Size = 65
    end
    object QrBloqInadMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrBloqInadMEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Size = 7
    end
    object QrBloqInadVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrBloqInadCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrBloqInadControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBloqInadFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBloqInadVCTO_TXT: TWideStringField
      FieldName = 'VCTO_TXT'
      Size = 8
    end
    object QrBloqInadNOMEEMPCOND: TWideStringField
      FieldName = 'NOMEEMPCOND'
      Size = 100
    end
    object QrBloqInadNOMEPRPIMOV: TWideStringField
      FieldName = 'NOMEPRPIMOV'
      Size = 100
    end
    object QrBloqInadAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrBloqInadNewVencto: TDateField
      FieldName = 'NewVencto'
    end
  end
end
