unit CondGerAvisos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, dmkGeral, DB, mySQLDbTables,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, UnDmkEnums;

type
  TFmCondGerAvisos = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Edit10: TEdit;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    MeAvisoVerso: TMemo;
    Label17: TLabel;
    Edit11: TEdit;
    Label18: TLabel;
    Edit12: TEdit;
    Label19: TLabel;
    Edit13: TEdit;
    Edit14: TEdit;
    Label20: TLabel;
    Label21: TLabel;
    Edit15: TEdit;
    Edit16: TEdit;
    Label22: TLabel;
    Label23: TLabel;
    Edit17: TEdit;
    Edit18: TEdit;
    Label24: TLabel;
    Label25: TLabel;
    Edit19: TEdit;
    Edit20: TEdit;
    Label26: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit21: TEdit;
    Edit0: TEdit;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label30: TLabel;
    Label35: TLabel;
    TabSheet3: TTabSheet;
    DsConfigBol: TDataSource;
    QrConfigBol: TmySQLQuery;
    RGModelBloq: TRadioGroup;
    RGCompe: TRadioGroup;
    GBConfig: TGroupBox;
    EdConfigBol: TdmkEditCB;
    CBConfigBol: TdmkDBLookupComboBox;
    Panel3: TPanel;
    CkBalAgrMens: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
    FmCondGerAvisos: TFmCondGerAvisos;

implementation

uses Module, CondGer, UMySQLModule, Principal, UnInternalConsts, ModuleCond,
  UnMyObjects;

{$R *.DFM}

procedure TFmCondGerAvisos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerAvisos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerAvisos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerAvisos.BitBtn1Click(Sender: TObject);
begin
  Edit21.Text := Edit20.Text;
  Edit20.Text := Edit19.Text;
  Edit19.Text := Edit18.Text;
  Edit18.Text := Edit17.Text;
  Edit17.Text := Edit16.Text;
  Edit16.Text := Edit15.Text;
  Edit15.Text := Edit14.Text;
  Edit14.Text := Edit13.Text;
  Edit13.Text := Edit12.Text;
  Edit12.Text := Edit11.Text;
  Edit11.Text := Edit10.Text;
  Edit10.Text := Edit9.Text;
  Edit9.Text  := Edit8.Text;
  Edit8.Text  := Edit7.Text;
  Edit7.Text  := Edit6.Text;
  Edit6.Text  := Edit5.Text;
  Edit5.Text  := Edit4.Text;
  Edit4.Text  := Edit3.Text;
  Edit3.Text  := Edit2.Text;
  Edit2.Text  := Edit1.Text;
  Edit1.Text  := Edit0.Text;
  Edit0.Text  := '';
end;

procedure TFmCondGerAvisos.BitBtn2Click(Sender: TObject);
begin
  Edit0.Text  := Edit1.Text;
  Edit1.Text  := Edit2.Text;
  Edit2.Text  := Edit3.Text;
  Edit3.Text  := Edit4.Text;
  Edit4.Text  := Edit5.Text;
  Edit5.Text  := Edit6.Text;
  Edit6.Text  := Edit7.Text;
  Edit7.Text  := Edit8.Text;
  Edit8.Text  := Edit9.Text;
  Edit9.Text  := Edit10.Text;
  Edit10.Text := Edit11.Text;
  Edit11.Text := Edit12.Text;
  Edit12.Text := Edit13.Text;
  Edit13.Text := Edit14.Text;
  Edit14.Text := Edit15.Text;
  Edit15.Text := Edit16.Text;
  Edit16.Text := Edit17.Text;
  Edit17.Text := Edit18.Text;
  Edit18.Text := Edit19.Text;
  Edit19.Text := Edit20.Text;
  Edit20.Text := Edit21.Text;
  Edit21.Text := '';
end;

procedure TFmCondGerAvisos.BtOKClick(Sender: TObject);
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, DmCond.FTabPrvA, False, [
    'Aviso01', 'Aviso02', 'Aviso03',
    'Aviso04', 'Aviso05', 'Aviso06',
    'Aviso07', 'Aviso08', 'Aviso09',
    'Aviso10', 'AvisoVerso', 'Aviso11',
    'Aviso12', 'Aviso13', 'Aviso14',
    'Aviso15', 'Aviso16', 'Aviso17',
    'Aviso18', 'Aviso19', 'Aviso20',
    'ModelBloq', 'ConfigBol',
    'BalAgrMens', 'Compe'
  ], ['Codigo'], [
    Edit1.Text, Edit2.Text, Edit3.Text,
    Edit4.Text, Edit5.Text, Edit6.Text,
    Edit7.Text, Edit8.Text, Edit9.Text,
    Edit10.Text, MeAvisoVerso.Text, Edit11.Text,
    Edit12.Text, Edit13.Text, Edit14.Text,
    Edit15.Text, Edit16.Text, Edit17.Text,
    Edit18.Text, Edit19.Text, Edit20.Text,
    RGModelBloq.ItemIndex, Geral.IMV(EdConfigBol.Text),
    MLAGeral.BTI(CkBalAgrMens.Checked), RGCompe.ItemIndex
  ], [FmCondGer.QrPrevCodigo.Value], True) then
  begin
    Close;
  end;
end;

procedure TFmCondGerAvisos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FmPrincipal.PreencheModelosBloq(RGModelBloq);
  PageControl1.ActivePageIndex := 0;
  QrConfigBol.Open;
end;

end.
