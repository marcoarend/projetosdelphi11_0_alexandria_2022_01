object FmPrevBaI: TFmPrevBaI
  Left = 339
  Top = 185
  Caption = 'CAD-PROVI-004 :: Item de Provis'#227'o Base'
  ClientHeight = 407
  ClientWidth = 719
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 719
    Height = 245
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 184
    ExplicitTop = 104
    ExplicitHeight = 249
    object Label4: TLabel
      Left = 16
      Top = 8
      Width = 37
      Height = 13
      Caption = 'ID Item:'
      FocusControl = EdControle
    end
    object Label5: TLabel
      Left = 176
      Top = 8
      Width = 109
      Height = 13
      Caption = 'Descri'#231#227'o da provis'#227'o:'
      FocusControl = EdNome
    end
    object Label8: TLabel
      Left = 568
      Top = 88
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object Label6: TLabel
      Left = 16
      Top = 48
      Width = 60
      Height = 13
      Caption = 'Condom'#237'nio:'
    end
    object Label11: TLabel
      Left = 16
      Top = 88
      Width = 340
      Height = 13
      Caption = 
        'Texto a ser usado no lugar do texto base (deixe vazio para usar ' +
        'o base):'
    end
    object Label1: TLabel
      Left = 96
      Top = 8
      Width = 44
      Height = 13
      Caption = 'Provis'#227'o:'
      FocusControl = EdCodigo
    end
    object EdControle: TdmkEdit
      Left = 16
      Top = 24
      Width = 76
      Height = 21
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utIdx
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdNome: TdmkEdit
      Left = 176
      Top = 24
      Width = 381
      Height = 21
      TabStop = False
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 30
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'PrevBaC_Txt'
      UpdType = utNil
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object RgSitCobr: TdmkRadioGroup
      Left = 564
      Top = 0
      Width = 133
      Height = 85
      Caption = ' Fluxo da cobran'#231'a: '
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o cobrar'
        'Cont'#237'nua (mensal)'
        'Programada'
        'Por agendamento')
      TabOrder = 5
      OnClick = RgSitCobrClick
      QryCampo = 'SitCobr'
      UpdCampo = 'SitCobr'
      UpdType = utYes
      OldValor = 0
    end
    object EdValor: TdmkEdit
      Left = 568
      Top = 104
      Width = 129
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Valor'
      UpdCampo = 'Valor'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object GBPer: TGroupBox
      Left = 16
      Top = 128
      Width = 549
      Height = 89
      Caption = 
        ' Per'#237'odo de cobran'#231'a programada (m'#234's e ano) ou cont'#237'nua (s'#243' m'#234's)' +
        ': '
      TabOrder = 8
      Visible = False
      object GBIni: TGroupBox
        Left = 9
        Top = 17
        Width = 264
        Height = 64
        Caption = ' Per'#237'odo Inicial [F4]: '
        TabOrder = 0
        object Label32: TLabel
          Left = 4
          Top = 15
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAnoI: TLabel
          Left = 180
          Top = 15
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBMesI: TComboBox
          Left = 5
          Top = 32
          Width = 172
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 0
          ParentFont = False
          TabOrder = 1
          TabStop = False
          OnChange = CBMesIChange
        end
        object CBAnoI: TComboBox
          Left = 179
          Top = 32
          Width = 78
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 0
          ParentFont = False
          TabOrder = 2
          TabStop = False
          OnChange = CBAnoIChange
        end
        object EdParcPerI: TdmkEdit
          Left = 104
          Top = 0
          Width = 36
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ParcPerI'
          UpdCampo = 'ParcPerI'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdParcPerIChange
          OnKeyDown = EdParcPerIKeyDown
        end
      end
      object GBFim: TGroupBox
        Left = 277
        Top = 17
        Width = 264
        Height = 64
        Caption = ' Per'#237'odo final [F4]: '
        TabOrder = 1
        object Label34: TLabel
          Left = 4
          Top = 15
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAnoF: TLabel
          Left = 180
          Top = 15
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBMesF: TComboBox
          Left = 5
          Top = 32
          Width = 172
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 0
          ParentFont = False
          TabOrder = 1
          TabStop = False
          OnChange = CBMesFChange
        end
        object CBAnoF: TComboBox
          Left = 179
          Top = 32
          Width = 78
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 0
          ParentFont = False
          TabOrder = 2
          TabStop = False
          OnChange = CBAnoFChange
        end
        object EdParcPerF: TdmkEdit
          Left = 96
          Top = 0
          Width = 36
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ParcPerF'
          UpdCampo = 'ParcPerF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdParcPerFChange
          OnKeyDown = EdParcPerFKeyDown
        end
      end
    end
    object EdEmpresa: TdmkEditCB
      Left = 16
      Top = 64
      Width = 41
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cond'
      UpdCampo = 'Cond'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 60
      Top = 64
      Width = 497
      Height = 21
      Color = clWhite
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      ReadOnly = True
      TabOrder = 4
      TabStop = False
      dmkEditCB = EdEmpresa
      QryCampo = 'Cond'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdTexto: TdmkEdit
      Left = 16
      Top = 104
      Width = 549
      Height = 21
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Texto'
      UpdCampo = 'Texto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object CkInfoParc: TdmkCheckBox
      Left = 16
      Top = 220
      Width = 245
      Height = 17
      Caption = 'Informar n'#250'mero da parcela / total de parcelas.'
      TabOrder = 10
      QryCampo = 'InfoParc'
      UpdCampo = 'InfoParc'
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object GbPro: TGroupBox
      Left = 568
      Top = 128
      Width = 129
      Height = 89
      Caption = ' Cobran'#231'a programada: '
      TabOrder = 9
      Visible = False
      object Label7: TLabel
        Left = 12
        Top = 40
        Width = 44
        Height = 13
        Caption = 'Parcelas:'
      end
      object EdParcelas: TdmkEdit
        Left = 64
        Top = 36
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Parcelas'
        UpdCampo = 'Parcelas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
    object EdCodigo: TdmkEdit
      Left = 96
      Top = 24
      Width = 76
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'PrevBaC_Cod'
      UpdType = utNil
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 719
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 671
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 623
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 273
        Height = 32
        Caption = 'Item de Provis'#227'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 273
        Height = 32
        Caption = 'Item de Provis'#227'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 273
        Height = 32
        Caption = 'Item de Provis'#227'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 293
    Width = 719
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 715
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 337
    Width = 719
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 715
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 571
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
end
