unit CreateApp;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTableApp = (ntrtt_Inadimp, ntrtt_Inadim2);
  TAcaoCreateApp = (acDrop, acCreate, acFind);
  TCreateApp = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrttInadimp(Qry: TmySQLQuery);
    procedure Cria_ntrttInadim2(Qry: TmySQLQuery);

  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTableApp;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UnCreateApp: TCreateApp;

implementation

uses UnMyObjects, Module, ModuleGeral;

procedure TCreateApp.Cria_ntrttInadim2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  ForneceI                       int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Apto                           int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Mez                            int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Vencimento                     date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  FatID                          int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum                         double(19,2) NOT NULL DEFAULT 0            ,');
  //
  Qry.SQL.Add('  Propriet                       int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Unidade                        varchar(10)                                ,');
  Qry.SQL.Add('  Imobiliaria                    int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Procurador                     int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Usuario                        int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Juridico                       tinyint(1)   NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Data                           date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  CliInt                         int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  CREDITO                        double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  PAGO                           double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  Juros                          double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  Multa                          double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  TOTAL                          double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  SALDO                          double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  PEND_VAL                       double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  Descricao                      varchar(100)                               ,');
  Qry.SQL.Add('  MEZ_TXT                        varchar(7)                                 ,');
  Qry.SQL.Add('  Compensado                     date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  VCTO_TXT                       varchar(10)                                ,');
  Qry.SQL.Add('  NOMEEMPCOND                    varchar(100)                               ,');
  Qry.SQL.Add('  NOMEPRPIMOV                    varchar(100)                               ,');
  Qry.SQL.Add('  NewVencto                      date         NOT NULL DEFAULT "0000-00-00" ,');
//
  Qry.SQL.Add('  Infl1Indice                    varchar(10)                                ,');
  Qry.SQL.Add('  Infl1PeriI                     int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Infl1PeriF                     int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Infl1FatorI                    double(19,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  Infl1FatorF                    double(19,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  Infl1ValorT                    double(19,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  Infl1Percen                    double(19,6) NOT NULL DEFAULT 0.000000     ,');
  Qry.SQL.Add('  Infl1ValorS                    double(19,3) NOT NULL DEFAULT 0.000        ,');
//
  Qry.SQL.Add('  Infl2Indice                    varchar(10)                                ,');
  Qry.SQL.Add('  Infl2PeriI                     int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Infl2PeriF                     int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Infl2FatorI                    double(19,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  Infl2FatorF                    double(19,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  Infl2ValorT                    double(19,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  Infl2Percen                    double(19,6) NOT NULL DEFAULT 0.000000     ,');
  Qry.SQL.Add('  Infl2ValorS                    double(19,3) NOT NULL DEFAULT 0.000        ,');
//
  Qry.SQL.Add('  InflTValorS                    double(19,2) NOT NULL DEFAULT 0.00         ,');
//
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (Empresa, ForneceI, Apto, Vencimento, FatNum)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;

end;

procedure TCreateApp.Cria_ntrttInadimp(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  ForneceI                       int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Apto                           int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Mez                            int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Vencimento                     date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  FatID                          int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum                         double(20,0) NOT NULL DEFAULT 0            ,');
  //
  Qry.SQL.Add('  Propriet                       int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Unidade                        varchar(10)                                ,');
  Qry.SQL.Add('  Imobiliaria                    int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Procurador                     int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Usuario                        int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Juridico                       tinyint(1)   NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Data                           date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  CliInt                         int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  CREDITO                        double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  PAGO                           double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  Juros                          double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  Multa                          double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  TOTAL                          double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  SALDO                          double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  PEND_VAL                       double(19,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  Descricao                      varchar(100)                               ,');
  Qry.SQL.Add('  MEZ_TXT                        varchar(7)                                 ,');
  Qry.SQL.Add('  Compensado                     date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  VCTO_TXT                       varchar(10)                                ,');
  Qry.SQL.Add('  NOMEEMPCOND                    varchar(100)                               ,');
  Qry.SQL.Add('  NOMEPRPIMOV                    varchar(100)                               ,');
  Qry.SQL.Add('  NewVencto                      date         NOT NULL DEFAULT "0000-00-00" ,');
//
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (Empresa, ForneceI, Apto, Vencimento, FatNum)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TCreateApp.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTableApp; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_Inadimp:          Nome := Lowercase('_Inadimp_');
      ntrtt_Inadim2:          Nome := Lowercase('_Inadim2_');
      //
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrtt_Inadimp: Cria_ntrttInadimp(Qry);
    ntrtt_Inadim2: Cria_ntrttInadim2(Qry);
    //
    else Geral.MensagemBox('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  Result := TabNo;
end;

end.

