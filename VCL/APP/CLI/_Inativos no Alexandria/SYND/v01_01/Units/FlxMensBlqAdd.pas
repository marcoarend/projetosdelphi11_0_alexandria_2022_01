unit FlxMensBlqAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkPermissoes, UnDmkEnums;

type
  TFmFlxMensBlqAdd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Panel6: TPanel;
    Label223: TLabel;
    Label2: TLabel;
    EdFlxB_Ordem: TdmkEdit;
    EdddVence: TdmkEdit;
    GroupBox39: TGroupBox;
    Label211: TLabel;
    Label212: TLabel;
    Label213: TLabel;
    Label214: TLabel;
    Label215: TLabel;
    Label210: TLabel;
    Label209: TLabel;
    Label208: TLabel;
    Label207: TLabel;
    Label206: TLabel;
    EdFlxB_Folha: TdmkEdit;
    EdFlxB_Proto: TdmkEdit;
    EdFlxB_Relat: TdmkEdit;
    EdFlxB_EMail: TdmkEdit;
    EdFlxB_Porta: TdmkEdit;
    EdFlxB_Postl: TdmkEdit;
    EdFlxB_Print: TdmkEdit;
    EdFlxB_Risco: TdmkEdit;
    EdFlxB_LeiAr: TdmkEdit;
    EdFlxB_Fecha: TdmkEdit;
    Panel7: TPanel;
    Label1: TLabel;
    LaMes: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdAnoMes: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFlxMensBlqAdd: TFmFlxMensBlqAdd;

implementation

uses Module, UnMyObjects, ModuleGeral, UMySQLModule;

{$R *.DFM}

procedure TFmFlxMensBlqAdd.BtOKClick(Sender: TObject);
  function CriaData(Ano, Mes, Dia: Integer): TDateTime;
  begin
    if Dia = 0 then
      Result := 0
    else
      Result := EncodeDate(Ano, Mes, 1) + Dia - 1;
  end;
var
  Ordem: Integer;
  Data,
  Folha_DLim,
  LeiAr_DLim,
  Fecha_DLim,
  Risco_DLim,
  Print_DLim,
  Proto_DLim,
  Relat_DLim,
  EMail_DLim,
  Porta_DLim,
  Postl_DLim,
  Abert_Data: TDateTime;
  //
  Codigo, AnoMes, Abert_User: Integer;
  //
  Ano, Mes, Dia, ddVence: Word;
begin
  Codigo := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Codigo=0, EdEmpresa, 'Informe a empresa!') then Exit;
  Data   := EdAnoMes.ValueVariant;
  AnoMes := Geral.DTAM(Data);
  if MyObjects.FIC(Data < 2, EdAnoMes, 'Informe o m�s') then Exit;
  //
  DecodeDate(Data, Ano, Mes, Dia);
  //
  Ordem      := EdFlxB_Ordem.ValueVariant;
  Folha_DLim := CriaData(Ano, Mes, EdFlxB_Folha.ValueVariant);
  LeiAr_DLim := CriaData(Ano, Mes, EdFlxB_LeiAr.ValueVariant);
  Fecha_DLim := CriaData(Ano, Mes, EdFlxB_Fecha.ValueVariant);
  Risco_DLim := CriaData(Ano, Mes, EdFlxB_Risco.ValueVariant);
  Print_DLim := CriaData(Ano, Mes, EdFlxB_Print.ValueVariant);
  Proto_DLim := CriaData(Ano, Mes, EdFlxB_Proto.ValueVariant);
  Relat_DLim := CriaData(Ano, Mes, EdFlxB_Relat.ValueVariant);
  EMail_DLim := CriaData(Ano, Mes, EdFlxB_EMail.ValueVariant);
  Porta_DLim := CriaData(Ano, Mes, EdFlxB_Porta.ValueVariant);
  Postl_DLim := CriaData(Ano, Mes, EdFlxB_Postl.ValueVariant);
  //
  Abert_User := VAR_USUARIO;
  Abert_Data := DModG.ObtemAgora();
  ddVence    := EdddVence.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'flxbloq', False, [
  'Ordem', 'ddVence', 'Abert_User', 'Abert_Data',
  'Folha_DLim',
  'LeiAr_DLim',
  'Fecha_DLim',
  'Risco_DLim',
  'Print_DLim',
  'Proto_DLim',
  'Relat_DLim',
  'EMail_DLim',
  'Porta_DLim',
  'Postl_DLim'
  ], [
  'AnoMes', 'Codigo'], [
  Ordem, ddVence, Abert_User, Abert_Data,
  Folha_DLim,
  LeiAr_DLim,
  Fecha_DLim,
  Risco_DLim,
  Print_DLim,
  Proto_DLim,
  Relat_DLim,
  EMail_DLim,
  Porta_DLim,
  Postl_DLim
  ], [
  AnoMes, Codigo], True) then
  Close;
end;

procedure TFmFlxMensBlqAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFlxMensBlqAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFlxMensBlqAdd.FormCreate(Sender: TObject);
begin
  EdAnoMes.ValueVariant := Date;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmFlxMensBlqAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
