object FmFlxMensSel: TFmFlxMensSel
  Left = 339
  Top = 185
  Caption = 'FLX-ENCER-001 :: Sele'#231#227'o de Fluxo'
  ClientHeight = 273
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 554
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 506
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 458
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 209
        Height = 32
        Caption = 'Sele'#231#227'o de Fluxo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 209
        Height = 32
        Caption = 'Sele'#231#227'o de Fluxo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 209
        Height = 32
        Caption = 'Sele'#231#227'o de Fluxo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 554
    Height = 111
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 554
      Height = 111
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 554
        Height = 111
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 64
          Width = 60
          Height = 13
          Caption = 'Condom'#237'nio:'
        end
        object LaMes: TLabel
          Left = 488
          Top = 64
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object RGFluxo: TRadioGroup
          Left = 12
          Top = 12
          Width = 533
          Height = 48
          Caption = ' Tipo de Fluxo: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            '[NENHUM]'
            'Balancete'
            'Bloqueto')
          TabOrder = 0
          OnClick = RGFluxoClick
        end
        object EdEmpresa: TdmkEditCB
          Left = 12
          Top = 80
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 68
          Top = 80
          Width = 413
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 2
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdAnoMes: TdmkEdit
          Left = 488
          Top = 80
          Width = 56
          Height = 21
          Alignment = taCenter
          TabOrder = 3
          FormatType = dmktfMesAno
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfShort
          QryCampo = 'Mez'
          UpdType = utYes
          Obrigatorio = True
          PermiteNulo = False
          ValueVariant = Null
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 159
    Width = 554
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 550
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 203
    Width = 554
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 408
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 406
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrLocBal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM flxmens'
      'WHERE Codigo=:P0'
      'AND AnoMes=:P1')
    Left = 316
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocBalConci_Data: TDateTimeField
      FieldName = 'Conci_Data'
    end
    object QrLocBalDocum_Data: TDateTimeField
      FieldName = 'Docum_Data'
    end
    object QrLocBalContb_Data: TDateTimeField
      FieldName = 'Contb_Data'
    end
    object QrLocBalAnali_Data: TDateTimeField
      FieldName = 'Anali_Data'
    end
    object QrLocBalEncad_Data: TDateTimeField
      FieldName = 'Encad_Data'
    end
    object QrLocBalEntrg_Data: TDateTimeField
      FieldName = 'Entrg_Data'
    end
    object QrLocBalConci_DLim: TDateField
      FieldName = 'Conci_DLim'
    end
    object QrLocBalDocum_DLim: TDateField
      FieldName = 'Docum_DLim'
    end
    object QrLocBalContb_DLim: TDateField
      FieldName = 'Contb_DLim'
    end
    object QrLocBalAnali_DLim: TDateField
      FieldName = 'Anali_DLim'
    end
    object QrLocBalEncad_DLim: TDateField
      FieldName = 'Encad_DLim'
    end
    object QrLocBalEntrg_DLim: TDateField
      FieldName = 'Entrg_DLim'
    end
    object QrLocBalWeb01_DLim: TDateField
      FieldName = 'Web01_DLim'
    end
    object QrLocBalWeb01_Data: TDateTimeField
      FieldName = 'Web01_Data'
    end
  end
  object QrLocBlq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM flxbloq'
      'WHERE Codigo=:P0'
      'AND AnoMes=:P1')
    Left = 344
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocBlqFolha_DLim: TDateField
      FieldName = 'Folha_DLim'
      Origin = 'flxbloq.Folha_DLim'
    end
    object QrLocBlqLeiAr_DLim: TDateField
      FieldName = 'LeiAr_DLim'
      Origin = 'flxbloq.LeiAr_DLim'
    end
    object QrLocBlqFecha_DLim: TDateField
      FieldName = 'Fecha_DLim'
      Origin = 'flxbloq.Fecha_DLim'
    end
    object QrLocBlqRisco_DLim: TDateField
      FieldName = 'Risco_DLim'
      Origin = 'flxbloq.Risco_DLim'
    end
    object QrLocBlqPrint_DLim: TDateField
      FieldName = 'Print_DLim'
      Origin = 'flxbloq.Print_DLim'
    end
    object QrLocBlqProto_DLim: TDateField
      FieldName = 'Proto_DLim'
      Origin = 'flxbloq.Proto_DLim'
    end
    object QrLocBlqRelat_DLim: TDateField
      FieldName = 'Relat_DLim'
      Origin = 'flxbloq.Relat_DLim'
    end
    object QrLocBlqEMail_DLim: TDateField
      FieldName = 'EMail_DLim'
      Origin = 'flxbloq.EMail_DLim'
    end
    object QrLocBlqPorta_DLim: TDateField
      FieldName = 'Porta_DLim'
      Origin = 'flxbloq.Porta_DLim'
    end
    object QrLocBlqPostl_DLim: TDateField
      FieldName = 'Postl_DLim'
      Origin = 'flxbloq.Postl_DLim'
    end
    object QrLocBlqFolha_Data: TDateTimeField
      FieldName = 'Folha_Data'
      Origin = 'flxbloq.Folha_Data'
    end
    object QrLocBlqLeiAr_Data: TDateTimeField
      FieldName = 'LeiAr_Data'
      Origin = 'flxbloq.LeiAr_Data'
    end
    object QrLocBlqFecha_Data: TDateTimeField
      FieldName = 'Fecha_Data'
      Origin = 'flxbloq.Fecha_Data'
    end
    object QrLocBlqRisco_Data: TDateTimeField
      FieldName = 'Risco_Data'
      Origin = 'flxbloq.Risco_Data'
    end
    object QrLocBlqPrint_Data: TDateTimeField
      FieldName = 'Print_Data'
      Origin = 'flxbloq.Print_Data'
    end
    object QrLocBlqProto_Data: TDateTimeField
      FieldName = 'Proto_Data'
      Origin = 'flxbloq.Proto_Data'
    end
    object QrLocBlqRelat_Data: TDateTimeField
      FieldName = 'Relat_Data'
      Origin = 'flxbloq.Relat_Data'
    end
    object QrLocBlqEMail_Data: TDateTimeField
      FieldName = 'EMail_Data'
      Origin = 'flxbloq.EMail_Data'
    end
    object QrLocBlqPorta_Data: TDateTimeField
      FieldName = 'Porta_Data'
      Origin = 'flxbloq.Porta_Data'
    end
    object QrLocBlqPostl_Data: TDateTimeField
      FieldName = 'Postl_Data'
      Origin = 'flxbloq.Postl_Data'
    end
    object QrLocBlqddVence: TSmallintField
      FieldName = 'ddVence'
    end
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FlxB_Ordem, FlxB_Folha, '
      'FlxB_LeiAr, FlxB_Fecha, FlxB_Risco, '
      'FlxB_Print, FlxB_Proto, FlxB_Relat, '
      'FlxB_EMail, FlxB_Porta, FlxB_Postl, '
      'FlxM_Ordem, FlxM_Conci, FlxM_Docum, '
      'FlxM_Anali, FlxM_Contb, FlxM_Encad, '
      'FlxM_Entrg, DiaVencto'
      'FROM cond'
      'WHERE Codigo=:P0')
    Left = 400
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondFlxB_Ordem: TIntegerField
      FieldName = 'FlxB_Ordem'
    end
    object QrCondFlxB_Folha: TSmallintField
      FieldName = 'FlxB_Folha'
    end
    object QrCondFlxB_LeiAr: TSmallintField
      FieldName = 'FlxB_LeiAr'
    end
    object QrCondFlxB_Fecha: TSmallintField
      FieldName = 'FlxB_Fecha'
    end
    object QrCondFlxB_Risco: TSmallintField
      FieldName = 'FlxB_Risco'
    end
    object QrCondFlxB_Print: TSmallintField
      FieldName = 'FlxB_Print'
    end
    object QrCondFlxB_Proto: TSmallintField
      FieldName = 'FlxB_Proto'
    end
    object QrCondFlxB_Relat: TSmallintField
      FieldName = 'FlxB_Relat'
    end
    object QrCondFlxB_EMail: TSmallintField
      FieldName = 'FlxB_EMail'
    end
    object QrCondFlxB_Porta: TSmallintField
      FieldName = 'FlxB_Porta'
    end
    object QrCondFlxB_Postl: TSmallintField
      FieldName = 'FlxB_Postl'
    end
    object QrCondFlxM_Ordem: TIntegerField
      FieldName = 'FlxM_Ordem'
    end
    object QrCondFlxM_Conci: TSmallintField
      FieldName = 'FlxM_Conci'
    end
    object QrCondFlxM_Docum: TSmallintField
      FieldName = 'FlxM_Docum'
    end
    object QrCondFlxM_Anali: TSmallintField
      FieldName = 'FlxM_Anali'
    end
    object QrCondFlxM_Contb: TSmallintField
      FieldName = 'FlxM_Contb'
    end
    object QrCondFlxM_Encad: TSmallintField
      FieldName = 'FlxM_Encad'
    end
    object QrCondFlxM_Entrg: TSmallintField
      FieldName = 'FlxM_Entrg'
    end
    object QrCondDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 288
    Top = 12
  end
end
