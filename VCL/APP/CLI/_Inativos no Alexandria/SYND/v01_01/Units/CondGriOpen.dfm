object FmCondGriOpen: TFmCondGriOpen
  Left = 339
  Top = 185
  Caption = 'CAD-CONDO-014 :: Bloquetos em Aberto de Grupos de UHs'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 398
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 782
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 162
        Height = 13
        Caption = 'Grupo de Unidades Habitacionais:'
      end
      object EdCondGri: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdCondGriChange
        DBLookupComboBox = CBCondGri
        IgnoraDBLookupComboBox = False
      end
      object CBCondGri: TdmkDBLookupComboBox
        Left = 72
        Top = 20
        Width = 677
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCondGri
        TabOrder = 1
        dmkEditCB = EdCondGri
        UpdType = utYes
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 52
      Width = 204
      Height = 280
      Align = alLeft
      DataSource = DsCondGriIts
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CliInt'
          Title.Caption = 'Entidade'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Depto'
          Title.Caption = 'UH'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ForneceI'
          Title.Caption = 'Prop.'
          Width = 56
          Visible = True
        end>
    end
    object DBGrid2: TDBGrid
      Left = 204
      Top = 52
      Width = 580
      Height = 280
      Align = alClient
      DataSource = DsAbertos
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 486
        Height = 32
        Caption = 'Bloquetos em Aberto de Grupos de UHs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 486
        Height = 32
        Caption = 'Bloquetos em Aberto de Grupos de UHs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 486
        Height = 32
        Caption = 'Bloquetos em Aberto de Grupos de UHs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 380
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 378
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 422
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Pesquisa'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrCondGri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM condgri'
      'ORDER BY Nome')
    Left = 308
    Top = 137
    object QrCondGriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondGriNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCondGri: TDataSource
    DataSet = QrCondGri
    Left = 336
    Top = 137
  end
  object QrCondGriIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo CliInt, grm.Apto Depto, grm.Propr ForneceI'
      'FROM condgriimv grm'
      'LEFT JOIN condgri gri ON gri.Codigo = grm.Codigo'
      'LEFT JOIN cond con ON con.Codigo = grm.Cond'
      'LEFT JOIN entidades ent ON ent.Codigo = con.Cliente'
      'WHERE gri.Codigo=:P0'
      '')
    Left = 364
    Top = 137
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondGriItsCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrCondGriItsDepto: TIntegerField
      FieldName = 'Depto'
      Required = True
    end
    object QrCondGriItsForneceI: TIntegerField
      FieldName = 'ForneceI'
      Required = True
    end
  end
  object DsCondGriIts: TDataSource
    DataSet = QrCondGriIts
    Left = 392
    Top = 137
  end
  object QrAbertos: TmySQLQuery
    Database = Dmod.MyDB
    Left = 420
    Top = 137
  end
  object DsAbertos: TDataSource
    DataSet = QrAbertos
    Left = 448
    Top = 137
  end
end
