unit WOpcoesSyndic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBLookupComboBox,
  dmkEditCB, mySQLDbTables, DmkDAC_PF, Variants;

type
  TFmWOpcoesSyndic = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrWControl: TmySQLQuery;
    QrDirWebBalan: TmySQLQuery;
    IntegerField10: TIntegerField;
    QrDirWebBalanNome: TWideStringField;
    DsDirWebBalan: TDataSource;
    QrSenha: TmySQLQuery;
    Label20: TLabel;
    EdUsername: TdmkEdit;
    EdPassword: TdmkEdit;
    Label21: TLabel;
    Label22: TLabel;
    EdNomeAdmi: TdmkEdit;
    Label23: TLabel;
    EdDirWebBalancete: TdmkEditCB;
    CBDirWebBalancete: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao();
  public
    { Public declarations }
  end;

  var
  FmWOpcoesSyndic: TFmWOpcoesSyndic;

implementation

uses UnMyObjects, Module, UMySQLModule, MyListas, ModuleGeral;

{$R *.DFM}

procedure TFmWOpcoesSyndic.BtOKClick(Sender: TObject);
var
  NomeAdmi, Username, Password: String;
  DirWebBalancete, User_ID: Integer;
begin
  NomeAdmi        := EdNomeAdmi.ValueVariant;
  Username        := EdUsername.ValueVariant;
  Password        := EdPassword.ValueVariant;
  DirWebBalancete := EdDirWebBalancete.ValueVariant;
  User_ID         := QrSenha.FieldByName('User_ID').AsInteger;
  //
  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
    'UPDATE wcontrol SET ',
    'NomeAdmi="' + NomeAdmi + '", ',
    'DirWebBalancete="' + Geral.FF0(DirWebBalancete) + '" ',
    '']) then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
      'UPDATE users SET ',
      'Username="' + Username + '", ',
      'Password="' + Password + '" ',
      'WHERE User_ID=' + Geral.FF0(User_ID),
      '']);
    Close;
  end;
end;

procedure TFmWOpcoesSyndic.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOpcoesSyndic.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWOpcoesSyndic.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrWControl, Dmod.MyDBn);
  UMyMod.AbreQuery(QrSenha, Dmod.MyDBn);
  UMyMod.AbreQuery(QrDirWebBalan, Dmod.MyDBn);
  //
  MostraEdicao();
end;

procedure TFmWOpcoesSyndic.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOpcoesSyndic.MostraEdicao();
begin
  EdUsername.ValueVariant        := QrSenha.FieldByName('Username').AsString;
  EdPassword.ValueVariant        := QrSenha.FieldByName('Password').AsString;
  EdNomeAdmi.ValueVariant        := QrWControl.FieldByName('NomeAdmi').AsString;
  EdDirWebBalancete.ValueVariant := QrWControl.FieldByName('DirWebBalancete').AsInteger;
  CBDirWebBalancete.KeyValue     := QrWControl.FieldByName('DirWebBalancete').AsInteger;
end;

end.
