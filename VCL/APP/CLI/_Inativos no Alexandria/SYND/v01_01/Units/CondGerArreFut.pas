unit CondGerArreFut;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, ComCtrls,
  Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  TFmCondGerArreFut = class(TForm)
    Panel1: TPanel;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    DsContas: TDataSource;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    LaDeb: TLabel;
    EdValor: TdmkEdit;
    CkContinuar: TCheckBox;
    Label1: TLabel;
    EdApto: TdmkEditCB;
    CBApto: TdmkDBLookupComboBox;
    QrAptos: TmySQLQuery;
    DsAptos: TDataSource;
    QrAptosUnidade: TWideStringField;
    QrAptosConta: TIntegerField;
    QrAptosPropriet: TIntegerField;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    LaAno: TLabel;
    RichEdit1: TRichEdit;
    Label3: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    EdCNAB_Cfg: TdmkEditCB;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    Label4: TLabel;
    QrCNAB_Cfg: TmySQLQuery;
    DsCNAB_Cfg: TDataSource;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    QrAptosCodigo: TIntegerField;
    QrAptosCliente: TIntegerField;
    QrLoc: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdAptoChange(Sender: TObject);
  private
    { Private declarations }
    FArreFutControle, FIni: Integer;
  public
    { Public declarations }
    FCond: Integer;
  end;

  var
  FmCondGerArreFut: TFmCondGerArreFut;

implementation

uses Module, UnInternalConsts, UMySQLModule, UnMSgInt, ModuleCond, CondGer,
  UnMyObjects, UnBloquetosCond, UnDmkProcFunc;

{$R *.DFM}

procedure TFmCondGerArreFut.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerArreFut.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerArreFut.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerArreFut.EdAptoChange(Sender: TObject);
var
  Apto: Integer;
begin
  Apto := EdApto.ValueVariant;
  //
  if Apto <> 0 then
  begin
    DmCond.ReopenCNAB_CfgSel(QrCNAB_Cfg, Dmod.MyDB, QrAptosCliente.Value);
  end;
end;

procedure TFmCondGerArreFut.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Periodo: Integer;
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if QrContasMensal.Value = 'V' then
    begin
      //Mes := FmCondGer.QrPrevPERIODO_TXT.Value else Mes := '';
      Periodo := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
      Mes := dmkPF.MesEAnoDoPeriodoLongo(Periodo);
    end;
    if key=VK_F4 then EdDescricao.Text := QrContasNome.Value+' '+Mes;
    if key=VK_F5 then EdDescricao.Text := QrContasNome2.Value+' '+Mes;
    if key=VK_F6 then EdDescricao.Text := QrContasNome3.Value+' '+Mes;
    EdDescricao.SetFocus;
    EdDescricao.SelStart := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end;
end;

procedure TFmCondGerArreFut.BtOKClick(Sender: TObject);
var
  Conta, Controle, Apto, Periodo, CNAB_Cfg: Integer;
  Valor: Double;
  Texto, Msg: String;
begin
  //Periodo := (((CBAno.ItemIndex + FIni) - 2000) * 12) + (CBMes.ItemIndex + 1);
  Periodo := MLAGeral.PeriodoEncode(Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex+1);
  //
  Apto     := EdApto.ValueVariant;
  Conta    := EdConta.ValueVariant;
  Valor    := EdValor.ValueVariant;
  Texto    := Trim(EdDescricao.Text);
  CNAB_Cfg := EdCNAB_Cfg.ValueVariant;
  //
  if (Apto = 0) or (Conta = 0) or (Valor < 0.01) or (Texto = '') then
  begin
    Geral.MensagemBox('A unidade, a conta, a descri��o e o valor devem ser informados!',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  if not UBloquetosCond.ValidaArrecadacaoConsumoCond(FCond, CNAB_Cfg, QrLoc,
    Dmod.MyDB, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    EdCNAB_Cfg.SetFocus;
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add('INSERT INTO ArreFut SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE ArreFut SET');
  Dmod.QrUpd.SQL.Add('Conta=:P0, Texto=:P1, Valor=:P2, Apto=:P3, ');
  Dmod.QrUpd.SQL.Add('Propriet=:P4, Cond=:P5, Periodo=:P6, CNAB_Cfg=:P7, ');
  Dmod.QrUpd.SQL.Add('');
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Controle=:Pc');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ArreFut', 'ArreFut', 'Controle');
  end else begin
    Dmod.QrUpd.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Controle=:Pc');
    Controle := FArreFutControle;
  end;
  Dmod.QrUpd.Params[00].AsInteger := Conta;
  Dmod.QrUpd.Params[01].AsString  := Texto;
  Dmod.QrUpd.Params[02].AsFloat   := Valor;
  Dmod.QrUpd.Params[03].AsInteger := Apto;
  Dmod.QrUpd.Params[04].AsInteger := QrAptosPropriet.Value;
  Dmod.QrUpd.Params[05].AsInteger := FCond;
  Dmod.QrUpd.Params[06].AsInteger := Periodo;
  Dmod.QrUpd.Params[07].AsInteger := CNAB_Cfg;
  //
  Dmod.QrUpd.Params[08].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[09].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[10].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  DmCond.ReopenArreFut(FCond,
    0{Geral.IMV(FmCondGer.EdMesesArreFut.Text)}, Controle);
  {FmCondGer.CalculaTotalArreFutEReabreArreEArreFut(
    FmCondGer.QrArreApto.Value,
    FmCondGer.QrArrePropriet.Value,
    Controle);}
  if CkContinuar.Checked then
  begin
    EdApto.ValueVariant := 0;
    CBApto.KeyValue     := NULL;
    //
    EdCNAB_Cfg.ValueVariant := 0;
    CBCNAB_Cfg.KeyValue     := Null;
    //
    CBMes.SetFocus;
    ImgTipo.SQLType := stIns;
    //
    Geral.MB_Aviso('Item inclu�do com sucesso!');
  end else Close;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmCondGerArreFut.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, 0);
  //
  QrContas.Open;
  //
(*
  RichEdit1.Lines :=
'{\rtf1\ansi\ansicpg1252\deff0\deflang1046{\fonttbl{\f0\fswiss\fcharset0 Arial;}{\f1\fnil\fcharset0 MS Sans Serif;}}' +
'{\colortbl ;\red255\green0\blue0;\red0\green0\blue128;\red0\green0\blue0;}' +
'\viewkind4\uc1\pard\cf1\b\f0\fs20 ATEN\'#39'c7\'#39'c3O:\cf0\b0\par\par' +
'     Os lan\'#39'e7amentos aqui realizados \cf1\b s\'#39'f3\cf0\b0  \cf1' +
'\b ser\'#39'e3o inclu\'#39'eddos \cf0\b0 na arrecada\'#39'e7\'#39'e3o quando for' +
' executada a arrecada\'#39'e7\'#39'e3o atrav\'#39'e9s da a\'#39'e7\'#39'e3o "\cf2\b ' +
'Inclui intens base de arrecada\'#39'e7\'#39'e3o\cf0\b0 " ou "\cf2\b Veri' +
'fica itens pr\'#39'e9-agendados\cf0\b0 " no menu do bot\'#39'e3o arrecad' +
'a\'#39'e7\'#39'e3o!\par\cf3\f1\fs16\par}';
*)
end;

end.
