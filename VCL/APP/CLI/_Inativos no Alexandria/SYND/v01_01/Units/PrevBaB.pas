unit PrevBaB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  Mask, DBCtrls, dmkGeral, dmkImage, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmPrevBaB = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    TbPrevBAB: TmySQLTable;
    DsPrevBAB: TDataSource;
    QrPrevBaC: TmySQLQuery;
    QrPrevBaCCodigo: TIntegerField;
    QrPrevBaCConta: TIntegerField;
    QrPrevBaCNome: TWideStringField;
    DsPrevBaC: TDataSource;
    QrConta: TmySQLQuery;
    QrContaZeros: TLargeintField;
    TbPrevBABConta: TIntegerField;
    TbPrevBABPrevBaI: TIntegerField;
    TbPrevBABPrevBaC: TIntegerField;
    TbPrevBABValor: TFloatField;
    TbPrevBABTexto: TWideStringField;
    TbPrevBABAdiciona: TSmallintField;
    TbPrevBABNOMECONTABASE: TWideStringField;
    TbPrevBABNOMECONTA: TWideStringField;
    TbPrevBABLastM: TWideStringField;
    TbPrevBABLast1: TFloatField;
    TbPrevBABLast6: TFloatField;
    Panel3: TPanel;
    Panel4: TPanel;
    StaticText2: TStaticText;
    Label2: TLabel;
    QrSoma: TmySQLQuery;
    QrSomaValor: TFloatField;
    DBEdit1: TDBEdit;
    DsSoma: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbPrevBABAfterInsert(DataSet: TDataSet);
    procedure TbPrevBABSitCobrValidate(Sender: TField);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbPrevBABAfterRefresh(DataSet: TDataSet);
    procedure TbPrevBABAfterOpen(DataSet: TDataSet);
    procedure TbPrevBABAfterPost(DataSet: TDataSet);
    procedure TbPrevBABAfterDelete(DataSet: TDataSet);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
  private
    { Private declarations }
    procedure SetaTodos(Marca: Boolean);
    procedure ReopenSoma;
  public
    { Public declarations }
    FNomeCliente: String;
    FCodiCliente: Integer;
  end;

  var
  FmPrevBaB: TFmPrevBaB;

implementation

uses Module, Principal, Periodo, UMySQLModule, UnInternalConsts, MyVCLSkin,
  CondGer, ModuleGeral, ModuleCond, UnMyObjects;

{$R *.DFM}

procedure TFmPrevBaB.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrevBaB.FormActivate(Sender: TObject);
begin
  StaticText2.Caption := FNomeCliente;
  MyObjects.CorIniComponente;
end;

procedure TFmPrevBaB.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrevBaB.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrPrevBaC, DMod.MyDB);
  UMyMod.AbreTable(TbPrevBAB, DModG.MyPID_DB);
end;

procedure TFmPrevBaB.TbPrevBABAfterInsert(DataSet: TDataSet);
begin
  TbPrevBAB.Cancel;
end;

procedure TFmPrevBaB.TbPrevBABSitCobrValidate(Sender: TField);
begin
  if Sender.AsString = '' then Sender.AsInteger := 0;
end;

procedure TFmPrevBaB.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := 0;
  //
  QrConta.Close;
  QrConta.Database := DModG.MyPID_DB;
  QrConta.SQL.Clear;
  QrConta.SQL.Add('SELECT COUNT(Adiciona) Zeros');
  QrConta.SQL.Add('FROM prevbab');
  QrConta.SQL.Add('WHERE Adiciona = 1');
  QrConta.Open;
  //
  if QrContaZeros.Value = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi selecionado!');
    Exit;
  end;
  //
  QrConta.Close;
  QrConta.Database := DModG.MyPID_DB;
  QrConta.SQL.Clear;
  QrConta.SQL.Add('SELECT COUNT(Valor) Zeros');
  QrConta.SQL.Add('FROM prevbab');
  QrConta.SQL.Add('WHERE Valor < 0.01 AND Adiciona=1');
  QrConta.Open;
  if QrContaZeros.Value > 0 then
  begin
    if Geral.MB_Pergunta('Existem ' + Geral.FF0(QrContaZeros.Value) +
      ' itens selecionados sem valor informado. Deseja continuar assim mesmo?') <> ID_YES
    then
      Exit;
  end;
  TbPrevBAB.First;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ' + DmCond.FTabPriA + ' SET ');
  Dmod.QrUpd.SQL.Add('Conta=:P0, Valor=:P1, Texto=:P2, ');
  Dmod.QrUpd.SQL.Add('PrevBaC=:P3, PrevBaI=:P4, ');
  Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
  //
  while not TbPrevBAB.Eof do
  begin
    if TbPrevBABAdiciona.Value = 1 then
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        DmCond.FTabPriA, TAB_PRI, 'Controle');
      //
      Dmod.QrUpd.Params[00].AsInteger := TbPrevBABConta.Value;
      Dmod.QrUpd.Params[01].AsFloat   := TbPrevBABValor.Value;
      Dmod.QrUpd.Params[02].AsString  := TbPrevBABTexto.Value;
      Dmod.QrUpd.Params[03].AsInteger := TbPrevBABPrevBaC.Value;
      Dmod.QrUpd.Params[04].AsInteger := TbPrevBABPrevBaI.Value;
      //
      Dmod.QrUpd.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpd.Params[06].AsInteger := VAR_USUARIO;
      Dmod.QrUpd.Params[07].AsInteger := FmCondGer.QrPrevCodigo.Value;
      Dmod.QrUpd.Params[08].AsInteger := Controle;
      //
      Dmod.QrUpd.ExecSQL;
    end;
    //
    TbPrevBAB.Next;
  end;
  FmCondGer.ReopenPRI(Controle);
  Close;
end;

procedure TFmPrevBaB.BitBtn1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a retirada do item "' +
    TbPrevBABNOMECONTABASE.Value + '" destes itens?') = ID_YES then
  begin
    TbPrevBAB.Delete;
  end;
end;

procedure TFmPrevBaB.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Cor: TColor;
  OldAlign: Integer;
begin
  if (Column.FieldName = 'Valor') then
  begin
    if      TbPrevBABValor.Value = 0 then Cor := clRed
    else if TbPrevBABLast6.Value > 0 then
    begin
      if TbPrevBABValor.Value > TbPrevBABLast6.Value then
        Cor := clFuchsia
      else
        Cor := clBlue;
    end
    else Cor := clGreen;
    with DBGrid1.Canvas do
    begin
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;

      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      FillRect(Rect);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);

      //FillRect(Rect);
      //TextOut(Rect.Right-2, Rect.Top+2, Column.Field.DisplayText);
    end;
  end;

  //

  if Column.FieldName = 'Adiciona' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbPrevBABAdiciona.Value);
end;

procedure TFmPrevBaB.BitBtn2Click(Sender: TObject);
begin
  SetaTodos(True);
end;

procedure TFmPrevBaB.BitBtn3Click(Sender: TObject);
begin
  SetaTodos(False);
end;

procedure TFmPrevBaB.SetaTodos(Marca: Boolean);
var
  Codigo: Integer;
begin
  Codigo := TbPrevBABPrevBaI.Value;
  TbPrevBAB.First;
  while not TbPrevBAB.Eof do
  begin
    TbPrevBAB.Edit;
    TbPrevBABAdiciona.Value := MLAGeral.BoolToInt(Marca);
    TbPrevBAB.Post;
    //
    TbPrevBAB.Next;
  end;
  TbPrevBAB.Locate('PrevBaI', Codigo, []);
end;

procedure TFmPrevBaB.DBGrid1CellClick(Column: TColumn);
begin
  if (DBGrid1.SelectedField.Name = 'TbPrevBABAdiciona') then
  begin
    TbPrevBAB.Edit;
    if TbPrevBABAdiciona.Value = 0 then
      TbPrevBABAdiciona.Value := 1
    else
      TbPrevBABAdiciona.Value := 0;
    TbPrevBAB.Post;
  end;
end;

procedure TFmPrevBaB.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
  begin
    TbPrevBAB.Edit;
    TbPrevBABValor.Value := TbPrevBABLast1.Value;
    TbPrevBAB.Post;
  end else
  if Key = VK_F6 then
  begin
    TbPrevBAB.Edit;
    TbPrevBABValor.Value := TbPrevBABLast6.Value;
    TbPrevBAB.Post;
  end;
end;

procedure TFmPrevBaB.TbPrevBABAfterRefresh(DataSet: TDataSet);
begin
  ReopenSoma;
end;

procedure TFmPrevBaB.ReopenSoma;
begin
  UMyMod.AbreQuery(QrSoma, DModG.MyPID_DB);
end;

procedure TFmPrevBaB.TbPrevBABAfterOpen(DataSet: TDataSet);
begin
  ReopenSoma;
end;

procedure TFmPrevBaB.TbPrevBABAfterPost(DataSet: TDataSet);
begin
  ReopenSoma;
end;

procedure TFmPrevBaB.TbPrevBABAfterDelete(DataSet: TDataSet);
begin
  ReopenSoma;
end;

procedure TFmPrevBaB.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = 'Valor' then
    DBGrid1.Options := DBGrid1.Options + [dgEditing] else
    DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmPrevBaB.DBGrid1ColExit(Sender: TObject);
begin
  DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

end.

