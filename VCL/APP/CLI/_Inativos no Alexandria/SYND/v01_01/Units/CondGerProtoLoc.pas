unit CondGerProtoLoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Variants, dmkImage,
  dmkGeral, UnDmkEnums;

type
  TFmCondGerProtoLoc = class(TForm)
    Panel1: TPanel;
    CBLote: TDBLookupComboBox;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure CBLoteClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FLote: Variant;
  end;

  var
  FmCondGerProtoLoc: TFmCondGerProtoLoc;

implementation

uses CondGerProto, UnMyObjects;

{$R *.DFM}

procedure TFmCondGerProtoLoc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FLote := NULL;
end;

procedure TFmCondGerProtoLoc.BtSaidaClick(Sender: TObject);
begin
  FLote := NULL;
  Close;
end;

procedure TFmCondGerProtoLoc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerProtoLoc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerProtoLoc.CBLoteClick(Sender: TObject);
begin
  BtOK.Enabled := CBLote.KeyValue <> NULL;
end;

procedure TFmCondGerProtoLoc.BtOKClick(Sender: TObject);
begin
  FLote := CBLote.KeyValue;
  Close;
end;

end.
