unit CondGerArreFutA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkImage,
  UnDmkEnums, Data.DB, mySQLDbTables;

type
  TFmCondGerArreFutA = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    CkNaoArreRisco: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BtSaida: TBitBtn;
    BitBtn1: TBitBtn;
    QrLoc: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure SetaTodos(Marca: Boolean);
  public
    { Public declarations }
    FCond: Integer;
  end;

  var
  FmCondGerArreFutA: TFmCondGerArreFutA;

implementation

uses ModuleCond, Module, MyVCLSkin, CondGer, UMySQLModule, UnInternalConsts,
  UnMyObjects, UnBloquetosCond;

{$R *.DFM}

procedure TFmCondGerArreFutA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerArreFutA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerArreFutA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmCondGerArreFutA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerArreFutA.BitBtn1Click(Sender: TObject);
const
  Msg = 'Falha ao localizar arrecada��o!';
var
  Controle: Integer;
begin
  if (DmCond.QrArreFutI.State <> dsInactive) and (DmCond.QrArreFutI.RecordCount > 0) then
  begin
    Controle := DmCond.QrArreFutAControle.Value;
    //
    if DmCond.QrArreFutI.Locate('Controle', Controle, []) then
    begin
      FmCondGer.MostraFmCondGerArreFut(stUpd);
      //
      DmCond.QrArreFutA.Close;
      DmCond.QrArreFutA.Open;
    end else
      Geral.MB_Aviso(Msg);
  end;
end;

procedure TFmCondGerArreFutA.BitBtn2Click(Sender: TObject);
begin
  SetaTodos(True);
end;

procedure TFmCondGerArreFutA.BitBtn3Click(Sender: TObject);
begin
  SetaTodos(False);
end;

procedure TFmCondGerArreFutA.SetaTodos(Marca: Boolean);
var
  Controle, Status: Integer;
begin
  Screen.Cursor := crHourGlass;
  Controle := DmCond.QrArreFutAControle.Value;
  Status   := MLAGeral.BoolToInt(Marca);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ArreFut SET ');
  Dmod.QrUpd.SQL.Add('Inclui=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.SQL.Add('');
  DmCond.QrArreFutA.DisableControls;
  DmCond.QrArreFutA.First;
  while not DmCond.QrArreFutA.Eof do
  begin
    Dmod.QrUpd.Params[00].AsInteger := Status;
    Dmod.QrUpd.Params[01].AsInteger := DmCond.QrArreFutAControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    DmCond.QrArreFutA.Next;
  end;
  DmCond.QrArreFutA.EnableControls;
  DmCond.QrArreFutA.Close;
  UMyMod.AbreQuery(DmCond.QrArreFutA, Dmod.MyDB);
  DmCond.QrArreFutA.Locate('Controle', Controle, []);
  Screen.Cursor := crDefault;
end;

procedure TFmCondGerArreFutA.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Inclui' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, DmCond.QrArreFutAInclui.Value);
end;

procedure TFmCondGerArreFutA.DBGrid1CellClick(Column: TColumn);
var
  Controle, Status: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  Controle := DmCond.QrArreFutAControle.Value;
  if DmCond.QrArreFutAInclui.Value = 0 then
    Status := 1
  else
    Status := 0;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ArreFut SET ');
  Dmod.QrUpd.SQL.Add('Inclui=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Status;
  Dmod.QrUpd.Params[01].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  DmCond.QrArreFutA.Close;
  UMyMod.AbreQuery(DmCond.QrArreFutA, Dmod.MyDB);
  DmCond.QrArreFutA.Locate('Controle', Controle, []);
  Screen.Cursor := crDefault;
end;

procedure TFmCondGerArreFutA.BtOKClick(Sender: TObject);
var
  Controle, Contar: Integer;
  Msg, MsgVal: String;
begin
  DmCond.QrArreFutA.First;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ' + DmCond.FTabAriA + ' SET ');
  Dmod.QrUpd.SQL.Add('ComoAdd=2, Conta=:P0, Valor=:P1, Texto=:P2, ');
  Dmod.QrUpd.SQL.Add('ArreBaC=:P3, ArreBaI=:P4, Apto=:P5, Propriet=:P6, ');
  Dmod.QrUpd.SQL.Add('NaoArreRisco=:P7, CNAB_Cfg=:P8, ');
  Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
  //
  DMod.QrUpdM.SQL.Clear;
  DMod.QrUpdM.SQL.Add('UPDATE ArreFut SET ');
  DMod.QrUpdM.SQL.Add(TAB_ARI + '=:P0 ');
  DMod.QrUpdM.SQL.Add('WHERE Controle=:P1');
  DMod.QrUpdM.SQL.Add('');
  //
  Contar := 0;
  MsgVal := '';
  //
  while not DmCond.QrArreFutA.Eof do
  begin
    if DmCond.QrArreFutAInclui.Value = 1 then
    begin
      if UBloquetosCond.ValidaArrecadacaoConsumoCond(FCond,
        DmCond.QrArreFutACNAB_Cfg.Value, QrLoc, Dmod.MyDB, MsgVal) then
      begin
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          DmCond.FTabAriA, TAB_ARI, 'Controle');
        //
        Dmod.QrUpd.Params[00].AsInteger := DmCond.QrArreFutAConta.Value;
        Dmod.QrUpd.Params[01].AsFloat   := DmCond.QrArreFutAValor.Value;
        Dmod.QrUpd.Params[02].AsString  := DmCond.QrArreFutATexto.Value;
        Dmod.QrUpd.Params[03].AsInteger := 0;//DmCond.QrArreFutAArreBaC.Value;
        Dmod.QrUpd.Params[04].AsInteger := 0;//DmCond.QrArreFutAArreBaI.Value;
        Dmod.QrUpd.Params[05].AsInteger := DmCond.QrArreFutAApto.Value;
        Dmod.QrUpd.Params[06].AsInteger := DmCond.QrArreFutAPropriet.Value;
        Dmod.QrUpd.Params[07].AsInteger := MLAGeral.BTI(CkNaoArreRisco.Checked);
        Dmod.QrUpd.Params[08].AsInteger := DmCond.QrArreFutACNAB_Cfg.Value;
        //
        Dmod.QrUpd.Params[09].AsString  := Geral.FDT(Date, 1);
        Dmod.QrUpd.Params[10].AsInteger := VAR_USUARIO;
        Dmod.QrUpd.Params[11].AsInteger := FmCondGer.QrPrevCodigo.Value;
        Dmod.QrUpd.Params[12].AsInteger := Controle;
        //
        Dmod.QrUpd.ExecSQL;
        //
        DMod.QrUpdM.Params[00].AsInteger := Controle;
        DMod.QrUpdM.Params[01].AsInteger := DmCond.QrArreFutAControle.Value;
        Dmod.QrUpdM.ExecSQL;
        //
        Contar := Contar + 1;
      end;
    end;
    DmCond.QrArreFutA.Next;
  end;
  if MsgVal <> '' then
    Geral.MB_Aviso(MsgVal);
  //
  DmCond.QrArreFutA.Close;
  UMyMod.AbreQuery(DmCond.QrArreFutA, Dmod.MyDB);
  //
  DmCond.QrArreFutI.Close;
  UMyMod.AbreQuery(DmCond.QrArreFutI, Dmod.MyDB);
  //
  case Contar of
    0: Msg := 'Nenhum item foi inclu�do!';
    1: Msg := 'Um item foi inclu�do!';
    else Msg := IntToStr(Contar) + ' itens foram inclu�dos!';
  end;
  Geral.MensagemBox(Msg, 'Informa��o', MB_OK+MB_ICONINFORMATION);
  //
  if DmCond.QrArreFutA.RecordCount = 0 then
    Close;
end;

end.
