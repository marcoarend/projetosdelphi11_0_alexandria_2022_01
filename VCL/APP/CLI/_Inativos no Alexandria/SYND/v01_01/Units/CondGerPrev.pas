unit CondGerPrev;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls,
  Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage,
  UnDmkEnums;

type
  TFmCondGerPrev = class(TForm)
    Panel1: TPanel;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    DsContas: TDataSource;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    LaDeb: TLabel;
    EdValor: TdmkEdit;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCondGerPrev: TFmCondGerPrev;

implementation

uses CondGer, Module, UnInternalConsts, UMySQLModule, ModuleCond, UnMyObjects;

{$R *.DFM}

procedure TFmCondGerPrev.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerPrev.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerPrev.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerPrev.EdValorExit(Sender: TObject);
begin
  EdValor.Text := Geral.TFT(EdValor.Text, 2, siPositivo);
end;

procedure TFmCondGerPrev.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if QrContasMensal.Value = 'V' then
      Mes := FmCondGer.QrPrevPERIODO_TXT.Value else Mes := '';
    if key=VK_F4 then EdDescricao.Text := QrContasNome.Value+' '+Mes;
    if key=VK_F5 then EdDescricao.Text := QrContasNome2.Value+' '+Mes;
    if key=VK_F6 then EdDescricao.Text := QrContasNome3.Value+' '+Mes;
    EdDescricao.SetFocus;
    EdDescricao.SelStart := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end;
end;

procedure TFmCondGerPrev.BtOKClick(Sender: TObject);
var
  Conta, Controle: Integer;
  Valor: Double;
  Texto: String;
begin
  Conta := Geral.IMV(EdConta.Text);
  Valor := Geral.DMV(EdValor.Text);
  Texto := Trim(EdDescricao.Text);
  if (Conta = 0) or (Valor < 0.01) or (Texto = '') then
  begin
    Geral.MensagemBox('A conta, a descri��o e o valor devem ser informados!',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;

  //

  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add('INSERT INTO ' + DmCond.FTabPriA + ' SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE ' + DmCond.FTabPriA + ' SET');
  Dmod.QrUpd.SQL.Add('Conta=:P0, Texto=:P1, Valor=:P2,');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('');
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    DmCond.FTabPriA, TAB_PRI, 'Controle');
  end else begin
    Dmod.QrUpd.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
    Controle := FmCondGer.QrPRIControle.Value;
  end;
  Dmod.QrUpd.Params[00].AsInteger := Conta;
  Dmod.QrUpd.Params[01].AsString  := Texto;
  Dmod.QrUpd.Params[02].AsFloat   := Valor;
  //
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[04].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[05].AsInteger := FmCondGer.QrPrevCodigo.Value;
  Dmod.QrUpd.Params[06].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  FmCondGer.CalculaTotalPRIEReabrePrevEPRI;
  if CkContinuar.Checked then
  begin
    EdConta.Text := '';
    CBConta.KeyValue := NULL;
    EdValor. Text := '';
    EdDescricao.Text := '';
    //
    EdConta.SetFocus;
    ImgTipo.SQLType := stIns;
  end else Close;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmCondGerPrev.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrContas.Open;
end;

end.
