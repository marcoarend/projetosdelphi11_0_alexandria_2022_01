unit Protocolo;

interface

uses
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, UnMsgInt, mySQLDbTables,
  DB,(* DbTables,*) DBGrids, mySQLExceptions, dmkGeral, UnInternalConsts, UnDmkEnums;

type
  TComoConf = (ptkTodos=-2, ptkOutros=-1, ptkManual=1, ptkEmail=2, ptkJaPagou=3, ptkWebLink=4, ptkWebAuto=5);
  TProtFinalidade = (ptkBoleto, ptkNFSeAut, ptkNFSeCan);
  TUnProtocolo = class(TObject)
  private
    {private declaration}
    function  LocalizaLote(ProtocoPak: Integer; QueryLoc: TmySQLQuery;
              Database: TmySQLDatabase): Integer;
    procedure GeraProtocolo_NFSeProto(RecipEmeio: String;
              QuerySource: TmySQLQuery; Database: TmySQLDatabase;
              ProtocoPak, EntCliInt, TipoProt: Integer);
  public
    {public declaration}
    function  IntOfComoConf(ComoConf: TComoConf): Integer;
    function  NomeDoTipoDeProtocolo(Tipo: Integer): String;
    //
    procedure ProtocolosCD_Lct(Quais: TSelType; DBGLct: TDBGrid;
                DataModule: TDataModule; CliInt: Integer; FormOrigem: String);
    procedure ProtocolosCD_Doc(Quais: TSelType; DBGLct: TDBGrid;
                DataModule: TDataModule; CliInt: Integer; FormOrigem: String);
    function  ObtemListaProEnPrFinalidade(): TStringList;
    function  RemoveProEnPrItsDuplicados(DataBase: TmySQLDatabase;
              TiposProt: String; Finalidade: TProtFinalidade; CliInt, Entidade,
              ProEnPr: Integer): Boolean;
    function  GeraProtocoloNFSe(Quais: TSelType; QueryIts: TmySQLQuery;
              Database: TmySQLDatabase; Grade: TDBGrid;
              Progress: TProgressBar; EntCliInt: Integer): Boolean;
    function  GeraLoteProtocolo(Database: TmySQLDatabase; Protocolo,
              CNAB_Cfg, TipoProt: Integer): Integer;
    procedure ExcluiLoteProtocoloNFSe(QueryLoc, QueryUpd: TmySQLQuery;
              Database: TmySQLDatabase; CliInt, Ambiente, EntiMail, ProtocoPak,
              RpsIDNumero: Integer; RpsIDSerie: String);
    function  Proto_Email_CriaProtocoloEnvio(DataBase: TmySQLDatabase; Cliente,
              Protocolo: Integer; Email: String): Boolean;
    procedure Proto_Email_GeraLinksConfirmacaoReceb(const DataBase: TmySQLDatabase;
              const Web_MyURL, Email: String; const Cliente, Protocolo,
              DMKID_APP: Integer; var ParamLink, ParamTopo: String);
    function  Proto_Email_AtualizDataE(DataBase: TmySQLDatabase;
              Protocolo: Integer; Agora: TDateTime): Boolean;
  end;

var
  UnProtocolo: TUnProtocolo;

const
  VAR_TIPO_PROTOCOLO_ND_00 = 0; // ND - N�o definido
  VAR_TIPO_PROTOCOLO_EB_01 = 1; // EB - Entrega de Bloquetos
  VAR_TIPO_PROTOCOLO_CE_02 = 2; // CE - Circula��o eletr�nica (emeio)
  VAR_TIPO_PROTOCOLO_EM_03 = 3; // EM - Entrega de material de consumo
  VAR_TIPO_PROTOCOLO_CR_04 = 4; // CR - Cobran�a registrada (banc�ria)
  VAR_TIPO_PROTOCOLO_CD_05 = 5; // CD - Circula��o de documentos

  VAR_TIPO_LINK_ID_01_GENERICO = 1; // ????
  VAR_TIPO_LINK_ID_02_FATURA_PROD_SERV = 2; // Fatura de produtos / servi�os (Parcelamento de fatura)
  VAR_TIPO_LINK_ID_03_ORDEM_DE_SERVCO1 = 3; // Ordens de servi�o (mais algum al�m do bgstrl?)

implementation

uses ModuleLct2, MyDBCheck, UCreateFin, ModuleGeral, UnMyObjects, DmkDAC_PF,
{$IfDef TEM_UH}CondGerProtoSel, {$EndIf}
UMySQLModule;

{ TUnProtocolo }

procedure TUnProtocolo.ExcluiLoteProtocoloNFSe(QueryLoc, QueryUpd: TmySQLQuery;
  Database: TmySQLDatabase; CliInt, Ambiente, EntiMail, ProtocoPak,
  RpsIDNumero: Integer; RpsIDSerie: String);

  procedure ExcluiLoteSeVazio(ProtocoPak: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
      'SELECT * ',
      'FROM protpakits ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      '']);
    if QueryLoc.RecordCount = 0 then
      UMyMod.ExcluiRegistroInt1('', 'protocopak', 'Controle', ProtocoPak, Database);
  end;

begin
  if ProtocoPak <> 0 then
  begin
    ExcluiLoteSeVazio(ProtocoPak);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Database, [
      'DELETE FROM protpakits ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      'AND ID_Cod1=' + Geral.FFI(Ambiente),
      'AND ID_Cod2="' + RpsIDSerie + '"',
      'AND ID_Cod3=' + Geral.FFI(RpsIDNumero),
      'AND ID_Cod4=' + Geral.FFI(EntiMail),
      'AND CliInt=' + Geral.FFI(CliInt),
      '']);
  end;
end;

function TUnProtocolo.GeraLoteProtocolo(Database: TmySQLDatabase; Protocolo,
  CNAB_Cfg, TipoProt: Integer): Integer;
var
  Controle: Integer;
  DataI, DataL, Mes: String;
  Agora: TDateTime;
  QueryUpd: TmySQLQuery;
begin
  QueryUpd          := TmySQLQuery.Create(TDataModule(Database.Owner));
  QueryUpd.DataBase := Database;
  Result            := 0;
  //
  if (TipoProt = 4) and (CNAB_Cfg = 0) then //CR
    Exit;
  //
  try
    Agora    := DModG.ObtemAgora();
    Controle := UMyMod.BuscaEmLivreY_Def('protocopak', 'Controle', stIns, 0);
    DataI    := Geral.FDT(Agora, 01);
    DataL    := Geral.FDT(Agora, 01);
    Mes      := Geral.DataToMez(Agora);
    //
    if (Controle > 0) and (Protocolo > 0 ) then
    begin
      if UMyMod.SQLInsUpd(QueryUpd, stIns, 'protocopak', False,
        ['DataI', 'DataL', 'Mez', 'Codigo', 'CNAB_Cfg'], ['Controle'],
        [DataI, DataL, Mes, Protocolo, CNAB_Cfg], [Controle], True)
      then
        Result := Controle;
    end;
  finally
    QueryUpd.Free;
  end;
end;

function TUnProtocolo.GeraProtocoloNFSe(Quais: TSelType; QueryIts: TmySQLQuery;
  Database: TmySQLDatabase; Grade: TDBGrid; Progress: TProgressBar;
  EntCliInt: Integer): Boolean;

  function ObtemLoteProtocolo(Protocolo, ProtocoPak, CNAN_Cfg,
    TipoProt: Integer; QueryLoc, QueryUpd: TmySQLQuery): Integer;
  var
    Agora: TDateTime;
    Lote: Integer;
    Mes: String;
  begin
    Lote := LocalizaLote(ProtocoPak, QueryLoc, Database);
    //
    if Lote = 0 then
    begin
      Agora := DModG.ObtemAgora();
      Mes   := Geral.DataToMez(Agora);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
        'SELECT Controle ',
        'FROM protocopak ',
        'WHERE Codigo=' + Geral.FF0(Protocolo),
        'AND Mez=' + Mes,
        'AND SeqArq = 0 ',
        '']);
      if QueryLoc.RecordCount > 0 then
        Lote := QueryLoc.FieldByName('Controle').AsInteger
      else
        Lote := GeraLoteProtocolo(Database, Protocolo, CNAN_Cfg, TipoProt);
    end;
    //
    Result := Lote;
  end;

var
  i, Lote, ProtocoPak, TipoProt, Protocol: Integer;
  ProtocoloCR: Boolean;
  RecipEmeio: String;
  QueryLoc, QueryUpd: TmySQLQuery;
begin
  Result := False;
  //
  if (QueryIts.State <> dsInactive) and (QueryIts.RecordCount > 0) then
  begin
    Progress.Position := 0;
    QueryLoc          := TmySQLQuery.Create(TDataModule(Database.Owner));
    QueryUpd          := TmySQLQuery.Create(TDataModule(Database.Owner));
    //
    try
      case Quais of
        istSelecionados:
        begin
          if Grade.SelectedRows.Count > 1 then
          begin
            try
              QueryIts.DisableControls;
              Grade.Enabled := False;
              Progress.Max  := Grade.SelectedRows.Count;
              //
              with Grade.DataSource.DataSet do
              begin
                for i := 0 to Grade.SelectedRows.Count-1 do
                begin
                  GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
                  //
                  Progress.Position := Progress.Position + 1;
                  Progress.Update;
                  Application.ProcessMessages;
                  //
                  Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
                  ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
                  TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
                  RecipEmeio := '';
                  Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, 0,
                                  TipoProt, QueryLoc, QueryUpd);
                  //
                  GeraProtocolo_NFSeProto(RecipEmeio, QueryIts, Database, Lote,
                    EntCliInt, TipoProt);
                end;
              end;
            finally
              Progress.Position := 0;
              Grade.Enabled     := True;
              QueryIts.EnableControls;
              //
              Result := True;
            end;
          end else
          begin
            Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
            ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
            TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
            RecipEmeio := '';
            Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, 0, TipoProt,
                            QueryLoc, QueryUpd);
            //
            GeraProtocolo_NFSeProto(RecipEmeio, QueryIts, Database, Lote,
              EntCliInt, TipoProt);
            //
            Result := True;
          end;
        end;
        istTodos:
        begin
          try
            QueryIts.DisableControls;
            Grade.Enabled := False;
            Progress.Max  := QueryIts.RecordCount;
            //
            QueryIts.First;
            //
            while not QueryIts.Eof do
            begin
              Progress.Position := Progress.Position + 1;
              Progress.Update;
              Application.ProcessMessages;
              //
              Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
              ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
              TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
              RecipEmeio := '';
              Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, 0,
                              TipoProt, QueryLoc, QueryUpd);
              //
              GeraProtocolo_NFSeProto(RecipEmeio, QueryIts, Database, Lote,
                EntCliInt, TipoProt);
              //
              QueryIts.Next;
            end;
          finally
            Progress.Position := 0;
            Grade.Enabled     := True;
            QueryIts.EnableControls;
            //
            Result := True;
          end;
        end;
        else
        begin
          Geral.MB_Aviso('Modo se sele��o n�o implementado!');
          Result := False;
          Exit;
        end;
      end;
    finally
      QueryLoc.Free;
      QueryUpd.Free;
    end;
  end else
    Result := True;
end;

procedure TUnProtocolo.GeraProtocolo_NFSeProto(RecipEmeio: String;
  QuerySource: TmySQLQuery; Database: TmySQLDatabase; ProtocoPak, EntCliInt,
  TipoProt: Integer);
var
  Protocolo, Entidade, Conta, Emeio_ID, Retorna, ProtPakIts, Ambiente,
  RpsIDNumero: Integer;
  Vencto: TDateTime;
  NfsNumero: Largeint;
  Valor: Double;
  LimiteSai, LimiteRem, LimiteRet, RpsIDSerie: String;
  QueryLoc, QueryUpd: TmySQLQuery;
begin
  Entidade  := 0;
  NfsNumero := 0;
  Vencto    := 0;
  Valor     := 0;
  //
  QueryLoc := TmySQLQuery.Create(TDataModule(Database.Owner));
  QueryUpd := TmySQLQuery.Create(TDataModule(Database.Owner));
  //
  QueryLoc.DataBase := Database;
  QueryUpd.DataBase := Database;
  //
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
      'SELECT cfg.MultaPerc, cfg.JurosPerc, pak.DataI, ',
      'pak.DataL, pak.DataF, pro.Def_Retorn ',
      'FROM protocopak pak ',
      'LEFT JOIN protocolos pro ON pro.Codigo = pak.Codigo ',
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pak.CNAB_Cfg ',
      'WHERE pak.Controle=' + Geral.FF0(ProtocoPak),
      '']);
    //
    Retorna     := QueryLoc.FieldByName('Def_Retorn').AsInteger;
    LimiteSai   := Geral.FDT(QueryLoc.FieldByName('DataI').AsDateTime, 1);
    LimiteRem   := Geral.FDT(QueryLoc.FieldByName('DataL').AsDateTime, 1);
    LimiteRet   := Geral.FDT(QueryLoc.FieldByName('DataF').AsDateTime, 1);
    Protocolo   := QuerySource.FieldByName('PROTOCOD').AsInteger;
    Entidade    := QuerySource.FieldByName('Entidade').AsInteger;
    NfsNumero   := QuerySource.FieldByName('NfsNumero').AsLargeInt;
    Vencto      := QuerySource.FieldByName('RpsDataEmissao').AsDateTime;
    Valor       := QuerySource.FieldByName('ValorServicos').AsFloat;
    ProtPakIts  := QuerySource.FieldByName('PROTOCOLO').AsInteger;
    EMeio_ID    := Trunc(QuerySource.FieldByName('EMeio_ID').AsFloat);
    Ambiente    := QuerySource.FieldByName('Ambiente').AsInteger;
    RpsIDSerie  := QuerySource.FieldByName('RpsIDSerie').AsString;
    RpsIDNumero := QuerySource.FieldByName('RpsIDNumero').AsInteger;
    //
    if Protocolo = 0 then
      Exit;
    //
    if ProtPakIts = 0 then
    begin
      Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
      //
      UMyMod.SQLInsUpd(QueryUpd, stIns, 'protpakits', False,
        [
          'Codigo', 'Controle', 'Link_ID', 'CliInt',
          'Cliente', 'Fornece', 'Periodo', 'Lancto', 'Docum',
          'Depto', 'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
          'Retorna', 'Vencto',
          'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
          'LimiteSai', 'LimiteRem', 'LimiteRet'
        ], ['Conta'], [
          Protocolo, ProtocoPak, VAR_TIPO_LINK_ID_01_GENERICO, EntCliInt,
          Entidade, 0, 0, 0, NfsNumero,
          0, Ambiente, RpsIDSerie, RpsIDNumero, EMeio_ID,
          Retorna, Geral.FDT(Vencto, 1),
          Valor, 0, 0, 0,
          LimiteSai, LimiteRem, LimiteRet
        ], [Conta], True);
    end;
  finally
    QueryLoc.Free;
    QueryUpd.Free;
  end;
end;

function TUnProtocolo.IntOfComoConf(ComoConf: TComoConf): Integer;
begin
  case ComoConf of
    ptkManual: //Manual(For�ado)
      Result := 1;
    ptkEmail: //E-mail
      Result := 2;
    ptkJaPagou: //J� pagou
      Result := 3;
    ptkWebLink: //Web Link
      Result := 4;
    ptkWebAuto: //Web Auto (Cabe�alho do e-mail)
      Result := 5;
  end;
end;

function TUnProtocolo.LocalizaLote(ProtocoPak: Integer; QueryLoc: TmySQLQuery;
  Database: TmySQLDatabase): Integer;
begin
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
      'SELECT Controle, SeqArq ',
      'FROM protocopak ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      '']);
    if (QueryLoc.RecordCount > 0) and (QueryLoc.FieldByName('SeqArq').AsInteger = 0) then
      Result := QueryLoc.FieldByName('Controle').AsInteger
    else
      Result := 0;
  except
    Result := -1;
  end;
end;

function TUnProtocolo.NomeDoTipoDeProtocolo(Tipo: Integer): String;
begin
  case Tipo of
    VAR_TIPO_PROTOCOLO_ND_00: Result := 'ND - N�o definido';
    VAR_TIPO_PROTOCOLO_EB_01: Result := 'EB - Entrega de Bloquetos';
    VAR_TIPO_PROTOCOLO_CE_02: Result := 'CE - Circula��o eletr�nica (e-mail)';
    VAR_TIPO_PROTOCOLO_EM_03: Result := 'EM - Entrega de material de consumo';
    VAR_TIPO_PROTOCOLO_CR_04: Result := 'CR - Cobran�a registrada (banc�ria)';
    VAR_TIPO_PROTOCOLO_CD_05: Result := 'CD - Circula��o de documentos';
    else Result := '? ? ? ? ?';
  end;
end;

function TUnProtocolo.ObtemListaProEnPrFinalidade: TStringList;
var
  Finalidade: TStringList;
begin
  Finalidade := TStringList.Create;
  try
    Finalidade.Add('Envio de boletos');
    Finalidade.Add('Envio de NFS-es');
  finally
    Result := Finalidade;
  end;
end;

procedure TUnProtocolo.ProtocolosCD_Doc(Quais: TSelType; DBGLct: TDBGrid;
  DataModule: TDataModule; CliInt: Integer; FormOrigem: String);
var
  QrSource: TmySQLQuery;
  I: Integer;
  LctProto: String;

  procedure IncluiAtual();
  var
    Data, SerieCH, Vencimento: String;
    Tipo, Carteira, Sub, CliInt, Cliente, Fornece: Integer;
    Controle, Documento, Credito, Debito: Double;
  begin
    Data       := Geral.FDT(QrSource.FieldByName('Data').AsDateTime, 1);
    Tipo       := QrSource.FieldByName('Tipo'      ).AsInteger;
    Carteira   := QrSource.FieldByName('Carteira'  ).AsInteger;
    Controle   := QrSource.FieldByName('Controle'  ).AsInteger;
    Sub        := QrSource.FieldByName('Sub'       ).AsInteger;
    CliInt     := QrSource.FieldByName('CliInt'    ).AsInteger;
    Cliente    := QrSource.FieldByName('Cliente'   ).AsInteger;
    Fornece    := QrSource.FieldByName('Fornecedor').AsInteger;
    Documento  := QrSource.FieldByName('Documento' ).AsFloat;
    Credito    := QrSource.FieldByName('Credito'   ).AsFloat;
    Debito     := QrSource.FieldByName('Debito'    ).AsFloat;
    SerieCH    := QrSource.FieldByName('SerieCH'   ).AsString;
    Vencimento := Geral.FDT(QrSource.FieldByName('Vencimento').AsDateTime, 1);

    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, LctProto, False, [
      'CliInt', 'Cliente', 'Fornecedor',
      'Documento', 'Credito', 'Debito',
      'SerieCH', 'Vencimento'], [
      'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
      CliInt, Cliente, Fornece,
      Documento, Credito, Debito,
      SerieCH, Vencimento], [
      Data, Tipo, Carteira, Controle, Sub], False);
    //
  end;

var
  Carteira, Controle: Integer;
begin
{$IfDef TEM_UH}
  LctProto := UCriarFin.RecriaTempTableNovo(ntrtt_LctProto, DmodG.QrUpdPID1, False);
  if MyObjects.FIC(DBGLct = nil, nil, 'DBGLct indefinido em UnProtocolo!') then
    Exit;
  QrSource := TmySQLQuery(DBGLct.DataSource.DataSet);
  if MyObjects.FIC(QrSource = nil, nil, 'DataSet indefinido em UnProtocolo!') then
    Exit;
  //
  if Quais = istTodos then
  begin
    if Geral.MensagemBox('Gera protocolo para todos itens', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      //
      QrSource.First;
      while not QrSource.Eof do
      begin
        IncluiAtual();
        QrSource.Next;
      end;
      //
    end;
  end else if Quais = istSelecionados then
  begin
    if DBGLct.SelectedRows.Count > 1 then
    begin
      with DBGLct.DataSource.DataSet do
      for I := 0 to DBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGLct.SelectedRows.Items[I]));
        IncluiAtual();
      end;
    end else IncluiAtual();
  end else if Quais = istAtual then IncluiAtual()
  else
  begin
    Geral.MensagemBox('SelType indefinido em UnProtocolo!',
    'ERRO', MB_OK+MB_ICONERROR);
    //
    Exit;
  end;
  //
  DmLct2.QrLcP.Close;
  DmLct2.QrLcP.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(DmLct2.QrLcP, DModG.MyPID_DB, [
  'SELECT Data, Tipo, Carteira, Controle, ',
  'Sub, CliInt, Cliente, Fornecedor, ',
  'Documento, SUM(Credito) Credito, SUM(Debito) ',
  'Debito, SerieCH, Vencimento, COUNT(Ativo) ITENS ',
  'FROM ' + LctProto,
  'GROUP BY Data, Tipo, Carteira, CliInt, ',
  'SerieCH, Documento ',
  '']);
  //
  if DmLct2.QrCrt.State = dsInactive then Carteira := 0
  else Carteira := DmLct2.QrCrtCodigo.Value;
  //
  if DBCheck.CriaFm(TFmCondGerProtoSel, FmCondGerProtoSel, afmoNegarComAviso) then
  begin
    {
    FmCondGerProtoSel.QrProtocolos.Close;
    FmCondGerProtoSel.QrProtocolos.Params[0].AsInteger := VAR_TIPO_PROTOCOLO_CD_05; // Circula��o de documentos
    FmCondGerProtoSel.QrProtocolos.Open;
    }
    FmCondGerProtoSel.ReopenProtocolos(VAR_TIPO_PROTOCOLO_CD_05, 0, CliInt);
    //
    //FmCondGerProtoSel.FQuais      := Quais;
    // Muda para todos pois foi criada uma tabela para o(s) selecionado(s)
    FmCondGerProtoSel.FQuais      := istTodos;
    FmCondGerProtoSel.FNomeQrSrc  := 'DmLct2.QrLcP';
    FmCondGerProtoSel.FQrSource   := DmLct2.QrLcP;
    FmCondGerProtoSel.FQrLci      := DmLct2.QrLcI;
    FmCondGerProtoSel.FDBGrid     := DBGLct;
    FmCondGerProtoSel.FTabLctA    := DmLct2.FTabLctA;
    FmCondGerProtoSel.FLctProto   := LctProto;
    FmCondGerProtoSel.FAskTodos   := False;
    FmCondGerProtoSel.FFormOrigem := FormOrigem;
    //
    FmCondGerProtoSel.ShowModal;
    FmCondGerProtoSel.Destroy;
    //
    Controle := DmLct2.QrLctControle.Value;
    //
    DmLct2.ReabreCarteiras(Carteira, DmLct2.QrCrt, DmLct2.QrCrtSum,
      'TFmCondGer.ProtocolosCD_Doc()');
    DmLct2.QrLct.Locate('Controle', Controle, []);
  end;
{$EndIf}
end;

procedure TUnProtocolo.ProtocolosCD_Lct(Quais: TSelType; DBGLct: TDBGrid;
  DataModule: TDataModule; CliInt: Integer; FormOrigem: String);
var
  Controle: Integer;
  Carteira: Integer;
begin
{$IfDef TEM_UH}
  if DmLct2.QrCrt.State = dsInactive then Carteira := 0
  else Carteira := DmLct2.QrCrtCodigo.Value;
  //
  if DBCheck.CriaFm(TFmCondGerProtoSel, FmCondGerProtoSel, afmoNegarComAviso) then
  begin
    {
    FmCondGerProtoSel.QrProtocolos.Close;
    FmCondGerProtoSel.QrProtocolos.Params[0].AsInteger := VAR_TIPO_PROTOCOLO_CD_05; // Circula��o de documentos
    FmCondGerProtoSel.QrProtocolos.Open;
    }
    FmCondGerProtoSel.ReopenProtocolos(VAR_TIPO_PROTOCOLO_CD_05, 0, CliInt);    
    //
    FmCondGerProtoSel.FQuais      := Quais;
    FmCondGerProtoSel.FNomeQrSrc  := 'DmLct2.QrLct';
    FmCondGerProtoSel.FQrSource   := DmLct2.QrLct;
    FmCondGerProtoSel.FDBGrid     := TDBGrid(DBGLct);
    FmCondGerProtoSel.FTabLctA    := DmLct2.FTabLctA;
    FmCondGerProtoSel.FFormOrigem := FormOrigem;
    //
    FmCondGerProtoSel.ShowModal;
    FmCondGerProtoSel.Destroy;
    //
    Controle := DmLct2.QrLctControle.Value;
    //
    DmLct2.ReabreCarteiras(Carteira, DmLct2.QrCrt, DmLct2.QrCrtSum,
      'TFmCondGer.ProtocolosCD_Lct()');
    DmLct2.QrLct.Locate('Controle', Controle, []);
  end;
{$EndIf}
end;

function TUnProtocolo.Proto_Email_AtualizDataE(DataBase: TmySQLDatabase;
  Protocolo: Integer; Agora: TDateTime): Boolean;
{$IfDef TEM_DBWEB}
var
  Query: TmySQLQuery;
begin
  Result := False;
  Query  := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    if UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
      'UPDATE protpakits SET AlterWeb=1, ',
      'DataE="' + Geral.FDT(Agora, 1) + '"',
      'WHERE Conta=' + Geral.FF0(Protocolo),
      ''])
    then
      Result := True;
  finally

  end;
{$Else}
  Result := True; //Sem DBWeb n�o faz nada
{$EndIf}
end;

function TUnProtocolo.Proto_Email_CriaProtocoloEnvio(DataBase: TmySQLDatabase;
  Cliente, Protocolo: Integer; Email: String): Boolean;
{$IfDef TEM_DBWEB}
var
  Query: TmySQLQuery;
begin
    Result := False;
    Query  := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
        'SELECT * ',
        'FROM wpropaits ',
        'WHERE Cliente=' + Geral.FF0(Cliente),
        'AND Protocolo=' + Geral.FF0(Protocolo),
        'AND Email="' + Email + '"',
        '']);
      if Query.RecordCount = 0 then
      begin
        //Registra no servidor que vai mandar o e-mail
        UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
          'INSERT INTO wpropaits SET ',
          'Cliente=' + Geral.FF0(Cliente) + ', ',
          'Protocolo=' + Geral.FF0(Protocolo) + ', ',
          'Email="' + Email + '"',
          '']);
      end;
      Result := True;
    finally
      Query.Free;
    end;
{$Else}
begin
    Result := True; //Sem DBWeb n�o faz nada
{$EndIf}
end;

procedure TUnProtocolo.Proto_Email_GeraLinksConfirmacaoReceb(
  const DataBase: TmySQLDatabase; const Web_MyURL, Email: String; const Cliente,
  Protocolo, DMKID_APP: Integer; var ParamLink, ParamTopo: String);
{$IfDef TEM_DBWEB}
const
  URL_Site = 'http://www.dermatek.net.br';
var
  Query: TmySQLQuery;
  ComoConfL, ComoConfT, Mail, Cli, Prot, IDWeb: String;
begin
    ComoConfL := Geral.FF0(UnProtocolo.IntOfComoConf(ptkWebLink));
    ComoConfT := Geral.FF0(UnProtocolo.IntOfComoConf(ptkWebAuto));

    if DMKID_APP = 17 then //DControl
      IDWeb := '&idweb=dmk'
    else if DMKID_APP = 4 then //Syndi2
      IDWeb := '&idweb=syndinet'
    else
      IDWeb := '';

    Query := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
        'SELECT ',
        'MD5("'+ Email +'") Email, ',
        'MD5("'+ Geral.FF0(Cliente) +'") Cliente, ',
        'MD5("'+ Geral.FF0(Protocolo) +'") Protocolo, ',
        'MD5("'+ ComoConfL +'") ComoConfL, ',
        'MD5("'+ ComoConfT +'") ComoConfT ',
        '']);

      ComoConfL := Query.FieldByName('ComoConfL').AsString;
      ComoConfT := Query.FieldByName('ComoConfT').AsString;
      Mail      := Query.FieldByName('Email').AsString;
      Cli       := Query.FieldByName('Cliente').AsString;
      Prot      := Query.FieldByName('Protocolo').AsString;

      ParamLink := Web_MyURL + '?page=recebibloq&comoconf=' + ComoConfL +
                     '&email=' + Mail + '&cliente=' + Cli + '&protocolo=' +
                     Prot;

      ParamTopo := '<img src=''' + URL_Site +
                     '/complementos/com_emailconfrecebi.php?comoconf=' +
                     ComoConfT + '&email=' + Mail + '&cliente=' + Cli +
                     '&protocolo=' + Prot + IDWeb + ''' width=''1'' height=''1''/>';
    finally
      Query.Free;
    end;
{$Else}
begin
    ParamLink := 'N�o esque�a de enviar a confirma��o de recebimento!';
    ParamTopo := '';
{$EndIf}
end;

function TUnProtocolo.RemoveProEnPrItsDuplicados(DataBase: TmySQLDatabase;
  TiposProt: String; Finalidade: TProtFinalidade; CliInt, Entidade,
  ProEnPr: Integer): Boolean;
var
  Query: TMySQLQuery;
begin
  Result := False;
  //
  if Finalidade = ptkBoleto then
  begin
    Query := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
        'DELETE proenprit its ',
        'FROM proenprit its ',
        'LEFT JOIN proenpr pep ON pep.Codigo = its.Codigo ',
        'LEFT JOIN protocolos pro ON pro.Codigo = pep.Protocolo ',
        'WHERE pro.Def_Client=' + Geral.FF0(CliInt),
        'AND its.Entidade=' + Geral.FF0(Entidade),
        'AND pro.Tipo IN ('+ TiposProt +') ',
        'AND pep.Finalidade = 0 ', //Boletos
        'AND its.Codigo <>' + Geral.FF0(ProEnPr),
        '']);
    finally
      Query.Free;
    end;
  end;
end;

end.
