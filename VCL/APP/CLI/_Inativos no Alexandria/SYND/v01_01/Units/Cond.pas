unit Cond;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, ShellAPI,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkDBLookupComboBox,
  dmkEditCB, dmkCheckBox, dmkMemo, dmkValUsu, Variants, Menus, ComCtrls, Grids,
  DBGrids, dmkRadioGroup, dmkDBGrid, TypInfo, ClipBrd, UnDmkProcFunc, dmkImage,
  DmkDAC_PF, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmCond = class(TForm)
    PainelDados: TPanel;
    DsCond: TDataSource;
    QrCond: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    DsClientes: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNome: TWideStringField;
    QrClientesCodUsu: TIntegerField;
    QrCondNCONDOM: TWideStringField;
    QrCondCodigo: TIntegerField;
    QrCondCliente: TIntegerField;
    QrCondTxtParecer: TWideMemoField;
    QrCondLk: TIntegerField;
    QrCondDataCad: TDateField;
    QrCondDataAlt: TDateField;
    QrCondUserCad: TIntegerField;
    QrCondUserAlt: TIntegerField;
    QrCondAlterWeb: TSmallintField;
    QrCondAtivo: TSmallintField;
    QrCondTELCOM: TWideStringField;
    QrCondTELCON_TXT: TWideStringField;
    PMCond: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    N1: TMenuItem;
    DsCondOutros: TDataSource;
    DsCondBloco: TDataSource;
    PMBloco: TPopupMenu;
    Inclui3: TMenuItem;
    Altera3: TMenuItem;
    Exclui3: TMenuItem;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    PageControl5: TPageControl;
    TabSheet26: TTabSheet;
    DsCondImov: TDataSource;
    PMImovel: TPopupMenu;
    Inclui4: TMenuItem;
    Altera4: TMenuItem;
    Exclui4: TMenuItem;
    N3: TMenuItem;
    Adicionarzerosesquerda1: TMenuItem;
    QrCartEmiss: TmySQLQuery;
    QrCartEmissCodigo: TIntegerField;
    QrCartEmissNome: TWideStringField;
    DsCartEmiss: TDataSource;
    QrCondDiaVencto: TIntegerField;
    Panel4: TPanel;
    DBMemo1: TDBMemo;
    QrCondSigla: TWideStringField;
    QrCondObserv: TWideMemoField;
    QrCondOutros: TmySQLQuery;
    QrCondBloco: TmySQLQuery;
    QrCondBlocoCodigo: TIntegerField;
    QrCondBlocoControle: TIntegerField;
    QrCondBlocoDescri: TWideStringField;
    QrCondBlocoOrdem: TIntegerField;
    QrCondBlocoSomaFracao: TFloatField;
    QrCondBlocoPrefixoUH: TWideStringField;
    QrCondImov: TmySQLQuery;
    QrCondImovNOMEPROP: TWideStringField;
    QrCondImovNOMECONJUGE: TWideStringField;
    QrCondImovSTATUS1: TWideStringField;
    QrCondImovNOMEEMP1: TWideStringField;
    QrCondImovNOMEEMP2: TWideStringField;
    QrCondImovNOMEEMP3: TWideStringField;
    QrCondImovIMOB: TWideStringField;
    QrCondImovCodigo: TIntegerField;
    QrCondImovControle: TIntegerField;
    QrCondImovConta: TIntegerField;
    QrCondImovPropriet: TIntegerField;
    QrCondImovConjuge: TIntegerField;
    QrCondImovAndar: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    QrCondImovQtdGaragem: TIntegerField;
    QrCondImovStatus: TIntegerField;
    QrCondImovENome1: TIntegerField;
    QrCondImovESegunda1: TSmallintField;
    QrCondImovETerca1: TSmallintField;
    QrCondImovEQuarta1: TSmallintField;
    QrCondImovEQuinta1: TSmallintField;
    QrCondImovESexta1: TSmallintField;
    QrCondImovESabado1: TSmallintField;
    QrCondImovEDomingo1: TSmallintField;
    QrCondImovEVeic1: TSmallintField;
    QrCondImovEFilho1: TSmallintField;
    QrCondImovENome2: TIntegerField;
    QrCondImovESegunda2: TSmallintField;
    QrCondImovETerca2: TSmallintField;
    QrCondImovEQuarta2: TSmallintField;
    QrCondImovEQuinta2: TSmallintField;
    QrCondImovESexta2: TSmallintField;
    QrCondImovESabado2: TSmallintField;
    QrCondImovEDomingo2: TSmallintField;
    QrCondImovEVeic2: TSmallintField;
    QrCondImovEFilho2: TSmallintField;
    QrCondImovENome3: TIntegerField;
    QrCondImovESegunda3: TSmallintField;
    QrCondImovETerca3: TSmallintField;
    QrCondImovEQuarta3: TSmallintField;
    QrCondImovEQuinta3: TSmallintField;
    QrCondImovESexta3: TSmallintField;
    QrCondImovESabado3: TSmallintField;
    QrCondImovEDomingo3: TSmallintField;
    QrCondImovEVeic3: TSmallintField;
    QrCondImovEFilho3: TSmallintField;
    QrCondImovEmNome1: TWideStringField;
    QrCondImovEmTel1: TWideStringField;
    QrCondImovEmNome2: TWideStringField;
    QrCondImovEmTel2: TWideStringField;
    QrCondImovObserv: TWideMemoField;
    QrCondImovSitImv: TIntegerField;
    QrCondImovLk: TIntegerField;
    QrCondImovDataCad: TDateField;
    QrCondImovDataAlt: TDateField;
    QrCondImovUserCad: TIntegerField;
    QrCondImovUserAlt: TIntegerField;
    QrCondImovImobiliaria: TIntegerField;
    QrCondImovContato: TWideStringField;
    QrCondImovContTel: TWideStringField;
    QrCondImovNOMESITIMV: TWideStringField;
    QrCondImovUsuario: TIntegerField;
    QrCondImovNOMEUSUARIO: TWideStringField;
    QrCondImovProtocolo: TIntegerField;
    QrCondImovNOMEPROTOCOLO: TWideStringField;
    QrCondImovMoradores: TIntegerField;
    QrCondImovFracaoIdeal: TFloatField;
    QrCondImovAtivo: TSmallintField;
    QrCondImovBloqEndTip: TSmallintField;
    QrCondImovBloqEndEnt: TSmallintField;
    QrCondImovBloqEndTip_TXT: TWideStringField;
    QrCondImovEnderLin1: TWideStringField;
    QrCondImovEnderLin2: TWideStringField;
    QrCondImovEnderNome: TWideStringField;
    QrCondImovProcurador: TIntegerField;
    QrCondImovNOMEPROCURADOR: TWideStringField;
    TabSheet1: TTabSheet;
    QrPesq: TmySQLQuery;
    QrPesqNO_BLOCO: TWideStringField;
    QrPesqUnidade: TWideStringField;
    QrPesqNO_ENT: TWideStringField;
    QrPesqControle: TIntegerField;
    QrPesqConta: TIntegerField;
    DsPesq: TDataSource;
    PageControl7: TPageControl;
    TabSheet29: TTabSheet;
    Panel41: TPanel;
    Splitter1: TSplitter;
    DBGUnidades: TDBGrid;
    Panel42: TPanel;
    DBGrid1: TDBGrid;
    Panel43: TPanel;
    Label205: TLabel;
    RGTipoEnt: TRadioGroup;
    EdParcial: TEdit;
    GradePesq: TdmkDBGrid;
    TabSheet31: TTabSheet;
    DBGrid9: TDBGrid;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    Label8: TLabel;
    Label129: TLabel;
    Label130: TLabel;
    Label131: TLabel;
    Bevel2: TBevel;
    DBEdit014: TDBEdit;
    DBEdit063: TDBEdit;
    DBEdit064: TDBEdit;
    DBEdit065: TDBEdit;
    TabSheet5: TTabSheet;
    DBGrid5: TDBGrid;
    TabSheet6: TTabSheet;
    DBGrid7: TDBGrid;
    TabSheet7: TTabSheet;
    DBGrid4: TDBGrid;
    TabSheet16: TTabSheet;
    DBGrid6: TDBGrid;
    TabSheet17: TTabSheet;
    PageControl6: TPageControl;
    TabSheet20: TTabSheet;
    Label12: TLabel;
    DBEdit015: TDBEdit;
    GroupBox18: TGroupBox;
    DBRadioGroup5: TDBRadioGroup;
    DBRadioGroup6: TDBRadioGroup;
    GroupBox27: TGroupBox;
    DBCheckBox15: TDBCheckBox;
    DBCheckBox16: TDBCheckBox;
    DBCheckBox17: TDBCheckBox;
    DBCheckBox18: TDBCheckBox;
    DBCheckBox19: TDBCheckBox;
    GroupBox28: TGroupBox;
    Label79: TLabel;
    Label80: TLabel;
    DBEdit028: TDBEdit;
    DBEdit029: TDBEdit;
    GroupBox29: TGroupBox;
    GroupBox30: TGroupBox;
    Label81: TLabel;
    Label82: TLabel;
    DBEdit030: TDBEdit;
    DBEdit031: TDBEdit;
    DBCheckBox20: TDBCheckBox;
    DBCheckBox21: TDBCheckBox;
    TabSheet21: TTabSheet;
    Label13: TLabel;
    DBEdit016: TDBEdit;
    GroupBox17: TGroupBox;
    DBRadioGroup3: TDBRadioGroup;
    DBRadioGroup4: TDBRadioGroup;
    GroupBox23: TGroupBox;
    DBCheckBox8: TDBCheckBox;
    DBCheckBox9: TDBCheckBox;
    DBCheckBox10: TDBCheckBox;
    DBCheckBox11: TDBCheckBox;
    DBCheckBox12: TDBCheckBox;
    GroupBox24: TGroupBox;
    Label75: TLabel;
    Label76: TLabel;
    DBEdit024: TDBEdit;
    DBEdit025: TDBEdit;
    GroupBox25: TGroupBox;
    GroupBox26: TGroupBox;
    Label77: TLabel;
    Label78: TLabel;
    DBEdit026: TDBEdit;
    DBEdit027: TDBEdit;
    DBCheckBox13: TDBCheckBox;
    DBCheckBox14: TDBCheckBox;
    TabSheet22: TTabSheet;
    Label18: TLabel;
    DBEdit017: TDBEdit;
    GroupBox16: TGroupBox;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    GroupBox19: TGroupBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    GroupBox20: TGroupBox;
    Label66: TLabel;
    Label67: TLabel;
    DBEdit018: TDBEdit;
    DBEdit019: TDBEdit;
    GroupBox21: TGroupBox;
    GroupBox22: TGroupBox;
    Label68: TLabel;
    Label74: TLabel;
    DBEdit020: TDBEdit;
    DBEdit023: TDBEdit;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    TabSheet18: TTabSheet;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit01: TDBEdit;
    DBEdit02: TDBEdit;
    DBEdit012: TDBEdit;
    DBEdit013: TDBEdit;
    TabSheet19: TTabSheet;
    DBMemo2: TDBMemo;
    TabSheet23: TTabSheet;
    StaticText1: TStaticText;
    Panel36: TPanel;
    Panel37: TPanel;
    BtEmeio: TBitBtn;
    DBGrid102: TDBGrid;
    Panel38: TPanel;
    TabSheet34: TTabSheet;
    DBGrid2: TDBGrid;
    QrCondEmeios: TmySQLQuery;
    QrCondEmeiosCodigo: TIntegerField;
    QrCondEmeiosControle: TIntegerField;
    QrCondEmeiosConta: TIntegerField;
    QrCondEmeiosItem: TIntegerField;
    QrCondEmeiosPronome: TWideStringField;
    QrCondEmeiosNome: TWideStringField;
    QrCondEmeiosEMeio: TWideStringField;
    DsCondEmeios: TDataSource;
    PMEmeio: TPopupMenu;
    Incluiemeio1: TMenuItem;
    Alteraemeio1: TMenuItem;
    Excluiemeio1: TMenuItem;
    MenuItem1: TMenuItem;
    Buscardocadastrodeentidade1: TMenuItem;
    TbUnidades: TmySQLTable;
    TbUnidadesUnidade: TWideStringField;
    TbUnidadesFracaoIdeal: TFloatField;
    TbUnidadesControle: TIntegerField;
    TbUnidadesConta: TIntegerField;
    TbUnidadesPropriet: TIntegerField;
    DsUnidades: TDataSource;
    TabSheet8: TTabSheet;
    QrCondModBol: TmySQLQuery;
    QrCondModBolUnidade: TWideStringField;
    QrCondModBolNOME_CONFIGBOL: TWideStringField;
    QrCondModBolApto: TIntegerField;
    QrCondModBolConfigBol: TIntegerField;
    QrCondModBolModelBloq: TSmallintField;
    QrCondModBolBalAgrMens: TSmallintField;
    QrCondModBolCompe: TSmallintField;
    QrCondModBolNOME_MODELBLOQ: TWideStringField;
    QrCondModBolCodigo: TIntegerField;
    DsCondModBol: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    PMBloquetos: TPopupMenu;
    Incluialteramodeloconfigurao1: TMenuItem;
    Excluiitemns1: TMenuItem;
    TabSheet9: TTabSheet;
    Panel7: TPanel;
    BtCtaMesExc: TBitBtn;
    BtCtaMesAlt: TBitBtn;
    BtCtaMesIns: TBitBtn;
    QrContasMes: TmySQLQuery;
    QrContasMesCodigo: TIntegerField;
    QrContasMesControle: TIntegerField;
    QrContasMesCliInt: TIntegerField;
    QrContasMesDescricao: TWideStringField;
    QrContasMesPeriodoIni: TIntegerField;
    QrContasMesPeriodoFim: TIntegerField;
    QrContasMesValorMin: TFloatField;
    QrContasMesValorMax: TFloatField;
    QrContasMesPERIODOINI_TXT: TWideStringField;
    QrContasMesPERIODOFIM_TXT: TWideStringField;
    QrContasMesNO_CliInt: TWideStringField;
    QrContasMesQtdeMin: TIntegerField;
    QrContasMesQtdeMax: TIntegerField;
    DsContasMes: TDataSource;
    GradeContasMes: TdmkDBGrid;
    PMContasMes: TPopupMenu;
    Incluimultiplascontas1: TMenuItem;
    Incluinicacontaeconfigura1: TMenuItem;
    TabSheet10: TTabSheet;
    QrCondAndares: TIntegerField;
    QrCondTotApt: TIntegerField;
    QrCondBanco: TIntegerField;
    QrCondAgencia: TIntegerField;
    QrCondPosto: TIntegerField;
    QrCondConta: TWideStringField;
    QrCondCodCedente: TWideStringField;
    QrCondBcoLogoPath: TWideStringField;
    QrCondCliLogoPath: TWideStringField;
    QrCondLocalPag: TWideStringField;
    QrCondEspecieDoc: TWideStringField;
    QrCondAceite: TWideStringField;
    QrCondCarteira: TWideStringField;
    QrCondEspecieVal: TWideStringField;
    QrCondInstrucao1: TWideStringField;
    QrCondInstrucao2: TWideStringField;
    QrCondInstrucao3: TWideStringField;
    QrCondInstrucao4: TWideStringField;
    QrCondInstrucao5: TWideStringField;
    QrCondInstrucao6: TWideStringField;
    QrCondInstrucao7: TWideStringField;
    QrCondInstrucao8: TWideStringField;
    QrCondCorreio: TWideStringField;
    QrCondCartEmiss: TIntegerField;
    QrCondPercJuros: TFloatField;
    QrCondPercMulta: TFloatField;
    QrCondDVAgencia: TWideStringField;
    QrCondDVConta: TWideStringField;
    QrCondAgContaCed: TWideStringField;
    QrCondModelBloq: TSmallintField;
    QrCondCedente: TIntegerField;
    QrCondSacadAvali: TIntegerField;
    QrCondMIB: TSmallintField;
    QrCondIDCobranca: TWideStringField;
    QrCondOperCodi: TWideStringField;
    QrCondMBB: TSmallintField;
    QrCondMRB: TSmallintField;
    QrCondPBB: TSmallintField;
    QrCondMSB: TSmallintField;
    QrCondPSB: TSmallintField;
    QrCondMPB: TSmallintField;
    QrCondMAB: TSmallintField;
    QrCondModalCobr: TSmallintField;
    QrCondVTCBBNITAR: TFloatField;
    QrCondPwdSite: TSmallintField;
    QrCondProLaINSSr: TFloatField;
    QrCondProLaINSSp: TFloatField;
    QrCondConfigBol: TIntegerField;
    QrCondOcultaBloq: TSmallintField;
    QrCondSomaFracao: TFloatField;
    QrCondMSP: TSmallintField;
    QrCondBalAgrMens: TSmallintField;
    QrCondCompe: TSmallintField;
    QrCondHideCompe: TSmallintField;
    QrCondFlxB_Ordem: TIntegerField;
    QrCondFlxB_Folha: TSmallintField;
    QrCondFlxB_LeiAr: TSmallintField;
    QrCondFlxB_Fecha: TSmallintField;
    QrCondFlxB_Risco: TSmallintField;
    QrCondFlxB_Print: TSmallintField;
    QrCondFlxB_Proto: TSmallintField;
    QrCondFlxB_Relat: TSmallintField;
    QrCondFlxB_EMail: TSmallintField;
    QrCondFlxB_Porta: TSmallintField;
    QrCondFlxB_Postl: TSmallintField;
    QrCondFlxM_Ordem: TIntegerField;
    QrCondFlxM_Conci: TSmallintField;
    QrCondFlxM_Docum: TSmallintField;
    QrCondFlxM_Anali: TSmallintField;
    QrCondFlxM_Contb: TSmallintField;
    QrCondFlxM_Encad: TSmallintField;
    QrCondFlxM_Entrg: TSmallintField;
    QrCondAssistente: TIntegerField;
    Panel44: TPanel;
    GroupBox31: TGroupBox;
    Panel45: TPanel;
    Label71: TLabel;
    EdFlxB_Ordem: TDBEdit;
    GroupBox39: TGroupBox;
    Label211: TLabel;
    Label212: TLabel;
    Label213: TLabel;
    Label214: TLabel;
    Label215: TLabel;
    Label210: TLabel;
    Label209: TLabel;
    Label208: TLabel;
    Label207: TLabel;
    Label206: TLabel;
    EdFlxB_Folha: TDBEdit;
    EdFlxB_Proto: TDBEdit;
    EdFlxB_Relat: TDBEdit;
    EdFlxB_EMail: TDBEdit;
    EdFlxB_Porta: TDBEdit;
    EdFlxB_Postl: TDBEdit;
    EdFlxB_Print: TDBEdit;
    EdFlxB_Risco: TDBEdit;
    EdFlxB_LeiAr: TDBEdit;
    EdFlxB_Fecha: TDBEdit;
    GroupBox38: TGroupBox;
    Panel46: TPanel;
    Label223: TLabel;
    EdFlxM_Ordem: TDBEdit;
    GroupBox40: TGroupBox;
    Label217: TLabel;
    Label218: TLabel;
    Label222: TLabel;
    Label221: TLabel;
    Label220: TLabel;
    Label219: TLabel;
    EdFlxM_Conci: TDBEdit;
    EdFlxM_Docum: TDBEdit;
    EdFlxM_Entrg: TDBEdit;
    EdFlxM_Encad: TDBEdit;
    EdFlxM_Contb: TDBEdit;
    EdFlxM_Anali: TDBEdit;
    Panel47: TPanel;
    Label216: TLabel;
    Fluxos1: TMenuItem;
    Panel33: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label121: TLabel;
    Label125: TLabel;
    Label27: TLabel;
    Label197: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit061: TDBEdit;
    DBEdit062: TDBEdit;
    DBEdTel: TDBEdit;
    DBEdit9: TDBEdit;
    PB1: TProgressBar;
    QrCondImovWebLogin: TWideStringField;
    QrCondImovWebPwd: TWideStringField;
    QrCondImovWebNivel: TSmallintField;
    QrCondImovWebLogID: TWideStringField;
    QrCondImovWebLastLog: TDateTimeField;
    QrCondImovAlterWeb: TSmallintField;
    QrCondImovModelBloq: TSmallintField;
    QrCondImovConfigBol: TIntegerField;
    QrCondImovddVctEsp: TSmallintField;
    QrAssist: TmySQLQuery;
    DsAssist: TDataSource;
    PainelEdit: TPanel;
    Panel16: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    DBEdit10: TDBEdit;
    PageControl4: TPageControl;
    TabSheet14: TTabSheet;
    Panel48: TPanel;
    LaCond: TLabel;
    SBCond: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    Label224: TLabel;
    SBAssistente: TSpeedButton;
    Label199: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    GroupBox37: TGroupBox;
    Label191: TLabel;
    Label192: TLabel;
    dmkEdProLaINSSr: TdmkEdit;
    dmkEdProLaINSSp: TdmkEdit;
    EdTAndares: TdmkEdit;
    EdTAptos: TdmkEdit;
    EdSigla: TdmkEdit;
    EdAssistente: TdmkEditCB;
    CBAssistente: TdmkDBLookupComboBox;
    TabSheet15: TTabSheet;
    MeObserv: TMemo;
    TabSheet24: TTabSheet;
    Panel31: TPanel;
    Panel34: TPanel;
    Label49: TLabel;
    Label54: TLabel;
    Label163: TLabel;
    Label134: TLabel;
    Label56: TLabel;
    Label164: TLabel;
    Label162: TLabel;
    Label57: TLabel;
    Label148: TLabel;
    Label151: TLabel;
    Label152: TLabel;
    Label185: TLabel;
    Label160: TLabel;
    Label161: TLabel;
    Label179: TLabel;
    Label180: TLabel;
    Label182: TLabel;
    Label149: TLabel;
    Label150: TLabel;
    LaCartEmiss: TLabel;
    Label189: TLabel;
    Label187: TLabel;
    Label3: TLabel;
    EdCondBanco: TdmkEdit;
    EdCondAgen: TdmkEdit;
    EdDVAgencia: TdmkEdit;
    EdCondPosto: TdmkEdit;
    EdCondConta: TdmkEdit;
    EdDVConta: TdmkEdit;
    EdCondCodCed: TdmkEdit;
    EdAgContaCed: TdmkEdit;
    EdCondLocalPg: TdmkEdit;
    EdCondEspDoc: TdmkEdit;
    EdCondCarteira: TdmkEdit;
    EdCondEspVal: TdmkEdit;
    EdOperCodi: TdmkEdit;
    EdPercMulta: TdmkEdit;
    EdPercJuros: TdmkEdit;
    dmkEdVTCBBNITAR: TdmkEdit;
    EdCedente: TdmkEditCB;
    CBCedente: TdmkDBLookupComboBox;
    EdSacadAvali: TdmkEditCB;
    CBSacadAvali: TdmkDBLookupComboBox;
    EdIDCobranca: TdmkEdit;
    EdCondBcoLogo: TdmkEdit;
    EdCondCliLogo: TdmkEdit;
    EdCartEmiss: TdmkEditCB;
    CBCartEmiss: TdmkDBLookupComboBox;
    Panel35: TPanel;
    Panel49: TPanel;
    RGAceita: TRadioGroup;
    RGCorreio: TRadioGroup;
    RGModalCobr: TRadioGroup;
    TabSheet25: TTabSheet;
    Panel8: TPanel;
    GroupBox32: TGroupBox;
    EdCondInst1: TdmkEdit;
    EdCondInst2: TdmkEdit;
    EdCondInst3: TdmkEdit;
    EdCondInst4: TdmkEdit;
    EdCondInst5: TdmkEdit;
    EdCondInst6: TdmkEdit;
    EdCondInst7: TdmkEdit;
    EdCondInst8: TdmkEdit;
    Label200: TLabel;
    LbVars: TListBox;
    OpenPictureDialog1: TOpenPictureDialog;
    Timer1: TTimer;
    Label51: TLabel;
    QrCondNO_ASSISTENTE: TWideStringField;
    DBEdit11: TDBEdit;
    Label52: TLabel;
    DBEdit12: TDBEdit;
    QrCondATIVO_TXT: TWideStringField;
    Label53: TLabel;
    DBEdAtivo: TDBEdit;
    QrCondNOMECARTEMISS: TWideStringField;
    QrCedente: TmySQLQuery;
    QrCedenteNOMEENT: TWideStringField;
    QrCedenteCodigo: TIntegerField;
    DsCedente: TDataSource;
    QrSacador: TmySQLQuery;
    DsSacador: TDataSource;
    QrSacadorNOMEENT: TWideStringField;
    QrSacadorCodigo: TIntegerField;
    QrCondBlocoIDExporta: TWideStringField;
    QrCondImovIDExporta: TWideStringField;
    TbUnidadesIDExporta: TWideStringField;
    Unico1: TMenuItem;
    Multiplos1: TMenuItem;
    Alteraumaum1: TMenuItem;
    Alteraselecionados1: TMenuItem;
    Protocolodeentregadebloqueto1: TMenuItem;
    FracaoIdeal1: TMenuItem;
    PnUmAUm: TPanel;
    CkUmAUm: TCheckBox;
    QrCondImovJuridico: TSmallintField;
    QrCondImovJuridico_TXT: TWideStringField;
    RGTipoCobranca: TRadioGroup;
    QrCondTipoCobranca: TIntegerField;
    TbUnidadesCodigo: TIntegerField;
    QrCB: TmySQLQuery;
    QrCBControle: TIntegerField;
    QrCBDescri: TWideStringField;
    TbUnidadesNOME_BLOCO: TWideStringField;
    EdCartTxt: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    QrCondCartTxt: TWideStringField;
    RGCNAB: TRadioGroup;
    QrCondCNAB: TIntegerField;
    Label10: TLabel;
    EdCtaCooper: TdmkEdit;
    QrCondCtaCooper: TWideStringField;
    Emails1: TMenuItem;
    odos1: TMenuItem;
    Conselho2: TMenuItem;
    Executarprogramapadro1: TMenuItem;
    Listaremails1: TMenuItem;
    Executarprogramapadro2: TMenuItem;
    Listaremails2: TMenuItem;
    BtPesquisa: TBitBtn;
    QrContasMesLk: TIntegerField;
    QrContasMesDataCad: TDateField;
    QrContasMesDataAlt: TDateField;
    QrContasMesUserCad: TIntegerField;
    QrContasMesUserAlt: TIntegerField;
    QrContasMesAlterWeb: TSmallintField;
    QrContasMesAtivo: TSmallintField;
    QrContasMesDiaAlert: TSmallintField;
    QrContasMesDiaVence: TSmallintField;
    QrContasMesTipMesRef: TSmallintField;
    QrCondFlxM_Web: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCond: TBitBtn;
    BtBloco: TBitBtn;
    BtImovel: TBitBtn;
    BtBloquetos: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel9: TPanel;
    BtDesiste: TBitBtn;
    Diasparaenviode1: TMenuItem;
    SMS1: TMenuItem;
    Email1: TMenuItem;
    Carta1: TMenuItem;
    QrCondImovInadCEMCad: TIntegerField;
    ConfiguraodeEnviodeMensagens1: TMenuItem;
    QrCondImovSMSCelTipo: TSmallintField;
    QrCondImovSMSCelNumr: TWideStringField;
    QrCondImovSMSCelNome: TWideStringField;
    QrCondImovSMSCelEnti: TIntegerField;
    N4: TMenuItem;
    ImportadeExcel1: TMenuItem;
    Conselho1: TMenuItem;
    N2: TMenuItem;
    ParecerdoconselhoTexto1: TMenuItem;
    Membrosdoconselho1: TMenuItem;
    BtEntidades: TBitBtn;
    QrCondImvProp: TmySQLQuery;
    QrCondImvPropNOME_PROPRIET: TWideStringField;
    TbUnidadesNOME_PROPRIET: TWideStringField;
    QrCondImvPropCodigo: TIntegerField;
    QrCfgInfl: TmySQLQuery;
    DsCfgInfl: TDataSource;
    QrCfgInflCodigo: TIntegerField;
    QrCfgInflNome: TWideStringField;
    QrCondCfgInfl: TIntegerField;
    QrCondNO_CfgInfl: TWideStringField;
    TabSheet13: TTabSheet;
    Panel10: TPanel;
    RGModelBloq: TRadioGroup;
    EdConfigBol: TdmkEditCB;
    CBConfigBol: TdmkDBLookupComboBox;
    RGCompe: TRadioGroup;
    CkHideCompe: TCheckBox;
    CkBalAgrMens: TCheckBox;
    Label11: TLabel;
    EdCfgInfl: TdmkEditCB;
    CBCfgInfl: TdmkDBLookupComboBox;
    Panel11: TPanel;
    RGMBB: TRadioGroup;
    RGMRB: TRadioGroup;
    RGPBB: TRadioGroup;
    RGMSP: TRadioGroup;
    RGMSB: TRadioGroup;
    RGPSB: TRadioGroup;
    RGMIB: TRadioGroup;
    RGMPB: TRadioGroup;
    RGMAB: TRadioGroup;
    SBConfigBol: TSpeedButton;
    SpeedButton6: TSpeedButton;
    QrConfigBol: TmySQLQuery;
    DsConfigBol: TDataSource;
    QrConfigBolCodigo: TIntegerField;
    QrConfigBolNome: TWideStringField;
    Label5: TLabel;
    QrCondOutrosCodigo: TIntegerField;
    QrCondOutrosControle: TIntegerField;
    QrCondOutrosNome: TWideStringField;
    QrCondOutrosCargo: TIntegerField;
    QrCondOutrosAssina: TSmallintField;
    QrCondOutrosOrdemLista: TIntegerField;
    QrCondOutrosObserv: TWideStringField;
    QrCondOutrosNOME_CARGO: TWideStringField;
    QrCondOutrosNO_ASSINA: TWideStringField;
    QrCondOutrosMandatoIni: TDateField;
    QrCondOutrosMandatoFim: TDateField;
    QrCondOutrosMandatoIni_TXT: TWideStringField;
    QrCondOutrosMandatoFim_TXT: TWideStringField;
    QrCondOutrosTe1: TWideStringField;
    QrCondOutrosCel: TWideStringField;
    QrCondOutrosEmail: TWideStringField;
    QrCondOutrosEntidade: TIntegerField;
    QrCondOutrosTe1_TXT: TWideStringField;
    QrCondOutrosCel_TXT: TWideStringField;
    DBGEntiRespon: TDBGrid;
    RGDiaVencto: TRadioGroup;
    LaConfigBol: TLabel;
    CkUsaCNAB_Cfg: TCheckBox;
    QrCondUsaCNAB_Cfg: TIntegerField;
    TabSheet11: TTabSheet;
    Panel6: TPanel;
    BitBtn3: TBitBtn;
    dmkDBGrid2: TdmkDBGrid;
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    DsCNAB_Cfg: TDataSource;
    Protocolo11: TMenuItem;
    Protocolo21: TMenuItem;
    Protocolo31: TMenuItem;
    QrCondImovNOMEPROTOCOLO2: TWideStringField;
    QrCondImovNOMEPROTOCOLO3: TWideStringField;
    BtWClients: TBitBtn;
    RGAtivo: TdmkRadioGroup;
    N6: TMenuItem;
    Editaremailsparaenviodeboletos2: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCondAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCondBeforeOpen(DataSet: TDataSet);
    procedure BtCondClick(Sender: TObject);
    procedure QrCondCalcFields(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Inclui3Click(Sender: TObject);
    procedure Altera3Click(Sender: TObject);
    procedure BtBlocoClick(Sender: TObject);
    procedure QrCondAfterScroll(DataSet: TDataSet);
    procedure QrCondBeforeClose(DataSet: TDataSet);
    procedure QrCondOutrosCalcFields(DataSet: TDataSet);
    procedure QrCondImovCalcFields(DataSet: TDataSet);
    procedure QrCondBlocoAfterScroll(DataSet: TDataSet);
    procedure Altera4Click(Sender: TObject);
    procedure BtImovelClick(Sender: TObject);
    procedure PMCondPopup(Sender: TObject);
    procedure PMBlocoPopup(Sender: TObject);
    procedure PMImovelPopup(Sender: TObject);
    procedure QrCondBlocoAfterOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure Adicionarzerosesquerda1Click(Sender: TObject);
    procedure RGTipoEntClick(Sender: TObject);
    procedure EdParcialChange(Sender: TObject);
    procedure BtEmeioClick(Sender: TObject);
    procedure Incluiemeio1Click(Sender: TObject);
    procedure Alteraemeio1Click(Sender: TObject);
    procedure Excluiemeio1Click(Sender: TObject);
    procedure Buscardocadastrodeentidade1Click(Sender: TObject);
    procedure PageControl7Change(Sender: TObject);
    procedure QrCondModBolCalcFields(DataSet: TDataSet);
    procedure BtBloquetosClick(Sender: TObject);
    procedure Incluialteramodeloconfigurao1Click(Sender: TObject);
    procedure Excluiitemns1Click(Sender: TObject);
    procedure BtCtaMesInsClick(Sender: TObject);
    procedure BtCtaMesExcClick(Sender: TObject);
    procedure BtCtaMesAltClick(Sender: TObject);
    procedure Incluimultiplascontas1Click(Sender: TObject);
    procedure Incluinicacontaeconfigura1Click(Sender: TObject);
    procedure QrContasMesCalcFields(DataSet: TDataSet);
    procedure Fluxos1Click(Sender: TObject);
    procedure QrCondBlocoBeforeClose(DataSet: TDataSet);
    procedure SBAssistenteClick(Sender: TObject);
    procedure QrCondImovAfterScroll(DataSet: TDataSet);
    procedure EdCondBcoLogoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCondCliLogoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCondInst1Enter(Sender: TObject);
    procedure EdCondInst1Exit(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure LbVarsDblClick(Sender: TObject);
    procedure DBEdAtivoChange(Sender: TObject);
    procedure GradePesqDblClick(Sender: TObject);
    procedure DBGUnidadesDblClick(Sender: TObject);
    procedure Unico1Click(Sender: TObject);
    procedure Multiplos1Click(Sender: TObject);
    procedure Alteraumaum1Click(Sender: TObject);
    procedure FracaoIdeal1Click(Sender: TObject);
    procedure SBCondClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure Exclui4Click(Sender: TObject);
    procedure Executarprogramapadro1Click(Sender: TObject);
    procedure Listaremails1Click(Sender: TObject);
    procedure Executarprogramapadro2Click(Sender: TObject);
    procedure Listaremails2Click(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure SMS1Click(Sender: TObject);
    procedure Email1Click(Sender: TObject);
    procedure Carta1Click(Sender: TObject);
    procedure ConfiguraodeEnviodeMensagens1Click(Sender: TObject);
    procedure ImportadeExcel1Click(Sender: TObject);
    procedure Membrosdoconselho1Click(Sender: TObject);
    procedure ParecerdoconselhoTexto1Click(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
    procedure PMBloquetosPopup(Sender: TObject);
    procedure QrCondImovBeforeClose(DataSet: TDataSet);
    procedure SBConfigBolClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure AlteraOutMor1Click(Sender: TObject);
    procedure RGModelBloqClick(Sender: TObject);
    procedure CkUsaCNAB_CfgClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure Protocolo11Click(Sender: TObject);
    procedure Protocolo21Click(Sender: TObject);
    procedure Protocolo31Click(Sender: TObject);
    procedure BtWClientsClick(Sender: TObject);
    procedure PMEmeioPopup(Sender: TObject);
    procedure Editaremailsparaenviodeboletos2Click(Sender: TObject);
  private
    FEdCondInst: TdmkEdit;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure PesquisaEntidades();
    procedure ReopenCondModBol(Apto: Integer);
    procedure ReopenTbImoveis(Forca: Boolean);
    procedure MostraContasMes(SQLType: TSQLType);
    procedure VerificaTabelas(Codigo: Integer);
    procedure EnviarEmail(EnvMail: Boolean);
    procedure EnviarEmailConselho(EnvMail: Boolean);
    procedure MostraEntidade2(Entidade, TabActive: Integer);
    procedure MostraCondProto(ProtocoField: String);
    procedure ConfiguraUsaCNAB_Cfg(Usa: Boolean);
  public
    { Public declarations }
    FSeq: Integer;
    FCondEmeios: Integer;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenCondBloco(Controle: Integer);
    procedure ReopenCondOutros(Controle: Integer);
    procedure ReopenCondImov(Conta: Integer);
    procedure ReopenContasMes(Controle: Integer);
    procedure ReopenCondEmeios(Item: Integer);
  end;

var
  FmCond: TFmCond;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal, MyDBCheck, MyVCLSkin, Textos, CondImov,
  CondImovImp, Carteiras, ModuleGeral, CondEmeios, AptosModBol,
  CondFlx, ContasMes, ModuleCond, CondEmeiosAdd, UCreate, ContasMesSelMulCta,
  EntiRapidoCond, CondProto, Curinga, CondLocEmail, EntiLoad05, Entidade2,
  EntiCfgRel, CondBloco, ModuleBloq, UnBloquetosCond;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCond.LbVarsDblClick(Sender: TObject);
var
  Texto: String;
  Ate: Integer;
begin
  if FEdCondInst <> nil then
  begin
    Texto := LbVars.Items[LbVars.ItemIndex];
    if Texto <> '' then
    begin
      Ate := Pos(']', Texto);
      Texto := Copy(Texto, 1, Ate);
      Clipboard.AsText := Texto;
      FEdCondInst.PasteFromClipboard;
    end;
    FEdCondInst.SetFocus;
  end;
end;

procedure TFmCond.Listaremails1Click(Sender: TObject);
begin
  EnviarEmail(False);
end;

procedure TFmCond.Listaremails2Click(Sender: TObject);
begin
  EnviarEmailConselho(False);
end;

procedure TFmCond.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCond.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCondCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmCond.VerificaTabelas(Codigo: Integer);
  procedure VerificaTabela(TabNome, TabBase: String; DataBase: TmySQLDatabase);
  begin
    if not DBCheck.TabelaExiste(TabNome) then
      DBCheck.CriaTabela(DataBase, TabNome, TabBase, nil, actCreate, False,
        nil, nil, nil, nil);
  end;
var
  DataBase: String;
begin
  Dmod.QrMas.Close;
  Dmod.QrMas.Database := Dmod.MyDB;
  Dmod.QrMas.SQL.Clear;
  Dmod.QrMas.SQL.Add('SHOW TABLES FROM ' + TMeuDB);
  UnDmkDAC_PF.AbreQuery(Dmod.QrMas, Dmod.MyDB);
  //
  VerificaTabela(DModG.NomeTab(TMeuDB, ntLct, False, ttA, Codigo), LAN_CTOS, Dmod.MyDB);
  VerificaTabela(DModG.NomeTab(TMeuDB, ntLct, False, ttB, Codigo), LAN_CTOS, Dmod.MyDB);
  VerificaTabela(DModG.NomeTab(TMeuDB, ntLct, False, ttD, Codigo), LAN_CTOS, Dmod.MyDB);
  //
  VerificaTabela(DModG.NomeTab(TMeuDB, ntAri, False, ttA, Codigo), TAB_ARI, Dmod.MyDB);
  VerificaTabela(DModG.NomeTab(TMeuDB, ntAri, False, ttB, Codigo), TAB_ARI, Dmod.MyDB);
  VerificaTabela(DModG.NomeTab(TMeuDB, ntAri, False, ttD, Codigo), TAB_ARI, Dmod.MyDB);
  //
  VerificaTabela(DModG.NomeTab(TMeuDB, ntCns, False, ttA, Codigo), TAB_CNS, Dmod.MyDB);
  VerificaTabela(DModG.NomeTab(TMeuDB, ntCns, False, ttB, Codigo), TAB_CNS, Dmod.MyDB);
  VerificaTabela(DModG.NomeTab(TMeuDB, ntCns, False, ttD, Codigo), TAB_CNS, Dmod.MyDB);
  //
  VerificaTabela(DModG.NomeTab(TMeuDB, ntPri, False, ttA, Codigo), TAB_PRI, Dmod.MyDB);
  VerificaTabela(DModG.NomeTab(TMeuDB, ntPri, False, ttB, Codigo), TAB_PRI, Dmod.MyDB);
  VerificaTabela(DModG.NomeTab(TMeuDB, ntPri, False, ttD, Codigo), TAB_PRI, Dmod.MyDB);
  //
  VerificaTabela(DModG.NomeTab(TMeuDB, ntPrv, False, ttA, Codigo), TAB_PRV, Dmod.MyDB);
  VerificaTabela(DModG.NomeTab(TMeuDB, ntPrv, False, ttB, Codigo), TAB_PRV, Dmod.MyDB);
  VerificaTabela(DModG.NomeTab(TMeuDB, ntPrv, False, ttD, Codigo), TAB_PRV, Dmod.MyDB);
  //
  if FmPrincipal.FTemModuloWEB then
  begin
    DataBase := Dmod.MyDBn.DatabaseName;
    //
    if DataBase <> '' then
    begin
      Dmod.QrMas.Close;
      Dmod.QrMas.Database := Dmod.MyDBn;
      Dmod.QrMas.SQL.Clear;
      Dmod.QrMas.SQL.Add('SHOW TABLES FROM ' + DataBase);
      UnDmkDAC_PF.AbreQuery(Dmod.QrMas, Dmod.MyDBn);
      //
      VerificaTabela(DModG.NomeTab(DataBase, ntLct, False, ttA, Codigo), LAN_CTOS, Dmod.MyDBn);
      VerificaTabela(DModG.NomeTab(DataBase, ntLct, False, ttB, Codigo), LAN_CTOS, Dmod.MyDBn);
      VerificaTabela(DModG.NomeTab(DataBase, ntLct, False, ttD, Codigo), LAN_CTOS, Dmod.MyDBn);
      //
      VerificaTabela(DModG.NomeTab(DataBase, ntAri, False, ttA, Codigo), TAB_ARI, Dmod.MyDBn);
      VerificaTabela(DModG.NomeTab(DataBase, ntAri, False, ttB, Codigo), TAB_ARI, Dmod.MyDBn);
      VerificaTabela(DModG.NomeTab(DataBase, ntAri, False, ttD, Codigo), TAB_ARI, Dmod.MyDBn);
      //
      VerificaTabela(DModG.NomeTab(DataBase, ntCns, False, ttA, Codigo), TAB_CNS, Dmod.MyDBn);
      VerificaTabela(DModG.NomeTab(DataBase, ntCns, False, ttB, Codigo), TAB_CNS, Dmod.MyDBn);
      VerificaTabela(DModG.NomeTab(DataBase, ntCns, False, ttD, Codigo), TAB_CNS, Dmod.MyDBn);
      //
      VerificaTabela(DModG.NomeTab(DataBase, ntPri, False, ttA, Codigo), TAB_PRI, Dmod.MyDBn);
      VerificaTabela(DModG.NomeTab(DataBase, ntPri, False, ttB, Codigo), TAB_PRI, Dmod.MyDBn);
      VerificaTabela(DModG.NomeTab(DataBase, ntPri, False, ttD, Codigo), TAB_PRI, Dmod.MyDBn);
      //
      VerificaTabela(DModG.NomeTab(DataBase, ntPrv, False, ttA, Codigo), TAB_PRV, Dmod.MyDBn);
      VerificaTabela(DModG.NomeTab(DataBase, ntPrv, False, ttB, Codigo), TAB_PRV, Dmod.MyDBn);
      VerificaTabela(DModG.NomeTab(DataBase, ntPrv, False, ttD, Codigo), TAB_PRV, Dmod.MyDBn);
    end;
  end;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCond.DefParams;
begin
  VAR_GOTOTABELA := 'cond';
  VAR_GOTOMYSQLTABLE := QrCond;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT CASE con.Ativo ');
  VAR_SQLx.Add('  WHEN 0 THEN "Inativo" ');
  VAR_SQLx.Add('  WHEN 1 THEN "Ativo" ');
  VAR_SQLx.Add('  ELSE "Inoperante" END ATIVO_TXT, con.*, ');
  VAR_SQLx.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NCONDOM,');
  VAR_SQLx.Add('IF (ent.Tipo=0, ent.ETe1, ent.PTe1) TELCOM,');
  VAR_SQLx.Add('ass.Nome NO_ASSISTENTE, car.Nome NOMECARTEMISS, nfl.Nome NO_CfgInfl');
  VAR_SQLx.Add('FROM cond con');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = con.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades ass ON ass.Codigo = con.Assistente');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo = con.CartEmiss');
  VAR_SQLx.Add('LEFT JOIN cfginfl   nfl ON nfl.Codigo = con.CfgInfl');
  VAR_SQLx.Add('WHERE con.Codigo <> 0');
  if VAR_USUARIO >= 0 then
  begin
    VAR_SQLx.Add('AND ');
    VAR_SQLx.Add('(');
    VAR_SQLx.Add('  con.Assistente = 0');
    VAR_SQLx.Add('  OR con.Assistente=');
    VAR_SQLx.Add('  (');
    VAR_SQLx.Add('    SELECT Funcionario ');
    VAR_SQLx.Add('    FROM senhas ');
    VAR_SQLx.Add('    WHERE Numero=' + FormatFloat('0', VAR_USUARIO));
    VAR_SQLx.Add('  )');
    VAR_SQLx.Add('  OR con.Cliente IN ');
    VAR_SQLx.Add('  (');
    VAR_SQLx.Add('    SELECT Empresa ');
    VAR_SQLx.Add('    FROM senhasits ');
    VAR_SQLx.Add('    WHERE Numero=' + FormatFloat('0', VAR_USUARIO));
    VAR_SQLx.Add('  )');
    VAR_SQLx.Add(')');
  end;
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND con.Codigo=:P0');
  //
  //VAR_SQLa.Add('AND con.Nome Like :P0');
  //
end;

procedure TFmCond.EdCondBcoLogoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenPictureDialog1.Execute then EdCondBcoLogo.Text :=
      OpenPictureDialog1.FileName;
  end;
end;

procedure TFmCond.EdCondCliLogoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenPictureDialog1.Execute then EdCondCliLogo.Text :=
      OpenPictureDialog1.FileName;
  end;
end;

procedure TFmCond.EdCondInst1Enter(Sender: TObject);
begin
  Timer1.Enabled := False;
  FEdCondInst := TdmkEdit(Sender);
end;

procedure TFmCond.EdCondInst1Exit(Sender: TObject);
begin
  Timer1.Enabled := True;
end;

procedure TFmCond.Editaremailsparaenviodeboletos2Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl7.ActivePageIndex := 1;
  PageControl2.ActivePageIndex := 8;
end;

procedure TFmCond.EdParcialChange(Sender: TObject);
begin
  PesquisaEntidades();
end;

procedure TFmCond.Email1Click(Sender: TObject);
begin
  //AlteraQuantidadeDeDias('DdEnviEma', 'email');
end;

procedure TFmCond.EnviarEmail(EnvMail: Boolean);
var
  Mail: AnsiString;
begin
  //EnvMail = True  => Abre o programa padr�o de e-mails
  //EnvMail = False => Apenas abre uma lista de e-mails
  //
  PageControl1.ActivePageIndex := 0;
  PageControl7.ActivePageIndex := 1;
  PageControl2.ActivePageIndex := 8;
  //
  Mail := 'mailto:';
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT EMeio');
  Dmod.QrAux.SQL.Add('FROM condemeios');
  Dmod.QrAux.SQL.Add('WHERE Codigo=' + Geral.FF0(QrCondCodigo.Value));
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    while not Dmod.QrAux.Eof do
    begin
      Mail := Mail + Dmod.QrAux.FieldByName('EMeio').AsString + ';';
      Dmod.QrAux.Next;
    end;
    if EnvMail then
    begin
      Mail := Mail + '?subject=&body=';
      ShellExecute(GetDesktopWindow, 'open', PChar(Mail), nil, nil, sw_ShowNormal);
    end else
      Geral.SalvaTextoEmArquivoEAbre(CO_DIR_RAIZ_DMK + '\Temp.txt', Mail, True);
  end else
    Geral.MB_Aviso('Nenhum e-mail foi localizado!');
end;

procedure TFmCond.EnviarEmailConselho(EnvMail: Boolean);
var
  Mail, Body: String;
begin
  //OBS.: No ShellAPI quebra de linha = %0A. O #13#10 n�o funciona
  //
  PageControl1.ActivePageIndex := 0;
  PageControl7.ActivePageIndex := 1;
  PageControl2.ActivePageIndex := 8;
  //
  Mail := 'mailto:';
  if EnvMail then
    Body := 'Prezados Senhores, %0A';
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT cou.EMeio,');
  Dmod.QrAux.SQL.Add('IF(pro.Tipo=0, pro.RazaoSocial, pro.Nome) NOME,');
  Dmod.QrAux.SQL.Add('car.Descri CARGO');
  Dmod.QrAux.SQL.Add('FROM condoutros cou');
  Dmod.QrAux.SQL.Add('LEFT JOIN entidades pro ON pro.Codigo=cou.Nome');
  Dmod.QrAux.SQL.Add('LEFT JOIN cargos car ON car.Codigo=cou.Cargo');
  Dmod.QrAux.SQL.Add('WHERE cou.Codigo=' + Geral.FF0(QrCondCodigo.Value));
  Dmod.QrAux.SQL.Add('AND cou.EMeio <> ""');
  Dmod.QrAux.SQL.Add('ORDER BY cou.OrdemLista, NOME');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    while not Dmod.QrAux.Eof do
    begin
      Mail := Mail + Dmod.QrAux.FieldByName('EMeio').AsString + ';';
      if EnvMail then
        Body := Body + Dmod.QrAux.FieldByName('NOME').AsString + '  -  ' +
          Dmod.QrAux.FieldByName('CARGO').AsString + '%0A';
      Dmod.QrAux.Next;
    end;
    if EnvMail then
    begin
      Mail := Mail + '?subject=E-mail ao Conselho&body=' +Body;
      ShellExecute(GetDesktopWindow, 'open', pchar(Mail), nil, nil, sw_ShowNormal);
    end else
      Geral.SalvaTextoEmArquivoEAbre(CO_DIR_RAIZ_DMK + '\Temp.txt', Mail, True);
  end else
    Geral.MB_Aviso('Nenhum componente do conselho tem e-mail cadastrado!');
end;

procedure TFmCond.Exclui4Click(Sender: TObject);
var
  TabLctA, TabLctB, TabLctD, TabAriA, TabAriB, TabAriD, TabCnsA, TabCnsB,
  TabCnsD, Apto, CliI: String;
  i: Integer;
begin
  if QrCondImov.RecordCount = 0 then Exit;
  Apto := FormatFloat('0', QrCondImovConta.Value);
  CliI := FormatFloat('0', QrCondImovCodigo.Value);
  //
  TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, QrCondImovCodigo.Value);
  TabLctB := DModG.NomeTab(TMeuDB, ntLct, False, ttB, QrCondImovCodigo.Value);
  TabLctD := DModG.NomeTab(TMeuDB, ntLct, False, ttD, QrCondImovCodigo.Value);
  //
  TabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, QrCondImovCodigo.Value);
  TabAriB := DModG.NomeTab(TMeuDB, ntAri, False, ttB, QrCondImovCodigo.Value);
  TabAriD := DModG.NomeTab(TMeuDB, ntAri, False, ttD, QrCondImovCodigo.Value);
  //
  TabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, QrCondImovCodigo.Value);
  TabCnsB := DModG.NomeTab(TMeuDB, ntCns, False, ttB, QrCondImovCodigo.Value);
  TabCnsD := DModG.NomeTab(TMeuDB, ntCns, False, ttD, QrCondImovCodigo.Value);
  //
  DmCond.QrCount.Close;
  DmCond.QrCount.SQL.Clear;
  for i := 0 to 3 - 1 do
  begin
    DmCond.QrCount.SQL.Add('SELECT COUNT(*) Itens');
    case i of
      0: DmCond.QrCount.SQL.Add('FROM ' + TabLctA + '');
      1: DmCond.QrCount.SQL.Add('FROM ' + TabLctB + '');
      2: DmCond.QrCount.SQL.Add('FROM ' + TabLctD + '');
    end;
    DmCond.QrCount.SQL.Add('WHERE Depto=' + Apto);
    DmCond.QrCount.SQL.Add('AND  CliInt=' + CliI);
    if i <> 2 then
      DmCond.QrCount.SQL.Add('UNION ');
  end;
  DmCond.QrCount.SQL.Add('ORDER BY Itens DESC');
  UMyMod.AbreQuery(DmCond.QrCount, Dmod.MyDB);
  //
  if DmCond.QrCountItens.Value > 0 then
  begin
    Geral.MB_Aviso('A unidade habitacional n�o pode ser ' +
      'exclu�da pois cont�m ' + Geral.FF0(DmCond.QrCountItens.Value) +
      ' lan�amentos financeiros atrelados a ela!');
    Exit;
  end;
  //
  DmCond.QrCount.Close;
  DmCond.QrCount.SQL.Clear;
  for i := 0 to 3 - 1 do
  begin
    DmCond.QrCount.SQL.Add('SELECT COUNT(*) Itens');
    case i of
      0: DmCond.QrCount.SQL.Add('FROM ' + TabAriA + '');
      1: DmCond.QrCount.SQL.Add('FROM ' + TabAriB + '');
      2: DmCond.QrCount.SQL.Add('FROM ' + TabAriD + '');
    end;
    DmCond.QrCount.SQL.Add('WHERE Apto=' + Apto);
    if i <> 2 then
      DmCond.QrCount.SQL.Add('UNION ');
  end;
  DmCond.QrCount.SQL.Add('ORDER BY Itens DESC');
  UMyMod.AbreQuery(DmCond.QrCount, Dmod.MyDB);
  //
  if DmCond.QrCountItens.Value > 0 then
  begin
    Geral.MB_Aviso('A unidade habitacional n�o pode ser ' +
      'exclu�da pois cont�m ' + Geral.FF0(DmCond.QrCountItens.Value) +
      ' arrecada��es de boletos atrelados a ela!');
    Exit;
  end;
  //
  DmCond.QrCount.Close;
  DmCond.QrCount.SQL.Clear;
  for i := 0 to 3 - 1 do
  begin
    DmCond.QrCount.SQL.Add('SELECT COUNT(*) Itens');
    case i of
      0: DmCond.QrCount.SQL.Add('FROM ' + TabCnsA + '');
      1: DmCond.QrCount.SQL.Add('FROM ' + TabCnsB + '');
      2: DmCond.QrCount.SQL.Add('FROM ' + TabCnsD + '');
    end;
    DmCond.QrCount.SQL.Add('WHERE Apto=' + Apto);
    if i <> 2 then
      DmCond.QrCount.SQL.Add('UNION ');
  end;
  DmCond.QrCount.SQL.Add('ORDER BY Itens DESC');
  UMyMod.AbreQuery(DmCond.QrCount, Dmod.MyDB);
  //
  if DmCond.QrCountItens.Value > 0 then
  begin
    Geral.MB_Aviso('A unidade habitacional n�o pode ser ' + 'exclu�da pois cont�m '
       + Geral.FF0(DmCond.QrCountItens.Value) +
       ' itens de consumo em boletos atrelados a ela!');
    Exit;
  end;
  //
  DmCond.QrCount.Close;
  DmCond.QrCount.SQL.Clear;
  DmCond.QrCount.SQL.Add('SELECT COUNT(*) Itens');
  DmCond.QrCount.SQL.Add('FROM arrebau');
  DmCond.QrCount.SQL.Add('WHERE Apto=' + Apto);
  UMyMod.AbreQuery(DmCond.QrCount, Dmod.MyDB);
  //
  if DmCond.QrCountItens.Value > 0 then
  begin
    Geral.MB_Aviso('A unidade habitacional n�o pode ser ' +
      'exclu�da pois est� inclu�da em ' + Geral.FF0(DmCond.QrCountItens.Value) +
      ' itens base de arrecada��o!');
    Exit;
  end;
  //
  if Geral.MB_Pergunta('Confirma a exclus�o defitiva da unidade ' +
    'habitacional selecionada?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM condimov WHERE Conta=' + Apto);
    Dmod.QrUpd.ExecSQL;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM users ',
      'WHERE CodigoEsp=' + Apto,
      'AND CodCliEsp=' + CliI,
      '']);
    //
    Dmcond.AtualizaSomaFracaoApto(QrCondImovControle.Value, QrCondImovCodigo.Value);
    ReopenCondImov(0);
  end;
end;

procedure TFmCond.Excluiemeio1Click(Sender: TObject);
begin
  UMyMod.SQLDel1(Dmod.QrUpd, QrCondEmeios, 'CondEmeios', 'Item',
    QrCondEMeiosItem.Value, True, 'Confirma a exclus�o do e-mail de ID = ' +
    Geral.FF0(QrCondEmeiosItem.Value) + '?', True);
end;

procedure TFmCond.Excluiitemns1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCondModBol, TDBGrid(dmkDBGrid1),
    'condmodbol', ['Codigo', 'Apto'], ['Codigo', 'Apto'], istPergunta, '');
end;

procedure TFmCond.Executarprogramapadro1Click(Sender: TObject);
begin
  EnviarEmail(True);
end;

procedure TFmCond.Executarprogramapadro2Click(Sender: TObject);
begin
  EnviarEmailConselho(True);
end;

procedure TFmCond.Membrosdoconselho1Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  MostraEntidade2(QrCondCliente.Value, 2); //2 = Conselho
end;

procedure TFmCond.MostraCondProto(ProtocoField: String);
begin
  if DBCheck.CriaFm(TFmCondProto, FmCondProto, afmoNegarComAviso) then
  begin
    FmCondProto.FCliente      := QrCondCliente.Value;
    FmCondProto.FProtocoField := ProtocoField;
    FmCondProto.ShowModal;
    FmCondProto.Destroy;
  end;
end;

procedure TFmCond.MostraContasMes(SQLType: TSQLType);
begin
  if UMyMod.FormInsUpd_Cria(TFmContasMes, FmContasMes, afmoNegarComAviso,
    QrContasMes, SQLType) then
  begin
    FmContasMes.FControle := QrContasMesControle.Value;
    if SQLType = stIns then
    begin
      FmContasMes.EdCliInt.ValueVariant := QrCondCodigo.Value;
      FmContasMes.CBCliInt.KeyValue     := QrCondCodigo.Value;
    end;
    FmContasMes.FQuery := QrContasMes;
    FmContasMes.ShowModal;
    FmContasMes.Destroy;
  end;
end;

procedure TFmCond.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
var
  Aceita, Correio: String;
  Compe, ModelBol: Boolean;
begin
  case Mostra of
    0: //co_travado
    begin
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
    end;
    1: //condom�nio
    begin
      PainelEdita.Visible          := True;
      PainelDados.Visible          := False;
      PageControl4.ActivePageIndex := 0;
      //
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant   := 0;
        EdEntidade.ValueVariant := 0;
        CBEntidade.KeyValue     := Null;
        EdTAndares.ValueVariant := 0;
        EdTAptos.ValueVariant   := 0;
        MeObserv.Text           := '';
        //
        EdCondBanco.Text      := '';
        EdCondAgen.Text       := '';
        EdCondPosto.Text      := '';
        EdCondConta.Text      := '';
        EdCondCodCed.Text     := '';
        EdAgContaCed.Text     := '';
        EdDVAgencia.Text      := '';
        EdDVConta.Text        := '';
        EdSigla.Text          := '';
        RGDiaVencto.ItemIndex := 4;
        RGModelBloq.ItemIndex := 0;
        EdCondLocalPg.Text    := '';
        EdCondEspDoc.Text     := 'NS';
        RGAceita.ItemIndex    := 0;
        RGCorreio.ItemIndex   := 0;
        EdCondCarteira.Text   := '';
        EdCartTxt.Text        := '';
        EdCondEspVal.Text     := '';
        EdCondInst1.Text      := '';
        EdCondInst2.Text      := '';
        EdCondInst3.Text      := '';
        EdCondInst4.Text      := '';
        EdCondInst5.Text      := '';
        EdCondInst6.Text      := '';
        EdCondInst7.Text      := '';
        EdCondInst8.Text      := '';
        EdCondBcoLogo.Text    := '';
        EdCondCliLogo.Text    := '';
        EdPercJuros.Text      := '1,00';
        EdPercMulta.Text      := '2,00';
        RGCNAB.ItemIndex      := 0;
        //
        CBEntidade.Enabled    := True;
        EdEntidade.Enabled    := True;
        LaCond.Enabled        := True;
        SBCond.Enabled        := True;
        //
        QrCartEmiss.Close;
        EdCartEmiss.Text      := '';
        CBCartEmiss.KeyValue  := Null;
        EdCedente.Text        := '';
        CBCedente.KeyValue    := Null;
        EdSacadAvali.Text     := '';
        CBSacadAvali.KeyValue := Null;
        //
        EdIDCobranca.Text     := '';
        EdOperCodi.Text       := '';
        EdCtaCooper.Text      := '';
        //
        RGMBB.ItemIndex       := 1;
        RGMRB.ItemIndex       := 1;
        RGPBB.ItemIndex       := 0;
        RGMSB.ItemIndex       := 1;
        RGMSP.ItemIndex       := 1;
        RGPSB.ItemIndex       := 0;
        RGMIB.ItemIndex       := 0;
        RGMPB.ItemIndex       := 1;
        RGMAB.ItemIndex       := 1;
        RGModalCobr.ItemIndex := 0;
        //
        dmkEdProLaINSSp.ValueVariant := 11;
        dmkEdProLaINSSr.ValueVariant := 20;
        //
        dmkEdVTCBBNITAR.ValueVariant := 0;
        //
        EdConfigBol.ValueVariant     := -1;
        CBConfigBol.KeyValue         := -1;
        //
        RGCompe.ItemIndex            := -1;
        CkBalAgrMens.Checked         := False;
        CkHideCompe.Checked          := False;
        RGAtivo.ItemIndex            := 2;//Ativo
        //
        EdAssistente.ValueVariant    := 0;
        CBAssistente.KeyValue        := Null;
        //
        RGTipoCobranca.ItemIndex     := 0;
        //
        ConfiguraUsaCNAB_Cfg(True);
        //
        CkUsaCNAB_Cfg.Enabled        := False;
        CkUsaCNAB_Cfg.Checked        := True;
        //
        EdCfgInfl.ValueVariant       := 0;
        CBCfgInfl.KeyValue           := Null;
        //
        LaConfigBol.Visible := False;
        EdConfigBol.Visible := False;
        CBConfigBol.Visible := False;
        SBConfigBol.Visible := False;
        RGCompe.Visible     := False;
        //
        EdEntidade.SetFocus;
      end else begin
        EdCodigo.ValueVariant   := QrCondCodigo.Value;
        EdEntidade.ValueVariant := QrCondCliente.Value;
        CBEntidade.KeyValue     := QrCondCliente.Value;
        EdTAndares.ValueVariant := QrCondAndares.Value;
        EdTAptos.ValueVariant   := QrCondTotApt.Value;
        MeObserv.Text           := QrCondObserv.Value;
        //
        EdCondBanco.ValueVariant := QrCondBanco.Value;
        EdCondAgen.ValueVariant  := QrCondAgencia.Value;
        EdCondPosto.ValueVariant := QrCondPosto.Value;
        EdCondConta.Text         := QrCondConta.Value;
        EdCondCodCed.Text        := QrCondCodCedente.Value;
        EdAgContaCed.Text        := QrCondAgContaCed.Value;
        EdDVAgencia.Text         := QrCondDVAgencia.Value;
        EdDVConta.Text           := QrCondDVConta.Value;
        EdSigla.Text             := QrCondSigla.Value;
        RGDiaVencto.ItemIndex    := QrCondDiaVencto.Value -1;
        RGModelBloq.ItemIndex    := QrCondModelBloq.Value;
        EdCondLocalPg.Text       := QrCondLocalPag.Value;
        EdCondEspDoc.Text        := QrCondEspecieDoc.Value;
        EdCondCarteira.Text      := QrCondCarteira.Value;
        EdCartTxt.Text           := QrCondCartTxt.Value;
        EdCondEspVal.Text        := QrCondEspecieVal.Value;
        EdCondInst1.Text         := QrCondInstrucao1.Value;
        EdCondInst2.Text         := QrCondInstrucao2.Value;
        EdCondInst3.Text         := QrCondInstrucao3.Value;
        EdCondInst4.Text         := QrCondInstrucao4.Value;
        EdCondInst5.Text         := QrCondInstrucao5.Value;
        EdCondInst6.Text         := QrCondInstrucao6.Value;
        EdCondInst7.Text         := QrCondInstrucao7.Value;
        EdCondInst8.Text         := QrCondInstrucao8.Value;
        EdCondBcoLogo.Text       := QrCondBcoLogoPath.Value;
        EdCondCliLogo.Text       := QrCondCliLogoPath.Value;
        EdPercJuros.Text         := Geral.FFT(QrCondPercJuros.Value, 2, siPositivo);
        EdPercMulta.Text         := Geral.FFT(QrCondPercMulta.Value, 2, siPositivo);
        //
        Aceita:= QrCondAceite.Value;
        if Aceita = 'S' then
          RGAceita.ItemIndex := 1
        else
          RGAceita.ItemIndex := 0;
        //
        Correio := QrCondCorreio.Value;
        if Correio = 'S' then
          RGCorreio.ItemIndex := 1
        else
          RGCorreio.ItemIndex := 0;
        //
        CBEntidade.Enabled := QrCondCliente.Value = 0;
        EdEntidade.Enabled := QrCondCliente.Value = 0;
        LaCond.Enabled := QrCondCliente.Value = 0;
        SBCond.Enabled := QrCondCliente.Value = 0;
        //
        QrCartEmiss.Close;
        QrCartEmiss.Params[0].AsInteger := QrCondCliente.Value;
        UnDmkDAC_PF.AbreQuery(QrCartEmiss, Dmod.MyDB);
        //
        EdCartEmiss.ValueVariant  := QrCondCartEmiss.Value;
        CBCartEmiss.KeyValue      := QrCondCartEmiss.Value;
        EdCedente.ValueVariant    := QrCondCedente.Value;
        CBCedente.KeyValue        := QrCondCedente.Value;
        EdSacadAvali.ValueVariant := QrCondSacadAvali.Value;
        CBSacadAvali.KeyValue     := QrCondSacadAvali.Value;
        //
        EdIdCobranca.Text     := QrCondIDCobranca.Value;
        EdOperCodi.Text       := QrCondOperCodi.Value;
        EdCtaCooper.Text      := QrCondCtaCooper.Value;
        //
        RGMBB.ItemIndex       := QrCondMBB.Value;
        RGMRB.ItemIndex       := QrCondMRB.Value;
        RGPBB.ItemIndex       := QrCondPBB.Value;
        RGMSB.ItemIndex       := QrCondMSB.Value;
        RGMSP.ItemIndex       := QrCondMSP.Value;
        RGPSB.ItemIndex       := QrCondPSB.Value;
        RGMIB.ItemIndex       := QrCondMIB.Value;
        RGMPB.ItemIndex       := QrCondMPB.Value;
        RGMAB.ItemIndex       := QrCondMAB.Value;
        RGModalCobr.ItemIndex := QrCondModalCobr.Value;
        //
        MyObjects.SetaRGCNAB(RGCNAB, QrCondCNAB.Value);
        //
        dmkEdProLaINSSp.ValueVariant := QrCondProLaINSSp.Value;
        dmkEdProLaINSSr.ValueVariant := QrCondProLaINSSr.Value;
        //
        dmkEdVTCBBNITAR.ValueVariant := QrCondVTCBBNITAR.Value;
        //
        EdConfigBol.ValueVariant     := QrCondConfigBol.Value;
        CBConfigBol.KeyValue         := QrCondConfigBol.Value;
        //
        RGCompe.ItemIndex            := QrCondCompe.Value;
        CkBalAgrMens.Checked         := MLAGeral.ITB(QrCondBalAgrMens.Value);
        CkHideCompe.Checked          := MLAGeral.ITB(QrCondHideCompe.Value);
        RGAtivo.ItemIndex            := QrCondAtivo.Value + 1;
        //
        EdAssistente.ValueVariant    := QrCondAssistente.Value;
        CBAssistente.KeyValue        := QrCondAssistente.Value;
        RGTipoCobranca.ItemIndex     := QrCondTipoCobranca.Value;
        //
        CkUsaCNAB_Cfg.Enabled        := QrCondBanco.Value <> 0;
        CkUsaCNAB_Cfg.Checked        := Geral.IntToBool(QrCondUsaCNAB_Cfg.Value);
        //
        EdCfgInfl.ValueVariant       := QrCondCfgInfl.Value;
        CBCfgInfl.KeyValue           := QrCondCfgInfl.Value;
        //
        DmBloq.ConfiguraCompeConfigModelBol(RGModelBloq, Compe, ModelBol);
        //
        LaConfigBol.Visible := ModelBol;
        EdConfigBol.Visible := ModelBol;
        CBConfigBol.Visible := ModelBol;
        SBConfigBol.Visible := ModelBol;
        RGCompe.Visible     := Compe;
        //
        if CkUsaCNAB_Cfg.Checked then
        begin
          TabSheet24.TabVisible := False; //Boleto (parte 2)
          TabSheet25.TabVisible := False; //Boleto (parte 3)
        end else
        begin
          TabSheet24.TabVisible := True; //Boleto (parte 2)
          TabSheet25.TabVisible := True; //Boleto (parte 3)
        end;
        EdTAndares.SetFocus;
      end;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCond.MostraEntidade2(Entidade, TabActive: Integer);
begin
  //0 = Padr�o
  //2 = Conselho
  //
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.LocCod(Entidade, Entidade);
    FmEntidade2.PCXtraGer.ActivePageIndex := TabActive;
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
end;

procedure TFmCond.Multiplos1Click(Sender: TObject);
begin
  if QrCondBloco.RecordCount = 0 then
  begin
    Geral.MensagemBox('N�o h� bloco cadastrado para adicionar ' +
    'unidades habitacionais!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if DBCheck.CriaFm(TFmEntiRapidoCond, FmEntiRapidoCond, afmoNegarComAviso) then
  begin
    FmEntiRapidoCond.FCliente := QrCondCliente.Value;
    FmEntiRapidoCond.ShowModal;
    FmEntiRapidoCond.Destroy;
  end;
end;

procedure TFmCond.PageControl1Change(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    //
    case PageControl1.ActivePageIndex of
      2: //Conselho
      begin
        ReopenCondOutros(0);
      end;
      3: // Modelos e configura��es de boletos espec�ficos de UHs
      begin
        ReopenCondModBol(0);
      end;
      4: //Confer�ncia de Provis�es (sobre Contas Mensais)
      begin
        ReopenContasMes(0);
      end;
      else
      begin
        QrCondOutros.Close;
        QrCondModBol.Close;
        QrContasMes.Close;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCond.PageControl7Change(Sender: TObject);
begin
  if PageControl7.ActivePageIndex = 2 then
    ReopenTbImoveis(True);
end;

procedure TFmCond.ParecerdoconselhoTexto1Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  if DBCheck.CriaFm(TFmEntiCfgRel, FmEntiCfgRel, afmoNegarComAviso) then
  begin
    FmEntiCfgRel.FCodigo := QrCondCliente.Value;
    FmEntiCfgRel.ShowModal;
    FmEntiCfgRel.Destroy;
  end;
end;

procedure TFmCond.PesquisaEntidades();
var
  Str, Cnd, Tip: String;
begin
  Screen.Cursor := crDefault;
  try
    Str := '%'+EdParcial.Text+'%';
    Cnd := FormatFloat('0', QrCondCodigo.Value);
    //
    QrPesq.Close;
    QrPesq.SQL.Clear;
    case RGTipoEnt.ItemIndex of
      0: Tip := 'Propriet';
      1: Tip := 'Usuario';
    end;
    QrPesq.SQL.Add('SELECT blc.Controle, blc.Descri NO_BLOCO, imv.Unidade, ');
    QrPesq.SQL.Add('imv.Conta, IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT');
    QrPesq.SQL.Add('FROM condimov imv');
    QrPesq.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=imv.' + Tip);
    QrPesq.SQL.Add('LEFT JOIN condbloco blc ON blc.Controle=imv.Controle');
    QrPesq.SQL.Add('WHERE imv.Codigo=' + Cnd);
    QrPesq.SQL.Add('AND IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) LIKE "'+str+'"');
    QrPesq.SQL.Add('ORDER BY NO_ENT');
    UnDmkDAC_PF.AbreQuery(QrPesq, Dmod.MyDB);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCond.PMBlocoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrCondBloco.State <> dsInactive) and (QrCondBloco.RecordCount > 0);
  //
  Altera3.Enabled := Enab;
end;

procedure TFmCond.PMBloquetosPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrCondModBol.State <> dsInactive) and (QrCondModBol.RecordCount > 0);
  //
  Excluiitemns1.Enabled := Enab;
end;

procedure TFmCond.PMCondPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrCond.State <> dsInactive) and (QrCond.RecordCount > 0);
  //
  Altera1.Enabled         := Enab;
  Conselho1.Enabled       := Enab;
  Fluxos1.Enabled         := Enab;
  Emails1.Enabled         := Enab;
  ImportadeExcel1.Enabled := Enab;
end;

procedure TFmCond.PMEmeioPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrCondImov.State <> dsInactive) and (QrCondImov.RecordCount > 0);
  Enab2 := (QrCondEmeios.State <> dsInactive) and (QrCondEmeios.RecordCount > 0);
  //
  Incluiemeio1.Enabled                := Enab;
  Alteraemeio1.Enabled                := Enab and Enab2;
  Excluiemeio1.Enabled                := Enab and Enab2;
  Buscardocadastrodeentidade1.Enabled := Enab;
end;

procedure TFmCond.PMImovelPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrCondImov.State <> dsInactive) and (QrCondImov.RecordCount > 0);
  //
  Altera4.Enabled                 := Enab;
  Alteraumaum1.Visible            := False; //N�o usa
  Exclui4.Enabled                 := Enab;
  Adicionarzerosesquerda1.Enabled := Enab;
  Alteraselecionados1.Enabled     := Enab;
end;

procedure TFmCond.Protocolo11Click(Sender: TObject);
begin
  MostraCondProto('Protocolo');
end;

procedure TFmCond.Protocolo21Click(Sender: TObject);
begin
  MostraCondProto('Protocolo2');
end;

procedure TFmCond.Protocolo31Click(Sender: TObject);
begin
  MostraCondProto('Protocolo3');
end;

procedure TFmCond.Carta1Click(Sender: TObject);
begin
  //AlteraQuantidadeDeDias('DdEnviCrt', 'carta');
end;

procedure TFmCond.CkUsaCNAB_CfgClick(Sender: TObject);
begin
  ConfiguraUsaCNAB_Cfg(CkUsaCNAB_Cfg.Checked);
end;

procedure TFmCond.ConfiguraUsaCNAB_Cfg(Usa: Boolean);
begin
  if Usa then
  begin
    TabSheet24.TabVisible := False; //Boleto (parte 2)
    TabSheet25.TabVisible := False; //Boleto (parte 3)
  end else
  begin
    TabSheet24.TabVisible := True; //Boleto (parte 2)
    TabSheet25.TabVisible := True; //Boleto (parte 3)
  end;
end;

procedure TFmCond.ConfiguraodeEnviodeMensagens1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Configura��o de Envio de Mensagens';
  Prompt = 'Informe a configura��o:';
  Campo  = 'Descricao';
var
  I, Conta, CodIni: Integer;
  InadCEMCad: Variant;
begin
  CodIni := QrCondImovConta.Value;
  InadCEMCad := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo, Nome ' + Campo,
  'FROM diarcemcad ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, True);
  if InadCEMCad <> Null then
  begin
    PB1.Position := 0;
    PB1.Max := DBGUnidades.SelectedRows.Count;
    with DBGUnidades.DataSource.DataSet do
    for I := 0 to DBGUnidades.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBGUnidades.SelectedRows.Items[i]));
      //
      Conta := QrCondImovConta.Value;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Alterando UH ID ' + Geral.FF0(Conta));
      PB1.Position := PB1.Position + 1;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'condimov', False, [
      'InadCEMCad'], ['Conta'], [
      InadCEMCad], [Conta], True);
    end;
    ReopenCondImov(CodIni);
  end;
   PB1.Position := 0;
end;

procedure TFmCond.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCond.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCond.ReopenCondBloco(Controle: Integer);
begin
  // Deve ser antes!!!! Usa no lookup do TbUnidades!
  // O TbUnidades  � aberto logo ap�s o QrCondBloco!
  QrCB.Close;
  QrCB.Params[0].AsInteger := QrCondCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCB, Dmod.MyDB);
  //
  QrCondBloco.Close;
  QrCondBloco.Params[0].AsInteger := QrCondCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCondBloco, Dmod.MyDB);
  //
  if Controle > 0 then
    QrCondBloco.Locate('Controle', Controle, []);
  //
end;

procedure TFmCond.ReopenCondEmeios(Item: Integer);
begin
  QrCondEmeios.Close;
  QrCondEmeios.Params[0].AsInteger := QrCondImovConta.Value;
  UnDmkDAC_PF.AbreQuery(QrCondEmeios, Dmod.MyDB);
  //
  if Item > 0 then
    QrCondEmeios.Locate('Item', Item, [])
  else
    if FCondEmeios > 0 then
      QrCondEmeios.Locate('Item', FCondEmeios, []);
end;
procedure TFmCond.ReopenCondImov(Conta: Integer);
begin
  QrCondImov.Close;
  QrCondImov.Params[0].AsInteger := QrCondBlocoControle.Value;
  UnDmkDAC_PF.AbreQuery(QrCondImov, Dmod.MyDB);
  if Conta > 0 then
    QrCondImov.Locate('Conta', Conta, [])
end;

procedure TFmCond.ReopenCondModBol(Apto: Integer);
begin
  QrCondModBol.Close;
  QrCondModBol.Params[0].AsInteger := QrCondCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCondModBol, Dmod.MyDB);
  //
  if Apto > 0 then
    QrCondModBol.Locate('Apto', Apto, []);
end;

procedure TFmCond.ReopenCondOutros(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCondOutros, Dmod.MyDB, [
    'SELECT ere.*, eca.Nome NOME_CARGO , ',
    'ELT(ere.Assina+1, "N�o", "Sim", "???") NO_ASSINA, ',
    'IF(ent.Tipo=0, ent.ETe1, PTe1) Te1, ',
    'IF(ent.Tipo=0, ent.ECel, PCel) Cel, ',
    'IF(ent.Tipo=0, ent.EEmail, PEmail) Email ',
    'FROM entirespon ere ',
    'LEFT JOIN enticargos eca ON eca.Codigo=ere.Cargo ',
    'LEFT JOIN entidades ent ON ent.Codigo = ere.Entidade ',
    'WHERE ere.Codigo=' + Geral.FF0(QrCondCliente.Value),
    '']);
  //
  if Controle > 0 then
    QrCondOutros.Locate('Controle', Controle, [])
end;

procedure TFmCond.ReopenContasMes(Controle: Integer);
begin
  QrContasMes.Close;
  QrContasMes.Params[0].AsInteger := QrCondCliente.Value;
  UnDmkDAC_PF.AbreQuery(QrContasMes, Dmod.MyDB);
  //
  QrContasMes.Locate('Controle', Controle, []);
end;

procedure TFmCond.ReopenTbImoveis(Forca: Boolean);
begin
  if (TbUnidades.State <> dsInactive) or Forca then
  begin
    TbUnidades.Close;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCondImvProp, Dmod.MyDB, [
      'SELECT ent.Codigo, IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_PROPRIET ',
      'FROM entidades ent ',
      'LEFT JOIN condimov imv ON imv.Propriet = ent.Codigo ',
      'WHERE imv.Controle=' + Geral.FF0(QrCondBlocoControle.Value),
      '']);
    //
    TbUnidades.Filtered := False;
    TbUnidades.Filter := 'Controle=' + FormatFloat('0', QrCondBlocoControle.Value);
    TbUnidades.Filtered := True;
    UnDmkDAC_PF.AbreTable(TbUnidades, Dmod.MyDB);
  end;
end;

procedure TFmCond.RGModelBloqClick(Sender: TObject);
var
  Modelo: String;
  Enab1, Enab2: Boolean;
begin
  Modelo := RGModelBloq.Items[RGModelBloq.ItemIndex][1];
  Enab1  := (Modelo = 'H') or (Modelo = 'R') or (Modelo = 'I');
  Enab2  := Modelo = 'E';
  //
  LaConfigBol.Visible := Enab1;
  EdConfigBol.Visible := Enab1;
  CBConfigBol.Visible := Enab1;
  SBConfigBol.Visible := Enab1;
  //
  RGCompe.Visible  := Enab2;
end;

procedure TFmCond.RGTipoEntClick(Sender: TObject);
begin
  PesquisaEntidades();
end;

procedure TFmCond.DBEdAtivoChange(Sender: TObject);
begin
  case QrCondAtivo.Value of
       0: DBEdAtivo.Font.Color := clRed;
       1: DBEdAtivo.Font.Color := clBlue;
    else DBEdAtivo.Font.Color := clSilver;
  end;
end;

procedure TFmCond.DBGUnidadesDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := DBGUnidades.Columns[THackDBGrid(DBGUnidades).Col - 1].FieldName;
  if LowerCase(Campo) = 'nomeprop'  then
    DModG.CadastroDeEntidade(QrCondImovPropriet.Value, fmcadEntidade2, fmcadEntidade2)
  else
  if LowerCase(Campo) = 'nomeusuario'  then
    DModG.CadastroDeEntidade(QrCondImovUsuario.Value, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmCond.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCond.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCond.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCond.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCond.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCond.SBConfigBolClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.MostraConfigBol(EdConfigBol.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrConfigBol, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdConfigBol, CBConfigBol, QrConfigBol, VAR_CADASTRO);
    CBConfigBol.SetFocus;
  end;
end;

procedure TFmCond.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.MostraCfgInfl(EdCfgInfl.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrCfgInfl, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdCfgInfl, CBCfgInfl, QrCfgInfl, VAR_CADASTRO);
    CBCfgInfl.SetFocus;
  end;
end;

procedure TFmCond.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FEdCondInst := nil;
end;

procedure TFmCond.Unico1Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmCondImov, FmCondImov, afmoNegarComAviso,
    QrCondImov, stIns);
end;

procedure TFmCond.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCondCodigo.Value;
  Close;
end;

procedure TFmCond.BtWClientsClick(Sender: TObject);
begin
  if (QrCond.State <> dsInactive) and (QrCond.RecordCount > 0) then
    FmPrincipal.MostraWClients2(QrCondCodigo.Value);
end;

procedure TFmCond.Buscardocadastrodeentidade1Click(Sender: TObject);
var
  Conta, ExtrCod, EntiCod, ExtrInt1, ExtrInt2: Integer;
  TipoStr, Emeio, EntiNom, ExtrNom, FEmails: String;
begin
  DmCond.QrEmails.Close;
  DmCond.QrEmails.Params[00].AsInteger := QrCondCodigo.Value;
  DmCond.QrEmails.Params[01].AsInteger := QrCondCodigo.Value;
  UMyMod.AbreQuery(DmCond.QrEmails, Dmod.MyDB);
  //
  if DmCond.QrEmails.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o h� emeios cadastrados nas entidades ' +
      '(donos e moradores) para este condom�nio!');
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  //
  FEmails := UCriar.RecriaTempTableNovo(ntrttEmeios, DModG.QrUpdPID1, False);
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT * FROM condemeios ');
  Dmod.QrAux.SQL.Add('WHERE Conta=:P0 ');
  Dmod.QrAux.SQL.Add('AND EMeio=:P1');
  Dmod.QrAux.SQL.Add('');
  //
  Conta := 0;
  while not DmCond.QrEmails.Eof do
  begin
    ExtrCod := DmCond.QrEmailsApto.Value;
    Emeio   := DmCond.QrEmailsEMEIO.Value;
    Dmod.QrAux.Close;
    Dmod.QrAux.Params[00].AsInteger := ExtrCod;
    Dmod.QrAux.Params[01].AsString  := EMeio;
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB, 'TFmCond.Buscardocadastrodeentidade1Click()');
    //
    if Dmod.QrAux.RecordCount = 0 then
    begin
      inc(Conta, 1);
      if DmCond.QrEmailsPropriet.Value <> 0 then
        TipoStr := DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O)
      else
        TipoStr := DModG.ReCaptionTexto(VAR_M_O_R_A_D_O_R);
      //
      EntiCod   := DmCond.QrEmailsPropriet.Value;
      EntiNom   := DmCond.QrEmailsNOME.Value;
      ExtrNom   := DmCond.QrEmailsUnidade.Value;
      ExtrInt1  := DmCond.QrEmailsCodigo.Value;
      ExtrInt2  := DmCond.QrEmailsControle.Value;
      //
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FEmails, False, [
        'EntiCod', 'TipoStr', 'ExtrCod',
        'Emeio', 'EntiNom', 'ExtrNom',
        'ExtrInt1', 'ExtrInt2'
      ], ['Item'], [
        EntiCod, TipoStr, ExtrCod,
        Emeio, EntiNom, ExtrNom,
        ExtrInt1, ExtrInt2
      ], [Conta], False);
    end;
    DmCond.QrEmails.Next;
  end;
  //
  if Conta = 0 then
  begin
    Geral.MensagemBox('N�o foram encontrados emeios deste ' +
    'condom�nio n�o adicionados � lista!', 'Aviso', MB_OK + MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if DBCheck.CriaFm(TFmCondEmeiosAdd, FmCondEmeiosAdd, afmoNegarComAviso) then
  begin
    Screen.Cursor := crDefault;
    FmCondEmeiosAdd.ShowModal;
    FmCondEmeiosAdd.Destroy;
  end else
    Screen.Cursor := crDefault;
end;

procedure TFmCond.Adicionarzerosesquerda1Click(Sender: TObject);
var
  Min, Max, Conta: Integer;
  Unidade: String;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT');
  Dmod.QrAux.SQL.Add('MAX(LENGTH(TRIM(Unidade))) Maximo,');
  Dmod.QrAux.SQL.Add('MIN(LENGTH(TRIM(Unidade))) Minimo');
  Dmod.QrAux.SQL.Add('FROM condimov');
  Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
  Dmod.QrAux.Params[0].AsInteger := QrCondCodigo.Value;
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  //
  Min := Dmod.QrAux.FieldByName('Minimo').AsInteger;
  Max := Dmod.QrAux.FieldByName('Maximo').AsInteger;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE condimov SET Unidade=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Conta=:P1');
  //
  if (Min <> Max) and (Max > 0) then
  begin
    if Geral.MB_Pergunta('Confirma a formata��o de todas unidades ' +
      'habitacionais para ' + Geral.FF0(Max) + ' casas decimais?') = ID_YES then
    begin
      Conta := QrCondImovConta.Value;
      QrCondImov.First;
      while not QrCondImov.Eof do
      begin
        Unidade := Trim(QrCondImovUnidade.Value);
        if Length(Unidade) < Max then
        begin
          while Length(Unidade) < Max do
            Unidade := '0' + Unidade;
          Dmod.QrUpd.Params[00].AsString  := Unidade;
          Dmod.QrUpd.Params[01].AsInteger := QrCondImovConta.Value;
          Dmod.QrUpd.ExecSQL;
        end;
        QrCondImov.Next;
      end;
      ReopenCondImov(Conta);
      
    end;
  end else
    Geral.MB_Aviso('N�o h� necessidade de adicionar ' +
      'zeros nas unidades, pois todas tem ' + Geral.FF0(Max) + ' casas decimais!');
end;

procedure TFmCond.Altera1Click(Sender: TObject);
var
  Cond : Integer;
begin
  Cond := QrCondCodigo.Value;
  if not UMyMod.SelLockY(Cond, Dmod.MyDB, 'Cond', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Cond, Dmod.MyDB, 'Cond', 'Codigo');
      QrClientes.Close;
      UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCond.BtConfirmaClick(Sender: TObject);
var
  Ativo, CartEmiss, Codigo, Cliente, CfgInfl, UsaCNAB_Cfg, ModalCobr: Integer;
  Aceita, Correio, CtaCooper: String;
begin
  Cliente     := EdEntidade.ValueVariant;
  CartEmiss   := EdCartEmiss.ValueVariant;
  UsaCNAB_Cfg := Geral.BoolToInt(CkUsaCNAB_Cfg.Checked);
  ModalCobr   := RGModalCobr.ItemIndex;
  Ativo       := RGAtivo.ItemIndex - 1;
  //
  if Cliente = 0 then
  begin
    Geral.MB_Aviso('Defina a entidade do condom�nio!');
    PageControl1.ActivePageIndex := 0;
    EdEntidade.SetFocus;
    Exit;
  end;
  if (CartEmiss = 0) and (ImgTipo.SQLType = stUpd) and (CkUsaCNAB_Cfg.Checked = False) then
  begin
    if Geral.MensagemBox('Faltou definir a carteira para emiss�o ' +
      'de boletos. Deseja continuar assim mesmo?', 'Pergunta',
      MB_YESNOCANCEL + MB_ICONQUESTION) <> ID_YES then
    begin
      PageControl1.ActivePageIndex := 2;
      EdCartEmiss.SetFocus;
      Exit;
    end;
  end;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Codigo FROM cond ');
  Dmod.QrAux.SQL.Add('WHERE Cliente=' + Geral.FF0(Cliente));
  if ImgTipo.SQLType = stUpd then
    Dmod.QrAux.SQL.Add('AND Codigo<>' + Geral.FF0(QrCondCodigo.Value));
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Geral.MensagemBox('O cliente interno "' + CBEntidade.Text + '" j� ' +
      'est� cadastrado como condom�nio sob o c�digo n� ' + Geral.FF0(
      Dmod.QrAux.FieldByName('Codigo').AsInteger) + '!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    EdEntidade.SetFocus;
    Exit;
  end;
  CtaCooper := EdCtaCooper.Text;
  //
  if MyObjects.FIC(CtaCooper <> Geral.SoNumero_TT(CtaCooper), EdCtaCooper,
    'Conta corrente cooperado inv�lida: "' + CtaCooper + '"')
  then
    Exit;
  //
  if MyObjects.FIC(not DmBloq.ValidaModeloBoleto(RGModelBloq, RGCompe,
    EdConfigBol.ValueVariant), nil,
    'Voc� deve selecionar uma configura��o de boleto v�lida!')
  then
    Exit;
  //
  if MyObjects.FIC(ModalCobr = 1, RGModalCobr,
    'Para emitir boletos com registro utilize a janela de configura��o de boletos!')
  then
    Exit;
  //
  Aceita := Geral.FF0(RGAceita.ItemIndex);
  if Aceita = '0' then Aceita := 'N';
  if Aceita = '1' then Aceita := 'S';
  //
  Correio := Geral.FF0(RGCorreio.ItemIndex);
  if Correio = '0' then Correio := 'N';
  if Correio = '1' then Correio := 'S';
  //
  CfgInfl := EdCfgInfl.ValueVariant;
  Codigo  := UMyMod.BuscaEmLivreY_Def('Cond', 'Codigo', ImgTipo.SQLType, QrCondCodigo.Value);
  //
  if Ativo = -1 then
  begin
    if Geral.MB_Pergunta('Ao selecionar o condom�nio como "Inoperante" ele n�o estar� dispon�vel para eventuais consultas / gerenciamento no aplicativo!' +
      sLineBreak + 'AVISO: Este procedimento poder� ser desfeito!' + sLineBreak + 'Deseja continuar?') <> ID_YES
    then
      Exit;
  end;
  if not DModG.ValidaQtdCliInt(Codigo, Ativo) then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'Cond', False,
  [
    'AlterWeb', 'Cliente', 'Banco', 'Agencia',
    'Conta', 'CodCedente', 'Andares', 'TotApt',
    'Observ', 'DiaVencto', 'Posto',
    'BcoLogoPath', 'CliLogoPath', 'Aceite',
    'Correio', 'LocalPag', 'EspecieDoc',
    'Carteira', 'EspecieVal', 'Instrucao1',
    'Instrucao2', 'Instrucao3', 'Instrucao4',
    'Instrucao5', 'Instrucao6', 'Instrucao7',
    'Instrucao8', 'CartEmiss', 'PercJuros',
    'PercMulta', 'AgContaCed', 'DVAgencia',
    'DVConta', 'ModelBloq',
    'Cedente', 'SacadAvali', 'MIB',
    'IDCobranca', 'OperCodi', 'MBB',
    'MRB', 'PBB', 'MSB', 'PSB',
    'MPB', 'MAB', 'ModalCobr',
    'VTCBBNITAR', 'ProLaINSSp', 'ProLaINSSr',
    'ConfigBol', 'MSP', 'Sigla',
    'BalAgrMens', 'Compe', 'HideCompe',
    'Assistente', 'TipoCobranca', 'UsaCNAB_Cfg',
    'CartTxt', 'CNAB', 'CtaCooper',
    'CfgInfl', 'Ativo'
  ], ['Codigo'],
  [
    1, Cliente, EdCondBanco.ValueVariant, EdCondAgen.ValueVariant,
    EdCondConta.Text, EdCondCodCed.Text, EdTAndares.ValueVariant, EdTAptos.ValueVariant,
    MeObserv.Text, RGDiaVencto.ItemIndex + 1, EdCondPosto.ValueVariant,
    EdCondBcoLogo.Text, EdCondCliLogo.Text, Aceita,
    Correio, EdCondLocalPg.Text, EdCondEspDoc.Text,
    EdCondCarteira.Text, EdCondEspVal.Text, EdCondInst1.Text,
    EdCondInst2.Text, EdCondInst3.Text, EdCondInst4.Text,
    EdCondInst5.Text, EdCondInst6.Text, EdCondInst7.Text,
    EdCondInst8.Text, CartEmiss, Geral.DMV(EdPercJuros.Text),
    Geral.DMV(EdPercMulta.Text), EdAgContaCed.Text, EdDVAgencia.Text,
    EdDVConta.Text, RGModelBloq.ItemIndex,
    EdCedente.Text, EdSacadAvali.Text, RGMIB.ItemIndex,
    EdIdCobranca.Text, EdOperCodi.Text, RGMBB.ItemIndex,
    RGMRB.ItemIndex, RGPBB.ItemIndex, RGMSB.ItemIndex, RGPSB.ItemIndex,
    RGMPB.ItemIndex, RGMAB.ItemIndex, ModalCobr,
    dmkEdVTCBBNITAR.ValueVariant, dmkEdProLaINSSp.ValueVariant, dmkEdProLaINSSr.ValueVariant,
    EdConfigBol.ValueVariant, RGMSP.ItemIndex,
    EdSigla.Text, MLAGeral.BTI(CkBalAgrMens.Checked), RGCompe.ItemIndex,
    MLAGeral.BTI(CkHideCompe.Checked), EdAssistente.ValueVariant, RGTipoCobranca.ItemIndex,
    UsaCNAB_Cfg, EdCartTxt.Text, RGCNAB.Items[RGCNAB.ItemIndex], CtaCooper,
    CfgInfl, Ativo
  ], [Codigo], True) then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE entidades ent, cond cnd');
    Dmod.QrUpd.SQL.Add('SET ent.CliInt=cnd.Codigo');
    Dmod.QrUpd.SQL.Add('WHERE cnd.Cliente=ent.Codigo');
    Dmod.QrUpd.ExecSQL;
    //
    DBCheck.VerificaEntiCliInt();
    DmCond.VerificaAccManager(saDeAssistenteParaAccManager);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE enticliint SET Ativo=' + Geral.FF0(Ativo) + ' ',
      'WHERE CodCliInt=' + Geral.FF0(Codigo),
      '']);
    //
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Cond', 'Codigo');
    LocCod(Codigo,Codigo);
    MostraEdicao(0, stLok, 0);
  end;
  //
  VerificaTabelas(Codigo);
  //
  if FSeq = 1 then
    Close;
end;

procedure TFmCond.BtCtaMesAltClick(Sender: TObject);
begin
  MostraContasMes(stUpd);
end;

procedure TFmCond.BtCtaMesExcClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(DMod.QrUpd, QrContasMes, TDBGrid(GradeContasMes),
    'contasmes', ['controle'], ['controle'], istPergunta, '');
end;

procedure TFmCond.BtCtaMesInsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMContasMes, BtCtaMesIns);
end;

procedure TFmCond.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.ValueVariant);
  //
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'cond', Codigo);
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cond', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cond', 'Codigo');
end;

procedure TFmCond.BtEmeioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEmeio, BtEmeio);
end;

procedure TFmCond.BtEntidadesClick(Sender: TObject);
begin
  if (QrCond.State <> dsInactive) and (QrCond.RecordCount > 0) then 
    MostraEntidade2(QrCondCliente.Value, 0);
end;

procedure TFmCond.BtImovelClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl7.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMImovel, BtImovel);
end;

procedure TFmCond.BtPesquisaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondLocEmail, FmCondLocEmail, afmoNegarComAviso) then
  begin
    FmCondLocEmail.ShowModal;
    FmCondLocEmail.Destroy;
  end;
end;

procedure TFmCond.Altera3Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmCondBloco, FmCondBloco, afmoNegarComAviso,
    QrCondBloco, stUpd);
end;

procedure TFmCond.Altera4Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmCondImov, FmCondImov, afmoNegarComAviso,
    QrCondImov, stUpd);
end;

procedure TFmCond.Alteraemeio1Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmCondEmeios, FmCondEmeios, afmoLiberado,
    QrCondEmeios, stUpd);
end;

procedure TFmCond.AlteraOutMor1Click(Sender: TObject);
begin

end;

{
procedure TFmCond.AlteraQuantidadeDeDias(Campo, Descri: String);
  procedure AlteraDiasAtual(Dias: Integer; Conta: Integer);
  begin
    Screen.Cursor := crHourGlass;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'condimov', False,
      [Campo], ['Conta'], [Dias], [Conta], True);
    Screen.Cursor := crDefault;
  end;
var
  DiasTxt: String;
  DiasNum: Integer;
  I: Integer;
begin
  DiasTxt := '0';
  if InputQuery('Altera��o de Dias', 'Informe a quantidade de dias:', DiasTxt) then
  begin
    DiasNum := Geral.IMV(DiasTxt);
    DiasTxt := Geral.FF0(DiasNum);
    DiasNum := Geral.IMV(DiasTxt);
    if DBGUnidades.SelectedRows.Count > 1 then
    begin
      if Geral.MensagemBox('Confirma a altera��o de dias de ' + Descri +
      ' para ' + DiasTxt + ' dos itens selecionados?', 'Pergunta',
      MB_ICONQUESTION+MB_YESNOCANCEL) =
      ID_YES then
      begin
        PB1.Position := 0;
        PB1.Max := DBGUnidades.SelectedRows.Count;
        with DBGUnidades.DataSource.DataSet do
        for i := 0 to DBGUnidades.SelectedRows.Count-1 do
        begin
          PB1.Position := PB1.Position + 1;
          GotoBookmark(pointer(DBGUnidades.SelectedRows.Items[i]));
          AlteraDiasAtual(DiasNum, QrCondImovConta.Value);
        end;
      end;
    end else AlteraDiasAtual(DiasNum, QrCondImovConta.Value);
    ReopenCondBloco(QrCondBlocoControle.Value);
    PB1.Position := 0;
  end;
end;
}

procedure TFmCond.Alteraumaum1Click(Sender: TObject);
begin
  PnUmAUm.Visible := True;
  CkUmAUm.Checked := False;
  QrCondImov.First;
  while not QrCondImov.Eof do
  begin
    if not CkUmAUm.Checked then
    begin
      UMyMod.FormInsUpd_Show(TFmCondImov, FmCondImov, afmoNegarComAviso,
        QrCondImov, stUpd);
    end else
      QrCondImov.Last;
    QrCondImov.Next;
  end;
  PnUmAUm.Visible := False;
end;

procedure TFmCond.BitBtn3Click(Sender: TObject);
var
  CNAB_Cfg: Integer;
begin
  if (QrCNAB_Cfg.State <> dsInactive) and (QrCNAB_Cfg.RecordCount > 0) then
    CNAB_Cfg := QrCNAB_CfgCodigo.Value
  else
    CNAB_Cfg := 0;
  //
  UBloquetosCond.MostraCNAB_Cfg(CNAB_Cfg);
  //
  if (QrCond.State <> dsInactive) and (QrCond.RecordCount > 0) then
    UBloquetosCond.ReopenCNAB_Cfg(QrCNAB_Cfg, Dmod.MyDB, QrCondCliente.Value);
end;

procedure TFmCond.BtBlocoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMBloco, BtBloco);
end;

procedure TFmCond.BtBloquetosClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  MyObjects.MostraPopUpDeBotao(PMBloquetos, BtBloquetos);
end;

procedure TFmCond.BtCondClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMCond, BtCond);
end;

procedure TFmCond.FormCreate(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenMaster;
  //
  CBEntidade.ListSource   := DsClientes;
  CBAssistente.ListSource := DsAssist;
  CBConfigBol.ListSource  := DsConfigBol;
  CBCfgInfl.ListSource    := DsCfgInfl;
  CBCedente.ListSource    := DsCedente;
  CBSacadAvali.ListSource := DsSacador;
  CBCartEmiss.ListSource  := DsCartEmiss;
  //
  QrCond.Database     := Dmod.MyDB;
  TbUnidades.Database := Dmod.MyDB;
  //
  FmPrincipal.PreencheModelosBloq(RGModelBloq);
  //
  FSeq                         := 0;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  PageControl5.ActivePageIndex := 0;
  PageControl6.ActivePageIndex := 0;
  PageControl7.ActivePageIndex := 0;
  PainelEdita.Align            := alClient;
  PainelDados.Align            := alClient;
  PainelEdit.Align             := alClient;
  PageControl2.Align           := alClient;
  //
  UnDmkDAC_PF.AbreQuery(QrAssist, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCedente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSacador, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrConfigBol, Dmod.MyDB);
  //
  CriaOForm;
  //
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  //
  UnDmkDAC_PF.AbreQuery(QrCfgInfl, Dmod.MyDB);
  //
  Enab  := (QrCond.State <> dsInactive) and (QrCond.RecordCount > 0);
  Enab2 := (QrCondBloco.State <> dsInactive) and (QrCondBloco.RecordCount > 0);
  BtBloco.Enabled  := Enab;
  BtImovel.Enabled := (Enab) and (Enab2);
  MyObjects.ConfiguraTipoCobranca(RGTipoCobranca, 2, 0);
  //
  TabSheet5.TabVisible  := False; //Tab - Filhos
  TabSheet6.TabVisible  := False; //Tab - Moradores
  TabSheet7.TabVisible  := False; //Tab - Animas
  TabSheet16.TabVisible := False; //Tab - Ve�culo
  TabSheet17.TabVisible := False; //Tab - Empregados dom�sticos
  TabSheet18.TabVisible := False; //Tab - Emerg�ncia
end;

procedure TFmCond.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCondCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCond.SBAssistenteClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdAssistente.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrAssist, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdAssistente, CBAssistente, QrClientes, VAR_CADASTRO);
    CBAssistente.SetFocus;
  end;
end;

procedure TFmCond.SBCondClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdEntidade.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
    UMyMod.AbreQuery(QrCedente, Dmod.MyDB);
    UMyMod.AbreQuery(QrSacador, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrClientes, VAR_CADASTRO);
    CBEntidade.SetFocus;
  end;
end;

procedure TFmCond.SbImprimeClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondImovImp, FmCondImovImp, afmoNegarComAviso) then
  begin
    FmCondImovImp.EdEmpresa.ValueVariant := QrCondCodigo.Value;
    FmCondImovImp.CBEmpresa.KeyValue     := QrCondCodigo.Value;
    FmCondImovImp.ShowModal;
    FmCondImovImp.Destroy;
  end;
end;

procedure TFmCond.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmCond.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Codigo(QrCondCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmCond.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCond.QrCondAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtBloco.Enabled := (QrCond.State <> dsInactive) and (QrCond.RecordCount > 0);
end;

procedure TFmCond.QrCondAfterScroll(DataSet: TDataSet);
begin
  ReopenCondBloco(0);
  PesquisaEntidades();
  UBloquetosCond.ReopenCNAB_Cfg(QrCNAB_Cfg, Dmod.MyDB, QrCondCliente.Value);
  //
  if PageControl1.ActivePageIndex = 2 then //Conselho fiscal
    ReopenCondOutros(0);
end;

procedure TFmCond.Fluxos1Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmCondFlx, FmCondFlx, afmoLiberado, QrCond, stUpd);
end;

procedure TFmCond.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FSeq = 1 then
    MostraEdicao(1, stIns, 0);
end;

procedure TFmCond.SbQueryClick(Sender: TObject);
begin
  //LocCod(QrCondCodigo.Value,
  //CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cond', Dmod.MyDB, CO_VAZIO));
  //
  LocCod(QrCondCodigo.Value, CuringaLoc.CriaForm('con.Codigo',
    'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome)', 'cond con', Dmod.MyDB,
    CO_VAZIO, False, 'LEFT JOIN entidades ent ON ent.Codigo = con.Cliente'));
end;

procedure TFmCond.SMS1Click(Sender: TObject);
begin
  //AlteraQuantidadeDeDias('DdEnviSMS', 'SMS');
end;

procedure TFmCond.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCond.FracaoIdeal1Click(Sender: TObject);
  procedure AlteraFracaoIdealAtual(FracaoIdeal: Double; Conta: Integer);
  begin
    Screen.Cursor := crHourGlass;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'condimov', False,
      ['FracaoIdeal'], ['Conta'], [FracaoIdeal], [Conta], True);
    Screen.Cursor := crDefault;
  end;
var
  FracaoTxt: String;
  FracaoNum: Double;
  I: Integer;
begin
  FracaoTxt := '1,000000';
  if InputQuery('Nova Fra��o Ideal', 'Informe a nova fra��o ideal:', FracaoTxt) then
  begin
    FracaoNum := Geral.DMV(FracaoTxt);
    FracaoTxt := Geral.FFT(FracaoNum, 6, siPositivo);
    FracaoNum := Geral.DMV(FracaoTxt);
    if DBGUnidades.SelectedRows.Count > 1 then
    begin
      if Geral.MensagemBox('Confirma a altera��o da fra��o ideal ' +
      'para ' + FracaoTxt + ' dos itens selecionados?', 'Pergunta',
      MB_ICONQUESTION+MB_YESNOCANCEL) =
      ID_YES then
      begin
        PB1.Position := 0;
        PB1.Max := DBGUnidades.SelectedRows.Count;
        with DBGUnidades.DataSource.DataSet do
        for i := 0 to DBGUnidades.SelectedRows.Count-1 do
        begin
          PB1.Position := PB1.Position + 1;
          GotoBookmark(pointer(DBGUnidades.SelectedRows.Items[i]));
          AlteraFracaoIdealAtual(FracaoNum, QrCondImovConta.Value);
        end;
      end;
    end else AlteraFracaoIdealAtual(FracaoNum, QrCondImovConta.Value);
    ReopenCondBloco(QrCondBlocoControle.Value);
    PB1.Position := 0;
  end;
end;

procedure TFmCond.GradePesqDblClick(Sender: TObject);
begin
  if (QrPesq.State = dsBrowse) and (QrPesq.RecordCount > 0) then
  begin
    if (QrCondBloco.State = dsBrowse) and (QrCondBloco.RecordCount > 0) then
      QrCondBloco.Locate('Controle', QrPesqControle.Value, []);
    if (QrCondImov.State = dsBrowse) and (QrCondImov.RecordCount > 0) then
      QrCondImov.Locate('Conta', QrPesqConta.Value, []);
  end;
end;

procedure TFmCond.ImportadeExcel1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiLoad05, FmEntiLoad05, afmoNegarComAviso) then
  begin
    FmEntiLoad05.FCod_Cond := QrCondCodigo.Value;
    FmEntiLoad05.ShowModal;
    FmEntiLoad05.Destroy;
    //
    Va(vpLast);
  end;
end;

procedure TFmCond.Inclui1Click(Sender: TObject);
begin
  QrClientes.Close;
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  MostraEdicao(1, stIns, 0);
end;

procedure TFmCond.Inclui3Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmCondBloco, FmCondBloco, afmoNegarComAviso,
    QrCondBloco, stIns);
end;

procedure TFmCond.Incluialteramodeloconfigurao1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmAptosModBol, FmAptosModBol, afmoNegarComAviso) then
  begin
    FmAptosModBol.FTabela := 'condmodbol';
    FmAptosModBol.FCodBol := QrCondCodigo.Value;
    FmAptosModBol.CriaQuery(QrCondCodigo.Value);
    FmAptosModBol.ShowModal;
    FmAptosModBol.Destroy;
    //
    ReopenCondModBol(0);
  end;
end;

procedure TFmCond.Incluiemeio1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondEmeios, FmCondEmeios, afmoNegarComAviso) then
  begin
    FmCondEmeios.ImgTipo.SQLType := stIns;
    FmCondEmeios.ShowModal;
    FmCondEmeios.Destroy;
  end;
end;

procedure TFmCond.Incluimultiplascontas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasMesSelMulCta, FmContasMesSelMulCta, afmoNegarComAviso) then
  begin
    FmContasMesSelMulCta.FCliIntCodi := QrCondCliente.Value;
    FmContasMesSelMulCta.FCliIntNome := QrCondNCONDOM.Value;
    FmContasMesSelMulCta.ShowModal;
    FmContasMesSelMulCta.Destroy;
    //
    ReopenContasMes(QrContasMesControle.Value);
  end;
end;

procedure TFmCond.Incluinicacontaeconfigura1Click(Sender: TObject);
begin
  MostraContasMes(stIns);
end;

procedure TFmCond.QrCondBeforeClose(DataSet: TDataSet);
begin
  BtBloco.Enabled := False;
  //
  QrCondBloco.Close;
  QrCB.Close;
  QrPesq.Close;
  QrCondOutros.Close;
  QrCNAB_Cfg.Close;
end;

procedure TFmCond.QrCondBeforeOpen(DataSet: TDataSet);
begin
  QrCondCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCond.QrCondBlocoAfterOpen(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  Enab := (QrCondBloco.State <> dsInactive) and (QrCondBloco.RecordCount > 0);
  //
  BtImovel.Enabled    := Enab;
  BtBloquetos.Enabled := Enab;
end;

procedure TFmCond.QrCondBlocoAfterScroll(DataSet: TDataSet);
begin
  ReopenCondImov(0);
  ReopenTbImoveis(True);
end;

procedure TFmCond.QrCondBlocoBeforeClose(DataSet: TDataSet);
begin
  QrCondImov.Close;
  TbUnidades.Close;
  //
  BtImovel.Enabled    := False;
  BtBloquetos.Enabled := False;
end;

procedure TFmCond.QrCondCalcFields(DataSet: TDataSet);
begin
  QrCondTELCON_TXT.Value := Geral.FormataTelefone_TT(QrCondTELCOM.Value);
end;

procedure TFmCond.QrCondImovAfterScroll(DataSet: TDataSet);
begin
  ReopenCondEmeios(0);
end;

procedure TFmCond.QrCondImovBeforeClose(DataSet: TDataSet);
begin
  QrCondEmeios.Close;
end;

procedure TFmCond.QrCondImovCalcFields(DataSet: TDataSet);
begin
  if QrCondImovSitImv.Value = 1 then
    QrCondImovNOMESITIMV.Value := 'Sim'
  else
    QrCondImovNOMESITIMV.Value := 'N�o';
  //
  QrCondImovJuridico_TXT.Value := dmkPF.DefineJuricoSigla(QrCondImovJuridico.Value);
end;

procedure TFmCond.QrCondModBolCalcFields(DataSet: TDataSet);
begin
  QrCondModBolNOME_MODELBLOQ.Value :=
    FmPrincipal.LetraModelosBloq(QrCondModBolModelBloq.Value);
end;

procedure TFmCond.QrCondOutrosCalcFields(DataSet: TDataSet);
begin
  QrCondOutrosMandatoIni_TXT.Value := Geral.FDT(QrCondOutrosMandatoIni.Value, 3);
  QrCondOutrosMandatoFim_TXT.Value := Geral.FDT(QrCondOutrosMandatoFim.Value, 3);
  QrCondOutrosTe1_TXT.Value        := Geral.FormataTelefone_TT(QrCondOutrosTe1.Value);
  QrCondOutrosCel_TXT.Value        := Geral.FormataTelefone_TT(QrCondOutrosCel.Value);
end;

procedure TFmCond.QrContasMesCalcFields(DataSet: TDataSet);
begin
  QrContasMesPERIODOINI_TXT.Value :=
    Geral.FDT(Geral.PeriodoToDate(QrContasMesPeriodoIni.Value, 1, False), 19);
  QrContasMesPERIODOFIM_TXT.Value :=
    Geral.FDT(Geral.PeriodoToDate(QrContasMesPeriodoFim.Value, 1, False), 19);
end;

end.

