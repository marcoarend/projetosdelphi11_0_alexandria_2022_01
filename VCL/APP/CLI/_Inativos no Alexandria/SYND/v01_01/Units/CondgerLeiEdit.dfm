object FmCondGerLeiEdit: TFmCondGerLeiEdit
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-015 :: Altera'#231#227'o de Leitura'
  ClientHeight = 328
  ClientWidth = 591
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 591
    Height = 113
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 24
      Width = 43
      Height = 13
      Caption = 'Unidade:'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 80
      Top = 24
      Width = 79
      Height = 13
      Caption = 'Medi'#231#227'o anterior'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 184
      Top = 24
      Width = 70
      Height = 13
      Caption = 'Medi'#231#227'o atual:'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 288
      Top = 24
      Width = 47
      Height = 13
      Caption = 'Consumo:'
      FocusControl = DBEdit4
    end
    object Label5: TLabel
      Left = 392
      Top = 24
      Width = 27
      Height = 13
      Caption = 'Valor:'
      FocusControl = DBEdit5
    end
    object Label6: TLabel
      Left = 12
      Top = 68
      Width = 41
      Height = 13
      Caption = 'Per'#237'odo:'
      FocusControl = DBEdit6
    end
    object Label7: TLabel
      Left = 484
      Top = 24
      Width = 68
      Height = 13
      Caption = 'Pre'#231'o unit'#225'rio:'
      FocusControl = DBEdit7
    end
    object DBText1: TDBText
      Left = 0
      Top = 0
      Width = 591
      Height = 17
      Align = alTop
      Alignment = taCenter
      DataField = 'NOMECLI'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 583
    end
    object DBEdit1: TDBEdit
      Left = 12
      Top = 40
      Width = 64
      Height = 21
      DataField = 'Unidade'
      DataSource = DmCond.DsCNS
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 80
      Top = 40
      Width = 100
      Height = 21
      DataField = 'MedAnt'
      DataSource = DmCond.DsCNS
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 184
      Top = 40
      Width = 100
      Height = 21
      DataField = 'MedAtu'
      DataSource = DmCond.DsCNS
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 288
      Top = 40
      Width = 100
      Height = 21
      DataField = 'Consumo'
      DataSource = DmCond.DsCNS
      TabOrder = 3
    end
    object DBEdit5: TDBEdit
      Left = 392
      Top = 40
      Width = 88
      Height = 21
      DataField = 'Valor'
      DataSource = DmCond.DsCNS
      TabOrder = 4
    end
    object DBEdit6: TDBEdit
      Left = 12
      Top = 84
      Width = 561
      Height = 21
      DataField = 'PERIODO_TXT'
      DataSource = FmCondGer.DsPrev
      TabOrder = 5
    end
    object DBEdit7: TDBEdit
      Left = 484
      Top = 40
      Width = 88
      Height = 21
      DataField = 'Preco'
      DataSource = DmCond.DsCNS
      TabOrder = 6
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 161
    Width = 591
    Height = 53
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Label9: TLabel
      Left = 16
      Top = 8
      Width = 73
      Height = 13
      Caption = 'Leitura anterior:'
    end
    object Label10: TLabel
      Left = 108
      Top = 8
      Width = 61
      Height = 13
      Caption = 'Leitura atual:'
    end
    object Label11: TLabel
      Left = 200
      Top = 8
      Width = 68
      Height = 13
      Caption = 'Pre'#231'o unit'#225'rio:'
    end
    object Label12: TLabel
      Left = 292
      Top = 8
      Width = 47
      Height = 13
      Caption = 'Consumo:'
    end
    object Label13: TLabel
      Left = 384
      Top = 8
      Width = 47
      Height = 13
      Caption = 'Consumo:'
    end
    object Label8: TLabel
      Left = 476
      Top = 8
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object Label14: TLabel
      Left = 340
      Top = 8
      Width = 6
      Height = 13
      Caption = '1'
    end
    object Label15: TLabel
      Left = 432
      Top = 8
      Width = 6
      Height = 13
      Caption = '2'
    end
    object EdLeiAnt: TdmkEdit
      Left = 16
      Top = 24
      Width = 88
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnChange = EdLeiAntChange
    end
    object EdLeiAtu: TdmkEdit
      Left = 108
      Top = 24
      Width = 88
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnChange = EdLeiAtuChange
    end
    object EdPreco: TdmkEdit
      Left = 200
      Top = 24
      Width = 88
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnChange = EdPrecoChange
    end
    object EdConsumo: TdmkEdit
      Left = 292
      Top = 24
      Width = 88
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdValor: TdmkEdit
      Left = 476
      Top = 24
      Width = 97
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdConsum2: TdmkEdit
      Left = 384
      Top = 24
      Width = 88
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 591
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 543
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 495
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 244
        Height = 32
        Caption = 'Altera'#231#227'o de Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 244
        Height = 32
        Caption = 'Altera'#231#227'o de Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 244
        Height = 32
        Caption = 'Altera'#231#227'o de Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 214
    Width = 591
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 587
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 258
    Width = 591
    Height = 70
    Align = alBottom
    TabOrder = 4
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 587
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 443
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
end
