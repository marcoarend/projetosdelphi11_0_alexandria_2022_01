object FmCondGerStep: TFmCondGerStep
  Left = 339
  Top = 185
  Caption = 
    'GER-CONDM-007 :: Desfazimento de Processamento de Item de Retorn' +
    'o CNAB'
  ClientHeight = 494
  ClientWidth = 840
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 840
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitWidth = 784
    ExplicitHeight = 398
    object DBGrid1: TdmkDBGrid
      Left = 0
      Top = 0
      Width = 840
      Height = 332
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Step'
          Title.Caption = 'S'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDNum'
          Title.Caption = 'Bloqueto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QuitaData'
          Title.Caption = 'Dta quit.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValTitul'
          Title.Caption = 'Val. titul.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValPago'
          Title.Caption = 'Val. pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValJuros'
          Title.Caption = 'Val. juros'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Arquivo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carteira'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsLei_Step
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Step'
          Title.Caption = 'S'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDNum'
          Title.Caption = 'Bloqueto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QuitaData'
          Title.Caption = 'Dta quit.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValTitul'
          Title.Caption = 'Val. titul.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValPago'
          Title.Caption = 'Val. pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValJuros'
          Title.Caption = 'Val. juros'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Arquivo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carteira'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 840
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 792
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 744
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 724
        Height = 32
        Caption = 'Desfazimento de Processamento de Item de Retorno CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 724
        Height = 32
        Caption = 'Desfazimento de Processamento de Item de Retorno CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 724
        Height = 32
        Caption = 'Desfazimento de Processamento de Item de Retorno CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 380
    Width = 840
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 836
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 227
        Height = 16
        Caption = 'S = Item processado no arquivo retorno'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 227
        Height = 16
        Caption = 'S = Item processado no arquivo retorno'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 840
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 836
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 692
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Val. titul'
        TabOrder = 1
        OnClick = BtOKClick
        NumGlyphs = 2
      end
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 112
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Val. juros'
        TabOrder = 2
        OnClick = BitBtn1Click
        NumGlyphs = 2
      end
    end
  end
  object QrLei_Step: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Step, Codigo, IDNum, ValTitul, '
      'ValPago, ValJuros,'#13
      'QuitaData, Arquivo, Carteira'
      'FROM cnab_lei'
      'WHERE IDNum=:P0'
      'AND Entidade=:P1'
      'ORDER BY OcorrData, QuitaData, Codigo DESC')
    Left = 112
    Top = 108
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLei_StepStep: TSmallintField
      FieldName = 'Step'
      MaxValue = 1
    end
    object QrLei_StepCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLei_StepIDNum: TIntegerField
      FieldName = 'IDNum'
    end
    object QrLei_StepValTitul: TFloatField
      FieldName = 'ValTitul'
    end
    object QrLei_StepValPago: TFloatField
      FieldName = 'ValPago'
    end
    object QrLei_StepQuitaData: TDateField
      FieldName = 'QuitaData'
    end
    object QrLei_StepArquivo: TWideStringField
      FieldName = 'Arquivo'
    end
    object QrLei_StepCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLei_StepValJuros: TFloatField
      FieldName = 'ValJuros'
    end
  end
  object DsLei_Step: TDataSource
    DataSet = QrLei_Step
    Left = 140
    Top = 108
  end
end
