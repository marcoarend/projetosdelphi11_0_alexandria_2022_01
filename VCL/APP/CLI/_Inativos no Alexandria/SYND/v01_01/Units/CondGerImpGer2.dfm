object FmCondGerImpGer2: TFmCondGerImpGer2
  Left = 0
  Top = 0
  Caption = 'GER-CONDM-018 :: Impress'#245'es do Gerenciamento de Comdom'#237'nios'
  ClientHeight = 558
  ClientWidth = 1005
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1005
    Height = 48
    Align = alTop
    Caption = 'Relat'#243'rio de Arrecada'#231#245'es (Lista de Cond'#244'minos)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1003
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 510
    Width = 1005
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 893
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtTitulo: TBitBtn
      Left = 396
      Top = 4
      Width = 90
      Height = 40
      Caption = 'T'#237'tulo'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtTituloClick
    end
    object BitBtn2: TBitBtn
      Tag = 5
      Left = 800
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Cross'
      NumGlyphs = 2
      TabOrder = 3
      Visible = False
      OnClick = BitBtn2Click
    end
    object CkDesign: TCheckBox
      Left = 116
      Top = 24
      Width = 85
      Height = 17
      Caption = 'Design mode.'
      TabOrder = 4
    end
    object dmkEdit5: TdmkEdit
      Left = 220
      Top = 20
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-33000'
      ValMax = '33000'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object dmkEdit6: TdmkEdit
      Left = 284
      Top = 20
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-33000'
      ValMax = '33000'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1005
    Height = 462
    Align = alClient
    TabOrder = 2
    object GradeA: TStringGrid
      Left = 1
      Top = 26
      Width = 1003
      Height = 315
      Align = alClient
      ColCount = 6
      DefaultColWidth = 32
      DefaultRowHeight = 18
      RowCount = 2
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Univers Light Condensed'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goColSizing, goColMoving, goEditing]
      ParentFont = False
      TabOrder = 0
      OnDrawCell = GradeADrawCell
      OnMouseDown = GradeAMouseDown
      OnMouseUp = GradeAMouseUp
      OnSelectCell = GradeASelectCell
      ColWidths = (
        32
        32
        32
        32
        33
        32)
    end
    object GradeB: TStringGrid
      Left = 1
      Top = 341
      Width = 1003
      Height = 120
      Align = alBottom
      ColCount = 6
      DefaultRowHeight = 18
      RowCount = 4
      TabOrder = 1
      Visible = False
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1003
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Label3: TLabel
        Left = 792
        Top = 6
        Width = 108
        Height = 13
        Caption = 'Largura total da folha:'
      end
      object Label4: TLabel
        Left = 964
        Top = 6
        Width = 31
        Height = 13
        Caption = 'pixels.'
      end
      object Label1: TLabel
        Left = 8
        Top = 6
        Width = 58
        Height = 13
        Caption = 'Altura linha:'
      end
      object Label2: TLabel
        Left = 104
        Top = 6
        Width = 57
        Height = 13
        Caption = 'Tam. fonte:'
      end
      object EdLarguraFolha: TdmkEdit
        Left = 904
        Top = 2
        Width = 57
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdLarguraFolhaChange
      end
      object EdLinhaAlt: TdmkEdit
        Left = 68
        Top = 2
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '18'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 18
        OnChange = EdLinhaAltChange
      end
      object EdTamFonte: TdmkEdit
        Left = 164
        Top = 2
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '10'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 10
        OnChange = EdTamFonteChange
        OnExit = EdTamFonteExit
      end
      object CkRecalcN: TCheckBox
        Left = 204
        Top = 4
        Width = 173
        Height = 17
        Caption = 'Recalcular larguras de valores.'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = CkRecalcNClick
      end
      object CkRecalcT: TCheckBox
        Left = 380
        Top = 4
        Width = 165
        Height = 17
        Caption = 'Recalcular larguras de Textos.'
        Checked = True
        State = cbChecked
        TabOrder = 4
        OnClick = CkRecalcTClick
      end
      object CkLargTit: TCheckBox
        Left = 552
        Top = 4
        Width = 217
        Height = 17
        Caption = 'Incluir os t'#237'tulos de colunas no rec'#225'lculos.'
        TabOrder = 5
        OnClick = CkLargTitClick
      end
    end
  end
  object frxGrade: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    EngineOptions.MaxMemSize = 10000000
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38006.786384259300000000
    ReportOptions.LastChange = 39872.778306180600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  LastLeft: Extended;'
      '  FLastRow: Extended;'
      '    '
      
        'procedure Cross1OnPrintCell(Memo: TfrxMemoView; RowIndex, Column' +
        'Index, CellIndex: Integer; RowValues, ColumnValues, Value: Varia' +
        'nt);'
      'var'
      '  i: Integer;'
      '  s1, s2: String;                         '
      'begin'
      
        '  //Memo.Height := ObtemRowHeight(RowIndex);                    ' +
        '                      '
      
        '  //ShowMessage('#39'Memo: '#39' + FloatToStr(RowIndex) + '#39' = '#39' + FloatT' +
        'oStr(Memo.Height));                                             ' +
        '                 '
      '  case ObtemHALign(ColumnIndex, RowIndex) of'
      '    0: Memo.HAlign := haLeft;'
      '    1: Memo.HAlign := haCenter;'
      '    2: Memo.HAlign := haRight;'
      '  end;'
      '  i := ObtemWidth(ColumnIndex);'
      '  Memo.Width := i;     '
      '  if ColumnIndex <> 0 then           '
      '  begin'
      
        '    Memo.Left := LastLeft;                                      ' +
        '                '
      
        '    LastLeft    := LastLeft + i;                                ' +
        '                         '
      '  end else begin'
      
        '    Memo.Left := 0;                                             ' +
        '         '
      '    LastLeft  := i;'
      '  end;'
      
        '  FLastRow := RowIndex;                                         ' +
        '       '
      'end;'
      ''
      
        'procedure Cross1OnCalcHeight(RowIndex: Integer; RowValues: Varia' +
        'nt; var Height: Extended);'
      'var'
      '  h: Extended;                                  '
      'begin'
      '  h := ObtemRowHeight(0);                  '
      '  if Height <> h then Height := h;'
      
        '  //ShowMessage('#39'Cross: ='#39' + FloatToStr(Height));               ' +
        '                                               '
      'end;'
      ''
      'begin'
      '  //MasterData1.Height := ObtemRowHeight(FLastRow);'
      
        '  //ShowMessage('#39'Band: '#39' + FloatToStr(FLastRow) + '#39' = '#39' + FloatT' +
        'oStr(MasterData1.Height));                                      ' +
        '                        '
      'end.')
    OnBeforePrint = frxGradeBeforePrint
    OnGetValue = frxGradeGetValue
    OnUserFunction = frxGradeUserFunction
    Left = 312
    Top = 224
    Datasets = <
      item
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 61.000000000000000000
        Top = 173.858380000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        RowCount = 1
        object Cross1: TfrxCrossView
          Left = 2.047310000000000000
          Width = 100.000000000000000000
          Height = 61.000000000000000000
          ShowHint = False
          DownThenAcross = False
          MinWidth = 30
          ShowColumnHeader = False
          ShowColumnTotal = False
          ShowRowHeader = False
          ShowRowTotal = False
          OnCalcHeight = 'Cross1OnCalcHeight'
          OnPrintCell = 'Cross1OnPrintCell'
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D2232322C30
            343733312220546F703D223139332C3835383338222057696474683D22363022
            204865696768743D22323122205265737472696374696F6E733D22323422204F
            6E4166746572446174613D2243726F73733143656C6C304F6E41667465724461
            746122204F6E4265666F72655072696E743D2243726F73733143656C6C304F6E
            4265666F72655072696E74222053686F7748696E743D2246616C73652220416C
            6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43686172
            7365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48656967
            68743D222D382220466F6E742E4E616D653D22417269616C2220466F6E742E53
            74796C653D223022204672616D652E5479703D22313522204672616D652E5769
            6474683D22302C312220476170583D22332220476170593D2233222048416C69
            676E3D22686143656E7465722220506172656E74466F6E743D2246616C736522
            2056416C69676E3D22766143656E7465722220546578743D2230222F3E3C5466
            72784D656D6F56696577205461673D223122204C6566743D22302220546F703D
            2230222057696474683D223022204865696768743D2230222052657374726963
            74696F6E733D2238222053686F7748696E743D2246616C73652220416C6C6F77
            45787072657373696F6E733D2246616C736522204672616D652E5479703D2231
            352220476170583D22332220476170593D2233222048416C69676E3D22686152
            6967687422205374796C653D2263656C6C222056416C69676E3D22766143656E
            7465722220546578743D22222F3E3C546672784D656D6F56696577205461673D
            223222204C6566743D22302220546F703D2230222057696474683D2230222048
            65696768743D223022205265737472696374696F6E733D2238222053686F7748
            696E743D2246616C73652220416C6C6F7745787072657373696F6E733D224661
            6C736522204672616D652E5479703D2231352220476170583D22332220476170
            593D2233222048416C69676E3D226861526967687422205374796C653D226365
            6C6C222056416C69676E3D22766143656E7465722220546578743D22222F3E3C
            546672784D656D6F56696577205461673D223322204C6566743D22302220546F
            703D2230222057696474683D223022204865696768743D223022205265737472
            696374696F6E733D2238222053686F7748696E743D2246616C73652220416C6C
            6F7745787072657373696F6E733D2246616C736522204672616D652E5479703D
            2231352220476170583D22332220476170593D2233222048416C69676E3D2268
            61526967687422205374796C653D2263656C6C222056416C69676E3D22766143
            656E7465722220546578743D22222F3E3C2F63656C6C6D656D6F733E3C63656C
            6C6865616465726D656D6F733E3C546672784D656D6F56696577204C6566743D
            22302220546F703D2230222057696474683D223022204865696768743D223022
            205265737472696374696F6E733D2238222053686F7748696E743D2246616C73
            652220416C6C6F7745787072657373696F6E733D2246616C736522204672616D
            652E5479703D2231352220476170583D22332220476170593D2233222056416C
            69676E3D22766143656E7465722220546578743D22222F3E3C546672784D656D
            6F56696577204C6566743D22302220546F703D2230222057696474683D223022
            204865696768743D223022205265737472696374696F6E733D2238222053686F
            7748696E743D2246616C73652220416C6C6F7745787072657373696F6E733D22
            46616C736522204672616D652E5479703D2231352220476170583D2233222047
            6170593D2233222056416C69676E3D22766143656E7465722220546578743D22
            222F3E3C2F63656C6C6865616465726D656D6F733E3C636F6C756D6E6D656D6F
            733E3C546672784D656D6F56696577205461673D2231303022204C6566743D22
            302220546F703D2230222057696474683D2232303022204865696768743D2231
            3030303022205265737472696374696F6E733D223234222053686F7748696E74
            3D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C7365
            22204672616D652E5479703D2231352220476170583D22332220476170593D22
            33222048416C69676E3D22686143656E74657222205374796C653D22636F6C75
            6D6E222056416C69676E3D22766143656E7465722220546578743D22222F3E3C
            2F636F6C756D6E6D656D6F733E3C636F6C756D6E746F74616C6D656D6F733E3C
            546672784D656D6F56696577205461673D2233303022204C6566743D22302220
            546F703D2230222057696474683D22333222204865696768743D223130303030
            22205265737472696374696F6E733D2238222056697369626C653D2246616C73
            65222053686F7748696E743D2246616C73652220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22312220
            4672616D652E5479703D2231352220476170583D22332220476170593D223322
            2048416C69676E3D22686143656E7465722220506172656E74466F6E743D2246
            616C736522205374796C653D22636F6C6772616E64222056416C69676E3D2276
            6143656E7465722220546578743D224772616E6420546F74616C222F3E3C2F63
            6F6C756D6E746F74616C6D656D6F733E3C636F726E65726D656D6F733E3C5466
            72784D656D6F56696577204C6566743D22302220546F703D2230222057696474
            683D2232303022204865696768743D223022205265737472696374696F6E733D
            2238222056697369626C653D2246616C7365222053686F7748696E743D224661
            6C73652220416C6C6F7745787072657373696F6E733D2246616C736522204672
            616D652E5479703D2231352220476170583D22332220476170593D2233222048
            416C69676E3D22686143656E746572222056416C69676E3D22766143656E7465
            722220546578743D22222F3E3C546672784D656D6F56696577204C6566743D22
            302220546F703D2230222057696474683D2232303022204865696768743D2230
            22205265737472696374696F6E733D2238222056697369626C653D2246616C73
            65222053686F7748696E743D2246616C73652220416C6C6F7745787072657373
            696F6E733D2246616C736522204672616D652E5479703D223135222047617058
            3D22332220476170593D2233222048416C69676E3D22686143656E7465722220
            56416C69676E3D22766143656E7465722220546578743D22222F3E3C54667278
            4D656D6F56696577204C6566743D22302220546F703D2230222057696474683D
            223022204865696768743D223022205265737472696374696F6E733D22382220
            56697369626C653D2246616C7365222053686F7748696E743D2246616C736522
            20416C6C6F7745787072657373696F6E733D2246616C736522204672616D652E
            5479703D2231352220476170583D22332220476170593D2233222048416C6967
            6E3D22686143656E746572222056416C69676E3D22766143656E746572222054
            6578743D22222F3E3C546672784D656D6F56696577204C6566743D2230222054
            6F703D2230222057696474683D2232303022204865696768743D223022205265
            737472696374696F6E733D2238222053686F7748696E743D2246616C73652220
            416C6C6F7745787072657373696F6E733D2246616C736522204672616D652E54
            79703D2231352220476170583D22332220476170593D2233222048416C69676E
            3D22686143656E746572222056416C69676E3D22766143656E74657222205465
            78743D22222F3E3C2F636F726E65726D656D6F733E3C726F776D656D6F733E3C
            546672784D656D6F56696577205461673D2232303022204C6566743D22302220
            546F703D2230222057696474683D2232303022204865696768743D2231303030
            3022205265737472696374696F6E733D223234222053686F7748696E743D2246
            616C73652220416C6C6F7745787072657373696F6E733D2246616C7365222046
            72616D652E5479703D2231352220476170583D22332220476170593D22332220
            48416C69676E3D22686143656E74657222205374796C653D22726F7722205641
            6C69676E3D22766143656E7465722220546578743D22222F3E3C2F726F776D65
            6D6F733E3C726F77746F74616C6D656D6F733E3C546672784D656D6F56696577
            205461673D2234303022204C6566743D22302220546F703D2230222057696474
            683D22333222204865696768743D22313030303022205265737472696374696F
            6E733D2238222056697369626C653D2246616C7365222053686F7748696E743D
            2246616C73652220416C6C6F7745787072657373696F6E733D2246616C736522
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D31332220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223122204672616D652E5479703D2231
            352220476170583D22332220476170593D2233222048416C69676E3D22686143
            656E7465722220506172656E74466F6E743D2246616C736522205374796C653D
            22726F776772616E64222056416C69676E3D22766143656E7465722220546578
            743D224772616E6420546F74616C222F3E3C2F726F77746F74616C6D656D6F73
            3E3C63656C6C66756E6374696F6E733E3C6974656D20302F3E3C2F63656C6C66
            756E6374696F6E733E3C636F6C756D6E736F72743E3C6974656D20322F3E3C2F
            636F6C756D6E736F72743E3C726F77736F72743E3C6974656D20322F3E3C2F72
            6F77736F72743E3C2F63726F73733E}
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 30.236240000000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
      end
      object PageHeader1: TfrxPageHeader
        Height = 94.488250000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object frxShapeView1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCond."NOMECLI"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 68.031540000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPrev."PERIODO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxCrossObject1: TfrxCrossObject
    Left = 340
    Top = 224
  end
  object Query_: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    Left = 368
    Top = 224
  end
  object frxGrade2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39873.799550636580000000
    ReportOptions.LastChange = 39873.799550636580000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 396
    Top = 224
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object frxDsQuery: TfrxDBDataset
    UserName = 'frxDsQuery'
    CloseDataSource = False
    DataSet = Query
    BCDToCurrency = False
    Left = 424
    Top = 224
  end
  object PMGradeA: TPopupMenu
    OnPopup = PMGradeAPopup
    Left = 452
    Top = 224
    object Alterasiglanocadastrodacontaplanodecontas1: TMenuItem
      Caption = 'Altera a sigla no cadastro da &Conta (plano de contas)'
      OnClick = Alterasiglanocadastrodacontaplanodecontas1Click
    end
    object Alterasiglanocadastrodaarrecadaobase1: TMenuItem
      Caption = 'Altera a sigla no cadastro da &Arrecada'#231#227'o base'
      OnClick = Alterasiglanocadastrodaarrecadaobase1Click
    end
    object AlterasiglanocadastrodaLeitura1: TMenuItem
      Caption = 'Altera a sigla no cadastro da &Leitura'
      OnClick = AlterasiglanocadastrodaLeitura1Click
    end
    object Alteratextodottuloselecionado1: TMenuItem
      Caption = 'Altera o &Texto do t'#237'tulo selecionado'
      OnClick = Alteratextodottuloselecionado1Click
    end
    object Larguradacolunaatual1: TMenuItem
      Caption = 'Altera a lar&Gura da coluna atual'
      OnClick = Larguradacolunaatual1Click
    end
  end
  object QrPgBloq: TmySQLQuery
    Database = Dmod.MyDB
    Left = 284
    Top = 224
    object QrPgBloqApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrPgBloqFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPgBloqData: TDateField
      FieldName = 'Data'
    end
    object QrPgBloqDATA_TXT: TWideStringField
      FieldName = 'DATA_TXT'
      Size = 10
    end
  end
  object QrAptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imc.Conta, imc.Andar, imc.Unidade, sta.Sigla, '
      
        'IF(imc.Propriet=0, "", IF(pro.Tipo=0, pro.RazaoSocial, pro.Nome)' +
        ') NOMEPROP,'
      
        'IF(imc.Usuario=0, "", IF (mor.Tipo=0, mor.RazaoSocial, mor.Nome)' +
        ') NOMEUSUARIO'
      'FROM condimov imc'
      'LEFT JOIN entidades pro ON pro.Codigo=imc.Propriet'
      'LEFT JOIN entidades mor ON mor.Codigo=imc.Usuario'
      'LEFT JOIN status sta ON sta.Codigo=imc.Status'
      'LEFT JOIN entidades end ON end.Codigo=imc.Imobiliaria'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=imc.Protocolo'
      'LEFT JOIN condbloco cnb ON cnb.Controle=imc.Controle'
      'WHERE imc.Ativo=1'
      'AND imc.Codigo = :P0'
      'ORDER BY cnb.Ordem, imc.Andar, imc.Unidade')
    Left = 256
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAptosAndar: TIntegerField
      FieldName = 'Andar'
      Required = True
    end
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrAptosNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrAptosNOMEUSUARIO: TWideStringField
      FieldName = 'NOMEUSUARIO'
      Size = 100
    end
    object QrAptosSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 1
    end
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrAptosDATAPG_TXT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'DATAPG_TXT'
      LookupDataSet = QrPgBloq
      LookupKeyFields = 'Apto'
      LookupResultField = 'DATA_TXT'
      KeyFields = 'Conta'
      Lookup = True
    end
  end
  object QrBol3: TmySQLQuery
    Database = Dmod.MyDB
    Left = 228
    Top = 224
    object QrBol3Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrBol3FracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
    end
    object QrBol3Andar: TIntegerField
      FieldName = 'Andar'
    end
    object QrBol3Boleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
    object QrBol3Apto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBol3Propriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrBol3Vencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrBol3Unidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrBol3NOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrBol3BOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Required = True
      Size = 65
    end
    object QrBol3KGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
  end
  object Query: TmySQLQuery
    Database = DModG.MyPID_DB
    Left = 344
    Top = 292
  end
end
