unit CB4_Data4;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Codebase, dmkRadioGroup;

type
  TFmCB4_Data4 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    RGFisicoSrc: TdmkRadioGroup;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FCB4: CODE4;
    FData4: DATA4;
    //
  public
    { Public declarations }
  end;

  var
  FmCB4_Data4: TFmCB4_Data4;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UMySQLModule, CB4_PF,
  MyDBCheck, Principal;

{$R *.DFM}


const
{
   dd : array[1..6] of FIELD4INFO = (
      (name:'CODIGO'     ; atype: integer('N'); len: 15; dec:0),
      (name:'IP_SRC'     ; atype: integer('C'); len: 64; dec:0),
      (name:'HD_SRC'     ; atype: integer('C'); len: 32; dec:0),
      (name:'USERID'     ; atype: integer('N'); len: 15; dec:0),
      (name:'ENTIDE'     ; atype: integer('N'); len: 15; dec:0),
      (name:'NFISCL'     ; atype: integer('N'); len: 15; dec:0),
      (name:'IMAGEM'     ; atype: integer('X'); len: 10; dec:0),
      (name:nil          ; atype: 0           ; len:  0; dec:0));
}
   dd : array[1..8] of FIELD4INFO = (
      (name:'CODIGO'     ; atype: integer('N'); len: 15; dec:0),
      (name:'IP_SRC'     ; atype: integer('C'); len: 64; dec:0),
      (name:'HD_SRC'     ; atype: integer('C'); len: 32; dec:0),
      (name:'USERID'     ; atype: integer('N'); len: 15; dec:0),
      (name:'TERCEI'     ; atype: integer('N'); len: 15; dec:0),
      (name:'NFISCL'     ; atype: integer('N'); len: 15; dec:0),
      (name:'IMAGEM'     ; atype: integer('X'); len: 10; dec:0),
      (name:nil          ; atype: 0           ; len:  0; dec:0));
{
   dd1 : array[1..3] of FIELD4INFO = (
      (name:'ENTIDE'     ; atype: integer('N'); len: 15; dec:0),
      (name:'NFISCL'     ; atype: integer('N'); len: 15; dec:0),
      (name:nil          ; atype: 0           ; len:  0; dec:0));
}
   t : array[1..2] of TAG4INFO = (
      (name:'TAG_CODIGO' ; expression:'CODIGO'    ; filter:''  ; unique:r4unique; descending:0),
      (name:nil          ; expression:nil         ; filter:nil ; unique:0; descending:0));


procedure TFmCB4_Data4.BitBtn1Click(Sender: TObject);
begin
{  Falta testar
  if UnCB4_PF.AbreTabelaFotos(EdEmpresa.ValueVariant, FCB4, FData4) then
  begin
    d4close(FData4);
    d4fieldsAdd(FData4, 5, @dd1);
  end;
}
end;

procedure TFmCB4_Data4.BtOKClick(Sender: TObject);
var
  {
  Codigo, NumVal, NO_ARQ, IP_SRC, HD_SRC: FIELD4 ;
  jp: TJPEGImage;
  }
(*
  ms: TMemoryStream;
  ptr: Pointer;
  field_ptr: FIELD4 ;
  I, rc: Integer;
  //
  Ini, Tempo1, Tempo2, Tempo3: TDateTime;
  Unidade: String;
  Cod, Num, Arq: String;
*)
  //
  Host, Port, User, Senha, Path, Tb, NoTab: String;
  Caminho: PChar;
  Empresa, Tabela: Integer;
  Tb4Reg: Boolean;
begin
  Empresa := EdEmpresa.ValueVariant;
  //
  Tb := '';
  Tabela := RGFisicoSrc.ItemIndex;
  case Tabela of
    2,3,4: Tb := CO_TAB_NAME_CB4_FOTODOCU;
    else
    begin
      Geral.MensagemBox('Fonte f�sica n�o implementada! SOLICITE � DERMATEK',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  if Tb = '' then
    Exit;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then
    Exit;
  //
  Host   := Dmod.QrControleCB4_Host.Value;
  Port   := Geral.FF0(Dmod.QrControleCB4_Port.Value);
  User   := Dmod.QrControleCB4_User.Value;
  Senha  := Dmod.QrControleCB4_Pwd.Value;
  //Path   := Dmod.QrControleCB4_DB.Value;
  if not FmPrincipal.TentaDefinirDiretorio(Empresa, '\Data\', Path) then
    Exit;
  //
  if not UnCB4_PF.VariaveisConexaoDefinidas(Host, Port, User, Senha, Path, True) then
    Exit;
  if Path[Length(Path)] <> '\' then
    Path := Path + '\';
  ForceDirectories(Path);

  //

  Screen.Cursor := crHourGlass;
  Tb4Reg := Dmod.ReopenCB4Data4(Empresa, Tabela);
  try
    if FCB4 <> nil then
    begin
      code4close(FCB4);
      code4initUndo(FCB4);
      FCB4 := nil;
    end;
    //
    FCB4 := code4init ;
    code4connect(FCB4,
      PChar(Host), PChar(Port), PChar(User), PChar(Senha), PChar(''));
    code4safety(FCB4, 0);
    //
    NoTab := Tb + Geral.FFN(Empresa, 4);
    Caminho := PChar(Path + NoTab);
    code4errOpen( FCB4, 0 ); { no message is displayed if NO_FILE does not exist }
    FData4 := d4open(FCB4, Caminho);
    if (FData4 = nil ) then     { file does not exist }
    begin
      if Tb4Reg then
      begin
        Geral.MensagemBox('Tabela n�o localizada! INFORME A DERMATEK' + #13#10 +
        'A cria��o na tabela foi registrada no MySQL mas n�o foi localizada!',
        'ERRO', MB_OK+MB_ICONERROR);
      end else
      begin
        code4safety(FCB4, 0);
        FData4 := d4create( FCB4, Caminho, @dd, @t);
        if (FData4 = nil ) then
          Geral.MensagemBox('Tabela n�o pode ser criada!', 'ERRO', MB_OK+MB_ICONERROR)
        else begin
          if not Tb4Reg then
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cb4data4', False, [
            ], ['Empresa', 'Tabela'], [], [Empresa, Tabela], True);
          end;
          Geral.MensagemBox('Tabela criada!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
        end;
      end;
    end
    else
      Geral.MensagemBox('Tabela j� existe!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
    //
    if FCB4 <> nil then
    begin
      code4close(FCB4);
      code4initUndo(FCB4);
      FCB4 := nil;
    end;
    //
  finally
    Screen.Cursor := crDefault;
    //WaitForClose ;
  end;
end;

procedure TFmCB4_Data4.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCB4_Data4.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCB4_Data4.FormCreate(Sender: TObject);// Adicionado por .DFM > .PAS
CB?.ListSource = DModG.DsEmpresas;

begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGFisicoSrc, CO_TAB_INDX_CB4_DESCREVE,
    Length(CO_TAB_INDX_CB4_DESCREVE), 0);
  //
end;

procedure TFmCB4_Data4.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
