unit EmailBloqueto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  dmkDBGrid, frxClass, ComCtrls, frxExportPDF, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, IdComponent, IdTCPConnection, IdTCPClient, IdMessage,
  IdMessageClient, IdSMTP, IdText, IdAttachment, IdBaseComponent, ActiveX,
  ComObj, IdAttachmentFile, IdExplicitTLSClientServerBase, IdSMTPBase,
  UnDmkProcFunc, dmkImage, UnDmkEnums, IdIOHandler, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL, UnMail, IdIMAP4;

type
  TFmEmailBloqueto = class(TForm)
    Panel1: TPanel;
    DBGrid1: TdmkDBGrid;
    QrUnidCond: TmySQLQuery;
    DsUnidCond: TDataSource;
    Panel3: TPanel;
    PB1: TProgressBar;
    StaticText1: TStaticText;
    STStatus: TStaticText;
    QrSel: TmySQLQuery;
    QrSelSelecio: TSmallintField;
    frxPDFExport: TfrxPDFExport;
    QrUnidCondBOLAPTO: TWideStringField;
    QrUnidCondDTA1_TXT: TWideStringField;
    QrUnidCondDTA2_TXT: TWideStringField;
    QrUnidCondApto: TIntegerField;
    QrUnidCondEntidad: TIntegerField;
    QrUnidCondProtoco: TIntegerField;
    QrUnidCondBloquet: TIntegerField;
    QrUnidCondUnidade: TWideStringField;
    QrUnidCondProprie: TWideStringField;
    QrUnidCondValor: TFloatField;
    QrUnidCondData1: TDateField;
    QrUnidCondData2: TDateField;
    QrUnidCondTexto: TWideStringField;
    QrUnidCondRecipNome: TWideStringField;
    QrUnidCondRecipEmeio: TWideStringField;
    QrUnidCondOrdem: TIntegerField;
    QrUnidCondSelecio: TSmallintField;
    QrUnidCondAndar: TIntegerField;
    QrUnidCondRecipItem: TIntegerField;
    QrUnidCondRecipProno: TWideStringField;
    Panel4: TPanel;
    RGFormato: TRadioGroup;
    IdMessage1: TIdMessage;
    IdSMTP1: TIdSMTP;
    CkRecriarImagens: TCheckBox;
    Panel5: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGMsg: TdmkDBGrid;
    MeMsg: TDBMemo;
    TabSheet2: TTabSheet;
    memEMailSource: TMemo;
    TabSheet3: TTabSheet;
    edMessageText: TMemo;
    TabSheet4: TTabSheet;
    MeEncode: TMemo;
    QrUnidCondVencimento: TDateField;
    QrUnidCondPreEmeio: TIntegerField;
    QrNCs: TmySQLQuery;
    QrNCsItens: TLargeintField;
    QrUnidCondCondominio: TWideStringField;
    QrUnidCondIDEmeio: TIntegerField;
    RGIndividuais: TRadioGroup;
    TabSheet5: TTabSheet;
    dmkDBGrid2: TdmkDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtSaida: TBitBtn;
    IdSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IMAP: TIdIMAP4;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrUnidCondBeforeOpen(DataSet: TDataSet);
    procedure QrUnidCondAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FIdMessage: TIdMessage;
    //
    procedure ReopenUnidCond(Protoco: Integer);
    procedure AtualizaTodos(Status: Integer);
    //function  SendEMail(aMessage: TIdMessage; Server: String): Boolean;
    function  EnviaEmeio(BloqPath: String; AnexaBlq: boolean): Boolean;
    //function  GetPriority(Index: Integer): TIdMessagePriority;
    function  SubstituiVariaveis(Texto: String): String;
    function  HaNaoConformidades(): Boolean;
  public
    { Public declarations }
    //FPermiteAnexarBloqueto: Boolean;
    FPrevCodigo: Integer;
  end;

  var
  FmEmailBloqueto: TFmEmailBloqueto;

implementation

uses Module, ModuleCond, UMySQLModule, ModuleGeral, dmkGeral, ModuleBloq,
  UnMyObjects, UnDmkWeb, UnInternalConsts, Protocolo;

const
  FCaminhoEMail = CO_DIR_RAIZ_DMK + '\Emeios\Bloquetos\';
  FIniPath      = CO_DIR_RAIZ_DMK + '\Base64\';
  FBase64       = CO_DIR_RAIZ_DMK + '\Base64\Base64.exe';

{$R *.DFM}

procedure TFmEmailBloqueto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmailBloqueto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ReopenUnidCond(0);
end;

procedure TFmEmailBloqueto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //FPermiteAnexarBloqueto := True;
  //
  MyObjects.frxConfiguraPDF(frxPDFExport);
  //
  DBGMsg.DataSource   := DModG.DsPreEmMsg;
  MeMsg.DataSource    := DModG.DsPreEmMsg;
  QrUnidCond.Database := DModG.MyPID_DB;
  QrSel.Database      := DModG.MyPID_DB;
  QrNCs.Database      := DModG.MyPID_DB;
  //QrNaoCarregados_.Database := DModG.MyPID_DB;
  //
  ForceDirectories(FCaminhoEMail);// = 'C:\Dermatek\Emeios\Bloquetos\';
  ForceDirectories(FIniPath);//      = 'C:\Dermatek\Base64\';
  if not FileExists(FBase64) then //       = 'C:\Dermatek\Base64\Base64.exe';
    Geral.MensagemBox(PChar('Sem o arquivo "' + FBase64 +
    '" n�o ser� poss�vel enviar imagens por emeio!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
end;

procedure TFmEmailBloqueto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmailBloqueto.DBGrid1CellClick(Column: TColumn);
var
  Status, Protoco, Bloquet: Integer;
begin
  if Column.FieldName = 'Selecio' then
  begin
    Status := QrUnidCondSelecio.Value;
    if Status = 0 then Status := 1 else Status := 0;
    Protoco := QrUnidCondProtoco.Value;
    Bloquet := QrUnidCondBloquet.Value;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE unidcond SET Selecio=:P0');
    DModG.QrUpdPID1.SQL.Add('WHERE Protoco=:P1 AND Bloquet=:P2');
    DModG.QrUpdPID1.Params[00].AsInteger := Status;
    DModG.QrUpdPID1.Params[01].AsInteger := Protoco;
    DModG.QrUpdPID1.Params[02].AsInteger := Bloquet;
    DModG.QrUpdPID1.ExecSQL;
    //
    ReopenUnidCond(Protoco);
  end;
end;


procedure TFmEmailBloqueto.ReopenUnidCond(Protoco: Integer);
begin
  //
  QrUnidCond.Close;
  if UMyMod.AbreQuery(QrUnidCond, DModG.MyPID_DB, 'TFmEmailBloqueto.ReopenUnidCond()') then
    QrUnidCond.Locate('Protoco', Protoco, []);
  //
  {
  QrNaoCarregados.Close;
  if UMyMod.AbreQuery(QrNaoCarregados, 'TFmEmailBloqueto.ReopenUnidCond()') then
    QrNaoCarregados.Locate('Protoco', Protoco, []);
  }  
  //
end;

procedure TFmEmailBloqueto.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmEmailBloqueto.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmEmailBloqueto.AtualizaTodos(Status: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE unidcond SET Selecio=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Status;
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenUnidCond(QrUnidCondApto.Value);
end;

procedure TFmEmailBloqueto.BtOKClick(Sender: TObject);
var
  Arquivo: String;
  frxModelo: TfrxReport;
  SdoJaAtz: Boolean;
begin
  SdoJaAtz := False;
  // necess�rio para iserir no servidor protocolos de emeios enviados
  if not DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then Exit;
  //
  try
  DModG.QrPreEmail.Close;
  // N�o conformidades
  if HaNaoConformidades then Exit;
  //
  Screen.Cursor := crHourGlass;
  BtOK.Enabled := False;
  frxModelo := nil;
  UMyMod.AbreQuery(QrSel, DModG.MyPID_DB, 'TFmEmailBloqueto.BtOKClick()');
  PB1.Position := 0;
  PB1.Max := QrSel.RecordCount * 2;
  QrUnidCond.First;
  // SQL para atualizar stutus do protocolo
  Dmod.QrUpdW.SQL.Clear;
  Dmod.QrUpdW.SQL.Add('UPDATE protpakits SET AlterWeb=1, ');
  Dmod.QrUpdW.SQL.Add('DataE=:P0 ');
  Dmod.QrUpdW.SQL.Add('WHERE Conta=:P1');
  Dmod.QrUpdW.SQL.Add('');
  // SQL para atualizar stutus do protocolo
  Dmod.QrUpdN.SQL.Clear;
  Dmod.QrUpdN.SQL.Add('INSERT INTO recebibloq SET ');
  Dmod.QrUpdN.SQL.Add('Apto=:P0, Protoco=:P1, Bloqueto=:P2, Emeio=:P3, ');
  Dmod.QrUpdN.SQL.Add('Envios=1 ');
  Dmod.QrUpdN.SQL.Add('ON DUPLICATE KEY UPDATE envios=envios+1');
  //
  while not QrUnidCond.Eof do
  begin
    Application.ProcessMessages;
    if QrUnidCondSelecio.Value > 0 then
    begin                                 
      if (DModG.QrPreEmailNaoEnvBloq.Value = 1) (*or not FPermiteAnexarBloqueto*) then
        //Reenvio sem bloqueto!
        EnviaEmeio('', False)
      else begin
        Arquivo := FCaminhoEMail + 'Prot_' +
          FormatFloat('000000', QrUnidCondProtoco.Value) + '_Bloq_' +
          FormatFloat('000000', QrUnidCondBloquet.Value) + '.pdf';
        if FileExists(Arquivo) then
          DeleteFile(Arquivo);
        if DmBloq.QrBoletos.State = dsInactive then
        begin
          Geral.MensagemBox(PChar('"DCond.QrBoletos" n�o est� aberta. ' + #13#10 +
          'Verifique se o envio de emeio requer anexo de bloqueto e comunique a DERMATEK!' +
          #13#10 + 'Em caso de re-aviso marque o campo indicativo no cadastro do pr�-emeio!'),
          'AVISO', MB_OK+MB_ICONWARNING);
          Screen.Cursor := crDefault;
          Exit;
        end;
        if DmBloq.QrBoletos.Locate('BOLAPTO', QrUnidCondBOLAPTO.Value, []) =
        True then
        begin
          PB1.Position := PB1.Position + 1;
          STStatus.Caption := '  Criando o bloqueto "' + Arquivo +'"...';
          Update;
          Application.ProcessMessages;
          //
          frxPDFExport.FileName := Arquivo;
          case RGIndividuais.ItemIndex of
            0: frxModelo := DmBloq.ImprimeBoletos_Novo(istAtual, ficExporta,
               Arquivo, frxPDFExport, (*FmCondGer.QrPrevCodigo.Value*) FPrevCodigo,
               (*FmCondGer.CkAntigo.Checked*) SdoJaAtz);
            1: frxModelo := DmBloq.ImprimeBoletos_Novo(istAtual, ficExporta,
               Arquivo, frxPDFExport, (*FmCondGer.QrPrevCodigo.Value*) FPrevCodigo,
               (*FmCondGer.CkAntigo.Checked*) SdoJaAtz);
          end;
          Application.ProcessMessages;
          if frxModelo = nil then
          begin
            STStatus.Caption := '  BLOQUETO N�O DEFINIDO: "' + Arquivo +'"...';
            Screen.Cursor := crDefault;
            Exit;
          end;
          // registra no servidor que vai mandar o emeio
          Dmod.QrUpdN.Params[00].AsInteger := QrUnidCondApto.Value;
          Dmod.QrUpdN.Params[01].AsInteger := QrUnidCondProtoco.Value;
          Dmod.QrUpdN.Params[02].AsInteger := QrUnidCondBloquet.Value;
          Dmod.QrUpdN.Params[03].AsInteger := QrUnidCondIDEmeio.Value;
          Dmod.QrUpdN.ExecSQL;
          // enviar emeio
          if EnviaEmeio(Arquivo, True) then
          begin
            Application.ProcessMessages;
            // alterar status do protocolo para enviado, colocando a data de envio
            Dmod.QrUpdW.Params[00].AsString  := Geral.FDT(Date, 1);
            Dmod.QrUpdW.Params[01].AsInteger := QrUnidCondProtoco.Value;
            Dmod.QrUpdW.ExecSQL;
          end;
        end else
          Geral.MensagemBox('O Bloqueto-UH ' + QrUnidCondBOLAPTO.Value +
            ' n�o foi localizado para ser salvo!', 'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
    QrUnidCond.Next;
  end;
  PB1.Position := 0;
  STStatus.Caption := '  ENVIO FINALIZADO!';
  Geral.MB_Aviso('Envio Finalizado!');
  BtOK.Enabled := True;
  finally
    // reabrir para mostrar quais foram enviados
    //Parei aqui ver o que precisa para mostrar
    (*
    FmCondGerProto.QrProtoMail.Close;
    FmCondGerProto.QrProtoMail.Open;
    QrUnidCond.Close;
    QrUnidCond.Open;
    *)
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEmailBloqueto.QrUnidCondBeforeOpen(DataSet: TDataSet);
begin
  {
  QrEmails.Close;
  QrEmails.Params[0].AsInteger := FmCondGer.QrCondCodigo.Value;
  UMyMod.AbreQuery(QrEmails, 'TFmEmailBloqueto.QrUnidCondBeforeOpen()');
  }
end;

{
function TFmEmailBloqueto.GetPriority(Index: Integer): TIdMessagePriority;
begin
  case Index of
    0: Result := mpHighest;
    1: Result := mpHigh;
    3: Result := mpLow;
    4: Result := mpLowest;
  else
    Result := mpNormal;
  end;
end;
}

{
function TFmEmailBloqueto.SendEMail(aMessage: TIdMessage; Server: String): Boolean;
begin
  Result := False;
  try
    IdSMTP1.Host := Server;
    if (IdSMTP1.Host = '') or (IdSMTP1.Username = '') or (IdSMTP1.Password = '') then
      raise Exception.Create('IdSMTP1 com dados incompletos...');
    try
      IdSMTP1.Connect;
    except
      ShowMessage('Erro na Conex�o...');
    end;
    IdSMTP1.Send(aMessage);
    Result := True;
  finally
    IdSMTP1.Disconnect;
  end;
end;
}

function TFmEmailBloqueto.SubstituiVariaveis(Texto: String): String;
var
  LinkConfirma, LinkRecebeu, ID_Protocolo, ComoConfL, ComoConfT: String;
begin
  Result := Texto;
  ID_Protocolo := FormatFloat('0', QrUnidCondApto.Value) + '#' +
                  FormatFloat('0', QrUnidCondProtoco.Value) + '#' +
                  FormatFloat('0', QrUnidCondBloquet.Value) + '#' +
                  FormatFloat('0', QrUnidCondIDEmeio.Value);
  Randomize;
  ID_Protocolo := dmkPF.PWDExEncode(ID_Protocolo, FRandStrWeb01);
  ComoConfL    := Geral.FF0(UnProtocolo.IntOfComoConf(ptkWebLink));
  ComoConfT    := Geral.FF0(UnProtocolo.IntOfComoConf(ptkWebAuto));
  LinkConfirma := 'http://syndinet.dermatek.net.br/' +
                    DModG.QrOpcoesGerl.FieldByName('WebId').AsString +
                    '.php?page=recebibloq&protocolo=' + ID_Protocolo +
                    '&comoconf=' + DModG.MD5_por_SQL(ComoConfL);
  LinkRecebeu  := 'http://www.dermatek.net.br/complementos/com_emailconfrecebi.php?idweb=syndinet&nomeweb=' +
                    DModG.QrOpcoesGerl.FieldByName('WebId').AsString +
                    '&protocolo=' + ID_Protocolo + '&comoconf=' + DModG.MD5_por_SQL(ComoConfT);
  //
  Result := Geral.Substitui(Result, '[Administradora]', DModG.ObtemNomeFantasiaEmpresa());
  Result := Geral.Substitui(Result, '[Nome]'          , QrUnidCondRecipNome.Value);
  Result := Geral.Substitui(Result, '[Saudacao]'      , DModG.QrPreEmailSaudacao.Value);
  Result := Geral.Substitui(Result, '[Pronome]'       , QrUnidCondRecipProno.Value);
  Result := Geral.Substitui(Result, '[Condominio]'    , QrUnidCondCondominio.Value);
  Result := Geral.Substitui(Result, '[Valor]'         , Geral.FFT(QrUnidCondValor.Value, 2, siPositivo));
  Result := Geral.Substitui(Result, '[Vencimento]'    , Geral.FDT(QrUnidCondVencimento.Value, 2));
  Result := Geral.Substitui(Result, '[LinkConfirma]'  , LinkConfirma);
  Result := Geral.Substitui(Result, '[LinkRecebeu]'   , LinkRecebeu);
  Result := Geral.Substitui(Result, '[UH]'            , QrUnidCondUnidade.Value);
  // Parei Aqui
  //[LinkConfirma]
  //mudar status
end;

function TFmEmailBloqueto.HaNaoConformidades(): Boolean;
begin
  Result := False;
  //
  QrNCs.Close;
  QrNCs.SQL.Clear;
  QrNCs.SQL.Add('SELECT COUNT(*) Itens');
  QrNCs.SQL.Add('FROM unidcond');
  QrNCs.SQL.Add('WHERE Selecio=1');
  QrNCs.SQL.Add('AND Protoco=0');
  QrNCs.Open;
  if QrNCsItens.Value > 0 then
  begin
    Geral.MensagemBox('Envio de e-mails cancelado! Existem ' +
    IntToStr(QrNCsItens.Value) + ' itens selecionados sem n�mero de ' +
    'protocolo!', 'Aviso', MB_OK+MB_ICONWARNING);
    Result := True;
    Exit;
  end;
  //
  QrNCs.Close;
  QrNCs.SQL.Clear;
  QrNCs.SQL.Add('SELECT COUNT(*) Itens');
  QrNCs.SQL.Add('FROM unidcond');
  QrNCs.SQL.Add('WHERE Selecio=1');
  QrNCs.SQL.Add('AND PreEmeio=0');
  QrNCs.Open;
  if QrNCsItens.Value > 0 then
  begin
    Geral.MensagemBox('Envio de e-mails cancelado! Existem ' +
    IntToStr(QrNCsItens.Value) + ' itens selecionados sem pr�-emeio ' +
    'cadastrado no seu protocolo!', 'Aviso', MB_OK+MB_ICONWARNING);
    Result := True;
    Exit;
  end;
  //
  QrNCs.Close;
  QrNCs.SQL.Clear;
  QrNCs.SQL.Add('SELECT COUNT(*) Itens');
  QrNCs.SQL.Add('FROM unidcond');
  QrNCs.SQL.Add('WHERE Selecio=1');
  QrNCs.SQL.Add('AND RecipEmeio=""');
  QrNCs.Open;
  if QrNCsItens.Value > 0 then
  begin
    Geral.MensagemBox('Envio de e-mails cancelado! Existem ' +
    IntToStr(QrNCsItens.Value) + ' itens selecionados sem e-mail destinat�rio!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Result := True;
    Exit;
  end;
  //
  QrNCs.Close;
  QrNCs.SQL.Clear;
  QrNCs.SQL.Add('SELECT COUNT(*) Itens');
  QrNCs.SQL.Add('FROM unidcond');
  QrNCs.SQL.Add('WHERE Selecio=1');
  QrNCs.Open;
  if QrNCsItens.Value = 0 then
  begin
    Geral.MensagemBox('Envio de e-mails cancelado! N�o h� ' +
    'itens selecionados!', 'Aviso', MB_OK+MB_ICONWARNING);
    Result := True;
    Exit;
  end;
end;

procedure TFmEmailBloqueto.QrUnidCondAfterScroll(DataSet: TDataSet);
begin
  if (DModG.QrPreEmail.State <> dsInactive) and (DModG.QrPreEmail.RecordCount > 0) then
  begin
    if DModG.QrPreEmailCodigo.Value = QrUnidCondPreEmeio.Value then
      Exit;
  end;
  DModG.ReopenPreEmail(QrUnidCondPreEmeio.Value);
end;


function TFmEmailBloqueto.EnviaEmeio(BloqPath: String; AnexaBlq: boolean): Boolean;

  procedure AnexaBloqueto();//(BloqPath: String);
  begin
    with TIdAttachmentFile.Create(FIdMessage.MessageParts, BloqPath) do
    begin
      ContentType        := 'application/pdf';
      ContentDisposition := 'attachment';
      ContentTransfer    := 'base64';
    end;
  end;

  //

  procedure AnexaTextoPlano;
  var
    Texto: String;
  begin
    if DModG.QrPEM_0.RecordCount > 0 then
    begin
      Texto := '';
      DModG.QrPEM_0.First;
      while not DModG.QrPEM_0.Eof do
      begin
        Texto := Texto + #13+#10 + DModG.QrPEM_0Texto.Value;
        DModG.QrPEM_0.Next;
      end;
    // � obrigat�rio mandar um texto plano
    end else Texto := 'E-mail sem texto plano!!!!';
    with TIdText.Create(FIdMessage.MessageParts) do
    begin
      Body.Text       := Texto;
      ContentTransfer := '7bit';
      ContentType     := 'text/plain';
      ParentPart      := 1;
    end;
  end;

  //

  procedure AnexaHTMLs;
  var
    TextoHTML: TIdText;
  begin
    if DModG.QrPEM_1.RecordCount > 0 then
    begin
      DModG.QrPEM_1.First;
      TextoHTML := TIdText.Create(FIdMessage.MessageParts);
      with TextoHTML do
      begin
        ContentTransfer := '7bit';
        ContentType     := 'text/html';
        ParentPart      := 1;
        Body.Text       := '';
        while not DModG.QrPEM_1.Eof do
        begin
          Body.Text := Body.Text +'<br/>' + SubstituiVariaveis(DModG.QrPEM_1Texto.Value);
          DModG.QrPEM_1.Next;
        end;
      end;
    end;
  end;

  //

  procedure AnexaImagensCorpo;
  var
    ImgPath, TxtPath: String;
    ArqExiste: Boolean;
    Comando: PChar;
  begin
    if DModG.QrPEM_2.RecordCount > 0 then
    begin
      DModG.QrPEM_2.First;
      while not DModG.QrPEM_2.Eof do
      begin
        ImgPath := FIniPath + FormatFloat('00000000', DModG.QrPEM_2Controle.Value) +
        '\' + DModG.QrPEM_2Descricao.Value;
        TxtPath := dmkPF.MudaExtensaoDeArquivo(ImgPath, 'txt');

        //
        ArqExiste := FileExists(ImgPath);
        if not ArqExiste or CkRecriarImagens.Checked then
        begin
          if FileExists(FBase64) then
          begin
            //if FileExist(TxtPath) then
              //DeleteFile(TxtPath);
            Geral.SalvaTextoEmArquivo(TxtPath, DModG.QrPEM_2Texto.Value, True);
            Comando := PChar(FBase64 + ' -d ' + TxtPath + ' ' + ImgPath);
            //"C:\base64 -e bg2.jpg bg2.txt"
            dmkPF.WinExecAndWaitOrNot(Comando, SW_SHOW, MeEncode, True);
            ArqExiste := FileExists(ImgPath);
          end else Geral.MensagemBox('O aplicativo "' + FBase64 +
          '" n�o existe para criar a imagem "' + ImgPath + '".', 'Aviso',
          MB_OK+MB_ICONWARNING);
        end;
        if ArqExiste then
        begin
          with TIdAttachmentFile.Create(FIdMessage.MessageParts, ImgPath) do
          begin
            ContentType        := 'image/jpg';//AcharContentType(ExtractFileExt(ImgPath));
            ContentDisposition := 'inline';
            ContentTransfer    := 'base64';
            ContentID          := '<' + DModG.QrPEM_2CidID_Img.Value + '>';
            ParentPart         := 0;
          end;
        end;
        DModG.QrPEM_2.Next;
      end;
    end;
  end;

  //

var
  //TextoHTML: TIdText;
  //PlainMsg, HTML_Msg,
  ImageMsg, AnexoArq: Boolean;
  //
  //Texto, Temp, ImgPath, TxtPath: String;
  //ArqExiste: Boolean;
  //Comando: PChar;
begin
  try
    Screen.Cursor      := crHourGlass;
    Result := False;
    PB1.Position := PB1.Position + 1;
    STStatus.Caption := '  Enviando o bloqueto "' + BloqPath +'"...';
    Update;
    Application.ProcessMessages;
    //
    if QrUnidCondPreEmeio.Value = 0 then
    begin
      Geral.MensagemBox('Envio cancelado! Nenhum pr�-emeio foi selecionado"',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //
    if IdSMTP1.Connected then
      IdSMTP1.Disconnect(True);
    IdSMTP1.Host     := DModG.QrPreEmailSMTPServer.Value;
    IdSMTP1.Username := DModG.QrPreEmailLogi_Name.Value;
    IdSMTP1.Password := DModG.QrPreEmailLogi_Pass.Value;
    //
    if DModG.QrPreEmailPorta_mail.Value > 0 then
      IdSMTP1.Port := DModG.QrPreEmailPorta_mail.Value
    else
      IdSMTP1.Port := 25;
    //
    if DModG.QrPreEmailLogi_SSL.Value = 1 then
    begin
      (*
      if not FileExists(ExtractFilePath(Application.ExeName) + 'libeay32.dll') then
      begin
        Geral.MB_Info('Falha ao enviar e-mail!' + #13#10 +
          'Motivo: N�o foi localizada a DLL libeay32.dll!');
        Exit;
      end;
      if not FileExists(ExtractFilePath(Application.ExeName) + 'ssleay32.dll') then
      begin
        Geral.MB_Info('Falha ao enviar e-mail!' + #13#10 +
          'Motivo: N�o foi localizada a DLL ssleay32.dll!');
        Exit;
      end;
      *)
      IdSMTP1.IOHandler := IdSSL1;
      IdSMTP1.UseTLS    := utUseExplicitTLS;
      //
      IdSSL1.SSLOptions.Mode   := sslmClient;
      IdSSL1.SSLOptions.Method := sslvTLSv1;
    end else
    begin
      IdSMTP1.IOHandler := nil;
      IdSMTP1.UseTLS    := utNoTLSSupport;
    end;
    //
    if (IdSMTP1.Host = '') or (IdSMTP1.Username = '') or (IdSMTP1.Password = '') then
      raise Exception.Create('SMTP com dados incompletos...');

    try
      IdSMTP1.Connect;
    except
      ShowMessage('Erro na Conex�o...');
    end;

    if IdSMTP1.Connected then
    begin
      //PlainMsg := DModG.QrPEM_0.RecordCount > 0;
      //HTML_Msg := DModG.QrPEM_1.RecordCount > 0;
      ImageMsg := DModG.QrPEM_2.RecordCount > 0;
      AnexoArq := FileExists(BloqPath) and AnexaBlq;
      //
      {Set up the message...}
      FIdMessage              := TIdMessage.Create(nil);
      FIdMessage.Subject      := SubstituiVariaveis(DModG.QrPreEmailMail_Titu.Value);
      FIdMessage.From.Address := DModG.QrPreEmailSend_Mail.Value;//edSenderEMail.Text;
      FIdMessage.From.Name    := DModG.QrPreEmailSend_Name.Value;//edSenderName.Text;
      FIdMessage.Encoding     := meMIME;
      FIdMessage.CharSet      := 'iso-8859-1';
      with FIdMessage.Recipients.Add do
      begin
        Name := QrUnidCondRecipNome.Value;//edRecipientName.Text;
        Address := QrUnidCondRecipEmeio.Value;//edRecipientEMail.Text;
      end;

      if not ImageMsg and not AnexoArq then
      begin
        // S� e texto puro e em HMTL
        if AnexaBlq then
          Geral.MensagemBox('N�o h� bloqueto para anexar � mensagem!',
          'Aviso', MB_OK+MB_ICONWARNING);
        FIdMessage.ContentType := 'multipart/alternative';
        //AnexaTextoPlano; n�o colocar!! Erro quando n�o tem bloqueto anexado
        AnexaHTMLs;
        // Final s� e texto puro e em HMTL
      end
      else if AnexoArq and ImageMsg then
      begin
        // Texto puro, HTML, Figura Inline e Anexo(s)
        FIdMessage.ContentType := 'multipart/mixed';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/related';
        end;

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
          ParentPart  := 0;
        end;

        AnexaTextoPlano;
        AnexaHTMLs;
        AnexaImagensCorpo;
        if AnexaBlq then
          AnexaBloqueto;
        // Final Texto puro, HTML, Figura Inline e Anexo(s)
      end
      else if not AnexoArq and ImageMsg then
      begin
        // Texto puro, HTML e a figura Inline
        FIdMessage.ContentType := 'multipart/related';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
        end;

        AnexaTextoPlano;
        AnexaHTMLs;
        AnexaImagensCorpo
        // Final texto puro, HTML e a figura Inline
      end
      else if AnexoArq and not ImageMsg then
      begin
        // Texto puro, HTML e anexos
        FIdMessage.ContentType := 'multipart/mixed';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/related';
        end;

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
          ParentPart  := 0;
        end;


        AnexaTextoPlano;
        AnexaHTMLs;
        if AnexaBlq then
          AnexaBloqueto;
        // Final texto puro, HTML e anexos
      end;
      try
        IdSMTP1.Send(FIdMessage);
        Result := True;
      except
        Geral.MensagemBox('Erro no envio do Email descrito no status',
          'Aviso', MB_OK+MB_ICONERROR);
      end;
    end;
    UMail.SalvaEmailNosItensEnviados(DModG.QrPreEmailEmailConta.Value,
      DModG.QrAux, Dmod.MyDB, IMAP, IdSSL1, FIdMessage);
  finally
    IdSMTP1.Disconnect;
    FIdMessage.Free;
    Screen.Cursor     := crDefault;
  end;
end;

(*
function TFmEmailBloqueto.EnviaEmeio4(BloqPath: String): Boolean;
  procedure AnexaBloqueto;//(BloqPath: String);
  begin
    with TIdAttachmentFile.Create(FIdMessage.MessageParts, BloqPath) do
    begin
      ContentType        := 'application/pdf';
      ContentDisposition := 'attachment';
      ContentTransfer    := 'base64';
    end;
  end;
var
  i: Integer;
  TextoHTML: TIdText;
  PlainMsg, HTML_Msg, ImageMsg, AnexoArq: Boolean;
  //
  Texto, Temp, ImgPath, TxtPath: String;
  ArqExiste: Boolean;
  Comando: PChar;
begin
  try
    Screen.Cursor      := crHourGlass;
    PB1.Position := PB1.Position + 1;
    STStatus.Caption := '  Enviando o bloqueto "' + BloqPath +'"...';
    Update;
    Application.ProcessMessages;
    //
    if QrUnidCondPreEmeio.Value = 0 then
    begin
      Geral.MensagemBox('Envio cancelado! Nenhum pr�-emeio foi selecionado"',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //

    IdSMTP1.Disconnect(True);
    IdSMTP1.Host     := DModG.QrPreEmailSMTPServer.Value;
    IdSMTP1.Username := DModG.QrPreEmailLogi_Name.Value;
    IdSMTP1.Password := DModG.QrPreEmailLogi_Pass.Value;;
    IdSMTP1.Port     := 25;//StrToInt(edtPorta.Text);

    if (IdSMTP1.Host = '') or (IdSMTP1.Username = '') or (IdSMTP1.Password = '') then
      raise Exception.Create('SMTP com dados incompletos...');

    try
      IdSMTP1.Connect;
    except
      ShowMessage('Erro na Conex�o...');
    end;

    if IdSMTP1.Connected then
    begin
      PlainMsg := DModG.QrPEM_0.RecordCount > 0;
      HTML_Msg := DModG.QrPEM_1.RecordCount > 0;
      ImageMsg := DModG.QrPEM_2.RecordCount > 0;
      AnexoArq := FileExists(BloqPath);
      //
      {Set up the message...}
      FIdMessage              := TIdMessage.Create(nil);
      FIdMessage.Subject      := DModG.QrPreEmailMail_Titu.Value;
      FIdMessage.From.Address := DModG.QrPreEmailSend_Mail.Value;//edSenderEMail.Text;
      FIdMessage.From.Name    := DModG.QrPreEmailSend_Name.Value;//edSenderName.Text;
      FIdMessage.Encoding     := meMIME;
      FIdMessage.CharSet      := 'iso-8859-1';
      with FIdMessage.Recipients.Add do
      begin
        Name := QrUnidCondRecipNome.Value;//edRecipientName.Text;
        Address := QrUnidCondRecipEmeio.Value;//edRecipientEMail.Text;
      end;

      {
      if (lbFiguraInline.Items.Count = 0) and (lbAnexos.Items.Count = 0) then
      begin
        // S� e texto puro e em HMTL
        FIdMessage.ContentType := 'multipart/alternative';
        with TIdText.Create(FIdMessage.MessageParts) do
        begin
          Body.Text       := 'E-mail sem texto plano!!!!';
          ContentTransfer := '7bit';
          ContentType     := 'text/plain';
        end;

        TextoHTML := TIdText.Create(FIdMessage.MessageParts);
        with TextoHTML do
        begin
          Body.LoadFromFile('emailativacao.html');
          ContentTransfer := '7bit';
          ContentType     := 'text/html';
        end;
        // Final s� e texto puro e em HMTL
      end;
      }
      //if ((lbAnexos.Items.Count > 0) and (lbFiguraInline.Items.Count > 0)) then
      if AnexoArq and ImageMsg then
      begin
        // Texto puro, HTML, Figura Inline e Anexo(s)
        FIdMessage.ContentType := 'multipart/mixed';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/related';
        end;

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
          ParentPart  := 0;
        end;

        with TIdText.Create(FIdMessage.MessageParts) do
        begin
          Body.Text       := 'E-mail sem texto plano!!!!';
          ContentTransfer := '7bit';
          ContentType     := 'text/plain';
          ParentPart      := 1;
        end;

        if DModG.QrPEM_1.RecordCount > 0 then
        begin
          DModG.QrPEM_1.First;
          TextoHTML := TIdText.Create(FIdMessage.MessageParts);
          with TextoHTML do
          begin
            ContentTransfer := '7bit';
            ContentType     := 'text/html';
            ParentPart      := 1;
            Body.Text       := '';
            while not DModG.QrPEM_1.Eof do
            begin
              Body.Text := Body.Text + SubstituiVariaveis(DModG.QrPEM_1Texto.Value);
              DModG.QrPEM_1.Next;
            end;
          end;
        end;
        if DModG.QrPEM_2.RecordCount > 0 then
        begin
          DModG.QrPEM_2.First;
          while not DModG.QrPEM_2.Eof do
          begin
            ImgPath := FIniPath + FormatFloat('00000000', DModG.QrPEM_2Controle.Value) +
            '\' + DModG.QrPEM_2Descricao.Value;
            TxtPath := MLAGeral.MudaExtensaoDeArquivo(ImgPath, 'txt');

            //
            ArqExiste := FileExists(ImgPath);
            if not ArqExiste or CkRecriarImagens.Checked then
            begin
              if FileExists(FBase64) then
              begin
                //if FileExist(TxtPath) then
                  //DeleteFile(TxtPath);
                Geral.SalvaTextoEmArquivo(TxtPath, DModG.QrPEM_2Texto.Value, True);
                Comando := PChar(FBase64 + ' -d ' + TxtPath + ' ' + ImgPath);
                //"C:\base64 -e bg2.jpg bg2.txt"
                dmkPF.WinExecAndWaitOrNot(Comando, SW_SHOW, MeEncode, True);
                ArqExiste := FileExists(ImgPath);
              end else Geral.MensagemBox('O aplicativo "' + FBase64 +
              '" n�o existe para criar a imagem "' + ImgPath + '".'), 'Aviso',
              MB_OK+MB_ICONWARNING);
            end;
            if ArqExiste then
            begin
              with TIdAttachmentFile.Create(FIdMessage.MessageParts, ImgPath) do
              begin
                ContentType        := 'image/jpg';//AcharContentType(ExtractFileExt(ImgPath));
                ContentDisposition := 'inline';
                ContentTransfer    := 'base64';
                ContentID          := '<' + DModG.QrPEM_2CidID_Img.Value + '>';
                ParentPart         := 0;
              end;
            end;
            DModG.QrPEM_2.Next;
          end;
        end;
        AnexaBloqueto;
        // Final Texto puro, HTML, Figura Inline e Anexo(s)
      end;

      {
      if (lbFiguraInline.Items.Count > 0) and (lbAnexos.Items.Count = 0) then
      begin
        // Texto puro, HTML e a figura Inline
        FIdMessage.ContentType := 'multipart/related';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
        end;

        with TIdText.Create(FIdMessage.MessageParts) do
        begin
          Body.Text       := 'E-mail sem texto plano!!!!';
          ContentTransfer := '7bit';
          ContentType     := 'text/plain';
          ParentPart      := 0;
        end;
        TextoHTML := TIdText.Create(FIdMessage.MessageParts);
        with TextoHTML do
        begin
          Body.LoadFromFile(edArqHTML.Text);
          ContentTransfer := '7bit';
          ContentType     := 'text/html';
          ParentPart      := 0;
        end;

        for i := 0 to lbFiguraInline.Items.Count - 1 do
        begin
          with TIdAttachmentFile.Create(FIdMessage.MessageParts, lbFiguraInline.Items[i]) do
          begin
            ContentType        := AcharContentType(ExtractFileExt(lbFiguraInline.Items[i]));
            ContentDisposition := 'inline';
            ContentTransfer    := 'base64';
            ContentID          := '<123456789>';
            ParentPart         := 0;
          end;
        end;

        // Final texto puro, HTML e a figura Inline
      end;
      }
      try
        IdSMTP1.Send(FIdMessage);
      except
        Geral.MensagemBox('Erro no envio do Email...',
          'Aviso', MB_OK+MB_ICONERROR);
      end;
    end;
  finally
    IdSMTP1.Disconnect;
    FIdMessage.Free;
    Screen.Cursor     := crDefault;
    //btnEnviar.Enabled := True;
  end;
end;
*)

end.

