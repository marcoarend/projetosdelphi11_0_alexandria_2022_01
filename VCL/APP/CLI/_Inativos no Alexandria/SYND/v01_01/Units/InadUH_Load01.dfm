object FmInadUH_Load01: TFmInadUH_Load01
  Left = 339
  Top = 185
  Caption = 'XLS-INADI-001 :: Importa'#231#227'o de Lan'#231'amentos - Modelo 01 (Boletos)'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 614
        Height = 32
        Caption = 'Importa'#231#227'o de Lan'#231'amentos - Modelo 01 (Boletos)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 614
        Height = 32
        Caption = 'Importa'#231#227'o de Lan'#231'amentos - Modelo 01 (Boletos)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 614
        Height = 32
        Caption = 'Importa'#231#227'o de Lan'#231'amentos - Modelo 01 (Boletos)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 150
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label27: TLabel
            Left = 8
            Top = 7
            Width = 116
            Height = 13
            Caption = 'Arquivo a ser carregado:'
          end
          object SpeedButton8: TSpeedButton
            Left = 974
            Top = 23
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton8Click
          end
          object LaAviso: TLabel
            Left = 12
            Top = 48
            Width = 15
            Height = 22
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object dmkLabel1: TdmkLabel
            Left = 12
            Top = 96
            Width = 149
            Height = 13
            Caption = 'N'#176' conta quota condominial (1):'
            UpdType = utYes
            SQLType = stNil
          end
          object dmkLabel2: TdmkLabel
            Left = 12
            Top = 124
            Width = 166
            Height = 13
            Caption = 'N'#176' conta acordo de atrasados (97):'
            UpdType = utYes
            SQLType = stNil
          end
          object dmkLabel3: TdmkLabel
            Left = 292
            Top = 96
            Width = 60
            Height = 13
            Caption = 'Condom'#237'nio:'
            Enabled = False
            UpdType = utYes
            SQLType = stNil
          end
          object dmkLabel4: TdmkLabel
            Left = 292
            Top = 124
            Width = 39
            Height = 13
            Caption = 'Carteira:'
            Enabled = False
            UpdType = utYes
            SQLType = stNil
          end
          object EdArq: TdmkEdit
            Left = 8
            Top = 23
            Width = 965
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'DirNFeGer'
            UpdCampo = 'DirNFeGer'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object PB1: TProgressBar
            Left = 12
            Top = 72
            Width = 985
            Height = 17
            TabOrder = 1
          end
          object EdContaTaxa: TdmkEdit
            Left = 184
            Top = 92
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdContaAcordo: TdmkEdit
            Left = 184
            Top = 120
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCondCod: TdmkEdit
            Left = 360
            Top = 92
            Width = 56
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCartCod: TdmkEdit
            Left = 360
            Top = 120
            Width = 56
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdCartCodChange
          end
          object EdCondNome: TdmkEdit
            Left = 416
            Top = 92
            Width = 580
            Height = 21
            ReadOnly = True
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdCartNome: TdmkEdit
            Left = 416
            Top = 120
            Width = 581
            Height = 21
            ReadOnly = True
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 165
          Width = 1004
          Height = 300
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 1
          object TabSheet1: TTabSheet
            Caption = ' Abertos '
            object Grade1: TStringGrid
              Left = 0
              Top = 0
              Width = 996
              Height = 272
              Align = alClient
              ColCount = 2
              DefaultColWidth = 44
              DefaultRowHeight = 18
              RowCount = 2
              TabOrder = 0
            end
          end
          object TabSheet2: TTabSheet
            Caption = ' Carregados '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object StringGrid1: TStringGrid
              Left = 784
              Top = 0
              Width = 212
              Height = 272
              Align = alRight
              ColCount = 3
              DefaultColWidth = 44
              DefaultRowHeight = 18
              RowCount = 2
              TabOrder = 0
              ColWidths = (
                44
                25
                113)
            end
            object Memo1: TMemo
              Left = 0
              Top = 0
              Width = 784
              Height = 272
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ScrollBars = ssBoth
              TabOrder = 1
              WordWrap = False
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Configura'#231#245'es'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 337
              Height = 272
              Align = alLeft
              TabOrder = 0
              object Label1: TLabel
                Left = 1
                Top = 1
                Width = 335
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Exce'#231#245'es:'
                ExplicitWidth = 50
              end
              object MeExcessoes: TMemo
                Left = 1
                Top = 14
                Width = 335
                Height = 257
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Courier New'
                Font.Style = []
                Lines.Strings = (
                  'CONDOMINIO VALE DOS SONHOS'
                  'CONTAS PROCESSADAS POR COBRAN'#199'A'
                  'Usu'#225'rio:'
                  'Per'#237'odo:'
                  'Unidade/Locat'#225'rio/Classe'
                  'Total:'
                  'Total Geral:')
                ParentFont = False
                ScrollBars = ssBoth
                TabOrder = 0
                WordWrap = False
              end
            end
            object Panel7: TPanel
              Left = 337
              Top = 0
              Width = 659
              Height = 272
              Align = alClient
              TabOrder = 1
              object Label2: TLabel
                Left = 1
                Top = 1
                Width = 98
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Linhas de exce'#231#245'es:'
              end
              object MeNaoCopiados: TMemo
                Left = 1
                Top = 14
                Width = 657
                Height = 257
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                ScrollBars = ssBoth
                TabOrder = 0
                WordWrap = False
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'Tabela reconstru'#237'da'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeOK: TStringGrid
              Left = 0
              Top = 0
              Width = 996
              Height = 272
              Align = alClient
              ColCount = 2
              DefaultColWidth = 44
              DefaultRowHeight = 18
              RowCount = 2
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAbre: TBitBtn
        Left = 16
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Abre xls'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAbreClick
      end
      object BtCarrega: TBitBtn
        Left = 142
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Carrega xls'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtCarregaClick
      end
    end
  end
  object QrImovel: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta DEPTO, Propriet '
      'FROM condimov'
      'WHERE Codigo=?'
      'AND Unidade="?"'
      '')
    Left = 228
    Top = 284
    object QrImovelDEPTO: TIntegerField
      FieldName = 'DEPTO'
    end
    object QrImovelPropriet: TIntegerField
      FieldName = 'Propriet'
    end
  end
  object QrCarteira: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo'
      'FROM carteiras'
      'WHERE Codigo=0')
    Left = 228
    Top = 332
    object QrCarteiraTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
end
