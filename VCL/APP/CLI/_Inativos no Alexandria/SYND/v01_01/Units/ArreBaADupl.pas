unit ArreBaADupl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, Grids, DBGrids,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmArreBaADupl = class(TForm)
    Panel1: TPanel;
    QrContas: TmySQLQuery;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DsContas: TDataSource;
    QrDuplic: TmySQLQuery;
    QrDuplicITENS: TLargeintField;
    QrDuplicConta: TIntegerField;
    QrDuplicValor: TFloatField;
    DsDuplic: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrDuplicNOMECONTA: TWideStringField;
    QrArreBaA: TmySQLQuery;
    DsArreBaA: TDataSource;
    QrArreBaASeq: TIntegerField;
    QrArreBaAConta: TIntegerField;
    QrArreBaAArreBaI: TIntegerField;
    QrArreBaAArreBaC: TIntegerField;
    QrArreBaAValor: TFloatField;
    QrArreBaATexto: TWideStringField;
    QrArreBaAAdiciona: TSmallintField;
    QrArreBaC: TmySQLQuery;
    DsArreBaC: TDataSource;
    QrArreBaANOME_ARREBAC: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrDuplicAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FContinua: Boolean;
  end;

  var
  FmArreBaADupl: TFmArreBaADupl;

implementation

{$R *.DFM}

uses Module, ModuleGeral, UnMyObjects;

procedure TFmArreBaADupl.BtOKClick(Sender: TObject);
begin
  FContinua := True;
  Close;
end;

procedure TFmArreBaADupl.BtSaidaClick(Sender: TObject);
begin
  FContinua := False;
  Close;
end;

procedure TFmArreBaADupl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmArreBaADupl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrDuplic.Database := DModG.MyPID_DB;
  QrArreBaA.Database := DModG.MyPID_DB;
  //
  FContinua := False;
  QrContas.Open;
  QrArreBaC.Open;
  QrDuplic.Open;
end;

procedure TFmArreBaADupl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmArreBaADupl.QrDuplicAfterScroll(DataSet: TDataSet);
begin
  QrArreBaA.Close;
  QrArreBaA.Params[00].AsInteger := QrDuplicConta.Value;
  QrArreBaA.Params[01].AsFloat   := QrDuplicValor.Value;
  QrArreBaA.Open;
end;


end.
