unit CondGri;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, Variants, dmkDBLookupComboBox, dmkEdit,
  dmkGeral, dmkEditCB, UnDmkProcFunc, dmkImage, UnDmkEnums, dmkCheckBox,
  DmkDAC_PF;

type
  TFmCondGri = class(TForm)
    PainelDados: TPanel;
    DsCondGri: TDataSource;
    QrCondGri: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    EdUsername: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    EdPassword: TdmkEdit;
    QrCondGriCodigo: TIntegerField;
    QrCondGriNome: TWideStringField;
    QrCondGriUsername: TWideStringField;
    QrCondGriPassword: TWideStringField;
    QrCondGriLk: TIntegerField;
    QrCondGriDataCad: TDateField;
    QrCondGriDataAlt: TDateField;
    QrCondGriUserCad: TIntegerField;
    QrCondGriUserAlt: TIntegerField;
    QrCondGriAlterWeb: TSmallintField;
    QrCondGriAtivo: TSmallintField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    PMGrupo: TPopupMenu;
    Crianovogrupo1: TMenuItem;
    Alteragrupoatual1: TMenuItem;
    PainelItens: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Label7: TLabel;
    EdDBCodigo: TDBEdit;
    EdDBNome: TDBEdit;
    Label8: TLabel;
    Panel10: TPanel;
    Label11: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label12: TLabel;
    EdPropriet: TdmkEditCB;
    Label13: TLabel;
    EdCondImov: TdmkEditCB;
    CBCondImov: TdmkDBLookupComboBox;
    CBPropriet: TdmkDBLookupComboBox;
    DsCondImov: TDataSource;
    QrCondImov: TmySQLQuery;
    QrCondImovConta: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    DsPropriet: TDataSource;
    QrPropriet: TmySQLQuery;
    QrProprietCodigo: TIntegerField;
    QrProprietNOMEPROP: TWideStringField;
    DsEntiCond: TDataSource;
    QrEntiCond: TmySQLQuery;
    QrEntiCondCodCond: TIntegerField;
    QrEntiCondCodEnti: TIntegerField;
    QrEntiCondNOMECOND: TWideStringField;
    PMUH: TPopupMenu;
    AdicionaUHaogrupo1: TMenuItem;
    RetiraUHselecionadadogrupo1: TMenuItem;
    QrCondGriImv: TmySQLQuery;
    DsCondGriImv: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrCondGriImvNOMEPROP: TWideStringField;
    QrCondGriImvNOMECOND: TWideStringField;
    QrCondGriImvUNIDADE: TWideStringField;
    QrCondGriImvCodigo: TIntegerField;
    QrCondGriImvControle: TIntegerField;
    QrCondGriImvApto: TIntegerField;
    QrCondGriImvPropr: TIntegerField;
    QrCondGriImvLk: TIntegerField;
    QrCondGriImvDataCad: TDateField;
    QrCondGriImvDataAlt: TDateField;
    QrCondGriImvUserCad: TIntegerField;
    QrCondGriImvUserAlt: TIntegerField;
    QrCondGriImvAlterWeb: TSmallintField;
    QrCondGriImvAtivo: TSmallintField;
    QrCondGriImvCond: TIntegerField;
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel11: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel12: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtGrupo: TBitBtn;
    BtUH: TBitBtn;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    CkAtivo: TdmkCheckBox;
    Label14: TLabel;
    EdEMail: TdmkEdit;
    QrCondGriEmail: TWideStringField;
    DBCheckBox2: TDBCheckBox;
    Label15: TLabel;
    DBEdit3: TDBEdit;
    CBPerfil: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    EdPerfil: TdmkEditCB;
    Label16: TLabel;
    DsWPerfil: TDataSource;
    QrWPerfil: TmySQLQuery;
    QrWPerfilCodigo: TIntegerField;
    QrWPerfilNome: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtGrupoClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCondGriAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCondGriBeforeOpen(DataSet: TDataSet);
    procedure Crianovogrupo1Click(Sender: TObject);
    procedure Alteragrupoatual1Click(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtUHClick(Sender: TObject);
    procedure AdicionaUHaogrupo1Click(Sender: TObject);
    procedure RetiraUHselecionadadogrupo1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdProprietChange(Sender: TObject);
    procedure CBCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrCondGriAfterScroll(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure PMGrupoPopup(Sender: TObject);
    procedure PMUHPopup(Sender: TObject);
    procedure EdProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenPropriet(Cond: Integer);
    procedure ReopenCondImov(Cond: Integer);
    procedure ReopenCondGriImv(Controle: Integer);
    procedure ReopenWPerfil(Query: TmySQLQuery; Tipo: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCondGri: TFmCondGri;
const
  FFormatFloat = '00000';

implementation

uses Module, ModuleGeral, UnMyObjects, UnDmkWeb, Principal, UnWUsersJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCondGri.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCondGri.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCondGriCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCondGri.DefParams;
begin
  VAR_GOTOTABELA := 'CondGri';
  VAR_GOTOMYSQLTABLE := QrCondGri;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT gri.*');
  VAR_SQLx.Add('FROM condgri gri');
  VAR_SQLx.Add('WHERE gri.Codigo <> 0');
  //
  VAR_SQL1.Add('AND gri.Codigo=:P0');
  //
  VAR_SQLa.Add('AND gri.Nome Like :P0');
  //
end;

procedure TFmCondGri.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PainelItens.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant   := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant     := '';
        EdUsername.ValueVariant := '';
        EdPassword.ValueVariant := '';
        EdEMail.ValueVariant    := '';
        CkAtivo.Checked         := True;
      end else begin
        EdCodigo.ValueVariant   := DBEdCodigo.Text;
        EdNome.ValueVariant     := DBEdNome.Text;
        EdUsername.ValueVariant := QrCondGriUsername.Value;
        EdPassword.ValueVariant := QrCondGriPassword.Value;
        EdEMail.ValueVariant    := QrCondGriEmail.Value;
        CkAtivo.Checked         := Geral.IntToBool(QrCondGriAtivo.Value);
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      PainelItens.Visible := True;
      PainelDados.Visible := False;
      //
      EdEmpresa.ValueVariant := 0;
      CBEmpresa.KeyValue     := Null;
      //
      EdPropriet.ValueVariant := 0;
      CBPropriet.KeyValue     := Null;
      //
      EdCondImov.ValueVariant := 0;
      CBCondImov.KeyValue     := Null;
      //
      EdEmpresa.SetFocus;
    end;
    else
      Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!', 'Aviso',
        MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCondGri.PMGrupoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrCondGri.State <> dsInactive) and (QrCondGri.RecordCount > 0);
  //
  Alteragrupoatual1.Enabled := Enab;
end;

procedure TFmCondGri.PMUHPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrCondGri.State <> dsInactive) and (QrCondGri.RecordCount > 0);
  Enab2 := (QrCondGriImv.State <> dsInactive) and (QrCondGriImv.RecordCount > 0);
  //
  AdicionaUHaogrupo1.Enabled          := Enab;
  RetiraUHselecionadadogrupo1.Enabled := Enab and Enab2;
end;

procedure TFmCondGri.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCondGri.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCondGri.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCondGri.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCondGri.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCondGri.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCondGri.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCondGri.SpeedButton5Click(Sender: TObject);
var
  Perfil: Integer;
begin
  VAR_CADASTRO := 0;
  Perfil       := EdPerfil.ValueVariant;
  //
  UWUsersJan.MostraWPerfis(Perfil);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPerfil, CBPerfil, QrWPerfil, VAR_CADASTRO);
    //
    EdPerfil.SetFocus;
  end;
end;

procedure TFmCondGri.BtGrupoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGrupo, BtGrupo);
end;

procedure TFmCondGri.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCondGriCodigo.Value;
  Close;
end;

procedure TFmCondGri.BtConfirmaClick(Sender: TObject);
var
  Codigo, Perfil, Ativo, Tip, Cod, TipoUsr, IDUsr: Integer;
  Nome, Usuario, Senha, Email: String;
begin
  Nome    := EdNome.ValueVariant;
  Usuario := EdUsername.ValueVariant;
  Senha   := EdPassword.ValueVariant;
  Email   := EdEMail.ValueVariant;
  Perfil  := EdPerfil.ValueVariant;
  Ativo   := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  //
  if Usuario <> '' then
  begin
    if (Dmod.ValidaUsuarioWeb(Usuario, Tip, Cod) = False) then
    begin
      if (QrCondGriCodigo.Value <> Cod) and (Tip <> 3) then
      begin
        Geral.MB_Aviso('Este usu�rio n�o est� dispon�vel!');
        EdUsername.SetFocus;
        Exit;
      end;
    end;
    if MyObjects.FIC(Senha = '', EdPassword, 'Defina uma senha!') then Exit;
    if MyObjects.FIC(Perfil = 0, EdPerfil, 'Perfil de senha de grupo n�o definida!') then Exit;
  end;
  //
  Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'CondGri', 'Codigo',
              [], [], ImgTipo.SQLType, QrCondGriCodigo.Value, siPositivo, nil);

  //
  if ImgTipo.SQLType = stIns then
  begin
    if Dmod.LocalizaUsuarioWeb(Dmod.QrAux, Dmod.MyDB, Usuario, TipoUsr, IDUsr) then
    begin
      Geral.MB_Aviso('Este usu�rio est� indispon�vel!');
      EdUsername.SetFocus;
      Exit;
    end;
  end else
  begin
    if Dmod.LocalizaUsuarioWeb(Dmod.QrAux, Dmod.MyDB, Usuario, TipoUsr, IDUsr) then
    begin
      if ((IDUsr <> Codigo) or ((IDUsr <> Codigo) and (TipoUsr <> 3))) then
      begin
        Geral.MB_Aviso('Este usu�rio est� indispon�vel!');
        EdUsername.SetFocus;
        Exit;
      end;
    end;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'CondGri', False,
    ['Nome', 'Username', 'Password', 'Email', 'Perfil', 'Ativo'], ['Codigo'],
    [Nome, Usuario, Senha, Email, Perfil, Ativo], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CondGri', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmCondGri.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CondGri', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CondGri', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CondGri', 'Codigo');
end;

procedure TFmCondGri.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  dmkDBGrid1.Align  := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrEntiCond, DMod.MyDB);
  UMyMod.AbreQuery(QrPropriet, DMod.MyDB);
  UMyMod.AbreQuery(QrCondImov, DMod.MyDB);
  //
  ReopenWPerfil(QrWPerfil, 3); //Grupo
end;

procedure TFmCondGri.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCondGriCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCondGri.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmCondGri.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCondGri.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmCondGri.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCondGri.QrCondGriAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCondGri.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGri.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCondGriCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CondGri', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCondGri.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGri.QrCondGriBeforeOpen(DataSet: TDataSet);
begin
  QrCondGriCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCondGri.Crianovogrupo1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmCondGri.Alteragrupoatual1Click(Sender: TObject);
begin
  MostraEdicao(1, stUpd, QrCondGriCodigo.Value);
end;

procedure TFmCondGri.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    ReopenPropriet(EdEmpresa.ValueVariant);
    ReopenCondImov(EdEmpresa.ValueVariant);
  end;
end;

procedure TFmCondGri.BtUHClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMUH, BtUH);
end;

procedure TFmCondGri.AdicionaUHaogrupo1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmCondGri.RetiraUHselecionadadogrupo1Click(Sender: TObject);
var
  Prox: Integer;
begin
  if Geral.MB_Pergunta('Confirma a retirada o item selecionado?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'DELETE FROM condgriimv ',
        'WHERE Controle=' + Geral.FF0(QrCondGriImvControle.Value),
        '']);
    finally
      Prox := UMyMod.ProximoRegistro(QrCondGriImv, 'Controle',
        QrCondGriImvControle.Value);
      ReopenCondGriImv(Prox);
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmCondGri.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmCondGri.BitBtn1Click(Sender: TObject);
var
  Propr, Cond, Apto, Codigo, Controle: Integer;
begin
  Codigo := QrCondGriCodigo.Value;
  Cond   := EdEmpresa.ValueVariant;
  Propr  := EdPropriet.ValueVariant;
  Apto   := EdCondImov.ValueVariant;
  //
  if MyObjects.FIC(Codigo = 0, nil, 'Defina o grupo!') then Exit;
  //
  if (Cond = 0) and (Propr = 0) and (Apto = 0) then
  begin
    Geral.MB_Aviso('Defina pelo menos o Condom�nio ou ' +
      DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) +
      ' ou a unidade habitacional!');
    Exit;
  end;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('Condgriimv', 'Controle', stIns, 0);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'Condgriimv', False,
    ['Codigo', 'Cond', 'Propr', 'Apto'], ['Controle'],
    [Codigo, Cond, Propr, Apto], [Controle], True) then
  begin
    MostraEdicao(0, stLok, 0);
    ReopenCondGriImv(Controle);
  end;
end;

procedure TFmCondGri.ReopenPropriet(Cond: Integer);
begin
  EdPropriet.Text     := '';
  CBPropriet.KeyValue := NULL;
  //
  try
    Screen.Cursor := crHourGlass;
    //
    QrPropriet.Close;
    QrPropriet.SQL.Clear;
    QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
    QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
    QrPropriet.SQL.Add('FROM entidades ent');
    QrPropriet.SQL.Add('LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet');
    QrPropriet.SQL.Add('WHERE ent.Cliente2="V"');
    QrPropriet.SQL.Add('');
    if Cond <> 0 then
      QrPropriet.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
    QrPropriet.SQL.Add('');
    QrPropriet.SQL.Add('ORDER BY NOMEPROP');
    QrPropriet.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGri.ReopenWPerfil(Query: TmySQLQuery; Tipo: Integer);
var
  Tip: Integer;
begin
  Tip := FmPrincipal.ConverteWPerfilSyndicToWPerfil(Tipo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDBn, [
    'SELECT Codigo, Nome ',
    'FROM wperfis ',
    'WHERE Tipo =' + Geral.FF0(Tip),
    '']);
end;

procedure TFmCondGri.ReopenCondImov(Cond: Integer);
var
  Propriet: Integer;
begin
  EdCondImov.Text     := '';
  CBCondImov.KeyValue := NULL;
  //
  Screen.Cursor := crHourGlass;
  try
    Cond     := Geral.IMV(EdEmpresa.Text);
    Propriet := Geral.IMV(EdPropriet.Text);
    //
    QrCondImov.Close;
    QrCondImov.SQL.Clear;
    QrCondImov.SQL.Add('SELECT cim.Conta, cim.Unidade');
    QrCondImov.SQL.Add('FROM condimov cim');
    QrCondImov.SQL.Add('WHERE cim.Codigo<>0');
    QrCondImov.SQL.Add('AND cim.Propriet<>0');
    QrCondImov.SQL.Add('');
    if Cond <> 0 then
      QrCondImov.SQL.Add('AND cim.Codigo=' + Geral.FF0(Cond));
    if Propriet <> 0 then
      QrCondImov.SQL.Add('AND cim.Propriet=' + Geral.FF0(Propriet));
    QrCondImov.SQL.Add('');
    QrCondImov.SQL.Add('ORDER BY cim.Unidade');
    QrCondImov.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGri.EdProprietChange(Sender: TObject);
begin
  if EdPropriet.ValueVariant <> 0 then
    ReopenCondImov(EdEmpresa.ValueVariant);
end;

procedure TFmCondGri.EdProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    ReopenPropriet(0);
    ReopenCondImov(0);
  end;
end;

procedure TFmCondGri.CBCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_DELETE then
  begin
    EdCondImov.ValueVariant := 0;
    CBCondImov.KeyValue     := Null;
  end;
end;

procedure TFmCondGri.CBProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    ReopenPropriet(0);
    ReopenCondImov(0);
  end;
end;

procedure TFmCondGri.QrCondGriAfterScroll(DataSet: TDataSet);
begin
  ReopenCondGriImv(0);
end;

procedure TFmCondGri.ReopenCondGriImv(Controle: Integer);
begin
  QrCondGriImv.Close;
  QrCondGriImv.Params[0].AsInteger := QrCondGriCodigo.Value;
  QrCondGriImv.Open;
  //
  if Controle > 0 then
    QrCondGriImv.Locate('Controle', Controle, []);
end;

end.

