object FmCondImov: TFmCondImov
  Left = 339
  Top = 185
  ActiveControl = EdCodigo
  Caption = 'CAD-CONDO-004 :: Im'#243'veis'
  ClientHeight = 679
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 561
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label12: TLabel
      Left = 156
      Top = 4
      Width = 57
      Height = 13
      Caption = 'ID Unidade:'
    end
    object Label1: TLabel
      Left = 84
      Top = 5
      Width = 44
      Height = 13
      Caption = 'ID Bloco:'
    end
    object Label45: TLabel
      Left = 355
      Top = 6
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object Label61: TLabel
      Left = 283
      Top = 6
      Width = 43
      Height = 13
      Caption = 'Unidade:'
    end
    object Label62: TLabel
      Left = 230
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Andar:'
    end
    object SpeedButton7: TSpeedButton
      Left = 468
      Top = 20
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton7Click
    end
    object Label53: TLabel
      Left = 12
      Top = 89
      Width = 56
      Height = 13
      Caption = 'Propriet'#225'rio:'
    end
    object SpeedButton5: TSpeedButton
      Left = 468
      Top = 106
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton5Click
    end
    object SpeedButton6: TSpeedButton
      Left = 468
      Top = 150
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton6Click
    end
    object Label44: TLabel
      Left = 12
      Top = 133
      Width = 42
      Height = 13
      Caption = 'Inquilino:'
    end
    object Label2: TLabel
      Left = 12
      Top = 5
      Width = 60
      Height = 13
      Caption = 'Condom'#237'nio:'
    end
    object Label65: TLabel
      Left = 13
      Top = 177
      Width = 42
      Height = 13
      Caption = 'C'#244'njuge:'
    end
    object SpeedButton10: TSpeedButton
      Left = 469
      Top = 194
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton10Click
    end
    object Label203: TLabel
      Left = 508
      Top = 272
      Width = 55
      Height = 13
      Caption = 'Procurador:'
    end
    object Label32: TLabel
      Left = 12
      Top = 49
      Width = 69
      Height = 13
      Caption = 'Qtd Garagens:'
    end
    object Label190: TLabel
      Left = 88
      Top = 48
      Width = 57
      Height = 13
      Caption = 'Moradores*:'
    end
    object Label194: TLabel
      Left = 148
      Top = 48
      Width = 68
      Height = 13
      Caption = 'Fra'#231#227'o ideal *:'
    end
    object Label3: TLabel
      Left = 12
      Top = 364
      Width = 66
      Height = 13
      Caption = 'Observa'#231#245'es:'
    end
    object Label196: TLabel
      Left = 288
      Top = 544
      Width = 371
      Height = 13
      Caption = 
        '*: Utilizado somente para rateio de consumo! N'#227'o '#233' utilizado em ' +
        'arrecada'#231#245'es!'
    end
    object Label4: TLabel
      Left = 668
      Top = 544
      Width = 294
      Height = 13
      Caption = #185': Dias corridos ap'#243's vencimento do boleto (0  para n'#227'o emitir).'
    end
    object SpeedButton1: TSpeedButton
      Left = 972
      Top = 288
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object EdCodigo: TdmkEdit
      Left = 156
      Top = 20
      Width = 71
      Height = 21
      Alignment = taRightJustify
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Conta'
      UpdCampo = 'Conta'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object dmkDBEdCodigo: TdmkDBEdit
      Left = 84
      Top = 20
      Width = 70
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Controle'
      DataSource = FmCond.DsCondBloco
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object EdAndar: TdmkEdit
      Left = 230
      Top = 20
      Width = 49
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Andar'
      UpdCampo = 'Andar'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdUnidade: TdmkEdit
      Left = 283
      Top = 20
      Width = 70
      Height = 21
      MaxLength = 10
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Unidade'
      UpdCampo = 'Unidade'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdStatus: TdmkEditCB
      Left = 356
      Top = 20
      Width = 28
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Status'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBStatus
      IgnoraDBLookupComboBox = False
    end
    object CBStatus: TdmkDBLookupComboBox
      Left = 388
      Top = 20
      Width = 77
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsStatus
      TabOrder = 6
      dmkEditCB = EdStatus
      QryCampo = 'Status'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdPropriet: TdmkEditCB
      Left = 12
      Top = 106
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Propriet'
      UpdCampo = 'Propriet'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBPropriet
      IgnoraDBLookupComboBox = False
    end
    object CBPropriet: TdmkDBLookupComboBox
      Left = 68
      Top = 106
      Width = 396
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsImovPro
      TabOrder = 12
      dmkEditCB = EdPropriet
      QryCampo = 'Propriet'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CBInquilino: TdmkDBLookupComboBox
      Left = 68
      Top = 150
      Width = 396
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsImovInq
      TabOrder = 14
      dmkEditCB = EdInquilino
      QryCampo = 'Usuario'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdInquilino: TdmkEditCB
      Left = 12
      Top = 150
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Usuario'
      UpdCampo = 'Usuario'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBInquilino
      IgnoraDBLookupComboBox = False
    end
    object dmkDBEdit1: TdmkDBEdit
      Left = 12
      Top = 20
      Width = 70
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Codigo'
      DataSource = FmCond.DsCond
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object CkContinuar: TCheckBox
      Left = 13
      Top = 540
      Width = 112
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 26
    end
    object RGddVctEsp: TdmkRadioGroup
      Left = 496
      Top = 3
      Width = 489
      Height = 74
      Caption = 
        ' Dia do m'#234's do vencimento da quota condominial (quando diferente' +
        ' do padr'#227'o do condom'#237'nio): '
      Columns = 10
      ItemIndex = 0
      Items.Strings = (
        '00'
        '01'
        '02'
        '03'
        '04'
        '05'
        '06'
        '07'
        '08'
        '09'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28')
      TabOrder = 19
      QryCampo = 'ddVctEsp'
      UpdCampo = 'ddVctEsp'
      UpdType = utYes
      OldValor = 0
    end
    object EdConjuge: TdmkEditCB
      Left = 12
      Top = 194
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Conjuge'
      UpdCampo = 'Conjuge'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBConjuge
      IgnoraDBLookupComboBox = False
    end
    object CBConjuge: TdmkDBLookupComboBox
      Left = 68
      Top = 194
      Width = 396
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsConjuges
      TabOrder = 16
      dmkEditCB = EdConjuge
      QryCampo = 'Conjuge'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdProcurador: TdmkEditCB
      Left = 508
      Top = 288
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 21
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Procurador'
      UpdCampo = 'Procurador'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBProcurador
      IgnoraDBLookupComboBox = False
    end
    object CBProcurador: TdmkDBLookupComboBox
      Left = 564
      Top = 288
      Width = 405
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOME_ENT'
      ListSource = DsProcuradores
      TabOrder = 22
      dmkEditCB = EdProcurador
      QryCampo = 'Procurador'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object MeObserv: TdmkMemo
      Left = 12
      Top = 380
      Width = 481
      Height = 157
      TabOrder = 25
      QryCampo = 'Observ'
      UpdCampo = 'ObServ'
      UpdType = utYes
    end
    object EdQtdGaragem: TdmkEdit
      Left = 12
      Top = 64
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'QtdGaragem'
      UpdCampo = 'QtdGaragem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdMoradores: TdmkEdit
      Left = 88
      Top = 64
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Moradores'
      UpdCampo = 'Moradores'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdFracaoIdeal: TdmkEdit
      Left = 148
      Top = 64
      Width = 97
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'FracaoIdeal'
      UpdCampo = 'FracaoIdeal'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object CkUmAUm: TCheckBox
      Left = 128
      Top = 540
      Width = 157
      Height = 17
      Caption = 'Aborta altera'#231#227'o "um a um".'
      TabOrder = 27
      Visible = False
    end
    object RGJuridico: TdmkRadioGroup
      Left = 508
      Top = 312
      Width = 485
      Height = 74
      Caption = ' Status Jur'#237'dico: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Sem restri'#231#245'es'
        'Processo de protesto'
        'Protestado'
        'Com a'#231#227'o de cobran'#231'a'
        'Protestado e a'#231#227'o de cobran'#231'a')
      TabOrder = 23
      QryCampo = 'Juridico'
      UpdCampo = 'Juridico'
      UpdType = utYes
      OldValor = 0
    end
    object CkSitImv: TdmkCheckBox
      Left = 256
      Top = 68
      Width = 190
      Height = 17
      Caption = 'Rateia itens base de arrecada'#231#227'o.'
      TabOrder = 10
      QryCampo = 'SitImv'
      UpdCampo = 'SitImv'
      UpdType = utYes
      ValCheck = '1'
      ValUncheck = '0'
      OldValor = #0
    end
    object GroupBox1: TGroupBox
      Left = 12
      Top = 216
      Width = 481
      Height = 89
      Caption = ' Imobili'#225'ria: '
      TabOrder = 17
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 477
        Height = 72
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object SpeedButton11: TSpeedButton
          Left = 454
          Top = 3
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton11Click
        end
        object Label128: TLabel
          Left = 9
          Top = 28
          Width = 40
          Height = 13
          Caption = 'Contato:'
        end
        object Label127: TLabel
          Left = 333
          Top = 28
          Width = 45
          Height = 13
          Caption = 'Telefone:'
        end
        object EdImobiliaria: TdmkEditCB
          Left = 8
          Top = 3
          Width = 57
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Imobiliaria'
          UpdCampo = 'Imobiliaria'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBImobiliaria
          IgnoraDBLookupComboBox = False
        end
        object CBImobiliaria: TdmkDBLookupComboBox
          Left = 65
          Top = 3
          Width = 385
          Height = 21
          KeyField = 'Codigo'
          ListField = 'RazaoSocial'
          ListSource = DsImobiliarias
          TabOrder = 1
          dmkEditCB = EdImobiliaria
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object DBImov_Cont: TDBEdit
          Left = 8
          Top = 45
          Width = 319
          Height = 21
          DataField = 'EContato'
          DataSource = DsImobiliarias
          Enabled = False
          TabOrder = 2
        end
        object DBImov_ContTel: TDBEdit
          Left = 333
          Top = 45
          Width = 117
          Height = 21
          DataField = 'IMOBTEL_TXT'
          DataSource = DsImobiliarias
          Enabled = False
          TabOrder = 3
        end
      end
    end
    object GroupBox3: TGroupBox
      Left = 12
      Top = 312
      Width = 481
      Height = 49
      Caption = ' Configura'#231#227'o de envio de mensagens por inadimpl'#234'ncia: '
      TabOrder = 18
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 477
        Height = 32
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object SBInadCEMCad: TSpeedButton
          Left = 454
          Top = 3
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBInadCEMCadClick
        end
        object EdInadCEMCad: TdmkEditCB
          Left = 8
          Top = 3
          Width = 46
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'InadCEMCad'
          UpdCampo = 'InadCEMCad'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBInadCEMCad
          IgnoraDBLookupComboBox = False
        end
        object CBInadCEMCad: TdmkDBLookupComboBox
          Left = 54
          Top = 3
          Width = 396
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsInadCEMCad
          TabOrder = 1
          dmkEditCB = EdInadCEMCad
          QryCampo = 'InadCEMCad'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 496
      Top = 80
      Width = 505
      Height = 189
      Caption = ' Endere'#231'o de envio do bloqueto condominial: '
      TabOrder = 20
      object Label201: TLabel
        Left = 12
        Top = 59
        Width = 202
        Height = 13
        Caption = 'Terceiro a receber o bloqueto condominial:'
      end
      object Label202: TLabel
        Left = 12
        Top = 101
        Width = 202
        Height = 13
        Caption = 'Endere'#231'o para tipo igual a "Descrito aqui":'
      end
      object SpeedButton16: TSpeedButton
        Left = 476
        Top = 77
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton16Click
      end
      object RGBloqEndTip: TdmkRadioGroup
        Left = 2
        Top = 15
        Width = 501
        Height = 41
        Align = alTop
        Caption = ' Quem vai receber: '
        Columns = 5
        ItemIndex = 0
        Items.Strings = (
          'Autom'#225'tico'
          'Morador'
          'Propriet'#225'rio'
          'Terceiro'
          'Descrito aqui')
        TabOrder = 0
        QryCampo = 'BloqEndTip'
        UpdCampo = 'BloqEndTip'
        UpdType = utYes
        OldValor = 0
      end
      object EdBloqEndEnt: TdmkEditCB
        Left = 12
        Top = 77
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'BloqEndEnt'
        UpdCampo = 'BloqEndEnt'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBBloqEndEnt
        IgnoraDBLookupComboBox = False
      end
      object CBBloqEndEnt: TdmkDBLookupComboBox
        Left = 72
        Top = 77
        Width = 401
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsBloqEndEnt
        TabOrder = 2
        OnClick = CBBloqEndEntClick
        dmkEditCB = EdBloqEndEnt
        QryCampo = 'BloqEndEnt'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEnderNome: TdmkEdit
        Left = 12
        Top = 119
        Width = 484
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'EnderNome'
        UpdCampo = 'EnderNome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdEnderLin1: TdmkEdit
        Left = 12
        Top = 140
        Width = 484
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'EnderLin1'
        UpdCampo = 'EnderLin1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdEnderLin2: TdmkEdit
        Left = 12
        Top = 161
        Width = 484
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'EnderLin2'
        UpdCampo = 'EnderLin2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object GroupBox5: TGroupBox
      Left = 496
      Top = 388
      Width = 505
      Height = 149
      Caption = ' Celular (cadastro de entidades "A") para envio de SMS: '
      TabOrder = 24
      object Label6: TLabel
        Left = 12
        Top = 59
        Width = 125
        Height = 13
        Caption = 'Terceiro a receber o SMS:'
      end
      object Label5: TLabel
        Left = 12
        Top = 101
        Width = 298
        Height = 13
        Caption = 'N'#250'mero do celular e...    Nome da pessoa para "Descrito aqui":'
      end
      object SBSMSCelEnti: TSpeedButton
        Left = 476
        Top = 77
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBSMSCelEntiClick
      end
      object RGSMSCelTipo: TdmkRadioGroup
        Left = 12
        Top = 16
        Width = 485
        Height = 41
        Caption = ' Quem vai receber: '
        Columns = 5
        ItemIndex = 2
        Items.Strings = (
          'Autom'#225'tico'
          'Morador'
          'Propriet'#225'rio'
          'Terceiro'
          'Descrito aqui')
        TabOrder = 0
        QryCampo = 'SMSCelTipo'
        UpdCampo = 'SMSCelTipo'
        UpdType = utYes
        OldValor = 0
      end
      object EdSMSCelEnti: TdmkEditCB
        Left = 12
        Top = 77
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SMSCelEnti'
        UpdCampo = 'SMSCelEnti'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBSMSCelEnti
        IgnoraDBLookupComboBox = False
      end
      object CBSMSCelEnti: TdmkDBLookupComboBox
        Left = 72
        Top = 77
        Width = 401
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSMSCelEnti
        TabOrder = 2
        dmkEditCB = EdSMSCelEnti
        QryCampo = 'SMSCelEnti'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSMSCelNumr: TdmkEdit
        Left = 12
        Top = 119
        Width = 112
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtTelLongo
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'SMSCelNumr'
        UpdCampo = 'SMSCelNumr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdSMSCelNome: TdmkEdit
        Left = 128
        Top = 119
        Width = 369
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'SMSCelNome'
        UpdCampo = 'SMSCelNome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 201
      Height = 48
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 93
        Height = 32
        Caption = 'Im'#243'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 93
        Height = 32
        Caption = 'Im'#243'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 93
        Height = 32
        Caption = 'Im'#243'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 249
      Top = 0
      Width = 711
      Height = 48
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 707
        Height = 31
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 609
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrStatus: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM status'
      'ORDER BY Nome')
    Left = 12
    Top = 12
    object QrStatusCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'status.Codigo'
    end
    object QrStatusCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStatusNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsStatus: TDataSource
    DataSet = QrStatus
    Left = 40
    Top = 12
  end
  object QrImovPro: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CASE WHEN Tipo=0 '
      'THEN RazaoSocial ELSE Nome END Nome'
      'FROM Entidades'
      'WHERE Cliente2='#39'V'#39
      'ORDER BY Nome')
    Left = 96
    Top = 12
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'entidades.CodUsu'
    end
  end
  object DsImovPro: TDataSource
    DataSet = QrImovPro
    Left = 124
    Top = 12
  end
  object QrImovInq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CASE WHEN Tipo=0 '
      'THEN RazaoSocial ELSE Nome END Nome'
      'FROM Entidades'
      'WHERE Cliente3 = '#39'V'#39
      'ORDER BY Nome')
    Left = 152
    Top = 28
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrImovInqCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'entidades.CodUsu'
    end
  end
  object DsImovInq: TDataSource
    DataSet = QrImovInq
    Left = 180
    Top = 28
  end
  object QrConjuges: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 '
      'THEN RazaoSocial ELSE Nome END NOME'
      'FROM entidades'
      'WHERE Cliente4 = '#39'V'#39
      'ORDER BY Nome')
    Left = 208
    Top = 28
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Origin = 'entidades.Nome'
      Size = 100
    end
  end
  object DsConjuges: TDataSource
    DataSet = QrConjuges
    Left = 236
    Top = 12
  end
  object QrImobiliarias: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, RazaoSocial, EContato, ETe1'
      'FROM entidades'
      'WHERE Fornece1 = '#39'V'#39' '
      'OR Terceiro = '#39'V'#39
      'ORDER BY RazaoSocial')
    Left = 264
    Top = 12
    object QrImobiliariasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImobiliariasRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 100
    end
    object QrImobiliariasEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrImobiliariasETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrImobiliariasIMOBTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IMOBTEL_TXT'
      Calculated = True
    end
  end
  object DsImobiliarias: TDataSource
    DataSet = QrImobiliarias
    Left = 292
    Top = 12
  end
  object DsBloqEndEnt: TDataSource
    DataSet = QrBloqEndEnt
    Left = 348
    Top = 12
  end
  object QrProcuradores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME_ENT'
      'FROM entidades'
      'ORDER BY NOME_ENT')
    Left = 376
    Top = 12
    object QrProcuradoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProcuradoresNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Required = True
      Size = 100
    end
  end
  object DsProcuradores: TDataSource
    DataSet = QrProcuradores
    Left = 404
    Top = 12
  end
  object QrBloqEndEnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 '
      'THEN RazaoSocial ELSE Nome END NOME'
      'FROM entidades'
      'WHERE Terceiro = '#39'V'#39
      'ORDER BY Nome')
    Left = 320
    Top = 12
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField5: TWideStringField
      FieldName = 'Nome'
      Origin = 'entidades.Nome'
      Size = 100
    end
  end
  object VUStatus: TdmkValUsu
    dmkEditCB = EdStatus
    QryCampo = 'Status'
    UpdCampo = 'Status'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 68
    Top = 28
  end
  object QrInadCEMCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM DiarCEMCad'
      'ORDER BY Nome')
    Left = 844
    Top = 416
    object QrInadCEMCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInadCEMCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsInadCEMCad: TDataSource
    DataSet = QrInadCEMCad
    Left = 872
    Top = 416
  end
  object QrSMSCelEnti: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 '
      'THEN RazaoSocial ELSE Nome END NOME'
      'FROM entidades'
      'WHERE Terceiro = '#39'V'#39
      'ORDER BY Nome')
    Left = 432
    Top = 12
    object QrSMSCelEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSMSCelEntiNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
  end
  object DsSMSCelEnti: TDataSource
    DataSet = QrSMSCelEnti
    Left = 460
    Top = 12
  end
end
