unit CondFlx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, dmkEdit, Mask, DBCtrls,
  dmkDBEdit, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmCondFlx = class(TForm)
    Panel3: TPanel;
    Label5: TLabel;
    Panel44: TPanel;
    GroupBox31: TGroupBox;
    Panel45: TPanel;
    Label71: TLabel;
    EdFlxB_Ordem: TdmkEdit;
    GroupBox39: TGroupBox;
    Label211: TLabel;
    Label212: TLabel;
    Label213: TLabel;
    Label214: TLabel;
    Label215: TLabel;
    Label210: TLabel;
    Label209: TLabel;
    Label208: TLabel;
    Label207: TLabel;
    Label206: TLabel;
    EdFlxB_Folha: TdmkEdit;
    EdFlxB_Proto: TdmkEdit;
    EdFlxB_Relat: TdmkEdit;
    EdFlxB_EMail: TdmkEdit;
    EdFlxB_Porta: TdmkEdit;
    EdFlxB_Postl: TdmkEdit;
    EdFlxB_Print: TdmkEdit;
    EdFlxB_Risco: TdmkEdit;
    EdFlxB_LeiAr: TdmkEdit;
    EdFlxB_Fecha: TdmkEdit;
    GroupBox38: TGroupBox;
    Panel46: TPanel;
    Label223: TLabel;
    EdFlxM_Ordem: TdmkEdit;
    GroupBox40: TGroupBox;
    Label217: TLabel;
    Label218: TLabel;
    Label222: TLabel;
    Label221: TLabel;
    Label220: TLabel;
    Label219: TLabel;
    EdFlxM_Conci: TdmkEdit;
    EdFlxM_Docum: TdmkEdit;
    EdFlxM_Entrg: TdmkEdit;
    EdFlxM_Encad: TdmkEdit;
    EdFlxM_Contb: TdmkEdit;
    EdFlxM_Anali: TdmkEdit;
    dmkDBEdit1: TdmkDBEdit;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    EdFlxM_Web: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCondFlx: TFmCondFlx;

implementation

uses Module, Cond, UMySQLModule, UnMyObjects;

{$R *.DFM}

procedure TFmCondFlx.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondFlx.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondFlx.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCondFlx.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondFlx.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := FmCond.QrCondCodigo.Value;
  if UMyMod.ExecSQLInsUpdFm(FmCondFlx, stUpd, 'cond',
  Codigo, Dmod.QrUpd) then
  begin
    FmCond.LocCod(Codigo, Codigo);
    Close;
  end;
end;

end.
