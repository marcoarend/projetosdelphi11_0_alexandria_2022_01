object FmCondGerModelBloq: TFmCondGerModelBloq
  Left = 525
  Top = 183
  Caption = 'GER-CONDM-013 :: Modelo de Bloqueto'
  ClientHeight = 537
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object RGModelBloq: TRadioGroup
    Left = 0
    Top = 113
    Width = 792
    Height = 201
    Align = alClient
    Caption = ' Modelo de impress'#227'o do bloqueto: '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'FmPrincipal.PreencheModelosBloq')
    TabOrder = 0
    OnClick = RGModelBloqClick
  end
  object GBConfig: TGroupBox
    Left = 0
    Top = 351
    Width = 792
    Height = 45
    Align = alBottom
    Caption = ' Configura'#231#227'o do modelo H ou R: '
    TabOrder = 1
    Visible = False
    object SpeedButton2: TSpeedButton
      Left = 747
      Top = 18
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object EdConfig: TdmkEditCB
      Left = 6
      Top = 18
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdConfigChange
      DBLookupComboBox = CBConfig
      IgnoraDBLookupComboBox = False
    end
    object CBConfig: TdmkDBLookupComboBox
      Left = 64
      Top = 18
      Width = 680
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsConfigBol
      TabOrder = 1
      dmkEditCB = EdConfig
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 396
    Width = 792
    Height = 27
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkAgrupaMensal: TCheckBox
      Left = 14
      Top = 6
      Width = 766
      Height = 17
      Caption = 
        'Separar as somas de valores no balancete por compet'#234'ncia ao agru' +
        'par valores.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object RGCompe: TRadioGroup
    Left = 0
    Top = 314
    Width = 792
    Height = 37
    Align = alBottom
    Caption = ' Ficha de compensa'#231#227'o (modelo E): '
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      '??'
      '1 (uma via)'
      '2 (duas vias)')
    TabOrder = 3
    Visible = False
    OnClick = RGModelBloqClick
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 792
    Height = 65
    Align = alTop
    Caption = ' Dados ESPEC'#205'FICOS para: '
    TabOrder = 4
    object ST_Condominio: TStaticText
      Left = 100
      Top = 20
      Width = 681
      Height = 17
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 0
    end
    object StaticText1: TStaticText
      Left = 8
      Top = 20
      Width = 89
      Height = 17
      AutoSize = False
      Caption = 'Condom'#237'nio:'
      TabOrder = 1
    end
    object StaticText3: TStaticText
      Left = 8
      Top = 40
      Width = 89
      Height = 17
      AutoSize = False
      Caption = 'UH / propriet'#225'rio:'
      TabOrder = 2
    end
    object STUH_Propriet: TStaticText
      Left = 100
      Top = 40
      Width = 681
      Height = 17
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 3
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 242
        Height = 32
        Caption = 'Modelo de Bloqueto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 242
        Height = 32
        Caption = 'Modelo de Bloqueto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 242
        Height = 32
        Caption = 'Modelo de Bloqueto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 423
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 6
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 467
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 7
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 644
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrConfigBol: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Colunas'
      'FROM configbol'
      'ORDER BY Nome')
    Left = 24
    Top = 16
    object QrConfigBolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigBolNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrConfigBolColunas: TSmallintField
      FieldName = 'Colunas'
    end
  end
  object DsConfigBol: TDataSource
    DataSet = QrConfigBol
    Left = 56
    Top = 16
  end
end
