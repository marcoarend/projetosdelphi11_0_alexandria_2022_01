unit blocparcpar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, dmkEdit, Mask, DBCtrls,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmbloqparcpar = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    dmkEdCredito: TdmkEdit;
    TPVencimento: TDateTimePicker;
    Label7: TLabel;
    Label8: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTabLctA: String;
  end;

  var
  Fmbloqparcpar: TFmbloqparcpar;

implementation

uses bloqparc, Module, UMySQLModule, UnInternalConsts, UnFinanceiro,
UnMyObjects;

{$R *.DFM}

procedure TFmbloqparcpar.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmbloqparcpar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmbloqparcpar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmbloqparcpar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmbloqparcpar.BtOKClick(Sender: TObject);
var
  Vencimento: String;
  Controle: Integer;
begin
  Controle   := Fmbloqparc.QrbloqparcparControle.Value;
  if Controle <> 0 then
  begin
    Vencimento := Geral.FDT(TPVencimento.Date, 1);
    //
    Dmod.QrWeb.Close;
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('UPDATE bloqparcpar SET ');
    Dmod.QrWeb.SQL.Add('ValBol=:P0, Vencimento=:P1 ');
    Dmod.QrWeb.SQL.Add('WHERE Controle=:P2');
    Dmod.QrWeb.Params[00].AsFloat   := dmkEdCredito.ValueVariant;
    Dmod.QrWeb.Params[01].AsString  := Vencimento;
    Dmod.QrWeb.Params[02].AsInteger := Controle;
    Dmod.QrWeb.ExecSQL;
    //
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL := Dmod.QrWeb.SQL;
    Dmod.QrUpd.Params := Dmod.QrWeb.Params;
    Dmod.QrUpd.ExecSQL;
    //
    if UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Vencimento', 'Credito'], ['Atrelado'], [
      Vencimento, dmkEdCredito.ValueVariant], [Controle], True, '', FTabLctA) then
    begin
      Fmbloqparc.LocCod(Fmbloqparc.QrbloqparcCodigo.Value, Fmbloqparc.QrbloqparcCodigo.Value);
      Fmbloqparc.Reopenbloqparcpar(Controle);
      Close;
    end;
  end else
    Geral.MB_ERRO('ERRO! AVISE A DERMATEK! Controle igual a zero!!!');
  //
end;

end.
