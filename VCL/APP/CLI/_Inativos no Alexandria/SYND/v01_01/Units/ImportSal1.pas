unit ImportSal1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkEdit, dmkGeral,
  ComCtrls, DBCtrls, dmkDBLookupComboBox, dmkEditCB, dmkEditDateTimePicker, DB,
  mySQLDbTables, Variants, dmkImage, UnDmkEnums;

type
  TFmImportSal1 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    EdPath: TEdit;
    SpeedButton1: TSpeedButton;
    GroupBox2: TGroupBox;
    EdCPFIni: TdmkEdit;
    Label5: TLabel;
    EdCPFTam: TdmkEdit;
    Label6: TLabel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    EdNomeIni: TdmkEdit;
    EdNomeTam: TdmkEdit;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label7: TLabel;
    EdValorIni: TdmkEdit;
    EdValorTam: TdmkEdit;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    EdChapaIni: TdmkEdit;
    EdChapaTam: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    EdEmpresa: TdmkEditCB;
    LaEmpresa: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    Label11: TLabel;
    CBCarteira: TdmkDBLookupComboBox;
    EdGenero: TdmkEditCB;
    Label12: TLabel;
    CBGenero: TdmkDBLookupComboBox;
    TPData: TdmkEditDateTimePicker;
    Label13: TLabel;
    LaMes: TLabel;
    EdMes: TdmkEdit;
    TPVencimento: TdmkEditDateTimePicker;
    LaVencimento: TLabel;
    Label14: TLabel;
    EdDescriA: TdmkEdit;
    EdDescriB: TdmkEdit;
    QrFunci: TmySQLQuery;
    QrFunciCodigo: TIntegerField;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    DsCarteiras: TDataSource;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    QrContasNOMEPLANO: TWideStringField;
    DsContas: TDataSource;
    GroupBox5: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    EdMezLin: TdmkEdit;
    EdMezIni: TdmkEdit;
    Label17: TLabel;
    EdMezTam: TdmkEdit;
    DsLct: TDataSource;
    DBGrid1: TDBGrid;
    QrEnti1: TmySQLQuery;
    QrEnti1Codigo: TIntegerField;
    QrFunciChapa: TWideStringField;
    QrEnti2: TmySQLQuery;
    QrEnti2Codigo: TIntegerField;
    TbLct: TmySQLTable;
    TbLctData: TDateField;
    TbLctTipo: TSmallintField;
    TbLctCarteira: TIntegerField;
    TbLctGenero: TIntegerField;
    TbLctDescricao: TWideStringField;
    TbLctNotaFiscal: TIntegerField;
    TbLctDebito: TFloatField;
    TbLctCredito: TFloatField;
    TbLctSerieCH: TWideStringField;
    TbLctDocumento: TFloatField;
    TbLctVencimento: TDateField;
    TbLctFatID: TIntegerField;
    TbLctFatID_Sub: TIntegerField;
    TbLctFatNum: TFloatField;
    TbLctFatParcela: TIntegerField;
    TbLctMez: TIntegerField;
    TbLctFornecedor: TIntegerField;
    TbLctCliente: TIntegerField;
    TbLctCliInt: TIntegerField;
    TbLctForneceI: TIntegerField;
    TbLctMoraDia: TFloatField;
    TbLctMulta: TFloatField;
    TbLctDataDoc: TDateField;
    TbLctVendedor: TIntegerField;
    TbLctAccount: TIntegerField;
    TbLctDuplicata: TWideStringField;
    TbLctDepto: TIntegerField;
    TbLctUnidade: TIntegerField;
    TbLctSit: TIntegerField;
    Memo1: TMemo;
    TbLctNO_CONTA: TWideStringField;
    GroupBox6: TGroupBox;
    EdSerieCH1: TdmkEdit;
    EdDocumento1: TdmkEdit;
    LaDoc: TLabel;
    CkAtribCHManu: TCheckBox;
    QrCarteirasTipoDoc: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel6: TPanel;
    BtPreGera: TBitBtn;
    BtConfirma: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPreGeraClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TPVencimentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEmpresaChange(Sender: TObject);
    procedure TbLctBeforeClose(DataSet: TDataSet);
    procedure TbLctAfterOpen(DataSet: TDataSet);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdPathChange(Sender: TObject);
    procedure EdGeneroChange(Sender: TObject);
    procedure CkAtribCHManuClick(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    Flct: String;
    FSetando: Boolean;
    procedure FechaPreGera();
    procedure VerificaTipoDoc();
  public
    { Public declarations }
    FTabLcta: String;
  end;

  var
  FmImportSal1: TFmImportSal1;

implementation

uses UnMyObjects, ModuleGeral, UCreate, UMySQLModule, Module, MyDBCheck,
FPFunciNew, UnInternalConsts, UnFinanceiro;

{$R *.DFM}

procedure TFmImportSal1.BtConfirmaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  TbLct.First;
  while not TbLct.Eof do
  begin
    UFinanceiro.LancamentoDefaultVARS;
    //
    FLAN_Data          := Geral.FDT(TbLctData.Value, 1);
    FLAN_Vencimento    := Geral.FDT(TbLctVencimento.Value, 1);
    FLAN_DataCad       := Geral.FDT(Date, 1);
    //FLAN_Mez           := MLAGeral.ITS_Null(TbLctMez.Value);
    FLAN_Mez           := TbLctMez.Value;
    FLAN_Descricao     := TbLctDescricao.Value;
    //FLAN_Compensado    := '';
    FLAN_Duplicata     := TbLctDuplicata.Value;
    //FLAN_Doc2          := '';
    FLAN_SerieCH       := TbLctSerieCH.Value;

    FLAN_Documento     := TbLctDocumento.Value;
    FLAN_Tipo          := TbLctTipo.Value;
    FLAN_Carteira      := TbLctCarteira.Value;
    FLAN_Credito       := TbLctCredito.Value;
    FLAN_Debito        := TbLctDebito.Value;
    FLAN_Genero        := TbLctGenero.Value;
    //FLAN_SerieNF       := '';
    FLAN_NotaFiscal    := TbLctNotaFiscal.Value;
    FLAN_Sit           := TbLctSit.Value;
    //FLAN_ID_Pgto       := 0;
    //FLAN_Cartao        := 0;
    //FLAN_Linha         := 0;
    FLAN_Fornecedor    := TbLctFornecedor.Value;
    FLAN_Cliente       := TbLctCliente.Value;
    FLAN_MoraDia       := TbLctMoraDia.Value;
    FLAN_Multa         := TbLctMulta.Value;
    FLAN_UserCad       := VAR_USUARIO;
    FLAN_DataDoc       := Geral.FDT(TbLctDataDoc.Value, 1);
    FLAN_Vendedor      := TbLctVendedor.Value;
    FLAN_Account       := TbLctAccount.Value;
    //FLAN_ICMS_P        := 0;
    //FLAN_ICMS_V        := 0;
    FLAN_CliInt        := TbLctCliInt.Value;
    FLAN_Depto         := TbLctDepto.Value;
    //FLAN_DescoPor      := 0;
    FLAN_ForneceI      := TbLctForneceI.Value;
    //FLAN_DescoVal      := 0;
    //FLAN_NFVal         := 0;
    FLAN_Unidade       := TbLctUnidade.Value;
    //FLAN_Qtde          := 0;
    FLAN_FatID         := TbLctFatID.Value;
    FLAN_FatID_Sub     := TbLctFatID_Sub.Value;
    FLAN_FatNum        := TbLctFatNum.Value;
    FLAN_FatParcela    := TbLctFatParcela.Value;
    //
    //FLAN_MultaVal      := 0;
    //FLAN_MoraVal       := 0;
    //FLAN_CtrlIni       := 0;
    //FLAN_Nivel         := 0;
    //FLAN_CNAB_Sit      := 0;
    //FLAN_TipoCH        := 0;
    //FLAN_Atrelado      := 0;
    //FLAN_SubPgto1      := 0;
    //FLAN_MultiPgto     := 0;
    //
    //FLAN_Emitente      := '';
    //FLAN_CNPJCPF       := '';
    //FLAN_Banco         := 0;
    //FLAN_Agencia       := '';
    //FLAN_ContaCorrente := '';

    case UFinanceiro.NaoDuplicarLancto(stIns, TbLctDocumento.Value,
    TbLctDocumento.Value > 0, FLAN_SerieCH, FLAN_NotaFiscal, FLAN_NotaFiscal <> 0,
    FLAN_Credito, FLAN_Debito, FLAN_Genero, FLAN_Mez, True, FLAN_CliInt, True,
    FLAN_Carteira, FLAN_Cliente, FLAN_Fornecedor, LaAviso1, LaAviso2, FTabLctA, '', '') of
     -1:
      begin
        Screen.Cursor := crDefault;
        Exit; // sai do loop
      end;
      0: ; // n�o faz nada
      1: // insere item
      begin
        FLAN_Sub           := 0;
        FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          FTabLctA, LAN_CTOS, 'Controle');
        if UFinanceiro.InsereLancamento(FTabLctA) then ;
      end;
    end;
    TbLct.Next;
  end;
  Screen.Cursor := crDefault;
  Close;
end;

procedure TFmImportSal1.BtPreGeraClick(Sender: TObject);
  procedure InsereItem(const CliInt, Fornecedor: Integer;
  const Nome, Valor: String; var Item: Integer);
  var
    Data, Vencimento, DataDoc, Descricao, ChSer: String;
    Sit, Tipo, Carteira, Genero, Mez, CHNum: Integer;
    Debito: Double;
  begin
    Data       := Geral.FDT(TPData.Date, 1);
    Vencimento := Geral.FDT(TPVencimento.Date, 1);
    DataDoc    := Geral.FDT(Date, 1);
    Descricao  := Trim(EdDescriA.Text + ' ' + Nome + ' ' + EdDescriB.Text);
    Tipo       := QrCarteirasTipo.Value;
    Carteira   := QrCarteirasCodigo.Value;
    Genero     := QrContasCodigo.Value;
    Mez        := Geral.IMV(EdMes.Text[6] + EdMes.Text[7] + EdMes.Text[1] + EdMes.Text[2]);
    //
    Debito     := Geral.DMV(Trim(Valor));
    if CkAtribCHManu.Checked then
    begin
      CHSer := EdSerieCH1.Text;
      CHNum := EdDocumento1.ValueVariant;
      if (CHNum > 0) and (Debito <> 0) then
      begin
        Item  := Item + 1;
        CHNum := CHNum + Item -1;
      end else
      begin
        CHNum := 0;
        //CHSer := '';
      end;
    end else
    begin
      FSetando := True;
      try
        if not UMyMod.ObtemProximoCHTalao(EdCarteira.ValueVariant,
        EdSerieCH1, EdDocumento1) then
          Exit
        else begin
          CHNum := EdDocumento1.ValueVariant;
          CHSer := EdSerieCH1.Text;
        end;
      finally
        FSetando := False;
      end;
    end;
    //
    case Tipo of
      0: Sit := 3;
      1: Sit := 3;
      2: Sit := 0;
      else Sit := -1;
    end;
    if Debito = 0 then
      Sit := 4;
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, Flct, False, [
      'Data', 'Tipo', 'Carteira',
      'Genero', 'Descricao', 'Debito',
      'Vencimento', 'Mez', 'Fornecedor',
      'CliInt', 'DataDoc', 'SerieCH',
      'Documento', 'Sit'
    ], [], [
      Data, Tipo, Carteira,
      Genero, Descricao, Debito,
      Vencimento, Mez, Fornecedor,
      CliInt, DataDoc, CHSer,
      CHNum, Sit], [], False);
  end;
var
  Txt, Nome, CPX, CPF, Valor, Chapa: String;
  N, I, Ini, Tam, CliInt, Filial, Item: Integer;
  //
begin
  if not CkAtribCHManu.Checked then
  begin
    N := 0;
    for I := 0 to Memo1.Lines.Count - 1 do
    begin
      Ini := EdCPFIni.ValueVariant;
      Tam := EdCPFTam.ValueVariant;
      CPX := Copy(Memo1.Lines[I], Ini, Tam);
      CPF := Geral.SoNumero_TT(CPX);
      if Length(CPF) = 11 then
        N := N + 1;
    end;
    if UMyMod.ObtemQtdeCHTaloes(EdCarteira.ValueVariant) < N then
      if Geral.MensagemBox(
      'N�o existem folhas de cheque suficientes para todas linhas.' +
      #13#10 + 'Deseja continuar assim mesmo?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
        Exit;
  end;
  Screen.Cursor := crHourGlass;
  Item := 0;
  Flct := UCriar.RecriaTempTableNovo(ntrttLct, DmodG.QrUpdPID1, False);
  if Memo1.Lines.Count > 0 then
  begin
    for I := 0 to Memo1.Lines.Count - 1 do
    begin
      Ini := EdChapaIni.ValueVariant;
      Tam := EdChapaTam.ValueVariant;
      Chapa := Copy(Memo1.Lines[I], Ini, Tam);
      //
      Ini := EdNomeIni.ValueVariant;
      Tam := EdNomeTam.ValueVariant;
      Nome := Trim(Copy(Memo1.Lines[I], Ini, Tam));
      //
      Ini := EdCPFIni.ValueVariant;
      Tam := EdCPFTam.ValueVariant;
      CPX := Copy(Memo1.Lines[I], Ini, Tam);
      //
      Ini := EdValorIni.ValueVariant;
      Tam := EdValorTam.ValueVariant;
      Valor := Copy(Memo1.Lines[I], Ini, Tam);
      //
      CPF := Geral.SoNumero_TT(CPX);
      if Length(CPF) = 11 then
      begin
        CliInt     := DModG.QrEmpresasCodigo.Value;
        Filial     := DModG.QrEmpresasFilial.Value;
        Txt := '';
        // Pesquisar se o funcion�rio tem cadastro com a Chapa!
        QrFunci.Close;
        QrFunci.SQL.Clear;
        QrFunci.SQL.Add('SELECT fpf.Codigo, fpf.Chapa');
        QrFunci.SQL.Add('FROM fpfunci fpf');
        QrFunci.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=fpf.Codigo');
        QrFunci.SQL.Add('WHERE fpf.Empresa=' + FormatFloat('0', CliInt));
        if Length(Geral.SoNumero1a9_TT(CPF)) > 0 then
        begin
          QrFunci.SQL.Add('AND (ent.CPF="' + CPF + '" OR CPF="" OR CPF="00000000000")');
        end;
        QrFunci.SQL.Add('AND fpf.Chapa="' + Chapa + '"');
        {
        QrFunci.Params[00].AsInteger := CliInt;
        QrFunci.Params[01].AsString  := CPF;
        QrFunci.Params[02].AsString  := Chapa;
        }
        //dmkPF.LeMeuTexto(QrFunci.SQL.Text);
        QrFunci.Open;
        case QrFunci.RecordCount of
          0:
          begin
            {
            if Lenght(MLAGeral.SoNumero1a9_TT(CPF)) = 0 then
              Txt := 'N�o foi localizado nenhum cadastro para:';
            else begin
              // Pesquisar se o funcion�rio tem cadastro sem o Chapa!
              QrFunci.Close;
              QrFunci.SQL.Clear;
              QrFunci.SQL.Add('SELECT fpf.Codigo, fpf.Chapa');
              QrFunci.SQL.Add('FROM fpfunci fpf');
              QrFunci.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=fpf.Codigo');
              QrFunci.SQL.Add('WHERE fpf.Empresa=:P0');
              QrFunci.SQL.Add('AND ent.CPF=:P1');
              //QrFunci.SQL.Add('AND fpf.Chapa=:P2');
              QrFunci.Params[00].AsInteger := CliInt;
              QrFunci.Params[01].AsString  := CPF;
              //QrFunci.Params[02].AsString  := Chapa;
              QrFunci.Open;
              case QrFunci.RecordCount of
                0:
                begin
                  QrEnti.Close;
                  QrEnti.Params[0].AsString := CPF;
                  QrEnti.Open;
                  case QrEnti.RecordCount of
                    0: Txt := 'N�o foi localizado nenhum cadastro para:';
                    1:
                    begin
                      if Geral.MensagemBox(Txt, 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
                      begin
                        if DBCheck.CriaFm(TFmFpFunci, FmFpFunci, afmoNegarComAviso) then
                        begin
                          FmFpFunci.ShowModal;
                          FmFpFunci.Destroy;
                        end;
                      end;
                    end
                    else begin
                      'Existem ' + IntToStr(QrEnti.RecordCount) +
                      ' cadastos de entidade para o CPF ' + CPX;
                    end;
                  end;
                end;
                1:
                begin
                  /
                end;
                else begin
                  /
                end;
              end;
              //
              }
              Txt := 'N�o foi localizado nenhum cadastro para:';
              Txt := Txt + #13#10 + 'Nome: ' + Nome + #13#10 + 'CPF: ' +
                     CPX + #13#10 + 'Chapa: ' + Chapa;
              //if QrFunci.RecordCount > 1 then
              Txt := Txt + #13#10 + 'Deseja cadastr�-lo agora?';
              case Geral.MensagemBox(Txt, 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) of
                ID_YES:
                begin
                  if DBCheck.CriaFm(TFmFpFunciNew, FmFpFunciNew, afmoNegarComAviso) then
                  begin
                    FmFpFunciNew.EdEmpresa.ValueVariant := Filial;
                    FmFpFunciNew.CBEmpresa.KeyValue     := Filial;
                    //
                    if Length(Geral.SoNumero1a9_TT(CPF)) > 0 then
                    begin
                      QrEnti1.Close;
                      QrEnti1.Params[0].AsString := CPF;
                      QrEnti1.Open;
                      if QrEnti1.RecordCount > 0 then
                      begin
                        FmFpFunciNew.EdCodigo.ValueVariant := QrEnti1Codigo.Value;
                        FmFpFunciNew.CBCodigo.KeyValue     := QrEnti1Codigo.Value;
                      end;
                    end;
                    if FmFpFunciNew.EdCodigo.ValueVariant = 0 then
                    begin
                      QrEnti2.Close;
                      QrEnti2.Params[0].AsString := Nome;
                      QrEnti2.Open;
                      if QrEnti2.RecordCount > 0 then
                      begin
                        FmFpFunciNew.EdCodigo.ValueVariant := QrEnti2Codigo.Value;
                        FmFpFunciNew.CBCodigo.KeyValue     := QrEnti2Codigo.Value;
                      end else Geral.MensagemBox('N�o foi poss�vel localizar a ' +
                      'entidade "' + Nome + '" pelo CPF ' + CPX + '.' + #13#10 +
                      'Antes de incluir um novo cadastro nas entidades ' +
                      'verifique se j� n�o h� um cadastro para este ' +
                      'funcion�rio sem a informa��o do CPF!', 'Aviso',
                      MB_OK+MB_ICONWARNING);
                    end;
                    FmFpFunciNew.EdChapa.Text := Chapa;
                    FmFpFunciNew.EdNome.Text  := Nome;
                    FmFpFunciNew.ShowModal;
                    FmFpFunciNew.Destroy;
                    if VAR_CADASTRO > 0 then
                      InsereItem(CliInt, VAR_CADASTRO, Nome, Valor, Item);
                  end;
                end;
                ID_CANCEL:
                begin
                  Screen.Cursor := crDefault;
                  Exit;
                end;
              end;
              Txt := '';
            //end;
          end;
          1: InsereItem(CliInt, QrFunciCodigo.Value, Nome, Valor, Item);
          else Txt := 'Foram localizados ' + IntToStr(QrFunci.RecordCount) +
          ' cadastros para:';
        end;
        if Trim(Txt) <> '' then
        begin
          Txt := Txt + #13#10 + 'CPF: ' + CPF + #13#10 + 'Chapa: ' + Chapa;
          if QrFunci.RecordCount > 1 then Txt := Txt + #13#10 + 'Nenhum ser� considerado!';
          Geral.MensagemBox(Txt, 'Aviso', MB_OK+MB_ICONWARNING);
        end;
      end;
    end;
    TbLct.Close;
    TbLct.Open;
    PageControl1.ActivePageIndex := 1;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportSal1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImportSal1.CkAtribCHManuClick(Sender: TObject);
begin
  LaDoc.Enabled        := CkAtribCHManu.Checked;
  EdSerieCH1.Enabled   := CkAtribCHManu.Checked;
  EdDocumento1.Enabled := CkAtribCHManu.Checked;
end;

procedure TFmImportSal1.EdCarteiraChange(Sender: TObject);
begin
  FechaPreGera();
  VerificaTipoDoc();
end;

procedure TFmImportSal1.EdEmpresaChange(Sender: TObject);
begin
  FechaPreGera();
  //
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := DModG.QrEmpresasCodigo.Value;
  QrCarteiras.Open;
  //
  if not QrCarteiras.Locate('Codigo', EdCarteira.ValueVariant, []) then
  begin
    EdCarteira.ValueVariant := 0;
    CBCarteira.KeyValue     := Null;
  end;
end;

procedure TFmImportSal1.EdGeneroChange(Sender: TObject);
begin
  EdDescriA.Text := CBGenero.Text;
  FechaPreGera();
end;

procedure TFmImportSal1.EdPathChange(Sender: TObject);
begin
  FechaPreGera();
  VerificaTipoDoc();
end;

procedure TFmImportSal1.TbLctAfterOpen(DataSet: TDataSet);
begin
  BtConfirma.Enabled := TbLct.RecordCount > 0;
end;

procedure TFmImportSal1.TbLctBeforeClose(DataSet: TDataSet);
begin
  BtConfirma.Enabled := False;
end;

procedure TFmImportSal1.TPVencimentoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F6 then
    TPVencimento.Date := TPdata.Date;
end;

procedure TFmImportSal1.VerificaTipoDoc;
var
  Habilita: Boolean;
begin
  Habilita := QrCarteirasTipoDoc.Value <> 1;
  EdSerieCH1.Enabled        := Habilita;
  EdDocumento1.Enabled      := Habilita;
  LaDoc.Enabled             := Habilita;
  CkAtribCHManu.Enabled     := Habilita;
  if (Habilita = False) and (FSetando = False) then
  begin
    EdSerieCH1.ValueVariant   := '';
    EdDocumento1.ValueVariant := 0;
  end;
end;

procedure TFmImportSal1.FechaPreGera();
var
  Habilita: Boolean;
begin
  TbLct.Close;
  //
  Habilita := (Memo1.Lines.Count > 1) and
              (EdEmpresa.ValueVariant <> 0) and
              (EdGenero.ValueVariant <> 0) and
              (EdCarteira.ValueVariant <> 0);
  BtPreGera.Enabled := Habilita;            
end;

procedure TFmImportSal1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmImportSal1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FSetando := False;
  PageControl1.ActivePageIndex := 0;
  TPData.Date := Date;
  TPVencimento.Date := Date;
  QrContas.Open;
  TbLct.Database := DModG.MyPID_DB;
end;

procedure TFmImportSal1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImportSal1.SpeedButton1Click(Sender: TObject);
var
  Arquivo: String;
  Ini, Tam: Integer;
begin
  if MyObjects.FileOpenDialog(Self, '', '', Label1.Caption, '', [], Arquivo) then
  begin
    if FileExists(Arquivo) then
    begin
      EdPath.Text := Arquivo;
      Memo1.Lines.LoadFromFile(Arquivo);
      if EdMezLin.ValueVariant > 0 then
      begin
        Ini := EdMezIni.ValueVariant;
        Tam := EdMezTam.ValueVariant;
        EdMes.Text := Copy(Memo1.Lines[EdMezLin.ValueVariant], Ini, Tam);
      end;
    end;
  end;
end;

end.
