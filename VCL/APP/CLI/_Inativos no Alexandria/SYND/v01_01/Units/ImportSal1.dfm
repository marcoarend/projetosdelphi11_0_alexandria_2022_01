object FmImportSal1: TFmImportSal1
  Left = 339
  Top = 185
  Caption = 'SYN-EXTRA-001:: Carrega Lan'#231'amentos de Sal'#225'rios - Modelo 1'
  ClientHeight = 589
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 427
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 446
    object Panel3: TPanel
      Left = 0
      Top = 60
      Width = 1008
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 1
      ExplicitTop = 61
      ExplicitWidth = 1006
      object LaEmpresa: TLabel
        Left = 8
        Top = 12
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label11: TLabel
        Left = 8
        Top = 36
        Width = 36
        Height = 13
        Caption = 'Carteira'
      end
      object Label12: TLabel
        Left = 8
        Top = 60
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label13: TLabel
        Left = 592
        Top = 2
        Width = 99
        Height = 13
        Caption = 'Data do lan'#231'amento:'
      end
      object LaMes: TLabel
        Left = 700
        Top = 2
        Width = 23
        Height = 13
        Caption = 'M'#234's:'
      end
      object LaVencimento: TLabel
        Left = 757
        Top = 2
        Width = 80
        Height = 13
        Caption = 'Vencimento [F6]:'
      end
      object Label14: TLabel
        Left = 592
        Top = 40
        Width = 217
        Height = 13
        Caption = 'Descri'#231#227'o + nome do funcion'#225'rio + descri'#231#227'o:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 56
        Top = 8
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 112
        Top = 8
        Width = 472
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCarteira: TdmkEditCB
        Left = 56
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCarteiraChange
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 112
        Top = 32
        Width = 472
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 3
        dmkEditCB = EdCarteira
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGenero: TdmkEditCB
        Left = 56
        Top = 56
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGeneroChange
        DBLookupComboBox = CBGenero
        IgnoraDBLookupComboBox = False
      end
      object CBGenero: TdmkDBLookupComboBox
        Left = 112
        Top = 56
        Width = 472
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 5
        dmkEditCB = EdGenero
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPData: TdmkEditDateTimePicker
        Left = 592
        Top = 18
        Width = 106
        Height = 21
        Date = 39615.655720300900000000
        Time = 39615.655720300900000000
        TabOrder = 6
        OnChange = EdPathChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'Data'
        UpdType = utYes
      end
      object EdMes: TdmkEdit
        Left = 700
        Top = 18
        Width = 54
        Height = 21
        Alignment = taCenter
        TabOrder = 7
        FormatType = dmktfMesAno
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        QryCampo = 'Mez'
        UpdType = utYes
        Obrigatorio = True
        PermiteNulo = False
        ValueVariant = Null
        ValWarn = False
        OnChange = EdPathChange
      end
      object TPVencimento: TdmkEditDateTimePicker
        Left = 757
        Top = 18
        Width = 112
        Height = 21
        Date = 39615.672523148100000000
        Time = 39615.672523148100000000
        TabOrder = 8
        OnChange = EdPathChange
        OnKeyDown = TPVencimentoKeyDown
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'Vencimento'
        UpdType = utYes
      end
      object EdDescriA: TdmkEdit
        Left = 592
        Top = 56
        Width = 153
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPathChange
      end
      object EdDescriB: TdmkEdit
        Left = 748
        Top = 56
        Width = 121
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPathChange
      end
      object GroupBox6: TGroupBox
        Left = 876
        Top = 1
        Width = 125
        Height = 86
        Caption = '                                   '
        TabOrder = 11
        object LaDoc: TLabel
          Left = 8
          Top = 34
          Width = 104
          Height = 13
          Caption = 'S'#233'rie  e docum. (CH) :'
          Enabled = False
        end
        object EdSerieCH1: TdmkEdit
          Left = 8
          Top = 50
          Width = 41
          Height = 21
          CharCase = ecUpperCase
          Enabled = False
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'SerieCH'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdPathChange
        end
        object EdDocumento1: TdmkEdit
          Left = 51
          Top = 50
          Width = 62
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          QryCampo = 'Documento'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPathChange
        end
        object CkAtribCHManu: TCheckBox
          Left = 12
          Top = 0
          Width = 105
          Height = 17
          Caption = 'Atrib. CH manual:'
          Enabled = False
          TabOrder = 0
          OnClick = CkAtribCHManuClick
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 1006
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 545
        Height = 60
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 16
          Width = 248
          Height = 13
          Caption = 'Caminho do arquivo com os pagamentos de sal'#225'rios:'
        end
        object SpeedButton1: TSpeedButton
          Left = 516
          Top = 32
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object EdPath: TEdit
          Left = 4
          Top = 32
          Width = 509
          Height = 21
          TabOrder = 0
          OnChange = EdPathChange
        end
      end
      object GroupBox2: TGroupBox
        Left = 713
        Top = 0
        Width = 84
        Height = 60
        Align = alLeft
        Caption = ' CPF: '
        TabOrder = 3
        object Label5: TLabel
          Left = 8
          Top = 16
          Width = 17
          Height = 13
          Caption = 'Ini.:'
        end
        object Label6: TLabel
          Left = 44
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Tam.:'
        end
        object EdCPFIni: TdmkEdit
          Left = 8
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '70'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 70
          ValWarn = False
          OnChange = EdPathChange
        end
        object EdCPFTam: TdmkEdit
          Left = 44
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '14'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 14
          ValWarn = False
          OnChange = EdPathChange
        end
      end
      object GroupBox1: TGroupBox
        Left = 629
        Top = 0
        Width = 84
        Height = 60
        Align = alLeft
        Caption = ' Nome: '
        TabOrder = 2
        object Label2: TLabel
          Left = 8
          Top = 16
          Width = 17
          Height = 13
          Caption = 'Ini.:'
        end
        object Label3: TLabel
          Left = 44
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Tam.:'
        end
        object EdNomeIni: TdmkEdit
          Left = 8
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '28'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 28
          ValWarn = False
          OnChange = EdPathChange
        end
        object EdNomeTam: TdmkEdit
          Left = 44
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '42'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 42
          ValWarn = False
          OnChange = EdPathChange
        end
      end
      object GroupBox3: TGroupBox
        Left = 797
        Top = 0
        Width = 84
        Height = 60
        Align = alLeft
        Caption = ' Valor: '
        TabOrder = 4
        object Label4: TLabel
          Left = 8
          Top = 16
          Width = 17
          Height = 13
          Caption = 'Ini.:'
        end
        object Label7: TLabel
          Left = 44
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Tam.:'
        end
        object EdValorIni: TdmkEdit
          Left = 8
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '117'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 117
          ValWarn = False
          OnChange = EdPathChange
        end
        object EdValorTam: TdmkEdit
          Left = 44
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '14'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 14
          ValWarn = False
          OnChange = EdPathChange
        end
      end
      object GroupBox4: TGroupBox
        Left = 545
        Top = 0
        Width = 84
        Height = 60
        Align = alLeft
        Caption = ' Chapa: '
        TabOrder = 1
        object Label8: TLabel
          Left = 8
          Top = 16
          Width = 17
          Height = 13
          Caption = 'Ini.:'
        end
        object Label9: TLabel
          Left = 44
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Tam.:'
        end
        object EdChapaIni: TdmkEdit
          Left = 8
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
          OnChange = EdPathChange
        end
        object EdChapaTam: TdmkEdit
          Left = 44
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '9'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 9
          ValWarn = False
          OnChange = EdPathChange
        end
      end
      object GroupBox5: TGroupBox
        Left = 881
        Top = 0
        Width = 120
        Height = 60
        Align = alLeft
        Caption = ' M'#234's compet'#234'ncia: '
        TabOrder = 5
        object Label15: TLabel
          Left = 8
          Top = 16
          Width = 29
          Height = 13
          Caption = 'Linha:'
        end
        object Label16: TLabel
          Left = 44
          Top = 16
          Width = 17
          Height = 13
          Caption = 'Ini.:'
        end
        object Label17: TLabel
          Left = 80
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Tam.:'
        end
        object EdMezLin: TdmkEdit
          Left = 8
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '5'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 5
          ValWarn = False
          OnChange = EdPathChange
        end
        object EdMezIni: TdmkEdit
          Left = 44
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '15'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 15
          ValWarn = False
          OnChange = EdPathChange
        end
        object EdMezTam: TdmkEdit
          Left = 80
          Top = 32
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '7'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 7
          ValWarn = False
          OnChange = EdPathChange
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 148
      Width = 1008
      Height = 279
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 1
      ExplicitTop = 149
      ExplicitWidth = 1006
      ExplicitHeight = 248
      object TabSheet1: TTabSheet
        Caption = ' Arquivo carregado '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 251
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          WordWrap = False
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Pr'#233'-lan'#231'amentos financeiros '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 998
        ExplicitHeight = 220
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 251
          Align = alClient
          DataSource = DsLct
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Genero'
              Title.Caption = 'Conta'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CONTA'
              Title.Caption = 'Descri'#231#227'o da conta'
              Width = 207
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 363
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SerieCH'
              Title.Caption = 'S'#233'rie Ch.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Title.Caption = 'N'#186' Ch.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sit'
              ReadOnly = True
              Width = 20
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 647
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 599
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 551
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 550
        Height = 32
        Caption = 'Carrega Lan'#231'amentos de Sal'#225'rios - Modelo 1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 550
        Height = 32
        Caption = 'Carrega Lan'#231'amentos de Sal'#225'rios - Modelo 1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 550
        Height = 32
        Caption = 'Carrega Lan'#231'amentos de Sal'#225'rios - Modelo 1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 475
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 135
    ExplicitWidth = 647
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 643
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 519
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 179
    ExplicitWidth = 647
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 501
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 499
      object BtPreGera: TBitBtn
        Tag = 241
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pr'#233'-gera'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPreGeraClick
      end
      object BtConfirma: TBitBtn
        Tag = 10035
        Left = 268
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BitBtn2: TBitBtn
        Tag = 11
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtPreGeraClick
      end
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fpf.Codigo, fpf.Chapa'
      'FROM fpfunci fpf'
      'LEFT JOIN entidades ent ON ent.Codigo=fpf.Codigo'
      'WHERE fpf.Empresa=:P0'
      'AND fpf.Registro=:P1'
      'AND ent.CPF=:P2')
    Left = 292
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciChapa: TWideStringField
      FieldName = 'Chapa'
      Size = 50
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, '
      'Tipo, ExigeNumCheque, ForneceI, TipoDoc'
      'FROM carteiras '
      'WHERE ForneceI=:P0'
      'ORDER BY Nome')
    Left = 616
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 644
    Top = 12
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 672
    Top = 12
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContasNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 700
    Top = 12
  end
  object DsLct: TDataSource
    DataSet = TbLct
    Left = 136
    Top = 272
  end
  object QrEnti1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM entidades'
      'WHERE CPF=:P0'
      'ORDER BY Codigo')
    Left = 320
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnti1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrEnti2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM entidades'
      'WHERE Nome=:P0'
      'ORDER BY Codigo')
    Left = 348
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnti2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object TbLct: TmySQLTable
    Database = DModG.MyPID_DB
    AfterOpen = TbLctAfterOpen
    BeforeClose = TbLctBeforeClose
    TableName = '_lct_'
    Left = 108
    Top = 272
    object TbLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TbLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object TbLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object TbLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object TbLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object TbLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object TbLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object TbLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000'
    end
    object TbLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TbLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object TbLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object TbLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object TbLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object TbLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object TbLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object TbLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object TbLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object TbLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object TbLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object TbLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object TbLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object TbLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object TbLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object TbLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object TbLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object TbLctUnidade: TIntegerField
      FieldName = 'Unidade'
    end
    object TbLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object TbLctNO_CONTA: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_CONTA'
      LookupDataSet = QrContas
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Genero'
      Size = 50
      Lookup = True
    end
  end
end
