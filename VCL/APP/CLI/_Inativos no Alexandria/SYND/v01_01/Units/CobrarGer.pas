unit CobrarGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, frxClass, UnDmkEnums;

type
  TFmCobrarGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInadimp: TBitBtn;
    LaTitulo1C: TLabel;
    Panel11: TPanel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    DBGPesq: TdmkDBGrid;
    BtAgendar: TBitBtn;
    PB1: TProgressBar;
    BtEmails: TBitBtn;
    CkForcaPsq: TCheckBox;
    BtSMS: TBitBtn;
    Memo5: TMemo;
    BtCartas: TBitBtn;
    dmkEditDateTimePicker1: TdmkEditDateTimePicker;
    QrProtTip: TmySQLQuery;
    QrProtTipConta: TIntegerField;
    QrProtTipProtocolo: TIntegerField;
    QrProtTipTipo: TIntegerField;
    QrLista: TmySQLQuery;
    QrListaDiarCEMIts: TIntegerField;
    QrListaSMS: TIntegerField;
    QrListaEmail: TIntegerField;
    QrListaCarta: TIntegerField;
    QrListaInadCEMCad: TIntegerField;
    QrListaEmpresa: TIntegerField;
    QrListaForneceI: TIntegerField;
    QrListaApto: TIntegerField;
    QrListaMez: TIntegerField;
    QrListaVencimento: TDateField;
    QrListaFatNum: TFloatField;
    QrListaPropriet: TIntegerField;
    QrListaUnidade: TWideStringField;
    QrListaImobiliaria: TIntegerField;
    QrListaProcurador: TIntegerField;
    QrListaUsuario: TIntegerField;
    QrListaJuridico: TSmallintField;
    QrListaData: TDateField;
    QrListaCliInt: TIntegerField;
    QrListaCREDITO: TFloatField;
    QrListaPAGO: TFloatField;
    QrListaJuros: TFloatField;
    QrListaMulta: TFloatField;
    QrListaTOTAL: TFloatField;
    QrListaSALDO: TFloatField;
    QrListaPEND_VAL: TFloatField;
    QrListaDescricao: TWideStringField;
    QrListaMEZ_TXT: TWideStringField;
    QrListaCompensado: TDateField;
    QrListaControle: TIntegerField;
    QrListaVCTO_TXT: TWideStringField;
    QrListaNOMEEMPCOND: TWideStringField;
    QrListaNOMEPRPIMOV: TWideStringField;
    QrListaNewVencto: TDateField;
    QrListaAtivo: TSmallintField;
    QrListaDias: TIntegerField;
    QrListaFim: TLargeintField;
    QrListaDATA_I: TDateTimeField;
    QrListaDATA_F: TDateTimeField;
    QrListaCliente: TIntegerField;
    QrCtrlGeral: TmySQLQuery;
    QrCtrlGeralUltPsqCobr: TDateField;
    QrLocBol: TmySQLQuery;
    QrLocBolCodigo: TIntegerField;
    QrEnvSMS: TmySQLQuery;
    QrEnvSMSPropriet: TIntegerField;
    QrEnvSMSVencimento: TDateField;
    QrEnvSMSData: TDateField;
    QrEnvSMSMez: TIntegerField;
    QrEnvSMSFatNum: TFloatField;
    QrEnvSMSDepto: TIntegerField;
    QrEnvSMSCodigo: TIntegerField;
    QrEnvSMSDiarCEMIts: TIntegerField;
    QrEnvSMSMetodo: TSmallintField;
    QrEnvSMSReInclu: TIntegerField;
    QrEnvSMSCadastro: TIntegerField;
    QrEnvSMSDtaInicio: TDateField;
    QrEnvSMSDtaLimite: TDateField;
    QrEnvSMSTEXTO: TWideStringField;
    QrEnvSMSTentativas: TIntegerField;
    QrEnvSMSLastExec: TDateTimeField;
    QrEnti: TmySQLQuery;
    QrEntiUsuario: TIntegerField;
    QrEntiPropriet: TIntegerField;
    QrEntiCELULAR_USU: TWideStringField;
    QrEntiCELULAR_PRP: TWideStringField;
    QrEntiCELULAR_TER: TWideStringField;
    QrEntiSMSCelNome: TWideStringField;
    QrEntiSMSCelNumr: TWideStringField;
    QrEntiSMSCelTipo: TSmallintField;
    QrEnvEmail: TmySQLQuery;
    QrEnvEmailPropriet: TIntegerField;
    QrEnvEmailVencimento: TDateField;
    QrEnvEmailData: TDateField;
    QrEnvEmailMez: TIntegerField;
    QrEnvEmailFatNum: TFloatField;
    QrEnvEmailDepto: TIntegerField;
    QrEnvEmailCodigo: TIntegerField;
    QrEnvEmailDiarCEMIts: TIntegerField;
    QrEnvEmailMetodo: TSmallintField;
    QrEnvEmailReInclu: TIntegerField;
    QrEnvEmailCadastro: TIntegerField;
    QrEnvEmailDtaInicio: TDateField;
    QrEnvEmailDtaLimite: TDateField;
    QrEnvEmailEnvio: TDateTimeField;
    QrEnvEmailPqNoEnv: TIntegerField;
    QrEnvEmailTentativas: TIntegerField;
    QrEnvEmailLastExec: TDateTimeField;
    QrEnvEmailConsidEnvi: TDateTimeField;
    QrEmails: TmySQLQuery;
    QrEmailsPronome: TWideStringField;
    QrEmailsNome: TWideStringField;
    QrEmailsEmeio: TWideStringField;
    QrCond: TmySQLQuery;
    QrCondCodigo: TIntegerField;
    QrCondCliente: TIntegerField;
    QrPesq3: TmySQLQuery;
    QrPesq3Empresa: TIntegerField;
    QrPesq3ForneceI: TIntegerField;
    QrPesq3Apto: TIntegerField;
    QrPesq3Mez: TIntegerField;
    QrPesq3Vencimento: TDateField;
    QrPesq3FatNum: TFloatField;
    QrPesq3Propriet: TIntegerField;
    QrPesq3Unidade: TWideStringField;
    QrPesq3Imobiliaria: TIntegerField;
    QrPesq3Procurador: TIntegerField;
    QrPesq3Usuario: TIntegerField;
    QrPesq3Juridico: TSmallintField;
    QrPesq3Data: TDateField;
    QrPesq3CliInt: TIntegerField;
    QrPesq3CREDITO: TFloatField;
    QrPesq3PAGO: TFloatField;
    QrPesq3Juros: TFloatField;
    QrPesq3Multa: TFloatField;
    QrPesq3TOTAL: TFloatField;
    QrPesq3SALDO: TFloatField;
    QrPesq3PEND_VAL: TFloatField;
    QrPesq3Descricao: TWideStringField;
    QrPesq3MEZ_TXT: TWideStringField;
    QrPesq3Compensado: TDateField;
    QrPesq3Controle: TIntegerField;
    QrPesq3VCTO_TXT: TWideStringField;
    QrPesq3NOMEEMPCOND: TWideStringField;
    QrPesq3NOMEPRPIMOV: TWideStringField;
    QrPesq3NewVencto: TDateField;
    QrPesq3Ativo: TSmallintField;
    QrPesq3Juridico_TXT: TWideStringField;
    QrPesq3Juridico_DESCRI: TWideStringField;
    DsPesq3: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtInadimpClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtAgendarClick(Sender: TObject);
    procedure BtEmailsClick(Sender: TObject);
    procedure BtSMSClick(Sender: TObject);
    procedure BtCartasClick(Sender: TObject);
    procedure BtMotNaoEnvClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCobrarGer: TFmCobrarGer;

implementation

uses UnMyObjects, Module, CreateApp, ModuleGeral, ModuleCond, UMySQLModule,
DmkDAC_PF, UnDiario_Tabs, UnProtocol_Tabs, UnFinanceiro, CartasImp,
ModCobranca, UnMailEnv;

{$R *.DFM}

procedure TFmCobrarGer.BtAgendarClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DmModCobranca.AgendarEnvioDeMensagensAInadimplentes(
      wcForm, PB1, LaAviso1, LaAviso2);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCobrarGer.BtCartasClick(Sender: TObject);
var
  Hoje, Celular, Mensagem, LastExec: String;
  //CodRetorno,
  Codigo, DiarCEMIts, Metodo,
  LastLstErr, LastCodRet: Integer;
begin
  Application.CreateForm(TFmCartasImp, FmCartasImpCG);
  FmCartasImpCG.FTipoFormCartasImp := tfciCobrancaGer;
  //
  // Ser� definido a cada registro da QrEnvCarta ///////////////////////////////
  FmCartasImpCG.FCarta    := 0;                                               //
  FmCartasImpCG.FDataPesq := 0;                                               //
  FmCartasImpCG.FEmpresa  := 0;                                               //
  FmCartasImpCG.FEmprEnti := 0;                                               //
  FmCartasImpCG.FDepto    := 0;                                               //
  FmCartasImpCG.FPropriet := 0;                                               //
  FmCartasImpCG.FTabLctA  := '';                                              //
  FmCartasImpCG.FUnidadesFiltered := False;                                   //
  // Ser� definido pelo usu�rio ////////////////////////////////////////////////
  //FmCartasImpCG.CkDesign.Checked := ?;                                      //
  //////////////////////////////////////////////////////////////////////////////
  //
  // Mode a ser impresso
  FmCartasImpCG.FRGModoItemIndex := CO_INADIMPL_APENAS_COM_PENDENCIAS;
  //
  FmCartasImpCG.ReopenCondComCartas(True, 0);
  //
  FmCartasImpCG.ShowModal;
  FmCartasImpCG.Destroy;
end;

procedure TFmCobrarGer.BtEmailsClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DmModCobranca.EnviaAgendados_Email(wcForm, PB1, LaAviso1, LaAviso2);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCobrarGer.BtInadimpClick(Sender: TObject);
var
//  CondCod, CodEnt, Hoje: Integer;
//  UltPsqCobr: String;
  Empresa, Entidade: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Empresa := EdEmpresa.ValueVariant;
    if Empresa <> 0 then
      Entidade := DModG.QrEmpresasCodigo.Value
    else
      Entidade := 0;  
    DmModCobranca.VerificaInadimplencia(wcForm, PB1, LaAviso1, LaAviso2,
      CkForcaPsq.Checked, Empresa, Entidade);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCobrarGer.BtMotNaoEnvClick(Sender: TObject);
begin

 { Fazer...

 Cabe�alho: Itens enviados e n�o enviados
SELECT Metodo, IF(ConsidEnvi > "1900-01-01", 1, 0) ENVIADO,
COUNT(Codigo) ITENS
FROM diarcemenv
GROUP BY Metodo, ENVIADO


 1�: SQL para ver n�o enviados:
 ///////////////////////////////////////////////////////////////////////////////
SELECT dci.DiarCEMIts, dci.Metodo, dci.Cadastro,
dci.LastVerify, dci.AutoCancel,
dcb.Empresa, dcb.Propriet, dcb.Depto, dcb.Vencimento,
dcb.FatNum, dcb.Mez
FROM diarcemenv dci
LEFT JOIN diarcembol dcb ON dcb.Codigo=dci.Codigo
WHERE dci.AutoCancel > "1900-01-01"



 2�: SQL para ver se n�o alterou vencimento
 ///////////////////////////////////////////////////////////////////////////////
SELECT Vencimento
FROM lct0006a
WHERE ForneceI=444
AND Depto=259
AND FatNum=4
AND Mez=801

 2�, 3�, 4�.... fazer SQL para ver se n�o alterou outro item como Mez ou propriet, etc
 ///////////////////////////////////////////////////////////////////////////////
SELECT Mez
FROM lct0006a
WHERE ForneceI=444
AND Depto=259
AND FatNum=4
//AND Vencimento="2012-01-02"

 ...en�simo: Alterar tabela diarcemenv informando motivo de n�o envio

 }
end;

procedure TFmCobrarGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCobrarGer.BtSMSClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DmModCobranca.EnviaAgendados_SMS(wcForm, PB1, LaAviso1, LaAviso2, Memo5);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCobrarGer.EdEmpresaChange(Sender: TObject);
begin
  DmModCobranca.QrPesq3.Close;
end;

procedure TFmCobrarGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCobrarGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DBGPesq.DataSource := DmModCobranca.DsPesq3;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmCobrarGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
