unit uploads;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, WinProcs, WinTypes, Menus, Variants, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkGeral, UnDmkProcFunc, dmkImage, dmkDBGrid, DmkDAC_PF,
  dmkEditDateTimePicker, UnDmkEnums;

type
  TFmUploads = class(TForm)
    PainelDados: TPanel;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    LaCond: TLabel;
    EdCond: TdmkEditCB;
    CBCond: TdmkDBLookupComboBox;
    QrNomeCond: TmySQLQuery;
    DsNomeCond: TDataSource;
    QrNomeCondCodigo: TIntegerField;
    QrNomeCondNCONDOM: TWideStringField;
    Label1: TLabel;
    EdDirWeb: TdmkEditCB;
    CBDirWeb: TdmkDBLookupComboBox;
    EdNome: TdmkEdit;
    QrNomeDir: TmySQLQuery;
    DsNomeDir: TDataSource;
    QrNomeDirCodigo: TAutoIncField;
    QrNomeDirNome: TWideStringField;
    QrNomeDirPasta: TWideStringField;
    OpenDialog1: TOpenDialog;
    Label3: TLabel;
    EdArquivo: TdmkEdit;
    Panel4: TPanel;
    Label2: TLabel;
    EdCondPesq: TdmkEditCB;
    CBCondPesq: TdmkDBLookupComboBox;
    BitBtn1: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnDados: TPanel;
    Splitter1: TSplitter;
    QrUploadsCon: TmySQLQuery;
    DsUploadsCon: TDataSource;
    Splitter2: TSplitter;
    QrUploadsDir: TmySQLQuery;
    DsUploadsDir: TDataSource;
    QrUploadsConCond: TIntegerField;
    QrUploadsConNCONDOM: TWideStringField;
    QrUploadsDirDirWeb: TIntegerField;
    QrUploadsDirDIRNOME: TWideStringField;
    QrUploads: TmySQLQuery;
    DsUploads: TDataSource;
    QrUploadsCodigo: TAutoIncField;
    QrUploadsNome: TWideStringField;
    QrUploadsArquivo: TWideStringField;
    QrUploadsCond: TIntegerField;
    QrUploadsLk: TIntegerField;
    QrUploadsDataCad: TDateField;
    QrUploadsDataAlt: TDateField;
    QrUploadsUserCad: TIntegerField;
    QrUploadsUserAlt: TIntegerField;
    QrUploadsAlterWeb: TSmallintField;
    QrUploadsAtivo: TSmallintField;
    QrUploadsDirWeb: TIntegerField;
    QrUploadsNDIRWEB: TWideStringField;
    QrUploadsDIRNOME: TWideStringField;
    QrUploadsNCONDOM: TWideStringField;
    QrUploadsDirCond: TIntegerField;
    Panel6: TPanel;
    DBGArquivos: TdmkDBGrid;
    StaticText1: TStaticText;
    Panel7: TPanel;
    DBGDiretorios: TdmkDBGrid;
    StaticText2: TStaticText;
    Panel8: TPanel;
    DBGCondominios: TdmkDBGrid;
    StaticText3: TStaticText;
    TPDataExp: TdmkEditDateTimePicker;
    Label4: TLabel;
    QrUploadsDataExp: TDateField;
    QrUploadsDataExp_TXT: TWideStringField;
    CkExpirados: TCheckBox;
    QrLoc: TmySQLQuery;
    PB1: TProgressBar;
    PMExclui: TPopupMenu;
    Excluiselecionados1: TMenuItem;
    ExcluiExpirados1: TMenuItem;
    ImgWEB: TdmkImage;
    QrFTPConfig: TmySQLQuery;
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure EdArquivoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrUploadsConAfterScroll(DataSet: TDataSet);
    procedure QrUploadsConBeforeClose(DataSet: TDataSet);
    procedure QrUploadsDirAfterScroll(DataSet: TDataSet);
    procedure QrUploadsDirBeforeClose(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure Excluiselecionados1Click(Sender: TObject);
    procedure ExcluiExpirados1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
   //Procedures do form
    procedure MostraEdicao(Mostra, Externo: Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure ReopenUploadsCon(Cond, CondLoc: Integer);
    procedure ReopenUploadsDir(Cond, Diretorio: Integer);
    procedure ReopenUploads(Cond, Diretorio, Codigo: Integer);
    //
    procedure ExcluiArquivosExpirados(Diretorio: Integer);
    function  ExcluiRegistro(Codigo: Integer; Arquivo, Nome, DirWeb: String;
                var Cond, CodDirWeb: Integer): Boolean;
  public
    { Public declarations }
    FCondominio, FDiretorio: Integer;
    FArquivo, FDescricao: String;
    FExcluiArqOrigem: Boolean;
  end;

var
  FmUploads: TFmUploads;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects, ModuleGeral, UnDmkWeb, UnFTP;

{$R *.DFM}

const
  DefCaption = 'FTP: Ftp Cliente';

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmUploads.MostraEdicao(Mostra, Externo: Boolean; SQLType: TSQLType; Codigo : Integer);
begin
  PB1.Visible := False;
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    BtConfirma.Enabled  := True;
    //
    if SQLType = stIns then
    begin
      if Externo then //Aberto por outra janela
      begin
        EdCodigo.ValueVariant  := 0;
        EdNome.ValueVariant    := FDescricao;
        EdArquivo.ValueVariant := FArquivo;
        EdCond.ValueVariant    := FCondominio;
        CBCond.KeyValue        := FCondominio;
        EdDirWeb.ValueVariant  := FDiretorio;
        CBDirWeb.KeyValue      := FDiretorio;
        TPDataExp.Date         := 0;
      end else
      begin
        EdCodigo.ValueVariant  := 0;
        EdNome.ValueVariant    := '';
        EdArquivo.ValueVariant := '';
        EdCond.ValueVariant    := '';
        CBCond.KeyValue        := Null;
        EdDirWeb.ValueVariant  := '';
        CBDirWeb.KeyValue      := Null;
        TPDataExp.Date         := 0;
      end;
      EdArquivo.Enabled   := True;
      EdDirWeb.Enabled    := True;
      CBDirWeb.Enabled    := True;
      CkExpirados.Checked := True;
      CkExpirados.Visible := True;
    end else begin
      EdCodigo.ValueVariant  := QrUploadsCodigo.Value;
      EdNome.ValueVariant    := QrUploadsNome.Value;
      EdArquivo.ValueVariant := QrUploadsArquivo.Value;
      EdCond.ValueVariant    := IntToStr(QrUploadsCond.Value);
      CBCond.KeyValue        := QrUploadsCond.Value;
      EdDirWeb.ValueVariant  := IntToStr(QrUploadsDirWeb.Value);
      CBDirWeb.KeyValue      := QrUploadsDirWeb.Value;
      TPDataExp.Date         := QrUploadsDataExp.Value;
      //
      EdArquivo.Enabled   := False;
      EdDirWeb.Enabled    := False;
      CBDirWeb.Enabled    := False;
      CkExpirados.Checked := False;
      CkExpirados.Visible := False;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmUploads.PMExcluiPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrUploads.State <> dsInactive) and (QrUploads.RecordCount > 0);
  //
  Excluiselecionados1.Enabled := Enab;
  ExcluiExpirados1.Enabled    := Enab;
end;

procedure TFmUploads.QrUploadsConAfterScroll(DataSet: TDataSet);
begin
  ReopenUploadsDir(QrUploadsConCond.Value, 0);
end;

procedure TFmUploads.QrUploadsConBeforeClose(DataSet: TDataSet);
begin
  QrUploadsDir.Close;
end;

procedure TFmUploads.QrUploadsDirAfterScroll(DataSet: TDataSet);
begin
  ReopenUploads(QrUploadsDirCond.Value, QrUploadsDirDirWeb.Value, 0);
end;

procedure TFmUploads.QrUploadsDirBeforeClose(DataSet: TDataSet);
begin
  QrUploads.Close;
end;

procedure TFmUploads.ReopenUploads(Cond, Diretorio, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUploads, Dmod.MyDBn, [
  'SELECT upl.*, dir.Pasta NDIRWEB, dir.Nome DIRNOME, ',
  'IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM, ',
  'IF (upl.DataExp < 2, "", DATE_FORMAT(upl.DataExp, "%d/%m/%Y")) DataExp_TXT ',
  'FROM uploads upl ',
  'LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb ',
  'LEFT JOIN cond con ON con.Codigo=upl.Cond ',
  'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente ',
  'WHERE upl.DirWeb=' + Geral.FF0(Diretorio),
  'AND upl.Cond=' + Geral.FF0(Cond),
  'ORDER BY upl.DataCad DESC, upl.Nome ASC ',
  '']);
  if Codigo > 0 then
    QrUploads.Locate('Codigo', Codigo, []);
end;

procedure TFmUploads.ReopenUploadsCon(Cond, CondLoc: Integer);
var
  SQL: String;
begin
  if Cond <> 0 then
    SQL := 'WHERE upl.Cond=' + Geral.FF0(Cond)
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrUploadsCon, Dmod.MyDBn, [
  'SELECT upl.Cond, ', 
  'IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM ',
  'FROM uploads upl ',
  'LEFT JOIN cond con ON con.Codigo=upl.Cond ',
  'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente ',
  SQL,
  'GROUP BY upl.Cond ',
  'ORDER BY NCONDOM ', 
  '']);
  if CondLoc > 0 then
    QrUploadsCon.Locate('Cond', CondLoc, []);
end;

procedure TFmUploads.ReopenUploadsDir(Cond, Diretorio: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUploadsDir, Dmod.MyDBn, [
  'SELECT upl.DirWeb, dir.Nome DIRNOME, upl.Cond ',
  'FROM uploads upl ',
  'LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb ',
  'WHERE upl.Cond = ' + Geral.FF0(Cond),
  'GROUP BY DIRNOME ',
  'ORDER BY DIRNOME ',
  '']);
  if Diretorio > 0 then
    QrUploadsDir.Locate('DirWeb', Diretorio, []);
end;

procedure TFmUploads.CriaOForm;
begin
  DefineONomeDoForm;
end;

procedure TFmUploads.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmUploads.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True, False, stIns, 0);
end;

procedure TFmUploads.BtSaidaClick(Sender: TObject);
begin
  VAR_SERVICOG := QrUploadsCodigo.Value;
  Close;
end;

procedure TFmUploads.BtConfirmaClick(Sender: TObject);
var
  DataExp, Raiz, Nome, Arquivo, Pasta, R, NomeArq, Host, User, Pass: String;
  Codigo, Cond, DirWeb : Integer;
  Passivo, Continua: Boolean;
begin
  //Faz upload do arquivo
  UFTP.ReopenFTPConfig(-1, QrFTPConfig, Dmod.MyDB);
  //
  Codigo  := EdCodigo.ValueVariant;
  Nome    := EdNome.ValueVariant;
  Arquivo := EdArquivo.ValueVariant;
  Cond    := EdCond.ValueVariant;
  DirWeb  := EdDirWeb.ValueVariant;
  Pasta   := QrNomeDirPasta.Value;
  NomeArq := ExtractFileName(Arquivo);
  Raiz    := QrFTPConfig.FieldByName('Web_Raiz').AsString;
  DataExp := Geral.FDT(TPDataExp.Date, 1);
  R       := '/' + Raiz + '/_local_/pdf/' + QrNomeDirPasta.Value + '/';
  Host    := QrFTPConfig.FieldByName('Web_FTPh').AsString;
  User    := QrFTPConfig.FieldByName('Web_FTPu').AsString;
  Pass    := QrFTPConfig.FieldByName('Web_FTPs').AsString;
  Passivo := Geral.IntToBool(QrFTPConfig.FieldByName('Passivo').AsInteger);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Cond = 0, EdCond, 'Defina o condom�nio!') then Exit;
  if MyObjects.FIC(DirWeb = 0, EdDirWeb, 'Defina o diret�rio WEB!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
  begin
    if MyObjects.FIC(Length(Arquivo) = 0, EdArquivo, 'Arquivo n�o definido!') then Exit;
    //
    Continua := DmkWeb.UploadFTP(Arquivo, NomeArq, R, Host, User, Pass, Passivo,
                  FExcluiArqOrigem);
    //
    if not Continua then
    begin
      Geral.MensagemBox('Falha ao realizar upload!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end else
    Continua := True;
  //
  if Continua then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      Continua := UMyMod.SQLInsUpd(Dmod.QrUpdN, ImgTipo.SQLType, 'uploads', True,
        ['Nome', 'Arquivo', 'Cond', 'DirWeb', 'DataExp'], [],
        [Nome, NomeArq, Cond, DirWeb, DataExp], [], True);
      //
      Codigo := Dmod.QrUpdN.LastInsertID;
    end else
    begin
      Continua := UMyMod.SQLInsUpd(Dmod.QrUpdN, ImgTipo.SQLType, 'uploads', False,
        ['Nome', 'Cond', 'DataExp'], ['Codigo'],
        [Nome, Cond, DataExp], [Codigo], True);
    end;
    if Continua then
    begin
      if (ImgTipo.SQLType = stIns) and (CkExpirados.Checked) then 
        //ExcluiArquivosExpirados(DirWeb);
      ReopenUploadsCon(0, Cond);
      ReopenUploadsDir(Cond, DirWeb);
      ReopenUploads(Cond, DirWeb, Codigo);
      //
      MostraEdicao(False, False, stLok, 0);
    end;
  end;
end;

procedure TFmUploads.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(False, False, stLok, 0);
end;

procedure TFmUploads.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmUploads.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrNomeCond, Dmod.MyDBn);
  UMyMod.AbreQuery(QrNomeDir, Dmod.MyDBn);
  //
  ReopenUploadsCon(0, 0);
  //
  PnDados.Align     := alClient;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  //
  FExcluiArqOrigem := False;
  //
  CriaOForm;
end;

procedure TFmUploads.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmUploads.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmUploads.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUploads.FormShow(Sender: TObject);
begin
  if (FCondominio <> 0) and (FDiretorio <> 0) and (FArquivo <> '') and
    (FDescricao <> '')
  then
  MostraEdicao(True, True, stIns, 0);
end;

procedure TFmUploads.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(True, False, stUpd, 0);
end;

procedure TFmUploads.EdArquivoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenDialog1.Execute then
      EdArquivo.ValueVariant := OpenDialog1.FileName;
  end;
end;

function TFmUploads.ExcluiRegistro(Codigo: Integer; Arquivo, Nome,
  DirWeb: String; var Cond, CodDirWeb: Integer): Boolean;
var
  Raiz, R, L, Host, User, Pass: String;
  Continua, Passivo: Boolean;
begin
  if MyObjects.FIC(Codigo = 0, nil, 'C�digo n�o definido!') then Exit;
  if MyObjects.FIC(Arquivo = '', nil, 'Arquivo n�o definido!') then Exit;
  if MyObjects.FIC(Nome = '', nil, 'Nome n�o definido!') then Exit;
  if MyObjects.FIC(DirWeb = '', nil, 'Nome diret�rio WEB n�o definido!') then Exit;
  if MyObjects.FIC(Cond = 0, nil, 'Condom�nio n�o definido!') then Exit;
  if MyObjects.FIC(CodDirWeb = 0, nil, 'C�digo do diret�rio WEB n�o definido!') then Exit;  
  //
  uFTP.ReopenFTPConfig(-1, QrFTPConfig, Dmod.MyDB);
  //
  Host    := QrFTPConfig.FieldByName('Web_FTPh').AsString;
  User    := QrFTPConfig.FieldByName('Web_FTPu').AsString;
  Pass    := QrFTPConfig.FieldByName('Web_FTPs').AsString;
  Passivo := Geral.IntToBool(QrFTPConfig.FieldByName('Passivo').AsInteger);
  //
  Continua := False;
  L        := Arquivo;
  Raiz     := QrFTPConfig.FieldByName('Web_Raiz').AsString;
  R        := Raiz + '/_local_/pdf/' + DirWeb + '/' + L;
  //
  //Faz upload do arquivo
  if DmkWeb.VerificaSeArquivoFTPExiteServidor(R, Host, User, Pass, Passivo) then //Verifica se arquivo existe no servidor FTP
  begin
    //Exclui arquivo do servidor FTP
    Continua := DmkWeb.FTPDeleteArquivo(Geral.Substitui(R, ('/' + L), ''), L,
                  Host, User, Pass, Passivo);
    //
    if not Continua then
    begin
      Geral.MensagemBox('Falha ao remover arquivo do servidor FTP!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      Exit;
    end;      
  end else
    Continua := True;
  //
  if Continua then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      Dmod.QrUpdN.SQL.Clear;
      Dmod.QrUpdN.SQL.Add('DELETE FROM uploads WHERE Codigo=:P0');
      Dmod.QrUpdN.Params[0].AsInteger := Codigo;
      Dmod.QrUpdN.ExecSQL;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  Result := Continua;
end;

procedure TFmUploads.Excluiselecionados1Click(Sender: TObject);
  procedure ReabreTabelas(Cond, CodDirWeb: Integer);
  begin
    //ReopenUploadsCon(0, Cond);
    //ReopenUploadsDir(Cond, CodDirWeb);
    ReopenUploads(Cond, CodDirWeb, 0);
  end;
var
  i, Cond, CodDirWeb: Integer;
begin
  if DBGArquivos.SelectedRows.Count > 1 then
  begin
    if Geral.MensagemBox('Confirma a exclus�o dos �tens selecionados?' + #13#10 +
      'ATEN��O: Ao excluir o item, este tamb�m ser� exclu�do do servidor FTP!',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES
    then
      Exit;
    with DBGArquivos.DataSource.DataSet do
    begin
      for i:= 0 to DBGArquivos.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGArquivos.SelectedRows.Items[i]));
        try
          Screen.Cursor := crHourGlass;
          //
          Cond      := QrUploadsCond.Value;
          CodDirWeb := QrUploadsDirWeb.Value;
          //
          if not ExcluiRegistro(QrUploadsCodigo.Value, QrUploadsArquivo.Value,
            QrUploadsNome.Value, QrUploadsNDIRWEB.Value, Cond, CodDirWeb) then
          begin
            Geral.MensagemBox('Falha ao excluir arquivo!', 'Aviso',
              MB_OK+MB_ICONWARNING);
            Exit;
          end;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
    ReabreTabelas(Cond, CodDirWeb);
  end else
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      if Geral.MensagemBox('Confirma a exclus�o do �tem ' + QrUploadsNome.Value + '?'
        + #13#10 + 'ATEN��O: Ao excluir o item, este tamb�m ser� exclu�do do servidor FTP!',
        'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Cond      := QrUploadsCond.Value;
        CodDirWeb := QrUploadsDirWeb.Value;
        //
        if not ExcluiRegistro(QrUploadsCodigo.Value, QrUploadsArquivo.Value,
          QrUploadsNome.Value, QrUploadsNDIRWEB.Value, Cond, CodDirWeb) then
        begin
          Geral.MensagemBox('Falha ao excluir arquivo!', 'Aviso',
            MB_OK+MB_ICONWARNING);
          Exit;
        end;
        //
        ReabreTabelas(Cond, CodDirWeb);
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmUploads.ExcluiArquivosExpirados(Diretorio: Integer);
var
  Cond, CodDirWeb: Integer;
  SQL: String;
begin
  if Diretorio <> 0 then
    SQL := 'AND upl.DirWeb=' + Geral.FF0(Diretorio)
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
  'SELECT upl.*, dir.Nome DIRNOME ',
  'FROM uploads upl ',
  'LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb ',
  'WHERE upl.DataExp > 2 ',
  SQL,
  'AND upl.DataExp < "' + Geral.FDT(DmodG.ObtemAgora(), 1) + '"',
  '']);
  if QrLoc.RecordCount > 0 then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      PB1.Position := 0;
      PB1.Max      := QrLoc.RecordCount;
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Excluindo itens expirados!');
      //
      QrLoc.First;
      while not QrLoc.Eof do
      begin
        Cond      := QrLoc.FieldByName('Cond').AsInteger;
        CodDirWeb := QrLoc.FieldByName('DirWeb').AsInteger;
        //
        if not ExcluiRegistro(QrLoc.FieldByName('Codigo').AsInteger,
          QrLoc.FieldByName('Arquivo').AsString, QrLoc.FieldByName('Nome').AsString,
          QrLoc.FieldByName('DIRNOME').AsString, Cond, CodDirWeb) then
        begin
          Geral.MensagemBox('Falha ao remover arquivo do servidor FTP!', 'Aviso',
            MB_OK+MB_ICONWARNING);
          Exit;
        end;
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        QrLoc.Next;
      end;
      ReopenUploadsCon(0, Cond);
      ReopenUploadsDir(Cond, CodDirWeb);
      ReopenUploads(Cond, CodDirWeb, 0);
    finally
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmUploads.ExcluiExpirados1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o de todos os arquivos expirados?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
  then
    ExcluiArquivosExpirados(0);
end;

procedure TFmUploads.BitBtn1Click(Sender: TObject);
var
  Cond: Integer;
begin
  Cond := EdCondPesq.ValueVariant;
  //
  ReopenUploadsCon(Cond, 0);
end;

end.


