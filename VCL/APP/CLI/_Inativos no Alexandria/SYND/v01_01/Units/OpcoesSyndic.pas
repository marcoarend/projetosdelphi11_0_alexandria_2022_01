unit OpcoesSyndic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, DBCtrls, dmkEdit, Db, UnMLAGeral,
  mySQLDbTables, dmkGeral, dmkDBLookupComboBox, dmkEditCB, Grids, DBGrids,
  dmkDBGridDAC, dmkDBGrid, dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmOpcoesSyndic = class(TForm)
    PageControl1: TPageControl;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    dmkEdProLaINSSr: TdmkEdit;
    dmkEdProLaINSSp: TdmkEdit;
    GroupBox4: TGroupBox;
    CkLookMedAnt: TCheckBox;
    GroupBox5: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    EdDdAutConfMail: TdmkEdit;
    QrPreEmail: TmySQLQuery;
    DsPreEmail: TDataSource;
    Panel1: TPanel;
    GroupBox6: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdBLQ_MEsqDestin: TdmkEdit;
    EdBLQ_TopoDestin: TdmkEdit;
    EdBLQ_MEsqAvisoV: TdmkEdit;
    EdBLQ_TopoAvisoV: TdmkEdit;
    EdBLQ_AltuAvisoV: TdmkEdit;
    EdBLQ_AltuDestin: TdmkEdit;
    EdBLQ_LargAvisoV: TdmkEdit;
    EdBLQ_LargDestin: TdmkEdit;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit3: TdmkEdit;
    dmkEdit5: TdmkEdit;
    dmkEdit7: TdmkEdit;
    dmkEdit8: TdmkEdit;
    dmkEdit9: TdmkEdit;
    dmkEdit10: TdmkEdit;
    dmkEdit11: TdmkEdit;
    dmkEdit12: TdmkEdit;
    dmkEdBAL_TopoNomCon: TdmkEdit;
    dmkEdBAL_TopoTitulo: TdmkEdit;
    dmkEdBAL_TopoPeriod: TdmkEdit;
    TabSheet4: TTabSheet;
    Panel3: TPanel;
    GroupBox39: TGroupBox;
    Label211: TLabel;
    Label212: TLabel;
    Label213: TLabel;
    Label214: TLabel;
    Label215: TLabel;
    Label210: TLabel;
    Label209: TLabel;
    Label208: TLabel;
    Label207: TLabel;
    Label206: TLabel;
    EdFlxB_Folha: TdmkEdit;
    EdFlxB_Proto: TdmkEdit;
    EdFlxB_Relat: TdmkEdit;
    EdFlxB_EMail: TdmkEdit;
    EdFlxB_Porta: TdmkEdit;
    EdFlxB_Postl: TdmkEdit;
    EdFlxB_Print: TdmkEdit;
    EdFlxB_Risco: TdmkEdit;
    EdFlxB_LeiAr: TdmkEdit;
    EdFlxB_Fecha: TdmkEdit;
    GroupBox40: TGroupBox;
    Label217: TLabel;
    Label218: TLabel;
    Label222: TLabel;
    Label221: TLabel;
    Label220: TLabel;
    Label219: TLabel;
    EdFlxM_Conci: TdmkEdit;
    EdFlxM_Docum: TdmkEdit;
    EdFlxM_Entrg: TdmkEdit;
    EdFlxM_Encad: TdmkEdit;
    EdFlxM_Contb: TdmkEdit;
    EdFlxM_Anali: TdmkEdit;
    TabSheet5: TTabSheet;
    Panel4: TPanel;
    RGContasMesEmiss: TRadioGroup;
    TabSheet6: TTabSheet;
    TbUserTxts: TmySQLTable;
    DsUserTxts: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    TbUserTxtsCodigo: TIntegerField;
    TbUserTxtsTxtSys: TWideStringField;
    TbUserTxtsTxtMeu: TWideStringField;
    TbUserTxtsOrdem: TIntegerField;
    TabSheet7: TTabSheet;
    Panel5: TPanel;
    Label22: TLabel;
    EdCB4_Host: TdmkEdit;
    Label23: TLabel;
    EdCB4_User: TdmkEdit;
    Label24: TLabel;
    EdCB4_Pwd: TdmkEdit;
    Label25: TLabel;
    EdCB4_DB: TdmkEdit;
    RGCB4_Uso: TRadioGroup;
    Label26: TLabel;
    EdCB4_Port: TdmkEdit;
    EdFlxM_Web: TdmkEdit;
    Label27: TLabel;
    Label21: TLabel;
    EdPreMailReenv: TdmkEditCB;
    CBPreMailReenv: TdmkDBLookupComboBox;
    Label31: TLabel;
    EdEntiCargSind: TdmkEditCB;
    CBEntiCargSind: TdmkDBLookupComboBox;
    QrEntiCargos: TmySQLQuery;
    DsEntiCargos: TDataSource;
    QrEntiCargosCodigo: TIntegerField;
    QrEntiCargosCodUsu: TIntegerField;
    QrEntiCargosNome: TWideStringField;
    VUEntiCargSind: TdmkValUsu;
    GroupBox7: TGroupBox;
    EdDdPrtstCrt: TdmkEdit;
    Label36: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel6: TPanel;
    BtOK: TBitBtn;
    TabSheet8: TTabSheet;
    Panel7: TPanel;
    GroupBox8: TGroupBox;
    Panel8: TPanel;
    Label37: TLabel;
    EdSMSCelCOM: TdmkEdit;
    SBSMSCelCOM: TSpeedButton;
    EdSMSCelPIN: TdmkEdit;
    Label38: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdBLQ_LargDestinExit(Sender: TObject);
    procedure TbUserTxtsBeforePost(DataSet: TDataSet);
    procedure BtSaidaClick(Sender: TObject);
    procedure SBSMSCelCOMClick(Sender: TObject);
  private
    { Private declarations }
    procedure  VerificaPasta(Dir: String);
  public
    { Public declarations }
  end;

  var
  FmOpcoesSyndic: TFmOpcoesSyndic;

implementation

uses UnMyObjects, Module, UnInternalConsts, PertoChek, UMySQLModule, UnFTP,
UnMyPrinters, Principal, DmkDAC_PF, COMSelPorta, MyDBCheck, ModuleGeral;

{$R *.DFM}

procedure TFmOpcoesSyndic.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  dmkDBGrid1.Options := dmkDBGrid1.Options + [dgEditing];
end;

procedure TFmOpcoesSyndic.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesSyndic.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  //
  PageControl1.ActivePageIndex := 0;
  //
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
  UMyMod.AbreQuery(QrPreEmail, Dmod.MyDB);
  UMyMod.abrequery(QrEntiCargos, Dmod.MyDB);
  EdBLQ_MEsqDestin.Text := Geral.FFT(Dmod.QrControleBLQ_MEsqDestin.Value, 0, siNegativo);
  EdBLQ_TopoDestin.Text := Geral.FFT(Dmod.QrControleBLQ_TopoDestin.Value, 0, siNegativo);
  EdBLQ_AltuDestin.Text := Geral.FFT(Dmod.QrControleBLQ_AltuDestin.Value, 0, siNegativo);
  EdBLQ_LargDestin.Text := Geral.FFT(Dmod.QrControleBLQ_LargDestin.Value, 0, siNegativo);

  EdBLQ_MEsqAvisoV.Text := Geral.FFT(Dmod.QrControleBLQ_MEsqAvisoV.Value, 0, siNegativo);
  EdBLQ_TopoAvisoV.Text := Geral.FFT(Dmod.QrControleBLQ_TopoAvisoV.Value, 0, siNegativo);
  EdBLQ_AltuAvisoV.Text := Geral.FFT(Dmod.QrControleBLQ_AltuAvisoV.Value, 0, siNegativo);
  EdBLQ_LargAvisoV.Text := Geral.FFT(Dmod.QrControleBLQ_LargAvisoV.Value, 0, siNegativo);
  //
  dmkEdBAL_TopoNomCon.ValueVariant := Dmod.QrControleBAL_TopoNomCon.Value;
  dmkEdBAL_TopoTitulo.ValueVariant := Dmod.QrControleBAL_TopoTitulo.Value;
  dmkEdBAL_TopoPeriod.ValueVariant := Dmod.QrControleBAL_TopoPeriod.Value;
  //
  dmkEdProLaINSSp.ValueVariant := Dmod.QrControleProLaINSSp.Value;
  dmkEdProLaINSSr.ValueVariant := Dmod.QrControleProLaINSSr.Value;
  CkLookMedAnt.Checked         := MLAGeral.IntToBool(Dmod.QrControleLokMedAnt.Value);
  EdDdAutConfMail.ValueVariant := Dmod.QrControleDdAutConfMail.Value;
  EdPreMailReenv.ValueVariant  := Dmod.QrControlePreMailReenv.Value;
  CBPreMailReenv.KeyValue      := Dmod.QrControlePreMailReenv.Value;
  EdFlxB_Folha.ValueVariant    := Dmod.QrControleFlxB_Folha.Value;
  EdFlxB_LeiAr.ValueVariant    := Dmod.QrControleFlxB_LeiAr.Value;
  EdFlxB_Fecha.ValueVariant    := Dmod.QrControleFlxB_Fecha.Value;
  EdFlxB_Risco.ValueVariant    := Dmod.QrControleFlxB_Risco.Value;
  EdFlxB_Print.ValueVariant    := Dmod.QrControleFlxB_Print.Value;
  EdFlxB_Proto.ValueVariant    := Dmod.QrControleFlxB_Proto.Value;
  EdFlxB_Relat.ValueVariant    := Dmod.QrControleFlxB_Relat.Value;
  EdFlxB_EMail.ValueVariant    := Dmod.QrControleFlxB_EMail.Value;
  EdFlxB_Porta.ValueVariant    := Dmod.QrControleFlxB_Porta.Value;
  EdFlxB_Postl.ValueVariant    := Dmod.QrControleFlxB_Postl.Value;
  EdFlxM_Conci.ValueVariant    := Dmod.QrControleFlxM_Conci.Value;
  EdFlxM_Docum.ValueVariant    := Dmod.QrControleFlxM_Docum.Value;
  EdFlxM_Anali.ValueVariant    := Dmod.QrControleFlxM_Anali.Value;
  EdFlxM_Contb.ValueVariant    := Dmod.QrControleFlxM_Contb.Value;
  EdFlxM_Encad.ValueVariant    := Dmod.QrControleFlxM_Encad.Value;
  EdFlxM_Entrg.ValueVariant    := Dmod.QrControleFlxM_Entrg.Value;
  EdFlxM_Web.ValueVariant      := Dmod.QrControleFlxM_Web.Value;
{
  EdTxtCnd_10.Text             := Dmod.QrControleTxtCnd_10.Value;
  EdTxtCnd_05.Text             := Dmod.QrControleTxtCnd_05.Value;
  EdTxtUsu_10.Text             := Dmod.QrControleTxtUsu_10.Value;
  EdTxtPrp_15.Text             := Dmod.QrControleTxtPrp_15.Value;
  EdTxtRis_25.Text             := Dmod.QrControleTxtRis_25.Value;
  EdTxtImb_15.Text             := Dmod.QrControleTxtImb_15.Value;
  EdTxtMor_15.Text             := Dmod.QrControleTxtMor_15.Value;
  EdTxtMor_20.Text             := Dmod.QrControleTxtMor_20.Value;
  EdTxtUHs_20.Text             := Dmod.QrControleTxtUHs_20.Value;
  EdTxtUHs_10.Text             := Dmod.QrControleTxtUHs_10.Value;
  EdTxtUHs_12.Text             := Dmod.QrControleTxtUHs_12.Value;
  EdTxtUHs_03.Text             := Dmod.QrControleTxtUHs_03.Value;
  EdTxtUHs_05.Text             := Dmod.QrControleTxtUHs_05.Value;
  EdTxtBlc_10.Text             := Dmod.QrControleTxtBlc_10.Value;
  EdTxtBlc_11.Text             := Dmod.QrControleTxtBlc_11.Value;
  EdTxtNiv_10.Text             := Dmod.QrControleTxtNiv_10.Value;
  EdTxtNiv_11.Text             := Dmod.QrControleTxtNiv_11.Value;
  EdTxtSin_10.Text             := Dmod.QrControleTxtSin_10.Value;
}
  //
  EdCB4_Host.Text              := Dmod.QrControleCB4_Host.Value;
  EdCB4_Port.ValueVariant      := Dmod.QrControleCB4_Port.Value;
  EdCB4_User.Text              := Dmod.QrControleCB4_User.Value;
  EdCB4_Pwd.Text               := Dmod.QrControleCB4_Pwd.Value;
  EdCB4_DB.Text                := Dmod.QrControleCB4_DB.Value;
  RGCB4_Uso.ItemIndex          := Dmod.QrControleCB4_Uso.Value;
  //
  VUEntiCargSind.ValueVariant := Dmod.QrControleEntiCargSind.Value;
  //
  RGContasMesEmiss.ItemIndex   := FmPrincipal.FContasMesEmiss;
  //
  EdDdPrtstCrt.ValueVariant    := Dmod.QrControleDdPrtstCrt.Value;
{
  EdSMSCelCOM.ValueVariant     := DModG.QrOpcoesGerlSMSCelCOM.Value;
  EdSMSCelPIN.ValueVariant     := DModG.QrOpcoesGerlSMSCelPIN.Value;
}
  //
  UMyMod.AbreTable(TBUserTxts, Dmod.MyDB);
end;

procedure TFmOpcoesSyndic.BtOKClick(Sender: TObject);
var
  EntiCargSind, DdPrtstCrt: Integer;
  Caminho(*, SMSCelCOM, SMSCelPIN*): String;
  Retorno: AnsiString;
begin
  if EdEntiCargSind.ValueVariant = 0 then
    EntiCargSind := 0
  else
    EntiCargSind := VUEntiCargSind.ValueVariant;
  //
  Geral.WriteAppKeyCU('ContasMesEmiss_Config', Application.Title,
    RGContasMesEmiss.ItemIndex, ktInteger);
  FmPrincipal.FContasMesEmiss :=
    Geral.ReadAppKeyCU('ContasMesEmiss_Config', Application.Title, ktInteger, 1);
  //
  DdPrtstCrt := EdDdPrtstCrt.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'Controle', False,
  ['BLQ_MEsqDestin', 'BLQ_TopoDestin', 'BLQ_AltuDestin', 'BLQ_LargDestin',
  'BLQ_MEsqAvisoV', 'BLQ_TopoAvisoV', 'BLQ_AltuAvisoV', 'BLQ_LargAvisoV',
  'BAL_TopoNomCon', 'BAL_TopoTitulo', 'BAL_TopoPeriod',
  'ProLaINSSp', 'ProLaINSSr', 'LokMedAnt',
  'DdAutConfMail', 'PreMailReenv',
  'FlxB_Folha', 'FlxB_LeiAr', 'FlxB_Fecha',
  'FlxB_Risco', 'FlxB_Print', 'FlxB_Proto',
  'FlxB_Relat', 'FlxB_EMail', 'FlxB_Porta',
  'FlxB_Postl', 'FlxM_Conci',
  'FlxM_Docum', 'FlxM_Anali', 'FlxM_Contb',
  'FlxM_Encad', 'FlxM_Entrg', 'FlxM_Web',
  (*, 'TxtCnd_10',
  'TxtCnd_05', 'TxtUsu_10', 'TxtPrp_15',
  'TxtRis_25', 'TxtImb_15', 'TxtMor_15', 'TxtMor_20',
  'TxtUHs_20', 'TxtUHs_10', 'TxtUHs_12',
  'TxtUHs_03', 'TxtUHs_05', 'TxtBlc_10',
  'TxtBlc_11', 'TxtNiv_10', 'TxtNiv_11',
  'TxtSin_10'*)
  'CB4_Host', 'CB4_Port', 'CB4_User',
  'CB4_Pwd', 'CB4_DB', 'CB4_Uso',
  'EntiCargSind', 'DdPrtstCrt'
  ], ['Codigo'], [
  Geral.IMV(EdBLQ_MEsqDestin.Text),
  Geral.IMV(EdBLQ_TopoDestin.Text),
  Geral.IMV(EdBLQ_AltuDestin.Text),
  Geral.IMV(EdBLQ_LargDestin.Text),

  Geral.IMV(EdBLQ_MEsqAvisoV.Text),
  Geral.IMV(EdBLQ_TopoAvisoV.Text),
  Geral.IMV(EdBLQ_AltuAvisoV.Text),
  Geral.IMV(EdBLQ_LargAvisoV.Text),

  dmkEdBAL_TopoNomCon.ValueVariant,
  dmkEdBAL_TopoTitulo.ValueVariant,
  dmkEdBAL_TopoPeriod.ValueVariant,

  dmkEdProLaINSSp.ValueVariant, dmkEdProLaINSSr.ValueVariant,
  CkLookMedAnt.Checked,
  EdDdAutConfMail.ValueVariant, EdPreMailReenv.ValueVariant,
  EdFlxB_Folha.ValueVariant, EdFlxB_LeiAr.ValueVariant, EdFlxB_Fecha.ValueVariant,
  EdFlxB_Risco.ValueVariant, EdFlxB_Print.ValueVariant, EdFlxB_Proto.ValueVariant,
  EdFlxB_Relat.ValueVariant, EdFlxB_EMail.ValueVariant, EdFlxB_Porta.ValueVariant,
  EdFlxB_Postl.ValueVariant, EdFlxM_Conci.ValueVariant,
  EdFlxM_Docum.ValueVariant, EdFlxM_Anali.ValueVariant, EdFlxM_Contb.ValueVariant,
  EdFlxM_Encad.ValueVariant, EdFlxM_Entrg.ValueVariant, EdFlxM_Web.ValueVariant,
  (*, EdTxtCnd_10.Text,
  EdTxtCnd_05.Text, EdTxtUsu_10.Text, EdTxtPrp_15.Text,
  EdTxtRis_25.Text, EdTxtImb_15.Text, EdTxtMor_15.Text, EdTxtMor_20.Text,
  EdTxtUHs_20.Text, EdTxtUHs_10.Text, EdTxtUHs_12.Text,
  EdTxtUHs_03.Text, EdTxtUHs_05.Text, EdTxtBlc_10.Text,
  EdTxtBlc_11.Text, EdTxtNiv_10.Text, EdTxtNiv_11.Text,
  EdTxtSin_10.Text*)

  EdCB4_Host.Text, EdCB4_Port.Text, EdCB4_User.Text,
  EdCB4_Pwd.Text, EdCB4_DB.Text, RGCB4_Uso.ItemIndex,
  EntiCargSind, DdPrtstCrt
  ], ['1'], False) then
  begin
    Dmod.ReopenControle();
    //Dmod.QrControle.Open;
    //
{
    SMSCelCOM := EdSMSCelCOM.Text;
    SMSCelPIN := EdSMSCelPIN.Text;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoesgerl', False, [
    'SMSCelCOM', 'SMSCelPIN'], ['Codigo'], [
    SMSCelCOM, SMSCelPIN], [1], False);
    DmodG.ReopenOpcoesGerl();
}
    //
    Caminho := 'Dermatek\Syndic\Opcoes';
    Geral.WriteAppKey('JaSetado', Caminho, True, ktBoolean, HKEY_LOCAL_MACHINE);
    //
    Close;
  end;
  VerificaPasta(EdCB4_DB.Text);
end;

procedure TFmOpcoesSyndic.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesSyndic.SBSMSCelCOMClick(Sender: TObject);
var
  OK: Boolean;
  Txt: String;
begin
  if DBCheck.CriaFm(TFmCOMSelPorta, FmCOMSelPorta, afmoNegarComAviso) then
  begin
    FmCOMSelPorta.ShowModal;
    OK := FmCOMSelPorta.FResult;
    Txt := FmCOMSelPorta.EdDevice.Text;
    FmCOMSelPorta.Destroy;
    if OK then
      EdSMSCelCOM.Text := Txt;
  end;
end;

procedure TFmOpcoesSyndic.TbUserTxtsBeforePost(DataSet: TDataSet);
begin
  if TbUserTxts.State = dsInsert then
  begin
    TbUserTxtsCodigo.Value := UMyMod.BuscaNovoCodigo_Int(
      Dmod.QrAux, 'usertxts', 'Codigo', [], [], stIns, 0, siPositivo, nil);
  end;
end;

procedure TFmOpcoesSyndic.VerificaPasta(Dir: String);
begin
  if Length(Trim(Dir)) > 1 then 
    ForceDirectories(Dir);
end;

procedure TFmOpcoesSyndic.EdBLQ_LargDestinExit(Sender: TObject);
begin
end;

{
object GroupBox7: TGroupBox
  Left = 0
  Top = 49
  Width = 784
  Height = 244
  Align = alTop
  Caption = ' Nomenclaturas internas do aplicativo: '
  TabOrder = 1
  object Label22: TLabel
    Left = 12
    Top = 24
    Width = 60
    Height = 13
    Caption = 'Condom'#237'nio:'
  end
  object Label23: TLabel
    Left = 12
    Top = 48
    Width = 31
    Height = 13
    Caption = 'Cond.:'
  end
  object Label24: TLabel
    Left = 12
    Top = 72
    Width = 56
    Height = 13
    Caption = 'Cond'#244'mino:'
  end
  object Label25: TLabel
    Left = 12
    Top = 96
    Width = 56
    Height = 13
    Caption = 'Propriet'#225'rio:'
  end
  object Label26: TLabel
    Left = 12
    Top = 120
    Width = 122
    Height = 13
    Caption = 'Administradora de Riscos:'
  end
  object Label27: TLabel
    Left = 12
    Top = 144
    Width = 49
    Height = 13
    Caption = 'Imobili'#225'ria:'
  end
  object Label31: TLabel
    Left = 404
    Top = 72
    Width = 42
    Height = 13
    Caption = 'Morador:'
  end
  object Label36: TLabel
    Left = 404
    Top = 120
    Width = 105
    Height = 13
    Caption = 'Unidade Habitacional:'
  end
  object Label37: TLabel
    Left = 404
    Top = 144
    Width = 43
    Height = 13
    Caption = 'Unidade:'
  end
  object Label38: TLabel
    Left = 404
    Top = 192
    Width = 19
    Height = 13
    Caption = 'UH:'
  end
  object Label39: TLabel
    Left = 12
    Top = 168
    Width = 30
    Height = 13
    Caption = 'Bloco:'
  end
  object Label40: TLabel
    Left = 404
    Top = 24
    Width = 31
    Height = 13
    Caption = 'Andar:'
  end
  object Label41: TLabel
    Left = 404
    Top = 168
    Width = 48
    Height = 13
    Caption = 'Unidades:'
  end
  object Label42: TLabel
    Left = 404
    Top = 48
    Width = 42
    Height = 13
    Caption = 'Andares:'
  end
  object Label43: TLabel
    Left = 12
    Top = 192
    Width = 35
    Height = 13
    Caption = 'Blocos:'
  end
  object Label44: TLabel
    Left = 404
    Top = 216
    Width = 24
    Height = 13
    Caption = 'UHs:'
  end
  object Label45: TLabel
    Left = 12
    Top = 216
    Width = 40
    Height = 13
    Caption = 'S'#237'ndico:'
  end
  object Label46: TLabel
    Left = 404
    Top = 96
    Width = 53
    Height = 13
    Caption = 'Moradores:'
  end
  object EdTxtCnd_10: TdmkEdit
    Left = 140
    Top = 20
    Width = 88
    Height = 21
    MaxLength = 10
    TabOrder = 0
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtCnd_05: TdmkEdit
    Left = 140
    Top = 44
    Width = 48
    Height = 21
    MaxLength = 5
    TabOrder = 1
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtUsu_10: TdmkEdit
    Left = 140
    Top = 68
    Width = 88
    Height = 21
    MaxLength = 10
    TabOrder = 2
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtPrp_15: TdmkEdit
    Left = 140
    Top = 92
    Width = 128
    Height = 21
    MaxLength = 15
    TabOrder = 3
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtRis_25: TdmkEdit
    Left = 140
    Top = 116
    Width = 208
    Height = 21
    MaxLength = 25
    TabOrder = 4
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtImb_15: TdmkEdit
    Left = 140
    Top = 140
    Width = 128
    Height = 21
    MaxLength = 15
    TabOrder = 5
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtMor_15: TdmkEdit
    Left = 532
    Top = 68
    Width = 128
    Height = 21
    MaxLength = 15
    TabOrder = 11
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtUHs_20: TdmkEdit
    Left = 532
    Top = 116
    Width = 168
    Height = 21
    MaxLength = 20
    TabOrder = 13
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtUHs_10: TdmkEdit
    Left = 532
    Top = 140
    Width = 88
    Height = 21
    MaxLength = 10
    TabOrder = 14
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtUHs_03: TdmkEdit
    Left = 532
    Top = 188
    Width = 32
    Height = 21
    MaxLength = 3
    TabOrder = 16
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtBlc_10: TdmkEdit
    Left = 140
    Top = 164
    Width = 88
    Height = 21
    MaxLength = 10
    TabOrder = 6
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtNiv_10: TdmkEdit
    Left = 532
    Top = 20
    Width = 88
    Height = 21
    MaxLength = 10
    TabOrder = 9
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtUHs_12: TdmkEdit
    Left = 532
    Top = 164
    Width = 104
    Height = 21
    MaxLength = 10
    TabOrder = 15
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtNiv_11: TdmkEdit
    Left = 532
    Top = 44
    Width = 104
    Height = 21
    MaxLength = 10
    TabOrder = 10
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtBlc_11: TdmkEdit
    Left = 140
    Top = 188
    Width = 104
    Height = 21
    MaxLength = 10
    TabOrder = 7
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtUHs_05: TdmkEdit
    Left = 532
    Top = 212
    Width = 48
    Height = 21
    MaxLength = 3
    TabOrder = 17
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtSin_10: TdmkEdit
    Left = 140
    Top = 212
    Width = 104
    Height = 21
    MaxLength = 10
    TabOrder = 8
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
  object EdTxtMor_20: TdmkEdit
    Left = 532
    Top = 92
    Width = 128
    Height = 21
    MaxLength = 15
    TabOrder = 12
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
  end
end
}

end.
