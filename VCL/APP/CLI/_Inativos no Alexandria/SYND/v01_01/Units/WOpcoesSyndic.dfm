object FmWOpcoesSyndic: TFmWOpcoesSyndic
  Left = 339
  Top = 185
  Caption = 'WEB-OPCOE-002 :: Op'#231#245'es Espec'#237'ficas do Meu Site'
  ClientHeight = 282
  ClientWidth = 514
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 514
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 466
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 418
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 394
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Meu Site'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 394
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Meu Site'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 394
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Meu Site'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 514
    Height = 120
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 514
      Height = 120
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 514
        Height = 120
        Align = alClient
        TabOrder = 0
        object Label20: TLabel
          Left = 8
          Top = 8
          Width = 74
          Height = 13
          Caption = 'Usu'#225'rio Master:'
        end
        object Label21: TLabel
          Left = 132
          Top = 8
          Width = 69
          Height = 13
          Caption = 'Senha Master:'
        end
        object Label22: TLabel
          Left = 256
          Top = 8
          Width = 111
          Height = 13
          Caption = 'Nome do administrador:'
        end
        object Label23: TLabel
          Left = 8
          Top = 54
          Width = 162
          Height = 13
          Caption = 'Diret'#243'rio utilizado para balancetes:'
        end
        object EdUsername: TdmkEdit
          Left = 8
          Top = 24
          Width = 121
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = True
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdPassword: TdmkEdit
          Left = 132
          Top = 24
          Width = 121
          Height = 21
          Font.Charset = SYMBOL_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Wingdings'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = True
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          PasswordChar = 'l'
        end
        object EdNomeAdmi: TdmkEdit
          Left = 256
          Top = 24
          Width = 121
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdDirWebBalancete: TdmkEditCB
          Left = 8
          Top = 70
          Width = 41
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBDirWebBalancete
          IgnoraDBLookupComboBox = False
        end
        object CBDirWebBalancete: TdmkDBLookupComboBox
          Left = 49
          Top = 70
          Width = 328
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsDirWebBalan
          TabOrder = 4
          dmkEditCB = EdDirWebBalancete
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 168
    Width = 514
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 510
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 212
    Width = 514
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 368
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 366
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrWControl: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT *'
      'FROM wcontrol')
    Left = 473
    Top = 67
  end
  object QrDirWebBalan: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT *'
      'FROM dirweb'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 388
    Top = 68
    object IntegerField10: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDirWebBalanNome: TWideStringField
      FieldName = 'Nome'
      Size = 32
    end
  end
  object DsDirWebBalan: TDataSource
    DataSet = QrDirWebBalan
    Left = 416
    Top = 68
  end
  object QrSenha: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT User_ID, Username, Password, Tipo '
      'FROM users'
      'WHERE Tipo=9')
    Left = 444
    Top = 68
  end
end
