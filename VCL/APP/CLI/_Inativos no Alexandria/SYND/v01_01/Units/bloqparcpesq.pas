unit bloqparcpesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkDBGridDAC, UnDmkProcFunc, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmbloqparcpesq = class(TForm)
    Panel1: TPanel;
    QrCond: TmySQLQuery;
    DsCond: TDataSource;
    QrCondCodCliInt: TIntegerField;
    QrCondCodEnti: TIntegerField;
    QrCondNOMECOND: TWideStringField;
    Panel3: TPanel;
    dmkDBGridDAC1: TdmkDBGridDAC;
    Panel4: TPanel;
    CkConfirmad: TCheckBox;
    QrProp: TmySQLQuery;
    DsProp: TDataSource;
    dmkDBGridDAC2: TdmkDBGridDAC;
    QrPropCodigoEnt: TIntegerField;
    QrPropNOMEPROP: TWideStringField;
    QrPropUnidade: TWideStringField;
    dmkDBGridDAC3: TdmkDBGridDAC;
    QrParc: TmySQLQuery;
    DsParc: TDataSource;
    QrParcCodigo: TAutoIncField;
    QrPropCodigoEsp: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure CkConfirmadClick(Sender: TObject);
    procedure QrCondBeforeClose(DataSet: TDataSet);
    procedure QrCondAfterScroll(DataSet: TDataSet);
    procedure QrPropBeforeClose(DataSet: TDataSet);
    procedure QrPropAfterScroll(DataSet: TDataSet);
    procedure dmkDBGridDAC3DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure ReabreCond(CodCliInt: Integer);
  public
    { Public declarations }
  end;

  var
  Fmbloqparcpesq: TFmbloqparcpesq;

implementation

{$R *.DFM}

uses Module, bloqparc, UnMyObjects;

procedure TFmbloqparcpesq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmbloqparcpesq.CkConfirmadClick(Sender: TObject);
begin
  ReabreCond(0);
end;

procedure TFmbloqparcpesq.dmkDBGridDAC3DblClick(Sender: TObject);
begin
  if (QrParc.State <> dsInactive) and (QrParc.RecordCount > 0) then
  begin
    Fmbloqparc.LocCod(QrParcCodigo.Value, QrParcCodigo.Value);
    Close;
  end;
end;

procedure TFmbloqparcpesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmbloqparcpesq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmbloqparcpesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmbloqparcpesq.QrCondAfterScroll(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  try
    QrProp.Close;
    QrProp.Database := Fmbloqparc.Qrbloqparc.Database;
    QrProp.SQL.Clear;
    QrProp.SQL.Add('SELECT DISTINCT blp.CodigoEnt, blp.CodigoEsp, ');
    QrProp.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP,');
    QrProp.SQL.Add('cim.Conta, cim.Unidade');
    QrProp.SQL.Add('FROM bloqparc blp');
    QrProp.SQL.Add('LEFT JOIN condimov cim ON cim.Conta=blp.CodigoEsp');
    QrProp.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=blp.CodigoEnt');
    QrProp.SQL.Add('LEFT JOIN enticliint eci ON eci.CodEnti=blp.CodCliEnt');
    QrProp.SQL.Add('WHERE eci.TipoTabLct=1');
    QrProp.SQL.Add('AND cim.Codigo=' + FormatFloat('0', QrCondCodCliInt.Value));
    QrProp.SQL.Add('ORDER BY NOMEPROP');
    QrProp.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmbloqparcpesq.QrCondBeforeClose(DataSet: TDataSet);
begin
  QrProp.Close;
end;

procedure TFmbloqparcpesq.QrPropAfterScroll(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  try
    QrParc.Close;
    QrParc.Database := Fmbloqparc.Qrbloqparc.Database;
    QrParc.SQL.Clear;
    QrParc.SQL.Add('SELECT blp.Codigo');
    QrParc.SQL.Add('FROM bloqparc blp');
    QrParc.SQL.Add('LEFT JOIN condimov cim ON cim.Conta=blp.CodigoEsp');
    QrParc.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=blp.CodigoEnt');
    QrParc.SQL.Add('LEFT JOIN enticliint eci ON eci.CodEnti=blp.CodCliEnt');
    QrParc.SQL.Add('WHERE eci.TipoTabLct=1');
    QrParc.SQL.Add('AND cim.Codigo=' + FormatFloat('0', QrCondCodCliInt.Value));
    QrParc.SQL.Add('AND (cim.Propriet=' + FormatFloat('0', QrPropCodigoEnt.Value));
    QrParc.SQL.Add('OR cim.Conta=' + FormatFloat('0', QrPropCodigoEsp.Value) + ')');
    QrParc.SQL.Add('ORDER BY blp.Codigo DESC');
    dmkPF.LeMeuTexto(QrParc.SQL.Text);
    QrParc.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmbloqparcpesq.QrPropBeforeClose(DataSet: TDataSet);
begin
  QrParc.Close;
end;

procedure TFmbloqparcpesq.ReabreCond(CodCliInt: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
    QrCond.Close;
    QrCond.Database := Fmbloqparc.Qrbloqparc.Database;
    QrCond.SQL.Clear;
    QrCond.SQL.Add('SELECT DISTINCT eci.CodCliInt, eci.CodEnti,');
    QrCond.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECOND');
    QrCond.SQL.Add('FROM bloqparc blp');
    QrCond.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=blp.CodCliEnt');
    QrCond.SQL.Add('LEFT JOIN enticliint eci ON eci.CodEnti=cli.Codigo');
    QrCond.SQL.Add('WHERE eci.TipoTabLct=1');
    if CkConfirmad.Checked then
      QrCond.SQL.Add('AND NOT (blp.Novo in (9,-9))');
    QrCond.SQL.Add('ORDER BY NOMECOND');
    QrCond.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

{
SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECOND,
IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP,
cim.Unidade, blp.*
FROM bloqparc blp
LEFT JOIN condimov cim ON cim.Conta=blp.CodigoEsp
LEFT JOIN entidades ent ON ent.Codigo=blp.CodigoEnt
LEFT JOIN entidades cli ON cli.Codigo=blp.CodCliEnt
LEFT JOIN enticliint eci ON eci.CodEnti=cli.Codigo
WHERE eci.TipoTabLct=1
ORDER BY Status
}

end.
