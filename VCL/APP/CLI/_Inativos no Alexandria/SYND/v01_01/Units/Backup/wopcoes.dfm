object Fmwopcoes: TFmwopcoes
  Left = 339
  Top = 185
  Caption = 'WEB-OPCOE-001 :: Op'#231#245'es do Meu Site'
  ClientHeight = 307
  ClientWidth = 403
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 403
    Height = 145
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 74
      Height = 13
      Caption = 'Usu'#225'rio Master:'
    end
    object Label2: TLabel
      Left = 140
      Top = 8
      Width = 69
      Height = 13
      Caption = 'Senha Master:'
    end
    object Label3: TLabel
      Left = 264
      Top = 8
      Width = 111
      Height = 13
      Caption = 'Nome do administrador:'
    end
    object LaForneceI: TLabel
      Left = 16
      Top = 52
      Width = 127
      Height = 13
      Caption = 'Entidade detentora do site:'
    end
    object Label4: TLabel
      Left = 16
      Top = 94
      Width = 162
      Height = 13
      Caption = 'Diret'#243'rio utilizado para balancetes:'
    end
    object EdUsername: TEdit
      Left = 16
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object EdPassword: TEdit
      Left = 140
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object EdNomeAdmi: TEdit
      Left = 264
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object EdForneceI: TdmkEditCB
      Left = 16
      Top = 68
      Width = 41
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBForneceI
      IgnoraDBLookupComboBox = False
    end
    object CBForneceI: TdmkDBLookupComboBox
      Left = 57
      Top = 68
      Width = 328
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsForneceI
      TabOrder = 4
      dmkEditCB = EdForneceI
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDirWebBalancete: TdmkEditCB
      Left = 16
      Top = 110
      Width = 41
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBDirWebBalancete
      IgnoraDBLookupComboBox = False
    end
    object CBDirWebBalancete: TdmkDBLookupComboBox
      Left = 57
      Top = 110
      Width = 328
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsDirWebBalan
      TabOrder = 6
      dmkEditCB = EdDirWebBalancete
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 403
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 315
      Top = 0
      Width = 88
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 267
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 248
        Height = 32
        Caption = 'Op'#231#245'es do Meu Site'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 248
        Height = 32
        Caption = 'Op'#231#245'es do Meu Site'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 248
        Height = 32
        Caption = 'Op'#231#245'es do Meu Site'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 193
    Width = 403
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 399
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 237
    Width = 403
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 399
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 255
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrSenha: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT User_ID, Username, Password, Tipo '
      'FROM users'
      'WHERE Tipo=9')
    Left = 12
    Top = 12
    object QrSenhaUser_ID: TAutoIncField
      FieldName = 'User_ID'
    end
    object QrSenhaUsername: TWideStringField
      FieldName = 'Username'
      Size = 32
    end
    object QrSenhaPassword: TWideStringField
      FieldName = 'Password'
      Size = 32
    end
    object QrSenhaTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object Qrwcontrol: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT *, NomeAdmi'
      'FROM wcontrol')
    Left = 40
    Top = 12
    object QrwcontrolNomeAdmi: TWideStringField
      FieldName = 'NomeAdmi'
      Size = 50
    end
    object QrwcontrolDono: TIntegerField
      FieldName = 'Dono'
    end
    object QrwcontrolDirWebBalancete: TIntegerField
      FieldName = 'DirWebBalancete'
    end
  end
  object QrForneceI: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NomeENTIDADE')
    Left = 68
    Top = 12
    object QrForneceICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceINOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsForneceI: TDataSource
    DataSet = QrForneceI
    Left = 96
    Top = 12
  end
  object QrDirWebBalan: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT *'
      'FROM dirweb'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 124
    Top = 12
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDirWebBalanNome: TWideStringField
      FieldName = 'Nome'
      Size = 32
    end
  end
  object DsDirWebBalan: TDataSource
    DataSet = QrDirWebBalan
    Left = 152
    Top = 12
  end
end
