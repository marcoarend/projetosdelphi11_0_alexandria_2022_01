unit wopcoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmwopcoes = class(TForm)
    Panel1: TPanel;
    EdUsername: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdPassword: TEdit;
    QrSenha: TmySQLQuery;
    Qrwcontrol: TmySQLQuery;
    EdNomeAdmi: TEdit;
    Label3: TLabel;
    QrSenhaUser_ID: TAutoIncField;
    QrSenhaUsername: TWideStringField;
    QrSenhaPassword: TWideStringField;
    QrSenhaTipo: TSmallintField;
    QrwcontrolNomeAdmi: TWideStringField;
    QrForneceI: TmySQLQuery;
    QrForneceICodigo: TIntegerField;
    QrForneceINOMEENTIDADE: TWideStringField;
    DsForneceI: TDataSource;
    LaForneceI: TLabel;
    EdForneceI: TdmkEditCB;
    CBForneceI: TdmkDBLookupComboBox;
    QrwcontrolDono: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Label4: TLabel;
    EdDirWebBalancete: TdmkEditCB;
    CBDirWebBalancete: TdmkDBLookupComboBox;
    QrDirWebBalan: TmySQLQuery;
    IntegerField1: TIntegerField;
    DsDirWebBalan: TDataSource;
    QrDirWebBalanNome: TWideStringField;
    QrwcontrolDirWebBalancete: TIntegerField;
    ImgWEB: TdmkImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  Fmwopcoes: TFmwopcoes;

implementation

uses Module, UnMyObjects, UMySQLModule, UnDmkWeb;

{$R *.DFM}

procedure TFmwopcoes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmwopcoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmwopcoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmwopcoes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrSenha, Dmod.MyDBn);
  UMyMod.AbreQuery(Qrwcontrol, Dmod.MyDBn);
  UMyMod.AbreQuery(QrForneceI, Dmod.MyDBn);
  UMyMod.AbreQuery(QrDirWebBalan, Dmod.MyDBn);
  //
  EdUsername.Text                := QrSenhaUsername.Value;
  EdPassword.Text                := QrSenhaPassword.Value;
  EdNomeAdmi.Text                := QrwcontrolNomeAdmi.Value;
  EdForneceI.ValueVariant        := QrwcontrolDono.Value;
  CBForneceI.KeyValue            := QrwcontrolDono.Value;
  EdDirWebBalancete.ValueVariant := QrwcontrolDirWebBalancete.Value;
  CBDirWebBalancete.KeyValue     := QrwcontrolDirWebBalancete.Value;
end;

procedure TFmwopcoes.BtOKClick(Sender: TObject);
begin
  Screen.Cursor := crDefault;
  try
    Dmod.QrWeb.Close;
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('UPDATE wcontrol SET NomeAdmi=:P0, Dono=:P1, ');
    Dmod.QrWeb.SQL.Add('DirWebBalancete=:P2 ');
    Dmod.QrWeb.Params[0].AsString  := EdNomeAdmi.Text;
    Dmod.QrWeb.Params[1].AsInteger := EdForneceI.ValueVariant;
    Dmod.QrWeb.Params[2].AsInteger := EdDirWebBalancete.ValueVariant;
    Dmod.QrWeb.ExecSQL;
    //
    Dmod.QrWeb.Close;
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('UPDATE users SET ');
    Dmod.QrWeb.SQL.Add('Username=:P0, Password=:P1 ');
    Dmod.QrWeb.SQL.Add('');
    Dmod.QrWeb.SQL.Add('WHERE User_ID=:Pa');
    Dmod.QrWeb.SQL.Add('');
    Dmod.QrWeb.Params[00].AsString  := EdUsername.Text;
    Dmod.QrWeb.Params[01].AsString  := EdPassword.Text;
    //
    Dmod.QrWeb.Params[02].AsInteger := QrSenhaUser_ID.Value;
    Dmod.QrWeb.ExecSQL;
    //
    Geral.MensagemBox('Dados atualizados com sucesso!', 'Informação',
      MB_OK+MB_ICONINFORMATION);
  finally
    Screen.Cursor := crDefault;
    Close;
  end;
end;

end.
