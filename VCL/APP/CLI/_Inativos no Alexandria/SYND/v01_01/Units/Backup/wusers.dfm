object Fmwusers: TFmwusers
  Left = 339
  Top = 185
  Caption = 'WEB-CLIEN-002 :: Pesquisa de Usu'#225'rios WEB'
  ClientHeight = 520
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 358
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel11: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 540
        Height = 88
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 4
          Top = 4
          Width = 60
          Height = 13
          Caption = 'Condom'#237'nio:'
        end
        object Label1: TLabel
          Left = 4
          Top = 44
          Width = 140
          Height = 13
          Caption = 'Propriet'#225'rio [F4 mostra todos]:'
        end
        object Label3: TLabel
          Left = 388
          Top = 44
          Width = 103
          Height = 13
          Caption = 'Unidade habitacional:'
        end
        object EdEmpresa: TdmkEditCB
          Left = 4
          Top = 20
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 52
          Top = 20
          Width = 479
          Height = 21
          Color = clWhite
          KeyField = 'CodCond'
          ListField = 'NOMECOND'
          ListSource = DsEntiCond
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPropriet: TdmkEditCB
          Left = 4
          Top = 61
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdProprietChange
          OnKeyDown = EdProprietKeyDown
          DBLookupComboBox = CBPropriet
          IgnoraDBLookupComboBox = False
        end
        object CBPropriet: TdmkDBLookupComboBox
          Left = 52
          Top = 60
          Width = 333
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEPROP'
          ListSource = DsPropriet
          TabOrder = 3
          OnKeyDown = CBProprietKeyDown
          dmkEditCB = EdPropriet
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCondImov: TdmkEditCB
          Left = 388
          Top = 60
          Width = 44
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdCondImovChange
          DBLookupComboBox = CBCondImov
          IgnoraDBLookupComboBox = False
        end
        object CBCondImov: TdmkDBLookupComboBox
          Left = 436
          Top = 60
          Width = 95
          Height = 21
          Color = clWhite
          KeyField = 'Conta'
          ListField = 'Unidade'
          ListSource = DsCondImov
          TabOrder = 5
          OnKeyDown = CBCondImovKeyDown
          dmkEditCB = EdCondImov
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object RGTipoUser: TRadioGroup
        Left = 540
        Top = 0
        Width = 244
        Height = 88
        Align = alClient
        Caption = ' Tipo de usu'#225'rio: '
        ItemIndex = 0
        Items.Strings = (
          '?'
          'Propriet'#225'rio')
        TabOrder = 1
        OnClick = RGTipoUserClick
      end
    end
    object PnPesq1: TPanel
      Left = 0
      Top = 88
      Width = 784
      Height = 270
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 784
        Height = 270
        Align = alClient
        DataSource = DsPesq1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CodCliEnt'
            Title.Caption = 'C'#243'digo'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Condom'
            Title.Caption = 'Empresa'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodigoEnt'
            Title.Caption = 'C'#243'digo'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Pessoa'
            Title.Caption = 'Propriet'#225'rio'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodigoEsp'
            Title.Caption = 'C'#243'digo'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Unidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Username'
            Title.Caption = 'Login'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Password'
            Title.Caption = 'Senha'
            Width = 88
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 336
        Height = 32
        Caption = 'Pesquisa de Usu'#225'rios WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 336
        Height = 32
        Caption = 'Pesquisa de Usu'#225'rios WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 336
        Height = 32
        Caption = 'Pesquisa de Usu'#225'rios WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 406
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 450
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
      object BtPesq: TBitBtn
        Tag = 20
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        Enabled = False
        TabOrder = 0
        OnClick = BtPesqClick
        NumGlyphs = 2
      end
      object BitBtn1: TBitBtn
        Tag = 12
        Left = 346
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui orf'#227'os'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn1Click
        NumGlyphs = 2
      end
    end
  end
  object QrEntiCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eci.CodCliInt CodCond, eci.CodEnti CodEnti,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECOND '
      'FROM enticliint eci'
      'LEFT JOIN entidades ent ON ent.Codigo = eci.CodEnti'
      'WHERE eci.CodCliInt<>0'
      'ORDER BY NOMECOND')
    Left = 13
    Top = 9
    object QrEntiCondCodCond: TIntegerField
      FieldName = 'CodCond'
      Origin = 'cond.Codigo'
    end
    object QrEntiCondCodEnti: TIntegerField
      FieldName = 'CodEnti'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEntiCondNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Origin = 'NOMECOND'
      Required = True
      Size = 100
    end
  end
  object DsEntiCond: TDataSource
    DataSet = QrEntiCond
    Left = 41
    Top = 9
  end
  object QrPropriet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP '
      'FROM entidades ent'
      'LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet'
      'WHERE ent.Cliente2="V"'
      'AND cim.Codigo<>0'
      'ORDER BY NOMEPROP')
    Left = 69
    Top = 9
    object QrProprietCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrProprietNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Origin = 'NOMEPROP'
      Required = True
      Size = 100
    end
  end
  object DsPropriet: TDataSource
    DataSet = QrPropriet
    Left = 97
    Top = 9
  end
  object QrCondImov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cim.Conta, cim.Unidade'
      'FROM condimov cim'
      'WHERE cim.Codigo<>0'
      'AND cim.Propriet<>0'
      'ORDER BY cim.Unidade'
      '')
    Left = 125
    Top = 9
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 153
    Top = 9
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesq1AfterOpen
    SQL.Strings = (
      'SELECT '
      'IF(con.Tipo=0,con.RazaoSocial,con.Nome) NO_Condom,'
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_Pessoa,'
      'cim.Unidade, CASE WHEN usr.Tipo=1 THEN "Morador" '
      'WHEN usr.Tipo=9 THEN "Admin" ELSE "TESTE" END NO_Tipo,'
      'usr.*'
      'FROM users usr'
      'LEFT JOIN entidades con ON con.Codigo=usr.CodCliEnt'
      'LEFT JOIN entidades ent ON ent.Codigo=usr.CodigoEnt'
      'LEFT JOIN condimov cim ON cim.Conta=usr.CodigoEsp'
      'WHERE usr.CodCliEsp=18'
      'AND usr.CodigoEnt=1389')
    Left = 36
    Top = 160
    object QrPesq1NO_Condom: TWideStringField
      FieldName = 'NO_Condom'
      Size = 100
    end
    object QrPesq1NO_Pessoa: TWideStringField
      FieldName = 'NO_Pessoa'
      Size = 100
    end
    object QrPesq1Unidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPesq1User_ID: TAutoIncField
      FieldName = 'User_ID'
    end
    object QrPesq1CodCliEsp: TIntegerField
      FieldName = 'CodCliEsp'
    end
    object QrPesq1CodCliEnt: TIntegerField
      FieldName = 'CodCliEnt'
    end
    object QrPesq1CodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
    end
    object QrPesq1CodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
    end
    object QrPesq1Username: TWideStringField
      FieldName = 'Username'
      Size = 32
    end
    object QrPesq1Password: TWideStringField
      FieldName = 'Password'
      Size = 32
    end
    object QrPesq1LoginID: TWideStringField
      FieldName = 'LoginID'
      Size = 32
    end
    object QrPesq1LastAcess: TDateTimeField
      FieldName = 'LastAcess'
    end
    object QrPesq1Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPesq1NO_Tipo: TWideStringField
      FieldName = 'NO_Tipo'
      Required = True
      Size = 7
    end
  end
  object DsPesq1: TDataSource
    DataSet = QrPesq1
    Left = 64
    Top = 160
  end
  object QrErrSenhas1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT usr.User_ID'
      'FROM users usr'
      'LEFT JOIN condimov imv ON imv.Propriet=usr.CodigoEnt'
      'WHERE usr.CodigoEsp<>imv.Conta'
      'ORDER BY User_ID')
    Left = 444
    Top = 456
    object QrErrSenhas1User_ID: TIntegerField
      FieldName = 'User_ID'
      Required = True
    end
  end
end
