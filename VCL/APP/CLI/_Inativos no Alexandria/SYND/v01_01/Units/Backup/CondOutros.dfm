object FmCondOutros: TFmCondOutros
  Left = 339
  Top = 185
  Caption = 'CAD-CONDO-002 :: Conselho Fiscal'
  ClientHeight = 324
  ClientWidth = 514
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 514
    Height = 162
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label9: TLabel
      Left = 12
      Top = 43
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object SpeedButton5: TSpeedButton
      Left = 475
      Top = 59
      Width = 21
      Height = 21
      Hint = 'Inclui item de carteira'
      Caption = '...'
      OnClick = SpeedButton5Click
    end
    object Label12: TLabel
      Left = 88
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label61: TLabel
      Left = 12
      Top = 87
      Width = 43
      Height = 13
      Caption = 'Unidade:'
    end
    object Label44: TLabel
      Left = 88
      Top = 86
      Width = 31
      Height = 13
      Caption = 'Cargo:'
    end
    object SpeedButton6: TSpeedButton
      Left = 301
      Top = 103
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton6Click
    end
    object Label62: TLabel
      Left = 439
      Top = 87
      Width = 55
      Height = 13
      Caption = 'Ordem lista:'
    end
    object Label1: TLabel
      Left = 12
      Top = 5
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object EdNome: TdmkEditCB
      Left = 12
      Top = 59
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBNome
      IgnoraDBLookupComboBox = False
    end
    object CBNome: TdmkDBLookupComboBox
      Left = 69
      Top = 59
      Width = 405
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsNome
      TabOrder = 2
      dmkEditCB = EdNome
      QryCampo = 'Nome'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCodigo: TdmkEdit
      Left = 88
      Top = 20
      Width = 71
      Height = 21
      Alignment = taRightJustify
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdUnidade: TdmkEdit
      Left = 12
      Top = 103
      Width = 70
      Height = 21
      MaxLength = 10
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Unidade'
      UpdCampo = 'Unidade'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdCargo: TdmkEditCB
      Left = 88
      Top = 103
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCargo
      IgnoraDBLookupComboBox = False
    end
    object CBCargo: TdmkDBLookupComboBox
      Left = 146
      Top = 103
      Width = 150
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCargos
      TabOrder = 5
      dmkEditCB = EdCargo
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdOrdem: TdmkEdit
      Left = 439
      Top = 103
      Width = 57
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'OrdemLista'
      UpdCampo = 'OrdemLista'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object RGParecer: TdmkRadioGroup
      Left = 329
      Top = 87
      Width = 104
      Height = 37
      Caption = 'Assina o parecer'
      Columns = 2
      Items.Strings = (
        'N'#227'o'
        'Sim')
      TabOrder = 6
      QryCampo = 'Assina'
      UpdCampo = 'Assina'
      UpdType = utYes
      OldValor = 0
    end
    object dmkDBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 20
      Width = 70
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Codigo'
      DataSource = FmCond.DsCond
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 8
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object CkContinuar: TCheckBox
      Left = 12
      Top = 130
      Width = 112
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 9
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 514
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 466
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 418
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 195
        Height = 32
        Caption = 'Conselho Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 195
        Height = 32
        Caption = 'Conselho Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 195
        Height = 32
        Caption = 'Conselho Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 210
    Width = 514
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 510
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 254
    Width = 514
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 368
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 366
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrNome: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM entidades ent'
      'WHERE ent.Codigo > 0'
      'ORDER BY Nome')
    Left = 164
    Top = 64
    object QrNomeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNomeNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrNomeCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsNome: TDataSource
    DataSet = QrNome
    Left = 192
    Top = 64
  end
  object VUNome: TdmkValUsu
    dmkEditCB = EdNome
    Panel = Panel1
    QryCampo = 'Nome'
    UpdCampo = 'Nome'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 220
    Top = 64
  end
  object QrCargos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM cargos'
      'ORDER BY Nome')
    Left = 248
    Top = 64
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsCargos: TDataSource
    DataSet = QrCargos
    Left = 276
    Top = 64
  end
  object VUCargos: TdmkValUsu
    dmkEditCB = EdCargo
    Panel = Panel1
    QryCampo = 'Cargo'
    UpdCampo = 'Cargo'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 304
    Top = 64
  end
end
