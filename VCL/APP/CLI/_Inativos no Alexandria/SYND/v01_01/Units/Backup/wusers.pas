unit wusers;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DBCtrls, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmwusers = class(TForm)
    Panel1: TPanel;
    QrEntiCond: TmySQLQuery;
    QrEntiCondCodCond: TIntegerField;
    QrEntiCondCodEnti: TIntegerField;
    QrEntiCondNOMECOND: TWideStringField;
    DsEntiCond: TDataSource;
    QrPropriet: TmySQLQuery;
    QrProprietCodigo: TIntegerField;
    QrProprietNOMEPROP: TWideStringField;
    DsPropriet: TDataSource;
    QrCondImov: TmySQLQuery;
    QrCondImovConta: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    DsCondImov: TDataSource;
    Panel11: TPanel;
    PnPesq1: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdPropriet: TdmkEditCB;
    CBPropriet: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdCondImov: TdmkEditCB;
    CBCondImov: TdmkDBLookupComboBox;
    RGTipoUser: TRadioGroup;
    QrPesq1: TmySQLQuery;
    QrPesq1NO_Condom: TWideStringField;
    QrPesq1NO_Pessoa: TWideStringField;
    QrPesq1Unidade: TWideStringField;
    QrPesq1User_ID: TAutoIncField;
    QrPesq1CodCliEsp: TIntegerField;
    QrPesq1CodCliEnt: TIntegerField;
    QrPesq1CodigoEnt: TIntegerField;
    QrPesq1CodigoEsp: TIntegerField;
    QrPesq1Username: TWideStringField;
    QrPesq1Password: TWideStringField;
    QrPesq1LoginID: TWideStringField;
    QrPesq1LastAcess: TDateTimeField;
    QrPesq1Tipo: TSmallintField;
    DsPesq1: TDataSource;
    DBGrid1: TDBGrid;
    QrPesq1NO_Tipo: TWideStringField;
    QrErrSenhas1: TmySQLQuery;
    QrErrSenhas1User_ID: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtPesq: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdProprietChange(Sender: TObject);
    procedure EdProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCondImovChange(Sender: TObject);
    procedure CBCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGTipoUserClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure QrPesq1AfterOpen(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure CBProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenPropriet(Todos: Boolean);
    procedure ReopenCondImov();
    procedure PesquisaEm_users();
    //procedure PesquisaEm_wclients();
  public
    { Public declarations }
  end;

  var
  Fmwusers: TFmwusers;

implementation

uses dmkGeral, ModuleCond, Module, ModuleGeral, UnMyObjects;

{$R *.DFM}

procedure TFmwusers.BitBtn1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    QrErrSenhas1.Close;
    QrErrSenhas1.Open;
    if QrErrSenhas1.RecordCount > 0 then
    begin
      if Geral.MensagemBox('Existem ' + IntToStr(
      QrErrSenhas1.RecordCount) + ' senhas web na qual mudou o ' +
      DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) + ' da ' +
      DModG.ReCaptionTexto(VAR_U_H) + '.' +
      #13#10'� aconselhav�l excluir estas senhas!'#13#10+
      'Deseja excluir estas senhas?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Screen.Cursor := crHourGlass;
        Dmod.QrUpdZ.SQL.Clear;
        Dmod.QrUpdZ.SQL.Add('DELETE FROM users WHERE ');
        Dmod.QrUpdZ.SQL.Add('User_ID=:P0');
        QrErrSenhas1.First;
        while not QrErrSenhas1.Eof do
        begin
          Dmod.QrUpdZ.Params[00].AsInteger := QrErrSenhas1User_ID.Value;
          Dmod.QrUpdZ.ExecSQL;
          //
          QrErrSenhas1.Next;
        end;
        Screen.Cursor := crDefault;
      end;
    end else
      Geral.MensagemBox('N�o existem senhas web na qual mudou o ' +
      DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) + ' da ' +
      DModG.ReCaptionTexto(VAR_U_H) + '.',
      'Mensagem', MB_OK+MB_ICONINFORMATION);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmwusers.BtPesqClick(Sender: TObject);
begin
  case RGTipoUser.ItemIndex of
    0,1: PesquisaEm_users();
    //1: PesquisaEm_wclients();
    else Geral.MensagemBox('Tipo de usu�rio sem implementa��o de pesquisa!',
    'Erro', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmwusers.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmwusers.CBCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_DELETE then
  begin
    EdCondImov.Text := '';
    CBCondImov.KeyValue := Null;
  end;
end;

procedure TFmwusers.CBProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmwusers.EdCondImovChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
end;

procedure TFmwusers.EdEmpresaChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  if EdEmpresa.ValueVariant <> 0 then
  begin
    ReopenPropriet(False);
    ReopenCondImov();
  end;
end;

procedure TFmwusers.EdProprietChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  ReopenCondImov;
end;

procedure TFmwusers.EdProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmwusers.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmwusers.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrEntiCond.Open;
  //QrPropriet.Open;
  //QrCondImov.Open;
end;

procedure TFmwusers.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmwusers.PesquisaEm_users();
var
  Cond, Prop, Apto: Integer;
begin
  Cond := EdEmpresa.ValueVariant;
  Prop := EdPropriet.ValueVariant;
  Apto := EdCondImov.ValueVariant;
  //
  QrPesq1.Close;
  QrPesq1.SQL.Clear;
  QrPesq1.SQL.Add('SELECT ');
  QrPesq1.SQL.Add('IF(con.Tipo=0,con.RazaoSocial,con.Nome) NO_Condom,');
  QrPesq1.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_Pessoa,');
  QrPesq1.SQL.Add('cim.Unidade, CASE WHEN usr.Tipo=1 THEN "Morador" ');
  QrPesq1.SQL.Add('WHEN usr.Tipo=9 THEN "Admin" ELSE "TESTE" END NO_Tipo,');
  QrPesq1.SQL.Add('usr.*');
  QrPesq1.SQL.Add('FROM users usr');
  QrPesq1.SQL.Add('LEFT JOIN entidades con ON con.Codigo=usr.CodCliEnt');
  QrPesq1.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=usr.CodigoEnt');
  QrPesq1.SQL.Add('LEFT JOIN condimov cim ON cim.Conta=usr.CodigoEsp');
  QrPesq1.SQL.Add('WHERE usr.User_ID>0'); // n�o mostrar -1
  //
  if Cond <> 0 then
    QrPesq1.SQL.Add('AND usr.Tipo=' + FormatFloat('0', RGTipoUser.ItemIndex));
  if Cond <> 0 then
    QrPesq1.SQL.Add('AND usr.CodCliEsp=' + FormatFloat('0', Cond));
  if Prop <> 0 then
    QrPesq1.SQL.Add('AND usr.CodigoEnt=' + FormatFloat('0', Prop));
  if Apto <> 0 then
    QrPesq1.SQL.Add('AND usr.CodigoEsp=' + FormatFloat('0', Apto));
  //
  QrPesq1.Open;
end;

procedure TFmwusers.QrPesq1AfterOpen(DataSet: TDataSet);
begin
  PnPesq1.Visible := True;
end;

{
procedure TFmwusers.PesquisaEm_wclients();
begin
end;
}

procedure TFmwusers.ReopenCondImov();
var
  Cond, Propriet: Integer;
begin
  EdCondImov.Text := '';
  CBCondImov.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  Propriet := Geral.IMV(EdPropriet.Text);
  QrCondImov.Close;
  QrCondImov.SQL.Clear;
  QrCondImov.SQL.Add('SELECT cim.Conta, cim.Unidade');
  QrCondImov.SQL.Add('FROM condimov cim');
  QrCondImov.SQL.Add('WHERE cim.Codigo<>0');
  QrCondImov.SQL.Add('AND cim.Propriet<>0');
  QrCondImov.SQL.Add('');
  if Cond <> 0 then
    QrCondImov.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  if Propriet <> 0 then
    QrCondImov.SQL.Add('AND cim.Propriet=' + IntToStr(Propriet));
  QrCondImov.SQL.Add('');
  QrCondImov.SQL.Add('ORDER BY cim.Unidade');
  QrCondImov.Open;
end;

procedure TFmwusers.ReopenPropriet(Todos: Boolean);
var
  Cond: Integer;
begin
  EdPropriet.Text := '';
  CBPropriet.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  QrPropriet.Close;
  QrPropriet.SQL.Clear;
  QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
  QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
  QrPropriet.SQL.Add('FROM entidades ent');
  QrPropriet.SQL.Add('LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet');
  QrPropriet.SQL.Add('WHERE ent.Cliente2="V"');
  QrPropriet.SQL.Add('');
  if (Cond <> 0) and not Todos then
    QrPropriet.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  QrPropriet.SQL.Add('');
  QrPropriet.SQL.Add('ORDER BY NOMEPROP');
  QrPropriet.Open;
end;

procedure TFmwusers.RGTipoUserClick(Sender: TObject);
begin
  BtPesq.Enabled := RGTipoUser.ItemIndex > 0;
end;

end.
