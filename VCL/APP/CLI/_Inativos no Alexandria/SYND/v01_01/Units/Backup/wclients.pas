unit wclients;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, Menus, ComCtrls, dmkDBGrid, dmkEdit, dmkGeral, UnDmkProcFunc,
  dmkImage, UnDmkEnums;

type
  TFmwclients = class(TForm)
    PainelDados: TPanel;
    Dswclients: TDataSource;
    Qrwclients: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    PainelData: TPanel;
    PainelWeb: TPanel;
    DBGClientes: TDBGrid;
    QrClientes: TmySQLQuery;
    Panel7: TPanel;
    DsClientes: TDataSource;
    EdPesq: TdmkEdit;
    Label3: TLabel;
    SpeedButton5: TSpeedButton;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLI: TWideStringField;
    QrClientesCNPJCPF: TWideStringField;
    QrWPesq: TmySQLQuery;
    QrWPesqCodigo: TIntegerField;
    QrClientesIERG: TWideStringField;
    QrClientesRua: TWideStringField;
    QrClientesNumero: TLargeintField;
    QrClientesCompl: TWideStringField;
    QrClientesBairro: TWideStringField;
    QrClientesCidade: TWideStringField;
    QrClientesCEP: TLargeintField;
    QrClientesUF: TLargeintField;
    QrClientesLimiCred: TFloatField;
    QrClientesTel1: TWideStringField;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit81: TDBEdit;
    Label80: TLabel;
    EdNomeWeb: TdmkEdit;
    Label8: TLabel;
    Label25: TLabel;
    EdPassword: TdmkEdit;
    Label26: TLabel;
    EdUsername: TdmkEdit;
    PainelUser: TPanel;
    Panel10: TPanel;
    Label27: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Edit001: TdmkEdit;
    EdULogin: TdmkEdit;
    EdUSenha: TdmkEdit;
    PMUsuario: TPopupMenu;
    QrUsers: TmySQLQuery;
    DsUsers: TDataSource;
    EdUser2: TdmkEdit;
    EdPass2: TdmkEdit;
    Label29: TLabel;
    Label30: TLabel;
    Alterausurioatual1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrVisitascli: TmySQLQuery;
    DsVisitascli: TDataSource;
    QrVisitascliControle: TAutoIncField;
    QrVisitascliIP: TWideStringField;
    QrVisitascliDataHora: TDateTimeField;
    QrVisitascliUsuario: TIntegerField;
    QrwclientsCodigo: TIntegerField;
    QrwclientsCliente: TIntegerField;
    QrwclientsAndares: TIntegerField;
    QrwclientsTotApt: TIntegerField;
    QrwclientsObserv: TWideMemoField;
    QrwclientsBanco: TIntegerField;
    QrwclientsAgencia: TIntegerField;
    QrwclientsDVAgencia: TWideStringField;
    QrwclientsPosto: TIntegerField;
    QrwclientsConta: TWideStringField;
    QrwclientsDVConta: TWideStringField;
    QrwclientsCodCedente: TWideStringField;
    QrwclientsAgContaCed: TWideStringField;
    QrwclientsIDCobranca: TWideStringField;
    QrwclientsDiaVencto: TIntegerField;
    QrwclientsBcoLogoPath: TWideStringField;
    QrwclientsCliLogoPath: TWideStringField;
    QrwclientsLocalPag: TWideStringField;
    QrwclientsEspecieDoc: TWideStringField;
    QrwclientsModalCobr: TSmallintField;
    QrwclientsOperCodi: TWideStringField;
    QrwclientsAceite: TWideStringField;
    QrwclientsCarteira: TWideStringField;
    QrwclientsEspecieVal: TWideStringField;
    QrwclientsInstrucao1: TWideStringField;
    QrwclientsInstrucao2: TWideStringField;
    QrwclientsInstrucao3: TWideStringField;
    QrwclientsInstrucao4: TWideStringField;
    QrwclientsInstrucao5: TWideStringField;
    QrwclientsInstrucao6: TWideStringField;
    QrwclientsInstrucao7: TWideStringField;
    QrwclientsInstrucao8: TWideStringField;
    QrwclientsCorreio: TWideStringField;
    QrwclientsCartEmiss: TIntegerField;
    QrwclientsPercJuros: TFloatField;
    QrwclientsPercMulta: TFloatField;
    QrwclientsTxtParecer: TWideMemoField;
    QrwclientsModelBloq: TSmallintField;
    QrwclientsCedente: TIntegerField;
    QrwclientsSacadAvali: TIntegerField;
    QrwclientsMBB: TSmallintField;
    QrwclientsMRB: TSmallintField;
    QrwclientsPBB: TSmallintField;
    QrwclientsMSB: TSmallintField;
    QrwclientsPSB: TSmallintField;
    QrwclientsMIB: TSmallintField;
    QrwclientsMPB: TSmallintField;
    QrwclientsMAB: TSmallintField;
    QrwclientsVTCBBNITAR: TFloatField;
    QrwclientsAlterWeb: TSmallintField;
    QrwclientsLk: TIntegerField;
    QrwclientsDataCad: TDateField;
    QrwclientsDataAlt: TDateField;
    QrwclientsUserCad: TIntegerField;
    QrwclientsUserAlt: TIntegerField;
    QrwclientsNCONDOM: TWideStringField;
    QrwclientsCONDTEL: TWideStringField;
    DBEdit001: TDBEdit;
    QrwclientsCNPJ_TXT: TWideStringField;
    QrwclientsCNPJ_CPF: TWideStringField;
    Label1: TLabel;
    DBEdit002: TDBEdit;
    dmkDBGUsers: TdmkDBGrid;
    QrPropriet: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    StringField2: TWideStringField;
    StringField3: TWideStringField;
    StringField4: TWideStringField;
    LargeintField1: TLargeintField;
    StringField5: TWideStringField;
    StringField6: TWideStringField;
    StringField7: TWideStringField;
    LargeintField2: TLargeintField;
    LargeintField3: TLargeintField;
    FloatField1: TFloatField;
    StringField8: TWideStringField;
    QrUsersCodigo: TIntegerField;
    QrUsersUSER_NOME: TWideStringField;
    QrUsersUSER_DOCU: TWideStringField;
    QrUsersUSER_TEL1: TWideStringField;
    QrUsersUnidade: TWideStringField;
    QrUsersAPTO: TIntegerField;
    QrUsersUser_ID: TIntegerField;
    QrUsersCodCliEsp: TIntegerField;
    QrUsersCodCliEnt: TIntegerField;
    QrUsersCodigoEnt: TIntegerField;
    QrUsersCodigoEsp: TIntegerField;
    QrUsersUsername: TWideStringField;
    QrUsersPassword: TWideStringField;
    QrUsersLoginID: TWideStringField;
    QrUsersLastAcess: TDateTimeField;
    QrUsersTipo: TSmallintField;
    QrUsersLk: TIntegerField;
    QrUsersDataCad: TDateField;
    QrUsersDataAlt: TDateField;
    QrUsersUserCad: TIntegerField;
    QrUsersUserAlt: TIntegerField;
    QrUsersNIVEL: TWideStringField;
    QrExiste: TmySQLQuery;
    QrExistePassword: TWideStringField;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    QrUsersUSUARIO: TLargeintField;
    QrUsersPropriet: TIntegerField;
    QrDonos: TmySQLQuery;
    DsDonos: TDataSource;
    dmkDBGDonos: TdmkDBGrid;
    QrDonosImoveis: TLargeintField;
    QrDonosDONO_NOME: TWideStringField;
    QrDonosDONO_DOCU: TWideStringField;
    QrDonosDONO_TEL1: TWideStringField;
    QrDonosPropriet: TIntegerField;
    QrDonosCodigo: TIntegerField;
    QrDonosUser_ID: TIntegerField;
    QrDonosCodCliEsp: TIntegerField;
    QrDonosCodCliEnt: TIntegerField;
    QrDonosCodigoEnt: TIntegerField;
    QrDonosCodigoEsp: TIntegerField;
    QrDonosUsername: TWideStringField;
    QrDonosPassword: TWideStringField;
    QrDonosLoginID: TWideStringField;
    QrDonosLastAcess: TDateTimeField;
    QrDonosTipo: TSmallintField;
    QrDonosLk: TIntegerField;
    QrDonosDataCad: TDateField;
    QrDonosDataAlt: TDateField;
    QrDonosUserCad: TIntegerField;
    QrDonosUserAlt: TIntegerField;
    QrDonosNIVEL: TWideStringField;
    PMDono: TPopupMenu;
    Alteraloginesenhadodonoatual1: TMenuItem;
    PainelDono: TPanel;
    Panel13: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label11: TLabel;
    Edit002: TdmkEdit;
    EdDLogin: TdmkEdit;
    EdDSenha: TdmkEdit;
    QrwclientsNomeWeb: TWideStringField;
    QrwclientsUsername: TWideStringField;
    QrwclientsPassword: TWideStringField;
    QrwclientsUser_ID: TAutoIncField;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    Label12: TLabel;
    DBEdit3: TDBEdit;
    Label13: TLabel;
    DBEdit4: TDBEdit;
    Label14: TLabel;
    DBEdit5: TDBEdit;
    RecriaLoginsesenhas1: TMenuItem;
    Atual1: TMenuItem;
    Selecionados1: TMenuItem;
    Todos1: TMenuItem;
    Recrialoginsesenhas2: TMenuItem;
    Atual2: TMenuItem;
    Selecionados2: TMenuItem;
    Todos2: TMenuItem;
    Label9: TLabel;
    DBEdit7: TDBEdit;
    Label15: TLabel;
    DBEdit8: TDBEdit;
    Label16: TLabel;
    DBEdit9: TDBEdit;
    GroupBox1: TGroupBox;
    dmkEdParc_ValMin: TdmkEdit;
    Label17: TLabel;
    Label18: TLabel;
    dmkEdParc_QtdMax: TdmkEdit;
    QrwclientsPwdSite: TSmallintField;
    QrwclientsParc_ValMin: TFloatField;
    QrwclientsParc_QtdMax: TIntegerField;
    GroupBox2: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label7: TLabel;
    EdNomeSindico: TdmkEdit;
    QrwclientsNomeSindico: TWideStringField;
    dmkEdMulta: TdmkEdit;
    dmkEdJuros: TdmkEdit;
    Label21: TLabel;
    Label22: TLabel;
    QrwclientsProLaINSSr: TFloatField;
    QrwclientsProLaINSSp: TFloatField;
    QrwclientsJuros: TFloatField;
    QrwclientsMulta: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel14: TPanel;
    BtSaida: TBitBtn;
    BtCliente: TBitBtn;
    BtUsuario: TBitBtn;
    BtDono: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel15: TPanel;
    PnSaiDesis: TPanel;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    GroupBox3: TGroupBox;
    Panel11: TPanel;
    Panel12: TPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Panel9: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GroupBox5: TGroupBox;
    Panel1: TPanel;
    Panel16: TPanel;
    BtConf2: TBitBtn;
    BitBtn2: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtClienteClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrwclientsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrwclientsAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesqKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtConf2Click(Sender: TObject);
    procedure QrwclientsCalcFields(DataSet: TDataSet);
    procedure QrClientesAfterOpen(DataSet: TDataSet);
    procedure QrClientesBeforeClose(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrwclientsBeforeClose(DataSet: TDataSet);
    procedure BtUsuarioClick(Sender: TObject);
    procedure Alterausurioatual1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure QrUsersCalcFields(DataSet: TDataSet);
    procedure QrDonosCalcFields(DataSet: TDataSet);
    procedure BtDonoClick(Sender: TObject);
    procedure Alteraloginesenhadodonoatual1Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure Atual1Click(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure Todos1Click(Sender: TObject);
    procedure Atual2Click(Sender: TObject);
    procedure Selecionados2Click(Sender: TObject);
    procedure Todos2Click(Sender: TObject);
    procedure dmkDBGUsersDblClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    FPageIndex: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    ////
    procedure RecriaLoginsESenhas(Quem: Integer; Quais: TSelType);
    procedure ReabreClientes;
    procedure ReopenUsers(APTO: Integer);
    procedure ReopenDonos(Propriet: Integer);
    function  GeraNovaSenha(): String;
    procedure EnableBtns();

  public
    { Public declarations }
    FSeq: Integer;
  end;

var
  Fmwclients: TFmwclients;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmwclients.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmwclients.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrwclientsCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmwclients.DefParams;
begin
  VAR_GOTOTABELA := 'cond';
  VAR_GOTOMYSQLTABLE := Qrwclients;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOMySQLDBNAME2 := nil;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT con.*, IF(rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM,');
  VAR_SQLx.Add('IF(rec.Tipo=0, rec.CNPJ, rec.CPF) CNPJ_CPF,');
  VAR_SQLx.Add('IF(rec.Tipo=0, rec.ETe1, rec.PTe2) CONDTEL,');
  VAR_SQLx.Add('wcl.NomeWeb, wcl.Username, wcl.Password, wcl.User_ID,');
  VAR_SQLx.Add('wcl.Parc_ValMin, wcl.Parc_QtdMax, wcl.NomeSindico, ');
  VAR_SQLx.Add('wcl.Juros, wcl.Multa ');
  VAR_SQLx.Add('FROM cond con');
  VAR_SQLx.Add('LEFT JOIN entidades rec ON rec.Codigo=con.Cliente');
  VAR_SQLx.Add('LEFT JOIN wclients  wcl ON wcl.CodCliEsp=con.Codigo');
  VAR_SQLx.Add('WHERE con.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND con.Codigo=:P0');
  //
  VAR_SQLa.Add('AND (IF(rec.Tipo=0, rec.RazaoSocial, rec.Nome) LIKE :P0');
  //
end;

procedure TFmwclients.dmkDBGUsersDblClick(Sender: TObject);
begin
  Geral.MensagemBox('Unidade: ' + QrUsersUnidade.Value + #13#10 +
  'Nome: ' + QrUsersUSER_NOME.Value + #13#10 +
  'Login: ' + QrUsersUsername.Value + #13#10 +
  'Senha: ' + QrUsersPassword.Value + #13#10 +
  'Telefone: ' + QrUsersUSER_TEL1.Value + #13#10, 'Dados',
  MB_OK+MB_ICONERROR(*for�ar envio de email*));
end;

procedure TFmwclients.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PainelWeb.Visible   := False;
      PainelUser.Visible  := False;
      PainelDono.Visible  := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      {if SQLType = stIns then
      begin
      end else begin}
        EdNomeWeb.Text     := QrwclientsNomeWeb.Value;
        EdUser2.Text       := QrwclientsUsername.Value;
        EdPass2.Text       := QrwclientsPassword.Value;
        EdNomeSindico.Text := QrwclientsNomeSindico.Value;
        dmkEdParc_ValMin.ValueVariant := QrwclientsParc_ValMin.Value;
        dmkEdParc_QtdMax.ValueVariant := QrwclientsParc_QtdMax.Value;
        dmkEdMulta.ValueVariant       := QrwclientsMulta.Value;
        dmkEdJuros.ValueVariant       := QrwclientsJuros.Value;
      //end;
      EdNomeWeb.SetFocus;
    end;
    2:
    begin
      PainelWeb.Visible   := True;
      PainelDados.Visible := False;
      EdPesq.SetFocus;
      ReabreClientes;
    end;
    3:
    begin
      PainelUser.Visible   := True;
      PainelDados.Visible := False;
      EdULogin.SetFocus;
      if SQLType = stUpd then
      begin
        EdULogin.Text := QrUsersUsername.Value;
        EdUSenha.Text := QrUsersPassword.Value;
      end;
    end;
    4:
    begin
      PainelDono.Visible   := True;
      PainelDados.Visible := False;
      EdDLogin.SetFocus;
      if SQLType = stUpd then
      begin
        EdDLogin.Text := QrDonosUsername.Value;
        EdDSenha.Text := QrDonosPassword.Value;
      end;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmwclients.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmwclients.QueryPrincipalAfterOpen;
begin
end;

procedure TFmwclients.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmwclients.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmwclients.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmwclients.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmwclients.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmwclients.BtClienteClick(Sender: TObject);
begin
  if QrwclientsUser_ID.Value = 0 then
    MostraEdicao(1, stIns, 0)
  else
    MostraEdicao(1, stUpd, 0);
end;

procedure TFmwclients.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrwclientsCodigo.Value;
  Close;
end;

procedure TFmwclients.BtConfirmaClick(Sender: TObject);
var
  Res: Boolean;
  NomeWeb : String;
  Parc_ValMin, Multa, Juros: Double;
  Parc_QtdMax: Integer;
begin
  NomeWeb := EdNomeWeb.Text;
  if Length(NomeWeb) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  Parc_ValMin := dmkEdParc_ValMin.ValueVariant;
  Parc_QtdMax := dmkEdParc_QtdMax.ValueVariant;
  Multa       := dmkEdMulta.ValueVariant;
  Juros       := dmkEdJuros.ValueVariant;
  //
  Res := False;
  if ImgTipo.SQLType = stIns then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wclients', True,
    [
      'NomeWeb', 'Username', 'Password', 'CodCliEsp',
      'CodCliEnt', 'Parc_ValMin', 'Parc_QtdMax', 'NomeSindico',
      'Multa', 'Juros'
    ], [],
    [
      EdNomeWeb.Text, EdUser2.Text, EdPass2.Text, QrwclientsCodigo.Value,
      QrwclientsCliente.Value, Parc_ValMin, Parc_QtdMax, EdNomeSindico.Text,
      Multa, Juros
    ], [], True) then
      Res := True;
  end else
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wclients', False,
    [
      'NomeWeb', 'Username', 'Password', 'CodCliEsp',
      'CodCliEnt', 'Parc_ValMin', 'Parc_QtdMax', 'NomeSindico',
      'Multa', 'Juros'
    ], ['User_ID'],
    [
      EdNomeWeb.Text, EdUser2.Text, EdPass2.Text, QrwclientsCodigo.Value,
      QrwclientsCliente.Value, Parc_ValMin, Parc_QtdMax, EdNomeSindico.Text,
      Multa, Juros
    ], [QrwclientsUser_ID.Value], True) then
      Res := True;
  end;
  if Res then
  begin
    LocCod(QrwclientsCodigo.Value, QrwclientsCodigo.Value);
    MostraEdicao(0, stLok, 0);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmwclients.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmwclients.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSeq := 0;
  QrPropriet.Open;
  Panel10.Align      := alClient;
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PainelEdit.Align   := alClient;
  DBGClientes.Align  := alClient;
  PageControl1.Align := alClient;
  PainelUser.Align   := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
end;

procedure TFmwclients.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrwclientsCodigo.Value,LaRegistro.Caption);
end;

procedure TFmwclients.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmwclients.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmwclients.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmwclients.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrwclientsCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmwclients.QrwclientsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  if Qrwclients.RecordCount > 0 then
    BtUsuario.Enabled := True
  else
    BtUsuario.Enabled := False;
end;

procedure TFmwclients.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'wclients', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, stIns, 0);
end;

procedure TFmwclients.QrwclientsAfterScroll(DataSet: TDataSet);
begin
  ReopenUsers(0);
  ReopenDonos(0);
end;

procedure TFmwclients.SbQueryClick(Sender: TObject);
begin
  LocCod(QrwclientsCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wclients', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmwclients.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmwclients.EdPesqKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ReabreClientes;
end;

procedure TFmwclients.SpeedButton5Click(Sender: TObject);
begin
  ReabreClientes;
end;

procedure TFmwclients.ReabreClientes;
begin
  QrClientes.Close;
  QrClientes.Params[0].AsString := '%'+EdPesq.Text+'%';
  QrClientes.Open;
end;

procedure TFmwclients.EdPesqChange(Sender: TObject);
begin
  QrClientes.Close;
end;

procedure TFmwclients.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmwclients.BtConf2Click(Sender: TObject);
begin
  if QrClientes.RecordCount = 0 then
  begin
    Geral.MensagemBox('Nenhum cliente foi selecionado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  QrWPesq.Close;
  QrWPesq.Params[0].AsInteger := QrClientesCodigo.Value;
  QrWPesq.Open;
  if QrWPesq.RecordCount > 0 then
  begin
    Geral.MensagemBox('Este cliente j� est� cadastrado na web!', 'Aviso',
      MB_OK+MB_ICONINFORMATION);
    Exit;
  end;
  if (Trim(EdUsername.Text) = '') or (Trim(EdPassword.Text) = '') then
  begin
    Geral.MensagemBox('Informa��es de Login e/ou senha inicial incompletas!',
      'Aviso', MB_OK+MB_ICONINFORMATION);
    Exit;
  end;
  //
  try
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO wclients SET Codigo=:P0, CNPJ=:P1, IE=:P2, ');
    Dmod.QrUpd.SQL.Add('Password=:P3, Nome=:P4, Rua=:P5, Numero=:P6, ');
    Dmod.QrUpd.SQL.Add('Compl=:P7, Bairro=:P8, Cidade=:P9, UF=:P10, CEP=:P11, ');
    Dmod.QrUpd.SQL.Add('Tel1=:P12, Risco=:P13, Username=:P14');
    //
    Dmod.QrUpd.Params[00].AsInteger := QrClientesCodigo.Value;
    Dmod.QrUpd.Params[01].AsString  := QrClientesCNPJCPF.Value;
    Dmod.QrUpd.Params[02].AsString  := QrClientesIERG.Value;
    Dmod.QrUpd.Params[03].AsString  := Trim(EdPassword.Text);
    Dmod.QrUpd.Params[04].AsString  := QrClientesNOMECLI.Value;
    Dmod.QrUpd.Params[05].AsString  := QrClientesRua.Value;
    Dmod.QrUpd.Params[06].AsInteger := QrClientesNumero.Value;
    Dmod.QrUpd.Params[07].AsString  := QrClientesCompl.Value;
    Dmod.QrUpd.Params[08].AsString  := QrClientesBairro.Value;
    Dmod.QrUpd.Params[09].AsString  := QrClientesCidade.Value;
    Dmod.QrUpd.Params[10].AsString  := MLAGeral.GetSiglaUF_do_CodigoUF(QrClientesUF.Value);
    Dmod.QrUpd.Params[11].AsInteger := QrClientesCEP.Value;
    Dmod.QrUpd.Params[12].AsString  := QrClientesTel1.Value;
    Dmod.QrUpd.Params[13].AsFloat   := QrClientesLimiCred.Value;
    Dmod.QrUpd.Params[14].AsString  := Trim(EdUsername.Text);
    Dmod.QrUpd.ExecSQL;
    //
    Geral.MensagemBox('Cliente adicionado � web!', 'Aviso',
      MB_OK+MB_ICONINFORMATION);
  except
    Geral.MensagemBox('N�o foi poss�vel adicionar o cliente � web!',
      'Aviso', MB_OK+MB_ICONINFORMATION);
    raise;
  end;
end;

procedure TFmwclients.QrwclientsCalcFields(DataSet: TDataSet);
begin
  QrwclientsCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrwclientsCNPJ_CPF.Value);
  //QrwclientsCEP_TXT.Value  := Geral.FormataCEP_NT(QrwclientsCEP.Value);
  //QrwclientsTEL1_TXT.value := Geral.FormataTelefone_TT(QrwclientsTel1.Value);
end;

procedure TFmwclients.QrClientesAfterOpen(DataSet: TDataSet);
begin
  if QrClientes.RecordCount > 0 then BtConf2.Enabled := True
  else BtConf2.Enabled := False;
end;

procedure TFmwclients.QrClientesBeforeClose(DataSet: TDataSet);
begin
  BtConf2.Enabled := False;
end;

procedure TFmwclients.BitBtn1Click(Sender: TObject);
var
  Res: Boolean;
begin
  Res := False;
  if ImgTipo.SQLType = stIns then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'users', True,
    [
      'Tipo', 'Username', 'Password', 'CodCliEsp', 'CodCliEnt',
      'CodigoEnt', 'CodigoEsp'
    ], [],
    [
      1, EdULogin.Text, EdUSenha.Text, QrwclientsCodigo.Value, QrwclientsCliente.Value,
      QrUsersPropriet.Value, QrUsersAPTO.Value
    ], [], True) then
      Res := True;
  end else
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'users', False,
    [
      'Tipo', 'Username', 'Password', 'CodCliEsp', 'CodCliEnt',
      'CodigoEnt', 'CodigoEsp'
    ], ['User_ID'],
    [
      1, EdULogin.Text, EdUSenha.Text, QrwclientsCodigo.Value, QrwclientsCliente.Value,
      QrUsersPropriet.Value, QrUsersAPTO.Value
    ], [QrUsersUser_ID.Value], True) then
      Res := True;
  end;
  if Res then
  begin
    ReopenUsers(QrUsersAPTO.Value);
    MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmwclients.ReopenUsers(APTO: Integer);
begin
  QrUsers.Close;
  QrUsers.Params[0].AsInteger := QrwclientsCodigo.Value;
  QrUsers.Open;
  if Apto <> 0 then
    QrUsers.Locate('APTO', APTO, []);
end;

procedure TFmwclients.ReopenDonos(Propriet: Integer);
begin
  QrDonos.Close;
  QrDonos.Params[0].AsInteger := QrwclientsCodigo.Value;
  QrDonos.Open;
  if Propriet <> 0 then
    QrDonos.Locate('Propriet', Propriet, []);
end;

procedure TFmwclients.QrwclientsBeforeClose(DataSet: TDataSet);
begin
  BtUsuario.Enabled := False;
end;

procedure TFmwclients.BtUsuarioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMUsuario, BtUsuario);
end;

procedure TFmwclients.Alterausurioatual1Click(Sender: TObject);
begin
  if QrUsersUser_ID.Value = 0 then
    MostraEdicao(3, stIns, 0)
  else
    MostraEdicao(3, stUpd, 0);
end;

procedure TFmwclients.PageControl1Change(Sender: TObject);
var
  User_ID: Integer;
begin
  if PageControl1.ActivePageIndex = 2 then
  begin
    QrVisitascli.Close;
    case FPageIndex of
      0: User_ID := QrUsersUser_ID.Value;
      1: User_ID := QrDonosUser_ID.Value;
      else User_ID := 0;
    end;
    if User_ID > 0 then
    begin
      QrVisitascli.Params[0].AsInteger := User_ID;
      QrVisitascli.Open;
    end;
  end;
  EnableBtns;
end;

procedure TFmwclients.QrUsersCalcFields(DataSet: TDataSet);
begin
  case QrUsersTipo.Value of
    1: QrUsersNIVEL.Value := 'Usu�rio';
    3: QrUsersNIVEL.Value := 'Dono de im�vel';
    7: QrUsersNIVEL.Value := 'S�ndico';
    9: QrUsersNIVEL.Value := 'Administrador';
    else QrUsersNIVEL.Value := '? ? ?';
  end;
end;

procedure TFmwclients.RecriaLoginsESenhas(Quem: Integer; Quais: TSelType);
  procedure Users;
  var
    Nivel: Integer;
    UserName, Password: String;
    ImgTipo: TSQLType;
  begin
    UserName := 'uh' + IntToStr(QrUsersAPTO.Value);
    Password := GeraNovaSenha;
    //
    if QrUsersUser_ID.Value = 0 then
    begin
      ImgTipo := stIns;
      Nivel  := 1;
    end else begin
      ImgTipo := stUpd;
      Nivel  := QrUsersTipo.Value;
    end;
    //
    if ImgTipo = stIns then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo, 'users', True,
      [
        'Tipo', 'Username', 'Password', 'CodCliEsp', 'CodCliEnt',
        'CodigoEnt', 'CodigoEsp'
      ], [],
      [
        Nivel, UserName, Password, QrwclientsCodigo.Value, QrwclientsCliente.Value,
        QrUsersPropriet.Value, QrUsersAPTO.Value
      ], [], True);
    end else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo, 'users', False,
      [
        'Tipo', 'Username', 'Password', 'CodCliEsp', 'CodCliEnt',
        'CodigoEnt', 'CodigoEsp'
      ], ['User_ID'],
      [
        Nivel, UserName, Password, QrwclientsCodigo.Value, QrwclientsCliente.Value,
        QrUsersPropriet.Value, QrUsersAPTO.Value
      ], [QrUsersUser_ID.Value], True);
    end;
  end;


  procedure Donos;
  var
    Nivel: Integer;
    DonoName, Password: String;
    ImgTipo: TSQLType;
  begin
    DonoName := 'di' + IntToStr(QrDonosPropriet.Value);
    Password := GeraNovaSenha;
    //
    if QrDonosUser_ID.Value = 0 then
    begin
      ImgTipo := stIns;
      Nivel  := 3;
    end else begin
      ImgTipo := stUpd;
      Nivel  := QrDonosTipo.Value;
    end;
    //
    if ImgTipo = stIns then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo, 'users', True,
      [
        'Tipo', 'Username', 'Password', 'CodCliEsp', 'CodCliEnt',
        'CodigoEnt', 'CodigoEsp'
      ], [],
      [
        Nivel, DonoName, Password, QrwclientsCodigo.Value, QrwclientsCliente.Value,
        QrDonosPropriet.Value, 0
      ], [], True);
    end else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo, 'users', False,
      [
        'Tipo', 'Username', 'Password', 'CodCliEsp', 'CodCliEnt',
        'CodigoEnt', 'CodigoEsp'
      ], ['User_ID'],
      [
        Nivel, DonoName, Password, QrwclientsCodigo.Value, QrwclientsCliente.Value,
        QrDonosPropriet.Value, 0
      ], [QrDonosUser_ID.Value], True);
    end;
  end;


var
  Apto, Itens, i, Propriet: Integer;
  sQuem, sQuais: String;
begin
  case Quem of
    1:
    begin
      sQuem := 'usu�rio';
      Itens := dmkDBGUsers.SelectedRows.Count;
    end;
    3:
    begin
      sQuem := 'dono';
      Itens := dmkDBGDonos.SelectedRows.Count;
    end;
    else
    begin
      sQuem := '?';
      Itens := 0;
    end;
  end;
  case Quais of
    istAtual: sQuais := 'do loguin e senha';
    istSelecionados:
    begin
      if itens < 2 then
        sQuais := 'do loguin e senha'
      else
        sQuais := 'dos loguins e senhas';
    end;
    istTodos: sQuais := 'de todos loguins e senhas';
  end;
  if Itens > 1 then
    sQuem := ' dos ' + sQuem + 's'
   else
    sQuem := ' do ' + sQuem;

  if Geral.MensagemBox('Confirma a recria��o ' + sQuais + sQuem +
  ' do condom�nio atual?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    if Quem = 1 then
    begin
      Apto := QrUsersAPTO.Value;
      if Quais = istTodos then
      begin
        QrUsers.First;
        while not QrUsers.Eof do
        begin
          Users;
          QrUsers.Next;
        end;
      end else if Quais = istSelecionados then
      begin
        if Itens < 2 then Users else
        with dmkDBGUsers.DataSource.DataSet do
        begin
          for i:= 0 to dmkDBGUsers.SelectedRows.Count-1 do
          begin
            GotoBookmark(pointer(dmkDBGUsers.SelectedRows.Items[i]));
            Users;
          end;
        end;
      end else Users;
      ReopenUsers(Apto);
    end  else if Quem = 3 then
    begin
      Propriet := QrDonosPropriet.Value;
      if Quais = istTodos then
      begin
        QrDonos.First;
        while not QrDonos.Eof do
        begin
          Donos;
          QrDonos.Next;
        end;
      end else if Quais = istSelecionados then
      begin
        if Itens < 2 then Donos else
        with dmkDBGDonos.DataSource.DataSet do
        begin
          for i:= 0 to dmkDBGDonos.SelectedRows.Count-1 do
          begin
            GotoBookmark(pointer(dmkDBGDonos.SelectedRows.Items[i]));
            Donos;
          end;
        end;
      end else Donos;
      ReopenDonos(Propriet);
    end;
  end;
end;

function TFmwclients.GeraNovaSenha(): String;
var
  Senha: String;
  Nova: Boolean;
begin
  Nova := False;
  while not Nova do
  begin
    Senha := MLAGeral.GeraPassword(True, False, True, 8);
    QrExiste.Close;
    QrExiste.Params[0].AsString := Senha;
    QrExiste.Open;
    Nova := QrExiste.RecordCount = 0;
  end;
  Result := Senha;
end;

procedure TFmwclients.QrDonosCalcFields(DataSet: TDataSet);
begin
  case QrDonosTipo.Value of
    1: QrDonosNIVEL.Value := 'Usu�rio';
    3: QrDonosNIVEL.Value := 'Dono de im�vel';
    7: QrDonosNIVEL.Value := 'S�ndico';
    9: QrDonosNIVEL.Value := 'Administrador';
    else QrDonosNIVEL.Value := '? ? ?';
  end;
end;

procedure TFmwclients.EnableBtns();
begin
  BtUsuario.Enabled := PageControl1.ActivePageIndex = 0;
  BtDono.Enabled    := PageControl1.ActivePageIndex = 1;
end;

procedure TFmwclients.BtDonoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDono, BtDono);
end;

procedure TFmwclients.Alteraloginesenhadodonoatual1Click(Sender: TObject);
begin
  if QrDonosUser_ID.Value = 0 then
    MostraEdicao(4, stIns, 0)
  else
    MostraEdicao(4, stUpd, 0);
end;

procedure TFmwclients.BitBtn4Click(Sender: TObject);
var
  Res: Boolean;
begin
  Res := False;
  if ImgTipo.SQLType = stIns then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'users', True,
    [
      'Tipo', 'Username', 'Password', 'CodCliEsp', 'CodCliEnt',
      'CodigoEnt', 'CodigoEsp'
    ], [],
    [
      3, EdDLogin.Text, EdDSenha.Text, QrwclientsCodigo.Value, QrwclientsCliente.Value,
      QrDonosPropriet.Value, 0
    ], [], True) then
      Res := True;
  end else
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'users', False,
    [
      'Tipo', 'Username', 'Password', 'CodCliEsp', 'CodCliEnt',
      'CodigoEnt', 'CodigoEsp'
    ], ['User_ID'],
    [
      3, EdDLogin.Text, EdDSenha.Text, QrwclientsCodigo.Value, QrwclientsCliente.Value,
      QrDonosPropriet.Value, 0
    ], [QrDonosUser_ID.Value], True) then
      Res := True;
  end;
  if Res then
  begin
    ReopenDonos(QrDonosUser_ID.Value);
    MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmwclients.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  FPageIndex := PageControl1.ActivePageIndex;
end;

procedure TFmwclients.Atual1Click(Sender: TObject);
begin
  RecriaLoginsESenhas(1, istAtual);
end;

procedure TFmwclients.Selecionados1Click(Sender: TObject);
begin
  RecriaLoginsESenhas(1, istSelecionados);
end;

procedure TFmwclients.Todos1Click(Sender: TObject);
begin
  RecriaLoginsESenhas(1, istTodos);
end;

procedure TFmwclients.Atual2Click(Sender: TObject);
begin
  RecriaLoginsESenhas(3, istAtual);
end;

procedure TFmwclients.Selecionados2Click(Sender: TObject);
begin
  RecriaLoginsESenhas(3, istSelecionados);
end;

procedure TFmwclients.Todos2Click(Sender: TObject);
begin
  RecriaLoginsESenhas(3, istTodos);
end;

end.

