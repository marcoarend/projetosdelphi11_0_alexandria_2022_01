object FmVerifiBloquetosValor: TFmVerifiBloquetosValor
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-034 :: Valor de Boleto - Gerencial X Financeiro'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 483
        Height = 32
        Caption = 'Valor de Boleto - Gerencial X Financeiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 483
        Height = 32
        Caption = 'Valor de Boleto - Gerencial X Financeiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 483
        Height = 32
        Caption = 'Valor de Boleto - Gerencial X Financeiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 103
    Width = 1008
    Height = 526
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1008
      Height = 526
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Pesquisa'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 498
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 498
            Align = alClient
            TabOrder = 0
            object Splitter1: TSplitter
              Left = 422
              Top = 15
              Width = 10
              Height = 411
              ExplicitLeft = 164
              ExplicitTop = 11
              ExplicitHeight = 450
            end
            object DBGrid1: TdmkDBGridZTO
              Left = 432
              Top = 15
              Width = 566
              Height = 411
              Align = alClient
              DataSource = DsArreLct
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
            end
            object dmkDBGrid1: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 420
              Height = 411
              Align = alLeft
              DataSource = DsEmpresas
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodCliInt'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 300
                  Visible = True
                end>
            end
            object GBRodaPe: TGroupBox
              Left = 2
              Top = 426
              Width = 996
              Height = 70
              Align = alBottom
              Color = clBtnFace
              ParentBackground = False
              ParentColor = False
              TabOrder = 2
              object PnSaiDesis: TPanel
                Left = 850
                Top = 15
                Width = 144
                Height = 53
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 1
                object BtSaida1: TBitBtn
                  Tag = 13
                  Left = 12
                  Top = 3
                  Width = 120
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Sair'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaida1Click
                end
              end
              object Panel1: TPanel
                Left = 2
                Top = 15
                Width = 848
                Height = 53
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object BtCarrega: TBitBtn
                  Tag = 14
                  Left = 12
                  Top = 4
                  Width = 120
                  Height = 40
                  Caption = '&Carrega'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtCarregaClick
                end
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Resultados'
        ImageIndex = 1
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 1000
          Height = 428
          Align = alClient
          DataSource = DsBolVal
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'CliInt'
              Title.Caption = 'Empresa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EMPR'
              Title.Caption = 'Nome empresa'
              Width = 378
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Lancto'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TIPO'
              Title.Caption = 'Tipo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorLct'
              Title.Caption = '$ Fina'#231'as'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorPeri'
              Title.Caption = '$ Gerencial'
              Width = 76
              Visible = True
            end>
        end
        object GroupBox2: TGroupBox
          Left = 0
          Top = 428
          Width = 1000
          Height = 70
          Align = alBottom
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 1
          object Panel5: TPanel
            Left = 854
            Top = 15
            Width = 144
            Height = 53
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BtSaida2: TBitBtn
              Tag = 13
              Left = 12
              Top = 3
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Sair'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaida1Click
            end
          end
          object Panel6: TPanel
            Left = 2
            Top = 15
            Width = 852
            Height = 53
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object BtImprime: TBitBtn
              Tag = 14
              Left = 12
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Imprime'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtImprimeClick
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 55
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
        Visible = False
      end
    end
  end
  object QrEmpresas: TmySQLQuery
    Database = DModG.AllID_DB
    BeforeClose = QrEmpresasBeforeClose
    AfterScroll = QrEmpresasAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM _empresas_'
      'WHERE CodCliInt > 0')
    Left = 96
    Top = 144
    object QrEmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object QrEmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object QrEmpresasCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
    object QrEmpresasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEmpresasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 125
    Top = 144
  end
  object QrPrev: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 88
    Top = 224
  end
  object QrLct: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 160
    Top = 240
  end
  object QrArreLct: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT bov.*, ari.Texto, lct.Vencimento, '
      'sen.Login UserCad, se2.Login UserAlt, lct.DataCad, lct.DataAlt'
      'FROM _boletos_valor_ bov'
      'LEFT JOIN syndic.ari0039a ari ON ari.Controle = bov.Controle'
      'LEFT JOIN syndic.lct0039a lct ON lct.Controle = bov.Lancto'
      'LEFT JOIN syndic.senhas sen ON sen.Numero = lct.UserCad'
      'LEFT JOIN syndic.senhas se2 ON se2.Numero = lct.UserAlt'
      'WHERE bov.CliInt=:P0')
    Left = 136
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArreLctVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrArreLctTexto: TWideStringField
      FieldName = 'Texto'
      Size = 50
    end
    object QrArreLctUserCad: TWideStringField
      FieldName = 'UserCad'
      Size = 30
    end
    object QrArreLctUserAlt: TWideStringField
      FieldName = 'UserAlt'
      Size = 30
    end
    object QrArreLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrArreLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrArreLctArreBaC: TIntegerField
      FieldName = 'ArreBaC'
    end
  end
  object DsArreLct: TDataSource
    DataSet = QrArreLct
    Left = 165
    Top = 400
  end
  object QrBolval: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPR, b_v.*,'
      'ELT(b_v.Tipo + 1, "Arrecada'#231#227'o", "Leitura", "? ? ? ? ?") NO_TIPO'
      'FROM _boletos_valor_ b_v'
      'LEFT JOIN syndic.enticliint eci ON eci.CodCliInt=b_v.CliInt'
      'LEFT JOIN syndic.entidades ent ON ent.Codigo=eci.CodEnti'
      'ORDER BY b_v.CliInt')
    Left = 464
    Top = 268
    object QrBolvalNO_EMPR: TWideStringField
      FieldName = 'NO_EMPR'
      Size = 100
    end
    object QrBolvalCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBolvalControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBolvalLancto: TIntegerField
      FieldName = 'Lancto'
      Required = True
    end
    object QrBolvalCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrBolvalValorLct: TFloatField
      FieldName = 'ValorLct'
      Required = True
    end
    object QrBolvalValorPeri: TFloatField
      FieldName = 'ValorPeri'
      Required = True
    end
    object QrBolvalTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrBolvalNO_TIPO: TWideStringField
      FieldName = 'NO_TIPO'
    end
  end
  object DsBolVal: TDataSource
    DataSet = QrBolval
    Left = 464
    Top = 316
  end
  object frxGER_CONDM_034: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxGER_CONDM_034GetValue
    Left = 464
    Top = 404
    Datasets = <
      item
        DataSet = frxDsBolVal
        DataSetName = 'frxDsBolVal'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 158.740260000000000000
          Top = 18.897650000000000000
          Width = 362.834880000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor de Boleto - Gerencial X Financeiro')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 521.575140000000100000
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 17.007874020000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        DataSet = frxDsBolVal
        DataSetName = 'frxDsBolVal'
        RowCount = 0
        object Memo9: TfrxMemoView
          Left = 264.567100000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Lancto'
          DataSet = frxDsBolVal
          DataSetName = 'frxDsBolVal'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBolVal."Lancto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 328.819110000000000000
          Width = 79.370130000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_TIPO'
          DataSet = frxDsBolVal
          DataSetName = 'frxDsBolVal'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBolVal."NO_TIPO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 408.189240000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDsBolVal
          DataSetName = 'frxDsBolVal'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBolVal."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 468.661720000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Controle'
          DataSet = frxDsBolVal
          DataSetName = 'frxDsBolVal'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBolVal."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 532.913730000000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'ValorLct'
          DataSet = frxDsBolVal
          DataSetName = 'frxDsBolVal'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBolVal."ValorLct"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 608.504330000000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'ValorPeri'
          DataSet = frxDsBolVal
          DataSetName = 'frxDsBolVal'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBolVal."ValorPeri"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Header1: TfrxHeader
        Height = 34.015779770000000000
        Top = 124.724490000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          Top = 0.000000001968487595
          Width = 260.787570000000000000
          Height = 34.015748031496060000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 264.567100000000000000
          Top = 17.007905750000020000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lancto')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 328.819110000000000000
          Top = 17.007905749999910000
          Width = 79.370130000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 408.189240000000000000
          Top = 17.007905750000020000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. Per'#237'odo')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 468.661720000000000000
          Top = 17.007905749999910000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 532.913730000000000000
          Top = 17.007905749999910000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Financeiro')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 608.504330000000000000
          Top = 17.007905749999910000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Gerencial')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 328.819110000000000000
          Width = 200.315090000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gerencial (boletos)')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 532.913730000000000000
          Width = 147.401670000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valores diferentes')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 264.567100000000000000
          Width = 60.472480000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Financeiro')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        Height = 7.559060000000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 17.007874020000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsBolVal."CliInt"'
        object Memo8: TfrxMemoView
          Width = 37.795300000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'CliInt'
          DataSet = frxDsBolVal
          DataSetName = 'frxDsBolVal'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBolVal."CliInt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 37.795300000000000000
          Width = 222.992270000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_EMPR'
          DataSet = frxDsBolVal
          DataSetName = 'frxDsBolVal'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBolVal."NO_EMPR"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsBolVal: TfrxDBDataset
    UserName = 'frxDsBolVal'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_EMPR=NO_EMPR'
      'Codigo=Codigo'
      'Controle=Controle'
      'Lancto=Lancto'
      'CliInt=CliInt'
      'ValorLct=ValorLct'
      'ValorPeri=ValorPeri'
      'Tipo=Tipo'
      'NO_TIPO=NO_TIPO')
    DataSet = QrBolval
    BCDToCurrency = False
    Left = 464
    Top = 360
  end
end
