unit CondOutros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkRadioGroup, Mask, dmkDBEdit,
  dmkValUsu, DB, mySQLDbTables, dmkGeral, Variants, dmkImage, UnDmkEnums;

type
  TFmCondOutros = class(TForm)
    Panel1: TPanel;
    Label9: TLabel;
    EdNome: TdmkEditCB;
    CBNome: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    Label12: TLabel;
    EdCodigo: TdmkEdit;
    Label61: TLabel;
    EdUnidade: TdmkEdit;
    EdCargo: TdmkEditCB;
    Label44: TLabel;
    CBCargo: TdmkDBLookupComboBox;
    SpeedButton6: TSpeedButton;
    EdOrdem: TdmkEdit;
    Label62: TLabel;
    RGParecer: TdmkRadioGroup;
    dmkDBEdCodigo: TdmkDBEdit;
    Label1: TLabel;
    QrNome: TmySQLQuery;
    QrNomeCodigo: TIntegerField;
    QrNomeNome: TWideStringField;
    QrNomeCodUsu: TIntegerField;
    DsNome: TDataSource;
    VUNome: TdmkValUsu;
    QrCargos: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    DsCargos: TDataSource;
    VUCargos: TdmkValUsu;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCondOutros: TFmCondOutros;

implementation

uses Principal, UnInternalConsts, UMySQLModule, Cond, Module, MyDBCheck, Cargos,
  ModuleGeral, UnMyObjects;

{$R *.DFM}

procedure TFmCondOutros.BtOKClick(Sender: TObject);
var
  Controle, Nome, Cargo: Integer;
begin
  Nome  := EdNome.ValueVariant;
  Cargo := EdCargo.ValueVariant;
  //
  if Nome = 0 then
  begin
    Geral.MensagemBox('Nome n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdNome.SetFocus;
    Exit;
  end;
  if Cargo = 0 then
  begin
    Geral.MensagemBox('Cargo n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdCargo.SetFocus;
    Exit;
  end;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('condoutros', 'Controle', ImgTipo.SQLType,
    FmCond.QrCondOutrosControle.Value);
  if UMyMod.ExecSQLInsUpdFm(FmCondOutros, ImgTipo.SQLType, 'condoutros', Controle,
    Dmod.QrUpd) then
  begin
    FmCond.ReopenCondOutros(Controle);
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
      ImgTipo.SQLType         := stIns;
      EdNome.ValueVariant    := 0;
      CBNome.KeyValue        := Null;
      EdUnidade.ValueVariant := '';
      EdOrdem.ValueVariant   := EdOrdem.ValueVariant + 1;
      EdNome.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmCondOutros.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondOutros.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  EdNome.SetFocus;
end;

procedure TFmCondOutros.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrNome.Open;
  QrCargos.Open;
end;

procedure TFmCondOutros.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondOutros.SpeedButton5Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(EdNome.ValueVariant, fmcadSelecionar, fmcadSelecionar);
  if VAR_CADASTRO > 0 then
  begin
    QrNome.Close;
    QrNome.Open;
    UMyMod.SetaCodUsuDeCodigo(EdNome, CBNome, QrNome, VAR_CADASTRO);
  end;
end;

procedure TFmCondOutros.SpeedButton6Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCargos, FmCargos, afmoNegarComAviso) then
  begin
    FmCargos.ShowModal;
    FmCargos.Destroy;
  end;
  if VAR_CADASTRO > 0 then
  begin
    QrCargos.Close;
    QrCargos.Open;
    UMyMod.SetaCodUsuDeCodigo(EdCargo, CBCargo, QrCargos, VAR_CADASTRO);
  end;
end;

end.
