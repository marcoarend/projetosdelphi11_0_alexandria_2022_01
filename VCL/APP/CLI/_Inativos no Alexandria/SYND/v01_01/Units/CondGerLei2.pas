unit CondGerLei2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Grids,
  DBGrids, dmkEdit, dmkDBLookupComboBox, dmkEditCB, Mask, dmkGeral,
  UnDmkProcFunc, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmCondGerLei2 = class(TForm)
    PnProduto: TPanel;
    QrCons: TmySQLQuery;
    DsCons: TDataSource;
    QrConsCodigo: TIntegerField;
    QrConsNome: TWideStringField;
    PnLeitura: TPanel;
    QrAptos: TmySQLQuery;
    QrAptosConta: TIntegerField;
    QrAnt: TmySQLQuery;
    QrAntApto: TIntegerField;
    QrAntMedAtu: TFloatField;
    QrAptosMedAnt: TFloatField;
    TbConsLei: TmySQLTable;
    DBGrid1: TDBGrid;
    DsConsLei: TDataSource;
    TbConsLeiID_Rand: TWideStringField;
    TbConsLeiApto_ID: TIntegerField;
    TbConsLeiMedAnt: TFloatField;
    TbConsLeiMedAtu: TFloatField;
    TbConsLeiAdiciona: TSmallintField;
    STCli: TStaticText;
    QrConsCasas: TSmallintField;
    TbConsLeiGASTO: TFloatField;
    QrPesq: TmySQLQuery;
    QrConsPreco: TFloatField;
    QrJaTem: TmySQLQuery;
    QrJaTemApto: TIntegerField;
    QrAptosAptoJa: TIntegerField;
    QrMax: TmySQLQuery;
    QrAptosUnidade: TWideStringField;
    TbConsLeiApto_Un: TWideStringField;
    QrConsUnidFat: TFloatField;
    QrConsUnidLei: TWideStringField;
    QrConsUnidImp: TWideStringField;
    TbConsLeiPropriet: TIntegerField;
    QrAptosPropriet: TIntegerField;
    TbConsLeiVALOR: TFloatField;
    QrMedias: TmySQLQuery;
    QrMediasVezes: TLargeintField;
    QrMediasMENOR: TFloatField;
    QrMediasMEDIA: TFloatField;
    QrMediasMAIOR: TFloatField;
    QrMediasApto: TIntegerField;
    TbConsLeiMEDIA_VEZES: TFloatField;
    TbConsLeiMEDIA_MENOR: TFloatField;
    TbConsLeiMEDIA_MEDIA: TFloatField;
    TbConsLeiMEDIA_MAIOR: TFloatField;
    QrUni: TmySQLQuery;
    QrUniApto: TIntegerField;
    QrUniMedAtu: TFloatField;
    QrConsContaBase: TIntegerField;
    PanelZ: TPanel;
    Label1: TLabel;
    EdPeriodo: TEdit;
    EdProduto: TdmkEditCB;
    Label2: TLabel;
    CBProduto: TdmkDBLookupComboBox;
    EdPerioBase: TdmkEdit;
    Label5: TLabel;
    Panel3: TPanel;
    EdValorBase: TdmkEdit;
    Label4: TLabel;
    QrFracao: TmySQLQuery;
    QrFracaoTOTAL: TFloatField;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DsFracao: TDataSource;
    QrConsFatorBase: TSmallintField;
    EdUnitario: TdmkEdit;
    Label7: TLabel;
    QrAptosMoradores: TIntegerField;
    QrAptosFracaoIdeal: TFloatField;
    EdGastoTotal: TdmkEdit;
    LaGastoTotal: TLabel;
    LaGastoUnita: TLabel;
    EdGastoUnita: TdmkEdit;
    TbConsLeiQtdFator: TFloatField;
    QrConsArredonda: TFloatField;
    QrConsCasRat: TSmallintField;
    QrConsPerioBase: TSmallintField;
    TbConsLeiValUnita: TFloatField;
    QrAptosAndar: TIntegerField;
    QrAptosOrdem: TIntegerField;
    QrConsNaoImpLei: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrConsCNAB_Cfg: TIntegerField;
    QrLoc: TmySQLQuery;
    SpeedButton7: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdProdutoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure TbConsLeiBeforePost(DataSet: TDataSet);
    procedure TbConsLeiCalcFields(DataSet: TDataSet);
    procedure TbConsLeiBeforeOpen(DataSet: TDataSet);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EdValorBaseChange(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
  private
    { Private declarations }
    FID_Rand: String;
    procedure CalculaValorUnitario;
    procedure AtualizaDadosProduto();
  public
    { Public declarations }
    FPeriodo, FCond, F_CliInt: Integer;
  end;

  var
  FmCondGerLei2: TFmCondGerLei2;

implementation

uses Module, UCreate, UnInternalConsts, UMySQLModule, ModuleCond, ModuleGeral,
  UnMyObjects, UnBloquetosCond;

{$R *.DFM}

procedure TFmCondGerLei2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerLei2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerLei2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerLei2.SpeedButton7Click(Sender: TObject);
var
  Produto: Integer;
begin
  Produto      := EdProduto.ValueVariant;
  VAR_CADASTRO := 0;
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrCons, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdProduto, CBProduto, QrCons, VAR_CADASTRO);
    EdProduto.SetFocus;
    //
    AtualizaDadosProduto();
  end;
end;

procedure TFmCondGerLei2.AtualizaDadosProduto();
var
  Mez: Integer;
begin
  if QrConsPerioBase.Value = 1 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT MAX(lan.Mez) Mez');
    Dmod.QrAux.SQL.Add('FROM ' + DmCond.FTabLctA + ' lan');
    Dmod.QrAux.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    Dmod.QrAux.SQL.Add('WHERE car.Tipo < 2');
    Dmod.QrAux.SQL.Add('AND lan.CliInt=:P0');
    Dmod.QrAux.SQL.Add('AND lan.Genero=:P1');
    Dmod.QrAux.Params[00].AsInteger := F_CliInt;
    Dmod.QrAux.Params[01].AsInteger := QrConsContaBase.Value;
    Dmod.QrAux.Open;
    //
    Mez := Dmod.QrAux.FieldByName('Mez').AsInteger;
  end else
    Mez := MLAGeral.PeriodoToMez(FPeriodo + QrConsPerioBase.Value);
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT SUM(lan.Debito) Debito');
  Dmod.QrAux.SQL.Add('FROM ' + DmCond.FTabLctA + ' lan');
  Dmod.QrAux.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  Dmod.QrAux.SQL.Add('WHERE car.Tipo < 2');
  Dmod.QrAux.SQL.Add('AND lan.CliInt=:P0');
  Dmod.QrAux.SQL.Add('AND lan.Genero=:P1');
  Dmod.QrAux.SQL.Add('AND lan.Mez=:P2');
  Dmod.QrAux.Params[00].AsInteger := F_CliInt;
  Dmod.QrAux.Params[01].AsInteger := QrConsContaBase.Value;
  Dmod.QrAux.Params[02].AsInteger := Mez;
  Dmod.QrAux.Open;
  //
  EdValorBase.ValueVariant := DMod.QrAux.FieldByName('Debito').AsFloat;
  EdPerioBase.Text := Uppercase(MLAGeral.MezToFDT(Mez, 0, 18));
  //
  QrFracao.Close;
  QrFracao.SQL.Clear;
  case QrConsFatorBase.Value of
    // Moradores
    0: QrFracao.SQL.Add('SELECT SUM(Moradores) TOTAL');
    // Fra��o ideal
    1: QrFracao.SQL.Add('SELECT SUM(FracaoIdeal) TOTAL');
    else // ERRO
    QrFracao.SQL.Add('SELECT SUM(???) TOTAL');
  end;
  QrFracao.SQL.Add('FROM condimov');
  QrFracao.SQL.Add('WHERE Codigo=' + FormatFloat('0', FCond));
  QrFracao.Open;
  //
  CalculaValorUnitario;
end;

procedure TFmCondGerLei2.EdProdutoChange(Sender: TObject);
begin
  AtualizaDadosProduto;
end;

procedure TFmCondGerLei2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TbConsLei.Close;
  TbConsLei.DataBase := DModG.MyPID_DB;
  QrPesq.Close;
  QrPesq.DataBase := DModG.MyPID_DB;
  //
  PnProduto.Align := alClient;
end;

procedure TFmCondGerLei2.BtOKClick(Sender: TObject);
var
  Produto, i, Controle, AptoNao, CNAB_Cfg: Integer;
  FmtTxT, Msg: String;
  MedAtu, Unitario, Qtde: Double;
begin
  Produto := EdProduto.ValueVariant;
  //
  if Produto = 0 then
  begin
    Geral.MB_Aviso('Nenhum produto foi selecionado!');
    EdProduto.SetFocus;
    Exit;
  end;
  Unitario := EdGastoUnita.ValueVariant;
  //
  if Unitario <= 0 then
  begin
    Geral.MB_Aviso('O valor do gasto unit�rio n�o foi definido!');
    EdGastoUnita.SetFocus;
    Exit;
  end;
  if QrFracaoTOTAL.Value <= 0 then
  begin
    Geral.MB_Aviso('O fator total n�o foi definido!' + sLineBreak +
      'Verifique no cadastro das unidades habitacionais e no cadastro de consumo!');
    Exit;
  end;
  Msg      := '';
  CNAB_Cfg := 0;
  //
  if not UBloquetosCond.ValidaArrecadacaoConsumoCond(FCond, CNAB_Cfg, QrLoc,
    Dmod.MyDB, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    EdProduto.SetFocus;
    Exit;
  end;
  if not PnLeitura.Visible then
  begin
    Screen.Cursor := crHourGlass;
    try
      AptoNao := 0;
      QrAptos.Close;
      QrAptos.Params[00].AsInteger := FCond;
      QrAptos.Open;
      //
      UCriar.RecriaTempTable('ConsLei', DModG.QrUpdPID1, False);
      FID_Rand := MLAGeral.SoNumeroELetra_TT(dmkPF.PWDGenerateSecutityString);
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('INSERT INTO conslei SET ');
      DModG.QrUpdPID1.SQL.Add('ID_Rand=:P0, Apto_ID=:P1, Apto_Un=:P2, ');
      DModG.QrUpdPID1.SQL.Add('MedAnt=:P3, MedAtu=:P4, Propriet=:P5, ');
      DModG.QrUpdPID1.SQL.Add('QtdFator=:P6, ValUnita=:P7, OrdBlc=:P8, ');
      DModG.QrUpdPID1.SQL.Add('Andar=:P9');
      QrAptos.First;
      while not QrAptos.Eof do
      begin
        case QrConsFatorBase.Value of
          // Moradores
          0: Qtde := QrAptosMoradores.Value;
          // Fra��o ideal
          1: Qtde := QrAptosFracaoIdeal.Value;
          else Qtde := 0;
        end;
        if Qtde = 0 then Inc(AptoNao, 1);
        //if Qtde > 0 then Inc(AptoSim, 1);
        // N�o h� medida anterior
        MedAtu := Qtde * Unitario;
        //
        DModG.QrUpdPID1.Params[00].AsString  := FID_Rand;
        DModG.QrUpdPID1.Params[01].AsInteger := QrAptosConta.Value;
        DModG.QrUpdPID1.Params[02].AsString  := QrAptosUnidade.Value;
        DModG.QrUpdPID1.Params[03].AsFloat   := 0;//MedAnt;
        DModG.QrUpdPID1.Params[04].AsFloat   := MedAtu;
        DModG.QrUpdPID1.Params[05].AsInteger := QrAptosPropriet.Value;
        DModG.QrUpdPID1.Params[06].AsFloat   := Qtde;
        DModG.QrUpdPID1.Params[07].AsFloat   := EdUnitario.ValueVariant;
        DModG.QrUpdPID1.Params[08].AsInteger := QrAptosOrdem.Value;
        DModG.QrUpdPID1.Params[09].AsInteger := QrAptosAndar.Value;
        DModG.QrUpdPID1.ExecSQL;
        QrAptos.Next;
      end;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
    Screen.Cursor := crDefault;
    {if AptoNao = 0 then
    begin
      Geral.MensagemBox('Todos os ' + IntToStr(QrAptos.RecordCount) +
      'apartamentos ativos deste condom�nio ' +
      'j� possuem leitura para este per�odo!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end else begin
    }
      Geral.MensagemBox('Foram encontrados ' + IntToStr(AptoNao)+ ' de ' +
      IntToStr(QrAptos.RecordCount) + ' unidades ativas que n�o geram consumo!',
      'Informa��o', MB_OK+MB_ICONINFORMATION);
      PnProduto.Enabled := False;
      PnProduto.Align   := alTop;
      PnLeitura.Visible := True;
      PnProduto.Height  := 113;
      //
      TbConsLei.Close;
      TbConsLei.Filtered := False;
      //TbConsLei.Filter := 'ID_Rand='''+FID_Rand+'''';
      //TbConsLei.Filtered := True;
      TbConsLei.Open;
      FmtTxT := '';
      for i := 1 to QrConsCasas.Value do FmtTxT := FmtTxT + '0';
      FmtTxT := '#,###,##0.' + FmtTxT +';-#,###,##0.'+FmtTxT+'; ';
      TbConsLeiMedAnt.DisplayFormat := FmtTxT;
      TbConsLeiMedAtu.DisplayFormat := FmtTxT;
      TbConsLeiGasto .DisplayFormat := FmtTxT;
      //
      TbConsLeiMEDIA_MENOR.DisplayFormat := FmtTxT;
      TbConsLeiMEDIA_MEDIA.DisplayFormat := FmtTxT;
      TbConsLeiMEDIA_MAIOR.DisplayFormat := FmtTxT;
      TbConsLeiMEDIA_VEZES.DisplayFormat := '0;-0; ';
    //end;
  end else begin
    Screen.Cursor := crHourGlass;
    try
      QrPesq.Close;
      QrPesq.Params[0].AsString := FID_Rand;
      QrPesq.Open;
      //
      if QrPesq.RecordCount > 0 then
      begin
        if Geral.MensagemBox('Existem '+IntToStr(QrPesq.RecordCount) +
        ' registros que n�o ser�o inclu�dos pois n�o possuem consumo. Deseja ' +
        'continuar assim mesmo?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)
        <> ID_YES then
        begin
          Screen.Cursor := crHourGlass;
          Exit;
        end;
      end;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO ' + DmCond.FTabCnsA + ' SET ');
      Dmod.QrUpd.SQL.Add('Apto=:P0, MedAnt=:P1, MedAtu=:P2, Consumo=:P3, ');
      Dmod.QrUpd.SQL.Add('Preco=:P4, Valor=:P5, Cond=:P6, Periodo=:P7, ');
      Dmod.QrUpd.SQL.Add('Propriet=:P8, Casas=:P9, ');
      Dmod.QrUpd.SQL.Add('UnidImp=:P10, UnidFat=:P11, UnidLei=:P12, ');
      Dmod.QrUpd.SQL.Add('GeraTyp=:P13, GeraFat=:P14, CasRat=:P15, ');
      Dmod.QrUpd.SQL.Add('NaoImpLei=:P16, CNAB_Cfg=:P17, ');
      Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
      TbConsLei.First;
      while not TbConsLei.Eof do
      begin
        if TbConsLeiGasto.Value >= 0.000001 then
        begin
          Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            DmCond.FTabCnsA, TAB_CNS, 'Controle');
          //
          Dmod.QrUpd.Params[00].AsInteger := TbConsLeiApto_ID.Value;
          Dmod.QrUpd.Params[01].AsFloat   := TbConsLeiMedAnt.Value;
          Dmod.QrUpd.Params[02].AsFloat   := TbConsLeiMedAtu.Value;
          Dmod.QrUpd.Params[03].AsFloat   := TbConsLeiGasto.Value;
          Dmod.QrUpd.Params[04].AsFloat   := QrConsPreco.Value;
          Dmod.QrUpd.Params[05].AsFloat   := TbConsLeiValor.Value;
          Dmod.QrUpd.Params[06].AsInteger := FCond;
          Dmod.QrUpd.Params[07].AsInteger := FPeriodo;
          Dmod.QrUpd.Params[08].AsInteger := TbConsLeiPropriet.Value;
          Dmod.QrUpd.Params[09].AsInteger := QrConsCasas.Value;
          Dmod.QrUpd.Params[10].AsString  := QrConsUnidImp.Value;
          Dmod.QrUpd.Params[11].AsFloat   := QrConsUnidFat.Value;
          Dmod.QrUpd.Params[12].AsString  := QrConsUnidLei.Value;
          Dmod.QrUpd.Params[13].AsInteger := 1;
          Dmod.QrUpd.Params[14].AsFloat   := TbConsLeiQtdFator.Value;
          Dmod.QrUpd.Params[15].AsInteger := QrConsCasRat.Value;
          Dmod.QrUpd.Params[16].AsInteger := QrConsNaoImpLei.Value;
          Dmod.QrUpd.Params[17].AsInteger := CNAB_Cfg;
          //Dmod.QrUpd.Params[15].AsString  := QrConsUnidImp.Value;
          //
          Dmod.QrUpd.Params[18].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
          Dmod.QrUpd.Params[19].AsInteger := VAR_USUARIO;
          Dmod.QrUpd.Params[20].AsInteger := Produto;
          Dmod.QrUpd.Params[21].AsInteger := Controle;
          //
          Dmod.QrUpd.ExecSQL;
          //
        end;
        TbConsLei.Next;
      end;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
    Screen.Cursor := crDefault;
    Close;
  end;
end;

procedure TFmCondGerLei2.TbConsLeiBeforePost(DataSet: TDataSet);
begin
  if TbConsLei.State = dsInsert then TbConsLei.Cancel;
end;

procedure TFmCondGerLei2.TbConsLeiCalcFields(DataSet: TDataSet);
var
  Valor: Double;
begin
  TbConsLeiGasto.Value := TbConsLeiMedAtu.Value - TbConsLeiMedAnt.Value;
  if TbConsLeiGasto.Value <= 0 then Valor := 0 else
  begin
    if QrConsFatorBase.Value = 1 then
      // se for por rateio calcula direto pela base
      Valor := TbConsLeiValUnita.Value * TbConsLeiQtdFator.Value
    else
      // caso contrario arredonda normalmente
      Valor := TbConsLeiGasto.Value * QrConsPreco.Value * QrConsUnidFat.Value;
  end;
  //  Arredondar s� depois
  if Valor <> 0 then
    Valor := Round(Valor / QrConsArredonda.Value) * QrConsArredonda.Value;
  TbConsLeiValor.Value := Valor;
end;

procedure TFmCondGerLei2.TbConsLeiBeforeOpen(DataSet: TDataSet);
begin
  QrMedias.Close;
  QrMedias.SQL.Clear;
  QrMedias.SQL.Add('SELECT COUNT(cdi.Conta) Vezes, MIN(cni.Consumo) MENOR,');
  QrMedias.SQL.Add('(SUM(cni.Consumo) / COUNT(cdi.Conta)) MEDIA,');
  QrMedias.SQL.Add('MAX(cni.Consumo) MAIOR, cdi.Conta Apto');
  QrMedias.SQL.Add('FROM cons cns');
  QrMedias.SQL.Add('LEFT JOIN ' + DmCond.FTabCnsA + '  cni ON cni.Codigo=cns.Codigo');
  QrMedias.SQL.Add('LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo');
  QrMedias.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=cni.Apto');
  QrMedias.SQL.Add('WHERE cni.Cond=:P0');
  QrMedias.SQL.Add('AND cnp.Cond=:P1');
  QrMedias.SQL.Add('AND cni.Codigo=:P2');
  QrMedias.SQL.Add('AND cni.Periodo > :P3 -13');
  QrMedias.SQL.Add('AND cni.Periodo < :P4');
  QrMedias.SQL.Add('GROUP BY cdi.Conta');
  QrMedias.Params[0].AsInteger := FCond;
  QrMedias.Params[1].AsInteger := FCond;
  QrMedias.Params[2].AsInteger := QrConsCodigo.Value;
  QrMedias.Params[3].AsInteger := FPeriodo;
  QrMedias.Params[4].AsInteger := FPeriodo;
  QrMedias.Open;
end;

procedure TFmCondGerLei2.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
  Extra = 1.3;
var
  OldAlign: Integer;
  Cor: TColor;
  Baixo, Medio, Alto, Gasto, Vi, Vf, Vt, Vm, R, G, B: Double;
  //Txt: String;
begin
  if (Column.FieldName = 'GASTO') then
  begin
    //Txt := '';
    Baixo := TbConsLeiMEDIA_MENOR.Value;
    Medio := TbConsLeiMEDIA_MEDIA.Value;
    Alto  := TbConsLeiMEDIA_MAIOR.Value;
    Gasto := TbConsLeiGASTO.Value;
    //
    if Gasto < 0 then
    begin
      R := 128;
      G := 128;
      B := 128;
    end else begin
      R := 0;
      G := 0;
      B := 0;
      if Medio > 0 then
      begin
        if Gasto > Medio then G := 0 else
        begin
          Vi := Baixo / Extra;
          if Gasto < Vi then Gasto := Vi;
          Vf := Medio;
          Vt := Vf - Vi;
          Vm := Vf - Gasto;
          G := (Vm / Vt) * 255;
        end;
        if Gasto < Medio then R := 0 else
        begin
          Vf := Alto * Extra;
          if Alto > Vf then Alto := Vf;
          Vi := Medio;
          Vt := Vf - Vi;
          Vm := Vf - Gasto;
          R := 255 - ((Vm / Vt) * 255);
        end;
        Vi := Baixo;
        Vf := Alto;
        if (Gasto < Vi) or (Gasto > Vf) then B := 0 else
        begin
          if Gasto < Medio then
          begin
            Gasto := Gasto - Vi;
            B := (Gasto / Medio) * 255;
          end else begin
            Gasto := Gasto - Medio;
            B := 255 - ((Gasto / Vf) * 255);
          end;
        end;

      end;
    end;
    {Txt := Txt + 'R'+FormatFloat('0',R);
    Txt := Txt + 'G'+FormatFloat('0',G);
    Txt := Txt + 'B'+FormatFloat('0',B);}
    with DBGrid1.Canvas do
    begin
      if R > 255 then R := 255;
      if G > 255 then G := 255;
      if B > 255 then B := 255;
      Cor := RGB(Trunc(R), Trunc(G), Trunc(B));
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;

      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      FillRect(Rect);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText {+ Txt});
      SetTextAlign(Handle, OldAlign);

    end;
  end;
end;

procedure TFmCondGerLei2.EdValorBaseChange(Sender: TObject);
begin
  CalculaValorUnitario;
end;

procedure TFmCondGerLei2.CalculaValorUnitario;
var
  Valor, Fator, ConsTot, ConsUni: Double;
begin
  Fator := EdValorBase.ValueVariant;
  if QrConsPreco.Value <= 0 then
    ConsTot := 0
  else
    ConsTot := Fator / QrConsPreco.Value;
  //
  if QrFracaoTOTAL.Value > 0 then
  begin
    Valor   := Round(Fator * 100 / QrFracaoTOTAL.Value) / 100;
    ConsUni := ConsTot / QrFracaoTOTAL.Value;
  end else begin
    Valor   := 0;
    ConsUni := 0;
  end;
  //
  EdGastoTotal.DecimalSize  := QrConsCasas.Value;
  EdGastoTotal.ValueVariant := ConsTot;
  LaGastoTotal.Caption      := 'Total ' + QrConsUnidLei.Value + ':';
  //
  EdGastoUnita.DecimalSize  := QrConsCasas.Value;
  EdGastoUnita.ValueVariant := ConsUni;
  LaGastoUnita.Caption      := QrConsUnidLei.Value + '/' + QrConsUnidImp.Value + ':';
  //
  Valor := Round(Valor / QrConsArredonda.Value) * QrConsArredonda.Value;
  EdUnitario.ValueVariant   := Valor;
  //
end;
{
SQL do QrAnt:
SELECT csi.Apto, csi.MedAtu
FROM ' + DmCond.FTabCnsA + ' csi
LEFT JOIN condimov cdi ON csi.Apto=cdi.Conta
WHERE cdi.Codigo=:P0
AND csi.Codigo=:P1
AND csi.Periodo = (
  SELECT MAX(csi.Periodo)
  FROM ' + DmCond.FTabCnsA + ' csi
  LEFT JOIN condimov cdi ON csi.Apto=cdi.Conta
  WHERE cdi.Codigo=:P2
  AND csi.Codigo=:P3
  AND csi.Periodo<:P4)

  QrJaTem:
SELECT Apto FROM ' + DmCond.FTabCnsA + '
WHERE Codigo=:P0
AND Cond=:P1
AND Periodo=:P2

QrUni:
SELECT csi.Apto, csi.MedAtu
FROM ' + DmCond.FTabCnsA + ' csi
LEFT JOIN condimov cdi ON csi.Apto=cdi.Conta
WHERE cdi.Codigo=:P0
AND csi.Codigo=:P1
AND csi.Apto=:P2
AND csi.Periodo = (
  SELECT MAX(csi.Periodo)
  FROM ' + DmCond.FTabCnsA + ' csi
  LEFT JOIN condimov cdi ON csi.Apto=cdi.Conta
  WHERE cdi.Codigo=:P3
  AND csi.Codigo=:P4
  AND csi.Periodo<:P5
  AND csi.Apto=:P6
)

}
end.

