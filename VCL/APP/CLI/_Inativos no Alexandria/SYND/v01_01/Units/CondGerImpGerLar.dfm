object FmCondGerImpGerLar: TFmCondGerImpGerLar
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-020 :: Largura Coluna'
  ClientHeight = 316
  ClientWidth = 311
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 93
    Width = 311
    Height = 109
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 24
    ExplicitTop = 149
    ExplicitHeight = 116
    object StaticText1: TStaticText
      Left = 8
      Top = 8
      Width = 69
      Height = 17
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'Coluna atual:'
      TabOrder = 0
    end
    object StaticText2: TStaticText
      Left = 8
      Top = 68
      Width = 200
      Height = 17
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'Diferen'#231'a para formato "retrato":'
      TabOrder = 1
    end
    object StaticText3: TStaticText
      Left = 8
      Top = 48
      Width = 200
      Height = 17
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'Nova largura total:'
      TabOrder = 2
    end
    object StaticText4: TStaticText
      Left = 8
      Top = 88
      Width = 200
      Height = 17
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'Diferen'#231'a para formato "paisagem":'
      TabOrder = 3
    end
    object STTota: TStaticText
      Left = 212
      Top = 48
      Width = 89
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSunken
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object STPais: TStaticText
      Left = 212
      Top = 88
      Width = 89
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSunken
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object STRetr: TStaticText
      Left = 212
      Top = 68
      Width = 89
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSunken
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
    object STNome: TStaticText
      Left = 112
      Top = 8
      Width = 189
      Height = 17
      AutoSize = False
      BorderStyle = sbsSunken
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
    object StaticText5: TStaticText
      Left = 8
      Top = 28
      Width = 200
      Height = 17
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'Largura atual:'
      TabOrder = 8
    end
    object STLarg: TStaticText
      Left = 212
      Top = 28
      Width = 89
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSunken
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 9
    end
    object STCodi: TStaticText
      Left = 80
      Top = 8
      Width = 29
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = '0'
      TabOrder = 10
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 311
    Height = 45
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 64
      Height = 13
      Caption = 'Nova largura:'
    end
    object EdLargura: TdmkEdit
      Left = 8
      Top = 24
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdLarguraChange
    end
    object Bt0680: TBitBtn
      Left = 92
      Top = 24
      Width = 35
      Height = 21
      Caption = '680'
      TabOrder = 1
      OnClick = Bt0680Click
    end
    object Bt1009: TBitBtn
      Left = 128
      Top = 24
      Width = 35
      Height = 21
      Caption = '1009'
      TabOrder = 2
      OnClick = Bt1009Click
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 311
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = -324
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 263
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 215
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 187
        Height = 32
        Caption = 'Largura Coluna'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 187
        Height = 32
        Caption = 'Largura Coluna'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 187
        Height = 32
        Caption = 'Largura Coluna'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 202
    Width = 311
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitLeft = -324
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 307
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 246
    Width = 311
    Height = 70
    Align = alBottom
    TabOrder = 4
    ExplicitLeft = -324
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 307
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 163
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 1
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
end
