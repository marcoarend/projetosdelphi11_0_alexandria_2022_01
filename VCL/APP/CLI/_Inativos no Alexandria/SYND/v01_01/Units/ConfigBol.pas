unit ConfigBol;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkEdit,
  ComCtrls, Grids, DBGrids, dmkDBGrid, Menus, dmkCheckBox, dmkGeral, dmkMemo,
  Clipbrd, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmConfigBol = class(TForm)
    PainelDados: TPanel;
    DsConfigBol: TDataSource;
    QrConfigBol: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel9: TPanel;
    Label29: TLabel;
    EdTitBTxtTit: TdmkEdit;
    EdTitBTxtSiz: TdmkEdit;
    Label30: TLabel;
    RGTitBTxtFmt: TRadioGroup;
    Label31: TLabel;
    EdTitBMrgSup: TdmkEdit;
    Label32: TLabel;
    EdTitBLinAlt: TdmkEdit;
    RGTitBPerFmt: TRadioGroup;
    GroupBox1: TGroupBox;
    Label12: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    EdCxaSTxtSiz: TdmkEdit;
    EdCxaSValSiz: TdmkEdit;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    EdCxaSIniTam: TdmkEdit;
    EdCxaSCreTam: TdmkEdit;
    EdCxaSDebTam: TdmkEdit;
    EdCxaSMovTam: TdmkEdit;
    EdCxaSFimTam: TdmkEdit;
    EdCxaSIniTxt: TdmkEdit;
    EdCxaSCreTxt: TdmkEdit;
    EdCxaSDebTxt: TdmkEdit;
    EdCxaSMovTxt: TdmkEdit;
    EdCxaSFimTxt: TdmkEdit;
    GroupBox4: TGroupBox;
    RGCxaSValFmt: TRadioGroup;
    RGCxaSTxtFmt: TRadioGroup;
    EdCxaSMrgSup: TdmkEdit;
    EdCxaSTxtAlt: TdmkEdit;
    EdCxaSValAlt: TdmkEdit;
    EdCxaSMrgInf: TdmkEdit;
    Label33: TLabel;
    TabSheet9: TTabSheet;
    Panel16: TPanel;
    GroupBox19: TGroupBox;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    EdTitITxtTit: TdmkEdit;
    RGTitITxtFmt: TRadioGroup;
    EdTitITxtSiz: TdmkEdit;
    EdTitIMrgSup: TdmkEdit;
    EdTitILinAlt: TdmkEdit;
    GroupBox20: TGroupBox;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    EdSdoITxTTxt: TdmkEdit;
    RGSdoITxTFmt: TRadioGroup;
    EdSdoITxTSiz: TdmkEdit;
    EdSdoILaTVal: TdmkEdit;
    EdSdoILiTAlt: TdmkEdit;
    EdSdoIVaTSiz: TdmkEdit;
    Label68: TLabel;
    RGSdoIVaTFmt: TRadioGroup;
    GroupBox18: TGroupBox;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label69: TLabel;
    EdSdoITxMTxt: TdmkEdit;
    RGSdoITxMFmt: TRadioGroup;
    EdSdoITxMSiz: TdmkEdit;
    EdSdoILaMVal: TdmkEdit;
    EdSdoILiMAlt: TdmkEdit;
    EdSdoIVaMSiz: TdmkEdit;
    RGSdoIVaMFmt: TRadioGroup;
    TabSheet10: TTabSheet;
    PageControl3: TPageControl;
    TabSheet16: TTabSheet;
    TabSheet17: TTabSheet;
    Panel14: TPanel;
    Label45: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    EdTitRTxtTit: TdmkEdit;
    EdTitRTxtSiz: TdmkEdit;
    RGTitRTxtFmt: TRadioGroup;
    EdTitRMrgSup: TdmkEdit;
    EdTitRLinAlt: TdmkEdit;
    RGTitRPerFmt: TRadioGroup;
    Panel15: TPanel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    EdTitCTxtTit: TdmkEdit;
    EdTitCTxtSiz: TdmkEdit;
    RGTitCTxtFmt: TRadioGroup;
    EdTitCMrgSup: TdmkEdit;
    EdTitCLinAlt: TdmkEdit;
    RGTitCPerFmt: TRadioGroup;
    GroupBox17: TGroupBox;
    Label54: TLabel;
    Label55: TLabel;
    RGSdoCTxtFmt: TRadioGroup;
    EdSdoCLinAlt: TdmkEdit;
    EdSdoCTxtSiz: TdmkEdit;
    Panel17: TPanel;
    PageControl4: TPageControl;
    TabSheet5: TTabSheet;
    PageControl5: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    Panel18: TPanel;
    GroupBox22: TGroupBox;
    RGCtaPValFmt: TRadioGroup;
    RGCtaPTxtFmt: TRadioGroup;
    Panel19: TPanel;
    Label74: TLabel;
    Label75: TLabel;
    GroupBox23: TGroupBox;
    Label76: TLabel;
    Label77: TLabel;
    EdCtaPTxtSiz: TdmkEdit;
    EdCtaPValSiz: TdmkEdit;
    EdCtaPLarVal: TdmkEdit;
    EdCtaPLinAlt: TdmkEdit;
    Panel20: TPanel;
    GroupBox24: TGroupBox;
    Label78: TLabel;
    Label79: TLabel;
    EdSgrPTitAlt: TdmkEdit;
    RGSgrPTitFmt: TRadioGroup;
    EdSgrPTitSiz: TdmkEdit;
    GroupBox25: TGroupBox;
    GroupBox26: TGroupBox;
    RGSgrPSumFmt: TRadioGroup;
    Panel21: TPanel;
    Label82: TLabel;
    EdSgrPSumAlt: TdmkEdit;
    GroupBox27: TGroupBox;
    Label83: TLabel;
    EdSgrPSumSiz: TdmkEdit;
    Label81: TLabel;
    EdSgrPLarVal: TdmkEdit;
    TabSheet8: TTabSheet;
    Panel22: TPanel;
    GroupBox21: TGroupBox;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    EdPrvPTitAlt: TdmkEdit;
    RGPrvPTitFmt: TRadioGroup;
    EdPrvPTitSiz: TdmkEdit;
    EdPrvPMrgSup: TdmkEdit;
    GroupBox28: TGroupBox;
    GroupBox29: TGroupBox;
    RGSumPValFmt: TRadioGroup;
    RGSumPTxtFmt: TRadioGroup;
    Panel23: TPanel;
    Label73: TLabel;
    Label84: TLabel;
    EdSumPLinAlt: TdmkEdit;
    EdSumPValLar: TdmkEdit;
    GroupBox30: TGroupBox;
    Label85: TLabel;
    Label86: TLabel;
    EdSumPTxtSiz: TdmkEdit;
    EdSumPValSiz: TdmkEdit;
    Label87: TLabel;
    EdPrvPTitStr: TdmkEdit;
    TabSheet15: TTabSheet;
    TabSheet18: TTabSheet;
    Panel24: TPanel;
    GroupBox31: TGroupBox;
    RGCtaAValFmt: TRadioGroup;
    RGCtaATxtFmt: TRadioGroup;
    Panel25: TPanel;
    Label80: TLabel;
    Label88: TLabel;
    GroupBox32: TGroupBox;
    Label89: TLabel;
    Label90: TLabel;
    EdCtaATxtSiz: TdmkEdit;
    EdCtaAValSiz: TdmkEdit;
    EdCtaALarVal: TdmkEdit;
    EdCtaALinAlt: TdmkEdit;
    Label91: TLabel;
    EdSumPTxtStr: TdmkEdit;
    Panel26: TPanel;
    GroupBox33: TGroupBox;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    Label95: TLabel;
    EdTitATitAlt: TdmkEdit;
    RGTitATitFmt: TRadioGroup;
    EdTitATitSiz: TdmkEdit;
    EdTitAMrgSup: TdmkEdit;
    EdTitATitStr: TdmkEdit;
    GroupBox34: TGroupBox;
    GroupBox35: TGroupBox;
    Panel27: TPanel;
    Label97: TLabel;
    EdTitASumAlt: TdmkEdit;
    GroupBox36: TGroupBox;
    Label99: TLabel;
    EdTitASumSiz: TdmkEdit;
    GroupBox37: TGroupBox;
    Label100: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    EdTitAAptAlt: TdmkEdit;
    RGTitAAptFmt: TRadioGroup;
    EdTitAAptSiz: TdmkEdit;
    EdTitAAptSup: TdmkEdit;
    RGTitASumFmt: TRadioGroup;
    QrConfigBolCodigo: TIntegerField;
    QrConfigBolNome: TWideStringField;
    QrConfigBolTitBTxtSiz: TSmallintField;
    QrConfigBolTitBTxtFmt: TSmallintField;
    QrConfigBolTitBMrgSup: TIntegerField;
    QrConfigBolTitBLinAlt: TIntegerField;
    QrConfigBolTitBTxtTit: TWideStringField;
    QrConfigBolTitBPerFmt: TSmallintField;
    QrConfigBolNiv1TxtSiz: TSmallintField;
    QrConfigBolNiv1ValSiz: TSmallintField;
    QrConfigBolNiv1TxtFmt: TSmallintField;
    QrConfigBolNiv1ValFmt: TSmallintField;
    QrConfigBolNiv1ValLar: TIntegerField;
    QrConfigBolNiv1LinAlt: TIntegerField;
    QrConfigBolCxaSTxtSiz: TSmallintField;
    QrConfigBolCxaSValSiz: TSmallintField;
    QrConfigBolCxaSTxtFmt: TSmallintField;
    QrConfigBolCxaSValFmt: TSmallintField;
    QrConfigBolCxaSTxtAlt: TIntegerField;
    QrConfigBolCxaSValAlt: TIntegerField;
    QrConfigBolCxaSMrgSup: TIntegerField;
    QrConfigBolCxaSIniTam: TFloatField;
    QrConfigBolCxaSCreTam: TFloatField;
    QrConfigBolCxaSDebTam: TFloatField;
    QrConfigBolCxaSMovTam: TFloatField;
    QrConfigBolCxaSFimTam: TFloatField;
    QrConfigBolCxaSIniTxt: TWideStringField;
    QrConfigBolCxaSCreTxt: TWideStringField;
    QrConfigBolCxaSDebTxt: TWideStringField;
    QrConfigBolCxaSMovTxt: TWideStringField;
    QrConfigBolCxaSFimTxt: TWideStringField;
    QrConfigBolCxaSMrgInf: TIntegerField;
    QrConfigBolTit2TxtSiz: TSmallintField;
    QrConfigBolTit2TxtFmt: TSmallintField;
    QrConfigBolTit2MrgSup: TIntegerField;
    QrConfigBolTit2LinAlt: TIntegerField;
    QrConfigBolSom2TxtSiz: TSmallintField;
    QrConfigBolSom2ValSiz: TSmallintField;
    QrConfigBolSom2TxtFmt: TSmallintField;
    QrConfigBolSom2ValFmt: TSmallintField;
    QrConfigBolSom2ValLar: TIntegerField;
    QrConfigBolSom2LinAlt: TIntegerField;
    QrConfigBolTit3TxtSiz: TSmallintField;
    QrConfigBolTit3TxtFmt: TSmallintField;
    QrConfigBolTit3MrgSup: TIntegerField;
    QrConfigBolTit3LinAlt: TIntegerField;
    QrConfigBolSom3TxtSiz: TSmallintField;
    QrConfigBolSom3ValSiz: TSmallintField;
    QrConfigBolSom3TxtFmt: TSmallintField;
    QrConfigBolSom3ValFmt: TSmallintField;
    QrConfigBolSom3ValLar: TIntegerField;
    QrConfigBolSom3LinAlt: TIntegerField;
    QrConfigBolTit4TxtSiz: TSmallintField;
    QrConfigBolTit4TxtFmt: TSmallintField;
    QrConfigBolTit4MrgSup: TIntegerField;
    QrConfigBolTit4LinAlt: TIntegerField;
    QrConfigBolSom4LinAlt: TIntegerField;
    QrConfigBolTitRTxtSiz: TSmallintField;
    QrConfigBolTitRTxtFmt: TSmallintField;
    QrConfigBolTitRMrgSup: TIntegerField;
    QrConfigBolTitRLinAlt: TIntegerField;
    QrConfigBolTitRTxtTit: TWideStringField;
    QrConfigBolTitRPerFmt: TSmallintField;
    QrConfigBolTitCTxtSiz: TSmallintField;
    QrConfigBolTitCTxtFmt: TSmallintField;
    QrConfigBolTitCMrgSup: TIntegerField;
    QrConfigBolTitCLinAlt: TIntegerField;
    QrConfigBolTitCTxtTit: TWideStringField;
    QrConfigBolTitCPerFmt: TSmallintField;
    QrConfigBolSdoCTxtSiz: TSmallintField;
    QrConfigBolSdoCTxtFmt: TSmallintField;
    QrConfigBolSdoCLinAlt: TIntegerField;
    QrConfigBolTitITxtSiz: TSmallintField;
    QrConfigBolTitITxtFmt: TSmallintField;
    QrConfigBolTitIMrgSup: TIntegerField;
    QrConfigBolTitILinAlt: TIntegerField;
    QrConfigBolTitITxtTit: TWideStringField;
    QrConfigBolSdoITxMSiz: TSmallintField;
    QrConfigBolSdoITxMFmt: TSmallintField;
    QrConfigBolSdoIVaMSiz: TSmallintField;
    QrConfigBolSdoIVaMFmt: TSmallintField;
    QrConfigBolSdoILiMAlt: TIntegerField;
    QrConfigBolSdoITxMTxt: TWideStringField;
    QrConfigBolSdoILaMVal: TIntegerField;
    QrConfigBolSdoITxTSiz: TSmallintField;
    QrConfigBolSdoITxTFmt: TSmallintField;
    QrConfigBolSdoIVaTSiz: TSmallintField;
    QrConfigBolSdoIVaTFmt: TSmallintField;
    QrConfigBolSdoILiTAlt: TIntegerField;
    QrConfigBolSdoITxTTxt: TWideStringField;
    QrConfigBolSdoILaTVal: TIntegerField;
    QrConfigBolCtaPTxtSiz: TSmallintField;
    QrConfigBolCtaPTxtFmt: TSmallintField;
    QrConfigBolCtaPValSiz: TSmallintField;
    QrConfigBolCtaPValFmt: TSmallintField;
    QrConfigBolCtaPLinAlt: TIntegerField;
    QrConfigBolCtaPLarVal: TIntegerField;
    QrConfigBolSgrPTitSiz: TSmallintField;
    QrConfigBolSgrPTitFmt: TSmallintField;
    QrConfigBolSgrPTitAlt: TIntegerField;
    QrConfigBolSgrPSumSiz: TSmallintField;
    QrConfigBolSgrPSumFmt: TSmallintField;
    QrConfigBolSgrPSumAlt: TIntegerField;
    QrConfigBolSgrPLarVal: TIntegerField;
    QrConfigBolPrvPTitSiz: TSmallintField;
    QrConfigBolPrvPTitFmt: TSmallintField;
    QrConfigBolPrvPTitAlt: TIntegerField;
    QrConfigBolPrvPTitStr: TWideStringField;
    QrConfigBolPrvPMrgSup: TIntegerField;
    QrConfigBolSumPTxtSiz: TSmallintField;
    QrConfigBolSumPTxtFmt: TSmallintField;
    QrConfigBolSumPValSiz: TSmallintField;
    QrConfigBolSumPValFmt: TSmallintField;
    QrConfigBolSumPLinAlt: TIntegerField;
    QrConfigBolSumPValLar: TIntegerField;
    QrConfigBolSumPTxtStr: TWideStringField;
    QrConfigBolCtaATxtSiz: TSmallintField;
    QrConfigBolCtaATxtFmt: TSmallintField;
    QrConfigBolCtaAValSiz: TSmallintField;
    QrConfigBolCtaAValFmt: TSmallintField;
    QrConfigBolCtaALinAlt: TIntegerField;
    QrConfigBolCtaALarVal: TIntegerField;
    QrConfigBolTitATitSiz: TSmallintField;
    QrConfigBolTitATitFmt: TSmallintField;
    QrConfigBolTitATitAlt: TIntegerField;
    QrConfigBolTitATitStr: TWideStringField;
    QrConfigBolTitAMrgSup: TIntegerField;
    QrConfigBolTitAAptSiz: TSmallintField;
    QrConfigBolTitAAptFmt: TSmallintField;
    QrConfigBolTitAAptAlt: TIntegerField;
    QrConfigBolTitAAptSup: TIntegerField;
    QrConfigBolTitASumSiz: TSmallintField;
    QrConfigBolTitASumFmt: TSmallintField;
    QrConfigBolTitASumAlt: TIntegerField;
    QrConfigBolLk: TIntegerField;
    QrConfigBolDataCad: TDateField;
    QrConfigBolDataAlt: TDateField;
    QrConfigBolUserCad: TIntegerField;
    QrConfigBolUserAlt: TIntegerField;
    QrConfigBolAlterWeb: TSmallintField;
    QrConfigBolAtivo: TSmallintField;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    PMInclui: TPopupMenu;
    Incluinovaconfiguraocombasenoatual1: TMenuItem;
    GroupBox38: TGroupBox;
    Label96: TLabel;
    OpenPictureDialog2: TOpenPictureDialog;
    EdMeuLogoAlt: TdmkEdit;
    Label103: TLabel;
    Label98: TLabel;
    EdMeuLogoLar: TdmkEdit;
    CkMeuLogoImp: TdmkCheckBox;
    EdMeuLogoArq: TdmkEdit;
    QrConfigBolMeuLogoImp: TSmallintField;
    QrConfigBolMeuLogoAlt: TIntegerField;
    QrConfigBolMeuLogoLar: TIntegerField;
    QrConfigBolMeuLogoArq: TWideStringField;
    QrConfigBolColunas: TSmallintField;
    QrConfigBolNiv1Grades: TSmallintField;
    EdCxaTrnsTxt: TdmkEdit;
    Label105: TLabel;
    PageControl2: TPageControl;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    PageControl6: TPageControl;
    TabSheet13: TTabSheet;
    Panel4: TPanel;
    GroupBox6: TGroupBox;
    RGNiv1ValFmt: TRadioGroup;
    RGNiv1TxtFmt: TRadioGroup;
    Panel6: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    GroupBox5: TGroupBox;
    Label13: TLabel;
    Label17: TLabel;
    EdNiv1TxtSiz: TdmkEdit;
    EdNiv1ValSiz: TdmkEdit;
    EdNiv1ValLar: TdmkEdit;
    EdNiv1LinAlt: TdmkEdit;
    CkNiv1Grades: TCheckBox;
    TabSheet14: TTabSheet;
    Panel7: TPanel;
    GroupBox9: TGroupBox;
    Label27: TLabel;
    Label28: TLabel;
    Label34: TLabel;
    EdTit2LinAlt: TdmkEdit;
    RGTit2TxtFmt: TRadioGroup;
    EdTit2TxtSiz: TdmkEdit;
    EdTit2MrgSup: TdmkEdit;
    GroupBox10: TGroupBox;
    GroupBox7: TGroupBox;
    RGSom2ValFmt: TRadioGroup;
    RGSom2TxtFmt: TRadioGroup;
    Panel8: TPanel;
    Label23: TLabel;
    Label24: TLabel;
    EdSom2LinAlt: TdmkEdit;
    EdSom2ValLar: TdmkEdit;
    GroupBox8: TGroupBox;
    Label25: TLabel;
    Label26: TLabel;
    EdSom2TxtSiz: TdmkEdit;
    EdSom2ValSiz: TdmkEdit;
    TabSheet19: TTabSheet;
    Panel10: TPanel;
    GroupBox11: TGroupBox;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    EdTit3LinAlt: TdmkEdit;
    RGTit3TxtFmt: TRadioGroup;
    EdTit3TxtSiz: TdmkEdit;
    EdTit3MrgSup: TdmkEdit;
    GroupBox12: TGroupBox;
    GroupBox13: TGroupBox;
    RGSom3ValFmt: TRadioGroup;
    RGSom3TxtFmt: TRadioGroup;
    Panel11: TPanel;
    Label38: TLabel;
    Label39: TLabel;
    EdSom3LinAlt: TdmkEdit;
    EdSom3ValLar: TdmkEdit;
    GroupBox14: TGroupBox;
    Label40: TLabel;
    Label41: TLabel;
    EdSom3TxtSiz: TdmkEdit;
    EdSom3ValSiz: TdmkEdit;
    TabSheet20: TTabSheet;
    Panel12: TPanel;
    GroupBox15: TGroupBox;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    EdTit4LinAlt: TdmkEdit;
    RGTit4TxtFmt: TRadioGroup;
    EdTit4TxtSiz: TdmkEdit;
    EdTit4MrgSup: TdmkEdit;
    GroupBox16: TGroupBox;
    Panel13: TPanel;
    Label46: TLabel;
    EdSom4LinAlt: TdmkEdit;
    Panel28: TPanel;
    GroupBox39: TGroupBox;
    Label106: TLabel;
    Label107: TLabel;
    Label108: TLabel;
    EdTit8LinAlt: TdmkEdit;
    RGTit8TxtFmt: TRadioGroup;
    EdTit8TxtSiz: TdmkEdit;
    EdTit8MrgSup: TdmkEdit;
    GroupBox40: TGroupBox;
    GroupBox41: TGroupBox;
    RGSom8ValFmt: TRadioGroup;
    RGSom8TxtFmt: TRadioGroup;
    Panel29: TPanel;
    Label109: TLabel;
    Label110: TLabel;
    EdSom8LinAlt: TdmkEdit;
    EdSom8ValLar: TdmkEdit;
    GroupBox42: TGroupBox;
    Label111: TLabel;
    Label112: TLabel;
    EdSom8TxtSiz: TdmkEdit;
    EdSom8ValSiz: TdmkEdit;
    GroupBox43: TGroupBox;
    Panel30: TPanel;
    Label113: TLabel;
    Label114: TLabel;
    GroupBox44: TGroupBox;
    Label115: TLabel;
    Label116: TLabel;
    EdNiv8TxtSiz: TdmkEdit;
    EdNiv8ValSiz: TdmkEdit;
    EdNiv8ValLar: TdmkEdit;
    EdNiv8LinAlt: TdmkEdit;
    CkNiv8Grades: TCheckBox;
    GroupBox45: TGroupBox;
    RGNiv8ValFmt: TRadioGroup;
    RGNiv8TxtFmt: TRadioGroup;
    QrConfigBolCxaTrnsTxt: TWideStringField;
    QrConfigBolTit8TxtSiz: TSmallintField;
    QrConfigBolTit8TxtFmt: TSmallintField;
    QrConfigBolTit8MrgSup: TIntegerField;
    QrConfigBolTit8LinAlt: TIntegerField;
    QrConfigBolNiv8TxtSiz: TSmallintField;
    QrConfigBolNiv8ValSiz: TSmallintField;
    QrConfigBolNiv8Grades: TSmallintField;
    QrConfigBolNiv8TxtFmt: TSmallintField;
    QrConfigBolNiv8ValFmt: TSmallintField;
    QrConfigBolNiv8ValLar: TIntegerField;
    QrConfigBolNiv8LinAlt: TIntegerField;
    QrConfigBolSom8TxtSiz: TSmallintField;
    QrConfigBolSom8ValSiz: TSmallintField;
    QrConfigBolSom8TxtFmt: TSmallintField;
    QrConfigBolSom8ValFmt: TSmallintField;
    QrConfigBolSom8ValLar: TIntegerField;
    QrConfigBolSom8LinAlt: TIntegerField;
    GroupBox46: TGroupBox;
    Label14: TLabel;
    MeTitAAptTex: TdmkMemo;
    LBTitAAptTex: TListBox;
    QrConfigBolTitAAptTex: TWideMemoField;
    QrConfigBolTitWebTxt: TWideStringField;
    TabSheet21: TTabSheet;
    EdTitWebTxt: TdmkEdit;
    Label15: TLabel;
    Panel31: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    EdNome: TdmkEdit;
    RGColunas: TRadioGroup;
    RGMesCompet: TRadioGroup;
    QrConfigBolMesCompet: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel32: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel33: TPanel;
    Label20: TLabel;
    Label104: TLabel;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BitBtn1: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrConfigBolAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrConfigBolBeforeOpen(DataSet: TDataSet);
    procedure Incluinovaconfiguraocombasenoatual1Click(Sender: TObject);
    procedure QrConfigBolAfterScroll(DataSet: TDataSet);
    procedure EdMeuLogoArqKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure LBTitAAptTexDblClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro(Codigo: Integer);
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmConfigBol: TFmConfigBol;
const
  FFormatFloat = '00000';

implementation

uses Module, UnFinanceiro, ModuleCond, ModuleBloq, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmConfigBol.LBTitAAptTexDblClick(Sender: TObject);
var
  Texto: String;
  Ate: Integer;
begin
  Texto := LBTitAAptTex.Items[LBTitAAptTex.ItemIndex];
  if Texto <> '' then
  begin
    Ate := Pos(']', Texto);
    Texto := Copy(Texto, 1, Ate);
    Clipboard.AsText := Texto;
    MeTitAAptTex.PasteFromClipboard;
  end;
  MeTitAAptTex.SetFocus;
end;

procedure TFmConfigBol.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmConfigBol.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrConfigBolCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmConfigBol.DefParams;
begin
  VAR_GOTOTABELA := 'ConfigBol';
  VAR_GOTOMYSQLTABLE := QrConfigBol;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM configbol');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmConfigBol.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
      end;
      // O restante copia
      CkNiv1Grades.Checked      := MLAGeral.IntToBool(QrConfigBolNiv1Grades.Value);
      RGColunas.ItemIndex       := QrConfigBolColunas.Value;
      EdTitBTxtSiz.ValueVariant := QrConfigBolTitBTxtSiz.Value;
      RGTitBTxtFmt.ItemIndex    := QrConfigBolTitBTxtFmt.Value;
      EdTitBMrgSup.ValueVariant := QrConfigBolTitBMrgSup.Value;
      EdTitBLinAlt.ValueVariant := QrConfigBolTitBLinAlt.Value;
      EdTitBTxtTit.ValueVariant := QrConfigBolTitBTxtTit.Value;
      RGTitBPerFmt.ItemIndex    := QrConfigBolTitBPerFmt.Value;
      EdNiv1TxtSiz.ValueVariant := QrConfigBolNiv1TxtSiz.Value;
      EdNiv1ValSiz.ValueVariant := QrConfigBolNiv1ValSiz.Value;
      RGNiv1TxtFmt.ItemIndex    := QrConfigBolNiv1TxtFmt.Value;
      RGNiv1ValFmt.ItemIndex    := QrConfigBolNiv1ValFmt.Value;
      EdNiv1ValLar.ValueVariant := QrConfigBolNiv1ValLar.Value;
      EdNiv1LinAlt.ValueVariant := QrConfigBolNiv1LinAlt.Value;
      EdCxaSTxtSiz.ValueVariant := QrConfigBolCxaSTxtSiz.Value;
      EdCxaSValSiz.ValueVariant := QrConfigBolCxaSValSiz.Value;
      RGCxaSTxtFmt.ItemIndex    := QrConfigBolCxaSTxtFmt.Value;
      RGCxaSValFmt.ItemIndex    := QrConfigBolCxaSValFmt.Value;
      EdCxaSTxtAlt.ValueVariant := QrConfigBolCxaSTxtAlt.Value;
      EdCxaSValAlt.ValueVariant := QrConfigBolCxaSValAlt.Value;
      EdCxaSMrgSup.ValueVariant := QrConfigBolCxaSMrgSup.Value;
      EdCxaSIniTam.ValueVariant := QrConfigBolCxaSIniTam.Value;
      EdCxaSCreTam.ValueVariant := QrConfigBolCxaSCreTam.Value;
      EdCxaSDebTam.ValueVariant := QrConfigBolCxaSDebTam.Value;
      EdCxaSMovTam.ValueVariant := QrConfigBolCxaSMovTam.Value;
      EdCxaSFimTam.ValueVariant := QrConfigBolCxaSFimTam.Value;
      EdCxaSIniTxt.ValueVariant := QrConfigBolCxaSIniTxt.Value;
      EdCxaSCreTxt.ValueVariant := QrConfigBolCxaSCreTxt.Value;
      EdCxaSDebTxt.ValueVariant := QrConfigBolCxaSDebTxt.Value;
      EdCxaSMovTxt.ValueVariant := QrConfigBolCxaSMovTxt.Value;
      EdCxaSFimTxt.ValueVariant := QrConfigBolCxaSFimTxt.Value;
      EdCxaSMrgInf.ValueVariant := QrConfigBolCxaSMrgInf.Value;
      EdTit2TxtSiz.ValueVariant := QrConfigBolTit2TxtSiz.Value;
      RGTit2TxtFmt.ItemIndex    := QrConfigBolTit2TxtFmt.Value;
      EdTit2MrgSup.ValueVariant := QrConfigBolTit2MrgSup.Value;
      EdTit2LinAlt.ValueVariant := QrConfigBolTit2LinAlt.Value;
      EdSom2TxtSiz.ValueVariant := QrConfigBolSom2TxtSiz.Value;
      EdSom2ValSiz.ValueVariant := QrConfigBolSom2ValSiz.Value;
      RGSom2TxtFmt.ItemIndex    := QrConfigBolSom2TxtFmt.Value;
      RGSom2ValFmt.ItemIndex    := QrConfigBolSom2ValFmt.Value;
      EdSom2ValLar.ValueVariant := QrConfigBolSom2ValLar.Value;
      EdSom2LinAlt.ValueVariant := QrConfigBolSom2LinAlt.Value;
      EdTit3TxtSiz.ValueVariant := QrConfigBolTit3TxtSiz.Value;
      RGTit3TxtFmt.ItemIndex    := QrConfigBolTit3TxtFmt.Value;
      EdTit3MrgSup.ValueVariant := QrConfigBolTit3MrgSup.Value;
      EdTit3LinAlt.ValueVariant := QrConfigBolTit3LinAlt.Value;
      EdSom3TxtSiz.ValueVariant := QrConfigBolSom3TxtSiz.Value;
      EdSom3ValSiz.ValueVariant := QrConfigBolSom3ValSiz.Value;
      RGSom3TxtFmt.ItemIndex    := QrConfigBolSom3TxtFmt.Value;
      RGSom3ValFmt.ItemIndex    := QrConfigBolSom3ValFmt.Value;
      EdSom3ValLar.ValueVariant := QrConfigBolSom3ValLar.Value;
      EdSom3LinAlt.ValueVariant := QrConfigBolSom3LinAlt.Value;
      EdTit4TxtSiz.ValueVariant := QrConfigBolTit4TxtSiz.Value;
      RGTit4TxtFmt.ItemIndex    := QrConfigBolTit4TxtFmt.Value;
      EdTit4MrgSup.ValueVariant := QrConfigBolTit4MrgSup.Value;
      EdTit4LinAlt.ValueVariant := QrConfigBolTit4LinAlt.Value;
      EdSom4LinAlt.ValueVariant := QrConfigBolSom4LinAlt.Value;
      EdTitRTxtSiz.ValueVariant := QrConfigBolTitRTxtSiz.Value;
      RGTitRTxtFmt.ItemIndex    := QrConfigBolTitRTxtFmt.Value;
      EdTitRMrgSup.ValueVariant := QrConfigBolTitRMrgSup.Value;
      EdTitRLinAlt.ValueVariant := QrConfigBolTitRLinAlt.Value;
      EdTitRTxtTit.ValueVariant := QrConfigBolTitRTxtTit.Value;
      RGTitRPerFmt.ItemIndex    := QrConfigBolTitRPerFmt.Value;
      EdTitCTxtSiz.ValueVariant := QrConfigBolTitCTxtSiz.Value;
      RGTitCTxtFmt.ItemIndex    := QrConfigBolTitCTxtFmt.Value;
      EdTitCMrgSup.ValueVariant := QrConfigBolTitCMrgSup.Value;
      EdTitCLinAlt.ValueVariant := QrConfigBolTitCLinAlt.Value;
      EdTitCTxtTit.ValueVariant := QrConfigBolTitCTxtTit.Value;
      RGTitCPerFmt.ItemIndex    := QrConfigBolTitCPerFmt.Value;
      EdSdoCTxtSiz.ValueVariant := QrConfigBolSdoCTxtSiz.Value;
      RGSdoCTxtFmt.ItemIndex    := QrConfigBolSdoCTxtFmt.Value;
      EdSdoCLinAlt.ValueVariant := QrConfigBolSdoCLinAlt.Value;
      EdTitITxtSiz.ValueVariant := QrConfigBolTitITxtSiz.Value;
      RGTitITxtFmt.ItemIndex    := QrConfigBolTitITxtFmt.Value;
      EdTitIMrgSup.ValueVariant := QrConfigBolTitIMrgSup.Value;
      EdTitILinAlt.ValueVariant := QrConfigBolTitILinAlt.Value;
      EdTitITxtTit.ValueVariant := QrConfigBolTitITxtTit.Value;
      EdSdoITxMSiz.ValueVariant := QrConfigBolSdoITxMSiz.Value;
      RGSdoITxMFmt.ItemIndex    := QrConfigBolSdoITxMFmt.Value;
      EdSdoIVaMSiz.ValueVariant := QrConfigBolSdoIVaMSiz.Value;
      RGSdoIVaMFmt.ItemIndex    := QrConfigBolSdoIVaMFmt.Value;
      EdSdoILiMAlt.ValueVariant := QrConfigBolSdoILiMAlt.Value;
      EdSdoITxMTxt.ValueVariant := QrConfigBolSdoITxMTxt.Value;
      EdSdoILaMVal.ValueVariant := QrConfigBolSdoILaMVal.Value;
      EdSdoITxTSiz.ValueVariant := QrConfigBolSdoITxTSiz.Value;
      RGSdoITxTFmt.ItemIndex    := QrConfigBolSdoITxTFmt.Value;
      EdSdoIVaTSiz.ValueVariant := QrConfigBolSdoIVaTSiz.Value;
      RGSdoIVaTFmt.ItemIndex    := QrConfigBolSdoIVaTFmt.Value;
      EdSdoILiTAlt.ValueVariant := QrConfigBolSdoILiTAlt.Value;
      EdSdoITxTTxt.ValueVariant := QrConfigBolSdoITxTTxt.Value;
      EdSdoILaTVal.ValueVariant := QrConfigBolSdoILaTVal.Value;
      EdCtaPTxtSiz.ValueVariant := QrConfigBolCtaPTxtSiz.Value;
      RGCtaPTxtFmt.ItemIndex    := QrConfigBolCtaPTxtFmt.Value;
      EdCtaPValSiz.ValueVariant := QrConfigBolCtaPValSiz.Value;
      RGCtaPValFmt.ItemIndex    := QrConfigBolCtaPValFmt.Value;
      EdCtaPLinAlt.ValueVariant := QrConfigBolCtaPLinAlt.Value;
      EdCtaPLarVal.ValueVariant := QrConfigBolCtaPLarVal.Value;
      EdSgrPTitSiz.ValueVariant := QrConfigBolSgrPTitSiz.Value;
      RGSgrPTitFmt.ItemIndex    := QrConfigBolSgrPTitFmt.Value;
      EdSgrPTitAlt.ValueVariant := QrConfigBolSgrPTitAlt.Value;
      EdSgrPSumSiz.ValueVariant := QrConfigBolSgrPSumSiz.Value;
      RGSgrPSumFmt.ItemIndex    := QrConfigBolSgrPSumFmt.Value;
      EdSgrPSumAlt.ValueVariant := QrConfigBolSgrPSumAlt.Value;
      EdSgrPLarVal.ValueVariant := QrConfigBolSgrPLarVal.Value;
      EdPrvPTitSiz.ValueVariant := QrConfigBolPrvPTitSiz.Value;
      RGPrvPTitFmt.ItemIndex    := QrConfigBolPrvPTitFmt.Value;
      EdPrvPTitAlt.ValueVariant := QrConfigBolPrvPTitAlt.Value;
      EdPrvPTitStr.ValueVariant := QrConfigBolPrvPTitStr.Value;
      EdPrvPMrgSup.ValueVariant := QrConfigBolPrvPMrgSup.Value;
      EdSumPTxtSiz.ValueVariant := QrConfigBolSumPTxtSiz.Value;
      RGSumPTxtFmt.ItemIndex    := QrConfigBolSumPTxtFmt.Value;
      EdSumPValSiz.ValueVariant := QrConfigBolSumPValSiz.Value;
      RGSumPValFmt.ItemIndex    := QrConfigBolSumPValFmt.Value;
      EdSumPLinAlt.ValueVariant := QrConfigBolSumPLinAlt.Value;
      EdSumPValLar.ValueVariant := QrConfigBolSumPValLar.Value;
      EdSumPTxtStr.ValueVariant := QrConfigBolSumPTxtStr.Value;
      EdCtaATxtSiz.ValueVariant := QrConfigBolCtaATxtSiz.Value;
      RGCtaATxtFmt.ItemIndex    := QrConfigBolCtaATxtFmt.Value;
      EdCtaAValSiz.ValueVariant := QrConfigBolCtaAValSiz.Value;
      RGCtaAValFmt.ItemIndex    := QrConfigBolCtaAValFmt.Value;
      EdCtaALinAlt.ValueVariant := QrConfigBolCtaALinAlt.Value;
      EdCtaALarVal.ValueVariant := QrConfigBolCtaALarVal.Value;
      EdTitATitSiz.ValueVariant := QrConfigBolTitATitSiz.Value;
      RGTitATitFmt.ItemIndex    := QrConfigBolTitATitFmt.Value;
      EdTitATitAlt.ValueVariant := QrConfigBolTitATitAlt.Value;
      EdTitATitStr.ValueVariant := QrConfigBolTitATitStr.Value;
      EdTitAMrgSup.ValueVariant := QrConfigBolTitAMrgSup.Value;
      EdTitAAptSiz.ValueVariant := QrConfigBolTitAAptSiz.Value;
      RGTitAAptFmt.ItemIndex    := QrConfigBolTitAAptFmt.Value;
      EdTitAAptAlt.ValueVariant := QrConfigBolTitAAptAlt.Value;
      EdTitAAptSup.ValueVariant := QrConfigBolTitAAptSup.Value;
      MeTitAAptTex.Text         := QrConfigBolTitAAptTex.Value;
      EdTitASumSiz.ValueVariant := QrConfigBolTitASumSiz.Value;
      RGTitASumFmt.ItemIndex    := QrConfigBolTitASumFmt.Value;
      EdTitASumAlt.ValueVariant := QrConfigBolTitASumAlt.Value;
      CkMeuLogoImp.Checked      := MLAGeral.IntToBool(QrConfigBolMeuLogoImp.Value);
      EdMeuLogoAlt.ValueVariant := QrConfigBolMeuLogoAlt.Value;
      EdMeuLogoLar.ValueVariant := QrConfigBolMeuLogoLar.Value;
      EdMeuLogoArq.Text         := QrConfigBolMeuLogoArq.Value;
      //
      EdTit8LinAlt.ValueVariant := QrConfigBolTit8LinAlt.Value;
      RGTit8TxtFmt.ItemIndex    := QrConfigBolTit8TxtFmt.Value;
      EdTit8TxtSiz.ValueVariant := QrConfigBolTit8TxtSiz.Value;
      EdTit8MrgSup.ValueVariant := QrConfigBolTit8MrgSup.Value;
      RGSom8ValFmt.ItemIndex    := QrConfigBolSom8ValFmt.Value;
      RGSom8TxtFmt.ItemIndex    := QrConfigBolSom8TxtFmt.Value;
      EdSom8LinAlt.ValueVariant := QrConfigBolSom8LinAlt.Value;
      EdSom8ValLar.ValueVariant := QrConfigBolSom8ValLar.Value;
      EdSom8TxtSiz.ValueVariant := QrConfigBolSom8TxtSiz.Value;
      EdSom8ValSiz.ValueVariant := QrConfigBolSom8ValSiz.Value;
      EdNiv8TxtSiz.ValueVariant := QrConfigBolNiv8TxtSiz.Value;
      EdNiv8ValSiz.ValueVariant := QrConfigBolNiv8ValSiz.Value;
      EdNiv8ValLar.ValueVariant := QrConfigBolNiv8ValLar.Value;
      EdNiv8LinAlt.ValueVariant := QrConfigBolNiv8LinAlt.Value;
      CkNiv8Grades.Checked      := MLAGeral.ITB(QrConfigBolNiv8Grades.Value);
      RGNiv8ValFmt.ItemIndex    := QrConfigBolNiv8ValFmt.Value;
      RGNiv8TxtFmt.ItemIndex    := QrConfigBolNiv8TxtFmt.Value;
      //
      EdTitWebTxt.ValueVariant  := QrConfigBolTitWebTxt.Value;
      RGMesCompet.ItemIndex     := QrConfigBolMesCompet.Value;
      //
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmConfigBol.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmConfigBol.AlteraRegistro;
var
  ConfigBol : Integer;
begin
  ConfigBol := QrConfigBolCodigo.Value;
  if QrConfigBolCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(ConfigBol, Dmod.MyDB, 'ConfigBol', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(ConfigBol, Dmod.MyDB, 'ConfigBol', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmConfigBol.QueryPrincipalAfterOpen;
begin
end;

procedure TFmConfigBol.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmConfigBol.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmConfigBol.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmConfigBol.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmConfigBol.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmConfigBol.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmConfigBol.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmConfigBol.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrConfigBolCodigo.Value;
  Close;
end;

procedure TFmConfigBol.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ConfigBol', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ConfigBol', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ConfigBol', 'Codigo');
end;

procedure TFmConfigBol.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  PageControl5.ActivePageIndex := 0;
  PageControl6.ActivePageIndex := 0;
  //
  PainelEdit.Align     := alClient;
  PainelData.Align     := alClient;
  CriaOForm;
end;

procedure TFmConfigBol.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrConfigBolCodigo.Value,LaRegistro.Caption);
end;

procedure TFmConfigBol.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmConfigBol.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmConfigBol.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmConfigBol.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmConfigBol.QrConfigBolAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmConfigBol.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmConfigBol.SbQueryClick(Sender: TObject);
begin
  LocCod(QrConfigBolCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ConfigBol', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmConfigBol.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmConfigBol.QrConfigBolBeforeOpen(DataSet: TDataSet);
begin
  QrConfigBolCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmConfigBol.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if RGColunas.ItemIndex <= 0 then
  begin
    Application.MessageBox('Informe a quantidade de colunas (Balancete)',
    'Aviso', MB_OK+MB_ICONWARNING);
    RGColunas.SetFocus;
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'configbol', False, [
    'Niv1Grades',
    'Nome', 'Colunas', 'TitBTxtSiz', 'TitBTxtFmt',
    'TitBMrgSup', 'TitBLinAlt', 'TitBTxtTit',
    'TitBPerFmt', 'Niv1TxtSiz', 'Niv1ValSiz',
    'Niv1TxtFmt', 'Niv1ValFmt', 'Niv1ValLar',
    'Niv1LinAlt', 'CxaSTxtSiz', 'CxaSValSiz',
    'CxaSTxtFmt', 'CxaSValFmt', 'CxaSTxtAlt',
    'CxaSValAlt', 'CxaSMrgSup',
    'CxaSIniTam', 'CxaSCreTam', 'CxaSDebTam',
    'CxaSMovTam', 'CxaSFimTam', 'CxaSIniTxt',
    'CxaSCreTxt', 'CxaSDebTxt', 'CxaSMovTxt',
    'CxaSFimTxt', 'CxaSMrgInf', 'Tit2TxtSiz',
    'Tit2TxtFmt', 'Tit2MrgSup', 'Tit2LinAlt',
    'Som2TxtSiz', 'Som2ValSiz', 'Som2TxtFmt',
    'Som2ValFmt', 'Som2ValLar', 'Som2LinAlt',
    'Tit3TxtSiz', 'Tit3TxtFmt', 'Tit3MrgSup',
    'Tit3LinAlt', 'Som3TxtSiz', 'Som3ValSiz',
    'Som3TxtFmt', 'Som3ValFmt', 'Som3ValLar',
    'Som3LinAlt', 'Tit4TxtSiz', 'Tit4TxtFmt',
    'Tit4MrgSup', 'Tit4LinAlt', 'Som4LinAlt',
    'TitRTxtSiz', 'TitRTxtFmt', 'TitRMrgSup',
    'TitRLinAlt', 'TitRTxtTit', 'TitRPerFmt',
    'TitCTxtSiz', 'TitCTxtFmt', 'TitCMrgSup',
    'TitCLinAlt', 'TitCTxtTit', 'TitCPerFmt',
    'SdoCTxtSiz', 'SdoCTxtFmt', 'SdoCLinAlt',
    'TitITxtSiz', 'TitITxtFmt', 'TitIMrgSup',
    'TitILinAlt', 'TitITxtTit', 'SdoITxMSiz',
    'SdoITxMFmt', 'SdoIVaMSiz', 'SdoIVaMFmt',
    'SdoILiMAlt', 'SdoITxMTxt', 'SdoILaMVal',
    'SdoITxTSiz', 'SdoITxTFmt', 'SdoIVaTSiz',
    'SdoIVaTFmt', 'SdoILiTAlt', 'SdoITxTTxt',
    'SdoILaTVal', 'CtaPTxtSiz', 'CtaPTxtFmt',
    'CtaPValSiz', 'CtaPValFmt', 'CtaPLinAlt',
    'CtaPLarVal', 'SgrPTitSiz', 'SgrPTitFmt',
    'SgrPTitAlt', 'SgrPSumSiz', 'SgrPSumFmt',
    'SgrPSumAlt', 'SgrPLarVal', 'PrvPTitSiz',
    'PrvPTitFmt', 'PrvPTitAlt', 'PrvPTitStr',
    'PrvPMrgSup', 'SumPTxtSiz', 'SumPTxtFmt',
    'SumPValSiz', 'SumPValFmt', 'SumPLinAlt',
    'SumPValLar', 'SumPTxtStr', 'CtaATxtSiz',
    'CtaATxtFmt', 'CtaAValSiz', 'CtaAValFmt',
    'CtaALinAlt', 'CtaALarVal', 'TitATitSiz',
    'TitATitFmt', 'TitATitAlt', 'TitATitStr',
    'TitAMrgSup', 'TitAAptSiz', 'TitAAptFmt',
    'TitAAptAlt', 'TitAAptSup',
    'TitAAptTex',
    'TitASumSiz',
    'TitASumFmt', 'TitASumAlt', 'MeuLogoImp',
    'MeuLogoAlt', 'MeuLogoLar', 'MeuLogoArq',
    'CxaTrnsTxt', 'Tit8LinAlt', 'Tit8TxtSiz',
    'Tit8MrgSup', 'Tit8TxtFmt', 'Niv8ValLar',
    'Niv8LinAlt', 'Niv8TxtSiz', 'Niv8ValSiz',
    'Niv8Grades', 'Niv8TxtFmt', 'Niv8ValFmt',
    'Som8ValLar', 'Som8LinAlt', 'Som8TxtSiz',
    'Som8ValSiz', 'Som8TxtFmt', 'Som8ValFmt',
    'TitWebTxt',
    'MesCompet'
  ], ['Codigo'], [
    MLAGeral.BoolToInt(CkNiv1Grades.Checked),
    Nome, RGColunas.ItemIndex, EdTitBTxtSiz.ValueVariant, RGTitBTxtFmt.ItemIndex,
    EdTitBMrgSup.ValueVariant, EdTitBLinAlt.ValueVariant, EdTitBTxtTit.Text,
    RGTitBPerFmt.ItemIndex,    EdNiv1TxtSiz.ValueVariant, EdNiv1ValSiz.ValueVariant,
    RGNiv1TxtFmt.ItemIndex,    RGNiv1ValFmt.ItemIndex,    EdNiv1ValLar.ValueVariant,
    EdNiv1LinAlt.ValueVariant, EdCxaSTxtSiz.ValueVariant, EdCxaSValSiz.ValueVariant,
    RGCxaSTxtFmt.ItemIndex,    RGCxaSValFmt.ItemIndex,    EdCxaSTxtAlt.ValueVariant,
    EdCxaSValAlt.ValueVariant, EdCxaSMrgSup.ValueVariant,
    EdCxaSIniTam.ValueVariant, EdCxaSCreTam.ValueVariant, EdCxaSDebTam.ValueVariant,
    EdCxaSMovTam.ValueVariant, EdCxaSFimTam.ValueVariant, EdCxaSIniTxt.Text,
    EdCxaSCreTxt.Text,         EdCxaSDebTxt.Text,         EdCxaSMovTxt.Text,
    EdCxaSFimTxt.Text,         EdCxaSMrgInf.ValueVariant, EdTit2TxtSiz.ValueVariant,
    RgTit2TxtFmt.ItemIndex,    EdTit2MrgSup.ValueVariant, EdTit2LinAlt.ValueVariant,
    EdSom2TxtSiz.ValueVariant, EdSom2ValSiz.ValueVariant, RGSom2TxtFmt.ItemIndex,
    RGSom2ValFmt.ItemIndex,    EdSom2ValLar.ValueVariant, EdSom2LinAlt.ValueVariant,
    EdTit3TxtSiz.ValueVariant, RGTit3TxtFmt.ItemIndex,    EdTit3MrgSup.ValueVariant,
    EdTit3LinAlt.ValueVariant, EdSom3TxtSiz.ValueVariant, EdSom3ValSiz.ValueVariant,
    RGSom3TxtFmt.ItemIndex,    RGSom3ValFmt.ItemIndex,    EdSom3ValLar.ValueVariant,
    EdSom3LinAlt.ValueVariant, EdTit4TxtSiz.ValueVariant, RGTit4TxtFmt.ItemIndex,
    EdTit4MrgSup.ValueVariant, EdTit4LinAlt.ValueVariant, EdSom4LinAlt.ValueVariant,
    EdTitRTxtSiz.ValueVariant, RGTitRTxtFmt.ItemIndex,    EdTitRMrgSup.ValueVariant,
    EdTitRLinAlt.ValueVariant, EdTitRTxtTit.Text,         RGTitRPerFmt.ItemIndex,
    EdTitCTxtSiz.ValueVariant, RGTitCTxtFmt.ItemIndex,    EdTitCMrgSup.ValueVariant,
    EdTitCLinAlt.ValueVariant, EdTitCTxtTit.Text,         RGTitCPerFmt.ItemIndex,
    EdSdoCTxtSiz.ValueVariant, RGSdoCTxtFmt.ItemIndex,    EdSdoCLinAlt.ValueVariant,
    EdTitITxtSiz.ValueVariant, RGTitITxtFmt.ItemIndex,    EdTitIMrgSup.ValueVariant,
    EdTitILinAlt.ValueVariant, EdTitITxtTit.ValueVariant, EdSdoITxMSiz.ValueVariant,
    RGSdoITxMFmt.ItemIndex,    EdSdoIVaMSiz.ValueVariant, RGSdoIVaMFmt.ItemIndex,
    EdSdoILiMAlt.ValueVariant, EdSdoITxMTxt.Text,         EdSdoILaMVal.ValueVariant,
    EdSdoITxTSiz.ValueVariant, RGSdoITxTFmt.ItemIndex,    EdSdoIVaTSiz.ValueVariant,
    RGSdoIVaTFmt.ItemIndex,    EdSdoILiTAlt.ValueVariant, EdSdoITxTTxt.Text,
    EdSdoILaTVal.ValueVariant, EdCtaPTxtSiz.ValueVariant, RGCtaPTxtFmt.ItemIndex,
    EdCtaPValSiz.ValueVariant, RGCtaPValFmt.ItemIndex,    EdCtaPLinAlt.ValueVariant,
    EdCtaPLarVal.ValueVariant, EdSgrPTitSiz.ValueVariant, RGSgrPTitFmt.ItemIndex,
    EdSgrPTitAlt.ValueVariant, EdSgrPSumSiz.ValueVariant, RGSgrPSumFmt.ItemIndex,
    EdSgrPSumAlt.ValueVariant, EdSgrPLarVal.ValueVariant, EdPrvPTitSiz.ValueVariant,
    RGPrvPTitFmt.ItemIndex,    EdPrvPTitAlt.ValueVariant, EdPrvPTitStr.Text,
    EdPrvPMrgSup.ValueVariant, EdSumPTxtSiz.ValueVariant, RGSumPTxtFmt.ItemIndex,
    EdSumPValSiz.ValueVariant, RGSumPValFmt.ItemIndex,    EdSumPLinAlt.ValueVariant,
    EdSumPValLar.ValueVariant, EdSumPTxtStr.ValueVariant, EdCtaATxtSiz.ValueVariant,
    RGCtaATxtFmt.ItemIndex,    EdCtaAValSiz.ValueVariant, RGCtaAValFmt.ItemIndex,
    EdCtaALinAlt.ValueVariant, EdCtaALarVal.ValueVariant, EdTitATitSiz.ValueVariant,
    RGTitATitFmt.ItemIndex,    EdTitATitAlt.ValueVariant, EdTitATitStr.ValueVariant,
    EdTitAMrgSup.ValueVariant, EdTitAAptSiz.ValueVariant, RGTitAAptFmt.ItemIndex,
    EdTitAAptAlt.ValueVariant, EdTitAAptSup.ValueVariant,
    MeTitAAptTex.Text,
    EdTitASumSiz.ValueVariant,
    RGTitASumFmt.ItemIndex,    EdTitASumAlt.ValueVariant, MLAGeral.BoolToInt(CkMeuLogoImp.Checked),
    EdMeuLogoAlt.ValueVariant, EdMeuLogoLar.ValueVariant,
    EdMeuLogoArq.ValueVariant,
    EdCxaTrnsTxt.Text,         EdTit8LinAlt.ValueVariant, EdTit8TxtSiz.ValueVariant,
    EdTit8MrgSup.ValueVariant, RGTit8TxtFmt.ItemIndex,    EdNiv8ValLar.ValueVariant,
    EdNiv8LinAlt.ValueVariant, EdNiv8TxtSiz.ValueVariant, EdNiv8ValSiz.ValueVariant,
    MLAGeral.BTI(CkNiv8Grades.Checked), RGNiv8TxtFmt.ItemIndex, RGNiv8ValFmt.ItemIndex,
    EdSom8ValLar.ValueVariant, EdSom8LinAlt.ValueVariant, EdSom8TxtSiz.ValueVariant,
    EdSom8ValSiz.ValueVariant, RGSom8TxtFmt.ItemIndex,    RGSom8ValFmt.ItemIndex,
    EdTitWebTxt.ValueVariant,
    RGMesCompet.ItemIndex
    ], [
    Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ConfigBol', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
    // For�ar referesh ao imprimir bloqueto novamente
    DmBloq.QrConfigBol.Close;
  end;
end;

procedure TFmConfigBol.Incluinovaconfiguraocombasenoatual1Click(
  Sender: TObject);
begin
  IncluiRegistro(QrConfigBolCodigo.Value);
end;

procedure TFmConfigBol.IncluiRegistro(Codigo: Integer);
var
  Cursor: TCursor;
  ConfigBol, Atual: Integer;
begin
  Atual := QrConfigBolCodigo.Value;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    if Codigo = -1 then LocCod(-1, -1);
    ConfigBol := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ConfigBol', 'ConfigBol', 'Codigo');
    if Length(FormatFloat(FFormatFloat, ConfigBol))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, ConfigBol);
    LocCod(Atual, Atual);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmConfigBol.QrConfigBolAfterScroll(DataSet: TDataSet);
var
  Sim: Boolean;
begin
  Sim := MLAGeral.IntToBool_Query(QrConfigBol);
  if Sim then Sim := QrConfigBolCodigo.Value > 0;
  BtAltera.Enabled := Sim;
  BtExclui.Enabled := Sim;
end;

procedure TFmConfigBol.EdMeuLogoArqKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenPictureDialog2.Execute then EdMeuLogoArq.Text :=
      OpenPictureDialog2.FileName;
  end;
end;

procedure TFmConfigBol.BitBtn1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a recria��o dos registros padr�es?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM configbol WHERE Codigo < 0');
    Dmod.QrUpd.ExecSQL;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'configbol', False, [
    'Nome', 'Niv1ValLar', 'TitBTxtSiz',
    'TitBTxtFmt', 'TitBMrgSup', 'TitBLinAlt',
    'TitBTxtTit', 'TitBPerFmt', 'CxaSTxtSiz',
    'CxaSValSiz', 'CxaSTxtFmt', 'CxaSValFmt',
    'CxaSTxtAlt', 'CxaSValAlt', 'CxaSMrgSup',
    'CxaSIniTam', 'CxaSCreTam', 'CxaSDebTam',
    'CxaSMovTam', 'CxaSFimTam', 'CxaSIniTxt',
    'CxaSCreTxt', 'CxaSDebTxt', 'CxaSMovTxt',
    'CxaSFimTxt', 'CxaSMrgInf', 'Niv1TxtSiz',
    'Niv1ValSiz', 'Niv1TxtFmt', 'Niv1ValFmt',
    'Niv1LinAlt', 'Tit2TxtSiz', 'Tit2TxtFmt', 
    'Tit2MrgSup', 'Tit2LinAlt', 'Som2TxtSiz', 
    'Som2ValSiz', 'Som2TxtFmt', 'Som2ValFmt', 
    'Som2ValLar', 'Som2LinAlt', 'Tit3TxtSiz', 
    'Tit3TxtFmt', 'Tit3MrgSup', 'Tit3LinAlt', 
    'Som3TxtSiz', 'Som3ValSiz', 'Som3TxtFmt', 
    'Som3ValFmt', 'Som3ValLar', 'Som3LinAlt', 
    'Tit4TxtSiz', 'Tit4TxtFmt', 'Tit4MrgSup', 
    'Tit4LinAlt', 'Som4LinAlt', 'TitRTxtSiz', 
    'TitRTxtFmt', 'TitRMrgSup', 'TitRLinAlt', 
    'TitRTxtTit', 'TitRPerFmt', 'TitCTxtSiz', 
    'TitCTxtFmt', 'TitCMrgSup', 'TitCLinAlt', 
    'TitCTxtTit', 'TitCPerFmt', 'SdoCTxtSiz', 
    'SdoCTxtFmt', 'SdoCLinAlt', 'TitITxtSiz', 
    'TitITxtFmt', 'TitIMrgSup', 'TitILinAlt', 
    'TitITxtTit', 'SdoITxMSiz', 'SdoITxMFmt', 
    'SdoIVaMSiz', 'SdoIVaMFmt', 'SdoILiMAlt', 
    'SdoITxMTxt', 'SdoILaMVal', 'SdoITxTSiz',
    'SdoITxTFmt', 'SdoIVaTSiz', 'SdoIVaTFmt', 
    'SdoILiTAlt', 'SdoITxTTxt', 'SdoILaTVal', 
    'CtaPTxtSiz', 'CtaPTxtFmt', 'CtaPValSiz', 
    'CtaPValFmt', 'CtaPLinAlt', 'CtaPLarVal', 
    'SgrPTitSiz', 'SgrPTitFmt', 'SgrPTitAlt',
    'SgrPSumSiz', 'SgrPSumFmt', 'SgrPSumAlt', 
    'SgrPLarVal', 'PrvPTitSiz', 'PrvPTitFmt', 
    'PrvPTitAlt', 'PrvPTitStr', 'PrvPMrgSup',
    'SumPTxtSiz', 'SumPTxtFmt', 'SumPValSiz', 
    'SumPValFmt', 'SumPLinAlt', 'SumPValLar', 
    'SumPTxtStr', 'CtaATxtSiz', 'CtaATxtFmt', 
    'CtaAValSiz', 'CtaAValFmt', 'CtaALinAlt', 
    'CtaALarVal', 'TitATitSiz', 'TitATitFmt', 
    'TitATitAlt', 'TitATitStr', 'TitAMrgSup', 
    'TitAAptSiz', 'TitAAptFmt', 'TitAAptAlt', 
    'TitAAptSup', 'TitASumSiz', 'TitASumFmt', 
    'TitASumAlt', 'MeuLogoImp', 'MeuLogoAlt', 
    'MeuLogoLar', 'MeuLogoArq', 'Colunas', 
    'Niv1Grades', 'CxaTrnsTxt'], [
    'Codigo'], [
    '[Padr�o 6 colunas]', '900', '8',
    '1', '0', '800',
    'BALANCETE DEMONSTRATIVO', '0', '4',
    '4', '1', '1',
    '220', '220', '100',
    '20', '20', '20',
    '20', '20', 'Sdo ant.',
    'Receitas', 'Despesas', 'Sdo m�s',
    'Sdo final', '100', '5',
    '5', '0', '0',
    '200', '6', '1',
    '50', '250', '6',
    '6', '2', '1',
    '1000', '250', '7',
    '1', '100', '350',
    '7', '7', '1',
    '1', '1000', '300',
    '8', '1', '0',
    '350', '220', '8',
    '1', '150', '800',
    'RESUMO FINANCEIRO', '0', '5',
    '1', '100', '220',
    'CONTAS CORRENTES -', '0', '5',
    '1', '270', '6',
    '1', '150', '270',
    'INADIMPL�NCIA', '5', '0',
    '5', '0', '200',
    'Inadimpl�ncia minha unidade at�', '1000', '5',
    '0', '5', '0',
    '200', 'Inadimpl�ncia todas unidades at�', '1000',
    '6', '0', '6',
    '0', '240', '1000',
    '6', '1', '270',
    '6', '1', '270',
    '1000', '7', '1',
    '300', 'PROVIS�O PARA AS DESPESAS DE', '270',
    '6', '1', '6',
    '1', '270', '1000',
    'TOTAL PROVIS�ES', '6', '0',
    '6', '0', '240',
    '1000', '6', '1',
    '300', 'COMPOSI��O DA ARRECADA��O -', '270',
    '6', '1', '300',
    '50', '8', '1',
    '350', '1', '1150',
    '3000', '', '6',
    '1', 'Transf.'], [
    '-6'], true);


    //


    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'configbol', False, [
    'Nome', 'Niv1ValLar', 'TitBTxtSiz',
    'TitBTxtFmt', 'TitBMrgSup', 'TitBLinAlt',
    'TitBTxtTit', 'TitBPerFmt', 'CxaSTxtSiz',
    'CxaSValSiz', 'CxaSTxtFmt', 'CxaSValFmt',
    'CxaSTxtAlt', 'CxaSValAlt', 'CxaSMrgSup',
    'CxaSIniTam', 'CxaSCreTam', 'CxaSDebTam',
    'CxaSMovTam', 'CxaSFimTam', 'CxaSIniTxt',
    'CxaSCreTxt', 'CxaSDebTxt', 'CxaSMovTxt',
    'CxaSFimTxt', 'CxaSMrgInf', 'Niv1TxtSiz',
    'Niv1ValSiz', 'Niv1TxtFmt', 'Niv1ValFmt',
    'Niv1LinAlt', 'Tit2TxtSiz', 'Tit2TxtFmt',
    'Tit2MrgSup', 'Tit2LinAlt', 'Som2TxtSiz',
    'Som2ValSiz', 'Som2TxtFmt', 'Som2ValFmt',
    'Som2ValLar', 'Som2LinAlt', 'Tit3TxtSiz',
    'Tit3TxtFmt', 'Tit3MrgSup', 'Tit3LinAlt',
    'Som3TxtSiz', 'Som3ValSiz', 'Som3TxtFmt',
    'Som3ValFmt', 'Som3ValLar', 'Som3LinAlt',
    'Tit4TxtSiz', 'Tit4TxtFmt', 'Tit4MrgSup',
    'Tit4LinAlt', 'Som4LinAlt', 'TitRTxtSiz',
    'TitRTxtFmt', 'TitRMrgSup', 'TitRLinAlt',
    'TitRTxtTit', 'TitRPerFmt', 'TitCTxtSiz',
    'TitCTxtFmt', 'TitCMrgSup', 'TitCLinAlt',
    'TitCTxtTit', 'TitCPerFmt', 'SdoCTxtSiz',
    'SdoCTxtFmt', 'SdoCLinAlt', 'TitITxtSiz',
    'TitITxtFmt', 'TitIMrgSup', 'TitILinAlt',
    'TitITxtTit', 'SdoITxMSiz', 'SdoITxMFmt',
    'SdoIVaMSiz', 'SdoIVaMFmt', 'SdoILiMAlt',
    'SdoITxMTxt', 'SdoILaMVal', 'SdoITxTSiz',
    'SdoITxTFmt', 'SdoIVaTSiz', 'SdoIVaTFmt',
    'SdoILiTAlt', 'SdoITxTTxt', 'SdoILaTVal',
    'CtaPTxtSiz', 'CtaPTxtFmt', 'CtaPValSiz',
    'CtaPValFmt', 'CtaPLinAlt', 'CtaPLarVal',
    'SgrPTitSiz', 'SgrPTitFmt', 'SgrPTitAlt',
    'SgrPSumSiz', 'SgrPSumFmt', 'SgrPSumAlt',
    'SgrPLarVal', 'PrvPTitSiz', 'PrvPTitFmt',
    'PrvPTitAlt', 'PrvPTitStr', 'PrvPMrgSup',
    'SumPTxtSiz', 'SumPTxtFmt', 'SumPValSiz',
    'SumPValFmt', 'SumPLinAlt', 'SumPValLar',
    'SumPTxtStr', 'CtaATxtSiz', 'CtaATxtFmt',
    'CtaAValSiz', 'CtaAValFmt', 'CtaALinAlt',
    'CtaALarVal', 'TitATitSiz', 'TitATitFmt',
    'TitATitAlt', 'TitATitStr', 'TitAMrgSup',
    'TitAAptSiz', 'TitAAptFmt', 'TitAAptAlt',
    'TitAAptSup', 'TitASumSiz', 'TitASumFmt',
    'TitASumAlt', 'MeuLogoImp', 'MeuLogoAlt',
    'MeuLogoLar', 'MeuLogoArq', 'Colunas',
    'Niv1Grades', 'CxaTrnsTxt'], [
    'Codigo'], [
    '[Padr�o 5 colunas]', '900', '5',
    '1', '0', '300',
    'BALANCETE DEMONSTRATIVO -', '0', '5',
    '5', '1', '1',
    '220', '220', '100',
    '20', '20', '20',
    '20', '20', 'Sdo anter.',
    'Receitas', 'Despesas', 'Saldo m�s',
    'Saldo final', '100', '6',
    '6', '0', '0',
    '220', '6', '1',
    '50', '250', '6',
    '6', '2', '1',
    '1000', '250', '7',
    '1', '100', '350',
    '7', '7', '1',
    '1', '1000', '350',
    '8', '1', '0',
    '350', '220', '6',
    '1', '150', '270',
    'RESUMO FINANCEIRO -', '0', '5',
    '1', '150', '220',
    'CONTAS CORRENTES -', '0', '5',
    '1', '270', '6',
    '1', '150', '270',
    'INADIMPL�NCIA', '5', '0',
    '5', '0', '200',
    'Inadimpl�ncia minha unidade at�', '1000', '5',
    '0', '5', '0',
    '200', 'Inadimpl�ncia todas unidades at�', '1000',
    '6', '0', '6',
    '0', '240', '1000',
    '6', '1', '270',
    '6', '1', '270',
    '1000', '7', '1',
    '300', 'PROVIS�O PARA AS DESPESAS DE', '270',
    '6', '1', '6',
    '1', '270', '1000',
    'TOTAL PROVIS�ES', '6', '0',
    '6', '0', '240',
    '1000', '6', '1',
    '300', 'COMPOSI��O DA ARRECADA��O -', '270',
    '6', '1', '300',
    '50', '6', '1',
    '350', '1', '1360',
    '3560', '', '5',
    '1', 'Transf'], [
    '-5'], True);


    //


    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'configbol', False, [
    'Nome', 'Niv1ValLar', 'TitBTxtSiz',
    'TitBTxtFmt', 'TitBMrgSup', 'TitBLinAlt',
    'TitBTxtTit', 'TitBPerFmt', 'CxaSTxtSiz',
    'CxaSValSiz', 'CxaSTxtFmt', 'CxaSValFmt',
    'CxaSTxtAlt', 'CxaSValAlt', 'CxaSMrgSup',
    'CxaSIniTam', 'CxaSCreTam', 'CxaSDebTam',
    'CxaSMovTam', 'CxaSFimTam', 'CxaSIniTxt',
    'CxaSCreTxt', 'CxaSDebTxt', 'CxaSMovTxt',
    'CxaSFimTxt', 'CxaSMrgInf', 'Niv1TxtSiz',
    'Niv1ValSiz', 'Niv1TxtFmt', 'Niv1ValFmt',
    'Niv1LinAlt', 'Tit2TxtSiz', 'Tit2TxtFmt',
    'Tit2MrgSup', 'Tit2LinAlt', 'Som2TxtSiz',
    'Som2ValSiz', 'Som2TxtFmt', 'Som2ValFmt',
    'Som2ValLar', 'Som2LinAlt', 'Tit3TxtSiz',
    'Tit3TxtFmt', 'Tit3MrgSup', 'Tit3LinAlt',
    'Som3TxtSiz', 'Som3ValSiz', 'Som3TxtFmt',
    'Som3ValFmt', 'Som3ValLar', 'Som3LinAlt',
    'Tit4TxtSiz', 'Tit4TxtFmt', 'Tit4MrgSup',
    'Tit4LinAlt', 'Som4LinAlt', 'TitRTxtSiz',
    'TitRTxtFmt', 'TitRMrgSup', 'TitRLinAlt',
    'TitRTxtTit', 'TitRPerFmt', 'TitCTxtSiz',
    'TitCTxtFmt', 'TitCMrgSup', 'TitCLinAlt',
    'TitCTxtTit', 'TitCPerFmt', 'SdoCTxtSiz',
    'SdoCTxtFmt', 'SdoCLinAlt', 'TitITxtSiz',
    'TitITxtFmt', 'TitIMrgSup', 'TitILinAlt',
    'TitITxtTit', 'SdoITxMSiz', 'SdoITxMFmt',
    'SdoIVaMSiz', 'SdoIVaMFmt', 'SdoILiMAlt',
    'SdoITxMTxt', 'SdoILaMVal', 'SdoITxTSiz',
    'SdoITxTFmt', 'SdoIVaTSiz', 'SdoIVaTFmt',
    'SdoILiTAlt', 'SdoITxTTxt', 'SdoILaTVal',
    'CtaPTxtSiz', 'CtaPTxtFmt', 'CtaPValSiz',
    'CtaPValFmt', 'CtaPLinAlt', 'CtaPLarVal',
    'SgrPTitSiz', 'SgrPTitFmt', 'SgrPTitAlt',
    'SgrPSumSiz', 'SgrPSumFmt', 'SgrPSumAlt',
    'SgrPLarVal', 'PrvPTitSiz', 'PrvPTitFmt',
    'PrvPTitAlt', 'PrvPTitStr', 'PrvPMrgSup',
    'SumPTxtSiz', 'SumPTxtFmt', 'SumPValSiz',
    'SumPValFmt', 'SumPLinAlt', 'SumPValLar',
    'SumPTxtStr', 'CtaATxtSiz', 'CtaATxtFmt',
    'CtaAValSiz', 'CtaAValFmt', 'CtaALinAlt',
    'CtaALarVal', 'TitATitSiz', 'TitATitFmt',
    'TitATitAlt', 'TitATitStr', 'TitAMrgSup',
    'TitAAptSiz', 'TitAAptFmt', 'TitAAptAlt',
    'TitAAptSup', 'TitASumSiz', 'TitASumFmt',
    'TitASumAlt', 'MeuLogoImp', 'MeuLogoAlt',
    'MeuLogoLar', 'MeuLogoArq', 'Colunas',
    'Niv1Grades', 'CxaTrnsTxt'], [
    'Codigo'], [
    '[Padr�o 4 colunas]', '1000', '7',
    '1', '0', '300',
    'BALANCETE DEMONSTRATIVO -', '0', '6',
    '6', '1', '1',
    '240', '240', '150',
    '20', '19', '19',
    '19', '20', 'Sdo anter.',
    'Receitas', 'Despesas', 'Saldo m�s',
    'Saldo final', '100', '6',
    '6', '0', '0',
    '240', '7', '1',
    '50', '270', '6',
    '6', '2', '1',
    '1000', '270', '8',
    '1', '100', '400',
    '7', '7', '1',
    '1', '1000', '350',
    '9', '1', '0',
    '400', '270', '7',
    '1', '270', '300',
    'RESUMO FINANCEIRO -', '0', '6',
    '1', '270', '270',
    'CONTAS CORRENTES -', '0', '6',
    '1', '270', '7',
    '1', '270', '270',
    'INADIMPL�NCIA', '6', '0',
    '6', '0', '270',
    'Inadimpl�ncia minha unidade at�', '1000', '6',
    '0', '6', '0',
    '270', 'Inadimpl�ncia todas unidades at�', '1000',
    '6', '0', '6',
    '0', '240', '1000',
    '6', '1', '270',
    '6', '1', '270',
    '1000', '7', '1',
    '300', 'PROVIS�O PARA AS DESPESAS DE', '270',
    '6', '1', '6',
    '1', '270', '1000',
    'TOTAL PROVIS�ES', '6', '0',
    '6', '0', '240',
    '1000', '7', '1',
    '300', 'COMPOSI��O DA ARRECADA��O -', '270',
    '7', '1', '300',
    '50', '7', '1',
    '400', '1', '1707',
    '4480', '', '4',
    '0', 'Transf'], [
    '-4'], True);


    //


    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'configbol', False, [
    'Nome', 'Niv1ValLar', 'TitBTxtSiz',
    'TitBTxtFmt', 'TitBMrgSup', 'TitBLinAlt',
    'TitBTxtTit', 'TitBPerFmt', 'CxaSTxtSiz',
    'CxaSValSiz', 'CxaSTxtFmt', 'CxaSValFmt',
    'CxaSTxtAlt', 'CxaSValAlt', 'CxaSMrgSup',
    'CxaSIniTam', 'CxaSCreTam', 'CxaSDebTam',
    'CxaSMovTam', 'CxaSFimTam', 'CxaSIniTxt',
    'CxaSCreTxt', 'CxaSDebTxt', 'CxaSMovTxt',
    'CxaSFimTxt', 'CxaSMrgInf', 'Niv1TxtSiz',
    'Niv1ValSiz', 'Niv1TxtFmt', 'Niv1ValFmt',
    'Niv1LinAlt', 'Tit2TxtSiz', 'Tit2TxtFmt',
    'Tit2MrgSup', 'Tit2LinAlt', 'Som2TxtSiz',
    'Som2ValSiz', 'Som2TxtFmt', 'Som2ValFmt',
    'Som2ValLar', 'Som2LinAlt', 'Tit3TxtSiz',
    'Tit3TxtFmt', 'Tit3MrgSup', 'Tit3LinAlt',
    'Som3TxtSiz', 'Som3ValSiz', 'Som3TxtFmt',
    'Som3ValFmt', 'Som3ValLar', 'Som3LinAlt',
    'Tit4TxtSiz', 'Tit4TxtFmt', 'Tit4MrgSup',
    'Tit4LinAlt', 'Som4LinAlt', 'TitRTxtSiz',
    'TitRTxtFmt', 'TitRMrgSup', 'TitRLinAlt',
    'TitRTxtTit', 'TitRPerFmt', 'TitCTxtSiz',
    'TitCTxtFmt', 'TitCMrgSup', 'TitCLinAlt',
    'TitCTxtTit', 'TitCPerFmt', 'SdoCTxtSiz',
    'SdoCTxtFmt', 'SdoCLinAlt', 'TitITxtSiz',
    'TitITxtFmt', 'TitIMrgSup', 'TitILinAlt',
    'TitITxtTit', 'SdoITxMSiz', 'SdoITxMFmt',
    'SdoIVaMSiz', 'SdoIVaMFmt', 'SdoILiMAlt',
    'SdoITxMTxt', 'SdoILaMVal', 'SdoITxTSiz',
    'SdoITxTFmt', 'SdoIVaTSiz', 'SdoIVaTFmt',
    'SdoILiTAlt', 'SdoITxTTxt', 'SdoILaTVal',
    'CtaPTxtSiz', 'CtaPTxtFmt', 'CtaPValSiz',
    'CtaPValFmt', 'CtaPLinAlt', 'CtaPLarVal',
    'SgrPTitSiz', 'SgrPTitFmt', 'SgrPTitAlt',
    'SgrPSumSiz', 'SgrPSumFmt', 'SgrPSumAlt',
    'SgrPLarVal', 'PrvPTitSiz', 'PrvPTitFmt',
    'PrvPTitAlt', 'PrvPTitStr', 'PrvPMrgSup',
    'SumPTxtSiz', 'SumPTxtFmt', 'SumPValSiz',
    'SumPValFmt', 'SumPLinAlt', 'SumPValLar',
    'SumPTxtStr', 'CtaATxtSiz', 'CtaATxtFmt',
    'CtaAValSiz', 'CtaAValFmt', 'CtaALinAlt',
    'CtaALarVal', 'TitATitSiz', 'TitATitFmt',
    'TitATitAlt', 'TitATitStr', 'TitAMrgSup',
    'TitAAptSiz', 'TitAAptFmt', 'TitAAptAlt',
    'TitAAptSup', 'TitASumSiz', 'TitASumFmt',
    'TitASumAlt', 'MeuLogoImp', 'MeuLogoAlt',
    'MeuLogoLar', 'MeuLogoArq', 'Colunas',
    'Niv1Grades', 'CxaTrnsTxt'], [
    'Codigo'], [
    '[Padr�o 3 colunas]', '1200', '8',
    '1', '0', '350',
    'BALANCETE DEMONSTRATIVO -', '0', '7',
    '7', '1', '1',
    '300', '300', '150',
    '20,34', '19,49', '19,49',
    '20,34', '20,34', 'Sdo Anterior',
    'Receitas', 'Despesas', 'Saldo M�s',
    'Saldo Final', '100', '7',
    '7', '0', '0',
    '270', '8', '1',
    '50', '300', '7',
    '7', '2', '1',
    '1200', '300', '9',
    '1', '100', '600',
    '8', '8', '1',
    '1', '1200', '450',
    '10', '1', '0',
    '450', '300', '8',
    '1', '300', '350',
    'RESUMO FINANCEIRO -', '0', '7',
    '1', '300', '300',
    'CONTAS CORRENTES -', '0', '7',
    '1', '300', '8',
    '1', '300', '300',
    'INADIMPL�NCIA', '7', '0',
    '7', '0', '300',
    'Inadimpl�ncia minha unidade at�', '1200', '7',
    '0', '7', '0',
    '300', 'Inadimpl�ncia todas unidades at�', '1200',
    '7', '0', '7',
    '0', '270', '1200',
    '7', '1', '300',
    '7', '1', '300',
    '1200', '8', '1',
    '350', 'PROVIS�O PARA AS DESPESAS DE', '300',
    '7', '1', '7',
    '1', '300', '1200',
    'TOTAL PROVIS�ES', '7', '0',
    '7', '0', '270',
    '1200', '8', '1',
    '350', 'COMPOSI��O DA ARRECADA��O -', '300',
    '8', '1', '350',
    '50', '8', '1',
    '500', '1', '1800',
    '4500', '', '3',
    '1', 'Transf'], [
    '-3'], True);


    //


    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'configbol', False, [
    'Nome', 'Niv1ValLar', 'TitBTxtSiz',
    'TitBTxtFmt', 'TitBMrgSup', 'TitBLinAlt',
    'TitBTxtTit', 'TitBPerFmt', 'CxaSTxtSiz',
    'CxaSValSiz', 'CxaSTxtFmt', 'CxaSValFmt',
    'CxaSTxtAlt', 'CxaSValAlt', 'CxaSMrgSup',
    'CxaSIniTam', 'CxaSCreTam', 'CxaSDebTam',
    'CxaSMovTam', 'CxaSFimTam', 'CxaSIniTxt',
    'CxaSCreTxt', 'CxaSDebTxt', 'CxaSMovTxt',
    'CxaSFimTxt', 'CxaSMrgInf', 'Niv1TxtSiz',
    'Niv1ValSiz', 'Niv1TxtFmt', 'Niv1ValFmt',
    'Niv1LinAlt', 'Tit2TxtSiz', 'Tit2TxtFmt',
    'Tit2MrgSup', 'Tit2LinAlt', 'Som2TxtSiz',
    'Som2ValSiz', 'Som2TxtFmt', 'Som2ValFmt',
    'Som2ValLar', 'Som2LinAlt', 'Tit3TxtSiz',
    'Tit3TxtFmt', 'Tit3MrgSup', 'Tit3LinAlt',
    'Som3TxtSiz', 'Som3ValSiz', 'Som3TxtFmt',
    'Som3ValFmt', 'Som3ValLar', 'Som3LinAlt',
    'Tit4TxtSiz', 'Tit4TxtFmt', 'Tit4MrgSup',
    'Tit4LinAlt', 'Som4LinAlt', 'TitRTxtSiz',
    'TitRTxtFmt', 'TitRMrgSup', 'TitRLinAlt',
    'TitRTxtTit', 'TitRPerFmt', 'TitCTxtSiz',
    'TitCTxtFmt', 'TitCMrgSup', 'TitCLinAlt',
    'TitCTxtTit', 'TitCPerFmt', 'SdoCTxtSiz',
    'SdoCTxtFmt', 'SdoCLinAlt', 'TitITxtSiz',
    'TitITxtFmt', 'TitIMrgSup', 'TitILinAlt',
    'TitITxtTit', 'SdoITxMSiz', 'SdoITxMFmt',
    'SdoIVaMSiz', 'SdoIVaMFmt', 'SdoILiMAlt',
    'SdoITxMTxt', 'SdoILaMVal', 'SdoITxTSiz',
    'SdoITxTFmt', 'SdoIVaTSiz', 'SdoIVaTFmt',
    'SdoILiTAlt', 'SdoITxTTxt', 'SdoILaTVal',
    'CtaPTxtSiz', 'CtaPTxtFmt', 'CtaPValSiz',
    'CtaPValFmt', 'CtaPLinAlt', 'CtaPLarVal',
    'SgrPTitSiz', 'SgrPTitFmt', 'SgrPTitAlt',
    'SgrPSumSiz', 'SgrPSumFmt', 'SgrPSumAlt',
    'SgrPLarVal', 'PrvPTitSiz', 'PrvPTitFmt',
    'PrvPTitAlt', 'PrvPTitStr', 'PrvPMrgSup',
    'SumPTxtSiz', 'SumPTxtFmt', 'SumPValSiz',
    'SumPValFmt', 'SumPLinAlt', 'SumPValLar',
    'SumPTxtStr', 'CtaATxtSiz', 'CtaATxtFmt',
    'CtaAValSiz', 'CtaAValFmt', 'CtaALinAlt',
    'CtaALarVal', 'TitATitSiz', 'TitATitFmt',
    'TitATitAlt', 'TitATitStr', 'TitAMrgSup',
    'TitAAptSiz', 'TitAAptFmt', 'TitAAptAlt',
    'TitAAptSup', 'TitASumSiz', 'TitASumFmt',
    'TitASumAlt', 'MeuLogoImp', 'MeuLogoAlt',
    'MeuLogoLar', 'MeuLogoArq', 'Colunas',
    'Niv1Grades', 'CxaTrnsTxt'], [
    'Codigo'], [
    '[Padr�o 2 colunas]', '1800', '13',
    '1', '0', '600',
    'BALANCETE DEMONSTRATIVO -', '1', '11',
    '11', '1', '1',
    '380', '380', '150',
    '20', '20', '20',
    '20', '20', 'Sdo Anterior',
    'Receitas', 'Despesas', 'Saldo M�s',
    'Saldo Final', '100', '10',
    '10', '0', '0',
    '340', '11', '1',
    '150', '450', '10',
    '10', '2', '1',
    '1800', '400', '12',
    '1', '100', '600',
    '12', '12', '1',
    '1', '1800', '550',
    '13', '1', '0',
    '550', '600', '9',
    '1', '300', '350',
    'RESUMO FINANCEIRO -', '1', '8',
    '1', '300', '300',
    'CONTAS CORRENTES -', '1', '8',
    '1', '300', '10',
    '1', '150', '400',
    'INADIMPL�NCIA', '10', '1',
    '10', '0', '340',
    'MEUS D�BITOS AT�', '1800', '10',
    '0', '10', '0',
    '340', 'Inadimpl�ncia Condom�nio', '1800',
    '10', '0', '10',
    '0', '340', '1800',
    '11', '1', '380',
    '11', '1', '450',
    '1800', '12', '1',
    '470', 'PROVIS�O PARA AS DESPESAS DE', '300',
    '12', '1', '12',
    '1', '470', '1800',
    'TOTAL PROVIS�ES', '10', '0',
    '10', '0', '340',
    '1800', '10', '1',
    '450', 'COMPOSI��O DA ARRECADA��O -', '500',
    '12', '1', '600',
    '150', '14', '1',
    '800', '1', '1430',
    '4500', '', '2',
    '1', 'Transf'], [
    '-2'], True);


    //


    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'configbol', False, [
    'Nome', 'Niv1ValLar', 'TitBTxtSiz',
    'TitBTxtFmt', 'TitBMrgSup', 'TitBLinAlt',
    'TitBTxtTit', 'TitBPerFmt', 'CxaSTxtSiz',
    'CxaSValSiz', 'CxaSTxtFmt', 'CxaSValFmt',
    'CxaSTxtAlt', 'CxaSValAlt', 'CxaSMrgSup',
    'CxaSIniTam', 'CxaSCreTam', 'CxaSDebTam',
    'CxaSMovTam', 'CxaSFimTam', 'CxaSIniTxt',
    'CxaSCreTxt', 'CxaSDebTxt', 'CxaSMovTxt',
    'CxaSFimTxt', 'CxaSMrgInf', 'Niv1TxtSiz',
    'Niv1ValSiz', 'Niv1TxtFmt', 'Niv1ValFmt',
    'Niv1LinAlt', 'Tit2TxtSiz', 'Tit2TxtFmt',
    'Tit2MrgSup', 'Tit2LinAlt', 'Som2TxtSiz',
    'Som2ValSiz', 'Som2TxtFmt', 'Som2ValFmt',
    'Som2ValLar', 'Som2LinAlt', 'Tit3TxtSiz',
    'Tit3TxtFmt', 'Tit3MrgSup', 'Tit3LinAlt',
    'Som3TxtSiz', 'Som3ValSiz', 'Som3TxtFmt',
    'Som3ValFmt', 'Som3ValLar', 'Som3LinAlt',
    'Tit4TxtSiz', 'Tit4TxtFmt', 'Tit4MrgSup',
    'Tit4LinAlt', 'Som4LinAlt', 'TitRTxtSiz',
    'TitRTxtFmt', 'TitRMrgSup', 'TitRLinAlt',
    'TitRTxtTit', 'TitRPerFmt', 'TitCTxtSiz',
    'TitCTxtFmt', 'TitCMrgSup', 'TitCLinAlt',
    'TitCTxtTit', 'TitCPerFmt', 'SdoCTxtSiz',
    'SdoCTxtFmt', 'SdoCLinAlt', 'TitITxtSiz',
    'TitITxtFmt', 'TitIMrgSup', 'TitILinAlt',
    'TitITxtTit', 'SdoITxMSiz', 'SdoITxMFmt',
    'SdoIVaMSiz', 'SdoIVaMFmt', 'SdoILiMAlt',
    'SdoITxMTxt', 'SdoILaMVal', 'SdoITxTSiz',
    'SdoITxTFmt', 'SdoIVaTSiz', 'SdoIVaTFmt',
    'SdoILiTAlt', 'SdoITxTTxt', 'SdoILaTVal',
    'CtaPTxtSiz', 'CtaPTxtFmt', 'CtaPValSiz',
    'CtaPValFmt', 'CtaPLinAlt', 'CtaPLarVal',
    'SgrPTitSiz', 'SgrPTitFmt', 'SgrPTitAlt',
    'SgrPSumSiz', 'SgrPSumFmt', 'SgrPSumAlt',
    'SgrPLarVal', 'PrvPTitSiz', 'PrvPTitFmt',
    'PrvPTitAlt', 'PrvPTitStr', 'PrvPMrgSup',
    'SumPTxtSiz', 'SumPTxtFmt', 'SumPValSiz',
    'SumPValFmt', 'SumPLinAlt', 'SumPValLar',
    'SumPTxtStr', 'CtaATxtSiz', 'CtaATxtFmt',
    'CtaAValSiz', 'CtaAValFmt', 'CtaALinAlt',
    'CtaALarVal', 'TitATitSiz', 'TitATitFmt',
    'TitATitAlt', 'TitATitStr', 'TitAMrgSup',
    'TitAAptSiz', 'TitAAptFmt', 'TitAAptAlt',
    'TitAAptSup', 'TitASumSiz', 'TitASumFmt',
    'TitASumAlt', 'MeuLogoImp', 'MeuLogoAlt',
    'MeuLogoLar', 'MeuLogoArq', 'Colunas',
    'Niv1Grades', 'CxaTrnsTxt'], [
    'Codigo'], [
    '[Padr�o 1 colunas]', '1800', '13',
    '1', '0', '600',
    'BALANCETE DEMONSTRATIVO -', '1', '11',
    '11', '1', '1',
    '380', '380', '150',
    '20', '20', '20',
    '20', '20', 'Sdo Anterior',
    'Receitas', 'Despesas', 'Saldo M�s',
    'Saldo Final', '100', '10',
    '10', '0', '0',
    '340', '11', '1',
    '150', '450', '10',
    '10', '2', '1',
    '1800', '400', '12',
    '1', '100', '600',
    '12', '12', '1',
    '1', '1800', '550',
    '13', '1', '0',
    '550', '600', '9',
    '1', '300', '350',
    'RESUMO FINANCEIRO -', '1', '8',
    '1', '300', '300',
    'CONTAS CORRENTES -', '1', '8',
    '1', '300', '10',
    '1', '150', '400',
    'INADIMPL�NCIA', '10', '1',
    '10', '0', '340',
    'MEUS D�BITOS AT�', '1800', '10',
    '0', '10', '0',
    '340', 'Inadimpl�ncia Condom�nio', '1800',
    '10', '0', '10',
    '0', '340', '1800',
    '11', '1', '380',
    '11', '1', '450',
    '1800', '12', '1',
    '470', 'PROVIS�O PARA AS DESPESAS DE', '300',
    '12', '1', '12',
    '1', '470', '1800',
    'TOTAL PROVIS�ES', '10', '0',
    '10', '0', '340',
    '1800', '10', '1',
    '450', 'COMPOSI��O DA ARRECADA��O -', '500',
    '12', '1', '600',
    '150', '14', '1',
    '800', '1', '1430',
    '4500', '', '1',
    '1', 'Transf.'], [
    '-1'], True);
    Application.MessageBox('Padr�es criados com sucesso!',
     'Mensagem', MB_OK+MB_ICONINFORMATION);
  end;
end;

//testar configbol e acertar frx
end.

