object FmArreBaCAptos: TFmArreBaCAptos
  Left = 339
  Top = 185
  Caption = 'CAD-ARREC-003 :: Arrecada'#231#227'o - Sele'#231#227'o de Unidades'
  ClientHeight = 494
  ClientWidth = 722
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 722
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 52
      Width = 722
      Height = 280
      Align = alClient
      DataSource = DsUnidCond
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnDrawColumnCell = DBGrid1DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Selecio'
          ReadOnly = True
          Title.Caption = 'Sel'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 71
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Proprie'
          Width = 550
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 722
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object dmkLabel1: TdmkLabel
        Left = 8
        Top = 8
        Width = 40
        Height = 13
        Caption = 'Inclus'#227'o'
        Enabled = False
        UpdType = utYes
        SQLType = stIns
      end
      object Label1: TLabel
        Left = 108
        Top = 28
        Width = 30
        Height = 13
        Caption = '..........'
      end
      object dmkEdPercent: TdmkEdit
        Left = 8
        Top = 24
        Width = 97
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 722
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 674
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 626
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 437
        Height = 32
        Caption = 'Arrecada'#231#227'o - Sele'#231#227'o de Unidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 437
        Height = 32
        Caption = 'Arrecada'#231#227'o - Sele'#231#227'o de Unidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 437
        Height = 32
        Caption = 'Arrecada'#231#227'o - Sele'#231#227'o de Unidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 380
    Width = 722
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 718
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 722
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 718
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 574
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 214
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Todos'
        TabOrder = 1
        OnClick = BtTodosClick
        NumGlyphs = 2
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 338
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        TabOrder = 2
        OnClick = BtNenhumClick
        NumGlyphs = 2
      end
    end
  end
  object QrUnidCond: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM unidcond'
      'ORDER BY Andar, Unidade')
    Left = 364
    Top = 148
    object QrUnidCondApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrUnidCondUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrUnidCondProprie: TWideStringField
      FieldName = 'Proprie'
      Size = 100
    end
    object QrUnidCondSelecio: TSmallintField
      FieldName = 'Selecio'
    end
  end
  object DsUnidCond: TDataSource
    DataSet = QrUnidCond
    Left = 392
    Top = 148
  end
  object QrSolo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT aru.Conta'
      'FROM arrebau aru'
      'LEFT JOIN condimov cni ON cni.Conta=aru.Apto'
      'WHERE aru.Controle=:P0'
      'AND Apto=:P1')
    Left = 136
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSoloConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
  end
end
