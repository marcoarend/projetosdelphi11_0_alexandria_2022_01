unit VisitasCli2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  Variants, mySQLDbTables, UMySQLModule, DmkDAC_PF, dmkDBGrid, UnDmkProcFunc,
  frxClass, frxDBSet, UnDmkEnums;

type
  TFmVisitasCli2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    LaTitulo1C: TLabel;
    PnPesquisa: TPanel;
    dmkCkCond: TdmkLabel;
    EdCond: TdmkEditCB;
    CBCond: TdmkDBLookupComboBox;
    dmkCkPropriet: TdmkLabel;
    EdPropriet: TdmkEditCB;
    CBPropriet: TdmkDBLookupComboBox;
    dmkCKUH: TdmkLabel;
    EdUH: TdmkEditCB;
    CBUH: TdmkDBLookupComboBox;
    dmkLabel1: TdmkLabel;
    CBGrupo: TdmkDBLookupComboBox;
    EdGrupo: TdmkEditCB;
    BtPesquisa: TBitBtn;
    GroupBox2: TGroupBox;
    CkTipoUHs: TCheckBox;
    CkTipoSindico: TCheckBox;
    CkTipoGrupo: TCheckBox;
    QrCond: TmySQLQuery;
    QrCondCodigo: TIntegerField;
    QrCondCOND: TWideStringField;
    DsCond: TDataSource;
    QrPropriet: TmySQLQuery;
    a: TWideStringField;
    QrProprietPropriet: TIntegerField;
    DsPropriet: TDataSource;
    QrUHs: TmySQLQuery;
    QrUHsUnidade: TWideStringField;
    QrUHsConta: TIntegerField;
    DsUHs: TDataSource;
    QrCondGri: TmySQLQuery;
    IntegerField1: TIntegerField;
    DsCondGri: TDataSource;
    QrCondGriNome: TWideStringField;
    RGTipoPesq: TRadioGroup;
    CkIniDta: TCheckBox;
    TPIniDta: TDateTimePicker;
    TPFimDta: TDateTimePicker;
    CkFimDta: TCheckBox;
    DBGPesq: TdmkDBGrid;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqNivelLogin: TWideStringField;
    QrPesqControle: TIntegerField;
    QrPesqUsuario: TIntegerField;
    QrPesqTipo: TIntegerField;
    QrPesqCondGrupo: TWideStringField;
    QrPesqUH: TWideStringField;
    QrPesqPropriet: TWideStringField;
    QrPesqDataHora: TDateTimeField;
    QrPesqIP: TWideStringField;
    QrPesqACESSOS: TFloatField;
    frxWEB_VISIT_001_001: TfrxReport;
    frxDsPesq: TfrxDBDataset;
    QrPesqCondGrupoCod: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCondChange(Sender: TObject);
    procedure EdProprietChange(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPropriet;
    procedure ReopenUHs;
  public
    { Public declarations }
  end;

  var
  FmVisitasCli2: TFmVisitasCli2;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmVisitasCli2.BtImprimeClick(Sender: TObject);
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
    MyObjects.frxMostra(frxWEB_VISIT_001_001, 'Controle de acessos WEB');
end;

procedure TFmVisitasCli2.BtPesquisaClick(Sender: TObject);
var
  Cond, Propriet, CondImov, CondGri, TipoPesq: Integer;
  SQL, SQLUHs, SQLSindico, SQLGrupo: WideString;
begin
  if (not CkTipoUHs.Checked) and (not CkTipoSindico.Checked) and
    (not CkTipoGrupo.Checked)
  then
    Exit;
  //
  Cond     := EdCond.ValueVariant;
  Propriet := EdPropriet.ValueVariant;
  CondImov := EdUH.ValueVariant;
  CondGri  := EdGrupo.ValueVariant;
  TipoPesq := RGTipoPesq.ItemIndex;
  //
  DBGPesq.Columns[6].Visible := RGTipoPesq.ItemIndex = 1; 
  //
  if CkTipoUHs.Checked then //Pesquisa nas senhas do tipo UHs
  begin
    SQLUHs :=
      'SELECT "Cond�mino" NivelLogin, vis.Controle, vis.Usuario, vis.Tipo, ' +
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CondGrupo, imv.Unidade UH, ' +
      'IF(enb.Tipo=0, enb.RazaoSocial, enb.Nome) Propriet, vis.IP, con.Codigo CondGrupoCod, ';
    //
    if TipoPesq = 0 then //�ltimo acesso
      SQLUHs := SQLUHs + 'MAX(vis.DataHora) DataHora, COUNT(vis.Controle) + 0.000 ACESSOS '
    else
      SQLUHs := SQLUHs + 'vis.DataHora, 1 + 0.000 ACESSOS ';
    //
    SQLUHs := SQLUHs +
      'FROM visitascli vis ' +
      'LEFT JOIN users usr ON usr.User_ID = vis.Usuario ' +
      'LEFT JOIN condimov imv ON imv.Conta = usr.CodigoEsp ' +
      'LEFT JOIN cond con ON con.Codigo = imv.Codigo ' +
      'LEFT JOIN entidades ent ON ent.Codigo = con.Cliente ' +
      'LEFT JOIN entidades enb ON enb.Codigo = imv.Propriet ' +
      'WHERE vis.Tipo = 1 ';
    if Cond <> 0 then
      SQLUHs := SQLUHs + 'AND CodCliEsp=' + Geral.FF0(Cond) + ' ';
    if Propriet <> 0 then
      SQLUHs := SQLUHs + 'AND CodigoEnt=' + Geral.FF0(Propriet) + ' ';
    if CondImov <> 0 then
      SQLUHs := SQLUHs + 'AND CodigoEsp=' + Geral.FF0(CondImov) + ' ';
    SQLUHs := SQLUHs + dmkPF.SQL_Periodo(
      'AND DATE_FORMAT(vis.DataHora,"%Y-%m-%d") ', TPIniDta.Date, TPFimDta.Date,
      CkIniDta.Checked, CkFimDta.Checked);
    if TipoPesq = 0 then
      SQLUHs := SQLUHs + ' GROUP BY vis.Usuario, vis.Tipo ';
  end;
  if CkTipoSindico.Checked then //Pesquisa nas senhas do tipo S�ndico
  begin
    SQLSindico :=
      'SELECT "S�ndico" NivelLogin, vis.Controle, vis.Usuario, vis.Tipo, ' +
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CondGrupo, "" UH, "" Propriet, ' +
      'vis.IP, wcl.CodCliEsp CondGrupoCod, ';
    //
    if TipoPesq = 0 then //�ltimo acesso
      SQLSindico := SQLSindico + 'MAX(vis.DataHora) DataHora, COUNT(vis.Controle) + 0.000 ACESSOS '
    else
      SQLSindico := SQLSindico + 'vis.DataHora, 1 + 0.000 ACESSOS ';
    //
    SQLSindico := SQLSindico +
      'FROM visitascli vis ' +
      'LEFT JOIN wclients wcl ON wcl.User_ID = vis.Usuario ' +
      'LEFT JOIN entidades ent ON ent.Codigo = wcl.CodCliEnt ' +
      'WHERE vis.Tipo = 7 ';
    if Cond <> 0 then
      SQLSindico := SQLSindico + 'AND wcl.CodCliEsp=' + Geral.FF0(Cond) + ' ';
    SQLSindico := SQLSindico + dmkPF.SQL_Periodo(
      'AND DATE_FORMAT(vis.DataHora,"%Y-%m-%d") ', TPIniDta.Date, TPFimDta.Date,
      CkIniDta.Checked, CkFimDta.Checked);
    if TipoPesq = 0 then
      SQLSindico := SQLSindico + ' GROUP BY vis.Usuario, vis.Tipo ';
  end;
  if CkTipoGrupo.Checked then //Pesquisa nas senhas do tipo Grupo
  begin
    SQLGrupo :=
      'SELECT "Grupo de UHs" NivelLogin, vis.Controle, vis.Usuario, vis.Tipo, ' +
      'cog.Nome CondGrupo, "" UH, "" Propriet, vis.IP, cog.Codigo CondGrupoCod, ';
    //
    if TipoPesq = 0 then //�ltimo acesso
      SQLGrupo := SQLGrupo + 'MAX(vis.DataHora) DataHora, COUNT(vis.Controle) + 0.000 ACESSOS '
    else
      SQLGrupo := SQLGrupo + 'vis.DataHora, 1 + 0.000 ACESSOS ';
    //
    SQLGrupo := SQLGrupo + 
      'FROM visitascli vis ' +
      'LEFT JOIN condgri cog ON cog.Codigo= vis.Usuario ' +
      'LEFT JOIN condgriimv cgi ON cgi.Codigo = cog.Codigo ' +
      'WHERE vis.Tipo = 3 ';
    if CondGri <> 0 then
      SQLGrupo := SQLGrupo + 'AND cog.Codigo=' + Geral.FF0(CondGri) + ' ';
    if Cond <> 0 then
      SQLGrupo := SQLGrupo + 'AND cgi.Cond=' + Geral.FF0(Cond) + ' ';
    if Propriet <> 0 then
      SQLGrupo := SQLGrupo + 'AND cgi.Propr=' + Geral.FF0(Propriet) + '';
    if CondImov <> 0 then
      SQLGrupo := SQLGrupo + 'AND cgi.Apto=' + Geral.FF0(CondImov) + ' ';
    SQLGrupo := SQLGrupo + dmkPF.SQL_Periodo(
      'AND DATE_FORMAT(vis.DataHora,"%Y-%m-%d") ', TPIniDta.Date, TPFimDta.Date,
      CkIniDta.Checked, CkFimDta.Checked);
    if TipoPesq = 0 then
      SQLGrupo := SQLGrupo + ' GROUP BY vis.Usuario, vis.Tipo '
    else
      SQLGrupo := SQLGrupo + ' GROUP BY vis.Controle ';
  end;
  SQL := '';
  //
  if SQLUHs <> '' then
    SQL := SQL + SQLUHs;
  //
  if (SQL <> '') and ((SQLSindico <> '') or (SQLGrupo <> '')) then
    SQL := SQL + ' UNION ';
  //
  if SQLSindico <> '' then
    SQL := SQL + SQLSindico;
  //
  if (SQL <> '') and (SQLGrupo <> '') then
    SQL := SQL + ' UNION ';
  //
  if SQLGrupo <> '' then
    SQL := SQL + SQLGrupo;
  //
  SQL := SQL + ' ORDER BY NivelLogin, CondGrupo, UH, DataHora';
  try
    Screen.Cursor := crHourGlass;
    //
    QrPesq.SQL.Text := SQL;
    //
    UMyMod.AbreQuery(QrPesq, Dmod.MyDBn);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVisitasCli2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVisitasCli2.EdCondChange(Sender: TObject);
begin
  if EdCond.ValueVariant <> 0 then
  begin
    ReopenPropriet;
    ReopenUHs;
  end;
end;

procedure TFmVisitasCli2.EdProprietChange(Sender: TObject);
begin
  ReopenUHs;
end;

procedure TFmVisitasCli2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVisitasCli2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CkTipoUHs.Checked       := True;
  CkTipoSindico.Checked   := True;
  CkTipoGrupo.Checked     := True;
  EdCond.ValueVariant     := 0;
  CBCond.KeyValue         := Null;
  EdPropriet.ValueVariant := 0;
  CBPropriet.KeyValue     := Null;
  EdUH.ValueVariant       := 0;
  CBUH.KeyValue           := Null;
  EdGrupo.ValueVariant    := 0;
  CBGrupo.KeyValue        := Null;
  CkIniDta.Checked        := True;
  TPIniDta.Date           := Date;
  CkFimDta.Checked        := True;
  TPFimDta.Date           := Date;
  //
  UMyMod.AbreQuery(QrCond, Dmod.MyDB);
  UMyMod.AbreQuery(QrCondGri, Dmod.MyDB);
end;

procedure TFmVisitasCli2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVisitasCli2.ReopenUHs;
var
  Cond, Propriet: Integer;
  SQL1, SQL2: String;
begin
  Cond     := EdCond.ValueVariant;
  Propriet := EdPropriet.ValueVariant;
  //
  if Cond <> 0 then
    SQL1 := 'AND imv.Codigo=' + Geral.FF0(Cond);
  if Propriet <> 0 then
    SQL2 := 'AND imv.Propriet=' + Geral.FF0(Propriet);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrUHs, Dmod.MyDB, [
  'SELECT imv.Conta, imv.Unidade',
  'FROM condimov imv',
  'WHERE imv.Conta <> 0',
  SQL1,
  SQL2,
  'ORDER BY imv.Unidade', 
  '']);  
end;

procedure TFmVisitasCli2.ReopenPropriet;
var
  Cond: Integer;
begin
  Cond := EdCond.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPropriet, Dmod.MyDB, [
  'SELECT imv.Propriet, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) PROP',
  'FROM condimov imv',
  'LEFT JOIN entidades ent ON ent.Codigo = imv.Propriet',
  'WHERE imv.Codigo=' + Geral.FF0(Cond),
  'GROUP BY imv.Propriet',
  'ORDER BY PROP',
  '']);
end;

end.
