unit CondGerDelArre;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkCheckGroup, mySQLDbTables,
  dmkImage, UnDmkEnums;

type
  TFmCondGerDelArre = class(TForm)
    Panel1: TPanel;
    CGQuais: TdmkCheckGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CGQuaisClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCondGerDelArre: TFmCondGerDelArre;

implementation

uses CondGer, dmkGeral, Module, ModuleCond, UnInternalConsts, UnMyObjects,
  UnFinanceiro;

{$R *.DFM}

procedure TFmCondGerDelArre.BtOKClick(Sender: TObject);
var
  ComoAdds: String;
  QrLocAriA, QrLocLct: TmySQLQuery;
begin
  if Geral.MB_Pergunta('Confirma a EXCLUS�O de TODOS itens de ' +
  'arrecada��o (que n�o possuem bloquetos gerados) deste per�odo deste condom�nio conforme o(s) "Tipo(s) de ' +
  'Inclus�o" Definido(s)?') = ID_YES then
  begin
    ComoAdds := '-1000';
    if Geral.IntInConjunto(1, CGQuais.Value) then
      ComoAdds := ComoAdds + ',0';
    if Geral.IntInConjunto(2, CGQuais.Value) then
      ComoAdds := ComoAdds + ',1';
    if Geral.IntInConjunto(4, CGQuais.Value) then
      ComoAdds := ComoAdds + ',2';
    if Geral.IntInConjunto(8, CGQuais.Value) then
      ComoAdds := ComoAdds + ',3';
    if Geral.IntInConjunto(16, CGQuais.Value) then
      ComoAdds := ComoAdds + ',4';
    //
    QrLocAriA := TmySQLQuery.Create(Dmod);
    QrLocAriA.Database := Dmod.MyDB;
    QrLocAriA.SQL.Clear;
    QrLocAriA.SQL.Add('SELECT Controle, Lancto ');
    QrLocAriA.SQL.Add('FROM ' + DmCond.FTabAriA + ' ');
    QrLocAriA.SQL.Add('WHERE Codigo=:P0');
    QrLocAriA.SQL.Add('AND ComoAdd IN (' + ComoAdds + ')' );
    QrLocAriA.SQL.Add('AND Lancto = 0');
    QrLocAriA.SQL.Add('AND Boleto = 0');
    QrLocAriA.Params[0].AsInteger := FmCondGer.QrPrevCodigo.Value;
    QrLocAriA.Open;
    if QrLocAriA.RecordCount > 0 then
    begin
      while not QrLocAriA.Eof do
      begin
        if QrLocAriA.FieldByName('Lancto').AsInteger <> 0 then
        begin
          QrLocLct := TmySQLQuery.Create(Dmod);
          QrLocLct.Database := Dmod.MyDB;
          QrLocLct.SQL.Clear;
          QrLocLct.SQL.Add('SELECT Data, Tipo, Carteira, Sub ');
          QrLocLct.SQL.Add('FROM ' + DmCond.FTabLctA + ' ');
          QrLocLct.SQL.Add('WHERE Controle=:P0');
          QrLocLct.SQL.Add('AND FatID = 600'); //600 FATID da Arrecada��o
          QrLocLct.Params[0].AsInteger := QrLocAriA.FieldByName('Lancto').AsInteger;
          QrLocLct.Open;
          if QrLocLct.RecordCount > 0 then
          begin
            UFinanceiro.ExcluiLct_Unico(DmCond.FTabLctA, Dmod.MyDB,
              QrLocLct.FieldByName('Data').AsDateTime,
              QrLocLct.FieldByName('Tipo').AsInteger,
              QrLocLct.FieldByName('Carteira').AsInteger,
              QrLocAriA.FieldByName('Lancto').AsInteger,
              QrLocLct.FieldByName('Sub').AsInteger,
              CO_MOTVDEL_306_EXCLUILCTARRECADACAO, False, False);
          end;
        end;
        //
        QrLocAriA.Next;
      end;
    end;
    //
    //N�o excluir se possuir bloquetos gerados
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + DmCond.FTabAriA );
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrUpd.SQL.Add('AND ComoAdd IN (' + ComoAdds + ')' );
    Dmod.QrUpd.SQL.Add('AND Lancto=0 ');
    Dmod.QrUpd.Params[0].AsInteger := FmCondGer.QrPrevCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    FmCondGer.CalculaTotalARIEReabreArreEARI(0, 0, 0);
    Close;
  end;
end;

procedure TFmCondGerDelArre.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerDelArre.CGQuaisClick(Sender: TObject);
begin
  BtOK.Enabled := CGQuais.Value > 0;
end;

procedure TFmCondGerDelArre.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerDelArre.FormCreate(Sender: TObject);
const
  Texto = 'Ser�o exclu�das apenas arrecada��es que n�o possuem bloquetos gerados!';
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
end;

procedure TFmCondGerDelArre.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
