object FmAtencoes: TFmAtencoes
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-030 :: Informa'#231#245'es que Precisam de Aten'#231#227'o'
  ClientHeight = 488
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 316
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1008
      Height = 316
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Quita'#231#227'o de Parcelamentos '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 100
          Width = 1000
          Height = 8
          Cursor = crVSplit
          Align = alTop
          ExplicitTop = 125
          ExplicitWidth = 820
        end
        object DBGErrRepa: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 100
          Align = alTop
          DataSource = DmAtencoes.DsErrRepa
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECOND'
              Title.Caption = 'Condom'#237'nio'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROP'
              Title.Caption = 'Propriet'#225'rio'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compensado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'Origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MES'
              Title.Caption = 'Compet.'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Reparcel'
              Visible = True
            end>
        end
        object DBGErrRepaIts: TdmkDBGrid
          Left = 0
          Top = 108
          Width = 1000
          Height = 132
          Align = alClient
          Color = clWindow
          DataSource = DmAtencoes.DsErrRepaIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object Panel5: TPanel
          Left = 0
          Top = 240
          Width = 1000
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object BtRatifQuit: TBitBtn
            Tag = 10116
            Left = 4
            Top = 4
            Width = 130
            Height = 40
            Caption = 'Ratifica quita'#231#227'o'
            TabOrder = 0
            OnClick = BtRatifQuitClick
          end
          object DBGErrRepaSum: TDBGrid
            Left = 530
            Top = 0
            Width = 470
            Height = 48
            Align = alRight
            DataSource = DmAtencoes.DsErrRepaSum
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
          end
          object BtLocPag: TBitBtn
            Tag = 22
            Left = 134
            Top = 4
            Width = 130
            Height = 40
            Caption = 'Localiza parcel.'
            TabOrder = 2
            OnClick = BtLocPagClick
          end
          object BtQuitaOrig: TBitBtn
            Tag = 174
            Left = 264
            Top = 4
            Width = 130
            Height = 40
            Caption = 'Quita originais'
            TabOrder = 3
            Visible = False
            OnClick = BtQuitaOrigClick
          end
          object BtExclLancto: TBitBtn
            Tag = 12
            Left = 394
            Top = 4
            Width = 130
            Height = 40
            Caption = 'Exclui lan'#231'to'
            TabOrder = 4
            Visible = False
            OnClick = BtExclLanctoClick
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Lan'#231'amento sem m'#234's de compet'#234'ncia'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGLct: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 240
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Bloqueto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatID'
              Title.Caption = 'Tipo'
              Width = 29
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'ID Pagto'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Protocolo'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SERIE_CHEQUE'
              Title.Caption = 'S'#233'rie/Docum.'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IndiPag'
              Title.Caption = 'iPg'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'to'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Cliente / Fornecedor'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UH'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECEI'
              Title.Caption = 'Propriet'#225'rio'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MultaVal'
              Title.Caption = 'Multa paga'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MoraVal'
              Title.Caption = 'Juros pagos'
              Width = 56
              Visible = True
            end>
          Color = clWindow
          DataSource = DmAtencoes.DsSemMez
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          FieldsCalcToOrder.Strings = (
            'NOMESIT=Sit,Vencimento'
            'SERIE_CHEQUE=SerieCH,Documento'
            'COMPENSADO_TXT=Compensado'
            'MENSAL=Mez')
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Bloqueto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatID'
              Title.Caption = 'Tipo'
              Width = 29
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'ID Pagto'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Protocolo'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SERIE_CHEQUE'
              Title.Caption = 'S'#233'rie/Docum.'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IndiPag'
              Title.Caption = 'iPg'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'to'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Cliente / Fornecedor'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UH'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECEI'
              Title.Caption = 'Propriet'#225'rio'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MultaVal'
              Title.Caption = 'Multa paga'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MoraVal'
              Title.Caption = 'Juros pagos'
              Width = 56
              Visible = True
            end>
        end
        object Panel3: TPanel
          Left = 0
          Top = 240
          Width = 1000
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object BitBtn1: TBitBtn
            Tag = 14
            Left = 332
            Top = 4
            Width = 338
            Height = 40
            Caption = '&Colocar m'#234's de compet'#234'ncia onde n'#227'o tem'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BitBtn1Click
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 364
    Width = 1008
    Height = 54
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 37
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PBAtz1: TProgressBar
        Left = 0
        Top = 20
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 418
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 467
        Height = 32
        Caption = 'Informa'#231#245'es que Precisam de Aten'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 467
        Height = 32
        Caption = 'Informa'#231#245'es que Precisam de Aten'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 467
        Height = 32
        Caption = 'Informa'#231#245'es que Precisam de Aten'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
end
