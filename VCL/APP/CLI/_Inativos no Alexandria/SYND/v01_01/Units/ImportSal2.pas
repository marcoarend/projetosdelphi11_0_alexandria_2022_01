unit ImportSal2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkEdit, dmkGeral,
  ComCtrls, DBCtrls, dmkDBLookupComboBox, dmkEditCB, dmkEditDateTimePicker, DB,
  mySQLDbTables, Variants, dmkImage, UnDmkEnums;

type
  TFmImportSal2 = class(TForm)
    Panel1: TPanel;
    PnConfig: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    EdPath: TEdit;
    SpeedButton1: TSpeedButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrFunci: TmySQLQuery;
    QrFunciCodigo: TIntegerField;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    DsCarteiras: TDataSource;
    QrImportSalCfgCab: TmySQLQuery;
    DsImportSalCfgCab: TDataSource;
    DsLct: TDataSource;
    DBGrid1: TDBGrid;
    QrEnti1: TmySQLQuery;
    QrEnti1Codigo: TIntegerField;
    QrFunciChapa: TWideStringField;
    QrEnti2: TmySQLQuery;
    QrEnti2Codigo: TIntegerField;
    TbLct: TmySQLTable;
    TbLctData: TDateField;
    TbLctTipo: TSmallintField;
    TbLctCarteira: TIntegerField;
    TbLctGenero: TIntegerField;
    TbLctDescricao: TWideStringField;
    TbLctNotaFiscal: TIntegerField;
    TbLctDebito: TFloatField;
    TbLctCredito: TFloatField;
    TbLctSerieCH: TWideStringField;
    TbLctDocumento: TFloatField;
    TbLctVencimento: TDateField;
    TbLctFatID: TIntegerField;
    TbLctFatID_Sub: TIntegerField;
    TbLctFatNum: TFloatField;
    TbLctFatParcela: TIntegerField;
    TbLctMez: TIntegerField;
    TbLctFornecedor: TIntegerField;
    TbLctCliente: TIntegerField;
    TbLctCliInt: TIntegerField;
    TbLctForneceI: TIntegerField;
    TbLctMoraDia: TFloatField;
    TbLctMulta: TFloatField;
    TbLctDataDoc: TDateField;
    TbLctVendedor: TIntegerField;
    TbLctAccount: TIntegerField;
    TbLctDuplicata: TWideStringField;
    TbLctDepto: TIntegerField;
    TbLctUnidade: TIntegerField;
    TbLctSit: TIntegerField;
    Memo1: TMemo;
    TbLctNO_CONTA: TWideStringField;
    QrCarteirasTipoDoc: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel6: TPanel;
    BtPreGera: TBitBtn;
    BtConfirma: TBitBtn;
    BitBtn2: TBitBtn;
    QrImportSalCfgCabCodigo: TIntegerField;
    QrImportSalCfgCabNome: TWideStringField;
    QrImportSalCfgCabEmpCNPJIni: TIntegerField;
    QrImportSalCfgCabEmpCNPJTam: TIntegerField;
    QrImportSalCfgCabFunCPFIni: TIntegerField;
    QrImportSalCfgCabFunCPFTam: TIntegerField;
    QrImportSalCfgCabFunCodIni: TIntegerField;
    QrImportSalCfgCabFunCodTam: TIntegerField;
    QrImportSalCfgCabFunNomIni: TIntegerField;
    QrImportSalCfgCabFunNomTam: TIntegerField;
    QrImportSalCfgCabCtaCodIni: TIntegerField;
    QrImportSalCfgCabCtaCodTam: TIntegerField;
    QrImportSalCfgCabMesRefIni: TIntegerField;
    QrImportSalCfgCabMesRefTam: TIntegerField;
    QrImportSalCfgCabAnoRefIni: TIntegerField;
    QrImportSalCfgCabAnoRefTam: TIntegerField;
    QrImportSalCfgCabValLiqIni: TIntegerField;
    QrImportSalCfgCabValLiqTam: TIntegerField;
    QrImportSalCfgCabSeparador: TWideStringField;
    QrImportSalCfgCabUsaSepara: TSmallintField;
    QrImportSalCfgCabFloatSep: TWideStringField;
    Panel3: TPanel;
    LaEmpresa: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    LaMes: TLabel;
    LaVencimento: TLabel;
    Label14: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    EdConfig: TdmkEditCB;
    CBConfig: TdmkDBLookupComboBox;
    TPData: TdmkEditDateTimePicker;
    EdMes: TdmkEdit;
    TPVencimento: TdmkEditDateTimePicker;
    EdDescriA: TdmkEdit;
    EdDescriB: TdmkEdit;
    GroupBox6: TGroupBox;
    LaDoc: TLabel;
    EdSerieCH1: TdmkEdit;
    EdDocumento1: TdmkEdit;
    CkAtribCHManu: TCheckBox;
    QrImportSalCfgCta: TmySQLQuery;
    QrImportSalCfgCtaNO_CTA: TWideStringField;
    QrImportSalCfgCtaCodigo: TIntegerField;
    QrImportSalCfgCtaControle: TIntegerField;
    QrImportSalCfgCtaGenero: TIntegerField;
    QrImportSalCfgCtaReferencia: TWideStringField;
    QrImportSalCfgCtaLk: TIntegerField;
    QrImportSalCfgCtaDataCad: TDateField;
    QrImportSalCfgCtaDataAlt: TDateField;
    QrImportSalCfgCtaUserCad: TIntegerField;
    QrImportSalCfgCtaUserAlt: TIntegerField;
    QrImportSalCfgCtaAlterWeb: TSmallintField;
    QrImportSalCfgCtaAtivo: TSmallintField;
    DsImportSalCfgCta: TDataSource;
    SbConfig: TSpeedButton;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    TbLctMENSAL: TWideStringField;
    TbLctAtivo: TIntegerField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPreGeraClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TPVencimentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEmpresaChange(Sender: TObject);
    procedure TbLctBeforeClose(DataSet: TDataSet);
    procedure TbLctAfterOpen(DataSet: TDataSet);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdPathChange(Sender: TObject);
    procedure EdConfigChange(Sender: TObject);
    procedure CkAtribCHManuClick(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure QrImportSalCfgCabAfterScroll(DataSet: TDataSet);
    procedure QrImportSalCfgCabBeforeClose(DataSet: TDataSet);
    procedure SbConfigClick(Sender: TObject);
    procedure TbLctCalcFields(DataSet: TDataSet);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    { Private declarations }
    Flct: String;
    FSetando: Boolean;
    //FEmpCNP, FFunCPF, FFunCod, FFunNom, FCtaCod, FMesRef, FAnoRef, FValLiq: Integer;
    //
    procedure FechaPreGera();
    procedure VerificaTipoDoc();
    function  ObtemValorDeCampo(const Linha: Integer; const Campo:
              String; var Valor: String): Boolean;
    function  ObtemValorDeTexto(ValLiq, Separador: String): Double;
    function  ObtemGeneroDeCtaCod(const CtaCod: String; var Genero: Integer):
              Boolean;
    procedure ReopenImportSalCfgCta(Controle: Integer);
    function  AdicionaAoArray(var ArrStr: TStringList; const Campo,
              PosIni: Integer): Boolean;
    procedure MsgAbortaPeloCNPJ();
    procedure SetaTodos(Marca: Boolean);
  public
    { Public declarations }
    FTabLcta: String;
  end;

  var
  FmImportSal2: TFmImportSal2;

implementation

uses UnMyObjects, ModuleGeral, UCreate, UMySQLModule, Module, MyDBCheck,
FPFunciNew, UnInternalConsts, UnFinanceiro, DmkDAC_PF, Principal, MyVCLSkin;

{$R *.DFM}

const
  FTamFldVal = 11;
  FTamFldQtd = 03;
  //
  FEmpCNP = 1;
  FFunCPF = 2;
  FFunCod = 3;
  FFunNom = 4;
  FCtaCod = 5;
  FMesRef = 6;
  FAnoRef = 7;
  FValLiq = 8;
  //FLixArq = 9;

function TFmImportSal2.AdicionaAoArray(var ArrStr: TStringList; const Campo,
PosIni: Integer): Boolean;
begin
  if PosIni(*QrImportSalCfgCabEmpCNPJIni.Value*) <> 0 then
  begin
    ArrStr.Add(Geral.FFN(PosIni, FTamFldVal) + '-' + Geral.FFN(Campo, FTamFldQtd));
    Result := True;
  end else
    Result := False;
end;

procedure TFmImportSal2.BtTodosClick(Sender: TObject);
begin
  SetaTodos(True);
end;

procedure TFmImportSal2.BtNenhumClick(Sender: TObject);
begin
  SetaTodos(False);
end;

procedure TFmImportSal2.BtConfirmaClick(Sender: TObject);
var
  N, I: Integer;
  CHManual: Boolean;
begin
  Screen.Cursor   := crHourGlass;
  DBGrid1.Enabled := False;
  CHManual        := CkAtribCHManu.Checked; 
  //
  if not CkAtribCHManu.Checked then
  begin
    N := 0;
    for I := 0 to Memo1.Lines.Count - 1 do
    begin
      if Length(Memo1.Lines[I]) > 3 then
        N := N + 1;
    end;
    if UMyMod.ObtemQtdeCHTaloes(EdCarteira.ValueVariant) < N then
      if Geral.MensagemBox(
      'N�o existem folhas de cheque suficientes para todas linhas.' +
      #13#10 + 'Deseja continuar assim mesmo?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
        Exit;
  end;
  //
  TbLct.First;
  while not TbLct.Eof do
  begin
    if TbLctAtivo.Value = 1 then
    begin
      UFinanceiro.LancamentoDefaultVARS;
      //
      FLAN_Data          := Geral.FDT(TbLctData.Value, 1);
      FLAN_Vencimento    := Geral.FDT(TbLctVencimento.Value, 1);
      FLAN_DataCad       := Geral.FDT(Date, 1);
      FLAN_Mez           := TbLctMez.Value;
      FLAN_Descricao     := TbLctDescricao.Value;
      //FLAN_Compensado    := '';
      FLAN_Duplicata     := TbLctDuplicata.Value;
      //FLAN_Doc2          := '';
      if CHManual then
      begin
        FLAN_SerieCH   := TbLctSerieCH.Value;
        FLAN_Documento := TbLctDocumento.Value;
      end else
      begin
        try
          Screen.Cursor := crHourGlass;
          //
          if not UMyMod.ObtemProximoCHTalao(EdCarteira.ValueVariant,
            EdSerieCH1, EdDocumento1) then
          begin
            Geral.MB_Aviso('Inclus�o abortada!' + #13#10 +
              'Motivo: Falha ao obter numera��o do cheque!');
            Exit;
          end else
          begin
            FLAN_SerieCH   := EdSerieCH1.Text;
            FLAN_Documento := EdDocumento1.ValueVariant;
          end;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
      FLAN_Tipo          := TbLctTipo.Value;
      FLAN_Carteira      := TbLctCarteira.Value;
      FLAN_Credito       := TbLctCredito.Value;
      FLAN_Debito        := TbLctDebito.Value;
      FLAN_Genero        := TbLctGenero.Value;
      //FLAN_SerieNF       := '';
      FLAN_NotaFiscal    := TbLctNotaFiscal.Value;
      FLAN_Sit           := TbLctSit.Value;
      //FLAN_ID_Pgto       := 0;
      //FLAN_Cartao        := 0;
      //FLAN_Linha         := 0;
      FLAN_Fornecedor    := TbLctFornecedor.Value;
      FLAN_Cliente       := TbLctCliente.Value;
      FLAN_MoraDia       := TbLctMoraDia.Value;
      FLAN_Multa         := TbLctMulta.Value;
      FLAN_UserCad       := VAR_USUARIO;
      FLAN_DataDoc       := Geral.FDT(TbLctDataDoc.Value, 1);
      FLAN_Vendedor      := TbLctVendedor.Value;
      FLAN_Account       := TbLctAccount.Value;
      //FLAN_ICMS_P        := 0;
      //FLAN_ICMS_V        := 0;
      FLAN_CliInt        := TbLctCliInt.Value;
      FLAN_Depto         := TbLctDepto.Value;
      //FLAN_DescoPor      := 0;
      FLAN_ForneceI      := TbLctForneceI.Value;
      //FLAN_DescoVal      := 0;
      //FLAN_NFVal         := 0;
      FLAN_Unidade       := TbLctUnidade.Value;
      //FLAN_Qtde          := 0;
      FLAN_FatID         := TbLctFatID.Value;
      FLAN_FatID_Sub     := TbLctFatID_Sub.Value;
      FLAN_FatNum        := TbLctFatNum.Value;
      FLAN_FatParcela    := TbLctFatParcela.Value;
      //
      //FLAN_MultaVal      := 0;
      //FLAN_MoraVal       := 0;
      //FLAN_CtrlIni       := 0;
      //FLAN_Nivel         := 0;
      //FLAN_CNAB_Sit      := 0;
      //FLAN_TipoCH        := 0;
      //FLAN_Atrelado      := 0;
      //FLAN_SubPgto1      := 0;
      //FLAN_MultiPgto     := 0;
      //
      //FLAN_Emitente      := '';
      //FLAN_CNPJCPF       := '';
      //FLAN_Banco         := 0;
      //FLAN_Agencia       := '';
      //FLAN_ContaCorrente := '';

      case UFinanceiro.NaoDuplicarLancto(stIns, TbLctDocumento.Value,
      TbLctDocumento.Value > 0, FLAN_SerieCH, FLAN_NotaFiscal, FLAN_NotaFiscal <> 0,
      FLAN_Credito, FLAN_Debito, FLAN_Genero, FLAN_Mez, True, FLAN_CliInt, True,
      FLAN_Carteira, FLAN_Cliente, FLAN_Fornecedor, LaAviso1, LaAviso2, FTabLctA, '', '') of
       -1:
        begin
          Screen.Cursor := crDefault;
          Exit; // sai do loop
        end;
        0: ; // n�o faz nada
        1: // insere item
        begin
          FLAN_Sub           := 0;
          FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            FTabLctA, LAN_CTOS, 'Controle');
          if UFinanceiro.InsereLancamento(FTabLctA) then ;
        end;
      end;
    end;
    TbLct.Next;
  end;
  Screen.Cursor   := crDefault;
  DBGrid1.Enabled := True;
  //Close;
end;

procedure TFmImportSal2.BtPreGeraClick(Sender: TObject);
  procedure InsereItem(const CliInt, Fornecedor, Genero: Integer;
  const Nome, Valor, Mes, Ano: String; var Item: Integer);
  var
    Data, Vencimento, DataDoc, Descricao, ChSer, M, A: String;
    Sit, Tipo, Carteira, Mez, CHNum: Integer;
    Debito: Double;
  begin
    Data       := Geral.FDT(TPData.Date, 1);
    Vencimento := Geral.FDT(TPVencimento.Date, 1);
    DataDoc    := Geral.FDT(Date, 1);
    Descricao  := Trim(EdDescriA.Text + ' ' + Nome + ' ' + EdDescriB.Text);
    Tipo       := QrCarteirasTipo.Value;
    Carteira   := QrCarteirasCodigo.Value;
    //Genero     := QrContasCodigo.Value;
    Debito     := Geral.DMV(Trim(Valor));
    try
      if Length(EdMes.Text) = 7 then
      begin
        Mez        := Geral.IMV(EdMes.Text[6] + EdMes.Text[7] + EdMes.Text[1] + EdMes.Text[2]);
      end else
      begin
        M := Mes;
        if Length(M) = 1 then
          M := '0' + M;
        //
        A := Copy(Ano, Length(Ano)- 1);
        if Length(A) = 1 then
          A := '0' + A;
        //
        Mez := Geral.IMV(A + M);
      end;
    except
      Geral.MB_Erro('N�o foi poss�vel definir a compet�ncia de:' + #13#10 +
      'M�s: ' + Mes + #13#10 + 'Ano: ' + Ano);
    end;
    //
    if CkAtribCHManu.Checked then
    begin
      CHSer := EdSerieCH1.Text;
      CHNum := EdDocumento1.ValueVariant;
      if (CHNum > 0) and (Debito <> 0) then
      begin
        Item  := Item + 1;
        CHNum := CHNum + Item -1;
      end else
      begin
        CHNum := 0;
        //CHSer := '';
      end;
    end else
    begin
      (* Obter na hora da inclus�o para n�o pular nenhum n�mero
      FSetando := True;
      try
        if not UMyMod.ObtemProximoCHTalao(EdCarteira.ValueVariant,
        EdSerieCH1, EdDocumento1) then
          Exit
        else begin
          CHNum := EdDocumento1.ValueVariant;
          CHSer := EdSerieCH1.Text;
        end;
      finally
        FSetando := False;
      end;
      *)
      ChSer    := '';
      CHNum    := 0;
      FSetando := False;
    end;
    //
    case Tipo of
      0: Sit := 3;
      1: Sit := 3;
      2: Sit := 0;
      else Sit := -1;
    end;
    if Debito = 0 then
      Sit := 4;
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, Flct, False, [
      'Data', 'Tipo', 'Carteira',
      'Genero', 'Descricao', 'Debito',
      'Vencimento', 'Mez', 'Fornecedor',
      'CliInt', 'DataDoc', 'SerieCH',
      'Documento', 'Sit', 'Ativo'
    ], [], [
      Data, Tipo, Carteira,
      Genero, Descricao, Debito,
      Vencimento, Mez, Fornecedor,
      CliInt, DataDoc, CHSer,
      CHNum, Sit, 1], [], False);
  end;
var
  Txt, Str, Lin, Nome, CPX, CPF, Valor, Chapa: String;
  N, I, Ini, Tam, CliInt, Filial, Item: Integer;
  //
  FloatSep, Separador, EmpCNPJ, FunCod, FunNom, FunCPF, CtaCod, MesRef, AnoRef,
  ValLiq, LixArq, Mes, Ano: String;
  Genero, UsaSepara, Nao: Integer;
  Continua: Boolean;
  //
  ArrStr: TStringList;
  ArrInt: array of Integer;
  K: Integer;
begin
  PnConfig.Enabled := False;
  ArrStr := TStringList.Create;
  ArrStr.Sorted := False;
  try
  Nao := 0;
  if not CkAtribCHManu.Checked then
  begin
    N := 0;
    for I := 0 to Memo1.Lines.Count - 1 do
    begin
      if Length(Memo1.Lines[I]) > 3 then
        N := N + 1;
    end;
    if UMyMod.ObtemQtdeCHTaloes(EdCarteira.ValueVariant) < N then
      if Geral.MensagemBox(
      'N�o existem folhas de cheque suficientes para todas linhas.' +
      #13#10 + 'Deseja continuar assim mesmo?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
        Exit;
  end;
  Screen.Cursor := crHourGlass;
  Item := 0;
  Flct := UCriar.RecriaTempTableNovo(ntrttLct, DmodG.QrUpdPID1, False);
  Separador := QrImportSalCfgCabSeparador.Value;
  UsaSepara := QrImportSalCfgCabUsaSepara.Value;
  FloatSep  := QrImportSalCfgCabFloatSep.Value;
  //
  if UsaSepara = 1 then
  begin
    if not AdicionaAoArray(ArrStr, FEmpCNP, QrImportSalCfgCabEmpCNPJIni.Value) then
    begin
      MsgAbortaPeloCNPJ();
      Screen.Cursor := crDefault;
      Exit;
    end;
    AdicionaAoArray(ArrStr, FFunCPF, QrImportSalCfgCabFunCPFIni.Value);
    AdicionaAoArray(ArrStr, FFunCod, QrImportSalCfgCabFunCodIni.Value);
    AdicionaAoArray(ArrStr, FFunNom, QrImportSalCfgCabFunNomIni.Value);
    AdicionaAoArray(ArrStr, FCtaCod, QrImportSalCfgCabCtaCodIni.Value);
    AdicionaAoArray(ArrStr, FMesRef, QrImportSalCfgCabMesRefIni.Value);
    AdicionaAoArray(ArrStr, FAnoRef, QrImportSalCfgCabAnoRefIni.Value);
    AdicionaAoArray(ArrStr, FValLiq, QrImportSalCfgCabValLiqIni.Value);
    ArrStr.Sort;
    SetLength(ArrInt, ArrStr.Count);
    for I := 0 to ArrStr.Count - 1 do
    begin
      Str := Copy(ArrStr[I], FTamFldVal + 1 + FTamFldQtd);
      ArrInt[I] := Geral.IMV(Str);
    end;
  end;
  if Memo1.Lines.Count > 0 then
  begin
    for I := 0 to Memo1.Lines.Count - 1 do
    begin
      Continua := True;
      if UsaSepara = 1 then
      begin
        EmpCNPJ := '';
        FunCPF := '';
        FunCod := '';
        FunNom := '';
        CtaCod := '';
        MesRef := '';
        AnoRef := '';
        ValLiq := '';
        LixArq := '';
        //Continua := False;
        K := 0;
        Lin := Memo1.Lines[I];
        while Length(Lin) > 0 do
        begin
          if Geral.SeparaPrimeiraOcorrenciaDeTexto(Separador, Lin, Str, Lin) then
          begin
            case ArrInt[K] of
              FEmpCNP: EmpCNPJ := Str;
              FFunCPF: FunCPF := Str;
              FFunCod: FunCod := Str;
              FFunNom: FunNom := Str;
              FCtaCod: CtaCod := Str;
              FMesRef: MesRef := Str;
              FAnoRef: AnoRef := Str;
              FValLiq: ValLiq := Str;
              //FLixArq: LixArq := Str;
              else begin
                if Trim(Str) <> '' then
                begin
                  Geral.MB_Erro('N�o foi poss�vel definir a vari�vel pai "' +
                  Geral.FF0(I) + '" de resultado (valor):' + #13#10 + Str);
                  Screen.Cursor := crDefault;
                  Exit;
                end;
              end;
            end;
            K := K + 1;
          end;
        end;
        if Trim(LixArq) <> '' then
          Geral.MB_Erro(
          'AVISE A DERMATEK! Localizado o seguinte texto extra na linha ' +
          Geral.FF0(I) + #13#10 + LixArq);
      end else
      begin
        if ObtemValorDeCampo(I, 'EmpCNPJ', EmpCNPJ) then
        begin
          if Geral.SoNumero_TT(EmpCNPJ) <> Geral.SoNumero_TT(DmodG.QrEmpresasCNPJ_CPF.Value) then
          begin
            Nao := Nao + 1;
            Continua := False;
          end;
        end;
        if Continua then
        begin
          ObtemValorDeCampo(I, 'FunCPF', FunCPF);
          ObtemValorDeCampo(I, 'FunCod', FunCod);
          ObtemValorDeCampo(I, 'FunNom', FunNom);
          ObtemValorDeCampo(I, 'CtaCod', CtaCod);
          ObtemValorDeCampo(I, 'MesRef', MesRef);
          ObtemValorDeCampo(I, 'AnoRef', AnoRef);
          ObtemValorDeCampo(I, 'ValLiq', ValLiq);
        end;
      end;
      if Continua then
      begin
        //
        Chapa := Trim(FunCod);
        Nome  := Trim(FunNom);
        CPX   := Trim(FunCPF);
        Valor := Trim(ValLiq);
        Valor := Geral.FFT(ObtemValorDeTexto(Valor, FloatSep), 2, siPositivo);
        Mes   := Trim(MesRef);
        Ano   := Trim(AnoRef);
        if not ObtemGeneroDeCtaCod(CtaCod, Genero) then
        begin
          Screen.Cursor := crDefault;
          exit;
        end;
        //
        CPF := Geral.SoNumero_TT(CPX);
        if Length(CPF) = 11 then
        begin
          CliInt     := DModG.QrEmpresasCodigo.Value;
          Filial     := DModG.QrEmpresasFilial.Value;
          Txt := '';
          // Pesquisar se o funcion�rio tem cadastro com a Chapa!
          UnDmkDAC_PF.AbreMySQLQuery0(QrFunci, Dmod.MyDB, [
          'SELECT fpf.Codigo, fpf.Chapa ',
          'FROM fpfunci fpf ',
          'LEFT JOIN entidades ent ON ent.Codigo=fpf.Codigo ',
          'WHERE fpf.Empresa=' + FormatFloat('0', CliInt),
          Geral.ATS_IF(Length(Geral.SoNumero1a9_TT(CPF)) > 0,
          ['AND (ent.CPF="' + CPF + '" OR CPF="" OR CPF="00000000000") ']),
          'AND fpf.Chapa="' + Chapa + '"',
          '']);
          //
          case QrFunci.RecordCount of
            0:
            begin
                Txt := 'N�o foi localizado nenhum cadastro para:';
                Txt := Txt + #13#10 + 'Nome: ' + Nome + #13#10 + 'CPF: ' +
                       CPX + #13#10 + 'Chapa: ' + Chapa;
                //if QrFunci.RecordCount > 1 then
                Txt := Txt + #13#10 + 'Deseja cadastr�-lo agora?';
                case Geral.MensagemBox(Txt, 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) of
                  ID_YES:
                  begin
                    if DBCheck.CriaFm(TFmFpFunciNew, FmFpFunciNew, afmoNegarComAviso) then
                    begin
                      FmFpFunciNew.EdEmpresa.ValueVariant := Filial;
                      FmFpFunciNew.CBEmpresa.KeyValue     := Filial;
                      //
                      if Length(Geral.SoNumero1a9_TT(CPF)) > 0 then
                      begin
                        QrEnti1.Close;
                        QrEnti1.Params[0].AsString := CPF;
                        UnDmkDAC_PF.AbreQuery(QrEnti1, Dmod.MyDB);
                        if QrEnti1.RecordCount > 0 then
                        begin
                          FmFpFunciNew.EdCodigo.ValueVariant := QrEnti1Codigo.Value;
                          FmFpFunciNew.CBCodigo.KeyValue     := QrEnti1Codigo.Value;
                        end;
                      end;
                      if FmFpFunciNew.EdCodigo.ValueVariant = 0 then
                      begin
                        QrEnti2.Close;
                        QrEnti2.Params[0].AsString := Nome;
                        UnDmkDAC_PF.AbreQuery(QrEnti2, Dmod.MyDB);
                        if QrEnti2.RecordCount > 0 then
                        begin
                          FmFpFunciNew.EdCodigo.ValueVariant := QrEnti2Codigo.Value;
                          FmFpFunciNew.CBCodigo.KeyValue     := QrEnti2Codigo.Value;
                        end else Geral.MensagemBox('N�o foi poss�vel localizar a ' +
                        'entidade "' + Nome + '" pelo CPF ' + CPX + '.' + #13#10 +
                        'Antes de incluir um novo cadastro nas entidades ' +
                        'verifique se j� n�o h� um cadastro para este ' +
                        'funcion�rio sem a informa��o do CPF!', 'Aviso',
                        MB_OK+MB_ICONWARNING);
                      end;
                      FmFpFunciNew.EdChapa.Text := Chapa;
                      FmFpFunciNew.EdNome.Text  := Nome;
                      FmFpFunciNew.ShowModal;
                      FmFpFunciNew.Destroy;
                      if VAR_CADASTRO > 0 then
                        InsereItem(CliInt, VAR_CADASTRO, Genero, Nome, Valor,
                          Mes, Ano, Item);
                    end;
                  end;
                  ID_CANCEL:
                  begin
                    Screen.Cursor := crDefault;
                    Exit;
                  end;
                end;
                Txt := '';
              //end;
            end;
            1: InsereItem(CliInt, QrFunciCodigo.Value, Genero, Nome, Valor,
              Mes, Ano, Item);
            else Txt := 'Foram localizados ' + IntToStr(QrFunci.RecordCount) +
            ' cadastros para:';
          end;
          if Trim(Txt) <> '' then
          begin
            Txt := Txt + #13#10 + 'CPF: ' + CPF + #13#10 + 'Chapa: ' + Chapa;
            if QrFunci.RecordCount > 1 then Txt := Txt + #13#10 + 'Nenhum ser� considerado!';
            Geral.MensagemBox(Txt, 'Aviso', MB_OK+MB_ICONWARNING);
          end;
        end else
          Geral.MB_Aviso('A linha n� ' + Geral.FF0(I) +
          ' n�o possui um CPF v�lido e n�o ser� carregada!');
      end;
    end;
    TbLct.Close;
    UnDmkDAC_PF.AbreTable(TbLct, DModG.MyPID_DB);
    PageControl1.ActivePageIndex := 1;
    if Nao > 0 then
      Geral.MB_Aviso(Geral.FF0(Nao) + ' linhas n�o pertenciam � empresa selecionada!');
    Screen.Cursor := crDefault;
  end;
  finally
    TbLctSerieCH.ReadOnly   := not CkAtribCHManu.Checked;
    TbLctDocumento.ReadOnly := not CkAtribCHManu.Checked;
    //
    PnConfig.Enabled := True;
    ArrStr.Free;
  end;
end;

procedure TFmImportSal2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImportSal2.CkAtribCHManuClick(Sender: TObject);
begin
  LaDoc.Enabled        := CkAtribCHManu.Checked;
  EdSerieCH1.Enabled   := CkAtribCHManu.Checked;
  EdDocumento1.Enabled := CkAtribCHManu.Checked;
end;

procedure TFmImportSal2.DBGrid1CellClick(Column: TColumn);
begin
  if (DBGrid1.SelectedField.Name = 'TbLctAtivo') then
  begin
    TbLct.Edit;
    if TbLctAtivo.Value = 0 then
      TbLctAtivo.Value := 1
    else
      TbLctAtivo.Value := 0;
    TbLct.Post;
  end;
end;

procedure TFmImportSal2.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbLctAtivo.Value);
end;

procedure TFmImportSal2.EdCarteiraChange(Sender: TObject);
begin
  FechaPreGera();
  VerificaTipoDoc();
end;

procedure TFmImportSal2.EdEmpresaChange(Sender: TObject);
begin
  FechaPreGera();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarteiras, Dmod.MyDB, [
  'SELECT Codigo, Nome, Fatura, Fechamento, Prazo,  ',
  'Tipo, ExigeNumCheque, ForneceI, TipoDoc ',
  'FROM carteiras  ',
  'WHERE ForneceI=' + Geral.FF0(DModG.QrEmpresasCodigo.Value),
  'ORDER BY Nome ',
  '']);
  //
  if not QrCarteiras.Locate('Codigo', EdCarteira.ValueVariant, []) then
  begin
    EdCarteira.ValueVariant := 0;
    CBCarteira.KeyValue     := Null;
  end;
end;

procedure TFmImportSal2.EdConfigChange(Sender: TObject);
begin
  FechaPreGera();
end;

procedure TFmImportSal2.EdPathChange(Sender: TObject);
begin
  FechaPreGera();
  VerificaTipoDoc();
end;

function TFmImportSal2.ObtemValorDeCampo(const Linha: Integer; const Campo:
String; var Valor: String): Boolean;
var
  Ini, Tam: Integer;
begin
  Ini := QrImportSalCfgCab.FieldByName(Campo + 'Ini').AsInteger;
  Tam := QrImportSalCfgCab.FieldByName(Campo + 'Tam').AsInteger;
  Result := (Ini >= 1) and (Tam >= 1);
  if Result then
    Valor := Copy(Memo1.Lines[Linha], Ini, Tam)
  else
    Valor := '';
end;

function TFmImportSal2.ObtemGeneroDeCtaCod(const CtaCod: String; var Genero:
Integer): Boolean;
var
  Referencia: String;
begin
  Referencia := Trim(CtaCod);
  if QrImportSalCfgCta.Locate('Referencia', Trim(Referencia), [loCaseInsensitive]) then
  begin
    Genero := QrImportSalCfgCtaGenero.Value;
    Result := True;
  end else
  begin
    Genero := 0;
    Result := False;
    Geral.MB_Aviso('Gera��o de pr�-lan�amento abortada!' + #13#10 +
    'N�o foi poss�vel definir a conta do plano de contas a partir da refer�ncia:' +
    '"' + Referencia + '"');
  end;
end;

function TFmImportSal2.ObtemValorDeTexto(ValLiq, Separador: String): Double;
var
  Lixo, Txt: String;
  I: Integer;
begin
  if Separador = '.' then
    Lixo := ','
  else
  if Separador = ',' then
    Lixo := '.'
  else
    Lixo := '';
  //
  if Lixo <> '' then
  begin
    Txt := Geral.Substitui(ValLiq, Lixo, '');
    //
    Txt := Geral.Substitui(ValLiq, '.', ',');
  end else
    Txt := ValLiq;
  //
  Result := Geral.DMV(Txt);
end;

procedure TFmImportSal2.QrImportSalCfgCabAfterScroll(DataSet: TDataSet);
begin
  ReopenImportSalCfgCta(0);
end;

procedure TFmImportSal2.QrImportSalCfgCabBeforeClose(DataSet: TDataSet);
begin
  QrImportSalCfgCta.Close;
end;

procedure TFmImportSal2.TbLctAfterOpen(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  Enab := (TbLct.State <> dsInactive) and (TbLct.RecordCount > 0);
  //
  BtConfirma.Enabled := Enab;
  BtTodos.Enabled    := Enab;
  BtNenhum.Enabled   := Enab;
end;

procedure TFmImportSal2.TbLctBeforeClose(DataSet: TDataSet);
begin
  BtConfirma.Enabled := False;
  BtTodos.Enabled    := False;
  BtNenhum.Enabled   := False;
end;

procedure TFmImportSal2.TbLctCalcFields(DataSet: TDataSet);
var
  Mes, Ano: Word;
begin
  TbLctMENSAL.Value := MLAGeral.MesEAnoDeAnoMes(TbLctMez.Value, Mes, Ano);
end;

procedure TFmImportSal2.TPVencimentoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F6 then
    TPVencimento.Date := TPdata.Date;
end;

procedure TFmImportSal2.VerificaTipoDoc;
var
  Habilita: Boolean;
begin
  Habilita := QrCarteirasTipoDoc.Value <> 1;
  EdSerieCH1.Enabled        := Habilita;
  EdDocumento1.Enabled      := Habilita;
  LaDoc.Enabled             := Habilita;
  CkAtribCHManu.Enabled     := Habilita;
  if (Habilita = False) and (FSetando = False) then
  begin
    EdSerieCH1.ValueVariant   := '';
    EdDocumento1.ValueVariant := 0;
  end;
end;

procedure TFmImportSal2.FechaPreGera();
var
  Habilita: Boolean;
begin
  TbLct.Close;
  //
  Habilita := (Memo1.Lines.Count > 1) and
              (EdEmpresa.ValueVariant <> 0) and
              (EdConfig.ValueVariant <> 0) and
              (EdCarteira.ValueVariant <> 0);
  BtPreGera.Enabled := Habilita;
end;

procedure TFmImportSal2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmImportSal2.FormCreate(Sender: TObject);
const
  Texto = 'M�s*: Deixe vazio quando informado no arquivo. / Caso o campo Atrib. CH manual esteja desmarcado o aplicativo ir� inserir o n�mero do cheque somente ap�s confirmar!';
begin
  ImgTipo.SQLType := stIns;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FSetando := False;
  PageControl1.ActivePageIndex := 0;
  TPData.Date := Date;
  TPVencimento.Date := Date;
  UnDmkDAC_PF.AbreQuery(QrImportSalCfgCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  TbLct.Database := DModG.MyPID_DB;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
end;

procedure TFmImportSal2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImportSal2.MsgAbortaPeloCNPJ();
begin
  Geral.MB_Aviso('Gera��o de pr�-lan�amento abortada!' + #13#10 +
  'N�o foi poss�vel definir o CNPJ da empresa!');
end;

procedure TFmImportSal2.SbConfigClick(Sender: TObject);
begin
  FmPrincipal.MostraFormImportSalCfgCab(EdConfig.ValueVariant);
  ReopenImportSalCfgCta(0);
end;

procedure TFmImportSal2.SetaTodos(Marca: Boolean);
var
  Codigo: Integer;
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + Flct + ' SET Ativo=:P0');
  DModG.QrUpdPID1.Params[0].AsInteger := MLAGeral.BoolToInt(Marca);
  DModG.QrUpdPID1.ExecSQL;
  //
  TbLct.Close;
  TbLct.Open;
end;

procedure TFmImportSal2.SpeedButton1Click(Sender: TObject);
var
  Arquivo: String;
  Ini, Tam: Integer;
begin
  if MyObjects.FileOpenDialog(Self, '', '', Label1.Caption, '', [], Arquivo) then
  begin
    if FileExists(Arquivo) then
    begin
      EdPath.Text := Arquivo;
      Memo1.Lines.LoadFromFile(Arquivo);
      //
      FechaPreGera();
    end;
  end;
end;

procedure TFmImportSal2.ReopenImportSalCfgCta(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrImportSalCfgCta, Dmod.MyDB, [
  'SELECT cta.Nome NO_CTA, scc.*  ',
  'FROM importsalcfgcta scc ',
  'LEFT JOIN contas cta ON cta.Codigo=scc.Genero  ',
  'WHERE scc.Codigo=' + Geral.FF0(QrImportSalCfgCabCodigo.Value),
  'ORDER BY Referencia ',
  '']);
  //
  QrImportSalCfgCta.Locate('Controle', Controle, []);
end;

end.
