object FmPrevImp: TFmPrevImp
  Left = 339
  Top = 185
  Caption = 'IMP-PROVI-001 :: Impress'#227'o de provis'#245'es por per'#237'odo'
  ClientHeight = 562
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 434
        Height = 32
        Caption = 'Impress'#227'o de provis'#245'es por per'#237'odo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 434
        Height = 32
        Caption = 'Impress'#227'o de provis'#245'es por per'#237'odo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 434
        Height = 32
        Caption = 'Impress'#227'o de provis'#245'es por per'#237'odo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 624
    Height = 400
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 400
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 624
        Height = 400
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 594
          Top = 15
          Height = 366
          Align = alRight
          Constraints.MaxWidth = 3
          Constraints.MinWidth = 3
          ExplicitLeft = 749
          ExplicitTop = 52
          ExplicitHeight = 440
        end
        object GroupBox2: TGroupBox
          Left = 12
          Top = 12
          Width = 193
          Height = 61
          Caption = ' Per'#237'odo de pesquisa: '
          TabOrder = 0
          object LaAno: TLabel
            Left = 116
            Top = 16
            Width = 22
            Height = 13
            Caption = 'Ano:'
          end
          object LaMes: TLabel
            Left = 8
            Top = 16
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object CBMes: TComboBox
            Left = 8
            Top = 32
            Width = 105
            Height = 21
            Color = clWhite
            DropDownCount = 12
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Text = 'CBMes'
          end
          object CBAno: TComboBox
            Left = 116
            Top = 32
            Width = 69
            Height = 21
            Color = clWhite
            DropDownCount = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Text = 'CBAno'
          end
        end
        object DockTabSet1: TTabSet
          Left = 597
          Top = 15
          Width = 25
          Height = 366
          Align = alRight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ShrinkToFit = True
          Style = tsModernPopout
          TabPosition = tpLeft
          AutoSelect = True
          DockSite = False
          OnDockDrop = DockTabSet1DockDrop
          OnTabAdded = DockTabSet1TabAdded
          OnTabRemoved = DockTabSet1TabRemoved
        end
        object PB1: TProgressBar
          Left = 2
          Top = 381
          Width = 620
          Height = 17
          Align = alBottom
          TabOrder = 2
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 448
    Width = 624
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 492
    Width = 624
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 478
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtReexibe: TBitBtn
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Reexibe'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtReexibeClick
      end
    end
  end
  object CkMorto: TdmkCheckBox
    Left = 20
    Top = 127
    Width = 177
    Height = 17
    Caption = 'Incluir arquivo morto na pesquisa'
    TabOrder = 4
    UpdType = utYes
    ValCheck = #0
    ValUncheck = #0
    OldValor = #0
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10
    OnTimer = Timer1Timer
    Left = 132
    Top = 224
  end
  object frxProv_e_UHs: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Page2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Page1.Visible := <VARF_IMPRIME_PAGE1>;                        ' +
        '                      '
      
        '  Page2.Visible := <VARF_IMPRIME_PAGE2>;                        ' +
        '                      '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxProv_e_UHsGetValue
    Left = 160
    Top = 224
    Datasets = <
      item
        DataSet = frxDsCond
        DataSetName = 'frxDsCond'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProv
        DataSetName = 'frxDsProv'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 109.606370000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object frxShapeView1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'PROVIS'#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCond."NOMECOND"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 68.031540000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Top = 94.488250000000000000
          Width = 508.346456690000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 508.346822830000000000
          Top = 94.488250000000000000
          Width = 171.968503940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 49.133858270000000000
        Top = 400.630180000000000000
        Width = 680.315400000000000000
        object Memo120: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MD_PRV: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        DataSet = frxDsProv
        DataSetName = 'frxDsProv'
        RowCount = 0
        object MePrv_Cta_Txt: TfrxMemoView
          Left = 1.889727170000000000
          Width = 508.346456690000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProv."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePrv_Cta_Val: TfrxMemoView
          Left = 510.236550000000000000
          Width = 171.968503940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProv."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PRV: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897642680000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsProv."NomeSubGrupo"'
        object MePrv_Sgr_Txt: TfrxMemoView
          Left = 1.889727170000000000
          Width = 680.314953310000000000
          Height = 18.897642680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProv."NomeSubGrupo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF_PRV: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.897637795275590000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        object MePrv_Sgr_Val: TfrxMemoView
          Left = 510.236550000000000000
          Width = 171.968503940000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPROV."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FO_PRV: TfrxFooter
        FillType = ftBrush
        Height = 28.346459130000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        object MePrv_Sum_Val: TfrxMemoView
          Left = 510.236550000000000000
          Top = 1.889763780000000000
          Width = 171.968503940000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14803425
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPROV."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePrv_Sum_Txt: TfrxMemoView
          Left = 1.889727170000000000
          Top = 1.889763780000000000
          Width = 508.346456690000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL PROVIS'#195'O')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      OnBeforePrint = 'Page2OnBeforePrint'
      object PageHeader2: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo4: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo5: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'PROVIS'#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCond."NOMECLI"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Top = 68.031540000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPrev."PERIODO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 49.133858270000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        object Memo10: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 56.692940240000000000
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        RowCount = 0
        object Memo11: TfrxMemoView
          Top = 22.677180000000000000
          Width = 510.236220470000000000
          Height = 18.897637800000000000
          DataField = 'Texto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsArreBaA."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 510.236550000000000000
          Top = 22.677180000000000000
          Width = 170.078740157480000000
          Height = 18.897637800000000000
          DataField = 'Valor'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArreBaA."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Top = 41.574830000000000000
          Width = 71.811021180000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 71.811070000000000000
          Top = 41.574830000000000000
          Width = 302.362204720000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Propriet'#225'rio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 582.047620000000000000
          Top = 41.574830000000000000
          Width = 98.267718980000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 374.173470000000000000
          Top = 41.574830000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 449.764070000000000000
          Top = 41.574830000000000000
          Width = 132.283501180000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Top = 3.779530000000000000
          Width = 510.236220470000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Arrecada'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 510.236550000000000000
          Top = 3.779530000000000000
          Width = 170.078740157480000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        RowCount = 0
        object Memo25: TfrxMemoView
          Width = 71.811021180000000000
          Height = 15.118110240000000000
          DataField = 'Unidade'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsArreBaAUni."Unidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 71.811070000000000000
          Width = 302.362204720000000000
          Height = 15.118110240000000000
          DataField = 'NomePropriet'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsArreBaAUni."NomePropriet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 582.047620000000000000
          Width = 98.267718980000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsArreBaAUni."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 374.173470000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Cotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArreBaAUni."Cotas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 449.764070000000000000
          Width = 132.283501180000000000
          Height = 15.118110240000000000
          DataField = 'TextoCota'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsArreBaAUni."TextoCota"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrPRI: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 40
    Top = 256
    object QrPRIConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPRITexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrPRIValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPRISubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrPRINOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrPRINOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrPRICodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPRIControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPRILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPRIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPRIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPRIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPRIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPRIPrevBaI: TIntegerField
      FieldName = 'PrevBaI'
      Required = True
    end
  end
  object QrEmpresas: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT CodCliInt, CodEnti'
      'FROM _empresas_'
      'WHERE Ativo = 1'
      'ORDER BY CodCliInt')
    Left = 68
    Top = 256
    object QrEmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object QrEmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
  end
  object QrCond: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT pri.CliInt,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECOND'
      'FROM _pri_ pri'
      'LEFT JOIN syndic.enticliint cli ON cli.CodCliInt = pri.CliInt'
      'LEFT JOIN syndic.entidades ent ON ent.Codigo = cli.CodEnti'
      'GROUP BY pri.CliInt'
      'ORDER BY pri.CliInt')
    Left = 132
    Top = 320
    object QrCondCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrCondNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
  end
  object frxDsCond: TfrxDBDataset
    UserName = 'frxDsCond'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CliInt=CliInt'
      'NOMECOND=NOMECOND')
    DataSet = QrCond
    BCDToCurrency = False
    Left = 160
    Top = 320
  end
  object QrProv: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT pit.*'
      'FROM _pri_ pit'
      'ORDER BY NomeSubGrupo, NomeConta')
    Left = 188
    Top = 320
    object QrProvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProvConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrProvValor: TFloatField
      FieldName = 'Valor'
    end
    object QrProvPrevBaC: TIntegerField
      FieldName = 'PrevBaC'
    end
    object QrProvPrevBaI: TIntegerField
      FieldName = 'PrevBaI'
    end
    object QrProvTexto: TWideStringField
      FieldName = 'Texto'
      Size = 50
    end
    object QrProvEmitVal: TFloatField
      FieldName = 'EmitVal'
    end
    object QrProvEmitSit: TIntegerField
      FieldName = 'EmitSit'
    end
    object QrProvNomeConta: TWideStringField
      FieldName = 'NomeConta'
      Size = 50
    end
    object QrProvNomeSubGrupo: TWideStringField
      FieldName = 'NomeSubGrupo'
      Size = 50
    end
    object QrProvPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrProvCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object frxDsProv: TfrxDBDataset
    UserName = 'frxDsProv'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'Valor=Valor'
      'PrevBaC=PrevBaC'
      'PrevBaI=PrevBaI'
      'Texto=Texto'
      'EmitVal=EmitVal'
      'EmitSit=EmitSit'
      'NomeConta=NomeConta'
      'NomeSubGrupo=NomeSubGrupo'
      'Periodo=Periodo'
      'CliInt=CliInt')
    DataSet = QrProv
    BCDToCurrency = False
    Left = 216
    Top = 320
  end
  object frxProvisoes: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39911.688433344920000000
    ReportOptions.LastChange = 39911.688433344920000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 188
    Top = 225
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
end
