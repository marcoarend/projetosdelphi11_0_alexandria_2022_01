object FmCondGerArreUni: TFmCondGerArreUni
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-024 :: Item de Arrecada'#231#227'o'
  ClientHeight = 369
  ClientWidth = 443
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 443
    Height = 207
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label2: TLabel
      Left = 12
      Top = 98
      Width = 31
      Height = 13
      Caption = 'Conta:'
    end
    object Label13: TLabel
      Left = 12
      Top = 142
      Width = 114
      Height = 13
      Caption = 'Descri'#231#227'o [F4 : F5 : F6]:'
    end
    object LaDeb: TLabel
      Left = 340
      Top = 142
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object Label1: TLabel
      Left = 12
      Top = 12
      Width = 43
      Height = 13
      Caption = 'Unidade:'
    end
    object Label4: TLabel
      Left = 12
      Top = 55
      Width = 113
      Height = 13
      Caption = 'Configura'#231#227'o do boleto:'
    end
    object EdConta: TdmkEditCB
      Left = 12
      Top = 114
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBConta
      IgnoraDBLookupComboBox = False
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 68
      Top = 114
      Width = 361
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 5
      dmkEditCB = EdConta
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDescricao: TdmkEdit
      Left = 12
      Top = 158
      Width = 325
      Height = 21
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnKeyDown = EdDescricaoKeyDown
    end
    object EdValor: TdmkEdit
      Left = 340
      Top = 158
      Width = 88
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdValorExit
    end
    object CkContinuar: TCheckBox
      Left = 16
      Top = 186
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 8
    end
    object EdApto: TdmkEditCB
      Left = 12
      Top = 28
      Width = 53
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBApto
      IgnoraDBLookupComboBox = False
    end
    object CBApto: TdmkDBLookupComboBox
      Left = 68
      Top = 28
      Width = 361
      Height = 21
      Color = clWhite
      KeyField = 'Conta'
      ListField = 'Unidade'
      ListSource = DsAptos
      TabOrder = 1
      dmkEditCB = EdApto
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCNAB_Cfg: TdmkEditCB
      Left = 12
      Top = 72
      Width = 53
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCNAB_Cfg
      IgnoraDBLookupComboBox = False
    end
    object CBCNAB_Cfg: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 361
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCNAB_Cfg
      TabOrder = 3
      dmkEditCB = EdCNAB_Cfg
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 443
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 395
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 347
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 252
        Height = 32
        Caption = 'Item de Arrecada'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 252
        Height = 32
        Caption = 'Item de Arrecada'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 252
        Height = 32
        Caption = 'Item de Arrecada'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 255
    Width = 443
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 439
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 299
    Width = 443
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 439
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 308
        Top = 0
        Width = 131
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 132
        Top = 3
        Width = 173
        Height = 40
        Caption = '&Ver dados atr'#225's'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BitBtn1Click
      end
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.*, sg.Nome NOMESUBGRUPO ,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1'
      'WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMEEMPRESA'
      'FROM contas co, Subgrupos sg,'
      'Grupos gr, Conjuntos cj, Entidades cl'
      'WHERE sg.Codigo=co.Subgrupo'
      'AND gr.Codigo=sg.Grupo'
      'AND cj.Codigo=gr.Conjunto'
      'AND cl.Codigo=co.Empresa'
      'AND co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 228
    Top = 64
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 256
    Top = 64
  end
  object QrAptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imv.Andar, imv.Unidade, imv.Conta, imv.Propriet,'
      'con.Codigo, con.Cliente'
      'FROM condimov imv'
      'LEFT JOIN cond con ON con.Codigo = imv.Codigo'
      'WHERE imv.Codigo=:P0'
      'ORDER BY imv.Andar, imv.Unidade'
      '')
    Left = 340
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrAptosPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrAptosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrAptosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsAptos: TDataSource
    DataSet = QrAptos
    Left = 368
    Top = 64
  end
  object QrCNAB_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM CNAB_Cfg'
      'WHERE Cedente=:P0')
    Left = 284
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 312
    Top = 64
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 204
    Top = 184
  end
end
