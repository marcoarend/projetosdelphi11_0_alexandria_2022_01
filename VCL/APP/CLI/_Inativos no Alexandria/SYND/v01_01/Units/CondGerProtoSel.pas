unit CondGerProtoSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Grids, DBGrids,
  dmkGeral, ComCtrls, dmkEditDateTimePicker, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkImage, UnDmkEnums;

type
  TFmCondGerProtoSel = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    CBTarefa: TdmkDBLookupComboBox;
    DsProtocolos: TDataSource;
    EdTarefa: TdmkEditCB;
    QrProtocolos: TmySQLQuery;
    Panel4: TPanel;
    Panel6: TPanel;
    CkAbertos: TCheckBox;
    DBGrid1: TDBGrid;
    QrProtocoPak: TmySQLQuery;
    DsProtocoPak: TDataSource;
    QrProtocolosCodigo: TIntegerField;
    QrProtocolosNome: TWideStringField;
    QrProtocoPakCodigo: TIntegerField;
    QrProtocoPakControle: TIntegerField;
    QrProtocoPakDataI: TDateField;
    QrProtocoPakDataL: TDateField;
    QrProtocoPakDataF: TDateField;
    QrProtocoPakLk: TIntegerField;
    QrProtocoPakDataCad: TDateField;
    QrProtocoPakDataAlt: TDateField;
    QrProtocoPakUserCad: TIntegerField;
    QrProtocoPakUserAlt: TIntegerField;
    QrProtocoPakAlterWeb: TSmallintField;
    QrProtocoPakAtivo: TSmallintField;
    QrProtocoPakMez: TIntegerField;
    QrProtocoPakMES: TWideStringField;
    QrProtocoPakDATAF_TXT: TWideStringField;
    CkRetorna: TCheckBox;
    QrProtocolosDef_Retorn: TIntegerField;
    GroupBox1: TGroupBox;
    TPLimiteSai: TdmkEditDateTimePicker;
    LaLimiteSai: TLabel;
    TPLimiteRem: TdmkEditDateTimePicker;
    LaLimiteRem: TLabel;
    TPLimiteRet: TdmkEditDateTimePicker;
    LaLimiteRet: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    BitBtn1: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrProtocolosTipo: TIntegerField;
    QrProtocolosDef_Client: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdTarefaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CkAbertosClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CkRetornaClick(Sender: TObject);
  private
    { Private declarations }
    FLimiteSai, FLimiteRem, FLimiteRet: String;
    procedure ReopenProtocoPak(Controle: Integer);
    //procedure GeraProtoclo_CondGerProto();
    //procedure GeraProtoclo_Lancamentos();
  public
    FQuais        : TSelType;
    FNomeQrSrc    : String;
    FQrLcI        : TmySQLQuery;
    FQrSource     : TmySQLQuery;
    FDBGrid       : TDBGrid;
    FTipoProtocolo: Integer;
    FTabLctA      : String;
    FLctProto     : String;
    FFormOrigem   : String;
    FAgruparDoc   : Boolean;
    FAskTodos     : Boolean;
    { Public declarations }
    procedure ReopenProtocolos(TipoProtocolo, Tarefa, Def_Client: Integer);
  end;

var
  FmCondGerProtoSel: TFmCondGerProtoSel;

implementation

uses UnFinanceiro, Module, Entidades, CondGerProto, ProtocoPak, MyDBCheck,
UnMyObjects, DmkDAC_PF, MyListas;

{$R *.DFM}

procedure TFmCondGerProtoSel.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerProtoSel.BtConfirmaClick(Sender: TObject);
  procedure GeraAtual(var SemEmeio, SemQuery: Integer);

    procedure GeraProtocolo_DocumLanctos(var SemQuery: Integer);
    var
      Protocolo, Controle, Sub, Conta, CliInt, Cliente, Fornece, ITENS, Lancto,
      Tipo, Carteira: Integer;
      Credito, Debito, Documento: Double;
      SerieCH: String;
      Data, Vencto: TDateTime;
    begin
      ITENS     := 0; 
      Protocolo := -1;
      Controle  := 0;
      CliInt    := 0;
      Cliente   := 0;
      Fornece   := 0;
      Credito   := 0;
      Debito    := 0;
      Documento := 0;
      Vencto    := 0;
      Sub       := 0;
      Tipo      := 0;
      Carteira  := 0;
      Data      := 0;
      if FQrSource <> nil then
      begin
        Protocolo := 0;
        Tipo      := FQrSource.FieldByName('Tipo'      ).AsInteger;
        Carteira  := FQrSource.FieldByName('Carteira'  ).AsInteger;
        Controle  := FQrSource.FieldByName('Controle'  ).AsInteger;
        Sub       := FQrSource.FieldByName('Sub'       ).AsInteger;
        CliInt    := FQrSource.FieldByName('CliInt'    ).AsInteger;
        Cliente   := FQrSource.FieldByName('Cliente'   ).AsInteger;
        Fornece   := FQrSource.FieldByName('Fornecedor').AsInteger;
        //
        Documento := FQrSource.FieldByName('Documento' ).AsFloat;
        Credito   := FQrSource.FieldByName('Credito'   ).AsFloat;
        Debito    := FQrSource.FieldByName('Debito'    ).AsFloat;
        //
        SerieCH   := FQrSource.FieldByName('SerieCH'   ).AsString;
        //
        Data      := FQrSource.FieldByName('Data'      ).AsDateTime;
        Vencto    := FQrSource.FieldByName('Vencimento').AsDateTime;
        //
        ITENS     := FQrSource.FieldByName('ITENS').AsInteger;
      end else begin
        Inc(SemQuery, 1);
      end;
      if Protocolo = 0 then
      begin
        Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
        //
        // Incluir antes o itens (quando mais de um!
        if ITENS > 1 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(FQrLcI, FQrLcI.Database, [
          'SELECT Controle Lancto ',
          'FROM ' + FLctProto,
          'WHERE Data="' + Geral.FDT(Data, 1) + '"',
          'AND Tipo=' + Geral.FF0(Tipo),
          'AND Carteira=' + Geral.FF0(Carteira),
          'AND CliInt=' + Geral.FF0(CliInt),
          'AND SerieCH="' + SerieCH + '"',
          'AND Documento=' + Geral.FFI(Documento),
          ' ']);
          FQrLcI.First;
          while not FQrLcI.Eof do
          begin
            Lancto := FQrLcI.FieldByName('Lancto').AsInteger;
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakmul', False, [
            'Codigo', 'Controle'], [
            'Conta', 'Lancto'], [
            QrProtocoPakCodigo.Value,
            QrProtocoPakControle.Value],
            [Conta, Lancto], True) then ;
            // J� antecipar aqui se tiver mais de um lancto com o mesmo protocolo!
            UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
              'Protocolo'], ['Controle', 'Sub'], [
               Conta], [Lancto, Sub], True, '', FTabLctA);
            //
            FQrLcI.Next;
          end;
        end;

        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakits', False,
        [
          'Codigo', 'Controle', 'Link_ID', 'CliInt',
          'Cliente', 'Fornece', 'Periodo', 'Lancto',
          'Docum', 'Depto',
          'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
          'Retorna', 'Vencto',
          'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
          'SerieCH', 'LimiteSai', 'LimiteRem', 'LimiteRet'
        ], ['Conta'], [
          QrProtocoPakCodigo.Value, QrProtocoPakControle.Value,
          VAR_TIPO_LINK_ID_01_GENERICO, CliInt,
          Cliente, Fornece, 0(*Periodo*), Controle,
          Documento, 0,
          0(*FID_Cod1*), 0(*FID_Cod2*), 0(*FID_Cod3*), 0(*FID_Cod4*),
          MLAGeral.BoolToInt(CkRetorna.Checked), Geral.FDT(Vencto, 1),
          Credito-Debito, 0(*MoraDiaVal*), 0(*MultaVal*), 0(*Cedente*),
          SerieCH, FLimiteSai, FLimiteRem, FLimiteRet
        ], [Conta], True) then
        // erro no controle de lancamentos?
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Protocolo'], ['Controle', 'Sub'], [
           Conta], [Controle, Sub], True, '', FTabLctA);
        //
      end;
      //
    end;

    procedure GeraProtocolo_Lancamentos(var SemQuery: Integer);
    var
      Protocolo, Controle, Sub, Conta, CliInt, Cliente, Fornece: Integer;
      Credito, Debito, Documento: Double;
      SerieCH: String;
      Vencto: TDateTime;
    begin
      Protocolo := -1;
      Controle  := 0;
      CliInt    := 0;
      Cliente   := 0;
      Fornece   := 0;
      Credito   := 0;
      Debito    := 0;
      Documento := 0;
      Vencto    := 0;
      Sub       := 0;
      if FQrSource <> nil then
      begin
        Protocolo := 0;
        Controle  := FQrSource.FieldByName('Controle'  ).AsInteger;
        Sub       := FQrSource.FieldByName('Sub'       ).AsInteger;
        CliInt    := FQrSource.FieldByName('CliInt'    ).AsInteger;
        Cliente   := FQrSource.FieldByName('Cliente'   ).AsInteger;
        Fornece   := FQrSource.FieldByName('Fornecedor').AsInteger;
        //
        Documento := FQrSource.FieldByName('Documento' ).AsFloat;
        Credito   := FQrSource.FieldByName('Credito'   ).AsFloat;
        Debito    := FQrSource.FieldByName('Debito'    ).AsFloat;
        //
        SerieCH   := FQrSource.FieldByName('SerieCH'   ).AsString;
        //
        Vencto    := FQrSource.FieldByName('Vencimento').AsDateTime;
        //
      end else begin
        Inc(SemQuery, 1);
      end;
      if Protocolo = 0 then
      begin
        Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakits', False,
        [
          'Codigo', 'Controle', 'Link_ID', 'CliInt',
          'Cliente', 'Fornece', 'Periodo', 'Lancto',
          'Docum', 'Depto',
          'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
          'Retorna', 'Vencto',
          'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
          'SerieCH', 'LimiteSai', 'LimiteRem', 'LimiteRet'
        ], ['Conta'], [
          QrProtocoPakCodigo.Value, QrProtocoPakControle.Value,
          VAR_TIPO_LINK_ID_01_GENERICO, CliInt,
          Cliente, Fornece, 0(*Periodo*), Controle,
          Documento, 0,
          0(*FID_Cod1*), 0(*FID_Cod2*), 0(*FID_Cod3*), 0(*FID_Cod4*),
          MLAGeral.BoolToInt(CkRetorna.Checked), Geral.FDT(Vencto, 1),
          Credito-Debito, 0(*MoraDiaVal*), 0(*MultaVal*), 0(*Cedente*),
          SerieCH, FLimiteSai, FLimiteRem, FLimiteRet
        ], [Conta], True) then
        // erro no controle de lancamentos?
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Protocolo'], ['Controle', 'Sub'], [
           Conta], [Controle, Sub], True, '', FTabLctA);
        //
      end;
    end;

    procedure GeraProtocolo_CondGerProto(var SemEmeio, SemQuery: Integer);
    var
      Protocolo, Propriet, Conta, Apto, Emeio_ID: Integer;
      Vencto: TDateTime;
      Bloqueto, Valor, MoraDiaVal, MultaVal: Double;
    begin
      Propriet   := 0;
      //Codigo     := 0;
      Bloqueto   := 0;
      Apto       := 0;
      EMeio_ID   := 0;
      Vencto     := 0;
      Valor      := 0;
      // Parei aqui! ver o que fazer!
      MoraDiaVal := Round(Valor * (1 / 30)) / 100;
      MultaVal   := Round(Valor * 2) / 100;
      //Cedente    := 0;
      if FNomeQrSrc = 'QrCD1' then
      begin
        Protocolo  := FmCondGerProto.QrCD1PROTOCOLO.Value;
        //Codigo     := FmCondGerProto.QrCD1PROTOCOD.Value;
        Propriet   := FmCondGerProto.QrCD1Propriet.Value;
        //Cedente   := FmCondGerProto.QrCD1Cedente.Value;
        Bloqueto   := FmCondGerProto.QrCD1Boleto.Value;
        Apto       := FmCondGerProto.QrCD1Apto.Value;
        Vencto     := FmCondGerProto.QrCD1Vencto.Value;
        Valor      := FmCondGerProto.QrCD1SUB_TOT.Value;
        MoraDiaVal := Round(Valor * (FmCondGerProto.FPercJuros / 30)) / 100;
        MultaVal   := Round(Valor * FmCondGerProto.FPercMulta) / 100;
      end else
      if FNomeQrSrc = 'QrCR1' then
      begin
        Protocolo := FmCondGerProto.QrCR1PROTOCOLO.Value;
        //Codigo    := FmCondGerProto.QrCR1PROTOCOD.Value;
        Propriet  := FmCondGerProto.QrCR1Propriet.Value;
        //Cedente   := FmCondGerProto.QrCR1Cedente.Value;
        Bloqueto  := FmCondGerProto.QrCR1Boleto.Value;
        Apto      := FmCondGerProto.QrCR1Apto.Value;
        Vencto    := FmCondGerProto.QrCR1Vencto.Value;
        Valor     := FmCondGerProto.QrCR1SUB_TOT.Value;
        MoraDiaVal := Round(Valor * (FmCondGerProto.FPercJuros / 30)) / 100;
        MultaVal   := Round(Valor * FmCondGerProto.FPercMulta) / 100;
      end else
      if FNomeQrSrc = 'QrProtoMail' then
      begin
        if FmCondGerProto.QrProtoMailRecipEmeio.Value <> '' then
        begin
          Protocolo := FmCondGerProto.QrProtoMailProtocolo.Value;
          //Codigo    := FmCondGerProto.QrProtoMailTaref_Cod.Value;
          Propriet  := FmCondGerProto.QrProtoMailEntid_Cod.Value;
          Bloqueto  := FmCondGerProto.QrProtoMailBloqueto.Value;
          Apto      := FmCondGerProto.QrProtoMailDepto_Cod.Value;
          EMeio_ID  := FmCondGerProto.QrProtoMailRecipItem.Value;
          //
          //  N�o � necess�rio?
          Vencto    := FmCondGerProto.QrProtoMailVencimento.Value;
          Valor     := FmCondGerProto.QrProtoMailValor.Value;
          //Cedente   :=
          MoraDiaVal := Round(Valor * (FmCondGerProto.FPercJuros / 30)) / 100;
          MultaVal   := Round(Valor * FmCondGerProto.FPercMulta) / 100;
        end else begin
          Inc(SemEmeio, 1);
          Protocolo := -1;
        end;
      end else begin
        Inc(SemQuery, 1);
        Protocolo := -1;
      end;
      //
      if Protocolo = 0 then
      begin
        Conta    := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakits', False,
        [
          'Codigo', 'Controle', 'Link_ID', 'CliInt',
          'Cliente', 'Fornece', 'Periodo',
          'Lancto', 'Docum',
          'Depto',
          'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
          'Retorna', 'Vencto',
          'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
          'LimiteSai', 'LimiteRem', 'LimiteRet'
        ], ['Conta'], [
          QrProtocoPakCodigo.Value, QrProtocoPakControle.Value,
          VAR_TIPO_LINK_ID_01_GENERICO, FmCondGerProto.FCliEnti,
          Propriet, 0, FmCondGerProto.FPeriodo,
          0, Bloqueto,
          Apto,
          FmCondGerProto.FID_Cod1, FmCondGerProto.FID_Cod2, EMeio_ID, 0,
          MLAGeral.BoolToInt(CkRetorna.Checked), Geral.FDT(Vencto, 1),
          Valor, MoraDiaVal, MultaVal, FmCondGerProto.FCedente,
          FLimiteSai, FLimiteRem, FLimiteRet
        ], [Conta], True);
        //
      end;
    end;
  begin
    if FNomeQrSrc = 'DmLct0.QrLcP' then
      GeraProtocolo_DocumLanctos(SemQuery)
    else
    if FNomeQrSrc = 'DmLct2.QrLcP' then
      GeraProtocolo_DocumLanctos(SemQuery)
    else
    if FNomeQrSrc = 'FmPrincipal.QrLct' then
      GeraProtocolo_Lancamentos(SemQuery)
    else
    if FNomeQrSrc = 'DmLct0.QrLct' then
      GeraProtocolo_Lancamentos(SemQuery)
    else
    if FNomeQrSrc = 'DmLct2.QrLct' then
      GeraProtocolo_Lancamentos(SemQuery)
    else
      GeraProtocolo_CondGerProto(SemEmeio, SemQuery);
  end;
var
  //Codigo, Conta,
  i, se, sq, Protocolo, Tarefa, Continua: Integer;
begin
  Tarefa := EdTarefa.ValueVariant;
  //
  if MyObjects.FIC(Tarefa = 0, EdTarefa, 'Informe a tarefa!') then Exit;
  //
  FLimiteSai := Geral.FDT(TPLimiteSai.Date, 1);
  FLimiteRem := Geral.FDT(TPLimiteSai.Date, 1);
  FLimiteRet := Geral.FDT(TPLimiteSai.Date, 1);
  se := 0;
  sq := 0;
  if QrProtocoPakControle.Value = 0 then
  begin
    Geral.MensagemBox('N�o h� lote selecionado!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  if FQuais = istTodos then
  begin
    if not FAskTodos then
      Continua := ID_YES
    else
    Continua := Geral.MensagemBox(
    'Gera protocolo para todos itens?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION);
    if Continua = ID_YES then
    begin
      //
      FQrSource.First;
      while not FQrSource.Eof do
      begin
        GeraAtual(se, sq);
        FQrSource.Next;
      end;
      //
    end;
  end else if FQuais = istSelecionados then
  begin
    if FDBGrid.SelectedRows.Count > 1 then
    begin
      with FDBGrid.DataSource.DataSet do
      for i := 0 to FDBGrid.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(FDBGrid.SelectedRows.Items[i]));
        GeraAtual(se, sq);
      end;
    end else
      GeraAtual(se, sq);
  end else
  if FQuais = istAtual then
    GeraAtual(se, sq);
  Screen.Cursor := crDefault;
  if FNomeQrSrc = 'QrProtoMail' then
    FmCondGerProto.VerificaEntregaPorEmail(EdTarefa.ValueVariant)
  else if FNomeQrSrc = 'FmPrincipal.QrLct' then
    // n�o faz nada abrir na janela de origem
  else if FNomeQrSrc = 'DmLct0.QrLct' then
    // n�o faz nada abrir na janela de origem
  else if FNomeQrSrc = 'DmLct0.QrLcP' then
    // n�o faz nada abrir na janela de origem
  else if FNomeQrSrc <> '' then
  begin
    if LowerCase(FFormOrigem) = 'fmcondgerproto' then
    begin
      case FmCondGerProto.PageControl1.ActivePageIndex of
          0: Protocolo := FmCondGerProto.QrProt1Codigo.Value;
          2: Protocolo := -1;
        else Protocolo := -1;
      end;
      FmCondGerProto.ReopenQuery(FQrSource, FmCondGerProto.GetTipoProtocolo,
        FQrSource.FieldByName('BOLAPTO').AsString, Protocolo);
    end;
  end else
    Geral.MensagemBox('Fonte de dados n�o definida para reabertura!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  if se > 0 then
    Geral.MensagemBox(FormatFloat('00', se) + ' protocolos n�o ' +
    'foram gerados porque o bloqueto n�o tem e-mail recipiente!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  if sq > 0 then
    Geral.MensagemBox(FormatFloat('00', sq) + ' protocolos n�o ' +
    'foram gerados porque o tipo de protocolo n�o foi impementado pela ' +
    ' DERMATEK!', 'Erro', MB_OK+MB_ICONERROR);
  Close;
end;

procedure TFmCondGerProtoSel.EdTarefaChange(Sender: TObject);
begin
  ReopenProtocoPak(QrProtocoPakControle.Value);
end;

procedure TFmCondGerProtoSel.FormCreate(Sender: TObject);
begin
  FAskTodos := True;
  FAgruparDoc := False;
  ImgTipo.SQLType := stLok;
  //QrProtocolos.Open;
  TPLimiteSai.Date := Date + 1;
  TPLimiteRem.Date := Date + 1;
  TPLimiteRet.Date := Date + 4;
end;

procedure TFmCondGerProtoSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerProtoSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerProtoSel.ReopenProtocolos(TipoProtocolo, Tarefa, Def_Client: Integer);
var
  SQLParam1, SQLParam2, SQLParam3: String;
begin
  if TipoProtocolo <> 0 then
    SQLParam1 := 'AND ptc.Tipo =' + Geral.FF0(TipoProtocolo)
  else
    SQLParam1 := '';
  if Tarefa <> 0 then
    SQLParam2 := 'AND ptc.Codigo =' + Geral.FF0(Tarefa)
  else
    SQLParam2 := '';
  if Def_Client <> 0 then
    SQLParam3 := 'AND ptc.Def_Client =' + Geral.FF0(Def_Client)
  else
    SQLParam3 := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrProtocolos, Dmod.MyDB, [
  'SELECT ptc.Codigo, ptc.Nome, ptc.Def_Retorn, ',
  'ptc.Tipo, ptc.Def_Client ',
  'FROM protocolos ptc ',
  'WHERE ptc.Codigo > 0 ',
  SQLParam1,
  SQLParam2,
  SQLParam3,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmCondGerProtoSel.ReopenProtocoPak(Controle: Integer);
var
  Proto: Integer;
begin
  CkRetorna.Checked := MLAGeral.IntToBool(QrProtocolosDef_Retorn.Value);
  QrProtocoPak.Close;
  Proto := Geral.IMV(EdTarefa.Text);
  if Proto > 0 then
  begin
    QrProtocoPak.SQL.Clear;
    QrProtocoPak.SQL.Add('SELECT ptp.*,');
    QrProtocoPak.SQL.Add('CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4)) MES,');
    QrProtocoPak.SQL.Add('IF(ptp.DataF=0,"", DATE_FORMAT(ptp.DataF, "%d/%m/%y" )) DATAF_TXT');
    QrProtocoPak.SQL.Add('FROM protocopak ptp');
    QrProtocoPak.SQL.Add('');
    QrProtocoPak.SQL.Add('WHERE ptp.Codigo=' + FormatFloat('0', Proto));
    //
    if CkAbertos.Checked then
      QrProtocoPak.SQL.Add('AND ptp.DataF=0');
    QrProtocoPak.SQL.Add('ORDER BY Controle DESC, DataI DESC');
    QrProtocoPak.Open;
    //
    if Controle > 0 then QrProtocoPak.Locate('Controle', Controle, []);
  end;
end;

procedure TFmCondGerProtoSel.CkAbertosClick(Sender: TObject);
begin
  ReopenProtocoPak(QrProtocoPakControle.Value);
end;

procedure TFmCondGerProtoSel.CkRetornaClick(Sender: TObject);
begin
  LaLimiteRet.Visible := CkRetorna.Checked;
  TPLimiteRet.Visible := CkRetorna.Checked;
end;

procedure TFmCondGerProtoSel.BitBtn1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := Geral.IMV(EdTarefa.Text);
  if Codigo = 0 then
  begin
    Geral.MensagemBox('Informe a tarefa!', 'Aviso', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if DBCheck.CriaFm(TFmProtocoPak, FmProtocoPak, afmoNegarComAviso) then
  begin
    {
    if SQLType = stIns then
    begin
    }
      FmProtocoPak.dmkEdDataI.ValueVariant := Date;
      FmProtocoPak.dmkEdDataL.ValueVariant := Date;
      FmProtocoPak.EdMes.Text := Geral.FDT(Date, 14);
    {
    end else begin
      FmProtocoPak.dmkEdDataI.ValueVariant := QrProtocoPakDataI.Value;
      FmProtocoPak.dmkEdDataL.ValueVariant := QrProtocoPakDataL.Value;
      FmProtocoPak.EdMes.Text              := dmkPF.MezToMesEAno(QrProtocoPakMez.Value);
    end;
    FmProtocoPak.ImgTipo.SQLType := SQLType;
    }
    FmProtocoPak.ImgTipo.SQLType := stIns;
    FmProtocoPak.FCodigo         := Codigo;
    FmProtocoPak.FControle       := 0;
    FmProtocoPak.FTipo           := QrProtocolosTipo.Value;
    FmProtocoPak.FDef_Client     := QrProtocolosDef_Client.Value;
    //
    FmProtocoPak.ShowModal;
    if FmProtocoPak.FExecuted then
    begin
      ReopenProtocoPak(FmProtocoPak.FControle);
    end;
    FmProtocoPak.Destroy;
  end;
end;

end.

