object Dmod: TDmod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 590
  Width = 1038
  object QrFields: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SHOW FIELDS FROM :p0')
    Left = 428
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrMaster: TmySQLQuery
    Database = MyDB
    AfterOpen = QrMasterAfterOpen
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      
        'SELECT cm.Dono, cm.Versao, ma.CNPJ, ma.SolicitaSenha, ma.Em, ma.' +
        'UsaAccMngr'
      'FROM controle cm'
      'LEFT JOIN entidades te ON te.Codigo=cm.Dono'
      'LEFT JOIN master ma ON ma.CNPJ=te.CNPJ')
    Left = 12
    Top = 144
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Required = True
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrMasterSolicitaSenha: TSmallintField
      FieldName = 'SolicitaSenha'
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Size = 100
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
  end
  object QrLivreY_D: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Controle Codigo FROM ?')
    Left = 320
    Top = 60
    object QrLivreY_DCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrRecCountX: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  ArtigosGrupos')
    Left = 392
    Top = 248
    object QrRecCountXRecord: TIntegerField
      FieldName = 'Record'
    end
  end
  object QrUser: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Login FROM senhas'
      'WHERE Numero=:Usuario')
    Left = 12
    Top = 96
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Usuario'
        ParamType = ptUnknown
      end>
    object QrUserLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
  end
  object QrSelX: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Lk FROM ?')
    Left = 272
    Top = 56
    object QrSelXLk: TIntegerField
      FieldName = 'Lk'
    end
  end
  object QrSB: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSBCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrSBNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.pq.Nome'
      Size = 41
    end
  end
  object DsSB: TDataSource
    DataSet = QrSB
    Left = 232
    Top = 8
  end
  object QrSB2: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSB2Codigo: TFloatField
      FieldName = 'Codigo'
    end
    object QrSB2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 128
    end
  end
  object DsSB2: TDataSource
    DataSet = QrSB2
    Left = 232
    Top = 52
  end
  object QrSB3: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT fa.Data Codigo, ca.Nome Nome'
      'FROM faturas fa, Carteiras ca'
      'WHERE ca.Codigo=fa.Emissao'
      'AND fa.Emissao=15')
    Left = 204
    Top = 96
    object QrSB3Codigo: TDateField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.faturas.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSB3Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
  end
  object DsSB3: TDataSource
    DataSet = QrSB3
    Left = 232
    Top = 96
  end
  object QrDuplicIntX: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT NF INTEIRO1, Cliente INTEIRO2, Codigo CODIGO'
      'FROM pqpse'
      'WHERE NF=1'
      'AND Cliente=1')
    Left = 392
    Top = 160
    object QrDuplicIntXINTEIRO1: TIntegerField
      FieldName = 'INTEIRO1'
      Origin = 'DBMBWET.pqpse.NF'
    end
    object QrDuplicIntXINTEIRO2: TIntegerField
      FieldName = 'INTEIRO2'
      Origin = 'DBMBWET.pqpse.Cliente'
    end
    object QrDuplicIntXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pqpse.Codigo'
    end
  end
  object QrDuplicStrX: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Nome NOME, Codigo CODIGO, IDCodigo ANTERIOR'
      'FROM pq'
      'WHERE Nome=:Nome')
    Left = 364
    Top = 160
    ParamData = <
      item
        DataType = ftString
        Name = 'Nome'
        ParamType = ptUnknown
      end>
    object QrDuplicStrXNOME: TWideStringField
      FieldName = 'NOME'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrDuplicStrXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrDuplicStrXANTERIOR: TIntegerField
      FieldName = 'ANTERIOR'
      Origin = 'DBMBWET.pq.IDCodigo'
    end
  end
  object QrInsLogX: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'INSERT INTO logs SET'
      'Data=NOW(),'
      'Tipo=:P0,'
      'Usuario=:P1,'
      'ID=:P2')
    Left = 392
    Top = 204
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDelLogX: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'DELETE FROM logs'
      'WHERE Tipo=:P0'
      'AND Usuario=:P1'
      'AND ID=:P2')
    Left = 364
    Top = 204
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDataBalY: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT MAX(Data) Data FROM pesagem'
      'WHERE Tipo=4')
    Left = 272
    Top = 8
    object QrDataBalYData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pesagem.Data'
    end
  end
  object QrSenha: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM senhas'
      'WHERE Login=:P0'
      'AND Senha=:P1')
    Left = 8
    Top = 240
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhaLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
    object QrSenhaNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'DNTEACH.senhas.Numero'
    end
    object QrSenhaSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'DNTEACH.senhas.Senha'
      Size = 128
    end
    object QrSenhaPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'DNTEACH.senhas.Perfil'
    end
    object QrSenhaLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DNTEACH.senhas.Lk'
    end
  end
  object QrMaster2: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM master')
    Left = 8
    Top = 196
    object QrMaster2Limite: TSmallintField
      FieldName = 'Limite'
    end
    object QrMaster2Em: TWideStringField
      FieldName = 'Em'
      Required = True
      Size = 100
    end
    object QrMaster2CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 30
    end
    object QrMaster2Monitorar: TSmallintField
      FieldName = 'Monitorar'
      Required = True
    end
    object QrMaster2Licenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrMaster2Distorcao: TIntegerField
      FieldName = 'Distorcao'
      Required = True
    end
    object QrMaster2DataI: TDateField
      FieldName = 'DataI'
    end
    object QrMaster2DataF: TDateField
      FieldName = 'DataF'
    end
    object QrMaster2Hoje: TDateField
      FieldName = 'Hoje'
    end
    object QrMaster2Hora: TTimeField
      FieldName = 'Hora'
    end
    object QrMaster2MasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrMaster2MasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrMaster2MasAtivar: TWideStringField
      FieldName = 'MasAtivar'
      Size = 1
    end
  end
  object QrProduto: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo, Controla'
      'FROM produtos'
      'WHERE Codigo=:P0')
    Left = 120
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutoControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
  end
  object QrVendas: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtossits'
      'WHERE Produto=:P0')
    Left = 92
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVendasPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtossits.Pecas'
    end
    object QrVendasValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtossits.Total'
    end
  end
  object QrEntrada: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtoscits'
      'WHERE Produto=:P0'
      '')
    Left = 64
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntradaPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtoscits.Pecas'
    end
    object QrEntradaValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtoscits.Total'
    end
  end
  object QrUpdM: TmySQLQuery
    Database = MyDB
    Left = 448
    Top = 48
  end
  object QrUpdU: TmySQLQuery
    Database = MyDB
    Left = 12
    Top = 48
  end
  object QrUpdL: TmySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 96
  end
  object QrUpdY: TmySQLQuery
    Database = MyDB
    Left = 64
    Top = 240
  end
  object QrLivreY: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo FROM livres')
    Left = 60
    Top = 192
    object QrLivreYCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrCountY: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  Entidades')
    Left = 464
    Top = 248
    object QrCountYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrLocY: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT MIN(Codigo) Record FROM carteiras'
      '')
    Left = 464
    Top = 204
    object QrLocYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrLocDataY: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT MIN(Data) Record FROM ?'
      '')
    Left = 320
    Top = 8
    object QrLocDataYRecord: TDateField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrIdx: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrMas: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object QrUpd: TmySQLQuery
    Database = MyDB
    Left = 176
    Top = 320
  end
  object QrAux: TmySQLQuery
    Database = MyDB
    Left = 176
    Top = 364
  end
  object QrSQL: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object MyDB: TmySQLDatabase
    DatabaseName = 'syndic'
    DesignOptions = []
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    ConnectionCharacterSet = 'latin1'
    ConnectionCollation = 'latin1_swedish_ci'
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=syndic'
      'UID=root'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 16
    Top = 4
  end
  object MyLocDatabase: TmySQLDatabase
    DatabaseName = 'LocSyndic'
    DesignOptions = []
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    LoginPrompt = True
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=LocSyndic'
      'UID=root'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 156
    Top = 4
  end
  object QrSB4: TmySQLQuery
    Database = MyLocDatabase
    Left = 200
    Top = 144
    object QrSB4Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 255
    end
    object QrSB4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsSB4: TDataSource
    DataSet = QrSB4
    Left = 232
    Top = 144
  end
  object QrPriorNext: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrAuxL: TmySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 52
  end
  object QrSoma1: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(TotalC*Tipo) TOTAL, '
      'SUM(Qtde*Tipo) Qtde '
      'FROM mov'
      'WHERE Produto=:P0')
    Left = 260
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSoma1TOTAL: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'TOTAL'
    end
    object QrSoma1Qtde: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Qtde'
    end
  end
  object QrEstoque: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, ProdutosMIts pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM>=:P1'
      'GROUP BY pm.Tipo')
    Left = 428
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEstoqueTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstoqueValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrPeriodoBal: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM balancos')
    Left = 344
    Top = 324
    object QrPeriodoBalPeriodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrMin: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Min(Periodo) Periodo'
      'FROM balancos')
    Left = 372
    Top = 324
    object QrMinPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'DBMBWET.balancos.Periodo'
    end
  end
  object QrBalancosIts: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(EstqQ) EstqQ, SUM(EstqV) EstqV,'
      'SUM(EstqQ_G) EstqQ_G, SUM(EstqV_G) EstqV_G'
      'FROM balancosits'
      'WHERE Produto=:P0'
      'AND Periodo=:P1'
      '')
    Left = 400
    Top = 324
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBalancosItsEstqQ: TFloatField
      FieldName = 'EstqQ'
      Origin = 'DBMBWET.balancosits.EstqQ'
    end
    object QrBalancosItsEstqV: TFloatField
      FieldName = 'EstqV'
      Origin = 'DBMBWET.balancosits.EstqV'
    end
    object QrBalancosItsEstqQ_G: TFloatField
      FieldName = 'EstqQ_G'
    end
    object QrBalancosItsEstqV_G: TFloatField
      FieldName = 'EstqV_G'
    end
  end
  object QrEstqperiodo: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, ProdutosMIts pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM BETWEEN :P1 AND :P2'
      'GROUP BY pm.Tipo')
    Left = 456
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEstqperiodoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstqperiodoQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstqperiodoValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrTerminal: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais'
      'WHERE IP=:P0')
    Left = 292
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrNTV: TmySQLQuery
    Database = MyDB
    Left = 60
    Top = 4
  end
  object QrNTI: TmySQLQuery
    Database = MyLocDatabase
    Left = 60
    Top = 48
  end
  object ZZDB: TmySQLDatabase
    DatabaseName = 'Syndic'
    DesignOptions = []
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    LoginPrompt = True
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=Syndic'
      'UID=root'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 96
    Top = 4
  end
  object QrPerfis: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      
        'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 300
    Top = 292
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrTerceiro: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT ufp.Nome NOMEpUF, ufe.Nome NOMEeUF, en.* '
      'FROM entidades en'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'WHERE en.Codigo=:P0')
    Left = 196
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroNOMEpUF: TWideStringField
      FieldName = 'NOMEpUF'
      Required = True
      Size = 2
    end
    object QrTerceiroNOMEeUF: TWideStringField
      FieldName = 'NOMEeUF'
      Required = True
      Size = 2
    end
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTerceiroRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrTerceiroFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrTerceiroPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrTerceiroMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrTerceiroNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTerceiroApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTerceiroELograd: TSmallintField
      FieldName = 'ELograd'
      Required = True
    end
    object QrTerceiroERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTerceiroENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTerceiroECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrTerceiroEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrTerceiroECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrTerceiroEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrTerceiroECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTerceiroEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrTerceiroETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrTerceiroEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrTerceiroEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrTerceiroECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrTerceiroEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrTerceiroEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrTerceiroEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrTerceiroENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTerceiroPLograd: TSmallintField
      FieldName = 'PLograd'
      Required = True
    end
    object QrTerceiroPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrTerceiroPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrTerceiroPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrTerceiroPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrTerceiroPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrTerceiroPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrTerceiroPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTerceiroPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrTerceiroPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrTerceiroPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrTerceiroPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrTerceiroPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrTerceiroPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrTerceiroPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTerceiroPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrTerceiroPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTerceiroSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrTerceiroResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrTerceiroProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrTerceiroCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrTerceiroRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrTerceiroDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrTerceiroAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrTerceiroAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrTerceiroCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrTerceiroCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrTerceiroFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrTerceiroFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrTerceiroFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrTerceiroFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrTerceiroTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrTerceiroCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTerceiroInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrTerceiroLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrTerceiroVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTerceiroMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrTerceiroObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrTerceiroCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrTerceiroCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrTerceiroCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrTerceiroCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrTerceiroCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrTerceiroCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrTerceiroCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTerceiroCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrTerceiroCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrTerceiroCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrTerceiroCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrTerceiroCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrTerceiroLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrTerceiroLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrTerceiroLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrTerceiroLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrTerceiroLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrTerceiroLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrTerceiroLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrTerceiroLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTerceiroLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrTerceiroLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrTerceiroLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrTerceiroLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrTerceiroLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrTerceiroComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTerceiroSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTerceiroNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrTerceiroGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTerceiroAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrTerceiroLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrTerceiroConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrTerceiroConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrTerceiroNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrTerceiroNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrTerceiroNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrTerceiroNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrTerceiroNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrTerceiroNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrTerceiroNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrTerceiroNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrTerceiroCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrTerceiroCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrTerceiroCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrTerceiroCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrTerceiroCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrTerceiroCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrTerceiroMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrTerceiroQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrTerceiroQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrTerceiroQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrTerceiroQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrTerceiroQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrTerceiroQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrTerceiroAgenda: TWideStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrTerceiroSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrTerceiroSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrTerceiroLimiCred: TFloatField
      FieldName = 'LimiCred'
      Required = True
    end
    object QrTerceiroDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrTerceiroCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrTerceiroTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrTerceiroLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTerceiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTerceiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTerceiroUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTerceiroUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTerceiroCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrTerceiroSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrTerceiroCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrTerceiroUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Required = True
    end
  end
  object QlLocal: TMySQLBatchExecute
    Action = baContinue
    Database = MyLocDatabase
    Delimiter = ';'
    Left = 368
    Top = 4
  end
  object QrAgora: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 60
    Top = 96
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrTerminais: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais')
    Left = 292
    Top = 228
    object QrTerminaisIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminaisLicenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrTerminaisTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrEndereco: TmySQLQuery
    Database = MyDB
    OnCalcFields = QrEnderecoCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END NUME' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 48
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnderecoNO_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO_DOC'
      Calculated = True
    end
    object QrEnderecoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEnderecoCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEnderecoNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrEnderecoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEnderecoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEnderecoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrEnderecoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEnderecoNUMERO: TLargeintField
      FieldName = 'NUMERO'
    end
    object QrEnderecoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEnderecoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEnderecoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEnderecoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEnderecoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEnderecoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEnderecoLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrEnderecoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEnderecoCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrEnderecoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEnderecoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEnderecoENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEnderecoPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEnderecoRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrEnderecoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrEnderecoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrEnderecoE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrEnderecoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
  end
  object QrBoss: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MasSenha MasZero, '
      'AES_DECRYPT(MasSenha, :P0) MasSenha, MasLogin, Em, CNPJ'
      'FROM master')
    Left = 340
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBossMasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrBossMasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrBossEm: TWideStringField
      FieldName = 'Em'
      Size = 100
    end
    object QrBossCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrBossMasZero: TWideStringField
      FieldName = 'MasZero'
      Size = 30
    end
  end
  object QrBSit: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM master')
    Left = 368
    Top = 376
    object QrBSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrSenhas: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT AES_DECRYPT(se.Senha, :P0) SenhaNew, se.Senha SenhaOld, '
      'se.login, se.Numero, se.Perfil, se.IP_Default, Funcionario'
      'FROM senhas se'
      'WHERE se.Login=:P1')
    Left = 396
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhasSenhaNew: TWideStringField
      FieldName = 'SenhaNew'
      Size = 30
    end
    object QrSenhasSenhaOld: TWideStringField
      FieldName = 'SenhaOld'
      Size = 30
    end
    object QrSenhaslogin: TWideStringField
      FieldName = 'login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
    end
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrSenhasIP_Default: TWideStringField
      FieldName = 'IP_Default'
      Size = 50
    end
  end
  object QrSSit: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM senhas'
      'WHERE login=:P0'
      '')
    Left = 424
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrControle: TmySQLQuery
    Database = MyDB
    AfterOpen = QrControleAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM controle')
    Left = 12
    Top = 288
    object QrControleSoMaiusculas: TWideStringField
      FieldName = 'SoMaiusculas'
      Origin = 'controle.SoMaiusculas'
      Size = 1
    end
    object QrControleMoeda: TWideStringField
      FieldName = 'Moeda'
      Origin = 'controle.Moeda'
      Size = 4
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
      Origin = 'controle.UFPadrao'
    end
    object QrControleCidade: TWideStringField
      FieldName = 'Cidade'
      Origin = 'controle.Cidade'
      Size = 100
    end
    object QrControleCartVen: TIntegerField
      FieldName = 'CartVen'
      Origin = 'controle.CartVen'
    end
    object QrControleCartCom: TIntegerField
      FieldName = 'CartCom'
      Origin = 'controle.CartCom'
    end
    object QrControleCartDeS: TIntegerField
      FieldName = 'CartDeS'
      Origin = 'controle.CartDeS'
    end
    object QrControleCartReS: TIntegerField
      FieldName = 'CartReS'
      Origin = 'controle.CartReS'
    end
    object QrControleCartDeG: TIntegerField
      FieldName = 'CartDeG'
      Origin = 'controle.CartDeG'
    end
    object QrControleCartReG: TIntegerField
      FieldName = 'CartReG'
      Origin = 'controle.CartReG'
    end
    object QrControleCartCoE: TIntegerField
      FieldName = 'CartCoE'
      Origin = 'controle.CartCoE'
    end
    object QrControleCartCoC: TIntegerField
      FieldName = 'CartCoC'
      Origin = 'controle.CartCoC'
    end
    object QrControleCartEmD: TIntegerField
      FieldName = 'CartEmD'
      Origin = 'controle.CartEmD'
    end
    object QrControleCartEmA: TIntegerField
      FieldName = 'CartEmA'
      Origin = 'controle.CartEmA'
    end
    object QrControleContraSenha: TWideStringField
      FieldName = 'ContraSenha'
      Origin = 'controle.ContraSenha'
      Size = 50
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
      Origin = 'controle.PaperTop'
    end
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
      Origin = 'controle.PaperLef'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
      Origin = 'controle.PaperWid'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
      Origin = 'controle.PaperHei'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
      Origin = 'controle.PaperFcl'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
      Origin = 'controle.TravaCidade'
    end
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
      Origin = 'controle.MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'controle.Multa'
    end
    object QrControleMensalSempre: TIntegerField
      FieldName = 'MensalSempre'
      Origin = 'controle.MensalSempre'
    end
    object QrControleEntraSemValor: TIntegerField
      FieldName = 'EntraSemValor'
      Origin = 'controle.EntraSemValor'
    end
    object QrControleMyPagTip: TIntegerField
      FieldName = 'MyPagTip'
      Origin = 'controle.MyPagTip'
    end
    object QrControleMyPagCar: TIntegerField
      FieldName = 'MyPagCar'
      Origin = 'controle.MyPagCar'
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
      Origin = 'controle.CorRecibo'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
      Origin = 'controle.IdleMinutos'
    end
    object QrControleMyPgParc: TSmallintField
      FieldName = 'MyPgParc'
      Origin = 'controle.MyPgParc'
    end
    object QrControleMyPgQtdP: TSmallintField
      FieldName = 'MyPgQtdP'
      Origin = 'controle.MyPgQtdP'
    end
    object QrControleMyPgPeri: TSmallintField
      FieldName = 'MyPgPeri'
      Origin = 'controle.MyPgPeri'
    end
    object QrControleMyPgDias: TSmallintField
      FieldName = 'MyPgDias'
      Origin = 'controle.MyPgDias'
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Origin = 'controle.MeuLogoPath'
      Size = 255
    end
    object QrControleLogoNF: TWideStringField
      FieldName = 'LogoNF'
      Origin = 'controle.LogoNF'
      Size = 255
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
      Origin = 'controle.VendaCartPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
      Origin = 'controle.VendaParcPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
      Origin = 'controle.VendaPeriPg'
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
      Origin = 'controle.VendaDiasPg'
    end
    object QrControleOrcaLFeed: TIntegerField
      FieldName = 'OrcaLFeed'
      Origin = 'controle.OrcaLFeed'
    end
    object QrControleLastBco: TIntegerField
      FieldName = 'LastBco'
      Origin = 'controle.LastBco'
    end
    object QrControleInfoSeq: TIntegerField
      FieldName = 'InfoSeq'
      Origin = 'controle.InfoSeq'
    end
    object QrControleAtzCritic: TIntegerField
      FieldName = 'AtzCritic'
      Origin = 'controle.AtzCritic'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
      Origin = 'controle.CNABCtaJur'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
      Origin = 'controle.CNABCtaMul'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
      Origin = 'controle.CNABCtaTar'
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
      Origin = 'controle.VerBcoTabs'
    end
    object QrControleProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
      Origin = 'controle.ProLaINSSr'
    end
    object QrControleProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
      Origin = 'controle.ProLaINSSp'
    end
    object QrControleBLQ_TopoDestin: TIntegerField
      FieldName = 'BLQ_TopoDestin'
      Origin = 'controle.BLQ_TopoDestin'
    end
    object QrControleBLQ_MEsqDestin: TIntegerField
      FieldName = 'BLQ_MEsqDestin'
      Origin = 'controle.BLQ_MEsqDestin'
    end
    object QrControleBLQ_AltuDestin: TIntegerField
      FieldName = 'BLQ_AltuDestin'
      Origin = 'controle.BLQ_AltuDestin'
    end
    object QrControleBLQ_TopoAvisoV: TIntegerField
      FieldName = 'BLQ_TopoAvisoV'
      Origin = 'controle.BLQ_TopoAvisoV'
    end
    object QrControleBLQ_MEsqAvisoV: TIntegerField
      FieldName = 'BLQ_MEsqAvisoV'
      Origin = 'controle.BLQ_MEsqAvisoV'
    end
    object QrControleBLQ_AltuAvisoV: TIntegerField
      FieldName = 'BLQ_AltuAvisoV'
      Origin = 'controle.BLQ_AltuAvisoV'
    end
    object QrControleBLQ_LargDestin: TIntegerField
      FieldName = 'BLQ_LargDestin'
      Origin = 'controle.BLQ_LargDestin'
    end
    object QrControleBLQ_LargAvisoV: TIntegerField
      FieldName = 'BLQ_LargAvisoV'
      Origin = 'controle.BLQ_LargAvisoV'
    end
    object QrControleVersao: TIntegerField
      FieldName = 'Versao'
      Origin = 'controle.Versao'
    end
    object QrControleVerWeb: TIntegerField
      FieldName = 'VerWeb'
      Origin = 'controle.VerWeb'
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
    end
    object QrControleMyLogo: TWideStringField
      FieldName = 'MyLogo'
      Origin = 'controle.MyLogo'
      Size = 255
    end
    object QrControleBAL_TopoNomCon: TIntegerField
      FieldName = 'BAL_TopoNomCon'
      Origin = 'controle.BAL_TopoNomCon'
    end
    object QrControleBAL_TopoTitulo: TIntegerField
      FieldName = 'BAL_TopoTitulo'
      Origin = 'controle.BAL_TopoTitulo'
    end
    object QrControleBAL_TopoPeriod: TIntegerField
      FieldName = 'BAL_TopoPeriod'
      Origin = 'controle.BAL_TopoPeriod'
    end
    object QrControleWeb_Page: TWideStringField
      FieldName = 'Web_Page'
      Origin = 'controle.Web_Page'
      Size = 255
    end
    object QrControleServSMTP: TWideStringField
      FieldName = 'ServSMTP'
      Origin = 'controle.ServSMTP'
      Size = 50
    end
    object QrControleNomeMailOC: TWideStringField
      FieldName = 'NomeMailOC'
      Origin = 'controle.NomeMailOC'
      Size = 50
    end
    object QrControleDonoMailOC: TWideStringField
      FieldName = 'DonoMailOC'
      Origin = 'controle.DonoMailOC'
      Size = 50
    end
    object QrControleMailOC: TWideStringField
      FieldName = 'MailOC'
      Origin = 'controle.MailOC'
      Size = 80
    end
    object QrControleCorpoMailOC: TWideMemoField
      FieldName = 'CorpoMailOC'
      Origin = 'controle.CorpoMailOC'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrControleConexaoDialUp: TWideStringField
      FieldName = 'ConexaoDialUp'
      Origin = 'controle.ConexaoDialUp'
      Size = 50
    end
    object QrControleMailCCCega: TWideStringField
      FieldName = 'MailCCCega'
      Origin = 'controle.MailCCCega'
      Size = 80
    end
    object QrControleMyPerMulta: TFloatField
      FieldName = 'MyPerMulta'
      Origin = 'controle.MyPerMulta'
    end
    object QrControleMyPerJuros: TFloatField
      FieldName = 'MyPerJuros'
      Origin = 'controle.MyPerJuros'
    end
    object QrControleLogoBig1: TWideStringField
      FieldName = 'LogoBig1'
      Origin = 'controle.LogoBig1'
      Size = 255
    end
    object QrControleLokMedAnt: TSmallintField
      FieldName = 'LokMedAnt'
      Origin = 'controle.LokMedAnt'
    end
    object QrControleMoedaBr: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'MoedaBr'
      Origin = 'controle.MoedaBr'
      Calculated = True
    end
    object QrControleDdAutConfMail: TIntegerField
      FieldName = 'DdAutConfMail'
      Origin = 'controle.DdAutConfMail'
    end
    object QrControlePreMailReenv: TIntegerField
      FieldName = 'PreMailReenv'
      Origin = 'controle.PreMailReenv'
    end
    object QrControleFlxB_Folha: TSmallintField
      FieldName = 'FlxB_Folha'
      Origin = 'controle.FlxB_Folha'
    end
    object QrControleFlxB_LeiAr: TSmallintField
      FieldName = 'FlxB_LeiAr'
      Origin = 'controle.FlxB_LeiAr'
    end
    object QrControleFlxB_Fecha: TSmallintField
      FieldName = 'FlxB_Fecha'
      Origin = 'controle.FlxB_Fecha'
    end
    object QrControleFlxB_Risco: TSmallintField
      FieldName = 'FlxB_Risco'
      Origin = 'controle.FlxB_Risco'
    end
    object QrControleFlxB_Print: TSmallintField
      FieldName = 'FlxB_Print'
      Origin = 'controle.FlxB_Print'
    end
    object QrControleFlxB_Proto: TSmallintField
      FieldName = 'FlxB_Proto'
      Origin = 'controle.FlxB_Proto'
    end
    object QrControleFlxB_Relat: TSmallintField
      FieldName = 'FlxB_Relat'
      Origin = 'controle.FlxB_Relat'
    end
    object QrControleFlxB_EMail: TSmallintField
      FieldName = 'FlxB_EMail'
      Origin = 'controle.FlxB_EMail'
    end
    object QrControleFlxB_Porta: TSmallintField
      FieldName = 'FlxB_Porta'
      Origin = 'controle.FlxB_Porta'
    end
    object QrControleFlxB_Postl: TSmallintField
      FieldName = 'FlxB_Postl'
      Origin = 'controle.FlxB_Postl'
    end
    object QrControleFlxM_Conci: TSmallintField
      FieldName = 'FlxM_Conci'
      Origin = 'controle.FlxM_Conci'
    end
    object QrControleFlxM_Docum: TSmallintField
      FieldName = 'FlxM_Docum'
      Origin = 'controle.FlxM_Docum'
    end
    object QrControleFlxM_Anali: TSmallintField
      FieldName = 'FlxM_Anali'
      Origin = 'controle.FlxM_Anali'
    end
    object QrControleFlxM_Contb: TSmallintField
      FieldName = 'FlxM_Contb'
      Origin = 'controle.FlxM_Contb'
    end
    object QrControleFlxM_Encad: TSmallintField
      FieldName = 'FlxM_Encad'
      Origin = 'controle.FlxM_Encad'
    end
    object QrControleFlxM_Entrg: TSmallintField
      FieldName = 'FlxM_Entrg'
      Origin = 'controle.FlxM_Entrg'
    end
    object QrControleCB4_Host: TWideStringField
      FieldName = 'CB4_Host'
      Size = 255
    end
    object QrControleCB4_User: TWideStringField
      FieldName = 'CB4_User'
      Size = 30
    end
    object QrControleCB4_Pwd: TWideStringField
      FieldName = 'CB4_Pwd'
      Size = 30
    end
    object QrControleCB4_DB: TWideStringField
      FieldName = 'CB4_DB'
      Size = 30
    end
    object QrControleCB4_Uso: TSmallintField
      FieldName = 'CB4_Uso'
    end
    object QrControleCB4_Port: TIntegerField
      FieldName = 'CB4_Port'
    end
    object QrControleFlxM_Web: TSmallintField
      FieldName = 'FlxM_Web'
    end
    object QrControleEntiCargSind: TIntegerField
      FieldName = 'EntiCargSind'
    end
    object QrControleDdPrtstCrt: TIntegerField
      FieldName = 'DdPrtstCrt'
    end
  end
  object QrLocCtrl: TmySQLQuery
    Database = MyLocDatabase
    SQL.Strings = (
      'SELECT * FROM locctrl')
    Left = 112
    Top = 240
    object QrLocCtrlCartConcilia: TIntegerField
      FieldName = 'CartConcilia'
    end
  end
  object QrUpd2: TmySQLQuery
    Database = MyDB
    Left = 116
    Top = 188
  end
  object frxDsControle: TfrxDBDataset
    UserName = 'frxDsControle'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SoMaiusculas=SoMaiusculas'
      'Moeda=Moeda'
      'UFPadrao=UFPadrao'
      'Cidade=Cidade'
      'CartVen=CartVen'
      'CartCom=CartCom'
      'CartDeS=CartDeS'
      'CartReS=CartReS'
      'CartDeG=CartDeG'
      'CartReG=CartReG'
      'CartCoE=CartCoE'
      'CartCoC=CartCoC'
      'CartEmD=CartEmD'
      'CartEmA=CartEmA'
      'ContraSenha=ContraSenha'
      'PaperTop=PaperTop'
      'PaperLef=PaperLef'
      'PaperWid=PaperWid'
      'PaperHei=PaperHei'
      'PaperFcl=PaperFcl'
      'TravaCidade=TravaCidade'
      'MoraDD=MoraDD'
      'Multa=Multa'
      'MensalSempre=MensalSempre'
      'EntraSemValor=EntraSemValor'
      'MyPagTip=MyPagTip'
      'MyPagCar=MyPagCar'
      'CorRecibo=CorRecibo'
      'IdleMinutos=IdleMinutos'
      'MyPgParc=MyPgParc'
      'MyPgQtdP=MyPgQtdP'
      'MyPgPeri=MyPgPeri'
      'MyPgDias=MyPgDias'
      'MeuLogoPath=MeuLogoPath'
      'LogoNF=LogoNF'
      'VendaCartPg=VendaCartPg'
      'VendaParcPg=VendaParcPg'
      'VendaPeriPg=VendaPeriPg'
      'VendaDiasPg=VendaDiasPg'
      'OrcaLFeed=OrcaLFeed'
      'LastBco=LastBco'
      'InfoSeq=InfoSeq'
      'AtzCritic=AtzCritic'
      'CNABCtaJur=CNABCtaJur'
      'CNABCtaMul=CNABCtaMul'
      'CNABCtaTar=CNABCtaTar'
      'VerBcoTabs=VerBcoTabs'
      'ProLaINSSr=ProLaINSSr'
      'ProLaINSSp=ProLaINSSp'
      'BLQ_TopoDestin=BLQ_TopoDestin'
      'BLQ_MEsqDestin=BLQ_MEsqDestin'
      'BLQ_AltuDestin=BLQ_AltuDestin'
      'BLQ_TopoAvisoV=BLQ_TopoAvisoV'
      'BLQ_MEsqAvisoV=BLQ_MEsqAvisoV'
      'BLQ_AltuAvisoV=BLQ_AltuAvisoV'
      'BLQ_LargDestin=BLQ_LargDestin'
      'BLQ_LargAvisoV=BLQ_LargAvisoV'
      'Versao=Versao'
      'VerWeb=VerWeb'
      'Dono=Dono'
      'MyLogo=MyLogo'
      'BAL_TopoNomCon=BAL_TopoNomCon'
      'BAL_TopoTitulo=BAL_TopoTitulo'
      'BAL_TopoPeriod=BAL_TopoPeriod'
      'Web_Page=Web_Page'
      'ServSMTP=ServSMTP'
      'NomeMailOC=NomeMailOC'
      'DonoMailOC=DonoMailOC'
      'MailOC=MailOC'
      'CorpoMailOC=CorpoMailOC'
      'ConexaoDialUp=ConexaoDialUp'
      'MailCCCega=MailCCCega'
      'MyPerMulta=MyPerMulta'
      'MyPerJuros=MyPerJuros'
      'SecuritStr=SecuritStr'
      'LogoBig1=LogoBig1'
      'LokMedAnt=LokMedAnt'
      'MoedaBr=MoedaBr'
      'DdAutConfMail=DdAutConfMail'
      'PreMailReenv=PreMailReenv'
      'FlxB_Folha=FlxB_Folha'
      'FlxB_LeiAr=FlxB_LeiAr'
      'FlxB_Fecha=FlxB_Fecha'
      'FlxB_Risco=FlxB_Risco'
      'FlxB_Print=FlxB_Print'
      'FlxB_Proto=FlxB_Proto'
      'FlxB_Relat=FlxB_Relat'
      'FlxB_EMail=FlxB_EMail'
      'FlxB_Porta=FlxB_Porta'
      'FlxB_Postl=FlxB_Postl'
      'FlxM_Conci=FlxM_Conci'
      'FlxM_Docum=FlxM_Docum'
      'FlxM_Anali=FlxM_Anali'
      'FlxM_Contb=FlxM_Contb'
      'FlxM_Encad=FlxM_Encad'
      'FlxM_Entrg=FlxM_Entrg'
      'CB4_Host=CB4_Host'
      'CB4_User=CB4_User'
      'CB4_Pwd=CB4_Pwd'
      'CB4_DB=CB4_DB'
      'CB4_Uso=CB4_Uso'
      'CB4_Port=CB4_Port'
      'FlxM_Web=FlxM_Web'
      'EntiCargSind=EntiCargSind'
      'DdPrtstCrt=DdPrtstCrt')
    DataSet = QrControle
    BCDToCurrency = False
    Left = 44
    Top = 288
  end
  object QrDono: TmySQLQuery
    Database = MyDB
    OnCalcFields = QrDonoCalcFields
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, EContato, E' +
        'Cel,'
      'Respons1, Respons2,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia   ELSE en.Apelido END APELI' +
        'DO, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero+0.000     ELSE en.PNumero+0.' +
        '000 END NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd+0.000     ELSE en.PLograd+0.' +
        '000 END Lograd, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP+0.000        ELSE en.PCEP+0.000' +
        '    END CEP, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.EEMail      ELSE en.PEMail  END EMai' +
        'l '
      'FROM controle co'
      'LEFT JOIN entidades en ON en.Codigo=co.Dono'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd')
    Left = 12
    Top = 332
    object QrDonoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrDonoCadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrDonoNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrDonoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrDonoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrDonoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrDonoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrDonoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrDonoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrDonoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrDonoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrDonoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrDonoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrDonoTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrDonoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrDonoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrDonoENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrDonoPNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
    end
    object QrDonoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrDonoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrDonoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrDonoE_CUC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_CUC'
      Size = 255
      Calculated = True
    end
    object QrDonoEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrDonoECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrDonoCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoE_LN2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LN2'
      Size = 255
      Calculated = True
    end
    object QrDonoAPELIDO: TWideStringField
      FieldName = 'APELIDO'
      Size = 60
    end
    object QrDonoNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrDonoRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Size = 60
    end
    object QrDonoRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Size = 60
    end
    object QrDonoEMail: TWideStringField
      FieldName = 'EMail'
      Size = 100
    end
    object QrDonoLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrDonoCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object frxDsDono: TfrxDBDataset
    UserName = 'frxDsDono'
    CloseDataSource = False
    DataSet = QrDono
    BCDToCurrency = False
    Left = 12
    Top = 376
  end
  object QrUpdZ: TmySQLQuery
    Database = MyDB
    Left = 448
    Top = 8
  end
  object frxDsEndereco: TfrxDBDataset
    UserName = 'frxDsEndereco'
    CloseDataSource = False
    DataSet = QrEndereco
    BCDToCurrency = False
    Left = 76
    Top = 332
  end
  object QrWeb: TmySQLQuery
    Database = MyDBn
    Left = 104
    Top = 96
  end
  object MyDBn: TmySQLDatabase
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 108
    Top = 52
  end
  object QrUpdN: TmySQLQuery
    Database = MyDBn
    Left = 496
    Top = 8
  end
  object frxDsMaster: TfrxDBDataset
    UserName = 'frxDsMaster'
    CloseDataSource = False
    DataSet = QrMaster
    BCDToCurrency = False
    Left = 40
    Top = 144
  end
  object QrUpdW: TmySQLQuery
    Database = MyDB
    Left = 496
    Top = 48
  end
  object QrRecebiBloq: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM recebibloq'
      'WHERE Sincro=0')
    Left = 548
    Top = 336
    object QrRecebiBloqApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrRecebiBloqProtoco: TIntegerField
      FieldName = 'Protoco'
    end
    object QrRecebiBloqBloqueto: TIntegerField
      FieldName = 'Bloqueto'
    end
    object QrRecebiBloqEmeio: TIntegerField
      FieldName = 'Emeio'
    end
    object QrRecebiBloqEnvios: TIntegerField
      FieldName = 'Envios'
    end
    object QrRecebiBloqDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrRecebiBloqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrRecebiBloqAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrRecebiBloqSincro: TSmallintField
      FieldName = 'Sincro'
    end
  end
  object DsRecebiBloq: TDataSource
    DataSet = QrRecebiBloq
    Left = 644
    Top = 336
  end
  object QrEmeioProt: TmySQLQuery
    Database = MyDB
    BeforeClose = QrEmeioProtBeforeClose
    AfterScroll = QrEmeioProtAfterScroll
    SQL.Strings = (
      'SELECT ptc.Codigo, ptc.Nome, COUNT(ppi.Conta) ITENS'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'WHERE ppi.DataD = 0'
      'AND ptc.Tipo=2'
      'GROUP BY ptc.Codigo')
    Left = 548
    Top = 144
    object QrEmeioProtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmeioProtNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEmeioProtITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsEmeioProt: TDataSource
    DataSet = QrEmeioProt
    Left = 644
    Top = 144
  end
  object QrEmeioProtPak: TmySQLQuery
    Database = MyDB
    BeforeClose = QrEmeioProtPakBeforeClose
    AfterScroll = QrEmeioProtPakAfterScroll
    SQL.Strings = (
      'SELECT ptp.Controle, ptp.Mez, COUNT(ppi.Conta) ITENS,'
      'CONCAT(RIGHT(Mez, 2), '#39'/'#39',  LEFT(Mez + 200000, 4)) NOMEMEZ'
      'FROM protpakits ppi'
      'LEFT JOIN protocopak ptp ON ptp.Controle=ppi.Controle'
      'WHERE ppi.DataD = 0'
      'AND ppi.DataE > 1'
      'AND ptp.Codigo=:P0'
      'GROUP BY ptp.Controle')
    Left = 548
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmeioProtPakControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmeioProtPakMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmeioProtPakNOMEMEZ: TWideStringField
      FieldName = 'NOMEMEZ'
      Size = 7
    end
    object QrEmeioProtPakITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsEmeioProtPak: TDataSource
    DataSet = QrEmeioProtPak
    Left = 644
    Top = 192
  end
  object QrEmeioProtPakIts: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, '
      'civ.Unidade, civ.Codigo CODCOND, IF(ppi.DataE=0, "", '
      'DATE_FORMAT(ppi.DataE, "%d/%m/%y")) DATAE_TXT,'
      'ppi.Conta, ppi.DataE, ppi.Cliente, ppi.Depto, ppi.Docum,'
      'ppi.ID_Cod1, ppi.ID_Cod2, ppi.ID_Cod3, ppi.ID_Cod4, '
      'ppi.Vencto, ppi.Lancto'
      'FROM protpakits ppi'
      'LEFT JOIN entidades cli ON cli.Codigo=ppi.Cliente'
      'LEFT JOIN condimov civ ON civ.Conta=ppi.Depto'
      'WHERE ppi.DataD = 0'
      'AND ppi.DataE > 1'
      'AND ppi.Controle=:P0')
    Left = 548
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmeioProtPakItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEmeioProtPakItsDataE: TDateField
      FieldName = 'DataE'
      Required = True
    end
    object QrEmeioProtPakItsCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrEmeioProtPakItsDepto: TIntegerField
      FieldName = 'Depto'
      Required = True
    end
    object QrEmeioProtPakItsDocum: TFloatField
      FieldName = 'Docum'
      Required = True
    end
    object QrEmeioProtPakItsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrEmeioProtPakItsUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrEmeioProtPakItsDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 8
    end
    object QrEmeioProtPakItsID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Required = True
    end
    object QrEmeioProtPakItsID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Required = True
    end
    object QrEmeioProtPakItsID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Required = True
    end
    object QrEmeioProtPakItsID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Required = True
    end
    object QrEmeioProtPakItsVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmeioProtPakItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmeioProtPakItsCODCOND: TIntegerField
      FieldName = 'CODCOND'
    end
  end
  object DsEmeioProtPakIts: TDataSource
    DataSet = QrEmeioProtPakIts
    Left = 644
    Top = 240
  end
  object QrEmeioProtTot: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT COUNT(Conta) ITENS'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'WHERE ppi.DataD = 0'
      'AND ppi.DataE > 1'
      'AND ptc.Tipo=2')
    Left = 548
    Top = 288
    object QrEmeioProtTotITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsEmeioProtTot: TDataSource
    DataSet = QrEmeioProtTot
    Left = 644
    Top = 288
  end
  object QrCondEmeios: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      
        'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECOND, cem.*' +
        ' '
      'FROM condemeios cem'
      'LEFT JOIN cond cnd ON cnd.Codigo=cem.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE cem.Conta=:P0')
    Left = 828
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondEmeiosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondEmeiosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCondEmeiosConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCondEmeiosItem: TIntegerField
      FieldName = 'Item'
    end
    object QrCondEmeiosPronome: TWideStringField
      FieldName = 'Pronome'
    end
    object QrCondEmeiosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCondEmeiosEMeio: TWideStringField
      FieldName = 'EMeio'
      Size = 100
    end
    object QrCondEmeiosNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
  end
  object QrPPIMail: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT ppi.Conta PROTOCOLO, ppi.Controle LOTE, ppi.DataE, '
      
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DATAE_TX' +
        'T,'
      
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DATAD_TX' +
        'T,'
      'ppi.DataD, ppi.Cancelado, ppi.Motivo, ppi.ID_Cod1, ppi.ID_Cod2, '
      'ppi.ID_Cod3, ppi.ID_Cod4, ppi.CliInt, ppi.Cliente, ppi.Periodo,'
      
        'ppi.Docum, ptc.Codigo TAREFA_COD, ptc.Nome TAREFA_NOM, ptc.Def_S' +
        'ender,'
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ptc.PreEmeio'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN entidades  snd ON snd.Codigo=ptc.Def_Sender'
      'WHERE ppi.Link_ID=1'
      'AND ppi.ID_Cod1=:P0'
      'AND ppi.ID_Cod2=:P1'
      'AND ppi.ID_Cod3=:P2')
    Left = 736
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPPIMailPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Required = True
    end
    object QrPPIMailLOTE: TIntegerField
      FieldName = 'LOTE'
      Required = True
    end
    object QrPPIMailDataE: TDateField
      FieldName = 'DataE'
      Required = True
    end
    object QrPPIMailDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 10
    end
    object QrPPIMailDATAD_TXT: TWideStringField
      FieldName = 'DATAD_TXT'
      Size = 10
    end
    object QrPPIMailCancelado: TIntegerField
      FieldName = 'Cancelado'
      Required = True
    end
    object QrPPIMailMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrPPIMailID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Required = True
    end
    object QrPPIMailID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Required = True
    end
    object QrPPIMailID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Required = True
    end
    object QrPPIMailID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Required = True
    end
    object QrPPIMailCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPPIMailCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPPIMailPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrPPIMailDef_Sender: TIntegerField
      FieldName = 'Def_Sender'
    end
    object QrPPIMailDELIVER: TWideStringField
      FieldName = 'DELIVER'
      Size = 100
    end
    object QrPPIMailDocum: TFloatField
      FieldName = 'Docum'
      Required = True
    end
    object QrPPIMailPreEmeio: TIntegerField
      FieldName = 'PreEmeio'
    end
    object QrPPIMailDataD: TDateTimeField
      FieldName = 'DataD'
      Required = True
    end
    object QrPPIMailTAREFA_COD: TIntegerField
      FieldName = 'TAREFA_COD'
      Required = True
    end
    object QrPPIMailTAREFA_NOM: TWideStringField
      FieldName = 'TAREFA_NOM'
      Size = 100
    end
  end
  object QrBolArr: TmySQLQuery
    Database = MyDB
    Left = 828
    Top = 192
    object QrBolArrValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
    object QrBolArrApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolArrBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolArrBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrBolLei: TmySQLQuery
    Database = MyDB
    Left = 736
    Top = 240
    object QrBolLeiValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
    object QrBolLeiApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolLeiBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolLeiBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrBolMail: TmySQLQuery
    Database = MyDB
    OnCalcFields = QrBolMailCalcFields
    Left = 736
    Top = 192
    object QrBolMailPROTOCOD: TIntegerField
      FieldName = 'PROTOCOD'
    end
    object QrBolMailApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolMailPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrBolMailVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrBolMailUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrBolMailNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrBolMailBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolMailBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
    object QrBolMailSUB_ARR: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_ARR'
      LookupDataSet = QrBolArr
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBolMailSUB_LEI: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_LEI'
      LookupDataSet = QrBolLei
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBolMailSUB_TOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOT'
      Calculated = True
    end
  end
  object QrProtPg: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Registros'
      'FROM lct0000x'
      'WHERE Tipo=2'
      'AND FatID >=600'
      'AND Sit =0  '
      'AND Compensado <= 1'
      'AND Mez=:P0'
      'AND Cliente=:P1'
      'AND Depto=:P2'
      'AND Documento=:P3')
    Left = 548
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrProtPgRegistros: TLargeintField
      FieldName = 'Registros'
      Required = True
    end
  end
  object QrProtVct: TmySQLQuery
    Database = MyDB
    Left = 644
    Top = 384
  end
  object QrReenv: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, '
      'civ.Conta Apto, civ.Unidade, IF(ppi.DataE=0, "", '
      'DATE_FORMAT(ppi.DataE, "%d/%m/%y")) DATAE_TXT,'
      'ppi.*'
      'FROM protpakits ppi'
      'LEFT JOIN entidades cli ON cli.Codigo=ppi.Cliente'
      'LEFT JOIN condimov civ ON civ.Conta=ppi.Depto'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'WHERE ppi.DataD = 0'
      'AND ptc.Tipo=2'
      'AND ppi.DataE > 1')
    Left = 740
    Top = 340
    object QrReenvNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrReenvUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrReenvDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 8
    end
    object QrReenvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrReenvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrReenvConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrReenvDataE: TDateField
      FieldName = 'DataE'
    end
    object QrReenvDataD: TDateTimeField
      FieldName = 'DataD'
    end
    object QrReenvRetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object QrReenvCancelado: TIntegerField
      FieldName = 'Cancelado'
    end
    object QrReenvMotivo: TIntegerField
      FieldName = 'Motivo'
    end
    object QrReenvLink_ID: TIntegerField
      FieldName = 'Link_ID'
    end
    object QrReenvCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrReenvCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrReenvFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrReenvPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrReenvLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrReenvDocum: TFloatField
      FieldName = 'Docum'
    end
    object QrReenvDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrReenvID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
    end
    object QrReenvID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
    end
    object QrReenvID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
    end
    object QrReenvID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
    end
    object QrReenvCedente: TIntegerField
      FieldName = 'Cedente'
    end
    object QrReenvVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrReenvValor: TFloatField
      FieldName = 'Valor'
    end
    object QrReenvMoraDiaVal: TFloatField
      FieldName = 'MoraDiaVal'
    end
    object QrReenvMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrReenvComoConf: TSmallintField
      FieldName = 'ComoConf'
    end
    object QrReenvSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrReenvManual: TIntegerField
      FieldName = 'Manual'
    end
    object QrReenvTexto: TWideStringField
      FieldName = 'Texto'
      Size = 30
    end
    object QrReenvApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
  end
  object QrProtoMail_: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM protomail'
      'ORDER BY NivelEmail')
    Left = 736
    Top = 288
    object QrProtoMail_Depto_Cod: TIntegerField
      FieldName = 'Depto_Cod'
      Origin = 'protomail.Depto_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMail_Depto_Txt: TWideStringField
      FieldName = 'Depto_Txt'
      Origin = 'protomail.Depto_Txt'
      Size = 100
    end
    object QrProtoMail_Entid_Cod: TIntegerField
      FieldName = 'Entid_Cod'
      Origin = 'protomail.Entid_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMail_Entid_Txt: TWideStringField
      FieldName = 'Entid_Txt'
      Origin = 'protomail.Entid_Txt'
      Size = 100
    end
    object QrProtoMail_Taref_Cod: TIntegerField
      FieldName = 'Taref_Cod'
      Origin = 'protomail.Taref_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMail_Taref_Txt: TWideStringField
      FieldName = 'Taref_Txt'
      Origin = 'protomail.Taref_Txt'
      Size = 100
    end
    object QrProtoMail_Deliv_Cod: TIntegerField
      FieldName = 'Deliv_Cod'
      Origin = 'protomail.Deliv_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMail_Deliv_Txt: TWideStringField
      FieldName = 'Deliv_Txt'
      Origin = 'protomail.Deliv_Txt'
      Size = 100
    end
    object QrProtoMail_DataE: TDateField
      FieldName = 'DataE'
      Origin = 'protomail.DataE'
    end
    object QrProtoMail_DataE_Txt: TWideStringField
      FieldName = 'DataE_Txt'
      Origin = 'protomail.DataE_Txt'
      Size = 30
    end
    object QrProtoMail_DataD: TDateField
      FieldName = 'DataD'
      Origin = 'protomail.DataD'
    end
    object QrProtoMail_DataD_Txt: TWideStringField
      FieldName = 'DataD_Txt'
      Origin = 'protomail.DataD_Txt'
      Size = 30
    end
    object QrProtoMail_ProtoLote: TIntegerField
      FieldName = 'ProtoLote'
      Origin = 'protomail.ProtoLote'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMail_NivelEmail: TSmallintField
      FieldName = 'NivelEmail'
      Origin = 'protomail.NivelEmail'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMail_Item: TIntegerField
      FieldName = 'Item'
      Origin = 'protomail.Item'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMail_NivelDescr: TWideStringField
      FieldName = 'NivelDescr'
      Origin = 'protomail.NivelDescr'
      Size = 50
    end
    object QrProtoMail_Protocolo: TIntegerField
      FieldName = 'Protocolo'
      Origin = 'protomail.Protocolo'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMail_RecipEmeio: TWideStringField
      FieldName = 'RecipEmeio'
      Size = 100
    end
    object QrProtoMail_RecipNome: TWideStringField
      FieldName = 'RecipNome'
      Size = 100
    end
    object QrProtoMail_RecipProno: TWideStringField
      FieldName = 'RecipProno'
    end
    object QrProtoMail_RecipItem: TIntegerField
      FieldName = 'RecipItem'
    end
    object QrProtoMail_Bloqueto: TFloatField
      FieldName = 'Bloqueto'
    end
    object QrProtoMail_Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrProtoMail_Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrProtoMail_PreEmeio: TIntegerField
      FieldName = 'PreEmeio'
    end
    object QrProtoMail_Condominio: TWideStringField
      FieldName = 'Condominio'
      Size = 100
    end
    object QrProtoMail_IDEmeio: TIntegerField
      FieldName = 'IDEmeio'
    end
  end
  object QrUserTxts: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT LENGTH(TxtSys) Tam, ut.* '
      'FROM usertxts ut'
      'WHERE TxtSys <> TxtMeu'
      'ORDER BY ut.Ordem, Tam Desc')
    Left = 100
    Top = 144
    object QrUserTxtsTam: TLargeintField
      FieldName = 'Tam'
      Required = True
    end
    object QrUserTxtsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUserTxtsTxtSys: TWideStringField
      FieldName = 'TxtSys'
      Size = 255
    end
    object QrUserTxtsTxtMeu: TWideStringField
      FieldName = 'TxtMeu'
      Size = 255
    end
    object QrUserTxtsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object QrCB4Data4: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT Registro '
      'FROM cb4data4'
      'WHERE Empresa=?'
      'AND Tabela=?')
    Left = 968
    Top = 512
    object QrCB4Data4Registro: TIntegerField
      FieldName = 'Registro'
    end
  end
  object QrSomaM: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT (SUM(Credito) - SUM(Debito)) Valor'
      'FROM Lanctos')
    Left = 648
    Top = 55
    object QrSomaMValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
  end
  object QrAuxN: TmySQLQuery
    Database = MyDBn
    Left = 792
    Top = 39
  end
end
