object FmCondImovImp: TFmCondImovImp
  Left = 447
  Top = 247
  Caption = 'CAD-CONDO-012 :: Impress'#227'o de Im'#243'veis'
  ClientHeight = 522
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 360
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 360
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 137
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 480
          Height = 137
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 122
            Height = 13
            Caption = 'Cliente (Condom'#237'nio) [F4]:'
          end
          object Label2: TLabel
            Left = 8
            Top = 44
            Width = 86
            Height = 13
            Caption = 'T'#237'tulo do relat'#243'rio:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = EdEmpresaKeyDown
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 67
            Top = 20
            Width = 408
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            OnKeyDown = CBEmpresaKeyDown
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdTitulo: TEdit
            Left = 8
            Top = 60
            Width = 466
            Height = 21
            TabOrder = 2
          end
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 480
          Top = 0
          Width = 528
          Height = 137
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Filial'
              Title.Caption = 'Condom'#237'nio'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Entidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFILIAL'
              Title.Caption = 'Raz'#227'o Social'
              Width = 615
              Visible = True
            end>
          Color = clWindow
          DataSource = DModG.DsEmpresas
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = dmkDBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Filial'
              Title.Caption = 'Condom'#237'nio'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Entidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFILIAL'
              Title.Caption = 'Raz'#227'o Social'
              Width = 615
              Visible = True
            end>
        end
      end
      object PCTipoRel: TPageControl
        Left = 0
        Top = 137
        Width = 1008
        Height = 223
        ActivePage = TabSheet5
        Align = alClient
        MultiLine = True
        TabOrder = 1
        object TabSheet1: TTabSheet
          Caption = ' Lista de propriet'#225'rios + moradores '
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 195
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object RGImp0: TRadioGroup
              Left = 380
              Top = 76
              Width = 277
              Height = 105
              Caption = ' Impress'#227'o: '
              ItemIndex = 0
              Items.Strings = (
                'Retrato - Sem endere'#231'o'
                'Paisagem - Com endere'#231'o')
              TabOrder = 0
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Protocolo para propriet'#225'rios ou procuradores '
          ImageIndex = 1
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 195
            Align = alClient
            Caption = 'N'#227'o h'#225' configura'#231#245'es espec'#237'ficas para este relat'#243'rio'
            TabOrder = 0
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Lista simples de propriet'#225'rios ou moradores '
          ImageIndex = 2
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 195
            Align = alClient
            Caption = 'N'#227'o h'#225' configura'#231#245'es espec'#237'ficas para este relat'#243'rio'
            TabOrder = 0
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Lista simples de propriet'#225'rios ou procuradores '
          ImageIndex = 3
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 195
            Align = alClient
            TabOrder = 0
            object CkInadimplentes: TCheckBox
              Left = 15
              Top = 8
              Width = 226
              Height = 17
              Caption = 'Marcar unidade habitacionais inadimplentes'
              TabOrder = 0
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = ' Livro de Ocorr'#234'ncias '
          ImageIndex = 4
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 195
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel11: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 36
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label4: TLabel
                Left = 8
                Top = 12
                Width = 86
                Height = 13
                Caption = 'Texto do Per'#237'odo:'
              end
              object Label5: TLabel
                Left = 500
                Top = 12
                Width = 226
                Height = 13
                Caption = 'Quantidade de p'#225'ginas de observa'#231#245'es gerais: '
              end
              object EdPeriodo: TEdit
                Left = 100
                Top = 8
                Width = 397
                Height = 21
                TabOrder = 0
              end
              object EdQtdPagObsGer04: TdmkEdit
                Left = 728
                Top = 8
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '2'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 2
                ValWarn = False
              end
            end
            object Grade04: TStringGrid
              Left = 0
              Top = 36
              Width = 1000
              Height = 159
              Align = alClient
              ColCount = 13
              DefaultColWidth = 24
              DefaultRowHeight = 18
              RowCount = 4
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing, goRowMoving, goColMoving, goEditing, goAlwaysShowEditor]
              TabOrder = 1
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 266
        Height = 32
        Caption = 'Impress'#227'o de Im'#243'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 266
        Height = 32
        Caption = 'Impress'#227'o de Im'#243'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 266
        Height = 32
        Caption = 'Impress'#227'o de Im'#243'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 408
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 452
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 5
        Left = 20
        Top = 3
        Width = 90
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Tag = 5
        Left = 116
        Top = 3
        Width = 90
        Height = 40
        Caption = '&Config.'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
      object Panel13: TPanel
        Left = 893
        Top = 0
        Width = 111
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
  end
  object frx_000A: TfrxReport
    Version = '4.15'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 39664.693294085700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frx_000AGetValue
    Left = 72
    Top = 136
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader9: TfrxPageHeader
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape7: TfrxShapeView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Left = 75.590600000000000000
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 170.078850000000000000
          Top = 56.692950000000000000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 83.149660000000000000
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 661.417750000000000000
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo294: TfrxMemoView
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '* P = Propriet'#225'rio   -  M = Morador')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 120.944960000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 151.181200000000000000
          Top = 120.944960000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 321.260050000000000000
          Top = 120.944960000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emeio')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 139.842610000000000000
          Top = 120.944960000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '*')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 604.724800000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Telefone')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 680.315400000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 510.236550000000000000
          Top = 120.944960000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        Height = 37.795300000000000000
        Top = 219.212740000000000000
        Width = 793.701300000000000000
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
        RowCount = 0
        object Memo301: TfrxMemoView
          Left = 75.590600000000000000
          Width = 64.252010000000000000
          Height = 37.795300000000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."Unidade"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 151.181200000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMEPROP'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."NOMEPROP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 321.260050000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'EMEIO_PROP'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."EMEIO_PROP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'TEL1_PROP_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."TEL1_PROP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CELU_PROP_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CELU_PROP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMEUSUARIO'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."NOMEUSUARIO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 321.260050000000000000
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'EMEIO_USU'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."EMEIO_USU"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 604.724800000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'TEL1_USU_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."TEL1_USU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 680.315400000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CELU_USU_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CELU_USU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 139.842610000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'P')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 139.842610000000000000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 510.236550000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CNPJCPF_PROP_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CNPJCPF_PROP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CNPJCPF_USU_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CNPJCPF_USU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 49.133890000000000000
        Top = 317.480520000000000000
        Width = 793.701300000000000000
        object Memo17: TfrxMemoView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object QrImoveis: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrImoveisCalcFields
    SQL.Strings = (
      
        'SELECT 0.0 Inadimplente, con.Nome NOMECONJUGE, sta.Descri STATUS' +
        '1,'
      '  IF(imc.Propriet=0, "",'
      'IF(pro.Tipo=0, pro.RazaoSocial, pro.Nome)) NOMEPROP,'
      'IF (pro.Tipo=0, pro.CNPJ, pro.CPF) CNPJCPF_PROP,'
      'IF (pro.Tipo=0, pro.ETe1, pro.PTe1) TEL1_PROP,'
      'IF (pro.Tipo=0, pro.ETe2, pro.PTe2) TEL2_PROP,'
      'IF (pro.Tipo=0, pro.ECel, pro.PCel) CELU_PROP,'
      'IF (pro.Tipo=0, pro.EEmail, pro.PEmail) EMEIO_PROP,'
      'IF (pro.Tipo=0, pro.ELograd, pro.PLograd) + 0.000 LOGRAD_PROP,'
      'IF (pro.Tipo=0, pro.ERua, pro.PRua) RUA_PROP,'
      'IF (pro.Tipo=0, pro.ENumero, pro.PNumero) NUMERO_PROP,'
      'IF (pro.Tipo=0, pro.ECompl, pro.PCompl) COMPL_PROP,'
      'IF (pro.Tipo=0, pro.EBairro, pro.PBairro) BAIRRO_PROP,'
      'IF (pro.Tipo=0, pro.ECidade, pro.PCidade) CIDADE_PROP,'
      'IF (pro.Tipo=0, pro.EUF, pro.PUF) UF_PROP,'
      'IF (pro.Tipo=0, pro.ECEP, pro.PCEP) CEP_PROP,'
      'll1.Nome NO_PRO_LOGRAD, uf1.Nome NO_PRO_UF,'
      ''
      ''
      '  IF(imc.Procurador=0, "",'
      'IF(pcu.Tipo=0, pcu.RazaoSocial, pcu.Nome)) NOMEPROC,'
      'IF (pcu.Tipo=0, pcu.CNPJ, pcu.CPF) CNPJCPF_PROC,'
      'IF (pcu.Tipo=0, pcu.ETe1, pcu.PTe1) TEL1_PROC,'
      'IF (pcu.Tipo=0, pcu.ECel, pcu.PCel) CELU_PROC,'
      'IF (pcu.Tipo=0, pcu.EEmail, pcu.PEmail) EMEIO_PROC,'
      ''
      '  IF(imc.Usuario=0, "",'
      'IF (mor.Tipo=0, mor.RazaoSocial, mor.Nome)) NOMEUSUARIO,'
      'IF (mor.Tipo=0, mor.CNPJ, mor.CPF) CNPJCPF_USU,'
      'IF (mor.Tipo=0, mor.ETe1, mor.PTe1) TEL1_USU,'
      'IF (mor.Tipo=0, mor.ETe2, mor.PTe2) TEL2_USU,'
      'IF (mor.Tipo=0, mor.ECel, mor.PCel) CELU_USU,'
      'IF (mor.Tipo=0, mor.EEmail, mor.PEmail) EMEIO_USU,'
      'IF (mor.Tipo=0, mor.ELograd, mor.PLograd)  + 0.000 LOGRAD_USU,'
      'IF (mor.Tipo=0, mor.ERua, mor.PRua) RUA_USU,'
      'IF (mor.Tipo=0, mor.ENumero, mor.PNumero) NUMERO_USU,'
      'IF (mor.Tipo=0, mor.ECompl, mor.PCompl) COMPL_USU,'
      'IF (mor.Tipo=0, mor.EBairro, mor.PBairro) BAIRRO_USU,'
      'IF (mor.Tipo=0, mor.ECidade, mor.PCidade) CIDADE_USU,'
      'IF (mor.Tipo=0, mor.EUF, mor.PUF) UF_USU,'
      'IF (mor.Tipo=0, mor.ECEP, mor.PCEP) CEP_USU,'
      'll2.Nome NO_MOR_LOGRAD, uf2.Nome NO_MOR_UF,'
      ''
      'IF(imc.ENome1=0, "",'
      '  IF (ena.Tipo=0, ena.RazaoSocial, ena.Nome)) NOMEEMP1,'
      'IF(imc.ENome2=0, "",'
      '  IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome)) NOMEEMP2,'
      'IF(imc.ENome3=0, "",'
      '  IF (enc.Tipo=0, enc.RazaoSocial, enc.Nome)) NOMEEMP3,'
      'end.RazaoSocial IMOB, ptc.Nome NOMEPROTOCOLO, imc.*'
      'FROM condimov imc'
      'LEFT JOIN entidades pro ON pro.Codigo=imc.Propriet'
      'LEFT JOIN entidades mor ON mor.Codigo=imc.Usuario'
      'LEFT JOIN entidades pcu ON pcu.Codigo=imc.Procurador'
      'LEFT JOIN entidades con ON con.Codigo=imc.Conjuge'
      'LEFT JOIN entidades ena ON ena.Codigo=imc.ENome1'
      'LEFT JOIN entidades enb ON enb.Codigo=imc.ENome2'
      'LEFT JOIN entidades enc ON enc.Codigo=imc.ENome3'
      'LEFT JOIN status sta ON sta.Codigo=imc.Status'
      'LEFT JOIN entidades end ON end.Codigo=imc.Imobiliaria'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=imc.Protocolo'
      
        'LEFT JOIN listalograd ll1 ON ll1.Codigo=IF(pro.Tipo=0, pro.ELogr' +
        'ad, pro.PLograd)'
      'LEFT JOIN ufs uf1 ON uf1.Codigo=IF(pro.Tipo=0, pro.EUF, pro.PUF)'
      
        'LEFT JOIN listalograd ll2 ON ll2.Codigo=IF(mor.Tipo=0, mor.ELogr' +
        'ad, mor.PLograd)'
      'LEFT JOIN ufs uf2 ON uf2.Codigo=IF(mor.Tipo=0, mor.EUF, mor.PUF)'
      'WHERE imc.Codigo=:P0'
      'ORDER BY Codigo, Controle, Andar, Unidade'
      '')
    Left = 100
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImoveisNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrImoveisNOMECONJUGE: TWideStringField
      FieldName = 'NOMECONJUGE'
      Origin = 'entidades.Nome'
      Size = 100
    end
    object QrImoveisSTATUS1: TWideStringField
      FieldName = 'STATUS1'
      Origin = 'status.Descri'
      Size = 100
    end
    object QrImoveisNOMEEMP1: TWideStringField
      FieldName = 'NOMEEMP1'
      Size = 100
    end
    object QrImoveisNOMEEMP2: TWideStringField
      FieldName = 'NOMEEMP2'
      Size = 100
    end
    object QrImoveisNOMEEMP3: TWideStringField
      FieldName = 'NOMEEMP3'
      Size = 100
    end
    object QrImoveisIMOB: TWideStringField
      FieldName = 'IMOB'
      Origin = 'entidades.RazaoSocial'
      Size = 100
    end
    object QrImoveisCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'condimov.Codigo'
      Required = True
    end
    object QrImoveisControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'condimov.Controle'
      Required = True
    end
    object QrImoveisConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'condimov.Conta'
      Required = True
    end
    object QrImoveisPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'condimov.Propriet'
      Required = True
    end
    object QrImoveisConjuge: TIntegerField
      FieldName = 'Conjuge'
      Origin = 'condimov.Conjuge'
      Required = True
    end
    object QrImoveisAndar: TIntegerField
      FieldName = 'Andar'
      Origin = 'condimov.Andar'
      Required = True
    end
    object QrImoveisUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Required = True
      Size = 10
    end
    object QrImoveisQtdGaragem: TIntegerField
      FieldName = 'QtdGaragem'
      Origin = 'condimov.QtdGaragem'
      Required = True
    end
    object QrImoveisStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'condimov.Status'
      Required = True
    end
    object QrImoveisENome1: TIntegerField
      FieldName = 'ENome1'
      Origin = 'condimov.ENome1'
      Required = True
    end
    object QrImoveisEHoraSSIni1: TTimeField
      FieldName = 'EHoraSSIni1'
      Origin = 'condimov.EHoraSSIni1'
    end
    object QrImoveisEHoraSSSai1: TTimeField
      FieldName = 'EHoraSSSai1'
      Origin = 'condimov.EHoraSSSai1'
    end
    object QrImoveisEHoraSDIni1: TTimeField
      FieldName = 'EHoraSDIni1'
      Origin = 'condimov.EHoraSDIni1'
    end
    object QrImoveisEHoraSDSai1: TTimeField
      FieldName = 'EHoraSDSai1'
      Origin = 'condimov.EHoraSDSai1'
    end
    object QrImoveisESegunda1: TSmallintField
      FieldName = 'ESegunda1'
      Origin = 'condimov.ESegunda1'
    end
    object QrImoveisETerca1: TSmallintField
      FieldName = 'ETerca1'
      Origin = 'condimov.ETerca1'
    end
    object QrImoveisEQuarta1: TSmallintField
      FieldName = 'EQuarta1'
      Origin = 'condimov.EQuarta1'
    end
    object QrImoveisEQuinta1: TSmallintField
      FieldName = 'EQuinta1'
      Origin = 'condimov.EQuinta1'
    end
    object QrImoveisESexta1: TSmallintField
      FieldName = 'ESexta1'
      Origin = 'condimov.ESexta1'
    end
    object QrImoveisESabado1: TSmallintField
      FieldName = 'ESabado1'
      Origin = 'condimov.ESabado1'
    end
    object QrImoveisEDomingo1: TSmallintField
      FieldName = 'EDomingo1'
      Origin = 'condimov.EDomingo1'
    end
    object QrImoveisEVeic1: TSmallintField
      FieldName = 'EVeic1'
      Origin = 'condimov.EVeic1'
    end
    object QrImoveisEFilho1: TSmallintField
      FieldName = 'EFilho1'
      Origin = 'condimov.EFilho1'
    end
    object QrImoveisENome2: TIntegerField
      FieldName = 'ENome2'
      Origin = 'condimov.ENome2'
      Required = True
    end
    object QrImoveisEHoraSSIni2: TTimeField
      FieldName = 'EHoraSSIni2'
      Origin = 'condimov.EHoraSSIni2'
    end
    object QrImoveisEHoraSSSai2: TTimeField
      FieldName = 'EHoraSSSai2'
      Origin = 'condimov.EHoraSSSai2'
    end
    object QrImoveisEHoraSDIni2: TTimeField
      FieldName = 'EHoraSDIni2'
      Origin = 'condimov.EHoraSDIni2'
    end
    object QrImoveisEHoraSDSai2: TTimeField
      FieldName = 'EHoraSDSai2'
      Origin = 'condimov.EHoraSDSai2'
    end
    object QrImoveisESegunda2: TSmallintField
      FieldName = 'ESegunda2'
      Origin = 'condimov.ESegunda2'
    end
    object QrImoveisETerca2: TSmallintField
      FieldName = 'ETerca2'
      Origin = 'condimov.ETerca2'
    end
    object QrImoveisEQuarta2: TSmallintField
      FieldName = 'EQuarta2'
      Origin = 'condimov.EQuarta2'
    end
    object QrImoveisEQuinta2: TSmallintField
      FieldName = 'EQuinta2'
      Origin = 'condimov.EQuinta2'
    end
    object QrImoveisESexta2: TSmallintField
      FieldName = 'ESexta2'
      Origin = 'condimov.ESexta2'
    end
    object QrImoveisESabado2: TSmallintField
      FieldName = 'ESabado2'
      Origin = 'condimov.ESabado2'
    end
    object QrImoveisEDomingo2: TSmallintField
      FieldName = 'EDomingo2'
      Origin = 'condimov.EDomingo2'
    end
    object QrImoveisEVeic2: TSmallintField
      FieldName = 'EVeic2'
      Origin = 'condimov.EVeic2'
    end
    object QrImoveisEFilho2: TSmallintField
      FieldName = 'EFilho2'
      Origin = 'condimov.EFilho2'
    end
    object QrImoveisENome3: TIntegerField
      FieldName = 'ENome3'
      Origin = 'condimov.ENome3'
      Required = True
    end
    object QrImoveisEHoraSSIni3: TTimeField
      FieldName = 'EHoraSSIni3'
      Origin = 'condimov.EHoraSSIni3'
    end
    object QrImoveisEHoraSSSai3: TTimeField
      FieldName = 'EHoraSSSai3'
      Origin = 'condimov.EHoraSSSai3'
    end
    object QrImoveisEHoraSDIni3: TTimeField
      FieldName = 'EHoraSDIni3'
      Origin = 'condimov.EHoraSDIni3'
    end
    object QrImoveisEHoraSDSai3: TTimeField
      FieldName = 'EHoraSDSai3'
      Origin = 'condimov.EHoraSDSai3'
    end
    object QrImoveisESegunda3: TSmallintField
      FieldName = 'ESegunda3'
      Origin = 'condimov.ESegunda3'
    end
    object QrImoveisETerca3: TSmallintField
      FieldName = 'ETerca3'
      Origin = 'condimov.ETerca3'
    end
    object QrImoveisEQuarta3: TSmallintField
      FieldName = 'EQuarta3'
      Origin = 'condimov.EQuarta3'
    end
    object QrImoveisEQuinta3: TSmallintField
      FieldName = 'EQuinta3'
      Origin = 'condimov.EQuinta3'
    end
    object QrImoveisESexta3: TSmallintField
      FieldName = 'ESexta3'
      Origin = 'condimov.ESexta3'
    end
    object QrImoveisESabado3: TSmallintField
      FieldName = 'ESabado3'
      Origin = 'condimov.ESabado3'
    end
    object QrImoveisEDomingo3: TSmallintField
      FieldName = 'EDomingo3'
      Origin = 'condimov.EDomingo3'
    end
    object QrImoveisEVeic3: TSmallintField
      FieldName = 'EVeic3'
      Origin = 'condimov.EVeic3'
    end
    object QrImoveisEFilho3: TSmallintField
      FieldName = 'EFilho3'
      Origin = 'condimov.EFilho3'
    end
    object QrImoveisEmNome1: TWideStringField
      FieldName = 'EmNome1'
      Origin = 'condimov.EmNome1'
      Required = True
      Size = 100
    end
    object QrImoveisEmTel1: TWideStringField
      FieldName = 'EmTel1'
      Origin = 'condimov.EmTel1'
    end
    object QrImoveisEmNome2: TWideStringField
      FieldName = 'EmNome2'
      Origin = 'condimov.EmNome2'
      Required = True
      Size = 100
    end
    object QrImoveisEmTel2: TWideStringField
      FieldName = 'EmTel2'
      Origin = 'condimov.EmTel2'
    end
    object QrImoveisObserv: TWideMemoField
      FieldName = 'Observ'
      Origin = 'condimov.Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrImoveisSitImv: TIntegerField
      FieldName = 'SitImv'
      Origin = 'condimov.SitImv'
      Required = True
    end
    object QrImoveisLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'condimov.Lk'
    end
    object QrImoveisDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'condimov.DataCad'
    end
    object QrImoveisDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'condimov.DataAlt'
    end
    object QrImoveisUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'condimov.UserCad'
    end
    object QrImoveisUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'condimov.UserAlt'
    end
    object QrImoveisImobiliaria: TIntegerField
      FieldName = 'Imobiliaria'
      Origin = 'condimov.Imobiliaria'
      Required = True
    end
    object QrImoveisContato: TWideStringField
      FieldName = 'Contato'
      Origin = 'condimov.Contato'
      Required = True
      Size = 60
    end
    object QrImoveisContTel: TWideStringField
      FieldName = 'ContTel'
      Origin = 'condimov.ContTel'
      Required = True
    end
    object QrImoveisNOMESITIMV: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESITIMV'
      Size = 3
      Calculated = True
    end
    object QrImoveisUsuario: TIntegerField
      FieldName = 'Usuario'
      Origin = 'condimov.Usuario'
      Required = True
    end
    object QrImoveisNOMEUSUARIO: TWideStringField
      FieldName = 'NOMEUSUARIO'
      Size = 100
    end
    object QrImoveisProtocolo: TIntegerField
      FieldName = 'Protocolo'
      Required = True
    end
    object QrImoveisNOMEPROTOCOLO: TWideStringField
      FieldName = 'NOMEPROTOCOLO'
      Size = 100
    end
    object QrImoveisTEL1_PROP: TWideStringField
      FieldName = 'TEL1_PROP'
    end
    object QrImoveisCELU_PROP: TWideStringField
      FieldName = 'CELU_PROP'
    end
    object QrImoveisEMEIO_PROP: TWideStringField
      FieldName = 'EMEIO_PROP'
      Size = 100
    end
    object QrImoveisTEL1_USU: TWideStringField
      FieldName = 'TEL1_USU'
    end
    object QrImoveisCELU_USU: TWideStringField
      FieldName = 'CELU_USU'
    end
    object QrImoveisEMEIO_USU: TWideStringField
      FieldName = 'EMEIO_USU'
      Size = 100
    end
    object QrImoveisTEL1_PROP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_PROP_TXT'
      Size = 40
      Calculated = True
    end
    object QrImoveisCELU_PROP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CELU_PROP_TXT'
      Size = 40
      Calculated = True
    end
    object QrImoveisTEL1_USU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_USU_TXT'
      Size = 40
      Calculated = True
    end
    object QrImoveisCELU_USU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CELU_USU_TXT'
      Size = 40
      Calculated = True
    end
    object QrImoveisCNPJCPF_PROP: TWideStringField
      FieldName = 'CNPJCPF_PROP'
      Size = 18
    end
    object QrImoveisCNPJCPF_USU: TWideStringField
      FieldName = 'CNPJCPF_USU'
      Size = 18
    end
    object QrImoveisCNPJCPF_PROP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_PROP_TXT'
      Size = 30
      Calculated = True
    end
    object QrImoveisCNPJCPF_USU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_USU_TXT'
      Size = 30
      Calculated = True
    end
    object QrImoveisNOMEPROC: TWideStringField
      FieldName = 'NOMEPROC'
      Size = 100
    end
    object QrImoveisCNPJCPF_PROC: TWideStringField
      FieldName = 'CNPJCPF_PROC'
      Size = 18
    end
    object QrImoveisTEL1_PROC: TWideStringField
      FieldName = 'TEL1_PROC'
    end
    object QrImoveisCELU_PROC: TWideStringField
      FieldName = 'CELU_PROC'
    end
    object QrImoveisEMEIO_PROC: TWideStringField
      FieldName = 'EMEIO_PROC'
      Size = 100
    end
    object QrImoveisProcurador: TIntegerField
      FieldName = 'Procurador'
    end
    object QrImoveisTEL1_PROC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_PROC_TXT'
      Size = 40
      Calculated = True
    end
    object QrImoveisCELU_PROC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CELU_PROC_TXT'
      Size = 50
      Calculated = True
    end
    object QrImoveisCNPJCPF_PROC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_PROC_TXT'
      Size = 30
      Calculated = True
    end
    object QrImoveisQUEM_TIPO_A: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'QUEM_TIPO_A'
      Size = 1
      Calculated = True
    end
    object QrImoveisQUEM_TIPO_B: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'QUEM_TIPO_B'
      Size = 1
      Calculated = True
    end
    object QrImoveisNOME_TIPO_A: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_A'
      Size = 100
      Calculated = True
    end
    object QrImoveisNOME_TIPO_B: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_B'
      Size = 100
      Calculated = True
    end
    object QrImoveisCNPJCPF_TIPO_A: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_TIPO_A'
      Size = 40
      Calculated = True
    end
    object QrImoveisCNPJCPF_TIPO_B: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_TIPO_B'
      Size = 40
      Calculated = True
    end
    object QrImoveisTEL2_PROP: TWideStringField
      FieldName = 'TEL2_PROP'
    end
    object QrImoveisTEL2_USU: TWideStringField
      FieldName = 'TEL2_USU'
    end
    object QrImoveisTEL2_PROP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL2_PROP_TXT'
      Size = 40
      Calculated = True
    end
    object QrImoveisTEL2_USU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL2_USU_TXT'
      Size = 40
      Calculated = True
    end
    object QrImoveisE_ALL_PRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL_PRO'
      Size = 255
      Calculated = True
    end
    object QrImoveisE_ALL_MOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL_MOR'
      Size = 255
      Calculated = True
    end
    object QrImoveisRUA_PROP: TWideStringField
      FieldName = 'RUA_PROP'
      Size = 30
    end
    object QrImoveisCOMPL_PROP: TWideStringField
      FieldName = 'COMPL_PROP'
      Size = 30
    end
    object QrImoveisBAIRRO_PROP: TWideStringField
      FieldName = 'BAIRRO_PROP'
      Size = 30
    end
    object QrImoveisCIDADE_PROP: TWideStringField
      FieldName = 'CIDADE_PROP'
      Size = 25
    end
    object QrImoveisNO_PRO_LOGRAD: TWideStringField
      FieldName = 'NO_PRO_LOGRAD'
      Size = 10
    end
    object QrImoveisNO_PRO_UF: TWideStringField
      FieldName = 'NO_PRO_UF'
      Size = 2
    end
    object QrImoveisRUA_USU: TWideStringField
      FieldName = 'RUA_USU'
      Size = 30
    end
    object QrImoveisCOMPL_USU: TWideStringField
      FieldName = 'COMPL_USU'
      Size = 30
    end
    object QrImoveisBAIRRO_USU: TWideStringField
      FieldName = 'BAIRRO_USU'
      Size = 30
    end
    object QrImoveisCIDADE_USU: TWideStringField
      FieldName = 'CIDADE_USU'
      Size = 25
    end
    object QrImoveisNO_MOR_LOGRAD: TWideStringField
      FieldName = 'NO_MOR_LOGRAD'
      Size = 10
    end
    object QrImoveisNO_MOR_UF: TWideStringField
      FieldName = 'NO_MOR_UF'
      Size = 2
    end
    object QrImoveisWebLogin: TWideStringField
      FieldName = 'WebLogin'
      Size = 30
    end
    object QrImoveisWebPwd: TWideStringField
      FieldName = 'WebPwd'
      Size = 30
    end
    object QrImoveisWebNivel: TSmallintField
      FieldName = 'WebNivel'
    end
    object QrImoveisWebLogID: TWideStringField
      FieldName = 'WebLogID'
      Size = 32
    end
    object QrImoveisWebLastLog: TDateTimeField
      FieldName = 'WebLastLog'
    end
    object QrImoveisAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrImoveisAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrImoveisMoradores: TIntegerField
      FieldName = 'Moradores'
    end
    object QrImoveisFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
    end
    object QrImoveisModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrImoveisConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrImoveisBloqEndTip: TSmallintField
      FieldName = 'BloqEndTip'
    end
    object QrImoveisBloqEndEnt: TIntegerField
      FieldName = 'BloqEndEnt'
    end
    object QrImoveisEnderLin1: TWideStringField
      FieldName = 'EnderLin1'
      Size = 100
    end
    object QrImoveisEnderLin2: TWideStringField
      FieldName = 'EnderLin2'
      Size = 100
    end
    object QrImoveisEnderNome: TWideStringField
      FieldName = 'EnderNome'
      Size = 100
    end
    object QrImoveisInadimplente: TFloatField
      FieldName = 'Inadimplente'
      Required = True
    end
    object QrImoveisINADCHECK: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'INADCHECK'
      Calculated = True
    end
    object QrImoveisLOGRAD_PROP: TFloatField
      FieldName = 'LOGRAD_PROP'
    end
    object QrImoveisLOGRAD_USU: TFloatField
      FieldName = 'LOGRAD_USU'
    end
    object QrImoveisNUMERO_PROP: TFloatField
      FieldName = 'NUMERO_PROP'
    end
    object QrImoveisNUMERO_USU: TFloatField
      FieldName = 'NUMERO_USU'
    end
    object QrImoveisCEP_PROP: TFloatField
      FieldName = 'CEP_PROP'
    end
    object QrImoveisCEP_USU: TFloatField
      FieldName = 'CEP_USU'
    end
    object QrImoveisUF_PROP: TFloatField
      FieldName = 'UF_PROP'
    end
    object QrImoveisUF_USU: TFloatField
      FieldName = 'UF_USU'
    end
  end
  object frxDsImoveis: TfrxDBDataset
    UserName = 'frxDsImoveis'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEPROP=NOMEPROP'
      'NOMECONJUGE=NOMECONJUGE'
      'STATUS1=STATUS1'
      'NOMEEMP1=NOMEEMP1'
      'NOMEEMP2=NOMEEMP2'
      'NOMEEMP3=NOMEEMP3'
      'IMOB=IMOB'
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'Propriet=Propriet'
      'Conjuge=Conjuge'
      'Andar=Andar'
      'Unidade=Unidade'
      'QtdGaragem=QtdGaragem'
      'Status=Status'
      'ENome1=ENome1'
      'EHoraSSIni1=EHoraSSIni1'
      'EHoraSSSai1=EHoraSSSai1'
      'EHoraSDIni1=EHoraSDIni1'
      'EHoraSDSai1=EHoraSDSai1'
      'ESegunda1=ESegunda1'
      'ETerca1=ETerca1'
      'EQuarta1=EQuarta1'
      'EQuinta1=EQuinta1'
      'ESexta1=ESexta1'
      'ESabado1=ESabado1'
      'EDomingo1=EDomingo1'
      'EVeic1=EVeic1'
      'EFilho1=EFilho1'
      'ENome2=ENome2'
      'EHoraSSIni2=EHoraSSIni2'
      'EHoraSSSai2=EHoraSSSai2'
      'EHoraSDIni2=EHoraSDIni2'
      'EHoraSDSai2=EHoraSDSai2'
      'ESegunda2=ESegunda2'
      'ETerca2=ETerca2'
      'EQuarta2=EQuarta2'
      'EQuinta2=EQuinta2'
      'ESexta2=ESexta2'
      'ESabado2=ESabado2'
      'EDomingo2=EDomingo2'
      'EVeic2=EVeic2'
      'EFilho2=EFilho2'
      'ENome3=ENome3'
      'EHoraSSIni3=EHoraSSIni3'
      'EHoraSSSai3=EHoraSSSai3'
      'EHoraSDIni3=EHoraSDIni3'
      'EHoraSDSai3=EHoraSDSai3'
      'ESegunda3=ESegunda3'
      'ETerca3=ETerca3'
      'EQuarta3=EQuarta3'
      'EQuinta3=EQuinta3'
      'ESexta3=ESexta3'
      'ESabado3=ESabado3'
      'EDomingo3=EDomingo3'
      'EVeic3=EVeic3'
      'EFilho3=EFilho3'
      'EmNome1=EmNome1'
      'EmTel1=EmTel1'
      'EmNome2=EmNome2'
      'EmTel2=EmTel2'
      'Observ=Observ'
      'SitImv=SitImv'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'Imobiliaria=Imobiliaria'
      'Contato=Contato'
      'ContTel=ContTel'
      'NOMESITIMV=NOMESITIMV'
      'Usuario=Usuario'
      'NOMEUSUARIO=NOMEUSUARIO'
      'Protocolo=Protocolo'
      'NOMEPROTOCOLO=NOMEPROTOCOLO'
      'TEL1_PROP=TEL1_PROP'
      'CELU_PROP=CELU_PROP'
      'EMEIO_PROP=EMEIO_PROP'
      'TEL1_USU=TEL1_USU'
      'CELU_USU=CELU_USU'
      'EMEIO_USU=EMEIO_USU'
      'TEL1_PROP_TXT=TEL1_PROP_TXT'
      'CELU_PROP_TXT=CELU_PROP_TXT'
      'TEL1_USU_TXT=TEL1_USU_TXT'
      'CELU_USU_TXT=CELU_USU_TXT'
      'CNPJCPF_PROP=CNPJCPF_PROP'
      'CNPJCPF_USU=CNPJCPF_USU'
      'CNPJCPF_PROP_TXT=CNPJCPF_PROP_TXT'
      'CNPJCPF_USU_TXT=CNPJCPF_USU_TXT'
      'NOMEPROC=NOMEPROC'
      'CNPJCPF_PROC=CNPJCPF_PROC'
      'TEL1_PROC=TEL1_PROC'
      'CELU_PROC=CELU_PROC'
      'EMEIO_PROC=EMEIO_PROC'
      'Procurador=Procurador'
      'TEL1_PROC_TXT=TEL1_PROC_TXT'
      'CELU_PROC_TXT=CELU_PROC_TXT'
      'CNPJCPF_PROC_TXT=CNPJCPF_PROC_TXT'
      'QUEM_TIPO_A=QUEM_TIPO_A'
      'QUEM_TIPO_B=QUEM_TIPO_B'
      'NOME_TIPO_A=NOME_TIPO_A'
      'NOME_TIPO_B=NOME_TIPO_B'
      'CNPJCPF_TIPO_A=CNPJCPF_TIPO_A'
      'CNPJCPF_TIPO_B=CNPJCPF_TIPO_B'
      'TEL2_PROP=TEL2_PROP'
      'TEL2_USU=TEL2_USU'
      'TEL2_PROP_TXT=TEL2_PROP_TXT'
      'TEL2_USU_TXT=TEL2_USU_TXT'
      'E_ALL_PRO=E_ALL_PRO'
      'E_ALL_MOR=E_ALL_MOR'
      'RUA_PROP=RUA_PROP'
      'COMPL_PROP=COMPL_PROP'
      'BAIRRO_PROP=BAIRRO_PROP'
      'CIDADE_PROP=CIDADE_PROP'
      'NO_PRO_LOGRAD=NO_PRO_LOGRAD'
      'NO_PRO_UF=NO_PRO_UF'
      'RUA_USU=RUA_USU'
      'COMPL_USU=COMPL_USU'
      'BAIRRO_USU=BAIRRO_USU'
      'CIDADE_USU=CIDADE_USU'
      'NO_MOR_LOGRAD=NO_MOR_LOGRAD'
      'NO_MOR_UF=NO_MOR_UF'
      'WebLogin=WebLogin'
      'WebPwd=WebPwd'
      'WebNivel=WebNivel'
      'WebLogID=WebLogID'
      'WebLastLog=WebLastLog'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Moradores=Moradores'
      'FracaoIdeal=FracaoIdeal'
      'ModelBloq=ModelBloq'
      'ConfigBol=ConfigBol'
      'BloqEndTip=BloqEndTip'
      'BloqEndEnt=BloqEndEnt'
      'EnderLin1=EnderLin1'
      'EnderLin2=EnderLin2'
      'EnderNome=EnderNome'
      'Inadimplente=Inadimplente'
      'INADCHECK=INADCHECK'
      'LOGRAD_PROP=LOGRAD_PROP'
      'LOGRAD_USU=LOGRAD_USU'
      'NUMERO_PROP=NUMERO_PROP'
      'NUMERO_USU=NUMERO_USU'
      'CEP_PROP=CEP_PROP'
      'CEP_USU=CEP_USU'
      'UF_PROP=UF_PROP'
      'UF_USU=UF_USU')
    DataSet = QrImoveis
    BCDToCurrency = False
    Left = 128
    Top = 136
  end
  object frx_001: TfrxReport
    Version = '4.15'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 39664.693294085700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frx_000AGetValue
    Left = 352
    Top = 132
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader9: TfrxPageHeader
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape7: TfrxShapeView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Left = 75.590600000000000000
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 170.078850000000000000
          Top = 56.692950000000000000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 83.149660000000000000
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 661.417750000000000000
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo294: TfrxMemoView
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '* D = Propriet'#225'rio   -  O = Procurador')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 120.944960000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 151.181200000000000000
          Top = 120.944960000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 566.929500000000000000
          Top = 120.944960000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Assinatura')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 139.842610000000000000
          Top = 120.944960000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '*')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 415.748300000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 491.338900000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 321.260050000000000000
          Top = 120.944960000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        Height = 37.795300000000000000
        Top = 219.212740000000000000
        Width = 793.701300000000000000
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
        RowCount = 0
        object Memo301: TfrxMemoView
          Left = 75.590600000000000000
          Width = 64.252010000000000000
          Height = 37.795300000000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."Unidade"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 151.181200000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMEPROP'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."NOMEPROP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 566.929500000000000000
          Width = 188.976500000000000000
          Height = 37.795300000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 415.748300000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 491.338900000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."NOMEPROC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 415.748300000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 491.338900000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 139.842610000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'D')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 139.842610000000000000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'O')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 321.260050000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CNPJCPF_PROP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 321.260050000000000000
          Top = 18.897650000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CNPJCPF_PROC_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 49.133890000000000000
        Top = 317.480520000000000000
        Width = 793.701300000000000000
        object Memo17: TfrxMemoView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frx_002: TfrxReport
    Version = '4.15'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 39664.693294085700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frx_000AGetValue
    Left = 380
    Top = 132
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader9: TfrxPageHeader
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape7: TfrxShapeView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Left = 75.590600000000000000
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 170.078850000000000000
          Top = 56.692950000000000000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 83.149660000000000000
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 661.417750000000000000
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo294: TfrxMemoView
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '* P = Propriet'#225'rio   -  M = Morador')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 120.944960000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 151.181200000000000000
          Top = 120.944960000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 566.929500000000000000
          Top = 120.944960000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Assinatura')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 139.842610000000000000
          Top = 120.944960000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '*')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 415.748300000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 491.338900000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 321.260050000000000000
          Top = 120.944960000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        Height = 18.897650000000000000
        Top = 219.212740000000000000
        Width = 793.701300000000000000
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
        RowCount = 0
        object Memo301: TfrxMemoView
          Left = 75.590600000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."Unidade"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 151.181200000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOME_TIPO_A'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."NOME_TIPO_A"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 566.929500000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 415.748300000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 491.338900000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 139.842610000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'QUEM_TIPO_A'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."QUEM_TIPO_A"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 321.260050000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CNPJCPF_TIPO_A'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CNPJCPF_TIPO_A"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 49.133890000000000000
        Top = 298.582870000000000000
        Width = 793.701300000000000000
        object Memo17: TfrxMemoView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frx_003: TfrxReport
    Version = '4.15'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 41002.484167662030000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frx_000AGetValue
    Left = 408
    Top = 132
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader9: TfrxPageHeader
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape7: TfrxShapeView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Left = 75.590600000000000000
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 170.078850000000000000
          Top = 56.692950000000000000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 83.149660000000000000
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 661.417750000000000000
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo294: TfrxMemoView
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '* D = Propriet'#225'rio   -  O = Procurador')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 120.944960000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 151.181200000000000000
          Top = 120.944960000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 566.929500000000000000
          Top = 120.944960000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Assinatura')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 139.842610000000000000
          Top = 120.944960000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '*')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 415.748300000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 491.338900000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 321.260050000000000000
          Top = 120.944960000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        Height = 18.897650000000000000
        Top = 219.212740000000000000
        Width = 793.701300000000000000
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
        RowCount = 0
        object Memo301: TfrxMemoView
          Left = 75.590600000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."Unidade"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 151.181200000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOME_TIPO_B'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."NOME_TIPO_B"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 566.929500000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 415.748300000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 491.338900000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 139.842610000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'QUEM_TIPO_B'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."QUEM_TIPO_B"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 321.260050000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CNPJCPF_TIPO_B'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CNPJCPF_TIPO_B"]')
          ParentFont = False
          WordWrap = False
        end
        object CkInad: TfrxCheckBoxView
          Left = 56.692950000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCross
          DataField = 'INADCHECK'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          Frame.Width = 0.100000000000000000
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 49.133890000000000000
        Top = 298.582870000000000000
        Width = 793.701300000000000000
        object Memo17: TfrxMemoView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frx_004: TfrxReport
    Version = '4.15'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40183.784731354200000000
    ReportOptions.LastChange = 40183.784731354200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MasterData2.RowCount := <QtdPagObsGer>;                       ' +
        '                                 '
      'end.')
    OnGetValue = frx_000AGetValue
    Left = 436
    Top = 132
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader1: TfrxPageHeader
        Height = 1077.166050000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          Left = 18.897650000000000000
          Top = 56.692950000000000000
          Width = 755.906000000000000000
          Height = 113.385900000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 18.897650000000000000
          Top = 925.984850000000000000
          Width = 755.906000000000000000
          Height = 132.283550000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LIVRO DE OCORR'#202'NCIAS MENSAIS'
            ''
            #9#9'[PERIODO]')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 1028.032160000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
        RowCount = 0
        object Shape7: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 94.488250000000000000
          Top = 18.897650000000000000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 585.827150000000000000
          Top = 18.897650000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 90.708720000000000000
          Width = 68.031540000000000000
          Height = 49.133890000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#202'S')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 139.842610000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dezembro')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 68.031540000000000000
          Top = 90.708720000000000000
          Width = 102.047244090000000000
          Height = 49.133890000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TITULO_01_A]'
            '[TITULO_01_B]'
            '[TITULO_01_C]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade: [frxDsImoveis."Unidade"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Top = 177.637910000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Janeiro')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Top = 215.433210000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fevereiro')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Top = 253.228510000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mar'#231'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Top = 291.023810000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Abril')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Top = 328.819110000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Maio')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Top = 366.614410000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Junho')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Top = 404.409710000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Julho')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Top = 442.205010000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Agosto')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Top = 480.000310000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Setembro')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Top = 517.795610000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Outubro')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Top = 555.590910000000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Novembro')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Top = 593.385824330000000000
          Width = 68.031496060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dezembro')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 170.078850000000000000
          Top = 90.708720000000000000
          Width = 102.047244090000000000
          Height = 49.133890000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TITULO_02_A]'
            '[TITULO_02_B]'
            '[TITULO_02_C]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 272.126160000000000000
          Top = 90.708720000000000000
          Width = 102.047244090000000000
          Height = 49.133890000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TITULO_03_A]'
            '[TITULO_03_B]'
            '[TITULO_03_C]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 374.173470000000000000
          Top = 90.708720000000000000
          Width = 102.047244090000000000
          Height = 49.133890000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TITULO_04_A]'
            '[TITULO_04_B]'
            '[TITULO_04_C]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 476.220780000000000000
          Top = 90.708720000000000000
          Width = 102.047244090000000000
          Height = 49.133890000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TITULO_05_A]'
            '[TITULO_05_B]'
            '[TITULO_05_C]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 578.268090000000000000
          Top = 90.708720000000000000
          Width = 102.047244090000000000
          Height = 49.133890000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TITULO_06_A]'
            '[TITULO_06_B]'
            '[TITULO_06_C]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 68.031540000000000000
          Top = 139.842610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 68.031540000000000000
          Top = 177.637910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 68.031540000000000000
          Top = 215.433210000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 68.031540000000000000
          Top = 253.228510000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 68.031540000000000000
          Top = 291.023810000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 68.031540000000000000
          Top = 328.819110000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 68.031540000000000000
          Top = 366.614410000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 68.031540000000000000
          Top = 404.409710000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 68.031540000000000000
          Top = 442.205010000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 68.031540000000000000
          Top = 480.000310000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 68.031540000000000000
          Top = 517.795610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 68.031540000000000000
          Top = 555.590910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 68.031540000000000000
          Top = 593.385824330000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 170.078850000000000000
          Top = 139.842610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 170.078850000000000000
          Top = 177.637910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 170.078850000000000000
          Top = 215.433210000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 170.078850000000000000
          Top = 253.228510000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 170.078850000000000000
          Top = 291.023810000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 170.078850000000000000
          Top = 328.819110000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 170.078850000000000000
          Top = 366.614410000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 170.078850000000000000
          Top = 404.409710000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 170.078850000000000000
          Top = 442.205010000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 170.078850000000000000
          Top = 480.000310000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 170.078850000000000000
          Top = 517.795610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 170.078850000000000000
          Top = 555.590910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 170.078850000000000000
          Top = 593.385824330000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 272.126160000000000000
          Top = 139.842610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 272.126160000000000000
          Top = 177.637910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 272.126160000000000000
          Top = 215.433210000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 272.126160000000000000
          Top = 253.228510000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 272.126160000000000000
          Top = 291.023810000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 272.126160000000000000
          Top = 328.819110000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 272.126160000000000000
          Top = 366.614410000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 272.126160000000000000
          Top = 404.409710000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 272.126160000000000000
          Top = 442.205010000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 272.126160000000000000
          Top = 480.000310000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 272.126160000000000000
          Top = 517.795610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 272.126160000000000000
          Top = 555.590910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 272.126160000000000000
          Top = 593.385824330000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 374.173470000000000000
          Top = 139.842610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 374.173470000000000000
          Top = 177.637910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 374.173470000000000000
          Top = 215.433210000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 374.173470000000000000
          Top = 253.228510000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 374.173470000000000000
          Top = 291.023810000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 374.173470000000000000
          Top = 328.819110000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 374.173470000000000000
          Top = 366.614410000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 374.173470000000000000
          Top = 404.409710000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 374.173470000000000000
          Top = 442.205010000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 374.173470000000000000
          Top = 480.000310000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 374.173470000000000000
          Top = 517.795610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 374.173470000000000000
          Top = 555.590910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 374.173470000000000000
          Top = 593.385824330000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 476.220780000000000000
          Top = 139.842610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 476.220780000000000000
          Top = 177.637910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 476.220780000000000000
          Top = 215.433210000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 476.220780000000000000
          Top = 253.228510000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 476.220780000000000000
          Top = 291.023810000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 476.220780000000000000
          Top = 328.819110000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 476.220780000000000000
          Top = 366.614410000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Left = 476.220780000000000000
          Top = 404.409710000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 476.220780000000000000
          Top = 442.205010000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 476.220780000000000000
          Top = 480.000310000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 476.220780000000000000
          Top = 517.795610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 476.220780000000000000
          Top = 555.590910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 476.220780000000000000
          Top = 593.385824330000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 578.268090000000000000
          Top = 139.842610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Left = 578.268090000000000000
          Top = 177.637910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Left = 578.268090000000000000
          Top = 215.433210000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 578.268090000000000000
          Top = 253.228510000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Left = 578.268090000000000000
          Top = 291.023810000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Left = 578.268090000000000000
          Top = 328.819110000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 578.268090000000000000
          Top = 366.614410000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Left = 578.268090000000000000
          Top = 404.409710000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 578.268090000000000000
          Top = 442.205010000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Left = 578.268090000000000000
          Top = 480.000310000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Left = 578.268090000000000000
          Top = 517.795610000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 578.268090000000000000
          Top = 555.590910000000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 578.268090000000000000
          Top = 593.385824330000000000
          Width = 102.047266060000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Top = 631.181510000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Anota'#231#245'es:')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Top = 684.094930000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Top = 737.008350000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Top = 789.921770000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Top = 842.835190000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Top = 895.748610000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Top = 948.662030000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Top = 1001.575450000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaBottom
        end
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData2: TfrxMasterData
        Height = 1028.032160000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo112: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Anota'#231#245'es gerais:')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Top = 128.504020000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Top = 181.417440000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Top = 234.330860000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Top = 287.244280000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Top = 340.157700000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Top = 393.071120000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Top = 445.984540000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Top = 498.897960000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Top = 1001.575450000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo122: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo123: TfrxMemoView
          Left = 94.488250000000000000
          Top = 18.897650000000000000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          Left = 585.827150000000000000
          Top = 18.897650000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Top = 551.811380000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Top = 604.724800000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Top = 657.638220000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Top = 710.551640000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Top = 763.465060000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Top = 816.378480000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Top = 869.291900000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Top = 922.205320000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Top = 975.118740000000000000
          Width = 680.315356060000000000
          Height = 26.456692910000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
    end
  end
  object frx_000B: TfrxReport
    Version = '4.15'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 39664.693294085700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frx_000AGetValue
    Left = 156
    Top = 136
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      object PageHeader9: TfrxPageHeader
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 1122.520410000000000000
        object Shape7: TfrxShapeView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 982.677800000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 967.559680000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Left = 75.590600000000000000
          Top = 56.692950000000000000
          Width = 982.677800000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 170.078850000000000000
          Top = 56.692950000000000000
          Width = 793.701300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 83.149660000000000000
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 963.780150000000000000
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 801.260360000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo294: TfrxMemoView
          Left = 876.850960000000000000
          Top = 79.370130000000000000
          Width = 181.417440000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '* P = Propriet'#225'rio   -  M = Morador')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 98.267780000000000000
          Width = 52.913385826771650000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 139.842610000000000000
          Top = 98.267780000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 309.921460000000000000
          Top = 98.267780000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emeio')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 128.504020000000000000
          Top = 98.267780000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '*')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 593.386210000000000000
          Top = 98.267780000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Telefone 1')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 668.976810000000000000
          Top = 98.267780000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Telefone 2')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 498.897960000000000000
          Top = 98.267780000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 744.567410000000000000
          Top = 98.267780000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 820.158010000000000000
          Top = 98.267780000000000000
          Width = 238.110341180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Endere'#231'o')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 124.724490000000000000
          Width = 1133.859000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
      object MasterData12: TfrxMasterData
        Height = 37.795300000000000000
        Top = 219.212740000000000000
        Width = 1122.520410000000000000
        DataSet = frxDsImoveis
        DataSetName = 'frxDsImoveis'
        RowCount = 0
        object Memo301: TfrxMemoView
          Left = 75.590600000000000000
          Width = 52.913385826771650000
          Height = 37.795300000000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."Unidade"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 139.842610000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMEPROP'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."NOMEPROP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 309.921460000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'EMEIO_PROP'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."EMEIO_PROP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 593.386210000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'TEL1_PROP_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."TEL1_PROP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 668.976810000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."TEL2_PROP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 139.842610000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMEUSUARIO'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."NOMEUSUARIO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 309.921460000000000000
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'EMEIO_USU'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."EMEIO_USU"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 593.386210000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."TEL1_USU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 668.976810000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'TEL2_USU_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."TEL2_USU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 128.504020000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'P')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 128.504020000000000000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 498.897960000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CNPJCPF_PROP_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CNPJCPF_PROP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 498.897960000000000000
          Top = 18.897650000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CNPJCPF_USU_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CNPJCPF_USU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 744.567410000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CELU_PROP_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CELU_PROP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 744.567410000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CELU_USU_TXT'
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsImoveis."CELU_USU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 820.158010000000000000
          Width = 238.110341180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."E_ALL_PRO"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 820.158010000000000000
          Top = 18.897650000000000000
          Width = 238.110341180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsImoveis
          DataSetName = 'frxDsImoveis'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsImoveis."E_ALL_MOR"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 49.133890000000000000
        Top = 317.480520000000000000
        Width = 1122.520410000000000000
        object Memo17: TfrxMemoView
          Left = 377.953000000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
end
