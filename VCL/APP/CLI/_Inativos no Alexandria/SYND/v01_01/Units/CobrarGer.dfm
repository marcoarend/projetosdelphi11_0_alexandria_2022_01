object FmCobrarGer: TFmCobrarGer
  Left = 339
  Top = 185
  Caption = 'CIA-GEREN-001 :: Cobran'#231'a de Inadimplentes Automatizada'
  ClientHeight = 633
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 505
        Height = 32
        Caption = 'Cobran'#231'a de Inadimplentes Automatizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 505
        Height = 32
        Caption = 'Cobran'#231'a de Inadimplentes Automatizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 505
        Height = 32
        Caption = 'Cobran'#231'a de Inadimplentes Automatizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 457
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 457
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 457
        Align = alClient
        TabOrder = 0
        object Panel11: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 50
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label2: TLabel
            Left = 4
            Top = 0
            Width = 60
            Height = 13
            Caption = 'Condom'#237'nio:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 4
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 60
            Top = 16
            Width = 470
            Height = 21
            Color = clWhite
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CkForcaPsq: TCheckBox
            Left = 540
            Top = 20
            Width = 181
            Height = 17
            Caption = 'For'#231'ar pesquisa de inadimplentes.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object dmkEditDateTimePicker1: TdmkEditDateTimePicker
            Left = 784
            Top = 8
            Width = 186
            Height = 21
            Date = 41619.499693935170000000
            Time = 41619.499693935170000000
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
        object DBGPesq: TdmkDBGrid
          Left = 2
          Top = 65
          Width = 1004
          Height = 283
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Empresa'
              Title.Caption = 'C'#243'd.'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEEMPCOND'
              Title.Caption = 'Condom'#237'nio'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPRPIMOV'
              Title.Caption = 'Propriet'#225'rio'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Juridico_TXT'
              Title.Caption = 'SJ'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CREDITO'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MEZ_TXT'
              Title.Caption = 'M'#234's'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PAGO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Title.Caption = 'Saldo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Juridico_DESCRI'
              Title.Caption = 'Situa'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end>
          Color = clWindow
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Empresa'
              Title.Caption = 'C'#243'd.'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEEMPCOND'
              Title.Caption = 'Condom'#237'nio'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPRPIMOV'
              Title.Caption = 'Propriet'#225'rio'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Juridico_TXT'
              Title.Caption = 'SJ'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CREDITO'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MEZ_TXT'
              Title.Caption = 'M'#234's'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PAGO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Title.Caption = 'Saldo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Juridico_DESCRI'
              Title.Caption = 'Situa'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end>
        end
        object Memo5: TMemo
          Left = 2
          Top = 348
          Width = 1004
          Height = 107
          Align = alBottom
          TabOrder = 2
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 505
    Width = 1008
    Height = 58
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 41
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 24
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 563
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInadimp: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Inadimpl'#234'ncias'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtInadimpClick
      end
      object BtAgendar: TBitBtn
        Tag = 20
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Agendar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAgendarClick
      end
      object BtEmails: TBitBtn
        Tag = 406
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Emails'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtEmailsClick
      end
      object BtSMS: TBitBtn
        Tag = 518
        Left = 384
        Top = 4
        Width = 120
        Height = 40
        Caption = '&SMS'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtSMSClick
      end
      object BtCartas: TBitBtn
        Tag = 156
        Left = 508
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Cartas'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtCartasClick
      end
    end
  end
  object QrProtTip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imv.Conta, imv.Protocolo, ptc.Tipo'
      'FROM condimov imv'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=imv.Protocolo'
      'WHERE imv.Conta=:P0')
    Left = 280
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProtTipConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrProtTipProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrProtTipTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrLista: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT cnd.Cliente, dci.Controle DiarCEMIts, dci.SMS, '
      'dci.Email, dci.Carta, dci.Dias, dci.Dias + dci.DdNoEnv Fim,'
      
        'DATE_SUB(SYSDATE(), INTERVAL dci.Dias + dci.DdNoEnv DAY)  DATA_I' +
        ','
      'DATE_SUB(SYSDATE(), INTERVAL dci.Dias DAY) DATA_F,'
      'imv.InadCEMCad, ina.* '
      'FROM _inadimp_ ina'
      'LEFT JOIN syndic.condimov imv ON imv.Conta=ina.Apto'
      'LEFT JOIN syndic.cond cnd ON cnd.Codigo=imv.Codigo'
      'LEFT JOIN syndic.diarcemits dci ON dci.Codigo=imv.InadCEMCad'
      'WHERE imv.InadCEMCad > 0'
      'AND dci.Dias > 0'
      'AND ina.Vencimento BETWEEN '
      '  DATE_SUB(SYSDATE(), INTERVAL (dci.Dias + dci.DdNoEnv) DAY) '
      '  AND DATE_SUB(SYSDATE(), INTERVAL dci.Dias DAY)  '
      'ORDER BY Vencimento;'
      ''
      ''
      '')
    Left = 252
    Top = 192
    object QrListaDiarCEMIts: TIntegerField
      FieldName = 'DiarCEMIts'
    end
    object QrListaSMS: TIntegerField
      FieldName = 'SMS'
    end
    object QrListaEmail: TIntegerField
      FieldName = 'Email'
    end
    object QrListaCarta: TIntegerField
      FieldName = 'Carta'
    end
    object QrListaInadCEMCad: TIntegerField
      FieldName = 'InadCEMCad'
    end
    object QrListaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrListaForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrListaApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrListaMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrListaVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrListaFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrListaPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrListaUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrListaImobiliaria: TIntegerField
      FieldName = 'Imobiliaria'
    end
    object QrListaProcurador: TIntegerField
      FieldName = 'Procurador'
    end
    object QrListaUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrListaJuridico: TSmallintField
      FieldName = 'Juridico'
    end
    object QrListaData: TDateField
      FieldName = 'Data'
    end
    object QrListaCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrListaCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
    object QrListaPAGO: TFloatField
      FieldName = 'PAGO'
    end
    object QrListaJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrListaMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrListaTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object QrListaSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrListaPEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
    end
    object QrListaDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrListaMEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Size = 7
    end
    object QrListaCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrListaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrListaVCTO_TXT: TWideStringField
      FieldName = 'VCTO_TXT'
      Size = 10
    end
    object QrListaNOMEEMPCOND: TWideStringField
      FieldName = 'NOMEEMPCOND'
      Size = 100
    end
    object QrListaNOMEPRPIMOV: TWideStringField
      FieldName = 'NOMEPRPIMOV'
      Size = 100
    end
    object QrListaNewVencto: TDateField
      FieldName = 'NewVencto'
    end
    object QrListaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrListaDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrListaFim: TLargeintField
      FieldName = 'Fim'
    end
    object QrListaDATA_I: TDateTimeField
      FieldName = 'DATA_I'
    end
    object QrListaDATA_F: TDateTimeField
      FieldName = 'DATA_F'
    end
    object QrListaCliente: TIntegerField
      FieldName = 'Cliente'
    end
  end
  object QrCtrlGeral: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctrlgeral'
      '')
    Left = 224
    Top = 192
    object QrCtrlGeralUltPsqCobr: TDateField
      FieldName = 'UltPsqCobr'
    end
  end
  object QrLocBol: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM diarcembol'
      'WHERE Vencimento=:P0'
      'AND Data=:P0'
      'AND Mez=:p0'
      'AND Apto=:P0'
      'AND FatNum=:P0'
      '')
    Left = 196
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocBolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrEnvSMS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT bol.Vencimento, bol.Data, Bol.Mez, Bol.FatNum, '
      'Bol.Apto, sms.Nome TEXTO, env.* '
      'FROM diarcemenv env'
      'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo'
      'LEFT JOIN txtsms sms ON sms.Codigo=env.Cadastro'
      'WHERE env.Metodo=1 '
      'AND bol.Juridica=0'
      'AND env.Envio < "1900-01-01" '
      'AND env.DtaLimite >= SYSDATE() '
      '')
    Left = 195
    Top = 220
    object QrEnvSMSPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrEnvSMSVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'diarcembol.Vencimento'
    end
    object QrEnvSMSData: TDateField
      FieldName = 'Data'
      Origin = 'diarcembol.Data'
    end
    object QrEnvSMSMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'diarcembol.Mez'
    end
    object QrEnvSMSFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'diarcembol.FatNum'
    end
    object QrEnvSMSDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'diarcembol.Apto'
    end
    object QrEnvSMSCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'diarcemenv.Codigo'
    end
    object QrEnvSMSDiarCEMIts: TIntegerField
      FieldName = 'DiarCEMIts'
      Origin = 'diarcemenv.DiarCEMIts'
    end
    object QrEnvSMSMetodo: TSmallintField
      FieldName = 'Metodo'
      Origin = 'diarcemenv.Metodo'
    end
    object QrEnvSMSReInclu: TIntegerField
      FieldName = 'ReInclu'
      Origin = 'diarcemenv.ReInclu'
    end
    object QrEnvSMSCadastro: TIntegerField
      FieldName = 'Cadastro'
      Origin = 'diarcemenv.Cadastro'
    end
    object QrEnvSMSDtaInicio: TDateField
      FieldName = 'DtaInicio'
      Origin = 'diarcemenv.DtaInicio'
    end
    object QrEnvSMSDtaLimite: TDateField
      FieldName = 'DtaLimite'
      Origin = 'diarcemenv.DtaLimite'
    end
    object QrEnvSMSTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Origin = 'txtsms.Nome'
      Size = 140
    end
    object QrEnvSMSTentativas: TIntegerField
      FieldName = 'Tentativas'
      Origin = 'diarcemenv.Tentativas'
    end
    object QrEnvSMSLastExec: TDateTimeField
      FieldName = 'LastExec'
      Origin = 'diarcemenv.LastExec'
    end
  end
  object QrEnti: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imv.SMSCelTipo, imv.SMSCelNumr, imv.SMSCelNome,'
      'imv.Usuario, imv.Propriet,'
      'IF(usu.Tipo=0, usu.ECel, usu.PCel) CELULAR_USU, '
      'IF(prp.Tipo=0, prp.ECel, prp.PCel) CELULAR_PRP, '
      'IF(ter.Tipo=0, ter.ECel, ter.PCel) CELULAR_TER '
      'FROM condimov imv'
      'LEFT JOIN entidades usu ON usu.Codigo=imv.Usuario'
      'LEFT JOIN entidades prp ON prp.Codigo=imv.Propriet'
      'LEFT JOIN entidades ter ON ter.Codigo=imv.SMSCelEnti'
      'WHERE imv.Conta=808'
      ''
      '')
    Left = 224
    Top = 220
    object QrEntiUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrEntiPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrEntiCELULAR_USU: TWideStringField
      FieldName = 'CELULAR_USU'
    end
    object QrEntiCELULAR_PRP: TWideStringField
      FieldName = 'CELULAR_PRP'
    end
    object QrEntiCELULAR_TER: TWideStringField
      FieldName = 'CELULAR_TER'
    end
    object QrEntiSMSCelNome: TWideStringField
      FieldName = 'SMSCelNome'
      Size = 60
    end
    object QrEntiSMSCelNumr: TWideStringField
      FieldName = 'SMSCelNumr'
      Size = 18
    end
    object QrEntiSMSCelTipo: TSmallintField
      FieldName = 'SMSCelTipo'
    end
  end
  object QrEnvEmail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT bol.Vencimento, bol.Data, Bol.Mez, Bol.FatNum, '
      'Bol.Apto, env.* '
      'FROM diarcemenv env'
      'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo'
      'WHERE env.Metodo=2'
      'AND bol.Juridica=0'
      'AND env.ConsidEnvi < "1900-01-01" '
      'AND env.DtaLimite >= SYSDATE() '
      ''
      '')
    Left = 195
    Top = 248
    object QrEnvEmailPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrEnvEmailVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrEnvEmailData: TDateField
      FieldName = 'Data'
    end
    object QrEnvEmailMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEnvEmailFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEnvEmailDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEnvEmailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnvEmailDiarCEMIts: TIntegerField
      FieldName = 'DiarCEMIts'
    end
    object QrEnvEmailMetodo: TSmallintField
      FieldName = 'Metodo'
    end
    object QrEnvEmailReInclu: TIntegerField
      FieldName = 'ReInclu'
    end
    object QrEnvEmailCadastro: TIntegerField
      FieldName = 'Cadastro'
    end
    object QrEnvEmailDtaInicio: TDateField
      FieldName = 'DtaInicio'
    end
    object QrEnvEmailDtaLimite: TDateField
      FieldName = 'DtaLimite'
    end
    object QrEnvEmailEnvio: TDateTimeField
      FieldName = 'Envio'
    end
    object QrEnvEmailPqNoEnv: TIntegerField
      FieldName = 'PqNoEnv'
    end
    object QrEnvEmailTentativas: TIntegerField
      FieldName = 'Tentativas'
    end
    object QrEnvEmailLastExec: TDateTimeField
      FieldName = 'LastExec'
    end
    object QrEnvEmailConsidEnvi: TDateTimeField
      FieldName = 'ConsidEnvi'
    end
  end
  object QrEmails: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Pronome, Nome, Emeio '
      'FROM condemeios'
      'WHERE Conta>0'
      '')
    Left = 224
    Top = 248
    object QrEmailsPronome: TWideStringField
      FieldName = 'Pronome'
    end
    object QrEmailsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEmailsEmeio: TWideStringField
      FieldName = 'Emeio'
      Size = 100
    end
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Cliente'
      'FROM Cond'
      '')
    Left = 68
    Top = 16
    object QrCondCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondCliente: TIntegerField
      FieldName = 'Cliente'
    end
  end
  object QrPesq3: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT *'
      'FROM _inadimp_'
      '')
    Left = 12
    Top = 16
    object QrPesq3Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = '_inadimp_.Empresa'
    end
    object QrPesq3ForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = '_inadimp_.ForneceI'
    end
    object QrPesq3Apto: TIntegerField
      FieldName = 'Apto'
      Origin = '_inadimp_.Apto'
    end
    object QrPesq3Mez: TIntegerField
      FieldName = 'Mez'
      Origin = '_inadimp_.Mez'
    end
    object QrPesq3Vencimento: TDateField
      FieldName = 'Vencimento'
      Origin = '_inadimp_.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3FatNum: TFloatField
      FieldName = 'FatNum'
      Origin = '_inadimp_.FatNum'
    end
    object QrPesq3Propriet: TIntegerField
      FieldName = 'Propriet'
      Origin = '_inadimp_.Propriet'
    end
    object QrPesq3Unidade: TWideStringField
      FieldName = 'Unidade'
      Origin = '_inadimp_.Unidade'
      Size = 10
    end
    object QrPesq3Imobiliaria: TIntegerField
      FieldName = 'Imobiliaria'
      Origin = '_inadimp_.Imobiliaria'
    end
    object QrPesq3Procurador: TIntegerField
      FieldName = 'Procurador'
      Origin = '_inadimp_.Procurador'
    end
    object QrPesq3Usuario: TIntegerField
      FieldName = 'Usuario'
      Origin = '_inadimp_.Usuario'
    end
    object QrPesq3Juridico: TSmallintField
      FieldName = 'Juridico'
      Origin = '_inadimp_.Juridico'
    end
    object QrPesq3Data: TDateField
      FieldName = 'Data'
      Origin = '_inadimp_.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3CliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = '_inadimp_.CliInt'
    end
    object QrPesq3CREDITO: TFloatField
      FieldName = 'CREDITO'
      Origin = '_inadimp_.CREDITO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3PAGO: TFloatField
      FieldName = 'PAGO'
      Origin = '_inadimp_.PAGO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3Juros: TFloatField
      FieldName = 'Juros'
      Origin = '_inadimp_.Juros'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3Multa: TFloatField
      FieldName = 'Multa'
      Origin = '_inadimp_.Multa'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3TOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = '_inadimp_.TOTAL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3SALDO: TFloatField
      FieldName = 'SALDO'
      Origin = '_inadimp_.SALDO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3PEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
      Origin = '_inadimp_.PEND_VAL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3Descricao: TWideStringField
      FieldName = 'Descricao'
      Origin = '_inadimp_.Descricao'
      Size = 100
    end
    object QrPesq3MEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Origin = '_inadimp_.MEZ_TXT'
      Size = 7
    end
    object QrPesq3Compensado: TDateField
      FieldName = 'Compensado'
      Origin = '_inadimp_.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3Controle: TIntegerField
      FieldName = 'Controle'
      Origin = '_inadimp_.Controle'
    end
    object QrPesq3VCTO_TXT: TWideStringField
      FieldName = 'VCTO_TXT'
      Origin = '_inadimp_.VCTO_TXT'
      Size = 10
    end
    object QrPesq3NOMEEMPCOND: TWideStringField
      FieldName = 'NOMEEMPCOND'
      Origin = '_inadimp_.NOMEEMPCOND'
      Size = 100
    end
    object QrPesq3NOMEPRPIMOV: TWideStringField
      FieldName = 'NOMEPRPIMOV'
      Origin = '_inadimp_.NOMEPRPIMOV'
      Size = 100
    end
    object QrPesq3NewVencto: TDateField
      FieldName = 'NewVencto'
      Origin = '_inadimp_.NewVencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = '_inadimp_.Ativo'
    end
    object QrPesq3Juridico_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Juridico_TXT'
      Size = 2
      Calculated = True
    end
    object QrPesq3Juridico_DESCRI: TWideStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'Juridico_DESCRI'
      Size = 50
      Calculated = True
    end
  end
  object DsPesq3: TDataSource
    DataSet = QrPesq3
    Left = 40
    Top = 16
  end
end
