unit Cons;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, Menus, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, dmkGeral,
  Variants, dmkDBGrid, UnDmkProcFunc, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmCons = class(TForm)
    PainelDados: TPanel;
    DsCons: TDataSource;
    QrCons: TmySQLQuery;
    QrConsLk: TIntegerField;
    QrConsDataCad: TDateField;
    QrConsDataAlt: TDateField;
    QrConsUserCad: TIntegerField;
    QrConsUserAlt: TIntegerField;
    QrConsCodigo: TSmallintField;
    QrConsNome: TWideStringField;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    PainelCab: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    PainelData: TPanel;
    DBGPeriodo: TDBGrid;
    QrConsPer: TmySQLQuery;
    DsConsPer: TDataSource;
    QrConsIts: TmySQLQuery;
    DsConsIts: TDataSource;
    QrConsPerCodigo: TIntegerField;
    QrConsPerControle: TIntegerField;
    QrConsPerPeriodo: TIntegerField;
    QrConsPerLk: TIntegerField;
    QrConsPerDataCad: TDateField;
    QrConsPerDataAlt: TDateField;
    QrConsPerUserCad: TIntegerField;
    QrConsPerUserAlt: TIntegerField;
    PMProduto: TPopupMenu;
    Incluinovoproduto1: TMenuItem;
    Alteraprodutoatual1: TMenuItem;
    Excluiprodutoatual1: TMenuItem;
    PMPeriodo: TPopupMenu;
    Incluinovoperodo1: TMenuItem;
    QrDup: TmySQLQuery;
    QrDupPeriodo: TIntegerField;
    QrConsPerPERIODO_TXT: TWideStringField;
    QrConsItsCodigo: TIntegerField;
    QrConsItsControle: TIntegerField;
    QrConsItsConta: TIntegerField;
    QrConsItsApto: TIntegerField;
    QrConsItsMedAnt: TFloatField;
    QrConsItsMedAtu: TFloatField;
    QrConsItsLk: TIntegerField;
    QrConsItsDataCad: TDateField;
    QrConsItsDataAlt: TDateField;
    QrConsItsUserCad: TIntegerField;
    QrConsItsUserAlt: TIntegerField;
    DBGrid1: TDBGrid;
    N1: TMenuItem;
    Excluiperodo1: TMenuItem;
    PMLeitura: TPopupMenu;
    Adicionarcondomnio1: TMenuItem;
    N2: TMenuItem;
    EdNome: TdmkEdit;
    QrConsPrc: TmySQLQuery;
    DsConsPrc: TDataSource;
    PMCond: TPopupMenu;
    Incluicondomnio1: TMenuItem;
    Alteradadosdocondomnioselecionado1: TMenuItem;
    PnCond: TPanel;
    Panel2: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    EdDBCodigo: TDBEdit;
    EdDBNome: TDBEdit;
    Panel6: TPanel;
    DBGCond: TdmkDBGrid;
    QrConsPrcNOMECLI: TWideStringField;
    QrConsPrcCodigo: TIntegerField;
    QrConsPrcControle: TIntegerField;
    QrConsPrcCond: TIntegerField;
    QrConsPrcPreco: TFloatField;
    QrConsPrcLk: TIntegerField;
    QrConsPrcDataCad: TDateField;
    QrConsPrcDataAlt: TDateField;
    QrConsPrcUserCad: TIntegerField;
    QrConsPrcUserAlt: TIntegerField;
    QrConsPrcCasas: TSmallintField;
    QrConsPrcUnidLei: TWideStringField;
    QrConsPrcUnidImp: TWideStringField;
    QrConsPrcUnidFat: TFloatField;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    Label3: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    QrConsNOMEGENERO: TWideStringField;
    QrConsGenero: TIntegerField;
    EdDBGenero: TDBEdit;
    Label5: TLabel;
    Label8: TLabel;
    DBEdGenero: TDBEdit;
    QrConsPrcContaBase: TIntegerField;
    GroupBox2: TGroupBox;
    RGFatorBase: TRadioGroup;
    RGPerioBase: TRadioGroup;
    EdContaBase: TdmkEditCB;
    CBContaBase: TdmkDBLookupComboBox;
    Label14: TLabel;
    GroupBox1: TGroupBox;
    Label12: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    LaFator: TLabel;
    EdUnidFat: TdmkEdit;
    EdUnidImp: TdmkEdit;
    EdUnidLei: TdmkEdit;
    RGCasas: TRadioGroup;
    LaPreco: TLabel;
    EdPreco: TdmkEdit;
    QrConsPrcFatorBase: TSmallintField;
    RGCasRat: TRadioGroup;
    QrConsPrcCasRat: TSmallintField;
    Label15: TLabel;
    EdArredonda: TdmkEdit;
    QrConsPrcArredonda: TFloatField;
    CkNaoImpLei: TCheckBox;
    QrConsPrcNaoImpLei: TSmallintField;
    Label16: TLabel;
    QrConsPrcPerioBase: TSmallintField;
    QrConsPrcCarencia: TFloatField;
    GroupBox4: TGroupBox;
    EdCarencia: TdmkEdit;
    Label17: TLabel;
    CkDifCaren: TCheckBox;
    QrConsPrcDifCaren: TSmallintField;
    GroupBox5: TGroupBox;
    RGExport_Apl: TRadioGroup;
    Label18: TLabel;
    EdExport_Med: TdmkEdit;
    EdExport_Med_TXT: TEdit;
    RGExport_Tip: TRadioGroup;
    QrConsExport_Tip: TSmallintField;
    QrConsPrcExport_Apl: TSmallintField;
    QrConsPrcExport_Med: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel7: TPanel;
    BtSaida: TBitBtn;
    BtProduto: TBitBtn;
    BtCond: TBitBtn;
    BtPeriodo: TBitBtn;
    BtLeitura: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GroupBox6: TGroupBox;
    Panel9: TPanel;
    Panel10: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    SpeedButton5: TSpeedButton;
    EdCNAB_Cfg: TdmkEditCB;
    Label23: TLabel;
    SpeedButton6: TSpeedButton;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    QrLoc: TmySQLQuery;
    DsCNAB_Cfg: TDataSource;
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    QrConsPrcCNAB_Cfg: TIntegerField;
    QrConsPrcCNAB_Cfg_Txt: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrConsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrConsAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrConsBeforeOpen(DataSet: TDataSet);
    procedure Incluinovoproduto1Click(Sender: TObject);
    procedure Alteraprodutoatual1Click(Sender: TObject);
    procedure BtProdutoClick(Sender: TObject);
    procedure BtPeriodoClick(Sender: TObject);
    procedure Incluinovoperodo1Click(Sender: TObject);
    procedure QrConsPerCalcFields(DataSet: TDataSet);
    procedure QrConsPerAfterScroll(DataSet: TDataSet);
    procedure PMPeriodoPopup(Sender: TObject);
    procedure Excluiperodo1Click(Sender: TObject);
    procedure Incluicondomnio1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtCondClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Alteradadosdocondomnioselecionado1Click(Sender: TObject);
    procedure EdUnidImpChange(Sender: TObject);
    procedure EdUnidLeiChange(Sender: TObject);
    procedure DBGCondDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EdExport_MedChange(Sender: TObject);
    procedure RGExport_AplClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenQrConsPer(Periodo: Integer);
    procedure ReopenQrConsPrc(Controle: Integer);
    procedure ReopenQrConsIts(Conta: Integer);
    //
    procedure DefineTextoLaFator;
    procedure DefineTexto_Export_Med(Export_Apl, Export_Tip, Export_Med: Integer;
              EdExport_Med_TXT: TEdit);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCons: TFmCons;
const
  FFormatFloat = '00000';

implementation

uses Module, Periodo, UnFinanceiro, MyVCLSkin, ModuleGeral, UnMyObjects,
  Principal, UnFinanceiroJan, UnBloquetosCond;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCons.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCons.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrConsCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCons.DefParams;
begin
  VAR_GOTOTABELA := 'Cons';
  VAR_GOTOMYSQLTABLE := QrCons;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT con.Nome NOMEGENERO, cns.*');
  VAR_SQLx.Add('FROM cons cns');
  VAR_SQLx.Add('LEFT JOIN contas con ON con.Codigo=cns.Genero');
  //
  VAR_SQL1.Add('WHERE cns.Codigo=:P0');
  //
  VAR_SQLa.Add('');
  //
end;

procedure TFmCons.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PnCond.Visible      := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text          := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text            := '';
        EdGenero.ValueVariant  := 0;
        CBGenero.KeyValue      := Null;
        RGExport_Tip.ItemIndex := 0;
      end else begin
        EdCodigo.Text          := DBEdCodigo.Text;
        EdNome.Text            := DBEdNome.Text;
        EdGenero.ValueVariant  := QrConsGenero.Value;
        CBGenero.KeyValue      := QrConsGenero.Value;
        RGExport_Tip.ItemIndex := QrConsExport_Tip.Value;
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      PnCond.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdEmpresa.ValueVariant    := 0;
        CBEmpresa.KeyValue        := Null;
        EdCNAB_Cfg.ValueVariant   := 0;
        CBCNAB_Cfg.KeyValue       := Null;
        EdPreco.ValueVariant      := 0;
        //
        EdEmpresa.Enabled         := True;
        CBEmpresa.Enabled         := True;
        //
        RGCasas.ItemIndex         := 3;
        EdUnidFat.Text            := '2,380000';
        EdUnidLei.Text            := 'm�';
        EdUnidImp.Text            := 'kg';
        //
        EdContaBase.ValueVariant  := 0;
        CBContaBase.KeyValue      := Null;
        RGPerioBase.ItemIndex     := 1; // Periodo atual
        RGFatorBase.ItemIndex     := 1; // Fra��o ideal
        RGCasRat.ItemIndex        := 0;
        EdArredonda.ValueVariant  := 0.01;
        EdCarencia.ValueVariant   := 0;
        CkNaoImpLei.Checked       := False;
        CkDifCaren.Checked        := False;
        RGExport_Apl.ItemIndex    := 0;
        EdExport_Med.ValueVariant := 0;
        //
{
        QrListaCond.Close;
        QrListaCond.SQL.Clear;
        QrListaCond.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
        QrListaCond.SQL.Add('ELSE ent.Nome END NOMECLI, cnd.Codigo');
        QrListaCond.SQL.Add('FROM cond cnd');
        QrListaCond.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente');
        QrListaCond.SQL.Add('WHERE cnd.Codigo NOT IN (');
        QrListaCond.SQL.Add('  SELECT Cond');
        QrListaCond.SQL.Add('  FROM consprc');
        QrListaCond.SQL.Add('  WHERE Codigo=:P0)');
        QrListaCond.SQL.Add('ORDER BY NOMECLI');
        QrListaCond.Params[0].AsInteger := QrConsCodigo.Value;
        QrListaCond.Open;
}
        EdEmpresa.SetFocus;
      end else begin
        EdEmpresa.ValueVariant    := QrConsPrcCond.Value;
        CBEmpresa.KeyValue        := QrConsPrcCond.Value;
        EdCNAB_Cfg.ValueVariant   := QrConsPrcCNAB_Cfg.Value;
        CBCNAB_Cfg.KeyValue       := QrConsPrcCNAB_Cfg.Value;
        EdPreco.ValueVariant      := QrConsPrcPreco.Value;
        //
        EdEmpresa.Enabled         := False;
        CBEmpresa.Enabled         := False;
        //
        RGCasas.ItemIndex         := QrConsPrcCasas.Value;
        EdUnidFat.Text            := Geral.FFT(QrConsPrcUnidFat.Value, 6, siPositivo);
        EdUnidLei.Text            := QrConsPrcUnidLei.Value;
        EdUnidImp.Text            := QrConsPrcUnidImp.Value;
        //
        EdContaBase.ValueVariant  := QrConsPrcContaBase.Value;
        CBContaBase.KeyValue      := QrConsPrcContaBase.Value;
        RGPerioBase.ItemIndex     := QrConsPrcPerioBase.Value + 1;
        RGFatorBase.ItemIndex     := QrConsPrcFatorBase.Value;
        RGCasRat.ItemIndex        := QrConsPrcCasRat.Value;
        EdArredonda.ValueVariant  := QrConsPrcArredonda.Value;
        EdCarencia.ValueVariant   := QrConsPrcCarencia.Value;
        CkNaoImpLei.Checked       := MLAGeral.IntToBool(QrConsPrcNaoImpLei.Value);
        CkDifCaren.Checked        := MLAGeral.IntToBool(QrConsPrcDifCaren.Value);
        RGExport_Apl.ItemIndex    := QrConsPrcExport_Apl.Value;
        EdExport_Med.ValueVariant := QrConsPrcExport_Med.Value;
        //
{
        QrListaCond.Close;
        QrListaCond.SQL.Clear;
        QrListaCond.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
        QrListaCond.SQL.Add('ELSE ent.Nome END NOMECLI, cnd.Codigo');
        QrListaCond.SQL.Add('FROM cond cnd');
        QrListaCond.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente');
        QrListaCond.SQL.Add('ORDER BY NOMECLI');
        QrListaCond.Open;
}
        EdPreco.SetFocus;
      end;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCons.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCons.AlteraRegistro;
var
  Cons : Integer;
begin
  Cons := QrConsCodigo.Value;
  if not UMyMod.SelLockY(Cons, Dmod.MyDB, 'Cons', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Cons, Dmod.MyDB, 'Cons', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCons.IncluiRegistro;
var
  Cursor : TCursor;
  Cons : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Cons := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Cons', 'Cons', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Cons))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, Cons);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCons.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCons.DBGCondDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'DifCaren' then
    MeuVCLSkin.DrawGrid(TDBGrid(DBGCond), Rect, 1, QrConsPrcDifCaren.Value);
end;

procedure TFmCons.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCons.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCons.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCons.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCons.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCons.SpeedButton5Click(Sender: TObject);
var
  Conta: Integer;
begin
  if EdGenero.Enabled and EdGenero.Enabled then
  begin
    VAR_CADASTRO := 0;
    Conta        := EdGenero.ValueVariant;
    //
    FinanceiroJan.CadastroDeContas(Conta);
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.AbreQuery(QrContas, DMod.MyDB);
      //
      EdGenero.ValueVariant := VAR_CADASTRO;
      CBGenero.KeyValue     := VAR_CADASTRO;
      EdGenero.SetFocus;
    end;
  end;
end;

procedure TFmCons.SpeedButton6Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdCNAB_Cfg.ValueVariant;
  //
  UBloquetosCond.MostraCNAB_Cfg(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCNAB_Cfg, CBCNAB_Cfg, QrCNAB_Cfg, VAR_CADASTRO);
    EdCNAB_Cfg.SetFocus;
  end;
end;

procedure TFmCons.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrConsCodigo.Value;
  Close;
end;

procedure TFmCons.BtConfirmaClick(Sender: TObject);
var
  Codigo, Genero: Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Genero := Geral.IMV(EdGenero.Text);
  if Genero < 1 then
  begin
    Geral.MensagemBox('Informe a conta (Plano de Contas) !', 'Erro', MB_OK+MB_ICONERROR);
    EdGenero.SetFocus;
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO cons SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE cons SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Genero=:P1, Export_Tip=:P2, ');
  //
  if ImgTipo.SQLType = StIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Genero;
  Dmod.QrUpdU.Params[02].AsInteger := RGExport_Tip.ItemIndex;
  //
  Dmod.QrUpdU.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[04].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[05].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Cons', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmCons.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Cons', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Cons', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Cons', 'Codigo');
end;

procedure TFmCons.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  Panel6.Align      := alClient;
  DBGCond.Align     := alClient;
  //
  CBGenero.ListSource    := DsContas;
  CBEmpresa.ListSource   := DModG.DsEmpresas;
  CBCNAB_Cfg.ListSource  := DsCNAB_Cfg;
  CBContaBase.ListSource := DsContas;
  //
  UMyMod.AbreQuery(QrContas, DMod.MyDB);
  //
  CriaOForm;
end;

procedure TFmCons.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrConsCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCons.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmCons.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCons.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmCons.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCons.QrConsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCons.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCons.QrConsAfterScroll(DataSet: TDataSet);
begin
  ReopenQrConsPrc(0);
  ReopenQrConsPer(0);
end;

procedure TFmCons.SbQueryClick(Sender: TObject);
begin
  LocCod(QrConsCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Cons', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCons.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCons.QrConsBeforeOpen(DataSet: TDataSet);
begin
  QrConsCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCons.BtProdutoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProduto, BtProduto);
end;

procedure TFmCons.Incluinovoproduto1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCons.Alteraprodutoatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmCons.ReopenQrConsPer(Periodo: Integer);
begin
  Exit;
  ///////////////////////////////////////////////////
  Screen.Cursor := crHourGlass;
  try
  QrConsPer.Close;
  QrConsPer.Params[0].AsInteger := QrConsCodigo.Value;
  QrConsPer.Open;
  //
  QrConsPer.Locate('Periodo', Periodo, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCons.ReopenQrConsPrc(Controle: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
{
    QrConsPrc.Close;
    QrConsPrc.Params[0].AsInteger := QrConsCodigo.Value;
    QrConsPrc.Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrConsPrc, Dmod.MyDB, [
    'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial',
    'ELSE ent.Nome END NOMECLI, cfg.Nome CNAB_Cfg_Txt, csp.*',
    'FROM consprc csp',
    'LEFT JOIN cond cnd ON cnd.Codigo=csp.Cond',
    'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente',
    'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=csp.CNAB_Cfg ',
    'WHERE csp.Codigo=' + FormatFloat('0', QrConsCodigo.Value),
    'AND cnd.Codigo IN (' + VAR_LIB_FILIAIS + ')',
    '']);
    //
    QrConsPrc.Locate('Controle', Controle, []);
  finally
    ScreEn.Cursor := crDefault;
  end;
end;

procedure TFmCons.RGExport_AplClick(Sender: TObject);
begin
  DefineTexto_Export_Med(QrConsExport_Tip.Value, RGExport_Apl.ItemIndex,
    EdExport_Med.ValueVariant, EdExport_Med_TXT);
end;

procedure TFmCons.BtPeriodoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPeriodo, BtPeriodo);
end;

procedure TFmCons.Incluinovoperodo1Click(Sender: TObject);
var
  Dia, Mes, Ano, Periodo: Word;
  Cancelou: Boolean;
  Controle: Integer;
begin
  DecodeDate(Date, Ano, Mes, Dia);
  MLAGeral.EscolhePeriodo_MesEAno(TFmPeriodo, FmPeriodo, Mes, Ano, Cancelou, True, True);
  Periodo := ((Ano - 2000) * 12) + Mes;
  QrDup.Close;
  QrDup.Params[00].AsInteger := QrConsCodigo.Value;
  QrDup.Params[01].AsInteger := Periodo;
  QrDup.Open;
  if QrDup.RecordCount > 0 then
  begin
    Geral.MensagemBox('Per�odo j� existe para este produto!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end else begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ConsPer', 'ConsPer', 'Controle');

    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO consper SET ');
    Dmod.QrUpd.SQL.Add('Periodo=:P0, ');
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
    //
    Dmod.QrUpd.Params[00].AsInteger := Periodo;
    //
    Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpd.Params[02].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.Params[03].AsInteger := QrConsCodigo.Value;
    Dmod.QrUpd.Params[04].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrConsPer(Periodo);
  end;
end;

procedure TFmCons.QrConsPerCalcFields(DataSet: TDataSet);
begin
  QrConsPerPERIODO_TXT.Value := dmkPF.MesEAnoDoPeriodo(QrConsPerPeriodo.Value);
end;

procedure TFmCons.QrConsPerAfterScroll(DataSet: TDataSet);
begin
  ReopenQrConsIts(0);
end;

procedure TFmCons.ReopenQrConsIts(Conta: Integer);
begin
  QrConsIts.Close;
  QrConsIts.Params[0].AsInteger := QrConsPerControle.Value;
  QrConsIts.Open;
  //
  QrConsIts.Locate('Conta', Conta, []);
end;

procedure TFmCons.PMPeriodoPopup(Sender: TObject);
begin
  Incluinovoperodo1.Enabled := MLAGeral.IntToBool_Query(QrCons);
  Excluiperodo1.Enabled := MLAGeral.IntToBool_Query_Not(QrConsIts);
end;

procedure TFmCons.Excluiperodo1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do per�odo "'+
  QrConsPerPERIODO_TXT.Value + '"?', 'Pergunta', MB_YESNOCANCEL+
  MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM consper WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrConsPerControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrConsPer(0);
  end;
end;

procedure TFmCons.Incluicondomnio1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmCons.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmCons.BtCondClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCond, BtCond);
end;

procedure TFmCons.BitBtn1Click(Sender: TObject);
var
  Cond, Controle, CNAB_Cfg: Integer;
  Preco: Double;
  Txt: String;
begin
  Cond     := EdEmpresa.ValueVariant;
  CNAB_Cfg := EdCNAB_Cfg.ValueVariant;
  //
  if ImgTipo.SQLType = stIns then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM consprc',
    'WHERE Codigo=' + FormatFloat('0', QrConsCodigo.Value),
    'AND Cond=' + FormatFloat('0', Cond),
    '']);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Geral.MensagemBox('Inclus�o cancelada! Empresa j� cadastrada!',
      'Inclus�o cancelada!', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  if Cond = 0 then
  begin
{ TODO : Tirar as refer�ncias "Condom�nio" }
    Geral.MensagemBox('Informe o condom�nio!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdEmpresa.SetFocus;
    Exit;
  end;
  Preco := Geral.DMV(EdPreco.Text);
  if Preco < 0.0001 then
  begin
    if Geral.MensagemBox('Confirma cadastro sem pre�o definido?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
  end;
  //
  if UBloquetosCond.ValidaArrecadacaoConsumoCond(Cond, CNAB_Cfg, QrLoc, Dmod.MyDB, Txt) = False then
  begin
    Geral.MB_Aviso(Txt);
    EdCNAB_Cfg.SetFocus;
    Exit;
  end;
  //
  //
  Controle := UMyMod.BuscaEmLivreY_Def('consprc', 'controle',
    ImgTipo.SQLType, QrConsPrcControle.Value);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'consprc', False, [
    'Preco', 'Casas', 'UnidLei', 'UnidImp',
    'UnidFat', 'Cond', 'Codigo',
    'ContaBase', 'PerioBase',
    'FatorBase', 'CasRat',
    'Arredonda', 'NaoImpLei',
    'Carencia', 'DifCaren',
    'Export_Apl', 'Export_Med',
    'CNAB_Cfg'
  ], ['Controle'], [
    Preco, RGCasas.ItemIndex, EdUnidLei.Text, EdUnidImp.Text,
    Geral.DMV(EdUnidFat.Text), Cond, QrConsCodigo.Value,
    Geral.IMV(EdContaBase.Text), RGPerioBase.ItemIndex - 1,
    RGFatorBase.ItemIndex, RGCasRat.ItemIndex,
    EdArredonda.ValueVariant, MLAGeral.BoolToInt(CkNaoImpLei.Checked),
    EdCarencia.ValueVariant, MLAGeral.BoolToInt(CkDifCaren.Checked),
    RGExport_Apl.ItemIndex, EdExport_Med.ValueVariant,
    CNAB_Cfg
  ],[Controle], True) then
  begin
    ReopenQrConsPrc(Controle);
    MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmCons.Alteradadosdocondomnioselecionado1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmCons.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  //
  if Empresa <> 0 then
    UBloquetosCond.ReopenCNAB_Cfg(QrCNAB_Cfg, Dmod.MyDB, DModG.QrEmpresasCodigo.Value)
  else
    QrCNAB_Cfg.Close;
end;

procedure TFmCons.EdExport_MedChange(Sender: TObject);
begin
  DefineTexto_Export_Med(QrConsExport_Tip.Value, RGExport_Apl.ItemIndex,
    EdExport_Med.ValueVariant, EdExport_Med_TXT);
end;

procedure TFmCons.EdUnidImpChange(Sender: TObject);
begin
  if Trim(EdUnidImp.Text) = '' then
    LaPreco.Caption := 'Pre�o $/Impress�o:'
  else
    LaPreco.Caption := 'Pre�o $/'+ Trim(EdUnidImp.Text)+':';
  DefineTextoLaFator;
end;

procedure TFmCons.EdUnidLeiChange(Sender: TObject);
begin
  DefineTextoLaFator;
end;

procedure TFmCons.DefineTextoLaFator;
var
 Lei, Imp: String;
begin
  if Trim(EdUnidImp.Text) = '' then
    Imp := 'Impress�o'
  else
    imp := Trim(EdUnidImp.Text);
  ////
  if Trim(EdUnidLei.Text) = '' then
    Lei := 'Leitura'
  else
    Lei := Trim(EdUnidLei.Text);
  ////
  LaFator.Caption := imp + '/' + Lei;
end;

procedure TFmCons.DefineTexto_Export_Med(Export_Apl, Export_Tip, Export_Med:
Integer; EdExport_Med_TXT: TEdit);
{
Tabela 2 
MEDIDAS G�S
1 - M3
2 - LEITURA
3 - MORADOR
4 - VALOR

Tabela 2
MEDIDAS �GUA
1 - VALOR
2 - MORADOR

Tabela 3
C�DIGO TAXA
1      TAXA DE CONDOMINIO
2	     COPEL
3	     SANEPAR
4	     FUNDO DE RESERVA
5	     CHAM CAPITAL
6	     GAS
7	     TAXA DE MUDAN�A
8	     SALAO DE FESTA
}
begin
  EdExport_Med_TXT.Text := '???';
  case Export_Apl of
    0: // Nenhum
       EdExport_Med_TXT.Text := 'N/D (Apl)';
    1: // Produsys
    begin
      case Export_Tip of
        0: // Outros
           EdExport_Med_TXT.Text := 'N/D (Tip)';
        1: // G�s
        begin
          case Export_Med of
            1: EdExport_Med_TXT.Text := 'm�';
            2: EdExport_Med_TXT.Text := 'Leitura';
            3: EdExport_Med_TXT.Text := DModG.ReCaptionTexto(VAR_M_O_R_A_D_O_R);
            4: EdExport_Med_TXT.Text := 'Valor';
            else EdExport_Med_TXT.Text := 'N/D (G�s)';
          end;
        end;
        2: // �gua
        begin
          case Export_Med of
            1: EdExport_Med_TXT.Text := 'Valor';
            2: EdExport_Med_TXT.Text := DModG.ReCaptionTexto(VAR_M_O_R_A_D_O_R);
            else EdExport_Med_TXT.Text := 'N/D (G�s)';
          end;
          EdExport_Med_TXT.Text := 'N/D (�gua)';
        end;
      end;
    end;
    else
      EdExport_Med_TXT.Text := '*ERRO*';
  end;
end;

end.

