object FmCondGerCarne: TFmCondGerCarne
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-003 :: Impress'#227'o de Carn'#234' de Bloquetos'
  ClientHeight = 628
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 466
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 112
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 101
        Height = 13
        Caption = 'Cliente (Condom'#237'nio):'
      end
      object SpeedButton1: TSpeedButton
        Left = 752
        Top = 20
        Width = 21
        Height = 21
        Caption = '?'
        OnClick = SpeedButton1Click
      end
      object EdEmpresa: TdmkEditCB
        Left = 12
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        OnEnter = EdEmpresaEnter
        OnExit = EdEmpresaExit
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 669
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object GBIni: TGroupBox
        Left = 13
        Top = 45
        Width = 264
        Height = 64
        Caption = ' Per'#237'odo Inicial: '
        TabOrder = 2
        object Label32: TLabel
          Left = 4
          Top = 15
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAnoI: TLabel
          Left = 180
          Top = 15
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBMesI: TComboBox
          Left = 5
          Top = 32
          Width = 172
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object CBAnoI: TComboBox
          Left = 179
          Top = 32
          Width = 78
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object GBFim: TGroupBox
        Left = 485
        Top = 45
        Width = 264
        Height = 64
        Caption = ' Per'#237'odo final: '
        TabOrder = 3
        object Label34: TLabel
          Left = 4
          Top = 15
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAnoF: TLabel
          Left = 180
          Top = 15
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBMesF: TComboBox
          Left = 5
          Top = 32
          Width = 172
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object CBAnoF: TComboBox
          Left = 179
          Top = 32
          Width = 78
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object CkZerado: TCheckBox
        Left = 292
        Top = 90
        Width = 181
        Height = 17
        Caption = 'Sem valor e sem vencimento.'
        TabOrder = 4
      end
      object BtTodoAno: TBitBtn
        Tag = 10118
        Left = 292
        Top = 43
        Width = 181
        Height = 40
        Caption = 'Seleciona ano inteiro'
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtTodoAnoClick
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 112
      Width = 784
      Height = 341
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Unidades selecionadas '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGridUnid: TdmkDBGridDAC
          Left = 0
          Top = 53
          Width = 776
          Height = 260
          SQLFieldsToChange.Strings = (
            'Ativo')
          SQLIndexesOnUpdate.Strings = (
            'Conta')
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Ok'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PROPRIET'
              Title.Caption = 'Propriet'#225'rio'
              Width = 575
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Caption = 'Sequencial'
              Width = 59
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCondImov0
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          SQLTable = 'CondImov0'
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Ok'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PROPRIET'
              Title.Caption = 'Propriet'#225'rio'
              Width = 575
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Caption = 'Sequencial'
              Width = 59
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 53
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label2: TLabel
            Left = 17
            Top = 2
            Width = 671
            Height = 16
            Caption = 
              'Para melhor otimiza'#231#227'o da montagem as folhas do mesmo carn'#234' (uni' +
              'dade) s'#227'o impressas em p'#225'ginas sequenciais.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 12698111
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object Label3: TLabel
            Left = 16
            Top = 1
            Width = 671
            Height = 16
            Caption = 
              'Para melhor otimiza'#231#227'o da montagem as folhas do mesmo carn'#234' (uni' +
              'dade) s'#227'o impressas em p'#225'ginas sequenciais.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object Label4: TLabel
            Left = 17
            Top = 18
            Width = 551
            Height = 16
            Caption = 
              'Para melhor aproveitamento de papel selecione a quantidade de un' +
              'idades em m'#250'ltiplos de tr'#234's.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 12698111
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object Label5: TLabel
            Left = 16
            Top = 17
            Width = 551
            Height = 16
            Caption = 
              'Para melhor aproveitamento de papel selecione a quantidade de un' +
              'idades em m'#250'ltiplos de tr'#234's.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object Label6: TLabel
            Left = 17
            Top = 34
            Width = 608
            Height = 16
            Caption = 
              'ATEN'#199#195'O!!!! a otimiza'#231#227'o da montagem s'#243' ocorrer'#225' se o n'#250'mero de ' +
              'folhas for igual para todas unidades!'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 12698111
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object Label7: TLabel
            Left = 16
            Top = 33
            Width = 608
            Height = 16
            Caption = 
              'ATEN'#199#195'O!!!! a otimiza'#231#227'o da montagem s'#243' ocorrer'#225' se o n'#250'mero de ' +
              'folhas for igual para todas unidades!'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Bloquetos a serem impressos '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGradeS: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 313
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Boleto'
              Title.Caption = 'Bloqueto'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROPRIET'
              Title.Caption = 'Propriet'#225'rio'
              Width = 162
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUB_TOT'
              Title.Caption = 'Valor'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VENCTO_TXT'
              Title.Caption = 'Vencimen.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOTE_PROTOCO'
              Title.Caption = 'Lote'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PROTOCOLO'
              Title.Caption = 'Protocolo'
              Width = 56
              Visible = True
            end>
          Color = clWindow
          DataSource = DsBoletos
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          FieldsCalcToOrder.Strings = (
            'VENCTO_TXT=Vencto')
          Columns = <
            item
              Expanded = False
              FieldName = 'Boleto'
              Title.Caption = 'Bloqueto'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROPRIET'
              Title.Caption = 'Propriet'#225'rio'
              Width = 162
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUB_TOT'
              Title.Caption = 'Valor'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VENCTO_TXT'
              Title.Caption = 'Vencimen.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOTE_PROTOCO'
              Title.Caption = 'Lote'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PROTOCOLO'
              Title.Caption = 'Protocolo'
              Width = 56
              Visible = True
            end>
        end
      end
    end
    object PnAviso: TPanel
      Left = 0
      Top = 453
      Width = 784
      Height = 13
      Align = alBottom
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 414
        Height = 32
        Caption = 'Impress'#227'o de Carn'#234' de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 414
        Height = 32
        Caption = 'Impress'#227'o de Carn'#234' de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 414
        Height = 32
        Caption = 'Impress'#227'o de Carn'#234' de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 514
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 558
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object Bt_: TBitBtn
        Tag = 14
        Left = 140
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = Bt_Click
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 286
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 410
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtNenhumClick
      end
    end
  end
  object QrCondImov0: TmySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrCondImov0CalcFields
    SQL.Strings = (
      'SELECT IF(prp.Tipo=0,RazaoSocial,Nome) NO_PROPRIET, ci0.*'
      'FROM condimov0 ci0'
      'LEFT JOIN syndic.entidades prp ON prp.Codigo=ci0.Propriet')
    Left = 252
    Top = 12
    object QrCondImov0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondImov0Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCondImov0Conta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCondImov0Propriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrCondImov0Andar: TIntegerField
      FieldName = 'Andar'
    end
    object QrCondImov0Unidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrCondImov0Status: TIntegerField
      FieldName = 'Status'
    end
    object QrCondImov0SitImv: TIntegerField
      FieldName = 'SitImv'
    end
    object QrCondImov0Usuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrCondImov0Ativo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrCondImov0Protocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrCondImov0Moradores: TIntegerField
      FieldName = 'Moradores'
    end
    object QrCondImov0FracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
    end
    object QrCondImov0ModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrCondImov0ConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrCondImov0BloqEndTip: TSmallintField
      FieldName = 'BloqEndTip'
    end
    object QrCondImov0BloqEndEnt: TIntegerField
      FieldName = 'BloqEndEnt'
    end
    object QrCondImov0EnderLin1: TWideStringField
      FieldName = 'EnderLin1'
      Size = 100
    end
    object QrCondImov0EnderLin2: TWideStringField
      FieldName = 'EnderLin2'
      Size = 100
    end
    object QrCondImov0EnderNome: TWideStringField
      FieldName = 'EnderNome'
      Size = 100
    end
    object QrCondImov0Procurador: TIntegerField
      FieldName = 'Procurador'
    end
    object QrCondImov0NO_PROPRIET: TWideStringField
      FieldName = 'NO_PROPRIET'
      Size = 100
    end
    object QrCondImov0Ordem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsCondImov0: TDataSource
    DataSet = QrCondImov0
    Left = 280
    Top = 12
  end
  object QrPrev: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 396
    Top = 12
    object QrPrevCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrevPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrPrevGastos: TFloatField
      FieldName = 'Gastos'
    end
    object QrPrevLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPrevDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPrevDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPrevUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPrevUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPrevCond: TIntegerField
      FieldName = 'Cond'
    end
    object QrPrevAviso01: TWideStringField
      FieldName = 'Aviso01'
      Size = 50
    end
    object QrPrevAviso02: TWideStringField
      FieldName = 'Aviso02'
      Size = 50
    end
    object QrPrevAviso03: TWideStringField
      FieldName = 'Aviso03'
      Size = 50
    end
    object QrPrevAviso04: TWideStringField
      FieldName = 'Aviso04'
      Size = 50
    end
    object QrPrevAviso05: TWideStringField
      FieldName = 'Aviso05'
      Size = 50
    end
    object QrPrevAviso06: TWideStringField
      FieldName = 'Aviso06'
      Size = 50
    end
    object QrPrevAviso07: TWideStringField
      FieldName = 'Aviso07'
      Size = 50
    end
    object QrPrevAviso08: TWideStringField
      FieldName = 'Aviso08'
      Size = 50
    end
    object QrPrevAviso09: TWideStringField
      FieldName = 'Aviso09'
      Size = 50
    end
    object QrPrevAviso10: TWideStringField
      FieldName = 'Aviso10'
      Size = 50
    end
    object QrPrevAvisoVerso: TWideStringField
      FieldName = 'AvisoVerso'
      Size = 180
    end
    object QrPrevAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPrevAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPrevAviso11: TWideStringField
      FieldName = 'Aviso11'
      Size = 50
    end
    object QrPrevAviso12: TWideStringField
      FieldName = 'Aviso12'
      Size = 50
    end
    object QrPrevAviso13: TWideStringField
      FieldName = 'Aviso13'
      Size = 50
    end
    object QrPrevAviso14: TWideStringField
      FieldName = 'Aviso14'
      Size = 50
    end
    object QrPrevAviso15: TWideStringField
      FieldName = 'Aviso15'
      Size = 50
    end
    object QrPrevAviso16: TWideStringField
      FieldName = 'Aviso16'
      Size = 50
    end
    object QrPrevAviso17: TWideStringField
      FieldName = 'Aviso17'
      Size = 50
    end
    object QrPrevAviso18: TWideStringField
      FieldName = 'Aviso18'
      Size = 50
    end
    object QrPrevAviso19: TWideStringField
      FieldName = 'Aviso19'
      Size = 50
    end
    object QrPrevAviso20: TWideStringField
      FieldName = 'Aviso20'
      Size = 50
    end
    object QrPrevModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrPrevConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrPrevBalAgrMens: TSmallintField
      FieldName = 'BalAgrMens'
    end
    object QrPrevCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrPrevEncerrado: TSmallintField
      FieldName = 'Encerrado'
    end
    object QrPrevCondCli: TIntegerField
      FieldName = 'CondCli'
    end
    object QrPrevCondCod: TIntegerField
      FieldName = 'CondCod'
      Required = True
    end
    object QrPrevNOMECONFIGBOL: TWideStringField
      FieldName = 'NOMECONFIGBOL'
      Size = 50
    end
  end
  object QrBolLei: TmySQLQuery
    Database = Dmod.MyDB
    Left = 660
    Top = 12
    object QrBolLeiValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
    object QrBolLeiApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolLeiBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolLeiBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrBolArr: TmySQLQuery
    Database = Dmod.MyDB
    Left = 688
    Top = 12
    object QrBolArrValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
    object QrBolArrApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolArrBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolArrBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrPPI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.Conta PROTOCOLO, ppi.Controle LOTE, ppi.DataE, '
      
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DATAE_TX' +
        'T,'
      
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DATAD_TX' +
        'T,'
      'ppi.DataD, ppi.Cancelado, ppi.Motivo, ppi.ID_Cod1, ppi.ID_Cod2, '
      'ppi.ID_Cod3, ppi.ID_Cod4, ppi.CliInt, ppi.Cliente, ppi.Periodo,'
      'ppi.Docum, ptc.Nome TAREFA, ptc.Def_Sender,'
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN entidades  snd ON snd.Codigo=ptc.Def_Sender'
      'WHERE ppi.Link_ID=1'
      'AND ppi.ID_Cod1=:P0'
      'AND ppi.ID_Cod2=:P1')
    Left = 716
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPPIPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'protpakits.Conta'
      Required = True
    end
    object QrPPIDataE: TDateField
      FieldName = 'DataE'
      Origin = 'protpakits.DataE'
      Required = True
    end
    object QrPPIDataD: TDateField
      FieldName = 'DataD'
      Origin = 'protpakits.DataD'
      Required = True
    end
    object QrPPICancelado: TIntegerField
      FieldName = 'Cancelado'
      Origin = 'protpakits.Cancelado'
      Required = True
    end
    object QrPPIMotivo: TIntegerField
      FieldName = 'Motivo'
      Origin = 'protpakits.Motivo'
      Required = True
    end
    object QrPPIID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Origin = 'protpakits.ID_Cod1'
      Required = True
    end
    object QrPPIID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Origin = 'protpakits.ID_Cod2'
      Required = True
    end
    object QrPPIID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Origin = 'protpakits.ID_Cod3'
      Required = True
    end
    object QrPPIID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Origin = 'protpakits.ID_Cod4'
      Required = True
    end
    object QrPPITAREFA: TWideStringField
      FieldName = 'TAREFA'
      Origin = 'protocolos.Nome'
      Size = 100
    end
    object QrPPIDELIVER: TWideStringField
      FieldName = 'DELIVER'
      Size = 100
    end
    object QrPPILOTE: TIntegerField
      FieldName = 'LOTE'
      Origin = 'protpakits.Controle'
      Required = True
    end
    object QrPPICliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'protpakits.CliInt'
      Required = True
    end
    object QrPPICliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'protpakits.Cliente'
      Required = True
    end
    object QrPPIPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'protpakits.Periodo'
      Required = True
    end
    object QrPPIDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 10
    end
    object QrPPIDATAD_TXT: TWideStringField
      FieldName = 'DATAD_TXT'
      Size = 10
    end
    object QrPPIDef_Sender: TIntegerField
      FieldName = 'Def_Sender'
      Origin = 'protocolos.Def_Sender'
    end
    object QrPPIDocum: TFloatField
      FieldName = 'Docum'
      Origin = 'protpakits.Docum'
      Required = True
    end
  end
  object QrBoletosIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 632
    Top = 12
    object QrBoletosItsTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Required = True
      Size = 50
    end
    object QrBoletosItsVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBoletosItsVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBoletosItsTEXTO_IMP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO_IMP'
      Size = 255
      Calculated = True
    end
    object QrBoletosItsMedAnt: TFloatField
      FieldName = 'MedAnt'
    end
    object QrBoletosItsMedAtu: TFloatField
      FieldName = 'MedAtu'
    end
    object QrBoletosItsConsumo: TFloatField
      FieldName = 'Consumo'
    end
    object QrBoletosItsCasas: TLargeintField
      FieldName = 'Casas'
    end
    object QrBoletosItsUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Size = 5
    end
    object QrBoletosItsUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Size = 5
    end
    object QrBoletosItsUnidFat: TFloatField
      FieldName = 'UnidFat'
    end
    object QrBoletosItsTipo: TLargeintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrBoletosItsVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrBoletosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBoletosItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrBoletosItsGeraTyp: TLargeintField
      FieldName = 'GeraTyp'
    end
    object QrBoletosItsGeraFat: TFloatField
      FieldName = 'GeraFat'
    end
    object QrBoletosItsCasRat: TLargeintField
      FieldName = 'CasRat'
    end
    object QrBoletosItsNaoImpLei: TLargeintField
      FieldName = 'NaoImpLei'
    end
  end
  object QrBoletos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrBoletosBeforeOpen
    AfterScroll = QrBoletosAfterScroll
    OnCalcFields = QrBoletosCalcFields
    Left = 604
    Top = 12
    object QrBoletosApto: TIntegerField
      FieldName = 'Apto'
      Origin = 'Apto'
      Required = True
    end
    object QrBoletosUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'Unidade'
      Size = 10
    end
    object QrBoletosSUB_ARR: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_ARR'
      LookupDataSet = QrBolArr
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBoletosSUB_LEI: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_LEI'
      LookupDataSet = QrBolLei
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBoletosSUB_TOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrBoletosVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBoletosPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'Propriet'
      Required = True
    end
    object QrBoletosNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Origin = 'NOMEPROPRIET'
      Size = 100
    end
    object QrBoletosBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBoletosVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrBoletosUSERNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'USERNAME'
      LookupDataSet = QrUsers
      LookupKeyFields = 'CodigoEsp'
      LookupResultField = 'Username'
      KeyFields = 'Apto'
      Size = 100
      Lookup = True
    end
    object QrBoletosPASSWORD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'PASSWORD'
      LookupDataSet = QrUsers
      LookupKeyFields = 'CodigoEsp'
      LookupResultField = 'Password'
      KeyFields = 'Apto'
      Size = 100
      Lookup = True
    end
    object QrBoletosPWD_WEB: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PWD_WEB'
      Size = 255
      Calculated = True
    end
    object QrBoletosPROTOCOLO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'PROTOCOLO'
      LookupDataSet = QrPPI
      LookupKeyFields = 'Docum'
      LookupResultField = 'PROTOCOLO'
      KeyFields = 'Boleto'
      DisplayFormat = '00000;-000000; '
      Lookup = True
    end
    object QrBoletosAndar: TIntegerField
      FieldName = 'Andar'
      Origin = 'Andar'
    end
    object QrBoletosBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'Boleto'
      Required = True
    end
    object QrBoletosBLOQUETO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'BLOQUETO'
      Calculated = True
    end
    object QrBoletosKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrBoletosLOTE_PROTOCO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LOTE_PROTOCO'
      LookupDataSet = QrPPI
      LookupKeyFields = 'Docum'
      LookupResultField = 'LOTE'
      KeyFields = 'Boleto'
      Lookup = True
    end
    object QrBoletosOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrBoletosFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
    end
    object QrBoletosSEQ: TIntegerField
      FieldKind = fkLookup
      FieldName = 'SEQ'
      LookupDataSet = QrCondImov0
      LookupKeyFields = 'Conta'
      LookupResultField = 'Ordem'
      KeyFields = 'Apto'
      Lookup = True
    end
  end
  object DsBoletos: TDataSource
    DataSet = QrCarnes0
    Left = 664
    Top = 68
  end
  object frxDsCarne0: TfrxDBDataset
    UserName = 'frxDsCarne0'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pagina=Pagina'
      'Carne=Carne'
      'Folha=Folha'
      'Vencto=Vencto'
      'SUB_TOT=SUB_TOT'
      'Boleto=Boleto'
      'Unidade=Unidade'
      'E_ALL=E_ALL'
      'PROPRI_E_MORADOR=PROPRI_E_MORADOR'
      'CODIGOBARRAS=CODIGOBARRAS'
      'NossoNumero=NossoNumero'
      'LinhaDigitavel=LinhaDigitavel'
      'Imprimir=Imprimir'
      'PWD_WEB=PWD_WEB')
    DataSet = QrCarnes0
    BCDToCurrency = False
    Left = 604
    Top = 68
  end
  object frxCarne_0: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 41256.382234583330000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Memo_000.Visible := <frxDsCarne0."Imprimir"> = 0;             ' +
        '                                                                ' +
        '                         '
      'end;'
      ''
      'begin'
      
        '  Memo_000.Color := clWhite;                                    ' +
        '                                                                ' +
        '  '
      '  if <LogoBancoExiste> = True then'
      '  begin'
      '      Picture_Bco.LoadFromFile(<LogoBancoPath>);'
      '      Memo_001.Visible := False;'
      '  end else Memo_001.Visible := True;    '
      'end.          ')
    OnGetValue = frxCarne_0GetValue
    Left = 296
    Top = 96
    Datasets = <
      item
        DataSet = frxDsCarne0
        DataSetName = 'frxDsCarne0'
      end
      item
      end
      item
        DataSet = DmCond.frxDsCond
        DataSetName = 'frxDsCond'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Width = 0.100000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 370.393700790000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsCarne0
        DataSetName = 'frxDsCarne0'
        RowCount = 0
        object Memo_000: TfrxMemoView
          Left = 3.779527560000000000
          Top = 3.779530000000001000
          Width = 786.142240000000000000
          Height = 362.834880000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Para melhor otimiza'#231#227'o da montagem as folhas do mesmo carn'#234' (uni' +
              'dade) s'#227'o impressas em p'#225'ginas sequenciais.'
            ''
            
              'Para melhor aproveitamento de papel selecione a quantidade de un' +
              'idades em m'#250'ltiplos de tr'#234's.'
            ''
            
              'ATEN'#199#195'O!!!! a otimiza'#231#227'o da montagem s'#243' ocorrer'#225' se o n'#250'mero de ' +
              'folhas for igual para todas unidades!')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo_002: TfrxMemoView
          Left = 272.126160000000000000
          Top = 14.229305750000000000
          Width = 68.031540000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VAX]')
          ParentFont = False
          WordWrap = False
        end
        object Memo_102: TfrxMemoView
          Left = 45.354330708661420000
          Top = 18.897637800000000000
          Width = 17.007874020000000000
          Height = 83.149652680000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarne0."Vencto"]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo_101: TfrxMemoView
          Left = 35.771653540000000000
          Top = 18.897637800000000000
          Width = 9.448818900000000000
          Height = 83.149606300000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_043: TfrxMemoView
          Left = 151.181102360000000000
          Top = 165.165354330000000000
          Width = 453.543307090000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VAR_INSTRUCAO3]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo_034: TfrxMemoView
          Left = 604.724409450000000000
          Top = 110.362199840000000000
          Width = 166.299212600000000000
          Height = 20.787406460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCarne0."SUB_TOT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_033: TfrxMemoView
          Left = 604.724409450000000000
          Top = 108.850393700000000000
          Width = 166.299212600000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '(=) Valor do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line_001: TfrxLineView
          Left = 759.685530000000000000
          Width = 41.574830000000000000
          Color = clBlack
          ArrowLength = 100
          ArrowWidth = 20
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object BarCode1: TfrxBarCodeView
          Left = 154.960671420000000000
          Top = 306.142130160000000000
          Width = 405.000000000000000000
          Height = 49.133858270000000000
          BarType = bcCode_2_5_interleaved
          Expression = '<VARF_CODIGOBARRAS>'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 3.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Line_008: TfrxLineView
          Left = 151.181200000000000000
          Top = 241.890161660000000000
          Width = 619.842920000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line_009: TfrxLineView
          Left = 151.181200000000000000
          Top = 298.583072600000000000
          Width = 619.842920000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo_013: TfrxMemoView
          Left = 151.181102360000000000
          Top = 87.307086620000010000
          Width = 94.488188980000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_025: TfrxMemoView
          Left = 151.181200000000000000
          Top = 108.850393700000000000
          Width = 94.488250000000000000
          Height = 22.299212600000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Uso do Banco')
          ParentFont = False
          WordWrap = False
        end
        object Memo_015: TfrxMemoView
          Left = 245.669450000000000000
          Top = 87.307086620000010000
          Width = 136.062992130000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#250'mero do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_017: TfrxMemoView
          Left = 381.732530000000000000
          Top = 87.307086620000010000
          Width = 71.811023620000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Esp'#233'cie do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_019: TfrxMemoView
          Left = 453.543600000000000000
          Top = 87.307086620000010000
          Width = 34.015748030000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Aceite')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_021: TfrxMemoView
          Left = 487.559370000000000000
          Top = 87.307086620000010000
          Width = 117.165354330000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data do Processamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_026: TfrxMemoView
          Left = 245.669450000000000000
          Top = 108.850393700000000000
          Width = 45.354328270000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_028: TfrxMemoView
          Left = 291.023810000000000000
          Top = 108.850393700000000000
          Width = 71.811023620000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Esp'#233'cie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_030: TfrxMemoView
          Left = 362.834880000000000000
          Top = 108.850393700000000000
          Width = 139.842519690000000000
          Height = 22.299212600000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo_032: TfrxMemoView
          Left = 502.677490000000000000
          Top = 108.850393700000000000
          Width = 102.047244090000000000
          Height = 22.299212600000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
        end
        object Memo_031: TfrxMemoView
          Left = 496.118430000000000000
          Top = 119.055118110000000000
          Width = 15.118120000000000000
          Height = 8.314960630000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'X')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_023: TfrxMemoView
          Left = 604.724409450000000000
          Top = 87.307086620000010000
          Width = 166.299212600000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nosso N'#250'mero')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_036: TfrxMemoView
          Left = 604.724409450000000000
          Top = 130.393700790000000000
          Width = 166.299212600000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '(-) Desconto/Abatimento')
          ParentFont = False
          WordWrap = False
        end
        object Memo_037: TfrxMemoView
          Left = 604.724409450000000000
          Top = 152.692913390000000000
          Width = 166.299212600000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '(-) Outras Dedu'#231#245'es')
          ParentFont = False
          WordWrap = False
        end
        object Memo_038: TfrxMemoView
          Left = 604.724409450000000000
          Top = 174.992125990000000000
          Width = 166.299212600000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '(+) Mora/Multa')
          ParentFont = False
          WordWrap = False
        end
        object Memo_039: TfrxMemoView
          Left = 604.724409450000000000
          Top = 197.291338580000000000
          Width = 166.299212600000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '(+) Outros Acr'#233'scimos')
          ParentFont = False
          WordWrap = False
        end
        object Memo_040: TfrxMemoView
          Left = 604.724409450000000000
          Top = 219.590551180000000000
          Width = 166.299212600000000000
          Height = 22.299212600000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '(=) Valor Cobrado')
          ParentFont = False
          WordWrap = False
        end
        object Memo_057: TfrxMemoView
          Left = 634.961040000000000000
          Top = 302.362885750000000000
          Width = 136.063080000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_056: TfrxMemoView
          Left = 702.992580000000000000
          Top = 268.347115750000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'digo de Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_035: TfrxMemoView
          Left = 151.181102360000000000
          Top = 131.149606300000000000
          Width = 453.543307090000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_014: TfrxMemoView
          Left = 151.181102360000000000
          Top = 96.377952760000000000
          Width = 94.488188980000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_016: TfrxMemoView
          Left = 245.669450000000000000
          Top = 96.377952760000000000
          Width = 136.062992130000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarne0."Boleto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_018: TfrxMemoView
          Left = 381.732530000000000000
          Top = 96.377952760000000000
          Width = 71.811023620000000000
          Height = 12.472440940000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."EspecieDoc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_020: TfrxMemoView
          Left = 453.543600000000000000
          Top = 96.377952760000000000
          Width = 34.015748030000000000
          Height = 12.472440940000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."ACEITETIT_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_022: TfrxMemoView
          Left = 487.559370000000000000
          Top = 96.377952760000000000
          Width = 117.165354330000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_027: TfrxMemoView
          Left = 245.669450000000000000
          Top = 117.921259840000000000
          Width = 45.354328270000010000
          Height = 13.228346460000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."CART_IMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_029: TfrxMemoView
          Left = 291.023810000000000000
          Top = 117.921259840000000000
          Width = 71.811023620000000000
          Height = 13.228346460000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."EspecieVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_024: TfrxMemoView
          Left = 604.724409450000000000
          Top = 92.598422760000000000
          Width = 166.299212600000000000
          Height = 15.118112680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCarne0."NossoNumero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_041: TfrxMemoView
          Left = 151.181102360000000000
          Top = 140.220472440000000000
          Width = 453.543307090000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VAR_INSTRUCAO1]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo_042: TfrxMemoView
          Left = 151.181102360000000000
          Top = 152.692913390000000000
          Width = 453.543307090000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VAR_INSTRUCAO2]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo_044: TfrxMemoView
          Left = 151.181102360000000000
          Top = 177.637795280000000000
          Width = 453.543307090000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VAR_INSTRUCAO4]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo_045: TfrxMemoView
          Left = 151.181102360000000000
          Top = 190.110236220000000000
          Width = 453.543307090000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VAR_INSTRUCAO5]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo_046: TfrxMemoView
          Left = 151.181102360000000000
          Top = 202.582677170000000000
          Width = 453.543307090000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VAR_INSTRUCAO6]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo_047: TfrxMemoView
          Left = 151.181102360000000000
          Top = 215.055118110000000000
          Width = 453.543307090000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VAR_INSTRUCAO7]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo_048: TfrxMemoView
          Left = 151.181102360000000000
          Top = 227.527559060000000000
          Width = 453.543307090000000000
          Height = 12.472440940000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_URL]  - [frxDsCarne0."PWD_WEB"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Picture_Bco: TfrxPictureView
          Left = 151.181200000000000000
          Top = 14.362204720000000000
          Width = 120.944881889764000000
          Height = 30.236220470000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo_008: TfrxMemoView
          Left = 604.724409450000000000
          Top = 45.732278580000000000
          Width = 166.299212600000000000
          Height = 18.897642680000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCarne0."Vencto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_007: TfrxMemoView
          Left = 604.724409450000000000
          Top = 44.220472440000000000
          Width = 166.299212600000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line_020: TfrxLineView
          Left = 604.724409450000000000
          Top = 44.220472440000000000
          Width = 166.299212600000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line_002: TfrxLineView
          Left = 272.126013540000000000
          Top = 14.229305750000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line_003: TfrxLineView
          Left = 340.157512050000000000
          Top = 14.229305750000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line_015: TfrxLineView
          Left = 604.724409450000000000
          Top = 43.465545750000000000
          Height = 200.315090000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Memo_003: TfrxMemoView
          Left = 340.157700000000000000
          Top = 18.008835750000000000
          Width = 430.866420000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCarne0."LinhaDigitavel"]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_005: TfrxMemoView
          Left = 151.181102360000000000
          Top = 44.220472440000000000
          Width = 453.543307090000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Local de Pagamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_009: TfrxMemoView
          Left = 151.181102360000000000
          Top = 65.763779530000000000
          Width = 453.543307090000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cedente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_006: TfrxMemoView
          Left = 151.181102360000000000
          Top = 53.291338580000000000
          Width = 453.543307090000000000
          Height = 12.472440940000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."LocalPag"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_011: TfrxMemoView
          Left = 604.724409450000000000
          Top = 65.763779530000000000
          Width = 166.299212600000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ag'#234'ncia/C'#243'digo Cedente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_010: TfrxMemoView
          Left = 151.181102360000000000
          Top = 74.834645670000000000
          Width = 453.543307090000000000
          Height = 12.472440940000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."NOMECED"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo_012: TfrxMemoView
          Left = 604.724409450000000000
          Top = 67.275585670000000000
          Width = 166.299212600000000000
          Height = 18.897642680000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."AgContaCed"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo_001: TfrxMemoView
          Left = 151.181200000000000000
          Top = 14.362204720000000000
          Width = 120.944881890000000000
          Height = 30.236220470000000000
          OnBeforePrint = 'Memo135OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."NOMEBANCO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo_004: TfrxMemoView
          Left = 121.322834645669300000
          Top = -0.000012200000000462
          Width = 15.118120000000000000
          Height = 370.393722760000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Style = fsDash
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Recibo do Sacado  -  Autentica'#231#227'o mec'#226'nica no verso')
          ParentFont = False
          Rotation = 90
        end
        object Memo_053: TfrxMemoView
          Left = 151.181102360000000000
          Top = 289.133858270000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sacador / Avalista')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_052: TfrxMemoView
          Left = 151.181102360000000000
          Top = 268.347115750000000000
          Width = 491.338900000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCarne0."E_ALL"]')
          ParentFont = False
        end
        object Memo_051: TfrxMemoView
          Left = 151.181102360000000000
          Top = 253.228995750000000000
          Width = 453.543600000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCarne0."PROPRI_E_MORADOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo_054: TfrxMemoView
          Left = 249.448980000000000000
          Top = 289.133858270000000000
          Width = 393.071120000000000000
          Height = 9.070866140000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."NOMESAC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_049: TfrxMemoView
          Left = 151.181102360000000000
          Top = 241.890405750000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_050: TfrxMemoView
          Left = 294.803340000000000000
          Top = 241.890405750000000000
          Width = 309.921460000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCond."NOMECLI"]  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo_055: TfrxMemoView
          Left = 604.724800000000000000
          Top = 241.890405750000000000
          Width = 162.519790000000000000
          Height = 18.897642680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade: [frxDsCarne0."Unidade"]')
          ParentFont = False
          WordWrap = False
        end
        object Line3: TfrxLineView
          Left = 771.024120000000000000
          Top = 241.890405750000000000
          Height = 56.692950000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Memo_104: TfrxMemoView
          Left = 71.811023622047240000
          Top = 18.897637800000000000
          Width = 21.921262280000000000
          Height = 71.811023620000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarne0."SUB_TOT"]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo_103: TfrxMemoView
          Left = 62.362204720000000000
          Top = 18.897637800000000000
          Width = 9.448818897637795000
          Height = 71.811023620000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VALOR DO DOCUMENTO')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_110: TfrxMemoView
          Left = 103.181102362204700000
          Top = 18.897637800000000000
          Width = 18.141732280000000000
          Height = 102.047244090000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarne0."NossoNumero"]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo_108: TfrxMemoView
          Left = 93.732283460000000000
          Top = 18.897637800000000000
          Width = 9.448818900000000000
          Height = 102.047244090000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NOSSO N'#218'MERO')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 45.354330710000000000
          Top = 102.048293700000000000
          Width = 17.007874020000000000
          Height = 253.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."NOMECED"]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 35.795300000000000000
          Top = 102.047795750000000000
          Width = 9.448818900000000000
          Height = 253.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CEDENTE')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 71.811023622047240000
          Top = 90.708644330000000000
          Width = 21.921262280000000000
          Height = 45.354330710000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarne0."Folha"] / [VARF_PERIODOS]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 62.362204720000000000
          Top = 90.708644330000000000
          Width = 9.448818897637795000
          Height = 45.354330710000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'PARCELA')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 71.811023620000000000
          Top = 136.063565750000000000
          Width = 21.921262280000000000
          Height = 219.212598430000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarne0."PROPRI_E_MORADOR"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 62.362204720000000000
          Top = 136.063565750000000000
          Width = 9.448818900000000000
          Height = 219.212598430000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'SACADO')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 103.181102362204700000
          Top = 120.945943700000000000
          Width = 18.141732280000000000
          Height = 120.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarne0."Boleto"]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 93.732283460000000000
          Top = 120.945445750000000000
          Width = 9.448818897637795000
          Height = 120.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO DO DOCUMENTO')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 103.181102360000000000
          Top = 241.890903700000000000
          Width = 18.141732280000000000
          Height = 113.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg_B."AgContaCed"]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 93.732283460000000000
          Top = 241.890405750000000000
          Width = 9.448818900000000000
          Height = 113.385826770000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AG'#202'NCIA / C'#211'DIGO CEDENTE')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo_112: TfrxMemoView
          Left = 26.456710000000000000
          Top = 18.898135750000000000
          Width = 9.448818900000000000
          Height = 336.378170000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'www.dermatek.com.br  - Software customizado')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Line_000: TfrxLineView
          Width = 41.574830000000000000
          Color = clBlack
          ArrowLength = 100
          ArrowWidth = 20
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
    end
  end
  object QrUsers: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT usu.CodigoEsp, usu.Username, usu.Password'
      'FROM condimov imv'
      'LEFT JOIN users usu ON usu.CodigoEsp=imv.Conta'
      'WHERE imv.Codigo=:P0')
    Left = 744
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsersCodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
      Origin = 'users.CodigoEnt'
    end
    object QrUsersCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
      Origin = 'users.CodigoEsp'
    end
    object QrUsersUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'users.Username'
      Size = 32
    end
    object QrUsersPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'users.Password'
      Size = 32
    end
  end
  object QrCarnes0: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM carne0'
      'ORDER BY Pagina, Banda')
    Left = 636
    Top = 68
    object QrCarnes0Pagina: TIntegerField
      FieldName = 'Pagina'
      Origin = 'carne0.Pagina'
    end
    object QrCarnes0Carne: TIntegerField
      FieldName = 'Carne'
      Origin = 'carne0.Carne'
    end
    object QrCarnes0Folha: TIntegerField
      FieldName = 'Folha'
      Origin = 'carne0.Folha'
    end
    object QrCarnes0Vencto: TDateField
      FieldName = 'Vencto'
      Origin = 'carne0.Vencto'
    end
    object QrCarnes0SUB_TOT: TFloatField
      FieldName = 'SUB_TOT'
      Origin = 'carne0.SUB_TOT'
    end
    object QrCarnes0Boleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'carne0.Boleto'
    end
    object QrCarnes0Unidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'carne0.Unidade'
      Size = 10
    end
    object QrCarnes0E_ALL: TWideStringField
      FieldName = 'E_ALL'
      Origin = 'carne0.E_ALL'
      Size = 255
    end
    object QrCarnes0PROPRI_E_MORADOR: TWideStringField
      FieldName = 'PROPRI_E_MORADOR'
      Origin = 'carne0.PROPRI_E_MORADOR'
      Size = 255
    end
    object QrCarnes0CODIGOBARRAS: TWideStringField
      FieldName = 'CODIGOBARRAS'
      Origin = 'carne0.CODIGOBARRAS'
      Size = 60
    end
    object QrCarnes0NossoNumero: TWideStringField
      FieldName = 'NossoNumero'
      Origin = 'carne0.NossoNumero'
      Size = 60
    end
    object QrCarnes0LinhaDigitavel: TWideStringField
      FieldName = 'LinhaDigitavel'
      Origin = 'carne0.LinhaDigitavel'
      Size = 60
    end
    object QrCarnes0Imprimir: TIntegerField
      FieldName = 'Imprimir'
      Origin = 'carne0.Imprimir'
    end
    object QrCarnes0PWD_WEB: TWideStringField
      FieldName = 'PWD_WEB'
      Size = 255
    end
  end
  object QrFPC: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT Carne, COUNT(Carne) Folhas'
      'FROM carne0'
      'GROUP BY Carne'
      'ORDER BY Folhas DESC')
    Left = 692
    Top = 68
  end
  object QrBol: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrBolBeforeOpen
    BeforeClose = QrBolBeforeClose
    AfterScroll = QrBolAfterScroll
    OnCalcFields = QrBolCalcFields
    Left = 604
    Top = 40
    object QrBolApto: TIntegerField
      FieldName = 'Apto'
      Origin = 'Apto'
      Required = True
    end
    object QrBolUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'Unidade'
      Size = 10
    end
    object QrBolVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBolPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'Propriet'
      Required = True
    end
    object QrBolNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Origin = 'NOMEPROPRIET'
      Size = 100
    end
    object QrBolBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrBolUSERNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'USERNAME'
      LookupDataSet = QrUsers
      LookupKeyFields = 'CodigoEsp'
      LookupResultField = 'Username'
      KeyFields = 'Apto'
      Size = 100
      Lookup = True
    end
    object QrBolPASSWORD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'PASSWORD'
      LookupDataSet = QrUsers
      LookupKeyFields = 'CodigoEsp'
      LookupResultField = 'Password'
      KeyFields = 'Apto'
      Size = 100
      Lookup = True
    end
    object QrBolPWD_WEB: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PWD_WEB'
      Size = 255
      Calculated = True
    end
    object QrBolPROTOCOLO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'PROTOCOLO'
      LookupDataSet = QrPPI
      LookupKeyFields = 'Docum'
      LookupResultField = 'PROTOCOLO'
      KeyFields = 'Boleto'
      DisplayFormat = '00000;-000000; '
      Lookup = True
    end
    object QrBolAndar: TIntegerField
      FieldName = 'Andar'
      Origin = 'Andar'
    end
    object QrBolBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'Boleto'
      Required = True
    end
    object QrBolBLOQUETO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'BLOQUETO'
      Calculated = True
    end
    object QrBolLOTE_PROTOCO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LOTE_PROTOCO'
      LookupDataSet = QrPPI
      LookupKeyFields = 'Docum'
      LookupResultField = 'LOTE'
      KeyFields = 'Boleto'
      Lookup = True
    end
    object QrBolOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrBolFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
    end
    object QrBolPrev: TIntegerField
      FieldName = 'Prev'
      Required = True
    end
    object QrBolCond: TIntegerField
      FieldName = 'Cond'
    end
    object QrBolSUB_TOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOT'
      Calculated = True
    end
    object QrBolKGT: TIntegerField
      FieldName = 'KGT'
    end
    object QrBolPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBolCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
  end
  object QrLei: TmySQLQuery
    Database = Dmod.MyDB
    Left = 660
    Top = 40
    object QrLeiValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrArr: TmySQLQuery
    Database = Dmod.MyDB
    Left = 688
    Top = 40
    object QrArrValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCNAB_Cfg_B: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      
        'IF(ent.Tipo=0, ent.EUF, ent.PUF) + 0.000 UF, ufs.Nome NOMEUF, bc' +
        's.DVB,'
      'bcs.Nome NOMEBANCO, car.Ativo CART_ATIVO,'
      'car.TipoDoc CAR_TIPODOC, con.*,'
      'IF(ced.Tipo=0, ced.RazaoSocial, ced.Nome) NOMECED,'
      'IF(sac.Tipo=0, sac.RazaoSocial, sac.Nome) NOMESAC,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCNUM'
      'FROM cond con'
      'LEFT JOIN entidades ced ON ced.Codigo=con.Cedente'
      'LEFT JOIN entidades sac ON sac.Codigo=con.SacadAvali'
      'LEFT JOIN entidades ent ON ent.Codigo=con.Cliente'
      'LEFT JOIN bancos   bcs ON bcs.Codigo=con.Banco'
      'LEFT JOIN carteiras car ON car.Codigo=con.CartEmiss'
      
        'LEFT JOIN ufs   ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PU' +
        'F)'
      'WHERE con.Codigo=:P0')
    Left = 168
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_Cfg_BNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCNAB_Cfg_BLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Size = 127
    end
    object QrCNAB_Cfg_BNOMECED: TWideStringField
      FieldName = 'NOMECED'
      Size = 100
    end
    object QrCNAB_Cfg_BEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
    end
    object QrCNAB_Cfg_BACEITETIT_TXT: TWideStringField
      FieldName = 'ACEITETIT_TXT'
      Size = 1
    end
    object QrCNAB_Cfg_BCART_IMP: TWideStringField
      FieldName = 'CART_IMP'
      Size = 10
    end
    object QrCNAB_Cfg_BEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Size = 5
    end
    object QrCNAB_Cfg_BCedBanco: TIntegerField
      FieldName = 'CedBanco'
    end
    object QrCNAB_Cfg_BAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Size = 40
    end
    object QrCNAB_Cfg_BCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
    end
    object QrCNAB_Cfg_BCedPosto: TIntegerField
      FieldName = 'CedPosto'
    end
    object QrCNAB_Cfg_BCedConta: TWideStringField
      FieldName = 'CedConta'
      Size = 30
    end
    object QrCNAB_Cfg_BCartNum: TWideStringField
      FieldName = 'CartNum'
      Size = 3
    end
    object QrCNAB_Cfg_BIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Size = 2
    end
    object QrCNAB_Cfg_BCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
    end
    object QrCNAB_Cfg_BTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrCNAB_Cfg_BCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrCNAB_Cfg_BCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCNAB_Cfg_BModalCobr: TIntegerField
      FieldName = 'ModalCobr'
    end
    object QrCNAB_Cfg_BMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrCNAB_Cfg_BJurosPerc: TFloatField
      FieldName = 'JurosPerc'
    end
    object QrCNAB_Cfg_BTexto01: TWideStringField
      FieldName = 'Texto01'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto02: TWideStringField
      FieldName = 'Texto02'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto03: TWideStringField
      FieldName = 'Texto03'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto04: TWideStringField
      FieldName = 'Texto04'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto05: TWideStringField
      FieldName = 'Texto05'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto06: TWideStringField
      FieldName = 'Texto06'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto07: TWideStringField
      FieldName = 'Texto07'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto08: TWideStringField
      FieldName = 'Texto08'
      Size = 100
    end
    object QrCNAB_Cfg_BCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Size = 1
    end
    object QrCNAB_Cfg_BCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Size = 1
    end
    object QrCNAB_Cfg_BOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Size = 3
    end
    object QrCNAB_Cfg_BLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrCNAB_Cfg_BNOMESAC: TWideStringField
      FieldName = 'NOMESAC'
      Size = 100
    end
    object QrCNAB_Cfg_BCorreio: TWideStringField
      FieldName = 'Correio'
      Size = 1
    end
    object QrCNAB_Cfg_BCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrCNAB_Cfg_BCedente: TIntegerField
      FieldName = 'Cedente'
    end
    object QrCNAB_Cfg_BCAR_TIPODOC: TIntegerField
      FieldName = 'CAR_TIPODOC'
    end
    object QrCNAB_Cfg_BCART_ATIVO: TIntegerField
      FieldName = 'CART_ATIVO'
    end
    object QrCNAB_Cfg_BDVB: TWideStringField
      FieldName = 'DVB'
      Size = 1
    end
    object QrCNAB_Cfg_BNosNumFxaU: TFloatField
      FieldName = 'NosNumFxaU'
    end
    object QrCNAB_Cfg_BNosNumFxaI: TFloatField
      FieldName = 'NosNumFxaI'
    end
    object QrCNAB_Cfg_BNosNumFxaF: TFloatField
      FieldName = 'NosNumFxaF'
    end
    object QrCNAB_Cfg_BCodigo: TFloatField
      FieldName = 'Codigo'
    end
    object QrCNAB_Cfg_BCorresBco: TIntegerField
      FieldName = 'CorresBco'
    end
    object QrCNAB_Cfg_BCorresAge: TIntegerField
      FieldName = 'CorresAge'
    end
    object QrCNAB_Cfg_BCorresCto: TWideStringField
      FieldName = 'CorresCto'
      Size = 30
    end
    object QrCNAB_Cfg_BNPrinBc: TWideStringField
      FieldName = 'NPrinBc'
      Size = 30
    end
  end
  object frxDsCNAB_Cfg_B: TfrxDBDataset
    UserName = 'frxDsCNAB_Cfg_B'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEBANCO=NOMEBANCO'
      'LocalPag=LocalPag'
      'NOMECED=NOMECED'
      'EspecieDoc=EspecieDoc'
      'ACEITETIT_TXT=ACEITETIT_TXT'
      'CART_IMP=CART_IMP'
      'EspecieVal=EspecieVal'
      'CedBanco=CedBanco'
      'AgContaCed=AgContaCed'
      'CedAgencia=CedAgencia'
      'CedPosto=CedPosto'
      'CedConta=CedConta'
      'CartNum=CartNum'
      'IDCobranca=IDCobranca'
      'CodEmprBco=CodEmprBco'
      'TipoCobranca=TipoCobranca'
      'CNAB=CNAB'
      'CtaCooper=CtaCooper'
      'ModalCobr=ModalCobr'
      'MultaPerc=MultaPerc'
      'JurosPerc=JurosPerc'
      'Texto01=Texto01'
      'Texto02=Texto02'
      'Texto03=Texto03'
      'Texto04=Texto04'
      'Texto05=Texto05'
      'Texto06=Texto06'
      'Texto07=Texto07'
      'Texto08=Texto08'
      'CedDAC_C=CedDAC_C'
      'CedDAC_A=CedDAC_A'
      'OperCodi=OperCodi'
      'LayoutRem=LayoutRem'
      'NOMESAC=NOMESAC'
      'Correio=Correio'
      'CartEmiss=CartEmiss'
      'Cedente=Cedente'
      'CAR_TIPODOC=CAR_TIPODOC'
      'CART_ATIVO=CART_ATIVO'
      'DVB=DVB'
      'NosNumFxaU=NosNumFxaU'
      'NosNumFxaI=NosNumFxaI'
      'NosNumFxaF=NosNumFxaF'
      'Codigo=Codigo'
      'CorresBco=CorresBco'
      'CorresAge=CorresAge'
      'CorresCto=CorresCto'
      'NPrinBc=NPrinBc')
    DataSet = QrCNAB_Cfg_B
    BCDToCurrency = False
    Left = 196
    Top = 364
  end
end
