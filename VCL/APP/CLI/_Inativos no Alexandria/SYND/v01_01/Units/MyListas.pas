unit MyListas;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, UnMLAGeral, dmkGeral, UnDmkProcFunc, UnDmkEnums;

type
  TMyListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function CriaListaImpDOS(FImpDOS: TStringList): Boolean;
    function CriaListaUserSets(FUserSets: TStringList): Boolean;
    //
    function CriaListaTabelas(Database: TmySQLDatabase; Lista:
              TList<TTabelas>): Boolean;
    //function CriaListaTabelas(Database: TmySQLDatabase; FTabelas: TStringList): Boolean;
    function CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
    function CriaListaIndices(TabelaBase, TabelaNome: String;
             FLIndices: TList<TIndices>): Boolean;
    function CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
             var TemControle: TTemControle): Boolean;
    function CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
    function CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function ExcluiTab: Boolean;
    function ExcluiReg: Boolean;
    function ExcluiIdx: Boolean;
    procedure VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
              DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ConfiguracoesIniciais(UsaCoresRel: Integer; AppIDtxt: String);
  end;

const
  VAR_TIPO_LINK_ID_01_GENERICO = 1;
  VAR_TIPO_LINK_ID_02_FATURA_PROD_SERV = 2;
  //
  CO_VERSAO = 1511031147;
  CO_SIGLA_APP = 'SYND';
  CO_DMKID_APP = 4;
  CO_GRADE_APP = False;
  CO_VLOCAL = True;
  CO_EXTRA_LCT_003 = False; // True somente para Credito2

var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;
  //
  _ArrClieSets: array[01..32] of String;
  _MaxClieSets: Integer;
  _ArrFornSets: array[01..32] of String;
  _MaxFornSets: Integer;

implementation

uses Module, UMySQLModule, UnDiario_Tabs, UnIBGE_DTB_Tabs, UnFinance_Tabs,
  WTextos_Tabs, UnProtocol_Tabs, UnPerfJan_Tabs, FPMin_Tabs, FPMax_Tabs,
  NFSe_TbTerc, NFSe_Tabs, Geral_TbTerc, ModuleGeral, SMS_Tabs, InflIdx_Tabs,
  Mail_Tabs, WUsers_Tabs, FTP_Tabs, UnCNAB_Tabs, UnEmpresas_Tabs, UnEnti_Tabs,
  UnALL_Tabs, UnAnotacoes_Tabs, UnMoedas_Tabs, UnTextos_Tabs, UnCheques_Tabs,
  UnEntities, WGerl_Tabs;

function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista:
  TList<TTabelas>): Boolean;
  //
  procedure TabelasPorCliente((*Tab: TTabsCli*));
  var
    TbCI: String;
    CliInt: Integer;
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW TABLES');
    Dmod.QrAux.SQL.Add('FROM ' + TMeuDB);
    Dmod.QrAux.SQL.Add('LIKE "entidades"');
    Dmod.QrAux.Open;
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT cliint');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE cliint <> 0');
      Dmod.QrAux.Open;
      //
      while not Dmod.QrAux.Eof do
      begin
        CliInt := Dmod.QrAux.FieldByName('cliint').AsInteger;
        { N�o permitir n�meros negativos para filial!
        if CliInt < 0 then
          TbCI := '_' + FormatFloat('000', CliInt )
        else
          TbCI := FormatFloat('0000', CliInt);
        }
        if CliInt > 0 then
        begin
          TbCI := FormatFloat('0000', CliInt);
          //
          MyLinguas.AdTbLst(Lista, False, Lowercase('prv' + TbCI + 'A'), LowerCase(TAB_PRV));
          MyLinguas.AdTbLst(Lista, False, Lowercase('prv' + TbCI + 'B'), LowerCase(TAB_PRV));
          MyLinguas.AdTbLst(Lista, False, Lowercase('prv' + TbCI + 'D'), LowerCase(TAB_PRV));

          MyLinguas.AdTbLst(Lista, False, Lowercase('pri' + TbCI + 'A'), LowerCase(TAB_PRI));
          MyLinguas.AdTbLst(Lista, False, Lowercase('pri' + TbCI + 'B'), LowerCase(TAB_PRI));
          MyLinguas.AdTbLst(Lista, False, Lowercase('pri' + TbCI + 'D'), LowerCase(TAB_PRI));

          MyLinguas.AdTbLst(Lista, False, Lowercase('cns' + TbCI + 'A'), LowerCase(TAB_CNS));
          MyLinguas.AdTbLst(Lista, False, Lowercase('cns' + TbCI + 'B'), LowerCase(TAB_CNS));
          MyLinguas.AdTbLst(Lista, False, Lowercase('cns' + TbCI + 'D'), LowerCase(TAB_CNS));

          MyLinguas.AdTbLst(Lista, False, Lowercase('ari' + TbCI + 'A'), LowerCase(TAB_ARI));
          MyLinguas.AdTbLst(Lista, False, Lowercase('ari' + TbCI + 'B'), LowerCase(TAB_ARI));
          MyLinguas.AdTbLst(Lista, False, Lowercase('ari' + TbCI + 'D'), LowerCase(TAB_ARI));

          if Database.DatabaseName = VAR_DBWEB then
          begin
            MyLinguas.AdTbLst(Lista, False, Lowercase('lct' + TbCI + 'A'), LowerCase(LAN_CTOS));
            MyLinguas.AdTbLst(Lista, False, Lowercase('lct' + TbCI + 'B'), LowerCase(LAN_CTOS));
            MyLinguas.AdTbLst(Lista, False, Lowercase('lct' + TbCI + 'D'), LowerCase(LAN_CTOS));
          end;
        end;
        //
        Dmod.QrAux.Next;
      end;
    end;
  end;
begin
  Result := True;
  try
    if Database = Dmod.MyDB then
    begin
      //CashTb.CarregaListaTabelasCashier(Lista);
      //
      Empresas_Tabs.CarregaListaTabelas(Lista);
      Enti_Tabs.CarregaListaTabelas(Lista);
      Finance_Tabs.CarregaListaTabelas(Lista);
      ALL_Tabs.CarregaListaTabelas(Lista);
      Anotacoes_Tabs.CarregaListaTabelas(Lista);
      Moedas_Tabs.CarregaListaTabelas(Lista);
      Textos_Tabs.CarregaListaTabelas(Lista);
      Cheques_Tabs.CarregaListaTabelas(Lista);
      //
      MyLinguas.AdTbLst(Lista, False, 'Animais', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'Arre', '');
      MyLinguas.AdTbLst(Lista, False, 'ArreFut', '');
      MyLinguas.AdTbLst(Lista, False, TAB_ARI, '');
      MyLinguas.AdTbLst(Lista, False, 'ArreBaC', '');
      MyLinguas.AdTbLst(Lista, False, 'ArreBaI', '');
      MyLinguas.AdTbLst(Lista, False, 'ArreBaU', '');
      MyLinguas.AdTbLst(Lista, False, 'ArreRat', '');
      //
      //Linguas.AdTbLst(Lista, False, 'BancosArq', '');
      MyLinguas.AdTbLst(Lista, False, 'BancosBlo', '');
      //Linguas.AdTbLst(Lista, False, 'BancosLey', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'bloqparc', '');
      MyLinguas.AdTbLst(Lista, False, 'bloqparcpar', '');
      MyLinguas.AdTbLst(Lista, False, 'bloqparcits', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'Cargos', '');
      MyLinguas.AdTbLst(Lista, False, 'CB4Data4', '');
      MyLinguas.AdTbLst(Lista, False, 'CNAB240Dir', '');
      MyLinguas.AdTbLst(Lista, False, 'CNAB_Dir', '');
      MyLinguas.AdTbLst(Lista, False, 'CNAB240Lei', '');
      MyLinguas.AdTbLst(Lista, False, 'Cond', '');
      MyLinguas.AdTbLst(Lista, False, 'CondAnimais', '');
      MyLinguas.AdTbLst(Lista, False, 'CondBloco', '');
      MyLinguas.AdTbLst(Lista, False, 'CondEmeios', '');
      MyLinguas.AdTbLst(Lista, False, 'CondGri', '');
      MyLinguas.AdTbLst(Lista, False, 'CondGriImv', '');
      MyLinguas.AdTbLst(Lista, False, 'CondImov', '');
      MyLinguas.AdTbLst(Lista, False, 'CondModBol', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'ConfigBol', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'Controle', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'Cons', '');
      MyLinguas.AdTbLst(Lista, False, 'ConsPrc', '');
      MyLinguas.AdTbLst(Lista, False, TAB_CNS, '');
      //
      //
      MyLinguas.AdTbLst(Lista, False, 'Filhos', '');
      MyLinguas.AdTbLst(Lista, False, 'Financas', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'FlxBloq', '');
      MyLinguas.AdTbLst(Lista, False, 'FlxMens', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'ImpCHMsg', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'OutMor', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'Prev', '');
      MyLinguas.AdTbLst(Lista, False, TAB_PRI, '');
      MyLinguas.AdTbLst(Lista, False, 'PrevBaC', '');
      MyLinguas.AdTbLst(Lista, False, 'PrevBaI', '');
      MyLinguas.AdTbLst(Lista, False, 'PrevBaILct', '');
      MyLinguas.AdTbLst(Lista, False, 'PrevModBol', '');
      MyLinguas.AdTbLst(Lista, False, 'PrevRat', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'Status', '');
      MyLinguas.AdTbLst(Lista, False, 'users', '');
      MyLinguas.AdTbLst(Lista, False, 'Veic', '');
      MyLinguas.AdTbLst(Lista, False, 'wclients', '');
      //
      TabelasPorCliente();
      //
      PerfJan_Tabs.CarregaListaTabelas(Lista);
      CNAB_Tabs.CarregaListaTabelas(Database, Lista);
      Diario_Tabs.CarregaListaTabelas(Lista);
      Protocol_Tabs.CarregaListaTabelas(Database, Lista);
      UnFPMin_Tabs.CarregaListaTabelas(Lista);
      UnFPMax_Tabs.CarregaListaTabelas(Lista);
      UnNFSe_Tabs.CarregaListaTabelas(Lista);
      UnSMS_Tabs.CarregaListaTabelas(Lista);
      UnInflIdx_Tabs.CarregaListaTabelas(Lista);
      Mail_Tb.CarregaListaTabelas(Lista);
   end else
   if Database.DatabaseName = TLocDB then
   begin
      MyLinguas.AdTbLst(Lista, False, 'ImpCH', '');
    end else
    if Database.DatabaseName = VAR_DBWEB then
    begin
      //Tem que ser aqui
      WTextos_Tb.CarregaListaTabelas(Database, Lista);
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase(VAR_LCT), '');
      MyLinguas.AdTbLst(Lista, False, 'bloqparc', '');
      MyLinguas.AdTbLst(Lista, False, 'bloqparcpar', '');
      MyLinguas.AdTbLst(Lista, False, 'bloqparcits', '');
      MyLinguas.AdTbLst(Lista, False, 'carteiras', '');
      MyLinguas.AdTbLst(Lista, False, 'cond', '');
      MyLinguas.AdTbLst(Lista, False, 'condbloco', '');
      MyLinguas.AdTbLst(Lista, False, 'condgri', '');
      MyLinguas.AdTbLst(Lista, False, 'condgriimv', '');
      MyLinguas.AdTbLst(Lista, False, 'condimov', '');
      MyLinguas.AdTbLst(Lista, False, 'dirweb', '');
      MyLinguas.AdTbLst(Lista, False, 'entidades', '');
      MyLinguas.AdTbLst(Lista, False, 'enticliint', '');
      MyLinguas.AdTbLst(Lista, False, 'feriados', '');
      MyLinguas.AdTbLst(Lista, False, 'users', '');
      MyLinguas.AdTbLst(Lista, False, 'wclients', '');
      MyLinguas.AdTbLst(Lista, False, 'Lanctoz', LAN_CTOS);
      MyLinguas.AdTbLst(Lista, False, 'PreEmail', '');
      MyLinguas.AdTbLst(Lista, False, 'PreEmMsg', '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('CNAB_Cfg'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('bacen_pais'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('dtb_munici'), '');
      // s� web
      MyLinguas.AdTbLst(Lista, False, 'uploads', '');
      MyLinguas.AdTbLst(Lista, False, 'visitas', '');
      MyLinguas.AdTbLst(Lista, False, 'visitasits', '');
      MyLinguas.AdTbLst(Lista, False, 'visitascli', '');
      MyLinguas.AdTbLst(Lista, False, 'wcond', '');
      MyLinguas.AdTbLst(Lista, False, 'wfotos', '');
      MyLinguas.AdTbLst(Lista, False, 'wfotosedit', '');
      MyLinguas.AdTbLst(Lista, False, 'wslidefts', '');
      //
      TabelasPorCliente();
      //
      WUsers_Tb.CarregaListaTabelas(Database, Lista, CO_DMKID_APP);
      WGerl_Tb.CarregaListaTabelas(Database, Lista, CO_DMKID_APP);
      FTP_Tb.CarregaListaTabelas(Database, Lista);
      Protocol_Tabs.CarregaListaTabelas(Database, Lista);
      Finance_Tabs.ComplementaListaComLcts(Lista);
    end;
    if DModG <> nil then
    begin
      if Database = DModG.AllID_DB then
      begin
        UnGeral_TbTerc.CarregaListaTabelas(Lista);
        IBGE_DTB_Tabs.CarregaListaTabelas(Lista);
        UnNFSe_TbTerc.CarregaListaTabelas(Lista);
      end;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    //CashTb.CarregaListaTabelasLocaisCashier(Lista);
    Finance_Tabs.CarregaListaTabelasLocais(Lista);
    //
    MyLinguas.AdTbLst(Lista, False, 'ArreBAA', '');
    MyLinguas.AdTbLst(Lista, False, 'ArreBAAUni', '');
    MyLinguas.AdTbLst(Lista, False, 'ChequesImp', '');
    MyLinguas.AdTbLst(Lista, False, 'CNAB_1', '');
    MyLinguas.AdTbLst(Lista, False, 'Concilia', '');
    MyLinguas.AdTbLst(Lista, False, 'ConsLei', '');
    MyLinguas.AdTbLst(Lista, False, 'CtasItsCtas', '');
    MyLinguas.AdTbLst(Lista, False, 'Enderecos', '');
    MyLinguas.AdTbLst(Lista, False, 'ExtratoCC', '');
    MyLinguas.AdTbLst(Lista, False, 'ExtratoCC2', '');
    MyLinguas.AdTbLst(Lista, False, 'InfoSeq', '');
    MyLinguas.AdTbLst(Lista, False, 'LocCtrl', '');
    //Linguas.AdTbLst(Lista, False, 'Mensalidade', '');
    MyLinguas.AdTbLst(Lista, False, 'MinhaEtiq', '');
    MyLinguas.AdTbLst(Lista, False, 'PrevBAB', '');
    MyLinguas.AdTbLst(Lista, False, 'PrevBAN', '');
    MyLinguas.AdTbLst(Lista, False, 'SdoNiveis', '');
    MyLinguas.AdTbLst(Lista, False, 'UnidCond', '');
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('ArreBaC') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"(Sem arrecada��o base)"');
    end else
    if Uppercase(Tabela) = Uppercase('ConfigBol') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('-1|"Padr�o"');
    end else
    if Uppercase(Tabela) = Uppercase('users') then
    begin
      FListaSQL.Add('User_ID|Username|Password|Tipo');
      FListaSQL.Add('-1|"admin"|"8i27ot4r"|9');
    end else
    if Uppercase(Tabela) = Uppercase('PerfisIts') then
    begin
      FListaSQL.Add('Janela|Descricao'); //N�O PODE TER V�RGULA NOS TEXTOS!!!!!!
      //FListaSQL.Add('""|""');
    end else
    {
    if Uppercase(Tabela) = Uppercase('ProtocoOco') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"Lan�amentos"');
    end else
    }
    begin
      PerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Empresas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Enti_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Finance_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      ALL_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Anotacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Moedas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Textos_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Cheques_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      //CashTb.CarregaListaSQLCashier(Tabela, FListaSQL);
      CNAB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Diario_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      WTextos_Tb.CarregaListaSQL(Tabela, FListaSQL);
      IBGE_DTB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Protocol_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      UnFPMin_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      UnFPMax_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      UnNFSe_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
      UnNFSe_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      UnGeral_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
      UnSMS_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      UnInflIdx_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      WUsers_Tb.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      WGerl_Tb.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      FTP_Tb.CarregaListaSQL(Tabela, FListaSQL);
      Mail_Tb.CarregaListaSQL(Tabela, FListaSQL);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
const
  Parecer: WideString =
'{\\rtf1\\ansi\\ansicpg1252\\deff0{\\fonttbl{\\f0\\fnil\\fcharset0 MS Shell ' +
'Dlg;}{\\f1\\fnil MS Sans Serif;}}' + #13#10+
'{\\colortbl ;\\red0\\green0\\blue0;}' + #13#10 +
'\\viewkind4\\uc1\\pard\\li277\\ri18\\tx720\\cf1\\lang1046\\f0\\fs20 Os abaixo ' +
'assinados, membros do Conselho Fiscal, declaram que examinaram o Demonstrativo ' +
'relativo ao m�s de [MES_ANO ], bem como a documenta��o de Receitas e ' +
'Despesas, raz�o pela qual s�o de parecer favor�vel a aprova��o e por estar o ' +
'Demonstrativo de presta��o de contas em perfeita ordem e exatid�o.\\cf0\\par' +
'\\pard\\cf1\\f1\\fs16\\par' + #13#10 +
'}';
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Contas') then
    begin
      (*FListaSQL.Add('-129|"Compra de mercadorias diversas"');
      FListaSQL.Add('-130|"Frete de mercadorias"');
      FListaSQL.Add('-131|"Compra de filmes"');
      FListaSQL.Add('-132|"Frete de filmes"');
      FListaSQL.Add('-133|"Inv�lido"');
      FListaSQL.Add('-134|"Venda e/ou loca��o"');*)
    end else
    if Uppercase(Tabela) = Uppercase('EntiCfgRel') then
    begin
      // j� tem no CashTabs
      //FListaSQL.Add('Controle|Codigo|RelTipo|RelItem|Ordem|RelTitu|RelPrcr');
      FListaSQL.Add(' -1| 0|  1|  1|  1| "BALANCETE DE VERIFICA��O"| ""');
      FListaSQL.Add(' -2| 0|  1|  2|  2| "PARECER DO CONSELHO"|"'+Parecer+'"');
      FListaSQL.Add(' -3| 0|  1| 11|  3| "COMPOSI��O DA PROVIS�O EFETUADA PARA ARRECADA��O"|""');
      FListaSQL.Add(' -4| 0|  1| 12|  4| "PROCESSAMENTO DE BLOQUETOS DE COBRAN�A"|""');
      FListaSQL.Add(' -5| 0|  1|  5|  5| "EXTRATO DAS CONTAS CORRENTES NO PER�ODO"|""');
      FListaSQL.Add(' -6| 0|  1|  8|  6| "DEMONSTRATIVO DE RECEITAS E DESPESAS NO PER�ODO"|""');
      FListaSQL.Add(' -7| 0|  1| 13|  7| "PEND�NCIAS DE COND�MINOS"|""');
      FListaSQL.Add(' -8| 0|  1| 14|  8| "REPARCELAMENTOS DE PEND�NCIAS"|""');
      FListaSQL.Add(' -9| 0|  1| 15|  9| "REPARCELAMENTOS EM ABERTO"|""');
      FListaSQL.Add('-10| 0|  1| 16| 10| "DOCUMENTOS COMPENSADOS NO PER�ODO"|""');
    end else
    if Uppercase(Tabela) = Uppercase('UserTxts') then
    begin
      FListaSQL.Add('Codigo|Ordem|TxtSys|TxtMeu');
      FListaSQL.Add('-1|01|"Condom�nios"|"Condom�nios"');
      FListaSQL.Add('-2|02|"Condom�nio"|"Condom�nio"');
      FListaSQL.Add('-3|03|"Cond."|"Cond."');
      FListaSQL.Add('-4|04|"Condom."|"Condom."');
      FListaSQL.Add('-5|05|"Cond�minos"|"Cond�minos"');
      FListaSQL.Add('-6|06|"Cond�mino"|"Cond�mino"');
      FListaSQL.Add('-7|07|"Propriet�rios"|"Propriet�rios"');
      FListaSQL.Add('-8|08|"Propriet�rio"|"Propriet�rio"');
      FListaSQL.Add('-9|09|"Prop."|"Prop."');
      FListaSQL.Add('-10|10|"Administradora de riscos"|"Administradora de riscos"');
      FListaSQL.Add('-11|11|"Imobili�rias"|"Imobili�rias"');
      FListaSQL.Add('-12|12|"Imobili�ria"|"Imobili�ria"');
      FListaSQL.Add('-13|13|"Moradores"|"Moradores"');
      FListaSQL.Add('-14|14|"Morador"|"Morador"');
      FListaSQL.Add('-15|15|"Inquilinos"|"Inquilinos"');
      FListaSQL.Add('-16|16|"Inquilino"|"Inquilino"');
      FListaSQL.Add('-17|17|"Unidades Habitacionais"|"Unidades Habitacionais"');
      FListaSQL.Add('-18|18|"Unidades habitacionais"|"Unidades habitacionais"');
      FListaSQL.Add('-19|19|"Unidade Habitacional"|"Unidade Habitacional"');
      FListaSQL.Add('-20|20|"Unidade habitacional"|"Unidade habitacional"');
      FListaSQL.Add('-21|21|"Unidades"|"Unidades"');
      FListaSQL.Add('-22|22|"Unidade"|"Unidade"');
      FListaSQL.Add('-23|23|"Im�veis"|"Im�veis"');
      FListaSQL.Add('-24|24|"Im�vel"|"Im�vel"');
      FListaSQL.Add('-25|25|"UHs"|"UHs"');
      FListaSQL.Add('-26|26|"UH"|"UH"');
      FListaSQL.Add('-27|27|"U.H.s"|"U.H.s"');
      FListaSQL.Add('-28|28|"U.H."|"U.H."');
      FListaSQL.Add('-29|29|"Blocos"|"Blocos"');
      FListaSQL.Add('-30|30|"Bloco"|"Bloco"');
      FListaSQL.Add('-31|31|"Andares"|"Andares"');
      FListaSQL.Add('-32|32|"Andar"|"Andar"');
      FListaSQL.Add('-33|33|"S�ndicos"|"S�ndicos"');
      FListaSQL.Add('-34|34|"S�ndico"|"S�ndico"');
      FListaSQL.Add('-35|35|"da cota condominial"|"da cota condominial"');
      FListaSQL.Add('-36|36|"Cota condominial"|"Cota condominial"');
      FListaSQL.Add('-37|37|"da quota condominial"|"da quota condominial"');
      FListaSQL.Add('-38|38|"Quota condominial"|"Quota condominial"');
      FListaSQL.Add('-39|39|"Condominial"|"Condominial"');
      FListaSQL.Add('-40|40|"Condominiais"|"Condominiais"');
    end;
    //
    PerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Finance_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ALL_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Anotacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    IBGE_DTB_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Textos_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Cheques_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    CNAB_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Diario_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    WTextos_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Protocol_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnFPMin_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnFPMax_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnNFSe_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    UnNFSe_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnGeral_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    UnSMS_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnInflIdx_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    WUsers_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    WGerl_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    FTP_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Mail_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    //
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('Animais') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Arre') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ArreFut') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase(TAB_ARI) then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ArreBaC') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ArreBaI') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ArreBaU') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ArreRat') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    {
    end else if Uppercase(TabelaBase) = Uppercase('BancosArq') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Banco';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Entidade';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 3;
      FRIndices.Column_name   := 'ProtoLote';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 4;
      FRIndices.Column_name   := 'Arquivo';
      FLIndices.Add(FRIndices);
      //
    }  
    end else if Uppercase(TabelaBase) = Uppercase('BancosBlo') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Entidade';
      FLIndices.Add(FRIndices);
      //
    {end else if Uppercase(TabelaBase) = Uppercase('BancosLey') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Ocorrencia';
      FLIndices.Add(FRIndices);
      //}
    end else if Uppercase(TabelaBase) = Uppercase('bloqparc') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('bloqparcpar') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('bloqparcits') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Cargos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CB4Data4') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Empresa';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Tabela';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CNAB240Dir') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CNAB_Dir') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CNAB240Lei') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Cond') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Condgri') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CondgriImv') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CondAnimais') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'SubConta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CondBloco') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CondEmeios') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Item';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CondImov') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
      {
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      }
    end else if Uppercase(TabelaBase) = Uppercase('CondModBol') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Apto';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ConfigBol') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Cons') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ConsPrc') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Cond';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('DirWeb') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase(TAB_CNS) then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Veic') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'SubConta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Filhos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'SubConta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Financas') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ImpCH') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Tabela';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('FlxMens') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'AnoMes';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('FlxBloq') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'AnoMes';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ImpCHMsg') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Tabela';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('OutMor') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'SubConta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Prev') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase(TAB_PRI) then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PrevBaC') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PrevBaI') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PrevBaILct') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Lancto';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PrevModBol') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Apto';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PrevRat') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    {
    end else if Uppercase(TabelaBase) = Uppercase('Protocolos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ProtocoOco') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ProtocoMot') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ProtocoPak') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ProtPakIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    }
    end else if Uppercase(TabelaBase) = Uppercase('Status') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'CodUsu';
      FLIndices.Add(FRIndices);
      // Fazer
{
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE2';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Sigla';
      FLIndices.Add(FRIndices);
      //
}
    end else if Uppercase(TabelaBase) = Uppercase('uploads') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('users') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'User_ID';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('visitasits') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('visitascli') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('wclients') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'User_ID';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('wcond') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('wfotos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('wfotosedit') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('wslidefts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else begin
      PerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Empresas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Enti_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Finance_Tabs.CarregaListaFRIndices(TabelaBase, TabelaNome, FRIndices, FLIndices);
      ALL_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Anotacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Moedas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Textos_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Cheques_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      IBGE_DTB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //CashTb.CarregaListaFRIndicesCashier(TabelaBase, TabelaNome, FRIndices, FLindices);
      CNAB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Diario_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      WTextos_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Protocol_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnFPMin_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnFPMax_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnNFSe_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnNFSe_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnGeral_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnSMS_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnInflIdx_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      WUsers_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      WGerl_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      FTP_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Mail_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.ConfiguracoesIniciais(UsaCoresRel: Integer;
  AppIDtxt: String);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
  if (Uppercase(AppIDtxt) = 'SYNDIC') or (Uppercase(AppIDtxt) = 'SYNDI2') then
  begin
    Entities.ConfiguracoesIniciaisEntidades(CO_DMKID_APP);
  end else
    Geral.MensagemBox(
    'Database para configura��es de "CheckBox" n�o definidos!', 'Aviso!',
    MB_OK+MB_ICONWARNING);
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('Arre') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Gastos';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ArreFut') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Propriet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := TAB_ARI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Inclui';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase(TAB_ARI) then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Propriet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArreBaC';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArreBaI';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Boleto';
      FRCampos.Tipo       := 'double(20,0)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vencto';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lancto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (*New(FRCampos);
      FRCampos.Field      := 'CodBarra';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // *)
      New(FRCampos);
      FRCampos.Field      := 'ComoAdd'; //0=Desconhecido, 1=Arrecada��es Base
      FRCampos.Tipo       := 'tinyint(1)'; //2=Arrecada��es Futuras,
      FRCampos.Null       := ''; // 3=Manuaias(Multiplas UHs), 4=Manuais (UH �nica)
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoArreRisco';  // Pr�-Cond�mino
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'ComoExport'; // 0: N�o exporta
      FRCampos.Tipo       := 'tinyint(1)'; // 1: Valor �nico para todas UHS
      FRCampos.Null       := '';           // 2: Valor individual da UH
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('ArreBaC') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ArreBaI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SitCobr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parcelas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ParcPerI';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ParcPerF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfoParc';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fator';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Percent';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DeQuem';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DoQue';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComoCobra';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoArreSobre';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Calculo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arredonda';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.01';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ListaBaU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescriCota';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Cotas';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValFracDef';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValFracAsk';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Partilha';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfoRelArr';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoArreRisco';  // Pr�-Cond�mino
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DestCobra';  // 0=Normal 1=Pr�-Cond�mino
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arred_5ou4';  // 0="Cima", 1="5/4", 2="Baixo" ou Arred_Vari
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arred_Vari';  // 0="Cima", 1="5/4" ou 2="Baixo"
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ArreBaU') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Percent';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ArreRat') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Morardor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    {
    end else if Uppercase(Tabela) = Uppercase('Bancos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Site';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DVCC';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '10';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DVB';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_240i';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_240t';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_240r';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_240s';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'T';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_400i';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_400t';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (*New(FRCampos);
      FRCampos.Field      := 'Bloqueto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ano';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);*)
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsLinha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsData';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsHist';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsDocu';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsHiDo';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsCred';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsDebi';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsCrDb';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsDouC';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsTCDB'; // Tipo cont�bil de d�bito e cr�dito
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsComp';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsCPMF';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'XlsSldo';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescriCNR';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Tarifa CNR';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    }
    {
    end else if Uppercase(Tabela) = Uppercase('BancosArq') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Banco';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtoLote';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arquivo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    }
    end else if Uppercase(Tabela) = Uppercase('BancosBlo') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Bloqueto';
      FRCampos.Tipo       := 'double(15,0)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    {end else if Uppercase(Tabela) = Uppercase('BancosLey') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ocorrencia';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carrega';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Genero';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);}
      //
    end else if Uppercase(Tabela) = Uppercase('BloqParc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'loginID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '???';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'autentica';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodCliEsp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodCliEnt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodigoEnt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodigoEsp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrOrigi';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrMulta';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrJuros';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataP';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Novo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxaJur';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxaMul';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('BloqParcPar') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parcela';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatNum';
      FRCampos.Tipo       := 'double(20,0)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrOrigi';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrMulta';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrJuros';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValBol';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vencimento';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lancto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrDesco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('BloqParcIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parcela';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Seq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtrlOrigi';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BloqOrigi';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercIni';
      FRCampos.Tipo       := 'double(15,10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercFim';
      FRCampos.Tipo       := 'double(15,10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrOrigi';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrMulta';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrJuros';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrAjust';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrSomas';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SParcA';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SParcD';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dias';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrMult2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrJuro2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VlrLanct';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Cargos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CB4Data4') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela'; // 1="FOTODOC"
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Registro'; // �ltimo registro inserido
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CNAB240Dir') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Envio';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carteira';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CNAB_Dir') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Envio';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carteira';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CNAB240Lei') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Banco';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NossoNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SeuNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OcorrCodi';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OcorrData';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTitul';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValAbati';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValDesco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValPago';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValJuros';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValMulta';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo1';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo2';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo3';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo4';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo5';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QuitaData';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Diretorio';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arquivo';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Step';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carteira';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DevJuros';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DevMulta';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'TxtCnd_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Condom�nio';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtCnd_05';  // Caption
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Cond.';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUsu_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Cond�mino';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtPrp_15';  // Caption
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Propriet�rio';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtRis_25';  // Caption
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Administradora de riscos';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtImb_15';  // Caption
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Imobili�ria';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtMor_15';  // Caption
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Morador';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtMor_20';  // Caption
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Moradores';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUHs_20';  // Caption
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Unidade Habitacional';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUHs_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Unidade';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUHs_12';  // Caption
      FRCampos.Tipo       := 'varchar(12)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Unidades';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUHs_03';  // Caption
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'UH';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUHs_05';  // Caption
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'UHs';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtBlc_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Bloco';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtBlc_11';  // Caption
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Blocos';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtNiv_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Andar';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtNiv_11';  // Caption
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Andares';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtSin_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'S�ndico';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'AtualizouCamposWEB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BloqParcPar'; // Negativo!! para reparcelamentos locais!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BloqParc'; // Negativo!! para reparcelamentos locais!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdAutConfMail'; // Dias para auto confirma��o de recebimento de e-mail de bloqueto condominial
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '60';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PreMailReenv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaSincro';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LokMedAnt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondEmeios';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Raiz';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MyLogo';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Campos WEB
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Page';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //   FIM WEB
      //
      New(FRCampos);
      FRCampos.Field      := 'BAL_TopoNomCon';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BAL_TopoTitulo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '5000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BAL_TopoPeriod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '25000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BLQ_TopoAvisoV';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '20300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BLQ_MEsqAvisoV';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3500';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BLQ_AltuAvisoV';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BLQ_LargAvisoV';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '10000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BLQ_TopoDestin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '21300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BLQ_MEsqDestin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3500';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BLQ_AltuDestin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BLQ_LargDestin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '10000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfoSeq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Dir';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB240Lei';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB240Dir';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondGri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondGriImv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondBloco';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondImov';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConfigBol';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cons';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConsPrc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := TAB_CNS;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (*New(FRCampos);
      FRCampos.Field      := 'ConsPer';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //*)
      {
      New(FRCampos);
      FRCampos.Field      := 'Protocolos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtocoOco';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtocoMot';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtocoPak';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtPakIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      }
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Veic';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Filhos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Financas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OutMor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arre';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArreFut';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := TAB_ARI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArreBaC';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArreBaI';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArreBaU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArreRat';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Prev';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := TAB_PRI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevBaC';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevBaI';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevRat';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Animais';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondAnimais';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cargos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //  Fluxo de Bloquetos
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Folha';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_LeiAr';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Fecha';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Risco';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Print';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Proto';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Relat';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_EMail';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Porta';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Postl';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //  Fluxo de Encerramento de Per�odo
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Conci';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Docum';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Anali';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Contb';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Encad';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Entrg';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Web';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_Host';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_Port';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '23165';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_User';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := 'PUBLIC';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_Pwd';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_DB';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_Uso';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntiCargSind'; //Cargo utilizado para o s�ndico
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdPrtstCrt'; //Protestar ap�s x dias do recebimento de carta.
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Cond') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Andares';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TotApt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'Longtext';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Banco';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Agencia';
      FRCampos.Tipo       := 'int(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DVAgencia';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Posto';
      FRCampos.Tipo       := 'int(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DVConta';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodCedente';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgContaCed';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Caixa Econ�mica Federal: Tipo de cobran�a sem registro => 80, 81 ou 82
      New(FRCampos);
      FRCampos.Field      := 'IDCobranca';
      FRCampos.Tipo       := 'varchar(16)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiaVencto';
      FRCampos.Tipo       := 'int(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '5';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BcoLogoPath';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliLogoPath';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LocalPag';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspecieDoc';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := 'NS';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ModalCobr';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OperCodi';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aceite';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carteira';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspecieVal';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Instrucao1';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Instrucao2';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Instrucao3';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Instrucao4';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Instrucao5';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Instrucao6';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Instrucao7';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Instrucao8';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Correio';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CartEmiss';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CartConcil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercJuros';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercMulta';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtParecer';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ModelBloq';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cedente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SacadAvali';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MBB'; // Mostra balancete no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MRB'; // Mostra resumo do balancete no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PBB'; // Per�odo do balancete no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MSB'; // Mostra saldos de contas correntes no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MSP'; // Mostra saldos do plano de contas no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PSB'; // Per�odo de saldos de contas no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MIB'; // Mostra inadimpencia bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';  // 0=N�o 1=Ambos 2=Cond�mino 3=Condom�nio
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPB'; // Mostra provis�es no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MAB'; // Mostra composicao de arrecadacao no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                       // Valor da tarifa de cobran�a banc�ria para bancos
      FRCampos.Field      := 'VTCBBNITAR'; // que n�o informam a tarifa no arquivo retorno (banco 756).
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PwdSite'; // Mostra no bloqueto senha de acesso ao site (quando houver)
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Prolabore sobre (parte do sal�rio recebedor)
      New(FRCampos);
      FRCampos.Field      := 'ProLaINSSr';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '11.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Prolabore sobre (parte do pagador)
      New(FRCampos);
      FRCampos.Field      := 'ProLaINSSp';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '20.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // 
      New(FRCampos);
      FRCampos.Field      := 'ConfigBol';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CorresBco'; // Banco correspondente ( Bradesco BB -> SICOOB)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CorresAge'; // Agencia correspondente ( Bradesco BB -> SICOOB)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CorresCto';   // Agencia correspondente ( Bradesco BB -> SICOOB)
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NPrinBc';    // Preencher com 8650 (Indica que foi impresso pelo usu�rio) ( Bradesco BB -> SICOOB)
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // ocultar valores de bloqueto no site quando tem cobradora que cuida
      // da cobran�a pelo condom�nio.
      New(FRCampos);
      FRCampos.Field      := 'OcultaBloq';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SomaFracao';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BalAgrMens';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Compe';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HideCompe';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //  Fluxo de Bloquetos
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Folha';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_LeiAr';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Fecha';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Risco';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Print';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Proto';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Relat';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_EMail';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Porta';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Postl';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //  Fluxo de Encerramento de Per�odo
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Conci';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Docum';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Anali';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Contb';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Encad';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Entrg';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Web';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Assistente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (* N�o usa mais 23/07/2014
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      *)
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoCobranca';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CartTxt';
      FRCampos.Tipo       := 'varchar(10)';  // Sigla da carteira: CSB, CNR, SR, 06, 09, 18, 20, 102, 109, 174, 175, etc
      FRCampos.Null       := 'YES';          // Pode ser o mesmo ou o complemento da Carteira (CartNum do CNAB_Cfg)
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtaCooper'; // Conta cooperado ( Bradesco -> UNIPRIME)
      FRCampos.Tipo       := 'varchar(7)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CfgInfl'; // Configura��o de reajuste por �ndice inflacionario
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsaCNAB_Cfg';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomeSindico';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Multa';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Juros';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //Campos WEB - Ini
      New(FRCampos);
      FRCampos.Field      := 'NomeWeb';
      FRCampos.Tipo       := 'varchar(150)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parc_ValMin';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parc_QtdMax';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '6';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perfil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerfilImv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //Campos WEB - Fim
    end else if Uppercase(Tabela) = Uppercase('CondGri') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Username';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Password';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perfil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondGriImv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Propr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondBloco') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '99';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SomaFracao';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrefixoUH';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDExporta';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondEmeios') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Item';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pronome';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EMeio';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondImov') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Propriet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conjuge';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Andar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (*New(FRCampos);
      FRCampos.Field      := 'NApart';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);*)
      //
      New(FRCampos);
      FRCampos.Field      := 'Unidade';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdGaragem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Imobiliaria';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Contato';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ContTel';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ENome1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSIni1';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSSai1';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDIni1';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDSai1';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESegunda1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ETerca1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuarta1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuinta1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESexta1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESabado1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EDomingo1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EVeic1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EFilho1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ENome2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSIni2';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSSai2';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDIni2';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDSai2';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESegunda2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ETerca2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuarta2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuinta2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESexta2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESabado2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EDomingo2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EVeic2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EFilho2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ENome3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSIni3';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSSai3';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDIni3';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDSai3';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESegunda3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ETerca3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuarta3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuinta3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESexta3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESabado3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EDomingo3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EVeic3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EFilho3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmNome1';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmTel1';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmNome2';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmTel2';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'Longtext';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SitImv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WebLogin';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WebPwd';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WebNivel';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WebLogID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WebLastLog';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Protocolo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Protocolo2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Protocolo3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Moradores';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FracaoIdeal';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ModelBloq';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConfigBol';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BloqEndTip'; //0: Autom (1/2) 1: Morador 2: Propriet�rio 3: Terceiro 4: Descritivo
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BloqEndEnt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EnderLin1';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EnderLin2';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EnderNome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Procurador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ddVctEsp';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDExporta';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      {
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      }
      New(FRCampos);
      FRCampos.Field      := 'Juridico';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InadCEMCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SMSCelTipo'; //0: Autom (1/2) 1: Morador 2: Propriet�rio 3: Terceiro 4: Descritivo
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SMSCelEnti'; // Terceiro
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SMSCelNumr';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SMSCelNome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondModBol') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ModelBloq';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConfigBol';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BalAgrMens';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Compe';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ConfigBol') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Colunas';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // B A L A N C E T E
      // T�tulo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'TitBTxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'TitBTxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da Margem superior
      New(FRCampos);
      FRCampos.Field      := 'TitBMrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'TitBLinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '350';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Texto do T�tulo
      New(FRCampos);
      FRCampos.Field      := 'TitBTxtTit';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'BALANCETE DEMONSTRATIVO -';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Formata��o do per�odo
      New(FRCampos);
      FRCampos.Field      := 'TitBPerFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Imprime logo ?
      New(FRCampos);
      FRCampos.Field      := 'MeuLogoImp';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MeuLogoAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1800';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MeuLogoLar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '4500';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MeuLogoArq';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // C A I X A S   D E   S A L D O S
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'CxaSTxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da Fonta
      New(FRCampos);
      FRCampos.Field      := 'CxaSValSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Formata��o
      New(FRCampos);
      FRCampos.Field      := 'CxaSTxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Formata��o
      New(FRCampos);
      FRCampos.Field      := 'CxaSValFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Altura linha t�tulo
      New(FRCampos);
      FRCampos.Field      := 'CxaSTxtAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Altura linha Valores
      New(FRCampos);
      FRCampos.Field      := 'CxaSValAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // GAP Margem superior
      New(FRCampos);
      FRCampos.Field      := 'CxaSMrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '150';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSIniTam';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '20.34';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSCreTam';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '19.49';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSDebTam';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '19.49';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSMovTam';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '20.34';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSFimTam';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '20.34';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSIniTxt';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Sdo Anterior';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSCreTxt';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Receitas';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSDebTxt';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Despesas';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSMovTxt';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Saldo M�s';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaTrnsTxt';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Transf.';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSFimTxt';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Saldo Final';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSMrgInf';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // N I V E L  1  - CONTAS
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Niv1TxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Niv1ValSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // MostraGrades
      New(FRCampos);
      FRCampos.Field      := 'Niv1Grades';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Niv1TxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Niv1ValFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura do memo (edit) do valor (o tamanho do texto ser� o resto da coluna)
      New(FRCampos);
      FRCampos.Field      := 'Niv1ValLar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'Niv1LinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '270';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //


      // N I V E L  2  - SUBGRUPOS
      // Titulo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Tit2TxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Tit2TxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da Margem superior
      New(FRCampos);
      FRCampos.Field      := 'Tit2MrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '50';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'Tit2LinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Subtotal
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Som2TxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Som2ValSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Som2TxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Som2ValFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura do memo (edit) do valor (o tamanho do texto ser� o resto da coluna)
      New(FRCampos);
      FRCampos.Field      := 'Som2ValLar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'Som2LinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // N I V E L  3  - GRUPOS
      // Titulo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Tit3TxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '9';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Tit3TxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da Margem superior
      New(FRCampos);
      FRCampos.Field      := 'Tit3MrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'Tit3LinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '600';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Subtotal
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Som3TxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Som3ValSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Som3TxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Som3ValFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura do memo (edit) do valor (o tamanho do texto ser� o resto da coluna)
      New(FRCampos);
      FRCampos.Field      := 'Som3ValLar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'Som3LinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '450';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // N I V E L  4  - CONJUNTOS
      // Titulo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Tit4TxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '10';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Tit4TxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da Margem superior
      New(FRCampos);
      FRCampos.Field      := 'Tit4MrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'Tit4LinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '450';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      {
      // Subtotal
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Som4TxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '9';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Som4ValSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '9';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Som4TxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Som4ValFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura do memo (edit) do valor (o tamanho do texto ser� o resto da coluna)
      New(FRCampos);
      FRCampos.Field      := 'Som4ValLar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      }
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'Som4LinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // N I V E L  8  - SALDOS TOTAIS DE CONJUTOS
      // Titulo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Tit8TxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '9';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Tit8TxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da Margem superior
      New(FRCampos);
      FRCampos.Field      := 'Tit8MrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'Tit8LinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '600';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);

      // Dados
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Niv8TxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Niv8ValSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // MostraGrades
      New(FRCampos);
      FRCampos.Field      := 'Niv8Grades';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Niv8TxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Niv8ValFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura do memo (edit) do valor (o tamanho do texto ser� o resto da coluna)
      New(FRCampos);
      FRCampos.Field      := 'Niv8ValLar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'Niv8LinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '270';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // Subtotal
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Som8TxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'Som8ValSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Som8TxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'Som8ValFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura do memo (edit) do valor (o tamanho do texto ser� o resto da coluna)
      New(FRCampos);
      FRCampos.Field      := 'Som8ValLar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'Som8LinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '450';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // R E S U M O   F I N A N C E I R O
      // Titulo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'TitRTxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'TitRTxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da Margem superior
      New(FRCampos);
      FRCampos.Field      := 'TitRMrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'TitRLinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '350';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Texto do T�tulo
      New(FRCampos);
      FRCampos.Field      := 'TitRTxtTit';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'RESUMO FINANCEIRO -';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Formata��o do per�odo
      New(FRCampos);
      FRCampos.Field      := 'TitRPerFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // S A L D O S   D E   C O N T A S   C O R R E N T E S
      // Titulo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'TitCTxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'TitCTxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da Margem superior
      New(FRCampos);
      FRCampos.Field      := 'TitCMrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'TitCLinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Texto do T�tulo
      New(FRCampos);
      FRCampos.Field      := 'TitCTxtTit';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'CONTAS CORRENTES -';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Formata��o do per�odo
      New(FRCampos);
      FRCampos.Field      := 'TitCPerFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Saldos
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'SdoCTxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'SdoCTxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'SdoCLinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // I N A D I M P L � N C I A   M I N H A   E   C O N D O M � N I O
      // Titulo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'TitITxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'TitITxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da Margem superior
      New(FRCampos);
      FRCampos.Field      := 'TitIMrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'TitILinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Texto do T�tulo
      New(FRCampos);
      FRCampos.Field      := 'TitITxtTit';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'INADIMPL�NCIA';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Saldos
      // Minha unidade
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'SdoITxMSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'SdoITxMFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'SdoIVaMSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'SdoIVaMFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'SdoILiMAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Texto
      New(FRCampos);
      FRCampos.Field      := 'SdoITxMTxt';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Inadimpl�ncia minha unidade at�';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura Valor
      New(FRCampos);
      FRCampos.Field      := 'SdoILaMVal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Todas unidades
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'SdoITxTSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'SdoITxTFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'SdoIVaTSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'SdoIVaTFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'SdoILiTAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Texto
      New(FRCampos);
      FRCampos.Field      := 'SdoITxTTxt';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Inadimpl�ncia todas unidades at�';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura Valor
      New(FRCampos);
      FRCampos.Field      := 'SdoILaTVal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

      // P R O V I S O E S
      // Conta
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'CtaPTxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'CtaPTxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'CtaPValSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'CtaPValFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'CtaPLinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '270';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura Valor
      New(FRCampos);
      FRCampos.Field      := 'CtaPLarVal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Sub-grupo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'SgrPTitSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'SgrPTitFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'SgrPTitAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'SgrPSumSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'SgrPSumFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'SgrPSumAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura Valor
      New(FRCampos);
      FRCampos.Field      := 'SgrPLarVal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // T�tulo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'PrvPTitSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'PrvPTitFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'PrvPTitAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '350';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Texto total
      New(FRCampos);
      FRCampos.Field      := 'PrvPTitStr';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'PROVIS�O PARA AS DESPESAS DE';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Margem superior
      New(FRCampos);
      FRCampos.Field      := 'PrvPMrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Total
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'SumPTxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'SumPTxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'SumPValSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'SumPValFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'SumPLinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura Valor
      New(FRCampos);
      FRCampos.Field      := 'SumPValLar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Texto total
      New(FRCampos);
      FRCampos.Field      := 'SumPTxtStr';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'TOTAL PROVIS�ES';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // A R R E C A D A � � E S
      // Conta
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'CtaATxtSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'CtaATxtFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'CtaAValSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '7';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'CtaAValFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'CtaALinAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '270';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Largura Valor
      New(FRCampos);
      FRCampos.Field      := 'CtaALarVal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1200';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // T�tulo
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'TitATitSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'TitATitFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'TitATitAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '350';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Texto
      New(FRCampos);
      FRCampos.Field      := 'TitATitStr';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'COMPOSI��O DA ARRECADA��O -';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Margem superior
      New(FRCampos);
      FRCampos.Field      := 'TitAMrgSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '300';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Apto
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'TitAAptSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'TitAAptFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'TitAAptAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '350';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Margem superior  (entre apto e t�tulo)
      New(FRCampos);
      FRCampos.Field      := 'TitAAptSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '50';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Total
      // Tamanho da fonte
      New(FRCampos);
      FRCampos.Field      := 'TitASumSiz';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '8';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Negrito = 1,  Italico = 2,  Ambos = 3
      New(FRCampos);
      FRCampos.Field      := 'TitASumFmt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  Altura da linha
      New(FRCampos);
      FRCampos.Field      := 'TitASumAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '500';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TitAAptTex';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // W E B
      //
      New(FRCampos);
      FRCampos.Field      := 'TitWebTxt';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Meu condom�nio na internet';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Geral
      //
      New(FRCampos);
      FRCampos.Field      := 'MesCompet';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Cons') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Genero';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Export_Tip'; //0=Outros, 1=G�s, 2=�gua
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    (*end else if Uppercase(Tabela) = Uppercase('ConsPer') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos); *)
      //
    end else if Uppercase(Tabela) = Uppercase(TAB_CNS) then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Propriet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MedAnt';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MedAtu';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carencia';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifCaren';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Consumo';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Casas';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '6';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Preco';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidLei';
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidImp';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidFat';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Boleto';
      FRCampos.Tipo       := 'double(20,0)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vencto';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lancto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GeraTyp';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GeraFat';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CasRat';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoImpLei';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ConsPrc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Casas';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '6';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Preco';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidLei';
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidImp';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidFat';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ContaBase';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerioBase';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorBase';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CasRat';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arredonda';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.01';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoImpLei';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carencia';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifCaren';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Export_Apl'; //0=Nenhum, 1=Produsys
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Export_Med'; //Depende do Export_Apl
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ImpCH') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := 'NIL';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Agora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cidade';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Banco';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Forma';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('FlxBloq') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ddVence';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Abertura
      //
      New(FRCampos);
      FRCampos.Field      := 'Abert_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Abert_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Folha pagamento
      //
      New(FRCampos);
      FRCampos.Field      := 'Folha_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Folha_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Folha_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Leitura / arrecada��o
      //
      New(FRCampos);
      FRCampos.Field      := 'LeiAr_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LeiAr_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LeiAr_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Fechamento
      //
      New(FRCampos);
      FRCampos.Field      := 'Fecha_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fecha_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fecha_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Administradora de riscos (Pr�-Condomino)
      //
      New(FRCampos);
      FRCampos.Field      := 'Risco_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Risco_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Risco_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Impress�o
      //
      New(FRCampos);
      FRCampos.Field      := 'Print_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Print_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Print_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Protocolo
      //
      New(FRCampos);
      FRCampos.Field      := 'Proto_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Proto_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Proto_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Relat�rio
      //
      New(FRCampos);
      FRCampos.Field      := 'Relat_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Relat_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Relat_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // E-Mail
      //
      New(FRCampos);
      FRCampos.Field      := 'EMail_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EMail_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EMail_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Portaria
      //
      New(FRCampos);
      FRCampos.Field      := 'Porta_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Porta_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Porta_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Postal
      //
      New(FRCampos);
      FRCampos.Field      := 'Postl_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Postl_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Postl_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('FlxMens') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Protocolo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Abertura
      //
      New(FRCampos);
      FRCampos.Field      := 'Abert_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Abert_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Concilia��o
      //
      New(FRCampos);
      FRCampos.Field      := 'Conci_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conci_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conci_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Documentos
      //
      New(FRCampos);
      FRCampos.Field      := 'Docum_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Docum_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Docum_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Contabilidade (Pagto ISS)
      //
      New(FRCampos);
      FRCampos.Field      := 'Contb_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Contb_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Contb_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // An�lise
      //
      New(FRCampos);
      FRCampos.Field      := 'Anali_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Anali_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Anali_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Encaderna��o
      //
      New(FRCampos);
      FRCampos.Field      := 'Encad_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Encad_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Encad_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Entrega
      //
      New(FRCampos);
      FRCampos.Field      := 'Entrg_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entrg_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entrg_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      // Web
      //
      New(FRCampos);
      FRCampos.Field      := 'Web01_DLim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web01_User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web01_Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ImpCHMsg') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := 'NIL';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Agora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Impressora';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodErr';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipMsg';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodMsg';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Benefic';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('dirweb') then   //Diret�rios FTP
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pasta';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fotos';  //Diret�rio de fotos
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('wfotos') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('wfotosedit') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Legenda';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('wslidefts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'URLFoto';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Legenda';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    {
    end else if Uppercase(Tabela) = Uppercase('Protocolos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Def_Client';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Def_Sender';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Def_Retorn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PreEmeio';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '???';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ProtocoOco') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ProtocoMot') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ProtocoPak') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';    // Protocolo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataL';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mez';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SeqArq'; // Seq��ncia do arquivo de remessa CNAB
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ProtPakIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Link_ID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cedente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lancto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Docum';
      FRCampos.Tipo       := 'double(20,0)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Depto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataE';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataD';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Retorna';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cancelado';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod4';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vencto'; // Vencimento do do documento
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor'; // Valor do documento
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MoraDiaVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MultaVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComoConf'; // 1=Manual(For�ado), 2=Emeio, 3=J� pagou
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieCH';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Manual';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Saiu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Recebeu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Retornou';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteSai';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteRem';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteRet';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataSai';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataRec';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataRet';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    }
    end else if Uppercase(Tabela) = Uppercase('Status') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';//'UNI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Animais') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondAnimais') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubConta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Animal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Veic') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubConta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cor';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Placa';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoM';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Filhos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubConta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataNasc';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Financas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Credito';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Debito';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataE';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataL';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataV';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('OutMor') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubConta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataNasc';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parentesco';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Prev') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Gastos';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso01';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso02';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso03';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso04';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso05';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso06';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso07';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso08';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso09';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso10';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso11';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso12';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso13';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso14';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso15';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso16';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso17';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso18';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso19';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aviso20';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AvisoVerso';
      FRCampos.Tipo       := 'varchar(180)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ModelBloq';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConfigBol';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BalAgrMens';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Compe';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Encerrado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase(TAB_PRI) then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';  // Prev
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle'; // Index
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';  // Plano de contas (Genero)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevBaC';   // PrevBaC->Codigo => C�digo da provis�o base
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevBaI'; // PrevBAI->Controle => Item da provis�o base
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmitVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmitSit';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PrevBaC') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PrevBaI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SitCobr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parcelas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ParcPerI';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ParcPerF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfoParc';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PrevBaILct') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lancto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PrevModBol') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ModelBloq';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConfigBol';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BalAgrMens';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Compe';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PrevRat') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Morardor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('uploads') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arquivo';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tags';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DirWeb';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataExp'; //Data de expira��o do arquivo
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('users') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'User_ID'; // Cadastro do usu�rio na Web
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodCliEsp'; // Condominio > QrCondCodigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodigoEsp';  // Unidade -> QrCondImovConta
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Username';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Password';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LastAcess';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
(*
      case 1: $NomeNivel = 'Morador';
      case 2: $NomeNivel = 'Teste';
      case 3: $NomeNivel = 'cliente ';
      case 9: $NomeNivel = 'admin ';
*)
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('visitas') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'IP';
      FRCampos.Tipo       := 'varchar(64)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0.0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Visitas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Primeira';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ultima';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('visitasits') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IP';
      FRCampos.Tipo       := 'varchar(64)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0.0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('visitascli') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IP';
      FRCampos.Tipo       := 'varchar(64)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0.0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('wclients') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'User_ID'; // Cadastro do condominio na Web
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodCliEsp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomeWeb';
      FRCampos.Tipo       := 'varchar(150)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parc_ValMin';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parc_QtdMax';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '6';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Multa';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Juros';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Username';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Password';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomeSindico';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('wcond') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MsgNoImpBlq'; //Texto para os condom�nios que n�o
      FRCampos.Tipo       := 'int(11)';     //ir�o imprimir bloquetos
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    end else
    begin
      PerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Empresas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Enti_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Finance_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      ALL_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Anotacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Moedas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Textos_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Cheques_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      IBGE_DTB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //CashTb.CarregaListaFRCamposCashier(Tabela, FRCampos, FLCampos, TemControle);
      CNAB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Diario_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      WTextos_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Protocol_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnFPMin_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnFPMax_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnNFSe_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnNFSe_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnGeral_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnSMS_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnInflIdx_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      WUsers_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle, CO_DMKID_APP);
      WGerl_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle, CO_DMKID_APP);
      FTP_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Mail_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    //
    PerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Empresas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enti_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Finance_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    ALL_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Anotacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Moedas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Cheques_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //CashTb.CompletaListaFRCamposCashier(Tabela, FRCampos, FLCampos);
    CNAB_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Diario_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WTextos_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Protocol_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnFPMin_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnFPMax_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnNFSe_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnNFSe_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnGeral_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnSMS_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnInflIdx_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WUsers_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WGerl_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    FTP_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Mail_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.ExcluiTab: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiReg: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiIdx: Boolean;
begin
  Result := True;
end;

function TMyListas.CriaListaUserSets(FUserSets: TStringList): Boolean;
begin
  Result := True;
  try
    FUserSets.Add('Edi��o de Itens de Mercadoria;C�digos Fiscais');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Representante');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Vendedor');
    FUserSets.Add('Edi��o de Itens de Mercadoria;IPI');
    FUserSets.Add('Edi��o de Itens de Mercadoria;ICMS');
    //
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaImpDOS(FImpDOS: TStringList): Boolean;
begin
  Result := True;
  try
    FImpDOS.Add('1001;01;NF (Topo)');
    FImpDOS.Add('1002;01;Sa�da [X]');
    FImpDOS.Add('1003;01;Entrada [X]');
    FImpDOS.Add('1004;01;Data emiss�o');
    FImpDOS.Add('1005;01;Data entra/sai');
    FImpDOS.Add('1006;01;C�digo CFOP');
    FImpDOS.Add('1007;01;Descri��o CFOP');
    FImpDOS.Add('1008;01;Base c�lculo ICMS');
    FImpDOS.Add('1009;01;Valor ICMS');
    FImpDOS.Add('1010;01;Base c�lc. ICMS subst.');
    FImpDOS.Add('1011;01;Valor ICMS subst.');
    FImpDOS.Add('1012;01;Valor frete');
    FImpDOS.Add('1013;01;Valor seguro');
    FImpDOS.Add('1014;01;Outras desp. aces.');
    FImpDOS.Add('1015;01;Valor total IPI');
    FImpDOS.Add('1016;01;Valor total produtos');
    FImpDOS.Add('1017;01;Valor total servicos');
    FImpDOS.Add('1018;01;Valor total nota');
    FImpDOS.Add('1019;01;Placa ve�culo');
    FImpDOS.Add('1020;01;UF placa ve�culo');
    FImpDOS.Add('1021;01;Vol. Quantidade');
    FImpDOS.Add('1022;01;Vol. Esp�cie');
    FImpDOS.Add('1023;01;Vol. Marca');
    FImpDOS.Add('1024;01;Vol. N�mero');
    FImpDOS.Add('1025;01;Vol. kg bruto');
    FImpDOS.Add('1026;01;Vol. kg l�quido');
    FImpDOS.Add('1027;01;Dados adicionais');
    FImpDOS.Add('1028;01;Frete por conta de ...');
    FImpDOS.Add('1029;01;Desconto especial');
    FImpDOS.Add('1030;01;NF (rodap�)');
    //
    FImpDOS.Add('2001;02;Nome ou Raz�o Social');
    FImpDOS.Add('2002;02;CNPJ ou CPF');
    FImpDOS.Add('2003;02;Endere�o');
    FImpDOS.Add('2004;02;Bairro');
    FImpDOS.Add('2005;02;CEP');
    FImpDOS.Add('2006;02;Cidade');
    FImpDOS.Add('2007;02;Telefone');
    FImpDOS.Add('2008;02;UF');
    FImpDOS.Add('2009;02;I.E. ou RG');
    FImpDOS.Add('2010;02;I.E.S.T.');
    //
    FImpDOS.Add('6001;06;Nome ou Raz�o Social');
    FImpDOS.Add('6002;06;CNPJ ou CPF');
    FImpDOS.Add('6003;06;Endere�o');
    FImpDOS.Add('6004;06;Bairro');
    FImpDOS.Add('6005;06;CEP');
    FImpDOS.Add('6006;06;Cidade');
    FImpDOS.Add('6007;06;Telefone');
    FImpDOS.Add('6008;06;UF');
    FImpDOS.Add('6009;06;I.E. ou RG');
    FImpDOS.Add('6010;06;I.E.S.T.');
    //
    FImpDOS.Add('3001;03;Descri��o');
    FImpDOS.Add('3002;03;Classifica��o Fiscal');
    FImpDOS.Add('3003;03;Situa��o Tribut�ria');
    FImpDOS.Add('3004;03;Unidade');
    FImpDOS.Add('3005;03;Quantidade');
    FImpDOS.Add('3006;03;Valor unit�rio');
    FImpDOS.Add('3007;03;Valor total');
    FImpDOS.Add('3008;03;Aliquota ICMS');
    FImpDOS.Add('3009;03;Aliquota IPI');
    FImpDOS.Add('3010;03;Valor IPI');
    FImpDOS.Add('3011;03;CFOP');
    FImpDOS.Add('3012;03;Refer�ncia');
    //
    FImpDOS.Add('4001;04;Descri��o');
    FImpDOS.Add('4002;04;Classifica��o Fiscal');
    FImpDOS.Add('4003;04;Situa��o Tribut�ria');
    FImpDOS.Add('4004;04;Unidade');
    FImpDOS.Add('4005;04;Quantidade');
    FImpDOS.Add('4006;04;Valor unit�rio');
    FImpDOS.Add('4007;04;Valor total');
    FImpDOS.Add('4008;04;Aliquota ICMS');
    FImpDOS.Add('4009;04;Aliquota IPI');
    FImpDOS.Add('4010;04;Valor IPI');
    FImpDOS.Add('4011;04;CFOP');
    FImpDOS.Add('4012;04;Refer�ncia');
    //
    FImpDOS.Add('5001;05;Parcela');
    FImpDOS.Add('5002;05;Valor');
    FImpDOS.Add('5003;05;Vencimento');
    //
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  //
end;

procedure TMyListas.VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

procedure TMyListas.ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
  DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  // CB4-GDATA-001 :: Gerenciamento de Tabelas CB4
  New(FRJanelas);
  FRJanelas.ID        := 'CB4-GDATA-001';
  FRJanelas.Nome      := 'FmCond';
  FRJanelas.Descricao := 'Cadastro de Condom�nios';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-001 :: Cadastro de Condom�nios
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-001';
  FRJanelas.Nome      := 'FmCond';
  FRJanelas.Descricao := 'Cadastro de Condom�nios';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-003 :: Blocos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-003';
  FRJanelas.Nome      := 'FmCondBloco';
  FRJanelas.Descricao := 'Blocos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-004 :: Im�veis
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-004';
  FRJanelas.Nome      := 'FmCondImov';
  FRJanelas.Descricao := 'Im�veis';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-005 :: Fluxos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-005';
  FRJanelas.Nome      := 'FmCondFlx';
  FRJanelas.Descricao := 'Fluxos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-006 :: Cadastro R�pido
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-006';
  FRJanelas.Nome      := 'FmEntiRapidoCond';
  FRJanelas.Descricao := 'Cadastro R�pido';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-007 :: Protocolos de UHs
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-007';
  FRJanelas.Nome      := 'FmCondProto';
  FRJanelas.Descricao := 'Protocolos de UHs';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-008 :: Localiza e-mails - Cond�mino
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-008';
  FRJanelas.Nome      := 'FmCondLocEmail';
  FRJanelas.Descricao := 'Localiza e-mails - Cond�mino';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-009 :: Sele��o de UHs - Modelo e Config. Bloq.
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-009';
  FRJanelas.Nome      := 'FmAptosModBol';
  FRJanelas.Descricao := 'Sele��o de UHs - Modelo e Config. Bloq.';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-010 :: Cadastro de E-mail de UH
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-010';
  FRJanelas.Nome      := 'FmCondEmeios';
  FRJanelas.Descricao := 'Cadastro de E-mail de UH';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-011 :: Adi��o de E-mails de UHs
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-011';
  FRJanelas.Nome      := 'FmCondEmeiosAdd';
  FRJanelas.Descricao := 'Adi��o de E-mails de UHs';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-012 :: Impress�o de Im�veis
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-012';
  FRJanelas.Nome      := 'FmCondImovImp';
  FRJanelas.Descricao := 'Impress�o de Im�veis';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-013 :: Gerenciamento de U.H. / Cond�mino
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-013';
  FRJanelas.Nome      := 'FmPesqCond';
  FRJanelas.Descricao := 'Gerenciamento de U.H. / Cond�mino';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-014 :: Bloquetos em Aberto de Grupos de UHs
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-014';
  FRJanelas.Nome      := 'FmCondGriOpen';
  FRJanelas.Descricao := 'Bloquetos em Aberto de Grupos de UHs';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-ARREC-001 :: Cadastro de Arrecada��es Bases
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ARREC-001';
  FRJanelas.Nome      := 'FmArreBaC';
  FRJanelas.Descricao := 'Cadastro de Arrecada��es Bases';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-ARREC-002 :: Arrecada��o - Inclui Unidade
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ARREC-002';
  FRJanelas.Nome      := 'FmArreBaCAddUni';
  FRJanelas.Descricao := 'Arrecada��o - Inclui Unidade';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-ARREC-003 :: Arrecada��o - Sele��o de Unidades
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ARREC-003';
  FRJanelas.Nome      := 'FmArreBaCAptos';
  FRJanelas.Descricao := 'Arrecada��o - Sele��o de Unidades';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-ARREC-004 ::
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ARREC-004';
  FRJanelas.Nome      := 'FmArreBaC';
  FRJanelas.Descricao := '';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-ARREC-005 ::
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ARREC-005';
  FRJanelas.Nome      := 'FmArreBaC';
  FRJanelas.Descricao := '';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-PROVI-001 :: Cadastro de Provis�es Bases
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-PROVI-001';
  FRJanelas.Nome      := 'FmPrevBaC';
  FRJanelas.Descricao := 'Cadastro de Provis�es Bases';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-PROVI-002 :: Agendamento de Provis�o
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-PROVI-002';
  FRJanelas.Nome      := 'FmPrevBaCLctos';
  FRJanelas.Descricao := 'Agendamento de Provis�o';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-PROVI-003 :: Agendamento de Provis�o
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-PROVI-003';
  FRJanelas.Nome      := 'FmCondGerAgefet';
  FRJanelas.Descricao := 'Agendamento de Provis�o';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-PROVI-004 :: Item de Provis�o Base
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-PROVI-004';
  FRJanelas.Nome      := 'FmPrevBaI';
  FRJanelas.Descricao := 'Item de Provis�o Base';
  FLJanelas.Add(FRJanelas);
  //
  // CFG-BOLET-001 :: Cfg. impress�o de boleto
  New(FRJanelas);
  FRJanelas.ID        := 'CFG-BOLET-001';
  FRJanelas.Nome      := 'FmConfigBol';
  FRJanelas.Descricao := 'Cfg. impress�o de boleto';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-STATU-001 :: Status
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-STATU-001';
  FRJanelas.Nome      := 'FmStatus';
  FRJanelas.Descricao := 'Status';
  FLJanelas.Add(FRJanelas);
  //
  // CIA-GEREN-001 :: Cobran�a de Inadimplentes Automatizada
  New(FRJanelas);
  FRJanelas.ID        := 'CIA-GEREN-001';
  FRJanelas.Nome      := 'FmCobrarGer';
  FRJanelas.Descricao := 'Cobran�a de Inadimplentes Automatizada';
  FLJanelas.Add(FRJanelas);
  //
  // CIA-GEREN-003 :: Confirma��o de Impress�o de Cartas
  New(FRJanelas);
  FRJanelas.ID        := 'CIA-GEREN-003';
  FRJanelas.Nome      := 'FmCartasEnv';
  FRJanelas.Descricao := 'Confirma��o de Impress�o de Cartas';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-OPCOE-002 :: Op��es Espec�ficas do Meu Site
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-OPCOE-002';
  FRJanelas.Nome      := 'FmOpcoesSyndic';
  FRJanelas.Descricao := 'Op��es Espec�ficas do Meu Site';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-VISIT-001 :: Controle de usu�rios web
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-VISIT-001';
  FRJanelas.Nome      := 'FmVisitasCli';
  FRJanelas.Descricao := 'Controle de usu�rios web';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-DIRET-001 :: Diret�rios WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-DIRET-001';
  FRJanelas.Nome      := 'FmDirWeb';
  FRJanelas.Descricao := 'Diret�rios WEB';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-UPLOA-001 :: Uploads de arquivos
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-UPLOA-001';
  FRJanelas.Nome      := 'FmUploads';
  FRJanelas.Descricao := 'Uploads de arquivos';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-UPLOA-002 :: Uploads de arquivos - Itens
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-UPLOA-002';
  FRJanelas.Nome      := 'FmUploadsIts';
  FRJanelas.Descricao := 'Uploads de arquivos - Itens';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // FER-OPCAO-002 :: Op��es Espec�ficas do Aplicativo
  New(FRJanelas);
  FRJanelas.ID        := 'FER-OPCAO-002';
  FRJanelas.Nome      := 'FmOpcoesSyndic';
  FRJanelas.Descricao := 'Op��es Espec�ficas do Aplicativo';
  FLJanelas.Add(FRJanelas);
  //
  {
  // CND-OCORR-001 :: Impress�o do Livro de Ocorr�ncias
  New(FRJanelas);
  FRJanelas.ID        := 'CND-OCORR-001';
  FRJanelas.Nome      := 'FmOcorrImp';
  FRJanelas.Descricao := 'Impress�o do Livro de Ocorr�ncias';
  FLJanelas.Add(FRJanelas);
  }
  //
  // CND-CADAS-002 :: Ficha Cadastral de Moradores
  New(FRJanelas);
  FRJanelas.ID        := 'CND-CADAS-002';
  FRJanelas.Nome      := 'FmCondFichaCad';
  FRJanelas.Descricao := 'Ficha Cadastral de Moradores';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-ENCER-001 :: Sele��o de Fluxo
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-ENCER-001';
  FRJanelas.Nome      := 'FmFlxMensSel';
  FRJanelas.Descricao := 'Sele��o de Fluxo';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-ENCER-002 :: Confirma��o de Passo
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-ENCER-002';
  FRJanelas.Nome      := 'FmFlxMensAti';
  FRJanelas.Descricao := 'Confirma��o de Passo';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BALAN-001 :: Abertura de Fluxo de Balancete
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BALAN-001';
  FRJanelas.Nome      := 'FmFlxMensBalAdd';
  FRJanelas.Descricao := 'Abertura de Fluxo de Balancete';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BALAN-002 :: Confirma��o de Etapa Mensal
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BALAN-002';
  FRJanelas.Nome      := 'FmFlxMensBalUpd';
  FRJanelas.Descricao := 'Confirma��o de Etapa Mensal';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BALAN-003 :: Panorama do Fluxo dos Balancetes
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BALAN-003';
  FRJanelas.Nome      := 'FmFlxMensBalViw';
  FRJanelas.Descricao := 'Panorama do Fluxo dos Balancetes';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BALAN-004 :: Novo Per�odo de Fluxo de Balancetes
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BALAN-004';
  FRJanelas.Nome      := 'FmFlxMensBalNew';
  FRJanelas.Descricao := 'Novo Per�odo de Fluxo de Balancetes';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BALAN-005 :: Novo Per�odo de Fluxo de Bloquetos
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BALAN-005';
  FRJanelas.Nome      := 'FmFlxMensBlqNew';
  FRJanelas.Descricao := 'Novo Per�odo de Fluxo de Bloquetos';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BLOQU-001 :: Abertura de Fluxo de Bloqueto
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BLOQU-001';
  FRJanelas.Nome      := 'FmFlxMensBlqAdd';
  FRJanelas.Descricao := 'Abertura de Fluxo de Bloqueto';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BLOQU-002 :: Confirma��o de Etapa Mensal
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BLOQU-002';
  FRJanelas.Nome      := 'FmFlxMensBlqUpd';
  FRJanelas.Descricao := 'Confirma��o de Etapa Mensal';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BLOQU-003 :: Panorama do Fluxo dos Bloquetos
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BLOQU-003';
  FRJanelas.Nome      := 'FmFlxMensBlqViw';
  FRJanelas.Descricao := 'Panorama do Fluxo dos Bloquetos';
  FLJanelas.Add(FRJanelas);
  //
  //
  {
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-001';
  FRJanelas.Nome      := 'FmProtocolos';
  FRJanelas.Descricao := 'Cadastro e Gerenciamento de Protocolos';
  FLJanelas.Add(FRJanelas);
  //
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-002';
  FRJanelas.Nome      := 'FmProtocoOco';
  FRJanelas.Descricao := 'Ocorr�ncias de Protocolos';
  FLJanelas.Add(FRJanelas);
  //
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-003';
  FRJanelas.Nome      := 'FmProtocoMot';
  FRJanelas.Descricao := 'Motivos de N�o Entrega de Protocolos';
  FLJanelas.Add(FRJanelas);
  //
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-004';
  FRJanelas.Nome      := 'FmProtocoPak';
  FRJanelas.Descricao := 'Novo Lote de Protocolos';
  FLJanelas.Add(FRJanelas);
  //
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-005';
  FRJanelas.Nome      := 'FmProtocoMan';
  FRJanelas.Descricao := 'Item de Protocolos Manual';
  FLJanelas.Add(FRJanelas);
  //
  }
  // GER-CONDM-000 :: Sele��o de Condom�nio
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-000';
  FRJanelas.Nome      := 'FmCondSel';
  FRJanelas.Descricao := 'Sele��o de Condom�nio';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-001 :: Gerenciamento de Condom�nio
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-001';
  FRJanelas.Nome      := 'FmCondGer';
  FRJanelas.Descricao := 'Gerenciamento de Condom�nio';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-002 :: Exclus�o de Arrecada��es
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-002';
  FRJanelas.Nome      := 'FmCondGerDelArre';
  FRJanelas.Descricao := 'Exclus�o de Arrecada��es';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-003 :: Impress�o de Carn� de Bloquetos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-003';
  FRJanelas.Nome      := 'FmCondGerCarne';
  FRJanelas.Descricao := 'Impress�o de Carn� de Bloquetos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-004 :: Agendamento por Varredura de Lan�amentos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-004';
  FRJanelas.Nome      := 'FmPrevVeri';
  FRJanelas.Descricao := 'Agendamento por Varredura de Lan�amentos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-005 :: Sele��o de Provis�o Base
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-005';
  FRJanelas.Nome      := 'FmPrevVeriBaC';
  FRJanelas.Descricao := 'Sele��o de Provis�o Base';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-006 :: Impress�o de Arrecada��es
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-006';
  FRJanelas.Nome      := 'FmCondGerImpArre';
  FRJanelas.Descricao := 'Impress�o de Arrecada��es';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-007 :: Desfazimento de Processamento de Item de Retorno CNAB
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-007';
  FRJanelas.Nome      := 'FmCondGerStep';
  FRJanelas.Descricao := 'Desfazimento de Processamento de Item de Retorno CNAB';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-008 :: Provis�es Agendadas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-008';
  FRJanelas.Nome      := 'FmPrevBaA';
  FRJanelas.Descricao := 'Provis�es Agendadas';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-009 :: Adi��o de Contas Bases no Or�amento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-009';
  FRJanelas.Nome      := 'FmPrevBaB';
  FRJanelas.Descricao := 'Adi��o de Contas Bases no Or�amento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-010 :: Protocolos de Entrega de Bloqueto
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-010';
  FRJanelas.Nome      := 'FmCondGerProto';
  FRJanelas.Descricao := 'Protocolos de Entrega de Bloqueto';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-011 :: Item de Or�amento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-011';
  FRJanelas.Nome      := 'FmCondGerPrev';
  FRJanelas.Descricao := 'Item de Or�amento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-012 :: Novo Or�amento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-012';
  FRJanelas.Nome      := 'FmCondGerNew';
  FRJanelas.Descricao := 'Novo Or�amento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-013 :: Modelo de Bloqueto
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-013';
  FRJanelas.Nome      := 'FmCondGerModelBloq';
  FRJanelas.Descricao := 'Modelo de Bloqueto';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-014 :: Per�odo do Or�amento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-014';
  FRJanelas.Nome      := 'FmCondGerLocper';
  FRJanelas.Descricao := 'Per�odo do Or�amento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-015 :: Altera��o de Leitura
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-015';
  FRJanelas.Nome      := 'FmCondGerLeiEdit';
  FRJanelas.Descricao := 'Altera��o de Leitura';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-016 :: Rateio de Consumo
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-016';
  FRJanelas.Nome      := 'FmCondGerLei2';
  FRJanelas.Descricao := 'Rateio de Consumo';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-017 :: Leituras de Produtos Consumidos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-017';
  FRJanelas.Nome      := 'FmCondGerLei1';
  FRJanelas.Descricao := 'Leituras de Produtos Consumidos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-018 :: Impress�es do Gerenciamento de Comdom�nios
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-018';
  FRJanelas.Nome      := 'FmCondGerImpGer2';
  FRJanelas.Descricao := 'Impress�es do Gerenciamento de Comdom�nios';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-019 :: Relat�rio de Arrecada��es (Lista de Cond�minos)
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-019';
  FRJanelas.Nome      := 'FmCondGerImpGer2a';
  FRJanelas.Descricao := 'Relat�rio de Arrecada��es (Lista de Cond�minos)';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-020 :: Largura Coluna
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-020';
  FRJanelas.Nome      := 'FmCondGerImpGerLar';
  FRJanelas.Descricao := 'Largura Coluna';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-021 :: Impress�es do Gerenciamento de Comdom�nios
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-021';
  FRJanelas.Nome      := 'FmCondGerImpGer';
  FRJanelas.Descricao := 'Impress�es do Gerenciamento de Comdom�nios';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-022 :: Adi��o de Contas Bases no Or�amento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-022';
  FRJanelas.Nome      := 'FmArreBaA';
  FRJanelas.Descricao := 'Adi��o de Contas Bases no Or�amento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-023 :: Arrecada��es Duplicadas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-023';
  FRJanelas.Nome      := 'FmArreBaADupl';
  FRJanelas.Descricao := 'Arrecada��es Duplicadas';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-024 :: Item de Arrecada��o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-024';
  FRJanelas.Nome      := 'FmCondGerArreUni';
  FRJanelas.Descricao := 'Item de Arrecada��o';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-025 :: Item de Arrecada��o Futura
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-025';
  FRJanelas.Nome      := 'FmCondGerArreFut';
  FRJanelas.Descricao := 'Item de Arrecada��o Futura';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-026 :: Arrecada��es Pr�-agendadas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-026';
  FRJanelas.Nome      := 'FmCondGerArreFutA';
  FRJanelas.Descricao := 'Arrecada��es Pr�-agendadas';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-027 :: Sele��o de Unidades
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-027';
  FRJanelas.Nome      := 'FmCondGerArreMul';
  FRJanelas.Descricao := 'Sele��o de Unidades';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-028 :: Envio de Bloquetos por Emeio
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-028';
  FRJanelas.Nome      := 'FmEmailBloqueto';
  FRJanelas.Descricao := 'Envio de Bloquetos por Emeio';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-029 :: Escolha de Bloqueto
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-029';
  FRJanelas.Nome      := 'FmCondGerBolSel';
  FRJanelas.Descricao := 'Escolha de Bloqueto';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-030 :: Informa��es que Precisam de Aten��o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-030';
  FRJanelas.Nome      := 'FmAtencoes';
  FRJanelas.Descricao := 'Informa��es que Precisam de Aten��o';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-031 :: Limpeza de Pacelamentos Exclu�dos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-031';
  FRJanelas.Nome      := 'FmBloqParcDel';
  FRJanelas.Descricao := 'Limpeza de Pacelamentos Exclu�dos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-032 :: Localiza Lote de Protocolo
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-032';
  FRJanelas.Nome      := 'FmCondGerProtoLoc';
  FRJanelas.Descricao := 'Localiza Lote de Protocolo';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-033 :: Gera Arquivo CNAB
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-033';
  FRJanelas.Nome      := 'FmGeraCNAB';
  FRJanelas.Descricao := 'Gera Arquivo CNAB';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-034 :: Valor de Boleto - Gerencial X Financeiro
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-034';
  FRJanelas.Nome      := 'FmVerifiBloquetosValor';
  FRJanelas.Descricao := 'Valor de Boleto - Gerencial X Financeiro';
  FLJanelas.Add(FRJanelas);
  //
  // GER-LANCT-001 :: Lan�amentos a Vencer
  New(FRJanelas);
  FRJanelas.ID        := 'GER-LANCT-001';
  FRJanelas.Nome      := 'FmAVencer';
  FRJanelas.Descricao := 'Lan�amentos a Vencer';
  FLJanelas.Add(FRJanelas);
  //
  // GER-PROPR-001 :: Pesquisa Pagamentos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROPR-001';
  FRJanelas.Nome      := 'FmInadimp';
  FRJanelas.Descricao := 'Pesquisa pagamentos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-PROPR-002 :: Grupos de Unidades Habitacionais
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROPR-002';
  FRJanelas.Nome      := 'FmCondGri';
  FRJanelas.Descricao := 'Grupos de Unidades Habitacionais';
  FLJanelas.Add(FRJanelas);
  //
  // GER-PROPR-003 :: Impress�o de Certid�es e Cartas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROPR-003';
  FRJanelas.Nome      := 'FmCertidaoNeg';
  FRJanelas.Descricao := 'Impress�o de Certid�es e Cartas';
  FLJanelas.Add(FRJanelas);
  //
  // GER-PROPR-004 :: Bloquetos Inadimplentes
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROPR-004';
  FRJanelas.Nome      := 'FmInadimpBloq';
  FRJanelas.Descricao := 'Bloquetos Inadimplentes';
  FLJanelas.Add(FRJanelas);
  //
  // IMP-PROVI-001 :: Impress�o de provis�es por per�odo
  New(FRJanelas);
  FRJanelas.ID        := 'IMP-PROVI-001';
  FRJanelas.Nome      := 'FmPrevImp';
  FRJanelas.Descricao := 'Impress�o de provis�es por per�odo';
  FLJanelas.Add(FRJanelas);
  //
  // INA-REPAR-001 :: Reparcelamento de D�bitos Condominiais
  New(FRJanelas);
  FRJanelas.ID        := 'INA-REPAR-001';
  FRJanelas.Nome      := 'FmReparc';
  FRJanelas.Descricao := 'Reparcelamento de D�bitos Condominiais';
  FLJanelas.Add(FRJanelas);
  //
  //  LEI-CADAS-001 :: Consumos por Leitura
  New(FRJanelas);
  FRJanelas.ID        := 'LEI-CADAS-001';
  FRJanelas.Nome      := 'FmCons';
  FRJanelas.Descricao := 'Consumos por Leitura';
  FLJanelas.Add(FRJanelas);
  //
  //  LEI-PRINT-001 :: Impress�es de Consumos
  New(FRJanelas);
  FRJanelas.ID        := 'LEI-PRINT-001';
  FRJanelas.Nome      := 'FmLeituraImp';
  FRJanelas.Descricao := 'Impress�es de Consumos';
  FLJanelas.Add(FRJanelas);
  //
  //  PRO-DUSYS-001 :: Exporta Produsys
  New(FRJanelas);
  FRJanelas.ID        := 'PRO-DUSYS-001';
  FRJanelas.Nome      := 'FmProdusys1';
  FRJanelas.Descricao := 'Exporta Produsys';
  FLJanelas.Add(FRJanelas);
  //
  //  PRO-DUSYS-002 :: Vencimento
  New(FRJanelas);
  FRJanelas.ID        := 'PRO-DUSYS-002';
  FRJanelas.Nome      := 'FmProdusys1Vencto';
  FRJanelas.Descricao := 'Vencimento';
  FLJanelas.Add(FRJanelas);
  //
  //  SYN-EXTRA-001:: Carrega Lan�amentos de Sal�rios - Modelo 1
  New(FRJanelas);
  FRJanelas.ID        := 'SYN-EXTRA-001';
  FRJanelas.Nome      := 'FmImportSal1';
  FRJanelas.Descricao := 'Carrega Lan�amentos de Sal�rios - Modelo 1';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-ANALI-001 :: An�lise de Lan�amentos de Bloquetos
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-ANALI-001';
  FRJanelas.Nome      := 'FmBloqAnalisa';
  FRJanelas.Descricao := 'An�lise de Lan�amentos de Bloquetos';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-REPAR-001 :: Parcelamentos de Bloquetos
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-REPAR-001';
  FRJanelas.Nome      := 'FmBloqParc';
  FRJanelas.Descricao := 'Parcelamentos de Bloquetos';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-REPAR-002 :: Edi��o de Parcela de Parcelamento
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-REPAR-002';
  FRJanelas.Nome      := 'FmBloqParcPar';
  FRJanelas.Descricao := 'Edi��o de Parcela de Parcelamento';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-REPAR-003 :: Recorre��o de Valores de Parcelamento
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-REPAR-003';
  FRJanelas.Nome      := 'FmBloqParcCorrige';
  FRJanelas.Descricao := 'Recorre��o de Valores de Parcelamento';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-REPAR-004 :: Pesquisa de Reparcelamentos
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-REPAR-004';
  FRJanelas.Nome      := 'FmBloqParcPesq';
  FRJanelas.Descricao := 'Pesquisa de Reparcelamentos';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-REPAR-005 :: Aviso de Reparcelamentos Abertos
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-REPAR-005';
  FRJanelas.Nome      := 'FmBloqParcAviso';
  FRJanelas.Descricao := 'Pesquisa de Reparcelamentos Abertos';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-CLIEN-001 :: Clientes WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-CLIEN-001';
  FRJanelas.Nome      := 'FmWClients2';
  FRJanelas.Descricao := 'Clientes WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-CLIEN-002 :: Pesquisa de Usu�rios WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-CLIEN-002';
  FRJanelas.Nome      := 'FmWUsers';
  FRJanelas.Descricao := 'Pesquisa de Usu�rios WEB';
  FLJanelas.Add(FRJanelas);
  //
  PerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Empresas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Enti_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Finance_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  ALL_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Anotacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Moedas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Textos_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Cheques_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  IBGE_DTB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WTextos_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CNAB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Diario_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Protocol_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnFPMin_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnFPMax_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnNFSe_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnNFSe_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnGeral_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnSMS_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnInflIdx_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WUsers_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WGerl_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  FTP_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Mail_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Result := True;
end;

end.
