unit PrevBaC;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, Menus, ComCtrls, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, UnDmkProcFunc, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmPrevBaC = class(TForm)
    PainelDados: TPanel;
    DsPrevBaC: TDataSource;
    QrPrevBaC: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrPrevBaCCodigo: TIntegerField;
    QrPrevBaCConta: TIntegerField;
    QrPrevBaCLk: TIntegerField;
    QrPrevBaCDataCad: TDateField;
    QrPrevBaCDataAlt: TDateField;
    QrPrevBaCUserCad: TIntegerField;
    QrPrevBaCUserAlt: TIntegerField;
    QrPrevBaCNome: TWideStringField;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    LaForneceI: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    EdDBConta: TDBEdit;
    Label3: TLabel;
    QrPrevBaCNOMECONTA: TWideStringField;
    DBGrid1: TDBGrid;
    QrPrevBaI: TmySQLQuery;
    DsPrevBaI: TDataSource;
    QrPrevBaICodigo: TIntegerField;
    QrPrevBaIControle: TIntegerField;
    QrPrevBaICond: TIntegerField;
    QrPrevBaIValor: TFloatField;
    QrPrevBaITexto: TWideStringField;
    QrPrevBaISitCobr: TIntegerField;
    QrPrevBaIParcelas: TIntegerField;
    QrPrevBaIParcPerI: TIntegerField;
    QrPrevBaIParcPerF: TIntegerField;
    QrPrevBaILk: TIntegerField;
    QrPrevBaIDataCad: TDateField;
    QrPrevBaIDataAlt: TDateField;
    QrPrevBaIUserAlt: TIntegerField;
    PMProvisao: TPopupMenu;
    Incluiproviso1: TMenuItem;
    Alteraproviso1: TMenuItem;
    Excluiproviso1: TMenuItem;
    PMCondom: TPopupMenu;
    AdicionaCondomnioprovisoselecionada1: TMenuItem;
    Retiraocondominiodaprovisoselecionada1: TMenuItem;
    PnCond: TPanel;
    PnCondData: TPanel;
    Editaprovisoselecionada1: TMenuItem;
    QrPrevBaINOMECOND: TWideStringField;
    QrPrevBaINOMESITCOBR: TWideStringField;
    QrPrevBaIINICIO: TWideStringField;
    QrPrevBaIFINAL: TWideStringField;
    QrPrevBaIInfoParc: TIntegerField;
    QrPrevBaIInfoParc_TXT: TWideStringField;
    Timer1: TTimer;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label4: TLabel;
    EdDBCodigoFluxo: TDBEdit;
    EdDBNomeFluxo: TDBEdit;
    Label5: TLabel;
    RgSitCobr: TRadioGroup;
    Label8: TLabel;
    EdValor: TdmkEdit;
    GBPer: TGroupBox;
    GBIni: TGroupBox;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBMesI: TComboBox;
    CBAnoI: TComboBox;
    GBFim: TGroupBox;
    Label34: TLabel;
    LaAnoF: TLabel;
    CBMesF: TComboBox;
    CBAnoF: TComboBox;
    EdEmpresa: TdmkEditCB;
    Label6: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdTexto: TdmkEdit;
    Label11: TLabel;
    CkInfoParc: TCheckBox;
    GbPro: TGroupBox;
    Label7: TLabel;
    EdParcelas: TdmkEdit;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    Panel4: TPanel;
    Panel6: TPanel;
    Label12: TLabel;
    EdCodigoFluxo: TDBEdit;
    EdNomeFluxo: TDBEdit;
    Label13: TLabel;
    DBGrid2: TDBGrid;
    Label14: TLabel;
    QrDupl: TmySQLQuery;
    QrPesq: TmySQLQuery;
    QrPesqCodigo: TIntegerField;
    QrPesqConta: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel7: TPanel;
    BtSaida: TBitBtn;
    BtProvisao: TBitBtn;
    BtCondom: TBitBtn;
    GroupBox1: TGroupBox;
    Panel8: TPanel;
    LaAviso2: TLabel;
    LaAviso1: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GroupBox2: TGroupBox;
    Panel10: TPanel;
    Panel11: TPanel;
    BtConfCond: TBitBtn;
    BitBtn2: TBitBtn;
    SpeedButton5: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtProvisaoClick(Sender: TObject);
    procedure BtCondomClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPrevBaCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPrevBaCAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrevBaCBeforeOpen(DataSet: TDataSet);
    procedure QrPrevBaCBeforeClose(DataSet: TDataSet);
    procedure Incluiproviso1Click(Sender: TObject);
    procedure Alteraproviso1Click(Sender: TObject);
    procedure PMProvisaoPopup(Sender: TObject);
    procedure PMCondomPopup(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure AdicionaCondomnioprovisoselecionada1Click(Sender: TObject);
    procedure BtConfCondClick(Sender: TObject);
    procedure EdParcelasExit(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure RgSitCobrClick(Sender: TObject);
    procedure Editaprovisoselecionada1Click(Sender: TObject);
    procedure Retiraocondominiodaprovisoselecionada1Click(Sender: TObject);
    procedure QrPrevBaICalcFields(DataSet: TDataSet);
    procedure Timer1Timer(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    FMes: Word;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenQrPrevBaI(Controle: Integer);
  public
    { Public declarations }
    FMostra: Integer;
    FMostrou: Boolean;
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPrevBaC: TFmPrevBaC;
const
  FFormatFloat = '00000';

implementation

uses Module, Principal, UnMyObjects, ModuleGeral, UnFinanceiroJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPrevBaC.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPrevBaC.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrevBaCCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPrevBaC.DefParams;
begin
  VAR_GOTOTABELA := 'PrevBaC';
  VAR_GOTOMYSQLTABLE := QrPrevBaC;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT con.Nome NOMECONTA, pbc.*');
  VAR_SQLx.Add('FROM prevbac pbc');
  VAR_SQLx.Add('LEFT JOIN contas con ON con.Codigo=pbc.Conta');
  VAR_SQLx.Add('WHERE pbc.Codigo > 0');
  //
  VAR_SQL1.Add('AND pbc.Codigo=:P0');
  //
  VAR_SQLa.Add('AND pbc.Nome Like :P0');
  //
end;

procedure TFmPrevBaC.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo : Integer);
var
  Mes, Ano: Word;
begin
  // Evitar erro
  PageControl1.ActivePageIndex := 0;
  FMostrou := True;
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PnCond.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text    := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text      := '';
        EdConta.Text     := '';
        CBConta.KeyValue := NULL;
        EdConta.Enabled  := True;
        CBConta.Enabled  := True;
      end else begin
        EdCodigo.Text    := DBEdCodigo.Text;
        EdNome.Text      := DBEdNome.Text;
        EdConta.Text     := IntToStr(QrPrevBaCConta.Value);
        CBConta.KeyValue := QrPrevBaCConta.Value;
        EdConta.Enabled  := False;
        CBConta.Enabled  := False;
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      PnCond.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdEmpresa.Text         := '';
        CBEmpresa.KeyValue     := NULL;
        RgSitCobr.ItemIndex := 0;
        EdValor.Text        := '';
        EdParcelas.Text     := '';
        EdTexto.Text        := '';
        CBAnoI.ItemIndex    := 50;
        CBAnoF.ItemIndex    := 50;
        CBMesI.ItemIndex    := FMes - 1;
        CBMesF.ItemIndex    := FMes;
        CkInfoParc.Checked  := True;
      end else begin
        EdEmpresa.Text         := IntToStr(QrPrevBaICond.Value);
        CBEmpresa.KeyValue     := QrPrevBaICond.Value;
        RgSitCobr.ItemIndex := QrPrevBaISitCobr.Value;
        EdValor.Text        := Geral.FFT(QrPrevBaIValor.Value, 2, siNegativo);
        EdParcelas.Text     := Geral.FFT(QrPrevBaIParcelas.Value, 0, siPositivo);
        EdTexto.Text        := QrPrevBaITexto.Value;
        //
        if QrPrevBaIParcPerI.Value < 13 then
        begin
          CBAnoI.Itemindex  := 50;
          CBMesI.ItemIndex  := QrPrevBaIParcPerI.Value - 1;
        end else begin
          MLAGeral.MesEAnoDePeriodoShort(QrPrevBaIParcPerI.Value, Mes, Ano);
          CBAnoI.Itemindex  := Ano-Geral.IMV(CBAnoI.Items[0]);
          CBMesI.ItemIndex  := Mes - 1;
        end;
        //
        if QrPrevBaIParcPerF.Value < 13 then
        begin
          CBAnoF.Itemindex  := 50;
          CBMesF.ItemIndex  := QrPrevBaIParcPerF.Value - 1;
        end else begin
          MLAGeral.MesEAnoDePeriodoShort(QrPrevBaIParcPerF.Value, Mes, Ano);
          CBAnoF.Itemindex  := Ano-Geral.IMV(CBAnoF.Items[0]);
          CBMesF.ItemIndex  := Mes - 1;
        end;
      end;
        CkInfoParc.Checked  := Geral.IntToBool_0(QrPrevBaIInfoParc.Value);
      EdEmpresa.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPrevBaC.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPrevBaC.AlteraRegistro;
var
  PrevBaC : Integer;
begin
  PrevBaC := QrPrevBaCCodigo.Value;
  if not UMyMod.SelLockY(PrevBaC, Dmod.MyDB, 'PrevBaC', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PrevBaC, Dmod.MyDB, 'PrevBaC', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPrevBaC.IncluiRegistro;
var
  Cursor : TCursor;
  PrevBaC : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    PrevBaC := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PrevBaC', 'PrevBaC', 'Codigo');
    if Length(FormatFloat(FFormatFloat, PrevBaC))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, PrevBaC);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmPrevBaC.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPrevBaC.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPrevBaC.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPrevBaC.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPrevBaC.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPrevBaC.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPrevBaC.SpeedButton5Click(Sender: TObject);
begin
  if EdConta.Enabled and CBConta.Enabled then
  begin
    VAR_CADASTRO := 0;
    FinanceiroJan.CadastroDeContas(EdConta.ValueVariant);
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.AbreQuery(QrContas, DMod.MyDB);
      //
      EdConta.ValueVariant := VAR_CADASTRO;
      CBConta.KeyValue     := VAR_CADASTRO;
      EdConta.SetFocus;
    end;
  end;
end;

procedure TFmPrevBaC.BtProvisaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProvisao, BtProvisao);
end;

procedure TFmPrevBaC.BtCondomClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCondom, BtCondom);
end;

procedure TFmPrevBaC.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPrevBaCCodigo.Value;
  Close;
end;

procedure TFmPrevBaC.BtConfirmaClick(Sender: TObject);
var
  Conta, Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Conta := Geral.IMV(EdConta.Text);
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO prevbac SET ');
    QrPesq.Close;
    QrPesq.Params[0].AsInteger := Conta;
    QrPesq.Open;
    if QrPesq.RecordCount > 0 then
    begin
      Geral.MensagemBox('A conta selecionada j� foi utilizada ' +
      'para a provis�o n� ' + IntToStr(QrPesqCodigo.Value) + '!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end else Dmod.QrUpdU.SQL.Add('UPDATE prevbac SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Conta=:P1, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Conta;
  //
  Dmod.QrUpdU.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[04].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PrevBaC', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmPrevBaC.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PrevBaC', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PrevBaC', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PrevBaC', 'Codigo');
end;

procedure TFmPrevBaC.FormCreate(Sender: TObject);
var
  i: Integer;
  //
  Ano, Dia: Word;
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  PageControl1.ActivePageIndex := 0;
  FMostra := 0;
  FMostrou := False;
  RgSitCobr.Items := FmPrincipal.RgSitCobr.Items;

  //////////////////////////////////////////////////////////////////////////

  with CBMesI.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date, Ano, FMes, Dia);
  with CBAnoI.Items do
  begin
    for i := Ano-50 to Ano+50 do Add(IntToStr(i));
  end;
  CBAnoI.ItemIndex := 50;
  CBMesI.ItemIndex := (FMes - 1);
  //////////////////////////////////////////////////////////////////////////
  with CBMesF.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date, Ano, FMes, Dia);
  with CBAnoF.Items do
  begin
    for i := Ano-50 to Ano+50 do Add(IntToStr(i));
  end;
  CBAnoF.ItemIndex := 50;
  CBMesF.ItemIndex := (FMes - 1);

  //////////////////////////////////////////////////////////////////////////

  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PnCondData.Align  := alClient;
  DBGrid1.Align     := alClient;
  //
  UMyMod.AbreQuery(QrContas, DMod.MyDB);
  CriaOForm;
end;

procedure TFmPrevBaC.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPrevBaCCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPrevBaC.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmPrevBaC.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPrevBaC.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmPrevBaC.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPrevBaC.QrPrevBaCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPrevBaC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'PrevBaC', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmPrevBaC.QrPrevBaCAfterScroll(DataSet: TDataSet);
begin
  //BtAltera.Enabled := GOTOy.BtEnabled(QrPrevBaCCodigo.Value, False);
  ReopenQrPrevBaI(0);
end;

procedure TFmPrevBaC.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPrevBaCCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PrevBaC', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPrevBaC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrevBaC.QrPrevBaCBeforeOpen(DataSet: TDataSet);
begin
  QrPrevBaCCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPrevBaC.QrPrevBaCBeforeClose(DataSet: TDataSet);
begin
  QrPrevBaI.Close;
end;

procedure TFmPrevBaC.ReopenQrPrevBaI(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrevBaI, Dmod.MyDB, [
  'SELECT DISTINCT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
  'ELSE ent.Nome END NOMECOND, pbi.*',
  'FROM cond cnd',
  'LEFT JOIN prevbai pbi ON cnd.Codigo=pbi.Cond',
  'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente',
  'WHERE pbi.Codigo=' + FormatFloat('0', QrPrevBaCCodigo.Value),
  'AND cnd.Codigo IN (' + VAR_LIB_FILIAIS + ')',
  '']);
  //
  QrPrevBaI.Locate('Controle', Controle, []);
end;

procedure TFmPrevBaC.Incluiproviso1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmPrevBaC.Alteraproviso1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmPrevBaC.PMProvisaoPopup(Sender: TObject);
begin
  Alteraproviso1.Enabled := Geral.IntToBool_0(QrPrevBaCCodigo.Value);
end;

procedure TFmPrevBaC.PMCondomPopup(Sender: TObject);
begin
  AdicionaCondomnioprovisoselecionada1.Enabled := Geral.IntToBool_0(QrPrevBaCCodigo.Value);
  Retiraocondominiodaprovisoselecionada1.Enabled := Geral.IntToBool_0(QrPrevBaIControle.Value);
  Editaprovisoselecionada1.Enabled := Geral.IntToBool_0(QrPrevBaIControle.Value);
end;

procedure TFmPrevBaC.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
  if FMostra > 0 then Close;
end;

procedure TFmPrevBaC.AdicionaCondomnioprovisoselecionada1Click(
  Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmPrevBaC.EdParcelasExit(Sender: TObject);
begin
  EdParcelas.Text := Geral.TFT(EdParcelas.Text, 0, siPositivo);
end;

procedure TFmPrevBaC.EdValorExit(Sender: TObject);
begin
  EdValor.Text := Geral.TFT(EdValor.Text, 2, siNegativo);
end;

procedure TFmPrevBaC.BtConfCondClick(Sender: TObject);
var
  SitCobr, Ano, PeriodoI, PeriodoF, PeriodoA, Parcelas, Controle, Cond, pp: Integer;
  Valor: Double;
begin
  //
  SitCobr := RgSitCobr.ItemIndex;
  Cond := Geral.IMV(EdEmpresa.Text);
  if Cond = 0 then
  begin
    Geral.MensagemBox('Informe a empresa!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdEmpresa.SetFocus;
    Exit;
  end;
  Valor := Geral.DMV(EdValor.Text);
  if (Valor < 0.01) and (Valor > -0.01) then
  begin
    if Geral.MensagemBox('Nenhum valor foi definido! Deseja continuar assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      EdValor.SetFocus;
      Exit;
    end;
  end;
  PeriodoA := Geral.Periodo2000(Date);
  Parcelas := Geral.IMV(EdParcelas.Text);
  //
  PeriodoI := CBMesI.ItemIndex + 1;
  if SitCobr = 2 then
  begin
    Ano := Geral.IMV(CBAnoI.Text);
    PeriodoI := ((Ano - 2000) * 12) + PeriodoI;
  end;
  //
  PeriodoF := CBMesF.ItemIndex + 1;
  if SitCobr = 2 then
  begin
    Ano := Geral.IMV(CBAnoF.Text);
    PeriodoF := ((Ano - 2000) * 12) + PeriodoF;
    //
    if (PeriodoF < PeriodoA) then
    if Geral.MensagemBox('Per�odo j� expirado! Deseja continuar assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
    //
    pp := PeriodoF - PeriodoI + 1;
    if Parcelas <> pp then
    begin
      Geral.MensagemBox('N�mero de parcelas n�o confere com per�odo! '+
      'Confirma��o abortada!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //
  end;
  Dmod.QrUpd.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add('INSERT INTO prevbai SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE prevbai SET ');
  Dmod.QrUpd.SQL.Add('Cond=:P0, Valor=:P1, Texto=:P2, SitCobr=:P3, ');
  Dmod.QrUpd.SQL.Add('Parcelas=:P4, ParcPerI=:P5, ParcPerF=:P6, ');
  Dmod.QrUpd.SQL.Add('InfoParc=:P7, ');
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PrevBaI', 'PrevBaI', 'Controle');
  end else begin
    Dmod.QrUpd.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
    Controle := QrPrevBaIControle.Value;
  end;
  Dmod.QrUpd.Params[00].AsInteger := Cond;
  Dmod.QrUpd.Params[01].AsFloat   := Valor;
  Dmod.QrUpd.Params[02].AsString  := EdTexto.Text;
  Dmod.QrUpd.Params[03].AsInteger := SitCobr;
  Dmod.QrUpd.Params[04].AsInteger := Parcelas;
  Dmod.QrUpd.Params[05].AsInteger := PeriodoI;
  Dmod.QrUpd.Params[06].AsInteger := PeriodoF;
  Dmod.QrUpd.Params[07].AsInteger := MLAGeral.BoolToInt(CkInfoParc.Checked);
  //
  Dmod.QrUpd.Params[08].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[09].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[10].AsInteger := QrPrevBaCCodigo.Value;
  Dmod.QrUpd.Params[11].AsInteger := Controle;
  //
  Dmod.QrUpd.ExecSQL;
  ReopenQrPrevBaI(Controle);
  //
  MostraEdicao(0, stLok, 0);
  if FMostra > 0 then Close;
end;

procedure TFmPrevBaC.RgSitCobrClick(Sender: TObject);
begin
  case RgSitCobr.ItemIndex of
    0: // N�o cobrar
    begin
      GbPer.Visible      := False;
      GbPro.Visible      := False;
      CkInfoParc.Visible := False;
    end;
    1: // Cont�nua
    begin
      GbPer.Visible      := True;
      GbPro.Visible      := False;
      LaAnoI.Visible     := False;
      CBAnoI.Visible     := False;
      LaAnoF.Visible     := False;
      CBAnoF.Visible     := False;
      CkInfoParc.Visible := True;
    end;
    2: // Programada
    begin
      GbPer.Visible      := True;
      GbPro.Visible      := True;
      LaAnoI.Visible     := True;
      CBAnoI.Visible     := True;
      LaAnoF.Visible     := True;
      CBAnoF.Visible     := True;
      CkInfoParc.Visible := True;
    end;
    3: // Por agendamento
    begin
      GbPer.Visible      := False;
      GbPro.Visible      := False;
      CkInfoParc.Visible := False;
    end;
  end;
end;

procedure TFmPrevBaC.Editaprovisoselecionada1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmPrevBaC.Retiraocondominiodaprovisoselecionada1Click(
  Sender: TObject);
var
  Prox: Integer;
begin
  if Geral.MensagemBox('Confirma a retirada do condom�nio "' +
  QrPrevBaINOMECOND.Value + '" desta provis�o base?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM prevbai WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrPrevBaIControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Prox := UMyMod.ProximoRegistro(QrPrevBaI, 'Controle', QrPrevBaIControle.Value);
    ReopenQrPrevBaI(Prox);
  end;
end;

procedure TFmPrevBaC.QrPrevBaICalcFields(DataSet: TDataSet);
var
  Ini, Fim: String;
begin
  QrPrevBaINOMESITCOBR.Value := RgSitCobr.Items[QrPrevBaISitCobr.Value];
  case QrPrevBaISitCobr.Value of
    1:
    begin
      Ini := dmkPF.VerificaMes(QrPrevBaIParcPerI.Value, False);
      Fim := dmkPF.VerificaMes(QrPrevBaIParcPerF.Value, False);
    end;
    2:
    begin
      Ini := dmkPF.MesEAnoDoPeriodo(QrPrevBaIParcPerI.Value);
      Fim := dmkPF.MesEAnoDoPeriodo(QrPrevBaIParcPerF.Value);
    end;
    else
    begin
      Ini := '';
      Fim := '';
    end;
  end;
  QrPrevBaIINICIO.Value := Ini;
  QrPrevBaIFINAL.Value := Fim;
  //
  if QrPrevBaIInfoParc.Value = 0 then
    QrPrevBaIInfoParc_TXT.Value := 'N�o'
  else
    QrPrevBaIInfoParc_TXT.Value := 'Sim';
end;

procedure TFmPrevBaC.Timer1Timer(Sender: TObject);
begin
  if FMostra > 0 then
  begin
    Timer1.Enabled := False;
    if FMostrou = False then
    begin
      FMostrou := True;
      if FMostra = 2 then MostraEdicao(2, stUpd, 0);
    end;  
  end;
end;

end.

