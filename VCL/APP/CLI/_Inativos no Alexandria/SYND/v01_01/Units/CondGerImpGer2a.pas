unit CondGerImpGer2a;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frxClass, frxDBSet, Grids, StdCtrls, Buttons, ExtCtrls, DB, frxCross,
  mySQLDbTables, dmkEdit, DBGrids, ABSMain, Menus, dmkGeral, printers, frxRich,
  frxChBox, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmCondGerImpGer2a = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    GradeA: TStringGrid;
    frxGrade: TfrxReport;
    frxCrossObject1: TfrxCrossObject;
    BtTitulo: TBitBtn;
    GradeB: TStringGrid;
    BitBtn2: TBitBtn;
    frxGrade2: TfrxReport;
    CkDesign: TCheckBox;
    frxDsQuery: TfrxDBDataset;
    PMGradeA: TPopupMenu;
    Alterasiglanocadastrodacontaplanodecontas1: TMenuItem;
    Alterasiglanocadastrodaarrecadaobase1: TMenuItem;
    Alteratextodottuloselecionado1: TMenuItem;
    Larguradacolunaatual1: TMenuItem;
    AlterasiglanocadastrodaLeitura1: TMenuItem;
    Panel3: TPanel;
    dmkEdit5: TdmkEdit;
    dmkEdit6: TdmkEdit;
    QrPgBloq: TmySQLQuery;
    QrPgBloqApto: TIntegerField;
    QrPgBloqFatNum: TFloatField;
    QrPgBloqData: TDateField;
    QrPgBloqDATA_TXT: TWideStringField;
    QrAptos: TmySQLQuery;
    QrAptosAndar: TIntegerField;
    QrAptosUnidade: TWideStringField;
    QrAptosNOMEPROP: TWideStringField;
    QrAptosNOMEUSUARIO: TWideStringField;
    QrAptosSigla: TWideStringField;
    QrAptosConta: TIntegerField;
    QrAptosDATAPG_TXT: TWideStringField;
    QrBol3: TmySQLQuery;
    QrBol3Ordem: TIntegerField;
    QrBol3FracaoIdeal: TFloatField;
    QrBol3Andar: TIntegerField;
    QrBol3Boleto: TFloatField;
    QrBol3Apto: TIntegerField;
    QrBol3Propriet: TIntegerField;
    QrBol3Vencto: TDateField;
    QrBol3Unidade: TWideStringField;
    QrBol3NOMEPROPRIET: TWideStringField;
    QrBol3BOLAPTO: TWideStringField;
    QrBol3KGT: TLargeintField;
    Query: TmySQLQuery;
    QrPgBloqVENCTO_TXT: TWideStringField;
    QrAptosVENCTO_TXT: TWideStringField;
    Label5: TLabel;
    QrAptosTDC: TWideStringField;
    QrPgBloqPago: TFloatField;
    QrAptosPAGO: TFloatField;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    EdLinhaAlt: TdmkEdit;
    Label2: TLabel;
    EdTamFonte: TdmkEdit;
    CkRecalcN: TCheckBox;
    CkRecalcT: TCheckBox;
    CkLargTit: TCheckBox;
    Label3: TLabel;
    EdLarguraFolha: TdmkEdit;
    Label4: TLabel;
    GroupBox1: TGroupBox;
    RGAgrup: TRadioGroup;
    QrAptosDATA: TDateField;
    QrAptosVENCIMENTO: TDateField;
    QrPgBloqVencimento: TDateField;
    Panel5: TPanel;
    Panel6: TPanel;
    CkDescen1: TCheckBox;
    RGOrdem1: TRadioGroup;
    Panel7: TPanel;
    Panel8: TPanel;
    CkDescen2: TCheckBox;
    Panel9: TPanel;
    Panel10: TPanel;
    CkDescen3: TCheckBox;
    Panel11: TPanel;
    Panel12: TPanel;
    CkDescen4: TCheckBox;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    Panel4: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    QrLin: TmySQLQuery;
    QrLinLinha: TIntegerField;
    QrLinSeqLn: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxGradeBeforePrint(Sender: TfrxReportComponent);
    procedure FormCreate(Sender: TObject);
    procedure frxGradeGetValue(const VarName: string; var Value: Variant);
    function frxGradeUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure BtTituloClick(Sender: TObject);
    procedure EdLinhaAltChange(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure PMGradeAPopup(Sender: TObject);
    procedure EdTamFonteChange(Sender: TObject);
    procedure Alterasiglanocadastrodacontaplanodecontas1Click(Sender: TObject);
    procedure GradeASelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GradeAMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EdLarguraFolhaChange(Sender: TObject);
    procedure Larguradacolunaatual1Click(Sender: TObject);
    procedure Alterasiglanocadastrodaarrecadaobase1Click(Sender: TObject);
    procedure Alteratextodottuloselecionado1Click(Sender: TObject);
    procedure AlterasiglanocadastrodaLeitura1Click(Sender: TObject);
    procedure EdTamFonteExit(Sender: TObject);
    procedure GradeAMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure CkLargTitClick(Sender: TObject);
    procedure CkRecalcNClick(Sender: TObject);
    procedure CkRecalcTClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RGOrdem1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FColClicked, FRowClicked, FXClicked, FYClicked, FCodCad, FTipoGrade: Integer;
    FTitulo, FSigla, FCam: String;
    //FTextoCopia: String;
    procedure RecalculaLarguraColunaTextos;
    procedure RecalculaAlturaLinhaTitulos;

    function ObtemFrxDataField(const Coluna: Integer;
             var Campo, Somas: String; var HAlinha: TfrxHAlign;
             var Somar: Boolean): Boolean;
    function ObtemSigla(EhLeitura: Boolean): Boolean;
    procedure DefineTamanhoTexto(const Texto: String; var Tam: Integer);
    procedure MostraPMTitulo(Como: Integer);
    procedure ReacertaFrxDataSets();
    function Descende(Ck: TCheckBox): String;
   public
    { Public declarations }
    FImprimePage1, FImprimePage2: Boolean;
    FColTotVal(*, FAptTot, FAptPag*): Integer;
    //
    FCalcula: Boolean;
    procedure RelatorioDeArrecadacoes_ListaDeCondominos(Mostra: Boolean);
    procedure ImprimeGrade2();
    procedure CalculaLarguraFolha();
    function  Define_frxCond(frx: TfrxReport): TfrxReport;
    procedure RecriaQuery();
    procedure AtualizaQuery(Coluna, Linha: Integer; Valor: String);
    procedure AdicionaMemos(const Band: TfrxBand; const T, M:
              Integer; Titulo: String; var TamFix: Integer);
    function ObtemlinhaDoBoleto(SeqLn, Boleto: Double): Integer;
  end;

var
  FmCondGerImpGer2a: TFmCondGerImpGer2a;

const
  FColIniVal = 9; //6
  FDotImp    = 0.0377953;
  //SSomasPG = '[FormatFloat(''#,###,##0.00;-#,###,##0.00; '', SUM(<frxDsQuery."QuitVal">))]';

implementation

uses Module, ModuleCond, CondGer, UnMLAGeral, UMySQLModule, UnInternalConsts,
MyDBCheck, CondGerImpGerLar, UnBancos, MyGlyfs, ModuleGeral, UnAuxCondGer,
Principal, UnMyObjects, ModuleBloq, ModuleLct2;

{$R *.dfm}
var
  Col0, Col1, Col2, Col3, Col4, Col5, Col6, Col7, Col8: Integer;

const
  OrdemPor_SQL: array [0..4] of String =
    ('Linha', 'Propriet', 'Vencim', 'Quitou', 'TDC');
  OrdemPor_frx: array [0..4] of String =
    ('UH', 'Propriet', 'Vencim', 'Quitou', 'TDC');
  Row0 = 0;
  Row1 = 1;
  Row2 = 2;
var
  OrdemPor_Tit: array of String;

procedure TFmCondGerImpGer2a.AdicionaMemos(const Band: TfrxBand; const T, M:
Integer; Titulo: String; var TamFix: Integer);
var
  Memo: TfrxMemoView;
  L, W, H, (*Posicao, Z, X,*) Y, U: Integer;
  I, C, N: Integer;
  Campo, Somas: String;
  AlinhaH: TfrxHAlign;
  Somar: Boolean;
begin
  L := 0;
  W := 0;
  H :=  GradeA.DefaultRowHeight;
  //
  for I := 1 to GradeA.ColCount -1 do
  begin
    C := Geral.IMV(GradeA.Cells[i, GradeA.RowCount -1]);
    ObtemFrxDataField(C, Campo, Somas, AlinhaH, Somar);
    if GradeA.ColWidths[i] < 5 then Campo := '';
    //
    L :=  L + W;
    W :=  GradeA.ColWidths[i];

    Y := Round(T / VAR_frCM);
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    if c < FColIniVal then
    begin
      if c = Col1 then // 'TOTAIS'
      begin
        U := 0;
        for n := 1 to 4 do
          U := U + GradeA.ColWidths[n];
        Memo.SetBounds(L, Y, U, H);
        if GradeA.ColWidths[i] < 4 then
          Memo.Memo.Text   := ''
        else
          Memo.Memo.Text   := Titulo;
        //
      end
      else
      if c = Col5 then
      begin
        U := GradeA.ColWidths[c];
        Memo.SetBounds(L, Y, U, H);
        //Memo.Memo.Text := Geral.FFT(FAptPag, 0, True);
        Memo.Memo.Text := Somas;
        //
        TamFix := Trunc(Memo.Left + Memo.Width);
      end
      else
      if c = Col6 then
      begin
        U := GradeA.ColWidths[c];
        Memo.SetBounds(L, Y, U, H);
        //Memo.Memo.Text := Geral.FFT(FAptTot, 0, True);
        Memo.Memo.Text := Somas;
        //Tam006 := U;
      end
      else
      if c = Col7 then
      begin
        U := GradeA.ColWidths[c];
        Memo.SetBounds(L, Y, U, H);
        Memo.Memo.Text := Somas;
        //Tam007 := U;
      end
      else
      if c = Col8 then
      begin
        U := GradeA.ColWidths[c];
        Memo.SetBounds(L, Y, U, H);
        Memo.Memo.Text := '';
        //Tam006 := U;
      end;
    end else begin
      Memo.SetBounds(L, Y, W, H);
      if GradeA.ColWidths[i] < 5 then
        Memo.Memo.Text   := ''
      else
        Memo.Memo.Text   := Somas;
    end;
    Memo.Visible     := True;
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := EdTamFonte.ValueVariant;
    Memo.HAlign      := AlinhaH;
    Memo.VAlign      := vaCenter;
    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
    Memo.WordWrap    := False;
    Memo.Font.Style  := [fsBold];
  end;
end;

procedure TFmCondGerImpGer2a.Alterasiglanocadastrodaarrecadaobase1Click(
  Sender: TObject);
begin
  if ObtemSigla(False) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'arrebac', False,
      ['Sigla'], ['Codigo'], [FSigla], [FCodCad], True);
    if CkRecalcN.Checked then
      RelatorioDeArrecadacoes_ListaDeCondominos(False)
    else
      GradeA.Cells[GradeA.Col, Row0] := FSigla;
  end;
end;

procedure TFmCondGerImpGer2a.Alterasiglanocadastrodacontaplanodecontas1Click(
  Sender: TObject);
begin
  if ObtemSigla(False) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'contas', False,
      ['Sigla'], ['Codigo'], [FSigla], [FCodCad], True);
    if CkRecalcN.Checked then
      RelatorioDeArrecadacoes_ListaDeCondominos(False)
    else
      GradeA.Cells[GradeA.Col, Row0] := FSigla;
  end;
end;

procedure TFmCondGerImpGer2a.AlterasiglanocadastrodaLeitura1Click(
  Sender: TObject);
var
  i, t, c: Integer;
begin
  if ObtemSigla(True) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cons', False,
      ['Sigla'], ['Codigo'], [FSigla], [FCodCad], True);
    if CkRecalcN.Checked then
      RelatorioDeArrecadacoes_ListaDeCondominos(False)
    else begin
      DmBloq.QrListaL.Close;
      DmBloq.QrListaL.Open;
      for i := FColIniVal to GradeA.ColCount - 1 do
      begin
        t := Geral.IMV(GradeB.Cells[i, Row0]);
        //
        if t in ([1,2]) then
        begin
          c := Geral.IMV(GradeB.Cells[i, Row1]);
          if DmBloq.QrListaL.Locate('Codigo', c, []) then
            GradeA.Cells[i, Row0] := DmBloq.QrListaLUnidLei.Value + ' ' + DmBloq.QrListaLSIGLA_IMP.Value;
        end;
      end;
    end;
  end;
end;

procedure TFmCondGerImpGer2a.Alteratextodottuloselecionado1Click(Sender: TObject);
begin
  if ObtemSigla(False) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'contas', False,
      ['Sigla'], ['Codigo'], [FSigla], [FCodCad], True);
    if CkRecalcN.Checked then
      RelatorioDeArrecadacoes_ListaDeCondominos(False)
    else
      GradeA.Cells[GradeA.Col, Row0] := FSigla;
  end;
end;

procedure TFmCondGerImpGer2a.AtualizaQuery(Coluna, Linha: Integer;
  Valor: String);
var
   Val, ColTxt: String;
begin
  if MLAGeral.EhIntOuFloat(Valor) then
  begin
    Val := MLAGeral.TTS(Valor);
    ColTxt := 'ValCol' + MLAGeral.FTX(Coluna - FColIniVal + 1, 3, 0, siPositivo);
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE stringgrid1');
    DModG.QrUpdPID1.SQL.Add('SET ' + ColTxt + ' = ' + ColTxt + ' + ' + Val);
    DModG.QrUpdPID1.SQL.Add('WHERE Seqln=' + FormatFloat('0', Linha));
    DModG.QrUpdPID1.ExecSQL;
  end;
  //
end;

procedure TFmCondGerImpGer2a.BtTituloClick(Sender: TObject);
begin
  MostraPMTitulo(1);
end;

procedure TFmCondGerImpGer2a.MostraPMTitulo(Como: Integer);
begin
  if (FColClicked > 0) and (FColClicked < GradeA.ColCount) then
  begin
    FSigla := GradeA.Cells[FColClicked, Row0];
    case Como of
      1: MyObjects.MostraPopUpDeBotao(PMGradeA, BtTitulo);
      //2: MLAGeral.MostraPopUpDeBotaoObject(PMGradeA, GradeA, FXClicked, FYClicked);
      2: PMGradeA.Popup(FXClicked + GradeA.Left + Panel1.Left + Left,
      FYClicked + GradeA.Top + Panel1.Top + Top);
    end;
  end else Geral.MensagemBox('Selecione uma coluna v�lida!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmCondGerImpGer2a.CalculaLarguraFolha;
var
  i, n: Integer;
begin
  n := 0;
  for i := 1 to GradeA.ColCount - 1 do
  n := n + GradeA.ColWidths[i];
  EdLarguraFolha.ValueVariant := n;
end;

procedure TFmCondGerImpGer2a.CkLargTitClick(Sender: TObject);
begin
  RelatorioDeArrecadacoes_ListaDeCondominos(False);
end;

procedure TFmCondGerImpGer2a.CkRecalcNClick(Sender: TObject);
begin
  RelatorioDeArrecadacoes_ListaDeCondominos(False);
end;

procedure TFmCondGerImpGer2a.CkRecalcTClick(Sender: TObject);
begin
  RelatorioDeArrecadacoes_ListaDeCondominos(False);
end;

procedure TFmCondGerImpGer2a.DefineTamanhoTexto(const Texto: String;
  var Tam: Integer);
var
  k: Integer;
begin
  k := Canvas.TextWidth(Texto) + 8;
  if k > Tam then
    Tam := k;
end;

procedure TFmCondGerImpGer2a.BitBtn2Click(Sender: TObject);
begin
  FTitulo := 'Lista de Arrecada��es';
  ReacertaFrxDataSets();
  MyObjects.frxMostra(frxGrade, 'Lista de Arrecada��es por unidade');
end;

procedure TFmCondGerImpGer2a.BtOKClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    SetLength(OrdemPor_Tit, 5);
    OrdemPor_Tit[0] := DModG.RecaptionTexto(VAR_U_H) + ': ';
    OrdemPor_Tit[1] := DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) + ': ';
    OrdemPor_Tit[2] := 'Vencimento: ';
    OrdemPor_Tit[3] := 'Quitado: ';
    OrdemPor_Tit[4] := 'Tipo de documento de cobran�a (TDC): ';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
    'SELECT * FROM stringgrid1',
    'ORDER BY ' +
      OrdemPor_SQL[RGOrdem1.ItemIndex] + Descende(CkDescen1) + ', ' +
      OrdemPor_SQL[RGOrdem2.ItemIndex] + Descende(CkDescen2) + ', ' +
      OrdemPor_SQL[RGOrdem3.ItemIndex] + Descende(CkDescen3) + ', ' +
      OrdemPor_SQL[RGOrdem4.ItemIndex] + Descende(CkDescen4),
      '']);
    //
    //
    ImprimeGrade2();
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGerImpGer2a.BtSaidaClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU(RGOrdem1.Name, FCam, RGOrdem1.ItemIndex, ktInteger);
  Geral.WriteAppKeyCU(RGOrdem2.Name, FCam, RGOrdem2.ItemIndex, ktInteger);
  Geral.WriteAppKeyCU(RGOrdem3.Name, FCam, RGOrdem3.ItemIndex, ktInteger);
  Geral.WriteAppKeyCU(RGOrdem4.Name, FCam, RGOrdem4.ItemIndex, ktInteger);
  //
  Geral.WriteAppKeyCU(RGAgrup.Name, FCam, RGAgrup.ItemIndex, ktInteger);
  //
  Geral.WriteAppKeyCU(CkDescen1.Name, FCam, CkDescen1.Checked, ktBoolean);
  Geral.WriteAppKeyCU(CkDescen2.Name, FCam, CkDescen2.Checked, ktBoolean);
  Geral.WriteAppKeyCU(CkDescen3.Name, FCam, CkDescen3.Checked, ktBoolean);
  Geral.WriteAppKeyCU(CkDescen4.Name, FCam, CkDescen4.Checked, ktBoolean);
  //
  Hide;
end;

procedure TFmCondGerImpGer2a.EdLarguraFolhaChange(Sender: TObject);
begin
  if EdLarguraFolha.ValueVariant > 1009 then
    EdLarguraFolha.Font.Color := clRed
  else
  if EdLarguraFolha.ValueVariant > 680 then
    EdLarguraFolha.Font.Color := $004080FF  // Laranja
  else
    EdLarguraFolha.Font.Color := clBlue;
end;

procedure TFmCondGerImpGer2a.EdLinhaAltChange(Sender: TObject);
begin
  GradeA.DefaultRowHeight := EdLinhaAlt.ValueVariant;
end;

procedure TFmCondGerImpGer2a.EdTamFonteChange(Sender: TObject);
begin
  GradeA.Font.Size := EdTamFonte.ValueVariant;
end;

procedure TFmCondGerImpGer2a.EdTamFonteExit(Sender: TObject);
begin
  RelatorioDeArrecadacoes_ListaDeCondominos(False)
end;

procedure TFmCondGerImpGer2a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ReacertaFrxDataSets();
end;

procedure TFmCondGerImpGer2a.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Fechar: TBasicAction;
begin
  if Self.Visible then
  begin
    Self.Hide;
    Fechar := TBasicAction(caNone);
    Self.Action := Fechar;
  end;  
end;

procedure TFmCondGerImpGer2a.FormCreate(Sender: TObject);
begin
  FCalcula := False;
  Col0 := 0;
  Col1 := 1;
  Col2 := 2;
  Col3 := 3;
  Col4 := 4;
  Col5 := 5;
  Col6 := 6;
  Col7 := 7;
  Col8 := 8;
  //
  FColClicked := -1;
  FRowClicked := -1;
  FXClicked   := 0;
  FYClicked   := 0;
  FTipoGrade  := 0;
  frxGrade.AddFunction(
        'function ObtemHALign(Col, Lin: Extended): Integer');
  frxGrade.AddFunction(
        'function ObtemWidth(Col: Extended): Integer');
  frxGrade.AddFunction(
        'function ObtemRowHeight(Row: Extended): Integer');
  GradeA.ColWidths[Col0] := 056;
  GradeA.ColWidths[Col1] := 070;
  GradeA.ColWidths[Col2] := 024;
  GradeA.ColWidths[Col3] := 024;
  GradeA.ColWidths[Col4] := 160;
  GradeA.ColWidths[Col5] := 078;
  GradeA.ColWidths[Col6] := 078;
  GradeA.ColWidths[Col7] := 024;
  //
  //
end;

procedure TFmCondGerImpGer2a.FormShow(Sender: TObject);
var
  I: Integer;
  B, Calcula: Boolean;
begin
  Calcula := False;
  FCalcula := False;
  //
  FCam := Application.Title + '\' + TMeuDB;
  I := Geral.ReadAppKeyCU(RGOrdem1.Name, FCam, ktInteger, RGOrdem1.ItemIndex);
  if I <> RGOrdem1.ItemIndex then
  begin
    RGOrdem1.ItemIndex := I;
    Calcula := True;
  end;
  I := Geral.ReadAppKeyCU(RGOrdem2.Name, FCam, ktInteger, RGOrdem2.ItemIndex);
  if I <> RGOrdem2.ItemIndex then
  begin
    RGOrdem2.ItemIndex := I;
    Calcula := True;
  end;
  I := Geral.ReadAppKeyCU(RGOrdem3.Name, FCam, ktInteger, RGOrdem3.ItemIndex);
  if I <> RGOrdem3.ItemIndex then
  begin
    RGOrdem3.ItemIndex := I;
    Calcula := True;
  end;
  I := Geral.ReadAppKeyCU(RGOrdem4.Name, FCam, ktInteger, RGOrdem4.ItemIndex);
  if I <> RGOrdem4.ItemIndex then
  begin
    RGOrdem4.ItemIndex := I;
    Calcula := True;
  end;
  //
  I := Geral.ReadAppKeyCU(RGAgrup.Name, FCam, ktInteger, RGAgrup.ItemIndex);
  if I <> RGAgrup.ItemIndex then
    RGAgrup.ItemIndex := I;
  //
  B := Geral.ReadAppKeyCU(CkDescen1.Name, FCam, ktBoolean, CkDescen1.Checked);
  if B <> CkDescen1.Checked then
  begin
    CkDescen1.Checked := B;
    Calcula := True;
  end;
  B := Geral.ReadAppKeyCU(CkDescen2.Name, FCam, ktBoolean, CkDescen2.Checked);
  if B <> CkDescen2.Checked then
  begin
    CkDescen2.Checked := B;
    Calcula := True;
  end;
  B := Geral.ReadAppKeyCU(CkDescen3.Name, FCam, ktBoolean, CkDescen3.Checked);
  if B <> CkDescen3.Checked then
  begin
    CkDescen3.Checked := B;
    Calcula := True;
  end;
  B := Geral.ReadAppKeyCU(CkDescen4.Name, FCam, ktBoolean, CkDescen4.Checked);
  if B <> CkDescen4.Checked then
  begin
    CkDescen4.Checked := B;
    Calcula := True;
  end;
  //
  FCalcula := True;
  if Calcula then
    RelatorioDeArrecadacoes_ListaDeCondominos(False);
end;

procedure TFmCondGerImpGer2a.frxGradeBeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    if FTipoGrade = 0 then
    begin
      for i := 1 to GradeA.RowCount do
        for j := 1 to GradeA.ColCount do
          Cross.AddValue([i], [j], [GradeA.Cells[j - 1, i - 1]]);
    end else begin
      for i := 1 to GradeA.RowCount do
        for j := 1 to GradeA.ColCount do
          Cross.AddValue([i], [j], [GradeA.Cells[i - 1, j - 1]]);
    end;
   // Cross.Align :=
  end;
end;

procedure TFmCondGerImpGer2a.frxGradeGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULO' then Value := FTitulo
  else
  if VarName = 'VARF_MEMO_HEIGHT' then
    Value := GradeA.RowHeights[0];
end;

function TFmCondGerImpGer2a.frxGradeUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if MethodName = Uppercase('ObtemHALign') then
  begin
    if (Params[1] = 0) or (Params[0] = 2) then
      Result := 0
    else
      Result := 2;
  end else
  if MethodName = Uppercase('ObtemWidth') then
    Result := GradeA.ColWidths[Params[0]]
  else
  if MethodName = Uppercase('ObtemRowHeight') then
    Result := GradeA.DefaultRowHeight
end;

procedure TFmCondGerImpGer2a.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor, c, r, Col, Row: Integer;
  Texto: String;
begin
  c := GradeA.FixedCols;
  r := GradeA.FixedRows;
  Col := Geral.IMV(GradeA.Cells[ACol, GradeA.RowCount -1]);
  Row := ARow;
  //
  //if GradeA.ColWidths[Col] < 5 then
    //Texto := ''
  //else
    Texto := GradeA.Cells[ACol, ARow];
  //
  if (Col = 0) or (Row = 0) then
    Cor := PainelConfirma.Color
  else if GradeA.Cells[Col, Row] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;
  //
  if Row = 0 then
    MyObjects.StringGridRotateTextOut(GradeA, Row, Col, Rect, 'Univers Light Condensed',
      10, clWindowText, taLeftJustify)
  else
  if (Row = GradeA.RowCount -1) then
  begin
    MyObjects.DesenhaTextoEmStringGridEx(GradeA, Rect, clWhite,
      clBtnFace, taCenter, Texto, c, r, False,
      'Arial', 10, [fsBold], Char(0))
  end else
  //if (Col in ([0,1,2,4])) then
  if (Col in ([0,1,2,3])) then
    MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taCenter,
      Texto, c, r, False)
  else
  if (Col in ([4])) or (Row = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      Texto, c, r, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taRightJustify,
      Texto, c, r, False);
end;

procedure TFmCondGerImpGer2a.GradeAMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  GradeA.MouseToCell(X, Y, FColClicked, FRowClicked);
  dmkEdit5.ValueVariant := FColClicked;
  dmkEdit6.ValueVariant := FRowClicked;
  FXClicked := X;
  FYClicked := Y;
end;

procedure TFmCondGerImpGer2a.GradeAMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  CalculaLarguraFolha();
end;

procedure TFmCondGerImpGer2a.GradeASelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow = GradeA.RowCount -1 then
    CanSelect := False;
end;

procedure TFmCondGerImpGer2a.Larguradacolunaatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondGerImpGerLar, FmCondGerImpGerLar, afmoNegarComAviso) then
  begin
    FmCondGerImpGerLar.STLarg.Caption := IntToStr(GradeA.ColWidths[FColClicked]);
    FmCondGerImpGerLar.STCodi.Caption := GradeA.Cells[FColClicked, GradeA.RowCount - 1];
    FmCondGerImpGerLar.STNome.Caption := GradeA.Cells[FColClicked, Row0];
    FmCondGerImpGerLar.ShowModal;
    FmCondGerImpGerLar.Destroy;
  end;
end;

function TFmCondGerImpGer2a.ObtemFrxDataField(const Coluna: Integer;
var Campo, Somas: String; var HAlinha: TfrxHAlign; var Somar: Boolean): Boolean;
var
  Fmt, Texto: String;
  i, n: integer;
begin
  n     := 0;
  Fmt   := '';
  Somar := False;
  Somas := '';
  //
  if Coluna = Col1 then
  begin
    Texto := 'Boleto';
    HAlinha := haCenter;
  end
  else
  if Coluna = Col2 then
  begin
    Texto := 'TDC';
    HAlinha := haCenter;
  end
  else
  if Coluna = Col3 then
  begin
    Texto := 'UH';
    HAlinha := haCenter;
  end
  else
  if Coluna = Col4 then
  begin
    Texto := 'Propriet';
    HAlinha := haLeft;
  end
  else
  if Coluna = Col5 then
  begin
    Texto := 'Venc_TXT';
    HAlinha := haCenter;
    Somar := True;
    Fmt := '.';
  end
  else
  if Coluna = Col6 then
  begin
    Texto := 'Quit_TXT';
    HAlinha := haCenter;
    Somar := True;
    Fmt := '.';
  end
  else
  if Coluna = Col7 then
  begin
    Texto := 'QuitVal';
    HAlinha := haRight;
    //Somas := SSomasPG;
    Somar := True;
    Fmt := '.00';
    GradeB.Cells[Col7, Row2] := '2';
  end else
  if Coluna = Col8 then
  begin
    Texto := 'Observacao';
    HAlinha := haLeft;
  end
  else
  begin
    Texto := 'ValCol' +
      MLAGeral.FTX(Coluna - FColIniVal + 1, 3, 0, siPositivo);
    HAlinha := haRight;
    if GradeB.Cells[Coluna, Row2] = '' then Fmt := '' else
    begin
      n := Geral.IMV(GradeB.Cells[Coluna, Row2]);
      for i := 1 to n do
        Fmt := Fmt + '0';
      if Fmt <> '' then
        Fmt := '.' + Fmt;
    end;
    Somar := n > 0;
  end;
  if Somar = True then
  begin
    if Fmt <> '' then
    begin
      Somar := True;
      if Fmt = '.' then
      begin
        Campo := '[frxDsQuery."' + Texto + '"]';
        if Coluna = Col5 then
          Somas := '[FormatFloat(''#,###,##0;-#,###,##0; '',SUM(<frxDsQuery."Ativo">))]'
        else
        if Coluna = Col6 then
          Somas := '[FormatFloat(''#,###,##0;-#,###,##0; '',SUM(<frxDsQuery."QuitInt">))]'
        ;
      end else
      begin
        Campo := '[FormatFloat(''#,###,##0' + Fmt + ';-#,###,##0' + Fmt + '; '',<frxDsQuery."' + Texto + '">)]';
        Somas := '[FormatFloat(''#,###,##0' + Fmt + ';-#,###,##0' + Fmt + '; '',SUM(<frxDsQuery."' + Texto + '">))]';
      end;
    end else begin
      Campo := '[frxDsQuery."' + Texto + '"]';
      Somas := '[SUM(<frxDsQuery."' + Texto + '">)]';
    end;
  end else
    Campo := '[frxDsQuery."' + Texto + '"]';
  Result := True;
end;

function TFmCondGerImpGer2a.ObtemlinhaDoBoleto(SeqLn, Boleto: Double): Integer;
(*
var
  I: Integer;
  *)
begin
  //Result := 0; // for�ar a colocar no t�tulo para o erro ficar visual!
  //
{
  for I := 1 to GradeA.RowCount - 1 do
  begin
    if GradeA.Cells[Col1,I] = FormatFloat('0', Boleto) then
    begin
      Result := I;
      Exit;
    end;
  end;
}
{ N�o funciona! Linha est� Errada!}
  QrLin.Close;
  QrLin.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLin, DModG.MyPID_DB, [
  'SELECT Linha, SeqLn ',
  'FROM stringgrid1 ',
  'WHERE Linha=' + FormatFloat('0', SeqLn),
  '']);
  //
  Result := QrLinLinha.Value;
{}
end;

function TFmCondGerImpGer2a.ObtemSigla(EhLeitura: Boolean): Boolean;
var
  c: Integer;
begin
  if EhLeitura then
  begin
    DmBloq.QrListaL.Close;
    DmBloq.QrListaL.Open;
    c := Geral.IMV(GradeB.Cells[GradeA.Col, Row1]);
    if DmBloq.QrListaL.Locate('Codigo', c, []) then
      FSigla := DmBloq.QrListaLSIGLA_IMP.Value
    else FSigla := '';
  end else FSigla := GradeA.Cells[GradeA.Col, Row0];
  Result := InputQuery('Nova Sigla', 'Informe a nova sigla:', FSigla);
end;

procedure TFmCondGerImpGer2a.PMGradeAPopup(Sender: TObject);
var
  Coluna, TipoTab, TipoCad: Integer;
begin
  Coluna  := Geral.IMV(GradeA.Cells[GradeA.Col, GradeA.RowCount -1]);
  TipoTab := Geral.IMV(GradeB.Cells[Coluna, Row0]);
  TipoCad := FmCondGer.RGAgrupListaA.ItemIndex;
  FCodCad := Geral.IMV(GradeB.Cells[Coluna, Row1]);
  //
  Alterasiglanocadastrodacontaplanodecontas1.Enabled := False;
  Alterasiglanocadastrodaarrecadaobase1.Enabled      := False;
  AlterasiglanocadastrodaLeitura1.Enabled            := False;
  if FCodCad > 0 then
  begin
    case TipoTab of
      0:
      begin
        case TipoCad of
          1: Alterasiglanocadastrodaarrecadaobase1.Enabled      := True;
          2: Alterasiglanocadastrodacontaplanodecontas1.Enabled := True;
        end;
      end;
      1..2: AlterasiglanocadastrodaLeitura1.Enabled := True;
    end;
  end;
end;

procedure TFmCondGerImpGer2a.RecalculaAlturaLinhaTitulos;
var
  i, Tam: Integer;
begin
  Tam := GradeA.DefaultRowHeight;
  for i := 1 to GradeA.ColCount - 1 do
    DefineTamanhoTexto(GradeA.Cells[i, Row0], Tam);
  GradeA.RowHeights[0] := Tam;
end;

procedure TFmCondGerImpGer2a.RecalculaLarguraColunaTextos;
var
  i, j, k, n, a, Tam: Integer;
begin
  if not CkRecalcT.Checked then Exit;
  //
  if CkLargTit.Checked then n := 0 else n := 1;
  a := GradeA.RowCount -2;
  //
  for i := 1 to GradeB.ColCount - 1 do
  begin
    Tam := 0;
    k := Geral.IMV(GradeB.Cells[i, Row0]);
    if k = -2 then
    begin
      for j := n to a do
        DefineTamanhoTexto(GradeA.Cells[i, j], Tam);
      GradeA.ColWidths[i] := Tam;
    end;
  end;
end;

procedure TFmCondGerImpGer2a.RecriaQuery();
var
  I: Integer;
begin
  Query.Close;
  Query.Database := DModG.MyPID_DB;
  //
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE IF EXISTS StringGrid1; ');
  Query.SQL.Add('CREATE TABLE StringGrid1 (');
  Query.SQL.Add('  SeqLn       int(11)      ,');
  Query.SQL.Add('  Linha       int(11)      ,');
  Query.SQL.Add('  Ordem       int(11)      ,');
  Query.SQL.Add('  Andar       int(11)      ,');
  Query.SQL.Add('  imvConta    int(11)      ,');
  Query.SQL.Add('  Boleto      double(15,0) ,');
  Query.SQL.Add('  TDC         char(3)      ,');
  Query.SQL.Add('  UH          varchar(10)  ,');
  Query.SQL.Add('  Propriet    varchar(100) ,');
  Query.SQL.Add('  Vencim      date         ,');
  Query.SQL.Add('  Quitado     date         ,');
  Query.SQL.Add('  Venc_TXT    varchar(10)  ,');
  Query.SQL.Add('  Quit_TXT    varchar(10)  ,');
  Query.SQL.Add('  Quitou      char(3)      ,');
  Query.SQL.Add('  QuitInt     tinyint(1)   ,');
  Query.SQL.Add('  QuitVal     double(15,2) ,');
  Query.SQL.Add('  Observacao  varchar(255) ,');
  for I := FColIniVal to FColTotVal do
    Query.SQL.Add('  ValCol' +
      MLAGeral.FTX(I - FColIniVal+1, 3, 0, siPositivo) + ' float DEFAULT 0,');
  Query.SQL.Add('  Ativo       smallint      ');
  Query.SQL.Add(');');
end;

procedure TFmCondGerImpGer2a.RelatorioDeArrecadacoes_ListaDeCondominos(Mostra: Boolean);
  procedure GeraParteSQL(TabLct, EntiTXT, MezTxt: String);
  begin
    //QrPgBloq.SQL.Add('SELECT DISTINCT imv.Conta Apto, lan.FatNum, ');
    QrPgBloq.SQL.Add('SELECT imv.Conta Apto, lan.FatNum, ');
    QrPgBloq.SQL.Add('lan.Compensado Data, lan.Carteira, lan.Controle, ');
    QrPgBloq.SQL.Add('IF(lan.Compensado<2, "", DATE_FORMAT(lan.Compensado, ');
    QrPgBloq.SQL.Add('"%d/%m/%Y")) DATA_TXT, ');
    QrPgBloq.SQL.Add('IF(lan.Vencimento<2, "", DATE_FORMAT(lan.Vencimento, ');
    QrPgBloq.SQL.Add('"%d/%m/%Y")) VENCTO_TXT, Vencimento, Pago');
    QrPgBloq.SQL.Add('FROM ' + TabLct + ' lan');
    QrPgBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrPgBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov imv ON imv.Conta=lan.Depto');
    QrPgBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas con ON con.Codigo=lan.Genero');
    QrPgBloq.SQL.Add('WHERE car.Tipo in (0,2)');
    QrPgBloq.SQL.Add('AND lan.FatID in (600,601)');
    QrPgBloq.SQL.Add('AND car.ForneceI=' + EntiTXT);
    QrPgBloq.SQL.Add('AND lan.Mez=' + MezTXT);
  end;
var
  OrdRow,
  Linha, Col, Lin, Idx, l, j, Tam02, Tam01, c1, c2(*, tf*): Integer;
  Valor, Somas, Leitu: Double;
  TxtCell, EntiTxt, MezTxt: String;
  Achou: Boolean;
  //
  imvConta, Ordem, Andar, QuitInt, Ativo, Tam00: Integer;
  Quitou, TDC, UH, Propriet, Venc_TXT, Quit_TXT, Pago: String;
  Vencim, Quitado: TDateTime;
  Boleto, QuitVal, TotVal: Double;
begin
  if not FCalcula then
  begin
    if Mostra then
      Show;
    Exit;
  end;
  //
  Achou := False;
  Screen.Cursor := crHourGlass;
  try
    {
    FAptTot := 0;
    FAptPag := 0;
    }
    //
    Canvas.Font.Name := 'Univers Light Condensed';
    Canvas.Font.Size := EdTamFonte.ValueVariant;
    //
    // deve ser antes para n�o acumular
    // deve ser todas colunas pois aptos podem ser inativados
    MyObjects.LimpaGrade(GradeA, 0, 0, True);
    GradeA.ColCount := FColIniVal;
    for Col := 0 to FColIniVal - 1 do
    begin
      TxtCell := '';
      if Col = Col0 then TxtCell := 'ID ' + DModG.ReCaptionTexto(VAR_U_H)
      else if Col = Col1 then TxtCell := 'Bloqueto'
      else if Col = Col2 then TxtCell := 'TDC*' // Tipo de documento de cobran�a
      else if Col = Col3 then TxtCell := DModG.ReCaptionTexto(VAR_U_H)
      else if Col = Col4 then TxtCell := DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O)
      else if Col = Col5 then TxtCell := 'Vencimento'
      else if Col = Col6 then TxtCell := 'Quita��o'
      else if Col = Col7 then TxtCell := '$ Pago'
      else if Col = Col8 then TxtCell := 'Observa��o';
      GradeA.Cells[Col, Row0] := TxtCell;
      if Col = Col7 then
        TxtCell := '0'
      else
      if Col = Col0 then
        TxtCell := '-3'
      else
        TxtCell := '-2';
      GradeB.Cells[Col, Row0] := TxtCell;
    end;

    // L I N H A S

    // UHs
    EntiTXT := FormatFloat('0', DmCond.QrCondCliente.Value);
    MezTXT  := FormatFloat('0', MLAGeral.PeriodoToMez(FmCondGer.QrPrevPeriodo.Value));

    QrPgBloq.Close;
    QrPgBloq.Database := DModG.MyPID_DB;
    QrPgBloq.SQL.Clear;
    //
    QrPgBloq.SQL.Add('DROP TABLE IF EXISTS _COND_GER_IMP_GER_2_;');
    QrPgBloq.SQL.Add('CREATE TABLE _COND_GER_IMP_GER_2_');
    QrPgBloq.SQL.Add('');
    GeraParteSQL(DmCond.FTabLctA, EntiTXT, MezTxt);
    QrPgBloq.SQL.Add('UNION');
    GeraParteSQL(DmCond.FTabLctB, EntiTXT, MezTxt);
    QrPgBloq.SQL.Add('UNION');
    GeraParteSQL(DmCond.FTabLctD, EntiTXT, MezTxt);
    QrPgBloq.SQL.Add(';');
    QrPgBloq.SQL.Add('');
    //QrPgBloq.SQL.Add('SELECT * FROM _COND_GER_IMP_GER_2_');
    QrPgBloq.SQL.Add('SELECT Apto, FatNum, Data, Carteira,  DATA_TXT, ');
    QrPgBloq.SQL.Add('VENCTO_TXT, Vencimento, Controle, SUM(Pago) Pago ');
    QrPgBloq.SQL.Add('FROM _cond_ger_imp_ger_2_ ');
    QrPgBloq.SQL.Add('GROUP BY Apto, FatNum');
    //
    QrPgBloq.SQL.Add('ORDER BY Data, Carteira, Controle;');
    QrPgBloq.SQL.Add('');
    QrPgBloq.SQL.Add('DROP TABLE IF EXISTS _COND_GER_IMP_GER_2_;');
    QrPgBloq.SQL.Add('');
    QrPgBloq.Open;
    //  QrPgBloq > QrAptos  (ligados pelo apto por lookup)
    QrAptos.Close;
    QrAptos.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
    QrAptos.Open;
    {
    GradeA.RowCount := QrAptos.RecordCount + 1;
    QrAptos.First;
    while not QrAptos.Eof do
    }
    QrBol3.Close;
    QrBol3.SQL.Clear;
    QrBol3.SQL.Add('SELECT DISTINCT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar,');
    QrBol3.SQL.Add('ari.Boleto, ari.Apto,');
    QrBol3.SQL.Add('ari.Propriet, ari.Vencto, cdi.Unidade,');
    QrBol3.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QrBol3.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
    QrBol3.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO, 0 KGT');
    QrBol3.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
    QrBol3.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
    QrBol3.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
    QrBol3.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
    QrBol3.SQL.Add('WHERE ari.Codigo=:P0');
    QrBol3.SQL.Add('AND ari.Boleto <> 0');
    QrBol3.SQL.Add('');
    QrBol3.SQL.Add('UNION');
    QrBol3.SQL.Add('');
    QrBol3.SQL.Add('SELECT DISTINCT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, cni.Boleto, cni.Apto,');
    QrBol3.SQL.Add('cni.Propriet, cni.Vencto, cdi.Unidade,');
    QrBol3.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QrBol3.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
    QrBol3.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO, 0 KGT');
    QrBol3.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
    QrBol3.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
    QrBol3.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
    QrBol3.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
    QrBol3.SQL.Add('WHERE cni.Cond=:P1');
    QrBol3.SQL.Add('AND cni.Periodo=:P2');
    QrBol3.SQL.Add('AND cni.Boleto <> 0');
    QrBol3.SQL.Add('');
    QrBol3.Params := DmBloq.QrBoletos.Params;
    QrBol3.Open;

    GradeA.RowCount := QrBol3.RecordCount + 1;
    QrBol3.First;
    FColTotVal := FColIniVal + 1; // 1 > Totais de arrecada��es + leituras
    //
    DmBloq.ReopenListaA(FmCondGer.RGGerado.ItemIndex = 1);
    DmBloq.QrListaA.First;
    while not DmBloq.QrListaA.Eof do
    begin
      if DmBloq.QrListaAInfoRelArr.Value = 1 then
        FColTotVal := FColTotVal + 2
      else
        FColTotVal := FColTotVal + 1;
      //
      DmBloq.QrListaA.Next;
    end;
    DmBloq.QrListaA.First;
    //
    DmBloq.ReopenListaL(FmCondGer.RGGerado.ItemIndex = 1);
    FColTotVal := FColTotVal + DmBloq.QrListaL.RecordCount * 2;

    //

    RecriaQuery();
    Query.ExecSQL;
    //
    TotVal := 0;
    while not QrBol3.Eof do
    begin
      Linha := QrBol3.RecNo;
      if QrAptos.Locate('Conta', QrBol3Apto.Value, []) then
      begin
        {
        FAptTot := FAptTot + 1;
        if QrAptosDATAPG_TXT.Value <> '' then
          FAptPag := FAptPag + 1;
        }
        //
        Ordem := QrBol3Ordem.Value;
        Andar := QrBol3Andar.Value;
        imvConta := QrAptosConta.Value;
        Boleto := QrBol3Boleto.Value;
        TDC := QrAptosTDC.Value;
        UH := QrAptosUnidade.Value;
        Propriet := QrAptosNOMEPROP.Value;
        Vencim := QrAptosVencimento.Value;
        Quitado := QrAptosData.Value;
        QuitVal := QrAptosPago.Value;
        TotVal := TotVal + QuitVal;
        Venc_TXT := QrAptosVENCTO_TXT.Value;
        Quit_TXT := QrAptosDATAPG_TXT.Value;
        if (QuitVal > 0) and (Quit_TXT <> '') then
        begin
          QuitInt := 1;
          Quitou := 'SIM'
        end else begin
          QuitInt := 0;
          Quitou := 'N�O';
        end;
        Ativo := 1;
        //
        UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'stringgrid1', False, [
        'Linha', 'Ordem', 'Andar',
        'imvConta', 'Boleto', 'TDC',
        'UH', 'Propriet', 'Vencim',
        'Quitado', 'Venc_TXT', 'Quit_TXT',
        'Quitou', 'QuitInt', 'QuitVal',
        'Ativo'], [
        ], [
        Linha, Ordem, Andar,
        imvConta, Boleto, TDC,
        UH, Propriet, Vencim,
        Quitado, Venc_TXT, Quit_TXT,
        Quitou, QuitInt, QuitVal,
        Ativo], [
        ], False);
        //
        QrBol3.Next;
      end else begin
        Geral.MensagemBox('Unidade n�o localizada: C�digo = ' +
        FormatFloat('0', QrBol3Apto.Value) + '. AVISE A DERMATEK',
        'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
    end;
    DefineTamanhoTexto(Geral.FFT(TotVal, 2, siNegativo), Tam00);
    GradeA.ColWidths[Col7] := Tam00;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
    'SELECT * FROM stringgrid1',
    'ORDER BY ' +
      OrdemPor_SQL[RGOrdem1.ItemIndex] + Descende(CkDescen1) + ', ' +
      OrdemPor_SQL[RGOrdem2.ItemIndex] + Descende(CkDescen2) + ', ' +
      OrdemPor_SQL[RGOrdem3.ItemIndex] + Descende(CkDescen3) + ', ' +
      OrdemPor_SQL[RGOrdem4.ItemIndex] + Descende(CkDescen4),
      '']);
    Query.First;
    while not Query.Eof do
    begin
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('UPDATE stringgrid1');
      DModG.QrUpdPID1.SQL.Add('SET SeqLn = ' + FormatFloat('0', Query.RecNo));
      DModG.QrUpdPID1.SQL.Add('WHERE Linha=' + FormatFloat('0', Query.FieldByName('Linha').AsInteger));
      DModG.QrUpdPID1.ExecSQL;
      //
      Query.Next;
    end;
    //
    Query.First;
    while not Query.Eof do
    begin
      Lin := Query.RecNo;
      if QrAptos.Locate('Conta', Query.FieldByName('ImvConta').AsInteger, []) then
      begin
        if QrAptosPAGO.Value < 0.01 then
          Pago := ''
        else
          Pago := Geral.FFT(QrAptosPAGO.Value, 2, siNegativo);
        //
        GradeA.Cells[Col0,Lin] := MLAGeral.FTX(QrAptosConta.Value, 6,0, siNegativo);
        GradeA.Cells[Col1,Lin] := FormatFloat('0', Query.FieldByName('Boleto').AsFloat);
        GradeA.Cells[Col2,Lin] := QrAptosTDC.Value;
        GradeA.Cells[Col3,Lin] := QrAptosUnidade.Value;
        GradeA.Cells[Col4,Lin] := QrAptosNOMEPROP.Value;
        GradeA.Cells[Col5,Lin] := QrAptosVENCTO_TXT.Value;
        GradeA.Cells[Col6,Lin] := QrAptosDATAPG_TXT.Value;
        GradeA.Cells[Col7,Lin] := Pago;
        GradeA.Cells[Col8,Lin] := '';
        //
        //QrAptos.Next;
        Query.Next;
      end else begin
        Geral.MensagemBox('Unidade n�o localizada: C�digo = ' +
        FormatFloat('0', DmBloq.QrListaAConta.Value) + '. AVISE A DERMATEK',
        'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
    end;
    //

    // C O L U N A S

    // 1/2 colunas: Arrecada��es
    // J� foi aberto acima
    //DmBloq.ReopenListaA(FmCondGer.RGGerado.ItemIndex = 1);
    DmBloq.QrListaA.First;
    while not DmBloq.QrListaA.Eof do
    begin
      //GradeA.Cells[01,Lin] := MLAGeral.FTX(QrAptosAndar.Value, 3,0, siNegativo);
      Tam01 := 16;
      if DmBloq.QrListaAInfoRelArr.Value = 1 then
      begin
        GradeA.ColCount := GradeA.ColCount + 2;
        c1 := GradeA.ColCount -2;
        c2 := GradeA.ColCount -1;
        GradeB.ColCount := c2;
        // Informar que � cota de arrecada��o
        GradeB.Cells[c2, Row0] := '3';
        // Informar qual � o codigo do cadastro cfe selecionado em FmCondger.RGAgrupListaA
        case FmCondger.RGAgrupListaA.ItemIndex of
          0: GradeB.Cells[c2, Row1] := '0';                                         // ArreaBaI
          1: GradeB.Cells[c2, Row1] := dmkPF.FFP(DmBloq.QrListaACodigo.Value, 0); // ArreBaC
          2: GradeB.Cells[c2, Row1] := dmkPF.FFP(DmBloq.QrListaAConta.Value, 0);  // Conta (plano de contas)
          else GradeB.Cells[c2, Row1] := '-1';
        end;
        //T�tulo
        GradeA.Cells[c2, Row0] := DmBloq.QrListaATITULO_FATOR.Value;
        // Informar a quantidade de casas decimais em n�meros flutuantes ('' = sem formata��o)
        GradeB.Cells[c2, Row2] := '';
      end else begin
        GradeA.ColCount := GradeA.ColCount + 1;
        c1 := GradeA.ColCount -1;
        c2 := -1;
        GradeB.ColCount := c1;
      end;
      GradeA.Cells[c1, Row0] := DmBloq.QrListaASIGLA_IMP.Value;
      //
      // Informar que � arrecada��o
      GradeB.Cells[c1, Row0] := '0';
      // Informar qual � o codigo do cadastro cfe selecionado em FmCondger.RGAgrupListaA
      case FmCondger.RGAgrupListaA.ItemIndex of
        0: GradeB.Cells[c1, Row1] := '0';                                           // ArreaBaI
        1: GradeB.Cells[c1, Row1] := dmkPF.FFP(DmBloq.QrListaACodigo.Value, 0);   // ArreBaC
        2: GradeB.Cells[c1, Row1] := dmkPF.FFP(DmBloq.QrListaAConta.Value, 0);    // Conta (plano de contas)
        else GradeB.Cells[c1, Row1] := '-1';
      end;
      //
      // Informar a quantidade de casas decimais em n�meros flutuantes
      GradeB.Cells[c1, Row2] := '2';

      DmBloq.QrListaAIts.First;
      Somas := 0;
      Leitu := 0;
      while not DmBloq.QrListaAIts.Eof do
      begin
        Achou := False;
        for Idx := 1 to GradeA.RowCount - 1 do
        begin
          if (Geral.IMV(GradeA.Cells[Col0, Idx]) = DmBloq.QrListaAItsApto.Value) and
          (Geral.IMV(GradeA.Cells[Col1, Idx]) = DmBloq.QrListaAItsBoleto.Value) then
          begin
            OrdRow := ObtemlinhaDoBoleto(Idx, DmBloq.QrListaAItsBoleto.Value);
            Achou := True;
            Valor := Geral.DMV(GradeA.Cells[c1, OrdRow]);
            Somas := Somas + DmBloq.QrListaAItsValor.Value;
            Valor := Valor + DmBloq.QrListaAItsValor.Value;
            TxtCell := Geral.FFT(Valor, 2, siNegativo);
            // Fazer!
            GradeA.Cells[c1, OrdRow] := TxtCell;
            AtualizaQuery(c1, OrdRow, TxtCell);
            DefineTamanhoTexto(TxtCell, Tam01);
            //
            if c2 > -1 then
            begin
              if DmBloq.QrListaAItsFATOR_COBRADO.Value > 0 then
              begin
                Leitu := Leitu + DmBloq.QrListaAItsFATOR_COBRADO.Value;
                TxtCell := FloatToStr(DmBloq.QrListaAItsFATOR_COBRADO.Value);
                // Fazer!
                GradeA.Cells[c2, OrdRow] := TxtCell;
                AtualizaQuery(c2, OrdRow, TxtCell);
                DefineTamanhoTexto(TxtCell, Tam02);
              end;
            end;
            //
            Break;
          end;
        end;
        //
        DmBloq.QrListaAIts.Next;
      end;
      if not Achou then
      begin
        Geral.MensagemBox('O bloqueto ' + FormatFloat('0',
        DmBloq.QrListaAItsBoleto.Value) + ' n�o foi localizado!'#13#10 +
        DmBloq.QrListaATEXTO_IMP.Value + #13#10 + #13#10 + 'AVISE A DERMATEK',
        'Erro', MB_OK+MB_ICONERROR);
        Screen.Cursor := crDefault;
        Exit;
      end;
      TxtCell := Geral.FFT(Somas, 2, siNegativo);
      DefineTamanhoTexto(TxtCell, Tam01);
      GradeA.ColWidths[c1] := Tam01;
      //
      if c2 > -1 then
      begin
        TxtCell := Geral.FFT(Leitu, 2, siNegativo);
        DefineTamanhoTexto(TxtCell, Tam02);
        if Tam02 > 160 then
          Tam02 := 160;
        GradeA.ColWidths[c2] := Tam02;
      end;  
      //
      DmBloq.QrListaA.Next;
    end;
    //  Fim arrecada��es

    // 2/2 colunas: Leituras
    // J� foi aberto acima
    //DmBloq.ReopenListaL(FmCondGer.RGGerado.ItemIndex = 1);
    DmBloq.QrListaL.First;
    while not DmBloq.QrListaL.Eof do
    begin
      Tam02 := 16;
      Tam01 := 16;
      GradeA.ColCount := GradeA.ColCount + 2;
      c1 := GradeA.ColCount - 1;
      c2 := GradeA.ColCount - 2;
      GradeA.Cells[c2, Row0] := DmBloq.QrListaLUnidLei.Value + ' ' + DmBloq.QrListaLSIGLA_IMP.Value;
      GradeA.Cells[c1, Row0] := Dmod.QrControleMoeda.Value + ' ' + DmBloq.QrListaLSIGLA_IMP.Value;
      GradeA.ColWidths[c1] := 72;
      GradeA.ColWidths[c2] := 72;
      //

      //
      GradeB.ColCount := GradeA.ColCount;
      // Informar que � a medi��o de uma leitura
      GradeB.Cells[c2, Row0] := '1';
      // Informar qual � o codigo de cadastro da leitura
      GradeB.Cells[c2, Row1] := dmkPF.FFP(DmBloq.QrListaLCodigo.Value, 0);
      // Informar a quantidade de casas decimais em n�meros flutuantes
      GradeB.Cells[c2, Row2] := dmkPF.FFP(DmBloq.QrListaLCasas.Value, 0);
      //

      // Informar que � o valor (R$) de uma leitura
      GradeB.Cells[c1, Row0] := '2';
      // Informar qual � o codigo de cadastro da leitura
      GradeB.Cells[c1, Row1] := dmkPF.FFP(DmBloq.QrListaLCodigo.Value, 0);
      // Informar a quantidade de casas decimais em n�meros flutuantes
      GradeB.Cells[c1, Row2] := '2';
      //

      Somas := 0;
      Leitu := 0;
      DmBloq.QrListaLIts.First;
      while not DmBloq.QrListaLIts.Eof do
      begin
        Achou := False;
        for Idx := 1 to GradeA.RowCount - 1 do
        begin
          if (Geral.IMV(GradeA.Cells[Col0, Idx]) = DmBloq.QrListaLItsApto.Value) and
          (Geral.IMV(GradeA.Cells[Col1, Idx]) = DmBloq.QrListaLItsBoleto.Value) then
          begin
            OrdRow := ObtemlinhaDoBoleto(Idx, DmBloq.QrListaAItsBoleto.Value);
            Achou := True;
            Valor := Geral.DMV(GradeA.Cells[c2, OrdRow]);
            Leitu := Leitu + DmBloq.QrListaLItsConsumo.Value;
            Valor := Valor + DmBloq.QrListaLItsConsumo.Value;
            TxtCell := Geral.FFT(Valor, DmBloq.QrListaLCasas.Value, siNegativo);
            // Fazer!
            GradeA.Cells[c2, OrdRow] := TxtCell;
            AtualizaQuery(c2, OrdRow, TxtCell);
            DefineTamanhoTexto(TxtCell, Tam02);
            //
            Valor := Geral.DMV(GradeA.Cells[c1, OrdRow]);
            Valor := Valor + DmBloq.QrListaLItsValor.Value;
            Somas := Somas + DmBloq.QrListaLItsValor.Value;
            TxtCell := Geral.FFT(Valor, 2, siNegativo);
            // Fazer!
            GradeA.Cells[c1, OrdRow] := TxtCell;
            AtualizaQuery(c1, OrdRow, TxtCell);
            DefineTamanhoTexto(TxtCell, Tam01);
            //
            Break;
          end;
        end;
        if not Achou then
        begin
          Geral.MensagemBox('O bloqueto ' + FormatFloat('0',
          DmBloq.QrListaAItsBoleto.Value) + ' n�o foi localizado!'#13#10 +
          DmBloq.QrListaATEXTO_IMP.Value + #13#10 + #13#10 + 'AVISE A DERMATEK',
          'Erro', MB_OK+MB_ICONERROR);
          Screen.Cursor := crDefault;
          Exit;
        end;
        //
        DmBloq.QrListaLIts.Next;
      end;
      //
      TxtCell := Geral.FFT(Leitu, 2, siNegativo);
      DefineTamanhoTexto(TxtCell, Tam02);
      if Tam02 > 160 then
        Tam02 := 160;
      GradeA.ColWidths[c2] := Tam02;
      //
      TxtCell := Geral.FFT(Somas, 2, siNegativo);
      DefineTamanhoTexto(TxtCell, Tam01);
      if Tam01 > 160 then
        Tam01 := 160;
      GradeA.ColWidths[c1] := Tam01;
      //
      DmBloq.QrListaL.Next;
    end;
    // Fim leituras


    // Totais de arrecada��es + leituras
    GradeA.ColCount := GradeA.ColCount + 1;
    c1 := GradeA.ColCount -1;
    GradeA.Cells[c1, Row0] := 'TOTAL';
    GradeB.ColCount := GradeA.ColCount;
    GradeB.Cells[c1, Row0] := '-1';
    GradeB.Cells[c1, Row1] := '';
    GradeB.Cells[c1, Row2] := '2';
    Tam01 := 16;
    c1 := GradeA.ColCount - 1;
    Somas := 0;
    for Idx := 1 to GradeA.RowCount - 1 do
    begin
      Valor := 0;
      for j := FColIniVal to GradeA.ColCount - 2 do
        if Geral.IMV(GradeB.Cells[j, Row0]) in ([0,2]) then
        begin
          Valor := Valor + Geral.DMV(GradeA.Cells[j, Idx]);
        end;
      Somas := Somas + Valor;
      TxtCell := Geral.FFT(Valor, 2, siNegativo);
      // Fazer!
      GradeA.Cells[c1, Idx] := TxtCell;
      AtualizaQuery(c1, Idx, TxtCell);
      DefineTamanhoTexto(TxtCell, Tam01);
    end;
    TxtCell := Geral.FFT(Somas, 2, siNegativo);
    DefineTamanhoTexto(TxtCell, Tam01);
    GradeA.ColWidths[c1] := Tam01;
    //
    // Numerar colunas na �ltima linha para saber qual coluna � ao mov�-la.
    //Valor := 0;
    GradeA.RowCount := GradeA.RowCount + 1;
    for Col := 0 to GradeA.ColCount - 1 do
      GradeA.Cells[Col, GradeA.RowCount - 1] := dmkPF.FFP(Col, 0);
    //
    CalculaLarguraFolha();
    //
    for Col := 0 to GradeB.ColCount - 1 do
      GradeB.ColWidths[Col] := GradeA.ColWidths[Col];
    //
    RecalculaLarguraColunaTextos;
    RecalculaAlturaLinhaTitulos;
    //
    Screen.Cursor := crDefault;
    if Mostra then Show;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGerImpGer2a.RGOrdem1Click(Sender: TObject);
begin
  RelatorioDeArrecadacoes_ListaDeCondominos(False);
end;

procedure TFmCondGerImpGer2a.ImprimeGrade2();
var
  Page: TfrxReportPage;
  Head: TfrxPageHeader;
  Shape: TfrxShapeView;
  Memo: TfrxMemoView;
  Line01: TfrxLineView;
  Band: TfrxMasterData;
  Summ: TfrxReportSummary;
  GH: TfrxGroupHeader;
  GF: TfrxGroupFooter;
  L, T, W, H, Z, M, X, Y, (*U,*) Posicao, TamFix(*, Tam006, Tam007*): Integer;
  i, c(*, n*): Integer;
  Campo, Somas, Titulo: String;
  AlinhaH: TfrxHAlign;
  Somar: Boolean;
begin
  // Configura frxReport
  frxGrade2.DataSets.Add(frxDsQuery);
  //frxGrade2.DataSets.Items[0] := frxDsQuery;
  //
  // Configura p�gina
  while frxGrade2.PagesCount > 0 do
    frxGrade2.Pages[0].Free;
  Page := TfrxReportPage.Create(frxGrade2);
  Page.CreateUniqueName;
  Page.Height       := 210;
  if EdLarguraFolha.ValueVariant > 680 then
  begin
    Page.Orientation  := poLandscape;
    X := 7700;                 // 2010-07-02 -> Original = 8700
    Page.LeftMargin   :=  20;  // 2010-07-02 -> Original = 10
    Page.RightMargin  :=  10;
    Page.TopMargin    :=  15;
    Page.BottomMargin :=  15;
  end else begin
    Page.Orientation  := poPortrait;
    X := 0;
    Page.LeftMargin   :=  15;
    Page.RightMargin  :=  15;
    Page.TopMargin    :=  10;
    Page.BottomMargin :=  10;
  end;
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L :=  Round(     0      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(  5000      / VAR_frCM);  // 2010-07-02 -> Original = 4500
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  ///  Shape
  L :=  Round(     0      / VAR_frCM);
  T :=  Round(     0      / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(  1000      / VAR_frCM);
  Shape := TfrxShapeView.Create(Head);
  Shape.CreateUniqueName;
  Shape.Visible := True;
  Shape.SetBounds(L, T, W, H);
  Shape.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  Shape.Frame.Width := 0.1;
  Shape.Shape := skRoundRectangle;
  //
  // Nome administradora
  L :=  Round(   200      / VAR_frCM);
  T :=  Round(     0      / VAR_frCM);
  W :=  Round((17600 + X) / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := Dmod.QrDonoNOMEDONO.Value;
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  //
  // Data, hora impress�o
  L :=  Round(   200      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round(  3800      / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := 'Impresso em [Date], [Time]';
  Memo.VAlign      := vaCenter;
  //
  // Nome Relat�rio
  L :=  Round(  4000      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round((10000 + X) / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := 'RELAT�RIO DE ARRECADA��ES';
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  //
  // P�gina ? de ?
  L :=  Round((14000 + X) / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round(  3800      / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := 'P�gina [Page] de [TotalPages]';
  Memo.HAlign      := haRight;
  Memo.VAlign      := vaCenter;
  //
  // Linha divis�ria shape
  L :=  Round(     0      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(     0      / VAR_frCM);
  Line01 := TfrxLineView.Create(Head);
  Line01.CreateUniqueName;
  Line01.SetBounds(L, T, W, H);
  Line01.Visible     := True;
  Line01.Frame.Width := 0.1;
  //
  // Nome Condom�nio
  L :=  Round(    0       / VAR_frCM);
  T :=  Round( 1200       / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(  500       / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := DmCond.QrCondNOMECLI.Value;
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  //
  // Per�odo do relat�rio (M�s e ano)
  L :=  Round(     0       / VAR_frCM);
  T :=  Round(  1800       / VAR_frCM);
  W :=  Round((18000 + X)  / VAR_frCM);
  H :=  Round(   500       / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 10;
  Memo.Memo.Text   := FmCondGer.QrPrevPERIODO_TXT.Value;
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  Memo.Frame.Width := 0.1;

  Posicao := Trunc(Head.Top + Head.Height + 1);
  H :=  GradeA.DefaultRowHeight;
  // GroupHeader 1
  if RGAgrup.ItemIndex > 0 then
  begin
    T := Posicao;
    GH := TfrxGroupHeader.Create(Page);
    GH.CreateUniqueName;
    GH.SetBounds(0, T, 0, H);
    GH.Condition := 'frxDsQuery."' + OrdemPor_frx[RGOrdem1.ItemIndex] + '"';
    //
    L :=  Round(     0       / VAR_frCM);
    //T :=  Round(  1800       / VAR_frCM);
    // desmarcar? Z :=  Round( 2500 / VAR_frCM);
    W :=  Round((18000 + X)  / VAR_frCM);
    //H :=  Round(   500       / VAR_frCM);
    Memo := TfrxMemoView.Create(GH);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, 0, W, H);
    Memo.Visible     := True;
    Memo.Font.Name   := 'Arial';
    Memo.Font.Size   := 10;
    Memo.Memo.Text   := OrdemPor_Tit[RGOrdem1.ItemIndex] + '[frxDsQuery."' + OrdemPor_frx[RGOrdem1.ItemIndex] + '"]';
    Memo.Font.Style  := [fsBold];
{
    Memo.HAlign      := haCenter;
    Memo.VAlign      := vaCenter;
    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
}
    //
    Posicao := Trunc(GH.Top + GH.Height + 1);
  end;

  // GroupHeader 2
  if RGAgrup.ItemIndex > 1 then
  begin
    T := Posicao;
    GH := TfrxGroupHeader.Create(Page);
    GH.CreateUniqueName;
    GH.SetBounds(0, T, 0, H);
    GH.Condition := 'frxDsQuery."' + OrdemPor_frx[RGOrdem2.ItemIndex] + '"';
    //
    L :=  Round(     0       / VAR_frCM);
    //T :=  Round(  1800       / VAR_frCM);
    // desmarcar? Z :=  Round( 2500 / VAR_frCM);
    W :=  Round((18000 + X)  / VAR_frCM);
    //H :=  Round(   500       / VAR_frCM);
    Memo := TfrxMemoView.Create(GH);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, 0, W, H);
    Memo.Visible     := True;
    Memo.Font.Name   := 'Arial';
    Memo.Font.Size   := 10;
    Memo.Memo.Text   := OrdemPor_Tit[RGOrdem2.ItemIndex] + '[frxDsQuery."' + OrdemPor_frx[RGOrdem2.ItemIndex] + '"]';
    Memo.Font.Style  := [fsBold];
{
    Memo.HAlign      := haCenter;
    Memo.VAlign      := vaCenter;
    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
}
    //
    Posicao := Trunc(GH.Top + GH.Height + 1);
  end;

  // Master data para linhas de dados
  //H :=  GradeA.DefaultRowHeight;
  T := Posicao;
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(0, T, 0, H);
  Band.DataSet     := frxDsQuery;
  //
  Posicao := Trunc(Band.Top + Band.Height + 1);
  // Linha de totais dos valores
  Y := H + Round(200/VAR_frCM);
  Summ := TfrxReportSummary.Create(Page);
  Summ.CreateUniqueName;
  Summ.SetBounds(0, 0, 0, Y);
  //
  // Dados do relat�rio
  //H := H;
  L := 0;
  T := 0;
  W := 0;
  H :=  GradeA.DefaultRowHeight;
  //
  for I := 1 to GradeA.ColCount -1 do
  begin
    C := Geral.IMV(GradeA.Cells[i, GradeA.RowCount -1]);

    // Dados das colunas
    ObtemFrxDataField(C, Campo, Somas, AlinhaH, Somar);
    //
    if GradeA.ColWidths[i] < 5 then Campo := '';
    L :=  L + W;
    W :=  GradeA.ColWidths[i];
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, T, W, H);
    Memo.Visible     := True;
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := EdTamFonte.ValueVariant;
    Memo.Memo.Text   := Campo;
    Memo.HAlign      := AlinhaH;
    Memo.VAlign      := vaCenter;
    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
    Memo.WordWrap    := False;
    Memo.Rotation    := 0;

    // T�tulos das colunas
    Z :=  Round( 2500 / VAR_frCM);
    M :=  Round( 2500 / VAR_frCM);  // 2010-07-02 -> Original = 2000
    Memo := TfrxMemoView.Create(Head);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, Z, W, M);
    Memo.Visible     := True;
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := EdTamFonte.ValueVariant;
    if GradeA.ColWidths[i] < 5 then
      Memo.Memo.Text   := ''
    else
      Memo.Memo.Text   := GradeA.Cells[i, Row0];

    // 2010-07-02 - Exporta��o para PDF incorreta
    { original:
    Memo.HAlign      := haLeft;
    Memo.VAlign      := vaCenter;
    } // Novo
    Memo.HAlign      := haCenter;
    Memo.VAlign      := vaCenter;
    // fim 2010-07-02

    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
    Memo.WordWrap    := False;
    Memo.Rotation    := 90;
    //
  end;
  //

  AdicionaMemos(Summ, 250, 0, 'TOTAIS: ', TamFix);
  //
  // GroupFooter 2
  H :=  GradeA.DefaultRowHeight;
  if RGAgrup.ItemIndex > 1 then
  begin
    T := Posicao;
    GF := TfrxGroupFooter.Create(Page);
    GF.CreateUniqueName;
    GF.SetBounds(0, T, 0, H);
    //
    // desmarcar? L :=  Round(     0       / VAR_frCM);
    //T :=  Round(  1800       / VAR_frCM);
    // desmarcar? Z :=  Round( 2500 / VAR_frCM);
    // desmarcar? W :=  TamFix;//Round((18000 + X)  / VAR_frCM);
    //H :=  Round(   500       / VAR_frCM);

    Titulo := 'Total de ' + OrdemPor_Tit[RGOrdem2.ItemIndex] +
      '[frxDsQuery."' + OrdemPor_frx[RGOrdem2.ItemIndex] + '"]';
    AdicionaMemos(GF, 0, 0, Titulo, TamFix);

    //
    Posicao := Trunc(GF.Top + GF.Height + 1);
  end;

  // GroupFooter 1
  if RGAgrup.ItemIndex > 0 then
  begin
    T := Posicao;
    GF := TfrxGroupFooter.Create(Page);
    GF.CreateUniqueName;
    GF.SetBounds(0, T, 0, H);
    //
    // desmarcar? L :=  Round(     0       / VAR_frCM);
    //T :=  Round(  1800       / VAR_frCM);
    // desmarcar? Z :=  Round( 2500 / VAR_frCM);
    // desmarcar? W :=  TamFix;//Round((18000 + X)  / VAR_frCM);
    //H :=  Round(   500       / VAR_frCM);

    Titulo := 'Total de ' + OrdemPor_Tit[RGOrdem1.ItemIndex] +
      '[frxDsQuery."' + OrdemPor_frx[RGOrdem1.ItemIndex] + '"]';
    AdicionaMemos(GF, 0, 0, Titulo, TamFix);

    //
    // desmarcar? Posicao := Trunc(GF.Top + GF.Height + 1);
  end;

  //

  ReacertaFrxDataSets();
  if CkDesign.Checked then
    frxGrade2.DesignReport
  else
    MyObjects.frxMostra(frxGrade2, 'Relat�rio de arrecada��es');
end;

function TFmCondGerImpGer2a.Define_frxCond(frx: TfrxReport): TfrxReport;
begin
  ReacertaFrxDataSets();
  //frx.EnabledDataSets.Add(                 frxDsBoletos);
  //frx.EnabledDataSets.Add(                 frxDsBoletosIts);
  frx.EnabledDataSets.Add(FmCondGer.       frxDsCarts);
  //frx.EnabledDataSets.Add(                 frxDsCond);
  //frx.EnabledDataSets.Add(                 frxDsConfigBol);
  frx.EnabledDataSets.Add(DMod.            frxDsDono);
  frx.EnabledDataSets.Add(DMod.            frxDsEndereco);
  //frx.EnabledDataSets.Add(                 frxDsInquilino);
  frx.EnabledDataSets.Add(DMod.            frxDsMaster);
  //frx.EnabledDataSets.Add(                 frxDsMov);
  //frx.EnabledDataSets.Add(                 frxDsMov2);
  //frx.EnabledDataSets.Add(FmCondGer.       frxDsPrev Its);
  //
  Result := frx;
end;

function TFmCondGerImpGer2a.Descende(Ck: TCheckBox): String;
begin
  if (Ck = nil) or (Ck.Checked = False) then
    Result := ''
  else
    Result := ' DESC';
end;

procedure TFmCondGerImpGer2a.ReacertaFrxDataSets();
begin
  //frxDsCons.DataSet := DCond.QrCons;
  //frxDsCons Its.DataSet := DCond.QrCons Its;
end;

end.

