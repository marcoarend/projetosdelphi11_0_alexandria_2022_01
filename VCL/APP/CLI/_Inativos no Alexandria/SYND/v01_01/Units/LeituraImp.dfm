object FmLeituraImp: TFmLeituraImp
  Left = 339
  Top = 185
  Caption = 'LEI-PRINT-001 :: Impress'#245'es de Consumos'
  ClientHeight = 582
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 420
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 200
    ExplicitHeight = 486
    object Splitter1: TSplitter
      Left = 0
      Top = 252
      Width = 784
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ExplicitLeft = 1
      ExplicitTop = 221
      ExplicitWidth = 99
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 782
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 101
        Height = 13
        Caption = 'Cliente (Condom'#237'nio):'
      end
      object SpeedButton1: TSpeedButton
        Left = 524
        Top = 20
        Width = 21
        Height = 21
        Caption = '?'
        OnClick = SpeedButton1Click
      end
      object LaMes: TLabel
        Left = 552
        Top = 3
        Width = 217
        Height = 13
        Caption = #218'ltimo m'#234's e ano do periodo a se pesquisado:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 12
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdEmpresaChange
        OnEnter = EdEmpresaEnter
        OnExit = EdEmpresaExit
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 441
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBMes: TComboBox
        Left = 552
        Top = 20
        Width = 145
        Height = 21
        Color = clWhite
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Text = 'CBMes'
        OnChange = CBMesChange
      end
      object CBAno: TComboBox
        Left = 700
        Top = 20
        Width = 73
        Height = 21
        Color = clWhite
        DropDownCount = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Text = 'CBAno'
        OnChange = CBAnoChange
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 48
      Width = 784
      Height = 104
      Align = alTop
      Caption = 'Panel4'
      TabOrder = 1
      ExplicitLeft = 1
      ExplicitTop = 49
      ExplicitWidth = 782
      object DBGrid1: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 682
        Height = 102
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'Ok'
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 177
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Caption = 'Pre'#231'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Casas'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnidFat'
            Title.Caption = 'Fator '
            Width = 68
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'UnidLei'
            Title.Alignment = taCenter
            Title.Caption = 'Unid. Leitura'
            Width = 92
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'UnidImp'
            Title.Alignment = taCenter
            Title.Caption = 'Unid. Impress'#227'o '
            Width = 92
            Visible = True
          end>
        Color = clWindow
        DataSource = DsCons
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGrid1CellClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'Ok'
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 177
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Caption = 'Pre'#231'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Casas'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnidFat'
            Title.Caption = 'Fator '
            Width = 68
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'UnidLei'
            Title.Alignment = taCenter
            Title.Caption = 'Unid. Leitura'
            Width = 92
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'UnidImp'
            Title.Alignment = taCenter
            Title.Caption = 'Unid. Impress'#227'o '
            Width = 92
            Visible = True
          end>
      end
      object Panel5: TPanel
        Left = 683
        Top = 1
        Width = 100
        Height = 102
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 681
        object BtNenhum: TBitBtn
          Tag = 128
          Left = 6
          Top = 52
          Width = 90
          Height = 40
          Caption = '&Nenhum'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtNenhumClick
        end
        object BtTodos: TBitBtn
          Tag = 127
          Left = 6
          Top = 8
          Width = 90
          Height = 40
          Caption = '&Todos'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtTodosClick
        end
      end
    end
    object Panel6: TPanel
      Left = 684
      Top = 152
      Width = 100
      Height = 100
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      ExplicitLeft = 683
      ExplicitTop = 153
      ExplicitHeight = 164
      object BitBtn1: TBitBtn
        Tag = 128
        Left = 6
        Top = 52
        Width = 90
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object BitBtn2: TBitBtn
        Tag = 127
        Left = 6
        Top = 8
        Width = 90
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn2Click
      end
    end
    object DBGrid2: TdmkDBGrid
      Left = 0
      Top = 152
      Width = 684
      Height = 100
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Selecio'
          ReadOnly = True
          Title.Caption = 'Ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 71
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Proprie'
          Width = 383
          Visible = True
        end>
      Color = clWindow
      DataSource = DsUnidCond
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid2CellClick
      OnDrawColumnCell = DBGrid2DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Selecio'
          ReadOnly = True
          Title.Caption = 'Ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 71
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Proprie'
          Width = 383
          Visible = True
        end>
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 255
      Width = 784
      Height = 165
      ActivePage = TabSheet1
      Align = alBottom
      TabOrder = 4
      ExplicitLeft = 1
      ExplicitTop = 320
      ExplicitWidth = 782
      object TabSheet1: TTabSheet
        Caption = ' Anal'#237'tico '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid3: TDBGrid
          Left = 0
          Top = 29
          Width = 776
          Height = 108
          Align = alClient
          DataSource = DsConsUHs
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_Leitura'
              Title.Caption = 'Leitura'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Bloco'
              Title.Caption = 'Bloco'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Visible = True
            end
            item
              Alignment = taRightJustify
              Expanded = False
              FieldName = 'MedAnt_TXT'
              Title.Alignment = taRightJustify
              Title.Caption = 'Medida inicial'
              Width = 80
              Visible = True
            end
            item
              Alignment = taRightJustify
              Expanded = False
              FieldName = 'MedAtu_TXT'
              Title.Alignment = taRightJustify
              Title.Caption = 'Medida final'
              Width = 80
              Visible = True
            end
            item
              Alignment = taRightJustify
              Expanded = False
              FieldName = 'Carencia_TXT'
              Title.Alignment = taRightJustify
              Title.Caption = 'Car'#234'ncia'
              Width = 56
              Visible = True
            end
            item
              Alignment = taRightJustify
              Expanded = False
              FieldName = 'Consumo_TXT'
              Title.Alignment = taRightJustify
              Title.Caption = 'Consumo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Preco'
              Title.Caption = 'Pre'#231'o unit'#225'rio'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Caption = 'Valor do consumo'
              Width = 92
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label2: TLabel
            Left = 8
            Top = 8
            Width = 34
            Height = 13
            Caption = 'Meses:'
          end
          object EdMesesA: TdmkEdit
            Left = 44
            Top = 4
            Width = 37
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '12'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 12
            OnChange = EdMesesAChange
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Sint'#233'tico '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 774
        ExplicitHeight = 0
        object DBGrid4: TDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 137
          Align = alClient
          DataSource = DsUnidCon2
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'LeitNom'
              Title.Caption = 'Leitura'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'UH'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UnidImp'
              Title.Caption = 'Medida'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q01_TXT'
              Title.Caption = 'Q01'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q02_TXT'
              Title.Caption = 'Q02'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q03_TXT'
              Title.Caption = 'Q03'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q04_TXT'
              Title.Caption = 'Q04'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q05_TXT'
              Title.Caption = 'Q05'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q06_TXT'
              Title.Caption = 'Q06'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q07_TXT'
              Title.Caption = 'Q07'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q08_TXT'
              Title.Caption = 'Q08'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q09_TXT'
              Title.Caption = 'Q09'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q10_TXT'
              Title.Caption = 'Q10'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q11_TXT'
              Title.Caption = 'Q11'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q12_TXT'
              Title.Caption = 'Q12'
              Width = 56
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 317
        Height = 32
        Caption = 'Impress'#245'es de Consumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 32
        Caption = 'Impress'#245'es de Consumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 317
        Height = 32
        Caption = 'Impress'#245'es de Consumos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 468
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 512
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object QrLis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT cns.Codigo, cns.Nome, cnp.Preco, '
      'cnp.Casas, cnp.UnidLei, cnp.UnidImp, cnp.UnidFat'
      'FROM consprc cnp'
      'LEFT JOIN cons    cns ON cns.Codigo=cnp.Codigo'
      'LEFT JOIN consits cni ON cni.Codigo=cns.Codigo'
      'WHERE cni.Cond=:P0'
      'AND cnp.Cond=:P1'
      'AND cni.Periodo BETWEEN :P2 AND :P3')
    Left = 92
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLisNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrLisPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrLisCasas: TSmallintField
      FieldName = 'Casas'
      Required = True
    end
    object QrLisUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Required = True
      Size = 5
    end
    object QrLisUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Required = True
      Size = 5
    end
    object QrLisUnidFat: TFloatField
      FieldName = 'UnidFat'
      Required = True
    end
  end
  object QrCons: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM cons')
    Left = 120
    Top = 8
    object QrConsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cons.Codigo'
    end
    object QrConsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cons.Nome'
      Size = 50
    end
    object QrConsPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'cons.Preco'
    end
    object QrConsCasas: TSmallintField
      FieldName = 'Casas'
      Origin = 'cons.Casas'
    end
    object QrConsUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Origin = 'cons.UnidImp'
      Size = 5
    end
    object QrConsUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Origin = 'cons.UnidLei'
      Size = 10
    end
    object QrConsUnidFat: TFloatField
      FieldName = 'UnidFat'
      Origin = 'cons.UnidFat'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrConsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cons.Ativo'
      MaxValue = 1
    end
  end
  object DsCons: TDataSource
    DataSet = QrCons
    Left = 148
    Top = 8
  end
  object QrUnidCond: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT uc.* '
      'FROM unidcond uc'
      'ORDER BY uc.Unidade')
    Left = 556
    Top = 12
    object QrUnidCondApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrUnidCondUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrUnidCondProprie: TWideStringField
      FieldName = 'Proprie'
      Size = 100
    end
    object QrUnidCondSelecio: TSmallintField
      FieldName = 'Selecio'
      MaxValue = 1
    end
    object QrUnidCondEntidad: TIntegerField
      FieldName = 'Entidad'
    end
  end
  object DsUnidCond: TDataSource
    DataSet = QrUnidCond
    Left = 584
    Top = 12
  end
  object QrConsUHs: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrConsUHsCalcFields
    SQL.Strings = (
      'SELECT cns.Nome NO_Leitura, cni.Codigo, cdb.Ordem,'
      'cdb.Descri NO_Bloco, cdi.Andar, cdi.Propriet,'
      'cdi.Unidade, cni.Periodo, cni.MedAnt,'
      'cni.MedAtu, cni.Carencia, cni.Consumo, cni.Preco,'
      'cni.Valor, cni.Boleto, cni.Controle, cni.Lancto,'
      'cni.Casas, cni.UnidLei, cni.UnidImp, cni.UnidFat,'
      'cni.Apto, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      'NO_PROPRIET'
      'FROM cons cns'
      'LEFT JOIN consits  cni ON cni.Codigo=cns.Codigo'
      'LEFT JOIN condimov cdi ON cdi.Conta=cni.Apto'
      'LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle'
      'LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet'
      ''
      '')
    Left = 636
    Top = 12
    object QrConsUHsNO_Leitura: TWideStringField
      DisplayWidth = 13
      FieldName = 'NO_Leitura'
      Origin = 'cons.Nome'
      Size = 50
    end
    object QrConsUHsNO_Bloco: TWideStringField
      DisplayWidth = 30
      FieldName = 'NO_Bloco'
      Origin = 'condbloco.Descri'
      Size = 100
    end
    object QrConsUHsUnidade: TWideStringField
      DisplayWidth = 12
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Size = 10
    end
    object QrConsUHsPERIODO_TXT: TWideStringField
      DisplayWidth = 15
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 7
      Calculated = True
    end
    object QrConsUHsMedAnt: TFloatField
      DisplayWidth = 12
      FieldName = 'MedAnt'
      Origin = 'consits.MedAnt'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrConsUHsMedAtu: TFloatField
      DisplayWidth = 12
      FieldName = 'MedAtu'
      Origin = 'consits.MedAtu'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrConsUHsCarencia: TFloatField
      DisplayWidth = 12
      FieldName = 'Carencia'
      Origin = 'consits.Carencia'
    end
    object QrConsUHsConsumo: TFloatField
      DisplayWidth = 12
      FieldName = 'Consumo'
      Origin = 'consits.Consumo'
    end
    object QrConsUHsPreco: TFloatField
      DisplayWidth = 12
      FieldName = 'Preco'
      Origin = 'consits.Preco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrConsUHsValor: TFloatField
      DisplayWidth = 12
      FieldName = 'Valor'
      Origin = 'consits.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrConsUHsBoleto: TFloatField
      DisplayWidth = 12
      FieldName = 'Boleto'
      Origin = 'consits.Boleto'
    end
    object QrConsUHsCodigo: TIntegerField
      DisplayWidth = 12
      FieldName = 'Codigo'
      Origin = 'consits.Codigo'
    end
    object QrConsUHsControle: TIntegerField
      DisplayWidth = 12
      FieldName = 'Controle'
      Origin = 'consits.Controle'
    end
    object QrConsUHsLancto: TIntegerField
      DisplayWidth = 12
      FieldName = 'Lancto'
      Origin = 'consits.Lancto'
    end
    object QrConsUHsPeriodo: TIntegerField
      DisplayWidth = 12
      FieldName = 'Periodo'
      Origin = 'consits.Periodo'
    end
    object QrConsUHsCasas: TSmallintField
      FieldName = 'Casas'
      Origin = 'consits.Casas'
    end
    object QrConsUHsMedAtu_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MedAtu_TXT'
      Size = 21
      Calculated = True
    end
    object QrConsUHsMedAnt_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MedAnt_TXT'
      Size = 21
      Calculated = True
    end
    object QrConsUHsCarencia_TXT: TWideStringField
      DisplayWidth = 21
      FieldKind = fkCalculated
      FieldName = 'Carencia_TXT'
      Size = 21
      Calculated = True
    end
    object QrConsUHsConsumo_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Consumo_TXT'
      Size = 21
      Calculated = True
    end
    object QrConsUHsCONSUMO2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO2'
      Calculated = True
    end
    object QrConsUHsCONSUMO2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO2_TXT'
      Size = 21
      Calculated = True
    end
    object QrConsUHsUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Size = 5
    end
    object QrConsUHsUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Size = 10
    end
    object QrConsUHsUnidFat: TFloatField
      FieldName = 'UnidFat'
    end
    object QrConsUHsApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrConsUHsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrConsUHsAndar: TIntegerField
      FieldName = 'Andar'
    end
    object QrConsUHsPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrConsUHsNO_PROPRIET: TWideStringField
      FieldName = 'NO_PROPRIET'
      Size = 100
    end
  end
  object DsConsUHs: TDataSource
    DataSet = QrConsUHs
    Left = 664
    Top = 12
  end
  object frxCUHs: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCUHsGetValue
    Left = 692
    Top = 12
    Datasets = <
      item
        DataSet = frxDsCUHs
        DataSetName = 'frxDsCUHs'
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 328.819110000000000000
        Width = 793.701300000000000000
        DataSet = frxDsCUHs
        DataSetName = 'frxDsCUHs'
        RowCount = 0
        object Memo4: TfrxMemoView
          Left = 132.283550000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'MedAnt_TXT'
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCUHs."MedAnt_TXT"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 245.669450000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'MedAtu_TXT'
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCUHs."MedAtu_TXT"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 438.425480000000000000
          Width = 79.370078740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Consumo_TXT'
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCUHs."Consumo_TXT"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 593.386210000000000000
          Width = 79.370078740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CONSUMO2_TXT'
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCUHs."CONSUMO2_TXT"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 672.756340000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Valor'
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCUHs."Valor"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 359.055350000000000000
          Width = 79.370078740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Carencia_TXT'
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCUHs."Carencia_TXT"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 56.692950000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'PERIODO_TXT'
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCUHs."PERIODO_TXT"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 517.795610000000000000
          Width = 75.590548740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 158.740238030000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape1: TfrxShapeView
          Left = 56.692950000000000000
          Top = 37.795300000000000000
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 64.252010000000000000
          Top = 37.795300000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 56.692950000000000000
          Top = 56.692950000000000000
          Width = 699.213050000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 207.874150000000000000
          Top = 56.692950000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CONSUMO POR LEITURA POR PER'#205'ODO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 64.252010000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 604.724800000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 56.692950000000000000
          Top = 83.149660000000000000
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOME_CLI]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 56.692950000000000000
          Top = 102.047310000000000000
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'PERIODO: [PERIODO_TXT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 132.283550000000000000
          Top = 124.724490000000000000
          Width = 113.385826770000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Medi'#231#227'o'
            'Anterior')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 245.669450000000000000
          Top = 124.724490000000000000
          Width = 113.385826770000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Medi'#231#227'o'
            'Atual')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 438.425480000000000000
          Top = 124.724490000000000000
          Width = 79.370078740000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Consumo'
            'Leitura')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 593.386210000000000000
          Top = 124.724490000000000000
          Width = 79.370078740000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Consumo'
            'Arrecada'#231#227'o')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 672.756340000000000000
          Top = 124.724490000000000000
          Width = 83.149660000000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pre'#231'o e'
            'Valor')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 359.055350000000000000
          Top = 124.724490000000000000
          Width = 79.370078740000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Car'#234'ncia'
            'Medi'#231#227'o')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 56.692950000000000000
          Top = 124.724490000000000000
          Width = 75.590551180000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's'
            'Ano')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 517.795610000000000000
          Top = 124.724490000000000000
          Width = 75.590548740000000000
          Height = 34.015748030000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator'
            'Leituras')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 26.456710000000000000
        Top = 238.110390000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsCUHs."NO_Leitura"'
        object Memo10: TfrxMemoView
          Left = 672.756340000000000000
          Top = 7.559060000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCUHs."Preco"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 56.692950000000000000
          Top = 7.559060000000000000
          Width = 457.323081180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCUHs."NO_Leitura"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 514.016080000000000000
          Top = 7.559060000000000000
          Width = 79.370078740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCUHs."UnidFat"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 593.386210000000000000
          Top = 7.559060000000000000
          Width = 79.370078740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCUHs."UnidImp"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 18.897650000000000000
        Top = 411.968770000000000000
        Width = 793.701300000000000000
        object Memo35: TfrxMemoView
          Left = 56.692950000000000000
          Width = 381.732481180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total leitura de [frxDsCUHs."NO_Leitura"]: ')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 438.425480000000000000
          Width = 79.370078740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCUHs."Consumo">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 593.386210000000000000
          Width = 79.370078740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCUHs."CONSUMO2">)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 672.756340000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCUHs."Valor">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 517.795610000000000000
          Width = 75.590548740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 287.244280000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsCUHs."Unidade"'
        object Memo3: TfrxMemoView
          Left = 56.692950000000000000
          Width = 699.213001180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'UNIDADE: [frxDsCUHs."Unidade"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 18.897650000000000000
        Top = 370.393940000000000000
        Width = 793.701300000000000000
        object Memo11: TfrxMemoView
          Left = 56.692950000000000000
          Width = 381.732481180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total unidade [frxDsCUHs."Unidade"]'
            ': ')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 438.425480000000000000
          Width = 79.370078740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCUHs."Consumo">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 593.386210000000000000
          Width = 79.370078740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCUHs."CONSUMO2">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 672.756340000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCUHs."Valor">)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 517.795610000000000000
          Width = 75.590548740000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 45.354360000000000000
        Top = 491.338900000000000000
        Width = 793.701300000000000000
      end
    end
  end
  object frxDsCUHs: TfrxDBDataset
    UserName = 'frxDsCUHs'
    CloseDataSource = False
    DataSet = QrConsUHs
    BCDToCurrency = False
    Left = 720
    Top = 12
  end
  object QrUnidCon2: TmySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrUnidCon2CalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM unidcon2')
    Left = 636
    Top = 44
    object QrUnidCon2BlocOrd: TIntegerField
      FieldName = 'BlocOrd'
      Origin = 'unidcon2.BlocOrd'
    end
    object QrUnidCon2Andar: TIntegerField
      FieldName = 'Andar'
      Origin = 'unidcon2.Andar'
    end
    object QrUnidCon2Apto: TIntegerField
      FieldName = 'Apto'
      Origin = 'unidcon2.Apto'
    end
    object QrUnidCon2Entidad: TIntegerField
      FieldName = 'Entidad'
      Origin = 'unidcon2.Entidad'
    end
    object QrUnidCon2BlocNom: TWideStringField
      FieldName = 'BlocNom'
      Origin = 'unidcon2.BlocNom'
      Size = 100
    end
    object QrUnidCon2Unidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'unidcon2.Unidade'
      Size = 10
    end
    object QrUnidCon2Proprie: TWideStringField
      FieldName = 'Proprie'
      Origin = 'unidcon2.Proprie'
      Size = 100
    end
    object QrUnidCon2Quant01: TFloatField
      FieldName = 'Quant01'
      Origin = 'unidcon2.Quant01'
    end
    object QrUnidCon2Quant02: TFloatField
      FieldName = 'Quant02'
      Origin = 'unidcon2.Quant02'
    end
    object QrUnidCon2Quant03: TFloatField
      FieldName = 'Quant03'
      Origin = 'unidcon2.Quant03'
    end
    object QrUnidCon2Quant04: TFloatField
      FieldName = 'Quant04'
      Origin = 'unidcon2.Quant04'
    end
    object QrUnidCon2Quant05: TFloatField
      FieldName = 'Quant05'
      Origin = 'unidcon2.Quant05'
    end
    object QrUnidCon2Quant06: TFloatField
      FieldName = 'Quant06'
      Origin = 'unidcon2.Quant06'
    end
    object QrUnidCon2Quant07: TFloatField
      FieldName = 'Quant07'
      Origin = 'unidcon2.Quant07'
    end
    object QrUnidCon2Quant08: TFloatField
      FieldName = 'Quant08'
      Origin = 'unidcon2.Quant08'
    end
    object QrUnidCon2Quant09: TFloatField
      FieldName = 'Quant09'
      Origin = 'unidcon2.Quant09'
    end
    object QrUnidCon2Quant10: TFloatField
      FieldName = 'Quant10'
      Origin = 'unidcon2.Quant10'
    end
    object QrUnidCon2Quant11: TFloatField
      FieldName = 'Quant11'
      Origin = 'unidcon2.Quant11'
    end
    object QrUnidCon2Quant12: TFloatField
      FieldName = 'Quant12'
      Origin = 'unidcon2.Quant12'
    end
    object QrUnidCon2Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'unidcon2.Ativo'
    end
    object QrUnidCon2LeitCod: TIntegerField
      FieldName = 'LeitCod'
      Origin = 'unidcon2.LeitCod'
    end
    object QrUnidCon2LeitNom: TWideStringField
      FieldName = 'LeitNom'
      Origin = 'unidcon2.LeitNom'
      Size = 50
    end
    object QrUnidCon2UnidImp: TWideStringField
      FieldName = 'UnidImp'
      Size = 10
    end
    object QrUnidCon2Casas: TSmallintField
      FieldName = 'Casas'
    end
    object QrUnidCon2Q01_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q01_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q02_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q02_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q03_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q03_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q04_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q04_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q05_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q05_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q06_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q06_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q07_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q07_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q08_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q08_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q09_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q09_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q10_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q10_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q11_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q11_TXT'
      Size = 21
      Calculated = True
    end
    object QrUnidCon2Q12_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Q12_TXT'
      Size = 21
      Calculated = True
    end
  end
  object DsUnidCon2: TDataSource
    DataSet = QrUnidCon2
    Left = 664
    Top = 44
  end
  object frxUC2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCUHsGetValue
    Left = 692
    Top = 44
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsUC2
        DataSetName = 'frxDsUC2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
      object PageHeader1: TfrxPageHeader
        Height = 143.622130240000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape1: TfrxShapeView
          Left = 56.692950000000000000
          Top = 37.795300000000000000
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 64.252010000000000000
          Top = 37.795300000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 56.692950000000000000
          Top = 56.692950000000000000
          Width = 699.213050000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 207.874150000000000000
          Top = 56.692950000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CONSUMO POR LEITURA POR PER'#205'ODO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 64.252010000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 604.724800000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 56.692950000000000000
          Top = 83.149660000000000000
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOME_CLI]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 56.692950000000000000
          Top = 102.047310000000000000
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'PERIODO: [PERIODO_TXT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 702.992580000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_12]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 650.079160000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_11]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 597.165740000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_10]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 544.252320000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_09]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 491.338900000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_08]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 438.425480000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_07]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 385.512060000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_06]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 332.598640000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_05]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 279.685220000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_04]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 226.771800000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_03]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 173.858380000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_02]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 120.944960000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_01]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 56.692950000000000000
          Top = 128.504020000000000000
          Width = 64.251975830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'UH')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 45.354360000000000000
        Top = 389.291590000000000000
        Width = 793.701300000000000000
      end
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 272.126160000000000000
        Width = 793.701300000000000000
        DataSet = frxDsUC2
        DataSetName = 'frxDsUC2'
        RowCount = 0
        object Memo9: TfrxMemoView
          Left = 702.992580000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q12_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q12_TXT"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 650.079160000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q11_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q11_TXT"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 597.165740000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q10_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q10_TXT"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 544.252320000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q09_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q09_TXT"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 491.338900000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q08_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q08_TXT"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 438.425480000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q07_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q07_TXT"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 385.512060000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q06_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q06_TXT"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 332.598640000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q05_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q05_TXT"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 279.685220000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q04_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q04_TXT"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q03_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q03_TXT"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 173.858380000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q02_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q02_TXT"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 120.944960000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q01_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q01_TXT"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 56.692950000000000000
          Width = 64.251975830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Unidade"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 26.456710000000000000
        Top = 222.992270000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsUC2."LeitCod"'
        object Memo1: TfrxMemoView
          Left = 56.692950000000000000
          Top = 7.559060000000000000
          Width = 487.559321180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsUC2."LeitNom"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 544.252320000000000000
          Top = 7.559060000000000000
          Width = 211.653631180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Grandeza: [frxDsUC2."UnidImp"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 18.897640240000000000
        Top = 309.921460000000000000
        Width = 793.701300000000000000
        object Memo35: TfrxMemoView
          Left = 56.692950000000000000
          Width = 64.251975830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 702.992580000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant12">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 650.079160000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant11">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 597.165740000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant10">)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 544.252320000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant09">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 491.338900000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant08">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 438.425480000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant07">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 385.512060000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant06">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 332.598640000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant05">)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 279.685220000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant04">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant03">)]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 173.858380000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant02">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 120.944960000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant01">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsUC2: TfrxDBDataset
    UserName = 'frxDsUC2'
    CloseDataSource = False
    DataSet = QrUnidCon2
    BCDToCurrency = False
    Left = 720
    Top = 44
  end
  object frxReport1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCUHsGetValue
    Left = 612
    Top = 148
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsUC2
        DataSetName = 'frxDsUC2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 272.126160000000000000
        Width = 793.701300000000000000
        DataSet = frxDsUC2
        DataSetName = 'frxDsUC2'
        RowCount = 0
        object Memo9: TfrxMemoView
          Left = 702.992580000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q12_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q12_TXT"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 650.079160000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q11_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q11_TXT"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 597.165740000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q10_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q10_TXT"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 544.252320000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q09_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q09_TXT"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 491.338900000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q08_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q08_TXT"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 438.425480000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q07_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q07_TXT"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 385.512060000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q06_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q06_TXT"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 332.598640000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q05_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q05_TXT"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 279.685220000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q04_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q04_TXT"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q03_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q03_TXT"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 173.858380000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q02_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q02_TXT"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 120.944960000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Q01_TXT'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Q01_TXT"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 56.692950000000000000
          Width = 64.251975830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUC2."Unidade"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 143.622130240000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape1: TfrxShapeView
          Left = 56.692950000000000000
          Top = 37.795300000000000000
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 64.252010000000000000
          Top = 37.795300000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 56.692950000000000000
          Top = 56.692950000000000000
          Width = 699.213050000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 207.874150000000000000
          Top = 56.692950000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CONSUMO POR LEITURA POR PER'#205'ODO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 64.252010000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 604.724800000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 56.692950000000000000
          Top = 83.149660000000000000
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOME_CLI]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 56.692950000000000000
          Top = 102.047310000000000000
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'PERIODO: [PERIODO_TXT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 702.992580000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_12]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 650.079160000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_11]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 597.165740000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_10]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 544.252320000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_09]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 491.338900000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_08]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 438.425480000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_07]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 385.512060000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_06]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 332.598640000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_05]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 279.685220000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_04]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 226.771800000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_03]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 173.858380000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_02]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 120.944960000000000000
          Top = 128.504020000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_01]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 56.692950000000000000
          Top = 128.504020000000000000
          Width = 64.251975830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'UH')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 26.456710000000000000
        Top = 222.992270000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsUC2."LeitCod"'
        object Memo1: TfrxMemoView
          Left = 56.692950000000000000
          Top = 7.559060000000000000
          Width = 487.559321180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsUC2."LeitNom"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 544.252320000000000000
          Top = 7.559060000000000000
          Width = 211.653631180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCUHs
          DataSetName = 'frxDsCUHs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Grandeza: [frxDsUC2."UnidImp"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 18.897640240000000000
        Top = 309.921460000000000000
        Width = 793.701300000000000000
        object Memo35: TfrxMemoView
          Left = 56.692950000000000000
          Width = 64.251975830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 702.992580000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant12">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 650.079160000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant11">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 597.165740000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant10">)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 544.252320000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant09">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 491.338900000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant08">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 438.425480000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant07">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 385.512060000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant06">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 332.598640000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant05">)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 279.685220000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant04">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant03">)]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 173.858380000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant02">)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 120.944960000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsUC2
          DataSetName = 'frxDsUC2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUC2."Quant01">)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 45.354360000000000000
        Top = 389.291590000000000000
        Width = 793.701300000000000000
      end
    end
  end
end
