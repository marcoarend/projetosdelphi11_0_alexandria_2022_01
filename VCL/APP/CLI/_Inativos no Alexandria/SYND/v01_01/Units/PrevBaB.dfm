object FmPrevBaB: TFmPrevBaB
  Left = 244
  Top = 182
  Caption = 'GER-CONDM-009 :: Adi'#231#227'o de Contas Bases no Or'#231'amento'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 28
      Width = 1008
      Height = 304
      Align = alClient
      DataSource = DsPrevBAB
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnColEnter = DBGrid1ColEnter
      OnColExit = DBGrid1ColExit
      OnDrawColumnCell = DBGrid1DrawColumnCell
      OnKeyDown = DBGrid1KeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'Adiciona'
          ReadOnly = True
          Title.Caption = 'OK'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Texto a ser impresso'
          Width = 286
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTABASE'
          Title.Caption = 'Conta Base'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTA'
          Title.Caption = 'Conta (Plano de contas)'
          Width = 175
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LastM'
          Title.Caption = #218'lt. m'#234's'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Last1'
          Title.Caption = #218'lt. valor [F5]'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Last6'
          Title.Caption = 'MU6M* [F6]'
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 28
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel4: TPanel
        Left = 810
        Top = 0
        Width = 198
        Height = 28
        Align = alRight
        BevelOuter = bvLowered
        TabOrder = 0
        object Label2: TLabel
          Left = 4
          Top = 8
          Width = 87
          Height = 13
          Caption = 'Soma dos valores:'
        end
        object DBEdit1: TDBEdit
          Left = 104
          Top = 4
          Width = 81
          Height = 21
          DataField = 'Valor'
          DataSource = DsSoma
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
      end
      object StaticText2: TStaticText
        Left = 0
        Top = 0
        Width = 110
        Height = 28
        Align = alClient
        Alignment = taCenter
        BorderStyle = sbsSunken
        Caption = 'StaticText2'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 380
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 664
        Height = 16
        Caption = 
          '* MU6M: m'#233'dia dos '#250'ltimos 6 meses.    *** Edit'#225'veis  clicando no' +
          ' bot'#227'o de edi'#231#227'o que aparece ao clicar na c'#233'lula.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 664
        Height = 16
        Caption = 
          '* MU6M: m'#233'dia dos '#250'ltimos 6 meses.    *** Edit'#225'veis  clicando no' +
          ' bot'#227'o de edi'#231#227'o que aparece ao clicar na c'#233'lula.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 4
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn2: TBitBtn
        Tag = 127
        Left = 172
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn2Click
      end
      object BitBtn3: TBitBtn
        Tag = 128
        Left = 297
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn3Click
      end
      object BitBtn1: TBitBtn
        Tag = 12
        Left = 480
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BitBtn1Click
      end
    end
  end
  object TbPrevBAB: TmySQLTable
    Database = DModG.MyPID_DB
    AfterOpen = TbPrevBABAfterOpen
    AfterInsert = TbPrevBABAfterInsert
    AfterPost = TbPrevBABAfterPost
    AfterDelete = TbPrevBABAfterDelete
    AfterRefresh = TbPrevBABAfterRefresh
    TableName = 'PrevBAB'
    Left = 288
    Top = 256
    object TbPrevBABConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'PrevBAB.Conta'
    end
    object TbPrevBABPrevBaI: TIntegerField
      FieldName = 'PrevBaI'
      Origin = 'PrevBAB.PrevBaI'
    end
    object TbPrevBABPrevBaC: TIntegerField
      FieldName = 'PrevBaC'
      Origin = 'PrevBAB.PrevBaC'
    end
    object TbPrevBABValor: TFloatField
      FieldName = 'Valor'
      Origin = 'PrevBAB.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object TbPrevBABTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'PrevBAB.Texto'
      Size = 40
    end
    object TbPrevBABAdiciona: TSmallintField
      DisplayWidth = 1
      FieldName = 'Adiciona'
      Origin = 'PrevBAB.Adiciona'
      MaxValue = 1
    end
    object TbPrevBABNOMECONTABASE: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECONTABASE'
      LookupDataSet = QrPrevBaC
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'PrevBaC'
      Size = 50
      Lookup = True
    end
    object TbPrevBABNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 100
    end
    object TbPrevBABLastM: TWideStringField
      FieldName = 'LastM'
      Size = 8
    end
    object TbPrevBABLast1: TFloatField
      FieldName = 'Last1'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object TbPrevBABLast6: TFloatField
      FieldName = 'Last6'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsPrevBAB: TDataSource
    DataSet = TbPrevBAB
    Left = 316
    Top = 256
  end
  object QrPrevBaC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Conta, Nome'
      'FROM prevbac')
    Left = 144
    Top = 284
    object QrPrevBaCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'prevbac.Codigo'
    end
    object QrPrevBaCConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'prevbac.Conta'
    end
    object QrPrevBaCNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'prevbac.Nome'
      Size = 40
    end
  end
  object DsPrevBaC: TDataSource
    DataSet = QrPrevBaC
    Left = 172
    Top = 284
  end
  object QrConta: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT COUNT(Valor) Zeros'
      'FROM prevban'
      'WHERE Valor < 0.01')
    Left = 120
    Top = 184
    object QrContaZeros: TLargeintField
      FieldName = 'Zeros'
      Required = True
    end
  end
  object QrSoma: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor'
      'FROM prevbab'
      'WHERE Adiciona=1'
      '')
    Left = 380
    Top = 176
    object QrSomaValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 409
    Top = 177
  end
end
