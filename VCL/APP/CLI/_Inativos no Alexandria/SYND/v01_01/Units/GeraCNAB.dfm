object FmGeraCNAB: TFmGeraCNAB
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-033 :: Gera Arquivo CNAB'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 398
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 81
      ExplicitTop = 5
      ExplicitWidth = 782
      object Label1: TLabel
        Left = 136
        Top = 8
        Width = 64
        Height = 13
        Caption = 'Data arquivo:'
      end
      object Label2: TLabel
        Left = 252
        Top = 8
        Width = 61
        Height = 13
        Caption = 'Data cr'#233'dito:'
      end
      object Label9: TLabel
        Left = 368
        Top = 8
        Width = 40
        Height = 13
        Caption = 'Multa %:'
      end
      object Label10: TLabel
        Left = 460
        Top = 8
        Width = 76
        Height = 13
        Caption = 'Juros ao m'#234's %:'
      end
      object Label3: TLabel
        Left = 20
        Top = 8
        Width = 59
        Height = 13
        Caption = 'Vencimento:'
      end
      object TPDataD: TdmkEditDateTimePicker
        Left = 136
        Top = 24
        Width = 112
        Height = 21
        Date = 39161.353282326400000000
        Time = 39161.353282326400000000
        TabOrder = 1
        OnChange = TPDataDChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPDataC: TdmkEditDateTimePicker
        Left = 252
        Top = 24
        Width = 112
        Height = 21
        Date = 39161.353282326400000000
        Time = 39161.353282326400000000
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdMulta: TdmkEdit
        Left = 368
        Top = 24
        Width = 88
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2.000000000000000000
        OnExit = EdMultaExit
      end
      object EdJuros: TdmkEdit
        Left = 460
        Top = 24
        Width = 88
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.000000000000000000
        OnExit = EdJurosExit
      end
      object TPDataV: TdmkEditDateTimePicker
        Left = 20
        Top = 24
        Width = 112
        Height = 21
        Date = 39161.353282326400000000
        Time = 39161.353282326400000000
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 64
      Width = 784
      Height = 268
      Align = alClient
      BevelOuter = bvNone
      Caption = 'Panel4'
      TabOrder = 1
      ExplicitLeft = 1
      ExplicitTop = 65
      ExplicitWidth = 782
      ExplicitHeight = 332
      object Splitter1: TSplitter
        Left = 0
        Top = 56
        Width = 784
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitLeft = 1
        ExplicitTop = 121
        ExplicitWidth = 788
      end
      object DBGradeS: TDBGrid
        Left = 0
        Top = 25
        Width = 784
        Height = 31
        Align = alClient
        DataSource = DsBoletos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = DBGradeSKeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'Boleto'
            Title.Caption = 'Bloqueto'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Unidade'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEPROPRIET'
            Title.Caption = 'Respons'#225'vel'
            Width = 299
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SUB_TOT'
            Title.Caption = 'Valor'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VENCTO_TXT'
            Title.Caption = 'Vencimen.'
            Width = 56
            Visible = True
          end>
      end
      object Memo1: TMemo
        Left = 0
        Top = 59
        Width = 784
        Height = 209
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        WordWrap = False
        ExplicitLeft = 93
        ExplicitTop = 138
        ExplicitWidth = 780
      end
      object Panel18: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        ExplicitLeft = 1
        ExplicitTop = 1
        ExplicitWidth = 780
        object Label5: TLabel
          Left = 4
          Top = 4
          Width = 91
          Height = 13
          Caption = 'Itens selecionados:'
        end
        object LaCIS: TLabel
          Left = 100
          Top = 4
          Width = 8
          Height = 13
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LaSVIS: TLabel
          Left = 360
          Top = 4
          Width = 26
          Height = 13
          Caption = '0,00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label25: TLabel
          Left = 160
          Top = 4
          Width = 197
          Height = 13
          Caption = 'Soma dos valores dos itens selecionados:'
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 241
        Height = 32
        Caption = 'Gera Arquivo CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 241
        Height = 32
        Caption = 'Gera Arquivo CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 241
        Height = 32
        Caption = 'Gera Arquivo CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 380
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object Label4: TLabel
        Left = 200
        Top = 4
        Width = 84
        Height = 13
        Caption = 'Nome do arquivo:'
      end
      object PnSaiDesis: TPanel
        Left = 680
        Top = 0
        Width = 100
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sair'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Gera'
        TabOrder = 1
        OnClick = BtOKClick
        NumGlyphs = 2
      end
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 104
        Top = 4
        Width = 90
        Height = 40
        Caption = 'G&rava'
        TabOrder = 2
        OnClick = BitBtn1Click
        NumGlyphs = 2
      end
      object EdArq: TEdit
        Left = 200
        Top = 20
        Width = 465
        Height = 21
        TabOrder = 3
      end
    end
  end
  object QrDir: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cnab240dir')
    Left = 248
    Top = 224
    object QrDirCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDirNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrDirEnvio: TSmallintField
      FieldName = 'Envio'
    end
    object QrDirCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDirLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDirDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDirDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDirUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDirUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDirCarteira: TIntegerField
      FieldName = 'Carteira'
    end
  end
  object QrBoletos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrBoletosAfterOpen
    OnCalcFields = QrBoletosCalcFields
    Left = 276
    Top = 380
    object QrBoletosSUB_ARR: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_ARR'
      LookupDataSet = QrBolArr
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBoletosSUB_LEI: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_LEI'
      LookupDataSet = QrBolLei
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBoletosSUB_TOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrBoletosVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrBoletosApto: TIntegerField
      FieldName = 'Apto'
      Origin = 'Apto'
      Required = True
    end
    object QrBoletosPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'Propriet'
      Required = True
    end
    object QrBoletosVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'Vencto'
      Required = True
    end
    object QrBoletosUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'Unidade'
      Size = 10
    end
    object QrBoletosNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Origin = 'NOMEPROPRIET'
      Size = 100
    end
    object QrBoletosBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBoletosInstante: TFloatField
      FieldName = 'Instante'
      Origin = 'Instante'
    end
    object QrBoletosBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
    object QrBoletosBLOQUETO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'BLOQUETO'
      Calculated = True
    end
  end
  object DsBoletos: TDataSource
    DataSet = QrBoletos
    Left = 304
    Top = 380
  end
  object QrBolArr: TmySQLQuery
    Database = DModG.MyPID_DB
    Left = 333
    Top = 380
    object QrBolArrApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrBolArrValor: TFloatField
      FieldName = 'Valor'
    end
    object QrBolArrBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Size = 23
    end
    object QrBolArrBoleto: TFloatField
      FieldName = 'Boleto'
    end
  end
  object QrBolLei: TmySQLQuery
    Database = DModG.MyPID_DB
    Left = 361
    Top = 380
    object QrBolLeiApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrBolLeiValor: TFloatField
      FieldName = 'Valor'
    end
    object QrBolLeiBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Size = 23
    end
    object QrBolLeiBoleto: TFloatField
      FieldName = 'Boleto'
    end
  end
end
