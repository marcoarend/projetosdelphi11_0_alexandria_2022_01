unit Inadimp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Grids,
  DBGrids, dmkDBGrid, Mask, ComCtrls, frxClass, frxDBSet, Variants, Menus,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UMySQLModule, dmkGeral,
  dmkEditDateTimePicker, UnDmkProcFunc, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmInadimp = class(TForm)
    Panel1: TPanel;
    QrPropriet: TmySQLQuery;
    DsPropriet: TDataSource;
    QrProprietCodigo: TIntegerField;
    QrProprietNOMEPROP: TWideStringField;
    QrCondImov: TmySQLQuery;
    DsCondImov: TDataSource;
    PnPesq1: TPanel;
    dmkDBGPesq: TdmkDBGrid;
    QrCondImovConta: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    PnPesq2: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Splitter1: TSplitter;
    QrPesq3: TmySQLQuery;
    DsPesq3: TDataSource;
    QrTot3: TmySQLQuery;
    DsTot3: TDataSource;
    QrPesq3Unidade: TWideStringField;
    QrPesq3Data: TDateField;
    QrPesq3PAGO: TFloatField;
    QrPesq3SALDO: TFloatField;
    QrPesq3Mez: TIntegerField;
    QrPesq3Vencimento: TDateField;
    QrPesq3Compensado: TDateField;
    QrPesq3Controle: TIntegerField;
    QrPesq3Descricao: TWideStringField;
    QrPesq3NOMEEMPCOND: TWideStringField;
    QrPesq3NOMEPRPIMOV: TWideStringField;
    QrTot3VALOR: TFloatField;
    QrTot3PAGO: TFloatField;
    QrTot3SALDO: TFloatField;
    Panel3: TPanel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    EdMezIni: TEdit;
    EdMezFim: TEdit;
    CkNaoMensais: TCheckBox;
    GBEmissao: TGroupBox;
    CkIniDta: TCheckBox;
    TPIniDta: TDateTimePicker;
    CkFimDta: TCheckBox;
    TPFimDta: TDateTimePicker;
    GBVencto: TGroupBox;
    CkIniVct: TCheckBox;
    TPIniVct: TDateTimePicker;
    CkFimVct: TCheckBox;
    TPFimVct: TDateTimePicker;
    CkFimPgt: TCheckBox;
    TPFimPgt: TDateTimePicker;
    Panel5: TPanel;
    Panel6: TPanel;
    Memo1: TMemo;
    Panel7: TPanel;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    frxPend_s0i0: TfrxReport;
    frxDsPesq3: TfrxDBDataset;
    RGGrupos: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    QrCondGri: TmySQLQuery;
    QrCondGriCodigo: TIntegerField;
    QrCondGriNome: TWideStringField;
    DsCondGri: TDataSource;
    PCAgrupamentos: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    QrCondGriImv: TmySQLQuery;
    QrCondGriImvNOMEPROP: TWideStringField;
    QrCondGriImvNOMECOND: TWideStringField;
    QrCondGriImvUNIDADE: TWideStringField;
    QrCondGriImvCodigo: TIntegerField;
    QrCondGriImvControle: TIntegerField;
    QrCondGriImvCond: TIntegerField;
    QrCondGriImvApto: TIntegerField;
    QrCondGriImvPropr: TIntegerField;
    QrCondGriImvLk: TIntegerField;
    QrCondGriImvDataCad: TDateField;
    QrCondGriImvDataAlt: TDateField;
    QrCondGriImvUserCad: TIntegerField;
    QrCondGriImvUserAlt: TIntegerField;
    QrCondGriImvAlterWeb: TSmallintField;
    QrCondGriImvAtivo: TSmallintField;
    DsCondGriImv: TDataSource;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Panel8: TPanel;
    EdCondGri: TdmkEditCB;
    CBCondGri: TDMKDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrPesq3Juros: TFloatField;
    QrPesq3Multa: TFloatField;
    QrPesq3TOTAL: TFloatField;
    QrPesq3FatNum: TFloatField;
    Panel9: TPanel;
    RGSomas: TRadioGroup;
    RGImpressao: TRadioGroup;
    Panel10: TPanel;
    RGDocumentos: TRadioGroup;
    RGSituacao: TRadioGroup;
    QrPesq3PEND_VAL: TFloatField;
    QrPesq3MEZ_TXT: TWideStringField;
    QrPesq3VCTO_TXT: TWideStringField;
    QrPesq3CREDITO: TFloatField;
    PMMulJur: TPopupMenu;
    AlterapercentualdeMulta1: TMenuItem;
    AlterapercentualdeJurosmensais1: TMenuItem;
    QrPesq3CliInt: TIntegerField;
    Panel11: TPanel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdPropriet: TdmkEditCB;
    CBPropriet: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdCondImov: TdmkEditCB;
    CBCondImov: TdmkDBLookupComboBox;
    QrPesq3Propriet: TIntegerField;
    QrPesq3Apto: TIntegerField;
    QrPesq3Imobiliaria: TIntegerField;
    QrPesq3Procurador: TIntegerField;
    QrPesq3Empresa: TIntegerField;
    QrPesq3Usuario: TIntegerField;
    PMImprime: TPopupMenu;
    Pendnciasdecondminos1: TMenuItem;
    Segundaviadebloquetos1: TMenuItem;
    frxCondE2: TfrxReport;
    QrIts3: TmySQLQuery;
    DsIts3: TDataSource;
    QrIts3Genero: TIntegerField;
    QrIts3Descricao: TWideStringField;
    QrIts3Credito: TFloatField;
    QrPesq3Juridico: TSmallintField;
    QrPesq3Juridico_TXT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel12: TPanel;
    BtPesquisa: TBitBtn;
    BtImprime: TBitBtn;
    BtCertidao: TBitBtn;
    BtMulJur: TBitBtn;
    BtDiarioAdd: TBitBtn;
    BtReparc: TBitBtn;
    QrPesq3Juridico_DESCRI: TWideStringField;
    QrBloqInad: TmySQLQuery;
    QrBloqInadEmpresa: TIntegerField;
    QrBloqInadPropriet: TIntegerField;
    QrBloqInadUnidade: TWideStringField;
    QrBloqInadApto: TIntegerField;
    QrBloqInadImobiliaria: TIntegerField;
    QrBloqInadProcurador: TIntegerField;
    QrBloqInadUsuario: TIntegerField;
    QrBloqInadData: TDateField;
    QrBloqInadCliInt: TIntegerField;
    QrBloqInadCREDITO: TFloatField;
    QrBloqInadPAGO: TFloatField;
    QrBloqInadJuros: TFloatField;
    QrBloqInadMulta: TFloatField;
    QrBloqInadTOTAL: TFloatField;
    QrBloqInadSALDO: TFloatField;
    QrBloqInadPEND_VAL: TFloatField;
    QrBloqInadDescricao: TWideStringField;
    QrBloqInadMez: TIntegerField;
    QrBloqInadMEZ_TXT: TWideStringField;
    QrBloqInadVencimento: TDateField;
    QrBloqInadCompensado: TDateField;
    QrBloqInadControle: TIntegerField;
    QrBloqInadFatNum: TFloatField;
    QrBloqInadVCTO_TXT: TWideStringField;
    QrBloqInadNOMEEMPCOND: TWideStringField;
    QrBloqInadNOMEPRPIMOV: TWideStringField;
    QrBloqInadAtivo: TSmallintField;
    QrBloqInadNewVencto: TDateField;
    BtCobranca: TBitBtn;
    RGPesquisa: TRadioGroup;
    QrIts3Controle: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdProprietChange(Sender: TObject);
    procedure EdCondImovChange(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure EdMezIniExit(Sender: TObject);
    procedure EdMezIniKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMezFimExit(Sender: TObject);
    procedure EdMezFimKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrTot3CalcFields(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxPend_s0i0GetValue(const VarName: String;
      var Value: Variant);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure RGOrdem3Click(Sender: TObject);
    procedure RGOrdem4Click(Sender: TObject);
    procedure EdCondGriChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrPesq3AfterOpen(DataSet: TDataSet);
    procedure QrPesq3BeforeClose(DataSet: TDataSet);
    procedure BtCertidaoClick(Sender: TObject);
    procedure BtMulJurClick(Sender: TObject);
    procedure AlterapercentualdeJurosmensais1Click(Sender: TObject);
    procedure AlterapercentualdeMulta1Click(Sender: TObject);
    procedure BtDiarioAddClick(Sender: TObject);
    procedure dmkDBGPesqDblClick(Sender: TObject);
    procedure BtReparcClick(Sender: TObject);
    procedure Pendnciasdecondminos1Click(Sender: TObject);
    procedure Segundaviadebloquetos1Click(Sender: TObject);
    procedure QrPesq3AfterScroll(DataSet: TDataSet);
    procedure QrPesq3CalcFields(DataSet: TDataSet);
    procedure EdEmpresaExit(Sender: TObject);
    procedure EdEmpresaEnter(Sender: TObject);
    procedure BtCobrancaClick(Sender: TObject);
    procedure EdCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FTabLctA: String;
    FDtaPgt: String;
    FTemDtP: Boolean;
    FCondAnt: Integer;
    procedure ReopenPropriet(Todos: Boolean);
    procedure ReopenCondImov();
    procedure ComplementaQuery3(Query: TMySQLQuery);
    {
    procedure ComplementaQuery1(Query: TMySQLQuery);
    procedure Pesquisa1();
    procedure Pesquisa2();
    }
    procedure Pesquisa3();
    procedure ReopenCondGriImv(Controle: Integer);
    procedure SetaEImprimeFrx(Frx: TfrxReport; Titulo: String);
    procedure MostraDiarioAdd();
    procedure ConfiguraOutroCondominio();
    procedure RefreshEmpresaSelecionada();
    procedure DefineCondImovPeloCodigo(Key: Integer);
  public
    { Public declarations }
  end;

  var
  FmInadimp: TFmInadimp;

implementation

uses Module, Principal, CondGri, UnInternalConsts, MyDBCheck, CertidaoNeg,
  DiarioAdd, Reparc, UnFinanceiro, CreateApp, ModuleGeral, InadimpBloq,
  UnMyObjects, ModuleCond, CobrarGer, ModCobranca, GetValor;

{$R *.DFM}

var
  HEADERS: array of String;

const
  Ordens: array[0..5] of String = ('Vencimento', 'NOMEEMPCOND', 'NOMEPRPIMOV', 'Unidade', 'Mez', 'Data');
  _Labels_: array[0..5] of String = ('Vencimento', 'NOMEEMPCOND', 'NOMEPRPIMOV', 'Unidade', 'MEZ_TXT', 'Data');
                                     
procedure TFmInadimp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmInadimp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmInadimp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmInadimp.EdEmpresaChange(Sender: TObject);
begin
  if not EdEmpresa.Focused then
    ConfiguraOutroCondominio();
end;

procedure TFmInadimp.EdEmpresaEnter(Sender: TObject);
begin
  FCondAnt := EdEmpresa.ValueVariant;
end;

procedure TFmInadimp.EdEmpresaExit(Sender: TObject);
begin
  if FCondAnt <> EdEmpresa.ValueVariant then
  begin
    FCondAnt := EdEmpresa.ValueVariant;
    ConfiguraOutroCondominio();
  end;
end;

procedure TFmInadimp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FCondAnt := 0;
  SetLength(Headers, 6);
  HEADERS[0] := 'Vencimento';
  HEADERS[1] := DModG.ReCaptionTexto(VAR_C_O_N_D_O_M_I_N_I_O);
  HEADERS[2] := DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O);
  HEADERS[3] := DModG.ReCaptionTexto(VAR_U_H_LONGO);
  HEADERS[4] := 'Compet�ncia';
  HEADERS[5] := 'Emiss�o';
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  UMyMod.AbreQuery(QrPropriet, Dmod.MyDB);
  UMyMod.AbreQuery(QrCondImov, Dmod.MyDB);
  UMyMod.AbreQuery(QrCondGri, Dmod.MyDB);
  //
  TPIniDta.Date := Date;
  TPFimDta.Date := Date;
  //
  TPIniVct.Date := Date;
  TPFimVct.Date := Date;
  //
  TPFimPgt.Date := Date;
  //
  PCAgrupamentos.ActivePageIndex := 0;
  //
  //case RGTipoPesq.ItemIndex of
    //3:
    //begin
      CkFimPgt.Visible  := True;
      TPFimPgt.Visible  := True;
      //
      //CkFimVct_.Checked := True;
      //TPFimVct.Date    := Date;
      //CkFimPgt.Checked := True;
      //TPFimPgt.Date    := Date;
      //
      CkIniVct.Checked := False;
      CkIniDta.Checked := False;
      CkFimDta.Checked := False;
    //end;
  //end;
  //if RGTipoPesq.ItemIndex <> 3 then CkFimPgt_.Checked := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'D� um duplo clique na grade para adicionar um evento ao di�rio!');
end;

procedure TFmInadimp.EdProprietChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
  ReopenCondImov;
end;

procedure TFmInadimp.EdCondImovChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp.EdCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  DefineCondImovPeloCodigo(Key);
end;

procedure TFmInadimp.ReopenPropriet(Todos: Boolean);
var
  Cond: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    EdPropriet.Text := '';
    CBPropriet.KeyValue := Null;
    //
    Cond := Geral.IMV(EdEmpresa.Text);
    QrPropriet.Close;
    QrPropriet.SQL.Clear;
    QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
    QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
    QrPropriet.SQL.Add('FROM condimov cim');
    QrPropriet.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cim.Propriet');
    QrPropriet.SQL.Add('WHERE cim.Codigo<>0');
    QrPropriet.SQL.Add('');
    if (Cond <> 0) and not Todos then
      QrPropriet.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
    QrPropriet.SQL.Add('');
    QrPropriet.SQL.Add('ORDER BY NOMEPROP');
    UMyMod.AbreQuery(QrPropriet, Dmod.MyDB);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmInadimp.ReopenCondImov();
var
  Cond, Propriet: Integer;
begin
  EdCondImov.Text := '';
  CBCondImov.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  Propriet := Geral.IMV(EdPropriet.Text);
  QrCondImov.Close;
  QrCondImov.SQL.Clear;
  QrCondImov.SQL.Add('SELECT cim.Conta, cim.Unidade');
  QrCondImov.SQL.Add('FROM condimov cim');
  QrCondImov.SQL.Add('WHERE cim.Codigo<>0');
  QrCondImov.SQL.Add('AND cim.Propriet<>0');
  QrCondImov.SQL.Add('');
  if Cond <> 0 then
    QrCondImov.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  if Propriet <> 0 then
    QrCondImov.SQL.Add('AND cim.Propriet=' + IntToStr(Propriet));
  QrCondImov.SQL.Add('');
  QrCondImov.SQL.Add('ORDER BY cim.Unidade');
  //
  UMyMod.AbreQuery(QrCondImov, Dmod.MyDB);
end;

procedure TFmInadimp.EdMezIniExit(Sender: TObject);
begin
  EdMezIni.Text := MLAGeral.TST(EdMezIni.Text, False);
end;

procedure TFmInadimp.EdMezIniKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Periodo: Integer;
begin
  if key in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
  begin
    if (EdMezIni.Text = '') or (key=VK_F5) then
      Periodo := Geral.Periodo2000(Date)
    else begin
      Periodo := dmkPF.MensalToPeriodo(EdMezIni.Text);
      if key=VK_DOWN then Periodo := Periodo -1;
      if key=VK_UP   then Periodo := Periodo +1;
      if key=VK_F4   then Periodo := Periodo -1;
      if key=VK_F5   then Periodo := Periodo   ;
    end;
    EdMezIni.Text := dmkPF.PeriodoToMensal(Periodo);
  end;
end;

procedure TFmInadimp.EdMezFimExit(Sender: TObject);
begin
  EdMezFim.Text := MLAGeral.TST(EdMezFim.Text, False);
end;

procedure TFmInadimp.EdMezFimKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Periodo: Integer;
begin
  if key in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
  begin
    if (EdMezFim.Text = '') or (key=VK_F5) then
      Periodo := Geral.Periodo2000(Date)
    else begin
      Periodo := dmkPF.MensalToPeriodo(EdMezFim.Text);
      if key=VK_DOWN then Periodo := Periodo -1;
      if key=VK_UP   then Periodo := Periodo +1;
      if key=VK_F4   then Periodo := Periodo -1;
      if key=VK_F5   then Periodo := Periodo   ;
    end;
    EdMezFim.Text := dmkPF.PeriodoToMensal(Periodo);
  end;
end;

procedure TFmInadimp.BtPesquisaClick(Sender: TObject);
var
  CondCod, CodEnt, Propriet, CondImov: Integer;
begin
  // 2012-05-16
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
  'Informe o Condom�nio!') then
    Exit;
  // 2012-05-16
  Screen.Cursor := crHourGlass;
  //
  Memo1.Lines.Clear;
  //
  {
  case RGTipoPesq.ItemIndex of
    1..2:
    begin
      Pesquisa1();
      dmkDBGPesq.DataSource := DsPesq1;
      DBEdit1.DataSource := DsTot1;
      DBEdit2.DataSource := DsTot1;
      DBEdit3.DataSource := DsTot1;
    end;
    3..4:
    begin
  }
  if RGPesquisa.ItemIndex = 0 then
    Pesquisa3()
  else
  begin
    QrPesq3.Close;
    CondCod  := Geral.IMV(EdEmpresa.Text);
    CodEnt   := DModG.QrEmpresasCodigo.Value;
    Propriet := Geral.IMV(EdPropriet.Text);
    CondImov := Geral.IMV(EdCondImov.Text);
    //
    QrPesq3.SQL.Text := DmModCobranca.Pesquisa3_InadimpDeCond(FTabLctA,
    CondCod, CodEnt, Propriet, CondImov,
    RGDocumentos.ItemIndex, PCAgrupamentos.ActivePageIndex,
    QrCondGriImv,
    EdMezIni.Text, EdMezFim.Text,
    CkNaoMensais.Checked,
    TPIniDta.Date, TPFimDta.Date, CkIniDta.Checked, CkFimDta.Checked, GBEmissao.Visible,
    TPIniVct.Date, TPFimVct.Date, CkIniVct.Checked, CkFimVct.Checked, GBVencto.Visible,
    CkFimPgt.Checked,
    TPFimPgt.Date,
    RGSomas.ItemIndex, RGSituacao.ItemIndex,
    RGOrdem1.ItemIndex, RGOrdem2.ItemIndex, RGOrdem3.ItemIndex, RGOrdem4.ItemIndex);
    //
    UMyMod.AbreQuery(QrPesq3, Dmod.MyDB);
    //
    if PnPesq1 <> nil then
      PnPesq1.Visible := True;
  end;
  //
  dmkDBGPesq.DataSource := DsPesq3;
  DBEdit1.DataSource := DsTot3;
  DBEdit2.DataSource := DsTot3;
  DBEdit3.DataSource := DsTot3;
  {
    end;
    else Geral.MensagemBox('Tipo de pesquisa n�o definido!', 'Aviso',
    MB_OK + MB_ICONWARNING);
  end;
  }
  //
  Screen.Cursor := crDefault;
end;

procedure TFmInadimp.BtReparcClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmReparc, FmReparc, afmoNegarComAviso) then
  begin
    FmReparc.EdEmpresa.ValueVariant := EdEmpresa.ValueVariant;
    FmReparc.CBEmpresa.KeyValue := CBEmpresa.KeyValue;
    //
    FmReparc.EdPropriet.ValueVariant := EdPropriet.ValueVariant;
    FmReparc.CBPropriet.KeyValue := CBPropriet.KeyValue;
    //
    FmReparc.EdCondImov.ValueVariant := EdCondImov.ValueVariant;
    FmReparc.CBCondImov.KeyValue := CBCondImov.KeyValue;
    //
    FmReparc.ShowModal;
    FmReparc.Destroy;

    //
    //  Evitar erro caso empresa mudou!
    RefreshEmpresaSelecionada();
  end;
end;

{
procedure TFmInadimp.ComplementaQuery1(Query: TMySQLQuery);
var
  CondCod, CondEnt, Propriet, Apto: Integer;
begin
  CondCod := Geral.IMV(EdEmpresa.Text);
  if CondCod <> 0 then CondEnt := QrEmpresasCodEnti.Value else CondEnt := 0;
  Propriet := Geral.IMV(EdPropriet.Text);
  Apto := Geral.IMV(EdCondImov.Text);
  //
  if CondEnt > 0 then
    Query.SQL.Add('AND car.ForneceI=' + IntToStr(CondEnt));
  if Propriet > 0 then
    Query.SQL.Add('AND lan.ForneceI=' + IntToStr(Propriet));
  if Apto > 0 then
    Query.SQL.Add('AND lan.Depto=' + IntToStr(Apto));


  Query.SQL.Add(dmkPF.SQL_Mensal('lan.Mez', EdMezIni.Text, EdMezFim.Text,
    CkNaoMensais.Checked));

  Query.SQL.Add(dmkPF.SQL_Periodo('AND lan.Data ', TPIniDta.Date,
    TPFimDta.Date, CkIniDta.Checked, CkFimDta.Checked));

  Query.SQL.Add(dmkPF.SQL_Periodo('AND lan.Vencimento ', TPIniVct.Date,
    TPFimVct.Date, CkIniVct.Checked, CkFimVct.Checked));


  Query.SQL.Add('');
  Query.SQL.Add('');


end;
}

procedure TFmInadimp.ComplementaQuery3(Query: TMySQLQuery);
var
  C1, P1, A1, CondCod, CondEnt, Propriet, Apto: Integer;
  Txt, Lig: String;
begin
  Query.SQL.Add('FROM ' + FTabLctA + ' lan');
  Query.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  Query.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
  Query.SQL.Add('LEFT JOIN entidades emp ON emp.Codigo=car.ForneceI');
  Query.SQL.Add('LEFT JOIN entidades prp ON prp.Codigo=lan.ForneceI');
  Query.SQL.Add('');
  //
  //Query.SQL.Add('WHERE lan.FatID in (' + FmPrincipal.GetFatIDs + ')');
  case RGDocumentos.ItemIndex of
    0: Query.SQL.Add('WHERE lan.FatID in (600,601)');
    1: Query.SQL.Add('WHERE lan.FatID in (610)');
    2: Query.SQL.Add('WHERE lan.FatID in (600,601,610)');
  end;
  //
  // Reparcelados n�o s�o d�bitos
  Query.SQL.Add('AND lan.Reparcel=0');
  Query.SQL.Add('AND lan.Tipo=2');
  Query.SQL.Add('');

  //

  if PCAgrupamentos.ActivePageIndex = 0 then
  begin
    CondCod := Geral.IMV(EdEmpresa.Text);
    if CondCod <> 0 then
      CondEnt := DModG.QrEmpresasCodigo.Value
    else
      CondEnt := 0;
    Propriet := Geral.IMV(EdPropriet.Text);
    Apto := Geral.IMV(EdCondImov.Text);
    //
    if CondEnt > 0 then
      Query.SQL.Add('AND car.ForneceI=' + IntToStr(CondEnt));
    if Propriet > 0 then
      Query.SQL.Add('AND lan.ForneceI=' + IntToStr(Propriet));
    if Apto > 0 then
      Query.SQL.Add('AND lan.Depto=' + IntToStr(Apto));
  end else if (QrCondGriImv.State = dsBrowse) and (QrCondGriImv.RecordCount > 0) then begin
    Txt := 'AND (';
    QrCondGriImv.First;
    while not QrCondGriImv.Eof do
    begin
      if QrCondGriImv.RecNo > 1 then
        Txt := Txt + Chr(13) + Chr(10) + '  OR' + Chr(13) + Chr(10);
      //
      C1 := QrCondGriImvCond.Value;
      P1 := QrCondGriImvPropr.Value;
      A1 := QrCondGriImvApto.Value;
      Lig := '';
      if C1 <> 0 then
      begin
        Txt := Txt + 'imv.Codigo=' + IntToStr(C1);
        Lig := ' AND ';
      end;
      if P1 <> 0 then
      begin
        Txt := Txt + Lig + 'prp.Codigo=' + IntToStr(P1);
        Lig := ' AND ';
      end;
      if A1 <> 0 then
      begin
        Txt := Txt + Lig + 'imv.Conta=' + IntToStr(A1);
        //Lig := ' AND ';
      end;
      //
      Txt := Txt + Chr(13) + Chr(10);
      QrCondGriImv.Next;
    end;
    Txt := Txt + ')';
  end;
  Query.SQL.Add(Txt);
  //

  Query.SQL.Add(dmkPF.SQL_Mensal('lan.Mez', EdMezIni.Text, EdMezFim.Text,
    CkNaoMensais.Checked));

  Query.SQL.Add(dmkPF.SQL_Periodo('AND lan.Data ', TPIniDta.Date,
    TPFimDta.Date,
    CkIniDta.Checked and GBEmissao.Visible,
    CkFimDta.Checked and GBEmissao.Visible));

  Query.SQL.Add(dmkPF.SQL_Periodo('AND lan.Vencimento ', TPIniVct.Date,
    TPFimVct.Date,
    CkIniVct.Checked and GBVencto.Visible,
    CkFimVct.Checked and GBVencto.Visible));

  (* Data que deseja ver os d�bitos ou hoje se n�o definido*)
  if FTemDtP then
    Query.SQL.Add(
    'AND (' +
    '  (lan.Compensado > "' + FDtaPgt + '")' +
    '  OR (lan.Compensado < 2)' +
    '  )')
  else
    Query.SQL.Add(
    'AND ' +
    '   (lan.Compensado < 2)' +
    '  ');


  Query.SQL.Add('');
  Query.SQL.Add('');


end;

procedure TFmInadimp.ConfiguraOutroCondominio();
begin
  FTabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, DModG.QrEmpresasFilial.Value);
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
  ReopenPropriet(False);
  ReopenCondImov();
end;

procedure TFmInadimp.DefineCondImovPeloCodigo(Key: Integer);
var
  CondImov: Variant;
begin
  if Key = VK_F4 then
  begin
    CondImov := 0;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, CondImov, 0, 0,
    '', '', True, 'Reduzido', 'Informe o Reduzido: ', 0, CondImov) then
    begin
      if QrCondImov.Locate('Conta', CondImov, []) then
      begin
        EdCondImov.ValueVariant := CondImov;
        CBCondImov.KeyValue := CondImov;
      end else
        Geral.MB_Aviso('O im�vel c�digo ' + Geral.FF0(CondImov) +
        ' n�o existe ou n�o pertence ao condom�nio selecionado!');
    end;
  end else
  if key = VK_DELETE then
  begin
    EdCondImov.Text := '';
    CBCondImov.KeyValue := Null;
  end;
end;

procedure TFmInadimp.dmkDBGPesqDblClick(Sender: TObject);
begin
  MostraDiarioAdd();
end;

procedure TFmInadimp.Pendnciasdecondminos1Click(Sender: TObject);
begin
  SetaEImprimeFrx(frxPend_s0i0, 'Pend�ncias de cond�minos');
end;

procedure TFmInadimp.Pesquisa3();
  function OrdemPesq: String;
  begin
    Result := 'ORDER BY ' +
    Ordens[RGOrdem1.ItemIndex] + ',' +
    Ordens[RGOrdem2.ItemIndex] + ',' +
    Ordens[RGOrdem3.ItemIndex] + ',' +
    Ordens[RGOrdem4.ItemIndex];
  end;
begin
  (* Data que deseja ver os d�bitos ou hoje se n�o definido*)
  QrPesq3.Close;

  if CkFimPgt.Checked and GBVencto.Visible then
  begin
    FTemDtP := True;
    FDtaPgt := Geral.FDT(TPFimPgt.Date, 1);
    if RGSomas.ItemIndex = 1 then
    begin
      Geral.MensagemBox('N�o � poss�vel ao mesmo tempo somar ' +
      '(bloquetos) e selecionar uma data limite para pesquisa!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end else begin
    FTemDtP := False;
    FDtaPgt := Geral.FDT(Date, 1);
  end;
  //
  QrPesq3.SQL.Clear;
  QrPesq3.SQL.Add('SELECT imv.Codigo Empresa, imv.Propriet, imv.Unidade, ');
  QrPesq3.SQL.Add('imv.Conta Apto, imv.Imobiliaria, imv.Procurador, ');
  QrPesq3.SQL.Add('imv.Usuario, imv.Juridico, lan.Data, lan.CliInt,');
  case RGSomas.ItemIndex of
    0:
    begin
      QrPesq3.SQL.Add('lan.Credito, lan.Pago,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000, 2), 0) Juros,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 0) Multa,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito TOTAL,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago SALDO,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago <0,0,');
      QrPesq3.SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) PEND_VAL,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('CONCAT("Bloqueto n� ", FatNum) Descricao,');
      QrPesq3.SQL.Add('');
    end;
    1:
    begin
      QrPesq3.SQL.Add('SUM(lan.Credito) CREDITO, SUM(lan.Pago) PAGO,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000, 2), 0)) Juros,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 0)) Multa,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito) TOTAL,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) SALDO,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) <0,0,');
      QrPesq3.SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago)) PEND_VAL,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('CONCAT("Bloqueto n� ", FatNum) Descricao,');
      QrPesq3.SQL.Add('');
    end;
    else
    begin
      QrPesq3.SQL.Add('?credito?');
      QrPesq3.SQL.Add('?Descricao?');
    end;
  end;
  {
  if FTemDtP then
  begin
    //ver soma com GROUP BY
    QrPesq3.SQL.Add('');
    QrPesq3.SQL.Add('  (SELECT SUM(la3.Credito)');
    QrPesq3.SQL.Add('  FROM ' + FTabLctA + ' la3');
    QrPesq3.SQL.Add('  WHERE la3.ID_Pgto = lan.Controle');
    QrPesq3.SQL.Add('  AND la3.Data <= "' + FDtaPgt + '") PAGO,');
    QrPesq3.SQL.Add('');
    QrPesq3.SQL.Add('  lan.Credito - (SELECT SUM(la3.Credito)');
    QrPesq3.SQL.Add('  FROM ' + FTabLctA + ' la3');
    QrPesq3.SQL.Add('  WHERE la3.ID_Pgto = lan.Controle');
    QrPesq3.SQL.Add('  AND la3.Data <= "' + FDtaPgt + '") SALDO,');
    QrPesq3.SQL.Add('');
  end else begin
    QrPesq3.SQL.Add('');
    QrPesq3.SQL.Add('lan.Pago PAGO, IF(lan.Credito - lan.Pago > 0, lan.Credito - lan.Pago, 0) SALDO, ');
    QrPesq3.SQL.Add('');
  end;
  }
  QrPesq3.SQL.Add('lan.Mez, CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4)) MEZ_TXT,');
  QrPesq3.SQL.Add('lan.Vencimento, lan.Compensado, lan.Controle, lan.FatNum,');
  QrPesq3.SQL.Add('IF(lan.Vencimento=0, "", DATE_FORMAT(lan.Vencimento, "%d/%m/%y")) VCTO_TXT,');
  QrPesq3.SQL.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMPCOND,');
  QrPesq3.SQL.Add('IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPRPIMOV,');
  QrPesq3.SQL.Add('lan.Ativo, lan.Vencimento NewVencto'); // Para segunda via de bloqueto

  ComplementaQuery3(QrPesq3);

  if RGSituacao.ItemIndex = 1 then
    QrPesq3.SQL.Add('AND lan.Vencimento < SYSDATE()');

  case RGSomas.ItemIndex of
    0: QrPesq3.SQL.Add('');
    1: QrPesq3.SQL.Add('GROUP BY lan.ForneceI, lan.Depto, lan.FatNum, lan.Mez, lan.Vencimento');
    else QrPesq3.SQL.Add('?GROUP BY?');
  end;
  QrPesq3.SQL.Add(OrdemPesq);
  //
  UMyMod.AbreQuery(QrPesq3, Dmod.MyDB);
  //
  PnPesq1.Visible := True;
  //BtImprime.Enabled := True;
end;

procedure TFmInadimp.CBCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_DELETE then
  begin
    EdCondImov.Text := '';
    CBCondImov.KeyValue := Null;
  end;
end;

procedure TFmInadimp.QrTot3CalcFields(DataSet: TDataSet);
begin
  QrTot3SALDO.Value := QrTot3VALOR.Value - QrTot3PAGO.Value;
end;

procedure TFmInadimp.AlterapercentualdeJurosmensais1Click(Sender: TObject);
var
  PercNum: Double;
  PercTxt: String;
begin
  PercTxt := '1,00';
  if InputQuery('Novo Percentual de Juros Mensais',
  'Informe o novo percentual de juros mensais', PercTxt) then
  begin
    PercNum := Geral.DMV(PercTxt);
    PercTxt := Geral.FFT(PercNum, 2, siPositivo);
    if Geral.MensagemBox('Confirma a altera��o do percentual de juros ' +
    'mensais de todos itens pesquisados para ' + PercTxt + '%?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lanc tos SET MoraDia=:P0');
      Dmod.QrUpd.SQL.Add('WHERE CliInt=:Pa AND FatNum=:Pb AND Data=:Pc');
      }
      QrPesq3.First;
      while not QrPesq3.Eof do
      begin
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'MoraDia'], ['CliInt', 'FatNum', 'Data'], [PercNum], [
          QrPesq3CliInt.Value, QrPesq3FatNum.Value,
          Geral.FDT(QrPesq3Data.Value, 1)], True, '', FTabLctA);
        {
        Dmod.QrUpd.Params[00].AsFloat := PercNum;
        //
        Dmod.QrUpd.Params[01].AsInteger := QrPesq3CliInt.Value;
        Dmod.QrUpd.Params[02].AsFloat   := QrPesq3FatNum.Value;
        Dmod.QrUpd.Params[03].AsString  := Geral.FDT(QrPesq3Data.Value, 1);
        Dmod.QrUpd.ExecSQL;
        }
        QrPesq3.Next;
      end;
      QrPesq3.Close;
      //
      UMyMod.AbreQuery(QrPesq3, Dmod.MyDB);
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmInadimp.AlterapercentualdeMulta1Click(Sender: TObject);
var
  PercNum: Double;
  PercTxt: String;
begin
  PercTxt := '2,00';
  if InputQuery('Novo Percentual de Multa',
  'Informe o novo percentual multa:', PercTxt) then
  begin
    PercNum := Geral.DMV(PercTxt);
    PercTxt := Geral.FFT(PercNum, 2, siPositivo);
    if Geral.MensagemBox('Confirma a altera��o do percentual de ' +
    'multa de todos itens pesquisados para ' + PercTxt + '%?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Multa=:P0');
      Dmod.QrUpd.SQL.Add('WHERE CliInt=:Pa AND FatNum=:Pb AND Data=:Pc');
      }
      QrPesq3.First;
      while not QrPesq3.Eof do
      begin
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'MoraDia'], ['CliInt', 'FatNum', 'Data'], [PercNum], [
          QrPesq3CliInt.Value, QrPesq3FatNum.Value,
          Geral.FDT(QrPesq3Data.Value, 1)], True, '', FTabLctA);
        {
        Dmod.QrUpd.Params[00].AsFloat := PercNum;
        //
        Dmod.QrUpd.Params[01].AsInteger := QrPesq3CliInt.Value;
        Dmod.QrUpd.Params[02].AsFloat   := QrPesq3FatNum.Value;
        Dmod.QrUpd.Params[03].AsString  := Geral.FDT(QrPesq3Data.Value, 1);
        Dmod.QrUpd.ExecSQL;
        }
        QrPesq3.Next;
      end;
      QrPesq3.Close;
      //
      UMyMod.AbreQuery(QrPesq3, Dmod.MyDB);
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmInadimp.BtCertidaoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCertidaoNeg, FmCertidaoNeg, afmoNegarComAviso) then
  begin
    //
    FmCertidaoNeg.EdEmpresa.ValueVariant := EdEmpresa.ValueVariant;
    FmCertidaoNeg.CBEmpresa.KeyValue := CBEmpresa.KeyValue;
    //
    FmCertidaoNeg.EdPropriet.ValueVariant := EdPropriet.ValueVariant;
    FmCertidaoNeg.CBPropriet.KeyValue := CBPropriet.KeyValue;
    //
    FmCertidaoNeg.EdCondImov.ValueVariant := EdCondImov.ValueVariant;
    FmCertidaoNeg.CBCondImov.KeyValue := CBCondImov.KeyValue;
    //
    FmCertidaoNeg.ShowModal;
    FmCertidaoNeg.Destroy;
  end;
end;

procedure TFmInadimp.BtCobrancaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCobrarGer, FmCobrarGer, afmoNegarComAviso) then
  begin
    FmCobrarGer.ShowModal;
    FmCobrarGer.Destroy;
    //
    //  Evitar erro caso empresa mudou!
    RefreshEmpresaSelecionada();
  end;
end;

procedure TFmInadimp.BtDiarioAddClick(Sender: TObject);
begin
  MostraDiarioAdd();
end;

procedure TFmInadimp.BtMulJurClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMulJur, BtMulJur);
end;

procedure TFmInadimp.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmInadimp.Segundaviadebloquetos1Click(Sender: TObject);
var
  NivelJuridico: Integer;
  Quando: String;
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
  'Informe o condom�nio e pesquise novamente!') then
    Exit;
  //
  // 2012-05-16  - Impedir impress�o de protestados e com a��o de cobran�a
  {
  if DBCheck.CriaFm(TFmInadimpBloq, FmInadimpBloq, afmoNegarComAviso) then
  begin
    FmInadimpBloq.FTabLctA := FTabLctA;
    UCriar.RecriaTempTable('BloqInad', DModG.QrUpdPID1, False);
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add(DELETE_FROM + VAR_MyPID_DB_NOME + '.bloqinad;');
    Dmod.QrAux.SQL.Add('INSERT INTO ' + VAR_MyPID_DB_NOME + '.bloqinad ');
    Dmod.QrAux.SQL.Add(QrPesq3.SQL.Text);
    Dmod.QrAux.Params := QrPesq3.Params;
    Dmod.QrAux.ExecSQL;
    //
    FmInadimpBloq.ShowModal;
    FmInadimpBloq.Destroy;
  end;
  }
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando impedimentos de impress�o');
  UnCreateApp.RecriaTempTableNovo(ntrtt_Inadimp, DModG.QrUpdPID1, False, 1, 'BloqInad');
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add(DELETE_FROM + VAR_MyPID_DB_NOME + '.bloqinad;');
  Dmod.QrAux.SQL.Add('INSERT INTO ' + VAR_MyPID_DB_NOME + '.bloqinad ');
  Dmod.QrAux.SQL.Add(QrPesq3.SQL.Text);
  Dmod.QrAux.Params := QrPesq3.Params;
  Dmod.QrAux.ExecSQL;
  //
  QrBloqInad.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(QrBloqInad, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM bloqinad ',
  'WHERE Juridico > 0 ',
  '']);
  if QrBloqInad.RecordCount > 0 then
  begin
    NivelJuridico := MyObjects.SelRadioGroup('Situa��o Jur�dica Detectada!',
    'Mostrar os boletos pesquisados dos cond�minos:', ['Sem restri��o',
    'Sem restri��o ou n�o vencidos (precisa de SENHA)',
    'TODOS (precisa de SENHA)'], 1);
    if NivelJuridico < 0 then
      Exit;
    if NivelJuridico > 0 then
      if not DBCheck.LiberaPelaSenhaBoss('SITUA��O JUR�DICA EXIGE SENHA!') then Exit;
  end else
    NivelJuridico := 0;
  //
{   N�VEL JUR�DICO
0 - Sem restri��o',
1 - Sem restri��o ou n�o vencidos (precisa de SENHA)',
2 - TODOS (precisa de SENHA)']
}
  case NivelJuridico of
    0: Quando := 'WHERE Juridico=0';
    1: Quando := 'WHERE Juridico=0 OR Vencimento >= SYSDATE()';
    else Quando := '';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrBloqInad, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM bloqinad ',
  Quando,
  '']);
  if QrBloqInad.RecordCount > 0 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    if DBCheck.CriaFm(TFmInadimpBloq, FmInadimpBloq, afmoNegarComAviso) then
    begin
      FmInadimpBloq.FTabLctA := FTabLctA;
      FmInadimpBloq.FNivelJuridico := NivelJuridico;
      FmInadimpBloq.ShowModal;
      FmInadimpBloq.Destroy;
    end;
  end else
    Geral.MensagemBox('Nenhum bloqueto passou pelo filtro jur�dico!',
    'Aviso', MB_OK+MB_ICONWARNING);
  // FIM 2012-05-16
end;

procedure TFmInadimp.SetaEImprimeFrx(Frx: TfrxReport; Titulo: String);
  function Footer(Indice: Integer): String;
  begin
    Result := 'Sub-total ' + Headers[Indice];
  end;
begin
  frx.Variables['LogoAdmiExiste']  := FileExists(Dmod.QrControleMyLogo.Value);
  frx.Variables['LogoAdmiCaminho'] := QuotedStr(Dmod.QrControleMyLogo.Value);
  frx.Variables['VARF_MAXGRUPO'] := RGGrupos.ItemIndex;

  frx.Variables['VARF_GRUPO1']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR1']   := ''''+Headers[RGOrdem1.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem1.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR1']   := ''''+Footer(RGOrdem1.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  frx.Variables['VARF_GRUPO2']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem2.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR2']   := ''''+Headers[RGOrdem2.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem2.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR2']   := ''''+Footer(RGOrdem2.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem2.ItemIndex]+'"]''';

  frx.Variables['VARF_GRUPO3']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem3.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR3']   := ''''+Headers[RGOrdem3.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem3.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR3']   := ''''+Footer(RGOrdem1.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  frx.Variables['VARF_GRUPO4']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem4.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR4']   := ''''+Headers[RGOrdem4.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem4.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR4']   := ''''+Footer(RGOrdem1.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  case RGSomas.ItemIndex of
    0: frx.Variables['VARF_ID_VAL'] := '''[frxDsPesq3."Controle"]''';
    1: frx.Variables['VARF_ID_VAL'] := '''[frxDsPesq3."FatNum"]''';
    else frx.Variables['VARF_ID_VAL'] := '';
  end;
    
  MyObjects.frxMostra(frx, Titulo);
end;

procedure TFmInadimp.frxPend_s0i0GetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO_VCT') = 0 then
    Value := MLAGeral.PeriodoImp2(TPIniVct.Date, TPFimVct.Date,
      CkIniVct.Checked, CkFimVct.Checked, 'Per�odo de vencimento: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_PERIODO_DTA') = 0 then
    Value := MLAGeral.PeriodoImp2(TPIniDta.Date, TPFimDta.Date,
      CkIniDta.Checked, CkFimDta.Checked, 'Per�odo de emiss�o: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_PERIODO_PSQ') = 0 then
    Value := MLAGeral.PeriodoImp2(0, TPFimPgt.Date,
      False, CkFimPgt.Checked, 'Data da pesquisa: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_PERIODO_CPT') = 0 then
    Value := MLAGeral.PeriodoImpMezAno(EdMezIni.Text, EdMezFim.Text,
      'Per�odo de compet�ncia: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_ID_TIT') = 0 then
  begin
    case RGSomas.ItemIndex of
      0: Value := 'Controle';
      1: Value := 'Bloqueto';
      else Value := '';
    end;
  end
  else if AnsiCompareText(VarName, 'VARF_TITULO') = 0 then
  begin
    case RGSituacao.ItemIndex of
      0: Value := 'TODOS VALORES ABERTOS DE COND�MINOS';
      1: Value := 'PEND�NCIAS DE COND�MINOS';
      else Value := RGSituacao.Items[RGSituacao.ItemIndex];
    end;
  end
  else begin
    case PCAgrupamentos.ActivePageIndex of
      0:
      begin
        if AnsiCompareText(VarName, 'VARF_EMPRESA') = 0 then
          Value := dmkPF.ParValueCodTxt('Cliente: ', CBEmpresa.Text, Null)
        else if AnsiCompareText(VarName, 'VARF_UH') = 0 then
          Value := dmkPF.ParValueCodTxt(DModG.ReCaptionTexto(VAR_U_H) + ': ', CBCondImov.Text, Null)
        else if AnsiCompareText(VarName, 'VARF_CONDOMINO') = 0 then
          Value := dmkPF.ParValueCodTxt(DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) + ': ', CBPropriet.Text, Null)
      end;
      1:
      begin
        case RGSomas.ItemIndex of
          0:
          begin
            if AnsiCompareText(VarName, 'VARF_EMPRESA') = 0 then
              Value := dmkPF.ParValueCodTxt('Cliente: ', CBEmpresa.Text, Null)
            else if AnsiCompareText(VarName, 'VARF_UH') = 0 then
              Value := dmkPF.ParValueCodTxt(DModG.ReCaptionTexto(VAR_U_H) + ': ', CBCondImov.Text, Null)
            else if AnsiCompareText(VarName, 'VARF_CONDOMINO') = 0 then
              Value := dmkPF.ParValueCodTxt(DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) + ': ', CBPropriet.Text, Null)
          end;
          1:
          begin
            if AnsiCompareText(VarName, 'VARF_EMPRESA') = 0 then
              Value := dmkPF.ParValueCodTxt('Grupo de pesquisa: ', CBCondGri.Text, Null)
            else if AnsiCompareText(VarName, 'VARF_UH') = 0 then
              Value := ''
            else if AnsiCompareText(VarName, 'VARF_CONDOMINO') = 0 then
              Value := ''
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmInadimp.MostraDiarioAdd();
const
  UsaDataHora = False;
  Data        = 0;
  Hora        = 0;
  DiarioAss   = 0;
  Texto       = '';
var
  CliInt, Entidade1, Entidade2, Depto: Integer;
begin
  if DBCheck.CriaFm(TFmDiarioAdd, FmDiarioAdd, afmoNegarComAviso) then
  begin
    FmDiarioAdd.FChamou        := -1;
    FmDiarioAdd.ImgTipo.SQLType := stIns;
    //
    CliInt    := QrPesq3Empresa.Value;
    Entidade1 := QrPesq3Propriet.Value;
    Entidade2 := QrPesq3Usuario.Value;
    Depto     := QrPesq3Apto.Value;
    //
    FmDiarioAdd.PreencheDados(UsaDataHora, Data, Hora, DiarioAss, CliInt,
      Depto, Entidade1, Entidade2, Texto);
    if QrPesq3Imobiliaria.Value > 0 then
    begin
      FmDiarioAdd.FProcurador := QrPesq3Imobiliaria.Value;
      FmDiarioAdd.FNomeTipoPRC := 'Imobili�ria';
    end else
    if QrPesq3Procurador.Value > 0 then
    begin
      FmDiarioAdd.FProcurador := QrPesq3Procurador.Value;
      FmDiarioAdd.FNomeTipoPRC := 'Procurador';
    end else FmDiarioAdd.FProcurador := 0;
    FmDiarioAdd.ShowModal;
    FmDiarioAdd.Destroy;
  end;
end;

procedure TFmInadimp.RGOrdem1Click(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp.RGOrdem2Click(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp.RGOrdem3Click(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp.RGOrdem4Click(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp.EdCondGriChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
  ReopenCondGriImv(0);
end;

procedure TFmInadimp.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCondGri, FmCondGri, afmoNegarComAviso) then
  begin
    FmCondGri.ShowModal;
    FmCondGri.Destroy;
  end;
  QrCondGri.Close;
  //
  UMyMod.AbreQuery(QrCondGri, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
  begin
    EdCondGri.Text := IntToStr(VAR_CADASTRO);
    CBCondGri.KeyValue := VAR_CADASTRO;
  end;
end;

procedure TFmInadimp.EdProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmInadimp.CBProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

// Evitar erro caso empresa mudou!
procedure TFmInadimp.RefreshEmpresaSelecionada();
var
  Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  EdEmpresa.ValueVariant := 0;
  CBEmpresa.KeyValue := Null;
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue := Empresa;
end;

procedure TFmInadimp.ReopenCondGriImv(Controle: Integer);
begin
  QrCondGriImv.Close;
  QrCondGriImv.Params[0].AsInteger := QrCondGriCodigo.Value;
  //
  UMyMod.AbreQuery(QrCondGriImv, Dmod.MyDB);
end;

procedure TFmInadimp.QrPesq3AfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrPesq3.RecordCount > 0;
  BtMulJur.Enabled := QrPesq3.RecordCount > 0;
end;

procedure TFmInadimp.QrPesq3AfterScroll(DataSet: TDataSet);
var
  Depto, FatNum, Mez, Vencto: String;
begin
  Depto  := FormatFloat('0', QrPesq3Apto.Value);
  FatNum := FormatFloat('0', QrPesq3FatNum.Value);
  Mez    := FormatFloat('0', QrPesq3Mez.Value);
  Vencto := Geral.FDT(QrPesq3Vencimento.Value, 1);
  //
  QrIts3.Close;
  QrIts3.SQL.Clear;
  QrIts3.SQL.Add('SELECT lct.Genero, lct.Descricao, lct.Credito, lct.Controle ');
  QrIts3.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrIts3.SQL.Add('WHERE lct.Depto=' + Depto);
  QrIts3.SQL.Add('AND lct.FatNum=' + FatNum);
  QrIts3.SQL.Add('AND lct.Mez=' + Mez);
  QrIts3.SQL.Add('AND lct.Vencimento="' + Vencto + '"');
  UMyMod.AbreQuery(QrIts3, Dmod.MyDB);
end;

procedure TFmInadimp.QrPesq3BeforeClose(DataSet: TDataSet);
begin
  QrIts3.Close;
  BtImprime.Enabled := False;
end;

procedure TFmInadimp.QrPesq3CalcFields(DataSet: TDataSet);
begin
  QrPesq3Juridico_TXT.Value := dmkPF.DefineJuricoSigla(QrPesq3Juridico.Value);
  QrPesq3Juridico_DESCRI.Value := dmkPF.DefineJuricoSigla(QrPesq3Juridico.Value);
end;

end.

