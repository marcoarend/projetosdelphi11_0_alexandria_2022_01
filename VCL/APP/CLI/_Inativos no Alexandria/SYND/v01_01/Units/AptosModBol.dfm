object FmAptosModBol: TFmAptosModBol
  Left = 339
  Top = 185
  Caption = 'CAD-CONDO-009 :: Sele'#231#227'o de UHs - Modelo e Config. Bloq.'
  ClientHeight = 589
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 427
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 256
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object RGModelBloq: TRadioGroup
        Left = 0
        Top = 0
        Width = 784
        Height = 147
        Align = alClient
        Caption = ' Modelo de impress'#227'o do bloqueto: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'FmPrincipal.PreencheModelosBloq')
        TabOrder = 0
      end
      object GBConfig: TGroupBox
        Left = 0
        Top = 147
        Width = 784
        Height = 45
        Align = alBottom
        Caption = ' Configura'#231#227'o do modelo H ou R: '
        TabOrder = 1
        object SpeedButton2: TSpeedButton
          Left = 749
          Top = 18
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object EdConfigBol: TdmkEditCB
          Left = 6
          Top = 18
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBConfigBol
          IgnoraDBLookupComboBox = False
        end
        object CBConfigBol: TdmkDBLookupComboBox
          Left = 68
          Top = 18
          Width = 678
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsConfigBol
          TabOrder = 1
          dmkEditCB = EdConfigBol
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object RGCompe: TRadioGroup
        Left = 0
        Top = 192
        Width = 784
        Height = 37
        Align = alBottom
        Caption = ' Ficha de compensa'#231#227'o (modelo E): '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '??'
          '1 (uma via)'
          '2 (duas vias)')
        TabOrder = 2
      end
      object Panel4: TPanel
        Left = 0
        Top = 229
        Width = 784
        Height = 27
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 3
        object CkBalAgrMens: TCheckBox
          Left = 6
          Top = 6
          Width = 407
          Height = 17
          Caption = 
            'Separar as somas de valores no balancete por compet'#234'ncia ao agru' +
            'par valores.'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
      end
    end
    object DBGrid1: TdmkDBGrid
      Left = 0
      Top = 256
      Width = 784
      Height = 171
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEBLOCO'
          Title.Caption = 'Bloco'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Andar'
          Width = 34
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPROPR'
          Title.Caption = 'Propriet'#225'rio'
          Width = 468
          Visible = True
        end>
      Color = clWindow
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEBLOCO'
          Title.Caption = 'Bloco'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Andar'
          Width = 34
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPROPR'
          Title.Caption = 'Propriet'#225'rio'
          Width = 468
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 497
        Height = 32
        Caption = 'Sele'#231#227'o de UHs - Modelo e Config. Bloq.'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 497
        Height = 32
        Caption = 'Sele'#231#227'o de UHs - Modelo e Config. Bloq.'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 497
        Height = 32
        Caption = 'Sele'#231#227'o de UHs - Modelo e Config. Bloq.'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 475
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 519
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrUHs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT imv.Conta Apto, imv.Andar, imv.Unidade, blc.Descri NOMEBL' +
        'OCO,'
      'IF(prp.Tipo=0,prp.RazaoSocial,prp.Nome) NOMEPROPR'#13
      'FROM condimov imv'
      'LEFT JOIN condbloco blc ON blc.Controle=imv.Controle'
      'LEFT JOIN entidades prp ON prp.Codigo=imv.Propriet'
      'WHERE imv.Codigo=:P0'
      'ORDER BY blc.Ordem, imv.Andar, imv.Unidade')
    Left = 68
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUHsApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrUHsUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrUHsNOMEBLOCO: TWideStringField
      FieldName = 'NOMEBLOCO'
      Size = 100
    end
    object QrUHsNOMEPROPR: TWideStringField
      FieldName = 'NOMEPROPR'
      Size = 100
    end
    object QrUHsAndar: TIntegerField
      FieldName = 'Andar'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 40
    Top = 12
  end
  object Query: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    Left = 12
    Top = 12
    object QueryNOMEBLOCO: TWideStringField
      FieldName = 'NOMEBLOCO'
      Size = 100
    end
    object QueryAndar: TIntegerField
      FieldName = 'Andar'
      Required = True
    end
    object QueryUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QueryNOMEPROPR: TWideStringField
      FieldName = 'NOMEPROPR'
      Size = 100
    end
    object QueryApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QueryAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object QrConfigBol: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM configbol'
      'ORDER BY Nome')
    Left = 716
    Top = 12
    object QrConfigBolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigBolNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsConfigBol: TDataSource
    DataSet = QrConfigBol
    Left = 744
    Top = 12
  end
end
