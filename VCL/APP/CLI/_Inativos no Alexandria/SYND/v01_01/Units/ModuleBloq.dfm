object DmBloq: TDmBloq
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 618
  Width = 1013
  object QrBolLei: TmySQLQuery
    Database = DModG.DBDmk
    Left = 384
    Top = 352
    object QrBolLeiValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
    object QrBolLeiApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolLeiBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolLeiBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrBolArr: TmySQLQuery
    Database = DModG.DBDmk
    Left = 384
    Top = 404
    object QrBolArrValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
    object QrBolArrApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolArrBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolArrBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrUsers: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT usu.CodigoEsp, usu.Username, usu.Password'
      'FROM condimov imv'
      'LEFT JOIN users usu ON usu.CodigoEsp = imv.Conta'
      'WHERE imv.Codigo=:P0')
    Left = 448
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsersCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
      Origin = 'users.CodigoEsp'
    end
    object QrUsersUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'users.Username'
      Size = 32
    end
    object QrUsersPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'users.Password'
      Size = 32
    end
  end
  object QrPPI: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT ppi.Conta PROTOCOLO, ppi.Controle LOTE, ppi.DataE, '
      
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DATAE_TX' +
        'T,'
      
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DATAD_TX' +
        'T,'
      'ppi.DataD, ppi.Cancelado, ppi.Motivo, ppi.ID_Cod1, ppi.ID_Cod2, '
      'ppi.ID_Cod3, ppi.ID_Cod4, ppi.CliInt, ppi.Cliente, ppi.Periodo,'
      'ppi.Docum, ptc.Nome TAREFA, ptc.Def_Sender,'
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN entidades  snd ON snd.Codigo=ptc.Def_Sender'
      'WHERE ppi.Link_ID=1'
      'AND ppi.ID_Cod1=:P0'
      'AND ppi.ID_Cod2=:P1')
    Left = 384
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPPIPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'protpakits.Conta'
      Required = True
    end
    object QrPPIDataE: TDateField
      FieldName = 'DataE'
      Origin = 'protpakits.DataE'
      Required = True
    end
    object QrPPIDataD: TDateField
      FieldName = 'DataD'
      Origin = 'protpakits.DataD'
      Required = True
    end
    object QrPPICancelado: TIntegerField
      FieldName = 'Cancelado'
      Origin = 'protpakits.Cancelado'
      Required = True
    end
    object QrPPIMotivo: TIntegerField
      FieldName = 'Motivo'
      Origin = 'protpakits.Motivo'
      Required = True
    end
    object QrPPIID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Origin = 'protpakits.ID_Cod1'
      Required = True
    end
    object QrPPIID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Origin = 'protpakits.ID_Cod2'
      Required = True
    end
    object QrPPIID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Origin = 'protpakits.ID_Cod3'
      Required = True
    end
    object QrPPIID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Origin = 'protpakits.ID_Cod4'
      Required = True
    end
    object QrPPITAREFA: TWideStringField
      FieldName = 'TAREFA'
      Origin = 'protocolos.Nome'
      Size = 100
    end
    object QrPPIDELIVER: TWideStringField
      FieldName = 'DELIVER'
      Size = 100
    end
    object QrPPILOTE: TIntegerField
      FieldName = 'LOTE'
      Origin = 'protpakits.Controle'
      Required = True
    end
    object QrPPICliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'protpakits.CliInt'
      Required = True
    end
    object QrPPICliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'protpakits.Cliente'
      Required = True
    end
    object QrPPIPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'protpakits.Periodo'
      Required = True
    end
    object QrPPIDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 10
    end
    object QrPPIDATAD_TXT: TWideStringField
      FieldName = 'DATAD_TXT'
      Size = 10
    end
    object QrPPIDef_Sender: TIntegerField
      FieldName = 'Def_Sender'
      Origin = 'protocolos.Def_Sender'
    end
    object QrPPIDocum: TFloatField
      FieldName = 'Docum'
      Origin = 'protpakits.Docum'
      Required = True
    end
  end
  object QrBoletos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrBoletosBeforeClose
    AfterScroll = QrBoletosAfterScroll
    OnCalcFields = QrBoletosCalcFields
    Left = 448
    Top = 352
    object QrBoletosApto: TIntegerField
      FieldName = 'Apto'
      Origin = 'Apto'
      Required = True
    end
    object QrBoletosUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'Unidade'
      Size = 10
    end
    object QrBoletosSUB_ARR: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_ARR'
      LookupDataSet = QrBolArr
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBoletosSUB_LEI: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_LEI'
      LookupDataSet = QrBolLei
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBoletosSUB_TOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrBoletosVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBoletosPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'Propriet'
      Required = True
    end
    object QrBoletosNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Origin = 'NOMEPROPRIET'
      Size = 100
    end
    object QrBoletosBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBoletosVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrBoletosUSERNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'USERNAME'
      LookupDataSet = QrUsers
      LookupKeyFields = 'CodigoEsp'
      LookupResultField = 'Username'
      KeyFields = 'Apto'
      Size = 100
      Lookup = True
    end
    object QrBoletosPASSWORD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'PASSWORD'
      LookupDataSet = QrUsers
      LookupKeyFields = 'CodigoEsp'
      LookupResultField = 'Password'
      KeyFields = 'Apto'
      Size = 100
      Lookup = True
    end
    object QrBoletosPWD_WEB: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PWD_WEB'
      Size = 255
      Calculated = True
    end
    object QrBoletosPROTOCOLO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'PROTOCOLO'
      LookupDataSet = QrPPI
      LookupKeyFields = 'Docum'
      LookupResultField = 'PROTOCOLO'
      KeyFields = 'Boleto'
      DisplayFormat = '00000;-000000; '
      Lookup = True
    end
    object QrBoletosAndar: TIntegerField
      FieldName = 'Andar'
      Origin = 'Andar'
    end
    object QrBoletosBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'Boleto'
      Required = True
    end
    object QrBoletosBLOQUETO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'BLOQUETO'
      Calculated = True
    end
    object QrBoletosKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrBoletosLOTE_PROTOCO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LOTE_PROTOCO'
      LookupDataSet = QrPPI
      LookupKeyFields = 'Docum'
      LookupResultField = 'LOTE'
      KeyFields = 'Boleto'
      Lookup = True
    end
    object QrBoletosOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrBoletosFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
    end
    object QrBoletosJuridico: TSmallintField
      FieldName = 'Juridico'
    end
    object QrBoletosJuridico_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Juridico_TXT'
      Size = 2
      Calculated = True
    end
    object QrBoletosJuridico_DESCRI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Juridico_DESCRI'
      Size = 50
      Calculated = True
    end
    object QrBoletosCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
  end
  object DsBoletos: TDataSource
    DataSet = QrBoletos
    Left = 508
    Top = 352
  end
  object QrBoletosIts: TmySQLQuery
    Database = DModG.DBDmk
    OnCalcFields = QrBoletosItsCalcFields
    Left = 448
    Top = 400
    object QrBoletosItsTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Required = True
      Size = 50
    end
    object QrBoletosItsVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBoletosItsVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBoletosItsTEXTO_IMP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO_IMP'
      Size = 255
      Calculated = True
    end
    object QrBoletosItsMedAnt: TFloatField
      FieldName = 'MedAnt'
    end
    object QrBoletosItsMedAtu: TFloatField
      FieldName = 'MedAtu'
    end
    object QrBoletosItsConsumo: TFloatField
      FieldName = 'Consumo'
    end
    object QrBoletosItsCasas: TLargeintField
      FieldName = 'Casas'
    end
    object QrBoletosItsUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Size = 5
    end
    object QrBoletosItsUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Size = 5
    end
    object QrBoletosItsUnidFat: TFloatField
      FieldName = 'UnidFat'
    end
    object QrBoletosItsTipo: TLargeintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrBoletosItsVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrBoletosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBoletosItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrBoletosItsGeraTyp: TLargeintField
      FieldName = 'GeraTyp'
    end
    object QrBoletosItsGeraFat: TFloatField
      FieldName = 'GeraFat'
    end
    object QrBoletosItsCasRat: TLargeintField
      FieldName = 'CasRat'
    end
    object QrBoletosItsNaoImpLei: TLargeintField
      FieldName = 'NaoImpLei'
    end
    object QrBoletosItsTabelaOrig: TWideStringField
      FieldName = 'TabelaOrig'
      Size = 10
    end
    object QrBoletosItsGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object DsBoletosIts: TDataSource
    DataSet = QrBoletosIts
    Left = 508
    Top = 400
  end
  object QrBLE: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrBLECalcFields
    SQL.Strings = (
      'SELECT * FROM lanctos')
    Left = 744
    Top = 404
    object QrBLECOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrBLEData: TDateField
      FieldName = 'Data'
    end
    object QrBLETipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrBLECarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrBLEControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBLESub: TSmallintField
      FieldName = 'Sub'
    end
    object QrBLEAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrBLEGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBLEQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrBLEDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBLENotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrBLEDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrBLECredito: TFloatField
      FieldName = 'Credito'
    end
    object QrBLECompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrBLEDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrBLESit: TIntegerField
      FieldName = 'Sit'
    end
    object QrBLEVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrBLEFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrBLEFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrBLEFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBLEFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrBLEID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrBLEID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrBLEFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrBLEEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrBLEBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrBLEContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrBLECNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrBLELocal: TIntegerField
      FieldName = 'Local'
    end
    object QrBLECartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrBLELinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrBLEOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrBLELancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrBLEPago: TFloatField
      FieldName = 'Pago'
    end
    object QrBLEMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrBLEFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrBLECliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrBLECliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrBLEForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrBLEMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrBLEMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrBLEProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrBLEDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrBLECtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrBLENivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrBLEVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrBLEAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrBLEICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrBLEICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrBLEDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrBLEDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrBLEDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrBLEDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrBLEDescoControle: TIntegerField
      FieldName = 'DescoControle'
    end
    object QrBLEUnidade: TIntegerField
      FieldName = 'Unidade'
    end
    object QrBLENFVal: TFloatField
      FieldName = 'NFVal'
    end
    object QrBLEAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrBLEExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrBLELk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBLEDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBLEDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBLEUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBLEUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBLESerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrBLEDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrBLEMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrBLEMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrBLECNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrBLETipoCH: TSmallintField
      FieldName = 'TipoCH'
    end
    object QrBLEAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrBLEReparcel: TIntegerField
      FieldName = 'Reparcel'
    end
    object QrBLEID_Quit: TIntegerField
      FieldName = 'ID_Quit'
    end
    object QrBLEAtrelado: TIntegerField
      FieldName = 'Atrelado'
    end
    object QrBLEAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBLEPagMul: TFloatField
      FieldName = 'PagMul'
    end
    object QrBLEPagJur: TFloatField
      FieldName = 'PagJur'
    end
    object QrBLESubPgto1: TIntegerField
      FieldName = 'SubPgto1'
    end
    object QrBLEMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrBLEProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrBLECtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrBLESerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrBLEEndossas: TSmallintField
      FieldName = 'Endossas'
    end
    object QrBLEEndossan: TFloatField
      FieldName = 'Endossan'
    end
    object QrBLEEndossad: TFloatField
      FieldName = 'Endossad'
    end
    object QrBLECancelado: TSmallintField
      FieldName = 'Cancelado'
    end
    object QrBLEEventosCad: TIntegerField
      FieldName = 'EventosCad'
    end
    object QrBLEEncerrado: TIntegerField
      FieldName = 'Encerrado'
    end
    object QrBLEErrCtrl: TIntegerField
      FieldName = 'ErrCtrl'
    end
    object QrBLEAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object QrBLC: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrBLCCalcFields
    Left = 744
    Top = 452
    object QrBLCCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrBLCFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBLCData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBLCTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrBLCCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrBLCControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBLCSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrBLCAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrBLCGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBLCQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrBLCDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBLCNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrBLCDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrBLCCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBLCCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBLCDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrBLCSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrBLCVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBLCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrBLCFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrBLCFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrBLCID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrBLCID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrBLCFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrBLCEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrBLCBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrBLCContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrBLCCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrBLCLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrBLCCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrBLCLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrBLCOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrBLCLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrBLCPago: TFloatField
      FieldName = 'Pago'
    end
    object QrBLCMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrBLCFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrBLCCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrBLCCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrBLCForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrBLCMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrBLCMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrBLCProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrBLCDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrBLCCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrBLCNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrBLCVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrBLCAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrBLCICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrBLCICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrBLCDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrBLCDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrBLCDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrBLCDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrBLCDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrBLCUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrBLCNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrBLCAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrBLCExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrBLCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBLCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBLCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBLCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBLCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBLCSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrBLCDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrBLCMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrBLCMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrBLCCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrBLCTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrBLCAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrBLCReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrBLCID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrBLCAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrBLCAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrBLCPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrBLCPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrBLCSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrBLCMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrBLCProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrBLCCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
      Required = True
    end
    object QrBLCAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsBLC: TDataSource
    DataSet = QrBLC
    Left = 804
    Top = 452
  end
  object DsBLE: TDataSource
    DataSet = QrBLE
    Left = 804
    Top = 404
  end
  object QrComposLIts: TmySQLQuery
    Database = DModG.DBDmk
    Left = 508
    Top = 148
    object QrComposLItsApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrComposLItsMedAnt: TFloatField
      FieldName = 'MedAnt'
      Required = True
    end
    object QrComposLItsMedAtu: TFloatField
      FieldName = 'MedAtu'
      Required = True
    end
    object QrComposLItsConsumo: TFloatField
      FieldName = 'Consumo'
      Required = True
    end
    object QrComposLItsNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrComposLItsUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrComposLItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrComposLItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrComposLItsBoleto: TFloatField
      FieldName = 'Boleto'
    end
    object QrComposLItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
  end
  object QrComposL: TmySQLQuery
    Database = DModG.DBDmk
    BeforeClose = QrComposLBeforeClose
    AfterScroll = QrComposLAfterScroll
    Left = 508
    Top = 100
    object QrComposLCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrComposLNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrComposLVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrComposLCasas: TSmallintField
      FieldName = 'Casas'
    end
  end
  object QrComposAIts: TmySQLQuery
    Database = DModG.DBDmk
    Left = 508
    Top = 52
    object QrComposAItsNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrComposAItsUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrComposAItsApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrComposAItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrComposAItsPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrComposAItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrComposAItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrComposAItsBoleto: TFloatField
      FieldName = 'Boleto'
    end
  end
  object QrComposA: TmySQLQuery
    Database = DModG.DBDmk
    BeforeClose = QrComposABeforeClose
    AfterScroll = QrComposAAfterScroll
    Left = 508
    Top = 4
    object QrComposATEXTO: TWideStringField
      FieldName = 'TEXTO'
      Required = True
      Size = 50
    end
    object QrComposAVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrComposAConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrComposAArreBaI: TIntegerField
      FieldName = 'ArreBaI'
      Required = True
    end
  end
  object DsComposA: TDataSource
    DataSet = QrComposA
    Left = 564
    Top = 4
  end
  object DsComposAIts: TDataSource
    DataSet = QrComposAIts
    Left = 564
    Top = 52
  end
  object DsComposL: TDataSource
    DataSet = QrComposL
    Left = 564
    Top = 100
  end
  object DsComposLIts: TDataSource
    DataSet = QrComposLIts
    Left = 564
    Top = 148
  end
  object QrInquilino: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrInquilinoCalcFields
    SQL.Strings = (
      'SELECT imv.Unidade, imv.BloqEndTip, blc.PrefixoUH,'
      'imv.Usuario, cnd.Cliente, imv.Propriet,'
      'imv.EnderNome, imv.EnderLin1, imv.EnderLin2,'
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF     END CNPJ' +
        '_CPF_USUARIO,'
      'ELT(en.Tipo+1, "CNPJ", "CPF") NOMETIPO_USUARIO,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome    END NO_U' +
        'SUARIO'
      'FROM condimov imv'
      'LEFT JOIN cond cnd ON cnd.Codigo=imv.Codigo'
      'LEFT JOIN condbloco blc ON imv.Controle=blc.Controle'
      'LEFT JOIN entidades en ON en.Codigo=imv.Usuario'
      'WHERE imv.Conta=:P0')
    Left = 508
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrInquilinoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrInquilinoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrInquilinoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrInquilinoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrInquilinoE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrInquilinoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrInquilinoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrInquilinoPROPRI_E_MORADOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PROPRI_E_MORADOR'
      Size = 255
      Calculated = True
    end
    object QrInquilinoUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrInquilinoPrefixoUH: TWideStringField
      FieldName = 'PrefixoUH'
    end
    object QrInquilinoLNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LNR'
      Size = 255
      Calculated = True
    end
    object QrInquilinoLN2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LN2'
      Size = 255
      Calculated = True
    end
    object QrInquilinoBloqEndTip: TSmallintField
      FieldName = 'BloqEndTip'
    end
    object QrInquilinoUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrInquilinoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrInquilinoPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrInquilinoEnderLin1: TWideStringField
      FieldName = 'EnderLin1'
      Size = 100
    end
    object QrInquilinoEnderLin2: TWideStringField
      FieldName = 'EnderLin2'
      Size = 100
    end
    object QrInquilinoEnderNome: TWideStringField
      FieldName = 'EnderNome'
      Size = 100
    end
    object QrInquilinoCNPJ_CPF_USUARIO: TWideStringField
      FieldName = 'CNPJ_CPF_USUARIO'
      Size = 18
    end
    object QrInquilinoNOMETIPO_USUARIO: TWideStringField
      FieldName = 'NOMETIPO_USUARIO'
      Size = 4
    end
    object QrInquilinoNO_USUARIO: TWideStringField
      FieldName = 'NO_USUARIO'
      Size = 100
    end
  end
  object QrDonoUH: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDonoUHCalcFields
    SQL.Strings = (
      'SELECT IF(cim.Usuario=0 OR cim.Usuario=cim.Propriet, "", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NOMEPROPRIET,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'ELT(ent.Tipo+1, "CNPJ", "CPF") NOMETIPO'
      'FROM condimov cim'
      'LEFT JOIN entidades ent ON ent.Codigo=cim.Propriet'
      'WHERE cim.Conta=:P0'
      '')
    Left = 628
    Top = 452
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDonoUHNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrDonoUHCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrDonoUHCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoUHNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Size = 4
    end
  end
  object QrListaA: TmySQLQuery
    Database = DModG.DBDmk
    BeforeClose = QrListaABeforeClose
    AfterScroll = QrListaAAfterScroll
    OnCalcFields = QrListaACalcFields
    Left = 856
    Top = 4
    object QrListaAConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrListaANOME_CONTA: TWideStringField
      FieldName = 'NOME_CONTA'
      Size = 50
    end
    object QrListaAArreBaI: TIntegerField
      FieldName = 'ArreBaI'
      Required = True
    end
    object QrListaATexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrListaACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaANOME_ARREC: TWideStringField
      FieldName = 'NOME_ARREC'
      Size = 40
    end
    object QrListaAVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrListaATEXTO_IMP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO_IMP'
      Size = 50
      Calculated = True
    end
    object QrListaASIGLA_CONTA: TWideStringField
      FieldName = 'SIGLA_CONTA'
    end
    object QrListaASIGLA_IMP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SIGLA_IMP'
      Size = 15
      Calculated = True
    end
    object QrListaASIGLA_BAC: TWideStringField
      FieldName = 'SIGLA_BAC'
    end
    object QrListaAInfoRelArr: TSmallintField
      FieldName = 'InfoRelArr'
    end
    object QrListaADescriCota: TWideStringField
      FieldName = 'DescriCota'
      Size = 10
    end
    object QrListaATITULO_FATOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TITULO_FATOR'
      Size = 50
      Calculated = True
    end
    object QrListaADeQuem: TSmallintField
      FieldName = 'DeQuem'
    end
    object QrListaAPartilha: TSmallintField
      FieldName = 'Partilha'
    end
    object QrListaACalculo: TSmallintField
      FieldName = 'Calculo'
    end
  end
  object QrListaAIts: TmySQLQuery
    Database = DModG.DBDmk
    OnCalcFields = QrListaAItsCalcFields
    Left = 856
    Top = 52
    object QrListaAItsNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrListaAItsUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrListaAItsApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrListaAItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrListaAItsPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrListaAItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrListaAItsDeQuem: TSmallintField
      FieldName = 'DeQuem'
    end
    object QrListaAItsCalculo: TSmallintField
      FieldName = 'Calculo'
    end
    object QrListaAItsPartilha: TSmallintField
      FieldName = 'Partilha'
    end
    object QrListaAItsPercent: TFloatField
      FieldName = 'Percent'
    end
    object QrListaAItsMoradores: TIntegerField
      FieldName = 'Moradores'
    end
    object QrListaAItsFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
    end
    object QrListaAItsArreBaI: TIntegerField
      FieldName = 'ArreBaI'
      Required = True
    end
    object QrListaAItsDescriCota: TWideStringField
      FieldName = 'DescriCota'
      Size = 10
    end
    object QrListaAItsFATOR_COBRADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FATOR_COBRADO'
      Calculated = True
    end
    object QrListaAItsTITULO_FATOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TITULO_FATOR'
      Size = 30
      Calculated = True
    end
    object QrListaAItsBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrListaL: TmySQLQuery
    Database = DModG.DBDmk
    BeforeClose = QrListaLBeforeClose
    AfterScroll = QrListaLAfterScroll
    OnCalcFields = QrListaLCalcFields
    Left = 856
    Top = 100
    object QrListaLCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrListaLNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrListaLVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrListaLCasas: TSmallintField
      FieldName = 'Casas'
    end
    object QrListaLUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Size = 5
    end
    object QrListaLSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
    end
    object QrListaLSIGLA_IMP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SIGLA_IMP'
      Size = 15
      Calculated = True
    end
  end
  object QrListaLIts: TmySQLQuery
    Database = DModG.DBDmk
    Left = 856
    Top = 148
    object QrListaLItsApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrListaLItsMedAnt: TFloatField
      FieldName = 'MedAnt'
      Required = True
    end
    object QrListaLItsMedAtu: TFloatField
      FieldName = 'MedAtu'
      Required = True
    end
    object QrListaLItsConsumo: TFloatField
      FieldName = 'Consumo'
      Required = True
    end
    object QrListaLItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrListaLItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrListaLItsNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrListaLItsUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrListaLItsBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object DsListaA: TDataSource
    DataSet = QrListaA
    Left = 912
    Top = 4
  end
  object DsListaAIts: TDataSource
    DataSet = QrListaAIts
    Left = 912
    Top = 52
  end
  object DsListaL: TDataSource
    DataSet = QrListaL
    Left = 912
    Top = 100
  end
  object DsListaLIts: TDataSource
    DataSet = QrListaLIts
    Left = 912
    Top = 148
  end
  object QrAptoModBloq: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT pmb.ModelBloq, pmb.ConfigBol, pmb.BalAgrMens, '
      'pmb.Compe, cfb.Colunas '
      'FROM prevmodbol pmb '
      'LEFT JOIN configbol cfb ON cfb.Codigo=pmb.ConfigBol '
      'WHERE pmb.Codigo=:P0 '
      'AND pmb.Apto=:P1 '
      ' ')
    Left = 924
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAptoModBloqModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrAptoModBloqConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrAptoModBloqBalAgrMens: TSmallintField
      FieldName = 'BalAgrMens'
    end
    object QrAptoModBloqCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrAptoModBloqColunas: TSmallintField
      FieldName = 'Colunas'
    end
  end
  object QrConfigBol: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT * '
      'FROM configbol'
      'WHERE Codigo=:P0')
    Left = 264
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfigBolCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'configbol.Codigo'
    end
    object QrConfigBolNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'configbol.Nome'
      Size = 50
    end
    object QrConfigBolTitBTxtSiz: TSmallintField
      FieldName = 'TitBTxtSiz'
      Origin = 'configbol.TitBTxtSiz'
    end
    object QrConfigBolTitBTxtFmt: TSmallintField
      FieldName = 'TitBTxtFmt'
      Origin = 'configbol.TitBTxtFmt'
    end
    object QrConfigBolTitBMrgSup: TIntegerField
      FieldName = 'TitBMrgSup'
      Origin = 'configbol.TitBMrgSup'
    end
    object QrConfigBolTitBLinAlt: TIntegerField
      FieldName = 'TitBLinAlt'
      Origin = 'configbol.TitBLinAlt'
    end
    object QrConfigBolTitBTxtTit: TWideStringField
      FieldName = 'TitBTxtTit'
      Origin = 'configbol.TitBTxtTit'
      Size = 50
    end
    object QrConfigBolTitBPerFmt: TSmallintField
      FieldName = 'TitBPerFmt'
      Origin = 'configbol.TitBPerFmt'
    end
    object QrConfigBolNiv1TxtSiz: TSmallintField
      FieldName = 'Niv1TxtSiz'
      Origin = 'configbol.Niv1TxtSiz'
    end
    object QrConfigBolNiv1ValSiz: TSmallintField
      FieldName = 'Niv1ValSiz'
      Origin = 'configbol.Niv1ValSiz'
    end
    object QrConfigBolNiv1TxtFmt: TSmallintField
      FieldName = 'Niv1TxtFmt'
      Origin = 'configbol.Niv1TxtFmt'
    end
    object QrConfigBolNiv1ValFmt: TSmallintField
      FieldName = 'Niv1ValFmt'
      Origin = 'configbol.Niv1ValFmt'
    end
    object QrConfigBolNiv1ValLar: TIntegerField
      FieldName = 'Niv1ValLar'
      Origin = 'configbol.Niv1ValLar'
    end
    object QrConfigBolNiv1LinAlt: TIntegerField
      FieldName = 'Niv1LinAlt'
      Origin = 'configbol.Niv1LinAlt'
    end
    object QrConfigBolCxaSTxtSiz: TSmallintField
      FieldName = 'CxaSTxtSiz'
      Origin = 'configbol.CxaSTxtSiz'
    end
    object QrConfigBolCxaSValSiz: TSmallintField
      FieldName = 'CxaSValSiz'
      Origin = 'configbol.CxaSValSiz'
    end
    object QrConfigBolCxaSTxtFmt: TSmallintField
      FieldName = 'CxaSTxtFmt'
      Origin = 'configbol.CxaSTxtFmt'
    end
    object QrConfigBolCxaSValFmt: TSmallintField
      FieldName = 'CxaSValFmt'
      Origin = 'configbol.CxaSValFmt'
    end
    object QrConfigBolCxaSTxtAlt: TIntegerField
      FieldName = 'CxaSTxtAlt'
      Origin = 'configbol.CxaSTxtAlt'
    end
    object QrConfigBolCxaSValAlt: TIntegerField
      FieldName = 'CxaSValAlt'
      Origin = 'configbol.CxaSValAlt'
    end
    object QrConfigBolCxaSMrgSup: TIntegerField
      FieldName = 'CxaSMrgSup'
      Origin = 'configbol.CxaSMrgSup'
    end
    object QrConfigBolCxaSIniTam: TFloatField
      FieldName = 'CxaSIniTam'
      Origin = 'configbol.CxaSIniTam'
    end
    object QrConfigBolCxaSCreTam: TFloatField
      FieldName = 'CxaSCreTam'
      Origin = 'configbol.CxaSCreTam'
    end
    object QrConfigBolCxaSDebTam: TFloatField
      FieldName = 'CxaSDebTam'
      Origin = 'configbol.CxaSDebTam'
    end
    object QrConfigBolCxaSMovTam: TFloatField
      FieldName = 'CxaSMovTam'
      Origin = 'configbol.CxaSMovTam'
    end
    object QrConfigBolCxaSFimTam: TFloatField
      FieldName = 'CxaSFimTam'
      Origin = 'configbol.CxaSFimTam'
    end
    object QrConfigBolCxaSIniTxt: TWideStringField
      FieldName = 'CxaSIniTxt'
      Origin = 'configbol.CxaSIniTxt'
      Size = 30
    end
    object QrConfigBolCxaSCreTxt: TWideStringField
      FieldName = 'CxaSCreTxt'
      Origin = 'configbol.CxaSCreTxt'
      Size = 30
    end
    object QrConfigBolCxaSDebTxt: TWideStringField
      FieldName = 'CxaSDebTxt'
      Origin = 'configbol.CxaSDebTxt'
      Size = 30
    end
    object QrConfigBolCxaSMovTxt: TWideStringField
      FieldName = 'CxaSMovTxt'
      Origin = 'configbol.CxaSMovTxt'
      Size = 30
    end
    object QrConfigBolCxaSFimTxt: TWideStringField
      FieldName = 'CxaSFimTxt'
      Origin = 'configbol.CxaSFimTxt'
      Size = 30
    end
    object QrConfigBolCxaSMrgInf: TIntegerField
      FieldName = 'CxaSMrgInf'
      Origin = 'configbol.CxaSMrgInf'
    end
    object QrConfigBolTit2TxtSiz: TSmallintField
      FieldName = 'Tit2TxtSiz'
      Origin = 'configbol.Tit2TxtSiz'
    end
    object QrConfigBolTit2TxtFmt: TSmallintField
      FieldName = 'Tit2TxtFmt'
      Origin = 'configbol.Tit2TxtFmt'
    end
    object QrConfigBolTit2MrgSup: TIntegerField
      FieldName = 'Tit2MrgSup'
      Origin = 'configbol.Tit2MrgSup'
    end
    object QrConfigBolTit2LinAlt: TIntegerField
      FieldName = 'Tit2LinAlt'
      Origin = 'configbol.Tit2LinAlt'
    end
    object QrConfigBolSom2TxtSiz: TSmallintField
      FieldName = 'Som2TxtSiz'
      Origin = 'configbol.Som2TxtSiz'
    end
    object QrConfigBolSom2ValSiz: TSmallintField
      FieldName = 'Som2ValSiz'
      Origin = 'configbol.Som2ValSiz'
    end
    object QrConfigBolSom2TxtFmt: TSmallintField
      FieldName = 'Som2TxtFmt'
      Origin = 'configbol.Som2TxtFmt'
    end
    object QrConfigBolSom2ValFmt: TSmallintField
      FieldName = 'Som2ValFmt'
      Origin = 'configbol.Som2ValFmt'
    end
    object QrConfigBolSom2ValLar: TIntegerField
      FieldName = 'Som2ValLar'
      Origin = 'configbol.Som2ValLar'
    end
    object QrConfigBolSom2LinAlt: TIntegerField
      FieldName = 'Som2LinAlt'
      Origin = 'configbol.Som2LinAlt'
    end
    object QrConfigBolTit3TxtSiz: TSmallintField
      FieldName = 'Tit3TxtSiz'
      Origin = 'configbol.Tit3TxtSiz'
    end
    object QrConfigBolTit3TxtFmt: TSmallintField
      FieldName = 'Tit3TxtFmt'
      Origin = 'configbol.Tit3TxtFmt'
    end
    object QrConfigBolTit3MrgSup: TIntegerField
      FieldName = 'Tit3MrgSup'
      Origin = 'configbol.Tit3MrgSup'
    end
    object QrConfigBolTit3LinAlt: TIntegerField
      FieldName = 'Tit3LinAlt'
      Origin = 'configbol.Tit3LinAlt'
    end
    object QrConfigBolSom3TxtSiz: TSmallintField
      FieldName = 'Som3TxtSiz'
      Origin = 'configbol.Som3TxtSiz'
    end
    object QrConfigBolSom3ValSiz: TSmallintField
      FieldName = 'Som3ValSiz'
      Origin = 'configbol.Som3ValSiz'
    end
    object QrConfigBolSom3TxtFmt: TSmallintField
      FieldName = 'Som3TxtFmt'
      Origin = 'configbol.Som3TxtFmt'
    end
    object QrConfigBolSom3ValFmt: TSmallintField
      FieldName = 'Som3ValFmt'
      Origin = 'configbol.Som3ValFmt'
    end
    object QrConfigBolSom3ValLar: TIntegerField
      FieldName = 'Som3ValLar'
      Origin = 'configbol.Som3ValLar'
    end
    object QrConfigBolSom3LinAlt: TIntegerField
      FieldName = 'Som3LinAlt'
      Origin = 'configbol.Som3LinAlt'
    end
    object QrConfigBolTit4TxtSiz: TSmallintField
      FieldName = 'Tit4TxtSiz'
      Origin = 'configbol.Tit4TxtSiz'
    end
    object QrConfigBolTit4TxtFmt: TSmallintField
      FieldName = 'Tit4TxtFmt'
      Origin = 'configbol.Tit4TxtFmt'
    end
    object QrConfigBolTit4MrgSup: TIntegerField
      FieldName = 'Tit4MrgSup'
      Origin = 'configbol.Tit4MrgSup'
    end
    object QrConfigBolTit4LinAlt: TIntegerField
      FieldName = 'Tit4LinAlt'
      Origin = 'configbol.Tit4LinAlt'
    end
    object QrConfigBolSom4LinAlt: TIntegerField
      FieldName = 'Som4LinAlt'
      Origin = 'configbol.Som4LinAlt'
    end
    object QrConfigBolTitRTxtSiz: TSmallintField
      FieldName = 'TitRTxtSiz'
      Origin = 'configbol.TitRTxtSiz'
    end
    object QrConfigBolTitRTxtFmt: TSmallintField
      FieldName = 'TitRTxtFmt'
      Origin = 'configbol.TitRTxtFmt'
    end
    object QrConfigBolTitRMrgSup: TIntegerField
      FieldName = 'TitRMrgSup'
      Origin = 'configbol.TitRMrgSup'
    end
    object QrConfigBolTitRLinAlt: TIntegerField
      FieldName = 'TitRLinAlt'
      Origin = 'configbol.TitRLinAlt'
    end
    object QrConfigBolTitRTxtTit: TWideStringField
      FieldName = 'TitRTxtTit'
      Origin = 'configbol.TitRTxtTit'
      Size = 50
    end
    object QrConfigBolTitRPerFmt: TSmallintField
      FieldName = 'TitRPerFmt'
      Origin = 'configbol.TitRPerFmt'
    end
    object QrConfigBolTitCTxtSiz: TSmallintField
      FieldName = 'TitCTxtSiz'
      Origin = 'configbol.TitCTxtSiz'
    end
    object QrConfigBolTitCTxtFmt: TSmallintField
      FieldName = 'TitCTxtFmt'
      Origin = 'configbol.TitCTxtFmt'
    end
    object QrConfigBolTitCMrgSup: TIntegerField
      FieldName = 'TitCMrgSup'
      Origin = 'configbol.TitCMrgSup'
    end
    object QrConfigBolTitCLinAlt: TIntegerField
      FieldName = 'TitCLinAlt'
      Origin = 'configbol.TitCLinAlt'
    end
    object QrConfigBolTitCTxtTit: TWideStringField
      FieldName = 'TitCTxtTit'
      Origin = 'configbol.TitCTxtTit'
      Size = 50
    end
    object QrConfigBolTitCPerFmt: TSmallintField
      FieldName = 'TitCPerFmt'
      Origin = 'configbol.TitCPerFmt'
    end
    object QrConfigBolSdoCTxtSiz: TSmallintField
      FieldName = 'SdoCTxtSiz'
      Origin = 'configbol.SdoCTxtSiz'
    end
    object QrConfigBolSdoCTxtFmt: TSmallintField
      FieldName = 'SdoCTxtFmt'
      Origin = 'configbol.SdoCTxtFmt'
    end
    object QrConfigBolSdoCLinAlt: TIntegerField
      FieldName = 'SdoCLinAlt'
      Origin = 'configbol.SdoCLinAlt'
    end
    object QrConfigBolTitITxtSiz: TSmallintField
      FieldName = 'TitITxtSiz'
      Origin = 'configbol.TitITxtSiz'
    end
    object QrConfigBolTitITxtFmt: TSmallintField
      FieldName = 'TitITxtFmt'
      Origin = 'configbol.TitITxtFmt'
    end
    object QrConfigBolTitIMrgSup: TIntegerField
      FieldName = 'TitIMrgSup'
      Origin = 'configbol.TitIMrgSup'
    end
    object QrConfigBolTitILinAlt: TIntegerField
      FieldName = 'TitILinAlt'
      Origin = 'configbol.TitILinAlt'
    end
    object QrConfigBolTitITxtTit: TWideStringField
      FieldName = 'TitITxtTit'
      Origin = 'configbol.TitITxtTit'
      Size = 50
    end
    object QrConfigBolSdoITxMSiz: TSmallintField
      FieldName = 'SdoITxMSiz'
      Origin = 'configbol.SdoITxMSiz'
    end
    object QrConfigBolSdoITxMFmt: TSmallintField
      FieldName = 'SdoITxMFmt'
      Origin = 'configbol.SdoITxMFmt'
    end
    object QrConfigBolSdoIVaMSiz: TSmallintField
      FieldName = 'SdoIVaMSiz'
      Origin = 'configbol.SdoIVaMSiz'
    end
    object QrConfigBolSdoIVaMFmt: TSmallintField
      FieldName = 'SdoIVaMFmt'
      Origin = 'configbol.SdoIVaMFmt'
    end
    object QrConfigBolSdoILiMAlt: TIntegerField
      FieldName = 'SdoILiMAlt'
      Origin = 'configbol.SdoILiMAlt'
    end
    object QrConfigBolSdoITxMTxt: TWideStringField
      FieldName = 'SdoITxMTxt'
      Origin = 'configbol.SdoITxMTxt'
      Size = 50
    end
    object QrConfigBolSdoILaMVal: TIntegerField
      FieldName = 'SdoILaMVal'
      Origin = 'configbol.SdoILaMVal'
    end
    object QrConfigBolSdoITxTSiz: TSmallintField
      FieldName = 'SdoITxTSiz'
      Origin = 'configbol.SdoITxTSiz'
    end
    object QrConfigBolSdoITxTFmt: TSmallintField
      FieldName = 'SdoITxTFmt'
      Origin = 'configbol.SdoITxTFmt'
    end
    object QrConfigBolSdoIVaTSiz: TSmallintField
      FieldName = 'SdoIVaTSiz'
      Origin = 'configbol.SdoIVaTSiz'
    end
    object QrConfigBolSdoIVaTFmt: TSmallintField
      FieldName = 'SdoIVaTFmt'
      Origin = 'configbol.SdoIVaTFmt'
    end
    object QrConfigBolSdoILiTAlt: TIntegerField
      FieldName = 'SdoILiTAlt'
      Origin = 'configbol.SdoILiTAlt'
    end
    object QrConfigBolSdoITxTTxt: TWideStringField
      FieldName = 'SdoITxTTxt'
      Origin = 'configbol.SdoITxTTxt'
      Size = 50
    end
    object QrConfigBolSdoILaTVal: TIntegerField
      FieldName = 'SdoILaTVal'
      Origin = 'configbol.SdoILaTVal'
    end
    object QrConfigBolCtaPTxtSiz: TSmallintField
      FieldName = 'CtaPTxtSiz'
      Origin = 'configbol.CtaPTxtSiz'
    end
    object QrConfigBolCtaPTxtFmt: TSmallintField
      FieldName = 'CtaPTxtFmt'
      Origin = 'configbol.CtaPTxtFmt'
    end
    object QrConfigBolCtaPValSiz: TSmallintField
      FieldName = 'CtaPValSiz'
      Origin = 'configbol.CtaPValSiz'
    end
    object QrConfigBolCtaPValFmt: TSmallintField
      FieldName = 'CtaPValFmt'
      Origin = 'configbol.CtaPValFmt'
    end
    object QrConfigBolCtaPLinAlt: TIntegerField
      FieldName = 'CtaPLinAlt'
      Origin = 'configbol.CtaPLinAlt'
    end
    object QrConfigBolCtaPLarVal: TIntegerField
      FieldName = 'CtaPLarVal'
      Origin = 'configbol.CtaPLarVal'
    end
    object QrConfigBolSgrPTitSiz: TSmallintField
      FieldName = 'SgrPTitSiz'
      Origin = 'configbol.SgrPTitSiz'
    end
    object QrConfigBolSgrPTitFmt: TSmallintField
      FieldName = 'SgrPTitFmt'
      Origin = 'configbol.SgrPTitFmt'
    end
    object QrConfigBolSgrPTitAlt: TIntegerField
      FieldName = 'SgrPTitAlt'
      Origin = 'configbol.SgrPTitAlt'
    end
    object QrConfigBolSgrPSumSiz: TSmallintField
      FieldName = 'SgrPSumSiz'
      Origin = 'configbol.SgrPSumSiz'
    end
    object QrConfigBolSgrPSumFmt: TSmallintField
      FieldName = 'SgrPSumFmt'
      Origin = 'configbol.SgrPSumFmt'
    end
    object QrConfigBolSgrPSumAlt: TIntegerField
      FieldName = 'SgrPSumAlt'
      Origin = 'configbol.SgrPSumAlt'
    end
    object QrConfigBolSgrPLarVal: TIntegerField
      FieldName = 'SgrPLarVal'
      Origin = 'configbol.SgrPLarVal'
    end
    object QrConfigBolPrvPTitSiz: TSmallintField
      FieldName = 'PrvPTitSiz'
      Origin = 'configbol.PrvPTitSiz'
    end
    object QrConfigBolPrvPTitFmt: TSmallintField
      FieldName = 'PrvPTitFmt'
      Origin = 'configbol.PrvPTitFmt'
    end
    object QrConfigBolPrvPTitAlt: TIntegerField
      FieldName = 'PrvPTitAlt'
      Origin = 'configbol.PrvPTitAlt'
    end
    object QrConfigBolPrvPTitStr: TWideStringField
      FieldName = 'PrvPTitStr'
      Origin = 'configbol.PrvPTitStr'
      Size = 50
    end
    object QrConfigBolPrvPMrgSup: TIntegerField
      FieldName = 'PrvPMrgSup'
      Origin = 'configbol.PrvPMrgSup'
    end
    object QrConfigBolSumPTxtSiz: TSmallintField
      FieldName = 'SumPTxtSiz'
      Origin = 'configbol.SumPTxtSiz'
    end
    object QrConfigBolSumPTxtFmt: TSmallintField
      FieldName = 'SumPTxtFmt'
      Origin = 'configbol.SumPTxtFmt'
    end
    object QrConfigBolSumPValSiz: TSmallintField
      FieldName = 'SumPValSiz'
      Origin = 'configbol.SumPValSiz'
    end
    object QrConfigBolSumPValFmt: TSmallintField
      FieldName = 'SumPValFmt'
      Origin = 'configbol.SumPValFmt'
    end
    object QrConfigBolSumPLinAlt: TIntegerField
      FieldName = 'SumPLinAlt'
      Origin = 'configbol.SumPLinAlt'
    end
    object QrConfigBolSumPValLar: TIntegerField
      FieldName = 'SumPValLar'
      Origin = 'configbol.SumPValLar'
    end
    object QrConfigBolSumPTxtStr: TWideStringField
      FieldName = 'SumPTxtStr'
      Origin = 'configbol.SumPTxtStr'
      Size = 50
    end
    object QrConfigBolCtaATxtSiz: TSmallintField
      FieldName = 'CtaATxtSiz'
      Origin = 'configbol.CtaATxtSiz'
    end
    object QrConfigBolCtaATxtFmt: TSmallintField
      FieldName = 'CtaATxtFmt'
      Origin = 'configbol.CtaATxtFmt'
    end
    object QrConfigBolCtaAValSiz: TSmallintField
      FieldName = 'CtaAValSiz'
      Origin = 'configbol.CtaAValSiz'
    end
    object QrConfigBolCtaAValFmt: TSmallintField
      FieldName = 'CtaAValFmt'
      Origin = 'configbol.CtaAValFmt'
    end
    object QrConfigBolCtaALinAlt: TIntegerField
      FieldName = 'CtaALinAlt'
      Origin = 'configbol.CtaALinAlt'
    end
    object QrConfigBolCtaALarVal: TIntegerField
      FieldName = 'CtaALarVal'
      Origin = 'configbol.CtaALarVal'
    end
    object QrConfigBolTitATitSiz: TSmallintField
      FieldName = 'TitATitSiz'
      Origin = 'configbol.TitATitSiz'
    end
    object QrConfigBolTitATitFmt: TSmallintField
      FieldName = 'TitATitFmt'
      Origin = 'configbol.TitATitFmt'
    end
    object QrConfigBolTitATitAlt: TIntegerField
      FieldName = 'TitATitAlt'
      Origin = 'configbol.TitATitAlt'
    end
    object QrConfigBolTitATitStr: TWideStringField
      FieldName = 'TitATitStr'
      Origin = 'configbol.TitATitStr'
      Size = 50
    end
    object QrConfigBolTitAMrgSup: TIntegerField
      FieldName = 'TitAMrgSup'
      Origin = 'configbol.TitAMrgSup'
    end
    object QrConfigBolTitAAptSiz: TSmallintField
      FieldName = 'TitAAptSiz'
      Origin = 'configbol.TitAAptSiz'
    end
    object QrConfigBolTitAAptFmt: TSmallintField
      FieldName = 'TitAAptFmt'
      Origin = 'configbol.TitAAptFmt'
    end
    object QrConfigBolTitAAptAlt: TIntegerField
      FieldName = 'TitAAptAlt'
      Origin = 'configbol.TitAAptAlt'
    end
    object QrConfigBolTitAAptSup: TIntegerField
      FieldName = 'TitAAptSup'
      Origin = 'configbol.TitAAptSup'
    end
    object QrConfigBolTitASumSiz: TSmallintField
      FieldName = 'TitASumSiz'
      Origin = 'configbol.TitASumSiz'
    end
    object QrConfigBolTitASumFmt: TSmallintField
      FieldName = 'TitASumFmt'
      Origin = 'configbol.TitASumFmt'
    end
    object QrConfigBolTitASumAlt: TIntegerField
      FieldName = 'TitASumAlt'
      Origin = 'configbol.TitASumAlt'
    end
    object QrConfigBolLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'configbol.Lk'
    end
    object QrConfigBolDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'configbol.DataCad'
    end
    object QrConfigBolDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'configbol.DataAlt'
    end
    object QrConfigBolUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'configbol.UserCad'
    end
    object QrConfigBolUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'configbol.UserAlt'
    end
    object QrConfigBolAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'configbol.AlterWeb'
    end
    object QrConfigBolAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'configbol.Ativo'
    end
    object QrConfigBolMeuLogoImp: TSmallintField
      FieldName = 'MeuLogoImp'
      Origin = 'configbol.MeuLogoImp'
    end
    object QrConfigBolMeuLogoAlt: TIntegerField
      FieldName = 'MeuLogoAlt'
      Origin = 'configbol.MeuLogoAlt'
    end
    object QrConfigBolMeuLogoLar: TIntegerField
      FieldName = 'MeuLogoLar'
      Origin = 'configbol.MeuLogoLar'
    end
    object QrConfigBolMeuLogoArq: TWideStringField
      FieldName = 'MeuLogoArq'
      Origin = 'configbol.MeuLogoArq'
      Size = 255
    end
    object QrConfigBolColunas: TSmallintField
      FieldName = 'Colunas'
      Origin = 'configbol.Colunas'
    end
    object QrConfigBolNiv1Grades: TSmallintField
      FieldName = 'Niv1Grades'
      Origin = 'configbol.Niv1Grades'
    end
    object QrConfigBolCxaTrnsTxt: TWideStringField
      FieldName = 'CxaTrnsTxt'
      Origin = 'configbol.CxaTrnsTxt'
      Size = 30
    end
    object QrConfigBolTit8TxtSiz: TSmallintField
      FieldName = 'Tit8TxtSiz'
      Origin = 'configbol.Tit8TxtSiz'
    end
    object QrConfigBolTit8TxtFmt: TSmallintField
      FieldName = 'Tit8TxtFmt'
      Origin = 'configbol.Tit8TxtFmt'
    end
    object QrConfigBolTit8MrgSup: TIntegerField
      FieldName = 'Tit8MrgSup'
      Origin = 'configbol.Tit8MrgSup'
    end
    object QrConfigBolTit8LinAlt: TIntegerField
      FieldName = 'Tit8LinAlt'
      Origin = 'configbol.Tit8LinAlt'
    end
    object QrConfigBolNiv8TxtSiz: TSmallintField
      FieldName = 'Niv8TxtSiz'
      Origin = 'configbol.Niv8TxtSiz'
    end
    object QrConfigBolNiv8ValSiz: TSmallintField
      FieldName = 'Niv8ValSiz'
      Origin = 'configbol.Niv8ValSiz'
    end
    object QrConfigBolNiv8Grades: TSmallintField
      FieldName = 'Niv8Grades'
      Origin = 'configbol.Niv8Grades'
    end
    object QrConfigBolNiv8TxtFmt: TSmallintField
      FieldName = 'Niv8TxtFmt'
      Origin = 'configbol.Niv8TxtFmt'
    end
    object QrConfigBolNiv8ValFmt: TSmallintField
      FieldName = 'Niv8ValFmt'
      Origin = 'configbol.Niv8ValFmt'
    end
    object QrConfigBolNiv8ValLar: TIntegerField
      FieldName = 'Niv8ValLar'
      Origin = 'configbol.Niv8ValLar'
    end
    object QrConfigBolNiv8LinAlt: TIntegerField
      FieldName = 'Niv8LinAlt'
      Origin = 'configbol.Niv8LinAlt'
    end
    object QrConfigBolSom8TxtSiz: TSmallintField
      FieldName = 'Som8TxtSiz'
      Origin = 'configbol.Som8TxtSiz'
    end
    object QrConfigBolSom8ValSiz: TSmallintField
      FieldName = 'Som8ValSiz'
      Origin = 'configbol.Som8ValSiz'
    end
    object QrConfigBolSom8TxtFmt: TSmallintField
      FieldName = 'Som8TxtFmt'
      Origin = 'configbol.Som8TxtFmt'
    end
    object QrConfigBolSom8ValFmt: TSmallintField
      FieldName = 'Som8ValFmt'
      Origin = 'configbol.Som8ValFmt'
    end
    object QrConfigBolSom8ValLar: TIntegerField
      FieldName = 'Som8ValLar'
      Origin = 'configbol.Som8ValLar'
    end
    object QrConfigBolTitAAptTex: TWideMemoField
      FieldName = 'TitAAptTex'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrConfigBolTitWebTxt: TWideStringField
      FieldName = 'TitWebTxt'
      Size = 50
    end
    object QrConfigBolSom8LinAlt: TIntegerField
      FieldName = 'Som8LinAlt'
      Origin = 'configbol.Som8LinAlt'
    end
    object QrConfigBolMesCompet: TSmallintField
      FieldName = 'MesCompet'
    end
  end
  object frxBloq: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39911.688433344920000000
    ReportOptions.LastChange = 39911.688433344920000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 912
    Top = 196
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object QrPrevBloq: TmySQLQuery
    Database = DModG.DBDmk
    Left = 924
    Top = 300
    object QrPrevBloqModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrPrevBloqConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrPrevBloqBalAgrMens: TSmallintField
      FieldName = 'BalAgrMens'
    end
    object QrPrevBloqCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrPrevBloqColunas: TSmallintField
      FieldName = 'Colunas'
    end
  end
  object QrCjSdo: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT Codigo, SdoAtu  '
      'FROM conjunsdo'
      ''
      'WHERE Entidade=:P0')
    Left = 741
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCjSdoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCjSdoSdoAtu: TFloatField
      FieldName = 'SdoAtu'
    end
  end
  object QrCJMov: TmySQLQuery
    Database = DModG.DBDmk
    Left = 740
    Top = 52
    object QrCJMovConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrCJMovCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrCJMovDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrCJMovValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrMov: TmySQLQuery
    Database = Dmod.MyDB
    AfterClose = QrMovAfterClose
    OnCalcFields = QrMovCalcFields
    Left = 384
    Top = 4
    object QrMovNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 50
    end
    object QrMovGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'subgrupos.Grupo'
    end
    object QrMovNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrMovSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
      Origin = 'contas.Subgrupo'
    end
    object QrMovNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrMovGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrMovMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrMovValor: TFloatField
      FieldName = 'Valor'
    end
    object QrMovNOMECONTA_EXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECONTA_EXT'
      Size = 255
      Calculated = True
    end
    object QrMovNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Origin = 'plano.Nome'
      Size = 50
    end
    object QrMovNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 50
    end
    object QrMovConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrMovCJ_CRED: TFloatField
      FieldKind = fkLookup
      FieldName = 'CJ_CRED'
      LookupDataSet = QrCJMov
      LookupKeyFields = 'Conjunto'
      LookupResultField = 'Credito'
      KeyFields = 'Conjunto'
      Lookup = True
    end
    object QrMovCJ_DEBI: TFloatField
      FieldKind = fkLookup
      FieldName = 'CJ_DEBI'
      LookupDataSet = QrCJMov
      LookupKeyFields = 'Conjunto'
      LookupResultField = 'Debito'
      KeyFields = 'Conjunto'
      Lookup = True
    end
    object QrMovCJ_SALF: TFloatField
      FieldKind = fkLookup
      FieldName = 'CJ_SALF'
      LookupDataSet = QrCjSdo
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SdoAtu'
      KeyFields = 'Conjunto'
      Lookup = True
    end
    object QrMovCJ_SALI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CJ_SALI'
      Calculated = True
    end
    object QrMovCJ_SALM: TFloatField
      FieldKind = fkLookup
      FieldName = 'CJ_SALM'
      LookupDataSet = QrCJMov
      LookupKeyFields = 'Conjunto'
      LookupResultField = 'Valor'
      KeyFields = 'Conjunto'
      Lookup = True
    end
    object QrMovSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
  end
  object QrMov3: TmySQLQuery
    Database = DModG.DBDmk
    OnCalcFields = QrMov3CalcFields
    SQL.Strings = (
      'SELECT pla.Nome NOMEPLANO, cjt.Nome NOMECONJUNTO,'
      'gru.Conjunto, gru.Nome NOMEGRUPO, '
      'sgr.Grupo , sgr.Nome NOMESUBGRUPO, '
      'con.SubGrupo , con.Nome NOMECONTA, '
      'lan.Genero, lan.Mez,'
      'SUM(lan.Credito - lan.Debito) Valor, lan.SubPgto1'
      'FROM lct0001a lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas    con ON con.Codigo=lan.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      ''
      'WHERE car.ForneceI=1'
      'AND car.Tipo < 2'
      'AND lan.Genero>0'
      'AND lan.Data BETWEEN "2010-07-01" AND "2010-07-31"'
      'GROUP BY lan.Genero, lan.Mez /*, lan.Subpgto1*/'
      'ORDER BY pla.OrdemLista, NOMEPLANO,'
      '                    cjt.OrdemLista, NOMECONJUNTO,'
      '                    gru.OrdemLista, NOMEGRUPO, '
      '                    sgr.OrdemLista, NOMESUBGRUPO, '
      '                    con.OrdemLista, NOMECONTA, lan.Mez')
    Left = 624
    Top = 4
    object QrMov3NOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrMov3NOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
    object QrMov3Conjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrMov3NOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrMov3Grupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrMov3NOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrMov3SubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrMov3NOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrMov3Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrMov3Mez: TIntegerField
      FieldName = 'Mez'
    end
    object QrMov3Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrMov3SDOANT: TFloatField
      FieldKind = fkLookup
      FieldName = 'SDOANT'
      LookupDataSet = QrCtasSdo
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SdoAnt'
      KeyFields = 'Genero'
      Lookup = True
    end
    object QrMov3SUMCRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUMCRE'
      LookupDataSet = QrCtasSdo
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SumCre'
      KeyFields = 'Genero'
      Lookup = True
    end
    object QrMov3SUMDEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUMDEB'
      LookupDataSet = QrCtasSdo
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SumDeb'
      KeyFields = 'Genero'
      Lookup = True
    end
    object QrMov3SDOFIM: TFloatField
      FieldKind = fkLookup
      FieldName = 'SDOFIM'
      LookupDataSet = QrCtasSdo
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SdoFim'
      KeyFields = 'Genero'
      Lookup = True
    end
    object QrMov3SDOMES: TFloatField
      FieldKind = fkLookup
      FieldName = 'SDOMES'
      LookupDataSet = QrCtasSdo
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Valor'
      KeyFields = 'Genero'
      Lookup = True
    end
    object QrMov3NOMECONTA_EXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECONTA_EXT'
      Size = 100
      Calculated = True
    end
    object QrMov3SubPgto1: TIntegerField
      FieldName = 'SubPgto1'
    end
    object QrMov3CJT_COD: TIntegerField
      FieldKind = fkLookup
      FieldName = 'CJT_COD'
      LookupDataSet = QrSdoCjt
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Codigo'
      KeyFields = 'Conjunto'
      Lookup = True
    end
    object QrMov3CJT_ANT: TFloatField
      FieldKind = fkLookup
      FieldName = 'CJT_ANT'
      LookupDataSet = QrSdoCjt
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SdoAnt'
      KeyFields = 'Conjunto'
      Lookup = True
    end
    object QrMov3CJT_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'CJT_CRE'
      LookupDataSet = QrSdoCjt
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SumCre'
      KeyFields = 'Conjunto'
      Lookup = True
    end
    object QrMov3CJT_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'CJT_DEB'
      LookupDataSet = QrSdoCjt
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SumDeb'
      KeyFields = 'Conjunto'
      Lookup = True
    end
    object QrMov3CJT_FIM: TFloatField
      FieldKind = fkLookup
      FieldName = 'CJT_FIM'
      LookupDataSet = QrSdoCjt
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SdoFim'
      KeyFields = 'Conjunto'
      Lookup = True
    end
    object QrMov3CJT_MOV: TFloatField
      FieldKind = fkLookup
      FieldName = 'CJT_MOV'
      LookupDataSet = QrSdoCjt
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Valor'
      KeyFields = 'Conjunto'
      Lookup = True
    end
    object QrMov3SGR_COD: TIntegerField
      FieldKind = fkLookup
      FieldName = 'SGR_COD'
      LookupDataSet = QrSdoSgr
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Codigo'
      KeyFields = 'SubGrupo'
      Lookup = True
    end
    object QrMov3SGR_ANT: TFloatField
      FieldKind = fkLookup
      FieldName = 'SGR_ANT'
      LookupDataSet = QrSdoSgr
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SdoAnt'
      KeyFields = 'SubGrupo'
      Lookup = True
    end
    object QrMov3SGR_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'SGR_CRE'
      LookupDataSet = QrSdoSgr
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SumCre'
      KeyFields = 'SubGrupo'
      Lookup = True
    end
    object QrMov3SGR_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'SGR_DEB'
      LookupDataSet = QrSdoSgr
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SumDeb'
      KeyFields = 'SubGrupo'
      Lookup = True
    end
    object QrMov3SGR_FIM: TFloatField
      FieldKind = fkLookup
      FieldName = 'SGR_FIM'
      LookupDataSet = QrSdoSgr
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SdoFim'
      KeyFields = 'SubGrupo'
      Lookup = True
    end
    object QrMov3SGR_MOV: TFloatField
      FieldKind = fkLookup
      FieldName = 'SGR_MOV'
      LookupDataSet = QrSdoSgr
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Valor'
      KeyFields = 'SubGrupo'
      Lookup = True
    end
    object QrMov3GRU_COD: TIntegerField
      FieldKind = fkLookup
      FieldName = 'GRU_COD'
      LookupDataSet = QrSdoGru
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Codigo'
      KeyFields = 'Grupo'
      Lookup = True
    end
    object QrMov3GRU_ANT: TFloatField
      FieldKind = fkLookup
      FieldName = 'GRU_ANT'
      LookupDataSet = QrSdoGru
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SdoAnt'
      KeyFields = 'Grupo'
      Lookup = True
    end
    object QrMov3GRU_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'GRU_CRE'
      LookupDataSet = QrSdoGru
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SumCre'
      KeyFields = 'Grupo'
      Lookup = True
    end
    object QrMov3GRU_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'GRU_DEB'
      LookupDataSet = QrSdoGru
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SumDeb'
      KeyFields = 'Grupo'
      Lookup = True
    end
    object QrMov3GRU_MOV: TFloatField
      FieldKind = fkLookup
      FieldName = 'GRU_MOV'
      LookupDataSet = QrSdoGru
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Valor'
      KeyFields = 'Grupo'
      Lookup = True
    end
    object QrMov3GRU_FIM: TFloatField
      FieldKind = fkLookup
      FieldName = 'GRU_FIM'
      LookupDataSet = QrSdoGru
      LookupKeyFields = 'Codigo'
      LookupResultField = 'SdoFim'
      KeyFields = 'Grupo'
      Lookup = True
    end
  end
  object QrCtasSdo: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT CO_CTA Codigo, SdoAnt, '
      'Credito SumCre, Debito SumDeb, Movim SdoFim, '
      'Credito-Debito Valor'
      'FROM saldosniv'
      'WHERE Nivel=1')
    Left = 624
    Top = 56
    object QrCtasSdoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCtasSdoSdoAnt: TFloatField
      FieldName = 'SdoAnt'
      Required = True
    end
    object QrCtasSdoSumCre: TFloatField
      FieldName = 'SumCre'
      Required = True
    end
    object QrCtasSdoSumDeb: TFloatField
      FieldName = 'SumDeb'
      Required = True
    end
    object QrCtasSdoSdoFim: TFloatField
      FieldName = 'SdoFim'
      Required = True
    end
    object QrCtasSdoValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
  end
  object QrSdoSgr: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT CO_SGR Codigo, SdoAnt, '
      'Credito SumCre, Debito SumDeb, Movim SdoFim, '
      'Credito-Debito Valor'
      'FROM saldosniv'
      'WHERE Nivel=2')
    Left = 624
    Top = 100
    object QrSdoSgrCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSdoSgrSumCre: TFloatField
      FieldName = 'SumCre'
    end
    object QrSdoSgrSumDeb: TFloatField
      FieldName = 'SumDeb'
    end
    object QrSdoSgrSdoFim: TFloatField
      FieldName = 'SdoFim'
    end
    object QrSdoSgrValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSdoSgrSdoAnt: TFloatField
      FieldName = 'SdoAnt'
    end
  end
  object QrSdoGru: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT CO_GRU Codigo, SdoAnt, '
      'Credito SumCre, Debito SumDeb, Movim SdoFim, '
      'Credito-Debito Valor'
      'FROM saldosniv'
      'WHERE Nivel=3')
    Left = 624
    Top = 148
    object QrSdoGruCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSdoGruSdoAnt: TFloatField
      FieldName = 'SdoAnt'
    end
    object QrSdoGruSumCre: TFloatField
      FieldName = 'SumCre'
    end
    object QrSdoGruSumDeb: TFloatField
      FieldName = 'SumDeb'
    end
    object QrSdoGruSdoFim: TFloatField
      FieldName = 'SdoFim'
    end
    object QrSdoGruValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrSdoCjt: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT NO_Cjt NOME_CJT, CO_Cjt Codigo, SdoAnt, '
      'Credito SumCre, Debito SumDeb, Movim SdoFim, '
      'Credito-Debito Valor'
      'FROM saldosniv'
      'WHERE Nivel=4')
    Left = 624
    Top = 196
    object QrSdoCjtCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSdoCjtSumCre: TFloatField
      FieldName = 'SumCre'
      Required = True
    end
    object QrSdoCjtSumDeb: TFloatField
      FieldName = 'SumDeb'
      Required = True
    end
    object QrSdoCjtSdoFim: TFloatField
      FieldName = 'SdoFim'
      Required = True
    end
    object QrSdoCjtValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrSdoCjtSdoAnt: TFloatField
      FieldName = 'SdoAnt'
    end
    object QrSdoCjtNOME_CJT: TWideStringField
      FieldName = 'NOME_CJT'
      Size = 50
    end
  end
  object DsMov3: TDataSource
    DataSet = QrMov3
    Left = 684
    Top = 4
  end
  object QrSumCtaBol: TmySQLQuery
    Database = DModG.DBDmk
    OnCalcFields = QrSumCtaBolCalcFields
    Left = 508
    Top = 196
    object QrSumCtaBolTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Required = True
      Size = 50
    end
    object QrSumCtaBolVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrSumCtaBolTipo: TLargeintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrSumCtaBolConsumo: TFloatField
      FieldName = 'Consumo'
    end
    object QrSumCtaBolCasas: TLargeintField
      FieldName = 'Casas'
    end
    object QrSumCtaBolUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Size = 5
    end
    object QrSumCtaBolUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Size = 10
    end
    object QrSumCtaBolUnidFat: TFloatField
      FieldName = 'UnidFat'
    end
    object QrSumCtaBolGeraTyp: TLargeintField
      FieldName = 'GeraTyp'
    end
    object QrSumCtaBolGeraFat: TFloatField
      FieldName = 'GeraFat'
    end
    object QrSumCtaBolCasRat: TLargeintField
      FieldName = 'CasRat'
    end
    object QrSumCtaBolNaoImpLei: TLargeintField
      FieldName = 'NaoImpLei'
    end
    object QrSumCtaBolTEXTO_IMP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO_IMP'
      Size = 256
      Calculated = True
    end
    object QrSumCtaBolKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
  end
  object frxDsSumCtaBol: TfrxDBDataset
    UserName = 'frxDsSumCtaBol'
    CloseDataSource = False
    DataSet = QrSumCtaBol
    BCDToCurrency = False
    Left = 568
    Top = 196
  end
  object QrPrevModBol: TmySQLQuery
    Database = DModG.DBDmk
    OnCalcFields = QrPrevModBolCalcFields
    SQL.Strings = (
      'SELECT imv.Unidade, cfb.Nome NOME_CONFIGBOL,'
      'pmb.Apto, pmb.ConfigBol, pmb.ModelBloq, '
      'pmb.BalAgrMens, pmb.Compe, pmb.Codigo'
      'FROM prevmodbol pmb'
      'LEFT JOIN condimov imv ON imv.Conta=pmb.Apto'
      'LEFT JOIN configbol cfb ON cfb.Codigo=pmb.ConfigBol'
      'WHERE pmb.Codigo=:P0'#13)
    Left = 860
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrevModBolUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPrevModBolNOME_CONFIGBOL: TWideStringField
      FieldName = 'NOME_CONFIGBOL'
      Size = 50
    end
    object QrPrevModBolApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrPrevModBolConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrPrevModBolModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrPrevModBolBalAgrMens: TSmallintField
      FieldName = 'BalAgrMens'
    end
    object QrPrevModBolCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrPrevModBolNOME_MODELBLOQ: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_MODELBLOQ'
      Size = 1
      Calculated = True
    end
    object QrPrevModBolCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsPrevModBol: TDataSource
    DataSet = QrPrevModBol
    Left = 916
    Top = 248
  end
  object QrBolB: TmySQLQuery
    Database = DModG.DBDmk
    Left = 324
    Top = 400
    object QrBolBGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBolBNOMECONS: TWideStringField
      FieldName = 'NOMECONS'
      Size = 50
    end
    object QrBolBValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrBolBPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrBolBApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolBVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrBolBMedAnt: TFloatField
      FieldName = 'MedAnt'
      Required = True
    end
    object QrBolBMedAtu: TFloatField
      FieldName = 'MedAtu'
      Required = True
    end
    object QrBolBConsumo: TFloatField
      FieldName = 'Consumo'
      Required = True
    end
    object QrBolBCasas: TSmallintField
      FieldName = 'Casas'
      Required = True
    end
    object QrBolBUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Required = True
      Size = 5
    end
    object QrBolBUnidFat: TFloatField
      FieldName = 'UnidFat'
      Required = True
    end
    object QrBolBUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Required = True
      Size = 5
    end
    object QrBolBControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBolBGeraTyp: TSmallintField
      FieldName = 'GeraTyp'
      Required = True
    end
    object QrBolBGeraFat: TFloatField
      FieldName = 'GeraFat'
    end
    object QrBolBCasRat: TSmallintField
      FieldName = 'CasRat'
    end
    object QrBolBNaoImpLei: TSmallintField
      FieldName = 'NaoImpLei'
    end
  end
  object QrBolA: TmySQLQuery
    Database = DModG.DBDmk
    Left = 264
    Top = 400
    object QrBolAControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBolAGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrBolAValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrBolATexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrBolAApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolAPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrBolAVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrBolAUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
  end
  object QrBolSel: TmySQLQuery
    Database = DModG.DBDmk
    Left = 264
    Top = 352
    object QrBolSelVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBolSelPROTOCOLO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'PROTOCOLO'
      LookupDataSet = QrPPI
      LookupKeyFields = 'Docum'
      LookupResultField = 'PROTOCOLO'
      KeyFields = 'Boleto'
      Lookup = True
    end
    object QrBolSelBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object DsBolSel: TDataSource
    DataSet = QrBolSel
    Left = 324
    Top = 352
  end
  object frxDsConfigBol: TfrxDBDataset
    UserName = 'frxDsConfigBol'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'TitBTxtSiz=TitBTxtSiz'
      'TitBTxtFmt=TitBTxtFmt'
      'TitBMrgSup=TitBMrgSup'
      'TitBLinAlt=TitBLinAlt'
      'TitBTxtTit=TitBTxtTit'
      'TitBPerFmt=TitBPerFmt'
      'Niv1TxtSiz=Niv1TxtSiz'
      'Niv1ValSiz=Niv1ValSiz'
      'Niv1TxtFmt=Niv1TxtFmt'
      'Niv1ValFmt=Niv1ValFmt'
      'Niv1ValLar=Niv1ValLar'
      'Niv1LinAlt=Niv1LinAlt'
      'CxaSTxtSiz=CxaSTxtSiz'
      'CxaSValSiz=CxaSValSiz'
      'CxaSTxtFmt=CxaSTxtFmt'
      'CxaSValFmt=CxaSValFmt'
      'CxaSTxtAlt=CxaSTxtAlt'
      'CxaSValAlt=CxaSValAlt'
      'CxaSMrgSup=CxaSMrgSup'
      'CxaSIniTam=CxaSIniTam'
      'CxaSCreTam=CxaSCreTam'
      'CxaSDebTam=CxaSDebTam'
      'CxaSMovTam=CxaSMovTam'
      'CxaSFimTam=CxaSFimTam'
      'CxaSIniTxt=CxaSIniTxt'
      'CxaSCreTxt=CxaSCreTxt'
      'CxaSDebTxt=CxaSDebTxt'
      'CxaSMovTxt=CxaSMovTxt'
      'CxaSFimTxt=CxaSFimTxt'
      'CxaSMrgInf=CxaSMrgInf'
      'Tit2TxtSiz=Tit2TxtSiz'
      'Tit2TxtFmt=Tit2TxtFmt'
      'Tit2MrgSup=Tit2MrgSup'
      'Tit2LinAlt=Tit2LinAlt'
      'Som2TxtSiz=Som2TxtSiz'
      'Som2ValSiz=Som2ValSiz'
      'Som2TxtFmt=Som2TxtFmt'
      'Som2ValFmt=Som2ValFmt'
      'Som2ValLar=Som2ValLar'
      'Som2LinAlt=Som2LinAlt'
      'Tit3TxtSiz=Tit3TxtSiz'
      'Tit3TxtFmt=Tit3TxtFmt'
      'Tit3MrgSup=Tit3MrgSup'
      'Tit3LinAlt=Tit3LinAlt'
      'Som3TxtSiz=Som3TxtSiz'
      'Som3ValSiz=Som3ValSiz'
      'Som3TxtFmt=Som3TxtFmt'
      'Som3ValFmt=Som3ValFmt'
      'Som3ValLar=Som3ValLar'
      'Som3LinAlt=Som3LinAlt'
      'Tit4TxtSiz=Tit4TxtSiz'
      'Tit4TxtFmt=Tit4TxtFmt'
      'Tit4MrgSup=Tit4MrgSup'
      'Tit4LinAlt=Tit4LinAlt'
      'Som4LinAlt=Som4LinAlt'
      'TitRTxtSiz=TitRTxtSiz'
      'TitRTxtFmt=TitRTxtFmt'
      'TitRMrgSup=TitRMrgSup'
      'TitRLinAlt=TitRLinAlt'
      'TitRTxtTit=TitRTxtTit'
      'TitRPerFmt=TitRPerFmt'
      'TitCTxtSiz=TitCTxtSiz'
      'TitCTxtFmt=TitCTxtFmt'
      'TitCMrgSup=TitCMrgSup'
      'TitCLinAlt=TitCLinAlt'
      'TitCTxtTit=TitCTxtTit'
      'TitCPerFmt=TitCPerFmt'
      'SdoCTxtSiz=SdoCTxtSiz'
      'SdoCTxtFmt=SdoCTxtFmt'
      'SdoCLinAlt=SdoCLinAlt'
      'TitITxtSiz=TitITxtSiz'
      'TitITxtFmt=TitITxtFmt'
      'TitIMrgSup=TitIMrgSup'
      'TitILinAlt=TitILinAlt'
      'TitITxtTit=TitITxtTit'
      'SdoITxMSiz=SdoITxMSiz'
      'SdoITxMFmt=SdoITxMFmt'
      'SdoIVaMSiz=SdoIVaMSiz'
      'SdoIVaMFmt=SdoIVaMFmt'
      'SdoILiMAlt=SdoILiMAlt'
      'SdoITxMTxt=SdoITxMTxt'
      'SdoILaMVal=SdoILaMVal'
      'SdoITxTSiz=SdoITxTSiz'
      'SdoITxTFmt=SdoITxTFmt'
      'SdoIVaTSiz=SdoIVaTSiz'
      'SdoIVaTFmt=SdoIVaTFmt'
      'SdoILiTAlt=SdoILiTAlt'
      'SdoITxTTxt=SdoITxTTxt'
      'SdoILaTVal=SdoILaTVal'
      'CtaPTxtSiz=CtaPTxtSiz'
      'CtaPTxtFmt=CtaPTxtFmt'
      'CtaPValSiz=CtaPValSiz'
      'CtaPValFmt=CtaPValFmt'
      'CtaPLinAlt=CtaPLinAlt'
      'CtaPLarVal=CtaPLarVal'
      'SgrPTitSiz=SgrPTitSiz'
      'SgrPTitFmt=SgrPTitFmt'
      'SgrPTitAlt=SgrPTitAlt'
      'SgrPSumSiz=SgrPSumSiz'
      'SgrPSumFmt=SgrPSumFmt'
      'SgrPSumAlt=SgrPSumAlt'
      'SgrPLarVal=SgrPLarVal'
      'PrvPTitSiz=PrvPTitSiz'
      'PrvPTitFmt=PrvPTitFmt'
      'PrvPTitAlt=PrvPTitAlt'
      'PrvPTitStr=PrvPTitStr'
      'PrvPMrgSup=PrvPMrgSup'
      'SumPTxtSiz=SumPTxtSiz'
      'SumPTxtFmt=SumPTxtFmt'
      'SumPValSiz=SumPValSiz'
      'SumPValFmt=SumPValFmt'
      'SumPLinAlt=SumPLinAlt'
      'SumPValLar=SumPValLar'
      'SumPTxtStr=SumPTxtStr'
      'CtaATxtSiz=CtaATxtSiz'
      'CtaATxtFmt=CtaATxtFmt'
      'CtaAValSiz=CtaAValSiz'
      'CtaAValFmt=CtaAValFmt'
      'CtaALinAlt=CtaALinAlt'
      'CtaALarVal=CtaALarVal'
      'TitATitSiz=TitATitSiz'
      'TitATitFmt=TitATitFmt'
      'TitATitAlt=TitATitAlt'
      'TitATitStr=TitATitStr'
      'TitAMrgSup=TitAMrgSup'
      'TitAAptSiz=TitAAptSiz'
      'TitAAptFmt=TitAAptFmt'
      'TitAAptAlt=TitAAptAlt'
      'TitAAptSup=TitAAptSup'
      'TitASumSiz=TitASumSiz'
      'TitASumFmt=TitASumFmt'
      'TitASumAlt=TitASumAlt'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'MeuLogoImp=MeuLogoImp'
      'MeuLogoAlt=MeuLogoAlt'
      'MeuLogoLar=MeuLogoLar'
      'MeuLogoArq=MeuLogoArq'
      'Colunas=Colunas'
      'Niv1Grades=Niv1Grades'
      'CxaTrnsTxt=CxaTrnsTxt'
      'Tit8TxtSiz=Tit8TxtSiz'
      'Tit8TxtFmt=Tit8TxtFmt'
      'Tit8MrgSup=Tit8MrgSup'
      'Tit8LinAlt=Tit8LinAlt'
      'Niv8TxtSiz=Niv8TxtSiz'
      'Niv8ValSiz=Niv8ValSiz'
      'Niv8Grades=Niv8Grades'
      'Niv8TxtFmt=Niv8TxtFmt'
      'Niv8ValFmt=Niv8ValFmt'
      'Niv8ValLar=Niv8ValLar'
      'Niv8LinAlt=Niv8LinAlt'
      'Som8TxtSiz=Som8TxtSiz'
      'Som8ValSiz=Som8ValSiz'
      'Som8TxtFmt=Som8TxtFmt'
      'Som8ValFmt=Som8ValFmt'
      'Som8ValLar=Som8ValLar'
      'TitAAptTex=TitAAptTex'
      'TitWebTxt=TitWebTxt'
      'Som8LinAlt=Som8LinAlt')
    DataSet = QrConfigBol
    BCDToCurrency = False
    Left = 324
    Top = 268
  end
  object frxDsMov: TfrxDBDataset
    UserName = 'frxDsMov'
    CloseDataSource = False
    DataSet = QrMov
    BCDToCurrency = False
    Left = 444
    Top = 4
  end
  object frxDsMov3: TfrxDBDataset
    UserName = 'frxDsMov3'
    CloseDataSource = False
    DataSet = QrMov3
    BCDToCurrency = False
    Left = 796
    Top = 4
  end
  object frxDsInquilino: TfrxDBDataset
    UserName = 'frxDsInquilino'
    CloseDataSource = False
    DataSet = QrInquilino
    BCDToCurrency = False
    Left = 568
    Top = 448
  end
  object frxDsBoletosIts: TfrxDBDataset
    UserName = 'frxDsBoletosIts'
    CloseDataSource = False
    DataSet = QrBoletosIts
    BCDToCurrency = False
    Left = 568
    Top = 400
  end
  object frxDsBoletos: TfrxDBDataset
    UserName = 'frxDsBoletos'
    CloseDataSource = False
    DataSet = QrBoletos
    BCDToCurrency = False
    Left = 568
    Top = 352
  end
  object frxDsSdoCjt: TfrxDBDataset
    UserName = 'frxDsSdoCjt'
    CloseDataSource = False
    DataSet = QrSdoCjt
    BCDToCurrency = False
    Left = 684
    Top = 196
  end
  object QrSaldosNiv: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM saldosniv'
      'ORDER BY OL_Pla, NO_Pla, OL_Cjt, NO_Cjt, OL_Gru, '
      'NO_Gru, OL_SGr, NO_SGr, OL_Cta, NO_Cta;')
    Left = 32
    Top = 8
    object QrSaldosNivNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrSaldosNivCO_Cta: TIntegerField
      FieldName = 'CO_Cta'
    end
    object QrSaldosNivCO_SGr: TIntegerField
      FieldName = 'CO_SGr'
    end
    object QrSaldosNivCO_Gru: TIntegerField
      FieldName = 'CO_Gru'
    end
    object QrSaldosNivCO_Cjt: TIntegerField
      FieldName = 'CO_Cjt'
    end
    object QrSaldosNivCO_Pla: TIntegerField
      FieldName = 'CO_Pla'
    end
    object QrSaldosNivNO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 100
    end
    object QrSaldosNivNO_SGr: TWideStringField
      FieldName = 'NO_SGr'
      Size = 100
    end
    object QrSaldosNivNO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 100
    end
    object QrSaldosNivNO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 100
    end
    object QrSaldosNivNO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 100
    end
    object QrSaldosNivOL_Cta: TIntegerField
      FieldName = 'OL_Cta'
    end
    object QrSaldosNivOL_SGr: TIntegerField
      FieldName = 'OL_SGr'
    end
    object QrSaldosNivOL_Gru: TIntegerField
      FieldName = 'OL_Gru'
    end
    object QrSaldosNivOL_Cjt: TIntegerField
      FieldName = 'OL_Cjt'
    end
    object QrSaldosNivOL_Pla: TIntegerField
      FieldName = 'OL_Pla'
    end
    object QrSaldosNivSdoAnt: TFloatField
      FieldName = 'SdoAnt'
    end
    object QrSaldosNivCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSaldosNivDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrSaldosNivMovim: TFloatField
      FieldName = 'Movim'
    end
    object QrSaldosNivEX_Cta: TIntegerField
      FieldName = 'EX_Cta'
    end
    object QrSaldosNivEX_SGr: TIntegerField
      FieldName = 'EX_SGr'
    end
    object QrSaldosNivEX_Gru: TIntegerField
      FieldName = 'EX_Gru'
    end
    object QrSaldosNivEX_Cjt: TIntegerField
      FieldName = 'EX_Cjt'
    end
    object QrSaldosNivEX_Pla: TIntegerField
      FieldName = 'EX_Pla'
    end
    object QrSaldosNivID_SEQ: TIntegerField
      FieldName = 'ID_SEQ'
    end
    object QrSaldosNivAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrPendT: TmySQLQuery
    Database = DModG.DBDmk
    Left = 33
    Top = 55
    object QrPendTSALDO: TFloatField
      FieldName = 'SALDO'
    end
  end
  object QrPendU: TmySQLQuery
    Database = DModG.DBDmk
    Left = 33
    Top = 103
    object QrPendUSALDO: TFloatField
      FieldName = 'SALDO'
    end
  end
  object QrIC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome    END NOME' +
        'ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF     END CNPJ' +
        '_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG      END IE_R' +
        'G, '
      
        'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""         END NIRE' +
        '_, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua    END RUA,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END + 0.' +
        '000 NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END + 0.' +
        '000 Lograd, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END + 0.' +
        '000 CEP, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX,'
      'ELT(en.Tipo+1, "CNPJ", "CPF") NOMETIPO'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN ufs ufc ON ufc.Codigo=en.CUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo = (SELECT '
      'CASE imv.BloqEndTip '
      
        '  WHEN 0 THEN (IF(imv.Usuario <> 0, cnd.Cliente, imv.Propriet) )' +
        '    '
      '  WHEN 1 THEN cnd.Cliente'
      '  WHEN 2 THEN imv.Propriet'
      '  WHEN 3 THEN BloqEndEnt '
      '  WHEN 4 THEN 0'
      'END '
      'FROM condimov imv'
      'LEFT JOIN cond cnd ON cnd.Codigo=imv.Codigo'
      'WHERE imv.Conta=:P0'
      ')')
    Left = 508
    Top = 496
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrICCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrICTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrICNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrICCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrICIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrICNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 15
    end
    object QrICRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrICCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrICBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrICCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 60
    end
    object QrICNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrICNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrICPais: TWideStringField
      FieldName = 'Pais'
      Size = 60
    end
    object QrICTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrICFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrICNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Size = 4
    end
    object QrICNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrICLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrICCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object frxDsCNAB_Cfg_B: TfrxDBDataset
    UserName = 'frxDsCNAB_Cfg_B'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEBANCO=NOMEBANCO'
      'LocalPag=LocalPag'
      'NOMECED=NOMECED'
      'EspecieDoc=EspecieDoc'
      'ACEITETIT_TXT=ACEITETIT_TXT'
      'CART_IMP=CART_IMP'
      'EspecieVal=EspecieVal'
      'CedBanco=CedBanco'
      'AgContaCed=AgContaCed'
      'CedAgencia=CedAgencia'
      'CedPosto=CedPosto'
      'CedConta=CedConta'
      'CartNum=CartNum'
      'IDCobranca=IDCobranca'
      'CodEmprBco=CodEmprBco'
      'TipoCobranca=TipoCobranca'
      'CNAB=CNAB'
      'CtaCooper=CtaCooper'
      'ModalCobr=ModalCobr'
      'MultaPerc=MultaPerc'
      'JurosPerc=JurosPerc'
      'Texto01=Texto01'
      'Texto02=Texto02'
      'Texto03=Texto03'
      'Texto04=Texto04'
      'Texto05=Texto05'
      'Texto06=Texto06'
      'Texto07=Texto07'
      'Texto08=Texto08'
      'CedDAC_C=CedDAC_C'
      'CedDAC_A=CedDAC_A'
      'OperCodi=OperCodi'
      'LayoutRem=LayoutRem'
      'NOMESAC=NOMESAC'
      'Correio=Correio'
      'CartEmiss=CartEmiss'
      'Cedente=Cedente'
      'CAR_TIPODOC=CAR_TIPODOC'
      'CART_ATIVO=CART_ATIVO'
      'DVB=DVB'
      'Codigo=Codigo'
      'NosNumFxaU=NosNumFxaU'
      'NosNumFxaI=NosNumFxaI'
      'NosNumFxaF=NosNumFxaF'
      'Status=Status'
      'CorresBco=CorresBco'
      'CorresAge=CorresAge'
      'CorresCto=CorresCto'
      'NPrinBc=NPrinBc')
    DataSet = QrCNAB_Cfg_B
    BCDToCurrency = False
    Left = 276
    Top = 188
  end
  object QrCNAB_Cfg_B: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      
        'IF(ent.Tipo=0, ent.EUF, ent.PUF) + 0.000 UF, ufs.Nome NOMEUF, bc' +
        's.DVB,'
      'bcs.Nome NOMEBANCO, car.Ativo CART_ATIVO,'
      'car.TipoDoc CAR_TIPODOC, con.*,'
      'IF(ced.Tipo=0, ced.RazaoSocial, ced.Nome) NOMECED,'
      'IF(sac.Tipo=0, sac.RazaoSocial, sac.Nome) NOMESAC,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCNUM'
      'FROM cond con'
      'LEFT JOIN entidades ced ON ced.Codigo=con.Cedente'
      'LEFT JOIN entidades sac ON sac.Codigo=con.SacadAvali'
      'LEFT JOIN entidades ent ON ent.Codigo=con.Cliente'
      'LEFT JOIN bancos   bcs ON bcs.Codigo=con.Banco'
      'LEFT JOIN carteiras car ON car.Codigo=con.CartEmiss'
      
        'LEFT JOIN ufs   ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PU' +
        'F)'
      'WHERE con.Codigo=:P0')
    Left = 168
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_Cfg_BNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCNAB_Cfg_BLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Size = 127
    end
    object QrCNAB_Cfg_BNOMECED: TWideStringField
      FieldName = 'NOMECED'
      Size = 100
    end
    object QrCNAB_Cfg_BEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
    end
    object QrCNAB_Cfg_BACEITETIT_TXT: TWideStringField
      FieldName = 'ACEITETIT_TXT'
      Size = 1
    end
    object QrCNAB_Cfg_BCART_IMP: TWideStringField
      FieldName = 'CART_IMP'
      Size = 10
    end
    object QrCNAB_Cfg_BEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Size = 5
    end
    object QrCNAB_Cfg_BCedBanco: TIntegerField
      FieldName = 'CedBanco'
    end
    object QrCNAB_Cfg_BAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Size = 40
    end
    object QrCNAB_Cfg_BCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
    end
    object QrCNAB_Cfg_BCedPosto: TIntegerField
      FieldName = 'CedPosto'
    end
    object QrCNAB_Cfg_BCedConta: TWideStringField
      FieldName = 'CedConta'
      Size = 30
    end
    object QrCNAB_Cfg_BCartNum: TWideStringField
      FieldName = 'CartNum'
      Size = 3
    end
    object QrCNAB_Cfg_BIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Size = 2
    end
    object QrCNAB_Cfg_BCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
    end
    object QrCNAB_Cfg_BTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrCNAB_Cfg_BCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrCNAB_Cfg_BCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCNAB_Cfg_BModalCobr: TIntegerField
      FieldName = 'ModalCobr'
    end
    object QrCNAB_Cfg_BMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrCNAB_Cfg_BJurosPerc: TFloatField
      FieldName = 'JurosPerc'
    end
    object QrCNAB_Cfg_BTexto01: TWideStringField
      FieldName = 'Texto01'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto02: TWideStringField
      FieldName = 'Texto02'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto03: TWideStringField
      FieldName = 'Texto03'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto04: TWideStringField
      FieldName = 'Texto04'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto05: TWideStringField
      FieldName = 'Texto05'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto06: TWideStringField
      FieldName = 'Texto06'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto07: TWideStringField
      FieldName = 'Texto07'
      Size = 100
    end
    object QrCNAB_Cfg_BTexto08: TWideStringField
      FieldName = 'Texto08'
      Size = 100
    end
    object QrCNAB_Cfg_BCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Size = 1
    end
    object QrCNAB_Cfg_BCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Size = 1
    end
    object QrCNAB_Cfg_BOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Size = 3
    end
    object QrCNAB_Cfg_BLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrCNAB_Cfg_BNOMESAC: TWideStringField
      FieldName = 'NOMESAC'
      Size = 100
    end
    object QrCNAB_Cfg_BCorreio: TWideStringField
      FieldName = 'Correio'
      Size = 1
    end
    object QrCNAB_Cfg_BCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrCNAB_Cfg_BCedente: TIntegerField
      FieldName = 'Cedente'
    end
    object QrCNAB_Cfg_BCAR_TIPODOC: TIntegerField
      FieldName = 'CAR_TIPODOC'
    end
    object QrCNAB_Cfg_BCART_ATIVO: TIntegerField
      FieldName = 'CART_ATIVO'
    end
    object QrCNAB_Cfg_BDVB: TWideStringField
      FieldName = 'DVB'
      Size = 1
    end
    object QrCNAB_Cfg_BCodigo: TFloatField
      FieldName = 'Codigo'
    end
    object QrCNAB_Cfg_BNosNumFxaU: TFloatField
      FieldName = 'NosNumFxaU'
    end
    object QrCNAB_Cfg_BNosNumFxaI: TFloatField
      FieldName = 'NosNumFxaI'
    end
    object QrCNAB_Cfg_BNosNumFxaF: TFloatField
      FieldName = 'NosNumFxaF'
    end
    object QrCNAB_Cfg_BStatus: TFloatField
      FieldName = 'Status'
    end
    object QrCNAB_Cfg_BCorresBco: TIntegerField
      FieldName = 'CorresBco'
    end
    object QrCNAB_Cfg_BCorresAge: TIntegerField
      FieldName = 'CorresAge'
    end
    object QrCNAB_Cfg_BCorresCto: TWideStringField
      FieldName = 'CorresCto'
      Size = 30
    end
    object QrCNAB_Cfg_BNPrinBc: TWideStringField
      FieldName = 'NPrinBc'
      Size = 30
    end
  end
end
