object FmCond: TFmCond
  Left = 368
  Top = 194
  Caption = 'CAD-CONDO-001 :: Cadastro de Condom'#237'nios'
  ClientHeight = 716
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 664
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 86
      Width = 1008
      Height = 468
      ActivePage = TabSheet1
      Align = alTop
      TabHeight = 25
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = ' Estrutura f'#237'sica '
        object PageControl7: TPageControl
          Left = 0
          Top = 0
          Width = 1000
          Height = 433
          ActivePage = TabSheet31
          Align = alClient
          TabHeight = 25
          TabOrder = 0
          OnChange = PageControl7Change
          object TabSheet29: TTabSheet
            Caption = 'Unidades'
            object Panel41: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 398
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Splitter1: TSplitter
                Left = 343
                Top = 0
                Width = 5
                Height = 398
                ExplicitLeft = 201
                ExplicitHeight = 269
              end
              object DBGUnidades: TDBGrid
                Left = 348
                Top = 0
                Width = 644
                Height = 398
                Align = alClient
                DataSource = DsCondImov
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDblClick = DBGUnidadesDblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Title.Caption = 'Controle'
                    Width = 58
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Andar'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    Width = 35
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Unidade'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Juridico_TXT'
                    Title.Caption = 'SJ'
                    Width = 20
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPROP'
                    Title.Caption = 'Propriet'#225'rio'
                    Width = 245
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'InadCEMCad'
                    Title.Caption = 'msgs'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Moradores'
                    Title.Caption = 'Pessoas'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FracaoIdeal'
                    Title.Caption = 'Fra'#231#227'o ideal'
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'BloqEndTip_TXT'
                    Title.Caption = 'End. entr.blq.'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESITIMV'
                    Title.Caption = 'Rateia'
                    Width = 36
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEUSUARIO'
                    Title.Caption = 'Inquilino'
                    Width = 198
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'STATUS1'
                    Title.Caption = 'Status'
                    Width = 82
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPROTOCOLO'
                    Title.Caption = 'Protocolo de entrega de boleto 1'
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPROTOCOLO2'
                    Title.Caption = 'Protocolo de entrega de boleto 2'
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPROTOCOLO3'
                    Title.Caption = 'Protocolo de entrega de boleto 3'
                    Width = 200
                    Visible = True
                  end>
              end
              object Panel42: TPanel
                Left = 0
                Top = 0
                Width = 343
                Height = 398
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 1
                object DBGrid1: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 343
                  Height = 109
                  Align = alClient
                  DataSource = DsCondBloco
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Width = 43
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Descri'
                      Title.Caption = 'Bloco'
                      Width = 121
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PrefixoUH'
                      Title.Caption = 'Prefixo UH'
                      Width = 59
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SomaFracao'
                      Title.Caption = 'Fra'#231#227'o ideal'
                      Width = 80
                      Visible = True
                    end>
                end
                object Panel43: TPanel
                  Left = 0
                  Top = 109
                  Width = 343
                  Height = 49
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                  object Label205: TLabel
                    Left = 176
                    Top = 4
                    Width = 72
                    Height = 13
                    Caption = 'Parte do nome:'
                  end
                  object RGTipoEnt: TRadioGroup
                    Left = 1
                    Top = 1
                    Width = 172
                    Height = 40
                    Caption = ' Tipo de entidade: '
                    Columns = 2
                    ItemIndex = 0
                    Items.Strings = (
                      'Propriet'#225'rio'
                      'Morador')
                    TabOrder = 0
                    OnClick = RGTipoEntClick
                  end
                  object EdParcial: TEdit
                    Left = 176
                    Top = 20
                    Width = 160
                    Height = 21
                    TabOrder = 1
                    OnChange = EdParcialChange
                  end
                end
                object GradePesq: TdmkDBGrid
                  Left = 0
                  Top = 158
                  Width = 343
                  Height = 240
                  Align = alBottom
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_BLOCO'
                      Title.Caption = 'Bloco'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Unidade'
                      Width = 70
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_ENT'
                      Title.Caption = 'Nome entidade'
                      Visible = True
                    end>
                  Color = clWindow
                  DataSource = DsPesq
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 2
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDblClick = GradePesqDblClick
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_BLOCO'
                      Title.Caption = 'Bloco'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Unidade'
                      Width = 70
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_ENT'
                      Title.Caption = 'Nome entidade'
                      Visible = True
                    end>
                end
              end
            end
          end
          object TabSheet31: TTabSheet
            Caption = 'Unidade selecionada'
            ImageIndex = 1
            object DBGrid9: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 41
              Align = alTop
              DataSource = DsCondImov
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'Bloco'
                  Width = 39
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Andar'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  Width = 35
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'Controle'
                  Width = 58
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Unidade'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  Width = 80
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROP'
                  Title.Caption = 'Propriet'#225'rio'
                  Width = 245
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'STATUS1'
                  Title.Caption = 'Status'
                  Width = 82
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROTOCOLO'
                  Title.Caption = 'Protocolo de entrega de boleto'
                  Width = 198
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMESITIMV'
                  Title.Caption = 'Rateia'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEUSUARIO'
                  Title.Caption = 'Inquilino'
                  Width = 172
                  Visible = True
                end>
            end
            object PageControl2: TPageControl
              Left = 0
              Top = 41
              Width = 992
              Height = 357
              ActivePage = TabSheet23
              Align = alClient
              TabHeight = 25
              TabOrder = 1
              object TabSheet4: TTabSheet
                Caption = 'Geral'
                object Label8: TLabel
                  Left = 8
                  Top = -2
                  Width = 42
                  Height = 13
                  Caption = 'C'#244'njuge:'
                end
                object Label129: TLabel
                  Left = 8
                  Top = 46
                  Width = 49
                  Height = 13
                  Caption = 'Imobili'#225'ria:'
                end
                object Label130: TLabel
                  Left = 257
                  Top = 46
                  Width = 45
                  Height = 13
                  Caption = 'Telefone:'
                end
                object Label131: TLabel
                  Left = 8
                  Top = 86
                  Width = 40
                  Height = 13
                  Caption = 'Contato:'
                end
                object Bevel2: TBevel
                  Left = 4
                  Top = 44
                  Width = 377
                  Height = 81
                end
                object DBEdit014: TDBEdit
                  Left = 8
                  Top = 13
                  Width = 369
                  Height = 21
                  Color = clWhite
                  DataField = 'NOMECONJUGE'
                  DataSource = DsCondImov
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object DBEdit063: TDBEdit
                  Left = 8
                  Top = 61
                  Width = 245
                  Height = 21
                  Color = clWhite
                  DataField = 'IMOB'
                  DataSource = DsCondImov
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
                object DBEdit064: TDBEdit
                  Left = 257
                  Top = 61
                  Width = 120
                  Height = 21
                  Color = clWhite
                  DataField = 'ContTel'
                  DataSource = DsCondImov
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                end
                object DBEdit065: TDBEdit
                  Left = 8
                  Top = 101
                  Width = 369
                  Height = 21
                  Color = clWhite
                  DataField = 'Contato'
                  DataSource = DsCondImov
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                end
              end
              object TabSheet5: TTabSheet
                Caption = 'Filhos'
                ImageIndex = 1
                object DBGrid5: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 984
                  Height = 322
                  Align = alClient
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'SubConta'
                      Title.Caption = 'Controle'
                      Width = 44
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFILHO'
                      Title.Caption = 'Nome do filho'
                      Width = 352
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PNATAL'
                      Title.Caption = 'Data de nasc.'
                      Width = 85
                      Visible = True
                    end>
                end
              end
              object TabSheet6: TTabSheet
                Caption = 'Moradores'
                ImageIndex = 2
                object DBGrid7: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 984
                  Height = 322
                  Align = alClient
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'SubConta'
                      Title.Caption = 'Controle'
                      Width = 47
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOUTMOR'
                      Title.Caption = 'Morador'
                      Width = 336
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Parentesco'
                      Width = 169
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DataNasc'
                      Title.Caption = 'Data de nasc.'
                      Width = 86
                      Visible = True
                    end>
                end
              end
              object TabSheet7: TTabSheet
                Caption = 'Animais'
                ImageIndex = 3
                object DBGrid4: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 984
                  Height = 322
                  Align = alClient
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'SubConta'
                      Title.Caption = 'Controle'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOMEANIMAL'
                      Title.Caption = 'Esp'#233'cie'
                      Width = 245
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Qtd'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Descri'
                      Title.Caption = 'Descri'#231#227'o'
                      Width = 199
                      Visible = True
                    end>
                end
              end
              object TabSheet16: TTabSheet
                Caption = 'Ve'#237'culos'
                ImageIndex = 4
                object DBGrid6: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 984
                  Height = 322
                  Align = alClient
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'SubConta'
                      Title.Caption = 'Controle'
                      Width = 47
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Nome'
                      Title.Caption = 'Descri'#231#227'o'
                      Width = 225
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Placa'
                      Width = 85
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Cor'
                      Width = 104
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AnoF'
                      Title.Caption = 'Ano fabric.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AnoM'
                      Title.Caption = 'Ano modelo'
                      Visible = True
                    end>
                end
              end
              object TabSheet17: TTabSheet
                Caption = 'Empregados dom'#233'sticos'
                ImageIndex = 5
                object PageControl6: TPageControl
                  Left = 0
                  Top = 0
                  Width = 984
                  Height = 322
                  ActivePage = TabSheet20
                  Align = alClient
                  TabOrder = 0
                  object TabSheet20: TTabSheet
                    Caption = 'Empregado 1'
                    object Label12: TLabel
                      Left = 8
                      Top = 2
                      Width = 31
                      Height = 13
                      Caption = 'Nome:'
                    end
                    object DBEdit015: TDBEdit
                      Left = 8
                      Top = 17
                      Width = 369
                      Height = 21
                      Color = clWhite
                      DataField = 'NOMEEMP1'
                      DataSource = DsCondImov
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                    end
                    object GroupBox18: TGroupBox
                      Left = 4
                      Top = 43
                      Width = 236
                      Height = 54
                      Caption = 'Autoriza'#231#227'o para'
                      TabOrder = 1
                      object DBRadioGroup5: TDBRadioGroup
                        Left = 10
                        Top = 14
                        Width = 104
                        Height = 33
                        Caption = 'Sair com ve'#237'culos'
                        Columns = 2
                        DataField = 'EVeic1'
                        DataSource = DsCondImov
                        Items.Strings = (
                          'Sim'
                          'N'#227'o')
                        ParentBackground = True
                        TabOrder = 0
                        Values.Strings = (
                          '0'
                          '1')
                      end
                      object DBRadioGroup6: TDBRadioGroup
                        Left = 122
                        Top = 14
                        Width = 104
                        Height = 33
                        Caption = 'Sair com filhos'
                        Columns = 2
                        DataField = 'EFilho1'
                        DataSource = DsCondImov
                        Items.Strings = (
                          'Sim'
                          'N'#227'o')
                        ParentBackground = True
                        TabOrder = 1
                        Values.Strings = (
                          '0'
                          '1')
                      end
                    end
                    object GroupBox27: TGroupBox
                      Left = 382
                      Top = 6
                      Width = 323
                      Height = 74
                      Caption = 'Periodicidade: '
                      TabOrder = 2
                      object DBCheckBox15: TDBCheckBox
                        Left = 5
                        Top = 17
                        Width = 65
                        Height = 17
                        Caption = 'Segunda'
                        DataField = 'ESegunda1'
                        DataSource = DsCondImov
                        TabOrder = 0
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox16: TDBCheckBox
                        Left = 72
                        Top = 17
                        Width = 54
                        Height = 17
                        Caption = 'Ter'#231'a'
                        DataField = 'ETerca1'
                        DataSource = DsCondImov
                        TabOrder = 1
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox17: TDBCheckBox
                        Left = 125
                        Top = 17
                        Width = 53
                        Height = 17
                        Caption = 'Quarta'
                        DataField = 'EQuarta1'
                        DataSource = DsCondImov
                        TabOrder = 2
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox18: TDBCheckBox
                        Left = 72
                        Top = 38
                        Width = 49
                        Height = 17
                        Caption = 'Quinta'
                        DataField = 'EQuinta1'
                        DataSource = DsCondImov
                        TabOrder = 3
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox19: TDBCheckBox
                        Left = 5
                        Top = 38
                        Width = 49
                        Height = 17
                        Caption = 'Sexta'
                        DataField = 'ESexta1'
                        DataSource = DsCondImov
                        TabOrder = 4
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object GroupBox28: TGroupBox
                        Left = 187
                        Top = 12
                        Width = 130
                        Height = 55
                        Caption = 'Hor'#225'rio m'#225'ximo: '
                        TabOrder = 5
                        object Label79: TLabel
                          Left = 9
                          Top = 12
                          Width = 40
                          Height = 13
                          Caption = 'Entrada:'
                        end
                        object Label80: TLabel
                          Left = 69
                          Top = 14
                          Width = 32
                          Height = 13
                          Caption = 'Sa'#237'da:'
                        end
                        object DBEdit028: TDBEdit
                          Left = 69
                          Top = 27
                          Width = 55
                          Height = 21
                          TabOrder = 0
                        end
                        object DBEdit029: TDBEdit
                          Left = 9
                          Top = 26
                          Width = 55
                          Height = 21
                          TabOrder = 1
                        end
                      end
                    end
                    object GroupBox29: TGroupBox
                      Left = 712
                      Top = 6
                      Width = 220
                      Height = 74
                      Caption = 'Periodicidade: '
                      TabOrder = 3
                      object GroupBox30: TGroupBox
                        Left = 81
                        Top = 14
                        Width = 130
                        Height = 55
                        Caption = 'Hor'#225'rio m'#225'ximo: '
                        TabOrder = 0
                        object Label81: TLabel
                          Left = 9
                          Top = 12
                          Width = 40
                          Height = 13
                          Caption = 'Entrada:'
                        end
                        object Label82: TLabel
                          Left = 69
                          Top = 14
                          Width = 32
                          Height = 13
                          Caption = 'Sa'#237'da:'
                        end
                        object DBEdit030: TDBEdit
                          Left = 69
                          Top = 27
                          Width = 55
                          Height = 21
                          TabOrder = 0
                        end
                        object DBEdit031: TDBEdit
                          Left = 9
                          Top = 26
                          Width = 55
                          Height = 21
                          TabOrder = 1
                        end
                      end
                      object DBCheckBox20: TDBCheckBox
                        Left = 9
                        Top = 24
                        Width = 65
                        Height = 17
                        Caption = 'S'#225'bado'
                        DataField = 'ESabado1'
                        DataSource = DsCondImov
                        TabOrder = 1
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox21: TDBCheckBox
                        Left = 9
                        Top = 44
                        Width = 64
                        Height = 17
                        Caption = 'Domingo'
                        DataField = 'EDomingo1'
                        DataSource = DsCondImov
                        TabOrder = 2
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                    end
                  end
                  object TabSheet21: TTabSheet
                    Caption = 'Empregado 2'
                    ImageIndex = 1
                    object Label13: TLabel
                      Left = 8
                      Top = 2
                      Width = 31
                      Height = 13
                      Caption = 'Nome:'
                    end
                    object DBEdit016: TDBEdit
                      Left = 8
                      Top = 17
                      Width = 369
                      Height = 21
                      Color = clWhite
                      DataField = 'NOMEEMP2'
                      DataSource = DsCondImov
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                    end
                    object GroupBox17: TGroupBox
                      Left = 4
                      Top = 43
                      Width = 236
                      Height = 54
                      Caption = 'Autoriza'#231#227'o para'
                      TabOrder = 1
                      object DBRadioGroup3: TDBRadioGroup
                        Left = 10
                        Top = 14
                        Width = 104
                        Height = 33
                        Caption = 'Sair com ve'#237'culos'
                        Columns = 2
                        DataField = 'EVeic2'
                        DataSource = DsCondImov
                        Items.Strings = (
                          'Sim'
                          'N'#227'o')
                        ParentBackground = True
                        TabOrder = 0
                        Values.Strings = (
                          '0'
                          '1')
                      end
                      object DBRadioGroup4: TDBRadioGroup
                        Left = 122
                        Top = 14
                        Width = 104
                        Height = 33
                        Caption = 'Sair com filhos'
                        Columns = 2
                        DataField = 'EFilho2'
                        DataSource = DsCondImov
                        Items.Strings = (
                          'Sim'
                          'N'#227'o')
                        ParentBackground = True
                        TabOrder = 1
                        Values.Strings = (
                          '0'
                          '1')
                      end
                    end
                    object GroupBox23: TGroupBox
                      Left = 382
                      Top = 6
                      Width = 323
                      Height = 74
                      Caption = 'Periodicidade: '
                      TabOrder = 2
                      object DBCheckBox8: TDBCheckBox
                        Left = 5
                        Top = 17
                        Width = 65
                        Height = 17
                        Caption = 'Segunda'
                        DataField = 'ESegunda2'
                        DataSource = DsCondImov
                        TabOrder = 0
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox9: TDBCheckBox
                        Left = 72
                        Top = 17
                        Width = 54
                        Height = 17
                        Caption = 'Ter'#231'a'
                        DataField = 'ETerca2'
                        DataSource = DsCondImov
                        TabOrder = 1
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox10: TDBCheckBox
                        Left = 125
                        Top = 17
                        Width = 53
                        Height = 17
                        Caption = 'Quarta'
                        DataField = 'EQuarta2'
                        DataSource = DsCondImov
                        TabOrder = 2
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox11: TDBCheckBox
                        Left = 72
                        Top = 38
                        Width = 49
                        Height = 17
                        Caption = 'Quinta'
                        DataField = 'EQuinta2'
                        DataSource = DsCondImov
                        TabOrder = 3
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox12: TDBCheckBox
                        Left = 5
                        Top = 38
                        Width = 49
                        Height = 17
                        Caption = 'Sexta'
                        DataField = 'ESexta2'
                        DataSource = DsCondImov
                        TabOrder = 4
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object GroupBox24: TGroupBox
                        Left = 187
                        Top = 12
                        Width = 130
                        Height = 55
                        Caption = 'Hor'#225'rio m'#225'ximo: '
                        TabOrder = 5
                        object Label75: TLabel
                          Left = 9
                          Top = 12
                          Width = 40
                          Height = 13
                          Caption = 'Entrada:'
                        end
                        object Label76: TLabel
                          Left = 69
                          Top = 14
                          Width = 32
                          Height = 13
                          Caption = 'Sa'#237'da:'
                        end
                        object DBEdit024: TDBEdit
                          Left = 69
                          Top = 27
                          Width = 55
                          Height = 21
                          TabOrder = 0
                        end
                        object DBEdit025: TDBEdit
                          Left = 9
                          Top = 26
                          Width = 55
                          Height = 21
                          TabOrder = 1
                        end
                      end
                    end
                    object GroupBox25: TGroupBox
                      Left = 712
                      Top = 6
                      Width = 220
                      Height = 74
                      Caption = 'Periodicidade: '
                      TabOrder = 3
                      object GroupBox26: TGroupBox
                        Left = 81
                        Top = 14
                        Width = 130
                        Height = 55
                        Caption = 'Hor'#225'rio m'#225'ximo: '
                        TabOrder = 0
                        object Label77: TLabel
                          Left = 9
                          Top = 12
                          Width = 40
                          Height = 13
                          Caption = 'Entrada:'
                        end
                        object Label78: TLabel
                          Left = 69
                          Top = 14
                          Width = 32
                          Height = 13
                          Caption = 'Sa'#237'da:'
                        end
                        object DBEdit026: TDBEdit
                          Left = 69
                          Top = 27
                          Width = 55
                          Height = 21
                          TabOrder = 0
                        end
                        object DBEdit027: TDBEdit
                          Left = 9
                          Top = 26
                          Width = 55
                          Height = 21
                          TabOrder = 1
                        end
                      end
                      object DBCheckBox13: TDBCheckBox
                        Left = 9
                        Top = 24
                        Width = 65
                        Height = 17
                        Caption = 'S'#225'bado'
                        DataField = 'ESabado2'
                        DataSource = DsCondImov
                        TabOrder = 1
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox14: TDBCheckBox
                        Left = 9
                        Top = 44
                        Width = 64
                        Height = 17
                        Caption = 'Domingo'
                        DataField = 'EDomingo2'
                        DataSource = DsCondImov
                        TabOrder = 2
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                    end
                  end
                  object TabSheet22: TTabSheet
                    Caption = 'Empregado 3'
                    ImageIndex = 2
                    object Label18: TLabel
                      Left = 8
                      Top = 2
                      Width = 31
                      Height = 13
                      Caption = 'Nome:'
                    end
                    object DBEdit017: TDBEdit
                      Left = 8
                      Top = 17
                      Width = 369
                      Height = 21
                      Color = clWhite
                      DataField = 'NOMEEMP3'
                      DataSource = DsCondImov
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                    end
                    object GroupBox16: TGroupBox
                      Left = 4
                      Top = 43
                      Width = 236
                      Height = 54
                      Caption = 'Autoriza'#231#227'o para'
                      TabOrder = 1
                      object DBRadioGroup1: TDBRadioGroup
                        Left = 10
                        Top = 14
                        Width = 104
                        Height = 33
                        Caption = 'Sair com ve'#237'culos'
                        Columns = 2
                        DataField = 'EVeic3'
                        DataSource = DsCondImov
                        Items.Strings = (
                          'Sim'
                          'N'#227'o')
                        ParentBackground = True
                        TabOrder = 0
                        Values.Strings = (
                          '0'
                          '1')
                      end
                      object DBRadioGroup2: TDBRadioGroup
                        Left = 122
                        Top = 14
                        Width = 104
                        Height = 33
                        Caption = 'Sair com filhos'
                        Columns = 2
                        DataField = 'EFilho3'
                        DataSource = DsCondImov
                        Items.Strings = (
                          'Sim'
                          'N'#227'o')
                        ParentBackground = True
                        TabOrder = 1
                        Values.Strings = (
                          '0'
                          '1')
                      end
                    end
                    object GroupBox19: TGroupBox
                      Left = 382
                      Top = 6
                      Width = 323
                      Height = 74
                      Caption = 'Periodicidade: '
                      TabOrder = 2
                      object DBCheckBox1: TDBCheckBox
                        Left = 5
                        Top = 17
                        Width = 65
                        Height = 17
                        Caption = 'Segunda'
                        DataField = 'ESegunda3'
                        DataSource = DsCondImov
                        TabOrder = 0
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox2: TDBCheckBox
                        Left = 72
                        Top = 17
                        Width = 54
                        Height = 17
                        Caption = 'Ter'#231'a'
                        DataField = 'ETerca3'
                        DataSource = DsCondImov
                        TabOrder = 1
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox3: TDBCheckBox
                        Left = 125
                        Top = 17
                        Width = 53
                        Height = 17
                        Caption = 'Quarta'
                        DataField = 'EQuarta3'
                        DataSource = DsCondImov
                        TabOrder = 2
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox4: TDBCheckBox
                        Left = 72
                        Top = 38
                        Width = 49
                        Height = 17
                        Caption = 'Quinta'
                        DataField = 'EQuinta3'
                        DataSource = DsCondImov
                        TabOrder = 3
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox5: TDBCheckBox
                        Left = 5
                        Top = 38
                        Width = 49
                        Height = 17
                        Caption = 'Sexta'
                        DataField = 'ESexta3'
                        DataSource = DsCondImov
                        TabOrder = 4
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object GroupBox20: TGroupBox
                        Left = 187
                        Top = 12
                        Width = 130
                        Height = 55
                        Caption = 'Hor'#225'rio m'#225'ximo: '
                        TabOrder = 5
                        object Label66: TLabel
                          Left = 9
                          Top = 12
                          Width = 40
                          Height = 13
                          Caption = 'Entrada:'
                        end
                        object Label67: TLabel
                          Left = 69
                          Top = 14
                          Width = 32
                          Height = 13
                          Caption = 'Sa'#237'da:'
                        end
                        object DBEdit018: TDBEdit
                          Left = 69
                          Top = 27
                          Width = 55
                          Height = 21
                          TabOrder = 0
                        end
                        object DBEdit019: TDBEdit
                          Left = 9
                          Top = 26
                          Width = 55
                          Height = 21
                          TabOrder = 1
                        end
                      end
                    end
                    object GroupBox21: TGroupBox
                      Left = 712
                      Top = 6
                      Width = 220
                      Height = 74
                      Caption = 'Periodicidade: '
                      TabOrder = 3
                      object GroupBox22: TGroupBox
                        Left = 81
                        Top = 14
                        Width = 130
                        Height = 55
                        Caption = 'Hor'#225'rio m'#225'ximo: '
                        TabOrder = 0
                        object Label68: TLabel
                          Left = 9
                          Top = 12
                          Width = 40
                          Height = 13
                          Caption = 'Entrada:'
                        end
                        object Label74: TLabel
                          Left = 69
                          Top = 14
                          Width = 32
                          Height = 13
                          Caption = 'Sa'#237'da:'
                        end
                        object DBEdit020: TDBEdit
                          Left = 69
                          Top = 27
                          Width = 55
                          Height = 21
                          TabOrder = 0
                        end
                        object DBEdit023: TDBEdit
                          Left = 9
                          Top = 26
                          Width = 55
                          Height = 21
                          TabOrder = 1
                        end
                      end
                      object DBCheckBox6: TDBCheckBox
                        Left = 9
                        Top = 24
                        Width = 65
                        Height = 17
                        Caption = 'S'#225'bado'
                        DataField = 'ESabado3'
                        DataSource = DsCondImov
                        TabOrder = 1
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox7: TDBCheckBox
                        Left = 9
                        Top = 44
                        Width = 64
                        Height = 17
                        Caption = 'Domingo'
                        DataField = 'EDomingo3'
                        DataSource = DsCondImov
                        TabOrder = 2
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                    end
                  end
                end
              end
              object TabSheet18: TTabSheet
                Caption = 'Emerg'#234'ncias'
                ImageIndex = 6
                object Label14: TLabel
                  Left = 8
                  Top = 3
                  Width = 31
                  Height = 13
                  Caption = 'Nome:'
                end
                object Label15: TLabel
                  Left = 380
                  Top = 1
                  Width = 45
                  Height = 13
                  Caption = 'Telefone:'
                end
                object Label16: TLabel
                  Left = 8
                  Top = 45
                  Width = 31
                  Height = 13
                  Caption = 'Nome:'
                end
                object Label17: TLabel
                  Left = 380
                  Top = 43
                  Width = 45
                  Height = 13
                  Caption = 'Telefone:'
                end
                object DBEdit01: TDBEdit
                  Left = 8
                  Top = 18
                  Width = 369
                  Height = 21
                  Color = clWhite
                  DataField = 'EmNome1'
                  DataSource = DsCondImov
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object DBEdit02: TDBEdit
                  Left = 380
                  Top = 17
                  Width = 113
                  Height = 21
                  DataField = 'EmTel1'
                  DataSource = DsCondImov
                  TabOrder = 1
                end
                object DBEdit012: TDBEdit
                  Left = 8
                  Top = 60
                  Width = 369
                  Height = 21
                  Color = clWhite
                  DataField = 'EmNome2'
                  DataSource = DsCondImov
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                end
                object DBEdit013: TDBEdit
                  Left = 380
                  Top = 59
                  Width = 113
                  Height = 21
                  DataField = 'EmTel2'
                  DataSource = DsCondImov
                  TabOrder = 3
                end
              end
              object TabSheet19: TTabSheet
                Caption = 'Observa'#231#245'es'
                ImageIndex = 7
                object DBMemo2: TDBMemo
                  Left = 0
                  Top = 0
                  Width = 984
                  Height = 322
                  Align = alClient
                  DataField = 'Observ'
                  DataSource = DsCondImov
                  TabOrder = 0
                end
              end
              object TabSheet23: TTabSheet
                Caption = 'E-mails para envio de boleto'
                ImageIndex = 8
                object StaticText1: TStaticText
                  Left = 0
                  Top = 0
                  Width = 984
                  Height = 33
                  Align = alTop
                  AutoSize = False
                  Caption = 
                    '  AVISO: Ao incluir e-mails na lista abaixo os e-mails do inquil' +
                    'ino e do dono ser'#227'o desconsiderados e somente os e-mails da list' +
                    'a ser'#227'o utilizados.'
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clActiveCaption
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentColor = False
                  ParentFont = False
                  TabOrder = 0
                  Visible = False
                end
                object Panel36: TPanel
                  Left = 0
                  Top = 33
                  Width = 984
                  Height = 289
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                  object Panel37: TPanel
                    Left = 0
                    Top = 0
                    Width = 104
                    Height = 289
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object BtEmeio: TBitBtn
                      Tag = 66
                      Left = 8
                      Top = 3
                      Width = 90
                      Height = 40
                      Cursor = crHandPoint
                      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
                      Caption = '&E-mail'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      NumGlyphs = 2
                      ParentFont = False
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtEmeioClick
                    end
                    object BtPesquisa: TBitBtn
                      Tag = 40
                      Left = 8
                      Top = 49
                      Width = 90
                      Height = 40
                      Cursor = crHandPoint
                      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
                      Caption = '&Pesquisa'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      NumGlyphs = 2
                      ParentFont = False
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 1
                      OnClick = BtPesquisaClick
                    end
                  end
                  object DBGrid102: TDBGrid
                    Left = 104
                    Top = 0
                    Width = 804
                    Height = 289
                    Align = alLeft
                    DataSource = DsCondEmeios
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Item'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Pronome'
                        Width = 100
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Nome'
                        Width = 300
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'EMeio'
                        Title.Caption = 'E-mail'
                        Width = 300
                        Visible = True
                      end>
                  end
                  object Panel38: TPanel
                    Left = 908
                    Top = 0
                    Width = 76
                    Height = 289
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 2
                  end
                end
              end
            end
          end
          object TabSheet34: TTabSheet
            Caption = 'Edit'#225'vel'
            ImageIndex = 2
            object DBGrid2: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 398
              Align = alClient
              DataSource = DsUnidades
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOME_BLOCO'
                  Title.Caption = 'Bloco'
                  Width = 148
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Unidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_PROPRIET'
                  Title.Caption = 'Propriet'#225'rio'
                  Width = 489
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FracaoIdeal'
                  Title.Caption = 'Fra'#231#227'o ideal'
                  Width = 115
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IDExporta'
                  Title.Caption = 'Nome Exporta'
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Geral '
        ImageIndex = 1
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 433
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label5: TLabel
            Left = 0
            Top = 0
            Width = 1000
            Height = 13
            Align = alTop
            Caption = 'Observa'#231#245'es:'
            ExplicitWidth = 66
          end
          object DBMemo1: TDBMemo
            Left = 0
            Top = 13
            Width = 1000
            Height = 420
            Align = alClient
            DataField = 'Observ'
            DataSource = DsCond
            TabOrder = 0
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Conselho fiscal'
        ImageIndex = 2
        object PageControl5: TPageControl
          Left = 0
          Top = 0
          Width = 1000
          Height = 433
          ActivePage = TabSheet26
          Align = alClient
          TabHeight = 25
          TabOrder = 0
          object TabSheet26: TTabSheet
            Caption = 'Integrantes do conselho'
            object DBGEntiRespon: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 398
              Align = alClient
              DataSource = DsCondOutros
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'OrdemLista'
                  Title.Caption = 'Ordem lista'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 214
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_CARGO'
                  Title.Caption = 'Cargo'
                  Width = 112
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ASSINA'
                  Title.Caption = 'Assina'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MandatoIni_TXT'
                  Title.Caption = 'Ini. mandato'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MandatoFim_TXT'
                  Title.Caption = 'Fim mandato'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Entidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Te1_TXT'
                  Title.Caption = 'Telefone'
                  Width = 110
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Cel_TXT'
                  Title.Caption = 'Celular'
                  Width = 110
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Email'
                  Title.Caption = 'E-mail'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observ'
                  Title.Caption = 'Observa'#231#245'es'
                  Width = 350
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet8: TTabSheet
        Caption = ' Modelos e configura'#231#245'es de boletos espec'#237'ficos de UHs '
        ImageIndex = 4
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 433
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Unidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_MODELBLOQ'
              Title.Caption = 'M'
              Width = 16
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CONFIGBOL'
              Title.Caption = 'Configura'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compe'
              Title.Caption = 'Fichas de compensa'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BalAgrMens'
              Title.Caption = 'Agrupamentos'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCondModBol
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Unidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_MODELBLOQ'
              Title.Caption = 'M'
              Width = 16
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CONFIGBOL'
              Title.Caption = 'Configura'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compe'
              Title.Caption = 'Fichas de compensa'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BalAgrMens'
              Title.Caption = 'Agrupamentos'
              Visible = True
            end>
        end
      end
      object TabSheet9: TTabSheet
        Caption = 'Confer'#234'ncia de Provis'#245'es (sobre Contas Mensais)'
        ImageIndex = 5
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BtCtaMesExc: TBitBtn
            Tag = 12
            Left = 188
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Exclui registro atual'
            Caption = 'E&xclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtCtaMesExcClick
          end
          object BtCtaMesAlt: TBitBtn
            Tag = 11
            Left = 96
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Exclui registro atual'
            Caption = '&Altera'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtCtaMesAltClick
          end
          object BtCtaMesIns: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Inclui novo registro'
            Caption = 'I&nsere'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtCtaMesInsClick
          end
        end
        object GradeContasMes: TdmkDBGrid
          Left = 0
          Top = 48
          Width = 1000
          Height = 385
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descric'#227'o'
              Width = 600
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERIODOINI_TXT'
              Title.Caption = 'In'#237'cio'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERIODOFIM_TXT'
              Title.Caption = 'Final'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMin'
              Title.Caption = 'Valor m'#237'n.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMax'
              Title.Caption = 'Valor m'#225'x.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeMin'
              Title.Caption = 'Qtde m'#237'n.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeMax'
              Title.Caption = 'Qtde m'#225'x.'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsContasMes
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descric'#227'o'
              Width = 600
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERIODOINI_TXT'
              Title.Caption = 'In'#237'cio'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERIODOFIM_TXT'
              Title.Caption = 'Final'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMin'
              Title.Caption = 'Valor m'#237'n.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMax'
              Title.Caption = 'Valor m'#225'x.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeMin'
              Title.Caption = 'Qtde m'#237'n.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeMax'
              Title.Caption = 'Qtde m'#225'x.'
              Visible = True
            end>
        end
      end
      object TabSheet10: TTabSheet
        Caption = ' Fluxos '
        ImageIndex = 6
        object Panel44: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 433
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox31: TGroupBox
            Left = 0
            Top = 0
            Width = 248
            Height = 403
            Align = alLeft
            Caption = ' Boleto: '
            TabOrder = 0
            object Panel45: TPanel
              Left = 2
              Top = 15
              Width = 244
              Height = 37
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label71: TLabel
                Left = 12
                Top = 12
                Width = 149
                Height = 13
                Caption = 'Ordem na lista de condom'#237'nios:'
                FocusControl = EdFlxB_Ordem
              end
              object EdFlxB_Ordem: TDBEdit
                Left = 168
                Top = 8
                Width = 68
                Height = 21
                DataField = 'FlxB_Ordem'
                DataSource = DsCond
                TabOrder = 0
              end
            end
            object GroupBox39: TGroupBox
              Left = 2
              Top = 52
              Width = 244
              Height = 349
              Align = alClient
              Caption = ' Dia (m'#234's) limite* : '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object Label211: TLabel
                Left = 12
                Top = 140
                Width = 111
                Height = 13
                Caption = 'Gera'#231#227'o de protocolos:'
                FocusControl = EdFlxB_Proto
              end
              object Label212: TLabel
                Left = 12
                Top = 164
                Width = 50
                Height = 13
                Caption = 'Relat'#243'rios:'
                FocusControl = EdFlxB_Relat
              end
              object Label213: TLabel
                Left = 12
                Top = 188
                Width = 130
                Height = 13
                Caption = 'Envio de boletos por e-mail:'
                FocusControl = EdFlxB_EMail
              end
              object Label214: TLabel
                Left = 12
                Top = 212
                Width = 129
                Height = 13
                Caption = 'Envio de boletos '#224' portaria:'
                FocusControl = EdFlxB_Porta
              end
              object Label215: TLabel
                Left = 12
                Top = 236
                Width = 148
                Height = 13
                Caption = 'Envio de boletos via postagem:'
                FocusControl = EdFlxB_Postl
              end
              object Label210: TLabel
                Left = 12
                Top = 116
                Width = 108
                Height = 13
                Caption = 'Impress'#227'o dos boletos:'
                FocusControl = EdFlxB_Print
              end
              object Label209: TLabel
                Left = 12
                Top = 92
                Width = 117
                Height = 13
                Caption = 'Administradora de riscos:'
                FocusControl = EdFlxB_Risco
              end
              object Label208: TLabel
                Left = 12
                Top = 68
                Width = 62
                Height = 13
                Caption = 'Fechamento:'
              end
              object Label207: TLabel
                Left = 12
                Top = 44
                Width = 116
                Height = 13
                Caption = 'Leituras / arrecada'#231#245'es:'
                FocusControl = EdFlxB_LeiAr
              end
              object Label206: TLabel
                Left = 12
                Top = 20
                Width = 100
                Height = 13
                Caption = 'Folha de pagamento:'
                FocusControl = EdFlxB_Folha
              end
              object EdFlxB_Folha: TDBEdit
                Left = 180
                Top = 16
                Width = 24
                Height = 21
                DataField = 'FlxB_Folha'
                DataSource = DsCond
                TabOrder = 0
              end
              object EdFlxB_Proto: TDBEdit
                Left = 180
                Top = 136
                Width = 24
                Height = 21
                DataField = 'FlxB_Proto'
                DataSource = DsCond
                TabOrder = 1
              end
              object EdFlxB_Relat: TDBEdit
                Left = 180
                Top = 160
                Width = 24
                Height = 21
                DataField = 'FlxB_Relat'
                DataSource = DsCond
                TabOrder = 2
              end
              object EdFlxB_EMail: TDBEdit
                Left = 180
                Top = 184
                Width = 24
                Height = 21
                DataField = 'FlxB_EMail'
                DataSource = DsCond
                TabOrder = 3
              end
              object EdFlxB_Porta: TDBEdit
                Left = 180
                Top = 208
                Width = 24
                Height = 21
                DataField = 'FlxB_Porta'
                DataSource = DsCond
                TabOrder = 4
              end
              object EdFlxB_Postl: TDBEdit
                Left = 180
                Top = 232
                Width = 24
                Height = 21
                DataField = 'FlxB_Postl'
                DataSource = DsCond
                TabOrder = 5
              end
              object EdFlxB_Print: TDBEdit
                Left = 180
                Top = 112
                Width = 24
                Height = 21
                DataField = 'FlxB_Print'
                DataSource = DsCond
                TabOrder = 6
              end
              object EdFlxB_Risco: TDBEdit
                Left = 180
                Top = 88
                Width = 24
                Height = 21
                DataField = 'FlxB_Risco'
                DataSource = DsCond
                TabOrder = 7
              end
              object EdFlxB_LeiAr: TDBEdit
                Left = 180
                Top = 40
                Width = 24
                Height = 21
                DataField = 'FlxB_LeiAr'
                DataSource = DsCond
                TabOrder = 8
              end
              object EdFlxB_Fecha: TDBEdit
                Left = 180
                Top = 64
                Width = 24
                Height = 21
                DataField = 'FlxB_Fecha'
                DataSource = DsCond
                TabOrder = 9
              end
            end
          end
          object GroupBox38: TGroupBox
            Left = 248
            Top = 0
            Width = 752
            Height = 403
            Align = alClient
            Caption = ' Fechamento de per'#237'odo: '
            TabOrder = 1
            object Panel46: TPanel
              Left = 2
              Top = 15
              Width = 748
              Height = 37
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label223: TLabel
                Left = 12
                Top = 12
                Width = 149
                Height = 13
                Caption = 'Ordem na lista de condom'#237'nios:'
              end
              object EdFlxM_Ordem: TDBEdit
                Left = 172
                Top = 8
                Width = 68
                Height = 21
                DataField = 'FlxM_Ordem'
                DataSource = DsCond
                TabOrder = 0
              end
            end
            object GroupBox40: TGroupBox
              Left = 2
              Top = 52
              Width = 748
              Height = 349
              Align = alClient
              Caption = ' Dia (m'#234's) limite* : '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object Label217: TLabel
                Left = 12
                Top = 20
                Width = 63
                Height = 13
                Caption = 'Concilia'#231#245'es:'
                FocusControl = EdFlxM_Conci
              end
              object Label218: TLabel
                Left = 12
                Top = 44
                Width = 66
                Height = 13
                Caption = 'Documentos: '
                FocusControl = EdFlxM_Docum
              end
              object Label222: TLabel
                Left = 12
                Top = 140
                Width = 40
                Height = 13
                Caption = 'Entrega:'
                FocusControl = EdFlxM_Entrg
              end
              object Label221: TLabel
                Left = 12
                Top = 116
                Width = 76
                Height = 13
                Caption = 'Encadernac'#227'o: '
                FocusControl = EdFlxM_Encad
              end
              object Label220: TLabel
                Left = 12
                Top = 68
                Width = 94
                Height = 13
                Caption = 'ISS / contabilidade:'
                FocusControl = EdFlxM_Contb
              end
              object Label219: TLabel
                Left = 12
                Top = 92
                Width = 37
                Height = 13
                Caption = 'An'#225'lise:'
                FocusControl = EdFlxM_Anali
              end
              object EdFlxM_Conci: TDBEdit
                Left = 180
                Top = 16
                Width = 24
                Height = 21
                DataField = 'FlxM_Conci'
                DataSource = DsCond
                TabOrder = 0
              end
              object EdFlxM_Docum: TDBEdit
                Left = 180
                Top = 40
                Width = 24
                Height = 21
                DataField = 'FlxM_Docum'
                DataSource = DsCond
                TabOrder = 1
              end
              object EdFlxM_Entrg: TDBEdit
                Left = 180
                Top = 136
                Width = 24
                Height = 21
                DataField = 'FlxM_Entrg'
                DataSource = DsCond
                TabOrder = 2
              end
              object EdFlxM_Encad: TDBEdit
                Left = 180
                Top = 112
                Width = 24
                Height = 21
                DataField = 'FlxM_Encad'
                DataSource = DsCond
                TabOrder = 3
              end
              object EdFlxM_Contb: TDBEdit
                Left = 180
                Top = 64
                Width = 24
                Height = 21
                DataField = 'FlxM_Contb'
                DataSource = DsCond
                TabOrder = 4
              end
              object EdFlxM_Anali: TDBEdit
                Left = 180
                Top = 88
                Width = 24
                Height = 21
                DataField = 'FlxM_Anali'
                DataSource = DsCond
                TabOrder = 5
              end
            end
          end
          object Panel47: TPanel
            Left = 0
            Top = 403
            Width = 1000
            Height = 30
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 2
            object Label216: TLabel
              Left = 12
              Top = 8
              Width = 253
              Height = 13
              Caption = '*: Deixe zero se n'#227'o deseja controlar o item!'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
        end
      end
      object TabSheet11: TTabSheet
        Caption = 'Configura'#231#245'es de boletos'
        ImageIndex = 6
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BitBtn3: TBitBtn
            Tag = 492
            Left = 4
            Top = 4
            Width = 150
            Height = 40
            Cursor = crHandPoint
            Hint = 'Inclui novo registro'
            Caption = '&Configura'#231#227'o boleto'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn3Click
          end
        end
        object dmkDBGrid2: TdmkDBGrid
          Left = 0
          Top = 48
          Width = 1000
          Height = 385
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCNAB_Cfg
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Visible = True
            end>
        end
      end
    end
    object Panel33: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 86
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label19: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label20: TLabel
        Left = 61
        Top = 4
        Width = 60
        Height = 13
        Caption = 'Condom'#237'nio:'
      end
      object Label121: TLabel
        Left = 145
        Top = 44
        Width = 63
        Height = 13
        Caption = 'Tot. andares:'
      end
      object Label125: TLabel
        Left = 218
        Top = 44
        Width = 68
        Height = 13
        Caption = 'Tot. unidades:'
      end
      object Label27: TLabel
        Left = 293
        Top = 44
        Width = 45
        Height = 13
        Caption = 'Telefone:'
      end
      object Label197: TLabel
        Left = 408
        Top = 44
        Width = 61
        Height = 13
        Caption = 'Fra'#231#227'o ideal:'
        FocusControl = DBEdit9
      end
      object Label51: TLabel
        Left = 656
        Top = 3
        Width = 51
        Height = 13
        Caption = 'Assistente:'
      end
      object Label52: TLabel
        Left = 8
        Top = 44
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        FocusControl = DBEdit12
      end
      object Label53: TLabel
        Left = 863
        Top = 44
        Width = 33
        Height = 13
        Caption = 'Status:'
        FocusControl = DBEdAtivo
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 20
        Width = 49
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCond
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 61
        Top = 20
        Width = 592
        Height = 21
        Color = clWhite
        DataField = 'NCONDOM'
        DataSource = DsCond
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit061: TDBEdit
        Left = 145
        Top = 60
        Width = 70
        Height = 21
        DataField = 'Andares'
        DataSource = DsCond
        TabOrder = 2
      end
      object DBEdit062: TDBEdit
        Left = 218
        Top = 60
        Width = 70
        Height = 21
        DataField = 'TotApt'
        DataSource = DsCond
        TabOrder = 3
      end
      object DBEdTel: TDBEdit
        Left = 293
        Top = 60
        Width = 113
        Height = 21
        DataField = 'TELCON_TXT'
        DataSource = DsCond
        TabOrder = 4
      end
      object DBEdit9: TDBEdit
        Left = 408
        Top = 60
        Width = 80
        Height = 21
        DataField = 'SomaFracao'
        DataSource = DsCond
        TabOrder = 5
      end
      object PB1: TProgressBar
        Left = 496
        Top = 60
        Width = 361
        Height = 21
        TabOrder = 6
      end
      object DBEdit11: TDBEdit
        Left = 656
        Top = 20
        Width = 340
        Height = 21
        DataField = 'NO_ASSISTENTE'
        DataSource = DsCond
        TabOrder = 7
      end
      object DBEdit12: TDBEdit
        Left = 8
        Top = 60
        Width = 134
        Height = 21
        DataField = 'Sigla'
        DataSource = DsCond
        TabOrder = 8
      end
      object DBEdAtivo: TDBEdit
        Left = 863
        Top = 60
        Width = 133
        Height = 21
        DataField = 'ATIVO_TXT'
        DataSource = DsCond
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
        OnChange = DBEdAtivoChange
      end
    end
    object PnUmAUm: TPanel
      Left = 0
      Top = 577
      Width = 1008
      Height = 23
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object CkUmAUm: TCheckBox
        Left = 16
        Top = 2
        Width = 157
        Height = 17
        Caption = 'Aborta altera'#231#227'o "um a um".'
        TabOrder = 0
        Visible = False
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 600
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 164
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 338
        Top = 15
        Width = 668
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 559
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCond: TBitBtn
          Tag = 10005
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cond.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCondClick
        end
        object BtBloco: TBitBtn
          Tag = 10003
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Bloco'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtBlocoClick
        end
        object BtImovel: TBitBtn
          Tag = 10021
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Im'#243'vel'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtImovelClick
        end
        object BtBloquetos: TBitBtn
          Tag = 492
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'Blo&queto'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtBloquetosClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 664
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = -6
      Top = 2
      Width = 1014
      Height = 567
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl4: TPageControl
        Left = 0
        Top = 41
        Width = 1014
        Height = 526
        ActivePage = TabSheet25
        Align = alClient
        TabHeight = 25
        TabOrder = 1
        object TabSheet14: TTabSheet
          Caption = 'Geral'
          object Panel48: TPanel
            Left = 0
            Top = 0
            Width = 1006
            Height = 491
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object LaCond: TLabel
              Left = 8
              Top = 2
              Width = 133
              Height = 13
              Caption = 'Entidade deste condom'#237'nio:'
            end
            object SBCond: TSpeedButton
              Left = 652
              Top = 18
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SBCondClick
            end
            object Label1: TLabel
              Left = 8
              Top = 42
              Width = 63
              Height = 13
              Caption = 'Tot. andares:'
              FocusControl = EdTAndares
            end
            object Label2: TLabel
              Left = 81
              Top = 42
              Width = 68
              Height = 13
              Caption = 'Tot. unidades:'
              FocusControl = EdTAptos
            end
            object Label224: TLabel
              Left = 231
              Top = 42
              Width = 51
              Height = 13
              Caption = 'Assistente:'
            end
            object SBAssistente: TSpeedButton
              Left = 652
              Top = 58
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SBAssistenteClick
            end
            object Label199: TLabel
              Left = 153
              Top = 42
              Width = 26
              Height = 13
              Caption = 'Sigla:'
              FocusControl = EdSigla
            end
            object EdEntidade: TdmkEditCB
              Left = 8
              Top = 18
              Width = 57
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEntidade
              IgnoraDBLookupComboBox = False
            end
            object CBEntidade: TdmkDBLookupComboBox
              Left = 65
              Top = 18
              Width = 585
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOME'
              ListSource = DsClientes
              TabOrder = 1
              dmkEditCB = EdEntidade
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object GroupBox37: TGroupBox
              Left = 8
              Top = 85
              Width = 185
              Height = 69
              Caption = ' % INSS s'#237'ndico: '
              TabOrder = 7
              object Label191: TLabel
                Left = 12
                Top = 20
                Width = 51
                Height = 13
                Caption = '% S'#237'ndico:'
              end
              object Label192: TLabel
                Left = 96
                Top = 20
                Width = 71
                Height = 13
                Caption = '% Condom'#237'nio:'
              end
              object dmkEdProLaINSSr: TdmkEdit
                Left = 12
                Top = 36
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object dmkEdProLaINSSp: TdmkEdit
                Left = 96
                Top = 36
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
            object EdTAndares: TdmkEdit
              Left = 8
              Top = 58
              Width = 70
              Height = 21
              Alignment = taRightJustify
              MaxLength = 30
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdTAptos: TdmkEdit
              Left = 81
              Top = 58
              Width = 70
              Height = 21
              Alignment = taRightJustify
              MaxLength = 30
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdSigla: TdmkEdit
              Left = 153
              Top = 58
              Width = 72
              Height = 21
              MaxLength = 50
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdAssistente: TdmkEditCB
              Left = 231
              Top = 58
              Width = 57
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBAssistente
              IgnoraDBLookupComboBox = False
            end
            object CBAssistente: TdmkDBLookupComboBox
              Left = 289
              Top = 58
              Width = 361
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsAssist
              TabOrder = 6
              dmkEditCB = EdAssistente
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object RGAtivo: TdmkRadioGroup
              Left = 199
              Top = 85
              Width = 451
              Height = 69
              Caption = 'Status do condom'#237'nio'
              Columns = 3
              ItemIndex = 2
              Items.Strings = (
                'Inoperante'
                'Inativo'
                'Ativo')
              TabOrder = 8
              UpdType = utYes
              OldValor = 0
            end
          end
        end
        object TabSheet13: TTabSheet
          Caption = 'Boleto (parte 1)'
          ImageIndex = 5
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 1006
            Height = 491
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object LaConfigBol: TLabel
              Left = 8
              Top = 256
              Width = 224
              Height = 13
              Caption = 'Configura'#231#227'o padr'#227'o de impress'#227'o dos boletos:'
            end
            object Label11: TLabel
              Left = 10
              Top = 298
              Width = 273
              Height = 13
              Caption = 'Configura'#231#227'o de corre'#231#227'o monet'#225'ria de boletos vencidos:'
            end
            object SBConfigBol: TSpeedButton
              Left = 754
              Top = 272
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SBConfigBolClick
            end
            object SpeedButton6: TSpeedButton
              Left = 754
              Top = 314
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton6Click
            end
            object RGModelBloq: TRadioGroup
              Left = 8
              Top = 4
              Width = 767
              Height = 200
              Caption = ' Modelo de impress'#227'o do boleto: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'FmPrincipal.PreencheModelosBloq')
              TabOrder = 0
              OnClick = RGModelBloqClick
            end
            object EdConfigBol: TdmkEditCB
              Left = 8
              Top = 272
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBConfigBol
              IgnoraDBLookupComboBox = False
            end
            object CBConfigBol: TdmkDBLookupComboBox
              Left = 64
              Top = 272
              Width = 685
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsConfigBol
              TabOrder = 3
              dmkEditCB = EdConfigBol
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object RGCompe: TRadioGroup
              Left = 8
              Top = 210
              Width = 767
              Height = 37
              Caption = ' Ficha de compensa'#231#227'o (modelo E): '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                '??'
                '1 (uma via)'
                '2 (duas vias)')
              TabOrder = 1
            end
            object CkHideCompe: TCheckBox
              Left = 426
              Top = 345
              Width = 349
              Height = 17
              Caption = 'N'#227'o mostrar a ficha de compensa'#231#227'o no boleto modelo H.'
              Checked = True
              State = cbChecked
              TabOrder = 7
            end
            object CkBalAgrMens: TCheckBox
              Left = 10
              Top = 345
              Width = 405
              Height = 17
              Caption = 
                'Separar as somas de valores no balancete por compet'#234'ncia ao agru' +
                'par valores.'
              Checked = True
              State = cbChecked
              TabOrder = 6
            end
            object EdCfgInfl: TdmkEditCB
              Left = 8
              Top = 314
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCfgInfl
              IgnoraDBLookupComboBox = False
            end
            object CBCfgInfl: TdmkDBLookupComboBox
              Left = 64
              Top = 314
              Width = 685
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCfgInfl
              ParentShowHint = False
              ShowHint = False
              TabOrder = 5
              dmkEditCB = EdCfgInfl
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object Panel11: TPanel
              Left = 792
              Top = 0
              Width = 214
              Height = 491
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 9
              object RGMBB: TRadioGroup
                Left = 0
                Top = 0
                Width = 214
                Height = 34
                Align = alTop
                Caption = ' Mostra balancete no boleto: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 0
              end
              object RGMRB: TRadioGroup
                Left = 0
                Top = 34
                Width = 214
                Height = 34
                Align = alTop
                Caption = ' Mostra resumo do balanc. no boleto: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 1
              end
              object RGPBB: TRadioGroup
                Left = 0
                Top = 68
                Width = 214
                Height = 34
                Align = alTop
                Caption = ' Per'#237'odo do balancete no boleto: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'Anterior'
                  'Atual'
                  'Pr'#243'ximo')
                TabOrder = 2
              end
              object RGMSP: TRadioGroup
                Left = 0
                Top = 102
                Width = 214
                Height = 34
                Align = alTop
                Caption = 'Mostra saldos do plano de ctas no bloq.:  '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim'
                  'S'#243' cjtos')
                TabOrder = 3
              end
              object RGMSB: TRadioGroup
                Left = 0
                Top = 136
                Width = 214
                Height = 34
                Align = alTop
                Caption = ' Mostra saldos de ctas corr. no boleto: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim'
                  'Fim')
                TabOrder = 4
              end
              object RGPSB: TRadioGroup
                Left = 0
                Top = 170
                Width = 214
                Height = 34
                Align = alTop
                Caption = ' Per'#237'odo dos saldos no boleto: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Anterior'
                  'Atual')
                TabOrder = 5
              end
              object RGMIB: TRadioGroup
                Left = 0
                Top = 204
                Width = 214
                Height = 51
                Align = alTop
                Caption = ' Mostra inadimpl'#234'ncia no boleto: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Ambos'
                  'Cond'#244'mino'
                  'Condom'#237'nio')
                TabOrder = 6
              end
              object RGMPB: TRadioGroup
                Left = 0
                Top = 255
                Width = 214
                Height = 34
                Align = alTop
                Caption = ' Mostra provis'#245'es para despesas bloq.: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 7
              end
              object RGMAB: TRadioGroup
                Left = 0
                Top = 289
                Width = 214
                Height = 34
                Align = alTop
                Caption = ' Mostra a composi'#231#227'o de arrecad. bloq.: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 8
              end
              object RGDiaVencto: TRadioGroup
                Left = 0
                Top = 323
                Width = 214
                Height = 165
                Align = alTop
                Caption = ' Dia do m'#234's do vencto. da quota condom.: '
                Columns = 4
                ItemIndex = 4
                Items.Strings = (
                  '01'
                  '02'
                  '03'
                  '04'
                  '05'
                  '06'
                  '07'
                  '08'
                  '09'
                  '10'
                  '11'
                  '12'
                  '13'
                  '14'
                  '15'
                  '16'
                  '17'
                  '18'
                  '19'
                  '20'
                  '21'
                  '22'
                  '23'
                  '24'
                  '25'
                  '26'
                  '27'
                  '28')
                TabOrder = 9
              end
            end
            object CkUsaCNAB_Cfg: TCheckBox
              Left = 10
              Top = 368
              Width = 405
              Height = 17
              Caption = 'Usa configura'#231#227'o de boleto'
              TabOrder = 8
              OnClick = CkUsaCNAB_CfgClick
            end
          end
        end
        object TabSheet24: TTabSheet
          Caption = 'Boleto (parte 2)'
          ImageIndex = 2
          object Panel31: TPanel
            Left = 0
            Top = 0
            Width = 1006
            Height = 491
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel34: TPanel
              Left = 0
              Top = 0
              Width = 1006
              Height = 491
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label49: TLabel
                Left = 8
                Top = 7
                Width = 34
                Height = 13
                Caption = 'Banco:'
              end
              object Label54: TLabel
                Left = 48
                Top = 7
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
              end
              object Label163: TLabel
                Left = 92
                Top = 7
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object Label134: TLabel
                Left = 116
                Top = 7
                Width = 30
                Height = 13
                Caption = 'Posto:'
              end
              object Label56: TLabel
                Left = 156
                Top = 7
                Width = 31
                Height = 13
                Caption = 'Conta:'
              end
              object Label164: TLabel
                Left = 244
                Top = 7
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object Label162: TLabel
                Left = 268
                Top = 7
                Width = 67
                Height = 13
                Caption = 'C'#243'd. cedente:'
              end
              object Label57: TLabel
                Left = 360
                Top = 7
                Width = 120
                Height = 13
                Caption = 'Ag./C'#243'd. cedente boleto:'
              end
              object Label148: TLabel
                Left = 8
                Top = 51
                Width = 62
                Height = 13
                Caption = 'Esp'#233'cie doc:'
              end
              object Label151: TLabel
                Left = 80
                Top = 51
                Width = 39
                Height = 13
                Caption = 'Carteira:'
              end
              object Label152: TLabel
                Left = 196
                Top = 51
                Width = 61
                Height = 13
                Caption = 'Esp'#233'cie val.:'
              end
              object Label185: TLabel
                Left = 300
                Top = 51
                Width = 50
                Height = 13
                Caption = 'Oper. c'#243'd.'
              end
              object Label160: TLabel
                Left = 356
                Top = 52
                Width = 40
                Height = 13
                Caption = '% Multa:'
              end
              object Label161: TLabel
                Left = 424
                Top = 52
                Width = 39
                Height = 13
                Caption = '% Juros:'
              end
              object Label179: TLabel
                Left = 8
                Top = 92
                Width = 43
                Height = 13
                Caption = 'Cedente:'
              end
              object Label180: TLabel
                Left = 344
                Top = 92
                Width = 95
                Height = 13
                Caption = 'Sacador / avalistas:'
              end
              object Label182: TLabel
                Left = 676
                Top = 91
                Width = 77
                Height = 13
                Caption = 'Ident. tipo cobr.:'
              end
              object Label149: TLabel
                Left = 7
                Top = 132
                Width = 291
                Height = 13
                Caption = 'Logo do banco (174 largura  X 30 altura no formato bmp): [F4]'
              end
              object Label150: TLabel
                Left = 308
                Top = 132
                Width = 292
                Height = 13
                Caption = 'Logo do cliente (160 largura  X 90 altura no formato bmp): [F4]'
              end
              object LaCartEmiss: TLabel
                Left = 8
                Top = 172
                Width = 156
                Height = 13
                Caption = 'Carteira para emiss'#227'o de boletos:'
              end
              object Label189: TLabel
                Left = 8
                Top = 337
                Width = 588
                Height = 13
                Caption = 
                  'VTCBBNITAR '#185': Valor da tarifa de cobran'#231'a banc'#225'ria para bancos q' +
                  'ue n'#227'o informam a tarifa no arquivo retorno (banco 756).'
              end
              object Label187: TLabel
                Left = 492
                Top = 52
                Width = 77
                Height = 13
                Caption = 'VTCBBNITAR '#185':'
              end
              object Label3: TLabel
                Left = 496
                Top = 7
                Width = 100
                Height = 13
                Caption = 'Local de pagamento:'
              end
              object Label6: TLabel
                Left = 136
                Top = 51
                Width = 43
                Height = 13
                Caption = 'Cart blq*:'
              end
              object Label7: TLabel
                Left = 8
                Top = 357
                Width = 338
                Height = 13
                Caption = 
                  'Cart blq*: Texto Alternativo para impress'#227'o do campo carteira no' +
                  ' boleto.'
              end
              object Label10: TLabel
                Left = 8
                Top = 315
                Width = 208
                Height = 13
                Caption = 'Conta Corrente Cooperado (Nosso N'#250'mero):'
              end
              object EdCondBanco: TdmkEdit
                Left = 8
                Top = 24
                Width = 36
                Height = 21
                Alignment = taRightJustify
                MaxLength = 30
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdCondAgen: TdmkEdit
                Left = 48
                Top = 24
                Width = 44
                Height = 21
                Alignment = taRightJustify
                MaxLength = 30
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdDVAgencia: TdmkEdit
                Left = 92
                Top = 24
                Width = 21
                Height = 21
                Alignment = taRightJustify
                MaxLength = 30
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCondPosto: TdmkEdit
                Left = 116
                Top = 24
                Width = 36
                Height = 21
                Alignment = taRightJustify
                MaxLength = 30
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdCondConta: TdmkEdit
                Left = 156
                Top = 24
                Width = 88
                Height = 21
                Alignment = taRightJustify
                MaxLength = 30
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDVConta: TdmkEdit
                Left = 244
                Top = 24
                Width = 21
                Height = 21
                Alignment = taRightJustify
                MaxLength = 30
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCondCodCed: TdmkEdit
                Left = 268
                Top = 24
                Width = 88
                Height = 21
                MaxLength = 30
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdAgContaCed: TdmkEdit
                Left = 360
                Top = 24
                Width = 132
                Height = 21
                Alignment = taRightJustify
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCondLocalPg: TdmkEdit
                Left = 496
                Top = 24
                Width = 273
                Height = 21
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCondEspDoc: TdmkEdit
                Left = 8
                Top = 68
                Width = 69
                Height = 21
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCondCarteira: TdmkEdit
                Left = 80
                Top = 68
                Width = 53
                Height = 21
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCondEspVal: TdmkEdit
                Left = 196
                Top = 68
                Width = 100
                Height = 21
                TabOrder = 12
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdOperCodi: TdmkEdit
                Left = 300
                Top = 68
                Width = 53
                Height = 21
                TabOrder = 13
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdPercMulta: TdmkEdit
                Left = 356
                Top = 68
                Width = 64
                Height = 21
                Alignment = taRightJustify
                TabOrder = 14
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdPercJuros: TdmkEdit
                Left = 424
                Top = 68
                Width = 64
                Height = 21
                Alignment = taRightJustify
                TabOrder = 15
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object dmkEdVTCBBNITAR: TdmkEdit
                Left = 492
                Top = 68
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 16
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdCedente: TdmkEditCB
                Left = 8
                Top = 108
                Width = 37
                Height = 21
                Alignment = taRightJustify
                TabOrder = 18
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBCedente
                IgnoraDBLookupComboBox = False
              end
              object CBCedente: TdmkDBLookupComboBox
                Left = 48
                Top = 108
                Width = 290
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOMEENT'
                ListSource = DsCedente
                TabOrder = 19
                dmkEditCB = EdCedente
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdSacadAvali: TdmkEditCB
                Left = 344
                Top = 108
                Width = 37
                Height = 21
                Alignment = taRightJustify
                TabOrder = 20
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBSacadAvali
                IgnoraDBLookupComboBox = False
              end
              object CBSacadAvali: TdmkDBLookupComboBox
                Left = 384
                Top = 108
                Width = 290
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOMEENT'
                ListSource = DsSacador
                TabOrder = 21
                dmkEditCB = EdSacadAvali
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdIDCobranca: TdmkEdit
                Left = 676
                Top = 108
                Width = 93
                Height = 21
                TabOrder = 22
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCondBcoLogo: TdmkEdit
                Left = 7
                Top = 148
                Width = 296
                Height = 21
                TabOrder = 23
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdCondBcoLogoKeyDown
              end
              object EdCondCliLogo: TdmkEdit
                Left = 308
                Top = 148
                Width = 461
                Height = 21
                TabOrder = 24
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdCondCliLogoKeyDown
              end
              object EdCartEmiss: TdmkEditCB
                Left = 8
                Top = 189
                Width = 37
                Height = 21
                Alignment = taRightJustify
                TabOrder = 25
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBCartEmiss
                IgnoraDBLookupComboBox = False
              end
              object CBCartEmiss: TdmkDBLookupComboBox
                Left = 48
                Top = 189
                Width = 285
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCartEmiss
                TabOrder = 26
                dmkEditCB = EdCartEmiss
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object Panel35: TPanel
                Left = 791
                Top = 0
                Width = 215
                Height = 491
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 29
                object Panel49: TPanel
                  Left = 0
                  Top = 0
                  Width = 215
                  Height = 41
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object RGAceita: TRadioGroup
                    Left = 0
                    Top = 0
                    Width = 106
                    Height = 41
                    Align = alLeft
                    Caption = ' Aceita: '
                    Columns = 2
                    ItemIndex = 0
                    Items.Strings = (
                      'N'#227'o'
                      'Sim')
                    TabOrder = 0
                  end
                  object RGCorreio: TRadioGroup
                    Left = 106
                    Top = 0
                    Width = 109
                    Height = 41
                    Align = alClient
                    Caption = ' Correio: '
                    Columns = 2
                    ItemIndex = 0
                    Items.Strings = (
                      'N'#227'o'
                      'Sim')
                    TabOrder = 1
                  end
                end
                object RGModalCobr: TRadioGroup
                  Left = 0
                  Top = 41
                  Width = 215
                  Height = 57
                  Align = alTop
                  Caption = 'Modalidade de cobran'#231'a: '
                  ItemIndex = 0
                  Items.Strings = (
                    'Sem registro no banco'
                    'Com registro no banco')
                  TabOrder = 1
                end
              end
              object RGTipoCobranca: TRadioGroup
                Left = 8
                Top = 214
                Width = 469
                Height = 89
                Caption = ' Tipo de cobran'#231'a: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'TUnMyObjects.ConfiguraTipoCobranca')
                TabOrder = 27
              end
              object EdCartTxt: TdmkEdit
                Left = 136
                Top = 68
                Width = 57
                Height = 21
                CharCase = ecUpperCase
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object RGCNAB: TRadioGroup
                Left = 580
                Top = 48
                Width = 188
                Height = 41
                Caption = ' CNAB (Remessa / Retorno):  '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  '000'
                  '240'
                  '400')
                TabOrder = 17
              end
              object EdCtaCooper: TdmkEdit
                Left = 220
                Top = 312
                Width = 93
                Height = 21
                TabOrder = 28
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
        end
        object TabSheet25: TTabSheet
          Caption = 'Boleto (parte 3)'
          ImageIndex = 3
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 1006
            Height = 491
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label200: TLabel
              Left = 684
              Top = 4
              Width = 135
              Height = 13
              Caption = 'Vari'#225'veis para as instru'#231#245'es:'
            end
            object GroupBox32: TGroupBox
              Left = 8
              Top = 1
              Width = 669
              Height = 184
              Caption = ' Instru'#231#245'es (Texto de responsabilidade do cedente): '
              TabOrder = 0
              object EdCondInst1: TdmkEdit
                Left = 8
                Top = 17
                Width = 650
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnEnter = EdCondInst1Enter
                OnExit = EdCondInst1Exit
              end
              object EdCondInst2: TdmkEdit
                Left = 8
                Top = 37
                Width = 650
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnEnter = EdCondInst1Enter
                OnExit = EdCondInst1Exit
              end
              object EdCondInst3: TdmkEdit
                Left = 8
                Top = 57
                Width = 650
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnEnter = EdCondInst1Enter
                OnExit = EdCondInst1Exit
              end
              object EdCondInst4: TdmkEdit
                Left = 8
                Top = 77
                Width = 650
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnEnter = EdCondInst1Enter
                OnExit = EdCondInst1Exit
              end
              object EdCondInst5: TdmkEdit
                Left = 8
                Top = 97
                Width = 650
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnEnter = EdCondInst1Enter
                OnExit = EdCondInst1Exit
              end
              object EdCondInst6: TdmkEdit
                Left = 8
                Top = 117
                Width = 650
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnEnter = EdCondInst1Enter
                OnExit = EdCondInst1Exit
              end
              object EdCondInst7: TdmkEdit
                Left = 8
                Top = 136
                Width = 650
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnEnter = EdCondInst1Enter
                OnExit = EdCondInst1Exit
              end
              object EdCondInst8: TdmkEdit
                Left = 8
                Top = 156
                Width = 650
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnEnter = EdCondInst1Enter
                OnExit = EdCondInst1Exit
              end
            end
            object LbVars: TListBox
              Left = 679
              Top = 21
              Width = 325
              Height = 401
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ItemHeight = 14
              Items.Strings = (
                '[MULTA_ATRASO] Valor da multa por atraso.'
                '[JUROS_ATRASO] Valor de juros ao dia.')
              ParentFont = False
              TabOrder = 1
              OnDblClick = LbVarsDblClick
            end
          end
        end
        object TabSheet15: TTabSheet
          Caption = 'Observa'#231#245'es'
          ImageIndex = 1
          object MeObserv: TMemo
            Left = 0
            Top = 0
            Width = 1006
            Height = 491
            Align = alClient
            Lines.Strings = (
              'Memo2')
            TabOrder = 0
          end
        end
      end
      object Panel16: TPanel
        Left = 0
        Top = 0
        Width = 1014
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 8
          Top = 0
          Width = 60
          Height = 13
          Caption = 'Condom'#237'nio:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 16
          Width = 49
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object DBEdit10: TDBEdit
          Left = 57
          Top = 16
          Width = 604
          Height = 21
          DataField = 'NCONDOM'
          DataSource = DsCond
          TabOrder = 1
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 601
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel9: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 302
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtEntidades: TBitBtn
        Tag = 132
        Left = 215
        Top = 8
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtEntidadesClick
      end
      object BtWClients: TBitBtn
        Tag = 10003
        Left = 258
        Top = 8
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = BtWClientsClick
      end
    end
    object GB_M: TGroupBox
      Left = 302
      Top = 0
      Width = 353
      Height = 52
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 315
        Height = 32
        Caption = 'Cadastro de Condom'#237'nios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 315
        Height = 32
        Caption = 'Cadastro de Condom'#237'nios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 315
        Height = 32
        Caption = 'Cadastro de Condom'#237'nios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 655
      Top = 0
      Width = 305
      Height = 52
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 301
        Height = 35
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object DsCond: TDataSource
    DataSet = QrCond
    Left = 600
    Top = 12
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCondBeforeOpen
    AfterOpen = QrCondAfterOpen
    BeforeClose = QrCondBeforeClose
    AfterScroll = QrCondAfterScroll
    OnCalcFields = QrCondCalcFields
    SQL.Strings = (
      'SELECT IF(con.Ativo=0, "N'#195'O", "SIM") ATIVO_TXT, con.*,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NCONDOM,'
      'IF (ent.Tipo=0, ent.ETe1, ent.PTe1) TELCOM,'
      'cfg.Nome NO_CNAB_CFG, ass.Nome NO_ASSISTENTE,'
      'cat.Nome NOMECARTCONCIL, car.Nome NOMECARTEMISS'
      'FROM cond con'
      'LEFT JOIN entidades ent ON ent.Codigo = con.Cliente'
      'LEFT JOIN entidades ass ON ass.Codigo = con.Assistente'
      'LEFT JOIN carteiras car ON car.Codigo = con.CartEmiss'
      'LEFT JOIN carteiras cat ON cat.Codigo = con.CartConcil'
      'LEFT JOIN cnab_cfg  cfg ON cfg.Codigo = con.CNAB_Cfg'
      'WHERE con.Codigo > 0'
      '')
    Left = 572
    Top = 12
    object QrCondNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Size = 100
    end
    object QrCondCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cond.Codigo'
    end
    object QrCondCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'cond.Cliente'
    end
    object QrCondTxtParecer: TWideMemoField
      FieldName = 'TxtParecer'
      Origin = 'cond.TxtParecer'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCondLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cond.Lk'
    end
    object QrCondDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cond.DataCad'
    end
    object QrCondDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cond.DataAlt'
    end
    object QrCondUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cond.UserCad'
    end
    object QrCondUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cond.UserAlt'
    end
    object QrCondAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cond.AlterWeb'
    end
    object QrCondAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cond.Ativo'
    end
    object QrCondTELCOM: TWideStringField
      FieldName = 'TELCOM'
    end
    object QrCondTELCON_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TELCON_TXT'
      Calculated = True
    end
    object QrCondDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
    object QrCondSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 10
    end
    object QrCondObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCondAndares: TIntegerField
      FieldName = 'Andares'
    end
    object QrCondTotApt: TIntegerField
      FieldName = 'TotApt'
    end
    object QrCondBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCondAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrCondPosto: TIntegerField
      FieldName = 'Posto'
    end
    object QrCondConta: TWideStringField
      FieldName = 'Conta'
      Size = 10
    end
    object QrCondCodCedente: TWideStringField
      FieldName = 'CodCedente'
    end
    object QrCondBcoLogoPath: TWideStringField
      FieldName = 'BcoLogoPath'
      Size = 255
    end
    object QrCondCliLogoPath: TWideStringField
      FieldName = 'CliLogoPath'
      Size = 255
    end
    object QrCondLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Size = 255
    end
    object QrCondEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
      Size = 30
    end
    object QrCondAceite: TWideStringField
      FieldName = 'Aceite'
      Size = 1
    end
    object QrCondCarteira: TWideStringField
      FieldName = 'Carteira'
      Size = 30
    end
    object QrCondEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Size = 10
    end
    object QrCondInstrucao1: TWideStringField
      FieldName = 'Instrucao1'
      Size = 255
    end
    object QrCondInstrucao2: TWideStringField
      FieldName = 'Instrucao2'
      Size = 255
    end
    object QrCondInstrucao3: TWideStringField
      FieldName = 'Instrucao3'
      Size = 255
    end
    object QrCondInstrucao4: TWideStringField
      FieldName = 'Instrucao4'
      Size = 255
    end
    object QrCondInstrucao5: TWideStringField
      FieldName = 'Instrucao5'
      Size = 255
    end
    object QrCondInstrucao6: TWideStringField
      FieldName = 'Instrucao6'
      Size = 255
    end
    object QrCondInstrucao7: TWideStringField
      FieldName = 'Instrucao7'
      Size = 255
    end
    object QrCondInstrucao8: TWideStringField
      FieldName = 'Instrucao8'
      Size = 255
    end
    object QrCondCorreio: TWideStringField
      FieldName = 'Correio'
      Size = 1
    end
    object QrCondCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrCondPercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrCondPercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrCondDVAgencia: TWideStringField
      FieldName = 'DVAgencia'
      Size = 1
    end
    object QrCondDVConta: TWideStringField
      FieldName = 'DVConta'
      Size = 1
    end
    object QrCondAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Size = 40
    end
    object QrCondModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrCondCedente: TIntegerField
      FieldName = 'Cedente'
    end
    object QrCondSacadAvali: TIntegerField
      FieldName = 'SacadAvali'
    end
    object QrCondMIB: TSmallintField
      FieldName = 'MIB'
    end
    object QrCondIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Size = 16
    end
    object QrCondOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Size = 10
    end
    object QrCondMBB: TSmallintField
      FieldName = 'MBB'
    end
    object QrCondMRB: TSmallintField
      FieldName = 'MRB'
    end
    object QrCondPBB: TSmallintField
      FieldName = 'PBB'
    end
    object QrCondMSB: TSmallintField
      FieldName = 'MSB'
    end
    object QrCondPSB: TSmallintField
      FieldName = 'PSB'
    end
    object QrCondMPB: TSmallintField
      FieldName = 'MPB'
    end
    object QrCondMAB: TSmallintField
      FieldName = 'MAB'
    end
    object QrCondModalCobr: TSmallintField
      FieldName = 'ModalCobr'
    end
    object QrCondVTCBBNITAR: TFloatField
      FieldName = 'VTCBBNITAR'
    end
    object QrCondPwdSite: TSmallintField
      FieldName = 'PwdSite'
    end
    object QrCondProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
    end
    object QrCondProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
    end
    object QrCondConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrCondOcultaBloq: TSmallintField
      FieldName = 'OcultaBloq'
    end
    object QrCondSomaFracao: TFloatField
      FieldName = 'SomaFracao'
    end
    object QrCondMSP: TSmallintField
      FieldName = 'MSP'
    end
    object QrCondBalAgrMens: TSmallintField
      FieldName = 'BalAgrMens'
    end
    object QrCondCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrCondHideCompe: TSmallintField
      FieldName = 'HideCompe'
    end
    object QrCondFlxB_Ordem: TIntegerField
      FieldName = 'FlxB_Ordem'
    end
    object QrCondFlxB_Folha: TSmallintField
      FieldName = 'FlxB_Folha'
    end
    object QrCondFlxB_LeiAr: TSmallintField
      FieldName = 'FlxB_LeiAr'
    end
    object QrCondFlxB_Fecha: TSmallintField
      FieldName = 'FlxB_Fecha'
    end
    object QrCondFlxB_Risco: TSmallintField
      FieldName = 'FlxB_Risco'
    end
    object QrCondFlxB_Print: TSmallintField
      FieldName = 'FlxB_Print'
    end
    object QrCondFlxB_Proto: TSmallintField
      FieldName = 'FlxB_Proto'
    end
    object QrCondFlxB_Relat: TSmallintField
      FieldName = 'FlxB_Relat'
    end
    object QrCondFlxB_EMail: TSmallintField
      FieldName = 'FlxB_EMail'
    end
    object QrCondFlxB_Porta: TSmallintField
      FieldName = 'FlxB_Porta'
    end
    object QrCondFlxB_Postl: TSmallintField
      FieldName = 'FlxB_Postl'
    end
    object QrCondFlxM_Ordem: TIntegerField
      FieldName = 'FlxM_Ordem'
    end
    object QrCondFlxM_Conci: TSmallintField
      FieldName = 'FlxM_Conci'
    end
    object QrCondFlxM_Docum: TSmallintField
      FieldName = 'FlxM_Docum'
    end
    object QrCondFlxM_Anali: TSmallintField
      FieldName = 'FlxM_Anali'
    end
    object QrCondFlxM_Contb: TSmallintField
      FieldName = 'FlxM_Contb'
    end
    object QrCondFlxM_Encad: TSmallintField
      FieldName = 'FlxM_Encad'
    end
    object QrCondFlxM_Entrg: TSmallintField
      FieldName = 'FlxM_Entrg'
    end
    object QrCondAssistente: TIntegerField
      FieldName = 'Assistente'
    end
    object QrCondNO_ASSISTENTE: TWideStringField
      FieldName = 'NO_ASSISTENTE'
      Size = 100
    end
    object QrCondATIVO_TXT: TWideStringField
      DisplayWidth = 10
      FieldName = 'ATIVO_TXT'
      Required = True
      Size = 10
    end
    object QrCondNOMECARTEMISS: TWideStringField
      FieldName = 'NOMECARTEMISS'
      Size = 100
    end
    object QrCondTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrCondCartTxt: TWideStringField
      FieldName = 'CartTxt'
      Size = 10
    end
    object QrCondCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrCondCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCondFlxM_Web: TSmallintField
      FieldName = 'FlxM_Web'
    end
    object QrCondCfgInfl: TIntegerField
      FieldName = 'CfgInfl'
    end
    object QrCondNO_CfgInfl: TWideStringField
      FieldName = 'NO_CfgInfl'
      Size = 100
    end
    object QrCondUsaCNAB_Cfg: TIntegerField
      FieldName = 'UsaCNAB_Cfg'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanDel01 = BtBloquetos
    Left = 628
    Top = 12
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 388
    Top = 404
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM entidades ent'
      'WHERE Cliente1 = "V"'
      'ORDER BY Nome')
    Left = 360
    Top = 404
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object PMCond: TPopupMenu
    OnPopup = PMCondPopup
    Left = 276
    Top = 404
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Conselho1: TMenuItem
      Caption = '&Conselho'
      object Membrosdoconselho1: TMenuItem
        Caption = '&Membros do conselho'
        OnClick = Membrosdoconselho1Click
      end
      object ParecerdoconselhoTexto1: TMenuItem
        Caption = '&Parecer do conselho (Texto)'
        OnClick = ParecerdoconselhoTexto1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Fluxos1: TMenuItem
      Caption = '&Fluxos'
      OnClick = Fluxos1Click
    end
    object Emails1: TMenuItem
      Caption = 'E-&mails'
      object odos1: TMenuItem
        Caption = '&Todos'
        object Executarprogramapadro1: TMenuItem
          Caption = '&Executar programa padr'#227'o'
          OnClick = Executarprogramapadro1Click
        end
        object Listaremails1: TMenuItem
          Caption = '&Listar e-mails'
          OnClick = Listaremails1Click
        end
      end
      object Conselho2: TMenuItem
        Caption = '&Conselho'
        object Executarprogramapadro2: TMenuItem
          Caption = '&Executar programa padr'#227'o'
          OnClick = Executarprogramapadro2Click
        end
        object Listaremails2: TMenuItem
          Caption = '&Listar e-mails'
          OnClick = Listaremails2Click
        end
      end
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object ImportadeExcel1: TMenuItem
      Caption = 'Importa de Excel'
      OnClick = ImportadeExcel1Click
    end
  end
  object DsCondOutros: TDataSource
    DataSet = QrCondOutros
    Left = 684
    Top = 12
  end
  object DsCondBloco: TDataSource
    DataSet = QrCondBloco
    Left = 740
    Top = 12
  end
  object PMBloco: TPopupMenu
    OnPopup = PMBlocoPopup
    Left = 304
    Top = 404
    object Inclui3: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui3Click
    end
    object Altera3: TMenuItem
      Caption = '&Altera'
      OnClick = Altera3Click
    end
    object Exclui3: TMenuItem
      Caption = '&Exclui'
      Enabled = False
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 796
    Top = 12
  end
  object PMImovel: TPopupMenu
    OnPopup = PMImovelPopup
    Left = 332
    Top = 404
    object Inclui4: TMenuItem
      Caption = '&Inclui'
      object Multiplos1: TMenuItem
        Caption = '&R'#225'pido'
        OnClick = Multiplos1Click
      end
      object Unico1: TMenuItem
        Caption = '&Detalhado'
        OnClick = Unico1Click
      end
    end
    object Altera4: TMenuItem
      Caption = '&Altera im'#243'vel atual'
      OnClick = Altera4Click
    end
    object Alteraselecionados1: TMenuItem
      Caption = 'Altera &selecionados'
      object Protocolodeentregadebloqueto1: TMenuItem
        Caption = '&Protocolo de entrega de boleto'
        object Protocolo11: TMenuItem
          Caption = 'Protocolo &1'
          OnClick = Protocolo11Click
        end
        object Protocolo21: TMenuItem
          Caption = 'Protocolo &2'
          OnClick = Protocolo21Click
        end
        object Protocolo31: TMenuItem
          Caption = 'Protocolo &3'
          OnClick = Protocolo31Click
        end
      end
      object FracaoIdeal1: TMenuItem
        Caption = '&Fracao Ideal'
        OnClick = FracaoIdeal1Click
      end
      object Diasparaenviode1: TMenuItem
        Caption = '&Dias para envio de...'
        Enabled = False
        Visible = False
        object SMS1: TMenuItem
          Caption = '&SMS'
          OnClick = SMS1Click
        end
        object Email1: TMenuItem
          Caption = '&E-mail'
          OnClick = Email1Click
        end
        object Carta1: TMenuItem
          Caption = '&Carta'
          OnClick = Carta1Click
        end
      end
      object ConfiguraodeEnviodeMensagens1: TMenuItem
        Caption = 'Configura'#231#227'o de Envio de Mensagens'
        OnClick = ConfiguraodeEnviodeMensagens1Click
      end
    end
    object Alteraumaum1: TMenuItem
      Caption = 'Altera &um a um'
      Visible = False
      OnClick = Alteraumaum1Click
    end
    object Exclui4: TMenuItem
      Caption = '&Exclui im'#243'vel atual'
      Enabled = False
      OnClick = Exclui4Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Adicionarzerosesquerda1: TMenuItem
      Caption = 'Adicionar &zeros '#224' esquerda'
      OnClick = Adicionarzerosesquerda1Click
    end
  end
  object QrCartEmiss: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Tipo=2'
      'AND ForneceI=:P0'
      'ORDER BY Nome'
      '')
    Left = 900
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCartEmissCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartEmissNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCartEmiss: TDataSource
    DataSet = QrCartEmiss
    Left = 928
    Top = 16
  end
  object QrCondOutros: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCondOutrosCalcFields
    Left = 656
    Top = 12
    object QrCondOutrosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCondOutrosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCondOutrosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrCondOutrosCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrCondOutrosAssina: TSmallintField
      FieldName = 'Assina'
      Required = True
    end
    object QrCondOutrosOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Required = True
    end
    object QrCondOutrosObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrCondOutrosNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrCondOutrosNO_ASSINA: TWideStringField
      FieldName = 'NO_ASSINA'
      Size = 3
    end
    object QrCondOutrosMandatoIni: TDateField
      FieldName = 'MandatoIni'
    end
    object QrCondOutrosMandatoFim: TDateField
      FieldName = 'MandatoFim'
    end
    object QrCondOutrosMandatoIni_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoIni_TXT'
      Size = 10
      Calculated = True
    end
    object QrCondOutrosMandatoFim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoFim_TXT'
      Size = 10
      Calculated = True
    end
    object QrCondOutrosTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrCondOutrosCel: TWideStringField
      FieldName = 'Cel'
    end
    object QrCondOutrosEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrCondOutrosEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrCondOutrosTe1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 30
      Calculated = True
    end
    object QrCondOutrosCel_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Cel_TXT'
      Size = 30
      Calculated = True
    end
  end
  object QrCondBloco: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCondBlocoAfterOpen
    BeforeClose = QrCondBlocoBeforeClose
    AfterScroll = QrCondBlocoAfterScroll
    SQL.Strings = (
      'SELECT * FROM condbloco'
      'WHERE Codigo =:P0'
      'ORDER BY Ordem, Descri')
    Left = 712
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondBlocoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'condbloco.Codigo'
    end
    object QrCondBlocoControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'condbloco.Controle'
    end
    object QrCondBlocoDescri: TWideStringField
      FieldName = 'Descri'
      Origin = 'condbloco.Descri'
      Size = 100
    end
    object QrCondBlocoOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'condbloco.Ordem'
    end
    object QrCondBlocoSomaFracao: TFloatField
      FieldName = 'SomaFracao'
      Origin = 'condbloco.SomaFracao'
      DisplayFormat = '0.000000'
    end
    object QrCondBlocoPrefixoUH: TWideStringField
      FieldName = 'PrefixoUH'
      Origin = 'condbloco.PrefixoUH'
    end
    object QrCondBlocoIDExporta: TWideStringField
      FieldName = 'IDExporta'
      Origin = 'condbloco.IDExporta'
    end
  end
  object QrCondImov: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCondImovBeforeClose
    AfterScroll = QrCondImovAfterScroll
    OnCalcFields = QrCondImovCalcFields
    SQL.Strings = (
      'SELECT con.Nome NOMECONJUGE, sta.Descri STATUS1, '
      'IF (pro.Tipo=0, pro.RazaoSocial, pro.Nome) NOMEPROP,'
      'IF (mor.Tipo=0, mor.RazaoSocial, mor.Nome) NOMEUSUARIO,'
      'IF (pcu.Tipo=0, pcu.RazaoSocial, pcu.Nome) NOMEPROCURADOR,'
      'IF (ena.Tipo=0, ena.RazaoSocial, ena.Nome) NOMEEMP1,'
      'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP2,'
      'IF (enc.Tipo=0, enc.RazaoSocial, enc.Nome) NOMEEMP3,'
      'end.RazaoSocial IMOB, ptc.Nome NOMEPROTOCOLO, '
      'pt2.Nome NOMEPROTOCOLO2, pt3.Nome NOMEPROTOCOLO3,'
      'imc.*'
      'FROM condimov imc'
      'LEFT JOIN entidades pro ON pro.Codigo=imc.Propriet'
      'LEFT JOIN entidades mor ON mor.Codigo=imc.Usuario'
      'LEFT JOIN entidades pcu ON pcu.Codigo=imc.Procurador'
      'LEFT JOIN entidades con ON con.Codigo=imc.Conjuge'
      'LEFT JOIN entidades ena ON ena.Codigo=imc.ENome1'
      'LEFT JOIN entidades enb ON enb.Codigo=imc.ENome2'
      'LEFT JOIN entidades enc ON enc.Codigo=imc.ENome3'
      'LEFT JOIN status sta ON sta.Codigo=imc.Status'
      'LEFT JOIN entidades end ON end.Codigo=imc.Imobiliaria'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=imc.Protocolo'
      'LEFT JOIN protocolos pt2 ON pt2.Codigo=imc.Protocolo2'
      'LEFT JOIN protocolos pt3 ON pt3.Codigo=imc.Protocolo3'
      'WHERE imc.Controle =:P0'
      'ORDER BY Andar, Unidade')
    Left = 768
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondImovNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrCondImovNOMECONJUGE: TWideStringField
      FieldName = 'NOMECONJUGE'
      Origin = 'entidades.Nome'
      Size = 100
    end
    object QrCondImovSTATUS1: TWideStringField
      FieldName = 'STATUS1'
      Origin = 'status.Descri'
      Size = 100
    end
    object QrCondImovNOMEEMP1: TWideStringField
      FieldName = 'NOMEEMP1'
      Size = 100
    end
    object QrCondImovNOMEEMP2: TWideStringField
      FieldName = 'NOMEEMP2'
      Size = 100
    end
    object QrCondImovNOMEEMP3: TWideStringField
      FieldName = 'NOMEEMP3'
      Size = 100
    end
    object QrCondImovIMOB: TWideStringField
      FieldName = 'IMOB'
      Origin = 'entidades.RazaoSocial'
      Size = 100
    end
    object QrCondImovCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'condimov.Codigo'
      Required = True
    end
    object QrCondImovControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'condimov.Controle'
      Required = True
    end
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'condimov.Conta'
      Required = True
    end
    object QrCondImovPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'condimov.Propriet'
      Required = True
    end
    object QrCondImovConjuge: TIntegerField
      FieldName = 'Conjuge'
      Origin = 'condimov.Conjuge'
      Required = True
    end
    object QrCondImovAndar: TIntegerField
      FieldName = 'Andar'
      Origin = 'condimov.Andar'
      Required = True
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Required = True
      Size = 10
    end
    object QrCondImovQtdGaragem: TIntegerField
      FieldName = 'QtdGaragem'
      Origin = 'condimov.QtdGaragem'
      Required = True
    end
    object QrCondImovStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'condimov.Status'
      Required = True
    end
    object QrCondImovENome1: TIntegerField
      FieldName = 'ENome1'
      Origin = 'condimov.ENome1'
      Required = True
    end
    object QrCondImovESegunda1: TSmallintField
      FieldName = 'ESegunda1'
      Origin = 'condimov.ESegunda1'
    end
    object QrCondImovETerca1: TSmallintField
      FieldName = 'ETerca1'
      Origin = 'condimov.ETerca1'
    end
    object QrCondImovEQuarta1: TSmallintField
      FieldName = 'EQuarta1'
      Origin = 'condimov.EQuarta1'
    end
    object QrCondImovEQuinta1: TSmallintField
      FieldName = 'EQuinta1'
      Origin = 'condimov.EQuinta1'
    end
    object QrCondImovESexta1: TSmallintField
      FieldName = 'ESexta1'
      Origin = 'condimov.ESexta1'
    end
    object QrCondImovESabado1: TSmallintField
      FieldName = 'ESabado1'
      Origin = 'condimov.ESabado1'
    end
    object QrCondImovEDomingo1: TSmallintField
      FieldName = 'EDomingo1'
      Origin = 'condimov.EDomingo1'
    end
    object QrCondImovEVeic1: TSmallintField
      FieldName = 'EVeic1'
      Origin = 'condimov.EVeic1'
    end
    object QrCondImovEFilho1: TSmallintField
      FieldName = 'EFilho1'
      Origin = 'condimov.EFilho1'
    end
    object QrCondImovENome2: TIntegerField
      FieldName = 'ENome2'
      Origin = 'condimov.ENome2'
      Required = True
    end
    object QrCondImovESegunda2: TSmallintField
      FieldName = 'ESegunda2'
      Origin = 'condimov.ESegunda2'
    end
    object QrCondImovETerca2: TSmallintField
      FieldName = 'ETerca2'
      Origin = 'condimov.ETerca2'
    end
    object QrCondImovEQuarta2: TSmallintField
      FieldName = 'EQuarta2'
      Origin = 'condimov.EQuarta2'
    end
    object QrCondImovEQuinta2: TSmallintField
      FieldName = 'EQuinta2'
      Origin = 'condimov.EQuinta2'
    end
    object QrCondImovESexta2: TSmallintField
      FieldName = 'ESexta2'
      Origin = 'condimov.ESexta2'
    end
    object QrCondImovESabado2: TSmallintField
      FieldName = 'ESabado2'
      Origin = 'condimov.ESabado2'
    end
    object QrCondImovEDomingo2: TSmallintField
      FieldName = 'EDomingo2'
      Origin = 'condimov.EDomingo2'
    end
    object QrCondImovEVeic2: TSmallintField
      FieldName = 'EVeic2'
      Origin = 'condimov.EVeic2'
    end
    object QrCondImovEFilho2: TSmallintField
      FieldName = 'EFilho2'
      Origin = 'condimov.EFilho2'
    end
    object QrCondImovENome3: TIntegerField
      FieldName = 'ENome3'
      Origin = 'condimov.ENome3'
      Required = True
    end
    object QrCondImovESegunda3: TSmallintField
      FieldName = 'ESegunda3'
      Origin = 'condimov.ESegunda3'
    end
    object QrCondImovETerca3: TSmallintField
      FieldName = 'ETerca3'
      Origin = 'condimov.ETerca3'
    end
    object QrCondImovEQuarta3: TSmallintField
      FieldName = 'EQuarta3'
      Origin = 'condimov.EQuarta3'
    end
    object QrCondImovEQuinta3: TSmallintField
      FieldName = 'EQuinta3'
      Origin = 'condimov.EQuinta3'
    end
    object QrCondImovESexta3: TSmallintField
      FieldName = 'ESexta3'
      Origin = 'condimov.ESexta3'
    end
    object QrCondImovESabado3: TSmallintField
      FieldName = 'ESabado3'
      Origin = 'condimov.ESabado3'
    end
    object QrCondImovEDomingo3: TSmallintField
      FieldName = 'EDomingo3'
      Origin = 'condimov.EDomingo3'
    end
    object QrCondImovEVeic3: TSmallintField
      FieldName = 'EVeic3'
      Origin = 'condimov.EVeic3'
    end
    object QrCondImovEFilho3: TSmallintField
      FieldName = 'EFilho3'
      Origin = 'condimov.EFilho3'
    end
    object QrCondImovEmNome1: TWideStringField
      FieldName = 'EmNome1'
      Origin = 'condimov.EmNome1'
      Required = True
      Size = 100
    end
    object QrCondImovEmTel1: TWideStringField
      FieldName = 'EmTel1'
      Origin = 'condimov.EmTel1'
    end
    object QrCondImovEmNome2: TWideStringField
      FieldName = 'EmNome2'
      Origin = 'condimov.EmNome2'
      Required = True
      Size = 100
    end
    object QrCondImovEmTel2: TWideStringField
      FieldName = 'EmTel2'
      Origin = 'condimov.EmTel2'
    end
    object QrCondImovObserv: TWideMemoField
      FieldName = 'Observ'
      Origin = 'condimov.Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCondImovSitImv: TIntegerField
      FieldName = 'SitImv'
      Origin = 'condimov.SitImv'
      Required = True
    end
    object QrCondImovLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'condimov.Lk'
    end
    object QrCondImovDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'condimov.DataCad'
    end
    object QrCondImovDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'condimov.DataAlt'
    end
    object QrCondImovUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'condimov.UserCad'
    end
    object QrCondImovUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'condimov.UserAlt'
    end
    object QrCondImovImobiliaria: TIntegerField
      FieldName = 'Imobiliaria'
      Origin = 'condimov.Imobiliaria'
      Required = True
    end
    object QrCondImovContato: TWideStringField
      FieldName = 'Contato'
      Origin = 'condimov.Contato'
      Required = True
      Size = 60
    end
    object QrCondImovContTel: TWideStringField
      FieldName = 'ContTel'
      Origin = 'condimov.ContTel'
      Required = True
    end
    object QrCondImovNOMESITIMV: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESITIMV'
      Size = 3
      Calculated = True
    end
    object QrCondImovUsuario: TIntegerField
      FieldName = 'Usuario'
      Origin = 'condimov.Usuario'
      Required = True
    end
    object QrCondImovNOMEUSUARIO: TWideStringField
      FieldName = 'NOMEUSUARIO'
      Size = 100
    end
    object QrCondImovProtocolo: TIntegerField
      FieldName = 'Protocolo'
      Origin = 'condimov.Protocolo'
      Required = True
    end
    object QrCondImovNOMEPROTOCOLO: TWideStringField
      FieldName = 'NOMEPROTOCOLO'
      Origin = 'protocolos.Nome'
      Size = 100
    end
    object QrCondImovMoradores: TIntegerField
      FieldName = 'Moradores'
      Origin = 'condimov.Moradores'
      Required = True
    end
    object QrCondImovFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
      Origin = 'condimov.FracaoIdeal'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrCondImovAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'condimov.Ativo'
      Required = True
    end
    object QrCondImovBloqEndTip: TSmallintField
      FieldName = 'BloqEndTip'
      Origin = 'condimov.BloqEndTip'
      Required = True
    end
    object QrCondImovBloqEndEnt: TSmallintField
      FieldName = 'BloqEndEnt'
      Origin = 'condimov.BloqEndEnt'
      Required = True
    end
    object QrCondImovBloqEndTip_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'BloqEndTip_TXT'
      Size = 10
      Calculated = True
    end
    object QrCondImovEnderLin1: TWideStringField
      FieldName = 'EnderLin1'
      Origin = 'condimov.EnderLin1'
      Size = 100
    end
    object QrCondImovEnderLin2: TWideStringField
      FieldName = 'EnderLin2'
      Origin = 'condimov.EnderLin2'
      Size = 100
    end
    object QrCondImovEnderNome: TWideStringField
      FieldName = 'EnderNome'
      Origin = 'condimov.EnderNome'
      Size = 100
    end
    object QrCondImovProcurador: TIntegerField
      FieldName = 'Procurador'
      Origin = 'condimov.Procurador'
    end
    object QrCondImovNOMEPROCURADOR: TWideStringField
      FieldName = 'NOMEPROCURADOR'
      Size = 100
    end
    object QrCondImovWebLogin: TWideStringField
      FieldName = 'WebLogin'
      Origin = 'condimov.WebLogin'
      Size = 30
    end
    object QrCondImovWebPwd: TWideStringField
      FieldName = 'WebPwd'
      Origin = 'condimov.WebPwd'
      Size = 30
    end
    object QrCondImovWebNivel: TSmallintField
      FieldName = 'WebNivel'
      Origin = 'condimov.WebNivel'
    end
    object QrCondImovWebLogID: TWideStringField
      FieldName = 'WebLogID'
      Origin = 'condimov.WebLogID'
      Size = 32
    end
    object QrCondImovWebLastLog: TDateTimeField
      FieldName = 'WebLastLog'
      Origin = 'condimov.WebLastLog'
    end
    object QrCondImovAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'condimov.AlterWeb'
    end
    object QrCondImovModelBloq: TSmallintField
      FieldName = 'ModelBloq'
      Origin = 'condimov.ModelBloq'
    end
    object QrCondImovConfigBol: TIntegerField
      FieldName = 'ConfigBol'
      Origin = 'condimov.ConfigBol'
    end
    object QrCondImovddVctEsp: TSmallintField
      FieldName = 'ddVctEsp'
      Origin = 'condimov.ddVctEsp'
    end
    object QrCondImovIDExporta: TWideStringField
      FieldName = 'IDExporta'
      Origin = 'condimov.IDExporta'
    end
    object QrCondImovJuridico: TSmallintField
      FieldName = 'Juridico'
      Origin = 'condimov.Juridico'
    end
    object QrCondImovJuridico_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Juridico_TXT'
      Size = 2
      Calculated = True
    end
    object QrCondImovInadCEMCad: TIntegerField
      FieldName = 'InadCEMCad'
      Origin = 'condimov.InadCEMCad'
    end
    object QrCondImovSMSCelTipo: TSmallintField
      FieldName = 'SMSCelTipo'
      Origin = 'condimov.SMSCelTipo'
    end
    object QrCondImovSMSCelNumr: TWideStringField
      FieldName = 'SMSCelNumr'
      Origin = 'condimov.SMSCelNumr'
      Size = 18
    end
    object QrCondImovSMSCelNome: TWideStringField
      FieldName = 'SMSCelNome'
      Origin = 'condimov.SMSCelNome'
      Size = 60
    end
    object QrCondImovSMSCelEnti: TIntegerField
      FieldName = 'SMSCelEnti'
      Origin = 'condimov.SMSCelEnti'
    end
    object QrCondImovNOMEPROTOCOLO2: TWideStringField
      FieldName = 'NOMEPROTOCOLO2'
      Size = 100
    end
    object QrCondImovNOMEPROTOCOLO3: TWideStringField
      FieldName = 'NOMEPROTOCOLO3'
      Size = 100
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT blc.Controle, blc.Descri NO_BLOCO, imv.Unidade, imv.Conta' +
        ','
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT'
      'FROM condimov imv'
      'LEFT JOIN entidades ent ON ent.Codigo=imv.Propriet'
      'LEFT JOIN condbloco blc ON blc.Controle=imv.Controle'
      'WHERE imv.Codigo=1'
      'AND IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) LIKE "%jo%"'
      'ORDER BY NO_ENT')
    Left = 704
    Top = 548
    object QrPesqNO_BLOCO: TWideStringField
      FieldName = 'NO_BLOCO'
      Size = 100
    end
    object QrPesqUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPesqNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 732
    Top = 548
  end
  object QrCondEmeios: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM condemeios'
      'WHERE Conta=:P0')
    Left = 592
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondEmeiosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondEmeiosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCondEmeiosConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCondEmeiosItem: TIntegerField
      FieldName = 'Item'
    end
    object QrCondEmeiosPronome: TWideStringField
      FieldName = 'Pronome'
    end
    object QrCondEmeiosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCondEmeiosEMeio: TWideStringField
      FieldName = 'EMeio'
      Size = 100
    end
  end
  object DsCondEmeios: TDataSource
    DataSet = QrCondEmeios
    Left = 620
    Top = 424
  end
  object PMEmeio: TPopupMenu
    OnPopup = PMEmeioPopup
    Left = 420
    Top = 572
    object Incluiemeio1: TMenuItem
      Caption = '&Inclui e-mail'
      OnClick = Incluiemeio1Click
    end
    object Alteraemeio1: TMenuItem
      Caption = '&Altera e-mail'
      OnClick = Alteraemeio1Click
    end
    object Excluiemeio1: TMenuItem
      Caption = '&Exclui e-mail'
      OnClick = Excluiemeio1Click
    end
    object MenuItem1: TMenuItem
      Caption = '-'
    end
    object Buscardocadastrodeentidade1: TMenuItem
      Caption = '&Buscar do cadastro de entidade'
      OnClick = Buscardocadastrodeentidade1Click
    end
  end
  object TbUnidades: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Codigo=-1000'
    Filtered = True
    SortFieldNames = 'Unidade'
    TableName = 'condimov'
    Left = 824
    Top = 12
    object TbUnidadesUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Size = 10
    end
    object TbUnidadesFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
      Origin = 'condimov.FracaoIdeal'
      DisplayFormat = '#,###,##0.000000'
    end
    object TbUnidadesControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'condimov.Controle'
    end
    object TbUnidadesConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'condimov.Conta'
    end
    object TbUnidadesPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'condimov.Propriet'
    end
    object TbUnidadesIDExporta: TWideStringField
      FieldName = 'IDExporta'
      Origin = 'condimov.IDExporta'
    end
    object TbUnidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'condimov.Codigo'
    end
    object TbUnidadesNOME_BLOCO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_BLOCO'
      LookupDataSet = QrCB
      LookupKeyFields = 'Controle'
      LookupResultField = 'Descri'
      KeyFields = 'Controle'
      Size = 100
      Lookup = True
    end
    object TbUnidadesNOME_PROPRIET: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PROPRIET'
      LookupDataSet = QrCondImvProp
      LookupKeyFields = 'Codigo'
      LookupResultField = 'NOME_PROPRIET'
      KeyFields = 'Propriet'
      Size = 100
      Lookup = True
    end
  end
  object DsUnidades: TDataSource
    DataSet = TbUnidades
    Left = 852
    Top = 12
  end
  object QrCondModBol: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCondModBolCalcFields
    SQL.Strings = (
      'SELECT imv.Unidade, cfb.Nome NOME_CONFIGBOL,'
      'cmb.Apto, cmb.ConfigBol, cmb.ModelBloq, '
      'cmb.BalAgrMens, cmb.Compe, cmb.Codigo'
      'FROM condmodbol cmb'
      'LEFT JOIN condimov imv ON imv.Conta=cmb.Apto'
      'LEFT JOIN configbol cfb ON cfb.Codigo=cmb.ConfigBol'
      'WHERE cmb.Codigo=:P0'#13)
    Left = 496
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondModBolUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrCondModBolNOME_CONFIGBOL: TWideStringField
      FieldName = 'NOME_CONFIGBOL'
      Size = 50
    end
    object QrCondModBolApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrCondModBolConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrCondModBolModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrCondModBolBalAgrMens: TSmallintField
      FieldName = 'BalAgrMens'
    end
    object QrCondModBolCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrCondModBolNOME_MODELBLOQ: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_MODELBLOQ'
      Size = 1
      Calculated = True
    end
    object QrCondModBolCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCondModBol: TDataSource
    DataSet = QrCondModBol
    Left = 524
    Top = 388
  end
  object PMBloquetos: TPopupMenu
    OnPopup = PMBloquetosPopup
    Left = 428
    Top = 204
    object Incluialteramodeloconfigurao1: TMenuItem
      Caption = '&Inclui altera modelo / configura'#231#227'o (UHs)'
      OnClick = Incluialteramodeloconfigurao1Click
    end
    object Excluiitemns1: TMenuItem
      Caption = '&Exclui item(ns) (UHs)'
      OnClick = Excluiitemns1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Editaremailsparaenviodeboletos2: TMenuItem
      Caption = 'Edi&tar e-mails para envio de boletos'
      OnClick = Editaremailsparaenviodeboletos2Click
    end
  end
  object QrContasMes: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContasMesCalcFields
    SQL.Strings = (
      'SELECT IF(en.Tipo=0,RazaoSocial,en.Nome) NO_CliInt, cm.* '
      'FROM contasmes cm'
      'LEFT JOIN entidades en ON en.Codigo=cm.CliInt'
      'WHERE cm.CliInt=:P0')
    Left = 520
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasMesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasMesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContasMesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrContasMesDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrContasMesPeriodoIni: TIntegerField
      FieldName = 'PeriodoIni'
    end
    object QrContasMesPeriodoFim: TIntegerField
      FieldName = 'PeriodoFim'
    end
    object QrContasMesValorMin: TFloatField
      FieldName = 'ValorMin'
    end
    object QrContasMesValorMax: TFloatField
      FieldName = 'ValorMax'
    end
    object QrContasMesPERIODOINI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODOINI_TXT'
      Size = 10
      Calculated = True
    end
    object QrContasMesPERIODOFIM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODOFIM_TXT'
      Size = 10
      Calculated = True
    end
    object QrContasMesNO_CliInt: TWideStringField
      FieldName = 'NO_CliInt'
      Size = 100
    end
    object QrContasMesQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
    end
    object QrContasMesQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
    end
    object QrContasMesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasMesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasMesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasMesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasMesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasMesAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrContasMesAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrContasMesDiaAlert: TSmallintField
      FieldName = 'DiaAlert'
    end
    object QrContasMesDiaVence: TSmallintField
      FieldName = 'DiaVence'
    end
    object QrContasMesTipMesRef: TSmallintField
      FieldName = 'TipMesRef'
    end
  end
  object DsContasMes: TDataSource
    DataSet = QrContasMes
    Left = 548
    Top = 336
  end
  object PMContasMes: TPopupMenu
    Left = 500
    Top = 204
    object Incluimultiplascontas1: TMenuItem
      Caption = 'Inclui m'#250'ltiplas contas'
      OnClick = Incluimultiplascontas1Click
    end
    object Incluinicacontaeconfigura1: TMenuItem
      Caption = 'Inclui '#250'nica conta (e configura)'
      OnClick = Incluinicacontaeconfigura1Click
    end
  end
  object QrAssist: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM entidades'
      'WHERE Fornece5="V"'
      'OR Codigo=0'
      'ORDER BY Nome')
    Left = 520
    Top = 580
  end
  object DsAssist: TDataSource
    DataSet = QrAssist
    Left = 548
    Top = 580
  end
  object OpenPictureDialog1: TOpenPictureDialog
    DefaultExt = 'bmp'
    Filter = 
      'Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Metafiles (*.wmf)|*.wm' +
      'f'
    Left = 636
    Top = 564
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = Timer1Timer
    Left = 728
    Top = 72
  end
  object QrCedente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial '
      'ELSE ent.Nome END NOMEENT, Codigo'
      'FROM entidades ent'
      'WHERE Terceiro="V"'
      'OR Cliente1="V"'
      'OR Fornece1="V"'
      'ORDER BY NOMEENT')
    Left = 220
    Top = 532
    object QrCedenteNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrCedenteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCedente: TDataSource
    DataSet = QrCedente
    Left = 248
    Top = 532
  end
  object QrSacador: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial '
      'ELSE ent.Nome END NOMEENT, Codigo'
      'FROM entidades ent'
      'WHERE Terceiro="V"'
      'OR Cliente1="V"'
      'OR Fornece1="V"'
      'ORDER BY NOMEENT')
    Left = 276
    Top = 532
    object QrSacadorNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrSacadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsSacador: TDataSource
    DataSet = QrSacador
    Left = 304
    Top = 532
  end
  object QrCB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Descri '
      'FROM condbloco'
      'WHERE Codigo =:P0')
    Left = 664
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCBControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCBDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
  end
  object QrCondImvProp: TmySQLQuery
    Database = Dmod.MyDB
    Left = 692
    Top = 388
    object QrCondImvPropNOME_PROPRIET: TWideStringField
      FieldName = 'NOME_PROPRIET'
      Size = 100
    end
    object QrCondImvPropCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrCfgInfl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cfginfl'
      'ORDER BY Nome')
    Left = 328
    Top = 600
    object QrCfgInflCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCfgInflNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCfgInfl: TDataSource
    DataSet = QrCfgInfl
    Left = 356
    Top = 600
  end
  object QrConfigBol: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContasMesCalcFields
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM configbol'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 368
    Top = 320
    object QrConfigBolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigBolNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsConfigBol: TDataSource
    DataSet = QrConfigBol
    Left = 396
    Top = 320
  end
  object QrCNAB_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT abc.Codigo, abc.Sigla, abc.Nome'
      'FROM arrebai abi'
      'LEFT JOIN arrebac abc ON abc.Codigo=abi.Codigo'
      'WHERE Cond=:P0'
      'ORDER BY Nome')
    Left = 128
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 156
    Top = 284
  end
end
