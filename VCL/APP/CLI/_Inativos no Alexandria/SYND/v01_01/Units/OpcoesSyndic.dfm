object FmOpcoesSyndic: TFmOpcoesSyndic
  Left = 339
  Top = 185
  Caption = 'FER-OPCAO-002 :: Op'#231#245'es Espec'#237'ficas do Aplicativo'
  ClientHeight = 555
  ClientWidth = 803
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 803
    Height = 393
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 0
    object TabSheet2: TTabSheet
      Caption = 'Impress'#245'es'
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 795
        Height = 365
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object GroupBox6: TGroupBox
          Left = 8
          Top = 8
          Width = 429
          Height = 90
          Caption = ' Bloqueto condominial: '
          TabOrder = 0
          object Label28: TLabel
            Left = 112
            Top = 20
            Width = 64
            Height = 13
            Caption = 'Margem esq.:'
          end
          object Label29: TLabel
            Left = 188
            Top = 20
            Width = 28
            Height = 13
            Caption = 'Topo:'
          end
          object Label30: TLabel
            Left = 12
            Top = 62
            Width = 94
            Height = 13
            Caption = 'Destinat'#225'rio correio:'
          end
          object Label1: TLabel
            Left = 12
            Top = 38
            Width = 58
            Height = 13
            Caption = 'Aviso verso:'
          end
          object Label2: TLabel
            Left = 264
            Top = 20
            Width = 30
            Height = 13
            Caption = 'Altura:'
          end
          object Label3: TLabel
            Left = 340
            Top = 20
            Width = 39
            Height = 13
            Caption = 'Largura:'
          end
          object EdBLQ_MEsqDestin: TdmkEdit
            Left = 112
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_TopoDestin: TdmkEdit
            Left = 188
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_MEsqAvisoV: TdmkEdit
            Left = 112
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_TopoAvisoV: TdmkEdit
            Left = 188
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_AltuAvisoV: TdmkEdit
            Left = 264
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_AltuDestin: TdmkEdit
            Left = 264
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_LargAvisoV: TdmkEdit
            Left = 340
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_LargDestin: TdmkEdit
            Left = 340
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnExit = EdBLQ_LargDestinExit
          end
        end
        object GroupBox2: TGroupBox
          Left = 8
          Top = 100
          Width = 429
          Height = 114
          Caption = ' Balancete: '
          TabOrder = 1
          object Label7: TLabel
            Left = 112
            Top = 20
            Width = 64
            Height = 13
            Caption = 'Margem esq.:'
            Enabled = False
          end
          object Label8: TLabel
            Left = 188
            Top = 20
            Width = 28
            Height = 13
            Caption = 'Topo:'
          end
          object Label9: TLabel
            Left = 12
            Top = 62
            Width = 31
            Height = 13
            Caption = 'T'#237'tulo:'
          end
          object Label10: TLabel
            Left = 12
            Top = 38
            Width = 90
            Height = 13
            Caption = 'Nome condom'#237'nio:'
          end
          object Label11: TLabel
            Left = 12
            Top = 86
            Width = 41
            Height = 13
            Caption = 'Per'#237'odo:'
          end
          object Label12: TLabel
            Left = 264
            Top = 20
            Width = 30
            Height = 13
            Caption = 'Altura:'
            Enabled = False
          end
          object Label13: TLabel
            Left = 340
            Top = 20
            Width = 39
            Height = 13
            Caption = 'Largura:'
            Enabled = False
          end
          object dmkEdit1: TdmkEdit
            Left = 112
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            MaxLength = 20
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit3: TdmkEdit
            Left = 112
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            MaxLength = 20
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit5: TdmkEdit
            Left = 112
            Top = 84
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            MaxLength = 20
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit7: TdmkEdit
            Left = 264
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            MaxLength = 20
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit8: TdmkEdit
            Left = 264
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            MaxLength = 20
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit9: TdmkEdit
            Left = 340
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            MaxLength = 20
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnExit = EdBLQ_LargDestinExit
          end
          object dmkEdit10: TdmkEdit
            Left = 340
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            MaxLength = 20
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit11: TdmkEdit
            Left = 264
            Top = 84
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            MaxLength = 20
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit12: TdmkEdit
            Left = 340
            Top = 84
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            MaxLength = 20
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnExit = EdBLQ_LargDestinExit
          end
          object dmkEdBAL_TopoNomCon: TdmkEdit
            Left = 188
            Top = 36
            Width = 73
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdBAL_TopoTitulo: TdmkEdit
            Left = 188
            Top = 60
            Width = 73
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdBAL_TopoPeriod: TdmkEdit
            Left = 188
            Top = 84
            Width = 73
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Miscel'#226'nea'
      ImageIndex = 2
      object Label31: TLabel
        Left = 12
        Top = 119
        Width = 141
        Height = 13
        Caption = 'Cargo utilizado para o sindico:'
      end
      object GroupBox1: TGroupBox
        Left = 12
        Top = 8
        Width = 105
        Height = 105
        Caption = ' % INSS s'#237'ndico: '
        TabOrder = 0
        object Label5: TLabel
          Left = 12
          Top = 20
          Width = 51
          Height = 13
          Caption = '% S'#237'ndico:'
        end
        object Label6: TLabel
          Left = 12
          Top = 60
          Width = 71
          Height = 13
          Caption = '% Condom'#237'nio:'
        end
        object dmkEdProLaINSSr: TdmkEdit
          Left = 12
          Top = 36
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object dmkEdProLaINSSp: TdmkEdit
          Left = 12
          Top = 76
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox4: TGroupBox
        Left = 123
        Top = 8
        Width = 206
        Height = 105
        Caption = 'Consumo por leitura'
        TabOrder = 1
        object CkLookMedAnt: TCheckBox
          Left = 6
          Top = 20
          Width = 191
          Height = 17
          Caption = 'Bloquear a edi'#231#227'o da leitura anterior'
          TabOrder = 0
        end
      end
      object GroupBox5: TGroupBox
        Left = 332
        Top = 8
        Width = 437
        Height = 105
        Caption = ' Envio de emeio de bloqueto: '
        TabOrder = 2
        object Label19: TLabel
          Left = 8
          Top = 24
          Width = 198
          Height = 13
          Caption = 'Considerar que o emeio foi recebido ap'#243's '
        end
        object Label20: TLabel
          Left = 304
          Top = 24
          Width = 115
          Height = 13
          Caption = 'dias do seu vencimento.'
        end
        object Label21: TLabel
          Left = 8
          Top = 44
          Width = 407
          Height = 13
          Caption = 
            'Pr'#233'-emeio para re-envio de aviso da falta de confirma'#231#227'o do rece' +
            'bimento do bloqueto:'
        end
        object EdDdAutConfMail: TdmkEdit
          Left = 216
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '60'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 60
          ValWarn = False
        end
        object EdPreMailReenv: TdmkEditCB
          Left = 8
          Top = 60
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPreMailReenv
          IgnoraDBLookupComboBox = False
        end
        object CBPreMailReenv: TdmkDBLookupComboBox
          Left = 68
          Top = 60
          Width = 349
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsPreEmail
          TabOrder = 2
          dmkEditCB = EdPreMailReenv
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object EdEntiCargSind: TdmkEditCB
        Left = 12
        Top = 135
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEntiCargSind
        IgnoraDBLookupComboBox = False
      end
      object CBEntiCargSind: TdmkDBLookupComboBox
        Left = 72
        Top = 135
        Width = 349
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsEntiCargos
        TabOrder = 4
        dmkEditCB = EdEntiCargSind
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object GroupBox7: TGroupBox
        Left = 428
        Top = 116
        Width = 341
        Height = 49
        Caption = ' Processo de protesto de boleto vencido:'
        TabOrder = 5
        object Label36: TLabel
          Left = 95
          Top = 24
          Width = 151
          Height = 13
          Caption = 'dias ap'#243's recebimento de carta.'
        end
        object EdDdPrtstCrt: TdmkEdit
          Left = 9
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'DdPrtstCrt'
          UpdCampo = 'DdPrtstCrt'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = ' Fluxos '
      ImageIndex = 3
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 795
        Height = 365
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object GroupBox39: TGroupBox
          Left = 0
          Top = 0
          Width = 244
          Height = 365
          Align = alLeft
          Caption = 'Bloqueto - Dia (m'#234's) limite* : '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object Label211: TLabel
            Left = 12
            Top = 140
            Width = 111
            Height = 13
            Caption = 'Gera'#231#227'o de protocolos:'
            FocusControl = EdFlxB_Proto
          end
          object Label212: TLabel
            Left = 12
            Top = 164
            Width = 50
            Height = 13
            Caption = 'Relat'#243'rios:'
            FocusControl = EdFlxB_Relat
          end
          object Label213: TLabel
            Left = 12
            Top = 188
            Width = 142
            Height = 13
            Caption = 'Envio de bloquetos por e-mail:'
            FocusControl = EdFlxB_EMail
          end
          object Label214: TLabel
            Left = 12
            Top = 212
            Width = 141
            Height = 13
            Caption = 'Envio de bloquetos '#224' portaria:'
            FocusControl = EdFlxB_Porta
          end
          object Label215: TLabel
            Left = 12
            Top = 236
            Width = 160
            Height = 13
            Caption = 'Envio de bloquetos via postagem:'
            FocusControl = EdFlxB_Postl
          end
          object Label210: TLabel
            Left = 12
            Top = 116
            Width = 120
            Height = 13
            Caption = 'Impress'#227'o dos bloquetos:'
            FocusControl = EdFlxB_Print
          end
          object Label209: TLabel
            Left = 12
            Top = 92
            Width = 117
            Height = 13
            Caption = 'Administradora de riscos:'
            FocusControl = EdFlxB_Risco
          end
          object Label208: TLabel
            Left = 12
            Top = 68
            Width = 62
            Height = 13
            Caption = 'Fechamento:'
          end
          object Label207: TLabel
            Left = 12
            Top = 44
            Width = 116
            Height = 13
            Caption = 'Leituras / arrecada'#231#245'es:'
            FocusControl = EdFlxB_LeiAr
          end
          object Label206: TLabel
            Left = 12
            Top = 20
            Width = 100
            Height = 13
            Caption = 'Folha de pagamento:'
            FocusControl = EdFlxB_Folha
          end
          object EdFlxB_Folha: TdmkEdit
            Left = 188
            Top = 16
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxB_Folha'
            UpdCampo = 'FlxB_Folha'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxB_Proto: TdmkEdit
            Left = 188
            Top = 136
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxB_Proto'
            UpdCampo = 'FlxB_Proto'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxB_Relat: TdmkEdit
            Left = 188
            Top = 160
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxB_Relat'
            UpdCampo = 'FlxB_Relat'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxB_EMail: TdmkEdit
            Left = 188
            Top = 184
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxB_EMail'
            UpdCampo = 'FlxB_EMail'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxB_Porta: TdmkEdit
            Left = 188
            Top = 208
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxB_Porta'
            UpdCampo = 'FlxB_Porta'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxB_Postl: TdmkEdit
            Left = 188
            Top = 232
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxB_Postl'
            UpdCampo = 'FlxB_Postl'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxB_Print: TdmkEdit
            Left = 188
            Top = 112
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxB_Print'
            UpdCampo = 'FlxB_Print'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxB_Risco: TdmkEdit
            Left = 188
            Top = 88
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxB_Risco'
            UpdCampo = 'FlxB_Risco'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxB_LeiAr: TdmkEdit
            Left = 188
            Top = 40
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxB_LeiAr'
            UpdCampo = 'FlxB_LeiAr'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxB_Fecha: TdmkEdit
            Left = 188
            Top = 64
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxB_Fecha'
            UpdCampo = 'FlxB_Fecha'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object GroupBox40: TGroupBox
          Left = 244
          Top = 0
          Width = 551
          Height = 365
          Align = alClient
          Caption = 'Balancete - Dia (m'#234's) limite* : '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object Label217: TLabel
            Left = 12
            Top = 20
            Width = 63
            Height = 13
            Caption = 'Concilia'#231#245'es:'
            FocusControl = EdFlxM_Conci
          end
          object Label218: TLabel
            Left = 12
            Top = 44
            Width = 66
            Height = 13
            Caption = 'Documentos: '
            FocusControl = EdFlxM_Docum
          end
          object Label222: TLabel
            Left = 12
            Top = 140
            Width = 40
            Height = 13
            Caption = 'Entrega:'
            FocusControl = EdFlxM_Entrg
          end
          object Label221: TLabel
            Left = 12
            Top = 116
            Width = 76
            Height = 13
            Caption = 'Encadernac'#227'o: '
            FocusControl = EdFlxM_Encad
          end
          object Label220: TLabel
            Left = 12
            Top = 68
            Width = 94
            Height = 13
            Caption = 'ISS / contabilidade:'
            FocusControl = EdFlxM_Contb
          end
          object Label219: TLabel
            Left = 12
            Top = 92
            Width = 37
            Height = 13
            Caption = 'An'#225'lise:'
            FocusControl = EdFlxM_Anali
          end
          object Label27: TLabel
            Left = 12
            Top = 164
            Width = 26
            Height = 13
            Caption = 'Web:'
            FocusControl = EdFlxM_Web
          end
          object EdFlxM_Conci: TdmkEdit
            Left = 188
            Top = 16
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxM_Conci'
            UpdCampo = 'FlxM_Conci'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxM_Docum: TdmkEdit
            Left = 188
            Top = 40
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxM_Docum'
            UpdCampo = 'FlxM_Docum'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxM_Entrg: TdmkEdit
            Left = 188
            Top = 136
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxM_Entrg'
            UpdCampo = 'FlxM_Entrg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxM_Encad: TdmkEdit
            Left = 188
            Top = 112
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxM_Encad'
            UpdCampo = 'FlxM_Encad'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxM_Contb: TdmkEdit
            Left = 188
            Top = 64
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxM_Contb'
            UpdCampo = 'FlxM_Contb'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxM_Anali: TdmkEdit
            Left = 188
            Top = 88
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxM_Anali'
            UpdCampo = 'FlxM_Anali'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFlxM_Web: TdmkEdit
            Left = 188
            Top = 160
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '31'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FlxM_Web'
            UpdCampo = 'FlxM_Web'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = ' Prefer'#234'ncias do Usu'#225'rio '
      ImageIndex = 4
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 795
        Height = 365
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object RGContasMesEmiss: TRadioGroup
          Left = 0
          Top = 0
          Width = 795
          Height = 49
          Align = alTop
          Caption = ' Pesquisa de emiss'#245'es de contas mensais pendentes: '
          Columns = 3
          Items.Strings = (
            'N'#227'o pesquisar'
            'Pesquisar apenas uma vez ao dia'
            'Pesquisar sempre que executar o aplicativo')
          TabOrder = 0
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = ' Legendas Customizadas '
      ImageIndex = 5
      object dmkDBGrid1: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 795
        Height = 365
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            ReadOnly = True
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ordem'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TxtSys'
            Title.Caption = 'Substituir'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TxtMeu'
            Title.Caption = 'Por'
            Width = 300
            Visible = True
          end>
        Color = clWindow
        DataSource = DsUserTxts
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            ReadOnly = True
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ordem'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TxtSys'
            Title.Caption = 'Substituir'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TxtMeu'
            Title.Caption = 'Por'
            Width = 300
            Visible = True
          end>
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'BD Fotos Docs'
      ImageIndex = 6
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 795
        Height = 365
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label22: TLabel
          Left = 16
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Servidor:'
        end
        object Label23: TLabel
          Left = 13
          Top = 84
          Width = 39
          Height = 13
          Caption = 'Usu'#225'rio:'
        end
        object Label24: TLabel
          Left = 13
          Top = 124
          Width = 34
          Height = 13
          Caption = 'Senha:'
        end
        object Label25: TLabel
          Left = 13
          Top = 164
          Width = 81
          Height = 13
          Caption = 'Banco de dados:'
        end
        object Label26: TLabel
          Left = 13
          Top = 44
          Width = 28
          Height = 13
          Caption = 'Porta:'
        end
        object EdCB4_Host: TdmkEdit
          Left = 13
          Top = 20
          Width = 201
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCB4_User: TdmkEdit
          Left = 13
          Top = 100
          Width = 201
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCB4_Pwd: TdmkEdit
          Left = 13
          Top = 140
          Width = 201
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCB4_DB: TdmkEdit
          Left = 13
          Top = 183
          Width = 201
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGCB4_Uso: TRadioGroup
          Left = 220
          Top = 16
          Width = 229
          Height = 189
          Caption = ' CodeBase no meu servidor: '
          ItemIndex = 0
          Items.Strings = (
            'N'#227'o possuo, ou n'#227'o tenho acesso direto.'
            'Conectar somente quando eu necessitar.')
          TabOrder = 4
        end
        object EdCB4_Port: TdmkEdit
          Left = 13
          Top = 60
          Width = 201
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
    object TabSheet8: TTabSheet
      Caption = ' Cobran'#231'a '
      ImageIndex = 7
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 795
        Height = 365
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object GroupBox8: TGroupBox
          Left = 8
          Top = 4
          Width = 769
          Height = 65
          Caption = ' Dados do modem (ou celular) para envio de SMS:'
          Enabled = False
          TabOrder = 0
          Visible = False
          object Panel8: TPanel
            Left = 2
            Top = 15
            Width = 765
            Height = 48
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label37: TLabel
              Left = 8
              Top = 4
              Width = 146
              Height = 13
              Caption = 'Nome do dispositivo instalado: '
            end
            object SBSMSCelCOM: TSpeedButton
              Left = 496
              Top = 19
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SBSMSCelCOMClick
            end
            object Label38: TLabel
              Left = 524
              Top = 4
              Width = 48
              Height = 13
              Caption = 'PIN code:'
            end
            object EdSMSCelCOM: TdmkEdit
              Left = 8
              Top = 20
              Width = 485
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'SMSCelCOM'
              UpdCampo = 'SMSCelCOM'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdSMSCelPIN: TdmkEdit
              Left = 524
              Top = 20
              Width = 53
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'SMSCelCOM'
              UpdCampo = 'SMSCelCOM'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 803
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 755
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 707
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 441
    Width = 803
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 799
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 485
    Width = 803
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 657
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 655
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 10
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 12
    Top = 8
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 40
    Top = 8
  end
  object QrPreEmail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 72
    Top = 8
  end
  object DsPreEmail: TDataSource
    DataSet = QrPreEmail
    Left = 100
    Top = 8
  end
  object TbUserTxts: TmySQLTable
    Database = Dmod.MyDB
    BeforePost = TbUserTxtsBeforePost
    TableName = 'usertxts'
    Left = 140
    Top = 8
    object TbUserTxtsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbUserTxtsTxtSys: TWideStringField
      FieldName = 'TxtSys'
      Size = 255
    end
    object TbUserTxtsTxtMeu: TWideStringField
      FieldName = 'TxtMeu'
      Size = 255
    end
    object TbUserTxtsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsUserTxts: TDataSource
    DataSet = TbUserTxts
    Left = 168
    Top = 8
  end
  object QrEntiCargos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM enticargos'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 196
    Top = 7
    object QrEntiCargosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiCargosCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntiCargosNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiCargos: TDataSource
    DataSet = QrEntiCargos
    Left = 224
    Top = 7
  end
  object VUEntiCargSind: TdmkValUsu
    dmkEditCB = EdEntiCargSind
    QryCampo = 'CodUsu'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 252
    Top = 7
  end
end
