unit bloqparc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, dmkDBGrid, dmkEdit, frxClass, frxDBSet, UnFinanceiro, ABSMain,
  dmkGeral, dmkDBLookupComboBox, dmkEditCB, Variants, Menus, UnDmkProcFunc,
  dmkImage, UnDmkEnums;

  //Menus,
  //ImgList;

type
  THackDBGrid = class(TDBGrid);
  TFmbloqparc = class(TForm)
    Dsbloqparc: TDataSource;
    Qrbloqparc: TmySQLQuery;
    QrbloqparcNOMEPROP: TWideStringField;
    QrbloqparcUnidade: TWideStringField;
    QrbloqparcCodigo: TAutoIncField;
    QrbloqparcVlrOrigi: TFloatField;
    QrbloqparcVlrMulta: TFloatField;
    QrbloqparcVlrJuros: TFloatField;
    QrbloqparcVlrTotal: TFloatField;
    QrbloqparcDataP: TDateTimeField;
    QrbloqparcLk: TIntegerField;
    QrbloqparcDataCad: TDateField;
    QrbloqparcDataAlt: TDateField;
    QrbloqparcUserCad: TIntegerField;
    QrbloqparcUserAlt: TIntegerField;
    QrbloqparcAlterWeb: TSmallintField;
    QrbloqparcloginID: TWideStringField;
    Qrbloqparcautentica: TWideMemoField;
    QrbloqparcNovo: TSmallintField;
    QrbloqparcCodCliEsp: TIntegerField;
    QrbloqparcCodCliEnt: TIntegerField;
    QrbloqparcCodigoEnt: TIntegerField;
    QrbloqparcCodigoEsp: TIntegerField;
    QrbloqparcTxaJur: TFloatField;
    QrbloqparcTxaMul: TFloatField;
    QrbloqparcSTATUS: TWideStringField;
    Qrlct: TmySQLQuery;
    Dslct: TDataSource;
    QrlctData: TDateField;
    QrlctControle: TIntegerField;
    QrlctDescricao: TWideStringField;
    QrlctVencimento: TDateField;
    QrlctMez: TIntegerField;
    QrlctCredito: TFloatField;
    QrlctMES_TXT: TWideStringField;
    Qrbloqparcpar: TmySQLQuery;
    QrbloqparcparCodigo: TIntegerField;
    QrbloqparcparControle: TAutoIncField;
    QrbloqparcparParcela: TIntegerField;
    QrbloqparcparVlrOrigi: TFloatField;
    QrbloqparcparVlrMulta: TFloatField;
    QrbloqparcparVlrJuros: TFloatField;
    QrbloqparcparLk: TIntegerField;
    QrbloqparcparDataCad: TDateField;
    QrbloqparcparDataAlt: TDateField;
    QrbloqparcparUserCad: TIntegerField;
    QrbloqparcparUserAlt: TIntegerField;
    QrbloqparcparAlterWeb: TSmallintField;
    QrbloqparcparValBol: TFloatField;
    QrbloqparcparVencimento: TDateField;
    Dsbloqparcpar: TDataSource;
    QrPesqlan: TmySQLQuery;
    QrPesqlanData: TDateField;
    QrPesqlanControle: TIntegerField;
    QrPesqlanDescricao: TWideStringField;
    QrPesqlanVencimento: TDateField;
    QrPesqlanMez: TIntegerField;
    QrPesqlanCredito: TFloatField;
    QrPesqlanSub: TSmallintField;
    QrlctSub: TSmallintField;
    QrPesqlanReparcel: TIntegerField;
    QrPesqlanMulta: TFloatField;
    QrPesqlanMoraDia: TFloatField;
    QrlctReparcel: TIntegerField;
    QrlctMulta: TFloatField;
    QrlctMoraDia: TFloatField;
    frxDsBloqParcPar: TfrxDBDataset;
    frxDsLcto: TfrxDBDataset;
    frxDsBloqParc: TfrxDBDataset;
    Qrbloqparcits: TmySQLQuery;
    Dsbloqparcits: TDataSource;
    QrbloqparcitsCodigo: TIntegerField;
    QrbloqparcitsControle: TIntegerField;
    QrbloqparcitsConta: TAutoIncField;
    QrbloqparcitsSeq: TIntegerField;
    QrbloqparcitsTipo: TWideStringField;
    QrbloqparcitsCtrlOrigi: TIntegerField;
    QrbloqparcitsBloqOrigi: TIntegerField;
    QrbloqparcitsPercIni: TFloatField;
    QrbloqparcitsPercFim: TFloatField;
    QrbloqparcitsVlrOrigi: TFloatField;
    QrbloqparcitsVlrMulta: TFloatField;
    QrbloqparcitsVlrJuros: TFloatField;
    QrbloqparcitsVlrTotal: TFloatField;
    QrbloqparcitsVlrAjust: TFloatField;
    QrbloqparcitsVlrSomas: TFloatField;
    QrbloqparcitsSParcA: TFloatField;
    QrbloqparcitsSParcD: TFloatField;
    QrbloqparcitsDias: TIntegerField;
    QrbloqparcitsLk: TIntegerField;
    QrbloqparcitsDataCad: TDateField;
    QrbloqparcitsDataAlt: TDateField;
    QrbloqparcitsUserCad: TIntegerField;
    QrbloqparcitsUserAlt: TIntegerField;
    QrbloqparcitsAlterWeb: TSmallintField;
    QrbloqparcitsAtivo: TSmallintField;
    frxDsBPI: TfrxDBDataset;
    QrBPI: TmySQLQuery;
    QrBPICodigo: TIntegerField;
    QrBPIControle: TIntegerField;
    QrBPIConta: TAutoIncField;
    QrBPISeq: TIntegerField;
    QrBPITipo: TWideStringField;
    QrBPICtrlOrigi: TIntegerField;
    QrBPIBloqOrigi: TIntegerField;
    QrBPIPercIni: TFloatField;
    QrBPIPercFim: TFloatField;
    QrBPIVlrOrigi: TFloatField;
    QrBPIVlrMulta: TFloatField;
    QrBPIVlrJuros: TFloatField;
    QrBPIVlrTotal: TFloatField;
    QrBPIVlrAjust: TFloatField;
    QrBPIVlrSomas: TFloatField;
    QrBPISParcA: TFloatField;
    QrBPISParcD: TFloatField;
    QrBPIDias: TIntegerField;
    QrBPILk: TIntegerField;
    QrBPIDataCad: TDateField;
    QrBPIDataAlt: TDateField;
    QrBPIUserCad: TIntegerField;
    QrBPIUserAlt: TIntegerField;
    QrBPIAlterWeb: TSmallintField;
    QrBPIAtivo: TSmallintField;
    QrBPIParcela: TIntegerField;
    QrbloqparcNOMECOND: TWideStringField;
    QrStatus: TmySQLQuery;
    QrStatusNovo: TSmallintField;
    QrSomas: TmySQLQuery;
    QrSomasVlrOrigi: TFloatField;
    QrSomasVlrMulta: TFloatField;
    QrSomasVlrJuros: TFloatField;
    QrSomasVlrTotal: TFloatField;
    QrSomaT: TmySQLQuery;
    QrBloqorig: TmySQLQuery;
    QrBloqorigBloqOrigi: TIntegerField;
    QrBloqorigVlrOrigi: TFloatField;
    QrBloqorigVlrMulta: TFloatField;
    QrBloqorigVlrJuros: TFloatField;
    QrBloqorigVlrTotal: TFloatField;
    frxDsBloqlan: TfrxDBDataset;
    QrBloqlan: TmySQLQuery;
    QrBloqlanCredito: TFloatField;
    QrBloqlanVencimento: TDateField;
    QrBloqlanVLRORIG: TFloatField;
    QrBloqlanVLRMULTA: TFloatField;
    QrBloqlanVLRJUROS: TFloatField;
    QrBloqlanVLRTOTAL: TFloatField;
    QrSomaBPI: TmySQLQuery;
    QrSomaBPP: TmySQLQuery;
    QrSomaBPIVlrSomas: TFloatField;
    QrSomaBPIVlrJuros: TFloatField;
    QrSomaBPPValBol: TFloatField;
    QrIts: TmySQLQuery;
    QrSomaBPIVlrTotal: TFloatField;
    QrItsConta: TAutoIncField;
    QrBPP_Pgt: TmySQLQuery;
    DsBPP_Pgt: TDataSource;
    QrbloqparcitsParcela: TIntegerField;
    QrbloqparcitsVlrLanct: TFloatField;
    QrBPP_PgtControle: TIntegerField;
    QrBPP_PgtData: TDateField;
    QrBPP_PgtNOMECONTA: TWideStringField;
    QrBPP_PgtGenero: TIntegerField;
    QrBPP_PgtDescricao: TWideStringField;
    QrBPP_PgtCredito: TFloatField;
    QrBPP_PgtMultaVal: TFloatField;
    QrBPP_PgtMoraVal: TFloatField;
    QrIncorr: TmySQLQuery;
    DsIncorr: TDataSource;
    QrIncorrControle: TIntegerField;
    QrIncorrData: TDateField;
    QrIncorrDescricao: TWideStringField;
    QrIncorrCredito: TFloatField;
    QrBPP_ToP: TmySQLQuery;
    QrBPP_ToPCredito: TFloatField;
    QrBPP_ToPMultaVal: TFloatField;
    QrBPP_ToPMoraVal: TFloatField;
    DsBPP_ToP: TDataSource;
    QrbloqparcitsVlrJuro2: TFloatField;
    QrbloqparcitsVlrMult2: TFloatField;
    QrIncorrTipo: TSmallintField;
    QrIncorrCarteira: TIntegerField;
    QrIncorrSit: TIntegerField;
    QrbloqparcparFatNum: TFloatField;
    QrBloqlanFatNum: TFloatField;
    QrlctBLOQUETO: TFloatField;
    QrPesqlanBLOQUETO: TFloatField;
    QrBPI2: TmySQLQuery;
    QrBPI2CTRL_LAN: TIntegerField;
    QrBPI2SUB_LAN: TSmallintField;
    QrIncorr2: TmySQLQuery;
    QrbloqparcparAtivo: TSmallintField;
    QrbloqparcparVlrTotal: TFloatField;
    QrIncorr2Controle: TIntegerField;
    QrIncorr2Data: TDateField;
    QrIncorr2Descricao: TWideStringField;
    QrIncorr2Credito: TFloatField;
    QrIncorr2Tipo: TSmallintField;
    QrIncorr2Carteira: TIntegerField;
    QrIncorr2Sit: TIntegerField;
    QrIncorr2Vencimento: TDateField;
    QrIncorr2Compensado: TDateField;
    QrBPI2Descricao: TWideStringField;
    QrBPI2Genero: TIntegerField;
    QrBPI2VlrTotal: TFloatField;
    QrBPI2MultaVal: TFloatField;
    QrBPI2CTRL_REP: TIntegerField;
    QrBPI2Dias: TIntegerField;
    QrBPI2VlrOrigi: TFloatField;
    Query: TABSQuery;
    DataSource1: TDataSource;
    QrBPI2Fator: TFloatField;
    QueryOrdem: TIntegerField;
    QueryOrigi: TFloatField;
    QueryFator: TFloatField;
    QueryValDif: TFloatField;
    QueryValItem: TFloatField;
    QueryAtivo: TIntegerField;
    QrSum: TABSQuery;
    DataSource2: TDataSource;
    QueryDias: TFloatField;
    QueryRestaFat: TFloatField;
    QueryRestaDif: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnVisual: TPanel;
    PainelDados: TPanel;
    PainelData: TPanel;
    Panel7: TPanel;
    Label5: TLabel;
    Label17: TLabel;
    Label13: TLabel;
    Label18: TLabel;
    Label12: TLabel;
    Label19: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label11: TLabel;
    DBEdit1: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    DBMemo1: TDBMemo;
    DBEdCodigo: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit111: TDBEdit;
    RichEdit1: TRichEdit;
    PainelGrid: TPanel;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    DBGbloqparcpar: TDBGrid;
    Panel6: TPanel;
    BtEdita: TBitBtn;
    Panel9: TPanel;
    PageControl4: TPageControl;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    Panel15: TPanel;
    BtReconfReparc: TBitBtn;
    TabSheet9: TTabSheet;
    DBGrid4: TDBGrid;
    Panel10: TPanel;
    BtRatificaQuit: TBitBtn;
    TabSheet4: TTabSheet;
    Panel11: TPanel;
    GroupBox1: TGroupBox;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    PnRecorrige: TPanel;
    BtReCorrige: TBitBtn;
    Panel13: TPanel;
    DBGrid3: TDBGrid;
    PnAtz1: TPanel;
    LaAtz1: TLabel;
    PBAtz1: TProgressBar;
    TabSheet10: TTabSheet;
    DBGrid5: TDBGrid;
    Panel14: TPanel;
    DBGrid6: TDBGrid;
    TabSheet5: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    PnCalc: TPanel;
    PageControl3: TPageControl;
    TabSheet7: TTabSheet;
    MeLog: TMemo;
    TabSheet8: TTabSheet;
    PnQuebra: TPanel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Edit4: TEdit;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    dmkEdit4: TdmkEdit;
    dmkEdit5: TdmkEdit;
    CkManual: TCheckBox;
    dmkEdit6: TdmkEdit;
    dmkEdit10: TdmkEdit;
    dmkEdit11: TdmkEdit;
    PageControl5: TPageControl;
    TabSheet6: TTabSheet;
    Splitter1: TSplitter;
    GradeA: TStringGrid;
    Panel8: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    dmkEdit7: TdmkEdit;
    dmkEdit8: TdmkEdit;
    dmkEdit9: TdmkEdit;
    TabSheet11: TTabSheet;
    GradeB: TStringGrid;
    TabSheet12: TTabSheet;
    Panel1: TPanel;
    QrPropriet: TmySQLQuery;
    QrProprietCodigo: TIntegerField;
    QrProprietNOMEPROP: TWideStringField;
    DsPropriet: TDataSource;
    QrCondImov: TmySQLQuery;
    QrCondImovConta: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    DsCondImov: TDataSource;
    Panel16: TPanel;
    PnPesq1: TPanel;
    Label3: TLabel;
    EdEmpresa: TdmkEditCB;
    Label9: TLabel;
    EdPropriet: TdmkEditCB;
    CBPropriet: TdmkDBLookupComboBox;
    CBEmpresa: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCondImov: TdmkEditCB;
    CBCondImov: TdmkDBLookupComboBox;
    EdReparcel: TdmkEdit;
    Label14: TLabel;
    EdFatNum: TdmkEdit;
    Label15: TLabel;
    DBGPesq_Dados: TDBGrid;
    BtReabre: TBitBtn;
    QrPesq: TmySQLQuery;
    CkSoAbertos: TCheckBox;
    GBVencimento: TGroupBox;
    CkIniVct: TCheckBox;
    TPIniVct: TDateTimePicker;
    CkFimVct: TCheckBox;
    TPFimVct: TDateTimePicker;
    GBParcelamento: TGroupBox;
    CkIniPar: TCheckBox;
    TPIniPar: TDateTimePicker;
    CkFimPar: TCheckBox;
    TPFimPar: TDateTimePicker;
    QrPesqVencimento: TDateField;
    QrPesqLancto: TIntegerField;
    QrPesqBPP_VlrTotal: TFloatField;
    QrPesqBPP_VlrDesco: TFloatField;
    QrPesqNO_EMP: TWideStringField;
    QrPesqNO_PES: TWideStringField;
    QrPesqUnidade: TWideStringField;
    QrPesqCodigo: TAutoIncField;
    QrPesqloginID: TWideStringField;
    QrPesqautentica: TWideMemoField;
    QrPesqCodCliEsp: TIntegerField;
    QrPesqCodCliEnt: TIntegerField;
    QrPesqCodigoEnt: TIntegerField;
    QrPesqCodigoEsp: TIntegerField;
    QrPesqVlrOrigi: TFloatField;
    QrPesqVlrMulta: TFloatField;
    QrPesqVlrJuros: TFloatField;
    QrPesqVlrTotal: TFloatField;
    QrPesqDataP: TDateTimeField;
    QrPesqNovo: TSmallintField;
    QrPesqTxaJur: TFloatField;
    QrPesqTxaMul: TFloatField;
    DsPesq: TDataSource;
    QrPesqControle: TAutoIncField;
    QrPesqParcela: TIntegerField;
    QrPesqFatNum: TFloatField;
    DBGPesq_Titul: TDBGrid;
    frxParcelamento: TfrxReport;
    frxCondE2: TfrxReport;
    QrBoletos: TmySQLQuery;
    QrBoletosSUB_TOT: TFloatField;
    QrBoletosVencto: TDateField;
    QrBoletosBoleto: TFloatField;
    frxDsBoletos: TfrxDBDataset;
    QrBoletosUnidade: TWideStringField;
    PMImprime: TPopupMenu;
    Reparcelamento1: TMenuItem;
    Bloqueto1: TMenuItem;
    CkZerado: TCheckBox;
    CkAntigo: TCheckBox;
    CkDesign: TCheckBox;
    QrBoletosBLOQUETO: TFloatField;
    QrBoletosParcela: TIntegerField;
    QrLocLct: TmySQLQuery;
    QrLocLctGenero: TIntegerField;
    QrLocLctFatID: TIntegerField;
    QrLocLctAtrelado: TIntegerField;
    QrPesqL: TmySQLQuery;
    QrbloqparcparLancto: TIntegerField;
    QrPesqLControle: TIntegerField;
    QrPesqLData: TDateField;
    QrPesqLTipo: TSmallintField;
    QrPesqLCarteira: TIntegerField;
    QrPesqLSub: TSmallintField;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel17: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BtRatificaParc: TBitBtn;
    BtExclui: TBitBtn;
    PBAtz: TProgressBar;
    QrCNAB_Cfg_B: TmySQLQuery;
    QrCNAB_Cfg_BNOMEBANCO: TWideStringField;
    QrCNAB_Cfg_BLocalPag: TWideStringField;
    QrCNAB_Cfg_BNOMECED: TWideStringField;
    QrCNAB_Cfg_BEspecieDoc: TWideStringField;
    QrCNAB_Cfg_BACEITETIT_TXT: TWideStringField;
    QrCNAB_Cfg_BCART_IMP: TWideStringField;
    QrCNAB_Cfg_BEspecieVal: TWideStringField;
    QrCNAB_Cfg_BCedBanco: TIntegerField;
    QrCNAB_Cfg_BAgContaCed: TWideStringField;
    QrCNAB_Cfg_BCedAgencia: TIntegerField;
    QrCNAB_Cfg_BCedPosto: TIntegerField;
    QrCNAB_Cfg_BCedConta: TWideStringField;
    QrCNAB_Cfg_BCartNum: TWideStringField;
    QrCNAB_Cfg_BIDCobranca: TWideStringField;
    QrCNAB_Cfg_BCodEmprBco: TWideStringField;
    QrCNAB_Cfg_BTipoCobranca: TIntegerField;
    QrCNAB_Cfg_BCNAB: TIntegerField;
    QrCNAB_Cfg_BCtaCooper: TWideStringField;
    QrCNAB_Cfg_BModalCobr: TIntegerField;
    QrCNAB_Cfg_BMultaPerc: TFloatField;
    QrCNAB_Cfg_BJurosPerc: TFloatField;
    QrCNAB_Cfg_BTexto01: TWideStringField;
    QrCNAB_Cfg_BTexto02: TWideStringField;
    QrCNAB_Cfg_BTexto03: TWideStringField;
    QrCNAB_Cfg_BTexto04: TWideStringField;
    QrCNAB_Cfg_BTexto05: TWideStringField;
    QrCNAB_Cfg_BTexto06: TWideStringField;
    QrCNAB_Cfg_BTexto07: TWideStringField;
    QrCNAB_Cfg_BTexto08: TWideStringField;
    QrCNAB_Cfg_BCedDAC_C: TWideStringField;
    QrCNAB_Cfg_BCedDAC_A: TWideStringField;
    QrCNAB_Cfg_BOperCodi: TWideStringField;
    QrCNAB_Cfg_BLayoutRem: TWideStringField;
    QrCNAB_Cfg_BNOMESAC: TWideStringField;
    QrCNAB_Cfg_BCorreio: TWideStringField;
    QrCNAB_Cfg_BCartEmiss: TIntegerField;
    QrCNAB_Cfg_BCedente: TIntegerField;
    QrCNAB_Cfg_BCAR_TIPODOC: TIntegerField;
    QrCNAB_Cfg_BCART_ATIVO: TIntegerField;
    frxDsCNAB_Cfg_B: TfrxDBDataset;
    QrLoc: TmySQLQuery;
    QrbloqparcCond: TIntegerField;
    QrCNAB_Cfg_BNosNumFxaU: TFloatField;
    QrCNAB_Cfg_BNosNumFxaI: TFloatField;
    QrCNAB_Cfg_BNosNumFxaF: TFloatField;
    QrCNAB_Cfg_BCodigo: TFloatField;
    QrbloqparcCNAB_Cfg: TIntegerField;
    QrCNAB_Cfg_BDVB: TWideStringField;
    QrCNAB_Cfg_BNPrinBc: TWideStringField;
    QrCNAB_Cfg_BCorresCto: TWideStringField;
    QrCNAB_Cfg_BCorresAge: TIntegerField;
    QrCNAB_Cfg_BCorresBco: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrbloqparcAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrbloqparcBeforeOpen(DataSet: TDataSet);
    procedure QrbloqparcCalcFields(DataSet: TDataSet);
    procedure QrbloqparcAfterScroll(DataSet: TDataSet);
    procedure QrlctCalcFields(DataSet: TDataSet);
    procedure QrbloqparcBeforeClose(DataSet: TDataSet);
    procedure BtRatificaParcClick(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeBDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrbloqparcparBeforeClose(DataSet: TDataSet);
    procedure QrbloqparcparAfterScroll(DataSet: TDataSet);
    procedure BtEditaClick(Sender: TObject);
    procedure QrbloqparcparAfterOpen(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtRatificaQuitClick(Sender: TObject);
    procedure QrIncorrBeforeClose(DataSet: TDataSet);
    procedure QrIncorrAfterOpen(DataSet: TDataSet);
    procedure BtReCorrigeClick(Sender: TObject);
    procedure BtReconfReparcClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdProprietChange(Sender: TObject);
    procedure EdProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtReabreClick(Sender: TObject);
    procedure EdCondImovChange(Sender: TObject);
    procedure EdReparcelChange(Sender: TObject);
    procedure EdFatNumChange(Sender: TObject);
    procedure CkIniParClick(Sender: TObject);
    procedure DBGPesq_DadosDblClick(Sender: TObject);
    procedure DBGPesq_DadosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure Bloqueto1Click(Sender: TObject);
    procedure Reparcelamento1Click(Sender: TObject);
    procedure frxCondE2GetValue(const VarName: string; var Value: Variant);
    procedure SbNovoClick(Sender: TObject);
  private
    FDtEncer, FDtMorto: TDateTime;
    FTabLctA, FTabLctB, FTabLctD: String;
    //
    FItem(*, FOrdem*): Integer;
    FNossoNumero: String;
    FLocNomeConta, FSetTablConta, FSelDB: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    //procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenLctos(Controle: Integer);
    procedure Reopenbloqparcits(Conta: Integer);
    procedure ReopenBPP_Pgt(Controle: Integer);
    procedure ReopenPropriet(Todos: Boolean);
    procedure ReopenCondImov();
    procedure ReabrirPesq(Aviso: String);
    procedure SomasBloqParc();
    procedure DistribuiRestos();
    procedure DefineFrx(frx: TfrxReport);
    function  RatificaParcelamento(Definitivo: Boolean; FatorRat: Double): Double;
    function  QuebraLancamentos_1(Definitivo: Boolean; FatorRat: Double): Double;
    function  QuebraLancamentos_2(Definitivo: Boolean; FatorRat: Double): Double;
    function  InsereReparceLct(): Boolean;
    function  StatusBloqParcMenorQue(Status, Codigo: Integer): Boolean;
    function  StatusBloqParcAtual(Codigo: Integer): Integer;
    function  DefineDadosCNAB_Cfg(const Cond, RepCNAB_Cfg: Integer;
                var JurosPerc, MultaPerc: Double; var CartEmiss: Integer): Boolean;
    //
    {
    procedure CriaABSQuery();
    procedure InsABSQuery(Origi, Dias, Fator, ValDif, ValItem, RestaFat,
              RestaDif: Double);
    procedure ReabreQuery();
    }

  public
    { Public declarations }
    FCols: array [0..4] of Integer;
    FParcWEB: Boolean;
    function LocCod(Atual, Codigo: Integer): Boolean;
    procedure Reopenbloqparcpar(Controle: Integer);
    procedure DefineDatabaseTabelas();
  end;

var
  Fmbloqparc: TFmbloqparc;
const
  FFormatFloat = '00000';
  FMaxRatif = 8;

implementation

uses Module, WinSkinData, Principal, blocparcpar, MyDBCheck, BloqParcCorrige,
  UnMyObjects, ModuleGeral, ModuleCond, UnBancos, MyGlyfs, CondGerImpGer,
  ModuleBloq, ModuleAtencoes, bloqparcpesq, DmkDAC_PF, UnBloquetosCond;
  //uses Module, MyVCLSkin, Principal;


{$R *.DFM}

const
  FGeneroParcelamento = -10;
  FFatIDParcelamento = 610;

procedure TFmbloqparc.ReabrirPesq(Aviso: String);
var
  wTabLctA: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Aviso);
  PBAtz.Position := 0;
  Update;
  Application.ProcessMessages;
  wTabLctA := DmodG.ObtemLctA_Para_Web(FTabLctA);
  //
  QrPesqlan.Close;
  QrPesqlan.SQL.Clear;
  QrPesqlan.SQL.Add('SELECT lan.Data, lan.Controle, lan.Descricao,');
  QrPesqlan.SQL.Add('lan.Vencimento, lan.FatNum BLOQUETO,');
  QrPesqlan.SQL.Add('lan.Mez, lan.Credito, lan.Sub, lan.Reparcel,');
  QrPesqlan.SQL.Add('lan.Multa, lan.MoraDia');
  if QrPesqlan.Database = Dmod.MyDBn then
    QrPesqlan.SQL.Add('FROM ' + wTabLctA + ' lan')
  else
    QrPesqlan.SQL.Add('FROM ' + FTabLctA + ' lan');
  QrPesqlan.SQL.Add('WHERE lan.Reparcel=:P0');
  QrPesqlan.SQL.Add('ORDER BY Vencimento, Credito DESC');
  QrPesqlan.Params[0].AsInteger := QrbloqparcCodigo.Value;
  QrPesqlan.Open;
  //
  PBAtz.Max := QrPesqlan.RecordCount;
  Update;
  Application.ProcessMessages;
  //
end;

/////////////////////////////////////////////////////////////////////////////////////
function TFmbloqparc.LocCod(Atual, Codigo: Integer): Boolean;
begin
  DefParams;
  Result := GOTOy.LC(Atual, Codigo);
end;

procedure TFmbloqparc.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrbloqparcCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmbloqparc.DefParams;
begin
  VAR_GOTOTABELA := 'bloqparc';
  VAR_GOTOMYSQLTABLE := Qrbloqparc;
  if FParcWEB then
    VAR_GOTONEG := gotoPiZ
  else
    VAR_GOTONEG := gotoNiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := '';

  if FmPrincipal.FDatabase1A = nil then
  begin
    VAR_GOTOMySQLDBNAME  := FmPrincipal.FDatabase2A;
    VAR_GOTOMySQLDBNAME2 := FmPrincipal.FDatabase2A;
  end else
  begin
    VAR_GOTOMySQLDBNAME  := FmPrincipal.FDatabase1A;
    VAR_GOTOMySQLDBNAME2 := FmPrincipal.FDatabase1A;
  end;

  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := (*'WHERE'*)'CodCliEsp IN (' + #13#10 +
  '  SELECT CodCliInt' + #13#10 +
  '  FROM enticliint' + #13#10 +
  '  WHERE TipoTabLct=1' + #13#10 +
  ')';
  //
  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECOND,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP,');
  VAR_SQLx.Add('cim.Unidade, cim.Codigo Cond, blp.*');
  VAR_SQLx.Add('FROM bloqparc blp');
  VAR_SQLx.Add('LEFT JOIN condimov cim ON cim.Conta=blp.CodigoEsp');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=blp.CodigoEnt');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=blp.CodCliEnt');
  VAR_SQLx.Add('LEFT JOIN enticliint eci ON eci.CodEnti=cli.Codigo');
  VAR_SQLx.Add('WHERE eci.TipoTabLct=1');
  VAR_SQLx.Add('');
  //
  //
  VAR_SQL1.Add('AND blp.Codigo=:P0');
  //
  VAR_SQLa.Add('');
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

{
procedure TFmbloqparc.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;
}

procedure TFmbloqparc.CriaOForm;
begin
  DefineONomeDoForm;
end;

{
procedure TFmbloqparc.AlteraRegistro;
var
  bloqparc : Integer;
begin
  bloqparc := QrbloqparcCodigo.Value;
  if QrbloqparcCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(bloqparc, Dmod.MyDBn, 'bloqparc', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(bloqparc, Dmod.MyDBn, 'bloqparc', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;
}

{
procedure TFmbloqparc.IncluiRegistro;
var
  Cursor : TCursor;
  bloqparc : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    bloqparc := UMyMod.BuscaEmLivreY(Dmod.MyDBn, 'Livres', 'Controle',
    'bloqparc', 'bloqparc', 'Codigo');
    if Length(FormatFloat(FFormatFloat, bloqparc))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, bloqparc);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmbloqparc.QueryPrincipalAfterOpen;
begin
end;

procedure TFmbloqparc.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmbloqparc.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmbloqparc.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmbloqparc.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmbloqparc.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmbloqparc.BtReabreClick(Sender: TObject);
var
  CodCliEsp, CodigoEnt, CodigoEsp, Reparcel, Bloqueto: Integer;
  wTabLctA: String;
begin
  Screen.Cursor := crHourGlass;
  try
    CodCliEsp := Geral.IMV(EdEmpresa.Text);
    CodigoEnt := Geral.IMV(EdPropriet.Text);
    CodigoEsp := Geral.IMV(EdCondImov.Text);
    Reparcel  := Geral.IMV(EdReparcel.Text);
    Bloqueto  := Geral.IMV(EdFatNum.Text);
    wTabLctA  := DmodG.ObtemLctA_Para_Web(FTabLctA);
    //
    QrPesq.Close;
    QrPesq.SQL.Clear;
    QrPesq.SQL.Add('SELECT bpp.Vencimento, bpp.Lancto, bpp.VlrTotal');
    QrPesq.SQL.Add('BPP_VlrTotal, bpp.VlrDesco BPP_VlrDesco,');
    QrPesq.SQL.Add('bpp.Controle, bpp.Parcela, bpp.FatNum,');
    QrPesq.SQL.Add('IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NO_EMP,');
    QrPesq.SQL.Add('IF(pes.Tipo=0,pes.RazaoSocial,pes.Nome) NO_PES,');
    QrPesq.SQL.Add('imv.Unidade, bpc.*');
    QrPesq.SQL.Add('FROM bloqparcpar bpp');
    QrPesq.SQL.Add('LEFT JOIN bloqparc bpc ON bpc.Codigo=bpp.Codigo');
    QrPesq.SQL.Add('LEFT JOIN entidades emp ON emp.Codigo=bpc.CodCliEnt');
    QrPesq.SQL.Add('LEFT JOIN entidades pes ON pes.Codigo=bpc.CodigoEnt');
    QrPesq.SQL.Add('LEFT JOIN condimov imv ON imv.Conta=bpc.CodigoEsp');
    if CkSoAbertos.Checked then
    begin
      if QrPesq.Database = Dmod.MyDBn then
        QrPesq.SQL.Add('LEFT JOIN ' + wTabLctA + ' lan ON lan.SubPgto1=bpp.Controle')
      else
        QrPesq.SQL.Add('LEFT JOIN ' + FTabLctA + ' lan ON lan.SubPgto1=bpp.Controle');
      QrPesq.SQL.Add('WHERE lan.Controle IS NULL');
    end else
      QrPesq.SQL.Add('WHERE bpp.Codigo <> 0');
    //
    if CodCliEsp <> 0 then
      QrPesq.SQL.Add('AND bpc.CodCliEsp=' + FormatFloat('0', CodCliEsp));
    if CodigoEnt <> 0 then
      QrPesq.SQL.Add('AND bpc.CodigoEnt=' + FormatFloat('0', CodigoEnt));
    if CodigoEsp <> 0 then
      QrPesq.SQL.Add('AND bpc.CodigoEsp=' + FormatFloat('0', CodigoEsp));
    if Reparcel <> 0 then
      QrPesq.SQL.Add('AND bpp.Codigo=' + FormatFloat('0', Reparcel));
    if Bloqueto <> 0 then
      QrPesq.SQL.Add('AND bpp.FatNum=' + FormatFloat('0', Bloqueto));
    //
    QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND bpc.DataP ',
      TPIniPar.Date, TPFimPar.Date,
      CkIniPar.Checked and GBParcelamento.Visible,
      CkFimPar.Checked and GBParcelamento.Visible));
    //
    QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND bpp.Vencimento ',
      TPIniVct.Date, TPFimVct.Date,
      CkIniVct.Checked and GBVencimento.Visible,
      CkFimVct.Checked and GBVencimento.Visible));
    //
    //QrPesq.SQL.Add('');
    QrPesq.SQL.Add('ORDER BY NO_EMP, NO_PES, Vencimento');
    //
    UMyMod.AbreQuery(QrPesq, Dmod.MyDB, 'TFmbloqparc.BtReabreClick()');
    //
    PnPesq1.Visible := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmbloqparc.BtReconfReparcClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Qrbloqparcits.First;
  while not Qrbloqparcits.Eof do
  begin
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Reparcel'], ['Controle'], [QrbloqparcCodigo.Value],
      [QrbloqparcitsCtrlOrigi.Value], True, '', FTabLctA);
    //
    Qrbloqparcits.Next;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmbloqparc.BtReCorrigeClick(Sender: TObject);
{###
var
  TotalI, TotalO, ValItem, TotalF, Fator2,
  RestaFat, RestaDif, AtuFat, ValDif2: Double;
  Carteira, TipoCart, SubPgto1, CtrlLoc: Integer;
  ValTxt, DataP_Txt: String;
  DataPgto: TDateTime;
  //
  Compensado: String;
  Sit, Reparcel: Integer;
}
begin
  ShowMessage('Em an�lise! Solicite � DERMATEK a implementa��o!');
{###
  PBAtz1.Position := 0;
  CtrlLoc := QrBPP_PgtControle.Value;
  LaAtz1.Caption := 'Aguarde...   Atualizando....';
  Update;
  Application.ProcessMessages;
  FOrdem := 0;
  //
  SubPgto1 := QrbloqparcparControle.Value;
  if SubPgto1 = 0 then
  begin
    Geral.MensagemBox('Recorre��o cancelada! "SubPgto1" n�o definido!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if QrbloqparcparLancto.Value = 0 then
  begin
    Geral.MensagemBox('Recorre��o cancelada! A parcela n�o possui ' +
    'lan�amento! Provavelmente porque pode n�o ter sido ratificada ainda!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT SUM(lan.Credito-lan.Debito) Valor');
  Dmod.QrAux.SQL.Add('FROM ' + VAR LCT + ' lan');
  Dmod.QrAux.SQL.Add('WHERE lan.SubPgto1=:P0');
  Dmod.QrAux.Params[0].AsInteger := SubPgto1;
  Dmod.QrAux.Open;
  //
  if DBCheck.CriaFm(TFmBloqParcCorrige, FmBloqParcCorrige, afmoNegarComAviso) then
  begin
    FmBloqParcCorrige.QrCarteiras.Close;
    FmBloqParcCorrige.QrCarteiras.Params[0].AsInteger := QrbloqparcCodCliEnt.Value;
    FmBloqParcCorrige.QrCarteiras.Open;
    FmBloqParcCorrige.EdValor.ValueVariant := Dmod.QrAux.FieldByName('Valor').AsFloat;
    FmBloqParcCorrige.TPDataP.Date := QrBPP_PgtData.Value;
    FmBloqParcCorrige.ShowModal;
    if FmBloqParcCorrige.FConfr then
    begin
      ValTxt    := Geral.FFT(FmBloqParcCorrige.EdValor.ValueVariant, 2, siNegativo);
      DataP_Txt := Geral.FDT(FmBloqParcCorrige.TPDataP.Date, 1);
      Carteira  := FmBloqParcCorrige.EdCarteira.ValueVariant;
      TipoCart  := FmBloqParcCorrige.QrCarteirasTipo.Value;
    end else Exit;
    FmBloqParcCorrige.Destroy;
  end else Exit;
  //
  TotalI := Geral.DMV(ValTxt);
  if TotalI <=0 then
  begin
    Geral.MensagemBox('Valor informado inv�lido: ' + ValTxt),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  LaAtz1.Caption := 'Quitando lan�amentos originais...';
  Update;
  Application.ProcessMessages;

  Onde abre o QrIncorr2? 
SELECT Controle, Data, Descricao, Credito, Tipo,
Carteira, Sit, Vencimento, Compensado
FROM lct
WHERE Genero=-10
AND ID_Pgto=:P0

  if QrIncorr2Compensado.Value > 0 then
    DataPgto := QrIncorr2Compensado.Value
  else
    DataPgto := QrbloqparcparVencimento.Value;
  //
  Compensado := Geral.FDT(DataPgto, 1);
  Sit        := 3;
  Reparcel   := QrbloqparcCodigo.Value;
  if Reparcel > 0 then
  begin
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Compensado', 'Sit'], ['Reparcel'], [
    Compensado, Sit], [Reparcel], true, '', FTabLctA);
  end;
  LaAtz1.Caption := 'Excluindo lan�amentos atuais...';
  Update;
  Application.ProcessMessages;
  PnAtz1.Visible := True;
  Update;
  Application.processMessages;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(EXCLUI_DE + VAR LCT + ' WHERE SubPgto1 > 0 AND SubPgto1=:P0');
  Dmod.QrUpd.Params[0].AsInteger := SubPgto1;
  Dmod.QrUpd.ExecSQL;
  //
  QrBPP_Pgt.First;
  while not QrBPP_Pgt.Eof do
  begin
    Dmod.MyDB.Execute('INSERT INTO livres SET Tabela="' + VAR LCT + '", Codigo=' +
    FormatFloat('0', QrBPP_PgtControle.Value));
    QrBPP_Pgt.Next;
  end;

  LaAtz1.Caption := 'Abrindo tabelas com os dados da parcela atual...';
  Update;
  Application.ProcessMessages;
  //
  QrIncorr.Close;
SELECT Controle, Data, Descricao, Credito, Tipo, Carteira, Sit
FROM lct
WHERE Genero=-10
AND ID_Pgto=:P0

  QrIncorr.Params[00].AsInteger := QrBloqParcParLancto.Value;
  QrIncorr.Open;
  //
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT SUM(VlrOrigi* Dias) Fator, ');
  Dmod.QrAux.SQL.Add('SUM(VlrOrigi) VlrOrigi, SUM(Dias) Dias');
  Dmod.QrAux.SQL.Add('FROM bloqparcits');
  Dmod.QrAux.SQL.Add('WHERE Controle=:P0');
  Dmod.QrAux.Params[0].AsInteger := QrbloqparcparControle.Value;
  Dmod.QrAux.Open;
  TotalF := Dmod.QrAux.FieldByName('Fator').AsFloat;
  TotalO := Dmod.QrAux.FieldByName('VlrOrigi').AsFloat;
  //TotalD := Trunc(Dmod.QrAux.FieldByName('Dias').AsFloat);
  //RestaD := TotalD;
  //
  //RestaDif := TotalI - TotalO;
  RestaFat := TotalF;
  //
  QrBPI2.Close;
SELECT lan.Data, lan.Controle CTRL_LAN, lan.Descricao, lan.Genero,
lan.Vencimento, lan.Credito, lan.Mez, lan.FatNum, lan.FatID,
lan.CliInt, lan.Cliente, lan.ForneceI, lan.Multa, lan.Depto,
lan.MultaVal, lan.MoraVal, lan.Sub SUB_LAN,
bpi.VlrAjust, bpi.Controle CTRL_REP,
bpi.VlrOrigi, bpi.VlrMulta, bpi.VlrJuros, bpi.VlrTotal,
bpi.Conta CONTA_REP, bpi.Dias, bpi.Dias * bpi.VlrOrigi Fator
FROM bloqparcits bpi
LEFT JOIN lct lan ON lan.Controle=bpi.CtrlOrigi
WHERE bpi.Controle=:P0
ORDER BY Fator
  QrBPI2.Params[0].AsInteger := QrbloqparcparControle.Value;
  QrBPI2.Open;
  PBAtz1.Max := QrBPI2.RecordCount;

  // tirar ID_Pgto de tarifas de quita��o para que n�o seja somado a quita��o indevidamente
  LaAtz1.Caption := 'Desvinculando lan�amentos estranhos � quita��o ...';
  Update;
  Application.ProcessMessages;
  //
  UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'ID_Pgto'], ['ID_Pgto'], [0], [QrbloqparcparLancto.Value], True, '', FTabLctA);
  //

  LaAtz1.Caption := 'Criando lan�amentos novos ...';
  Update;
  Application.ProcessMessages;
  //
  RestaDif := TotalI - TotalO;
  CriaABSQuery;
  QrBPI2.First;
  while not QrBPI2.Eof do
  begin
    PBAtz1.Position := PBAtz1.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    //ValDif2 := 0;
    AtuFat   := QrBPI2Dias.Value * QrBPI2VlrOrigi.Value;
    if RestaFat = 0 then Fator2 := 0 else
      Fator2 := AtuFat / RestaFat;
    ValDif2  := Round(Fator2 * RestaDif * 100) / 100;
    //
    RestaFat := RestaFat - AtuFat;
    RestaDif := RestaDif - ValDif2;
    //
    ValItem := QrBPI2VlrOrigi.Value + ValDif2;
    InsABSQuery(QrBPI2VlrOrigi.Value, QrBPI2Dias.Value, ValItem,
                RestaFat, RestaDif, Fator2, ValDif2);
    UFinanceiro.LancamentoDuplica(Dmod.QrAux, QrBPI2CTRL_LAN.Value, QrBPI2Sub_LAN.Value, FTabLcta);
    FLAN_Tipo        := TipoCart;
    FLAN_Carteira    := Carteira;
    FLAN_Sit         := 2;
    FLAN_Data        := DataP_Txt;
    FLAN_Vencimento  := Geral.FDT(QrBloqparcparVencimento.Value, 1);
    FLAN_Compensado  := DataP_Txt;

    FLAN_Genero      := QrBPI2Genero.Value;
    FLAN_Descricao   := '[Reparc] ' + QrBPI2Descricao.Value;
    FLAN_Credito     := ValItem;
    FLAN_MultaVal    := QrBPI2MultaVal.Value;
    FLAN_MoraVal     := ValDif2 - QrBPI2MultaVal.Value;
    FLAN_SubPgto1    := QrBPI2CTRL_REP.Value;
    // 11/06/2009
    FLAN_FatID       := FFatIDParcelamento;
    FLAN_FatNum      := QrbloqparcparFatNum.Value;
    FLAN_ID_Pgto     := QrbloqparcparLancto.Value;
    // fim 11/06/2009
    FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      VAR LCT, VAR LCT, 'Controle');
    if UFinanceiro.InsereLancamento(FTabLctA) then ;
    //
    QrBPI2.Next;
  end;
  //

  LaAtz1.Caption := 'Quitando bloqueto da parcela ...';
  Update;
  Application.ProcessMessages;
  //
  if UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Sit', 'Compesado'], ['Controle'], [
    3, DataP_Txt], [QrbloqparcparLancto.Value], True, '', FTabLctA) then
  //

  //
  ReabreQuery;
  ReopenBPP_Pgt(CtrlLoc);
  PageControl4.ActivePageIndex := 3;
  //
  LaAtz1.Caption := 'Recorre��o finalizada!!!';
  Update;
  PBAtz1.Position := 0;
  Application.ProcessMessages;
  //
  Screen.Cursor := crDefault;
}
end;

procedure TFmbloqparc.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrbloqparcCodigo.Value;
  Close;
end;

procedure TFmbloqparc.Bloqueto1Click(Sender: TObject);
  procedure IncluiIntegerAoArrStr(const Inteiro: Integer; var ArrStr: String);
  var
    Txt: String;
  begin
    Txt := FormatFloat('0', Inteiro);
    if Length(ArrStr) <> 0 then
      Txt := ',' + Txt;
    ArrStr := ArrStr + Txt;
  end;
var
  q: TSelType;
  n: Integer;
  Txt, Bloqs: String;
begin
  DefineFrx(frxCondE2);
  //
  DmBloq.ReopenEnderecoInquilino(QrbloqparcCodigoEsp.Value); // Apto (Conta)
  DmCond.ReopenQrCond(QrbloqparcCodCliEsp.Value);
  //
  QrBoletos.Close;
  QrBoletos.SQL.Clear;
  QrBoletos.SQL.Add('SELECT ValBol SUB_TOT, Vencimento Vencto, ');
  QrBoletos.SQL.Add('FatNum Boleto, Parcela');
  QrBoletos.SQL.Add('FROM bloqparcpar');
  if not DBCheck.Quais_Selecionou(Qrbloqparcpar, DBGbloqparcpar, q) then Exit;
  Bloqs := '';
  //
  case q of
    istAtual: Txt := 'WHERE Controle=' + FormatFloat('0', QrbloqparcparControle.Value);
    istSelecionados:
    begin
      with DBGbloqparcpar.DataSource.DataSet do
      for n := 0 to DBGbloqparcpar.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGbloqparcpar.SelectedRows.Items[n]));
        IncluiIntegerAoArrStr(QrbloqparcparControle.Value, Bloqs);
      end;
      Txt := 'WHERE Controle IN (' + Bloqs + ')';
    end;
    istTodos: Txt := 'WHERE Codigo=' + FormatFloat('0', QrbloqparcCodigo.Value);
    else Exit;
  end;
  QrBoletos.SQL.Add(Txt);
  QrBoletos.SQL.Add('ORDER BY Vencimento');
  QrBoletos.Open;
  //
  MyObjects.frxMostra(frxCondE2, 'Bloqueto(s) reparcelamento');
end;

procedure TFmbloqparc.BtConfirmaClick(Sender: TObject);
{
var
  Codigo : Integer;
  Nome : String;
}
begin
{#
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdN.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdN.SQL.Add('INSERT INTO bloqparc SET ')
  else Dmod.QrUpdN.SQL.Add('UPDATE bloqparc SET ');
  Dmod.QrUpdN.SQL.Add('Nome=:P0, ID=:P1, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdN.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc')
  else Dmod.QrUpdN.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc');
  //
  Dmod.QrUpdN.Params[00].AsString  := Nome;
  Dmod.QrUpdN.Params[01].AsString  := EdID.Text;
  //
  Dmod.QrUpdN.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdN.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpdN.Params[04].AsInteger := Codigo;
  Dmod.QrUpdN.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'bloqparc', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
  #}
end;

procedure TFmbloqparc.BtDesisteClick(Sender: TObject);
{
var
  Codigo : Integer;
}
begin
{#
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDBn, 'Livres', 'bloqparc', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'bloqparc', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'bloqparc', 'Codigo');
  #}
end;

procedure TFmbloqparc.SbNumeroClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  LaRegistro.Caption := GOTOy.Codigo(QrbloqparcCodigo.Value,LaRegistro.Caption);
end;

procedure TFmbloqparc.SbNomeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmbloqparc.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmbloqparc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmbloqparc.QrbloqparcAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmbloqparc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmbloqparc.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrbloqparcCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'bloqparc', Dmod.MyDBn, CO_VAZIO));
}
  PageControl1.ActivePageIndex := 0;
  //
  if DBCheck.CriaFm(TFmbloqparcpesq, Fmbloqparcpesq, afmoNegarComAviso) then
  begin
    Fmbloqparcpesq.ShowModal;
    Fmbloqparcpesq.Destroy;
  end;
end;

procedure TFmbloqparc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmbloqparc.frxCondE2GetValue(const VarName: string;
  var Value: Variant);
var
  DVB, NossoNumero_Rem(*, LocalEData, UH*): String;
  Banco: Integer;
  //ModelBloq: Integer;
begin
  if AnsiCompareText(VarName, 'VARF_PARCELAS') = 0 then
    Value := Qrbloqparcpar.RecordCount

  // In�cio da Ficha de compensa��o
  else
  if AnsiCompareText(VarName, 'VARF_AGCodCed') = 0 then
    Value := QrCNAB_Cfg_B.FieldByName('AgContaCed').AsString
  else if AnsiCompareText(VarName, 'VARF_NossoNumero') = 0 then
  begin
    UBancos.GeraNossoNumero(
      QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
      QrBoletosBLOQUETO.Value,
      QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
      QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
      QrBoletosVencto.Value,
      QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
      QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
      QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
      QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString,
      FNossoNumero, NossoNumero_Rem);
    Value := FNossoNumero;
  end else if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
  begin
    UBancos.GeraNossoNumero(
      QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
      QrBoletosBLOQUETO.Value,
      QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
      QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
      QrBoletosVencto.Value,
      QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
      QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
      QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
      QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString,
      FNossoNumero, NossoNumero_Rem);
    //
    Value := UBancos.CodigoDeBarra_BoletoDeCobranca(
               QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
               QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
               QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger,
               QrCNAB_Cfg_B.FieldByName('CorresAge').AsInteger,
               QrCNAB_Cfg_B.FieldByName('CedDAC_A').AsString,
               QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
               QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
               QrCNAB_Cfg_B.FieldByName('CedDAC_C').AsString,
               QrCNAB_Cfg_B.FieldByName('CorresCto').AsString,
               9, 3, 1, FNossoNumero,
               QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString,
               QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
               QrCNAB_Cfg_B.FieldByName('CART_IMP').AsString,
               QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
               QrCNAB_Cfg_B.FieldByName('OperCodi').AsString,
               QrBoletosVencto.Value, QrBoletosSUB_TOT.Value,
               0, 0, not CkZerado.Checked,
               QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
               QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString)
  end else if AnsiCompareText(VarName, 'VAX') = 0 then
  begin
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015) then
    begin
      Banco := QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger;
      //
      DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end else
    begin
      Banco := QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger;
      //
      if QrCNAB_Cfg_B.FieldByName('DVB').AsString <> '?' then
        DVB :=  QrCNAB_Cfg_B.FieldByName('DVB').AsString
      else
        DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end;
    Value := FormatFloat('000', Banco) + '-' + DVB;
  end
  else if AnsiCompareText(VarName, 'VARF_LINHADIGITAVEL') = 0 then
  begin
    UBancos.GeraNossoNumero(
      QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
      QrBoletosBLOQUETO.Value,
      QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
      QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
      QrBoletosVencto.Value,
      QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
      QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
      QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
      QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString,
      FNossoNumero, NossoNumero_Rem);
    //
    Value := '';
    Value := UBancos.LinhaDigitavel_BoletoDeCobranca(
               UBancos.CodigoDeBarra_BoletoDeCobranca(
                 QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('CorresAge').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('CedDAC_A').AsString,
                 QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
                 QrCNAB_Cfg_B.FieldByName('CedDAC_C').AsString,
                 QrCNAB_Cfg_B.FieldByName('CorresCto').AsString,
                 9, 3, 1, FNossoNumero,
                 QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString,
                 QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
                 QrCNAB_Cfg_B.FieldByName('CART_IMP').AsString,
                 QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
                 QrCNAB_Cfg_B.FieldByName('OperCodi').AsString,
                 QrBoletosVencto.Value, QrBoletosSUB_TOT.Value,
                 0, 0, not CkZerado.Checked,
                 QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString))
  end else if AnsiCompareText(VarName, 'LogoBancoExiste') = 0 then
  begin
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      Value := FmMyGlyfs.LogoBancoExiste(QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.LogoBancoExiste(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger);
  end
  else if AnsiCompareText(VarName, 'LogoBancoPath') = 0 then
  begin
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      Value := FmMyGlyfs.CaminhoLogoBanco(QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.CaminhoLogoBanco(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger);
  end
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO1') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto01').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO2') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto02').AsString,
      QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO3') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto03').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO4') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto04').AsString,
      QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO5') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto05').AsString,
      QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO6') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto06').AsString,
      QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO7') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto07').AsString,
      QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO8') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto08').AsString,
      QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, DmBloq.QrBoletosSUB_TOT.Value)
  // Fim Ficha de compensa��o
end;

procedure TFmbloqparc.QrbloqparcBeforeOpen(DataSet: TDataSet);
begin
  QrbloqparcCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmbloqparc.QrbloqparcCalcFields(DataSet: TDataSet);
begin
  if QrbloqparcCodigo.Value > 0 then
  begin
    case QrbloqparcNovo.Value of
      -9: QrbloqparcSTATUS.Value := 'CANCELADO';
      1: QrbloqparcSTATUS.Value := '[1] Novo parcelmento web identificado';
      2: QrbloqparcSTATUS.Value := '[2] Download do reparcelamento conclu�do';
      3: QrbloqparcSTATUS.Value := '[3] Cria��o de novos lan�amentos conclu�do';
      4: QrbloqparcSTATUS.Value := '[4] Lan�amentos reparcelados marcados.';
      5: QrbloqparcSTATUS.Value := '[5] Lan�amentos atrelados criados';
      6: QrbloqparcSTATUS.Value := '[6] Restos distribu�dos pelos lan�amentos';
      7: QrbloqparcSTATUS.Value := '[7] Upload de ratifica��o realizado';
      8: QrbloqparcSTATUS.Value := '[8] Somas dos totais';
      9: QrbloqparcSTATUS.Value := 'RATIFICADO';
      else QrbloqparcSTATUS.Value := '[?] Lan�amentos atrelados';
    end;
  end else begin
    case QrbloqparcNovo.Value of
      -9: QrbloqparcSTATUS.Value := 'CANCELADO';
      1: QrbloqparcSTATUS.Value := '[1] Novo parcelmento local identificado';
      2: QrbloqparcSTATUS.Value := '[2] Upload do reparcelamento conclu�do';
      3: QrbloqparcSTATUS.Value := '[3] Cria��o de novos lan�amentos conclu�do';
      4: QrbloqparcSTATUS.Value := '[4] Lan�amentos reparcelados marcados.';
      5: QrbloqparcSTATUS.Value := '[5] Lan�amentos atrelados criados';
      6: QrbloqparcSTATUS.Value := '[6] Restos distribu�dos pelos lan�amentos';
      7: QrbloqparcSTATUS.Value := '[7] Upload de ratifica��o realizado';
      8: QrbloqparcSTATUS.Value := '[8] Somas dos totais';
      9: QrbloqparcSTATUS.Value := 'RATIFICADO';
      else QrbloqparcSTATUS.Value := '[?] Lan�amentos atrelados';
    end;
  end;
  case QrbloqparcNovo.Value of
    1..4: DBEdit5.Font.Color := clRed;
    5: DBEdit5.Font.Color := $004080FF; // Laranja
    6..9: DBEdit5.Font.Color := clBlue;
    else DBEdit5.Font.Color := clFuchsia;
  end;
end;

procedure TFmbloqparc.QrbloqparcAfterScroll(DataSet: TDataSet);
//var
  //Ponto: String;
begin
  {
  if FSelDB <> '' then
    Ponto := '.'
  else
    Ponto := '';
  }
  DModG.Def_EM_ABD(FSelDB, QrbloqparcCodCliEnt.Value, QrbloqparcCodCliEsp.Value,
    FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  //
  DmBloq.ReopenCNAB_Cfg_Cond(QrCNAB_Cfg_B, Dmod.MyDB, QrbloqparcCodCliEsp.Value, QrbloqparcCNAB_Cfg.Value);
  //
  ReopenLctos(0);
  Reopenbloqparcpar(0);
  BtRatificaParc.Visible :=
    ((Qrbloqparc.Database = FmPrincipal.FDatabase2A) and (QrbloqparcCodigo.Value > 0))
    or
    ((Qrbloqparc.Database = FmPrincipal.FDatabase1A) and (QrbloqparcCodigo.Value < 0));
  //
  BtRatificaParc.Enabled := QrbloqparcNovo.Value < FMaxRatif;
  PnAtz1.Visible         := False;
end;

procedure TFmbloqparc.ReopenLctos(Controle: Integer);
var
  wTabLctA: String;
begin
  wTabLctA := DmodG.ObtemLctA_Para_Web(FTabLctA);
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT lct.Data, lct.Controle, lct.Descricao,');
  QrLct.SQL.Add('lct.Vencimento, lct.FatNum BLOQUETO,');
  QrLct.SQL.Add('lct.Mez, lct.Credito, lct.Sub, lct.Reparcel,');
  QrLct.SQL.Add('lct.Multa, lct.MoraDia');
  if QrLct.Database = Dmod.MyDBn then
    QrLct.SQL.Add('FROM ' + wTabLctA + ' lct')
  else
    QrLct.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrLct.SQL.Add('WHERE lct.Reparcel=:P0');
  QrLct.SQL.Add('ORDER BY Vencimento, Credito DESC');
  QrLct.Params[0].AsInteger := QrbloqparcCodigo.Value;
  QrLct.Open;
  //
  QrLct.Locate('Controle', Controle, []);
end;

procedure TFmbloqparc.ReopenPropriet(Todos: Boolean);
var
  Cond: Integer;
begin
  EdPropriet.Text := '';
  CBPropriet.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  QrPropriet.Close;
  QrPropriet.SQL.Clear;
  QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
  QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
  QrPropriet.SQL.Add('FROM entidades ent');
  QrPropriet.SQL.Add('LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet');
  QrPropriet.SQL.Add('WHERE ent.Cliente2="V"');
  QrPropriet.SQL.Add('');
  if (Cond <> 0) and not Todos then
    QrPropriet.SQL.Add('AND cim.Codigo=' + FormatFloat('0', Cond));
  QrPropriet.SQL.Add('');
  QrPropriet.SQL.Add('ORDER BY NOMEPROP');
  QrPropriet.Open;
end;

procedure TFmbloqparc.Reparcelamento1Click(Sender: TObject);
var
  Cod_TXT: String;
begin
  DefineFrx(frxParcelamento);
  //
  // Precisa abrir antes por causa do lookup
  QrBloqorig.Close;
  QrBloqorig.Params[0].AsInteger := QrbloqparcCodigo.Value;
  QrBloqorig.Open;
  //
  Cod_TXT := FormatFloat('0', QrbloqparcCodigo.Value);
  QrBloqlan.Close;
(*
SELECT FatNum, SUM(Credito) Credito, Vencimento
FROM lct
WHERE Reparcel=:P0
GROUP BY FatNum
ORDER BY Vencimento
*)
  QrBloqlan.SQL.Clear;
  QrBloqlan.SQl.Add('DROP TABLE IF EXISTS _BLQ_REPAR_001_BLOQLAN_;');
  QrBloqlan.SQl.Add('');
  QrBloqlan.SQl.Add('CREATE TABLE _BLQ_REPAR_001_BLOQLAN_');
  QrBloqlan.SQl.Add('');
  QrBloqlan.SQl.Add('SELECT FatNum, SUM(Credito) Credito, Vencimento');
  QrBloqlan.SQl.Add('FROM ' + FTabLctA);
  QrBloqlan.SQl.Add('WHERE Reparcel=' + Cod_TXT);
  QrBloqlan.SQl.Add('GROUP BY FatNum');
  QrBloqlan.SQl.Add('UNION');
  QrBloqlan.SQl.Add('SELECT FatNum, SUM(Credito) Credito, Vencimento');
  QrBloqlan.SQl.Add('FROM ' + FTabLctB);
  QrBloqlan.SQl.Add('WHERE Reparcel=' + Cod_TXT);
  QrBloqlan.SQl.Add('GROUP BY FatNum');
  QrBloqlan.SQl.Add('UNION');
  QrBloqlan.SQl.Add('SELECT FatNum, SUM(Credito) Credito, Vencimento');
  QrBloqlan.SQl.Add('FROM ' + FTabLctD);
  QrBloqlan.SQl.Add('WHERE Reparcel=' + Cod_TXT);
  QrBloqlan.SQl.Add('GROUP BY FatNum');
  QrBloqlan.SQl.Add(';');
  QrBloqlan.SQl.Add('');
  QrBloqlan.SQl.Add('SELECT FatNum, SUM(Credito) Credito, Vencimento');
  QrBloqlan.SQl.Add('FROM _BLQ_REPAR_001_BLOQLAN_');
  QrBloqlan.SQl.Add('GROUP BY FatNum');
  QrBloqlan.SQl.Add('ORDER BY Vencimento;');
  QrBloqlan.SQl.Add('');
  QrBloqlan.SQl.Add('DROP TABLE IF EXISTS _BLQ_REPAR_001_BLOQLAN_;');
  QrBloqlan.Open;
  //
  QrBPI.Close;
  QrBPI.Params[0].AsInteger := QrbloqparcCodigo.Value;
  QrBPI.Open;
  //
  MyObjects.frxMostra(frxParcelamento, 'Parcelamento de pend�ncias');
  QrBPI.Close;
end;

procedure TFmbloqparc.QrlctCalcFields(DataSet: TDataSet);
begin
  if QrLctMez.Value > 0 then QrLctMES_TXT.Value :=
    MLAGeral.MezToFDT(QrLctMez.Value, 0, 104)
  else QrLctMES_TXT.Value := '';
end;

procedure TFmbloqparc.Reopenbloqparcpar(Controle: Integer);
begin
  Qrbloqparcpar.Close;
  Qrbloqparcpar.Params[0].AsInteger := QrbloqparcCodigo.Value;
  Qrbloqparcpar.Open;
  //
  Qrbloqparcpar.Locate('Controle', Controle, []);
end;

procedure TFmbloqparc.QrbloqparcBeforeClose(DataSet: TDataSet);
begin
  DModG.Def_EM_ABD_Limpa(FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  QrLct.Close;
  Qrbloqparcpar.Close;
  QrCNAB_Cfg_B.Close;
  //
  BtRatificaParc.Enabled := False;
end;

procedure TFmbloqparc.BtRatificaParcClick(Sender: TObject);
begin
  RatificaParcelamento(False, 0);
  LocCod(QrbloqparcCodigo.Value, QrbloqparcCodigo.Value);
end;

function TFmbloqparc.RatificaParcelamento(Definitivo: Boolean; FatorRat: Double): Double;
  function Atualizabloqparc(bloqparc, Status: Integer): Boolean;
  begin
    //
    Dmod.QrWeb.Close;
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('UPDATE bloqparc SET Novo=:P0');
    Dmod.QrWeb.SQL.Add('WHERE Codigo=:P1');
    //
    Dmod.QrWeb.Params[00].AsInteger := Status;
    Dmod.QrWeb.Params[01].AsInteger := bloqparc;
    Dmod.QrWeb.ExecSQL;
    //
    Dmod.QrUpdZ.Close;
    Dmod.QrUpdZ.SQL.Clear;
    Dmod.QrUpdZ.SQL.Add('UPDATE bloqparc SET Novo=:P0');
    Dmod.QrUpdZ.SQL.Add('WHERE Codigo=:P1');
    //
    Dmod.QrUpdZ.Params[00].AsInteger := Status;
    Dmod.QrUpdZ.Params[01].AsInteger := bloqparc;
    Dmod.QrUpdZ.ExecSQL;
    //
    Result := True;
  end;
var
  FatorRat1: Double;
  DBSource, DBDest: TmySQLDatabase;
begin
  Result := 0;
  if Qrbloqparc.RecordCount <> 1 then
  begin
    Geral.MensagemBox('A quantidade de registros da tabela ' +
    '"bloqparc" deve ser igual a 1!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Result := 0;
  if Qrbloqparcpar.RecordCount = 0 then Exit;
  // 2010-04-22
  if QrbloqparcCodigo.Value < 0 then
  begin
    DBSource := Dmod.MyDB;
    DBDest   := Dmod.MyDBn;
  end else begin
    DBSource := Dmod.MyDBn;
    DBDest   := Dmod.MyDB;
  end;
  // fim 2010-04-22
  //PnAtz.Visible := True;
  Qrbloqparcpar.First;
  // desabilitado em 2010-04-22
  //PageControl1.ActivePageIndex := 2;
  MeLog.Lines.Clear;
  MeLog.Lines.Add('Seq - tip -  % ini  -  % fim  - $ origi   - $ Multa   ' +
  '- $ Juros   - $ Total   -  Ctrl  - Bloq.  - C�d - Parcel ' +
  '- $ ParcA   - $ ParcD   - Dias - ');
  if StatusBloqParcMenorQue(9, QrbloqparcCodigo.Value) then
  //if QrbloqparcNovo.Value < 9 then
  begin
    Screen.Cursor := crHourGlass;
    //
    //  Download do novo parcelamento
    if StatusBloqParcMenorQue(2, QrbloqparcCodigo.Value) then
    //if QrbloqparcNovo.Value < 2 then
    begin
      if not UMyMod.EspelhaRegistroEntreTabelas(RichEdit1,
        'bloqparc', 'bloqparc', DBSource, DBDest,
        'WHERE Codigo='+FormatFloat('0', QrbloqparcCodigo.Value)) then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
      //

      if not UMyMod.EspelhaRegistroEntreTabelas(RichEdit1,
        'bloqparcpar', 'bloqparcpar', DBSource, DBDest,
        'WHERE Codigo='+FormatFloat('0', QrbloqparcCodigo.Value)) then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
      //
      Atualizabloqparc(QrbloqparcCodigo.Value, 2);
    end;
    //

    //  Cria��o de lan�amentos do reparcelamento
    if StatusBloqParcMenorQue(3, QrbloqparcCodigo.Value) then
    //if QrbloqparcNovo.Value < 3 then
    begin
      if InsereReparceLct() then
        Atualizabloqparc(QrbloqparcCodigo.Value, 3);
    end;
    //

    //  Tornar os lan�amentos que foram reparcelados em reparcelados
    if StatusBloqParcMenorQue(4, QrbloqparcCodigo.Value) then
    //if QrbloqparcNovo.Value < 4 then
    begin
      ReabrirPesq('Atualizando lan�amentos dos bloquetos reparcelados...');
      //
      QrPesqlan.First;
      while not QrPesqlan.Eof do
      begin
        PBAtz.Position := PBAtz.Position + 1;
        Update;
        Application.ProcessMessages;
        //
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Reparcel'], ['Controle', 'Sub'], [QrPesqlanReparcel.Value], [
          QrPesqlanControle.Value, QrPesqlanSub.Value], True, '', FTabLctA);
        //
        RichEdit1.Text :=
          'Atz do controle ' + FormatFloat('0', QrPesqlanControle.Value)+
          ' para o reparcelamento ' + FormatFloat('0', QrPesqlanReparcel.Value) +
          Chr(13) + Chr(10) + RichEdit1.Text;
        //
        QrPesqlan.Next;
      end;
      if Definitivo then
      begin
        Atualizabloqparc(QrbloqparcCodigo.Value, 4);
      end;
    end;
    // Criar registros que ligam os lan�amentos reparcelados aos
    // lan�amentos de reparcelamentos
    if StatusBloqParcMenorQue(5, QrbloqparcCodigo.Value) then
    //if QrbloqparcNovo.Value < 5 then
    begin
      //Result := QuebraLancamentos(Definitivo, FatorRat);
      if QrbloqparcCodigo.Value > 0 then
      begin
        FatorRat1 := QuebraLancamentos_1(False, 0);
        QuebraLancamentos_1(True, FatorRat1);
      end else begin
        FatorRat1 := QuebraLancamentos_2(False, 0);
        QuebraLancamentos_2(True, FatorRat1);
      end;
      Atualizabloqparc(QrbloqparcCodigo.Value, 5);
    end;

    // Distribuir restos de valores pelos itens
    // lan�amentos de reparcelamentos
    if StatusBloqParcMenorQue(6, QrbloqparcCodigo.Value) then
    //if QrbloqparcNovo.Value < 6 then
    begin
      DistribuiRestos();
      Atualizabloqparc(QrbloqparcCodigo.Value, 6);
    end;

    // Fazer upload de iten de v�nculo
    if StatusBloqParcMenorQue(7, QrbloqparcCodigo.Value) then
    //if QrbloqparcNovo.Value < 7 then
    begin
      UMyMod.ExportaRegistrosEntreDBs_Novo('bloqparcits', 'bloqparcits',
        'WHERE Codigo=' + FormatFloat('0', QrbloqparcCodigo.Value),
        Dmod.MyDB, Dmod.MyDBn, RichEdit1);
      //
      Atualizabloqparc(QrbloqparcCodigo.Value, 7);
    end;
    if StatusBloqParcMenorQue(8, QrbloqparcCodigo.Value) then
    begin
      SomasBloqParc;
      //
      Atualizabloqparc(QrbloqparcCodigo.Value, 8);
    end;
    if StatusBloqParcAtual(QrbloqparcCodigo.Value) >= 8 then
      Atualizabloqparc(QrbloqparcCodigo.Value, 9);
  end;
  //PnAtz.Visible := False;
  Screen.Cursor := crDefault;
end;

procedure TFmbloqparc.FormCreate(Sender: TObject);
begin
  Imgtipo.SQLType := stLok;
  //
  PnRecorrige.Visible  := False;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  DModG.Def_EM_ABD_Limpa(FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  //
  FCols[00] := 3;
  FCols[01] := 2;
  FCols[02] := 2;
  FCols[03] := 2;
  FCols[04] := 6;
  THackDBGrid(DBGPesq_Titul).ScrollBars := ssNone;
  DBGPesq_Titul.Height := 18;
  MyObjects.DBGridLarguraSegundoTitulo(DBGPesq_Titul, DBGPesq_Dados, FCols);
  QrPropriet.Open;
  QrCondImov.Open;
  //QrCondGri.Open;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  {#
  PainelEdita.Align := alClient;
  PainelEdit.Align  := alClient;
  #}
  PainelDados.Align := alClient;
  PainelGrid.Align  := alClient;
  CriaOForm;
  //
  GradeA.ColCount := 11;
  GradeA.Cells[00, 0] := 'Seq';
  GradeA.Cells[01, 0] := 'Vlr.Orig.';
  GradeA.Cells[02, 0] := 'Vecimto';
  GradeA.Cells[03, 0] := 'Dias';
  GradeA.Cells[04, 0] := '% Juros';
  GradeA.Cells[05, 0] := '$ Juros';
  GradeA.Cells[06, 0] := '$ Multa';
  GradeA.Cells[07, 0] := '$ Total';
  GradeA.Cells[08, 0] := '#';
  GradeA.Cells[09, 0] := 'Dias * Orig.';
  GradeA.Cells[10, 0] := '';
  //
  GradeA.ColWidths[00] := 32;
  GradeA.ColWidths[01] := 72;
  GradeA.ColWidths[02] := 56;
  GradeA.ColWidths[03] := 92;
  GradeA.ColWidths[04] := 72;
  GradeA.ColWidths[05] := 64;
  GradeA.ColWidths[06] := 64;
  GradeA.ColWidths[07] := 72;
  GradeA.ColWidths[08] := 7;
  GradeA.ColWidths[09] := 120;
  GradeA.ColWidths[10] := 7;
  //
  GradeB.ColCount := 20;
  GradeB.Cells[00, 0] := 'Seq';
  GradeB.Cells[01, 0] := 'Item';
  GradeB.Cells[02, 0] := 'Vlr.Orig.';
  GradeB.Cells[03, 0] := 'Vecimto';
  GradeB.Cells[04, 0] := 'Dias';
  GradeB.Cells[05, 0] := '% Juros';
  GradeB.Cells[06, 0] := '$ Juros';
  GradeB.Cells[07, 0] := '$ Multa';
  GradeB.Cells[08, 0] := '$ Total';
  //
  GradeB.Cells[09, 0] := 'Dias';
  GradeB.Cells[10, 0] := '% Ini';
  GradeB.Cells[11, 0] := '% Fim';
  GradeB.Cells[12, 0] := 'Vlr.Orig.';
  GradeB.Cells[13, 0] := '$ Juros';
  GradeB.Cells[14, 0] := '$ Multa';
  GradeB.Cells[15, 0] := '$ Total';
  GradeB.Cells[16, 0] := '$ Parc.Ant';
  GradeB.Cells[17, 0] := '$ Parc.Dep';
  GradeB.Cells[18, 0] := 'Calc';
  GradeB.Cells[19, 0] := 'Dias * Vlr.Orig';
  //

  //
  GradeB.ColWidths[00] := 28;
  GradeB.ColWidths[01] := 28;
  GradeB.ColWidths[02] := 72;
  GradeB.ColWidths[03] := 56;
  GradeB.ColWidths[04] := 46;
  GradeB.ColWidths[05] := 72;
  GradeB.ColWidths[06] := 64;
  GradeB.ColWidths[07] := 64;
  GradeB.ColWidths[08] := 72;
  //
  GradeB.ColWidths[09] := 46;
  GradeB.ColWidths[10] := 74;
  GradeB.ColWidths[11] := 74;
  GradeB.ColWidths[12] := 72;
  GradeB.ColWidths[13] := 64;
  GradeB.ColWidths[14] := 64;
  GradeB.ColWidths[15] := 72;
  GradeB.ColWidths[16] := 72;
  GradeB.ColWidths[17] := 72;
  GradeB.ColWidths[18] := 24;
  GradeB.ColWidths[19] := 80;
  //
  DBGrid2.Height := 131;
  //
  TPIniPar.Date := Date - 180;
  TPFimPar.Date := Date;
  TPIniVct.Date := Date - 180;
  TPFimVct.Date := Date;
end;

function TFmbloqparc.QuebraLancamentos_1(Definitivo: Boolean; FatorRat: Double): Double;
  procedure InsereLinhaGradeB(var Linha: Integer; const Item: Integer;
  const Valor: Double; const Vencimento: TDateTime; const DiasE, Juros,
  ValJu, ValMu, ValTo: Double; var NewParc: String; const Vencimento2, Dias2:
  String; const PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
           CtrlOrigi, BloqOrigi: Double; const n: Integer; const Tipo: String;
           const ParceA, ParceD, Dias, VlrDD: Double);
  var
    Txt_Valor, Txt_Venci, Txt_DiasE, Txt_Juros, Txt_ValJu, Txt_ValMU,
    Txt_ValTo: String;
  begin
    if Linha = 0 then FItem := 0;
    Linha := Linha + 1;
    if FItem <> Item then
    begin
      Txt_Valor := Geral.FFT(Valor, 2, siNegativo);
      Txt_Venci := Geral.FDT(Vencimento, 3);
      Txt_DiasE := Geral.FFT(DiasE, 0, siNegativo);
      Txt_Juros := Geral.FFT(Juros, 6, siNegativo);
      Txt_ValJu := Geral.FFT(ValJu, 2, siNegativo);
      Txt_ValMU := Geral.FFT(ValMu, 2, siNegativo);
      Txt_ValTo := Geral.FFT(ValTo, 2, siNegativo);
    end else begin
      Txt_Valor := '';
      Txt_Venci := '';
      Txt_DiasE := '';
      Txt_Juros := '';
      Txt_ValJu := '';
      Txt_ValMU := '';
      Txt_ValTo := '';
    end;
    GradeB.RowCount := Linha + 1;
    GradeB.Cells[00, Linha] := IntToStr(Linha);
    GradeB.Cells[01, Linha] := IntToStr(Item);
    GradeB.Cells[02, Linha] := Txt_Valor;
    GradeB.Cells[03, Linha] := Txt_Venci;
    GradeB.Cells[04, Linha] := Txt_DiasE;
    GradeB.Cells[05, Linha] := Txt_Juros;
    GradeB.Cells[06, Linha] := Txt_ValJu;
    GradeB.Cells[07, Linha] := Txt_ValMU;
    GradeB.Cells[08, Linha] := Txt_ValTo;
    GradeB.Cells[09, Linha] := Geral.FFT(Dias, 0, siNegativo);
    GradeB.Cells[10, Linha] := Geral.FFT(PercIni, 6, siNegativo);
    GradeB.Cells[11, Linha] := Geral.FFT(PercFim, 6, siNegativo);
    GradeB.Cells[12, Linha] := Geral.FFT(VlrOrigi, 2, siNegativo);
    GradeB.Cells[13, Linha] := Geral.FFT(VlrJuros, 2, siNegativo);
    GradeB.Cells[14, Linha] := Geral.FFT(VlrMulta, 2, siNegativo);
    GradeB.Cells[15, Linha] := Geral.FFT(VlrTotal, 2, siNegativo);
    GradeB.Cells[16, Linha] := Geral.FFT(ParceA, 2, siNegativo);
    GradeB.Cells[17, Linha] := Geral.FFT(ParceD, 2, siNegativo);
    GradeB.Cells[18, Linha] := Tipo;
    GradeB.Cells[19, Linha] := Geral.FFT(VlrDD, 2, siNegativo);
    FItem := Item;
  end; // Fim InsereLinhaGradeB

  function InsereItem(PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal:
  Double; CtrlOrigi: Integer; BloqOrigi: Double; Linha: Integer; Tipo: String;
  ParcAnt, ParcDep, Dias, FatorExt: Double; Definitiva: Boolean): Double;
    function FF(var num: Double; const Tam: Integer): String;
    var
      str: String;
    begin
      str := FormatFloat('#,###,##0.00', num);
      while Length(str) < Tam do str := ' ' + str;
      result := str;
    end;
    var
      VlrAjust, VlrSomas: Double;
  begin
    // N�o Fazer por enquanto
    //Exit;
    MeLog.Lines.Add(
      FormatFloat('000', Linha) + ' - ' + '<' + Tipo + '> - ' +
      FF(PercIni, 7) + ' - ' +
      FF(PercFim, 7) + ' - ' +
      FF(VlrOrigi, 9) + ' - ' +
      FF(VlrMulta, 9) + ' - ' +
      FF(VlrJuros, 9) + ' - ' +
      FF(VlrTotal, 9) + ' - ' +
      FormatFloat('000000', CtrlOrigi) + ' - ' +
      FormatFloat('000000', BloqOrigi) + ' - ' +
      FormatFloat('000', QrbloqparcCodigo.Value) + ' - ' +
      FormatFloat('000000', QrbloqparcparControle.Value) + ' - ' +
      FF(ParcAnt, 9) + ' - ' +
      FF(ParcDep, 9) + ' - ' +
      FormatFloat('0000', Dias) +
      '');
    //

    //VlrAjust := FatorExt * VlrJuros;
    VlrAjust := 0;
    VlrSomas := VlrAjust + VlrTotal;
    if Definitiva then
    begin
      Dmod.QrUpdU.Params[00].AsInteger := Linha;
      Dmod.QrUpdU.Params[01].AsString  := Tipo;
      Dmod.QrUpdU.Params[02].AsFloat   := PercIni;
      Dmod.QrUpdU.Params[03].AsFloat   := PercFim;
      Dmod.QrUpdU.Params[04].AsFloat   := VlrOrigi;
      Dmod.QrUpdU.Params[05].AsFloat   := VlrMulta;
      Dmod.QrUpdU.Params[06].AsFloat   := VlrJuros;
      Dmod.QrUpdU.Params[07].AsFloat   := VlrTotal;
      Dmod.QrUpdU.Params[08].AsFloat   := VlrAjust;
      Dmod.QrUpdU.Params[09].AsFloat   := VlrSomas;
      Dmod.QrUpdU.Params[10].AsInteger := CtrlOrigi;
      Dmod.QrUpdU.Params[11].AsFloat   := BloqOrigi;
      Dmod.QrUpdU.Params[12].AsFloat   := ParcAnt;
      Dmod.QrUpdU.Params[13].AsFloat   := ParcDep;
      Dmod.QrUpdU.Params[14].AsFloat   := Dias;
      Dmod.QrUpdU.Params[15].AsInteger := QrbloqparcparParcela.Value;

      Dmod.QrUpdU.Params[16].AsInteger := QrbloqparcCodigo.Value;
      Dmod.QrUpdU.Params[17].AsInteger := QrbloqparcparControle.Value;
      Dmod.QrUpdU.ExecSQL;
    end;
    Result := VlrAjust;
  end;
  // Fim Insere Item

  {
  procedure ConfiguraVariaveis(var DiasT, DiasE, Juros: Double);
  begin
    DiasT := Int(QrbloqparcparVencimento.Value);
    DiasE := DiasT - Int(QrLctVencimento.Value);
    Juros := DiasE / 30;
  end;
  }
  
  procedure ConfiguraVariaveis2(var DiasT, DiasE, Juros,
    OrigiVal, JurosVal, MultaVal, TotalVal: Double);
  var
    Meses, Multa: Double;
  begin
    // Parei Aqui ver c�lculo correto de juros
    DiasT := Int(QrbloqparcparVencimento.Value);
    DiasE := DiasT - Int(QrpesqlanVencimento.Value);
    if QrbloqparcCodigo.Value > 0 then
    begin
      Juros := DiasE / 30;
      Multa := QrPesqlanMulta.Value;
    end else begin
      Meses := DModG.MesesEntreDatas(QrpesqlanVencimento.Value, QrbloqparcparVencimento.Value);
      Juros := QrbloqparcTxaJur.Value * Meses;
      Multa := QrbloqparcTxaMul.Value;
    end;
    OrigiVal := QrPesqlanCredito.Value;
    JurosVal := Round(Juros * OrigiVal) / 100;
    MultaVal := Round(Multa * OrigiVal) / 100;
    TotalVal := OrigiVal + JurosVal + MultaVal;
  end;

  function ObtemValorParcela: Double;
  begin
    if CkManual.Checked and (dmkEdit6.ValueVariant > 0) then
      Result := dmkEdit6.ValueVariant
    else
      Result := dmkEdit5.ValueVariant;
  end;
var
  i: Integer;
  DiasT, DiasE, JurosPer, Valor, ValJu, ValMu, ValTo, SumJu, SumMu, SumTo,
  SumOr, ValParc2, ParceA, ParceD, ItsOri, ItsJur, ItsMul, ItsTot,
  PerIni, PerFim, SumVD, SumDD: Double;
  NewParc: String;
  //
  Valor_Sum, Meses, PercIni, PercFIm, MesesQtd, JurosVal, MultaVal,
  TotalVal, VlrOrigi, VlrMulta, VlrJuros, VlrTotal, SumOrigi, SumMulta,
  SumJuros, SumTotal, Fator, UltDta, AtuDta, JurosExt, FatSumJur,
  OrigiVal, SumAjust, Vlr_Dias, TotVlrDD, TotOrigi: Double;
  BloqOrigi: Double;
  CtrlOrigi, n: Integer;
  VlrAjust, VlrSomas, MultaPer: Double;
const
  mul1 = 1;
  div1 = 100;
begin
  if Qrbloqparcpar.RecordCount = 0 then Exit;
  PnQuebra.Visible := True;
  Qrbloqparcpar.First;
  DiasT := 0;
  SumJu := 0;
  SumMu := 0;
  SumTo := 0;
  SumOr := 0;
  SumDD := 0;
  SumVD := 0;
  FatSumJur := 0;
  SumAjust  := 0;
 //
  while not Qrbloqparcpar.Eof do
  begin
    DiasT := DiasT + Int(QrbloqparcparVencimento.Value);
    Qrbloqparcpar.Next;
  end;
  if Qrbloqparcpar.RecordCount > 0 then
    DiasT := DiasT / Qrbloqparcpar.RecordCount
  else Geral.MensagemBox('Quantidade de registros=0 para BloqParcpar!',
  'Erro', MB_OK+MB_ICONERROR);
  //
  Edit4.Text := FormatDateTime('dd/mm/yy hh:nn:ss', DiasT);
  //
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  GradeA.RowCount := QrLct.RecordCount + 1;
  QrLct.First;
  while not QrLct.Eof do
  begin
    i := QrLct.RecNo;
    if QrbloqparcparCodigo.Value > 0 then
    begin
      DiasE := DiasT - Int(QrLctVencimento.Value);
      JurosPer := DiasE / 30;
      MultaPer := 2;
    end else begin
      Meses := DModG.MesesEntreDatas(QrlctVencimento.Value, QrbloqparcparVencimento.Value);
      JurosPer := QrbloqparcTxaJur.Value * Meses;
      MultaPer := QrbloqparcTxaMul.Value;
    end;
    Valor := QrLctCredito.Value;
    ValJu := Round(Valor * JurosPer) / 100;
    ValMu := Round(Valor * MultaPer) / 100;
    ValTo := Valor + ValMu + ValJu;
    //
    GradeA.Cells[00, i] := IntToStr(i);
    GradeA.Cells[01, i] := Geral.FFT(Valor, 2, siNegativo);
    GradeA.Cells[02, i] := Geral.FDT(QrLctVencimento.Value, 3);
    GradeA.Cells[03, i] := Geral.FFT(DiasE, 6, siNegativo);
    GradeA.Cells[04, i] := Geral.FFT(JurosPer, 6, siNegativo);
    GradeA.Cells[05, i] := Geral.FFT(ValJu, 2, siNegativo);
    GradeA.Cells[06, i] := Geral.FFT(ValMu, 2, siNegativo);
    GradeA.Cells[07, i] := Geral.FFT(ValTo, 2, siNegativo);
    GradeA.Cells[09, i] := Geral.FFT(Valor * DiasE, 2, siNegativo);
    //
    SumOr := SumOr + Valor;
    SumJu := SumJu + ValJu;
    SumMu := SumMu + ValMu;
    SumTo := SumTo + ValTo;
    SumDD := SumDD + DiasE;
    SumVD := SumVD + Valor * DiasE;
    QrLct.Next;
  end;
  dmkEdit1.ValueVariant := SumOr;
  dmkEdit2.ValueVariant := SumJu;
  dmkEdit3.ValueVariant := SumMu;
  dmkEdit4.ValueVariant := SumTo;
  if Qrbloqparcpar.RecordCount > 0 then
    dmkEdit5.ValueVariant := Round(SumTo / Qrbloqparcpar.RecordCount * 100) / 100
  else
    dmkEdit5.ValueVariant := 0;
  dmkEdit7.ValueVariant := SumVD;

  //

  if SumOr > 0 then
    dmkEdit8.ValueVariant := SumVD / SumOr
  else
    dmkEdit8.ValueVariant := 0;

  //

  if SumDD > 0 then
    dmkEdit9.ValueVariant := SumVD / SumDD
  else
    dmkEdit9.ValueVariant := 0;

  // Calcular Itens de parcela
  i := 0;
  PerIni  := 0;

  ReabrirPesq('Calculando juros ...');
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO bloqparcits SET ');
  Dmod.QrUpdU.SQL.Add('Seq=:P0, Tipo=:P1, PercIni=:P2, PercFim=:P3, ');
  Dmod.QrUpdU.SQL.Add('VlrOrigi=:P4, VlrMulta=:P5, VlrJuros=:P6, ');
  Dmod.QrUpdU.SQL.Add('VlrTotal=:P7, VlrAjust=:P8, VlrSomas=:P9, ');
  Dmod.QrUpdU.SQL.Add('CtrlOrigi=:P10, BloqOrigi=:P11, SParcA=:P12, ');
  Dmod.QrUpdU.SQL.Add('SParcD=:P13, Dias=:P14, Parcela=:P15, ');
  Dmod.QrUpdU.SQL.Add('');
  Dmod.QrUpdU.SQL.Add('Codigo=:Pa, Controle=:Pb ');

  Qrbloqparcpar.First;

  ValParc2 := ObtemValorParcela;

  PercIni := 0;
  SumOrigi := 0;
  SumMulta := 0;
  SumJuros := 0;
  SumTotal := 0;
  TotVlrDD := 0;
  TotOrigi := 0;
  n := 1;
  i := 0;
  while not QrPesqlan.Eof do
  begin
    PBAtz.Position := PBAtz.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    CtrlOrigi := QrPesqlanControle.Value;
    BloqOrigi := QrPesqlanBLOQUETO.Value;
    //
    ConfiguraVariaveis2(DiasT, DiasE, JurosPer,
    OrigiVal, JurosVal, MultaVal, TotalVal);
    UltDta := QrbloqparcparVencimento.Value;
    AtuDta := UltDta;
    //
    if (ValParc2 + 0.00999) >= (TotalVal - SumTotal) then
    begin
      VlrOrigi := QrPesqlanCredito.Value - SumOrigi;
      VlrMulta := MultaVal - SumMulta;
      VlrJuros := JurosVal - SumJuros;
      VlrTotal := TotalVal - SumTotal;
      Vlr_Dias := JurosPer * 30 * VlrOrigi;
      PercFim := 100;
      //
      SumOrigi := 0;
      SumMulta := 0;
      SumJuros := 0;
      SumTotal := 0;
      TotVlrDD := TotVlrDD + Vlr_Dias;
      TotOrigi := TotOrigi + VlrOrigi;
      //
      InsereLinhaGradeB(i, QrPesqLan.RecNo, OrigiVal, QrPesqlanVencimento.Value,
        DiasE, JurosPer, JurosVal, MultaVal, TotalVal, NewParc, '', '',
        PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
       CtrlOrigi, BloqOrigi, n, 'A', ValParc2, ValParc2 - TotalVal,
       JurosPer * 30, Vlr_Dias);
      //
      //InsereItem(PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
       //CtrlOrigi, BloqOrigi, n, 'A', ValParc2, ValParc2 - TotalVal, Juros * 30);
        VlrAjust := InsereItem(PercIni, PercFim,
          VlrOrigi, VlrMulta, VlrJuros, VlrTotal, CtrlOrigi, BloqOrigi, n,
          'A', ValParc2, ValParc2 - TotalVal, JurosPer * 30, FatorRat,
          Definitivo);

      // atualiza valor restante da parcela a deduzir
      ValParc2 := ValParc2 - TotalVal - VlrAjust;
      SumAjust := SumAjust + VlrAjust;

      // acabou o parcelamento atual
      if (ValParc2 < 0.00999) then
      begin
        PercIni := 0;
        if Qrbloqparcpar.RecNo <> Qrbloqparcpar.RecordCount then
        begin
          Qrbloqparcpar.Next;

          ConfiguraVariaveis2(DiasT, DiasE, JurosPer,
            OrigiVal, JurosVal, MultaVal, TotalVal);
          //ValParc2 := QrbloqparcparValBol.Value;
          ValParc2 := ObtemValorParcela;
          AtuDta := UltDta;
          //UltDta := Int(QrbloqparcparVencimento.Value);
        end else begin
          ValParc2 := 0;
        end;;
      end;
    end else begin
      PercFim := 0;
      while (((TotalVal - SumTotal) > (ValParc2 + 0.00999) )
      and (ValParc2 > 0) ) do
      begin
        if TotalVal > 0 then
          Fator := ValParc2 / TotalVal * 100
        else
          Fator := 0;
        //
        //if Qrbloqparcpar.RecNo <> Qrbloqparcpar.RecordCount then
          PercFim := PercFim + Fator;
        //else
          //PercFim := 100;

        VlrOrigi := Round(Fator * QrPesqlanCredito.Value) / 100;
        SumOrigi := SumOrigi + VlrOrigi;

        VlrMulta := Round(Fator * MultaVal) / 100;
        SumMulta := SumMulta + VlrMulta;

        VlrJuros := Round(Fator * JurosVal) / 100;
        SumJuros := SumJuros + VlrJuros;

        VlrTotal := Round(Fator * TotalVal) / 100;
        SumTotal := SumTotal + VlrTotal;

        Vlr_Dias := JurosPer * 30 * VlrOrigi;
        TotVlrDD := TotVlrDD + Vlr_Dias;
        TotOrigi := TotOrigi + VlrOrigi;

        InsereLinhaGradeB(i, QrPesqLan.RecNo, OrigiVal, QrPesqlanVencimento.Value,
          DiasE, JurosPer, JurosVal, MultaVal, TotalVal, NewParc, '', '',
          PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
          CtrlOrigi, BloqOrigi, n, 'B', ValParc2, 0, JurosPer * 30, Vlr_Dias);
      //
        FatSumJur := FatSumJur + VlrJuros;
        VlrAjust := InsereItem(PercIni, PercFim,
          VlrOrigi, VlrMulta, VlrJuros, VlrTotal, CtrlOrigi, BloqOrigi, n,
          'B', ValParc2, 0, JurosPer * 30, FatorRat, Definitivo);
        //InsereItem(PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
          //CtrlOrigi, BloqOrigi, n, 'B', ValParc2, 0, Juros * 30);
        if Qrbloqparcpar.RecNo <> Qrbloqparcpar.RecordCount then
        begin
          Qrbloqparcpar.Next;

          ConfiguraVariaveis2(DiasT, DiasE, JurosPer,
            OrigiVal, JurosVal, MultaVal, TotalVal);

          // atualiza valor da parcela a deduzir
          ValParc2 := ObtemValorParcela;
          SumAjust := SumAjust + VlrAjust;

        end else begin
          //ValParc2 := 0;
        end;
        PercIni := PercFim;
      end;

      // restante do lancto pois � menor que a parcela
      if (TotalVal - SumTotal) >= 0.01 then
      begin
        PercFim := 100;
        Fator := 100 - PercIni;
        if Fator > 0 then
        begin
          //
          //VlrOrigi := Round(Fator * QrPesqlanCredito.Value) / 100;
          VlrOrigi := QrPesqlanCredito.Value - SumOrigi;

          VlrMulta := Round(Fator * MultaVal) / 100;

          VlrJuros := Round(Fator * JurosVal) / 100;

          VlrTotal := Round(Fator * TotalVal) / 100;

          Vlr_Dias := JurosPer * 30 * VlrOrigi;
          TotVlrDD := TotVlrDD + Vlr_Dias;
          TotOrigi := TotOrigi + VlrOrigi;

          InsereLinhaGradeB(i, QrPesqLan.RecNo, OrigiVal, QrPesqlanVencimento.Value,
            DiasE, JurosPer, JurosVal, MultaVal, TotalVal, NewParc, '', '',
            PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
            CtrlOrigi, BloqOrigi, n, 'C', ValParc2, ValParc2 - VlrTotal,
            JurosPer * 30, Vlr_Dias);
          //
            FatSumJur := FatSumJur + VlrJuros;
            VlrAjust := InsereItem(PercIni, PercFim,
              VlrOrigi, VlrMulta, VlrJuros, VlrTotal, CtrlOrigi, BloqOrigi, n,
              'C', ValParc2, ValParc2 - VlrTotal, JurosPer * 30, FatorRat,
              Definitivo);
          //InsereItem(PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
            //CtrlOrigi, BloqOrigi, n, 'C', ValParc2, ValParc2 - VlrTotal,
            //JurosPer * 30);

          // atualiza valor restante da parcela a deduzir
          ValParc2 := ValParc2 - VlrTotal - VlrAjust;
          SumAjust := SumAjust + VlrAjust;

        end;
        SumMulta := 0;
        SumOrigi := 0;
        SumJuros := 0;
        SumTotal := 0;
        PercIni  := 0;
      end;
    end;
    //
    n := n + 1;
    QrPesqlan.Next;
  end;
  dmkEdit10.ValueVariant := TotVlrDD;
  if TotOrigi > 0 then
    dmkEdit11.ValueVariant := TotVlrDD / TotOrigi
  else
    dmkEdit11.ValueVariant := 0;

  //

  if FatSumJur = 0 then
    Result := 0
  else begin
    if FatSumJur <> 0 then
      Result := ValParc2 / FatSumJur
    else
      Result := 0;
  end;
end;

function TFmbloqparc.QuebraLancamentos_2(Definitivo: Boolean; FatorRat: Double): Double;
  procedure InsereLinhaGradeB(var Linha: Integer; const Item: Integer;
  const Valor: Double; const Vencimento: TDateTime; const DiasE, Juros,
  ValJu, ValMu, ValTo: Double; var NewParc: String; const Vencimento2, Dias2:
  String; const PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
           CtrlOrigi, BloqOrigi: Double; const n: Integer; const Tipo: String;
           const ParceA, ParceD, Dias, VlrDD: Double);
  var
    Txt_Valor, Txt_Venci, Txt_DiasE, Txt_Juros, Txt_ValJu, Txt_ValMU,
    Txt_ValTo: String;
  begin
    if Linha = 0 then FItem := 0;
    Linha := Linha + 1;
    if FItem <> Item then
    begin
      Txt_Valor := Geral.FFT(Valor, 2, siNegativo);
      Txt_Venci := Geral.FDT(Vencimento, 3);
      Txt_DiasE := Geral.FFT(DiasE, 0, siNegativo);
      Txt_Juros := Geral.FFT(Juros, 6, siNegativo);
      Txt_ValJu := Geral.FFT(ValJu, 2, siNegativo);
      Txt_ValMU := Geral.FFT(ValMu, 2, siNegativo);
      Txt_ValTo := Geral.FFT(ValTo, 2, siNegativo);
    end else begin
      Txt_Valor := '';
      Txt_Venci := '';
      Txt_DiasE := '';
      Txt_Juros := '';
      Txt_ValJu := '';
      Txt_ValMU := '';
      Txt_ValTo := '';
    end;
    GradeB.RowCount := Linha + 1;
    GradeB.Cells[00, Linha] := IntToStr(Linha);
    GradeB.Cells[01, Linha] := IntToStr(Item);
    GradeB.Cells[02, Linha] := Txt_Valor;
    GradeB.Cells[03, Linha] := Txt_Venci;
    GradeB.Cells[04, Linha] := Txt_DiasE;
    GradeB.Cells[05, Linha] := Txt_Juros;
    GradeB.Cells[06, Linha] := Txt_ValJu;
    GradeB.Cells[07, Linha] := Txt_ValMU;
    GradeB.Cells[08, Linha] := Txt_ValTo;
    GradeB.Cells[09, Linha] := Geral.FFT(Dias, 0, siNegativo);
    GradeB.Cells[10, Linha] := Geral.FFT(PercIni, 6, siNegativo);
    GradeB.Cells[11, Linha] := Geral.FFT(PercFim, 6, siNegativo);
    GradeB.Cells[12, Linha] := Geral.FFT(VlrOrigi, 2, siNegativo);
    GradeB.Cells[13, Linha] := Geral.FFT(VlrJuros, 2, siNegativo);
    GradeB.Cells[14, Linha] := Geral.FFT(VlrMulta, 2, siNegativo);
    GradeB.Cells[15, Linha] := Geral.FFT(VlrTotal, 2, siNegativo);
    GradeB.Cells[16, Linha] := Geral.FFT(ParceA, 2, siNegativo);
    GradeB.Cells[17, Linha] := Geral.FFT(ParceD, 2, siNegativo);
    GradeB.Cells[18, Linha] := Tipo;
    GradeB.Cells[19, Linha] := Geral.FFT(VlrDD, 2, siNegativo);
    FItem := Item;
  end; // Fim InsereLinhaGradeB

  function InsereItem(PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal:
  Double; CtrlOrigi: Integer; BloqOrigi: Double; Linha: Integer; Tipo: String;
  ParcAnt, ParcDep, Dias, FatorExt: Double; Definitiva: Boolean): Double;
    function FF(var num: Double; const Tam: Integer): String;
    var
      str: String;
    begin
      str := FormatFloat('#,###,##0.00', num);
      while Length(str) < Tam do str := ' ' + str;
      result := str;
    end;
    var
      VlrAjust, VlrSomas: Double;
  begin
    // N�o Fazer por enquanto
    //Exit;
    MeLog.Lines.Add(
      FormatFloat('000', Linha) + ' - ' + '<' + Tipo + '> - ' +
      FF(PercIni, 7) + ' - ' +
      FF(PercFim, 7) + ' - ' +
      FF(VlrOrigi, 9) + ' - ' +
      FF(VlrMulta, 9) + ' - ' +
      FF(VlrJuros, 9) + ' - ' +
      FF(VlrTotal, 9) + ' - ' +
      FormatFloat('000000', CtrlOrigi) + ' - ' +
      FormatFloat('000000', BloqOrigi) + ' - ' +
      FormatFloat('000', QrbloqparcCodigo.Value) + ' - ' +
      FormatFloat('000000', QrbloqparcparControle.Value) + ' - ' +
      FF(ParcAnt, 9) + ' - ' +
      FF(ParcDep, 9) + ' - ' +
      FormatFloat('0000', Dias) +
      '');
    //

    //VlrAjust := FatorExt * VlrJuros;
    VlrAjust := 0;
    VlrSomas := VlrAjust + VlrTotal;
    if Definitiva then
    begin
      Dmod.QrUpdU.Params[00].AsInteger := Linha;
      Dmod.QrUpdU.Params[01].AsString  := Tipo;
      Dmod.QrUpdU.Params[02].AsFloat   := PercIni;
      Dmod.QrUpdU.Params[03].AsFloat   := PercFim;
      Dmod.QrUpdU.Params[04].AsFloat   := VlrOrigi;
      Dmod.QrUpdU.Params[05].AsFloat   := VlrMulta;
      Dmod.QrUpdU.Params[06].AsFloat   := VlrJuros;
      Dmod.QrUpdU.Params[07].AsFloat   := VlrTotal;
      Dmod.QrUpdU.Params[08].AsFloat   := VlrAjust;
      Dmod.QrUpdU.Params[09].AsFloat   := VlrSomas;
      Dmod.QrUpdU.Params[10].AsInteger := CtrlOrigi;
      Dmod.QrUpdU.Params[11].AsFloat   := BloqOrigi;
      Dmod.QrUpdU.Params[12].AsFloat   := ParcAnt;
      Dmod.QrUpdU.Params[13].AsFloat   := ParcDep;
      Dmod.QrUpdU.Params[14].AsFloat   := Dias;
      Dmod.QrUpdU.Params[15].AsInteger := QrbloqparcparParcela.Value;

      Dmod.QrUpdU.Params[16].AsInteger := QrbloqparcCodigo.Value;
      Dmod.QrUpdU.Params[17].AsInteger := QrbloqparcparControle.Value;
      Dmod.QrUpdU.ExecSQL;
    end;
    Result := VlrAjust;
  end;
  // Fim Insere Item

  {
  procedure ConfiguraVariaveis(var DiasT, DiasE, Juros: Double);
  begin
    DiasT := Int(QrbloqparcparVencimento.Value);
    DiasE := DiasT - Int(QrLctVencimento.Value);
    Juros := DiasE / 30;
  end;
  }
  
  procedure ConfiguraVariaveis2(var DiasT, DiasE, Juros,
    OrigiVal, JurosVal, MultaVal, TotalVal: Double);
  var
    Meses, Multa: Double;
  begin
    // Parei Aqui ver c�lculo correto de juros
    DiasT := Int(QrbloqparcparVencimento.Value);
    DiasE := DiasT - Int(QrpesqlanVencimento.Value);
    {
    if QrbloqparcCodigo.Value > 0 then
    begin
      Juros := DiasE / 30;
      Multa := QrPesqlanMulta.Value;
    end else begin
    }
      Meses := DModG.MesesEntreDatas(QrpesqlanVencimento.Value, QrbloqparcparVencimento.Value);
      Juros := QrbloqparcTxaJur.Value * Meses;
      Multa := QrbloqparcTxaMul.Value;
    //end;
    OrigiVal := QrPesqlanCredito.Value;
    JurosVal := Round(Juros * OrigiVal) / 100;
    MultaVal := Round(Multa * OrigiVal) / 100;
    TotalVal := OrigiVal + JurosVal + MultaVal;
  end;

  function ObtemValorParcela: Double;
  begin
    if CkManual.Checked and (dmkEdit6.ValueVariant > 0) then
      Result := dmkEdit6.ValueVariant
    else begin
      //Result := dmkEdit5.ValueVariant;
      Result := QrbloqparcparValBol.Value;
    end;
  end;
var
  i: Integer;
  DiasT, DiasE, JurosPer, Valor, ValJu, ValMu, ValTo, SumJu, SumMu, SumTo,
  SumOr, ValParc2, ParceA, ParceD, ItsOri, ItsJur, ItsMul, ItsTot,
  PerIni, PerFim, SumVD, SumDD: Double;
  NewParc: String;
  //
  Valor_Sum, Meses, PercIni, PercFIm, MesesQtd, JurosVal, MultaVal,
  TotalVal, VlrOrigi, VlrMulta, VlrJuros, VlrTotal, SumOrigi, SumMulta,
  SumJuros, SumTotal, Fator, UltDta, AtuDta, JurosExt, FatSumJur,
  OrigiVal, SumAjust, Vlr_Dias, TotVlrDD, TotOrigi: Double;
  BloqOrigi: Double;
  CtrlOrigi, n: Integer;
  VlrAjust, VlrSomas, MultaPer: Double;
const
  mul1 = 1;
  div1 = 100;
begin
  Pagecontrol2.ActivePageIndex := 1;
  Pagecontrol3.ActivePageIndex := 0;
  if Qrbloqparcpar.RecordCount = 0 then Exit;
  PnQuebra.Visible := True;
  Qrbloqparcpar.First;
  DiasT := 0;
  SumJu := 0;
  SumMu := 0;
  SumTo := 0;
  SumOr := 0;
  SumDD := 0;
  SumVD := 0;
  FatSumJur := 0;
  SumAjust  := 0;
 //
  while not Qrbloqparcpar.Eof do
  begin
    DiasT := DiasT + Int(QrbloqparcparVencimento.Value);
    Qrbloqparcpar.Next;
  end;
  if Qrbloqparcpar.RecordCount > 0 then
    DiasT := DiasT / Qrbloqparcpar.RecordCount
  else Geral.MensagemBox('Quantidade de registros=0 para BloqParcpar!',
  'Erro', MB_OK+MB_ICONERROR);
  //
  Edit4.Text := FormatDateTime('dd/mm/yy hh:nn:ss', DiasT);
  //
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  GradeA.RowCount := QrLct.RecordCount + 1;
  QrLct.First;
  while not QrLct.Eof do
  begin
    i := QrLct.RecNo;
    {
    if QrbloqparcparCodigo.Value > 0 then
    begin
      DiasE := DiasT - Int(QrLctVencimento.Value);
      JurosPer := DiasE / 30;
      MultaPer := 2;
    end else begin
    }
      Meses := DModG.MesesEntreDatas(QrlctVencimento.Value, QrbloqparcparVencimento.Value);
      JurosPer := QrbloqparcTxaJur.Value * Meses;
      MultaPer := QrbloqparcTxaMul.Value;
    //end;
    Valor := QrLctCredito.Value;
    ValJu := Round(Valor * JurosPer) / 100;
    ValMu := Round(Valor * MultaPer) / 100;
    ValTo := Valor + ValMu + ValJu;
    //
    GradeA.Cells[00, i] := IntToStr(i);
    GradeA.Cells[01, i] := Geral.FFT(Valor, 2, siNegativo);
    GradeA.Cells[02, i] := Geral.FDT(QrLctVencimento.Value, 3);
    GradeA.Cells[03, i] := Geral.FFT(DiasE, 6, siNegativo);
    GradeA.Cells[04, i] := Geral.FFT(JurosPer, 6, siNegativo);
    GradeA.Cells[05, i] := Geral.FFT(ValJu, 2, siNegativo);
    GradeA.Cells[06, i] := Geral.FFT(ValMu, 2, siNegativo);
    GradeA.Cells[07, i] := Geral.FFT(ValTo, 2, siNegativo);
    GradeA.Cells[09, i] := Geral.FFT(Valor * DiasE, 2, siNegativo);
    //
    SumOr := SumOr + Valor;
    SumJu := SumJu + ValJu;
    SumMu := SumMu + ValMu;
    SumTo := SumTo + ValTo;
    SumDD := SumDD + DiasE;
    SumVD := SumVD + Valor * DiasE;
    QrLct.Next;
  end;
  dmkEdit1.ValueVariant := SumOr;
  dmkEdit2.ValueVariant := SumJu;
  dmkEdit3.ValueVariant := SumMu;
  dmkEdit4.ValueVariant := SumTo;
  {
  if Qrbloqparcpar.RecordCount > 0 then
    dmkEdit5.ValueVariant := Qrbloqparcpar.
  else
    dmkEdit5.ValueVariant := 0;
  }
  dmkEdit7.ValueVariant := SumVD;

  //

  if SumOr > 0 then
    dmkEdit8.ValueVariant := SumVD / SumOr
  else
    dmkEdit8.ValueVariant := 0;

  //

  if SumDD > 0 then
    dmkEdit9.ValueVariant := SumVD / SumDD
  else
    dmkEdit9.ValueVariant := 0;

  // Calcular Itens de parcela
  i := 0;
  PerIni  := 0;

  ReabrirPesq('Calculando juros ...');
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO bloqparcits SET ');
  Dmod.QrUpdU.SQL.Add('Seq=:P0, Tipo=:P1, PercIni=:P2, PercFim=:P3, ');
  Dmod.QrUpdU.SQL.Add('VlrOrigi=:P4, VlrMulta=:P5, VlrJuros=:P6, ');
  Dmod.QrUpdU.SQL.Add('VlrTotal=:P7, VlrAjust=:P8, VlrSomas=:P9, ');
  Dmod.QrUpdU.SQL.Add('CtrlOrigi=:P10, BloqOrigi=:P11, SParcA=:P12, ');
  Dmod.QrUpdU.SQL.Add('SParcD=:P13, Dias=:P14, Parcela=:P15, ');
  Dmod.QrUpdU.SQL.Add('');
  Dmod.QrUpdU.SQL.Add('Codigo=:Pa, Controle=:Pb ');

  Qrbloqparcpar.First;

  ValParc2 := ObtemValorParcela;

  PercIni := 0;
  SumOrigi := 0;
  SumMulta := 0;
  SumJuros := 0;
  SumTotal := 0;
  TotVlrDD := 0;
  TotOrigi := 0;
  n := 1;
  i := 0;
  while not QrPesqlan.Eof do
  begin
    PBAtz.Position := PBAtz.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    CtrlOrigi := QrPesqlanControle.Value;
    BloqOrigi := QrPesqlanBLOQUETO.Value;
    //
    ConfiguraVariaveis2(DiasT, DiasE, JurosPer,
    OrigiVal, JurosVal, MultaVal, TotalVal);
    UltDta := QrbloqparcparVencimento.Value;
    AtuDta := UltDta;
    //
    if (ValParc2 + 0.00999) >= (TotalVal - SumTotal) then
    begin
      VlrOrigi := QrPesqlanCredito.Value - SumOrigi;
      VlrMulta := MultaVal - SumMulta;
      VlrJuros := JurosVal - SumJuros;
      VlrTotal := TotalVal - SumTotal;
      Vlr_Dias := JurosPer * 30 * VlrOrigi;
      PercFim := 100;
      //
      SumOrigi := 0;
      SumMulta := 0;
      SumJuros := 0;
      SumTotal := 0;
      TotVlrDD := TotVlrDD + Vlr_Dias;
      TotOrigi := TotOrigi + VlrOrigi;
      //
      InsereLinhaGradeB(i, QrPesqLan.RecNo, OrigiVal, QrPesqlanVencimento.Value,
        DiasE, JurosPer, JurosVal, MultaVal, TotalVal, NewParc, '', '',
        PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
       CtrlOrigi, BloqOrigi, n, 'A', ValParc2, ValParc2 - TotalVal,
       JurosPer * 30, Vlr_Dias);
      //
      //InsereItem(PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
       //CtrlOrigi, BloqOrigi, n, 'A', ValParc2, ValParc2 - TotalVal, Juros * 30);
        VlrAjust := InsereItem(PercIni, PercFim,
          VlrOrigi, VlrMulta, VlrJuros, VlrTotal, CtrlOrigi, BloqOrigi, n,
          'A', ValParc2, ValParc2 - TotalVal, JurosPer * 30, FatorRat,
          Definitivo);

      // atualiza valor restante da parcela a deduzir
      ValParc2 := ValParc2 - TotalVal - VlrAjust;
      SumAjust := SumAjust + VlrAjust;

      // acabou o parcelamento atual
      if (ValParc2 < 0.00999) then
      begin
        PercIni := 0;
        if Qrbloqparcpar.RecNo <> Qrbloqparcpar.RecordCount then
        begin
          Qrbloqparcpar.Next;

          ConfiguraVariaveis2(DiasT, DiasE, JurosPer,
            OrigiVal, JurosVal, MultaVal, TotalVal);
          //ValParc2 := QrbloqparcparValBol.Value;
          ValParc2 := ObtemValorParcela;
          AtuDta := UltDta;
          //UltDta := Int(QrbloqparcparVencimento.Value);
        end else begin
          ValParc2 := 0;
        end;;
      end;
    end else begin
      PercFim := 0;
      while (((TotalVal - SumTotal) > (ValParc2 + 0.00999) )
      and (ValParc2 > 0) ) do
      begin
        if TotalVal > 0 then
          Fator := ValParc2 / TotalVal * 100
        else
          Fator := 0;
        //
        //if Qrbloqparcpar.RecNo <> Qrbloqparcpar.RecordCount then
          PercFim := PercFim + Fator;
        //else
          //PercFim := 100;

        VlrOrigi := Round(Fator * QrPesqlanCredito.Value) / 100;
        SumOrigi := SumOrigi + VlrOrigi;

        VlrMulta := Round(Fator * MultaVal) / 100;
        SumMulta := SumMulta + VlrMulta;

        VlrJuros := Round(Fator * JurosVal) / 100;
        SumJuros := SumJuros + VlrJuros;

        VlrTotal := Round(Fator * TotalVal) / 100;
        SumTotal := SumTotal + VlrTotal;

        Vlr_Dias := JurosPer * 30 * VlrOrigi;
        TotVlrDD := TotVlrDD + Vlr_Dias;
        TotOrigi := TotOrigi + VlrOrigi;

        InsereLinhaGradeB(i, QrPesqLan.RecNo, OrigiVal, QrPesqlanVencimento.Value,
          DiasE, JurosPer, JurosVal, MultaVal, TotalVal, NewParc, '', '',
          PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
          CtrlOrigi, BloqOrigi, n, 'B', ValParc2, 0, JurosPer * 30, Vlr_Dias);
      //
        FatSumJur := FatSumJur + VlrJuros;
        VlrAjust := InsereItem(PercIni, PercFim,
          VlrOrigi, VlrMulta, VlrJuros, VlrTotal, CtrlOrigi, BloqOrigi, n,
          'B', ValParc2, 0, JurosPer * 30, FatorRat, Definitivo);
        //InsereItem(PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
          //CtrlOrigi, BloqOrigi, n, 'B', ValParc2, 0, Juros * 30);
        if Qrbloqparcpar.RecNo <> Qrbloqparcpar.RecordCount then
        begin
          Qrbloqparcpar.Next;

          ConfiguraVariaveis2(DiasT, DiasE, JurosPer,
            OrigiVal, JurosVal, MultaVal, TotalVal);

          // atualiza valor da parcela a deduzir
          ValParc2 := ObtemValorParcela;
          SumAjust := SumAjust + VlrAjust;

        end else begin
          //ValParc2 := 0;
        end;
        PercIni := PercFim;
      end;

      // restante do lancto pois � menor que a parcela
      if (TotalVal - SumTotal) >= 0.01 then
      begin
        PercFim := 100;
        Fator := 100 - PercIni;
        if Fator > 0 then
        begin
          //
          //VlrOrigi := Round(Fator * QrPesqlanCredito.Value) / 100;
          VlrOrigi := QrPesqlanCredito.Value - SumOrigi;

          VlrMulta := Round(Fator * MultaVal) / 100;

          VlrJuros := Round(Fator * JurosVal) / 100;

          VlrTotal := Round(Fator * TotalVal) / 100;

          Vlr_Dias := JurosPer * 30 * VlrOrigi;
          TotVlrDD := TotVlrDD + Vlr_Dias;
          TotOrigi := TotOrigi + VlrOrigi;

          InsereLinhaGradeB(i, QrPesqLan.RecNo, OrigiVal, QrPesqlanVencimento.Value,
            DiasE, JurosPer, JurosVal, MultaVal, TotalVal, NewParc, '', '',
            PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
            CtrlOrigi, BloqOrigi, n, 'C', ValParc2, ValParc2 - VlrTotal,
            JurosPer * 30, Vlr_Dias);
          //
            FatSumJur := FatSumJur + VlrJuros;
            VlrAjust := InsereItem(PercIni, PercFim,
              VlrOrigi, VlrMulta, VlrJuros, VlrTotal, CtrlOrigi, BloqOrigi, n,
              'C', ValParc2, ValParc2 - VlrTotal, JurosPer * 30, FatorRat,
              Definitivo);
          //InsereItem(PercIni, PercFim, VlrOrigi, VlrMulta, VlrJuros, VlrTotal,
            //CtrlOrigi, BloqOrigi, n, 'C', ValParc2, ValParc2 - VlrTotal,
            //JurosPer * 30);

          // atualiza valor restante da parcela a deduzir
          ValParc2 := ValParc2 - VlrTotal - VlrAjust;
          SumAjust := SumAjust + VlrAjust;

        end;
        SumMulta := 0;
        SumOrigi := 0;
        SumJuros := 0;
        SumTotal := 0;
        PercIni  := 0;
      end;
    end;
    //
    n := n + 1;
    QrPesqlan.Next;
  end;
  dmkEdit10.ValueVariant := TotVlrDD;
  if TotOrigi > 0 then
    dmkEdit11.ValueVariant := TotVlrDD / TotOrigi
  else
    dmkEdit11.ValueVariant := 0;

  //

  if FatSumJur = 0 then
    Result := 0
  else begin
    if FatSumJur <> 0 then
      Result := ValParc2 / FatSumJur
    else
      Result := 0;
  end;
end;

procedure TFmbloqparc.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if (ACol = 0) or (ARow = 0) then
  begin
  end else
    with GradeA.Canvas do
    begin
      if ACol in ([0]) then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          FmPrincipal.sd1.Colors[csButtonFace], taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
      if ACol in ([0]) then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          FmPrincipal.sd1.Colors[csButtonFace], taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          FmPrincipal.sd1.Colors[csButtonFace], taRightJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  //end;
    //MeuVCLSkin.DrawGrid3(GradeA, Rect, 0, Geral.IMV(GradeA.Cells[ACol, ARow]));

end;

procedure TFmbloqparc.GradeBDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if (ACol = 0) or (ARow = 0) then
  begin
  end else
    with GradeB.Canvas do
    begin
      if ACol in ([0]) then
        MyObjects.DesenhaTextoEmStringGrid(GradeB, Rect, clBlack,
          FmPrincipal.sd1.Colors[csButtonFace], taLeftJustify,
          GradeB.Cells[Acol, ARow], 1, 1, False)
      else
      if ACol in ([0]) then
        MyObjects.DesenhaTextoEmStringGrid(GradeB, Rect, clBlack,
          FmPrincipal.sd1.Colors[csButtonFace], taCenter,
          GradeB.Cells[Acol, ARow], 1, 1, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeB, Rect, clBlack,
          FmPrincipal.sd1.Colors[csButtonFace], taRightJustify,
          GradeB.Cells[Acol, ARow], 1, 1, False);
    end;
  //end;
    //MeuVCLSkin.DrawGrid3(GradeB, Rect, 0, Geral.IMV(GradeB.Cells[ACol, ARow]));

end;

procedure TFmbloqparc.SbImprimeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmbloqparc.QrbloqparcparBeforeClose(DataSet: TDataSet);
begin
  BtReCorrige.Enabled := False;
  Qrbloqparcits.Close;
  QrIncorr.Close;
  BtEdita.Enabled := False;
end;

procedure TFmbloqparc.QrBoletosCalcFields(DataSet: TDataSet);
begin
  QrBoletosUnidade.Value := QrbloqparcUnidade.Value;
  if QrBoletosBoleto.Value = 0 then
    QrBoletosBLOQUETO.Value := -1
  else
    QrBoletosBLOQUETO.Value := QrBoletosBoleto.Value;
  //
{
  QrBoletosVENCTO_TXT.Value := Geral.FDT(QrBoletosVencto.Value, 3);
  //
  if ( (Trim(QrBoletosUSERNAME.Value) <> '')
  and (Trim(QrBoletosPASSWORD.Value) <> '')) then
    QrBoletosPWD_WEB.Value := 'Login: ' + QrBoletosUSERNAME.Value +
    '   Senha: ' + QrBoletosPASSWORD.Value
  else QrBoletosPWD_WEB.Value := '';
}
end;

procedure TFmbloqparc.QrbloqparcparAfterScroll(DataSet: TDataSet);
begin
  Reopenbloqparcits(0);
  ReopenBPP_Pgt(0);
  //
  BtReCorrige.Enabled :=
    (FmPrincipal.FDatabase1A <> nil)
    and
    (FmPrincipal.FDatabase2A = nil)
    and
    (QrBPP_Pgt.RecordCount > 0);
  //
  BtReconfReparc.Enabled :=
    (FmPrincipal.FDatabase1A <> nil)
    and
    (FmPrincipal.FDatabase2A = nil);
end;

procedure TFmbloqparc.Reopenbloqparcits(Conta: Integer);
begin
  Qrbloqparcits.Close;
  Qrbloqparcits.Params[00].AsInteger := QrbloqparcCodigo.Value;
  Qrbloqparcits.Params[01].AsInteger := QrbloqparcparControle.Value;
  {
  Qrbloqparcits.SQL.Clear;
  Qrbloqparcits.SQL.Add('SELECT *');
  Qrbloqparcits.SQL.Add('FROM bloqparcits');
  Qrbloqparcits.SQL.Add('WHERE Controle="' +
    FormatFloat('0', QrbloqparcparControle.Value) + '"');
  }
  Qrbloqparcits.Open;
  //
  Qrbloqparcits.Locate('Conta', Conta, []);
end;

procedure TFmbloqparc.ReopenBPP_Pgt(Controle: Integer);
var
  CtrlTXT, LctoTXT: String;
begin

  QrBPP_Pgt.Close;
(*
SELECT lan.Controle, lan.Data, cta.Nome NOMECONTA,
lan.Genero, lan.Descricao, lan.Credito,
lan.MultaVal, lan.MoraVal
FROM lct lan
LEFT JOIN contas cta ON cta.Codigo=lan.Genero
WHERE lan.SubPgto1=:P0
*)

  QrBPP_ToP.Close;
(*
SELECT SUM(lan.Credito) Credito,
SUM(lan.MultaVal) MultaVal, SUM(lan.MoraVal) MoraVal
FROM lct lan
WHERE lan.SubPgto1=:P0
*)
  if QrbloqparcparControle.Value <> 0 then
  begin
    CtrlTXT := FormatFloat('0', QrbloqparcparControle.Value);
    QrBPP_Pgt.SQL.Clear;
    QrBPP_Pgt.SQL.Add('DROP TABLE IF EXISTS _BLQ_REPAR_001_BPP_PGT_;');
    QrBPP_Pgt.SQL.Add('');
    QrBPP_Pgt.SQL.Add('CREATE TABLE _BLQ_REPAR_001_BPP_PGT_');
    QrBPP_Pgt.SQL.Add('');
    QrBPP_Pgt.SQL.Add('SELECT lan.Controle, lan.Data, ' + FLocNomeConta);
    QrBPP_Pgt.SQL.Add('lan.Genero, lan.Descricao, lan.Credito,');
    QrBPP_Pgt.SQL.Add('lan.MultaVal, lan.MoraVal');
    QrBPP_Pgt.SQL.Add('FROM ' + FTabLctA + ' lan');
    QrBPP_Pgt.SQL.Add(FSetTablConta);
    QrBPP_Pgt.SQL.Add('WHERE lan.SubPgto1=' + CtrlTXT);
    QrBPP_Pgt.SQL.Add('');
    QrBPP_Pgt.SQL.Add('UNION');
    QrBPP_Pgt.SQL.Add('');
    QrBPP_Pgt.SQL.Add('SELECT lan.Controle, lan.Data, ' + FLocNomeConta);
    QrBPP_Pgt.SQL.Add('lan.Genero, lan.Descricao, lan.Credito,');
    QrBPP_Pgt.SQL.Add('lan.MultaVal, lan.MoraVal');
    QrBPP_Pgt.SQL.Add('FROM ' + FTabLctB + ' lan');
    QrBPP_Pgt.SQL.Add(FSetTablConta);
    QrBPP_Pgt.SQL.Add('WHERE lan.SubPgto1=' + CtrlTXT);
    QrBPP_Pgt.SQL.Add('');
    QrBPP_Pgt.SQL.Add('UNION');
    QrBPP_Pgt.SQL.Add('');
    QrBPP_Pgt.SQL.Add('SELECT lan.Controle, lan.Data, ' + FLocNomeConta);
    QrBPP_Pgt.SQL.Add('lan.Genero, lan.Descricao, lan.Credito,');
    QrBPP_Pgt.SQL.Add('lan.MultaVal, lan.MoraVal');
    QrBPP_Pgt.SQL.Add('FROM ' + FTabLctD + ' lan');
    QrBPP_Pgt.SQL.Add(FSetTablConta);
    QrBPP_Pgt.SQL.Add('WHERE lan.SubPgto1=' + CtrlTXT);
    QrBPP_Pgt.SQL.Add('');
    QrBPP_Pgt.SQL.Add(';');
    QrBPP_Pgt.SQL.Add('');
    QrBPP_Pgt.SQL.Add('SELECT * FROM _BLQ_REPAR_001_BPP_PGT_;');
    QrBPP_Pgt.SQL.Add('');
    QrBPP_Pgt.SQL.Add('DROP TABLE IF EXISTS _BLQ_REPAR_001_BPP_PGT_;');
    QrBPP_Pgt.SQL.Add('');
    QrBPP_Pgt.SQL.Add('');
    //dmkPF.LeMeuTexto(QrBPP_Pgt.SQL.Text);
    QrBPP_Pgt.Open;
    //
    QrBPP_Pgt.Locate('Controle', Controle, []);
    //
    QrBPP_ToP.SQL.Clear;
    QrBPP_ToP.SQL.Add('DROP TABLE IF EXISTS _BLQ_REPAR_001_BPP_TOP_;');
    QrBPP_ToP.SQL.Add('');
    QrBPP_ToP.SQL.Add('CREATE TABLE _BLQ_REPAR_001_BPP_TOP_');
    QrBPP_ToP.SQL.Add('');
    QrBPP_ToP.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    QrBPP_ToP.SQL.Add('SUM(lan.MultaVal) MultaVal, SUM(lan.MoraVal) MoraVal');
    QrBPP_ToP.SQL.Add('FROM ' + FTabLctA + ' lan');
    QrBPP_ToP.SQL.Add('WHERE lan.SubPgto1=' + CtrlTXT);
    QrBPP_ToP.SQL.Add('');
    QrBPP_ToP.SQL.Add('UNION');
    QrBPP_ToP.SQL.Add('');
    QrBPP_ToP.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    QrBPP_ToP.SQL.Add('SUM(lan.MultaVal) MultaVal, SUM(lan.MoraVal) MoraVal');
    QrBPP_ToP.SQL.Add('FROM ' + FTabLctB + ' lan');
    QrBPP_ToP.SQL.Add('WHERE lan.SubPgto1=' + CtrlTXT);
    QrBPP_ToP.SQL.Add('');
    QrBPP_ToP.SQL.Add('UNION');
    QrBPP_ToP.SQL.Add('');
    QrBPP_ToP.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    QrBPP_ToP.SQL.Add('SUM(lan.MultaVal) MultaVal, SUM(lan.MoraVal) MoraVal');
    QrBPP_ToP.SQL.Add('FROM ' + FTabLctD + ' lan');
    QrBPP_ToP.SQL.Add('WHERE lan.SubPgto1=' + CtrlTXT);
    QrBPP_ToP.SQL.Add('');
    QrBPP_ToP.SQL.Add(';');
    QrBPP_ToP.SQL.Add('');
    QrBPP_ToP.SQL.Add('SELECT SUM(Credito) Credito,');
    QrBPP_ToP.SQL.Add('SUM(MultaVal) MultaVal, SUM(MoraVal) MoraVal');
    QrBPP_ToP.SQL.Add('FROM _BLQ_REPAR_001_BPP_TOP_;');
    QrBPP_ToP.SQL.Add('');
    QrBPP_ToP.SQL.Add('DROP TABLE IF EXISTS _BLQ_REPAR_001_BPP_TOP_;');
    QrBPP_ToP.Open;
  end;
  //
  QrIncorr.Close;
  if QrbloqparcparLancto.Value <> 0 then
  begin
    LctoTXT := FormatFloat('0', QrbloqparcparLancto.Value);
(*
SELECT Controle, Data, Descricao, Credito, Tipo, Carteira, Sit
FROM lct
WHERE Genero=-10
AND ID_Pgto=:P0
*)
    QrIncorr.SQL.Clear;
    QrIncorr.SQL.Add('DROP TABLE IF EXISTS _BLQ_REPAR_001_INCORR_;');
    QrIncorr.SQL.Add('');
    QrIncorr.SQL.Add('CREATE TABLE _BLQ_REPAR_001_INCORR_');
    QrIncorr.SQL.Add('');
    QrIncorr.SQL.Add('SELECT Controle, Data, Descricao, Credito, Tipo, Carteira, Sit');
    QrIncorr.SQL.Add('FROM ' + FTabLctA);
    QrIncorr.SQL.Add('WHERE Genero=-10');
    QrIncorr.SQL.Add('AND ID_Pgto=' + LctoTXT);
    QrIncorr.SQL.Add('');
    QrIncorr.SQL.Add('UNION');
    QrIncorr.SQL.Add('');
    QrIncorr.SQL.Add('SELECT Controle, Data, Descricao, Credito, Tipo, Carteira, Sit');
    QrIncorr.SQL.Add('FROM ' + FTabLctB);
    QrIncorr.SQL.Add('WHERE Genero=-10');
    QrIncorr.SQL.Add('AND ID_Pgto=' + LctoTXT);
    QrIncorr.SQL.Add('');
    QrIncorr.SQL.Add('UNION');
    QrIncorr.SQL.Add('');
    QrIncorr.SQL.Add('SELECT Controle, Data, Descricao, Credito, Tipo, Carteira, Sit');
    QrIncorr.SQL.Add('FROM ' + FTabLctD);
    QrIncorr.SQL.Add('WHERE Genero=-10');
    QrIncorr.SQL.Add('AND ID_Pgto=' + LctoTXT);
    QrIncorr.SQL.Add('');
    QrIncorr.SQL.Add(';');
    QrIncorr.SQL.Add('');
    QrIncorr.SQL.Add('SELECT * FROM _BLQ_REPAR_001_INCORR_;');
    QrIncorr.SQL.Add('');
    QrIncorr.SQL.Add('DROP TABLE IF EXISTS _BLQ_REPAR_001_INCORR_;');
    QrIncorr.Open;
  end;
  //
end;

procedure TFmbloqparc.ReopenCondImov;
var
  Cond, Propriet: Integer;
begin
  EdCondImov.Text := '';
  CBCondImov.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  Propriet := Geral.IMV(EdPropriet.Text);
  QrCondImov.Close;
  QrCondImov.SQL.Clear;
  QrCondImov.SQL.Add('SELECT cim.Conta, cim.Unidade');
  QrCondImov.SQL.Add('FROM condimov cim');
  QrCondImov.SQL.Add('WHERE cim.Codigo<>0');
  QrCondImov.SQL.Add('AND cim.Propriet<>0');
  QrCondImov.SQL.Add('');
  if Cond <> 0 then
    QrCondImov.SQL.Add('AND cim.Codigo=' + FormatFloat('0', Cond));
  if Propriet <> 0 then
    QrCondImov.SQL.Add('AND cim.Propriet=' + FormatFloat('0', Propriet));
  QrCondImov.SQL.Add('');
  QrCondImov.SQL.Add('ORDER BY cim.Unidade');
  QrCondImov.Open;
end;

function TFmBloqParc.InsereReparceLct(): Boolean;
var
  JurosPerc, MultaPerc: Double;
  CartEmiss: Integer;
begin
  if not DefineDadosCNAB_Cfg(QrbloqparcCond.Value, QrbloqparcCNAB_Cfg.Value,
    JurosPerc, MultaPerc, CartEmiss) then
  begin
    Result := False;
    Exit;
  end;
  //
  Dmod.QrUpd2.SQL.Clear;
  Dmod.QrUpd2.SQL.Add('UPDATE bloqparcpar SET Lancto=:P0 WHERE Controle=:P1');
  //
  DmCond.ReopenQrCond(QrbloqparcCodCliEsp.Value);
  //
  Qrbloqparcpar.First;
  while not Qrbloqparcpar.Eof do
  begin
    UFinanceiro.LancamentoDefaultVARS;
    //
    FLAN_Data       := Geral.FDT(QrbloqparcDataP.Value, 1);
    FLAN_Tipo       := 2;
    FLAN_Documento  := Trunc(QrbloqparcparFatNum.Value);
    FLAN_Credito    := QrbloqparcparValBol.Value;
    FLAN_MoraDia    := JurosPerc;
    FLAN_Multa      := MultaPerc;
    FLAN_Carteira   := CartEmiss;
    FLAN_Genero     := FGeneroParcelamento;
    FLAN_Cliente    := QrbloqparcCodigoEnt.Value;
    FLAN_CliInt     := DmCond.QrCondCliente.Value;
    FLAN_Depto      := QrbloqparcCodigoEsp.Value;
    FLAN_ForneceI   := QrbloqparcCodigoEnt.Value;
    FLAN_Vencimento := Geral.FDT(QrbloqparcparVencimento.Value, 1);
    FLAN_Mez        := Geral.IMV(MLAGeral.DataToMez(QrbloqparcparVencimento.Value));
    FLAN_FatID      := FFatIDParcelamento;
    FLAN_FatNum     := Trunc(QrbloqparcparFatNum.Value);
    FLAN_FatParcela := QrbloqparcparParcela.Value;
    FLAN_Descricao  := 'Reparc. UH ' +
                       QrbloqparcUnidade.Value + ' parc. ' +
                       FormatFloat('0', QrbloqparcparParcela.Value) + '/' +
                       FormatFloat('0', Qrbloqparcpar.RecordCount) + ' (Reparc. ' +
                       FormatFloat('0', QrbloqparcCodigo.Value) + ')';
    FLAN_Atrelado   := QrbloqparcparControle.Value;
    FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', FTabLctA, LAN_CTOS, 'Controle');
    //
    if UFinanceiro.InsereLancamento(FTabLctA) then
    begin
      Dmod.QrUpd2.SQL.Clear;
      Dmod.QrUpd2.SQL.Add('UPDATE bloqparcpar SET Lancto=:P0 WHERE Controle=:P1');
      Dmod.QrUpd2.Params[00].AsInteger := FLAN_Controle;
      Dmod.QrUpd2.Params[01].AsInteger := QrbloqparcparControle.Value;
      Dmod.QrUpd2.ExecSQL;
      //Acertar Lancto nos reparcelamentos existentes
    end;
    Qrbloqparcpar.Next;
  end;
  Result := True;
end;

function TFmBloqParc.StatusBloqParcMenorQue(Status, Codigo: Integer): Boolean;
begin
  QrStatus.Close;
  QrStatus.Params[0].AsInteger := Codigo;
  QrStatus.Open;
  //
  Result :=  (QrStatusNovo.Value > 0) and (QrStatusNovo.Value < Status);
end;

function TFmBloqParc.StatusBloqParcAtual(Codigo: Integer): Integer;
begin
  QrStatus.Close;
  QrStatus.Params[0].AsInteger := Codigo;
  QrStatus.Open;
  //
  Result :=  QrStatusNovo.Value;
end;

{  Parei Aqui 2008 04 28
Mostrar lancamentos de parcelas
Deixar exclui parcelas e lan�amentos?
colocar filtro de UH nos lancamentos
}

procedure TFmbloqparc.DBGPesq_DadosDblClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  if LocCod(QrPesqCodigo.Value, QrPesqCodigo.Value) then
  begin
    Qrbloqparcpar.Locate('Controle', QrPesqControle.Value, []);
    PageControl1.ActivePageIndex := 0;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmbloqparc.DBGPesq_DadosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  MyObjects.DBGridLarguraSegundoTitulo(DBGPesq_Titul, DBGPesq_Dados, FCols);
end;

function TFmbloqparc.DefineDadosCNAB_Cfg(const Cond, RepCNAB_Cfg: Integer;
  var JurosPerc, MultaPerc: Double; var CartEmiss: Integer): Boolean;
begin
  CartEmiss := 0;
  JurosPerc := 0;
  MultaPerc := 0;
  //
  Result := False;
  //
  if RepCNAB_Cfg <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT CartEmiss, JurosPerc, MultaPerc ',
      'FROM cnab_cfg ',
      'WHERE Codigo=' + Geral.FF0(RepCNAB_Cfg),
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      CartEmiss := QrLoc.FieldByName('CartEmiss').AsInteger;
      JurosPerc := QrLoc.FieldByName('JurosPerc').AsInteger;
      MultaPerc := QrLoc.FieldByName('MultaPerc').AsInteger;
      //
      Result := True;
    end;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT CartEmiss, PercJuros, PercMulta ',
      'FROM cond ',
      'WHERE Codigo=' + Geral.FF0(Cond),
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      CartEmiss := QrLoc.FieldByName('CartEmiss').AsInteger;
      JurosPerc := QrLoc.FieldByName('PercJuros').AsInteger;
      MultaPerc := QrLoc.FieldByName('PercMulta').AsInteger;
      //
      Result := True;
    end;
  end;
end;

procedure TFmBloqParc.DefineDatabaseTabelas();
var
  BaseDadosA, BaseDadosB: TmySQLDatabase;
begin
  if FmPrincipal.FDatabase1A <> nil then
  begin
    BasedadosA := FmPrincipal.FDatabase1A;
    BasedadosB := FmPrincipal.FDatabase1B;
    BtRatificaParc.Visible := False;
    PnCalc.Visible := False;
    //PnAtz.Visible := False;
    FSelDB := TMeuDB;
    FLocNomeConta := 'cta.Nome NOMECONTA,';
    FSetTablConta := 'LEFT JOIN ' + FSelDB + '.contas cta ON cta.Codigo=lan.Genero';
  end else begin
    BasedadosA := FmPrincipal.FDatabase2A;
    BasedadosB := FmPrincipal.FDatabase2B;
    BtRatificaParc.Visible := True;
    PnCalc.Visible := True;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando');
    // mostra quando abre normal !!!???
    //PnAtz.Visible := True;
    FSelDB := '';
    FLocNomeConta := '"" NOMECONTA,';
    FSetTablConta := '';
  end;
  //
  Qrbloqparc.Database    := BasedadosA;
  Qrbloqparcpar.Database := BasedadosA;
  QrLct.Database         := BasedadosA;
  QrPesqlan.Database     := BasedadosA;
  QrStatus.Database      := BasedadosA;
  QrPesqL.Database       := BasedadosA;
  //
  QrBloqLan.Database     := BasedadosB;
  QrBPP_Pgt.Database     := BasedadosB;
  QrBPP_ToP.Database     := BasedadosB;
  QrIncorr.Database      := BasedadosB;
  //
  DefParams;
  Va(vpLast);
end;

procedure TFmbloqparc.DefineFrx(frx: TfrxReport);
begin
  frxDsBloqlan.DataSet           := QrBloqlan;
  frxDsBloqParc.DataSet          := Qrbloqparc;
  frxDsBloqParcPar.DataSet       := Qrbloqparcpar;
  frxDsBoletos.DataSet           := QrBoletos;
  frxDsBPI.DataSet               := QrBPI;
  frxDsLcto.DataSet              := Qrlct;
  DModG.frxDsDono.DataSet        := DModG.QrDono;
  DModG.frxDsMaster.DataSet      := DModG.QrMaster;
  DmBloq.frxDsInquilino.DataSet  := DmBloq.QrInquilino;
  DmCond.frxDsCond.DataSet       := DmCond.QrCond;
  frxDsCNAB_Cfg_B.DataSet        := QrCNAB_Cfg_B;
  //
  MyObjects.frxDefineDataSets(frx, [
    frxDsBloqlan,
    frxDsBloqParc,
    frxDsBloqParcPar,
    frxDsBoletos,
    frxDsBPI,
    frxDsLcto,
    DModG.frxDsDono,
    DModG.frxDsMaster,
    DmBloq.frxDsInquilino,
    DmCond.frxDsCond,
    frxDsCNAB_Cfg_B
    ]);
end;

procedure TFmBloqParc.SomasBloqParc();
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE bloqparcpar SET ');
  Dmod.QrUpd.SQL.Add('VlrOrigi=:P0, VlrMulta=:P1, ');
  Dmod.QrUpd.SQL.Add('VlrJuros=:P2, VlrTotal=:P3 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
  //
  Dmod.QrWeb.SQL := Dmod.QrUpd.SQL;
  //
  QrSomas.Close;
  QrSomas.SQL.Clear;
  QrSomas.SQL.Add('SELECT SUM(VlrOrigi) VlrOrigi, SUM(VlrMulta) VlrMulta,');
  QrSomas.SQL.Add('SUM(VlrJuros+VlrAjust) VlrJuros, SUM(VlrTotal) VlrTotal');
  QrSomas.SQL.Add('FROM bloqparcits');
  QrSomas.SQL.Add('WHERE Controle=:Pa');
  Qrbloqparcpar.First;
  while not Qrbloqparcpar.Eof do
  begin
    QrSomas.Close;
    QrSomas.Params[0].AsInteger := QrbloqparcparControle.Value;
    QrSomas.Open;
    //
    Dmod.QrUpd.Params[00].AsFloat   := QrSomasVlrOrigi.Value;
    Dmod.QrUpd.Params[01].AsFloat   := QrSomasVlrMulta.Value;
    Dmod.QrUpd.Params[02].AsFloat   := QrSomasVlrJuros.Value;
    Dmod.QrUpd.Params[03].AsFloat   := QrSomasVlrTotal.Value;
    //
    Dmod.QrUpd.Params[04].AsInteger := QrbloqparcparControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrWeb.Params := Dmod.QrUpd.Params;
    Dmod.QrWeb.ExecSQL;
    //
    Qrbloqparcpar.Next;
  end;

  //

  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE bloqparc SET ');
  Dmod.QrUpd.SQL.Add('VlrOrigi=:P0, VlrMulta=:P1, ');
  Dmod.QrUpd.SQL.Add('VlrJuros=:P2, VlrTotal=:P3 ');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  //
  QrSomas.Close;
  QrSomas.SQL.Clear;
  QrSomas.SQL.Add('SELECT SUM(VlrOrigi) VlrOrigi, SUM(VlrMulta) VlrMulta,');
  QrSomas.SQL.Add('SUM(VlrJuros+VlrAjust) VlrJuros, SUM(VlrTotal) VlrTotal');
  QrSomas.SQL.Add('FROM bloqparcits ');
  QrSomas.SQL.Add('WHERE Codigo=:Pa');
  QrSomas.Params[0].AsInteger := QrbloqparcCodigo.Value;
  QrSomas.Open;
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSomasVlrOrigi.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSomasVlrMulta.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSomasVlrJuros.Value;
  Dmod.QrUpd.Params[03].AsFloat   := QrSomasVlrTotal.Value;
  Dmod.QrUpd.Params[04].AsInteger := QrbloqparcCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrWeb.SQL := Dmod.QrUpd.SQL;
  Dmod.QrWeb.Params := Dmod.QrUpd.Params;
  Dmod.QrWeb.ExecSQL;
end;

procedure TFmbloqparc.DistribuiRestos();
  procedure AtzBlocParcIts(Codigo: Integer);
  begin
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE bloqparcits SET');
    Dmod.QrUpdM.SQL.Add('VlrTotal=VlrOrigi+VlrMulta+VlrJuros+VlrAjust');
    Dmod.QrUpdM.SQL.Add('WHERE Codigo=:P0 ');
    Dmod.QrUpdM.Params[0].AsInteger := Codigo;
    Dmod.QrUpdM.ExecSQL;
    //
  end;
var
  Resto, Fator: Double;
  Codigo: Integer;
begin
  //Exit;

  Codigo := QrbloqparcCodigo.Value;
  AtzBlocParcIts(Codigo);
  //
  QrSomaBPI.Close;
  QrSomaBPI.Params[0].AsInteger := Codigo;
  QrSomaBPI.Open;
  //
  QrSomaBPP.Close;
  QrSomaBPP.Params[0].AsInteger := Codigo;
  QrSomaBPP.Open;
  //
  if QrSomaBPIVlrJuros.Value <> 0 then
  begin
    Resto := QrSomaBPPValBol.Value - QrSomaBPIVlrSomas.Value;
    Fator := Resto / QrSomaBPIVlrJuros.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE bloqparcits SET ');
    Dmod.QrUpd.SQL.Add('VlrAjust=VlrJuros*:P0 ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 ');
    Dmod.QrUpd.Params[00].AsFloat   := Fator;
    Dmod.QrUpd.Params[01].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    AtzBlocParcIts(Codigo);
    //
    QrSomaBPI.Close;
    QrSomaBPI.Params[0].AsInteger := Codigo;
    QrSomaBPI.Open;
    //
    QrSomaBPP.Close;
    QrSomaBPP.Params[0].AsInteger := Codigo;
    QrSomaBPP.Open;
    //
  end;
  Resto := QrSomaBPPValBol.Value - QrSomaBPIVlrTotal.Value;
  QrIts.Close;
  QrIts.Params[0].AsInteger := Codigo;
  QrIts.Open;
  if QrIts.RecordCount > 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE bloqparcits SET ');
    Dmod.QrUpd.SQL.Add('VlrAjust=VlrAjust+:P0 ');
    Dmod.QrUpd.SQL.Add('WHERE Conta=:P1 ');
    //
    //Exit;
    if Resto > 0 then Fator := 0.01 else Fator := -0.01;
    while Resto <> 0 do
    begin
      QrIts.First;
      while not QrIts.Eof do
      begin
        if (Resto <= -0.00999) or (Resto >= 0.00999) then
        begin
          Resto := Resto - Fator;
          Dmod.QrUpd.Params[00].AsFloat   := Fator;
          Dmod.QrUpd.Params[01].AsInteger := QrItsConta.Value;
          Dmod.QrUpd.ExecSQL;
        end else
        begin
          AtzBlocParcIts(Codigo);
          Exit;
        end;
        QrIts.Next;
      end;
    end;
  end else Geral.MensagemBox('N�o h� itens para serem divididos os restos!',
  'Aviso', MB_OK+MB_ICONWARNING);
  //
  AtzBlocParcIts(Codigo);
end;

procedure TFmbloqparc.EdCondImovChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
end;

procedure TFmbloqparc.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    PnPesq1.Visible := False;
    ReopenPropriet(False);
    ReopenCondImov();
  end;
end;

procedure TFmbloqparc.EdFatNumChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
end;

procedure TFmbloqparc.EdProprietChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
  ReopenCondImov;
end;

procedure TFmbloqparc.EdProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmbloqparc.EdReparcelChange(Sender: TObject);
begin
  PnPesq1.Visible := False;
end;

procedure TFmbloqparc.BtEditaClick(Sender: TObject);
begin
  if Qrbloqparcpar.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o h� bloqueto para ser alterado!');
    Exit;
  end;
  if (QrIncorr.State <> dsInactive) and (QrIncorr.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Altera��o cancelada!' + sLineBreak +
      'Motivo: A parcela selecionada j� possui pagamentos!');
    Exit;
  end;
  if (QrBPP_Pgt.State <> dsInactive) and (QrBPP_Pgt.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Altera��o cancelada!' + sLineBreak +
      'Motivo: A parcela selecionada j� possui pagamentos ratificados!');
    Exit;
  end;
  Application.CreateForm(TFmbloqparcpar, Fmbloqparcpar);
  Fmbloqparcpar.FTabLctA := FTabLctA;
  Fmbloqparcpar.TPVencimento.Date := QrbloqparcparVencimento.Value;
  Fmbloqparcpar.dmkEdCredito.ValueVariant := QrbloqparcparValBol.Value;
  Fmbloqparcpar.ShowModal;
  Fmbloqparcpar.Destroy;
end;

procedure TFmbloqparc.QrbloqparcparAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita :=
    ((FmPrincipal.FDatabase2A <> nil)
    and
    (Qrbloqparcpar.RecordCount > 0)
    and
    (QrbloqparcparCodigo.Value > 0))
    or
    ((FmPrincipal.FDatabase1A <> nil)
    and
    (Qrbloqparcpar.RecordCount > 0)
    and
    (QrbloqparcparCodigo.Value < 0));
  BtEdita.Enabled := Habilita;
  // ?? Parei aqui!
  BtExclui.Enabled := Habilita;
end;

procedure TFmbloqparc.BtExcluiClick(Sender: TObject);
var
  //Genero, FatID, Atrelado,
  BloqParc: Integer;
  Parcela, Bloqueto, Controle, wTabLctA: String;
begin
  //DsBPP_Pgt correr a tabela e verificar todas as parccelas

  if (QrIncorr.State <> dsInactive) and (QrIncorr.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Este parcelamento j� possui pagamentos!' +
      sLineBreak + 'Para exclu�-lo voc� deve desfazer os pagamentos antes!');
    Exit;
  end;
  if (QrBPP_Pgt.State <> dsInactive) and (QrBPP_Pgt.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Este parcelamento j� possui pagamentos ratificados!' +
      sLineBreak + 'Para exclu�-lo voc� deve desfazer os pagamentos antes!');
    Exit;
  end;
  //
  if Geral.MensagemBox('Confirma a EXCLUS�O de todo reparcelamento ' +
    'e REVERS�O para vencidos dos lan�amentos que foram reparcelados?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    //PnAtz.Visible := True;
    PageControl2.ActivePageIndex := 1;
    Update;
    Application.ProcessMessages;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo lan�amentos criados (Bloquetos)');

    BloqParc := QrbloqparcCodigo.Value;

    wTabLctA := DmodG.ObtemLctA_Para_Web(FTabLctA);

    // mudei 2010-07-26 n�o atualiza Lancto na WEB ent�o vou usar o local
(*
    QrLocLct.Close;
SELECT Genero, FatID, Atrelado
FROM lct
WHERE Controle=:P0
    QrLocLct.Params[0].AsInteger := QrbloqparcparLancto.Value;
    QrLocLct.Open;
    Genero   := QrLocLctGenero.Value;
    FatID    := QrLocLctFatID.Value;
    Atrelado := QrLocLctAtrelado.Value;
    if (Genero <> 0) and (FatID <> 0) and (Atrelado <> 0) then
    begin
      UMyMod.SQLDel2(Dmod.QrWeb, Qrbloqparcpar, VAR LCT, '', ['Genero','FatID','Atrelado'], [Genero,FatID,Atrelado], False, '');
      UMyMod.SQLDel2(Dmod.QrUpd, Qrbloqparcpar, VAR LCT, '', ['Genero','FatID','Atrelado'], [Genero,FatID,Atrelado], False, '');
    end;
*)
    Qrbloqparcpar.First;
    while not Qrbloqparcpar.Eof do
    begin
      Parcela  := FormatFloat('0', QrbloqparcparParcela.Value);//1
      Bloqueto := FormatFloat('0', QrbloqparcparFatNum.Value);//990341
      Controle := FormatFloat('0', QrbloqparcparControle.Value);//341
      //
      QrPesqL.Close;
      QrPesqL.SQL.Clear;
      QrPesqL.SQL.Add('SELECT Data, Tipo, Carteira, Controle, Sub');
      QrPesqL.SQL.Add('FROM ' + FTabLctA);
      QrPesqL.SQL.Add('WHERE Genero=' + FormatFloat('0', FGeneroParcelamento));
      QrPesqL.SQL.Add('AND FatID=' + FormatFloat('0', FFatIDParcelamento));
      QrPesqL.SQL.Add('AND Atrelado=' + Controle);
      QrPesqL.SQL.Add('AND FatNum=' + Bloqueto);
      QrPesqL.SQL.Add('AND FatParcela=' + Parcela);
      QrPesqL.Open;
      //
      case QrPesqL.RecordCount of
        0: Geral.MensagemBox('N�o foi localizado o lan�amento da parcela '
        + Parcela + ' (Bloqueto ' + Bloqueto +
        '). Caso ele exista, dever� ser exclu�do manualmente!',
        'Aviso', MB_OK+MB_ICONWARNING);
        1:
        begin
          //UMyMod.SQLDel2(Dmod.QrWeb, nil, wTabLctA, '', ['Controle'], [QrPesqLControle.Value], False, '');

          UFinanceiro.ExcluiLct_Unico(wTabLctA, Dmod.QrWeb.DataBase, QrPesqLData.Value,
          QrPesqLTipo.Value, QrPesqLCarteira.Value, QrPesqLControle.Value,
          QrPesqLSub.Value, CO_MOTVDEL_310_EXCLUILCTREPARC, False, False);
          {
          UFinanceiro.ExcluiLct_Unico(FTabLctA, Dmod.QrWeb.DataBase, QrPesqLData.Value,
          QrPesqLTipo.Value, QrPesqLCarteira.Value, QrPesqLControle.Value,
          QrPesqLSub.Value, False);
          }

          //UMyMod.SQLDel2(Dmod.QrUpd, nil, FTabLctA, '', ['Controle'], [QrPesqLControle.Value], False, '');
          UFinanceiro.ExcluiLct_Unico(FTabLctA, Dmod.QrUpd.Database, QrPesqLData.Value,
            QrPesqLTipo.Value, QrPesqLCarteira.Value, QrPesqLControle.Value,
            QrPesqLSub.Value, CO_MOTVDEL_310_EXCLUILCTREPARC, False);
        end;
        else Geral.MensagemBox('Foram localizados ' + FormatFloat('0',
        QrPesqL.RecordCount) + ' lan�amentos para a parcela '
        + Parcela + ' (Bloqueto ' + Bloqueto + ').' + #13#10 +
        'Por seguran�a nenhum foi exclu�do. O lan�amento correto dever� ser exclu�do manualmente!',
        'Aviso', MB_OK+MB_ICONWARNING);
        //
      end;
      Qrbloqparcpar.Next;
    end;
    Qrbloqparcpar.First;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Alterando itens "Web"');

    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('UPDATE ' + wTabLctA + ' SET Reparcel=0 ');
    Dmod.QrWeb.SQL.Add('WHERE Reparcel=' + FormatFloat('0', BloqParc));
    Dmod.QrWeb.ExecSQL;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Alterando itens "Local"');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA + ' SET Reparcel=0 ');
    Dmod.QrUpd.SQL.Add('WHERE Reparcel=' + FormatFloat('0', BloqParc));
    Dmod.QrUpd.ExecSQL;
    //
{
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Alterando itens "Web"');
    DBCheck.QuaisItens_Altera(Dmod.QrWeb, QrLct, TDBGrid(DmkDBGrid1), VAR LCT,
      ['Reparcel'], ['Controle'], ['Controle'], [0], istTodos, False, False);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Alterando itens "Local"');
    DBCheck.QuaisItens_Altera(Dmod.QrUpd, QrLct, TDBGrid(DmkDBGrid1), VAR LCT,
      ['Reparcel'], ['Controle'], ['Controle'], [0], istTodos, False, False);
    //
}
    //  fim 2010-07-26
    PageControl1.ActivePageIndex := 0;
    Update;
    Application.ProcessMessages;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo itens de parcelamento "Web"');
    UMyMod.SQLDel1(Dmod.QrWeb, Qrbloqparcits, 'bloqparcits', 'Codigo', BloqParc, False, '', True);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo itens de parcelamento "Local"');
    UMyMod.SQLDel1(Dmod.QrUpd, Qrbloqparcits, 'bloqparcits', 'Codigo', BloqParc, False, '', True);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo parcelas "Web"');
    UMyMod.SQLDel1(Dmod.QrWeb, Qrbloqparcpar, 'bloqparcpar', 'Codigo', BloqParc, False, '', True);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo parcelas "Local"');
    UMyMod.SQLDel1(Dmod.QrUpd, Qrbloqparcpar, 'bloqparcpar', 'Codigo', BloqParc, False, '', True);
    //
{
    //
    //  N�o pode mais ser exclu�do, porque se for exclu�do n�o se
    //  acha mais de quem �, e a� n�o se sabe a tabela de lan�amentos
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo cabe�alho "Web"');
    UMyMod.SQLDel1(Dmod.QrWeb, Qrbloqparc, 'bloqparc', 'Codigo', BloqParc, False, '', True);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo cabe�alho "Local"');
    UMyMod.SQLDel1(Dmod.QrUpd, Qrbloqparc, 'bloqparc', 'Codigo', BloqParc, False, '', True);
}
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Alterando cabe�alho "Web"');
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('UPDATE bloqparc SET Novo=-9 ');
    Dmod.QrWeb.SQL.Add('WHERE Codigo=' + FormatFloat('0', BloqParc));
    Dmod.QrWeb.ExecSQL;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Alterando cabe�alho "Local"');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE bloqparc SET Novo=-9 ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=' + FormatFloat('0', BloqParc));
    Dmod.QrUpd.ExecSQL;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //PnAtz.Visible := False;
    //
    LocCod(BloqParc, BloqParc);
  end;
end;

procedure TFmbloqparc.BtRatificaQuitClick(Sender: TObject);
const
  ActivePageIndex = 3;
begin
  Screen.Cursor := crHourGlass;
  try
    DmAtencoes.AtualizaParcelaReparcNovo(QrbloqparcCodigo.Value,
      QrbloqparcparControle.Value, QrIncorrControle.Value,
      QrbloqparcparLancto.Value, PBAtz1, LaAtz1, PnAtz1, PageControl3,
      ActivePageIndex, QrBPP_Pgt, FTabLctA);
    //
    Reopenbloqparcpar(QrbloqparcparControle.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmbloqparc.QrIncorrBeforeClose(DataSet: TDataSet);
begin
  BtRatificaQuit.Enabled := False;
end;

procedure TFmbloqparc.QrIncorrAfterOpen(DataSet: TDataSet);
begin
  BtRatificaQuit.Enabled :=
    (QrIncorr.RecordCount > 0)
  and
    (FmPrincipal.FDatabase1A <> nil)
  and
    (FmPrincipal.FDatabase2A = nil);
end;

procedure TFmbloqparc.CkIniParClick(Sender: TObject);
begin
  PnPesq1.Visible := False;
end;

{
procedure TFmbloqparc.CriaABSQuery();
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE Teste001; ');
  Query.SQL.Add('CREATE TABLE Teste001 (');
  Query.SQL.Add('  Ordem     Integer     ,');
  Query.SQL.Add('  Origi     float       ,');
  Query.SQL.Add('  Dias      float       ,');
  Query.SQL.Add('  Fator     float       ,');
  Query.SQL.Add('  ValDif    float       ,');
  Query.SQL.Add('  ValItem   float       ,');
  Query.SQL.Add('  RestaFat  float       ,');
  Query.SQL.Add('  RestaDif  float       ,');
  Query.SQL.Add('  Ativo     Integer      ');
  Query.SQL.Add(');');
  //
end;

procedure TFmbloqparc.InsABSQuery(Origi, Dias, Fator, ValDif, ValItem, RestaFat,
RestaDif: Double);
begin
  FOrdem := FOrdem + 1;
  Query.SQL.Add('INSERT INTO teste001 (' +
  'Ordem,Origi,Dias,Fator,ValDif,RestaFat,RestaDif,ValItem) VALUES (' +
    dmkPF.FFP(FOrdem,  1) + ', ' +
    dmkPF.FFP(Origi,   2) + ', ' +
    dmkPF.FFP(Dias,    0) + ', ' +
    dmkPF.FFP(Fator,   6) + ', ' +
    dmkPF.FFP(ValDif,  2) + ', ' +
    dmkPF.FFP(ValItem, 2) + ', ' +
    dmkPF.FFP(RestaFat,6) + ', ' +
    dmkPF.FFP(RestaDif,2) +
  ');');
end;

procedure TFmbloqparc.ReabreQuery();
begin
  Query.Close;
  Query.SQL.Add('SELECT * FROM teste001 ORDER BY Ordem;');
  Query.Open;
  //
  QrSum.Close;
  QrSum.SQL.Clear;
  QrSum.SQL.Add('SELECT SUM(Origi) Origi, SUM(Dias) Dias, SUM(Fator) Fator, ');
  QrSum.SQL.Add('SUM(ValDif) ValDif, SUM(ValItem) ValItem FROM teste001;');
  QrSum.SQL.Add('');
  QrSum.Open;
end;
}

end.

