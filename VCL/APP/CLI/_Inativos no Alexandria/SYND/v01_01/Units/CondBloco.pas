unit CondBloco;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkRadioGroup, Mask, dmkDBEdit,
  dmkValUsu, DB, mySQLDbTables, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmCondBloco = class(TForm)
    Panel1: TPanel;
    Label12: TLabel;
    EdCodigo: TdmkEdit;
    dmkDBEdCodigo: TdmkDBEdit;
    Label1: TLabel;
    EdPrefixoUH: TdmkEdit;
    Label198: TLabel;
    Label195: TLabel;
    EdOrdem: TdmkEdit;
    EdDescri: TdmkEdit;
    Label9: TLabel;
    CkContinuar: TCheckBox;
    Label2: TLabel;
    EdIDExporta: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCondBloco: TFmCondBloco;

implementation

uses Principal, UnInternalConsts, UMySQLModule, Cond, Module, 
UnMyObjects;

{$R *.DFM}

procedure TFmCondBloco.BtOKClick(Sender: TObject);
var
  Controle, Ordem: Integer;
  Descri, Prefixo: String;
begin
  Descri    := EdDescri.ValueVariant;
  Ordem   := EdOrdem.ValueVariant;
  Prefixo := EdPrefixoUH.ValueVariant;
  //
  if Length(Descri) = 0 then
  begin
    Geral.MensagemBox('Descri��o n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdDescri.SetFocus;
    Exit;
  end;
  if Ordem = 0 then
  begin
    Geral.MensagemBox('Orderm n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdOrdem.SetFocus;
    Exit;
  end;
  if Length(Prefixo) = 0 then
  begin
    Geral.MensagemBox('Prefixo n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdPrefixoUH.SetFocus;
    Exit;
  end;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('condbloco', 'Controle', ImgTipo.SQLType,
    FmCond.QrCondBlocoControle.Value);
  if UMyMod.ExecSQLInsUpdFm(FmCondBloco, ImgTipo.SQLType, 'condbloco', Controle,
    Dmod.QrUpd) then
  begin
    FmCond.ReopenCondBloco(Controle);
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
      ImgTipo.SQLType       := stIns;
      EdDescri.ValueVariant  := '';
      EdOrdem.ValueVariant := EdOrdem.ValueVariant + 1;
      EdDescri.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmCondBloco.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondBloco.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  EdDescri.SetFocus;
end;

procedure TFmCondBloco.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCondBloco.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
