unit FlxMensBalPer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Variants, dmkPermissoes, UnDmkEnums;

type
  TFmFlxMensBalPer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    LBPeriodos: TDBLookupListBox;
    StaticText2: TStaticText;
    QrAnoMes: TmySQLQuery;
    QrAnoMesAnoMes: TIntegerField;
    QrAnoMesAnoEMes: TWideStringField;
    DsAnoMes: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LBPeriodosClick(Sender: TObject);
    procedure LBPeriodosDblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure Localizar;
  public
    { Public declarations }
    FLocalizar: Boolean;
    FAnoMes   : Integer;
  end;

  var
  FmFlxMensBalPer: TFmFlxMensBalPer;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmFlxMensBalPer.BtOKClick(Sender: TObject);
begin
  Localizar;
end;

procedure TFmFlxMensBalPer.BtSaidaClick(Sender: TObject);
begin
  FLocalizar := False;
  Close;
end;

procedure TFmFlxMensBalPer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFlxMensBalPer.FormCreate(Sender: TObject);
begin
  FLocalizar := False;
  QrAnoMes.Open;
end;

procedure TFmFlxMensBalPer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, False, taCenter, 2, 10, 20);
end;

procedure TFmFlxMensBalPer.LBPeriodosClick(Sender: TObject);
begin
  BtOK.Enabled := LBPeriodos.KeyValue <> NULL;
end;

procedure TFmFlxMensBalPer.LBPeriodosDblClick(Sender: TObject);
begin
  Localizar;
end;

procedure TFmFlxMensBalPer.Localizar;
begin
  FLocalizar := True;
  FAnoMes   := QrAnoMesAnoMes.Value;
  //
  Close;
end;

end.
