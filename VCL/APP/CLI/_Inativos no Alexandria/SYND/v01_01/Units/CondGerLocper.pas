unit CondGerLocper;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, Variants,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmCondGerLocper = class(TForm)
    Panel1: TPanel;
    QrPrev: TmySQLQuery;
    QrPrevPeriodo: TIntegerField;
    QrPrevNOME_PER: TWideStringField;
    DsPrev: TDataSource;
    LBPeriodos: TDBLookupListBox;
    StaticText2: TStaticText;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrevCalcFields(DataSet: TDataSet);
    procedure LBPeriodosClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure LBPeriodosDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure Localizar;
  public
    { Public declarations }
    FLocalizar: Boolean;
    FPeriodo  : Integer;
  end;

  var
  FmCondGerLocper: TFmCondGerLocper;

implementation

uses Module, CondGer, UnMyObjects, UnDmkProcFunc;

{$R *.DFM}

procedure TFmCondGerLocper.BtSaidaClick(Sender: TObject);
begin
  FLocalizar := False;
  Close;
end;

procedure TFmCondGerLocper.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerLocper.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerLocper.QrPrevCalcFields(DataSet: TDataSet);
begin
  QrPrevNOME_PER.Value := dmkPF.MesEAnoDoPeriodo(QrPrevPeriodo.Value);
end;

procedure TFmCondGerLocper.LBPeriodosClick(Sender: TObject);
begin
  BtOK.Enabled := LBPeriodos.KeyValue <> NULL;
end;

procedure TFmCondGerLocper.BtOKClick(Sender: TObject);
begin
  Localizar;
end;

procedure TFmCondGerLocper.LBPeriodosDblClick(Sender: TObject);
begin
  Localizar;
end;

procedure TFmCondGerLocper.Localizar;
begin
  FLocalizar := True;
  FPeriodo   := QrprevPeriodo.Value;
  //
  Close;
end;

procedure TFmCondGerLocper.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FLocalizar := False;
end;

end.
