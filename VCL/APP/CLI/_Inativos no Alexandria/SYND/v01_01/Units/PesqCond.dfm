object FmPesqCond: TFmPesqCond
  Left = 331
  Top = 166
  Caption = 'CAD-CONDO-013 :: Gerenciamento de U.H. / Cond'#244'mino'
  ClientHeight = 552
  ClientWidth = 934
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 934
    Height = 390
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 934
      Height = 236
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 543
        Height = 236
        ActivePage = TabSheet3
        Align = alLeft
        TabOrder = 0
        object TabSheet3: TTabSheet
          Caption = 'Filtros normais'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel9: TPanel
            Left = 0
            Top = 148
            Width = 535
            Height = 60
            Align = alBottom
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object GBEmissao: TGroupBox
              Left = 0
              Top = 0
              Width = 196
              Height = 60
              Align = alLeft
              Caption = ' Per'#237'odo de emiss'#227'o: '
              TabOrder = 0
              object CkIniDta: TCheckBox
                Left = 8
                Top = 14
                Width = 90
                Height = 17
                Caption = 'Data inicial:'
                TabOrder = 0
                OnClick = CkIniDtaClick
              end
              object TPIniDta: TDateTimePicker
                Left = 8
                Top = 32
                Width = 90
                Height = 21
                CalColors.TextColor = clMenuText
                Date = 37636.777157974500000000
                Time = 37636.777157974500000000
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                OnChange = TPIniDtaChange
                OnExit = TPIniDtaExit
              end
              object CkFimDta: TCheckBox
                Left = 100
                Top = 14
                Width = 90
                Height = 17
                Caption = 'Data final:'
                TabOrder = 2
                OnClick = CkIniDtaClick
              end
              object TPFimDta: TDateTimePicker
                Left = 100
                Top = 32
                Width = 90
                Height = 21
                Date = 37636.777203761600000000
                Time = 37636.777203761600000000
                TabOrder = 3
                OnChange = TPIniDtaChange
                OnExit = TPIniDtaExit
              end
            end
            object GBVencto: TGroupBox
              Left = 196
              Top = 0
              Width = 196
              Height = 60
              Align = alLeft
              Caption = ' Per'#237'odo de vencimento: '
              TabOrder = 1
              object CkIniVct: TCheckBox
                Left = 8
                Top = 14
                Width = 90
                Height = 17
                Caption = 'Data inicial:'
                TabOrder = 0
                OnClick = CkIniDtaClick
              end
              object TPIniVct: TDateTimePicker
                Left = 8
                Top = 32
                Width = 90
                Height = 21
                CalColors.TextColor = clMenuText
                Date = 37636.777157974500000000
                Time = 37636.777157974500000000
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                OnChange = TPIniDtaChange
                OnExit = TPIniDtaExit
              end
              object CkFimVct: TCheckBox
                Left = 100
                Top = 14
                Width = 89
                Height = 17
                Caption = 'Data final:'
                TabOrder = 2
                OnClick = CkIniDtaClick
              end
              object TPFimVct: TDateTimePicker
                Left = 100
                Top = 32
                Width = 90
                Height = 21
                Date = 37636.777203761600000000
                Time = 37636.777203761600000000
                TabOrder = 3
                OnChange = TPIniDtaChange
                OnExit = TPIniDtaExit
              end
            end
            object GroupBox2: TGroupBox
              Left = 392
              Top = 0
              Width = 141
              Height = 60
              Align = alLeft
              Caption = ' Per'#237'odo de compet'#234'ncia:'
              TabOrder = 2
              object Label6: TLabel
                Left = 64
                Top = 20
                Width = 6
                Height = 13
                Caption = #224
              end
              object CkNaoMensais: TCheckBox
                Left = 8
                Top = 38
                Width = 121
                Height = 17
                Caption = 'Permite n'#227'o mensais.'
                TabOrder = 2
                OnClick = CkIniDtaClick
              end
              object dmkEdMezIni: TdmkEdit
                Left = 8
                Top = 16
                Width = 53
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnExit = dmkEdMezIniExit
              end
              object dmkEdMezFim: TdmkEdit
                Left = 76
                Top = 16
                Width = 56
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnExit = dmkEdMezFimExit
              end
            end
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 535
            Height = 148
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label2: TLabel
              Left = 4
              Top = 2
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label1: TLabel
              Left = 4
              Top = 42
              Width = 361
              Height = 13
              Caption = 
                'Propriet'#225'rio [F4 mostra todos] (ou digite o nome parcial na caix' +
                'a logo abaixo):'
            end
            object Label3: TLabel
              Left = 388
              Top = 42
              Width = 124
              Height = 13
              Caption = 'Unidade habitacional [F4]:'
            end
            object Label4: TLabel
              Left = 4
              Top = 106
              Width = 71
              Height = 13
              Caption = 'Transferir para:'
            end
            object SpeedButton5: TSpeedButton
              Left = 510
              Top = 123
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton5Click
            end
            object EdEmpresa: TdmkEditCB
              Left = 4
              Top = 18
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdEmpresaChange
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 52
              Top = 18
              Width = 479
              Height = 21
              Color = clWhite
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              ListSource = DModG.DsEmpresas
              TabOrder = 1
              dmkEditCB = EdEmpresa
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdPropriet: TdmkEditCB
              Left = 4
              Top = 58
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdProprietChange
              OnKeyDown = EdProprietKeyDown
              DBLookupComboBox = CBPropriet
              IgnoraDBLookupComboBox = False
            end
            object CBPropriet: TdmkDBLookupComboBox
              Left = 52
              Top = 58
              Width = 333
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMEPROP'
              ListSource = DsPropriet
              TabOrder = 3
              OnKeyDown = CBProprietKeyDown
              dmkEditCB = EdPropriet
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCondImov: TdmkEditCB
              Left = 388
              Top = 58
              Width = 44
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdCondImovChange
              OnKeyDown = EdCondImovKeyDown
              DBLookupComboBox = CBCondImov
              IgnoraDBLookupComboBox = False
            end
            object CBCondImov: TdmkDBLookupComboBox
              Left = 436
              Top = 58
              Width = 95
              Height = 21
              Color = clWhite
              KeyField = 'Conta'
              ListField = 'Unidade'
              ListSource = DsCondImov
              TabOrder = 5
              OnKeyDown = CBCondImovKeyDown
              dmkEditCB = EdCondImov
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdPesqProp: TEdit
              Left = 4
              Top = 82
              Width = 381
              Height = 21
              TabOrder = 6
              OnChange = EdPesqPropChange
            end
            object EdEntDono: TdmkEditCB
              Left = 4
              Top = 123
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEntDono
              IgnoraDBLookupComboBox = False
            end
            object CBEntDono: TdmkDBLookupComboBox
              Left = 50
              Top = 123
              Width = 455
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMECONDOMINO'
              ListSource = DsCondominos
              TabOrder = 9
              dmkEditCB = EdEntDono
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdPesqApto: TEdit
              Left = 388
              Top = 82
              Width = 143
              Height = 21
              TabOrder = 7
              OnChange = EdPesqAptoChange
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Grupo de UHs / Cond'#244'minos:'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid1: TDBGrid
            Left = 0
            Top = 22
            Width = 535
            Height = 186
            Align = alClient
            DataSource = DsCondGriImv
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMECOND'
                Title.Caption = 'Condom'#237'nio'
                Width = 216
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEPROP'
                Title.Caption = 'Propriet'#225'rio'
                Width = 216
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UNIDADE'
                Title.Caption = 'U.H.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Cond'
                Title.Caption = 'Cond.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Apto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Propr'
                Title.Caption = 'Propr.'
                Visible = True
              end>
          end
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 535
            Height = 22
            Align = alTop
            BevelOuter = bvNone
            Caption = 'Panel8'
            ParentBackground = False
            TabOrder = 0
            object SpeedButton1: TSpeedButton
              Left = 510
              Top = 0
              Width = 21
              Height = 21
              Caption = '...'
            end
            object EdCondGri: TdmkEditCB
              Left = 0
              Top = 0
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdCondGriChange
              DBLookupComboBox = CBCondGri
              IgnoraDBLookupComboBox = False
            end
            object CBCondGri: TdmkDBLookupComboBox
              Left = 48
              Top = 0
              Width = 457
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCondGri
              TabOrder = 1
              dmkEditCB = EdCondGri
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
      object Panel5: TPanel
        Left = 543
        Top = 0
        Width = 391
        Height = 236
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel5'
        TabOrder = 1
        object StaticText1: TStaticText
          Left = 0
          Top = 0
          Width = 177
          Height = 17
          Align = alTop
          Caption = '     U.H.s/ Cond'#244'minos pesquisados:'
          TabOrder = 0
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 17
          Width = 391
          Height = 219
          Align = alClient
          Color = clWindow
          DataSource = DsPesq1
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 236
      Width = 934
      Height = 154
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object dmkDBGrid2: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 614
        Height = 154
        Align = alClient
        Color = clWindow
        DataSource = DsLct
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object dmkDBGrid3: TdmkDBGrid
        Left = 614
        Top = 0
        Width = 320
        Height = 154
        Align = alRight
        Columns = <
          item
            Expanded = False
            FieldName = 'PERIODO_TXT'
            Title.Caption = 'Compet'#234'ncia'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Boleto'
            Width = 100
            Visible = True
          end>
        Color = clWindow
        DataSource = DsBloqs
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'PERIODO_TXT'
            Title.Caption = 'Compet'#234'ncia'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Boleto'
            Width = 100
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 438
    Width = 934
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 930
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 482
    Width = 934
    Height = 70
    Align = alBottom
    TabOrder = 2
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 930
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label5: TLabel
        Left = 396
        Top = 8
        Width = 56
        Height = 13
        Caption = 'Propriet'#225'rio:'
      end
      object Label7: TLabel
        Left = 396
        Top = 28
        Width = 103
        Height = 13
        Caption = 'Unidade habitacional:'
      end
      object Label8: TLabel
        Left = 504
        Top = 8
        Width = 63
        Height = 13
        Caption = 'N'#227'o definido!'
      end
      object Label9: TLabel
        Left = 504
        Top = 28
        Width = 63
        Height = 13
        Caption = 'N'#227'o definida!'
      end
      object PnSaiDesis: TPanel
        Left = 786
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtPesquisa: TBitBtn
        Tag = 18
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPesquisaClick
      end
      object BtReabre: TBitBtn
        Tag = 14
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Reabrir'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtReabreClick
      end
      object BtTransfere: TBitBtn
        Tag = 14
        Left = 268
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Transferir'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtTransfereClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 934
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 886
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 838
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 447
        Height = 32
        Caption = 'Gerenciamento de U.H. / Cond'#244'mino'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 447
        Height = 32
        Caption = 'Gerenciamento de U.H. / Cond'#244'mino'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 447
        Height = 32
        Caption = 'Gerenciamento de U.H. / Cond'#244'mino'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object QrPropriet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP '
      'FROM entidades ent'
      'LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet'
      'WHERE ent.Cliente2="V"'
      'AND cim.Codigo<>0'
      'ORDER BY NOMEPROP')
    Left = 297
    Top = 57
    object QrProprietCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrProprietNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Origin = 'NOMEPROP'
      Required = True
      Size = 100
    end
  end
  object DsPropriet: TDataSource
    DataSet = QrPropriet
    Left = 325
    Top = 57
  end
  object QrCondImov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cim.Conta, cim.Unidade'
      'FROM condimov cim'
      'WHERE cim.Codigo<>0'
      'AND cim.Propriet<>0'
      'ORDER BY cim.Unidade'
      '')
    Left = 353
    Top = 57
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 381
    Top = 57
  end
  object QrCondGri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM condgri'
      'ORDER BY Nome')
    Left = 409
    Top = 57
    object QrCondGriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondGriNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCondGri: TDataSource
    DataSet = QrCondGri
    Left = 437
    Top = 57
  end
  object QrCondGriImv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(cii.Propr=0,"T O D O S", '
      '  IF(pro.Tipo=0, pro.RazaoSocial, pro.Nome)) NOMEPROP,'
      'IF(cii.Cond=0,"T O D O S", '
      '  IF(con.Tipo=0, con.RazaoSocial, con.Nome)) NOMECOND,'
      'IF(cii.Apto=0,"TODAS", '
      '  imv.Unidade) UNIDADE, cii.* '
      'FROM condgriimv cii'
      'LEFT JOIN condimov  imv ON imv.Conta=cii.Apto'
      'LEFT JOIN cond      cnd ON cnd.Codigo=cii.Cond'
      'LEFT JOIN entidades con ON con.Codigo=cnd.Cliente'
      'LEFT JOIN entidades pro ON pro.Codigo=cii.Propr'
      'WHERE cii.Codigo=:P0')
    Left = 465
    Top = 57
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondGriImvNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrCondGriImvNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
    object QrCondGriImvUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object QrCondGriImvCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCondGriImvControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCondGriImvCond: TIntegerField
      FieldName = 'Cond'
      Required = True
    end
    object QrCondGriImvApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrCondGriImvPropr: TIntegerField
      FieldName = 'Propr'
      Required = True
    end
    object QrCondGriImvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCondGriImvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCondGriImvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCondGriImvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCondGriImvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCondGriImvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCondGriImvAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsCondGriImv: TDataSource
    DataSet = QrCondGriImv
    Left = 493
    Top = 57
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT imv.Conta APTO, imv.Unidade, imv.Codigo COND, imv.Proprie' +
        't,'
      'IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPROP,'
      'IF(con.Tipo=0, con.RazaoSocial, con.Nome) NOMECOND'
      'FROM entidades prp'
      'LEFT JOIN condimov  imv ON imv.Propriet=prp.Codigo'
      'LEFT JOIN cond      cnd ON imv.Codigo=cnd.Codigo'
      'LEFT JOIN entidades con ON cnd.Cliente=con.Codigo'
      'WHERE prp.Cliente2="V"'
      'AND (IF(prp.Tipo=0,prp.RazaoSocial,prp.Nome) LIKE "%o%")'
      '')
    Left = 605
    Top = 105
    object QrPesq1APTO: TIntegerField
      FieldName = 'APTO'
      Required = True
    end
    object QrPesq1Unidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPesq1COND: TIntegerField
      FieldName = 'COND'
    end
    object QrPesq1Propriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrPesq1NOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Required = True
      Size = 100
    end
    object QrPesq1NOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
  end
  object DsPesq1: TDataSource
    DataSet = QrPesq1
    Left = 633
    Top = 105
  end
  object QrCondominos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECONDOMINO '
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMECONDOMINO')
    Left = 709
    Top = 13
    object QrCondominosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCondominosNOMECONDOMINO: TWideStringField
      FieldName = 'NOMECONDOMINO'
      Required = True
      Size = 100
    end
  end
  object DsCondominos: TDataSource
    DataSet = QrCondominos
    Left = 737
    Top = 13
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLctBeforeClose
    SQL.Strings = (
      '')
    Left = 225
    Top = 351
    object QrLctData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 253
    Top = 351
  end
  object QrBoletos: TmySQLQuery
    Database = Dmod.MyDB
    Left = 113
    Top = 273
  end
  object QrBloqs: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrBloqsCalcFields
    Left = 477
    Top = 333
    object QrBloqsPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBloqsPERIODO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 30
      Calculated = True
    end
    object QrBloqsBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
    object QrBloqsLancto: TIntegerField
      FieldName = 'Lancto'
    end
  end
  object DsBloqs: TDataSource
    DataSet = QrBloqs
    Left = 505
    Top = 333
  end
  object PMTransfere: TPopupMenu
    OnPopup = PMTransferePopup
    Left = 73
    Top = 429
    object TransfereLanamentos1: TMenuItem
      Caption = 'Transfere &Lan'#231'amentos'
      OnClick = TransfereLanamentos1Click
    end
    object TransfereBloquetos1: TMenuItem
      Caption = '&Transfere &Bloquetos'
      OnClick = TransfereBloquetos1Click
    end
  end
  object QrPgBloq: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPgBloqCalcFields
    Left = 144
    Top = 316
    object QrPgBloqCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPgBloqMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrPgBloqMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrPgBloqNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrPgBloqUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrPgBloqNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrPgBloqMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPgBloqVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPgBloqDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPgBloqORIGINAL: TFloatField
      FieldName = 'ORIGINAL'
    end
    object QrPgBloqData: TDateField
      FieldName = 'Data'
    end
    object QrPgBloqMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrPgBloqDATA_TXT: TWideStringField
      FieldName = 'DATA_TXT'
      Size = 10
    end
  end
  object frxDsPgBloq: TfrxDBDataset
    UserName = 'frxDsPgBloq'
    CloseDataSource = False
    DataSet = QrPgBloq
    BCDToCurrency = False
    Left = 172
    Top = 316
  end
  object QrSuBloq: TmySQLQuery
    Database = Dmod.MyDB
    Left = 144
    Top = 344
    object QrSuBloqMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrSuBloqMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrSuBloqNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrSuBloqORIGINAL: TFloatField
      FieldName = 'ORIGINAL'
    end
    object QrSuBloqKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrSuBloqPAGO: TFloatField
      FieldName = 'PAGO'
    end
  end
  object frxDsSuBloq: TfrxDBDataset
    UserName = 'frxDsSuBloq'
    CloseDataSource = False
    DataSet = QrSuBloq
    BCDToCurrency = False
    Left = 172
    Top = 344
  end
  object frxBal_A_12A: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40318.458037951390000000
    ReportOptions.LastChange = 40318.458037951390000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_12AGetValue
    Left = 320
    Top = 320
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPgBloq
        DataSetName = 'frxDsPgBloq'
      end
      item
        DataSet = frxDsSuBloq
        DataSetName = 'frxDsSuBloq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader6: TfrxPageHeader
        FillType = ftBrush
        Height = 54.913393150000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 28.346456692913390000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 13.228346456692910000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 13.228346460000000000
          Width = 468.661720000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'HIST'#211'RICO DE PAGAMENTOS DE BLOQUETOS DE COBRAN'#199'A')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 13.228346460000000000
          Width = 98.267716540000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 13.228346460000000000
          Width = 98.267716540000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 7.559060000000000000
          Top = 28.346456690000000000
          Width = 665.197280000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.685046690000000000
          Width = 665.197280000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader7: TfrxGroupHeader
        FillType = ftBrush
        Height = 24.566929130000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPgBloq."Documento"'
        KeepTogether = True
        object Memo157: TfrxMemoView
          Top = 13.228346456692910000
          Width = 253.228510000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade/Locat'#225'rio/Conta')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 385.512060000000000000
          Top = 13.228346456692910000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Titulo')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 253.228510000000000000
          Top = 13.228346456692910000
          Width = 34.015748030000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 287.244280000000000000
          Top = 13.228346456692910000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Left = 336.378170000000000000
          Top = 13.228346456692910000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pagamento')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Left = 468.661720000000000000
          Top = 13.228346456692910000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Left = 525.354670000000000000
          Top = 13.228346456692910000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Left = 582.047620000000000000
          Top = 13.228346456692910000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Left = 631.181510000000000000
          Top = 13.228346456692910000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Left = 491.338900000000000000
          Top = 1.889763779527559000
          Width = 188.976463390000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade habitacional: [frxDsPgBloq."UH"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo167: TfrxMemoView
          Top = 1.889763779527559000
          Width = 491.338863390000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPgBloq."NOMEPROPRIET"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 185.196970000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPgBloq
        DataSetName = 'frxDsPgBloq'
        KeepTogether = True
        RowCount = 0
        object Memo168: TfrxMemoView
          Width = 253.228510000000000000
          Height = 11.338582680000000000
          DataField = 'NOMECONTA'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPgBloq."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo169: TfrxMemoView
          Left = 385.512060000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          DataField = 'Documento'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."Documento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo170: TfrxMemoView
          Left = 253.228510000000000000
          Width = 34.015748030000000000
          Height = 11.338582677165350000
          DataField = 'MES'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."MES"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo171: TfrxMemoView
          Left = 287.244280000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'Vencimento'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo172: TfrxMemoView
          Left = 336.378170000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'DATA_TXT'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."DATA_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo173: TfrxMemoView
          Left = 468.661720000000000000
          Width = 56.692913390000000000
          Height = 11.338582677165350000
          DataField = 'ORIGINAL'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."ORIGINAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo174: TfrxMemoView
          Left = 525.354670000000000000
          Width = 56.692913390000000000
          Height = 11.338582677165350000
          DataField = 'Credito'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo175: TfrxMemoView
          Left = 582.047620000000000000
          Width = 49.133858270000000000
          Height = 11.338582677165350000
          DataField = 'MultaVal'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."MultaVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo176: TfrxMemoView
          Left = 631.181510000000000000
          Width = 49.133858270000000000
          Height = 11.338582677165350000
          DataField = 'MoraVal'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."MoraVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter7: TfrxGroupFooter
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        object Memo177: TfrxMemoView
          Width = 468.661720000000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              'Total do t'#237'tulo [frxDsPgBloq."Documento"] da unidade habitaciona' +
              'l [frxDsPgBloq."UH"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo178: TfrxMemoView
          Left = 468.661720000000000000
          Width = 56.692913390000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."ORIGINAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo179: TfrxMemoView
          Left = 525.354670000000000000
          Width = 56.692913390000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo180: TfrxMemoView
          Left = 582.047620000000000000
          Width = 49.133858270000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."MultaVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo181: TfrxMemoView
          Left = 631.181510000000000000
          Width = 49.133858270000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter4: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 415.748300000000000000
        Width = 680.315400000000000000
        object Memo182: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSuBloq
        DataSetName = 'frxDsSuBloq'
        KeepTogether = True
        RowCount = 0
        object Memo188: TfrxMemoView
          Width = 347.716760000000000000
          Height = 11.338582677165350000
          DataField = 'NOMECONTA'
          DataSet = frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSuBloq."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo189: TfrxMemoView
          Left = 347.716760000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          DataField = 'ORIGINAL'
          DataSet = frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."ORIGINAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo190: TfrxMemoView
          Left = 430.866420000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          DataField = 'PAGO'
          DataSet = frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."PAGO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo191: TfrxMemoView
          Left = 514.016080000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          DataField = 'MultaVal'
          DataSet = frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."MultaVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo192: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          DataField = 'MoraVal'
          DataSet = frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."MoraVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader13: TfrxGroupHeader
        FillType = ftBrush
        Height = 32.125976930000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSuBloq."KGT"'
        KeepTogether = True
        object Memo183: TfrxMemoView
          Top = 20.787394251968500000
          Width = 347.716760000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Left = 347.716760000000000000
          Top = 20.787394251968500000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Left = 430.866420000000000000
          Top = 20.787394251968500000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Left = 514.016080000000000000
          Top = 20.787394251968500000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Left = 597.165740000000000000
          Top = 20.787394251968500000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo330: TfrxMemoView
          Top = 7.559059999999999000
          Width = 680.315400000000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAIS DAS CONTAS NO PROCESSAMENTO DE BLOQUETOS DE COBRAN'#199'A')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter13: TfrxGroupFooter
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        object Memo193: TfrxMemoView
          Left = 347.716760000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."ORIGINAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo194: TfrxMemoView
          Left = 430.866420000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."PAGO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo195: TfrxMemoView
          Left = 514.016080000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."MULTAVAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo196: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."MORAVAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo197: TfrxMemoView
          Width = 347.716760000000000000
          Height = 11.338582677165350000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  ')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxBal_A_12B: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40318.458037951390000000
    ReportOptions.LastChange = 40318.458037951390000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_12AGetValue
    Left = 320
    Top = 348
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPgBloq
        DataSetName = 'frxDsPgBloq'
      end
      item
        DataSet = frxDsSuBloq
        DataSetName = 'frxDsSuBloq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader6: TfrxPageHeader
        FillType = ftBrush
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'HIST'#211'RICO DE PAGAMENTOS DE BLOQUETOS DE COBRAN'#199'A')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader7: TfrxGroupHeader
        FillType = ftBrush
        Height = 41.574812910000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPgBloq."Documento"'
        KeepTogether = True
        object Memo157: TfrxMemoView
          Top = 26.456710000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade/Locat'#225'rio/Conta')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 325.039580000000000000
          Top = 26.456710000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Titulo')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 151.181200000000000000
          Top = 26.456710000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 196.535560000000000000
          Top = 26.456710000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Left = 260.787570000000000000
          Top = 26.456710000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pagamento')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Left = 419.527830000000000000
          Top = 26.456710000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Left = 487.559370000000000000
          Top = 26.456710000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Left = 555.590910000000000000
          Top = 26.456710000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Left = 616.063390000000000000
          Top = 26.456710000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade habitacional: [frxDsPgBloq."UH"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo167: TfrxMemoView
          Top = 13.228346460000000000
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPgBloq."NOMEPROPRIET"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPgBloq
        DataSetName = 'frxDsPgBloq'
        KeepTogether = True
        RowCount = 0
        object Memo168: TfrxMemoView
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          DataField = 'NOMECONTA'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPgBloq."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo169: TfrxMemoView
          Left = 325.039580000000000000
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          DataField = 'Documento'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."Documento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo170: TfrxMemoView
          Left = 151.181200000000000000
          Width = 45.354360000000000000
          Height = 13.228346460000000000
          DataField = 'MES'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."MES"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo171: TfrxMemoView
          Left = 196.535560000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataField = 'Vencimento'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo172: TfrxMemoView
          Left = 260.787570000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataField = 'DATA_TXT'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."DATA_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo173: TfrxMemoView
          Left = 419.527830000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          DataField = 'ORIGINAL'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."ORIGINAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo174: TfrxMemoView
          Left = 487.559370000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          DataField = 'Credito'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo175: TfrxMemoView
          Left = 555.590910000000000000
          Width = 60.472480000000000000
          Height = 13.228346460000000000
          DataField = 'MultaVal'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."MultaVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo176: TfrxMemoView
          Left = 616.063390000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataField = 'MoraVal'
          DataSet = frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."MoraVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter7: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        object Memo177: TfrxMemoView
          Width = 419.527830000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              'Total do t'#237'tulo [frxDsPgBloq."Documento"] da unidade habitaciona' +
              'l [frxDsPgBloq."UH"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo178: TfrxMemoView
          Left = 419.527830000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."ORIGINAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo179: TfrxMemoView
          Left = 487.559370000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo180: TfrxMemoView
          Left = 555.590910000000000000
          Width = 60.472480000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."MultaVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo181: TfrxMemoView
          Left = 616.063390000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter4: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 495.118430000000000000
        Width = 680.315400000000000000
        object Memo182: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 374.173470000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSuBloq
        DataSetName = 'frxDsSuBloq'
        KeepTogether = True
        RowCount = 0
        object Memo188: TfrxMemoView
          Width = 347.716760000000000000
          Height = 13.228346460000000000
          DataField = 'NOMECONTA'
          DataSet = frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSuBloq."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo189: TfrxMemoView
          Left = 347.716760000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataField = 'ORIGINAL'
          DataSet = frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."ORIGINAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo190: TfrxMemoView
          Left = 430.866420000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataField = 'PAGO'
          DataSet = frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."PAGO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo191: TfrxMemoView
          Left = 514.016080000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataField = 'MultaVal'
          DataSet = frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."MultaVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo192: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataField = 'MoraVal'
          DataSet = frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."MoraVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader13: TfrxGroupHeader
        FillType = ftBrush
        Height = 49.133890000000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSuBloq."KGT"'
        KeepTogether = True
        object Memo183: TfrxMemoView
          Top = 34.015770000000000000
          Width = 347.716760000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Left = 347.716760000000000000
          Top = 34.015770000000000000
          Width = 83.149606300000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Left = 430.866420000000000000
          Top = 34.015770000000000000
          Width = 83.149606300000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Left = 514.016080000000000000
          Top = 34.015770000000000000
          Width = 83.149606300000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Left = 597.165740000000000000
          Top = 34.015770000000000000
          Width = 83.149606300000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo330: TfrxMemoView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAIS DAS CONTAS NO PROCESSAMENTO DE BLOQUETOS DE COBRAN'#199'A')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter13: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 411.968770000000000000
        Width = 680.315400000000000000
        object Memo193: TfrxMemoView
          Left = 347.716760000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."ORIGINAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo194: TfrxMemoView
          Left = 430.866420000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."PAGO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo195: TfrxMemoView
          Left = 514.016080000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."MULTAVAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo196: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."MORAVAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo197: TfrxMemoView
          Width = 347.716760000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  ')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object PMImprime: TPopupMenu
    Left = 64
    Top = 20
    object Histricodepagamentos1: TMenuItem
      Caption = '&Hist'#243'rico de pagamentos'
      object Fontetamanho61: TMenuItem
        Caption = 'Fonte tamanho &6'
        OnClick = Fontetamanho61Click
      end
      object Fontetamanho81: TMenuItem
        Caption = 'Fonte tamanho &8'
        OnClick = Fontetamanho81Click
      end
    end
  end
end
