unit ModuleGeral;

interface

uses
  Windows, Forms, Controls, SysUtils, Classes, DB, mySQLDbTables, UnInternalConsts,
  dmkGeral;

type
  TDModG = class(TDataModule)
    MyPID_DB: TmySQLDatabase;
    QrUpdPID1: TmySQLQuery;
    QrDono: TmySQLQuery;
    QrDonoCodigo: TIntegerField;
    QrDonoCadastro: TDateField;
    QrDonoNOMEDONO: TWideStringField;
    QrDonoCNPJ_CPF: TWideStringField;
    QrDonoIE_RG: TWideStringField;
    QrDonoNIRE_: TWideStringField;
    QrDonoRUA: TWideStringField;
    QrDonoCOMPL: TWideStringField;
    QrDonoBAIRRO: TWideStringField;
    QrDonoCIDADE: TWideStringField;
    QrDonoNOMELOGRAD: TWideStringField;
    QrDonoNOMEUF: TWideStringField;
    QrDonoPais: TWideStringField;
    QrDonoTipo: TSmallintField;
    QrDonoTE1: TWideStringField;
    QrDonoTE2: TWideStringField;
    QrDonoFAX: TWideStringField;
    QrDonoENatal: TDateField;
    QrDonoPNatal: TDateField;
    QrDonoECEP_TXT: TWideStringField;
    QrDonoNUMERO_TXT: TWideStringField;
    QrDonoCNPJ_TXT: TWideStringField;
    QrDonoFAX_TXT: TWideStringField;
    QrDonoTE1_TXT: TWideStringField;
    QrDonoTE2_TXT: TWideStringField;
    QrDonoNATAL_TXT: TWideStringField;
    QrDonoE_LNR: TWideStringField;
    QrDonoE_CUC: TWideStringField;
    QrDonoEContato: TWideStringField;
    QrDonoECel: TWideStringField;
    QrDonoCEL_TXT: TWideStringField;
    QrDonoE_LN2: TWideStringField;
    QrDonoAPELIDO: TWideStringField;
    QrDonoNUMERO: TFloatField;
    QrDonoRespons1: TWideStringField;
    QrDonoRespons2: TWideStringField;
    QrDonoCEP: TIntegerField;
    QrDonoECEP: TIntegerField;
    QrDonoPCEP: TIntegerField;
    QrDonoEUF: TSmallintField;
    QrDonoPUF: TSmallintField;
    QrDonoUF: TSmallintField;
    QrDonoFONES: TWideStringField;
    QrPreEmail: TmySQLQuery;
    QrPreEmailCodigo: TIntegerField;
    QrPreEmailNome: TWideStringField;
    QrPreEmailSMTPServer: TWideStringField;
    QrPreEmailSend_Name: TWideStringField;
    QrPreEmailSend_Mail: TWideStringField;
    QrPreEmailPass_Mail: TWideStringField;
    QrPreEmailLogi_Name: TWideStringField;
    QrPreEmailLogi_Pass: TWideStringField;
    QrPreEmailLogi_Auth: TSmallintField;
    QrPreEmailMail_Titu: TWideStringField;
    QrPreEmailSaudacao: TWideStringField;
    QrPreEmailNaoEnvBloq: TSmallintField;
    QrPreEmailPorta_mail: TIntegerField;
    QrPreEmailLogi_SSL: TSmallintField;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrAux: TmySQLQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
{
    function  NomeTab(DBName: String; Tab: TNomeTab; SetaVAR_LCT: Boolean;
              Letra: TTipoTab = ttA; Empresa: Integer = 0): String;
}
    procedure ReopenDono();
    function ObtemNomeFantasiaEmpresa(): String;
    function ObtemAgora(): TDateTime;
    function MyPID_DB_Cria(): Boolean;
    function MyPID_DB_Excl(Nome: String): Boolean;
  end;

var
  DModG: TDModG;

implementation

uses Module;

{$R *.dfm}

procedure TDModG.DataModuleCreate(Sender: TObject);
begin
  MyPID_DB.LoginPrompt := False;
  DModG.MyPID_DB_Cria();
end;

(*
function TDModG.NomeTab(DBName: String; Tab: TNomeTab; SetaVAR_LCT: Boolean;
  Letra: TTipoTab; Empresa: Integer): String;
var
  N1, N2, N3, Nome: String;
  CliInt: Integer;
begin
  if Empresa = 0 then
    CliInt := EmpresaAtual_ObtemCodigo(tecCliInt)
  else CliInt := Empresa;
  //
  if CliInt = 0 then
  begin
    if VAR_FilialUnica <> 0 then
    begin
      CliInt := VAR_FilialUnica
    end else
    begin
      {
      Geral.MensagemBox('ERRO! Tabela de lan�amentos ficaria indefinida!' +
      #13#10 + 'function TDModG.NomeTab()' + #13#10
      + 'O aplicativo ser� encerrado!', 'ERRO', MB_OK+MB_ICONERROR);
      }
      raise Exception.Create(
        'ERRO! Tabela de lan�amentos ficou indefinida!' +
        #13#10 + 'function TDModG.NomeTab()');
      Halt;
    end;
  end;
  if CliInt < 0 then
    N2 := '_' + FormatFloat('000', - CliInt)
  else
    N2 := FormatFloat('0000', CliInt);
  //

  case Letra of
    tt_: N3 := Lowercase(VAR_LETRA_LCT);
    ttA: N3 := 'a';
    ttB: N3 := 'b';
    ttD: N3 := 'd';
  end;
  //
  if not (N3[1] in (['a','b','c','d','e','f'])) then
  begin
    Geral.MensagemBox('Tipo de tabela de lan�amentos n�o definida!',
    'Erro', MB_OK+MB_ICONERROR);
  end;
  if N3 = 'c' then
    N1 := VAR_MyPID_DB_NOME
  else
    N1 := DBName;
  if N1 <> '' then
  begin
    if N1[Length(N1)] <> '.' then
      N1 := N1 + '.';
  end;
  //
  case Tab of
    ntLct: Nome := 'lct';
    ntAri: Nome := 'ari';
    ntCns: Nome := 'cns';
    ntPri: Nome := 'pri';
    ntPrv: Nome := 'prv';
    else   Nome := 'err';
  end;
  Result := Lowercase(N1 + Nome + N2 + N3);
  //
  if SetaVAR_LCT then
  begin
    {$IFDEF DEFINE_VARLCT}
    if (LowerCase(Application.Title) = 'exul2')
    or (LowerCase(Application.Name) = 'credito2')
    or (LowerCase(Application.Name) = 'sindi2') then
      VAR_LCT := '!ERRO!'
    else
      VAR_LCT := Result;
    {$ENDIF}
  end;
end;
*)

function TDModG.MyPID_DB_Cria(): Boolean;
var
  P: Integer;
  Nome: String;
var
  I, K: Integer;
  Fonte: String;
begin
  try
    // Aproveita aqui para ver as fontes!
    //  Configurar no IP certo para poder criar o database pessoal!
    MyPID_DB.Disconnect;
    MyPID_DB.Host         := Dmod.MyDB.Host;
    MYPID_DB.UserName     := Dmod.MyDB.UserName;
    MyPID_DB.UserPassword := Dmod.MyDB.UserPassword;
    MyPID_DB.DatabaseName := 'mysql';
    MyPID_DB.Port         := VAR_PORTA;
    try
      Screen.Cursor := crHourGlass;
      MyPID_DB.Connect;
    finally
      Screen.Cursor := crDefault;
    end;
    //
    if Trim(VAR_MyPID_DB_NOME) = '' then
    begin
      {$IFNDEf SemDBLocal}
      if TLocDB <> '' then
        Nome := TLocDB
      else
      {$EndIf}
        Nome := TMeuDB;
      VAR_MyPID_DB_NOME := Lowercase(Nome + '_' +
        FormatFloat('0', VAR_USUARIO));
      //  caso for Master ou Admin (n�meros negativos)
      P := pos('-', VAR_MyPID_DB_NOME);
      if P > 0 then
        VAR_MyPID_DB_NOME[P] := '_';
    end;
    QrUpdPID1.Close;
    QrUpdPID1.SQL.Clear;
    QrUpdPID1.SQL.Add('SHOW DATABASES LIKE "' + VAR_MyPID_DB_NOME + '"');
    QrUpdPID1.Open;
    //
    if QrUpdPID1.RecordCount = 0 then
    begin
      // Para garantir !!!
      MyPID_DB_Excl(VAR_MyPID_DB_NOME);
      //
      QrUpdPID1.Close;
      QrUpdPID1.SQL.Clear;
      QrUpdPID1.SQL.Add('CREATE DATABASE ' + VAR_MyPID_DB_NOME);
      QrUpdPID1.ExecSQL;
    end;
    //
    MyPID_DB.Disconnect;
    MyPID_DB.Host         := Dmod.MyDB.Host;
    MYPID_DB.UserName     := Dmod.MyDB.UserName;
    MyPID_DB.UserPassword := Dmod.MyDB.UserPassword;
    MyPID_DB.DatabaseName := VAR_MyPID_DB_NOME;
    MyPID_DB.Port         := VAR_PORTA;
    MyPID_DB.Connect;
    Result := True;
    //
  finally
    ;
  end;
end;

function TDModG.MyPID_DB_Excl(Nome: String): Boolean;
var
  Continua: Boolean;
  I: Integer;
begin
  Result := False;
  Continua := False;
  if pos('_', Nome) > 0 then
  begin
    I := pos('_', Nome);
    if Geral.SoNumero_TT(Copy(Nome, I)) <> '' then
      Continua := True;
  end;
  if Continua then
  begin
    //MyPID_DB.Execute('CREATE DATABASE ' + VAR_MyPID_DB_NOME);
    QrAux.SQL.Clear;
    QrAux.SQL.Add('DROP DATABASE IF EXISTS ' + Nome);
    QrAux.ExecSQL;
    //
    Result := True;
  end else Geral.MensagemBox('N�o � permitido excluir a base de ' +
  'dados "' + Nome + '". Avise a DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

function TDModG.ObtemAgora(): TDateTime;
///////////////////////////////////
// ver tamb�m > ObtemDataHora    //
///////////////////////////////////
var
  Hoje: TDateTime;
  Agora: TTime;
begin
  QrAgora.Close;
  QrAgora.Open;
  Hoje  := EncodeDate(QrAgoraAno.Value, QrAgoraMes.Value, QrAgoraDia.Value);
  Agora := EncodeTime(QrAgoraHora.Value, QrAgoraMINUTO.Value, QrAgoraSEGUNDO.Value, 0);
  Result := Hoje + Agora;
end;

function TDModG.ObtemNomeFantasiaEmpresa: String;
begin
  ReopenDono;
  //
  Result := QrDonoAPELIDO.Value;
  if Trim(Result) = '' then
    Result := QrDonoNOMEDONO.Value;
end;

procedure TDModG.ReopenDono();
begin
  {$IfNDef SemEntidade}
  QrDono.Close;
  QrDono.Open;
  {$EndIf}
end;

end.
