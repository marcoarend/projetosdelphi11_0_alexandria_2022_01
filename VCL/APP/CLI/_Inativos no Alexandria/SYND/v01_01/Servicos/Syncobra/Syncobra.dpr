program Syncobra;

uses
  ExceptionLog,
  Forms,
  Principal in 'Principal.pas' {FmPrincipal},
  Module in 'Module.pas' {Dmod: TDataModule},
  ModuleGeral in 'ModuleGeral.pas' {DModG: TDataModule},
  ModCobranca in '..\..\..\..\..\Cobranca\ModCobranca.pas' {DmModCobranca: TDataModule},
  AxSms_TLB in '..\..\..\..\..\SMS\AxSms_TLB.pas',
  Protocol_Tabs in '..\..\..\..\..\Outros\Protocolos\Protocol_Tabs.pas',
  UMySQLModule in 'Replicadios\UMySQLModule.pas',
  UnMyObjects in 'Replicadios\UnMyObjects.pas',
  UnDmkProcFunc in '..\..\..\..\..\Geral\Units\UnDmkProcFunc.pas',
  dmkDAC_PF in '..\..\..\..\..\Outros\MySQL\dmkDAC_PF.pas',
  Diario_Tabs in '..\..\..\..\..\Outros\Diario\Diario_Tabs.pas',
  CreateApp in '..\..\CreateApp.pas',
  UnMailEnv in '..\..\..\..\..\Outros\Email\UnMailEnv.pas',
  MailEnv in '..\..\..\..\..\Outros\Email\MailEnv.pas' {FmMailEnv},
  MailEnvEmail in '..\..\..\..\..\Outros\Email\MailEnvEmail.pas' {FmMailEnvEmail},
  GModule in '..\..\..\..\..\Outros\MySQL\GModule.pas' {GMod: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Syncobra';
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TDmod, Dmod);
  Application.CreateForm(TDmModCobranca, DmModCobranca);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TDModG, DModG);
  Application.Run;
end.
