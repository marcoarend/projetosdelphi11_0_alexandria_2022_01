object DModG: TDModG
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 597
  Width = 1037
  object MyPID_DB: TmySQLDatabase
    DatabaseName = 'mysql'
    DesignOptions = []
    UserName = 'root'
    ConnectOptions = []
    ConnectionCharacterSet = 'latin1'
    ConnectionCollation = 'latin1_swedish_ci'
    LoginPrompt = True
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=mysql'
      'UID=root')
    Left = 800
    Top = 4
  end
  object QrUpdPID1: TmySQLQuery
    Database = MyPID_DB
    Left = 800
    Top = 52
  end
  object QrDono: TmySQLQuery
    Database = MyPID_DB
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, EContato, E' +
        'Cel,'
      'Respons1, Respons2, ECEP, PCEP, EUF, PUF,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O,'
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia   ELSE en.Apelido END APELI' +
        'DO,'
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F,'
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG,'
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_,'
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA,'
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero+0.000     ELSE en.PNumero+0.' +
        '000 END NUMERO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE,'
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD,'
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF,'
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ','
      
        '/*CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Lo' +
        'grad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        '*/'
      'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,'
      
        'CASE WHEN en.Tipo=0 THEN en.ETe2        ELSE en.PTe2    END TE2,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM controle co'
      'LEFT JOIN entidades en ON en.Codigo=co.Dono'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd')
    Left = 8
    Top = 56
    object QrDonoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrDonoCadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrDonoNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrDonoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrDonoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrDonoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrDonoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrDonoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrDonoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrDonoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrDonoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrDonoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrDonoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrDonoTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrDonoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrDonoTE2: TWideStringField
      FieldName = 'TE2'
    end
    object QrDonoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrDonoENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrDonoPNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
    end
    object QrDonoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrDonoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrDonoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE2_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrDonoE_CUC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_CUC'
      Size = 255
      Calculated = True
    end
    object QrDonoEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrDonoECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrDonoCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoE_LN2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LN2'
      Size = 255
      Calculated = True
    end
    object QrDonoAPELIDO: TWideStringField
      FieldName = 'APELIDO'
      Size = 60
    end
    object QrDonoNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrDonoRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Size = 60
    end
    object QrDonoRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Size = 60
    end
    object QrDonoCEP: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CEP'
      Calculated = True
    end
    object QrDonoECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrDonoPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrDonoEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrDonoPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrDonoUF: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'UF'
      Calculated = True
    end
    object QrDonoFONES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FONES'
      Size = 255
      Calculated = True
    end
  end
  object QrPreEmail: TmySQLQuery
    Database = MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM preemail'
      'WHERE Codigo=:P0'
      '')
    Left = 628
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPreEmailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreEmailNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPreEmailSMTPServer: TWideStringField
      FieldName = 'SMTPServer'
      Size = 50
    end
    object QrPreEmailSend_Name: TWideStringField
      FieldName = 'Send_Name'
      Size = 50
    end
    object QrPreEmailSend_Mail: TWideStringField
      FieldName = 'Send_Mail'
      Size = 50
    end
    object QrPreEmailPass_Mail: TWideStringField
      FieldName = 'Pass_Mail'
      Size = 30
    end
    object QrPreEmailLogi_Name: TWideStringField
      FieldName = 'Logi_Name'
      Size = 30
    end
    object QrPreEmailLogi_Pass: TWideStringField
      FieldName = 'Logi_Pass'
      Size = 30
    end
    object QrPreEmailLogi_Auth: TSmallintField
      FieldName = 'Logi_Auth'
    end
    object QrPreEmailMail_Titu: TWideStringField
      FieldName = 'Mail_Titu'
      Size = 100
    end
    object QrPreEmailSaudacao: TWideStringField
      FieldName = 'Saudacao'
      Size = 100
    end
    object QrPreEmailNaoEnvBloq: TSmallintField
      FieldName = 'NaoEnvBloq'
    end
    object QrPreEmailPorta_mail: TIntegerField
      FieldName = 'Porta_mail'
    end
    object QrPreEmailLogi_SSL: TSmallintField
      FieldName = 'Logi_SSL'
    end
  end
  object QrAgora: TmySQLQuery
    Database = MyPID_DB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 20
    Top = 447
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrAux: TmySQLQuery
    Database = MyPID_DB
    Left = 80
    Top = 448
  end
end
