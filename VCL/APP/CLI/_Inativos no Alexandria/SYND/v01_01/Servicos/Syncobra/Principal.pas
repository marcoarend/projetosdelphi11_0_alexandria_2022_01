unit Principal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, AppEvnts, dmkGeral, ComCtrls, StdCtrls, Buttons,
  AdvGlowButton, dmkCheckBox, Registry;

type
  TFmPrincipal = class(TForm)
    TrayIcon1: TTrayIcon;
    ApplicationEvents1: TApplicationEvents;
    Timer1: TTimer;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    Memo1: TMemo;
    Timer2: TTimer;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    CkInicializacao: TdmkCheckBox;
    procedure ApplicationEvents1Minimize(Sender: TObject);
    procedure TrayIcon1DblClick(Sender: TObject);
    procedure ApplicationEvents1Activate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CkInicializacaoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
  public
    { Public declarations }
    FFirstTime: Boolean;
    FLastAtzInadimplentes: TDateTime;
    FAtualizandoInadimplentes: Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses ModuleGeral, ModCobranca;

{$R *.dfm}


procedure TFmPrincipal.ApplicationEvents1Activate(Sender: TObject);
begin
  if FFirstTime then
  begin
    FFirstTime := False;
    Hide();
  end;
end;

procedure TFmPrincipal.ApplicationEvents1Minimize(Sender: TObject);
begin
  Hide();
end;

procedure TFmPrincipal.CkInicializacaoClick(Sender: TObject);
begin
  ExecutaNaInicializacao(CkInicializacao.Checked, 'Syncobra - Auxiliar do Syndi2', Application.ExeName);
end;

procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo,
  Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_LOCAL_MACHINE;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  //DModG.MyPID_DB_Cria();
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  Inicializ: String;
begin
  FFirstTime := True;
  FLastAtzInadimplentes := 0;
  FAtualizandoInadimplentes := False;
  Geral.DefineFormatacoes();
  //
  Inicializ := Geral.ReadAppKey('Syncobra - Auxiliar do Syndi2',
    'Microsoft\Windows\CurrentVersion\Run', ktString, '', HKEY_LOCAL_MACHINE);
  if Inicializ <> '' then
    CkInicializacao.Checked := True
  else
    CkInicializacao.Checked := False;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
const
  CkForcaPsqChecked = False;
  Empr = 0;
  Enti = 0;
begin
  if FAtualizandoInadimplentes then
    exit;
  Timer1.Interval := 300000; // 5 minutos
  FAtualizandoInadimplentes := True;
  try
    if (Trunc(FLastAtzInadimplentes) < Trunc(DModG.ObtemAgora())) then
    begin
      //
      DmModCobranca.VerificaInadimplencia(wcForm, PB1, LaAviso1, LaAviso2,
        CkForcaPsqChecked, Empr, Enti);
      //
      DmModCobranca.AgendarEnvioDeMensagensAInadimplentes(wcForm,
                PB1, LaAviso1, LaAviso2);
      //
    end;
    DmModCobranca.EnviaAgendados_Email(wcForm, PB1, LaAviso1, LaAviso2);
    DmModCobranca.EnviaAgendados_SMS(wcForm, PB1, LaAviso1, LaAviso2, Memo1);
  finally
    FAtualizandoInadimplentes := False;
  end;
end;

procedure TFmPrincipal.TrayIcon1DblClick(Sender: TObject);
begin
  FmPrincipal.WindowState := wsMaximized;
  Show();
end;

end.
