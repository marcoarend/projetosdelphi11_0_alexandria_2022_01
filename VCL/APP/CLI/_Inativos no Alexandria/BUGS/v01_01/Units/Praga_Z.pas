unit Praga_Z;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, Grids, DBGrids, Jpeg, ComCtrls,
  dmkRichEdit, UnDmkEnums, UnProjGroup_Consts;

type
  TFmPraga_Z = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPraga_Z: TmySQLQuery;
    DsPraga_Z: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdNivSup: TdmkEditCB;
    CBNivSup: TdmkDBLookupComboBox;
    Label3: TLabel;
    QrPraga_A: TmySQLQuery;
    DsPraga_A: TDataSource;
    QrPraga_ACodigo: TIntegerField;
    QrPraga_ANome: TWideStringField;
    SbNivSup: TSpeedButton;
    QrPraga_ZNO_NIVSUP: TWideStringField;
    QrPraga_ZNivSup: TIntegerField;
    QrPraga_ZCodigo: TIntegerField;
    QrPraga_ZNome: TWideStringField;
    QrPraga_ZOrdem: TIntegerField;
    Label4: TLabel;
    EdOrdem: TdmkEdit;
    QrPraga_ZEspecie: TWideStringField;
    Label10: TLabel;
    EdEspecie: TdmkEdit;
    QrPraga_ZPrevencao: TWideMemoField;
    QrPraga_ZArqDir: TWideStringField;
    QrPraga_ZCopyright: TWideStringField;
    QrPraga_ZArqImg: TWideStringField;
    EdArqDir: TdmkEdit;
    EdArqImg: TdmkEdit;
    EdCopyright: TdmkEdit;
    SpeedButton5: TSpeedButton;
    Panel6: TPanel;
    Label19: TLabel;
    Label18: TLabel;
    QrPraga_ZTax: TmySQLQuery;
    DsPraga_ZTax: TDataSource;
    QrPraga_ZTaxCodigo: TIntegerField;
    QrPraga_ZTaxTaxonIts: TIntegerField;
    QrPraga_ZTaxNome: TWideStringField;
    QrPraga_ZTaxNO_TAXON_ITS: TWideStringField;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Panel8: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    Label14: TLabel;
    DBEdit5: TDBEdit;
    Label17: TLabel;
    Label15: TLabel;
    DBEdit7: TDBEdit;
    Label16: TLabel;
    DBEdit6: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnFoto: TPanel;
    Image1: TImage;
    DBGDados: TDBGrid;
    TabSheet2: TTabSheet;
    DBRichEdit1: TDBRichEdit;
    Panel7: TPanel;
    BtConfirma: TBitBtn;
    BtImportar: TBitBtn;
    BtVerFoto: TBitBtn;
    BtTexto: TBitBtn;
    TabSheet3: TTabSheet;
    DBRichEdit2: TDBRichEdit;
    QrPraga_ZObservacao: TWideMemoField;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    REObservacao: TdmkRichEdit;
    RePrevencao: TdmkRichEdit;
    CkContinuar: TCheckBox;
    BitBtn1: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPraga_ZAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPraga_ZBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbNivSupClick(Sender: TObject);
    procedure QrPraga_ZAfterScroll(DataSet: TDataSet);
    procedure QrPraga_ZBeforeClose(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtTextoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Image1Click(Sender: TObject);
  private
    FImagem: String;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenPraga_ZTax();
    //procedure TentaCarregarFoto();
    procedure TentaCarregarFoto2();
  public
    { Public declarations }
    FDataBase: TmySQLDatabase;
    FListaNiveis: TAppGrupLst;
    FNomTabCad, FNomTabGru: String;
    FNovoCodigo: TNovoCodigo;
    //
    procedure CriaOForm;
    procedure ReopenGru(Codigo: Integer);
    procedure VerificaTaxonomia(CodTxtTaxon, Descricao: String);
  end;

var
  FmPraga_Z: TFmPraga_Z;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal, DmkDAC_PF, CfgCadLista, UnAppListas,
  TaxonNet, MyDBCheck, UnBugs_Tabs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPraga_Z.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPraga_Z.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPraga_ZCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmPraga_Z.VerificaTaxonomia(CodTxtTaxon, Descricao: String);
var
  Nome, Codigo, TaxonIts: String;
begin
  Nome := Descricao;
  Codigo := Geral.FF0(EdCodigo.ValueVariant);
  TaxonIts := CodTxtTaxon;
  //
  UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'praga_ztax', False, [
  'Nome'], ['Codigo', 'TaxonIts'], ['Nome'], [
  Nome], [Codigo, TaxonIts], [Nome], True);
  //
  //CO_COD_TAXON_ITS_9
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPraga_Z.DefParams;
begin
  VAR_GOTOTABELA := FNomTabCad;
  VAR_GOTOMYSQLTABLE := QrPraga_Z;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := FDataBase;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  if FNomTabGru <> '' then
  begin
    VAR_SQLx.Add('SELECT gru.Nome NO_NIVSUP, cad.*');
    VAR_SQLx.Add('FROM ' + FNomTabCad + ' cad');
    VAR_SQLx.Add('LEFT JOIN ' + FNomTabGru + ' gru ON gru.Codigo=cad.NivSup');
  end else
  begin
    VAR_SQLx.Add('SELECT "N�o tem" NO_NIVSUP, cad.*');
    VAR_SQLx.Add('FROM ' + FNomTabCad + ' cad');
  end;
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE cad.Codigo > 0');
  //
  VAR_SQL1.Add('AND cad.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND cad.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cad.Nome LIKE :P0');
  //
end;

procedure TFmPraga_Z.EdCodigoChange(Sender: TObject);
begin
  BtImportar.Enabled := EdCodigo.ValueVariant <> 0;
end;

procedure TFmPraga_Z.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPraga_Z.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPraga_Z.ReopenGru(Codigo: Integer);
var
  C: Integer;
begin
  if Codigo <> 0 then
    C := Codigo
  else
    C := EdNivSup.ValueVariant;
  EdNivSup.ValueVariant := 0;
  CBNivSup.KeyValue := 0;
  //
  EdNivSup.Enabled := False;
  CBNivSup.Enabled := False;
  SbNivSup.Enabled := False;
  //
  QrPraga_A.Close;
  if FNomTabGru <> '' then
  begin
    EdNivSup.Enabled := True;
    CBNivSup.Enabled := True;
    SbNivSup.Enabled := True;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPraga_A, FDatabase, [
    'SELECT Codigo, Nome ',
    'FROM ' + FNomTabGru,
    'ORDER BY Nome ',
    '']);
    //
    if QrPraga_A.Locate('Codigo', C, []) then
    begin
      EdNivSup.ValueVariant := C;
      CBNivSup.KeyValue := C;
    end;
  end else
  begin
  end;
end;

procedure TFmPraga_Z.ReopenPraga_ZTax();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPraga_ZTax, Dmod.MyDB, [
  'SELECT pzt.Codigo, pzt.TaxonIts,pzt.Nome, txi.Nome NO_TAXON_ITS ',
  'FROM praga_ztax pzt ',
  'LEFT JOIN taxonits txi ON txi.Codigo=pzt.TaxonIts ',
  'WHERE pzt.Codigo=' + Geral.FF0(QrPraga_ZCodigo.Value),
  'ORDER BY txi.Ordem ',
  '']);
end;

procedure TFmPraga_Z.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPraga_Z.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPraga_Z.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPraga_Z.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPraga_Z.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPraga_Z.SpeedButton5Click(Sender: TObject);
var
  Res, Host, SDir, RDir, DirDest, ArqDir, ArqImg: String;
  Ext: TStringList;
  I: Integer;
begin
  Host := Dmod.QrOpcoesBugsImgIPv4.Value;
  SDir := Dmod.QrOpcoesBugsImgSDir.Value;
  RDir := QrPraga_ZArqDir.Value;
  //
  if (SDir <> '') then
  begin
    if MyObjects.TentaDefinirDiretorio(Host, SDir, 0, RDir, LaAviso1, LaAviso2, False, nil, DirDest) then
    begin
      Ext := TStringList.Create;
      try
        Ext.Add('.jpeg');
        Ext.Add('.jpg');
        Ext.Add('.png');
        Ext.Add('.gif');
        Ext.Add('.bmp');
        //
        Res := FmPrincipal.SelecionaImg(DirDest, QrPraga_ZArqImg.Value, Ext, True);
      finally
        Ext.Free;
      end;

      if Res <> '' then
      begin
        I := pos(LowerCase(DirDest), LowerCase(Res));
        if I <> 1 then
        begin
          Geral.MB_Aviso('Diret�rio inv�lido! ' + sLineBreak +
            'O diret�rio deve estar dentro do diret�rio raiz!');
        end else
        begin
          ArqImg := ExtractFileName(Res);
          //F := pos(ArqImg, IniDir);
          //ArqDir := Copy(Res, 1, F - 1);
          I := I + Length(DirDest);
          ArqDir := Copy(ExtractFilePath(Res), I);
          //
          EdArqDir.Text := ArqDir;
          EdArqImg.Text := ArqImg;
        end;
      end;
    end;
  end else
  begin
    Geral.MB_Aviso('O diret�rio raiz obrigat�rio definido nas ' + sLineBreak +
      'op��es espec�ficas n�o foi localizado:' + sLineBreak + DirDest);
  end;
end;

(*
procedure TFmPraga_Z.TentaCarregarFoto();
var
  //Titulo,
  ImgIPv4, ImgSDir, ArqDir, ArqImg, IniDir,
  //Res,
  FullDir: String;
  //I, F: Integer;
begin
  Image1.Visible := False;
  FImagem        := '';
  ImgIPv4        := dmkPF.IgnoraLocalhost(Dmod.QrOpcoesBugsImgIPv4.Value);
  ImgSDir        := dmkPF.VeSeEhDirDmk(Dmod.QrOpcoesBugsImgSDir.Value, '', False);
  ArqDir         := QrPraga_ZArqDir.Value;
  ArqImg         := QrPraga_ZArqImg.Value;

  if ImgIPv4 <> '' then
    ImgIPv4 :=  '\\' + ImgIPv4 + '\';
  IniDir := ImgIPv4 + ImgSDir;

  if DirectoryExists(IniDir) then
  begin
    FullDir := Geral.Substitui(IniDir + '\' + ArqDir + '\' + ArqImg, '\\', '\');
    while pos('\\', FullDir) > 0 do
      FullDir := Geral.Substitui(IniDir + '\' + ArqDir, '\\', '\');
    if (FullDir <> '') and (FullDir[1] = '\') then
      FullDir := '\' + FullDir;
    if FileExists(FullDir) then
    begin
      try
        Image1.Picture.LoadFromFile(FullDir);
        FImagem := FullDir;
        Image1.Visible := True;
      except
        MyObjects.Informa2(LaAviso1, LaAviso2, False,
          'N�o foi poss�vel carregar a foto!')
      end;
    end
    else
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'N�o foi poss�vel localizar o arquivo da foto!')
  end else
  begin
    if Geral.MB_Pergunta('O diret�rio raiz obrigat�rio definido nas op��es espec�ficas n�o foi localizado?' +
      sLineBreak + 'Deseja defini-lo agora?') = ID_YES
    then
      Dmod.MostraFormOpcoesBugs(1);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'O diret�rio raiz obrigat�rio definido nas op��es espec�ficas n�o foi localizado: '
      + IniDir);
  end;
end;
*)

procedure TFmPraga_Z.TentaCarregarFoto2;
var
  Host, SDir, RDir, DirDest: String;
begin
  Image1.Visible := False;
  FImagem        := '';
  Host           := Dmod.QrOpcoesBugsImgIPv4.Value;
  SDir           := Dmod.QrOpcoesBugsImgSDir.Value;
  RDir           := QrPraga_ZArqDir.Value;
  //
  if (SDir <> '') then
  begin
    if MyObjects.TentaDefinirDiretorio(Host, SDir, 0, RDir, LaAviso1, LaAviso2, False, nil, DirDest) then
      FImagem := DmkPF.CaminhoArquivo(DirDest, QrPraga_ZArqImg.Value, '');
    //
    if FImagem <> '' then
    begin
      if FileExists(FImagem) then
      begin
        try
          Image1.Picture.LoadFromFile(FImagem);
          Image1.Visible := True;
        except
          MyObjects.Informa2(LaAviso1, LaAviso2, False,
            'N�o foi poss�vel carregar a foto!')
        end;
      end
      else
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'N�o foi poss�vel localizar o arquivo da foto!');
    end;
  end else
  begin
    if Geral.MB_Pergunta('O diret�rio raiz obrigat�rio definido nas op��es espec�ficas n�o foi localizado?' +
      sLineBreak + 'Deseja defini-lo agora?') = ID_YES
    then
      Dmod.MostraFormOpcoesBugs(1);
  end;
end;

procedure TFmPraga_Z.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmPraga_Z.SbNivSupClick(Sender: TObject);
var
  NivSup: Integer;
begin
  VAR_CADASTRO := 0;
  NivSup       := EdNivSup.ValueVariant;

  FmPrincipal.MostraFormPraga_A(NivSup);

  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdNivSup, CBNivSup, QrPraga_A, VAR_CADASTRO);
end;

procedure TFmPraga_Z.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPraga_Z.BtTextoClick(Sender: TObject);
begin
  MyObjects.EditaTextoRichEdit(REObservacao, 10, 'Tahoma', []);
end;

procedure TFmPraga_Z.BitBtn1Click(Sender: TObject);
begin
  MyObjects.EditaTextoRichEdit(RePrevencao, 10, 'Tahoma', []);
end;

procedure TFmPraga_Z.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPraga_Z, [PnDados],
  [PnEdita], EdNome, ImgTipo, FNomTabCad);
end;

procedure TFmPraga_Z.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPraga_ZCodigo.Value;
  Close;
end;

procedure TFmPraga_Z.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
    //TxtTitRelC := MyObjects.ObtemTextoRichEdit(Self, RETxtTitRelC);
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32(FNomTabCad, 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrPraga_ZCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    FNomTabCad, Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if CkContinuar.Checked then
      BtIncluiClick(BtInclui);
  end;
end;

procedure TFmPraga_Z.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, FDataBase, FNomTabCad, 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPraga_Z.BtImportarClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTaxonNet, FmTaxonNet, afmoNegarComAviso) then
  begin
    FmTaxonNet.ShowModal;
    if FmTaxonNet.FSelecionou then
    begin
      EdEspecie.Text := FmTaxonNet.QrTaxonNetEspecie.Value;
      EdArqDir.Text := FmTaxonNet.QrTaxonNetArqDir.Value;
      EdArqImg.Text := FmTaxonNet.QrTaxonNetArqImg.Value;
      EdCopyright.Text := FmTaxonNet.QrTaxonNetCopyright.Value;
      //
      VerificaTaxonomia(CO_COD_TAXON_ITS_9, FmTaxonNet.QrTaxonNetFilo.Value);
      VerificaTaxonomia(CO_COD_TAXON_ITS_18, FmTaxonNet.QrTaxonNetClasse.Value);
      VerificaTaxonomia(CO_COD_TAXON_ITS_40, FmTaxonNet.QrTaxonNetOrdem.Value);
      VerificaTaxonomia(CO_COD_TAXON_ITS_58, FmTaxonNet.QrTaxonNetFamilia.Value);
      //
    end;
    FmTaxonNet.Destroy;
  end;
end;

procedure TFmPraga_Z.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPraga_Z, [PnDados],
  [PnEdita], EdNome, ImgTipo, FNomTabCad);
end;

procedure TFmPraga_Z.FormCreate(Sender: TObject);
begin
  FNomTabCad := 'praga_z';
  FNomTabGru := 'praga_a';
  FDataBase := Dmod.MyDB;
  FListaNiveis := AppListas.ListaNiveisPragas();
  FNovoCodigo := ncGerlSeq1;
  //
  ImgTipo.SQLType := stLok;
  PageControl1.Align := alClient;
  PageControl2.Align := alClient;
  //
  PageControl1.ActivePageIndex := 0;
  UMyMod.AbreQuery(QrPraga_A, Dmod.MyDB);
  //
  CriaOForm;
end;

procedure TFmPraga_Z.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPraga_ZCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPraga_Z.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPraga_Z.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrPraga_ZCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPraga_Z.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPraga_Z.QrPraga_ZAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPraga_Z.QrPraga_ZAfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrPraga_ZCodigo.Value <> 0;
  BtAltera.Enabled := Habilita;
  //BtExclui.Enabled := Habilita;
  //
  ReopenPraga_ZTax();
  //
  TentaCarregarFoto2();
  //
end;

procedure TFmPraga_Z.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPraga_Z.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPraga_ZCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, FNomTabCad, FDataBase, CO_VAZIO));
end;

procedure TFmPraga_Z.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPraga_Z.Image1Click(Sender: TObject);
begin
  if (FImagem = '') or (not FileExists(FImagem)) then Exit;
  //
  if Geral.MB_Pergunta('Deseja abrir o arquivo?') = ID_YES then
  begin
    Geral.AbreArquivo(FImagem, True);
  end;
end;

procedure TFmPraga_Z.QrPraga_ZBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
  //BtExclui.Enabled := False;
  //
  QrPraga_ZTax.Close;
  Image1.Visible := False;
end;

procedure TFmPraga_Z.QrPraga_ZBeforeOpen(DataSet: TDataSet);
begin
  QrPraga_ZCodigo.DisplayFormat := FFormatFloat;
end;

end.

