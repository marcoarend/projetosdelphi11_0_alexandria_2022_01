object FmFormulIF: TFmFormulIF
  Left = 339
  Top = 185
  Caption = 'FRM-APLIC-002 :: Itens de F'#243'rmula de Aplica'#231#227'o'
  ClientHeight = 814
  ClientWidth = 965
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 906
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 847
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 430
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de F'#243'rmula de Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 430
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de F'#243'rmula de Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 430
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de F'#243'rmula de Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 965
    Height = 614
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 965
      Height = 614
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 80
        Width = 965
        Height = 534
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 1
        object GBEdita: TGroupBox
          Left = 2
          Top = 18
          Width = 961
          Height = 514
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Itens da F'#243'rmula: '
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object DBGrid1: TDBGrid
            Left = 2
            Top = 18
            Width = 957
            Height = 494
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Color = clWhite
            DataSource = DsOSFrmRec
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGrid1CellClick
            OnColEnter = DBGrid1ColEnter
            OnColExit = DBGrid1ColExit
            OnDrawColumnCell = DBGrid1DrawColumnCell
            OnKeyDown = DBGrid1KeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'EhDiluente'
                Title.Caption = 'Dil'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ordem'
                Title.Caption = 'Seq.'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrvQtd'
                Title.Alignment = taRightJustify
                Title.Caption = 'Quantidade'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLAUNIDMED'
                Title.Caption = 'Unidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Alignment = taRightJustify
                Title.Caption = 'C'#243'd [F4]'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_GGX'
                Title.Caption = 'Descri'#231#227'o do Produto [F7]'
                Width = 465
                Visible = True
              end>
          end
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 965
        Height = 80
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 18
          Width = 961
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object Label10: TLabel
            Left = 10
            Top = 5
            Width = 68
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID F'#243'rmula:'
            FocusControl = DBEdit1
          end
          object Label11: TLabel
            Left = 158
            Top = 5
            Width = 65
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
            FocusControl = DBEdit2
          end
          object Label13: TLabel
            Left = 10
            Top = 34
            Width = 83
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Equipamento:'
            FocusControl = DBEdit4
          end
          object Label14: TLabel
            Left = 487
            Top = 34
            Width = 101
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade total:'
            FocusControl = DBEdit7
          end
          object Label15: TLabel
            Left = 719
            Top = 34
            Width = 107
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade '#225'gua:'
            FocusControl = DBEdit9
          end
          object DBEdit1: TDBEdit
            Left = 84
            Top = 0
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Codigo'
            DataSource = DsFormulas
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 226
            Top = 0
            Width = 725
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Nome'
            DataSource = DsFormulas
            TabOrder = 1
          end
          object DBEdit4: TDBEdit
            Left = 94
            Top = 30
            Width = 390
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_EQUIAPLIC'
            DataSource = DsFormulas
            TabOrder = 2
          end
          object DBEdit7: TDBEdit
            Left = 591
            Top = 30
            Width = 123
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'QtdTot'
            DataSource = DsFormulas
            TabOrder = 3
          end
          object DBEdit9: TDBEdit
            Left = 827
            Top = 30
            Width = 123
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'QtdQSP'
            DataSource = DsFormulas
            TabOrder = 4
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 673
    Width = 965
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 961
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 339
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Grade edit'#225'vel / Ctrl + Delete para excluir itens'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 339
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Grade edit'#225'vel / Ctrl + Delete para excluir itens'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 727
    Width = 965
    Height = 87
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 786
      Top = 18
      Width = 177
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 784
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
        OnClick = BtOKClick
      end
      object BtIts: TBitBtn
        Tag = 30
        Left = 166
        Top = 5
        Width = 148
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Produtos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtItsClick
      end
    end
  end
  object TbFormulIF: TmySQLTable
    Database = Dmod.MyDB
    BeforeInsert = TbFormulIFBeforeInsert
    BeforePost = TbFormulIFBeforePost
    AfterPost = TbFormulIFAfterPost
    AfterDelete = TbFormulIFAfterDelete
    OnNewRecord = TbFormulIFNewRecord
    SortFieldNames = 'Ordem'
    TableName = 'formulif'
    Left = 72
    Top = 196
    object TbFormulIFCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbFormulIFControle: TIntegerField
      FieldName = 'Controle'
    end
    object TbFormulIFGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object TbFormulIFPrvQtd: TFloatField
      FieldName = 'PrvQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object TbFormulIFPrvPrc: TFloatField
      FieldName = 'PrvPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbFormulIFPrvVal: TFloatField
      FieldName = 'PrvVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbFormulIFUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object TbFormulIFUsoPrc: TFloatField
      FieldName = 'UsoPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbFormulIFUsoVal: TFloatField
      FieldName = 'UsoVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbFormulIFUsoDec: TFloatField
      FieldName = 'UsoDec'
      DisplayFormat = '#,###,###,##0.00'
    end
    object TbFormulIFUsoTot: TFloatField
      FieldName = 'UsoTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object TbFormulIFOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object TbFormulIFReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object TbFormulIFEhDiluente: TSmallintField
      FieldName = 'EhDiluente'
    end
    object TbFormulIFNO_GGX: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_GGX'
      LookupDataSet = QrGGXs
      LookupKeyFields = 'Controle'
      LookupResultField = 'NO_PRD_TAM_COR'
      KeyFields = 'GraGruX'
      Size = 120
      Lookup = True
    end
    object TbFormulIFSIGLAUNIDMED: TWideStringField
      FieldKind = fkLookup
      FieldName = 'SIGLAUNIDMED'
      LookupDataSet = QrGGXs
      LookupKeyFields = 'Controle'
      LookupResultField = 'SIGLAUNIDMED'
      KeyFields = 'GraGruX'
      Size = 10
      Lookup = True
    end
  end
  object DsOSFrmRec: TDataSource
    DataSet = TbFormulIF
    Left = 72
    Top = 244
  end
  object DsGGxs: TDataSource
    DataSet = QrGGXs
    Left = 172
    Top = 244
  end
  object QrReordem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE formulif SET'
      'Ordem=Reordem'
      'WHERE Codigo=:P0'
      '')
    Left = 68
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrFormulas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFormulasAfterScroll
    SQL.Strings = (
      'SELECT gg1.Nome NO_EQUIAPLIC, frm.* '
      'FROM formulas frm'
      'LEFT JOIN gragrux ggx ON ggx.Controle = frm.EquipAplic'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE frm.Codigo=:P0'
      '')
    Left = 8
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFormulasNO_EQUIAPLIC: TWideStringField
      FieldName = 'NO_EQUIAPLIC'
      Size = 120
    end
    object QrFormulasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFormulasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFormulasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulasEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrFormulasQtdTot: TFloatField
      FieldName = 'QtdTot'
    end
    object QrFormulasQtdQSP: TFloatField
      FieldName = 'QtdQSP'
    end
    object QrFormulasCusTot: TFloatField
      FieldName = 'CusTot'
    end
    object QrFormulasDiluente: TSmallintField
      FieldName = 'Diluente'
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 36
    Top = 5
  end
  object QrGGXs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      '/*WHERE gg1.Nivel2 IN (-3,-2)*/'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 172
    Top = 196
    object QrGGXsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXsCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
end
