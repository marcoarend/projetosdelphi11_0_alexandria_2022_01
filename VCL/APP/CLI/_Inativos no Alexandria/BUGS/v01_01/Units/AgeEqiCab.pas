unit AgeEqiCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, dmkDBGridZTO, UnDmkEnums;

type
  TFmAgeEqiCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrAgeEqiCab: TmySQLQuery;
    DsAgeEqiCab: TDataSource;
    QrAgeEqiIts: TmySQLQuery;
    DsAgeEqiIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TdmkDBGridZTO;
    QrAgeEqiCabCodigo: TIntegerField;
    QrAgeEqiCabNome: TWideStringField;
    QrAgeEqiCabLk: TIntegerField;
    QrAgeEqiCabDataCad: TDateField;
    QrAgeEqiCabDataAlt: TDateField;
    QrAgeEqiCabUserCad: TIntegerField;
    QrAgeEqiCabUserAlt: TIntegerField;
    QrAgeEqiCabAlterWeb: TSmallintField;
    QrAgeEqiCabAtivo: TSmallintField;
    QrAgeEqiItsCodigo: TIntegerField;
    QrAgeEqiItsControle: TIntegerField;
    QrAgeEqiItsEntidade: TIntegerField;
    QrAgeEqiItsEhLider: TSmallintField;
    QrAgeEqiItsLk: TIntegerField;
    QrAgeEqiItsDataCad: TDateField;
    QrAgeEqiItsDataAlt: TDateField;
    QrAgeEqiItsUserCad: TIntegerField;
    QrAgeEqiItsUserAlt: TIntegerField;
    QrAgeEqiItsAlterWeb: TSmallintField;
    QrAgeEqiItsAtivo: TSmallintField;
    QrAgeEqiItsNO_AGENTE: TWideStringField;
    BtGerenciar: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrAgeEqiCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAgeEqiCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrAgeEqiCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrAgeEqiCabBeforeClose(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtGerenciarClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraAgeEqiIts(SQLType: TSQLType);
    procedure AtivaDesativaAgenteEntidade(Codigo, Ativo: Integer);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenAgeEqiIts(Controle: Integer);
  end;

var
  FmAgeEqiCab: TFmAgeEqiCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, DmkDAC_PF, AgeEqiIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAgeEqiCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAgeEqiCab.MostraAgeEqiIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmAgeEqiIts, FmAgeEqiIts, afmoNegarComAviso) then
  begin
    FmAgeEqiIts.ImgTipo.SQLType := SQLType;
    FmAgeEqiIts.FQrCab := QrAgeEqiCab;
    FmAgeEqiIts.FDsCab := DsAgeEqiCab;
    FmAgeEqiIts.FQrIts := QrAgeEqiIts;
    if SQLType = stIns then
      //
    else
    begin
      FmAgeEqiIts.EdControle.ValueVariant := QrAgeEqiItsControle.Value;
      //
      FmAgeEqiIts.CkEhLider.Checked := Geral.IntToBool(QrAgeEqiItsEhLider.Value);
      FmAgeEqiIts.EdEntidade.ValueVariant := QrAgeEqiItsEntidade.Value;
      FmAgeEqiIts.EdEntidade.ValueVariant := QrAgeEqiItsEntidade.Value;
      //
    end;
    FmAgeEqiIts.ShowModal;
    FmAgeEqiIts.Destroy;
  end;
end;

procedure TFmAgeEqiCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrAgeEqiCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrAgeEqiCab, QrAgeEqiIts);
end;

procedure TFmAgeEqiCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrAgeEqiCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrAgeEqiIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrAgeEqiIts);
end;

procedure TFmAgeEqiCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrAgeEqiCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAgeEqiCab.DefParams;
begin
  VAR_GOTOTABELA := 'ageeqicab';
  VAR_GOTOMYSQLTABLE := QrAgeEqiCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ageeqicab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmAgeEqiCab.ItsAltera1Click(Sender: TObject);
begin
  MostraAgeEqiIts(stUpd);
end;

procedure TFmAgeEqiCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmAgeEqiCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmAgeEqiCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAgeEqiCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada do agente selecionado?',
  'AgeEqiIts', 'Controle', QrAgeEqiItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrAgeEqiIts,
      QrAgeEqiItsControle, QrAgeEqiItsControle.Value);
    ReopenAgeEqiIts(Controle);
  end;
end;

procedure TFmAgeEqiCab.ReopenAgeEqiIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgeEqiIts, Dmod.MyDB, [
  'SELECT aei.*, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_AGENTE ',
  'FROM ageeqiits aei ',
  'LEFT JOIN entidades ent ON ent.Codigo=aei.Entidade ',
  'WHERE aei.Codigo=' + Geral.FF0(QrAgeEqiCabCodigo.Value),
  '']);
  //
  QrAgeEqiIts.Locate('Controle', Controle, []);
end;


procedure TFmAgeEqiCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmAgeEqiCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAgeEqiCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAgeEqiCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAgeEqiCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAgeEqiCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAgeEqiCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrAgeEqiCabCodigo.Value;
  Close;
end;

procedure TFmAgeEqiCab.ItsInclui1Click(Sender: TObject);
begin
  MostraAgeEqiIts(stIns);
end;

procedure TFmAgeEqiCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrAgeEqiCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ageeqicab');
end;

procedure TFmAgeEqiCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  Codigo := UMyMod.BPGS1I32('ageeqicab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ageeqicab', False, [
  'Nome'], [
  'Codigo'], [
  Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmAgeEqiCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ageeqicab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ageeqicab', 'Codigo');
end;

procedure TFmAgeEqiCab.BtGerenciarClick(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Gerenciamento de Agentes';
  Prompt1 = 'Informe se o cadastro do agente est� ativo:';
  Prompt2 = 'Desmarque os agentes que est�o inativos:';
var
  Entidade, Ativo: Integer;
begin
  if DBCheck.EscolheCodigosMultiplos_0(
    Aviso, Caption, Prompt2, nil, 'Ativo', 'Nivel1', 'Nome', [
    'DELETE FROM _selcods_; ',
    'INSERT INTO _selcods_ ',
    'SELECT Codigo Nivel1, 0 Nivel2, ',
    '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, Ativo ',
    'FROM ' + TMeuDB + '.entidades ',
    'WHERE Nome <> "" ',
    'AND ' + VAR_FP_FUNCION,
    'ORDER BY Nome;',
    ''],[
    'SELECT * FROM _selcods_; ',
    ''], Dmod.QrUpd) then
  begin
    DModG.QrSelCods.First;
    while not DModG.QrSelCods.Eof do
    begin
      Entidade := DModG.QrSelCodsNivel1.Value;
      Ativo    := DModG.QrSelCodsAtivo.Value;
      //
      AtivaDesativaAgenteEntidade(Entidade, Ativo);
      //
      DModG.QrSelCods.Next;
    end;
  end;
end;

procedure TFmAgeEqiCab.BtItsClick(Sender: TObject);
begin
////////////////////////////////////////////////////////////////////////////////
///
///  N�o permitir altera��o!
///  A equipe � cadastrada pelo sistema!
///
////////////////////////////////////////////////////////////////////////////////
//
  //MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
//
end;

procedure TFmAgeEqiCab.AtivaDesativaAgenteEntidade(Codigo, Ativo: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entidades', False,
    ['Ativo'], ['Codigo'], [Ativo], [Codigo], True);
end;

procedure TFmAgeEqiCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmAgeEqiCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  DGDados.Align   := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmAgeEqiCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrAgeEqiCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAgeEqiCab.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmAgeEqiCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAgeEqiCab.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrAgeEqiCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAgeEqiCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmAgeEqiCab.QrAgeEqiCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmAgeEqiCab.QrAgeEqiCabAfterScroll(DataSet: TDataSet);
begin
  ReopenAgeEqiIts(0);
end;

procedure TFmAgeEqiCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrAgeEqiCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmAgeEqiCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrAgeEqiCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ageeqicab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmAgeEqiCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgeEqiCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrAgeEqiCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'ageeqicab');
end;

procedure TFmAgeEqiCab.QrAgeEqiCabBeforeClose(
  DataSet: TDataSet);
begin
  QrAgeEqiIts.Close;
end;

procedure TFmAgeEqiCab.QrAgeEqiCabBeforeOpen(DataSet: TDataSet);
begin
  QrAgeEqiCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

