unit RMIP_R005;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  mySQLDbTables, Data.DB, frxClass, frxDBSet, UnDmkEnums, DmkGeral,
  UnInternalConsts, UnProjGroup_Consts, UnDmkProcFunc, UnMyObjects,
  frxChart, VCLTee.Chart;

type
  TFmRMIP_R005 = class(TForm)
    frxReport005A: TfrxReport;
    Qr005B_PrgLstIts: TmySQLQuery;
    Qr005B_PrgLstItsPrgLstCab: TIntegerField;
    Qr005B_PrgLstItsPrgLstIts: TIntegerField;
    Qr005B_PrgLstItsNO_LST: TWideStringField;
    Qr005B_PrgLstItsSigla: TWideStringField;
    Qr005B_PrgLstItsNome: TWideStringField;
    Qr005B_PrgLstItsFuncoes: TIntegerField;
    Qr005B_PrgLstItsCodigo: TFloatField;
    Qr005A_OSPipIts: TmySQLQuery;
    Qr005A_OSPipItsITENS: TLargeintField;
    Qr005A_OSPipItsRespondido: TSmallintField;
    Qr005A_OSPipItsRespAtrCad: TIntegerField;
    Qr005A_OSPipItsRespAtrIts: TIntegerField;
    Qr005A_OSPipItsResposta: TWideStringField;
    Qr005A_OSPipItsCorPizza: TFloatField;
    Qr005A_OSPipItsDtaExeFim: TDateTimeField;
    Qr005A_OSs: TmySQLQuery;
    Qr005A_OSsCodigo: TIntegerField;
    Qr005A_OSsEntidade: TIntegerField;
    Qr005A_OSsSiapTerCad: TIntegerField;
    Qr005A_OSsEstatus: TIntegerField;
    Qr005A_OSsDtaExeFim: TDateTimeField;
    Qr005A_OSsNumContrat: TIntegerField;
    Qr005A_OSsGrupo: TIntegerField;
    Qr005A_OSsNO_FatoGeradr: TWideStringField;
    Qr005A_OSsNO_ESTATUS: TWideStringField;
    Qr005A_OSsNO_SiapTerCad: TWideStringField;
    Qr005A_OSsNO_ENT: TWideStringField;
    frxDs005A_OSs: TfrxDBDataset;
    Qr005B_PrgLstItsRespCod: TFloatField;
    Qr005B_PrgLstItsResposta: TWideStringField;
    frxDs005B_PrgLstIts: TfrxDBDataset;
    Qr005A_PrgLstIts: TmySQLQuery;
    Qr005A_PrgLstItsPrgLstCab: TIntegerField;
    Qr005A_PrgLstItsPrgLstIts: TIntegerField;
    Qr005A_PrgLstItsNO_LST: TWideStringField;
    Qr005A_PrgLstItsSigla: TWideStringField;
    Qr005A_PrgLstItsNome: TWideStringField;
    Qr005A_PrgLstItsFuncoes: TIntegerField;
    Qr005A_PrgLstItsCodigo: TFloatField;
    frxDs005A_PrgLstIts: TfrxDBDataset;
    Qr005B_PrgLstItsRespondido: TIntegerField;
    frxDs005A_OSPipIts: TfrxDBDataset;
    procedure Qr005B_PrgLstItsAfterScroll(DataSet: TDataSet);
    procedure Qr005A_PrgLstItsAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FAplicacao: Integer;
    FAbrangencia: TRelAbrange;
    //
    procedure Reopen003A_PrgLstIts();
    procedure Reopen005A_PrgLstIts();
    procedure Reopen005A_OSPipIts(Localizador, PrgLstIts, Funcoes: Integer;
              DtaIni, DtaFim: TDateTime; Abrangencia: TRelAbrange; ItemRel: Integer);
  public
    { Public declarations }
    FOSCab, FEmpresa, FCliente, FItemRel: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    //
    procedure GeraImp_PizzaUltimaRevisaoPMV();
    procedure GeraImp_PizzaRevisaoPMVPeriodo();
  end;

var
  FmRMIP_R005: TFmRMIP_R005;

implementation

{$R *.dfm}

uses Module, ModuleGeral, ModuleRMIP, DmkDAC_PF;

{ TFmRMIP_R003 }

procedure TFmRMIP_R005.FormCreate(Sender: TObject);
var
  Chart: TfrxChartView;
begin
  //////////// Saber (registrar) as mudancas para outros gr�ficos
  Chart := frxReport005A.FindObject('Chart005A') as TfrxChartView;
  Chart.Chart.Legend.LegendStyle := lsAuto;  // VCLTee.Chart
  // FIM Chart
end;

procedure TFmRMIP_R005.GeraImp_PizzaRevisaoPMVPeriodo();
const
  Cliente = 0;
var
  Aviso: String;
begin
  //FOSCab := 0;
  FAplicacao := CO_GeraGrafRelatPMV_0004_COD_PizzaMonitsDep;
  FAbrangencia := relAbrangeEmpresa;
  Qr005B_PrgLstIts.Close;
  Aviso := ' ';
  Reopen003A_PrgLstIts();
  //
  if Qr005A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso :=
      'Question�rios dos monitoramentos pesquisados sem perguntas geradoras do gr�fico "' +
      CO_GeraGrafRelatPMV_0004_TXT_PizzaMonitsDep + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Per�odo: ' + Geral.FDT(FDtaIni, 3) + ' a ' + Geral.FDT(FDtaFim, 2);
  //
  frxReport005A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport005A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport005A.Variables['VARF_DATA']    := FDtaImp;
  frxReport005A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport005A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  MyObjects.frxDefineDataSets(frxReport005A, [
    frxDs005A_OSPipIts,
    frxDs005A_PrgLstIts,
    frxDs005B_PrgLstIts,
    frxDs005A_OSs]);
  frxReport005A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
  //MyObjects.frxPrepara(frxReport005A, CO_GeraGrafRelatPMV_0004_TXT_PizzaMonitsDep);
end;

procedure TFmRMIP_R005.GeraImp_PizzaUltimaRevisaoPMV();
const
  DtaIni = 0;
  DtaFim = 0;
var
  Qry: TmySQLQuery;
  Aviso: String;
begin
  Qr005B_PrgLstIts.Close;
  Aviso := ' ';
(*
// Ultimo monitormento finalizado do cliente selecionado!
  FOSCab := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT cab.Codigo, cab.DtaExeFim ',
    'FROM oscab cab ',
    'LEFT JOIN ' + TMeuDB + '.ospipits opi ON opi.Codigo=cab.Codigo ',
    'WHERE cab.Empresa=' + Geral.FF0(FEmpresa),
    'AND cab.Entidade=' + Geral.FF0(FCliente),
    'AND cab.DtaExeFim > "1900-01-01" ',
    'AND cab.Operacao & ' + Geral.FF0(CO_OS_OPERACAO_SERVICO_MONITORAMENTO),
    'AND opi.Codigo IS NOT NULL ',
    'ORDER BY cab.DtaExeFim DESC ',
    'LIMIT 1 ',
    '']);
    //
    FOSCab := Qry.FieldByName('Codigo').AsInteger;
  finally
    Qry.Free;
  end;
  //
  if FOSCab = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Cliente sem monitoramento respondido e finalizado!';
  end;
*)
  FAplicacao := CO_GeraGrafRelatPMV_0004_COD_PizzaMonitsDep;
  FAbrangencia := relAbrangeCliente;

  Reopen003A_PrgLstIts();
  //
  if Qr005A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Question�rios do monitoramento do localizador ' +
      Geral.FF0(FOSCab) + ' sem perguntas geradoras do gr�fico "' +
      CO_GeraGrafRelatPMV_0004_TXT_PizzaMonitsDep + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Localizador: ' + Geral.FF0(FOSCab);

  frxReport005A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport005A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport005A.Variables['VARF_DATA']    := FDtaImp;
  frxReport005A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport005A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  MyObjects.frxDefineDataSets(frxReport005A, [
    frxDs005A_OSPipIts,
    frxDs005A_PrgLstIts,
    frxDs005B_PrgLstIts,
    frxDs005A_OSs]);
  frxReport005A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
  //MyObjects.frxMostra(frxReport005A, CO_GeraGrafRelatPMV_0004_TXT_PizzaMonitsDep);
  //MyObjects.frxPrepara(frxReport005A, CO_GeraGrafRelatPMV_0004_TXT_PizzaMonitsDep);
end;

procedure TFmRMIP_R005.Qr005A_PrgLstItsAfterScroll(DataSet: TDataSet);
begin
  Reopen005A_PrgLstIts();
end;

procedure TFmRMIP_R005.Qr005B_PrgLstItsAfterScroll(DataSet: TDataSet);
var
  Localizador, PrgLstIts, Funcoes: Integer;
  Chart1: TfrxChartView;
  Abrangencia: TRelAbrange;
begin
  if Qr005B_PrgLstIts.RecordCount = 0 then
    Exit; // ????
  //  Agrupar por Dependencia!
  Localizador := Trunc(Qr005B_PrgLstItsCodigo.Value);
  PrgLstIts   := Qr005B_PrgLstItsPrgLstIts.Value;
  Funcoes     := Qr005B_PrgLstItsFuncoes.Value;
  Abrangencia := DmRMIP.DefineAbrangencia(FCliente);
  Reopen005A_OSPipIts(Localizador, PrgLstIts, Funcoes, FDtaIni, FDtaFim,
    Abrangencia, FItemRel);
  Chart1 := frxReport005A.FindObject('Chart005A') as TfrxChartView;
  // Source3 = Cores!
(*  Usar cores do Fast Report!!
  if Funcoes in ([CO_PRG_LST_QUANTIT, CO_PRG_LST_BXAPROD, CO_PRG_LST_TXTLIVR]) then
    Chart1.SeriesData.Items[0].Source3 := ''
  else
    Chart1.SeriesData.Items[0].Source3 := 'frxDs003A_OSPipIts."CorPizza"';
*)
end;

procedure TFmRMIP_R005.Reopen005A_OSPipIts(Localizador, PrgLstIts,
  Funcoes: Integer; DtaIni, DtaFim: TDateTime;
  Abrangencia: TRelAbrange; ItemRel: Integer);
var
  Resposta, CorPizza, LeftJoin, OrderBy, GroupBy, SQL_Extra, Xtra_Group,
  SQL_Qtde, SQLPadrao, FldAgrup, SQL_Extr2, RespTxt: String;
  Respondido, RespCod: Integer;
begin
  case TItensRMIP(ItemRel) of
    rmipLinhaEvoluHistoriDep: Xtra_Group := '';
    rmipPizzaMonitsDep: Xtra_Group := ', DtaExeFim';
    else Xtra_Group := ', ???';
  end;
  //
  if not DmRMIP.SQL_Funcoes(PrgLstIts, Funcoes, Resposta, CorPizza, LeftJoin, OrderBy,
  GroupBy, False) then
    Exit;
  //
  if (Abrangencia = relAbrangeCliente)
  and (TItensRMIP(ItemRel) = rmipPizzaMonitsDep) then
    SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(Localizador)
  else //relAbrangeEmpresa
    SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True);
  //
  Respondido := Qr005B_PrgLstItsRespondido.Value;
  RespCod := Trunc(Qr005B_PrgLstItsRespCod.Value);
  RespTxt := Qr005B_PrgLstItsResposta.Value;
  //
  //Resposta, // Fixo pela dependencia!
  Resposta := Geral.ATS([
  'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
  'IF(dep.Codigo > 0, dep.Nome, "' + CO_PIP_SemDependencia + '"))']);
  case Funcoes of
    CO_PRG_LST_BXAPROD: // 3
      SQL_Extr2 := 'AND opr.GraGruX =' + Geral.FF0(RespCod);
    CO_PRG_LST_ATRIBUT: // 4
      SQL_Extr2 := 'AND opi.RespAtrIts=' + Geral.FF0(RespCod);
    CO_PRG_LST_BINARIO, // 1
    CO_PRG_LST_QUANTIT, // 2
    CO_PRG_LST_TXTLIVR: // 5
      SQL_Extr2 := 'AND ' + Resposta + '="' + RespTxt + '"';
    else
    begin
      Geral.MB_Erro(
      '"Funcoes" nao implementado em "TFmRMIP_R005.Reopen005A_OSPipIts()"' +
      sLineBreak +
      'Funcoes = ' + Geral.FF0(Funcoes));
      EXIT;
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr005A_OSPipIts, Dmod.MyDB, [
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  //Resposta, // Fixo pela dependencia!
  Resposta + ' Resposta, ',
  //CorPizza,
  '8421504/*cinza*/ + 0.000 CorPizza ',  // usar do FastReport!
  LeftJoin,
  'LEFT JOIN ' + TMeuDB + '.ospipmon mon ON mon.Controle=opi.Controle ',
  'LEFT JOIN ' + TMeuDB + '.pipcad pip ON pip.Codigo=mon.PipCad ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=pip.Dependenci ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dep ON dep.Codigo=sid.Dependenci ',
  //
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  //
  SQL_Extra,
  'AND opi.Respondido=' + Geral.FF0(Respondido),
  SQL_Extr2,

  'GROUP BY cab.DtaExeFim, opi.Respondido, Resposta ',
  'ORDER BY cab.DtaExeFim, ITENS DESC ',
  '']);

  // OSs afetadas:
  DmRMIP.ReopenOSsAfetadas(Qr005A_OSs, PrgLstIts, LeftJoin, SQL_Extra);
end;

procedure TFmRMIP_R005.Reopen005A_PrgLstIts();
var
  PrgLstIts, Funcoes: Integer;
  Resposta, CorPizza, LeftJoin, OrderBy, GroupBy, SQL_Extra, Xtra_Group,
  SQL_Qtde, SQLPadrao, FldAgrup, RespCod, SQL_Codigo, LeftJoi2: String;
begin
  // Evitar avisos desnecessarios:
  if Qr005A_PrgLstIts.RecordCount = 0 then
  begin
    Qr005B_PrgLstIts.SQL.Text := '';
    Qr005B_PrgLstIts.Close;
    Exit;
  end;
  case FAbrangencia of
    relAbrangeCliente:
    begin
      SQL_Codigo := 'opi.Codigo + 0.000';
      SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(FOSCab);
    end;
    relAbrangeEmpresa:
    begin
      SQL_Codigo := '0.000';
      SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', FDtaIni, FDtaFim, True, True);
    end
    else
    begin
      SQL_Codigo := '???';
      SQL_Extra := '? ? ?';
    end;
  end;
  //
  PrgLstIts := Qr005A_PrgLstItsPrgLstIts.Value;
  Funcoes   := Qr005A_PrgLstItsFuncoes.Value;

  if not DmRMIP.SQL_Funcoes2(PrgLstIts, Funcoes, RespCod, Resposta, CorPizza,
  LeftJoin, OrderBy, GroupBy) then
    Exit;
  //

  if Funcoes = CO_PRG_LST_QUANTIT then
  LeftJoi2 := Geral.ATS([
  'LEFT JOIN ' + TMeuDB + '.prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  //'LEFT JOIN ' + TMeuDB + '.prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN ' + TMeuDB + '.prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  ''])
  else
  LeftJoi2 := Geral.ATS([
  'LEFT JOIN ' + TMeuDB + '.prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN ' + TMeuDB + '.prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN ' + TMeuDB + '.prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(Qr005B_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT ' + RespCod,
  Resposta,
  SQL_Codigo + ' Codigo, opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes, opi.Respondido ',
  LeftJoin,
  LeftJoi2,
  'WHERE pli.Aplicacao & ' + Geral.FF0(FAplicacao),
  'AND opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
end;

procedure TFmRMIP_R005.Reopen003A_PrgLstIts();
var
  SQL_Codigo, SQL_Extra, SQL_DtExe: String;
begin
  case FAbrangencia of
    relAbrangeCliente:
    begin
      SQL_Codigo := 'opi.Codigo + 0.000';
      SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(FOSCab);
      SQL_DtExe := '0 DtaExeFim, ';
    end;
    relAbrangeEmpresa:
    begin
      SQL_Codigo := '0.000';
      SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', FDtaIni, FDtaFim, True, True);
      SQL_DtExe := '0 DtaExeFim, ';
    end
    else
    begin
      SQL_Codigo := '???';
      SQL_Extra := '? ? ?';
      SQL_DtExe := '??? DtaExeFim, ';
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr005A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT ' +
  SQL_DtExe,
  SQL_Codigo + ' Codigo, opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN ' + TMeuDB + '.prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN ' + TMeuDB + '.prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(FAplicacao),
  SQL_Extra,
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
  //Geral.MB_SQL(Self, Qr005A_PrgLstIts);
end;

end.
