unit RMIP_R004;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Data.DB, mySQLDbTables, dmkGeral, UnDmkEnums, frxClass,
  frxDBSet, frxChart, UnProjGroup_Consts, UnInternalConsts, UnMyObjects,
  UnDmkProcFunc, VCLTee.Chart, VCLTee.Series(*, fs_ichartrtti*);

type
  TFmRMIP_R004 = class(TForm)
    Qr004A_PrgLstIts: TmySQLQuery;
    frxReport004A: TfrxReport;
    Qr004A_PrgLstItsPrgLstCab: TIntegerField;
    Qr004A_PrgLstItsPrgLstIts: TIntegerField;
    Qr004A_PrgLstItsNO_LST: TWideStringField;
    Qr004A_PrgLstItsSigla: TWideStringField;
    Qr004A_PrgLstItsNome: TWideStringField;
    Qr004A_PrgLstItsFuncoes: TSmallintField;
    Qr004Grupos: TmySQLQuery;
    Qr004NResp: TmySQLQuery;
    IntegerField1: TIntegerField;
    Qr004XVals: TmySQLQuery;
    Qr004Itens: TmySQLQuery;
    frxDs004A_PrgLstIts: TfrxDBDataset;
    Qr004ItensITENS: TFloatField;
    Qr004ItensDtaExeFim: TDateField;
    Qr004XValsDtaExeFim: TDateField;
    Qr004XValsDIAS: TIntegerField;
    Qr004ItensDIAS: TIntegerField;
    Qr004Dados: TmySQLQuery;
    Qr004DadosResposta: TWideStringField;
    Qr004DadosCorPizza: TIntegerField;
    Qr004GruposResposta: TWideStringField;
    Qr004A_OSs: TmySQLQuery;
    Qr004A_OSsCodigo: TIntegerField;
    Qr004A_OSsEntidade: TIntegerField;
    Qr004A_OSsSiapTerCad: TIntegerField;
    Qr004A_OSsEstatus: TIntegerField;
    Qr004A_OSsDtaExeFim: TDateTimeField;
    Qr004A_OSsNumContrat: TIntegerField;
    Qr004A_OSsGrupo: TIntegerField;
    Qr004A_OSsNO_FatoGeradr: TWideStringField;
    Qr004A_OSsNO_ESTATUS: TWideStringField;
    Qr004A_OSsNO_SiapTerCad: TWideStringField;
    Qr004A_OSsNO_ENT: TWideStringField;
    frxDs004A_OSs: TfrxDBDataset;
    procedure FormCreate(Sender: TObject);
    procedure Qr004A_PrgLstItsAfterScroll(DataSet: TDataSet);
    procedure Qr004GruposAfterOpen(DataSet: TDataSet);
    procedure frxReport004AGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    F_RMIP_004Atr: String;
    //
    function  ObtemNomeArr(Nome: String; Index: Integer): String;
    procedure Reopen004A_PrgLstIts();
    procedure ReopenXVals();
    function  R004_GeraDados(PrgLstIts, Cliente: Integer; DtaIni, DtaFim:
              TDateTime; Funcoes: Integer): Boolean;
  public
    { Public declarations }
    FCliente, FAplicacao: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL, FAplic_Tit: String;
    //
    procedure GeraImp_LinhaEvolucaoHistorico((*FmRMIP: TForm;*));
    procedure frxReport000GetValue(frxReport: TfrxReport; const VarName: string; var Value: Variant);
  end;

var
  FmRMIP_R004: TFmRMIP_R004;

implementation

uses Module, ModuleRMIP, DmkDAC_PF, ModuleGeral, CreateBugs;

var
  FSeries: Integer = 0;
  FYCount: Integer = 0;
  FNResp: Boolean;
  FXSource: String;
  FXValues: array of String;
  FYValues: array of array of String;
  FTitles: array of String;
  FCores: array of TColor;

{$R *.dfm}

procedure TFmRMIP_R004.FormCreate(Sender: TObject);
var
  I: Integer;
  Chart1: TfrxChartView;
begin
  F_RMIP_004Atr := UnCreateBugs.RecriaTempTableNovo(ntrtt_RMIP_004Atr,
    DmodG.QrUpdPID1, False);
  //
  //////////// Saber (registrar) as mudancas para outros gr�ficos
  Chart1 := frxReport004A.FindObject('Chart004A') as TfrxChartView;
  Chart1.Chart.LeftAxis.Axis.Color := clSilver;
  Chart1.Chart.LeftAxis.Axis.Width := 1;
  Chart1.Chart.LeftAxis.Grid.Color := clSilver;
  Chart1.Chart.LeftAxis.Grid.Width := 1;
  Chart1.Chart.LeftAxis.Grid.SmallDots := True;
  Chart1.Chart.BottomAxis.Axis.Color := clSilver;
  Chart1.Chart.BottomAxis.Axis.Width := 1;
  Chart1.Chart.BottomAxis.Grid.Color := clSilver;
  Chart1.Chart.BottomAxis.Grid.Width := 1;
  Chart1.Chart.BottomAxis.Grid.SmallDots := True;
  Chart1.Chart.Legend.LegendStyle := lsSeries;  // VCLTee.Chart
  // FIM Chart 1
end;

procedure TFmRMIP_R004.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
var
  Chart1: TfrxChartView;
  //x: TfrxChartSeries;
  x: TfrxSeriesItem;
  //
  YSource: String;
  I, J: Integer;
begin
  if VarName = 'VAR_LINE_TEE_004' then
  begin
    Value := True;
    //
    Chart1 := frxReport.FindObject('Chart004A') as TfrxChartView;
    while Chart1.SeriesData.Count > 0 do
      Chart1.SeriesData[0].Free;
    while Chart1.Chart.SeriesCount > 0 do
      Chart1.Chart.Series[0].Free;
    //
    //Chart1.Chart.Legend.LegendStyle := lsSerie;
    for I := 0 to FSeries - 1 do
    begin
      YSource := '';
      for J := 0 to FYCount - 1 do
      begin
        YSource := YSource + ';' + FYValues[I][J];
      end;
      if Length(YSource) > 0 then
        YSource := Copy(YSource, 2);
      //
      Chart1.AddSeries(TfrxChartSeries.csLine);

      // do VCLTee.Chart.CustomChart:
      Chart1.Chart.Series[I].Pen.Width := 4;
      Chart1.Chart.Series[I].LegendTitle := FTitles[I];
      //Chart1.Chart.Series[I].Color := FCores[I];

      // do frxChat
      Chart1.SeriesData[I].DataType := dtFixedData;
      Chart1.SeriesData[I].XSource := FXSource; //'value1; value2; value3';
      Chart1.SeriesData[I].YSource := YSource; //'31.5;28.54;31.58';
      Chart1.SeriesData[I].SortOrder := soNone;
      Chart1.SeriesData[I].TopN := 0;
      Chart1.SeriesData[I].XType := xtText;
    end;
  end;
end;

procedure TFmRMIP_R004.frxReport004AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport004A, VarName, Value)
end;

procedure TFmRMIP_R004.GeraImp_LinhaEvolucaoHistorico();
const
  OSCab = 0;
  Sim = 1;
  //
var
  Aviso: String;
  I, J, PrgLstIts: Integer;
  Qryes: array of TmySQLQuery;
  //Chart1: TfrxChartView;
  //
begin
  Qr004A_PrgLstIts.Close;
  Aviso := ' ';
  Reopen004A_PrgLstIts();
  //
  if Qr004A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso :=
      'Question�rios dos monitoramentos pesquisados sem perguntas geradoras do gr�fico "' +
      FAplic_Tit + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Per�odo: ' + Geral.FDT(FDtaIni, 3) + ' a ' + Geral.FDT(FDtaFim, 2);
  //
  frxReport004A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport004A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport004A.Variables['VARF_DATA']    := FDtaImp;
  frxReport004A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport004A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  //
  MyObjects.frxDefineDataSets(frxReport004A, [
  frxDs004A_PrgLstIts,
  frxDs004A_OSs
  ]);
  //MyObjects.frxPrepara(frxReport004A, FAplic_Tit);
end;

function TFmRMIP_R004.ObtemNomeArr(Nome: String; Index: Integer): String;
begin
  Result := Nome + FormatFloat('00', Index);
end;

procedure TFmRMIP_R004.Qr004GruposAfterOpen(DataSet: TDataSet);
  procedure DefineDadosInformados(Item: Integer);
  begin
    Qr004Itens.First;
    while not Qr004Itens.Eof do
    begin
      if Qr004XVals.Locate('DIAS', Qr004ItensDIAS.Value, []) then
      begin
        FYValues[Item][Qr004XVals.RecNo - 1] := Geral.FFI(Qr004ItensITENS.Value);
      end else
      begin
        raise Exception.Create('Erro: Data n�o localizada (1)!');
      end;
      //
      Qr004Itens.Next;
    end;
  end;
var
  I, J, Item: Integer;
begin
  ReopenXVals();
  FYCount := Qr004XVals.RecordCount;
  SetLength(FXValues, FYCount);
  Qr004XVals.First;
  FXSource := '';
  while not Qr004XVals.Eof do
  begin
    FXSource := FXSource + ';' + Geral.FDT(Qr004XValsDtaExeFim.Value, 3);
    FXValues[Qr004XVals.RecNo - 1] := Geral.FDT(Qr004XValsDtaExeFim.Value, 3);
    //
    Qr004XVals.Next;
  end;
  if Length(FXSource) > 1 then
    FXSource := Copy(FXSource, 2);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr004NResp, DModG.MyPID_DB, [
  'SELECT RespAtrIts ',
  'FROM ' + F_RMIP_004Atr,
  'WHERE Respondido = 0 ',
  '']);
  FNResp := Qr004NResp.RecordCount > 0;
  if FNResp then
    FSeries := 1
  else
    FSeries := 0;
  FSeries := FSeries + Qr004Grupos.RecordCount;
  SetLength(FYValues, FSeries);
  SetLength(FTitles, FSeries);
  SetLength(FCores, FSeries);
  for I := 0 to FSeries - 1 do
  begin
    SetLength(FYValues[I], FYCount);
    for J := 0 to FYCount - 1 do
      FYValues[I][J] := '0';
  end;
  Item := -1;
///////////////////////// Nao respondidos
  if FNResp then
  begin
    Item := Item + 1;
    FTitles[Item] := CO_NAO_RESPONDIDO;
    FCores[Item]  := 8421504; // Cinza
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr004Itens, DModG.MyPID_DB, [
    'SELECT DtaExeFim, SUM(ITENS) + 0.000 ITENS, ',
    'TO_DAYS(DtaExeFim) DIAS ',
    'FROM ' + F_RMIP_004Atr,
    'WHERE Respondido = 0 ',
    'GROUP BY DtaExeFim ',
    'ORDER BY DtaExeFim ',
    '']);
    DefineDadosInformados(Item);
  end;

///////////////////////// Respondidos
  Qr004Grupos.First;
  while not Qr004Grupos.Eof do
  begin
    Item := Item +1;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qr004Dados, DmodG.MyPID_DB, [
    'SELECT Resposta, CorPizza ',
    'FROM ' + F_RMIP_004Atr,
    'WHERE Respondido = 1 ',
    'AND Resposta="' + Qr004GruposResposta.Value + '" ',
    '']);

    FTitles[Item] := Qr004DadosResposta.Value;
    FCores[Item] := Qr004DadosCorPizza.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr004Itens, DModG.MyPID_DB, [
    'SELECT DtaExeFim, SUM(ITENS) + 0.000 ITENS, ',
    'TO_DAYS(DtaExeFim) DIAS ',
    'FROM ' + F_RMIP_004Atr,
    'WHERE Respondido = 1 ',
    'AND Resposta="' + Qr004GruposResposta.Value + '" ',
    'GROUP BY DtaExeFim ',
    'ORDER BY DtaExeFim ',
    '']);
    DefineDadosInformados(Item);
    //
    Qr004Grupos.Next;
  end;
end;

procedure TFmRMIP_R004.Qr004A_PrgLstItsAfterScroll(DataSet: TDataSet);
const
  Sim = 1;
var
  PrgLstIts, Funcoes: Integer;
begin
  PrgLstIts := Qr004A_PrgLstIts.FieldByName('PrgLstIts').AsInteger;
  Funcoes   := Qr004A_PrgLstIts.FieldByName('Funcoes').AsInteger;
  R004_GeraDados(PrgLstIts, FCliente, FDtaIni, FDtaFim, Funcoes);
  //
  UnDMkDAC_PF.AbreMySQLQuery0(Qr004Grupos, DmodG.MyPID_DB, [
  'SELECT DISTINCT Resposta ',
  'FROM ' + F_RMIP_004Atr,
  'WHERE Respondido=' + Geral.FF0(Sim),
  '']);
end;

function TFmRMIP_R004.R004_GeraDados(PrgLstIts, Cliente: Integer; DtaIni,
  DtaFim: TDateTime; Funcoes: Integer): Boolean;
var
  SQL_Exec, SQL_Extra, SQL_Qtde, SQLPadrao, FldAgrup,
  Resposta, CorPizza, LeftJoin, GroupBy, OrderBy: String;
begin
  Result := False;
  if Cliente <> 0 then
    SQL_Extra := 'AND cab.Entidade=' + Geral.FF0(Cliente)
  else
    SQL_Extra := '';
  //
  SQL_Extra := Geral.ATS([
    SQL_Extra,
    dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True)
  ]);
  if not DmRMIP.SQL_Funcoes(PrgLstIts, Funcoes, Resposta, CorPizza, LeftJoin,
  OrderBy, GroupBy, False) then
    Exit;
  SQL_Exec := Geral.ATS([
  'DELETE FROM ' + F_RMIP_004Atr + '; ',
  'INSERT INTO ' + F_RMIP_004Atr,
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  Resposta,
  CorPizza,
  ', 1 Ativo ',
  LeftJoin,
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  GroupBy,
  '']);
  //if Funcoes = CO_PRG_LST_BINARIO then
    //Geral.MB_Info(SQL_Exec);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL_Exec);

  // Desiste do relaorio se houver apenas uma data!
(*
  ReopenXVals();
  if Qr004XVals.RecordCount < 2 then
    FMemo.Lines.Add(FAplic_Tit + ' cancelado. Motivo: Apenas um item por s�rie!')
  else
  begin
*)
    // OSs afetadas:
    DmRMIP.ReopenOSsAfetadas(Qr004A_OSs, PrgLstIts, LeftJoin, SQL_Extra);
  //end;
end;

procedure TFmRMIP_R004.Reopen004A_PrgLstIts;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr004A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(FAplicacao),
  dmkPF.SQL_Periodo('AND cab.DtaExeFim', FDtaIni, FDtaFim, True, True),
  Geral.ATS_IF(FCliente <> 0, [
    'AND cab.Entidade=' + Geral.FF0(FCliente)]),
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
  //Geral.MB_SQL(Self, Qr004A_PrgLstIts);
end;

procedure TFmRMIP_R004.ReopenXVals();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr004XVals, DModG.MyPID_DB, [
  'SELECT DISTINCT DtaExeFim, ',
  'TO_DAYS(DtaExeFim) DIAS ',
  'FROM ' + F_RMIP_004Atr,
  'ORDER BY DtaExeFim ',
  '']);
end;

end.
