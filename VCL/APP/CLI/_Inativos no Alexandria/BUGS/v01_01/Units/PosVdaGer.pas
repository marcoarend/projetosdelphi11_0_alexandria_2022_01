unit PosVdaGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkCompoStore, UnDmkProcFunc, UnAppListas, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmPosVdaGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtReabre: TBitBtn;
    LaTitulo1C: TLabel;
    QrOSPos: TmySQLQuery;
    DsOSPos: TDataSource;
    DBGrid1: TDBGrid;
    Panel5: TPanel;
    LaCliInt: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrOSPosEmpresa: TIntegerField;
    QrOSPosEntidade: TIntegerField;
    QrOSPosSiapTerCad: TIntegerField;
    QrOSPosDtaExeFim: TDateTimeField;
    QrOSPosEstatus: TIntegerField;
    QrOSPosFatoGeradr: TIntegerField;
    QrOSPosEntiContat: TIntegerField;
    QrOSPosMulServico: TLargeintField;
    QrOSPosDATA_ACAO: TDateTimeField;
    QrOSPosNO_ENT: TWideStringField;
    QrOSPosNO_STC: TWideStringField;
    QrOSPosNO_STATUS: TWideStringField;
    QrOSPosNOME_FATOGERA: TWideStringField;
    QrOSPosNO_CONTAT: TWideStringField;
    QrOSPosNO_SERVICOS: TWideStringField;
    QrOSPosNO_AgeEqiCab: TWideStringField;
    QrOSPosCodigo: TIntegerField;
    QrOSPosControle: TIntegerField;
    QrOSPosDias: TIntegerField;
    QrOSPosAplicacao: TIntegerField;
    QrOSPosAplicID: TIntegerField;
    QrOSPosEmitido: TIntegerField;
    QrOSPosExecutado: TIntegerField;
    QrOSPosExeCodi: TIntegerField;
    QrOSPosExeCtrl: TIntegerField;
    QrOSPosNO_ACAO: TWideStringField;
    QrOSPosNO_AplicID: TWideStringField;
    CSTabSheetChamou: TdmkCompoStore;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Splitter1: TSplitter;
    DBGDiarioAdd: TDBGrid;
    DBMemo1: TDBMemo;
    DsDiarioAdd: TDataSource;
    QrDiarioAdd: TmySQLQuery;
    QrDiarioAddCodigo: TIntegerField;
    QrDiarioAddDepto: TIntegerField;
    QrDiarioAddData: TDateField;
    QrDiarioAddHora: TTimeField;
    QrDiarioAddInterloctr: TIntegerField;
    QrDiarioAddPrintOS: TIntegerField;
    QrDiarioAddNome: TWideStringField;
    QrDiarioAddIMP: TWideStringField;
    QrOSPosEntContrat: TIntegerField;
    BtDiario: TBitBtn;
    BtEncerrar: TBitBtn;
    BtEmail: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrOSPosBeforeClose(DataSet: TDataSet);
    procedure QrOSPosAfterScroll(DataSet: TDataSet);
    procedure BtDiarioClick(Sender: TObject);
    procedure BtEncerrarClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraDiarioAdd2();
    procedure ReopenDiarioAdd(Codigo: Integer);
  public
    { Public declarations }
    procedure FechaPesquisa();
    procedure ReopenOSPos(Controle: Integer);
  end;

  var
  FmPosVdaGer: TFmPosVdaGer;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, MyGlyfs, UMySQLModule, ModOS,
Principal, MyDBCheck, DiarioAdd2, UnDiario_Tabs;

{$R *.DFM}

procedure TFmPosVdaGer.BtDiarioClick(Sender: TObject);
begin
  MostraDiarioAdd2();
end;

procedure TFmPosVdaGer.BtEncerrarClick(Sender: TObject);
var
  Encerrado: String;
  Controle: Integer;
begin
  Encerrado := Geral.FDT(DModG.ObtemAgora(), 109);
  Controle := QrOSPosControle.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ospos', False, [
  'Encerrado'], ['Controle'], [
  Encerrado], [Controle], True) then
  begin
    Controle := UMyMod.ProximoRegistro(QrOSPos, 'Controle', Controle);
    ReopenOSPos(Controle);
  end;
end;

procedure TFmPosVdaGer.BtReabreClick(Sender: TObject);
begin
  ReopenOSPos(0);
end;

procedure TFmPosVdaGer.BtSaidaClick(Sender: TObject);
begin
  if TFmPosVdaGer(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmPosVdaGer(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmPosVdaGer.DBGrid1DblClick(Sender: TObject);
var
  Codigo, Controle, Dias, Aplicacao, AplicID: Integer;
begin
  Codigo    := QrOSPosCodigo.Value;
  Controle  := QrOSPosControle.Value;
  Dias      := QrOSPosDias.Value;
  Aplicacao := QrOSPosAplicacao.Value;
  AplicID   := QrOSPosAplicID.Value;
  //
  case QrOSPosAplicacao.Value of
     CO_OS_POS_APLICACAO_000_NENHUMA:
    begin
      if DmModOS.MostraOSPos(
      stUpd, Codigo, Controle, Dias, Aplicacao, AplicID) then
        ReopenOSPos(Controle);
    end;
    CO_OS_POS_APLICACAO_001_TELEFON:
    begin
      MostraDiarioAdd2();
    end;
    CO_OS_POS_APLICACAO_002_ENVMAIL:
    begin
          //Parei aqui!!!
    end;
  end;
end;

procedure TFmPosVdaGer.EdEmpresaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPosVdaGer.FechaPesquisa;
begin
  QrOSPos.Close;
  BtReabre.Enabled := EdEmpresa.ValueVariant <> 0;
end;

procedure TFmPosVdaGer.FormActivate(Sender: TObject);
begin
  if TFmPosVdaGer(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmPosVdaGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmPosVdaGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPosVdaGer.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  //
(*
  if (DModG.QrEmpresas.State <> dsInactive) and
  (DModG.QrEmpresas.RecordCount = 1) then
  begin
    EdEmpresa.ValueVariant := DModG.QrEmpresasFilial.Value;
    CBEmpresa.KeyValue     := DModG.QrEmpresasFilial.Value;
  end;
*)
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  if EdEmpresa.ValueVariant <> 0 then
    ReopenOSPos(0);
end;

procedure TFmPosVdaGer.MostraDiarioAdd2();
begin
  if DBCheck.CriaFm(TFmDiarioAdd2, FmDiarioAdd2, afmoNegarComAviso) then
  begin
    FmDiarioAdd2.ImgTipo.SQLType := stIns;
    FmDiarioAdd2.FGenero := CO_DIARIO_ADD_GENERO_POS_VDA;
    FmDiarioAdd2.FDepto  := QrOSPosCodigo.Value;
    FmDiarioAdd2.FLink2  := QrOSPosControle.Value;
    FmDiarioAdd2.FQry    := QrDiarioAdd;
    //
    FmDiarioAdd2.EdCliInt.ValueVariant := QrOSPosEmpresa.Value;
    FmDiarioAdd2.CBCliInt.KeyValue := QrOSPosEmpresa.Value;
    //
    FmDiarioAdd2.EdEntidade1.ValueVariant := QrOSPosEntidade.Value;
    FmDiarioAdd2.CBEntidade1.KeyValue := QrOSPosEntidade.Value;
    //
    FmDiarioAdd2.EdTerceiro01.ValueVariant := QrOSPosEntContrat.Value;
    FmDiarioAdd2.CBTerceiro01.KeyValue := QrOSPosEntContrat.Value;
    //
    FmDiarioAdd2.EdInterloctr.ValueVariant := QrOSPosEntiContat.Value;
    FmDiarioAdd2.CBInterloctr.KeyValue := QrOSPosEntiContat.Value;
    //
    UMyMod.SetaCodUsuDeCodigo(FmDiarioAdd2.EdDiarioAss,
      FmDiarioAdd2.CBDiarioAss, FmDiarioAdd2.QrDiarioAss,
      Dmod.QrOpcoesBugsAssPosVda.Value, 'Codigo', 'CodUsu');
    //
    FmDiarioAdd2.EdFormContat.ValueVariant := Dmod.QrOpcoesBugsFrmCPosVda.Value;
    FmDiarioAdd2.CBFormContat.KeyValue := Dmod.QrOpcoesBugsFrmCPosVda.Value;
    //
    FmDiarioAdd2.LaEntidade1.Caption := '[Sub]Cliente:';
    FmDiarioAdd2.LaEntidade2.Caption := 'COntratante:';

    FmDiarioAdd2.ShowModal;
    FmDiarioAdd2.Destroy;
    //
    ReopenDiarioAdd(0);
  end;
end;

procedure TFmPosVdaGer.QrOSPosAfterScroll(DataSet: TDataSet);
begin
  BtEncerrar.Enabled := True;
  BtDiario.Enabled   := False;
  BtEmail.Enabled    := False;
  QrDiarioAdd.Close;
  case QrOSPosAplicacao.Value of
    CO_OS_POS_APLICACAO_000_NENHUMA: ; //
    CO_OS_POS_APLICACAO_001_TELEFON:
    begin
      BtDiario.Enabled := True;
      ReopenDiarioAdd(0);
      //
      if PageControl1.ActivePageIndex <> 0 then
        PageControl1.ActivePageIndex := 0;
    end;
    CO_OS_POS_APLICACAO_002_ENVMAIL: ; //
  end;
end;

procedure TFmPosVdaGer.QrOSPosBeforeClose(DataSet: TDataSet);
begin
  QrDiarioAdd.Close;
  BtEncerrar.Enabled := False;
  BtDiario.Enabled   := False;
  BtEmail.Enabled    := False;
end;

procedure TFmPosVdaGer.ReopenDiarioAdd(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDiarioAdd, Dmod.MyDB, [
  'SELECT Codigo, Depto, Data, Hora,  ',
  'Interloctr, PrintOS, ',
  'ELT(PrintOS + 2, " ", "N", "S", "?") IMP, Nome',
  'FROM diarioadd ',
  'WHERE Depto=' + Geral.FF0(QrOSPosCodigo.Value),
  'ORDER BY Codigo DESC ',
  '']);
  //
  if Codigo <> 0 then
    QrDiarioAdd.Locate('Codigo', Codigo, []);
end;

procedure TFmPosVdaGer.ReopenOSPos(Controle: Integer);
var
  Acao: String;
  Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  Acao := dmkPF.ArrayToTexto('osp.Aplicacao', 'NO_ACAO', pvPos, True,
    sListaAcaoPosVda);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPos, Dmod.MyDB, [
  'SELECT cab.Empresa, cab.Entidade, cab.SiapTerCad,  ',
  'cab.DtaExeFim, cab.Estatus, cab.FatoGeradr,  ',
  'cab.EntiContat, cab.EntContrat, cab.MulServico, ',
  'DATE_ADD(cab.DtaExeFim, INTERVAL osp.Dias DAY) DATA_ACAO,  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,  ',
  'stc.Nome NO_STC, sos.Nome NO_STATUS,  ',
  'fge.Nome NOME_FATOGERA, ect.Nome NO_CONTAT,  ',
  'mls.Nome NO_SERVICOS, aec.Nome NO_AgeEqiCab, ',
  'osp.Codigo, osp.Controle, osp.Dias, osp.Aplicacao,  ',
  'osp.AplicID, osp.Emitido, osp.Executado,  ',
  'osp.ExeCodi, osp.ExeCtrl, ',
  Acao,
  'IF(osp.Aplicacao=2, pre.Nome, "") NO_AplicID ',
  'FROM ospos osp ',
  'LEFT JOIN oscab cab ON cab.Codigo=osp.Codigo ',
  'LEFT JOIN entidades ent ON ent.Codigo=cab.entidade ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapterCad ',
  'LEFT JOIN estatusoss sos ON sos.Codigo=cab.Estatus ',
  'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
  'LEFT JOIN enticontat ect ON ect.Controle=cab.Enticontat ',
  'LEFT JOIN mulservico mls ON mls.Codigo=cab.MulServico ',
  'LEFT JOIN ageeqicab aec ON aec.Codigo=cab.AgeEqiCab ',
  'LEFT JOIN preemail pre ON pre.Codigo=osp.AplicID ',
  //'WHERE Executado=0 ',
  'WHERE osp.Encerrado < "1900-01-01" ',
  'AND cab.DtaExeFim > "1900-01-01" ',
  'AND DATE_ADD(cab.DtaExeFim, INTERVAL osp.Dias DAY) < NOW() ',
  'AND cab.Empresa=' + Geral.FF0(Empresa),
  'ORDER BY DATA_ACAO ',
  '']);
  //
  QrOSPos.Locate('Controle', Controle, []);
end;

end.
