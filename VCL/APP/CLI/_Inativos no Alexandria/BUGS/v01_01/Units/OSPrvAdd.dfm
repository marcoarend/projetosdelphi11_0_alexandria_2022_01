object FmOSPrvAdd: TFmOSPrvAdd
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-025 :: Solicita'#231#227'o de Provid'#234'ncia'
  ClientHeight = 526
  ClientWidth = 616
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 616
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 568
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 520
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 318
        Height = 32
        Caption = 'Solicita'#231#227'o de Provid'#234'ncia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 318
        Height = 32
        Caption = 'Solicita'#231#227'o de Provid'#234'ncia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 318
        Height = 32
        Caption = 'Solicita'#231#227'o de Provid'#234'ncia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 616
    Height = 364
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 616
      Height = 364
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 616
        Height = 364
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 612
          Height = 62
          Align = alTop
          TabOrder = 0
          object Label52: TLabel
            Left = 16
            Top = 16
            Width = 26
            Height = 13
            Caption = 'Data:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label3: TLabel
            Left = 124
            Top = 16
            Width = 26
            Height = 13
            Caption = 'Hora:'
            Color = clBtnFace
            ParentColor = False
          end
          object SbFormContat: TSpeedButton
            Left = 484
            Top = 32
            Width = 21
            Height = 21
            Caption = '...'
            Visible = False
            OnClick = SbFormContatClick
          end
          object Label1: TLabel
            Left = 188
            Top = 16
            Width = 86
            Height = 13
            Caption = 'Forma de contato:'
            Color = clBtnFace
            ParentColor = False
          end
          object TPDtHrContat: TdmkEditDateTimePicker
            Left = 16
            Top = 32
            Width = 108
            Height = 21
            Date = 0.639644131944805900
            Time = 0.639644131944805900
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtHrContat'
            UpdCampo = 'DtHrContat'
            UpdType = utYes
          end
          object EdDtHrContat: TdmkEdit
            Left = 124
            Top = 32
            Width = 60
            Height = 21
            TabOrder = 1
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            QryCampo = 'DtHrContat'
            UpdCampo = 'DtHrContat'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdFormContat: TdmkEditCB
            Left = 188
            Top = 32
            Width = 32
            Height = 22
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FormContat'
            UpdCampo = 'FormContat'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBFormContat
            IgnoraDBLookupComboBox = False
          end
          object CBFormContat: TdmkDBLookupComboBox
            Left = 220
            Top = 32
            Width = 261
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsFormContat
            TabOrder = 3
            dmkEditCB = EdFormContat
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 77
          Width = 612
          Height = 148
          Align = alTop
          TabOrder = 1
          object Label4: TLabel
            Left = 12
            Top = 12
            Width = 32
            Height = 13
            Caption = 'Cliente'
          end
          object Label5: TLabel
            Left = 12
            Top = 56
            Width = 40
            Height = 13
            Caption = 'Contato:'
          end
          object Label6: TLabel
            Left = 12
            Top = 100
            Width = 37
            Height = 13
            Caption = 'Agente:'
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 68
            Top = 28
            Width = 537
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NO_ENT'
            ListSource = DsEntidades
            TabOrder = 1
            dmkEditCB = EdCliente
            QryCampo = 'Cliente'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCliente: TdmkEditCB
            Left = 12
            Top = 28
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdClienteChange
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
          end
          object EdContato: TdmkEditCB
            Left = 12
            Top = 72
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Contato'
            UpdCampo = 'Contato'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBContato
            IgnoraDBLookupComboBox = False
          end
          object CBContato: TdmkDBLookupComboBox
            Left = 68
            Top = 72
            Width = 537
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsEntiContat
            TabOrder = 3
            dmkEditCB = EdContato
            QryCampo = 'Contato'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdAgente: TdmkEditCB
            Left = 12
            Top = 116
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Agente'
            UpdCampo = 'Agente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBAgente
            IgnoraDBLookupComboBox = False
          end
          object CBAgente: TdmkDBLookupComboBox
            Left = 68
            Top = 116
            Width = 537
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsAgentes
            TabOrder = 5
            dmkEditCB = EdAgente
            QryCampo = 'Agente'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 225
          Width = 612
          Height = 137
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Label2: TLabel
            Left = 12
            Top = 4
            Width = 184
            Height = 13
            Caption = 'Observa'#231#245'es (m'#225'ximo 255 caracteres):'
          end
          object Label7: TLabel
            Left = 12
            Top = 92
            Width = 106
            Height = 13
            Caption = 'Status da provid'#234'ncia:'
            Color = clBtnFace
            ParentColor = False
          end
          object SbPrvStatus: TSpeedButton
            Left = 452
            Top = 108
            Width = 21
            Height = 21
            Caption = '...'
            Visible = False
            OnClick = SbPrvStatusClick
          end
          object MeNome: TdmkMemo
            Left = 12
            Top = 24
            Width = 589
            Height = 61
            MaxLength = 255
            TabOrder = 0
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
          end
          object CkContinuar: TCheckBox
            Left = 488
            Top = 112
            Width = 113
            Height = 17
            Caption = 'Continuar inserindo.'
            TabOrder = 3
          end
          object EdPrvStatus: TdmkEditCB
            Left = 12
            Top = 108
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PrvStatus'
            UpdCampo = 'PrvStatus'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBPrvStatus
            IgnoraDBLookupComboBox = False
          end
          object CBPrvStatus: TdmkDBLookupComboBox
            Left = 68
            Top = 108
            Width = 381
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsOSPrvStatus
            TabOrder = 2
            dmkEditCB = EdPrvStatus
            QryCampo = 'PrvStatus'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 412
    Width = 616
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 612
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 456
    Width = 616
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 470
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 468
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtContatos: TBitBtn
        Tag = 243
        Left = 236
        Top = 4
        Width = 153
        Height = 40
        Caption = '&Abrir todos contatos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtContatosClick
      end
    end
  end
  object QrFormContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM formcontat'
      'ORDER BY Nome')
    Left = 4
    Top = 12
    object QrFormContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFormContat: TDataSource
    DataSet = QrFormContat
    Left = 32
    Top = 12
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 56
    Top = 12
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 84
    Top = 12
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT eco.Controle, eco.Nome'
      'FROM enticontat eco'
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle'
      'WHERE eco.Codigo=100'
      'ORDER BY Nome')
    Left = 112
    Top = 12
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 140
    Top = 12
  end
  object QrAgentes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidades'
      'WHERE Nome <> '#39#39' '
      'AND Fornece2="V"'
      'ORDER BY Nome')
    Left = 384
    Top = 12
    object QrAgentesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgentesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsAgentes: TDataSource
    DataSet = QrAgentes
    Left = 412
    Top = 12
  end
  object QrOSPrvStatus: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM osprvsta'
      'ORDER BY Nome')
    Left = 468
    Top = 12
    object QrOSPrvStatusCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPrvStatusNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOSPrvStatus: TDataSource
    DataSet = QrOSPrvStatus
    Left = 496
    Top = 12
  end
end
