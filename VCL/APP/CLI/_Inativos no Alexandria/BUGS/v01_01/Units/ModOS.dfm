object DmModOS: TDmModOS
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 793
  Width = 945
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntidadesBeforeClose
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 268
    Top = 4
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 336
    Top = 4
  end
  object QrSiapTerCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM siaptercad'
      'WHERE Cliente=100')
    Left = 268
    Top = 52
    object QrSiapTerCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapTerCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsSiapterCad: TDataSource
    DataSet = QrSiapTerCad
    Left = 336
    Top = 52
  end
  object QrFatoGeradr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, NeedOS_Ori'
      'FROM fatogeradr'
      'ORDER BY Nome')
    Left = 268
    Top = 148
    object QrFatoGeradrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFatoGeradrNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFatoGeradrNeedOS_Ori: TSmallintField
      FieldName = 'NeedOS_Ori'
    end
  end
  object DsFatoGeradr: TDataSource
    DataSet = QrFatoGeradr
    Left = 336
    Top = 148
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 268
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 336
    Top = 196
  end
  object QrOSSrv: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSSrvBeforeClose
    AfterScroll = QrOSSrvAfterScroll
    OnCalcFields = QrOSSrvCalcFields
    SQL.Strings = (
      'SELECT des.Nome NO_DesServico, '
      'des.Sigla NO_SIGLA, srv.* '
      'FROM ossrv srv '
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico '
      'WHERE srv.Codigo=:P0')
    Left = 40
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSSrvNO_DesServico: TWideStringField
      FieldName = 'NO_DesServico'
      Size = 60
    end
    object QrOSSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSSrvDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrOSSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSSrvGarantiaDd: TIntegerField
      FieldName = 'GarantiaDd'
      DisplayFormat = '00'
    end
    object QrOSSrvHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
      DisplayFormat = '00'
    end
    object QrOSSrvHrExecutar: TFloatField
      FieldName = 'HrExecutar'
    end
    object QrOSSrvValCalc: TFloatField
      FieldName = 'ValCalc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvValInfo: TFloatField
      FieldName = 'ValInfo'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvValDesc: TFloatField
      FieldName = 'ValDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvValTota: TFloatField
      FieldName = 'ValTota'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvAutorizado: TSmallintField
      FieldName = 'Autorizado'
      MaxValue = 1
    end
    object QrOSSrvVAL_CALCeINFO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_CALCeINFO'
      Calculated = True
    end
    object QrOSSrvAUTORIZADO_BOOL: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'AUTORIZADO_BOOL'
      Calculated = True
    end
    object QrOSSrvNO_SIGLA: TWideStringField
      FieldName = 'NO_SIGLA'
      Size = 10
    end
    object QrOSSrvDetalhes: TWideMemoField
      FieldName = 'Detalhes'
      BlobType = ftWideMemo
    end
  end
  object DsOSSrv: TDataSource
    DataSet = QrOSSrv
    Left = 109
    Top = 52
  end
  object QrOSFrmCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSFrmCabBeforeClose
    AfterScroll = QrOSFrmCabAfterScroll
    SQL.Strings = (
      'SELECT ofc.*, frm.Nome NO_FORMULA, gg1.Nome NO_EquipAplic,'
      'med.CodUsu CU_UNIDMED,'
      'med.SIGLA, med.Nome NO_UNIDMED'
      'FROM osfrmcab ofc'
      'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ofc.EquipAplic'
      'LEFT JOIN gragru1 gg1 ON ggx.Gragru1=gg1.Nivel1'
      'LEFT JOIN unidmed med ON med.Codigo=ofc.UnidMed'
      'WHERE ofc.Controle=:P0'
      ''
      '')
    Left = 40
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmCabConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOSFrmCabFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrOSFrmCabEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrOSFrmCabQtdTot: TFloatField
      FieldName = 'QtdTot'
    end
    object QrOSFrmCabQtdQSP: TFloatField
      FieldName = 'QtdQSP'
    end
    object QrOSFrmCabCusTot: TFloatField
      FieldName = 'CusTot'
    end
    object QrOSFrmCabNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrOSFrmCabNO_EquipAplic: TWideStringField
      FieldName = 'NO_EquipAplic'
      Size = 60
    end
    object QrOSFrmCabDiluente: TSmallintField
      FieldName = 'Diluente'
    end
    object QrOSFrmCabTipoAplica: TIntegerField
      FieldName = 'TipoAplica'
    end
    object QrOSFrmCabUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrOSFrmCabCU_UNIDMED: TIntegerField
      FieldName = 'CU_UNIDMED'
      Required = True
    end
    object QrOSFrmCabSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrOSFrmCabNO_UNIDMED: TWideStringField
      FieldName = 'NO_UNIDMED'
      Size = 30
    end
  end
  object DsOSFrmCab: TDataSource
    DataSet = QrOSFrmCab
    Left = 108
    Top = 148
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome '
      'FROM EntiContat'
      'WHERE Codigo=:P0'
      'OR Codigo=:P1'
      'ORDER BY Nome')
    Left = 268
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 336
    Top = 244
  end
  object QrEntContrat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 268
    Top = 292
    object QrEntContratCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntContratNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntContrat: TDataSource
    DataSet = QrEntContrat
    Left = 336
    Top = 292
  end
  object QrEntPagante: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 268
    Top = 340
    object QrEntPaganteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntPaganteNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntPagante: TDataSource
    DataSet = QrEntPagante
    Left = 336
    Top = 340
  end
  object QrEstatusOSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM estatusoss'
      'ORDER BY Codigo')
    Left = 268
    Top = 100
    object QrEstatusOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstatusOSsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEstatusOSs: TDataSource
    DataSet = QrEstatusOSs
    Left = 336
    Top = 100
  end
  object QrOSPrz: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT opz.Codigo, opz.Controle, opz.Condicao, '
      'opz.DescoPer, opz.Escolhido, ppc.Nome NO_CONDICAO'
      'FROM osprz opz'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=opz.Condicao'
      'WHERE opz.Codigo=:P0')
    Left = 40
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSPrzCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osprz.Codigo'
    end
    object QrOSPrzControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osprz.Controle'
    end
    object QrOSPrzCondicao: TIntegerField
      FieldName = 'Condicao'
      Origin = 'osprz.Condicao'
    end
    object QrOSPrzNO_CONDICAO: TWideStringField
      FieldName = 'NO_CONDICAO'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrOSPrzDescoPer: TFloatField
      FieldName = 'DescoPer'
      Origin = 'osprz.DescoPer'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrOSPrzEscolhido: TSmallintField
      FieldName = 'Escolhido'
      Origin = 'osprz.Escolhido'
      MaxValue = 1
    end
  end
  object DsOSPrz: TDataSource
    DataSet = QrOSPrz
    Left = 108
    Top = 436
  end
  object QrOSCabAlv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT oca.*, IF(oca.Praga_Z <> 0, '#39'E'#39', '#39'G'#39') NO_NIVEL, '
      'IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) NO_PRAGA'
      'FROM oscabalv oca'
      'LEFT JOIN praga_a pca ON pca.Codigo=oca.Praga_A'
      'LEFT JOIN praga_z pcz ON pcz.Codigo=oca.Praga_Z'
      'WHERE oca.Codigo=:P0'
      'ORDER BY oca.Ordem')
    Left = 40
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSCabAlvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabAlvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSCabAlvOrdem: TSmallintField
      FieldName = 'Ordem'
    end
    object QrOSCabAlvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCabAlvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCabAlvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCabAlvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCabAlvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCabAlvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCabAlvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCabAlvPraga_A: TIntegerField
      FieldName = 'Praga_A'
    end
    object QrOSCabAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
    end
    object QrOSCabAlvNO_NIVEL: TWideStringField
      FieldName = 'NO_NIVEL'
      Required = True
      Size = 1
    end
    object QrOSCabAlvNO_PRAGA: TWideStringField
      FieldName = 'NO_PRAGA'
      Size = 60
    end
  end
  object DsOSCabAlv: TDataSource
    DataSet = QrOSCabAlv
    Left = 109
    Top = 4
  end
  object QrOSAlv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prc.Nome NO_Praga_Z, osa.*'
      'FROM osalv osa'
      'LEFT JOIN praga_z prc ON prc.Codigo=osa.Praga_Z'
      'WHERE osa.Controle=:P0'
      '')
    Left = 40
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSAlvCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osalv.Codigo'
    end
    object QrOSAlvControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osalv.Controle'
    end
    object QrOSAlvConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'osalv.Conta'
    end
    object QrOSAlvNO_Praga_Z: TWideStringField
      FieldName = 'NO_Praga_Z'
      Origin = 'praga_z.Nome'
      Size = 60
    end
    object QrOSAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
      Origin = 'osalv.Praga_Z'
    end
  end
  object DsOSAlv: TDataSource
    DataSet = QrOSAlv
    Left = 108
    Top = 100
  end
  object QrOSFrmRec: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NO_GG1, ofr.*'
      'FROM osfrmrec ofr'
      'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX'
      'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1'
      'WHERE ofr.Conta=:P0'
      'ORDER BY Ordem')
    Left = 40
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmRecNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrOSFrmRecCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmRecControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmRecConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmRecIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSFrmRecGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOSFrmRecPrvQtd: TFloatField
      FieldName = 'PrvQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSFrmRecPrvPrc: TFloatField
      FieldName = 'PrvPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSFrmRecPrvVal: TFloatField
      FieldName = 'PrvVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSFrmRecUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSFrmRecUsoPrc: TFloatField
      FieldName = 'UsoPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSFrmRecUsoVal: TFloatField
      FieldName = 'UsoVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSFrmRecUsoDec: TFloatField
      FieldName = 'UsoDec'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOSFrmRecUsoTot: TFloatField
      FieldName = 'UsoTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOSFrmRecOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSFrmRecReordem: TIntegerField
      FieldName = 'Reordem'
    end
  end
  object DsOSFrmRec: TDataSource
    DataSet = QrOSFrmRec
    Left = 108
    Top = 196
  end
  object QrOSMonCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSMonCabBeforeClose
    AfterScroll = QrOSMonCabAfterScroll
    SQL.Strings = (
      
        'SELECT plc.Nome NO_prglstcab, dep.Nome NO_DEPENDENCI, ofc.*, mon' +
        '.Nome NO_FORMULA,  '
      'gg1.Nome NO_EquipAplic, pip.Nome NO_PIP, '
      'med.CodUsu CU_UNIDMED, '
      'med.SIGLA, med.Nome NO_UNIDMED '
      'FROM osmoncab ofc '
      'LEFT JOIN formulas mon ON mon.Codigo=ofc.Formula '
      'LEFT JOIN gragrux ggx ON ggx.Controle=ofc.EquipAplic '
      'LEFT JOIN gragru1 gg1 ON ggx.Gragru1=gg1.Nivel1 '
      'LEFT JOIN pipcad  pip ON pip.Codigo=ofc.PipCad '
      'LEFT JOIN unidmed med ON med.Codigo=ofc.UnidMed '
      'LEFT JOIN prglstcab plc ON plc.Codigo=pip.PrgLstCab '
      'LEFT JOIN siapimadep sid ON sid.Controle=pip.Dependenci '
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci '
      'WHERE ofc.Controle=:P0')
    Left = 40
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSMonCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osmoncab.Codigo'
    end
    object QrOSMonCabControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osmoncab.Controle'
    end
    object QrOSMonCabConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'osmoncab.Conta'
    end
    object QrOSMonCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'osmoncab.Nome'
      Size = 60
    end
    object QrOSMonCabFormula: TIntegerField
      FieldName = 'Formula'
      Origin = 'osmoncab.Formula'
    end
    object QrOSMonCabEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
      Origin = 'osmoncab.EquipAplic'
    end
    object QrOSMonCabQtdTot: TFloatField
      FieldName = 'QtdTot'
      Origin = 'osmoncab.QtdTot'
    end
    object QrOSMonCabQtdQSP: TFloatField
      FieldName = 'QtdQSP'
      Origin = 'osmoncab.QtdQSP'
    end
    object QrOSMonCabCusTot: TFloatField
      FieldName = 'CusTot'
      Origin = 'osmoncab.CusTot'
    end
    object QrOSMonCabNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Origin = 'formulas.Nome'
      Size = 60
    end
    object QrOSMonCabNO_EquipAplic: TWideStringField
      FieldName = 'NO_EquipAplic'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrOSMonCabPipCad: TIntegerField
      FieldName = 'PipCad'
      Origin = 'osmoncab.PipCad'
    end
    object QrOSMonCabNO_PIP: TWideStringField
      FieldName = 'NO_PIP'
      Origin = 'pipcad.Nome'
      Size = 30
    end
    object QrOSMonCabDiluente: TIntegerField
      FieldName = 'Diluente'
    end
    object QrOSMonCabTipoAplica: TIntegerField
      FieldName = 'TipoAplica'
    end
    object QrOSMonCabUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrOSMonCabCU_UNIDMED: TIntegerField
      FieldName = 'CU_UNIDMED'
      Required = True
    end
    object QrOSMonCabSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrOSMonCabNO_UNIDMED: TWideStringField
      FieldName = 'NO_UNIDMED'
      Size = 30
    end
    object QrOSMonCabNO_prglstcab: TWideStringField
      FieldName = 'NO_prglstcab'
      Size = 100
    end
    object QrOSMonCabNO_DEPENDENCI: TWideStringField
      FieldName = 'NO_DEPENDENCI'
      Size = 60
    end
    object QrOSMonCabPerioDd: TIntegerField
      FieldName = 'PerioDd'
    end
    object QrOSMonCabDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
  end
  object QrOSMonRec: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NO_GG1, ofr.*'
      'FROM osmonrec ofr'
      'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX'
      'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1'
      'WHERE ofr.Conta=:P0'
      'ORDER BY Ordem')
    Left = 40
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSMonRecNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrOSMonRecCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSMonRecControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSMonRecConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSMonRecIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSMonRecGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOSMonRecPrvQtd: TFloatField
      FieldName = 'PrvQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSMonRecPrvPrc: TFloatField
      FieldName = 'PrvPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSMonRecPrvVal: TFloatField
      FieldName = 'PrvVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSMonRecUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSMonRecUsoPrc: TFloatField
      FieldName = 'UsoPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSMonRecUsoVal: TFloatField
      FieldName = 'UsoVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSMonRecUsoDec: TFloatField
      FieldName = 'UsoDec'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOSMonRecUsoTot: TFloatField
      FieldName = 'UsoTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOSMonRecOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSMonRecReordem: TIntegerField
      FieldName = 'Reordem'
    end
  end
  object DsOSMonCab: TDataSource
    DataSet = QrOSMonCab
    Left = 108
    Top = 292
  end
  object DsOSMonRec: TDataSource
    DataSet = QrOSMonRec
    Left = 108
    Top = 340
  end
  object QrTotalOS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ValCalc) ValCalc, SUM(ValInfo) ValInfo,'
      'SUM(ValDesc) ValDesc, SUM(ValTota) ValTota'
      'FROM  ossrv'
      'WHERE Codigo=:P0')
    Left = 188
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotalOSValCalc: TFloatField
      FieldName = 'ValCalc'
    end
    object QrTotalOSValInfo: TFloatField
      FieldName = 'ValInfo'
    end
    object QrTotalOSValDesc: TFloatField
      FieldName = 'ValDesc'
    end
    object QrTotalOSValTota: TFloatField
      FieldName = 'ValTota'
    end
  end
  object TbDesServico: TmySQLTable
    Database = Dmod.MyDB
    TableName = 'desservico'
    Left = 812
    object TbDesServicoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'desservico.Codigo'
    end
    object TbDesServicoNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'desservico.Nome'
      Size = 60
    end
    object TbDesServicoSigla: TWideStringField
      FieldName = 'Sigla'
      Origin = 'desservico.Sigla'
      Size = 10
    end
  end
  object DsDesServico: TDataSource
    DataSet = TbDesServico
    Left = 880
  end
  object QrSTC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT llo.Nome NOMELOGRAD, stc.* '
      'FROM siaptercad stc'
      'LEFT JOIN listalograd llo ON llo.Codigo=stc.SLograd'
      'WHERE stc.Cliente=:P0')
    Left = 188
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSTCNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrSTCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSTCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSTCCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSTCSLograd: TSmallintField
      FieldName = 'SLograd'
    end
    object QrSTCSRua: TWideStringField
      FieldName = 'SRua'
      Size = 60
    end
    object QrSTCSNumero: TIntegerField
      FieldName = 'SNumero'
    end
    object QrSTCSCompl: TWideStringField
      FieldName = 'SCompl'
      Size = 60
    end
    object QrSTCSBairro: TWideStringField
      FieldName = 'SBairro'
      Size = 60
    end
    object QrSTCSCidade: TWideStringField
      FieldName = 'SCidade'
      Size = 60
    end
    object QrSTCSUF: TWideStringField
      FieldName = 'SUF'
      Size = 3
    end
    object QrSTCSCEP: TIntegerField
      FieldName = 'SCEP'
    end
    object QrSTCSPais: TWideStringField
      FieldName = 'SPais'
      Size = 60
    end
    object QrSTCSEndeRef: TWideStringField
      FieldName = 'SEndeRef'
      Size = 100
    end
    object QrSTCSCodMunici: TIntegerField
      FieldName = 'SCodMunici'
    end
    object QrSTCSCodiPais: TIntegerField
      FieldName = 'SCodiPais'
    end
    object QrSTCSTe1: TWideStringField
      FieldName = 'STe1'
    end
    object QrSTCM2Constru: TFloatField
      FieldName = 'M2Constru'
    end
    object QrSTCM2NaoBuild: TFloatField
      FieldName = 'M2NaoBuild'
    end
    object QrSTCM2Terreno: TFloatField
      FieldName = 'M2Terreno'
    end
    object QrSTCM2Total: TFloatField
      FieldName = 'M2Total'
    end
  end
  object QrLctFatRef: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctFatRefCalcFields
    SQL.Strings = (
      'SELECT lfr.* '
      'FROM lctfatref lfr '
      'WHERE lfr.Controle=0')
    Left = 268
    Top = 388
    object QrLctFatRefPARCELA: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'PARCELA'
      Calculated = True
    end
    object QrLctFatRefCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLctFatRefControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctFatRefConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrLctFatRefLancto: TLargeintField
      FieldName = 'Lancto'
    end
    object QrLctFatRefValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctFatRefVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsLctFatRef: TDataSource
    DataSet = QrLctFatRef
    Left = 336
    Top = 388
  end
  object QrOSFrmAbr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT abr.Nome NO_ABRANGE, ofa.*'
      'FROM osfrmabr ofa'
      'LEFT JOIN abrangicie abr ON abr.Codigo=ofa.Abrangicie'
      'WHERE ofa.Conta=:P0')
    Left = 40
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmAbrNO_ABRANGE: TWideStringField
      FieldName = 'NO_ABRANGE'
      Size = 60
    end
    object QrOSFrmAbrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmAbrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmAbrConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmAbrIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSFrmAbrAbrangicie: TIntegerField
      FieldName = 'Abrangicie'
    end
  end
  object DsOSFrmAbr: TDataSource
    DataSet = QrOSFrmAbr
    Left = 108
    Top = 484
  end
  object QrEntiMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto, ema.Ordem'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      'ORDER BY ema.Ordem, ema.Conta'#10)
    Left = 268
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMailNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEntiMailConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrEntiMailEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
    object QrEntiMailOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsEntiMail: TDataSource
    DataSet = QrEntiMail
    Left = 336
    Top = 436
  end
  object QrEntiTel: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiTelCalcFields
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 268
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTelNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrEntiTelConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrEntiTelTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrEntiTelEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrEntiTelTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiTelRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object DsEntiTel: TDataSource
    DataSet = QrEntiTel
    Left = 336
    Top = 484
  end
  object QrOSPos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM ospos'
      'WHERE Codigo=0'
      '')
    Left = 40
    Top = 532
    object QrOSPosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSPosDias: TIntegerField
      FieldName = 'Dias'
    end
  end
  object DsOSPos: TDataSource
    DataSet = QrOSPos
    Left = 108
    Top = 532
  end
  object QrOSFrmDep: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT osd.*, ELT(osd.Tabela, "I", "M", "?") SIGLA,'
      'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") NO_Campo'
      'FROM osfrmdep osd'
      'LEFT JOIN siapimadep sid ON sid.Controle=osd.Cadastro'
      'LEFT JOIN dependenci dpd ON dpd.Codigo=sid.Dependenci'
      'LEFT JOIN movamovcad mac ON mac.Codigo=osd.Cadastro'
      'WHERE osd.Conta=:P0')
    Left = 40
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmDepCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmDepControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmDepConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmDepTabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrOSFrmDepCadastro: TIntegerField
      FieldName = 'Cadastro'
    end
    object QrOSFrmDepLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSFrmDepDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSFrmDepDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSFrmDepUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSFrmDepUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSFrmDepAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSFrmDepAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSFrmDepSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 1
    end
    object QrOSFrmDepNO_Campo: TWideStringField
      FieldName = 'NO_Campo'
      Size = 100
    end
    object QrOSFrmDepIDIts: TIntegerField
      FieldName = 'IDIts'
    end
  end
  object DsOSFrmDep: TDataSource
    DataSet = QrOSFrmDep
    Left = 108
    Top = 244
  end
  object QrOSCxa: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSCxaBeforeClose
    AfterScroll = QrOSCxaAfterScroll
    SQL.Strings = (
      'SELECT cxa.*, cxm.Nome NO_MATERIAL,'
      'cxf.Nome NOME_FORMA,'
      'six.MatersCxa, six.FormasCxa, six.VolumeL,'
      'six.Local, six.Acesso, six.Medidas'
      'FROM oscxa cxa'
      'LEFT JOIN siapimacxa six ON six.Controle=cxa.Caixa'
      'LEFT JOIN cxamaters cxm ON'
      '  cxm.Codigo=six.MatersCxa'
      'LEFT JOIN cxaformas cxf ON'
      '  cxf.Codigo=six.FormasCxa'
      'WHERE cxa.Codigo=:P0')
    Left = 420
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSCxaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCxaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSCxaCaixa: TIntegerField
      FieldName = 'Caixa'
    end
    object QrOSCxaGarantiaDd: TIntegerField
      FieldName = 'GarantiaDd'
    end
    object QrOSCxaHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
    end
    object QrOSCxaHrExecutar: TFloatField
      FieldName = 'HrExecutar'
    end
    object QrOSCxaValCalc: TFloatField
      FieldName = 'ValCalc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCxaValInfo: TFloatField
      FieldName = 'ValInfo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCxaValDesc: TFloatField
      FieldName = 'ValDesc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCxaValTota: TFloatField
      FieldName = 'ValTota'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCxaAutorizado: TSmallintField
      FieldName = 'Autorizado'
    end
    object QrOSCxaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCxaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCxaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCxaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCxaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCxaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCxaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCxaNO_MATERIAL: TWideStringField
      FieldName = 'NO_MATERIAL'
      Size = 60
    end
    object QrOSCxaNOME_FORMA: TWideStringField
      FieldName = 'NOME_FORMA'
      Size = 60
    end
    object QrOSCxaMatersCxa: TIntegerField
      FieldName = 'MatersCxa'
    end
    object QrOSCxaFormasCxa: TIntegerField
      FieldName = 'FormasCxa'
    end
    object QrOSCxaVolumeL: TFloatField
      FieldName = 'VolumeL'
    end
    object QrOSCxaLocal: TWideStringField
      FieldName = 'Local'
      Size = 255
    end
    object QrOSCxaAcesso: TWideStringField
      FieldName = 'Acesso'
      Size = 255
    end
    object QrOSCxaMedidas: TWideStringField
      FieldName = 'Medidas'
      Size = 255
    end
    object QrOSCxaChekLstCab: TIntegerField
      FieldName = 'ChekLstCab'
    end
    object QrOSCxaDetalhes: TWideMemoField
      FieldName = 'Detalhes'
      BlobType = ftWideMemo
    end
  end
  object DsOSCxa: TDataSource
    DataSet = QrOSCxa
    Left = 488
    Top = 52
  end
  object QrOSCxaAtrib: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts,'
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,'
      'cad.Nome NO_CAD, its.Nome NO_ITS'
      'FROM atrsicxdef def'
      'LEFT JOIN atrsicxits its ON its.Controle=def.AtrIts'
      'LEFT JOIN atrsicxcad cad ON cad.Codigo=def.AtrCad'
      'WHERE def.ID_Sorc=:P0'
      'ORDER BY NO_CAD, NO_ITS')
    Left = 420
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSCxaAtribID_Item: TIntegerField
      FieldName = 'ID_Item'
    end
    object QrOSCxaAtribID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
    end
    object QrOSCxaAtribAtrCad: TIntegerField
      FieldName = 'AtrCad'
    end
    object QrOSCxaAtribAtrIts: TIntegerField
      FieldName = 'AtrIts'
    end
    object QrOSCxaAtribCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
      Required = True
    end
    object QrOSCxaAtribCU_ITS: TIntegerField
      FieldName = 'CU_ITS'
      Required = True
    end
    object QrOSCxaAtribNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrOSCxaAtribNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 50
    end
  end
  object DsOSCxaAtrib: TDataSource
    DataSet = QrOSCxaAtrib
    Left = 488
    Top = 100
  end
  object QrFormulIF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ofr.*'
      'FROM formulif ofr'
      'WHERE ofr.Codigo=:P0'
      'ORDER BY Ordem')
    Left = 572
    Top = 1
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFormulIFCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulIFControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFormulIFGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFormulIFPrvQtd: TFloatField
      FieldName = 'PrvQtd'
    end
    object QrFormulIFPrvPrc: TFloatField
      FieldName = 'PrvPrc'
    end
    object QrFormulIFPrvVal: TFloatField
      FieldName = 'PrvVal'
    end
    object QrFormulIFUsoQtd: TFloatField
      FieldName = 'UsoQtd'
    end
    object QrFormulIFUsoPrc: TFloatField
      FieldName = 'UsoPrc'
    end
    object QrFormulIFUsoVal: TFloatField
      FieldName = 'UsoVal'
    end
    object QrFormulIFUsoDec: TFloatField
      FieldName = 'UsoDec'
    end
    object QrFormulIFUsoTot: TFloatField
      FieldName = 'UsoTot'
    end
    object QrFormulIFOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFormulIFReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrFormulIFEhDiluente: TSmallintField
      FieldName = 'EhDiluente'
    end
  end
  object DsFormulIF: TDataSource
    DataSet = QrFormulIF
    Left = 640
    Top = 1
  end
  object QrFormulIA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ofa.*'
      'FROM formulia ofa'
      'WHERE ofa.Codigo=:P0')
    Left = 572
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFormulIACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulIAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFormulIAAbrangicie: TIntegerField
      FieldName = 'Abrangicie'
    end
  end
  object DsFormulIA: TDataSource
    DataSet = QrFormulIA
    Left = 640
    Top = 52
  end
  object DsExeTxtCli1: TDataSource
    DataSet = QrExeTxtCli1
    Left = 488
    Top = 148
  end
  object QrExeTxtCli1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM txtgeneric'
      'WHERE Aplicacao & 1'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 420
    Top = 148
    object QrExeTxtCli1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrExeTxtCli1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrAgentes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidades'
      'WHERE Nome <> '#39#39' '
      'AND Fornece2="V"'
      'ORDER BY Nome')
    Left = 572
    Top = 196
    object QrAgentesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgentesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsAgentes: TDataSource
    DataSet = QrAgentes
    Left = 640
    Top = 196
  end
  object QrOSPipMon: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pip.Nome NO_PIP, opm.*'
      'FROM ospipmon opm'
      'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad'
      'WHERE opm.Codigo=:P0'
      ''
      ''
      ''
      '')
    Left = 420
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSPipMonNO_PIP: TWideStringField
      FieldName = 'NO_PIP'
      Size = 30
    end
    object QrOSPipMonCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPipMonControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSPipMonPipCad: TIntegerField
      FieldName = 'PipCad'
    end
  end
  object DsOSPipMon: TDataSource
    DataSet = QrOSPipMon
    Left = 488
    Top = 244
  end
  object QrPIPs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pip.Codigo '
      'FROM pipcad pip'
      'LEFT JOIN osmoncab omc ON omc.Conta=pip.OSMonCab'
      'LEFT JOIN oscab    osc ON osc.Codigo=omc.Codigo'
      'WHERE pip.MotDesativ=0'
      'AND pip.DtaDesativ < "1900-01-01"'
      'AND osc.Entidade=1454'
      'AND osc.SiapTerCad=1269'
      ''
      '')
    Left = 188
    Top = 100
    object QrPIPsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrOSCxI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ELT(cxi.Aplicacao, "OR'#199'AM", "C.EXE", "AMBOS")'
      'NO_APLICACAO, cxi.* '
      'FROM oscxi cxi '
      'WHERE cxi.Codigo>0')
    Left = 420
    Top = 292
    object QrOSCxICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCxIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSCxIFotoCxa: TWideStringField
      FieldName = 'FotoCxa'
      Size = 255
    end
    object QrOSCxIDetalhes: TWideMemoField
      FieldName = 'Detalhes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCxILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCxIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCxIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCxIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCxIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCxIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCxIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCxIAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrOSCxINO_APLICACAO: TWideStringField
      FieldName = 'NO_APLICACAO'
      Size = 5
    end
  end
  object DsOSCxI: TDataSource
    DataSet = QrOSCxI
    Left = 488
    Top = 292
  end
  object DsOSAge: TDataSource
    DataSet = QrOSAge
    Left = 488
    Top = 340
  end
  object QrOSAge: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Nome NO_AGENTE, age.* '
      'FROM osage age'
      'LEFT JOIN entidades ent on ent.Codigo=age.Agente'
      'WHERE age.Codigo=:P0')
    Left = 420
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSAgeNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
    object QrOSAgeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSAgeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSAgeAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrOSAgeResponsa: TSmallintField
      FieldName = 'Responsa'
    end
  end
  object QrLo2PIP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM ospipmon'
      'WHERE Codigo=60'
      'AND PipCad=2')
    Left = 188
    Top = 148
    object QrLo2PIPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLo2PIPControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLo2PIPPipCad: TIntegerField
      FieldName = 'PipCad'
    end
    object QrLo2PIPOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrLo2PIPReordem: TIntegerField
      FieldName = 'Reordem'
    end
  end
  object QrLocCGCS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle '
      'FROM cunsits '
      'WHERE CunsGru=1 '
      'AND CunsSub=2 ')
    Left = 188
    Top = 196
    object QrLocCGCSControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrExeTxtCli2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM txtgeneric'
      'WHERE Aplicacao & 2'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 420
    Top = 196
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsExeTxtCli2: TDataSource
    DataSet = QrExeTxtCli2
    Left = 488
    Top = 196
  end
  object QrEscolhOSPrz: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DescoPer '
      'FROM osprz'
      'WHERE Escolhido=1'
      'AND Codigo=62')
    Left = 188
    Top = 244
    object QrEscolhOSPrzDescoPer: TFloatField
      FieldName = 'DescoPer'
    end
  end
  object QrAgeEmOS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle '
      'FROM osage '
      'WHERE Codigo=69 '
      'AND Agente=1339 '
      '')
    Left = 188
    Top = 292
    object QrAgeEmOSControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrOSChk: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT chi.Nome NO_ITEM, chk.*'
      'FROM oschk chk'
      'LEFT JOIN cheklstits chi ON chi.Controle=chk.ChekLstIts'
      'WHERE chk.Codigo=:P0')
    Left = 40
    Top = 580
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSChkNO_ITEM: TWideStringField
      FieldName = 'NO_ITEM'
      Size = 255
    end
    object QrOSChkCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSChkControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSChkChekLstIts: TIntegerField
      FieldName = 'ChekLstIts'
    end
  end
  object DsOSChk: TDataSource
    DataSet = QrOSChk
    Left = 105
    Top = 580
  end
  object QrLocOS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Grupo, Opcao, SiapTerCad'
      'FROM oscab')
    Left = 188
    Top = 340
    object QrLocOSGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrLocOSOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrLocOSSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrLocOSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLastEvo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PercFeito'
      'FROM osfrmevo'
      'WHERE Conta=519'
      'ORDER BY DataHora DESC'
      'LIMIT 1')
    Left = 188
    Top = 388
    object QrLastEvoPercFeito: TFloatField
      FieldName = 'PercFeito'
    end
  end
  object QrEvoFrm: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(PercFeito) ITENS,'
      'SUM(PercFeito) SUMPERC,'
      'SUM(PercFeito) / COUNT(PercFeito) MEDIA'
      'FROM osfrmcab'
      'WHERE Controle=200')
    Left = 188
    Top = 436
    object QrEvoFrmITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrEvoFrmSUMPERC: TFloatField
      FieldName = 'SUMPERC'
    end
    object QrEvoFrmMEDIA: TFloatField
      FieldName = 'MEDIA'
    end
  end
  object QrFormulFiCb: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ffc.*'
      'FROM formulficb ffc'
      'WHERE ffc.Codigo=:P0')
    Left = 572
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFormulFiCbCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulFiCbControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFormulFiCbFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrFormulFiCbPeriodd: TIntegerField
      FieldName = 'Periodd'
    end
    object QrFormulFiCbDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
  end
  object DsFormulFiCb: TDataSource
    DataSet = QrFormulFiCb
    Left = 640
    Top = 100
  end
  object QrOSOriPsq: TmySQLQuery
    Database = Dmod.MyDB
    Left = 188
    Top = 484
    object QrOSOriPsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSOriPsqGrupo: TIntegerField
      FieldName = 'Grupo'
    end
  end
  object QrFormulFiDd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ffd.*'
      'FROM formulfidd ffd'
      'WHERE ffd.Controle=:P0'
      'ORDER BY Ordem, Controle')
    Left = 572
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFormulFiDdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulFiDdControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFormulFiDdConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFormulFiDdOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFormulFiDdDias: TIntegerField
      FieldName = 'Dias'
    end
  end
  object DsFormulFiDd: TDataSource
    DataSet = QrFormulFiDd
    Left = 640
    Top = 148
  end
  object QrFrmFil: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(ffc.IDIts) ITENS'
      'FROM osfrmflhcb ffc'
      'LEFT JOIN oscab cab ON cab.Codigo=ffc.Codigo'
      ''
      'WHERE ffc.EmisStatus < 2'
      'AND ffc.EmisUltDta < SYSDATE()'
      ''
      'AND cab.Entidade=499'
      'AND cab.SiapTerCad=499'
      'AND cab.DtaExeFim > "1900-01-01"')
    Left = 188
    Top = 532
    object QrFrmFilITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object QrNulServico: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _mul_servico_;'
      ''
      'CREATE TABLE _mul_servico_'
      ''
      ''
      'SELECT DISTINCT'
      'SUM(DISTINCT(POWER(2, DesServico-1))) Codigo'
      'FROM ossrv'
      'GROUP BY Codigo'
      'ORDER BY Codigo'
      ';'
      ''
      'SELECT m_s.*'
      'FROM _mul_servico_ m_s'
      'LEFT JOIN mulservico mul ON mul.codigo=m_s.Codigo'
      'WHERE mul.Codigo IS NULL'
      ';'
      ''
      'DROP TABLE IF EXISTS _mul_servico_;'
      '')
    Left = 728
    object QrNulServicoCodigo: TFloatField
      FieldName = 'Codigo'
    end
  end
  object QrDesServico: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT des.codigo, des.Nome, des.Sigla'
      'FROM desservico des'
      'WHERE 19 & POWER(2, des.Codigo-1)'
      'ORDER BY Sigla')
    Left = 728
    Top = 48
    object QrDesServicoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDesServicoNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrDesServicoSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 10
    end
  end
  object QrMSrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT'
      'SUM(DISTINCT(POWER(2, DesServico-1))) MulServico'
      'FROM ossrv'
      'WHERE Codigo =0'
      'GROUP BY Codigo')
    Left = 188
    Top = 580
    object QrMSrvMulServico: TFloatField
      FieldName = 'MulServico'
    end
  end
  object QrAgeEqiIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT aei.Controle, aei.Entidade'
      'FROM ageeqiits aei'
      'WHERE aei.Codigo=1')
    Left = 728
    Top = 93
    object QrAgeEqiItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAgeEqiItsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrAgeEqiItsEhLider: TSmallintField
      FieldName = 'EhLider'
    end
  end
  object QrPIPCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pipcad'
      'WHERE Codigo=:P0'
      '')
    Left = 728
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPIPCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPIPCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPIPCadEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrPIPCadOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrPIPCadMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrPIPCadDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrPIPCadDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrPIPCadPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrPIPCadDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrPIPCadLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPIPCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPIPCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPIPCadUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPIPCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPIPCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPIPCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPIPCadOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPIPCadReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrPIPCadDtaInutili: TDateField
      FieldName = 'DtaInutili'
    end
    object QrPIPCadMotInutili: TIntegerField
      FieldName = 'MotInutili'
    end
  end
  object QrOsPM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM ospipmon'
      'WHERE Codigo=:P0')
    Left = 728
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOsPMCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOsPMControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOsPMPipCad: TIntegerField
      FieldName = 'PipCad'
    end
    object QrOsPMOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOsPMReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrOsPMPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrOsPMOSFMCbMae: TIntegerField
      FieldName = 'OSFMCbMae'
    end
    object QrOsPMInsercoes: TIntegerField
      FieldName = 'Insercoes'
    end
  end
  object QrLocPip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pip.*'
      'FROM pipcad pip'
      'LEFT JOIN osmoncab omc ON omc.PipCad=pip.Codigo'
      'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo'
      'WHERE cab.SiapTerCad=1249'
      'AND pip.DtaDesativ < "1900-01-01"'
      'ORDER BY pip.Codigo')
    Left = 728
    Top = 240
    object QrLocPipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocPipPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrLocPipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrLocPipEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrLocPipOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrLocPipMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrLocPipDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrLocPipDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrLocPipDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrLocPipOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object QrGgxPip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT omr.Codigo, omr.Controle, omr.Conta, omr.IDIts,'
      'omr.Ordem, omr.GraGruX, omr.UsoQtd, omr.ValCliDd, '
      '2 Tabela'
      'FROM osmonrec omr'
      'LEFT JOIN osmoncab omc ON omc.Conta=omr.Conta'
      'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo'
      'WHERE omc.PipCad=17'
      'AND omr.Desativado=0'
      'AND cab.SiapTerCad=1249'
      ''
      'UNION'
      ''
      'SELECT omr.Codigo, omr.Controle, omr.Conta, omr.IDIts,'
      'omc.Ordem, omr.GraGruX, omr.UsoQtd, omr.ValCliDd, '
      '3 Tabela'
      'FROM ospipitspr omr'
      'LEFT JOIN ospipmon omc ON omc.Controle=omr.Controle'
      'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo'
      'WHERE omc.PipCad=17'
      'AND omr.Desativado=0'
      'AND cab.SiapTerCad=1249'
      ''
      ''
      'ORDER BY Codigo, Controle,'
      'Conta, IDIts, Ordem'
      '')
    Left = 728
    Top = 288
    object QrGgxPipCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGgxPipControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGgxPipConta: TLargeintField
      FieldName = 'Conta'
      Required = True
    end
    object QrGgxPipIDIts: TLargeintField
      FieldName = 'IDIts'
      Required = True
    end
    object QrGgxPipOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrGgxPipGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrGgxPipUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      Required = True
    end
    object QrGgxPipValCliDd: TIntegerField
      FieldName = 'ValCliDd'
      Required = True
    end
    object QrGgxPipTabela: TLargeintField
      FieldName = 'Tabela'
      Required = True
    end
    object QrGgxPipDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
    end
  end
  object QrPrgLstIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM prglstits'
      'WHERE Codigo =:P0'
      'ORDER BY Ordem, Controle')
    Left = 728
    Top = 333
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrgLstItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgLstItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPrgLstItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrPrgLstItsFuncoes: TSmallintField
      FieldName = 'Funcoes'
    end
    object QrPrgLstItsDependente: TSmallintField
      FieldName = 'Dependente'
    end
    object QrPrgLstItsPrgAtrCad: TIntegerField
      FieldName = 'PrgAtrCad'
    end
    object QrPrgLstItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPrgLstItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPrgLstItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPrgLstItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPrgLstItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPrgLstItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPrgLstItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPrgLstItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPrgLstItsFiliacao: TIntegerField
      FieldName = 'Filiacao'
    end
    object QrPrgLstItsRelacao: TIntegerField
      FieldName = 'Relacao'
    end
    object QrPrgLstItsNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrPrgLstItsNivSeq: TIntegerField
      FieldName = 'NivSeq'
    end
    object QrPrgLstItsPergunta: TIntegerField
      FieldName = 'Pergunta'
    end
    object QrPrgLstItsBinarCad0: TIntegerField
      FieldName = 'BinarCad0'
    end
    object QrPrgLstItsBinarCad1: TIntegerField
      FieldName = 'BinarCad1'
    end
    object QrPrgLstItsAcaoPrd: TSmallintField
      FieldName = 'AcaoPrd'
    end
    object QrPrgLstItsBinario0: TWideStringField
      FieldName = 'Binario0'
      Size = 30
    end
    object QrPrgLstItsBinario1: TWideStringField
      FieldName = 'Binario1'
      Size = 30
    end
    object QrPrgLstItsLupForma: TSmallintField
      FieldName = 'LupForma'
    end
    object QrPrgLstItsLupQtdVzs: TSmallintField
      FieldName = 'LupQtdVzs'
    end
  end
  object TbPrgCadPrg: TmySQLTable
    Database = Dmod.MyDB
    TableName = 'prgcadprg'
    Left = 812
    Top = 48
    object TbPrgCadPrgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbPrgCadPrgNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object TbPrgCadPrgSigla: TWideStringField
      DisplayWidth = 100
      FieldName = 'Sigla'
      Size = 100
    end
  end
  object DsPrgCadPrg: TDataSource
    DataSet = TbPrgCadPrg
    Left = 880
    Top = 48
  end
  object QrUsoRatif: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT omr.Codigo, omr.Controle, omr.Conta,'
      'omr.IDIts, omr.GraGruX, omr.PrvQtd,'
      'ggv.Controle CtrlGGV, ggv.CustoPreco'
      'FROM osmonrec omr'
      'LEFT JOIN gragruval ggv ON ggv.GraGruX=omr.GraGruX'
      '     AND ggv.GraCusPrc=1 '
      '     AND ggv.Entidade=-11'
      'WHERE omr.RatifUso=0'
      'AND omr.Codigo>0')
    Left = 728
    Top = 380
    object QrUsoRatifCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUsoRatifControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrUsoRatifConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrUsoRatifIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrUsoRatifGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrUsoRatifPrvQtd: TFloatField
      FieldName = 'PrvQtd'
    end
    object QrUsoRatifCtrlGGV: TIntegerField
      FieldName = 'CtrlGGV'
      Required = True
    end
    object QrUsoRatifCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
  end
  object QrErrRatif: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT omr.GraGruX, ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM osmonrec omr'
      'LEFT JOIN gragrux ggx ON ggx.Controle=omr.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragruval ggv ON ggv.GraGruX=ggx.Controle'
      '  AND ggv.GraCusPrc=1'
      '  AND ggv.Entidade=-11'
      'WHERE omr.Codigo=4'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 728
    Top = 428
    object QrErrRatifGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrErrRatifGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrErrRatifNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object frxGER_OSERV_001_ModOS_01: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  //MePagina.Visible := <VARF_PAGINAR>;  '
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnClickObject = frxGER_OSERV_001_ModOS_01ClickObject
    Left = 728
    Top = 524
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsErrRatif
        DataSetName = 'frxDsErrRatif'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 109.606370000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Produtos sem pre'#231'o de custo definido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 105.826840000000000000
          Top = 45.354360000000000000
          Width = 574.488354960000100000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 45.354360000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 105.826840000000000000
          Top = 90.708720000000000000
          Width = 574.488354960000100000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lugar')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 45.354360000000000000
          Top = 90.708720000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Top = 90.708720000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#237'vel 1')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 105.826840000000000000
          Top = 64.252010000000000000
          Width = 574.488354960000100000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_LISTA_VALORES]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Top = 64.252010000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lista de valores:')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        DataSet = frxDsErrRatif
        DataSetName = 'frxDsErrRatif'
        RowCount = 0
        object Memo7: TfrxMemoView
          Left = 45.354360000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Cursor = crHandPoint
          TagStr = '[frxDsErrRatif."GraGru1"]'
          DataField = 'GraGruX'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrRatif."GraGruX"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Cursor = crHandPoint
          TagStr = '[frxDsErrRatif."GraGru1"]'
          DataField = 'GraGru1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrRatif."GraGru1"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 105.826840000000000000
          Width = 574.488560000000000000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrRatif."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrAge: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 728
    Top = 572
    object QrAgeAgente: TIntegerField
      FieldName = 'Agente'
    end
  end
  object QrPrzSel: TmySQLQuery
    Database = Dmod.MyDB
    Left = 728
    Top = 620
    object QrPrzSelControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPrzSelCondicao: TIntegerField
      FieldName = 'Condicao'
    end
  end
  object QrUL_OSCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, cab.Grupo, cab.Opcao, cab.Empresa,'
      'cab.Entidade, cab.SiapTerCad, cab.Estatus, cab.FatoGeradr,'
      'cab.DtaContat,'
      'cab.DtaVisPrv, cab.FimVisPrv,'
      'cab.DtaExePrv, cab.FimVisExe,'
      'cab.DtaVisExe, cab.FimExePrv,'
      'cab.DtaExeIni, cab.DtaExeFim,'
      ''
      'cab.OrcamServi, cab.OrcamDesco, cab.OrcamOutrs, cab.OrcamTotal,'
      'cab.InvalServi, cab.InvalDesco, cab.InvalOutrs, cab.InvalTotal,'
      'cab.ValorServi, cab.ValorDesco, cab.ValorOutrs, cab.ValorTotal,'
      'cab.EntiContat, cab.NumContrat, cab.EntPagante, cab.EntContrat,'
      'cab.CondicaoPg, cab.Operacao,'
      'cab.ObsGaranti, cab.ExeTxtCli1, cab.ExeTxtCli2,'
      'cab.MulServico, cab.AgeEqiCab,'
      ''
      'eco.Nome NO_ENTICONTAT,'
      'fge.Nome NO_FatoGeradr, sta.Nome NO_ESTATUS,'
      'stc.Nome NO_SiapTerCad, stc.LstCusPrd,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT,'
      'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG,'
      'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR,'
      'car.Nome NO_CART, ppc.Nome NO_PRZ,'
      'tg1.Nome NO_ExeTxtCli1, tg2.Nome NO_ExeTxtCli2,'
      'aec.Nome NO_AgeEqiCab'
      ''
      'FROM oscab cab'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante'
      'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN carteiras car ON car.Codigo=cab.CartEmis'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondicaoPg'
      'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat'
      'LEFT JOIN txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1'
      'LEFT JOIN txtgeneric tg2 ON tg2.Codigo=cab.ExeTxtCli2'
      'LEFT JOIN ageeqicab aec ON aec.Codigo=cab.AgeEqiCab')
    Left = 188
    Top = 628
    object QrUL_OSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUL_OSCabGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrUL_OSCabOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrUL_OSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrUL_OSCabEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrUL_OSCabSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrUL_OSCabEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object QrUL_OSCabFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrUL_OSCabDtaContat: TDateTimeField
      FieldName = 'DtaContat'
    end
    object QrUL_OSCabDtaVisPrv: TDateTimeField
      FieldName = 'DtaVisPrv'
    end
    object QrUL_OSCabFimVisPrv: TDateTimeField
      FieldName = 'FimVisPrv'
    end
    object QrUL_OSCabDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
    end
    object QrUL_OSCabFimVisExe: TDateTimeField
      FieldName = 'FimVisExe'
    end
    object QrUL_OSCabDtaVisExe: TDateTimeField
      FieldName = 'DtaVisExe'
    end
    object QrUL_OSCabFimExePrv: TDateTimeField
      FieldName = 'FimExePrv'
    end
    object QrUL_OSCabDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
    end
    object QrUL_OSCabDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrUL_OSCabOrcamServi: TFloatField
      FieldName = 'OrcamServi'
    end
    object QrUL_OSCabOrcamDesco: TFloatField
      FieldName = 'OrcamDesco'
    end
    object QrUL_OSCabOrcamOutrs: TFloatField
      FieldName = 'OrcamOutrs'
    end
    object QrUL_OSCabOrcamTotal: TFloatField
      FieldName = 'OrcamTotal'
    end
    object QrUL_OSCabInvalServi: TFloatField
      FieldName = 'InvalServi'
    end
    object QrUL_OSCabInvalDesco: TFloatField
      FieldName = 'InvalDesco'
    end
    object QrUL_OSCabInvalOutrs: TFloatField
      FieldName = 'InvalOutrs'
    end
    object QrUL_OSCabInvalTotal: TFloatField
      FieldName = 'InvalTotal'
    end
    object QrUL_OSCabValorServi: TFloatField
      FieldName = 'ValorServi'
    end
    object QrUL_OSCabValorDesco: TFloatField
      FieldName = 'ValorDesco'
    end
    object QrUL_OSCabValorOutrs: TFloatField
      FieldName = 'ValorOutrs'
    end
    object QrUL_OSCabValorTotal: TFloatField
      FieldName = 'ValorTotal'
    end
    object QrUL_OSCabEntiContat: TIntegerField
      FieldName = 'EntiContat'
    end
    object QrUL_OSCabNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrUL_OSCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
    end
    object QrUL_OSCabEntContrat: TIntegerField
      FieldName = 'EntContrat'
    end
    object QrUL_OSCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrUL_OSCabOperacao: TIntegerField
      FieldName = 'Operacao'
    end
    object QrUL_OSCabObsGaranti: TWideMemoField
      FieldName = 'ObsGaranti'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrUL_OSCabObsExecuta: TWideMemoField
      FieldName = 'ObsExecuta'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrUL_OSCabExeTxtCli1: TIntegerField
      FieldName = 'ExeTxtCli1'
    end
    object QrUL_OSCabExeTxtCli2: TIntegerField
      FieldName = 'ExeTxtCli2'
    end
    object QrUL_OSCabMulServico: TLargeintField
      FieldName = 'MulServico'
    end
    object QrUL_OSCabAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object QrUL_OSCabNO_ENTICONTAT: TWideStringField
      FieldName = 'NO_ENTICONTAT'
      Size = 30
    end
    object QrUL_OSCabNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
    end
    object QrUL_OSCabNO_ESTATUS: TWideStringField
      FieldName = 'NO_ESTATUS'
      Size = 60
    end
    object QrUL_OSCabNO_SiapTerCad: TWideStringField
      FieldName = 'NO_SiapTerCad'
      Size = 100
    end
    object QrUL_OSCabLstCusPrd: TIntegerField
      FieldName = 'LstCusPrd'
    end
    object QrUL_OSCabNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrUL_OSCabTel_ENT: TWideStringField
      FieldName = 'Tel_ENT'
    end
    object QrUL_OSCabNO_PAG: TWideStringField
      FieldName = 'NO_PAG'
      Size = 100
    end
    object QrUL_OSCabNO_CTR: TWideStringField
      FieldName = 'NO_CTR'
      Size = 100
    end
    object QrUL_OSCabNO_CART: TWideStringField
      FieldName = 'NO_CART'
      Size = 100
    end
    object QrUL_OSCabNO_PRZ: TWideStringField
      FieldName = 'NO_PRZ'
      Size = 50
    end
    object QrUL_OSCabNO_ExeTxtCli1: TWideStringField
      FieldName = 'NO_ExeTxtCli1'
      Size = 100
    end
    object QrUL_OSCabNO_ExeTxtCli2: TWideStringField
      FieldName = 'NO_ExeTxtCli2'
      Size = 100
    end
    object QrUL_OSCabNO_AgeEqiCab: TWideStringField
      FieldName = 'NO_AgeEqiCab'
      Size = 255
    end
    object QrUL_OSCabLCPUsed: TIntegerField
      FieldName = 'LCPUsed'
    end
  end
  object QrUL_OSPipMon: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT plc.Nome NO_PrgLstCab, pip.Nome NO_PIP, opm.* '
      'FROM ospipmon opm '
      'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad '
      'LEFT JOIN prglstcab plc ON plc.Codigo=opm.PrgLstCab '
      'WHERE opm.Codigo=1000 ')
    Left = 188
    Top = 672
    object QrUL_OSPipMonNO_PrgLstCab: TWideStringField
      FieldName = 'NO_PrgLstCab'
      Size = 100
    end
    object QrUL_OSPipMonNO_PIP: TWideStringField
      FieldName = 'NO_PIP'
      Size = 30
    end
    object QrUL_OSPipMonCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUL_OSPipMonControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrUL_OSPipMonPipCad: TIntegerField
      FieldName = 'PipCad'
    end
    object QrUL_OSPipMonOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrUL_OSPipMonReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrUL_OSPipMonLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrUL_OSPipMonDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrUL_OSPipMonDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrUL_OSPipMonUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrUL_OSPipMonUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrUL_OSPipMonAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrUL_OSPipMonAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrUL_OSPipMonPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrUL_OSPipMonOSFMCbMae: TIntegerField
      FieldName = 'OSFMCbMae'
    end
    object QrUL_OSPipMonInsercoes: TIntegerField
      FieldName = 'Insercoes'
    end
  end
  object QrEqMo: TmySQLQuery
    Database = Dmod.MyDB
    Left = 812
    Top = 96
    object QrEqMoNaoUsaPrdt: TSmallintField
      FieldName = 'NaoUsaPrdt'
    end
  end
  object QrImpQrcPmv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT /*plc.Nome NO_prglstcab, */'
      'ofc.Codigo, ofc.Controle, ofc.Conta, ofc.PipCad,'
      'pip.Nome NO_PIP, gg1.Nome NO_EquipAplic'
      '/* , mon.Nome NO_FORMULA,  '
      'med.CodUsu CU_UNIDMED, '
      'med.SIGLA, med.Nome NO_UNIDMED */'
      'FROM osmoncab ofc '
      '/*'
      'LEFT JOIN formulas mon ON mon.Codigo=ofc.Formula '
      '*/'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ofc.EquipAplic '
      'LEFT JOIN gragru1 gg1 ON ggx.Gragru1=gg1.Nivel1 '
      'LEFT JOIN pipcad  pip ON pip.Codigo=ofc.PipCad '
      '/*'
      'LEFT JOIN unidmed med ON med.Codigo=ofc.UnidMed '
      'LEFT JOIN prglstcab plc ON plc.Codigo=pip.PrgLstCab '
      'LEFT JOIN siapimadep sid ON sid.Controle=pip.Dependenci '
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci '
      '*/'
      'WHERE ofc.Controle>0')
    Left = 812
    Top = 148
    object QrImpQrcPmvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImpQrcPmvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrImpQrcPmvConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrImpQrcPmvPipCad: TIntegerField
      FieldName = 'PipCad'
    end
    object QrImpQrcPmvNO_PIP: TWideStringField
      FieldName = 'NO_PIP'
      Size = 30
    end
    object QrImpQrcPmvNO_EquipAplic: TWideStringField
      FieldName = 'NO_EquipAplic'
      Size = 120
    end
  end
  object frxDsImpQrcPmv: TfrxDBDataset
    UserName = 'frxDsImpQrcPmv'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'PipCad=PipCad'
      'NO_PIP=NO_PIP'
      'NO_EquipAplic=NO_EquipAplic')
    DataSet = QrImpQrcPmv
    BCDToCurrency = False
    Left = 812
    Top = 196
  end
  object frxGER_OSERV_001_ModOS_02: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41581.385905150460000000
    ReportOptions.LastChange = 41581.490219907400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Picture1.Visible := <VARF_OBTEM_BMP>;                         ' +
        '         '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxGER_OSERV_001_ModOS_02GetValue
    Left = 812
    Top = 244
    Datasets = <
      item
        DataSet = frxDsImpQrcPmv
        DataSetName = 'frxDsImpQrcPmv'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 196.535560000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsImpQrcPmv
        DataSetName = 'frxDsImpQrcPmv'
        KeepChild = True
        KeepTogether = True
        RowCount = 0
        object Shape1: TfrxShapeView
          Width = 718.110700000000000000
          Height = 196.535433070866100000
          Frame.Width = 0.500000000000000000
        end
        object Memo1: TfrxMemoView
          Left = 204.094620000000000000
          Top = 3.779530000000001000
          Width = 506.457020000000000000
          Height = 105.826840000000000000
          DataSet = frxDsImpQrcPmv
          DataSetName = 'frxDsImpQrcPmv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            ''
            'Nome: [frxDsImpQrcPmv."NO_PIP"] '
            '')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 3.779530000000000000
          Top = 3.779530000000001000
          Width = 188.976377950000000000
          Height = 188.976377950000000000
          Stretched = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo2: TfrxMemoView
          Left = 204.094620000000000000
          Top = 109.606370000000000000
          Width = 506.457020000000000000
          Height = 41.574830000000000000
          DataSet = frxDsImpQrcPmv
          DataSetName = 'frxDsImpQrcPmv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'ID: [frxDsImpQrcPmv."PipCad"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 204.094620000000000000
          Top = 151.181200000000000000
          Width = 506.457020000000000000
          Height = 41.574830000000000000
          DataSet = frxDsImpQrcPmv
          DataSetName = 'frxDsImpQrcPmv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tipo: [frxDsImpQrcPmv."NO_EquipAplic"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 196.535560000000000000
          Height = 196.535560000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
      end
    end
  end
  object frxDsErrRatif: TfrxDBDataset
    UserName = 'frxDsErrRatif'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR')
    DataSet = QrErrRatif
    BCDToCurrency = False
    Left = 728
    Top = 480
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 188
    Top = 720
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrLocOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrLocSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
  end
  object QrBxaFrm: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM osfrmrec'
      'WHERE SMI_IDCtrl = 0'
      'AND RatifUso=1'
      'ORDER BY IDIts')
    Left = 40
    Top = 388
    object QrBxaFrmEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrBxaFrmCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object QrBxaFrmStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrBxaFrmCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBxaFrmControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBxaFrmConta: TFloatField
      FieldName = 'Conta'
    end
    object QrBxaFrmIDIts: TFloatField
      FieldName = 'IDIts'
    end
    object QrBxaFrmGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrBxaFrmUsoQtd: TFloatField
      FieldName = 'UsoQtd'
    end
    object QrBxaFrmUsoCusTot: TFloatField
      FieldName = 'UsoCusTot'
    end
    object QrBxaFrmSMI_IDCtrl: TFloatField
      FieldName = 'SMI_IDCtrl'
    end
  end
  object QrOSDesat: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 108
    Top = 388
    object QrOSDesatDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrOSDesatNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrOSDesatControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrDadosCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Estatus, sta.Nome NO_STATUS'
      'FROM oscab cab'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'WHERE cab.Codigo>0'
      '')
    Left = 488
    Top = 504
    object QrDadosCabEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object QrDadosCabNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object QrDadosSrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT des.Nome NO_SERVICO'
      'FROM ossrv srv'
      'LEFT JOIN desservico des ON des.Codigo=srv.Desservico '
      'WHERE srv.Codigo>0')
    Left = 488
    Top = 548
    object QrDadosSrvNO_SERVICO: TWideStringField
      FieldName = 'NO_SERVICO'
      Size = 60
    end
    object QrDadosSrvControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrDadosFrmCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frm.Nome NO_FORMULA'
      'FROM osfrmcab ofc'
      'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula'
      'WHERE ofc.Controle>0')
    Left = 488
    Top = 592
    object QrDadosFrmCabNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrDadosFrmCabConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrDadosFrmDep: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frm.Nome NO_FORMULA'
      'FROM osfrmcab ofc'
      'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula'
      'WHERE ofc.Controle>0')
    Left = 488
    Top = 636
    object QrDadosFrmDepNOME: TWideStringField
      FieldName = 'NOME'
      Size = 255
    end
  end
  object QrNaoExeRat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT srv.Codigo Localizador, '
      
        'srv.Controle IDServico, des.Nome NO_DesServico, des.Sigla NO_SIG' +
        'LA, '
      '1 Tabela, "Aplica'#231#227'o" TipoFormula, '
      'ofc.Conta IDFormula, frm.Nome NO_FORMULA, '
      'IF((srv.TudoFeitoM OR srv.TudoFeitoA), 1, '
      '  IF(ofc.PercFeito>99.99, 1,  0)) REALIZADO, '
      'med.Sigla SIGLAUNIDMED, gg1.Nome NO_GG1, '
      'ELT(RatifUso+1, "N", "S", "?") NO_RatifUso '
      'FROM osfrmrec ofr '
      'LEFT JOIN osfrmcab ofc ON ofc.Conta=ofr.Conta '
      'LEFT JOIN ossrv srv ON srv.Controle=ofc.Controle '
      'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX '
      'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed med on med.Codigo=gg1.UnidMed '
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico '
      'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula '
      'WHERE srv.Codigo=4293 '
      'AND '
      'IF((srv.TudoFeitoM OR srv.TudoFeitoA), 1, '
      '  IF(ofc.PercFeito>99.99, 1,  0)) = 0 '
      ' '
      'UNION '
      ' '
      'SELECT srv.Codigo Localizador, '
      
        'srv.Controle IDServico, des.Nome NO_DesServico, des.Sigla NO_SIG' +
        'LA, '
      '2 Tabela, "Monitoramento" TipoFormula, '
      'ofc.Conta IDFormula, frm.Nome NO_FORMULA, '
      'IF((srv.TudoFeitoM OR srv.TudoFeitoA), 1,0) REALIZADO, '
      'med.Sigla SIGLAUNIDMED, gg1.Nome NO_GG1, '
      'ELT(RatifUso+1, "N", "S", "?") NO_RatifUso '
      'FROM osmonrec ofr '
      'LEFT JOIN osmoncab ofc ON ofc.Conta=ofr.Conta '
      'LEFT JOIN ossrv srv ON srv.Controle=ofc.Controle '
      'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX '
      'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed med on med.Codigo=gg1.UnidMed '
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico '
      'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula '
      'WHERE srv.Codigo=777 '
      'AND '
      'IF((srv.TudoFeitoM OR srv.TudoFeitoA), 1, 0) = 0 ')
    Left = 812
    Top = 292
    object QrNaoExeRatLocalizador: TIntegerField
      FieldName = 'Localizador'
    end
    object QrNaoExeRatIDServico: TIntegerField
      FieldName = 'IDServico'
    end
    object QrNaoExeRatNO_DesServico: TWideStringField
      FieldName = 'NO_DesServico'
      Size = 60
    end
    object QrNaoExeRatNO_SIGLA: TWideStringField
      FieldName = 'NO_SIGLA'
      Size = 10
    end
    object QrNaoExeRatTabela: TLargeintField
      FieldName = 'Tabela'
      Required = True
    end
    object QrNaoExeRatTipoFormula: TWideStringField
      FieldName = 'TipoFormula'
      Required = True
      Size = 13
    end
    object QrNaoExeRatIDFormula: TIntegerField
      FieldName = 'IDFormula'
    end
    object QrNaoExeRatNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrNaoExeRatREALIZADO: TLargeintField
      FieldName = 'REALIZADO'
      Required = True
    end
    object QrNaoExeRatSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrNaoExeRatNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrNaoExeRatNO_RatifUso: TWideStringField
      FieldName = 'NO_RatifUso'
      Size = 1
    end
  end
  object frxDsNaoExeRat: TfrxDBDataset
    UserName = 'frxDsNaoExeRat'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Localizador=Localizador'
      'IDServico=IDServico'
      'NO_DesServico=NO_DesServico'
      'NO_SIGLA=NO_SIGLA'
      'Tabela=Tabela'
      'TipoFormula=TipoFormula'
      'IDFormula=IDFormula'
      'NO_FORMULA=NO_FORMULA'
      'REALIZADO=REALIZADO'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'NO_GG1=NO_GG1'
      'NO_RatifUso=NO_RatifUso')
    DataSet = QrNaoExeRat
    BCDToCurrency = False
    Left = 812
    Top = 340
  end
  object frxGER_OSERV_001_ModOS_03: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  //MePagina.Visible := <VARF_PAGINAR>;  '
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnClickObject = frxGER_OSERV_001_ModOS_03ClickObject
    Left = 812
    Top = 388
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNaoExeRat
        DataSetName = 'frxDsNaoExeRat'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 68.031540000000010000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Memo1: TfrxMemoView
          Top = 49.133890000000010000
          Width = 56.692913385826770000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 56.692950000000000000
          Top = 49.133890000000010000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Servi'#231'o')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133890000000010000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID F'#243'rm. Monitoram.')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 396.850650000000000000
          Top = 49.133890000000010000
          Width = 359.055118110236200000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome F'#243'rmula')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 113.385900000000000000
          Top = 49.133890000000010000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla Servi'#231'o')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 264.567100000000000000
          Top = 49.133890000000010000
          Width = 75.590551181102360000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo f'#243'rmula')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 755.905999999999900000
          Top = 49.133890000000010000
          Width = 215.433087950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do produto')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 759.685530000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Itens sem confirma'#231#227'o de realiza'#231#227'o de sua f'#243'rmula')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 865.512370000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 147.401670000000000000
        Width = 971.339210000000000000
        DataSet = frxDsNaoExeRat
        DataSetName = 'frxDsNaoExeRat'
        RowCount = 0
        object Memo5: TfrxMemoView
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          Cursor = crHandPoint
          TagStr = '[frxDsNaoExeRat."IDServico"]'
          DataField = 'Localizador'
          DataSet = frxDsNaoExeRat
          DataSetName = 'frxDsNaoExeRat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNaoExeRat."Localizador"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 56.692950000000010000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          Cursor = crHandPoint
          TagStr = '[frxDsNaoExeRat."IDServico"]'
          DataField = 'IDServico'
          DataSet = frxDsNaoExeRat
          DataSetName = 'frxDsNaoExeRat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNaoExeRat."IDServico"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 340.157700000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'IDFormula'
          DataSet = frxDsNaoExeRat
          DataSetName = 'frxDsNaoExeRat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNaoExeRat."IDFormula"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 396.850650000000000000
          Width = 359.055118110236200000
          Height = 18.897650000000000000
          DataField = 'NO_FORMULA'
          DataSet = frxDsNaoExeRat
          DataSetName = 'frxDsNaoExeRat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNaoExeRat."NO_FORMULA"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 113.385900000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataField = 'NO_SIGLA'
          DataSet = frxDsNaoExeRat
          DataSetName = 'frxDsNaoExeRat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNaoExeRat."NO_SIGLA"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 264.567100000000000000
          Width = 75.590551180000010000
          Height = 18.897650000000000000
          DataField = 'TipoFormula'
          DataSet = frxDsNaoExeRat
          DataSetName = 'frxDsNaoExeRat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNaoExeRat."TipoFormula"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 755.905999999999900000
          Width = 215.433087950000000000
          Height = 18.897650000000000000
          DataField = 'NO_GG1'
          DataSet = frxDsNaoExeRat
          DataSetName = 'frxDsNaoExeRat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNaoExeRat."NO_GG1"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 226.771800000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 650.079160000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrOSCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '      SELECT * '
      '      FROM oscab ')
    Left = 40
    Top = 628
    object QrOSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrOSCabEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrOSCabSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrOSCabEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object QrOSCabFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrOSCabDtaContat: TDateTimeField
      FieldName = 'DtaContat'
    end
    object QrOSCabDtaVisPrv: TDateTimeField
      FieldName = 'DtaVisPrv'
    end
    object QrOSCabDtaVisExe: TDateTimeField
      FieldName = 'DtaVisExe'
    end
    object QrOSCabDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
    end
    object QrOSCabDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
    end
    object QrOSCabDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrOSCabValorTotal: TFloatField
      FieldName = 'ValorTotal'
    end
    object QrOSCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCabDdsPosVda: TIntegerField
      FieldName = 'DdsPosVda'
    end
    object QrOSCabEntiContat: TIntegerField
      FieldName = 'EntiContat'
    end
    object QrOSCabNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrOSCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
    end
    object QrOSCabEntContrat: TIntegerField
      FieldName = 'EntContrat'
    end
    object QrOSCabOperacao: TIntegerField
      FieldName = 'Operacao'
    end
    object QrOSCabValorPre: TFloatField
      FieldName = 'ValorPre'
    end
    object QrOSCabFimVisPrv: TDateTimeField
      FieldName = 'FimVisPrv'
    end
    object QrOSCabFimExePrv: TDateTimeField
      FieldName = 'FimExePrv'
    end
    object QrOSCabGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrOSCabNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrOSCabOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrOSCabAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object QrOSCabDtaLibFat: TDateTimeField
      FieldName = 'DtaLibFat'
    end
    object QrOSCabDtaFimFat: TDateTimeField
      FieldName = 'DtaFimFat'
    end
    object QrOSCabValorServi: TFloatField
      FieldName = 'ValorServi'
    end
    object QrOSCabValorDesco: TFloatField
      FieldName = 'ValorDesco'
    end
    object QrOSCabValorOutrs: TFloatField
      FieldName = 'ValorOutrs'
    end
    object QrOSCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrOSCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrOSCabSerNF: TWideStringField
      FieldName = 'SerNF'
      Size = 3
    end
    object QrOSCabNumNF: TIntegerField
      FieldName = 'NumNF'
    end
    object QrOSCabInvalServi: TFloatField
      FieldName = 'InvalServi'
    end
    object QrOSCabInvalDesco: TFloatField
      FieldName = 'InvalDesco'
    end
    object QrOSCabInvalOutrs: TFloatField
      FieldName = 'InvalOutrs'
    end
    object QrOSCabInvalTotal: TFloatField
      FieldName = 'InvalTotal'
    end
    object QrOSCabOrcamServi: TFloatField
      FieldName = 'OrcamServi'
    end
    object QrOSCabOrcamDesco: TFloatField
      FieldName = 'OrcamDesco'
    end
    object QrOSCabOrcamOutrs: TFloatField
      FieldName = 'OrcamOutrs'
    end
    object QrOSCabOrcamTotal: TFloatField
      FieldName = 'OrcamTotal'
    end
    object QrOSCabValiDdOrca: TIntegerField
      FieldName = 'ValiDdOrca'
    end
    object QrOSCabExeTxtCli1: TIntegerField
      FieldName = 'ExeTxtCli1'
    end
    object QrOSCabObsGaranti: TWideMemoField
      FieldName = 'ObsGaranti'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCabObsExecuta: TWideMemoField
      FieldName = 'ObsExecuta'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCabExeTxtCli2: TIntegerField
      FieldName = 'ExeTxtCli2'
    end
    object QrOSCabFimVisExe: TDateTimeField
      FieldName = 'FimVisExe'
    end
    object QrOSCabOptado: TSmallintField
      FieldName = 'Optado'
    end
    object QrOSCabHowGerou: TSmallintField
      FieldName = 'HowGerou'
    end
    object QrOSCabPosGerou: TSmallintField
      FieldName = 'PosGerou'
    end
    object QrOSCabOSFlhUltGe: TIntegerField
      FieldName = 'OSFlhUltGe'
    end
    object QrOSCabMulServico: TLargeintField
      FieldName = 'MulServico'
    end
    object QrOSCabOSFlhGrCab: TIntegerField
      FieldName = 'OSFlhGrCab'
    end
    object QrOSCabOSFlhGrIts: TIntegerField
      FieldName = 'OSFlhGrIts'
    end
    object QrOSCabSohInicial: TIntegerField
      FieldName = 'SohInicial'
    end
    object QrOSCabStPipAdPrg: TSmallintField
      FieldName = 'StPipAdPrg'
    end
    object QrOSCabLstUplWeb: TDateTimeField
      FieldName = 'LstUplWeb'
    end
  end
  object QrFormulEPI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT epi.*'
      'FROM formulepi epi'
      'WHERE epi.Codigo=:P0')
    Left = 572
    Top = 257
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFormulEPICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulEPIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFormulEPIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DsFormulEPI: TDataSource
    DataSet = QrFormulEPI
    Left = 640
    Top = 257
  end
end
