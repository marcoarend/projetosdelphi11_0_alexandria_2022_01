unit GraGruMoW;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBGridZTO, mySQLDbTables, UnDmkEnums;

type
  TFmGraGruMoW = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrGraGruMoW: TmySQLQuery;
    DsGraGruMoW: TDataSource;
    QrGraGruMoWGraGru1: TIntegerField;
    QrGraGruMoWGraGruX: TIntegerField;
    QrGraGruMoWNome: TWideStringField;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenGraGruMoW(LocFld: String; LocVal: Integer);
  end;

  var
  FmGraGruMoW: TFmGraGruMoW;

implementation

uses UnMyObjects, Module, DmkDAC_PF, MyDBCheck, UMySQLModule, UnGOTOy;

{$R *.DFM}

procedure TFmGraGruMoW.BtExcluiClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  if (QrGraGruMoW.State = dsInactive) or (QrGraGruMoW.RecordCount = 0) then
    Exit;
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada do produto selecionado?',
  'gragrumow', 'GraGruX', QrGraGruMoWGraGruX.Value, Dmod.MyDBn) = ID_YES then
  begin
    GraGruX := GOTOy.LocalizaPriorNextIntQr(QrGraGruMoW, QrGraGruMoWGraGruX,
    QrGraGruMoWGraGruX.Value);
    ReopenGraGruMoW('GraGruX', GraGruX);
  end;
end;

procedure TFmGraGruMoW.BtIncluiClick(Sender: TObject);
const
  Aviso   = '...';
  Caption = 'Adi��o de produto';
  Prompt  = 'Informe o produto';
  Campo   = 'Descricao';
var
  QrySel, QryExe: TmySQLQuery;
  Nome: String;
  GraGru1, GraGruX: Integer;
begin
  GraGruX :=
  DBCheck.EscolheCodigoUnico(Aviso, Caption, Prompt, nil, nil, Campo, 0, [
  'SELECT ggx.Controle Codigo, gg1.Nome ' + Campo,
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, False);
  if GraGruX <> 0 then
  begin
    QrySel := TmySQLQuery.Create(Dmod);
    QryExe := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QrySel, Dmod.MyDBn, [
      'SELECT GraGruX ',
      'FROM gragrumow ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if QrySel.RecordCount = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrySel, Dmod.MyDB, [
        'SELECT ggx.Controle, gg1.Nivel1, gg1.Nome ',
        'FROM gragrux ggx ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
        'WHERE Controle=' + Geral.FF0(GraGruX),
        '']);
        GraGru1 := QrySel.FieldByName('Nivel1').AsInteger;
        Nome    := QrySel.FieldByName('Nome').AsString;
        //
        QryExe.DataBase := DMod.MyDBn;
        if UMyMod.SQLInsUpd(QryExe, stIns, 'gragrumow', False, [
        'GraGru1', 'Nome'], [
        'GraGruX'], [
        GraGru1, Nome], [
        GraGruX], True) then
          ReopenGraGruMoW('GraGruX', GraGruX);
      end else
      begin
        Geral.MB_Aviso('O item selecionado j� est� inserido na tabela!');
        Exit;
      end;
    finally
      QrySel.Free;
      QryExe.Free;
    end;
  end;
end;

procedure TFmGraGruMoW.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruMoW.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruMoW.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenGraGruMoW('', 0);
end;

procedure TFmGraGruMoW.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruMoW.ReopenGraGruMoW(LocFld: String; LocVal: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruMoW, Dmod.MyDBn, [
  'SELECT * FROM gragrumow',
  '']);
  if (LocFld <> '') and (LocVal <> 0) then
    QrGraGruMoW.Locate(LocFld, LocVal, []);
end;

end.
