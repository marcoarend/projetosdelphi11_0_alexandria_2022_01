object FmOSGarantRnw: TFmOSGarantRnw
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-042 :: Garantias Vencidas e a Vencer'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 374
        Height = 32
        Caption = 'Garantias Vencidas e a Vencer'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 374
        Height = 32
        Caption = 'Garantias Vencidas e a Vencer'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 374
        Height = 32
        Caption = 'Garantias Vencidas e a Vencer'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object LaTotal: TLabel
          Left = 2
          Top = 452
          Width = 3
          Height = 13
          Align = alBottom
        end
        object DBGGarant: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 437
          Align = alClient
          DataSource = DsGarant
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'DtGarantia'
              Title.Caption = 'T'#233'rmino Garantia'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Localizador'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaExeIni'
              Title.Caption = 'Ini. execu'#231#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SERVICO'
              Title.Caption = 'Servi'#231'o'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GarantiaDd'
              Title.Caption = 'Dias Gar.'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SiapTerCad'
              Title.Caption = 'Lugar'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENT'
              Title.Caption = 'Sub-cliente'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENTICONTAT'
              Title.Caption = 'Contato'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tel_ENT'
              Title.Caption = 'Telefone'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FatoGeradr'
              Title.Caption = 'Fato gerador'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CTR'
              Title.Caption = 'Contratante'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PAG'
              Title.Caption = 'Pagante'
              Width = 120
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtEncerra: TBitBtn
        Tag = 11
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Encerra'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtEncerraClick
      end
      object BtConversas: TBitBtn
        Tag = 399
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Conversas'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtConversasClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 341
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 445
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtNenhumClick
      end
    end
  end
  object QrGarant: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGarantAfterOpen
    BeforeClose = QrGarantBeforeClose
    AfterScroll = QrGarantAfterScroll
    SQL.Strings = (
      'SELECT '#39'ossrv'#39' Tabela, eco.Nome NO_ENTICONTAT, '
      'fge.Nome NO_FatoGeradr, stc.Nome NO_SiapTerCad,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT,'
      'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG,'
      'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR,'
      ''
      
        'cab.Codigo, srv.Controle, des.Nome NO_SERVICO, cab.DtaExeIni, sr' +
        'v.GarantiaDd,'
      'DATE_ADD(cab.DtaExeIni, INTERVAL srv.GarantiaDd DAY) DtGarantia'
      'FROM ossrv srv'
      'LEFT JOIN oscab cab ON cab.Codigo=srv.Codigo'
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico'
      ''
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante'
      'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat'
      ''
      'WHERE DATE_ADD(cab.DtaExeini, INTERVAL srv.GarantiaDd DAY)'
      '  BETWEEN "2014-06-29" AND "2014-07-29"'
      ''
      'UNION'
      ''
      'SELECT '#39'oscxa'#39' Tabela, eco.Nome NO_ENTICONTAT, '
      'fge.Nome NO_FatoGeradr, stc.Nome NO_SiapTerCad,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT,'
      'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG,'
      'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR,'
      ''
      
        'cab.Codigo, cxa.Controle, "LIMPEZA DE CAIXA D'#39#193'GUA" NO_SERVICO, ' +
        'cab.DtaExeIni, cxa.GarantiaDd,'
      'DATE_ADD(cab.DtaExeIni, INTERVAL cxa.GarantiaDd DAY) DtGarantia'
      'FROM oscxa cxa'
      'LEFT JOIN oscab cab ON cab.Codigo=cxa.Codigo'
      ''
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante'
      'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat'
      ''
      'WHERE DATE_ADD(cab.DtaExeini, INTERVAL cxa.GarantiaDd DAY)'
      '  BETWEEN "2014-06-29" AND "2014-08-29"'
      ''
      'ORDER BY DtGarantia')
    Left = 380
    Top = 180
    object QrGarantNO_ENTICONTAT: TWideStringField
      FieldName = 'NO_ENTICONTAT'
      Size = 30
    end
    object QrGarantNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
    end
    object QrGarantNO_SiapTerCad: TWideStringField
      FieldName = 'NO_SiapTerCad'
      Size = 100
    end
    object QrGarantNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrGarantTel_ENT: TWideStringField
      FieldName = 'Tel_ENT'
    end
    object QrGarantNO_PAG: TWideStringField
      FieldName = 'NO_PAG'
      Size = 100
    end
    object QrGarantNO_CTR: TWideStringField
      FieldName = 'NO_CTR'
      Size = 100
    end
    object QrGarantCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGarantControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGarantNO_SERVICO: TWideStringField
      FieldName = 'NO_SERVICO'
      Size = 60
    end
    object QrGarantDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrGarantGarantiaDd: TIntegerField
      FieldName = 'GarantiaDd'
      Required = True
    end
    object QrGarantDtGarantia: TDateTimeField
      FieldName = 'DtGarantia'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrGarantTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 50
    end
    object QrGarantDiarGen: TFloatField
      FieldName = 'DiarGen'
    end
  end
  object DsGarant: TDataSource
    DataSet = QrGarant
    Left = 380
    Top = 228
  end
  object QrOSCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM oscab'
      'WHERE Codigo=0')
    Left = 172
    Top = 200
    object QrOSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrOSCabEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrOSCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
    end
    object QrOSCabEntContrat: TIntegerField
      FieldName = 'EntContrat'
    end
    object QrOSCabEntiContat: TIntegerField
      FieldName = 'EntiContat'
    end
    object QrOSCabSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrOSCabEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object QrOSCabFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrOSCabDtaContat: TDateTimeField
      FieldName = 'DtaContat'
    end
    object QrOSCabDtaVisPrv: TDateTimeField
      FieldName = 'DtaVisPrv'
    end
    object QrOSCabDtaVisExe: TDateTimeField
      FieldName = 'DtaVisExe'
    end
    object QrOSCabDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
    end
    object QrOSCabDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
    end
    object QrOSCabDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrOSCabDtaLibFat: TDateTimeField
      FieldName = 'DtaLibFat'
    end
    object QrOSCabDtaFimFat: TDateTimeField
      FieldName = 'DtaFimFat'
    end
    object QrOSCabValorServi: TFloatField
      FieldName = 'ValorServi'
    end
    object QrOSCabValorDesco: TFloatField
      FieldName = 'ValorDesco'
    end
    object QrOSCabValorOutrs: TFloatField
      FieldName = 'ValorOutrs'
    end
    object QrOSCabValorTotal: TFloatField
      FieldName = 'ValorTotal'
    end
    object QrOSCabDdsPosVda: TIntegerField
      FieldName = 'DdsPosVda'
    end
    object QrOSCabNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrOSCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrOSCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrOSCabSerNF: TWideStringField
      FieldName = 'SerNF'
      Size = 3
    end
    object QrOSCabNumNF: TIntegerField
      FieldName = 'NumNF'
    end
    object QrOSCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCabInvalServi: TFloatField
      FieldName = 'InvalServi'
    end
    object QrOSCabInvalDesco: TFloatField
      FieldName = 'InvalDesco'
    end
    object QrOSCabInvalOutrs: TFloatField
      FieldName = 'InvalOutrs'
    end
    object QrOSCabInvalTotal: TFloatField
      FieldName = 'InvalTotal'
    end
    object QrOSCabOrcamServi: TFloatField
      FieldName = 'OrcamServi'
    end
    object QrOSCabOrcamDesco: TFloatField
      FieldName = 'OrcamDesco'
    end
    object QrOSCabOrcamOutrs: TFloatField
      FieldName = 'OrcamOutrs'
    end
    object QrOSCabOrcamTotal: TFloatField
      FieldName = 'OrcamTotal'
    end
    object QrOSCabValiDdOrca: TIntegerField
      FieldName = 'ValiDdOrca'
    end
    object QrOSCabOperacao: TIntegerField
      FieldName = 'Operacao'
    end
    object QrOSCabExeTxtCli1: TIntegerField
      FieldName = 'ExeTxtCli1'
    end
    object QrOSCabValorPre: TFloatField
      FieldName = 'ValorPre'
    end
    object QrOSCabObsGaranti: TWideMemoField
      FieldName = 'ObsGaranti'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCabExeTxtCli2: TIntegerField
      FieldName = 'ExeTxtCli2'
    end
    object QrOSCabFimVisPrv: TDateTimeField
      FieldName = 'FimVisPrv'
    end
    object QrOSCabFimVisExe: TDateTimeField
      FieldName = 'FimVisExe'
    end
    object QrOSCabFimExePrv: TDateTimeField
      FieldName = 'FimExePrv'
    end
    object QrOSCabGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrOSCabNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrOSCabOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrOSCabOptado: TSmallintField
      FieldName = 'Optado'
    end
    object QrOSCabHowGerou: TSmallintField
      FieldName = 'HowGerou'
    end
    object QrOSCabPosGerou: TSmallintField
      FieldName = 'PosGerou'
    end
    object QrOSCabOSFlhUltGe: TIntegerField
      FieldName = 'OSFlhUltGe'
    end
    object QrOSCabMulServico: TLargeintField
      FieldName = 'MulServico'
    end
    object QrOSCabAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object QrOSCabOSFlhGrCab: TIntegerField
      FieldName = 'OSFlhGrCab'
    end
    object QrOSCabOSFlhGrIts: TIntegerField
      FieldName = 'OSFlhGrIts'
    end
    object QrOSCabSohInicial: TIntegerField
      FieldName = 'SohInicial'
    end
    object QrOSCabStPipAdPrg: TSmallintField
      FieldName = 'StPipAdPrg'
    end
    object QrOSCabLstUplWeb: TDateTimeField
      FieldName = 'LstUplWeb'
    end
    object QrOSCabLCPUsed: TIntegerField
      FieldName = 'LCPUsed'
    end
    object QrOSCabConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSCabObsExecuta: TWideMemoField
      FieldName = 'ObsExecuta'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCabDtIniMonGr: TDateField
      FieldName = 'DtIniMonGr'
    end
    object QrOSCabMobiliCad: TIntegerField
      FieldName = 'MobiliCad'
    end
  end
  object PMEncerra: TPopupMenu
    Left = 64
    Top = 516
  end
end
