object FmSiapImaCxa: TFmSiapImaCxa
  Left = 339
  Top = 185
  Caption = 'CAD-SUBCL-007 :: Cadastro de Caixa D`'#225'gua'
  ClientHeight = 533
  ClientWidth = 501
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 501
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 453
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 405
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 319
        Height = 32
        Caption = 'Cadastro de Caixa D`'#225'gua'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 319
        Height = 32
        Caption = 'Cadastro de Caixa D`'#225'gua'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 319
        Height = 32
        Caption = 'Cadastro de Caixa D`'#225'gua'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 501
    Height = 371
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 501
      Height = 371
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 501
        Height = 371
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 497
          Height = 66
          Align = alTop
          Caption = ' Dados Gerais: '
          TabOrder = 0
          object Label2: TLabel
            Left = 12
            Top = 20
            Width = 93
            Height = 13
            Caption = 'Local de aplica'#231#227'o:'
            Enabled = False
          end
          object Label3: TLabel
            Left = 420
            Top = 20
            Width = 42
            Height = 13
            Caption = 'Controle:'
            Enabled = False
          end
          object EdCodigo: TdmkEdit
            Left = 12
            Top = 36
            Width = 64
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNome: TdmkEdit
            Left = 80
            Top = 36
            Width = 337
            Height = 21
            Enabled = False
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdControle: TdmkEdit
            Left = 420
            Top = 36
            Width = 64
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 81
          Width = 497
          Height = 288
          Align = alClient
          Caption = ' Dados da caixa d`'#225'gua: '
          TabOrder = 1
          object Panel8: TPanel
            Left = 2
            Top = 15
            Width = 493
            Height = 271
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label16: TLabel
              Left = 8
              Top = 0
              Width = 82
              Height = 13
              Caption = 'Descri'#231#227'o (local):'
              FocusControl = EdLocal
            end
            object Label1: TLabel
              Left = 8
              Top = 40
              Width = 83
              Height = 13
              Caption = 'Material da caixa:'
            end
            object Label5: TLabel
              Left = 8
              Top = 80
              Width = 75
              Height = 13
              Caption = 'Forma da caixa:'
            end
            object SbObjeto: TSpeedButton
              Left = 460
              Top = 56
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbObjetoClick
            end
            object SbFinalidade: TSpeedButton
              Left = 460
              Top = 96
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbFinalidadeClick
            end
            object Label8: TLabel
              Left = 92
              Top = 120
              Width = 40
              Height = 13
              Caption = 'Medidas'
              FocusControl = EdMedidas
            end
            object Label9: TLabel
              Left = 8
              Top = 120
              Width = 69
              Height = 13
              Caption = 'Volume (Litros)'
            end
            object Label4: TLabel
              Left = 8
              Top = 160
              Width = 38
              Height = 13
              Caption = 'Acesso:'
            end
            object EdLocal: TdmkEdit
              Left = 8
              Top = 16
              Width = 473
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Local'
              UpdCampo = 'Local'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdMatersCxa: TdmkEditCB
              Left = 8
              Top = 56
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'MatersCxa'
              UpdCampo = 'MatersCxa'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMatersCxa
              IgnoraDBLookupComboBox = False
            end
            object CBMatersCxa: TdmkDBLookupComboBox
              Left = 64
              Top = 56
              Width = 392
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMatersCxa
              TabOrder = 2
              dmkEditCB = EdMatersCxa
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdFormasCxa: TdmkEditCB
              Left = 8
              Top = 96
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FormasCxa'
              UpdCampo = 'FormasCxa'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFormasCxa
              IgnoraDBLookupComboBox = False
            end
            object CBFormasCxa: TdmkDBLookupComboBox
              Left = 64
              Top = 96
              Width = 392
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsFormasCxa
              TabOrder = 4
              dmkEditCB = EdFormasCxa
              QryCampo = 'FormasCxa'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdMedidas: TdmkEdit
              Left = 92
              Top = 136
              Width = 389
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Medidas'
              UpdCampo = 'Medidas'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdVolumeL: TdmkEdit
              Left = 8
              Top = 136
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'VolumeL'
              UpdCampo = 'VolumeL'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object MeAcesso: TdmkMemo
              Left = 8
              Top = 176
              Width = 473
              Height = 89
              TabOrder = 7
              QryCampo = 'Acesso'
              UpdCampo = 'Acesso'
              UpdType = utYes
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 419
    Width = 501
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 497
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 463
    Width = 501
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 355
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 353
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrCxaMaters: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cxamaters'
      'ORDER BY Nome')
    Left = 96
    Top = 52
    object QrCxaMatersCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCxaMatersNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsMatersCxa: TDataSource
    DataSet = QrCxaMaters
    Left = 124
    Top = 52
  end
  object QrCxaFormas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cxaformas'
      'ORDER BY Nome')
    Left = 152
    Top = 52
    object QrCxaFormasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCxaFormasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFormasCxa: TDataSource
    DataSet = QrCxaFormas
    Left = 180
    Top = 52
  end
end
