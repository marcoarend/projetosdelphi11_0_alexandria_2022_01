unit SiapImaSVG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkMemo, Variants, UnDmkEnums, dmkRadioGroup;

type
  TFmSiapImaSVG = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaPrompt: TLabel;
    EdNome: TdmkEdit;
    RGRotarGraus: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodiAtual, FCtrlAtual: Integer;
    FQrySiapImaSVG: TmySQLQuery;
  end;

  var
  FmSiapImaSVG: TFmSiapImaSVG;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista, Principal;

{$R *.DFM}

procedure TFmSiapImaSVG.BtOKClick(Sender: TObject);
var
  Nome(*, NoArq, Arquivo*): String;
  Codigo, Controle, RotarGraus: Integer;
begin
  Codigo         := FCodiAtual;
  Controle       := FCtrlAtual;
  Nome           := EdNome.Text;
  //NoArq          := ;
  //Arquivo        := ;
  RotarGraus     := RGRotarGraus.ItemIndex;
  //
  Controle :=
    UMyMod.BPGS1I32('siapimasvg', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'siapimasvg', False, [
  'Codigo', 'Nome', (*'NoArq',
  'Arquivo',*) 'RotarGraus'], [
  'Controle'], [
  Codigo, Nome, (*NoArq,
  Arquivo,*) RotarGraus], [
  Controle], True) then
  begin
    if FQrySiapImaSVG <> nil then
    begin
      FQrySiapImaSVG.Close;
      FQrySiapImaSVG.Open;
      FQrySiapImaSVG.Locate('Controle', Controle, []);
    end;
    Close;
  end;
end;

procedure TFmSiapImaSVG.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSiapImaSVG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSiapImaSVG.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
