object FmOSMonCabIns: TFmOSMonCabIns
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-022 :: Ordem de Servi'#231'o - Multi-itens de Monitoramento'
  ClientHeight = 889
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 698
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Multi-itens de Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 698
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Multi-itens de Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 698
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Multi-itens de Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 802
    Width = 1241
    Height = 87
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 18
      Width = 177
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1060
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 59
    Width = 754
    Height = 743
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 689
      Width = 754
      Height = 54
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 0
      object Panel4: TPanel
        Left = 2
        Top = 18
        Width = 750
        Height = 34
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 16
          Top = 2
          Width = 150
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 15
          Top = 1
          Width = 150
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 754
      Height = 689
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 754
        Height = 689
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 0
          Top = 46
          Width = 754
          Height = 512
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 1
          object GBEdita: TGroupBox
            Left = 2
            Top = 18
            Width = 750
            Height = 151
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 0
            object Label1: TLabel
              Left = 10
              Top = 20
              Width = 16
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'ID:'
              Color = clBtnFace
              ParentColor = False
            end
            object Label9: TLabel
              Left = 10
              Top = 74
              Width = 91
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Descri'#231#227'o: [F4]'
              Color = clBtnFace
              ParentColor = False
            end
            object SbFormulas: TSpeedButton
              Left = 714
              Top = 39
              Width = 26
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SbFormulasClick
            end
            object Label3: TLabel
              Left = 84
              Top = 20
              Width = 134
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'F'#243'rmula de refer'#234'ncia:'
            end
            object Label10: TLabel
              Left = 556
              Top = 74
              Width = 85
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '*Per'#237'odo dias:'
            end
            object Label13: TLabel
              Left = 646
              Top = 74
              Width = 94
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Interv. p'#243'steros:'
            end
            object Label14: TLabel
              Left = 10
              Top = 127
              Width = 730
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              AutoSize = False
              Caption = 
                '*Per'#237'odo dias: Per'#237'odo de monitoramento v'#225'lido apenas para OSs N' +
                #195'O contratuais!'
              Font.Charset = ANSI_CHARSET
              Font.Color = clGreen
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              Visible = False
            end
            object EdConta: TdmkEdit
              Left = 10
              Top = 39
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Conta'
              UpdCampo = 'Conta'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdNome: TdmkEdit
              Left = 10
              Top = 94
              Width = 543
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Nome'
              UpdCampo = 'Nome'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnKeyDown = EdNomeKeyDown
            end
            object CBFormula: TdmkDBLookupComboBox
              Left = 153
              Top = 39
              Width = 560
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsFormulas
              TabOrder = 2
              OnExit = CBFormulaExit
              dmkEditCB = EdFormula
              QryCampo = 'Formula'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdFormula: TdmkEditCB
              Left = 84
              Top = 39
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Formula'
              UpdCampo = 'Formula'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdFormulaChange
              OnEnter = EdFormulaEnter
              OnExit = EdFormulaExit
              DBLookupComboBox = CBFormula
              IgnoraDBLookupComboBox = False
            end
            object EdPerioDd: TdmkEdit
              Left = 556
              Top = 94
              Width = 85
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'PerioDd'
              UpdCampo = 'PerioDd'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdDdPostero: TdmkEdit
              Left = 646
              Top = 94
              Width = 94
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'DdPostero'
              UpdCampo = 'DdPostero'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
          object RGDiluente: TdmkRadioGroup
            Left = 2
            Top = 169
            Width = 750
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Diluente: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              'N'#227'o tem'
              #193'gua'
              'Produto indicado')
            TabOrder = 1
            QryCampo = 'Diluente'
            UpdCampo = 'Diluente'
            UpdType = utYes
            OldValor = 0
          end
          object Panel8: TPanel
            Left = 2
            Top = 228
            Width = 750
            Height = 108
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            object SbEquipAplic: TSpeedButton
              Left = 714
              Top = 74
              Width = 26
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SbEquipAplicClick
            end
            object Label2: TLabel
              Left = 246
              Top = 54
              Width = 187
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Equipamento a ser monitorado:'
            end
            object Label8: TLabel
              Left = 128
              Top = 54
              Width = 104
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Quantidade QSP:'
            end
            object Label6: TLabel
              Left = 10
              Top = 54
              Width = 101
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Quantidade total:'
            end
            object SpeedButton5: TSpeedButton
              Left = 714
              Top = 18
              Width = 26
              Height = 28
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SpeedButton5Click
            end
            object Label12: TLabel
              Left = 399
              Top = 0
              Width = 149
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Unidade de Medida [F3]:'
            end
            object SBTipoAplica: TSpeedButton
              Left = 369
              Top = 20
              Width = 26
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SBTipoAplicaClick
            end
            object Label11: TLabel
              Left = 10
              Top = 0
              Width = 113
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Tipo de aplica'#231#227'o:'
            end
            object CBEquipAplic: TdmkDBLookupComboBox
              Left = 315
              Top = 74
              Width = 398
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Controle'
              ListField = 'Nome'
              ListSource = DsEquipAplic
              TabOrder = 8
              dmkEditCB = EdEquipAplic
              QryCampo = 'EquipAplic'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdEquipAplic: TdmkEditCB
              Left = 246
              Top = 74
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'EquipAplic'
              UpdCampo = 'EquipAplic'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdEquipAplicChange
              DBLookupComboBox = CBEquipAplic
              IgnoraDBLookupComboBox = False
            end
            object EdQtdQSP: TdmkEdit
              Left = 128
              Top = 74
              Width = 113
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              QryCampo = 'QtdQSP'
              UpdCampo = 'QtdQSP'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdQtdTot: TdmkEdit
              Left = 10
              Top = 74
              Width = 113
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              QryCampo = 'QtdTot'
              UpdCampo = 'QtdTot'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object CBUnidMed: TdmkDBLookupComboBox
              Left = 497
              Top = 20
              Width = 216
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsUnidMed
              TabOrder = 4
              dmkEditCB = EdUnidMed
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdSigla: TdmkEdit
              Left = 448
              Top = 20
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdSiglaChange
              OnExit = EdSiglaExit
            end
            object EdUnidMed: TdmkEditCB
              Left = 399
              Top = 20
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdUnidMedChange
              DBLookupComboBox = CBUnidMed
              IgnoraDBLookupComboBox = False
            end
            object CBTipoAplica: TdmkDBLookupComboBox
              Left = 79
              Top = 20
              Width = 287
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTipoAplica
              TabOrder = 1
              dmkEditCB = EdTipoAplica
              QryCampo = 'TipoAplica'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdTipoAplica: TdmkEditCB
              Left = 10
              Top = 20
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'TipoAplica'
              UpdCampo = 'TipoAplica'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBTipoAplica
              IgnoraDBLookupComboBox = False
            end
          end
          object Panel9: TPanel
            Left = 586
            Top = 336
            Width = 166
            Height = 174
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 3
            object BtPIPs: TBitBtn
              Tag = 511
              Left = 10
              Top = 113
              Width = 148
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&PMV'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtPIPsClick
            end
            object BtTudo: TBitBtn
              Tag = 127
              Left = 10
              Top = 5
              Width = 148
              Height = 49
              Hint = 'Marca todos itens'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Tudo'
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtTudoClick
            end
            object BtNenhum: TBitBtn
              Tag = 128
              Left = 10
              Top = 59
              Width = 148
              Height = 49
              Hint = 'Desmarca todos itens'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Nenhum'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtNenhumClick
            end
          end
          object Memo1: TMemo
            Left = 2
            Top = 336
            Width = 584
            Height = 174
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Alignment = taCenter
            BevelInner = bvNone
            BevelOuter = bvNone
            BorderStyle = bsNone
            Color = clGradientActiveCaption
            Font.Charset = ANSI_CHARSET
            Font.Color = clHotLight
            Font.Height = -15
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            Lines.Strings = (
              '    '
              '- Ser'#225' inserido uma f'#243'rmula de monitoramento e seus itens '
              'conforme "Itens da F'#243'rmula" na grade abaixo para cada PMV '
              'selecionado.'
              ''
              '- Para cadastrar m'#250'ltiplos PMVs, selecione a op'#231#227'o "Inclus'#227'o de '
              'multi PMVs" no menu suspenso do bot'#227'o "PMV".'
              ''
              '- Somente s'#227'o mostrados PMVs para sele'#231#227'o cujo equipamento '
              'coincida com o equipamento selecionado nesta janela.')
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 4
          end
        end
        object GBDados: TGroupBox
          Left = 0
          Top = 0
          Width = 754
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Label7: TLabel
            Left = 10
            Top = 15
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label4: TLabel
            Left = 286
            Top = 15
            Width = 49
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Servi'#231'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label5: TLabel
            Left = 133
            Top = 15
            Width = 65
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID Servi'#231'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object DBEdit6: TDBEdit
            Left = 340
            Top = 10
            Width = 39
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            DataField = 'DesServico'
            DataSource = DsOSSrv
            TabOrder = 1
          end
          object DBEdit5: TDBEdit
            Left = 379
            Top = 10
            Width = 335
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            DataField = 'NO_DesServico'
            DataSource = DsOSSrv
            TabOrder = 2
          end
          object DBEdit8: TDBEdit
            Left = 202
            Top = 10
            Width = 79
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            DataField = 'Controle'
            DataSource = DsOSSrv
            TabOrder = 3
          end
          object DBEdit15: TDBEdit
            Left = 59
            Top = 10
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsOSSrv
            TabOrder = 0
          end
        end
        object GroupBox3: TGroupBox
          Left = 0
          Top = 558
          Width = 754
          Height = 131
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Itens da F'#243'rmula: '
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 2
          object Label15: TLabel
            Left = 2
            Top = 18
            Width = 750
            Height = 18
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = 'Grade edit'#225'vel / Crtl + DEL para excluir'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitWidth = 298
          end
          object DBGrid1: TDBGrid
            Left = 2
            Top = 36
            Width = 750
            Height = 93
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Color = clWhite
            DataSource = DsOSMonRec
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGrid1CellClick
            OnColEnter = DBGrid1ColEnter
            OnColExit = DBGrid1ColExit
            OnDrawColumnCell = DBGrid1DrawColumnCell
            OnKeyDown = DBGrid1KeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'EhDiluente'
                Title.Caption = 'Dil'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ordem'
                Title.Caption = 'Seq.'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrvQtd'
                Title.Alignment = taRightJustify
                Title.Caption = 'Qtd de uso'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Sigla'
                Title.Caption = 'Unidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Alignment = taRightJustify
                Title.Caption = 'C'#243'd [F4]'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_GGX'
                Title.Caption = 'Descri'#231#227'o do Produto [F7]'
                Width = 280
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValCliDd'
                Title.Caption = 'Val. dias'
                Width = 44
                Visible = True
              end>
          end
        end
      end
    end
  end
  object Panel6: TPanel
    Left = 754
    Top = 59
    Width = 487
    Height = 743
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 487
      Height = 558
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = ' PMVs dispon'#237'veis para o equipamento selecionado: '
      TabOrder = 0
      object DBGPIPs: TdmkDBGridDAC
        Left = 2
        Top = 18
        Width = 483
        Height = 538
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        SQLFieldsToChange.Strings = (
          'Ativo')
        SQLIndexesOnUpdate.Strings = (
          'Codigo')
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'ok'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 268
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'ID'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsPIPs
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        EditForceNextYear = False
        Columns = <
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'ok'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 268
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'ID'
            Visible = True
          end>
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 558
      Width = 487
      Height = 185
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Caption = ' Per'#237'odos iniciais de monitoramento:'
      TabOrder = 1
      object LaServicoAviso: TLabel
        Left = 2
        Top = 18
        Width = 483
        Height = 18
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Caption = 'Grade edit'#225'vel / Crtl + DEL para excluir'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 298
      end
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 2
        Top = 36
        Width = 483
        Height = 147
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsDef_DMP
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        OptionsEx = [dgAllowTitleClick, dgAllowAppendAfterEof]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'Ordem'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Dias'
            Visible = True
          end>
      end
    end
  end
  object QrOSSrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT des.Nome NO_DesServico, srv.* '
      'FROM ossrv srv'
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico'
      'WHERE srv.Codigo=:P0')
    Left = 200
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSSrvNO_DesServico: TWideStringField
      FieldName = 'NO_DesServico'
      Size = 60
    end
    object QrOSSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSSrvDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrOSSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsOSSrv: TDataSource
    DataSet = QrOSSrv
    Left = 228
    Top = 80
  end
  object QrFormulas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frm.*, med.CodUsu CU_UNIDMED, med.SIGLA'
      'FROM formulas frm'
      'LEFT JOIN unidmed med ON med.Codigo=frm.UnidMed'
      'WHERE frm.Aplicacao & 2'
      'ORDER BY frm.Nome')
    Left = 464
    Top = 12
    object QrFormulasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFormulasEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrFormulasQtdTot: TFloatField
      FieldName = 'QtdTot'
    end
    object QrFormulasQtdQSP: TFloatField
      FieldName = 'QtdQSP'
    end
    object QrFormulasCusTot: TFloatField
      FieldName = 'CusTot'
    end
    object QrFormulasAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrFormulasDiluente: TSmallintField
      FieldName = 'Diluente'
    end
    object QrFormulasTipoAplica: TIntegerField
      FieldName = 'TipoAplica'
    end
    object QrFormulasCU_UNIDMED: TIntegerField
      FieldName = 'CU_UNIDMED'
      Required = True
    end
    object QrFormulasSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 492
    Top = 12
  end
  object QrEquipAplic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM equiaplic'
      'ORDER BY Nome')
    Left = 520
    Top = 12
    object QrEquipAplicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEquipAplicNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrEquipAplicNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object DsEquipAplic: TDataSource
    DataSet = QrEquipAplic
    Left = 548
    Top = 12
  end
  object QrPIPs: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM pipcad'
      'WHERE OSMonCab=0'
      'AND MotDesativ=0'
      'AND (DtaDesativ < "1900-01-01" '
      '  OR DtaDesativ IS NULL)'
      'ORDER BY Nome')
    Left = 780
    Top = 152
    object QrPIPsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPIPsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPIPsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsPIPs: TDataSource
    DataSet = QrPIPs
    Left = 808
    Top = 152
  end
  object QrTipoAplica: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tipoaplica'
      'ORDER BY Nome')
    Left = 464
    Top = 40
    object QrTipoAplicaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTipoAplicaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTipoAplica: TDataSource
    DataSet = QrTipoAplica
    Left = 492
    Top = 40
  end
  object QrUnidMed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 256
    Top = 80
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 284
    Top = 80
  end
  object PMPIPs: TPopupMenu
    OnPopup = PMPIPsPopup
    Left = 496
    Top = 476
    object InclusodePIPnico1: TMenuItem
      Caption = 'Incl&us'#227'o de PMV '#250'nico'
      OnClick = InclusodePIPnico1Click
    end
    object InclusodemultiPIPs1: TMenuItem
      Caption = 'Inclus'#227'o de &multi PMVs'
      OnClick = InclusodemultiPIPs1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object EditaPMVselecionado1: TMenuItem
      Caption = '&Edita PMV selecionado'
      OnClick = EditaPMVselecionado1Click
    end
  end
  object QrSel: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM _sel_pip'
      'WHERE Ativo=1'
      'ORDER BY Codigo')
    Left = 716
    Top = 212
    object QrSelCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSelNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrSelAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsOSMonRec: TDataSource
    DataSet = TbOSMonRec
    Left = 352
    Top = 540
  end
  object QrGGXs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      '/*WHERE gg1.Nivel2 IN (-3,-2)'
      'AND gg1.PrdGrupTip=-2*/'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 252
    Top = 576
    object QrGGXsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
    end
    object QrGGXsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gragrux.Controle'
    end
    object QrGGXsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Origin = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXsNO_MED: TWideStringField
      FieldName = 'NO_MED'
      Size = 30
    end
    object QrGGXsSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
  end
  object DsGGxs: TDataSource
    DataSet = QrGGXs
    Left = 280
    Top = 576
  end
  object TbOSMonRec: TmySQLTable
    Database = Dmod.MyDB
    SortFieldNames = 'Ordem'
    TableName = 'osmonrec'
    Left = 324
    Top = 540
    object TbOSMonRecNO_GGX: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_GGX'
      LookupDataSet = QrGGXs
      LookupKeyFields = 'Controle'
      LookupResultField = 'NO_PRD_TAM_COR'
      KeyFields = 'GraGruX'
      Size = 120
      Lookup = True
    end
    object TbOSMonRecCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osmonrec.Codigo'
    end
    object TbOSMonRecControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osmonrec.Controle'
    end
    object TbOSMonRecConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'osmonrec.Conta'
    end
    object TbOSMonRecIDIts: TIntegerField
      FieldName = 'IDIts'
      Origin = 'osmonrec.IDIts'
    end
    object TbOSMonRecGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'osmonrec.GraGruX'
    end
    object TbOSMonRecPrvQtd: TFloatField
      FieldName = 'PrvQtd'
      Origin = 'osmonrec.PrvQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object TbOSMonRecPrvPrc: TFloatField
      FieldName = 'PrvPrc'
      Origin = 'osmonrec.PrvPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSMonRecPrvVal: TFloatField
      FieldName = 'PrvVal'
      Origin = 'osmonrec.PrvVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSMonRecUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      Origin = 'osmonrec.UsoQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object TbOSMonRecUsoPrc: TFloatField
      FieldName = 'UsoPrc'
      Origin = 'osmonrec.UsoPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSMonRecUsoVal: TFloatField
      FieldName = 'UsoVal'
      Origin = 'osmonrec.UsoVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSMonRecUsoDec: TFloatField
      FieldName = 'UsoDec'
      Origin = 'osmonrec.UsoDec'
      DisplayFormat = '#,###,###,##0.00'
    end
    object TbOSMonRecUsoTot: TFloatField
      FieldName = 'UsoTot'
      Origin = 'osmonrec.UsoTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object TbOSMonRecOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'osmonrec.Ordem'
    end
    object TbOSMonRecReordem: TIntegerField
      FieldName = 'Reordem'
      Origin = 'osmonrec.Reordem'
    end
    object TbOSMonRecValCliDd: TIntegerField
      FieldName = 'ValCliDd'
      Origin = 'osmonrec.ValCliDd'
    end
    object TbOSMonRecEhDiluente: TSmallintField
      FieldName = 'EhDiluente'
    end
    object TbOSMonRecSigla: TWideStringField
      FieldKind = fkLookup
      FieldName = 'Sigla'
      LookupDataSet = QrGGXs
      LookupKeyFields = 'Controle'
      LookupResultField = 'Sigla'
      KeyFields = 'GraGruX'
      Size = 6
      Lookup = True
    end
  end
  object QrIts: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM _def_omr')
    Left = 380
    Top = 540
    object QrItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osmonrec.Codigo'
    end
    object QrItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osmonrec.Controle'
    end
    object QrItsConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'osmonrec.Conta'
    end
    object QrItsIDIts: TIntegerField
      FieldName = 'IDIts'
      Origin = 'osmonrec.IDIts'
    end
    object QrItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'osmonrec.GraGruX'
    end
    object QrItsPrvQtd: TFloatField
      FieldName = 'PrvQtd'
      Origin = 'osmonrec.PrvQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrItsPrvPrc: TFloatField
      FieldName = 'PrvPrc'
      Origin = 'osmonrec.PrvPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrItsPrvVal: TFloatField
      FieldName = 'PrvVal'
      Origin = 'osmonrec.PrvVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrItsUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      Origin = 'osmonrec.UsoQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrItsUsoPrc: TFloatField
      FieldName = 'UsoPrc'
      Origin = 'osmonrec.UsoPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrItsUsoVal: TFloatField
      FieldName = 'UsoVal'
      Origin = 'osmonrec.UsoVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrItsUsoDec: TFloatField
      FieldName = 'UsoDec'
      Origin = 'osmonrec.UsoDec'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrItsUsoTot: TFloatField
      FieldName = 'UsoTot'
      Origin = 'osmonrec.UsoTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'osmonrec.Ordem'
    end
    object QrItsReordem: TIntegerField
      FieldName = 'Reordem'
      Origin = 'osmonrec.Reordem'
    end
    object QrItsValCliDd: TIntegerField
      FieldName = 'ValCliDd'
      Origin = 'osmonrec.ValCliDd'
    end
    object QrItsEhDiluente: TSmallintField
      FieldName = 'EhDiluente'
    end
  end
  object TbDef_DMP: TmySQLTable
    Database = Dmod.MyDBn
    SortFieldNames = 'Ordem'
    TableName = '_def_dmp'
    Left = 672
    Top = 564
    object TbDef_DMPOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object TbDef_DMPDias: TIntegerField
      FieldName = 'Dias'
    end
  end
  object DsDef_DMP: TDataSource
    DataSet = TbDef_DMP
    Left = 740
    Top = 564
  end
  object QrDMP: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 812
    Top = 564
    object QrDMPOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrDMPDias: TIntegerField
      FieldName = 'Dias'
    end
  end
  object VUUnidMed: TdmkValUsu
    dmkEditCB = EdUnidMed
    Panel = Panel8
    QryCampo = 'CodUsu'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 651
    Top = 283
  end
end
