unit RMIP_R004;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Data.DB, mySQLDbTables, dmkGeral, UnDmkEnums, frxClass,
  frxDBSet, frxChart, UnProjGroup_Consts, UnInternalConsts, UnMyObjects,
  UnDmkProcFunc;

type
  TFmRMIP_R004 = class(TForm)
    Button1: TButton;
    Qr004A_PrgLstIts: TmySQLQuery;
    frxReport004A: TfrxReport;
    Qr004A_PrgLstItsPrgLstCab: TIntegerField;
    Qr004A_PrgLstItsPrgLstIts: TIntegerField;
    Qr004A_PrgLstItsNO_LST: TWideStringField;
    Qr004A_PrgLstItsSigla: TWideStringField;
    Qr004A_PrgLstItsNome: TWideStringField;
    Qr004A_PrgLstItsFuncoes: TSmallintField;
    frxDs004A_PrgLstIts: TfrxDBDataset;
    Qr004Atr: TmySQLQuery;
    Qr004AtrRespAtrIts: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure Qr004A_PrgLstItsAfterScroll(DataSet: TDataSet);
    procedure Qr004AtrAfterOpen(DataSet: TDataSet);
    procedure Qr004AtrAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    F_RMIP_004Atr: String;
    //
    procedure AbreQr004AtrXX(Qry: TmySQLQuery; RespAtrIts: Integer);
    function  ObtemNomeArr(Nome: String; Index: Integer): String;
    procedure Reopen004A_PrgLstIts();
    procedure R004_GeraDados(PrgLstIts, Cliente: Integer; DtaIni, DtaFim: TDateTime);
  public
    { Public declarations }
    FCliente: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    //
    procedure GeraImp_LinhaEvolucaoHistorico((*FmRMIP: TForm;*));
  end;

var
  FmRMIP_R004: TFmRMIP_R004;

implementation

uses Module, ModuleRMIP, DmkDAC_PF, ModuleGeral, CreateBugs;

const
  FNome_Qr004Atr    = 'Qr004Atr';
  FNome_frxDs004Atr = 'frxDs004Atr';
var
  FMaxQr004Atr: Integer = 0;
  FArrQr004Atr: array of TmySQLQuery;
  FfrxDs004Atr: array of TfrxDBDataSet;


{$R *.dfm}

procedure TFmRMIP_R004.AbreQr004AtrXX(Qry: TmySQLQuery; RespAtrIts: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(Qry, DmodG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + F_RMIP_004Atr,
  'WHERE RespAtrIts=' + Geral.FF0(RespAtrIts),
  'ORDER BY DtaExeFim ',
  '']);
end;

procedure TFmRMIP_R004.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  F_RMIP_004Atr := UnCreateBugs.RecriaTempTableNovo(ntrtt_RMIP_004Atr,
    DmodG.QrUpdPID1, False);
  //
  //  Recriar!
  FMaxQr004Atr := 4;
  SetLength(FArrQr004Atr, FMaxQr004Atr);
  SetLength(FfrxDs004Atr, FMaxQr004Atr);
  for I := 0 to FMaxQr004Atr - 1 do
  begin
    FArrQr004Atr[I] := TmySQLQuery.Create(Self);
    FArrQr004Atr[I].Name := ObtemNomeArr(FNome_Qr004Atr, I);
    FArrQr004Atr[I].Database := DMOdG.MyPID_DB;
    //
    FfrxDs004Atr[I] := TfrxDBDataSet.Create(Self);
    FfrxDs004Atr[I].Name := ObtemNomeArr(FNome_frxDs004Atr, I);
    FfrxDs004Atr[I].UserName := FfrxDs004Atr[I].Name;
    FfrxDs004Atr[I].DataSet := FArrQr004Atr[I];
  end;
  //
end;

procedure TFmRMIP_R004.GeraImp_LinhaEvolucaoHistorico();
const
  OSCab = 0;
  Sim = 1;
  //
var
  Aviso: String;
  I, PrgLstIts: Integer;
  Qryes: array of TmySQLQuery;
  DataSets: array of TfrxDataset;
  Chart1: TfrxChartView;
  //
begin
  Qr004A_PrgLstIts.Close;
  Aviso := ' ';
  Reopen004A_PrgLstIts();
  //
  if Qr004A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso :=
      'Question�rios dos monitoramentos pesquisados sem perguntas geradoras do gr�fico "' +
      CO_GeraGrafRelatPMV_0001_TXT_PizzaLastMonit + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Per�odo: ' + Geral.FDT(FDtaIni, 3) + ' a ' + Geral.FDT(FDtaFim, 2);
  //
(*
  Qr004A_PrgLstIts.First;
  while not Qr004A_PrgLstIts.Eof do
  begin
    PrgLstIts := Qr004A_PrgLstIts.FieldByName('PrgLstIts').AsInteger;
    R004_GeraDados(PrgLstIts, Cliente, DtaIni, DtaFim);
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qr004Atr, DmodG.MyPID_DB, [
    'SELECT DISTINCT RespAtrIts ',
    'FROM ' + F_RMIP_004Atr,
    'WHERE Respondido=' + Geral.FF0(Sim),
    '']);
    //
    Qr004A_PrgLstIts.Next;
  end;
*)

  frxReport004A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport004A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport004A.Variables['VARF_DATA']    := FDtaImp;
  frxReport004A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport004A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  SetLength(DataSets, FMaxQr004Atr + 1);
  for I := 0 to FMaxQr004Atr - 1 do
    DataSets[I] := FfrxDs004Atr[I];
  DataSets[FMaxQr004Atr + 0] := frxDs004A_PrgLstIts;
  MyObjects.frxDefineDataSets(frxReport004A, DataSets, False);
  //
  Chart1 := frxReport004A.FindObject('Chart1') as TfrxChartView;
  //Chart1.SeriesData.Clear;
  while Chart1.SeriesData.Count > 0 do
    Chart1.SeriesData[0].Free;
  //for I := 0 to FMaxQr004Atr - 1 do
  for I := 0 to Qr004Atr.RecordCount - 1 do
  begin
(*
          SeriesData = <
            item
              DataType = dtDBData
              DataSet = frxDs004Atr
              DataSetName = 'frxDs004Atr'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDs004Atr01."DtaExeFim"'
              Source2 = 'frxDs004Atr01."ITENS"'
              Source3 = 'frxDs004Atr01."Resposta"'
              Source4 = 'frxDs004Atr01."CorPizza"'
              XSource = 'frxDs004Atr01."DtaExeFim"'
              YSource = 'frxDs004Atr01."ITENS"'
            end
*)

(*
 TfrxChartView(Sender).AddSeries(TfrxChartSeries.csBar);
 TfrxChartView(Sender).Chart.Series[0].LegendTitle := '09/10/2013';
 TfrxChartView(Sender).Chart.Series[0].Marks.Style := smsValue;

 TfrxChartView(Sender).SeriesData[0].DataType := dtFixedData;
 TfrxChartView(Sender).SeriesData[0].XSource := 'value1; value2; value3';
 TfrxChartView(Sender).SeriesData[0].YSource := '31.5;28.54;31.58';
*)
    Chart1.AddSeries(TfrxChartSeries.csLine);
    Chart1.SeriesData[I].DataType := dtDBData;
    Chart1.SeriesData[I].DataSet := FfrxDs004Atr[I];
    //DataSetName := 'frxDs004Atr'
    Chart1.SeriesData[I].SortOrder := soNone;
    Chart1.SeriesData[I].TopN := 0;
    Chart1.SeriesData[I].XType := xtText;
    Chart1.SeriesData[I].Source1 := ObtemNomeArr(FNome_frxDs004Atr, I) + '."DtaExeFim"';
    Chart1.SeriesData[I].Source2 := ObtemNomeArr(FNome_frxDs004Atr, I) + '."ITENS"';
    Chart1.SeriesData[I].Source3 := ObtemNomeArr(FNome_frxDs004Atr, I) + '."Resposta"';
    //Chart1.SeriesData[I].Source4 := ObtemNomeArr(FNome_frxDs004Atr, I) + '."CorPizza"';
    Chart1.SeriesData[I].XSource := ObtemNomeArr(FNome_frxDs004Atr, I) + '."DtaExeFim"';
    Chart1.SeriesData[I].YSource := ObtemNomeArr(FNome_frxDs004Atr, I) + '."ITENS"';

    //Chart1.SeriesData[I].DataSet := FfrxDs004Atr[I];
    //SeriesData := TfrxSeriesData.Create(frxReport004A);
  end;
  //
(*
  // Source3 = Cores!
  if Funcoes in ([CO_PRG_LST_QUANTIT, CO_PRG_LST_BXAPROD, CO_PRG_LST_TXTLIVR]) then
    Chart1.SeriesData.Items[0].Source3 := ''
  else
    Chart1.SeriesData.Items[0].Source3 := 'frxDs003A_OSPipIts."CorPizza"';
*)
  //
  MyObjects.frxMostra(frxReport004A, CO_GeraGrafRelatPMV_0002_TXT_LinhaEvoluHistor);
(*
begin
  mySQLQuery1.Database := DMod.MyDB;
  mySQLQuery1.Open;
  ShowMessage(Geral.FF0(mySQLQuery1.Fields[0].AsInteger));
*)
end;

function TFmRMIP_R004.ObtemNomeArr(Nome: String; Index: Integer): String;
begin
  Result := Nome + FormatFloat('00', Index);
end;

procedure TFmRMIP_R004.Qr004AtrAfterOpen(DataSet: TDataSet);
var
  I: Integer;
begin
(*
  // Destruir caso existam!
  for I := 0 to FMaxQr004Atr - 1 do
  begin
    if FArrQr004Atr[I] <> nil then
      FArrQr004Atr[I].Free;
    //
    if FfrxDs004Atr[I] <> nil then
      FfrxDs004Atr[I].Free;
  end;

  //  Recriar!
  FMaxQr004Atr := Qr004Atr.RecordCount;
  SetLength(FArrQr004Atr, FMaxQr004Atr - 1);
  SetLength(FfrxDs004Atr, FMaxQr004Atr - 1);
  for I := 0 to FMaxQr004Atr - 1 do
  begin
    FArrQr004Atr[I] := TmySQLQuery.Create(DmRMIP);
    FArrQr004Atr[I].Name := ObtemNomeArr(FNome_Qr004Atr, I);
    FArrQr004Atr[I].Database := DMOdG.MyPID_DB;
    //
    FfrxDs004Atr[I] := TfrxDBDataSet.Create(DmRMIP);
    FfrxDs004Atr[I].Name := ObtemNomeArr(FNome_frxDs004Atr, I);
    FfrxDs004Atr[I].UserName := FfrxDs004Atr[I].Name;
    FfrxDs004Atr[I].DataSet := FArrQr004Atr[I];
  end;
  //
*)
  Geral.MB_Aviso(Geral.FF0(Qr004Atr.RecordCount));
  Qr004Atr.First;
  while not Qr004Atr.Eof do
  begin
    if Qr004Atr.RecNo <= FMaxQr004Atr then
      AbreQr004AtrXX(FArrQr004Atr[Qr004Atr.RecNo -1], Qr004AtrRespAtrIts.Value);
    Qr004Atr.Next;
  end;
end;

procedure TFmRMIP_R004.Qr004AtrAfterScroll(DataSet: TDataSet);
begin
  //AbreQr004AtrXX(FArrQr004Atr[Qr004Atr.RecNo -1], Qr004AtrRespAtrIts.Value);
end;

procedure TFmRMIP_R004.Qr004A_PrgLstItsAfterScroll(DataSet: TDataSet);
const
  Sim = 1;
var
  PrgLstIts: Integer;
begin
  PrgLstIts := Qr004A_PrgLstIts.FieldByName('PrgLstIts').AsInteger;
  R004_GeraDados(PrgLstIts, FCliente, FDtaIni, FDtaFim);
  //
  UnDMkDAC_PF.AbreMySQLQuery0(Qr004Atr, DmodG.MyPID_DB, [
  'SELECT DISTINCT RespAtrIts ',
  'FROM ' + F_RMIP_004Atr,
  'WHERE Respondido=' + Geral.FF0(Sim),
  '']);
end;

procedure TFmRMIP_R004.R004_GeraDados(PrgLstIts, Cliente: Integer; DtaIni,
  DtaFim: TDateTime);
var
  SQL_Cliente: String;
  //Qry: TmySQLQuery;
begin
  if Cliente <> 0 then
    SQL_Cliente := 'AND cab.Entidade=' + Geral.FF0(Cliente)
  else
    SQL_Cliente := '';
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DELETE FROM ' + F_RMIP_004Atr + '; ',
  'INSERT INTO ' + F_RMIP_004Atr,
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
  'IF(opi.RespAtrIts > 0, pat.Nome, pac.Padrao)) Resposta, ',
  //
  'IF(opi.Respondido=0, 8421504/*cinza*/, ',
  'IF(opi.RespAtrIts > 0, pat.CorPie, pac.CorPie)) + 0.000 CorPizza, ',
  //
  '1 Ativo ',
  //
  'FROM ' + TMeuDB + '.ospipits opi ',
  'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.prgatrcad pac ON pac.Codigo=opi.PrgLstCab ',
  'LEFT JOIN ' + TMeuDB + '.prgatrits pat ON pat.Controle=opi.RespAtrIts ',
  //
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True),
  SQL_Cliente,
  'GROUP BY opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, cab.DtaExeFim ',
  '']));
end;

procedure TFmRMIP_R004.Reopen004A_PrgLstIts;
var
  Abrangencia: TRelAbrange;
  Aplicacao: Integer;
begin
  Abrangencia := DmRMIP.DefineAbrangencia(FCliente);
  Aplicacao := CO_GeraGrafRelatPMV_0002_COD_LinhaEvoluHistor;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr004A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(Aplicacao),
  dmkPF.SQL_Periodo('AND cab.DtaExeFim', FDtaIni, FDtaFim, True, True),
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
  Geral.MB_Info(Qr004A_PrgLstIts.SQL.Text);
end;

end.
