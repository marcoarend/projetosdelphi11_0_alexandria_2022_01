object FmRMIP_R004: TFmRMIP_R004
  Left = 0
  Top = 0
  Caption = 'OK'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 8
    Top = 6
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
  end
  object Qr004A_PrgLstIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = Qr004A_PrgLstItsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts,  '
      'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , '
      'plp.Sigla, plp.Nome, pli.Funcoes '
      'FROM ospipits opi '
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo '
      'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab '
      'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts '
      'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta '
      'WHERE pli.Aplicacao & 2'
      'AND cab.DtaExeFim BETWEEN "2013-09-09" AND "2014-03-08 23:59:59"'
      'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ')
    Left = 196
    Top = 52
    object Qr004A_PrgLstItsPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object Qr004A_PrgLstItsPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
    object Qr004A_PrgLstItsNO_LST: TWideStringField
      FieldName = 'NO_LST'
      Size = 255
    end
    object Qr004A_PrgLstItsSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 100
    end
    object Qr004A_PrgLstItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Qr004A_PrgLstItsFuncoes: TSmallintField
      FieldName = 'Funcoes'
    end
  end
  object frxReport004A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 196
    Top = 4
    Datasets = <
      item
        DataSet = frxDs004A_PrgLstIts
        DataSetName = 'frxDs004A_PrgLstIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Height = 124.724463150000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 60.472480000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 41.574803150000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 83.149660000000000000
          Width = 680.315400000000000000
          Height = 41.574803150000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVISO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        Height = 313.700990000000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDs004A_PrgLstIts
        DataSetName = 'frxDs004A_PrgLstIts'
        KeepTogether = True
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_LST'
          DataSet = frxDs004A_PrgLstIts
          DataSetName = 'frxDs004A_PrgLstIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs004A_PrgLstIts."NO_LST"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Nome'
          DataSet = frxDs004A_PrgLstIts
          DataSetName = 'frxDs004A_PrgLstIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs004A_PrgLstIts."Nome"]')
          ParentFont = False
        end
        object Chart1: TfrxChartView
          Left = 3.779530000000000000
          Top = 41.574829999999990000
          Width = 668.976810000000000000
          Height = 268.346630000000000000
          ShowHint = False
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C6508124C6567656E642E4672616D652E436F6C6F720708636C5369
            6C766572164C6567656E642E4672616D652E536D616C6C446F747309154C6567
            656E642E536861646F772E56697369626C65080D4672616D652E56697369626C
            65080656696577334408165669657733444F7074696F6E732E526F746174696F
            6E02000B56696577334457616C6C73080A426576656C4F75746572070662764E
            6F6E6505436F6C6F720707636C576869746511436F6C6F7250616C6574746549
            6E646578020D0000}
          ChartElevation = 345
          SeriesData = <>
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 578.268090000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDs004A_PrgLstIts: TfrxDBDataset
    UserName = 'frxDs004A_PrgLstIts'
    CloseDataSource = False
    DataSet = Qr004A_PrgLstIts
    BCDToCurrency = False
    Left = 196
    Top = 100
  end
  object Qr004Atr: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = Qr004AtrAfterOpen
    AfterScroll = Qr004AtrAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT RespAtrIts '
      'FROM _rmip_004atr_'
      'WHERE Respondido = 1')
    Left = 196
    Top = 148
    object Qr004AtrRespAtrIts: TIntegerField
      FieldName = 'RespAtrIts'
    end
  end
end
