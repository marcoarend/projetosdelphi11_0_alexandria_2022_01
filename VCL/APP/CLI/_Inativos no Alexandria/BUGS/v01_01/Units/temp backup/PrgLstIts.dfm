object FmPrgLstIts: TFmPrgLstIts
  Left = 339
  Top = 185
  Caption = 'PRG-LISTA-002 :: Item de Lista de Pergunta'
  ClientHeight = 682
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 527
    Width = 1008
    Height = 41
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 12
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 929
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 1008
    Height = 415
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 584
      Top = 16
      Width = 46
      Height = 13
      Caption = 'Pergunta:'
    end
    object Label9: TLabel
      Left = 524
      Top = 16
      Width = 34
      Height = 13
      Caption = 'Ordem:'
    end
    object Label4: TLabel
      Left = 96
      Top = 16
      Width = 303
      Height = 13
      Caption = 'Filia'#231#227'o (s'#227'o mostrados itens pais com forma de gera'#231#227'o v'#225'lida):'
    end
    object SbPrgCadPrg: TSpeedButton
      Left = 980
      Top = 32
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbPrgCadPrgClick
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGFuncoes: TdmkRadioGroup
      Left = 12
      Top = 59
      Width = 435
      Height = 82
      Caption = ' Fun'#231#227'o desta pergunta: '
      Columns = 2
      Items.Strings = (
        '1'
        '2'
        '3. AppListas.sListaFuncaoPrgAtrCab'
        '4'
        '5')
      TabOrder = 6
      OnClick = RGFuncoesClick
      QryCampo = 'Funcoes'
      UpdCampo = 'Funcoes'
      UpdType = utYes
      OldValor = 0
    end
    object RGDependente: TdmkRadioGroup
      Left = 452
      Top = 59
      Width = 188
      Height = 82
      Caption = ' Depende da '#250'ltima resposta bin'#225'ria? '
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o'
        'Sim')
      TabOrder = 7
      QryCampo = 'Dependente'
      UpdCampo = 'Dependente'
      UpdType = utYes
      OldValor = 0
    end
    object EdOrdem: TdmkEdit
      Left = 524
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdFiliacao: TdmkEditCB
      Left = 96
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Filiacao'
      UpdCampo = 'Filiacao'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFiliacao
      IgnoraDBLookupComboBox = False
    end
    object CBFiliacao: TdmkDBLookupComboBox
      Left = 152
      Top = 32
      Width = 369
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PERGUNTA'
      ListSource = DsPrgLstPai
      TabOrder = 2
      dmkEditCB = EdFiliacao
      QryCampo = 'Filiacao'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object RGRelacao: TdmkRadioGroup
      Left = 646
      Top = 59
      Width = 354
      Height = 82
      Caption = ' Pergunta relacionada a: '
      Columns = 2
      Items.Strings = (
        'AppListas.sListaPrgLstItsRelacao')
      TabOrder = 8
      QryCampo = 'Relacao'
      UpdCampo = 'Relacao'
      UpdType = utYes
      OldValor = 0
    end
    object EdPergunta: TdmkEditCB
      Left = 584
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Pergunta'
      UpdCampo = 'Pergunta'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPergunta
      IgnoraDBLookupComboBox = False
    end
    object CBPergunta: TdmkDBLookupComboBox
      Left = 640
      Top = 32
      Width = 337
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPrgCadPrg
      TabOrder = 5
      dmkEditCB = EdPergunta
      QryCampo = 'Pergunta'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object GroupBox4: TGroupBox
      Left = 12
      Top = 144
      Width = 985
      Height = 65
      Caption = 'Gera'#231#227'o de descendentes: '
      TabOrder = 9
      object Label10: TLabel
        Left = 488
        Top = 36
        Width = 253
        Height = 13
        Caption = 'Quantidade de loops na impress'#227'o de monitoramento:'
      end
      object RGLupForma: TdmkRadioGroup
        Left = 8
        Top = 16
        Width = 473
        Height = 41
        Caption = ' Forma da gera'#231#227'o:'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o tem filhos'
          'Condicional (Bin'#225'rio)'
          'La'#231'o (Loop - quantitativo)')
        TabOrder = 0
        UpdType = utYes
        OldValor = 0
      end
      object EdLupQtdVzs: TdmkEdit
        Left = 776
        Top = 32
        Width = 33
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object PageControl1: TPageControl
      Left = 2
      Top = 217
      Width = 1004
      Height = 196
      ActivePage = TabSheet2
      Align = alBottom
      TabHeight = 25
      TabOrder = 10
      object TabSheet1: TTabSheet
        Caption = 'Dados espec'#237'ficos da fun'#231#227'o selecionada'
        object PCFuncao: TPageControl
          Left = 0
          Top = 0
          Width = 996
          Height = 161
          ActivePage = TabSheet6
          Align = alClient
          TabHeight = 25
          TabOrder = 0
          object TabSheet3: TTabSheet
            Caption = 'Bin'#225'rio'
            object GroupBox3: TGroupBox
              Left = 10
              Top = 10
              Width = 349
              Height = 61
              Caption = 'Textos da fun'#231#227'o "Bin'#225'rio" (quando houver):'
              TabOrder = 0
              object Label2: TLabel
                Left = 180
                Top = 16
                Width = 78
                Height = 13
                Caption = 'Texto afirmativo:'
              end
              object Label8: TLabel
                Left = 12
                Top = 16
                Width = 74
                Height = 13
                Caption = 'Texto negativo:'
              end
              object SbBinarCad0: TSpeedButton
                Left = 152
                Top = 32
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbBinarCad0Click
              end
              object SbBinarCad1: TSpeedButton
                Left = 320
                Top = 32
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbBinarCad1Click
              end
              object EdBinarCad0: TdmkEditCB
                Left = 12
                Top = 32
                Width = 32
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'BinarCad0'
                UpdCampo = 'BinarCad0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBBinarCad0
                IgnoraDBLookupComboBox = False
              end
              object CBBinarCad0: TdmkDBLookupComboBox
                Left = 44
                Top = 32
                Width = 108
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPrgBinCad0
                TabOrder = 1
                dmkEditCB = EdBinarCad0
                QryCampo = 'BinarCad0'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdBinarCad1: TdmkEditCB
                Left = 180
                Top = 32
                Width = 32
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'BinarCad1'
                UpdCampo = 'BinarCad1'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBBinarCad1
                IgnoraDBLookupComboBox = False
              end
              object CBBinarCad1: TdmkDBLookupComboBox
                Left = 212
                Top = 32
                Width = 108
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPrgBinCad1
                TabOrder = 3
                dmkEditCB = EdBinarCad1
                QryCampo = 'BinarCad1'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Quantitativo'
            ImageIndex = 3
            object RGCasasQtde: TdmkRadioGroup
              Left = 10
              Top = 10
              Width = 217
              Height = 45
              Caption = ' Casas decimais  para quantidade: '
              Columns = 4
              ItemIndex = 0
              Items.Strings = (
                '0'
                '1'
                '2'
                '3')
              TabOrder = 0
              QryCampo = 'CasasQtde'
              UpdCampo = 'CasasQtde'
              UpdType = utYes
              OldValor = 0
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'Adi/reti. Produto'
            ImageIndex = 1
            object RGAcaoPrd: TdmkRadioGroup
              Left = 10
              Top = 10
              Width = 593
              Height = 49
              Caption = ' Item obrigatorio para a fun'#231#227'o "Adi/reti. Produto": '
              Columns = 4
              ItemIndex = 0
              Items.Strings = (
                'N/I'
                'Adiciona'
                'Substitui'
                'Remove')
              TabOrder = 0
              QryCampo = 'Relacao'
              UpdCampo = 'Relacao'
              UpdType = utYes
              OldValor = 0
            end
          end
          object TabSheet5: TTabSheet
            Caption = 'Uso de Atributo'
            ImageIndex = 2
            object Label1: TLabel
              Left = 10
              Top = 10
              Width = 291
              Height = 13
              Caption = 'Tipo de Atributo (obrigat'#243'rio para a fun'#231#227'o "Uso de Atributo"):'
            end
            object SBPrgAtrCad: TSpeedButton
              Left = 582
              Top = 27
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SBPrgAtrCadClick
            end
            object EdPrgAtrCad: TdmkEditCB
              Left = 10
              Top = 27
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'PrgAtrCad'
              UpdCampo = 'PrgAtrCad'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBPrgAtrCad
              IgnoraDBLookupComboBox = False
            end
            object CBPrgAtrCad: TdmkDBLookupComboBox
              Left = 66
              Top = 27
              Width = 513
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsPrgAtrCad
              TabOrder = 1
              dmkEditCB = EdPrgAtrCad
              QryCampo = 'PrgAtrCad'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object TabSheet7: TTabSheet
            Caption = 'Texto Livre'
            ImageIndex = 4
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Outros dados'
        ImageIndex = 1
        object Label11: TLabel
          Left = 8
          Top = 84
          Width = 81
          Height = 13
          Caption = 'Cor gr'#225'fico pizza:'
        end
        object CBCorPie: TColorBox
          Left = 8
          Top = 101
          Width = 153
          Height = 22
          Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
          TabOrder = 0
        end
        object CGAplicacao: TdmkCheckGroup
          Left = 10
          Top = 10
          Width = 980
          Height = 65
          Caption = ' Gera'#231#227'o de gr'#225'ficos em relat'#243'rios: '
          Columns = 2
          Items.Strings = (
            'sListaGeraGrafRelatPMV....')
          TabOrder = 1
          QryCampo = 'Aplicacao'
          UpdCampo = 'Aplicacao'
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 312
        Height = 32
        Caption = 'Item de Lista de Pergunta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 312
        Height = 32
        Caption = 'Item de Lista de Pergunta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 312
        Height = 32
        Caption = 'Item de Lista de Pergunta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 568
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 612
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPrgAtrCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM prgatrcad'
      'ORDER BY Nome')
    Left = 4
    Top = 4
    object QrPrgAtrCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgAtrCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrgAtrCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPrgAtrCad: TDataSource
    DataSet = QrPrgAtrCad
    Left = 36
    Top = 4
  end
  object VUPrgAtrCad: TdmkValUsu
    dmkEditCB = CBBinarCad0
    QryCampo = 'PrgAtrCad'
    UpdCampo = 'PrgAtrCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 68
    Top = 4
  end
  object QrPrgLstPai: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome '
      'FROM prgatrits'
      'ORDER BY Nome')
    Left = 388
    Top = 16
    object QrPrgLstPaiControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPrgLstPaiNO_PERGUNTA: TWideStringField
      FieldName = 'NO_PERGUNTA'
      Size = 100
    end
  end
  object DsPrgLstPai: TDataSource
    DataSet = QrPrgLstPai
    Left = 416
    Top = 16
  end
  object QrPrgCadPrg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM prgcadprg'
      'ORDER BY Nome')
    Left = 500
    Top = 12
    object QrPrgCadPrgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgCadPrgNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPrgCadPrg: TDataSource
    DataSet = QrPrgCadPrg
    Left = 528
    Top = 12
  end
  object QrPrgBinCad0: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM prgbincad'
      'ORDER BY Nome')
    Left = 360
    Top = 68
    object QrPrgBinCad0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgBinCad0Nome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsPrgBinCad0: TDataSource
    DataSet = QrPrgBinCad0
    Left = 388
    Top = 68
  end
  object QrPrgBinCad1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM prgbincad'
      'ORDER BY Nome')
    Left = 464
    Top = 68
    object QrPrgBinCad1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgBinCad1Nome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsPrgBinCad1: TDataSource
    DataSet = QrPrgBinCad1
    Left = 492
    Top = 68
  end
end
