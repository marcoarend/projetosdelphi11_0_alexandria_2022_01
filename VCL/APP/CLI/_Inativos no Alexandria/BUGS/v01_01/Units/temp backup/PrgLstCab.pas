unit PrgLstCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, ComCtrls, UnAppListas, UnDmkEnums, UnProjGroup_Consts,
  dmkDBGridZTO, Variants;

type
  TFmPrgLstCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrPrgLstCab: TmySQLQuery;
    DsPrgLstCab: TDataSource;
    QrPrgLstIts: TmySQLQuery;
    DsPrgLstIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    N1: TMenuItem;
    Nomeiagrupo1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrPrgLstCabCodigo: TIntegerField;
    QrPrgLstCabNome: TWideStringField;
    QrPrgLstCabLk: TIntegerField;
    QrPrgLstCabDataCad: TDateField;
    QrPrgLstCabDataAlt: TDateField;
    QrPrgLstCabUserCad: TIntegerField;
    QrPrgLstCabUserAlt: TIntegerField;
    QrPrgLstCabAlterWeb: TSmallintField;
    QrPrgLstCabAtivo: TSmallintField;
    QrPrgLstItsCodigo: TIntegerField;
    QrPrgLstItsControle: TIntegerField;
    QrPrgLstItsNome: TWideStringField;
    QrPrgLstItsFuncoes: TSmallintField;
    QrPrgLstItsDependente: TSmallintField;
    QrPrgLstItsPrgAtrCad: TIntegerField;
    QrPrgLstItsLk: TIntegerField;
    QrPrgLstItsDataCad: TDateField;
    QrPrgLstItsDataAlt: TDateField;
    QrPrgLstItsUserCad: TIntegerField;
    QrPrgLstItsUserAlt: TIntegerField;
    QrPrgLstItsAlterWeb: TSmallintField;
    QrPrgLstItsAtivo: TSmallintField;
    QrPrgLstItsOrdem: TIntegerField;
    TVDados: TTreeView;
    QrPrgLstItsFiliacao: TIntegerField;
    QrPrgLstItsRelacao: TIntegerField;
    QrLoc: TmySQLQuery;
    QrLocCodigo: TIntegerField;
    QrLocControle: TIntegerField;
    QrLocFuncoes: TSmallintField;
    QrLocDependente: TSmallintField;
    QrLocPrgAtrCad: TIntegerField;
    QrLocOrdem: TIntegerField;
    QrLocLk: TIntegerField;
    QrLocDataCad: TDateField;
    QrLocDataAlt: TDateField;
    QrLocUserCad: TIntegerField;
    QrLocUserAlt: TIntegerField;
    QrLocAlterWeb: TSmallintField;
    QrLocAtivo: TSmallintField;
    QrLocFiliacao: TIntegerField;
    QrLocRelacao: TIntegerField;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label3: TLabel;
    EdControle: TdmkEdit;
    Label4: TLabel;
    EdNomeIts: TdmkEdit;
    N2: TMenuItem;
    Refresh1: TMenuItem;
    QrLocNivel: TIntegerField;
    QrLocNivSeq: TIntegerField;
    QrLocPergunta: TIntegerField;
    QrPrgLstItsPergunta: TIntegerField;
    QrPrgLstItsNO_PERGUNTA: TWideStringField;
    QrLocBinarCad0: TIntegerField;
    QrLocBinarCad1: TIntegerField;
    QrPrgLstItsAcaoPrd: TSmallintField;
    QrLocAcaoPrd: TSmallintField;
    QrLocNome: TWideStringField;
    QrLocBinario0: TWideStringField;
    QrLocBinario1: TWideStringField;
    QrLocLupForma: TSmallintField;
    QrLocLupQtdVzs: TSmallintField;
    QrLocAplicacao: TIntegerField;
    QrPrgLstItsCorPie: TIntegerField;
    QrPrgLstItsAplicacao: TIntegerField;
    QrLocCorPie: TIntegerField;
    EdOrdem: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdTitulo: TdmkEdit;
    QrPrgLstCabOrdem: TIntegerField;
    QrPrgLstCabTitulo: TWideStringField;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    Label8: TLabel;
    Label10: TLabel;
    QrPrgLstItsCasasQtde: TSmallintField;
    QrLocCasasQtde: TSmallintField;
    BtFxa: TBitBtn;
    PMFxa: TPopupMenu;
    IncluiFaixa1: TMenuItem;
    AlteraFaixa1: TMenuItem;
    ExcluiFaixa1: TMenuItem;
    QrPrgLstFxa: TmySQLQuery;
    QrPrgLstFxaCodigo: TIntegerField;
    QrPrgLstFxaControle: TIntegerField;
    QrPrgLstFxaConta: TIntegerField;
    QrPrgLstFxaQtdMax: TFloatField;
    DsPrgLstFxa: TDataSource;
    dmkDBGridZTO1: TdmkDBGridZTO;
    N3: TMenuItem;
    N4: TMenuItem;
    Adicionasubitem1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPrgLstCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrgLstCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrPrgLstCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrPrgLstItsAfterOpen(DataSet: TDataSet);
    procedure QrPrgLstItsBeforeClose(DataSet: TDataSet);
    procedure TVDadosChange(Sender: TObject; Node: TTreeNode);
    procedure TVDadosClick(Sender: TObject);
    procedure Refresh1Click(Sender: TObject);
    procedure IncluiFaixa1Click(Sender: TObject);
    procedure AlteraFaixa1Click(Sender: TObject);
    procedure ExcluiFaixa1Click(Sender: TObject);
    procedure EdControleChange(Sender: TObject);
    procedure QrPrgLstCabAfterClose(DataSet: TDataSet);
    procedure BtFxaClick(Sender: TObject);
    procedure PMFxaPopup(Sender: TObject);
    procedure Adicionasubitem1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraPrgLstIts(SQLType: TSQLType; Subitem: Boolean = False);
    procedure PreencheTreeView();
    procedure ReopenPrgLstFxa(Conta: Integer);
    procedure InsAltFxa(SQLType: TSQLType);
    procedure InfoItem();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    procedure ReopenPrgLstIts(Controle: Integer);
    procedure AtualizarNivSeq(Codigo: Integer);
    procedure LocCod(Atual, Codigo: Integer);
    procedure LocCodByControle(Controle: Integer);
  end;

var
  FmPrgLstCab: TFmPrgLstCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, PrgLstIts, DmkDAC_PF, MyGlyfs, GetValor;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPrgLstCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPrgLstCab.LocCodByControle(Controle: Integer);
var
  Codigo: Integer;
  Query: TmySQLQuery;
begin
  Query := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM prglstits ',
      'WHERE Controle = ' + Geral.FF0(COntrole),
      '']);
    if Query.RecordCount > 0 then
    begin
      Codigo := Query.FieldByName('Codigo').AsInteger;

      LocCod(Codigo, Codigo);
      ReopenPrgLstIts(Controle);
    end;
  finally
    Query.Free;
  end;
end;

procedure TFmPrgLstCab.MostraPrgLstIts(SQLType: TSQLType; Subitem: Boolean = False);
begin
  if (TVDados.Selected <> nil) and (TVDados.Selected.ImageIndex = 0)
  and (SQLType = stUpd) then
  begin
    Geral.MB_ERRO('Item n�o definido para altera��o!');
    Exit;
  end;
  if Subitem and (QrlocControle.Value = 0) then
  begin
    Geral.MB_Aviso('Para inserir um subitem primeiro voc� deve selecionar um item!');
    Exit;
  end;
  if DBCheck.CriaFm(TFmPrgLstIts, FmPrgLstIts, afmoNegarComAviso) then
  begin
    FmPrgLstIts.ImgTipo.SQLType := SQLType;
    FmPrgLstIts.FQrCab          := QrPrgLstCab;
    FmPrgLstIts.FDsCab          := DsPrgLstCab;
    FmPrgLstIts.FQrIts          := QrPrgLstIts;
    FmPrgLstIts.ReopenPrgLstPai(QrPrgLstCabCodigo.Value);
    //
    if SQLType = stIns then
    begin
      if QrPrgLstIts.Recordcount = 0 then
        FmPrgLstIts.EdOrdem.ValueVariant := 1
      else
        FmPrgLstIts.EdOrdem.ValueVariant := QrPrgLstItsOrdem.Value + 1;
      //
      FmPrgLstIts.RGFuncoes.ItemIndex := 0;
      //
      if Subitem then
      begin
        FmPrgLstIts.EdFiliacao.ValueVariant := QrlocControle.Value;
        FmPrgLstIts.CBFiliacao.KeyValue     := QrlocControle.Value;
      end;
      //
      FmPrgLstIts.ConfiguraFuncao(0);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
        'SELECT * ',
        'FROM prglstits ',
        'WHERE Controle=' + Geral.FF0(TVDados.Selected.ImageIndex),
        '']);
      FmPrgLstIts.EdControle.ValueVariant  := QrlocControle.Value;
      FmPrgLstIts.RGFuncoes.ItemIndex      := QrlocFuncoes.Value;
      FmPrgLstIts.RGDependente.ItemIndex   := QrlocDependente.Value;
      FmPrgLstIts.EdBinarCad0.ValueVariant := QrlocBinarCad0.Value;
      FmPrgLstIts.CBBinarCad0.KeyValue     := QrlocBinarCad0.Value;
      FmPrgLstIts.EdBinarCad1.ValueVariant := QrlocBinarCad1.Value;
      FmPrgLstIts.CBBinarCad1.KeyValue     := QrlocBinarCad1.Value;
      FmPrgLstIts.EdPrgAtrCad.ValueVariant := QrlocPrgAtrCad.Value;
      FmPrgLstIts.CBPrgAtrCad.KeyValue     := QrlocPrgAtrCad.Value;
      FmPrgLstIts.EdPergunta.ValueVariant  := QrlocPergunta.Value;
      FmPrgLstIts.CBPergunta.KeyValue      := QrlocPergunta.Value;
      FmPrgLstIts.EdOrdem.ValueVariant     := QrlocOrdem.Value;
      FmPrgLstIts.EdFiliacao.ValueVariant  := QrlocFiliacao.Value;
      FmPrgLstIts.RGRelacao.ItemIndex      := QrlocRelacao.Value;
      FmPrgLstIts.RGAcaoPrd.ItemIndex      := QrlocAcaoPrd.Value;
      FmPrgLstIts.RGLupForma.ItemIndex     := QrlocLupForma.Value;
      FmPrgLstIts.EdLupQtdVzs.ValueVariant := QrlocLupQtdVzs.Value;
      FmPrgLstIts.CGAplicacao.Value        := QrlocAplicacao.Value;
      FmPrgLstIts.CBCorPie.Selected        := QrlocCorPie.Value;
      FmPrgLstIts.RGCasasQtde.ItemIndex    := QrlocCasasQtde.Value;
      //
      FmPrgLstIts.ConfiguraFuncao(QrlocFuncoes.Value);
      //
      FmPrgLstIts.RGFuncoes.Enabled := not (
        (QrlocFuncoes.Value = CO_PRG_LST_BINARIO)
        and (TVDados.Selected.HasChildren));
      FmPrgLstIts.CkContinuar.Checked := False;
    end;
    FmPrgLstIts.ShowModal;
    FmPrgLstIts.Destroy;
  end;
end;

procedure TFmPrgLstCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrPrgLstCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrPrgLstCab, QrPrgLstIts);
end;

procedure TFmPrgLstCab.PMFxaPopup(Sender: TObject);
begin
  IncluiFaixa1.Enabled := EdControle.ValueVariant <> 0;
  MyObjects.HabilitaMenuItemItsUpd(AlteraFaixa1, QrPrgLstFxa);
  MyObjects.HabilitaMenuItemItsDel(ExcluiFaixa1, QrPrgLstFxa);
end;

procedure TFmPrgLstCab.PMItsPopup(Sender: TObject);

  function ValidaSubItem: Boolean;
  var
    Qry: TmySQLQuery;
  begin
    Result := False;
    Qry    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    //
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT pli.Controle, pcp.Nome NO_PERGUNTA',
        'FROM prglstits pli',
        'LEFT JOIN prgcadprg pcp ON pcp.Codigo=pli.Pergunta',
        'WHERE pli.Codigo=' + Geral.FF0(QrPrgLstCabCodigo.Value),
        'AND pli.Controle=' + Geral.FF0(TVDados.Selected.ImageIndex),
        'AND pli.LupForma > 0 ',
        'ORDER BY NO_PERGUNTA ',
        '']);
      Result := Qry.RecordCount > 0;
    finally
      Qry.Free;
    end;
  end;

begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrPrgLstCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrPrgLstIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrPrgLstIts);
  //
  ItsAltera1.Enabled := ItsAltera1.Enabled and (TVDados.Selected <> nil) and
    (TVDados.Selected.ImageIndex <> 0);
  ItsExclui1.Enabled := ItsExclui1.Enabled and (TVDados.Selected <> nil) and
    (TVDados.Selected.ImageIndex <> 0) and (TVDados.Selected.HasChildren = False);
  //
  if ItsAltera1.Enabled then
  begin
     Adicionasubitem1.Enabled := ValidaSubItem;
  end;
end;

procedure TFmPrgLstCab.PreencheTreeView();
(*
var
  Idx1, Idx2, Idx3, Idx4, Idx5: Integer;
*)
  function CriaNoh(const Noh: TTreeNode; const nN: String; const cN: Integer): TTreeNode;
  begin
    if Noh = nil then
      Result := TVDados.Items.Add(Noh, nN)
    else
      Result := TVDados.Items.AddChild(Noh, nN);
    Result.ImageIndex := cN;
    //Result.StateIndex := QrPrgLstItsFuncoes.Value + Integer(cCBNot);
  end;
var
  Pai: TTreeNode;
  nN: String;
  cN: Integer;
  Nos: array of TTreeNode;
  Idx: array of Integer;
  I, J: Integer;
begin
  {$WARNINGS OFF}
  TVDados.StateImages := FmMyGlyfs.ImgChkTV;
  TVDados.Items.Clear;
  SetLength(Nos, QrPrgLstIts.RecordCount);
  SetLength(Idx, QrPrgLstIts.RecordCount);
  QrPrgLstIts.First;
  while not QrPrgLstIts.Eof do
  begin
    I := QrPrgLstIts.RecNo - 1;
    nN := QrPrgLstItsNO_PERGUNTA.Value;
    cN := QrPrgLstItsControle.Value;
    Idx[I] := cN;
    if QrPrgLstItsFiliacao.Value = 0 then
      Nos[I] := CriaNoh( nil, nN, cN)
    else
    begin
      Pai := nil;
      for J := 0 to I -1 do
      begin
        if Idx[J] = QrPrgLstItsFiliacao.Value then
        begin
          Pai := Nos[J];
          Break;
        end;
      end;
      //
      Nos[I] := CriaNoh(Pai, nN, cN);
    end;
    QrPrgLstIts.Next;
  end;
  TVDados.FullExpand;
  {$WARNINGS ON}
end;

procedure TFmPrgLstCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrgLstCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPrgLstCab.DefParams;
begin
  VAR_GOTOTABELA := 'prglstcab';
  VAR_GOTOMYSQLTABLE := QrPrgLstCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM prglstcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmPrgLstCab.EdControleChange(Sender: TObject);
begin
  ReopenPrgLstFxa(0);
end;

procedure TFmPrgLstCab.ExcluiFaixa1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da faixa selecionada?',
  'prglstfxa', 'Conta', QrPrgLstFxaConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrPrgLstFxa,
      QrPrgLstFxaConta, QrPrgLstFxaConta.Value);
    ReopenPrgLstFxa(Conta);
  end;
end;

procedure TFmPrgLstCab.IncluiFaixa1Click(Sender: TObject);
var
  Continua: Boolean;
begin
  if QrPrgLstFxa.RecordCount > 0 then
    Continua := True
  else
    Continua := Geral.MB_Pergunta(
    'Informa��es v�lidas apenas para perguntas quantitativas!' + sLineBreak +
    'Informa��o n�o obrigat�ria!' + sLineBreak +
    'Deseja continuar assim mesmo?') = ID_YES;
  if Continua then
    InsAltFxa(stIns);
end;

procedure TFmPrgLstCab.InfoItem;
begin
  if TVDados.Selected <> nil then
  begin
    EdControle.ValueVariant := TVDados.Selected.ImageIndex;
    EdNomeIts.Text := TVDados.Selected.Text;
  end else
  begin
    EdControle.ValueVariant := 0;
    EdNomeIts.Text := '';
  end;
end;

procedure TFmPrgLstCab.InsAltFxa(SQLType: TSQLType);
var
  Cod: Variant;
var
  Codigo, Controle, Conta: Integer;
  QtdMax: Double;
begin
  Codigo         := QrPrgLstCabCodigo.Value;
  Controle       := EdControle.ValueVariant;
  if MyObjects.FIC(Controle = 0, nil, 'Selecione uma pergunta!') then
    Exit;
  if SQLType = stUpd then
  begin
    Conta        := QrPrgLstFxaConta.Value;
    QtdMax       := QrPrgLstFxaQtdMax.Value;
  end else
  begin
    Conta        := 0;
    QtdMax       := 0;
  end;
  //
(*
function TUnMyObjects.GetValorDmk(ComponentClass: TComponentClass;
  Reference: TComponent; FormatType: TAllFormat; Default: Variant; Casas,
  LeftZeros: Integer; ValMin, ValMax: String; Obrigatorio: Boolean; FormCaption,
  ValCaption: String; WidthVal: Integer; var Resultado: Variant): Boolean;
*)
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, QtdMax, 3, 0,
  '', '', True, 'Faixa de quantidade', 'Quantidade m�xima da faixa: ', 0, Cod) then
  begin
    QtdMax  := Cod;
    if MyObjects.FIC(QtdMax < 0.000, nil, 'Valor inv�lido!') then
      Exit;
(*
    if UMyMod.VerificaDuplicadoInt(Dmod.MyDB, 'prglstfxa', 'Codigo', 'Controle',
    'QtdMax', Codigo, Controle, QtdMax) then
      Exit;
*)
    if UMyMod.VerificaDuplicado1((*Database: TmySQLDatabase;*) Dmod.MyDB,
    (*Tabela: String;*)'prglstfxa', (*CamposFind: array of String;*)
    ['Controle', 'QtdMax'],  (*ValoresFind: array of Variant;*)
    [Controle, QtdMax], (*CampoOmit: String; ValorOmit: Variant)*) '', Null) then
      Exit;

    Conta := UMyMod.BPGS1I32('prglstfxa', 'Conta', '', '', tsPos, SQLType, Conta);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'prglstfxa', False, [
    'Codigo', 'Controle', 'QtdMax'], [
    'Conta'], [
    Codigo, Controle, QtdMax], [
    Conta], True) then
      ReopenPrgLstFxa(Conta);
  end;
end;

procedure TFmPrgLstCab.ItsAltera1Click(Sender: TObject);
begin
  MostraPrgLstIts(stUpd);
end;

procedure TFmPrgLstCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
  //  Arrumar itens depois!!!
  if QrPrgLstCabCodigo.Value <> 0 then
    AtualizarNivSeq(QrPrgLstCabCodigo.Value);
end;

procedure TFmPrgLstCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPrgLstCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPrgLstCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrPrgLstIts.Locate('Controle', TVDados.Selected.ImageIndex, []) then
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item descrito abaixo?' +
    sLineBreak + QrPrgLstItsNO_PERGUNTA.Value, 'PrgLstIts', 'Controle',
    QrPrgLstItsControle.Value, Dmod.MyDB) = ID_YES then
    begin
      // Locate > Controle ... Sem necessidade
      Controle := GOTOy.LocalizaPriorNextIntQr(QrPrgLstIts,
        QrPrgLstItsControle, QrPrgLstItsControle.Value);
      ReopenPrgLstIts(Controle);
    end;
  end;
end;

procedure TFmPrgLstCab.Refresh1Click(Sender: TObject);
begin
  if QrPrgLstCabCodigo.Value <> 0 then
    AtualizarNivSeq(QrPrgLstCabCodigo.Value);
end;

procedure TFmPrgLstCab.ReopenPrgLstIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrgLstIts, Dmod.MyDB, [
  'SELECT pcp.Nome NO_PERGUNTA, pli.*  ',
  'FROM prglstits pli ',
  'LEFT JOIN prgcadprg pcp ON pcp.Codigo=pli.Pergunta ',
  'WHERE pli.Codigo=' + Geral.FF0(QrPrgLstCabCodigo.Value),
  'ORDER BY pli.Ordem, pli.Controle ',
  '']);
  //
  if Controle > 0 then
    QrPrgLstIts.Locate('Controle', Controle, [])
  else
    QrPrgLstIts.Last;  // > para incluir novo registro na ordem sequencial!
end;


procedure TFmPrgLstCab.ReopenPrgLstFxa(Conta: Integer);
var
  Controle: Integer;
begin
  Controle := EdControle.ValueVariant;
  if Controle = 0 then
    QrPrgLstFxa.Close
  else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPrgLstFxa, Dmod.MyDB, [
    'SELECT * FROM prglstfxa',
    'WHERE Controle=' + Geral.FF0(Controle),
    'ORDER BY QtdMax ',
    '']);
    //
    QrPrgLstFxa.Locate('Conta', Conta, []);
  end;
end;

procedure TFmPrgLstCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPrgLstCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPrgLstCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPrgLstCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPrgLstCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPrgLstCab.TVDadosChange(Sender: TObject; Node: TTreeNode);
begin
  InfoItem();
end;

procedure TFmPrgLstCab.TVDadosClick(Sender: TObject);
begin
  EdControle.ValueVariant := TVDados.Selected.ImageIndex;
  EdNomeIts.Text := TVDados.Selected.Text;
end;

procedure TFmPrgLstCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrgLstCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO  := QrPrgLstCabCodigo.Value;

  if (QrPrgLstIts.State <> dsInactive) and (QrPrgLstIts.RecordCount > 0) then
    VAR_CADASTRO2 := QrPrgLstItsControle.Value;

  Close;
end;

procedure TFmPrgLstCab.ItsInclui1Click(Sender: TObject);
begin
  MostraPrgLstIts(stIns);
end;

procedure TFmPrgLstCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPrgLstCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'prglstcab');
end;

procedure TFmPrgLstCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, Ordem: Integer;
  Nome, Titulo: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Ordem  := EdOrdem.ValueVariant;
  Titulo := EdTitulo.Text;
  Codigo := UMyMod.BPGS1I32('prglstcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType,
    QrPrgLstCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'prglstcab',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmPrgLstCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'prglstcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'prglstcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPrgLstCab.BtFxaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFxa, BtFxa);
end;

procedure TFmPrgLstCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmPrgLstCab.Adicionasubitem1Click(Sender: TObject);
begin
  MostraPrgLstIts(stIns, True);
end;

procedure TFmPrgLstCab.AlteraFaixa1Click(Sender: TObject);
begin
  InsAltFxa(stUpd);
end;

procedure TFmPrgLstCab.AtualizarNivSeq(Codigo: Integer);
  procedure UpdNiv(Controle, Nivel, NivSeq: Integer);
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'prglstits', False, [
    'Nivel', 'NivSeq'], [
    'Controle'], [
    Nivel, NivSeq], [
    Controle], True);
  end;
  procedure AtualizaSubNiv(const Codigo, OriCtrl, Nivel: Integer; var NivSeq:
  Integer);
  var
    QryN: TmySQLQuery;
    Controle: Integer;
  begin
    QryN := TmySQLQuery.Create(Dmod);
    QryN.Name :=
      'Qr_PLI_Ctrl_' + Geral.FF0(OriCtrl) +
      '_Niv_' + Geral.FF0(Nivel) +
      '_NS_' + Geral.FF0(NivSeq);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QryN, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM prglstits ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Filiacao=' + Geral.FF0(OriCtrl),
      'ORDER BY Ordem ',
      '']);
      //
      QryN.First;
      QryN.First;
      while not QryN.Eof do
      begin
        NivSeq := NivSeq + 1;
        //
        Controle := QryN.FieldByName('Controle').AsInteger;
        UpdNiv(Controle, Nivel, NivSeq);
        //
        AtualizaSubNiv(Codigo, Controle, Nivel + 1, NivSeq);
        //
        QryN.Next;
      end;
      //
    finally
      QryN.Free;
    end;
  end;
var
  Qry0: TmySQLQuery;
  Controle, Nivel, NivSeq: Integer;
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Qry0 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry0, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM prglstits ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Filiacao=0 ',
      'ORDER BY Ordem ',
      '']);
      //
      NivSeq := 0;
      Qry0.First;
      while not Qry0.Eof do
      begin
        NivSeq := NivSeq + 1;
        //
        Controle := Qry0.FieldByName('Controle').AsInteger;
        Nivel    := 0;
        UpdNiv(Controle, Nivel, NivSeq);
        //
        AtualizaSubNiv(Codigo, Controle, Nivel + 1, NivSeq);
        //
        Qry0.Next;
      end;
    finally
      Qry0.Free;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmPrgLstCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmPrgLstCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  TVDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmPrgLstCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPrgLstCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPrgLstCab.SbImprimeClick(Sender: TObject);
const
  Arq = 'C:\Users\LATITUDE E6520\Desktop\Teste.txt';
var
  I: Integer;
  Lista: TStringList;
  Perguntas: String;
begin
  //Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
  //
  Perguntas := '';
  //
  TVDados.SaveToFile(Arq);
  Lista := TStringList.Create;
  try
    Lista.LoadFromFile(Arq);
    //
    for I := 0 to Lista.Count - 1 do
    begin
      Perguntas := Perguntas + Lista.Strings[I] + sLineBreak;
    end;
  finally
    Lista.Free;
  end;
  Geral.MB_Aviso(Perguntas);
end;

procedure TFmPrgLstCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPrgLstCab.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrPrgLstCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPrgLstCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPrgLstCab.QrPrgLstCabAfterClose(DataSet: TDataSet);
begin
  InfoItem();
end;

procedure TFmPrgLstCab.QrPrgLstCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPrgLstCab.QrPrgLstCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPrgLstIts(0);
  PreencheTreeView();
  InfoItem();
end;

procedure TFmPrgLstCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrPrgLstCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmPrgLstCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPrgLstCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'prglstcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPrgLstCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrgLstCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPrgLstCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'prglstcab');
end;

procedure TFmPrgLstCab.QrPrgLstCabBeforeOpen(DataSet: TDataSet);
begin
  QrPrgLstCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPrgLstCab.QrPrgLstItsAfterOpen(DataSet: TDataSet);
begin
  PreencheTreeView();
end;

procedure TFmPrgLstCab.QrPrgLstItsBeforeClose(DataSet: TDataSet);
begin
  TVDados.Items.Clear;
end;

end.

