unit ModuleRMIP;

interface

uses
  Forms,
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables, UnProjGroup_Consts,
  dmkGeral, UnMyObjects, frxClass, frxDBSet, UnDmkEnums, UnDmkProcFunc,
  UnInternalConsts;

type
  TDmRMIP = class(TDataModule)
    Qr004Atr: TmySQLQuery;
    Qr004AtrRespAtrIts: TIntegerField;
    Qr004Atr01: TmySQLQuery;
    frxDs004Atr: TfrxDBDataset;
    frxReport004A: TfrxReport;
    Qr004A_PrgLstIts: TmySQLQuery;
    Qr004A_PrgLstItsPrgLstCab: TIntegerField;
    Qr004A_PrgLstItsPrgLstIts: TIntegerField;
    Qr004A_PrgLstItsNO_LST: TWideStringField;
    Qr004A_PrgLstItsSigla: TWideStringField;
    Qr004A_PrgLstItsNome: TWideStringField;
    Qr004A_PrgLstItsFuncoes: TIntegerField;
    frxDs004A_PrgLstIts: TfrxDBDataset;
    Qr004Atr01ITENS: TIntegerField;
    Qr004Atr01Respondido: TSmallintField;
    Qr004Atr01RespAtrCad: TIntegerField;
    Qr004Atr01RespAtrIts: TIntegerField;
    Qr004Atr01DtaExeFim: TDateField;
    Qr004Atr01Resposta: TWideStringField;
    Qr004Atr01CorPizza: TIntegerField;
    Qr004Atr01Ativo: TSmallintField;
    Qr004Atr02: TmySQLQuery;
    Qr004Atr02ITENS: TIntegerField;
    Qr004Atr02Respondido: TSmallintField;
    Qr004Atr02RespAtrCad: TIntegerField;
    Qr004Atr02RespAtrIts: TIntegerField;
    Qr004Atr02DtaExeFim: TDateField;
    Qr004Atr02Resposta: TWideStringField;
    Qr004Atr02CorPizza: TIntegerField;
    Qr004Atr02Ativo: TSmallintField;
    frxDs004Atr01: TfrxDBDataset;
    frxDs004Atr02: TfrxDBDataset;
    procedure DataModuleCreate(Sender: TObject);
    procedure Qr004AtrAfterScroll(DataSet: TDataSet);
  private
    F_RMIP_004Atr: String;
    //
    procedure Reopen004A_OSPipIts(Localizador, PrgLstIts, Funcoes: Integer;
      DtaIni, DtaFim: TDateTime; Qr004A_OSPipIts: TmySQLQuery;
      Abrangencia: TRelAbrange; ItemRel: Integer);
    { Private declarations }
  public
    { Public declarations }
    procedure CriaTempTables();
    function  DefineAbrangencia(Cliente: Integer): TRelAbrange;

    ////////////////////////////////////////////////////////////////////////////

    procedure GeraImp_PizzaUltimaRevisaoPMV(Empresa, Cliente: Integer;
              Qr003A_PrgLstIts: TmySQLQuery; frxReport003A: TfrxReport;
              frxDatasets: array of TfrxDataset);
    procedure GeraImp_PizzaRevisaoPMVPeriodo(Empresa: Integer;
              Qr003A_PrgLstIts: TmySQLQuery; DtaIni, DtaFim: TDateTime;
              frxReport003A: TfrxReport; frxDatasets: array of TfrxDataset);
    procedure Reopen003A_OSPipIts(Localizador, PrgLstIts, Funcoes: Integer;
              DtaIni, DtaFim: TDateTime; Qr003A_OSPipIts, Qr003A_OSs: TmySQLQuery;
              Abrangencia: TRelAbrange; ItemRel: Integer);
    procedure Reopen003A_PrgLstIts(Qr003A_PrgLstIts: TmySQLQuery; Cliente,
              OSCab, Aplicacao: Integer; DtaIni, DtaFim: TDateTime; Abrangencia:
              TRelAbrange);
    ////////////////////////////////////////////////////////////////////////////
    ///
    procedure GeraImp_LinhaEvolucaoHistorico(FmRMIP: TForm; Empresa, Cliente:
              Integer; DtaIni, DtaFim: TDateTime);
    procedure Reopen004A_PrgLstIts(Qr004A_PrgLstIts: TmySQLQuery; Cliente,
              OSCab, Aplicacao: Integer; DtaIni, DtaFim: TDateTime; Abrangencia:
              TRelAbrange);
    procedure R004_GeraDados(PrgLstIts, Cliente: Integer; DtaIni, DtaFim: TDateTime);

    ////////////////////////////////////////////////////////////////////////////

  end;

var
  DmRMIP: TDmRMIP;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses Module, ModuleGeral, DmkDAC_PF, PrgLstIts, CreateBugs, RMIP_Cab;

const
  FMaxQr004Atr = 2;
var
  FArrQr004Atr: array[1..FMaxQr004Atr] of TmySQLQuery;


{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

(*
////////////////////////////////////////////////////////////////////////////////
Dedetiza��es Mae ativas

SELECT
DATE_ADD(cab.DtaExeFim, INTERVAL fcb.Periodd + 15 DAY) DataFinal,
cab.DtaExeFim, cab.Estatus, fcb.*
FROM osfrmflhcb fcb
LEFT JOIN osfrmcab frc ON fcb.Conta=frc.Conta
LEFT JOIN oscab cab ON fcb.Codigo=cab.Codigo
LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus
WHERE sta.Execucao <> 3
AND (
    cab.DtaExeFim<="1900-01-01"
    OR
    SYSDATE() <
      DATE_ADD(cab.DtaExeFim, INTERVAL fcb.Periodd + 15 DAY)
)
ORDER BY EmisUltDta, Codigo, IDIts

////////////////////////////////////////////////////////////////////////////////

Clientes de Dedetiza��es Mae ativas

SELECT DISTINCT cab.Entidade
FROM osfrmflhcb fcb
LEFT JOIN osfrmcab frc ON fcb.Conta=frc.Conta
LEFT JOIN oscab cab ON fcb.Codigo=cab.Codigo
LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus
WHERE sta.Execucao <> 3
AND (
    cab.DtaExeFim<="1900-01-01"
    OR
    SYSDATE() <
      DATE_ADD(cab.DtaExeFim, INTERVAL fcb.Periodd + 15 DAY)
)

////////////////////////////////////////////////////////////////////////////////
Monitoramento Mae ativas

SELECT
DATE_ADD(cab.DtaExeFim, INTERVAL mon.Periodd + 15 DAY) DataFinal,
cab.DtaExeFim, cab.Estatus, mon.*
FROM osmoncab mon
LEFT JOIN oscab cab ON mon.Codigo=cab.Codigo
LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus
WHERE sta.Execucao <> 3
AND (
    cab.DtaExeFim<="1900-01-01"
    OR
    SYSDATE() <
      DATE_ADD(cab.DtaExeFim, INTERVAL mon.Periodd + 15 DAY)
)

////////////////////////////////////////////////////////////////////////////////
Clientes de Monitoramento Mae ativas


SELECT DISTINCT cab.Entidade
FROM osmoncab mon
LEFT JOIN oscab cab ON mon.Codigo=cab.Codigo
LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus
WHERE sta.Execucao <> 3
AND (
    cab.DtaExeFim<="1900-01-01"
    OR
    SYSDATE() <
      DATE_ADD(cab.DtaExeFim, INTERVAL mon.Periodd + 15 DAY)
)


*)

{ TDmRMIP }

procedure TDmRMIP.CriaTempTables;
begin
  F_RMIP_004Atr := UnCreateBugs.RecriaTempTableNovo(ntrtt_RMIP_004Atr, DmodG.QrUpdPID1, False);
end;

procedure TDmRMIP.DataModuleCreate(Sender: TObject);
var
  I: Integer;
begin
  for I := 1 to FMaxQr004Atr do
  case I of
    1: FArrQr004Atr[I] := Qr004Atr01;
    2: FArrQr004Atr[I] := Qr004Atr02;
  end;
end;

function TDmRMIP.DefineAbrangencia(Cliente: Integer): TRelAbrange;
begin
  if Cliente <> 0 then
    Result := relAbrangeCliente
  else
    Result := relAbrangeEmpresa;
end;

procedure TDmRMIP.GeraImp_LinhaEvolucaoHistorico(FmRMIP: TForm; Empresa,
Cliente: Integer; DtaIni, DtaFim: TDateTime);
const
  OSCab = 0;
  Sim = 1;
  //
var
  Aviso: String;
  Abrangencia: TRelAbrange;
  I, PrgLstIts: Integer;
  Qryes: array of TmySQLQuery;
begin
  Qr004A_PrgLstIts.Close;
  Aviso := ' ';
  Abrangencia := DefineAbrangencia(Cliente);
  Reopen004A_PrgLstIts(Qr004A_PrgLstIts, Cliente, OSCab,
    CO_GeraGrafRelatPMV_0002_COD_LinhaEvoluHistor, DtaIni, DtaFim, Abrangencia);
  //
  if Qr004A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso :=
      'Question�rios dos monitoramentos pesquisados sem perguntas geradoras do gr�fico "' +
      CO_GeraGrafRelatPMV_0001_TXT_PizzaLastMonit + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Per�odo: ' + Geral.FDT(DtaIni, 3) + ' a ' + Geral.FDT(DtaFim, 2);
  //
  Qr004A_PrgLstIts.First;
  while not Qr004A_PrgLstIts.Eof do
  begin
    PrgLstIts := Qr004A_PrgLstIts.FieldByName('PrgLstIts').AsInteger;
    R004_GeraDados(PrgLstIts, Cliente, DtaIni, DtaFim);
    //
    //parei aqui multiplas series!
(*
    MyObjects.frxDefineDataSets(frxReport004A, frxDatasets);
    frxReport004A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
    MyObjects.frxMostra(frxReport004A, CO_GeraGrafRelatPMV_0002_TXT_LinhaEvoluHistor);
*)

    UnDMkDAC_PF.AbreMySQLQuery0(Qr004Atr, DmodG.MyPID_DB, [
    'SELECT DISTINCT RespAtrIts ',
    'FROM ' + F_RMIP_004Atr,
    'WHERE Respondido=' + Geral.FF0(Sim),
    '']);
    //
    Qr004A_PrgLstIts.Next;
  end;

  frxReport004A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
  frxReport004A.Variables['VARF_EMPRESA'] :=
    QuotedStr(TFmRMIP_Cab(FmRMIP).CBEmpresa.Text);
  frxReport004A.Variables['VARF_DATA'] :=
    TFmRMIP_Cab(FmRMIP).TPData.Date;
  frxReport004A.Variables['VARF_CLIENTE'] :=
    QuotedStr(dmkPF.ParValueCodTxt('', TFmRMIP_Cab(FmRMIP).CBCliente.Text,
    TFmRMIP_Cab(FmRMIP).EdCliente.ValueVariant, 'TODOS CLIENTES'));
  frxReport004A.Variables['VARF_TITULO'] :=
    QuotedStr(TFmRMIP_Cab(FmRMIP).QrListaNO_REL.Value);


  MyObjects.frxDefineDataSets(frxReport004A, [
    frxDs004Atr,
    frxDs004A_PrgLstIts,
    frxDs004Atr01,
    frxDs004Atr02
  ]);
  MyObjects.frxMostra(frxReport004A, CO_GeraGrafRelatPMV_0002_TXT_LinhaEvoluHistor);
end;

procedure TDmRMIP.GeraImp_PizzaRevisaoPMVPeriodo(Empresa: Integer;
              Qr003A_PrgLstIts: TmySQLQuery; DtaIni, DtaFim: TDateTime;
              frxReport003A: TfrxReport; frxDatasets: array of TfrxDataset);
const
  OSCab = 0;
  Cliente = 0;
  Abrangencia = relAbrangeEmpresa;
var
  Aviso: String;
begin
  Qr003A_PrgLstIts.Close;
  Aviso := ' ';
  Reopen003A_PrgLstIts(Qr003A_PrgLstIts, Cliente, OSCab,
    CO_GeraGrafRelatPMV_0001_COD_PizzaLastMonit, DtaIni, DtaFim, Abrangencia);
  //
  if Qr003A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso :=
      'Question�rios dos monitoramentos pesquisados sem perguntas geradoras do gr�fico "' +
      CO_GeraGrafRelatPMV_0001_TXT_PizzaLastMonit + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Per�odo: ' + Geral.FDT(DtaIni, 3) + ' a ' + Geral.FDT(DtaFim, 2);
  //
  MyObjects.frxDefineDataSets(frxReport003A, frxDatasets);
  frxReport003A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
  MyObjects.frxMostra(frxReport003A, CO_GeraGrafRelatPMV_0001_TXT_PizzaLastMonit);
end;

procedure TDmRMIP.GeraImp_PizzaUltimaRevisaoPMV(Empresa, Cliente: Integer;
              Qr003A_PrgLstIts: TmySQLQuery; frxReport003A: TfrxReport;
              frxDatasets: array of TfrxDataset);
const
  Abrangencia = relAbrangeCliente;
  DtaIni = 0;
  DtaFim = 0;
var
  Qry: TmySQLQuery;
  OSCab: Integer;
  Aviso: String;
begin
  Qr003A_PrgLstIts.Close;
  OSCab := 0;
  Aviso := ' ';
// Ultimo monitormento finalizado do cliente selecionado!
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT cab.Codigo, cab.DtaExeFim ',
    'FROM oscab cab ',
    'LEFT JOIN ospipits opi ON opi.Codigo=cab.Codigo ',
    'WHERE cab.Empresa=' + Geral.FF0(Empresa),
    'AND cab.Entidade=' + Geral.FF0(Cliente),
    'AND cab.DtaExeFim > "1900-01-01" ',
    'AND cab.Operacao & ' + Geral.FF0(CO_OS_OPERACAO_SERVICO_MONITORAMENTO),
    'AND opi.Codigo IS NOT NULL ',
    'ORDER BY cab.DtaExeFim DESC ',
    'LIMIT 1 ',
    '']);
    //
    OSCab := Qry.FieldByName('Codigo').AsInteger;
  finally
    Qry.Free;
  end;
  //
  if OSCab = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Cliente sem monitoramento respondido e finalizado!';
  end;
  Reopen003A_PrgLstIts(Qr003A_PrgLstIts, Cliente, OSCab,
    CO_GeraGrafRelatPMV_0001_COD_PizzaLastMonit, DtaIni, DtaFim, Abrangencia);
  //
  if Qr003A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Question�rios do monitoramento do localizador ' +
      Geral.FF0(OSCab) + ' sem perguntas geradoras do gr�fico "' +
      CO_GeraGrafRelatPMV_0001_TXT_PizzaLastMonit + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Localizador: ' + Geral.FF0(OSCab);
  MyObjects.frxDefineDataSets(frxReport003A, frxDatasets);
  frxReport003A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
  MyObjects.frxMostra(frxReport003A, CO_GeraGrafRelatPMV_0001_TXT_PizzaLastMonit);
end;

procedure TDmRMIP.Qr004AtrAfterScroll(DataSet: TDataSet);
  procedure AbreQr004AtrXX(Qry: TmySQLQuery; RespAtrIts: Integer);
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, DmodG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + F_RMIP_004Atr,
    'WHERE RespAtrIts=' + Geral.FF0(RespAtrIts),
    'ORDER BY DtaExeFim ',
    '']);
  end;
begin
(*
    Qr004Atr.First;
    while not Qr004Atr.Eof do
    begin
*)
      if Qr004Atr.RecNo <= FMaxQr004Atr then
        AbreQr004AtrXX(FArrQr004Atr[Qr004Atr.RecNo], Qr004AtrRespAtrIts.Value)
(*
      else
        Qr004Atr.Last;
      //
      Qr004Atr.Next;
    end;
    for I := Qr004Atr.RecordCount + 1 to FMaxQr004Atr do
      AbreQr004AtrXX(FArrQr004Atr[I], -1);
    //
*)
end;

procedure TDmRMIP.R004_GeraDados(PrgLstIts, Cliente: Integer; DtaIni, DtaFim: TDateTime);
var
  SQL_Cliente: String;
  //Qry: TmySQLQuery;
begin
  if Cliente <> 0 then
    SQL_Cliente := 'AND cab.Entidade=' + Geral.FF0(Cliente)
  else
    SQL_Cliente := '';
(*
  case Funcoes of
    CO_PRG_LST_NENHUMA: // 0;
    CO_PRG_LST_BINARIO: // 1
    begin
    end;
    CO_PRG_LST_QUANTIT: // 2
    begin
    end;
    CO_PRG_LST_BXAPROD: // 3
    begin
    end;
    CO_PRG_LST_ATRIBUT: // 4
    begin
    end;
    CO_PRG_LST_TXTLIVR: // 5
    begin
    end;
    //
    else
    begin
      Geral.MB_Erro(
      '"Funcoes" nao implementado em "TDmRMIP.Reopen003A_OSPipIts()"' +
      sLineBreak +
      'Funcoes = ' + Geral.FF0(Funcoes));
      EXIT;
    end;
  end;
*)
      UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
      'DELETE FROM ' + F_RMIP_004Atr + '; ',
      'INSERT INTO ' + F_RMIP_004Atr,
      'SELECT COUNT(opi.RespAtrIts) ITENS, ',
      'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
      'cab.DtaExeFim, ',
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'IF(opi.RespAtrIts > 0, pat.Nome, pac.Padrao)) Resposta, ',
      //
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      'IF(opi.RespAtrIts > 0, pat.CorPie, pac.CorPie)) + 0.000 CorPizza, ',
      //
      '1 Ativo ',
      //
      'FROM ' + TMeuDB + '.ospipits opi ',
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.prgatrcad pac ON pac.Codigo=opi.PrgLstCab ',
      'LEFT JOIN ' + TMeuDB + '.prgatrits pat ON pat.Controle=opi.RespAtrIts ',
      //
      'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
      dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True),
      SQL_Cliente,
      'GROUP BY opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, cab.DtaExeFim ',
      '']));
  //

(*
  if (Abrangencia = relAbrangeCliente)
  and (TItensRMIP(ItemRel) = rmipPizzaLastMonit) then
    SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(Localizador)
  else //relAbrangeEmpresa
    SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_OSPipIts, Dmod.MyDB, [
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  Resposta,
  CorPizza,
  LeftJoin,
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  GroupBy,
  OrderBy,
  '']);
  Geral.MB_Aviso(Qr003A_OSPipIts.SQL.Text);
  //
*)
  // Qry: TmySQLQuery;
  //SELECT DISTINCT dtaexefim from _rmip_004atr_
  //

end;

procedure TDmRMIP.Reopen003A_OSPipIts(Localizador, PrgLstIts, Funcoes: Integer;
  DtaIni, DtaFim: TDateTime; Qr003A_OSPipIts, Qr003A_OSs: TmySQLQuery;
  Abrangencia: TRelAbrange; ItemRel: Integer);
var
  Resposta, CorPizza, LeftJoin, OrderBy, GroupBy, SQL_Extra, Xtra_Group: String;
begin
  case TItensRMIP(ItemRel) of
    rmipLinhaEvoluHistori: Xtra_Group := '';
    rmipPizzaLastMonit: Xtra_Group := ', DtaExeFim';
    else Xtra_Group := ', ???';
  end;
  case Funcoes of
    //CO_PRG_LST_NENHUMA = 0;
    CO_PRG_LST_BINARIO: // 1
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'IF(opi.RespBin = 0, pb0.Nome,',
      'IF(opi.RespBin = 1, pb1.Nome,',
      'IF(opi.RespBin = 2, "N�O SE APLICA", "? ? ?")))) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      'IF(opi.RespBin = 0, 5324774/*vermelho excel 2013*/,',
      'IF(opi.RespBin = 1, 4243590/*verde excel 2013*/,',
      'IF(opi.RespBin = 2, 47871/*Amarelo excel 2013*/, ',
      '7686045/*Roxo excel 2013*/)))) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ospipits opi ',
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN prgbincad pb0 ON pb0.Codigo=opi.BinarCad0 ',
      'LEFT JOIN prgbincad pb1 ON pb1.Codigo=opi.BinarCad1 ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, opi.RespBin ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_QUANTIT: // 2
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'LPAD(FORMAT(RespQtd, pli.CasasQtde), 14, " ")) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ospipits opi ',
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts',
       '']);
      GroupBy := 'GROUP BY opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY Resposta DESC ';
    end;
    CO_PRG_LST_BXAPROD: // 3
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO",  ',
      'gg1.Nome) Resposta,  ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ospipitspr opr ',
      'LEFT JOIN ospipits opi ON opr.Conta=opi.Conta ',
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN gragrux ggx ON opr.GraGruX=ggx.Controle ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_ATRIBUT: // 4
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'IF(opi.RespAtrIts > 0, pat.Nome, pac.Padrao)) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      'IF(opi.RespAtrIts > 0, pat.CorPie, pac.CorPie)) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ospipits opi ',
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN prgatrcad pac ON pac.Codigo=opi.PrgLstCab ',
      'LEFT JOIN prgatrits pat ON pat.Controle=opi.RespAtrIts ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, opi.RespAtrCad, opi.RespAtrIts ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_TXTLIVR: // 5
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO",  ',
      'IF(LEFT(RespTxtLvr, 512) = "", "RESPOSTA EM BRANCO", ',
      'LEFT(RespTxtLvr, 512))) Resposta,  ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ospipits opi ',
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    //
    else
    begin
      Geral.MB_Erro(
      '"Funcoes" nao implementado em "TDmRMIP.Reopen003A_OSPipIts()"' +
      sLineBreak +
      'Funcoes = ' + Geral.FF0(Funcoes));
      EXIT;
    end;
  end;
  //

  if (Abrangencia = relAbrangeCliente)
  and (TItensRMIP(ItemRel) = rmipPizzaLastMonit) then
    SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(Localizador)
  else (*relAbrangeEmpresa*)
    SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_OSPipIts, Dmod.MyDB, [
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  Resposta,
  CorPizza,
  LeftJoin,
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  GroupBy,
  OrderBy,
  '']);
  Geral.MB_Aviso(Qr003A_OSPipIts.SQL.Text);
  //


  // OSs afetadas:
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_OSs, Dmod.MyDB, [
  'SELECT DISTINCT cab.Codigo, cab.Entidade, cab.SiapTerCad, ',
  'cab.Estatus, cab.DtaExeFim, cab.NumContrat, cab.Grupo, ',
  'fge.Nome NO_FatoGeradr, ',
  'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  LeftJoin,
  'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
  'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
  'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  'ORDER BY cab.DtaExeFim',
  '']);
end;

(*
procedure TDmRMIP.Reopen003A_PrgLstIts*Antigo*(Qr003A_PrgLstIts: TmySQLQuery; OSCab,
  Aplicacao: Integer; DtaIni, DtaFim: TDateTime; Abrangencia: TRelAbrange);
var
  SQL_Codigo, SQL_Extra: String;
begin
  case Abrangencia of
    relAbrangeCliente:
    begin
      SQL_Codigo := 'opi.Codigo + 0.000';
      SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(OSCab);
    end;
    relAbrangeEmpresa:
    begin
      SQL_Codigo := '0.000';
      SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True);
    end
    else
    begin
      SQL_Codigo := '???';
      SQL_Extra := '? ? ?';
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT ' + SQL_Codigo + ' Codigo, opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(CO_GeraGrafRelatPMV_0001_COD_PizzaLastMonit),
  SQL_Extra,
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
end;
*)

procedure TDmRMIP.Reopen003A_PrgLstIts(Qr003A_PrgLstIts: TmySQLQuery; Cliente, OSCab,
  Aplicacao: Integer; DtaIni, DtaFim: TDateTime; Abrangencia: TRelAbrange);
var
  SQL_Codigo, SQL_Extra, SQL_DtExe: String;
begin
  case Abrangencia of
    relAbrangeCliente:
    begin
      (*
      case Aplicacao of
        CO_GeraGrafRelatPMV_0001_COD_PizzaLastMonit:
        begin
        *)
          SQL_Codigo := 'opi.Codigo + 0.000';
          SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(OSCab);
          SQL_DtExe := '0 DtaExeFim, ';
        (*
        end;
        CO_GeraGrafRelatPMV_0002_COD_LinhaEvoluHistor:
        begin
          SQL_Codigo := '0.000';
          SQL_Extra := 'AND cab.Entidade=' + Geral.FF0(Cliente);
          SQL_DtExe := '0 DtaExeFim, ';
        end
        else
        begin
          SQL_Codigo := '0.000';
          SQL_Extra := 'AND opi.????=????';
          SQL_DtExe := '??? DtaExeFim, ';
        end;
      end;
      *)
    end;
    relAbrangeEmpresa:
    begin
      SQL_Codigo := '0.000';
      SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True);
      //SQL_DtExe := 'DtaExeFim DtaExeFim, ';
      SQL_DtExe := '0 DtaExeFim, ';
    end
    else
    begin
      SQL_Codigo := '???';
      SQL_Extra := '? ? ?';
      SQL_DtExe := '??? DtaExeFim, ';
    end;
  end;
(*
  case Aplicacao of
    CO_GeraGrafRelatPMV_0001_COD_PizzaLastMonit:
      SQL_DtExe := '0 DtaExeFim, ';
    CO_GeraGrafRelatPMV_0002_COD_LinhaEvoluHistor:
      SQL_DtExe := 'DtaExeFim DtaExeFim, ';
    else
      SQL_DtExe := '??? DtaExeFim, ';
  end;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT ' +
  SQL_DtExe,
  SQL_Codigo + ' Codigo, opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(Aplicacao),
  SQL_Extra,
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
end;

procedure TDmRMIP.Reopen004A_OSPipIts(Localizador, PrgLstIts, Funcoes: Integer;
  DtaIni, DtaFim: TDateTime; Qr004A_OSPipIts: TmySQLQuery;
  Abrangencia: TRelAbrange; ItemRel: Integer);
begin
//
end;

procedure TDmRMIP.Reopen004A_PrgLstIts(Qr004A_PrgLstIts: TmySQLQuery; Cliente,
  OSCab, Aplicacao: Integer; DtaIni, DtaFim: TDateTime;
  Abrangencia: TRelAbrange);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr004A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(Aplicacao),
  dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True),
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
  Geral.MB_Aviso(Qr004A_PrgLstIts.SQL.Text);
end;

end.
