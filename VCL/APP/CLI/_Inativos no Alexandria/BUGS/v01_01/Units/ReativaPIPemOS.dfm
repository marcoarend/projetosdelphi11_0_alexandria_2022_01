object FmReativaPIPemOS: TFmReativaPIPemOS
  Left = 339
  Top = 185
  Caption = 'PIP-RTVOS-001 :: Reativar PMV em OS'
  ClientHeight = 629
  ClientWidth = 859
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 859
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 811
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 763
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 242
        Height = 32
        Caption = 'Reativar PMV em OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 242
        Height = 32
        Caption = 'Reativar PMV em OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 242
        Height = 32
        Caption = 'Reativar PMV em OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 859
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 859
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 859
        Height = 467
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 812
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 855
          Height = 450
          Align = alClient
          DataSource = DsGgxPip
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IDIts'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tabela'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaExeIni'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 859
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 855
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 812
        Height = 16
        Caption = 
          'Para n'#227'o visualizar esta janela desmarque a "Auto reativa'#231#227'o de ' +
          'PMVs em gera'#231#227'o de perguntas de monitoramento" nas op'#231#245'es espec'#237 +
          'ficas.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 812
        Height = 16
        Caption = 
          'Para n'#227'o visualizar esta janela desmarque a "Auto reativa'#231#227'o de ' +
          'PMVs em gera'#231#227'o de perguntas de monitoramento" nas op'#231#245'es espec'#237 +
          'ficas.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 859
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 713
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 711
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsGgxPip: TDataSource
    DataSet = QrGgxPip
    Left = 100
    Top = 120
  end
  object QrGgxPip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT omr.Codigo, omr.Controle, omr.Conta, omr.IDIts,'
      'omr.Ordem, omr.GraGruX, omr.UsoQtd, omr.ValCliDd, '
      '2 Tabela'
      'FROM osmonrec omr'
      'LEFT JOIN osmoncab omc ON omc.Conta=omr.Conta'
      'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo'
      'WHERE omc.PipCad=17'
      'AND omr.Desativado=0'
      'AND cab.SiapTerCad=1249'
      ''
      'UNION'
      ''
      'SELECT omr.Codigo, omr.Controle, omr.Conta, omr.IDIts,'
      'omc.Ordem, omr.GraGruX, omr.UsoQtd, omr.ValCliDd, '
      '3 Tabela'
      'FROM ospipitspr omr'
      'LEFT JOIN ospipmon omc ON omc.Controle=omr.Controle'
      'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo'
      'WHERE omc.PipCad=17'
      'AND omr.Desativado=0'
      'AND cab.SiapTerCad=1249'
      ''
      ''
      'ORDER BY Codigo, Controle,'
      'Conta, IDIts, Ordem'
      '')
    Left = 24
    Top = 120
    object QrGgxPipCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGgxPipControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGgxPipConta: TLargeintField
      FieldName = 'Conta'
      Required = True
    end
    object QrGgxPipIDIts: TLargeintField
      FieldName = 'IDIts'
      Required = True
    end
    object QrGgxPipOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrGgxPipGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrGgxPipUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      Required = True
    end
    object QrGgxPipValCliDd: TIntegerField
      FieldName = 'ValCliDd'
      Required = True
    end
    object QrGgxPipTabela: TLargeintField
      FieldName = 'Tabela'
      Required = True
    end
    object QrGgxPipDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
    end
  end
end
