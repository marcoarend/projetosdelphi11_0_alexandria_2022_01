unit ContratLoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, UnBugs_Tabs, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmContratLoc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    Label7: TLabel;
    DBEdit15: TDBEdit;
    QrContratos: TmySQLQuery;
    DsContratos: TDataSource;
    Timer1: TTimer;
    Qr_OS_Loc_: TmySQLQuery;
    Ds_OS_Loc_: TDataSource;
    AdvGridLoc: TDBAdvGrid;
    Qr_OS_Loc_o4Ter: TIntegerField;
    Qr_OS_Loc_c4Ter: TIntegerField;
    Qr_OS_Loc_n4Ter: TWideStringField;
    Qr_OS_Loc_a4Ter: TSmallintField;
    Qr_OS_Loc_o3Loc: TIntegerField;
    Qr_OS_Loc_c3Loc: TIntegerField;
    Qr_OS_Loc_n3Loc: TWideStringField;
    Qr_OS_Loc_a3Loc: TSmallintField;
    Qr_OS_Loc_Ativo: TSmallintField;
    QrPsq1: TmySQLQuery;
    Qr_OS_Loc_Tabela: TSmallintField;
    QrPsq1o4Ter: TIntegerField;
    QrPsq1c4Ter: TIntegerField;
    QrPsq1n4Ter: TWideStringField;
    QrPsq1a4Ter: TSmallintField;
    QrPsq1o3Loc: TIntegerField;
    QrPsq1c3Loc: TIntegerField;
    QrPsq1n3Loc: TWideStringField;
    QrPsq1a3Loc: TSmallintField;
    QrPsq1Tabela: TSmallintField;
    QrPsq1Ativo: TSmallintField;
    QrOL: TmySQLQuery;
    QrOLo4Ter: TIntegerField;
    QrOLc4Ter: TIntegerField;
    QrOLn4Ter: TWideStringField;
    QrOLa4Ter: TSmallintField;
    QrOLo3Loc: TIntegerField;
    QrOLc3Loc: TIntegerField;
    QrOLn3Loc: TWideStringField;
    QrOLa3Loc: TSmallintField;
    QrOLTabela: TSmallintField;
    QrOLAtivo: TSmallintField;
    Qr_OS_Loc_NO_Ativo: TWideStringField;
    QrContratosCodigo: TIntegerField;
    QrContratosContratante: TIntegerField;
    QrContratLoc: TmySQLQuery;
    QrContratLocTabela: TSmallintField;
    QrContratLocCadastro: TIntegerField;
    Qr_OS_Loc_o5Ent: TIntegerField;
    Qr_OS_Loc_c5Ent: TIntegerField;
    Qr_OS_Loc_n5Ent: TWideStringField;
    Qr_OS_Loc_a5Ent: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AdvGridLocClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure Qr_OS_Loc_AfterOpen(DataSet: TDataSet);
    procedure AdvGridLocClick(Sender: TObject);
  private
    { Private declarations }
    FCriou: Boolean;
    F_OS_Loc_: String;
    //
    procedure Reopen_OS_Loc_(Campos: String; Tabela, Codigo: Integer);
  public
    { Public declarations }
    FQrContratLoc: TmySQLQuery;
    //FSiapTerCad, FControle, FConta,
    FEntidade, FCodigo: Integer;
  end;

  var
  FmContratLoc: TFmContratLoc;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, CreateBugs, ModuleGeral,
  UnOSApp_PF, UnDmkProcFunc, UnAppListas;

{$R *.DFM}

const
  FColsToMerge: array[0..1] of Integer = (6,8);
  FMinNiv = 3;

procedure TFmContratLoc.AdvGridLocClick(Sender: TObject);
begin
  MyObjects.UpdMergeDBAvGrid(AdvGridLoc, FColsToMerge);
end;

procedure TFmContratLoc.AdvGridLocClickCell(Sender: TObject; ARow, ACol: Integer);
  function CampoDeNivel(Nivel: Integer; Letra: String): String;
  begin
    case Nivel of
      {
      1: Result := 'Dep';
      2: Result := 'Typ';
      }
      3: Result := 'Loc';
      4: Result := 'Ter';
      else Result := '???';
    end;
    //
    Result := Letra + Geral.FF0(Nivel) + Result;
  end;
const
  MaxNivel = 4;
var
  cN1, Ativo, Nivel, I, Tabela: Integer;
  Campo, Codigo, AtivS, FldAtiv, SQL, _AND_, xFld, nFld: String;
begin
  FldAtiv := TDBAdvGrid(AdvGridLoc).Columns[ACol].FieldName;
  if Copy(FldAtiv, 1, 1) = 'a' then
  begin
    Nivel := Geral.IMV(Copy(FldAtiv, 2, 1));
    if Nivel = 0 then
      Exit;
    cN1 := Geral.IMV(TDBAdvGrid(Sender).Cells[4, ARow]);      // <- Cuidado!!! Coluna fixa
    Tabela := Geral.IMV(TDBAdvGrid(Sender).Cells[1, ARow]);   // <- Cuidado!!! Coluna fixa
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq1, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_OS_Loc_,
    'WHERE c3Loc=' + Geral.FF0(cN1),
    'AND Tabela=' + Geral.FF0(Tabela),
    '']);
    Ativo := QrPsq1.FieldByName(FldAtiv).AsInteger;
    Campo := CampoDeNivel(Nivel, 'c');
    Codigo := Geral.FF0(QrPsq1.FieldByName(Campo).AsInteger);
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    AtivS := Geral.FF0(Ativo);
    //
    SQL := '';
    for I := FMinNiv to Nivel do
      SQL := SQL + ' ' + CampoDeNivel(I, 'a') + '=' + AtivS + ', ';
    SQL := SQL + 'Ativo=' + AtivS;
    //
    _AND_ := '';
    for I := Nivel to MaxNivel do
    begin
      xFld := CampoDeNivel(I, 'c');
      nFld := Geral.FF0(QrPsq1.FieldByName(xFld).AsInteger);
      _AND_ := _AND_ + 'AND ' + xFld + '=' + nFld + sLineBreak;
    end;
    //
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'UPDATE ' + F_OS_Loc_,
    ' SET ',
    SQL,
    'WHERE Tabela=' + Geral.FF0(Tabela),
    //'AND ' + Campo + '=' + Codigo,
    _AND_ +
    '']);
    //
    Reopen_OS_Loc_('Tabela;c3Loc', Tabela, cN1);
  end;
end;

procedure TFmContratLoc.BtOKClick(Sender: TObject);
var
  Controle, Tabela, Cadastro, SubCliente: Integer;
  Codigo: String;
begin
//  Controle := 0;
  Screen.Cursor := crHourGlass;
  try
    SubCliente := FEntidade;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo selecionados');
    //
    Codigo := Geral.FF0(FCodigo);
    //
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'INSERT INTO livre2 (Tabela, Campo, Codigo, Vezes, AlterWeb, Ativo) ',
    'SELECT "contratloc" Tabela, "Controle" Campo, ',
    'Controle Codigo, 1 Vezes, 0 AlterWeb, 0 Ativo ',
    'FROM contratloc ',
    'WHERE Codigo=' + Codigo,
    ';',
    'DELETE FROM contratloc ',
    'WHERE Codigo=' + Codigo,
    '']);
    //
    QrOL.Close;
    UnDmkDAC_PF.AbreMySQLQuery0(QrOL, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_OS_Loc_,
    'WHERE Ativo=1 ',
    'ORDER BY Tabela, o4Ter, n4Ter, ',
    'c4Ter, o3Loc, n3Loc, c3Loc',
    '']);
    QrOL.First;
    while not QrOL.Eof do
    begin
      Tabela         := QrOLTabela.Value;
      Cadastro       := QrOLc3Loc.Value;
      //
      Controle := UMyMod.BPGS1I32_Reaproveita('contratloc', 'Controle', '', '', tsDef, stIns, 0);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contratloc', False, [
      'Codigo', 'Tabela',
      'Cadastro', 'SubCliente'], [
      'Controle'], [
      Codigo, Tabela,
      Cadastro, SubCliente], [
      Controle], True);
      //
      QrOL.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //OSApp_PF.ReopenContratLoc(FQrContratLoc, FConta, IDIts);
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmContratLoc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContratLoc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := True;
end;

procedure TFmContratLoc.FormCreate(Sender: TObject);
begin
  FCriou := False;
end;

procedure TFmContratLoc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  //
  MyObjects.UpdMergeDBAvGrid(AdvGridLoc, FColsToMerge);
end;

procedure TFmContratLoc.Qr_OS_Loc_AfterOpen(DataSet: TDataSet);
begin
  MyObjects.UpdMergeDBAvGrid(AdvGridLoc, FColsToMerge);
end;

procedure TFmContratLoc.Reopen_OS_Loc_(Campos: String; Tabela, Codigo: Integer);
begin
  Qr_OS_Loc_.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr_OS_Loc_, DModG.MyPID_DB, [
  'SELECT ol.*, ELT(ol.Ativo + 1, "N", "S") NO_Ativo ',
  'FROM ' + F_OS_Loc_ + ' ol ',
  'ORDER BY ol.Tabela, ol.o4Ter, ol.n4Ter, ',
  'ol.c4Ter, ol.o3Loc, ol.n3Loc, ol.c3Loc ',
  '']);
  if Trim(Campos) <> '' then
  begin
    Qr_OS_Loc_.Locate(Campos, VarArrayOf([Tabela, Codigo]), [])
  end;
end;

procedure TFmContratLoc.Timer1Timer(Sender: TObject);
var
  c3Loc, Tabela, Ativo, a4Ter, a3Loc, Lugares: Integer;
  Tipos, Txt: String;
  Qry: TmySQLQuery;
begin
  Timer1.Enabled := False;
  if not FCriou then
  begin
    Screen.Cursor := crHourGlass;
    try
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
{
        'SELECT COUNT(stc.Codigo) ITENS ',
        'FROM siaptercad stc ',
        'LEFT JOIN siapimacad sic ON sic.SiapImaTer=stc.Codigo ',
        'WHERE stc.Cliente=' + Geral.FF0(FEntidade),
        'AND sic.Codigo IS NULL ',
}
        'DROP TABLE IF EXISTS STC_sem_DEP_ou_SIC; ',
        ' ',
        'CREATE TABLE STC_sem_DEP_ou_SIC ',
        'SELECT stc.Codigo ',
        'FROM ' + TMeuDB + '.siaptercad stc ',
        'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.SiapImaTer=stc.Codigo ',
        'WHERE stc.Cliente=' + Geral.FF0(FEntidade),
        'AND sic.Codigo IS NULL ',
        ' ',
        'UNION ',
        ' ',
        'SELECT DISTINCT sic.Codigo  ',
        'FROM ' + TMeuDB + '.siapimacad sic ',
        'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sic.Codigo=sid.Codigo ',
        'WHERE sic.Codigo IN  ',
        '  (SELECT Codigo ',
        '   FROM ' + TMeuDB + '.siapimacad ',
        '   WHERE siapimater IN ',
        '     (SELECT Codigo',
        '      FROM ' + TMeuDB + '.siaptercad',
        '      WHERE Cliente=' + Geral.FF0(FEntidade),
        '     ) ',
        '  ) ',
        'AND sid.Codigo IS NULL ',
        ' ',
        '; ',
        ' ',
        ' ',
        'SELECT DISTINCT Codigo ',
        'FROM STC_sem_DEP_ou_SIC; ',
        ' ',
        'DROP TABLE IF EXISTS STC_sem_DEP_ou_SIC; ',
        ' ',
        '']);
        Lugares := Qry.RecordCount;
        if Lugares > 0 then
        begin
          if Lugares = 1 then
            Txt := 'Um lugar n�o sera exibido por n�o possuir locais cadastrados!'
          else
            Txt := Geral.FF0(Lugares) +
          ' lugares n�o ser�o exibidos por estarem com o cadastro de locais e/ou depend�ncias incompleto!';
          Geral.MB_Aviso(Txt);
        end;
      finally
        Qry.Free;
      end;
      FCriou := True;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela tempor�ria!');
      F_OS_Loc_ :=
        UnCreateBugs.RecriaTempTableNovo(ntrtt_OS_Loc_, DmodG.QrUpdPID1, False);
      //
      UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
      'DELETE FROM ' + F_OS_Loc_ + ' ; ',
      'INSERT INTO ' + F_OS_Loc_ + ' ',

      // 2013-07-14 N�o traz Lugares quando n�o tem Locais cadastrados

      'SELECT DISTINCT ',
      // 2013-09-02
      '0 o5Ent, sit.Cliente c3Loc, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) n5Ent, 0 a5Ent, ',
      // FIM 2013-09-02
      '0 o4Ter, sic.SiapImaTer c4Ter, sit.Nome n4Ter, 0 a4Ter, ',
      '0 o3Loc, sid.Codigo c3Loc, sic.SCompl2 n3Loc, 0 a3Loc, ',
      Geral.FF0(CO_TABELA_TIPO_LUGAR_001_SIAPIMADEP) + ' Tabela, 0 Ativo ',
      'FROM ' + TMeuDB + '.siapimadep sid ',
      'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.siaptercad sit ON sit.Codigo=sic.SiapImaTer ',
      'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=sit.Cliente ',
      'WHERE sit.Cliente=' + Geral.FF0(FEntidade),
      // 2013-09-02
      'OR sit.Cliente IN (',
      '     SELECT cug.CunsSub ',
      '     FROM ' + TMeuDB + '.cunsits cug ',
      '     WHERE cug.Cunsgru=' + Geral.FF0(FEntidade),
      ')',
      // FIM 2013-09-02
      '']);
      //
      Tipos :=
        dmkPF.ArrayToTexto('mac.Tipo', ''(*'NO_TIPO'*), pvNo, True, sListaTiposMoveis);
      UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
      'INSERT INTO ' + F_OS_Loc_ + ' ',
      'SELECT ',
      // 2013-09-02
      '0 o5Ent, mac.Cliente c3Loc, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) n5Ent, 0 a5Ent, ',
      // FIM 2013-09-02
      '0 o4Ter, mac.Tipo c4Ter, ' + Tipos +
      //ELT(mac.Tipo, "M�vel", "Autom�vel", "Animal", "Outro", "???")
      ' n4Ter, 0 a4Ter, ',
      '0 o3Loc, mac.Codigo c3Loc, mac.Nome n3Loc, 0 a3Loc, ',
      Geral.FF0(CO_TABELA_TIPO_LUGAR_002_MOVAMOVCAD) + ' Tabela, 0 Ativo ',
      'FROM ' + TMeuDB + '.movamovcad mac ',
      'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=mac.Cliente ',
      'WHERE mac.Cliente=' + Geral.FF0(FEntidade),
      // FIM 2013-07-14
      // 2013-09-02
      'OR mac.Cliente IN (',
      '     SELECT cug.CunsSub ',
      '     FROM ' + TMeuDB + '.cunsits cug ',
      '     WHERE cug.Cunsgru=' + Geral.FF0(FEntidade),
      ')',
      // FIM 2013-09-02
      '']);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrContratLoc, Dmod.MyDB, [
      'SELECT Tabela, Cadastro ',
      'FROM contratloc ',
      'WHERE Codigo=' + Geral.FF0(FCodigo),
      '']);
      Ativo := 1;
      a4Ter := 1;
      a3Loc := 1;
      QrContratLoc.First;
      while not QrContratLoc.Eof do
      begin
        c3Loc  := QrContratLocCadastro.Value;
        Tabela := QrContratLocTabela.Value;
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_OS_Loc_, False, [
        'Ativo', 'a4Ter', 'a3Loc'], ['c3Loc', 'Tabela'
        ], [
        Ativo, a4Ter, a3Loc], [c3Loc, Tabela
        ], False);
        //
        QrContratLoc.Next;
      end;
      //
      Reopen_OS_Loc_('', 0, 0);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
