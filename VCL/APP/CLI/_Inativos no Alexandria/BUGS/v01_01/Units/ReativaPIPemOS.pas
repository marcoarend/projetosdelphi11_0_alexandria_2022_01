unit ReativaPIPemOS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, UnBugs_Tabs, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmReativaPIPemOS = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBGrid1: TDBGrid;
    DsGgxPip: TDataSource;
    QrGgxPip: TmySQLQuery;
    QrGgxPipCodigo: TIntegerField;
    QrGgxPipControle: TIntegerField;
    QrGgxPipConta: TLargeintField;
    QrGgxPipIDIts: TLargeintField;
    QrGgxPipOrdem: TIntegerField;
    QrGgxPipGraGruX: TIntegerField;
    QrGgxPipUsoQtd: TFloatField;
    QrGgxPipValCliDd: TIntegerField;
    QrGgxPipTabela: TLargeintField;
    QrGgxPipDtaExeIni: TDateTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtivaPipDeOS();
    procedure DesativaPipsDeOSs();
    procedure ObtemDadosParaSQL(var Tabela, Campo: String; var Valor: Int64);

  public
    { Public declarations }
    FBgstDoPip: TbgstDoPip;
  end;

  var
  FmReativaPIPemOS: TFmReativaPIPemOS;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmReativaPIPemOS.AtivaPipDeOS;
const
  Desativado = 0;
var
  Tabela, Campo: String;
  Valor: Int64;
begin
  ObtemDadosParaSQL(Tabela, Campo, Valor);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
  'Desativado'], [
  Campo], [
  Desativado], [
  Valor], True) then
    Close;
end;

procedure TFmReativaPIPemOS.BtOKClick(Sender: TObject);
begin
  case FBgstDoPip of
    gstDoPipNone: Geral.MB_Erro('A��o n�o definida! "FmReativaPipemOS"');
    gstDoPipAtivaSelec: AtivaPipDeOS();
    gstDoPipDesativaNaoSelec: DesativaPipsDeOSs();
  end;
end;

procedure TFmReativaPIPemOS.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReativaPIPemOS.DesativaPipsDeOSs();
const
  Desativado = 1;
var
  Tabela, Campo: String;
  Deixar, Valor: Int64;
begin
  ObtemDadosParaSQL(Tabela, Campo, Valor);
  Deixar := Valor;
  //
  QrGgxPip.DisableControls;
  QrGgxPip.First;
  while not QrGgxPip.Eof do
  begin
    ObtemDadosParaSQL(Tabela, Campo, Valor);
    if Valor <> Deixar then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
      'Desativado'], [
      Campo], [
      Desativado], [
      Valor], True);
    end;
    QrGgxPip.Next;
  end;
  Close;
end;

procedure TFmReativaPIPemOS.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmReativaPIPemOS.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmReativaPIPemOS.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReativaPIPemOS.ObtemDadosParaSQL(var Tabela, Campo: String;
var Valor: Int64);
begin
  case QrggxPipTabela.Value of
    CO_OSPipIts_OSMONREC (*2*):
    begin
      Tabela := 'osmonrec';
      Campo := 'Conta';
      Valor := QrggxPipConta.Value;
    end;
    CO_OSPipIts_OSPIPITSPR (*3*):
    begin
      Tabela := 'ospipitspr';
      Campo := 'IDIts';
      Valor := QrggxPipIDIts.Value;
    end;
    else begin
      Tabela := '*?tab?*';
      Tabela := '*?fld?*';
      Valor := 0;
    end;
  end;
end;

end.
