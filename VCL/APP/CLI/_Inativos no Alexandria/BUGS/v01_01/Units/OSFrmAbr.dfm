object FmOSFrmAbr: TFmOSFrmAbr
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-016 :: Abrang'#234'ncia da Aplica'#231#227'o'
  ClientHeight = 673
  ClientWidth = 839
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 839
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 780
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 721
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 370
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abrang'#234'ncia da Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 370
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abrang'#234'ncia da Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 370
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abrang'#234'ncia da Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 839
    Height = 474
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 839
      Height = 474
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 46
        Width = 839
        Height = 428
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object dmkDBGrid1: TdmkDBGrid
          Left = 2
          Top = 18
          Width = 835
          Height = 408
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              ReadOnly = True
              Title.Caption = 'A'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 555
              Visible = True
            end>
          Color = clWindow
          DataSource = Ds_OS_Frm_Abr
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dmkDBGrid1CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              ReadOnly = True
              Title.Caption = 'A'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 555
              Visible = True
            end>
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 839
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        object Label7: TLabel
          Left = 10
          Top = 15
          Width = 22
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'OS:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label4: TLabel
          Left = 266
          Top = 15
          Width = 80
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID Aplica'#231#227'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label5: TLabel
          Left = 113
          Top = 15
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID Servi'#231'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdit6: TDBEdit
          Left = 350
          Top = 10
          Width = 68
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Conta'
          DataSource = DsOSFrmCab
          TabOrder = 1
        end
        object DBEdit5: TDBEdit
          Left = 418
          Top = 10
          Width = 414
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Nome'
          DataSource = DsOSFrmCab
          TabOrder = 2
        end
        object DBEdit8: TDBEdit
          Left = 182
          Top = 10
          Width = 79
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Controle'
          DataSource = DsOSFrmCab
          TabOrder = 3
        end
        object DBEdit15: TDBEdit
          Left = 39
          Top = 10
          Width = 69
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Codigo'
          DataSource = DsOSFrmCab
          TabOrder = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 533
    Width = 839
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 835
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 587
    Width = 839
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 660
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 658
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 238
        Top = 5
        Width = 110
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 366
        Top = 5
        Width = 110
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object QrOSFrmCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle, Conta, Nome '
      'FROM osfrmcab'
      'WHERE Conta=:P0')
    Left = 12
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmCabConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOSFrmCab: TDataSource
    DataSet = QrOSFrmCab
    Left = 40
    Top = 16
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10
    OnTimer = Timer1Timer
    Left = 360
    Top = 96
  end
  object Qr_OS_Frm_Abr: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM _os_frm_abr')
    Left = 276
    Top = 96
    object Qr_OS_Frm_AbrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr_OS_Frm_AbrNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object Qr_OS_Frm_AbrAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_OS_Frm_Abr: TDataSource
    DataSet = Qr_OS_Frm_Abr
    Left = 304
    Top = 96
  end
  object QrPsq1: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM _rece_desp_'
      'WHERE cN1=189')
    Left = 332
    Top = 96
    object QrPsq1o4Ter: TIntegerField
      FieldName = 'o4Ter'
    end
    object QrPsq1c4Ter: TIntegerField
      FieldName = 'c4Ter'
    end
    object QrPsq1n4Ter: TWideStringField
      FieldName = 'n4Ter'
      Size = 100
    end
    object QrPsq1a4Ter: TSmallintField
      FieldName = 'a4Ter'
    end
    object QrPsq1o3Loc: TIntegerField
      FieldName = 'o3Loc'
    end
    object QrPsq1c3Loc: TIntegerField
      FieldName = 'c3Loc'
    end
    object QrPsq1n3Loc: TWideStringField
      FieldName = 'n3Loc'
      Size = 60
    end
    object QrPsq1a3Loc: TSmallintField
      FieldName = 'a3Loc'
    end
    object QrPsq1o2Typ: TIntegerField
      FieldName = 'o2Typ'
    end
    object QrPsq1c2Typ: TIntegerField
      FieldName = 'c2Typ'
    end
    object QrPsq1n2Typ: TWideStringField
      FieldName = 'n2Typ'
      Size = 60
    end
    object QrPsq1a2Typ: TSmallintField
      FieldName = 'a2Typ'
    end
    object QrPsq1o1Dep: TIntegerField
      FieldName = 'o1Dep'
    end
    object QrPsq1c1Dep: TIntegerField
      FieldName = 'c1Dep'
    end
    object QrPsq1n1Dep: TWideStringField
      FieldName = 'n1Dep'
      Size = 60
    end
    object QrPsq1a1Dep: TSmallintField
      FieldName = 'a1Dep'
    end
    object QrPsq1Tabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrPsq1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrOFA: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM _os_frm_abr')
    Left = 388
    Top = 96
    object QrOFAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOFACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOFANome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
end
