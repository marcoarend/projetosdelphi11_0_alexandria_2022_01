object FmFormulFiCb: TFmFormulFiCb
  Left = 339
  Top = 185
  Caption = 'FRM-APLIC-004 :: F'#243'rmulas Filhas de Monitoramento'
  ClientHeight = 317
  ClientWidth = 699
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 173
    Width = 699
    Height = 30
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 617
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 699
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    ExplicitWidth = 617
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 613
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 699
    Height = 61
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    ExplicitWidth = 617
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label1: TLabel
      Left = 96
      Top = 16
      Width = 62
      Height = 13
      Caption = 'F'#243'rmula filha:'
    end
    object Label2: TLabel
      Left = 536
      Top = 16
      Width = 63
      Height = 13
      Caption = 'Per'#237'odo dias:'
    end
    object Label4: TLabel
      Left = 608
      Top = 16
      Width = 76
      Height = 13
      Caption = 'Interv. p'#243'steros:'
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdFormula: TdmkEditCB
      Left = 96
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'PerioDd'
      UpdCampo = 'PerioDd'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBFormula
      IgnoraDBLookupComboBox = False
    end
    object CBFormula: TdmkDBLookupComboBox
      Left = 152
      Top = 32
      Width = 381
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsFormulas
      TabOrder = 2
      dmkEditCB = EdFormula
      QryCampo = 'PerioDd'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdPerioDd: TdmkEdit
      Left = 536
      Top = 32
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'PerioDd'
      UpdCampo = 'PerioDd'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdDdPostero: TdmkEdit
      Left = 608
      Top = 32
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'DdPostero'
      UpdCampo = 'DdPostero'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 699
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 617
    object GB_R: TGroupBox
      Left = 651
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 569
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 603
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 521
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 419
        Height = 32
        Caption = 'F'#243'rmulas Filhas de Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 419
        Height = 32
        Caption = 'F'#243'rmulas Filhas de Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 419
        Height = 32
        Caption = 'F'#243'rmulas Filhas de Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 203
    Width = 699
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitWidth = 617
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 695
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 613
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 247
    Width = 699
    Height = 70
    Align = alBottom
    TabOrder = 5
    ExplicitWidth = 617
    object PnSaiDesis: TPanel
      Left = 553
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 471
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 551
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 469
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrFormulas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frm.Codigo, frm.Nome '
      'FROM formulas frm'
      'ORDER BY frm.Nome')
    Left = 164
    Top = 184
    object QrFormulasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 192
    Top = 184
  end
end
