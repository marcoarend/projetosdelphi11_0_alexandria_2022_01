object FmOSFlhGer: TFmOSFlhGer
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-030 :: OSs Monitoramento de F'#243'rmulas Filhas'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 482
        Height = 32
        Caption = 'OSs Monitoramento de F'#243'rmulas Filhas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 482
        Height = 32
        Caption = 'OSs Monitoramento de F'#243'rmulas Filhas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 482
        Height = 32
        Caption = 'OSs Monitoramento de F'#243'rmulas Filhas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 526
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 526
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GBEtapa1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 145
        Align = alTop
        TabOrder = 0
        ExplicitLeft = -2
        ExplicitTop = -58
        object LaTotal: TLabel
          Left = 2
          Top = 60
          Width = 3
          Height = 13
          Align = alBottom
        end
        object DBGrid1: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 45
          Align = alClient
          DataSource = DsFrmFil
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          PopupMenu = PMGrid1
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Entidade'
              Title.Caption = 'Cliente'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CLIENTE'
              Title.Caption = 'Cliente'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIGLA_TAB'
              Title.Caption = 'Tipo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_STC'
              Title.Caption = 'Lugar'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Localizador'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID Servi'#231'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Title.Caption = 'ID F. A/M'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CO_FORMULA'
              Title.Caption = 'F'#243'rmula'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORMULA'
              Title.Caption = 'Descri'#231#227'o da f'#243'rmula'
              Width = 184
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EmisUltDta'
              Title.Caption = #218'lt. emis.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EmisStatus'
              Title.Caption = 'Status'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Periodd'
              Title.Caption = 'Per'#237'odo dias'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DdPostero'
              Title.Caption = 'Intervalo'
              Width = 48
              Visible = True
            end>
        end
        object GBBotoes1: TGroupBox
          Left = 2
          Top = 73
          Width = 1004
          Height = 70
          Align = alBottom
          TabOrder = 1
          object PnSaiDesis: TPanel
            Left = 858
            Top = 15
            Width = 144
            Height = 53
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BtSaida1: TBitBtn
              Tag = 15
              Left = 12
              Top = 3
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Desiste'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaida1Click
            end
          end
          object Panel1: TPanel
            Left = 2
            Top = 15
            Width = 856
            Height = 53
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object BtPrepara1: TBitBtn
              Tag = 10125
              Left = 136
              Top = 3
              Width = 120
              Height = 40
              Caption = '&Prepara'
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtPrepara1Click
            end
            object BtImprime1: TBitBtn
              Tag = 5
              Left = 12
              Top = 3
              Width = 120
              Height = 40
              Caption = '&Imprime'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtImprime1Click
            end
            object BtTodos: TBitBtn
              Tag = 127
              Left = 405
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Todos'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtTodosClick
            end
            object BtNenhum: TBitBtn
              Tag = 128
              Left = 509
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Nenhum'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = BtNenhumClick
            end
          end
        end
      end
      object GBEtapa2: TGroupBox
        Left = 0
        Top = 145
        Width = 1008
        Height = 145
        Align = alTop
        TabOrder = 1
        Visible = False
        object GroupBox2: TGroupBox
          Left = 2
          Top = 73
          Width = 1004
          Height = 70
          Align = alBottom
          TabOrder = 0
          object Panel5: TPanel
            Left = 858
            Top = 15
            Width = 144
            Height = 53
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BtSaida2: TBitBtn
              Tag = 15
              Left = 12
              Top = 3
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Desiste'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaida2Click
            end
          end
          object Panel6: TPanel
            Left = 2
            Top = 15
            Width = 856
            Height = 53
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object BtImprime2: TBitBtn
              Tag = 5
              Left = 12
              Top = 3
              Width = 120
              Height = 40
              Caption = '&Imprime'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtImprime2Click
            end
            object BtGera: TBitBtn
              Tag = 10
              Left = 136
              Top = 3
              Width = 120
              Height = 40
              Caption = '&Gera OSs'
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtGeraClick
            end
          end
        end
        object DBGrid2: TDBGrid
          Left = 2
          Top = 15
          Width = 1004
          Height = 58
          Align = alClient
          DataSource = DsMoniNewOSs
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Cliente'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CLIENTE'
              Title.Caption = 'Cliente'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SiapTerCad'
              Title.Caption = 'Lugar'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_STC'
              Title.Caption = 'Lugar'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TitItemTab'
              Title.Caption = 'Descri'#231#227'o da f'#243'rmula ou Lista'
              Width = 184
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EmisStatus'
              Title.Caption = 'Status'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Localizador'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EmisUltDta'
              Title.Caption = #218'lt. emis.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EmisStatus'
              Title.Caption = 'Status'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GrupoOS'
              Title.Caption = 'SeqOS'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaOSCalc'
              Title.Caption = 'Data calc'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaOSReal'
              Title.Caption = 'Data real'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Proforma'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Servi'#231'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Title.Caption = 'ID M'#227'e'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IDIts'
              Title.Caption = 'ID Filha'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NumContrat'
              Title.Caption = 'Contrato'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaExeFim'
              Title.Caption = 'Dt. exe fim'
              Visible = True
            end>
        end
      end
      object GBEtapa3: TGroupBox
        Left = 0
        Top = 290
        Width = 1008
        Height = 145
        Align = alTop
        TabOrder = 2
        Visible = False
        object GroupBox3: TGroupBox
          Left = 2
          Top = 73
          Width = 1004
          Height = 70
          Align = alBottom
          TabOrder = 0
          object Panel7: TPanel
            Left = 858
            Top = 15
            Width = 144
            Height = 53
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BtSaida3: TBitBtn
              Tag = 13
              Left = 12
              Top = 3
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Sair'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaida3Click
            end
          end
          object Panel8: TPanel
            Left = 2
            Top = 15
            Width = 856
            Height = 53
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object BitBtn2: TBitBtn
              Tag = 5
              Left = 12
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Imprime'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 0
            end
          end
        end
        object DBGrid3: TDBGrid
          Left = 2
          Top = 15
          Width = 1004
          Height = 58
          Align = alClient
          DataSource = DsNovasOSs
          Enabled = False
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'SeqNovaOS'
              Title.Caption = 'Sequencia'
              Width = 80
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 57
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 450
        Height = 16
        Caption = 
          'Duplo clique na linha seleciona apenas itens do do lugar da linh' +
          'a selecionada!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 450
        Height = 16
        Caption = 
          'Duplo clique na linha seleciona apenas itens do do lugar da linh' +
          'a selecionada!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object QrFrmFil: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFrmFilBeforeClose
    AfterScroll = QrFrmFilAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE, '
      
        ' ELT(omc.EmisStatus + 1,"Aberto","Parcial","Total") NO_EmisStatu' +
        's, '
      'stc.Nome NO_STC, frm.Nome NO_FORMULA, cab.Entidade, '
      'cab.SiapTerCad, cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw, '
      '2 Tabela, '
      
        'omc.Conta IDSuperior, omc.EmisUltDta, omc.Periodd, omc.EmisStatu' +
        's'
      'FROM osmoncab omc '
      'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo '
      'LEFT JOIN formulas frm ON frm.Codigo=omc.Formula '
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade '
      'LEFT JOIN SiapTerCad stc ON stc.Codigo=cab.SiapTerCad '
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat '
      'WHERE omc.EmisStatus < 2 '
      'AND omc.EmisUltDta < SYSDATE() '
      'AND cab.DtaExeFim > "1900-01-01" '
      'AND '
      '( '
      '     cab.NumContrat=0 '
      '     OR '
      '     ( '
      '          ctr.DtaCntrFim < "1900-01-01" '
      '     ) '
      ') ')
    Left = 16
    Top = 128
    object QrFrmFilNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrFrmFilNO_EmisStatus: TWideStringField
      FieldName = 'NO_EmisStatus'
      Size = 7
    end
    object QrFrmFilNO_STC: TWideStringField
      FieldName = 'NO_STC'
      Size = 100
    end
    object QrFrmFilNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrFrmFilEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrFrmFilSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrFrmFilDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrFrmFilNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrFrmFilDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
    end
    object QrFrmFilTabela: TLargeintField
      FieldName = 'Tabela'
      Required = True
    end
    object QrFrmFilSIGLA_TAB: TWideStringField
      FieldName = 'SIGLA_TAB'
      Size = 3
    end
    object QrFrmFilIDSuperior: TIntegerField
      FieldName = 'IDSuperior'
    end
    object QrFrmFilEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
      OnGetText = QrFrmFilEmisUltDtaGetText
    end
    object QrFrmFilPeriodd: TIntegerField
      FieldName = 'Periodd'
    end
    object QrFrmFilEmisStatus: TSmallintField
      FieldName = 'EmisStatus'
    end
    object QrFrmFilCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrmFilControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrmFilConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFrmFilCO_FORMULA: TIntegerField
      FieldName = 'CO_FORMULA'
    end
    object QrFrmFilDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
    object QrFrmFilEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFrmFilExtenDd: TIntegerField
      FieldName = 'ExtenDd'
    end
    object QrFrmFilOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrFrmFilGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrFrmFilDataSincOS: TDateField
      FieldName = 'DataSincOS'
    end
  end
  object DsFrmFil: TDataSource
    DataSet = QrFrmFil
    Left = 64
    Top = 128
  end
  object QrFFLE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ffc.Codigo, ffc.Controle, ffc.Conta, ffc.IDIts, '
      'ffc.EmisUltDta, ffc.Formula, ffc.Periodd, ffc.DdPostero, '
      'ffc.EmisStatus,'
      'cab.NumContrat, cab.DtaExeFim, cab.Entidade,'
      'cab.SiapTerCad, ctr.DtaCntrFim'
      'FROM osfrmflhcb ffc '
      'LEFT JOIN oscab cab ON cab.Codigo=ffc.Codigo '
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat '
      'WHERE ffc.EmisStatus < 2'
      'AND ffc.EmisUltDta < SYSDATE() '
      'AND ffc.IDIts=3'
      'AND cab.DtaExeFim > "1900-01-01" '
      'AND '
      '( '
      '     cab.NumContrat=0 '
      '     OR '
      '     ( '
      '          ctr.DtaCntrFim < "1900-01-01" '
      '     ) '
      ') '
      '')
    Left = 164
    Top = 128
    object QrFFLECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFFLEControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFFLEConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFFLEIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrFFLEEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
    end
    object QrFFLEFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrFFLEPeriodd: TIntegerField
      FieldName = 'Periodd'
    end
    object QrFFLEDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
    object QrFFLENumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrFFLEDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrFFLEDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
    end
    object QrFFLEEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrFFLESiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrFFLEEmisStatus: TIntegerField
      FieldName = 'EmisStatus'
    end
    object QrFFLEEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFFLEDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrFFLEHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
    end
    object QrFFLEHrExecutar: TFloatField
      FieldName = 'HrExecutar'
    end
    object QrFFLENO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrFFLEExtenDd: TIntegerField
      FieldName = 'ExtenDd'
    end
  end
  object QrFFII: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ffd.Codigo, ffd.Controle, ffd.Conta, ffd.IDIts,'
      'ffd.IDIt2, ffd.Ordem, ffd.Dias, ffc.EmisUltDta,'
      'ffc.Formula, ffc.Periodd, ffc.DdPostero,'
      'ffc.EmisStatus,'
      'cab.NumContrat, cab.DtaExeFim, cab.Entidade,'
      'cab.SiapTerCad, ctr.DtaCntrFim'
      'FROM osfrmflhdd ffd'
      'LEFT JOIN osfrmflhcb ffc ON ffc.IDIts=ffd.IDIts'
      'LEFT JOIN oscab cab ON cab.Codigo=ffc.Codigo'
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat'
      'WHERE ffc.EmisStatus < 2'
      'AND ffc.EmisUltDta < SYSDATE()'
      'AND ffd.IDIts=3'
      'AND cab.DtaExeFim > "1900-01-01"'
      'AND'
      '('
      '     cab.NumContrat=0'
      '     OR'
      '     ('
      '          ctr.DtaCntrFim < "1900-01-01"'
      '          OR'
      '          ctr.DtaCntrFim > SYSDATE()'
      '     )'
      ')'
      'ORDER BY Ordem'
      '')
    Left = 116
    Top = 128
    object QrFFIICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFFIIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFFIIConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFFIIIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrFFIIIDIt2: TIntegerField
      FieldName = 'IDIt2'
    end
    object QrFFIIOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFFIIDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrFFIIEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
    end
    object QrFFIIFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrFFIIPeriodd: TIntegerField
      FieldName = 'Periodd'
    end
    object QrFFIIDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
    object QrFFIINumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrFFIIDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrFFIIDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
    end
    object QrFFIIEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrFFIISiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrFFIIEmisStatus: TIntegerField
      FieldName = 'EmisStatus'
    end
    object QrFFIIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFFIIDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrFFIIHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
    end
    object QrFFIIHrExecutar: TFloatField
      FieldName = 'HrExecutar'
    end
    object QrFFIINO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrFFIIExtenDD: TIntegerField
      FieldName = 'ExtenDD'
    end
  end
  object QrCtrIndf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ctr.Codigo CONTRATO '
      'FROM osfrmflhcb ffc  '
      'LEFT JOIN oscab cab ON cab.Codigo=ffc.Codigo  '
      'LEFT JOIN formulas frm ON frm.Codigo=ffc.Formula  '
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade '
      'LEFT JOIN SiapTerCad stc ON stc.Codigo=cab.SiapTerCad '
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat  '
      'WHERE ffc.EmisStatus < 2 '
      'AND ffc.EmisUltDta < SYSDATE()  '
      'AND cab.DtaExeFim > "1900-01-01"  '
      'AND cab.NumContrat<>0  '
      'AND  '
      '(  '
      '     ctr.DtaCntrFim < "1900-01-01"  '
      '     OR  '
      '     ctr.DtaCntrFim > ffc.EmisUltDta  '
      ')  ')
    Left = 216
    Top = 128
    object QrCtrIndfCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
  end
  object QrLocais: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT DISTINCT Cliente, SiapTerCad '
      'FROM _moninewoss'
      'ORDER BY Cliente, SiapTerCad')
    Left = 268
    Top = 128
    object QrLocaisCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrLocaisSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
      Required = True
    end
    object QrLocaisEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrDatas: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM _moninewoss'
      'WHERE DtaOSReal < 2'
      'AND Cliente=499'
      'AND SiapTerCad=499'
      'ORDER BY DtaOSCalc')
    Left = 328
    Top = 128
    object QrDatasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDatasControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDatasConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrDatasIDIts: TIntegerField
      FieldName = 'IDIts'
      Required = True
    end
    object QrDatasIDIt2: TIntegerField
      FieldName = 'IDIt2'
      Required = True
    end
    object QrDatasOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrDatasDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrDatasEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
    end
    object QrDatasFormula: TIntegerField
      FieldName = 'Formula'
      Required = True
    end
    object QrDatasPeriodd: TIntegerField
      FieldName = 'Periodd'
      Required = True
    end
    object QrDatasDdPostero: TIntegerField
      FieldName = 'DdPostero'
      Required = True
    end
    object QrDatasNumContrat: TIntegerField
      FieldName = 'NumContrat'
      Required = True
    end
    object QrDatasDtaExeFim: TDateField
      FieldName = 'DtaExeFim'
    end
    object QrDatasDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
    end
    object QrDatasDtaOSCalc: TDateField
      FieldName = 'DtaOSCalc'
    end
    object QrDatasDtaOSReal: TDateField
      FieldName = 'DtaOSReal'
    end
    object QrDatasCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrDatasSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
      Required = True
    end
    object QrDatasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrDatasIDItZ: TIntegerField
      FieldName = 'IDItZ'
    end
    object QrDatasDtaOSUtil: TDateField
      FieldName = 'DtaOSUtil'
    end
  end
  object QrMoniNewOSs: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,'
      
        ' ELT(mno.EmisStatus + 1,"Aberto","Parcial","Total") NO_EmisStatu' +
        's,'
      'stc.Nome NO_STC, frm.Nome NO_FORMULA, '
      'ctr.DtaPrxRenw, mno.*'
      'FROM _moninewoss mno'
      'LEFT JOIN bugstrol.oscab cab ON cab.Codigo=mno.Codigo '
      'LEFT JOIN bugstrol.formulas frm ON frm.Codigo=mno.Formula '
      'LEFT JOIN bugstrol.entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN bugstrol.SiapTerCad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN bugstrol.contratos ctr ON ctr.Codigo=cab.NumContrat '
      'ORDER BY mno.SeqNovaOS, mno.Cliente, mno.SiapTerCad, '
      'mno.DtaOSReal, mno.DtaOsCalc, mno.DdPostero')
    Left = 28
    Top = 272
    object QrMoniNewOSsNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrMoniNewOSsNO_EmisStatus: TWideStringField
      FieldName = 'NO_EmisStatus'
      Size = 7
    end
    object QrMoniNewOSsNO_STC: TWideStringField
      FieldName = 'NO_STC'
      Size = 100
    end
    object QrMoniNewOSsDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMoniNewOSsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMoniNewOSsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMoniNewOSsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrMoniNewOSsIDIts: TIntegerField
      FieldName = 'IDIts'
      Required = True
    end
    object QrMoniNewOSsIDIt2: TIntegerField
      FieldName = 'IDIt2'
      Required = True
    end
    object QrMoniNewOSsIDItZ: TIntegerField
      FieldName = 'IDItZ'
      Required = True
    end
    object QrMoniNewOSsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrMoniNewOSsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrMoniNewOSsEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
      OnGetText = QrMoniNewOSsEmisUltDtaGetText
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMoniNewOSsFormula: TIntegerField
      FieldName = 'Formula'
      Required = True
    end
    object QrMoniNewOSsPeriodd: TIntegerField
      FieldName = 'Periodd'
      Required = True
    end
    object QrMoniNewOSsDdPostero: TIntegerField
      FieldName = 'DdPostero'
      Required = True
    end
    object QrMoniNewOSsNumContrat: TIntegerField
      FieldName = 'NumContrat'
      Required = True
    end
    object QrMoniNewOSsDtaExeFim: TDateField
      FieldName = 'DtaExeFim'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMoniNewOSsDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMoniNewOSsDtaOSCalc: TDateField
      FieldName = 'DtaOSCalc'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMoniNewOSsDtaOSReal: TDateField
      FieldName = 'DtaOSReal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMoniNewOSsCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrMoniNewOSsSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
      Required = True
    end
    object QrMoniNewOSsEmisStatus: TIntegerField
      FieldName = 'EmisStatus'
      Required = True
    end
    object QrMoniNewOSsGrupoOS: TIntegerField
      FieldName = 'GrupoOS'
    end
    object QrMoniNewOSsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMoniNewOSsDtaOSUtil: TDateField
      FieldName = 'DtaOSUtil'
    end
    object QrMoniNewOSsDiaSemTxt: TWideStringField
      FieldName = 'DiaSemTxt'
    end
    object QrMoniNewOSsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMoniNewOSsSeqNovaOS: TIntegerField
      FieldName = 'SeqNovaOS'
    end
    object QrMoniNewOSsTitItemTab: TWideStringField
      FieldName = 'TitItemTab'
      Size = 60
    end
    object QrMoniNewOSsTitTipoTab: TWideStringField
      FieldName = 'TitTipoTab'
    end
  end
  object DsMoniNewOSs: TDataSource
    DataSet = QrMoniNewOSs
    Left = 112
    Top = 272
  end
  object frxGER_OSERV_030_002_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  //MePagina.Visible := <VARF_PAGINAR>;  '
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxGER_OSERV_030_002_AGetValue
    Left = 216
    Top = 272
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMoniNewOSs
        DataSetName = 'frxDsMoniNewOSs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 56.692950000000010000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Rela'#231#227'o de OSs a serem criadas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsMoniNewOSs."SiapTerCad"'
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo51: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 22.677158030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Lugar: [frxDsMoniNewOSs."SiapTerCad"] - [frxDsMoniNewOSs."NO_STC' +
              '"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 49.133890000000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsMoniNewOSs."SeqNovaOS"'
        object Shape2: TfrxShapeView
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 604.724800000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo53: TfrxMemoView
          Left = 41.574830000000000000
          Top = 15.118119999999980000
          Width = 597.165740000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Sequencial de cria'#231#227'o de OS: [frxDsMoniNewOSs."SeqNovaOS"]    Da' +
              'ta:  [FormatDateTime('#39'dd/mm/yyyy'#39',<frxDsMoniNewOSs."DtaOSReal">)' +
              ']')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 83.149660000000000000
          Top = 34.015769999999970000
          Width = 52.913390710000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 136.063080000000000000
          Top = 34.015769999999970000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID Srv.')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 181.417440000000000000
          Top = 34.015769999999970000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID m'#227'e')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 226.771800000000000000
          Top = 34.015769999999970000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID filha')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 272.126160000000000000
          Top = 34.015769999999970000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID interv')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 634.961040000000000000
          Top = 34.015769999999970000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Top = 34.015769999999970000
          Width = 83.149630710000000000
          Height = 15.118120000000000000
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data calculada')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 612.283860000000000000
          Top = 34.015769999999970000
          Width = 22.677165350000000000
          Height = 15.118120000000000000
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dias')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 317.480520000000000000
          Top = 34.015769999999970000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Contrato')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 362.834880000000000000
          Top = 34.015769999999970000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dt.exc.m'#227'e')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 434.645950000000000000
          Top = 34.015769999999970000
          Width = 177.637870940000000000
          Height = 15.118120000000000000
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'F'#243'rmula base / Lista de perguntas')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 408.189240000000000000
          Top = 34.015769999999970000
          Width = 26.456670940000000000
          Height = 15.118120000000000000
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 336.378170000000000000
        Width = 680.315400000000000000
        DataSet = frxDsMoniNewOSs
        DataSetName = 'frxDsMoniNewOSs'
        RowCount = 0
        object Memo11: TfrxMemoView
          Left = 83.149660000000000000
          Width = 52.913390710000000000
          Height = 15.118120000000000000
          DataField = 'Codigo'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."Codigo"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 136.063080000000000000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          DataField = 'Controle'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."Controle"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 181.417440000000000000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          DataField = 'Conta'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."Conta"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 226.771800000000000000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          DataField = 'IDIts'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."IDIts"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 272.126160000000000000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          DataField = 'IDIt2'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."IDIt2"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 634.961040000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataField = 'NO_EmisStatus'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."NO_EmisStatus"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          DataField = 'DtaOSCalc'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."DtaOSCalc"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 612.283860000000000000
          Width = 22.677165350000000000
          Height = 15.118120000000000000
          DataField = 'Dias'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."Dias"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 317.480520000000000000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          DataField = 'NumContrat'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."NumContrat"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 362.834880000000000000
          Width = 45.354330710000000000
          Height = 15.118120000000000000
          DataField = 'DtaExeFim'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."DtaExeFim"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 434.645950000000000000
          Width = 177.637870940000000000
          Height = 15.118120000000000000
          DataField = 'TitItemTab'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."TitItemTab"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 45.354360000000000000
          Width = 37.795275590551180000
          Height = 15.118120000000000000
          DataField = 'DiaSemTxt'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."DiaSemTxt"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 408.189240000000000000
          Width = 26.456692910000000000
          Height = 15.118120000000000000
          DataField = 'TitTipoTab'
          DataSet = frxDsMoniNewOSs
          DataSetName = 'frxDsMoniNewOSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."TitTipoTab"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 374.173470000000000000
        Width = 680.315400000000000000
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 400.630180000000000000
        Width = 680.315400000000000000
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 491.338900000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 56.692928030000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsMoniNewOSs."Cliente"'
        object Memo1: TfrxMemoView
          Top = 7.559059999999988000
          Width = 680.315400000000000000
          Height = 22.677158030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente n'#176' [frxDsMoniNewOSs."Cliente"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Top = 30.236240000000010000
          Width = 680.315400000000000000
          Height = 22.677158030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoniNewOSs."NO_CLIENTE"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 427.086890000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsMoniNewOSs: TfrxDBDataset
    UserName = 'frxDsMoniNewOSs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_CLIENTE=NO_CLIENTE'
      'NO_EmisStatus=NO_EmisStatus'
      'NO_STC=NO_STC'
      'DtaPrxRenw=DtaPrxRenw'
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'IDIts=IDIts'
      'IDIt2=IDIt2'
      'IDItZ=IDItZ'
      'Ordem=Ordem'
      'Dias=Dias'
      'EmisUltDta=EmisUltDta'
      'Formula=Formula'
      'Periodd=Periodd'
      'DdPostero=DdPostero'
      'NumContrat=NumContrat'
      'DtaExeFim=DtaExeFim'
      'DtaCntrFim=DtaCntrFim'
      'DtaOSCalc=DtaOSCalc'
      'DtaOSReal=DtaOSReal'
      'Cliente=Cliente'
      'SiapTerCad=SiapTerCad'
      'EmisStatus=EmisStatus'
      'GrupoOS=GrupoOS'
      'Ativo=Ativo'
      'DtaOSUtil=DtaOSUtil'
      'DiaSemTxt=DiaSemTxt'
      'Empresa=Empresa'
      'SeqNovaOS=SeqNovaOS'
      'TitItemTab=TitItemTab'
      'TitTipoTab=TitTipoTab')
    DataSet = QrMoniNewOSs
    BCDToCurrency = False
    Left = 328
    Top = 272
  end
  object QrOSCab: TmySQLQuery
    Database = Dmod.MyDB
    Left = 88
    Top = 432
    object QrOSCabEntiContat: TIntegerField
      FieldName = 'EntiContat'
    end
    object QrOSCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
    end
    object QrOSCabEntContrat: TIntegerField
      FieldName = 'EntContrat'
    end
    object QrOSCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrOSCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrOSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrOSCabEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrOSCabSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrOSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrOSCabMulServico: TLargeintField
      FieldName = 'MulServico'
    end
    object QrOSCabAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object QrOSCabPdrMntsMon: TIntegerField
      FieldName = 'PdrMntsMon'
    end
    object QrOSCabDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
    end
    object QrOSCabMobiliCad: TIntegerField
      FieldName = 'MobiliCad'
    end
  end
  object QrOSSrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ossrv')
    Left = 88
    Top = 476
    object QrOSSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSSrvDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrOSSrvGarantiaDd: TIntegerField
      FieldName = 'GarantiaDd'
    end
    object QrOSSrvHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
    end
    object QrOSSrvHrExecutar: TFloatField
      FieldName = 'HrExecutar'
    end
    object QrOSSrvValCalc: TFloatField
      FieldName = 'ValCalc'
    end
    object QrOSSrvValInfo: TFloatField
      FieldName = 'ValInfo'
    end
    object QrOSSrvValDesc: TFloatField
      FieldName = 'ValDesc'
    end
    object QrOSSrvValTota: TFloatField
      FieldName = 'ValTota'
    end
    object QrOSSrvAutorizado: TSmallintField
      FieldName = 'Autorizado'
    end
    object QrOSSrvDetalhes: TWideMemoField
      FieldName = 'Detalhes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSSrvTudoFeitoM: TSmallintField
      FieldName = 'TudoFeitoM'
    end
    object QrOSSrvTudoFeitoA: TSmallintField
      FieldName = 'TudoFeitoA'
    end
    object QrOSSrvHowEstav: TSmallintField
      FieldName = 'HowEstav'
    end
    object QrOSSrvHowOrige: TIntegerField
      FieldName = 'HowOrige'
    end
    object QrOSSrvMoniDdTotl: TIntegerField
      FieldName = 'MoniDdTotl'
    end
    object QrOSSrvMoniDdIntv: TSmallintField
      FieldName = 'MoniDdIntv'
    end
  end
  object QrSNOS: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT DISTINCT GrupoOS, Codigo '
      'FROM _moninewoss')
    Left = 388
    Top = 128
    object QrSNOSGrupoOS: TIntegerField
      FieldName = 'GrupoOS'
    end
  end
  object QrNovasOSs: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrNovasOSsAfterOpen
    SQL.Strings = (
      'SELECT SeqNovaOS'
      'FROM _moninewoss'
      'ORDER BY SeqNovaOS')
    Left = 28
    Top = 432
    object QrNovasOSsSeqNovaOS: TIntegerField
      FieldName = 'SeqNovaOS'
    end
  end
  object QrNovosSrv: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 28
    Top = 524
    object QrNovosSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNovosSrvDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
  end
  object QrOSAlv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT osa.Praga_Z'
      'FROM osalv osa'
      'WHERE osa.Controle=:P0'
      '')
    Left = 88
    Top = 520
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
      Origin = 'osalv.Praga_Z'
    end
  end
  object QrFMOI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ffd.Codigo, ffd.Controle, ffd.Conta, '
      'ffc.Ordem, ffc.Dias, ffd.EmisUltDta, '
      'pip.PrgLstCab, ffd.Periodd, ffd.DdPostero, '
      'ffd.EmisStatus, '
      'cab.NumContrat, cab.DtaExeFim, cab.Entidade, '
      'cab.SiapTerCad, cab.Empresa, '
      'srv.DesServico, srv.HrEvacuar, srv.HrExecutar, '
      'ctr.DtaCntrFim '
      'FROM osmoncab ffd '
      'LEFT JOIN osmonpipdd ffc ON ffc.Conta=ffd.Conta '
      'LEFT JOIN oscab cab ON cab.Codigo=ffc.Codigo '
      'LEFT JOIN ossrv srv ON srv.Controle=ffd.Controle'
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat '
      'LEFT JOIN pipcad pip ON pip.Codigo=ffd.PipCad'
      'WHERE ffd.EmisStatus < 2'
      'AND ffd.EmisUltDta < SYSDATE() '
      'AND ffd.Conta=57'
      'AND cab.DtaExeFim > "1900-01-01" '
      'AND '
      '( '
      '     cab.NumContrat=0 '
      '     OR '
      '     ( '
      '          ctr.DtaCntrFim < "1900-01-01" '
      '     ) '
      ') ')
    Left = 316
    Top = 400
    object QrFMOICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFMOIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFMOIConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFMOIOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFMOIDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrFMOIEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
    end
    object QrFMOIPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrFMOIPeriodd: TIntegerField
      FieldName = 'Periodd'
    end
    object QrFMOIDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
    object QrFMOIEmisStatus: TSmallintField
      FieldName = 'EmisStatus'
    end
    object QrFMOINumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrFMOIDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrFMOIEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrFMOISiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrFMOIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFMOIDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrFMOIHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
    end
    object QrFMOIHrExecutar: TFloatField
      FieldName = 'HrExecutar'
    end
    object QrFMOIDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
    end
    object QrFMOIIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrFMOINO_PrgLstCab: TWideStringField
      FieldName = 'NO_PrgLstCab'
      Size = 60
    end
    object QrFMOIExtenDd: TIntegerField
      FieldName = 'ExtenDd'
    end
  end
  object QrFMOE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT omc.Codigo, omc.Controle, omc.Conta,'
      'omc.EmisUltDta, omc.Formula, omc.Periodd, omc.DdPostero,'
      'omc.EmisStatus,'
      'cab.NumContrat, cab.DtaExeFim, cab.Entidade,'
      'cab.SiapTerCad, cab.Empresa,'
      'srv.DesServico, srv.HrEvacuar, srv.HrExecutar,'
      'ctr.DtaCntrFim'
      'FROM osmoncab omc'
      'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo'
      'LEFT JOIN ossrv srv ON srv.Controle=omc.Controle'
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat'
      'WHERE omc.EmisStatus < 2'
      'AND omc.EmisUltDta < SYSDATE()'
      'AND omc.Conta>6'
      'AND cab.DtaExeFim > "1900-01-01"'
      'AND'
      '('
      '     cab.NumContrat=0'
      '     OR'
      '     ('
      '          ctr.DtaCntrFim < "1900-01-01"'
      '     )'
      ')')
    Left = 316
    Top = 444
    object QrFMOECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFMOEControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFMOEConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFMOEEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
    end
    object QrFMOEFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrFMOEPeriodd: TIntegerField
      FieldName = 'Periodd'
    end
    object QrFMOEDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
    object QrFMOEEmisStatus: TSmallintField
      FieldName = 'EmisStatus'
    end
    object QrFMOENumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrFMOEDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrFMOEEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrFMOESiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrFMOEEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFMOEDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrFMOEHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
    end
    object QrFMOEHrExecutar: TFloatField
      FieldName = 'HrExecutar'
    end
    object QrFMOEDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
    end
    object QrFMOEPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrFMOENO_PrgLstCab: TWideStringField
      FieldName = 'NO_PrgLstCab'
      Size = 60
    end
    object QrFMOEExtenDd: TIntegerField
      FieldName = 'ExtenDd'
    end
  end
  object QrOper: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 316
    Top = 488
    object QrOperTabela: TIntegerField
      FieldName = 'Tabela'
    end
  end
  object QrNewF_M: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT *'
      'FROM _moninewoss'
      'WHERE SeqNovaOS=1'
      'AND Codigo=4'
      'AND DtaOSReal="2013-07-15"'
      'AND Controle=4'
      'AND DdPostero=30')
    Left = 28
    Top = 568
    object QrNewF_MTabela: TIntegerField
      FieldName = 'Tabela'
      Required = True
    end
    object QrNewF_MCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrNewF_MControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrNewF_MConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrNewF_MIDIts: TIntegerField
      FieldName = 'IDIts'
      Required = True
    end
    object QrNewF_MIDIt2: TIntegerField
      FieldName = 'IDIt2'
      Required = True
    end
    object QrNewF_MIDItZ: TIntegerField
      FieldName = 'IDItZ'
      Required = True
    end
    object QrNewF_MOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrNewF_MDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrNewF_MEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
    end
    object QrNewF_MDesServico: TIntegerField
      FieldName = 'DesServico'
      Required = True
    end
    object QrNewF_MHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
      Required = True
    end
    object QrNewF_MHrExecutar: TFloatField
      FieldName = 'HrExecutar'
      Required = True
    end
    object QrNewF_MFormula: TIntegerField
      FieldName = 'Formula'
      Required = True
    end
    object QrNewF_MPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
      Required = True
    end
    object QrNewF_MPeriodd: TIntegerField
      FieldName = 'Periodd'
      Required = True
    end
    object QrNewF_MDdPostero: TIntegerField
      FieldName = 'DdPostero'
      Required = True
    end
    object QrNewF_MNumContrat: TIntegerField
      FieldName = 'NumContrat'
      Required = True
    end
    object QrNewF_MDtaExeFim: TDateField
      FieldName = 'DtaExeFim'
    end
    object QrNewF_MDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
    end
    object QrNewF_MDtaOSCalc: TDateField
      FieldName = 'DtaOSCalc'
    end
    object QrNewF_MDtaOSUtil: TDateField
      FieldName = 'DtaOSUtil'
    end
    object QrNewF_MDtaOSReal: TDateField
      FieldName = 'DtaOSReal'
    end
    object QrNewF_MEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrNewF_MCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrNewF_MSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
      Required = True
    end
    object QrNewF_MEmisStatus: TIntegerField
      FieldName = 'EmisStatus'
      Required = True
    end
    object QrNewF_MGrupoOS: TIntegerField
      FieldName = 'GrupoOS'
    end
    object QrNewF_MSeqNovaOS: TIntegerField
      FieldName = 'SeqNovaOS'
    end
    object QrNewF_MDiaSemTxt: TWideStringField
      FieldName = 'DiaSemTxt'
    end
    object QrNewF_MTitTipoTab: TWideStringField
      FieldName = 'TitTipoTab'
    end
    object QrNewF_MTitItemTab: TWideStringField
      FieldName = 'TitItemTab'
      Size = 60
    end
    object QrNewF_MAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFormula: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM Formulas'
      'WHERE Codigo=8')
    Left = 316
    Top = 536
    object QrFormulaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFormulaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFormulaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulaEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrFormulaQtdTot: TFloatField
      FieldName = 'QtdTot'
    end
    object QrFormulaQtdQSP: TFloatField
      FieldName = 'QtdQSP'
    end
    object QrFormulaCusTot: TFloatField
      FieldName = 'CusTot'
    end
    object QrFormulaAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrFormulaDiluente: TSmallintField
      FieldName = 'Diluente'
    end
    object QrFormulaTipoAplica: TIntegerField
      FieldName = 'TipoAplica'
    end
    object QrFormulaUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
  end
  object QrOSFrmDep: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM osfrmdep '
      'WHERE Codigo=4'
      'AND Controle=4'
      'AND Conta=1272')
    Left = 316
    Top = 580
    object QrOSFrmDepCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmDepControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmDepConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmDepIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSFrmDepTabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrOSFrmDepCadastro: TIntegerField
      FieldName = 'Cadastro'
    end
    object QrOSFrmDepLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSFrmDepDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSFrmDepDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSFrmDepUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSFrmDepUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSFrmDepAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSFrmDepAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrOSAge: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT age.* '
      'FROM osage age '
      'WHERE age.Codigo>0')
    Left = 88
    Top = 568
    object QrOSAgeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSAgeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSAgeAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrOSAgeResponsa: TSmallintField
      FieldName = 'Responsa'
    end
    object QrOSAgeLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSAgeDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSAgeDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSAgeUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSAgeUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSAgeAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSAgeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNovasOSs: TDataSource
    DataSet = QrNovasOSs
    Left = 28
    Top = 476
  end
  object QrLocPip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pip.*'
      'FROM pipcad pip'
      'LEFT JOIN osmoncab omc ON omc.PipCad=pip.Codigo'
      'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo'
      'WHERE cab.SiapTerCad=1249'
      'AND pip.DtaDesativ < "1900-01-01"'
      'ORDER BY pip.Codigo')
    Left = 492
    Top = 480
    object QrLocPipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocPipPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrLocPipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrLocPipEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrLocPipOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrLocPipMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrLocPipDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrLocPipDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrLocPipDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrLocPipOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrLocPipPerioDd: TIntegerField
      FieldName = 'PerioDd'
    end
    object QrLocPipDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
  end
  object QrDtaIndf: TmySQLQuery
    Database = Dmod.MyDB
    Left = 216
    Top = 176
    object QrDtaIndfDataSincOS: TDateField
      FieldName = 'DataSincOS'
    end
  end
  object _QrNovasOSs_: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrNovasOSsAfterOpen
    SQL.Strings = (
      'SELECT DISTINCT DtaOSReal, SeqNovaOS, Codigo'
      'FROM _moninewoss'
      'ORDER BY DtaOSReal, SeqNovaOS, Codigo')
    Left = 676
    Top = 340
    object DateField1: TDateField
      FieldName = 'DtaOSReal'
    end
    object IntegerField1: TIntegerField
      FieldName = 'SeqNovaOS'
    end
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrOSs: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrNovasOSsAfterOpen
    SQL.Strings = (
      'SELECT Codigo, DtaOSReal'
      'FROM _moninewoss'
      'WHERE SeqNovaOS=:P0'
      'ORDER BY Codigo, DtaOSReal')
    Left = 164
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSsDtaOSReal: TDateField
      FieldName = 'DtaOSReal'
    end
    object QrOSsMinHAgeExe: TTimeField
      FieldName = 'MinHAgeExe'
    end
    object QrOSsDtInicial: TDateField
      FieldName = 'DtInicial'
    end
  end
  object QrSiapTerFlh: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM siapterflh'
      'WHERE Codigo=:P0'
      'AND Empresa=:P1')
    Left = 492
    Top = 524
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSiapTerFlhCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapTerFlhControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapTerFlhEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSiapTerFlhMonAgeEqi: TIntegerField
      FieldName = 'MonAgeEqi'
    end
    object QrSiapTerFlhMonEntCtt: TIntegerField
      FieldName = 'MonEntCtt'
    end
    object QrSiapTerFlhMonNumCtr: TIntegerField
      FieldName = 'MonNumCtr'
    end
    object QrSiapTerFlhMonEntCtr: TIntegerField
      FieldName = 'MonEntCtr'
    end
    object QrSiapTerFlhMonEntPag: TIntegerField
      FieldName = 'MonEntPag'
    end
    object QrSiapTerFlhMonCondPg: TIntegerField
      FieldName = 'MonCondPg'
    end
    object QrSiapTerFlhMonCrtEmi: TIntegerField
      FieldName = 'MonCrtEmi'
    end
    object QrSiapTerFlhDataSincOS: TDateField
      FieldName = 'DataSincOS'
    end
    object QrSiapTerFlhMinHAgeExe: TTimeField
      FieldName = 'MinHAgeExe'
    end
  end
  object QrDistin: TmySQLQuery
    Database = DModG.MyPID_DB
    Left = 492
    Top = 572
    object QrDistinSubTab: TIntegerField
      FieldName = 'SubTab'
    end
  end
  object QrAtzSrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT frc.OSFFCbMae Conta'
      'FROM osfrmcab frc '
      'LEFT JOIN oscab cab ON cab.Codigo=frc.Codigo '
      'WHERE cab.OSFlhGrCab>0'
      'AND frc.OSFFCbMae > 0 '
      'ORDER BY OSFFCbMae')
    Left = 604
    Top = 396
    object QrAtzSrcConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrLastAd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(cab.DtaExePrv) DtaExePrv'
      'FROM ospipmon opm'
      'LEFT JOIN oscab cab ON cab.Codigo=opm.Codigo'
      'WHERE opm.OSFMCbMae=58')
    Left = 600
    Top = 448
    object QrLastAdDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
    end
  end
  object QrErrOMC: TmySQLQuery
    Database = Dmod.MyDB
    Left = 268
    Top = 176
    object QrErrOMCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrErrOMCControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrErrOMCConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrErrOMCNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object frxGER_OSERV_030_002_B: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  //MePagina.Visible := <VARF_PAGINAR>;  '
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxGER_OSERV_030_002_AGetValue
    Left = 452
    Top = 268
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsErrOMC
        DataSetName = 'frxDsErrOMC'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 68.031540000000010000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'F'#243'rmulas de Monitoramento Incompletas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Top = 49.133890000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 94.488250000000000000
          Top = 49.133890000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Servi'#231'o')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 188.976500000000000000
          Top = 49.133890000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID F'#243'rm. Monitoram.')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 283.464750000000000000
          Top = 49.133890000000010000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome F'#243'rmula')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 147.401670000000000000
        Width = 680.315400000000000000
        DataSet = frxDsErrOMC
        DataSetName = 'frxDsErrOMC'
        RowCount = 0
        object Memo5: TfrxMemoView
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'Codigo'
          DataSet = frxDsErrOMC
          DataSetName = 'frxDsErrOMC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrOMC."Codigo"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 94.488250000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'Controle'
          DataSet = frxDsErrOMC
          DataSetName = 'frxDsErrOMC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrOMC."Controle"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 188.976500000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'Conta'
          DataSet = frxDsErrOMC
          DataSetName = 'frxDsErrOMC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrOMC."Conta"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 283.464750000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          DataField = 'Nome'
          DataSet = frxDsErrOMC
          DataSetName = 'frxDsErrOMC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrOMC."Nome"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsErrOMC: TfrxDBDataset
    UserName = 'frxDsErrOMC'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'Nome=Nome')
    DataSet = QrErrOMC
    BCDToCurrency = False
    Left = 328
    Top = 176
  end
  object frxGER_OSERV_030_001_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  //MePagina.Visible := <VARF_PAGINAR>;  '
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxGER_OSERV_030_002_AGetValue
    Left = 452
    Top = 220
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsFrmFil
        DataSetName = 'frxDsFrmFil'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 68.031540000000010000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 759.685530000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'F'#243'rmulas de Monitoramento Filhas a Serem Criadas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 865.512370000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Top = 49.133890000000010000
          Width = 219.212598430000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 219.212740000000000000
          Top = 49.133890000000010000
          Width = 22.677165350000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TL')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 241.889920000000000000
          Top = 49.133890000000010000
          Width = 185.196850393700800000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lugar')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 427.086890000000000000
          Top = 49.133890000000010000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Localiz.ador')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 480.000310000000000000
          Top = 49.133890000000010000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID Servi;o')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 536.693260000000000000
          Top = 49.133890000000010000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID F. A/M')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 593.386210000000000000
          Top = 49.133890000000010000
          Width = 162.519685040000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'F'#243'rmula')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 755.906000000000000000
          Top = 49.133890000000010000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            #218'lt. Emiss.')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 805.039890000000000000
          Top = 49.133890000000010000
          Width = 71.811023620000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 876.850960000000000000
          Top = 49.133890000000010000
          Width = 52.913383390000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per. DD')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 929.764380000000000000
          Top = 49.133890000000010000
          Width = 41.574803150000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Intrv. DD')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 147.401670000000000000
        Width = 971.339210000000000000
        DataSet = frxDsFrmFil
        DataSetName = 'frxDsFrmFil'
        RowCount = 0
        object Memo5: TfrxMemoView
          Width = 219.212598430000000000
          Height = 18.897650000000000000
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFrmFil."Entidade"] - [frxDsFrmFil."NO_CLIENTE"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 219.212740000000000000
          Width = 22.677165350000000000
          Height = 18.897650000000000000
          DataField = 'SIGLA_TAB'
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFrmFil."SIGLA_TAB"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 241.889920000000000000
          Width = 185.196850393700800000
          Height = 18.897650000000000000
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFrmFil."SiapTerCad"] - [frxDsFrmFil."NO_STC"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 427.086890000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataField = 'Codigo'
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFrmFil."Codigo"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 480.000310000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'Controle'
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFrmFil."Controle"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 536.693260000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'Conta'
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFrmFil."Conta"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 593.386210000000000000
          Width = 162.519685040000000000
          Height = 18.897650000000000000
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFrmFil."CO_FORMULA"] - [frxDsFrmFil."NO_FORMULA"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 755.906000000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataField = 'EmisUltDta'
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFrmFil."EmisUltDta"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 805.039890000000000000
          Width = 71.811023622047240000
          Height = 18.897650000000000000
          DataField = 'NO_EmisStatus'
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFrmFil."NO_EmisStatus"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 876.850960000000000000
          Width = 52.913383390000000000
          Height = 18.897650000000000000
          DataField = 'Periodd'
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFrmFil."Periodd"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 929.764380000000000000
          Width = 41.574803150000000000
          Height = 18.897650000000000000
          DataField = 'DdPostero'
          DataSet = frxDsFrmFil
          DataSetName = 'frxDsFrmFil'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFrmFil."DdPostero"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 226.771800000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 971.339210000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 646.299630000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsFrmFil: TfrxDBDataset
    UserName = 'frxDsFrmFil'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataSincOS=DataSincOS'
      'NO_CLIENTE=NO_CLIENTE'
      'NO_EmisStatus=NO_EmisStatus'
      'NO_STC=NO_STC'
      'NO_FORMULA=NO_FORMULA'
      'Entidade=Entidade'
      'SiapTerCad=SiapTerCad'
      'DtaExeFim=DtaExeFim'
      'NumContrat=NumContrat'
      'DtaPrxRenw=DtaPrxRenw'
      'Tabela=Tabela'
      'SIGLA_TAB=SIGLA_TAB'
      'IDSuperior=IDSuperior'
      'EmisUltDta=EmisUltDta'
      'Periodd=Periodd'
      'EmisStatus=EmisStatus'
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'CO_FORMULA=CO_FORMULA'
      'DdPostero=DdPostero')
    DataSet = QrFrmFil
    BCDToCurrency = False
    Left = 16
    Top = 180
  end
  object QrIncompl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT cun.DataSincOS, xxx.Codigo, xxx.Controle, xxx.Conta, xxx.' +
        'DdPostero,'
      'cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw,'
      'xxx.IDIts IDSuperior,'
      'xxx.EmisUltDta, xxx.Periodd, xxx.EmisStatus'
      'FROM osfrmflhcb xxx'
      'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo'
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat'
      'LEFT JOIN cunscad cun ON cun.Codigo=cab.Entidade'
      'WHERE xxx.EmisStatus < 2'
      'AND xxx.Ativo > 0'
      'AND xxx.EmisUltDta < SYSDATE()'
      'AND cab.DtaExeFim > "1900-01-01"'
      'AND'
      '('
      '     cab.NumContrat=0'
      '     OR'
      '     ('
      '          ctr.DtaCntrFim < "1900-01-01"'
      '     )'
      ')'
      'AND (cab.NumContrat=0 AND xxx.Periodd=0)'
      ''
      'UNION'
      ''
      
        'SELECT cun.DataSincOS, xxx.Codigo, xxx.Controle, xxx.Conta, xxx.' +
        'DdPostero,'
      'cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw,'
      'xxx.Conta IDSuperior,'
      'xxx.EmisUltDta, xxx.Periodd, xxx.EmisStatus'
      'FROM osmoncab xxx'
      'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo'
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat'
      'LEFT JOIN cunscad cun ON cun.Codigo=cab.Entidade'
      'WHERE xxx.EmisStatus < 2'
      'AND xxx.EmisUltDta < SYSDATE()'
      'AND cab.DtaExeFim > "1900-01-01"'
      'AND'
      '('
      '     cab.NumContrat=0'
      '     OR'
      '     ('
      '          ctr.DtaCntrFim < "1900-01-01"'
      '     )'
      ')'
      'AND (cab.NumContrat=0 AND xxx.Periodd=0)'
      ''
      'ORDER BY Codigo, Controle, Conta')
    Left = 600
    Top = 496
    object QrIncomplCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIncomplControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrIncomplConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrIncomplDdPostero: TIntegerField
      FieldName = 'DdPostero'
      Required = True
    end
    object QrIncomplDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrIncomplNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrIncomplDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
    end
    object QrIncomplIDSuperior: TIntegerField
      FieldName = 'IDSuperior'
      Required = True
    end
    object QrIncomplEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
      Required = True
    end
    object QrIncomplPeriodd: TIntegerField
      FieldName = 'Periodd'
      Required = True
    end
    object QrIncomplEmisStatus: TSmallintField
      FieldName = 'EmisStatus'
      Required = True
    end
    object QrIncomplNO_TAB: TWideStringField
      DisplayWidth = 10
      FieldName = 'SIGLA_TAB'
      Size = 10
    end
    object QrIncomplFaltaPip: TWideStringField
      FieldName = 'FaltaPip'
      Size = 10
    end
    object QrIncomplTabela: TLargeintField
      FieldName = 'Tabela'
    end
    object QrIncomplExtenDd: TIntegerField
      FieldName = 'ExtenDd'
    end
  end
  object frxGER_OSERV_030_002_C: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  //MePagina.Visible := <VARF_PAGINAR>;  '
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxGER_OSERV_030_002_AGetValue
    Left = 452
    Top = 316
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsIncompl
        DataSetName = 'frxDsIncompl'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 68.031540000000010000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'F'#243'rmulas de Monitoramento Incompletas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Top = 49.133890000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 94.488250000000000000
          Top = 49.133890000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Servi'#231'o')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 188.976500000000000000
          Top = 49.133890000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID F'#243'rm. Monitoram.')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 283.464750000000000000
          Top = 49.133890000000010000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome F'#243'rmula')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 309.921460000000000000
          Top = 49.133890000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID m'#227'e')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 404.409710000000000000
          Top = 49.133890000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo Dd')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 498.897960000000000000
          Top = 49.133890000000010000
          Width = 181.417440000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Falta PMV?')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 147.401670000000000000
        Width = 680.315400000000000000
        DataSet = frxDsIncompl
        DataSetName = 'frxDsIncompl'
        RowCount = 0
        object Memo5: TfrxMemoView
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'Codigo'
          DataSet = frxDsIncompl
          DataSetName = 'frxDsIncompl'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsIncompl."Codigo"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 94.488250000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'Controle'
          DataSet = frxDsIncompl
          DataSetName = 'frxDsIncompl'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsIncompl."Controle"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 188.976500000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'Conta'
          DataSet = frxDsIncompl
          DataSetName = 'frxDsIncompl'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsIncompl."Conta"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 283.464750000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DataField = 'SIGLA_TAB'
          DataSet = frxDsIncompl
          DataSetName = 'frxDsIncompl'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsIncompl."SIGLA_TAB"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 309.921460000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'IDSuperior'
          DataSet = frxDsIncompl
          DataSetName = 'frxDsIncompl'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsIncompl."IDSuperior"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 404.409710000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'Periodd'
          DataSet = frxDsIncompl
          DataSetName = 'frxDsIncompl'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsIncompl."Periodd"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 498.897960000000000000
          Width = 181.417440000000000000
          Height = 18.897650000000000000
          DataField = 'FaltaPip'
          DataSet = frxDsIncompl
          DataSetName = 'frxDsIncompl'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsIncompl."FaltaPip"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsIncompl: TfrxDBDataset
    UserName = 'frxDsIncompl'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataSincOS=DataSincOS'
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'DdPostero=DdPostero'
      'DtaExeFim=DtaExeFim'
      'NumContrat=NumContrat'
      'DtaPrxRenw=DtaPrxRenw'
      'IDSuperior=IDSuperior'
      'EmisUltDta=EmisUltDta'
      'Periodd=Periodd'
      'EmisStatus=EmisStatus'
      'Tabela=Tabela'
      'SIGLA_TAB=SIGLA_TAB'
      'FaltaPip=FaltaPip')
    DataSet = QrIncompl
    BCDToCurrency = False
    Left = 664
    Top = 496
  end
  object PMGrid1: TPopupMenu
    OnPopup = PMGrid1Popup
    Left = 584
    Top = 136
    object Cliente1: TMenuItem
      Caption = 'Cadastro de Cliente'
      OnClick = Cliente1Click
    end
    object OS1: TMenuItem
      Caption = 'Gerenciamento de OS'
      OnClick = OS1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AlteradatadaltimaemissodeOSsfilhas1: TMenuItem
      Caption = 'Altera data da '#250'ltima emiss'#227'o de OSs filhas'
      OnClick = AlteradatadaltimaemissodeOSsfilhas1Click
    end
  end
  object QrSTCsemEMP: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT sse.*, stc.Nome, stc.Cliente,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE'
      'FROM _stc_sem_emp sse'
      'LEFT JOIN bugstrol.siaptercad stc ON stc.Codigo=sse.SiapTerCad'
      'LEFT JOIN bugstrol.Entidades ent ON ent.Codigo=stc.Cliente')
    Left = 384
    Top = 360
    object QrSTCsemEMPSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrSTCsemEMPEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSTCsemEMPAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSTCsemEMPNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSTCsemEMPCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSTCsemEMPNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
  end
  object frxDsSTCsemEMP: TfrxDBDataset
    UserName = 'frxDsSTCsemEMP'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SiapTerCad=SiapTerCad'
      'Empresa=Empresa'
      'Ativo=Ativo'
      'Nome=Nome'
      'Cliente=Cliente'
      'NO_CLIENTE=NO_CLIENTE')
    DataSet = QrSTCsemEMP
    BCDToCurrency = False
    Left = 412
    Top = 360
  end
  object frxGER_OSERV_030_001_B: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  //MePagina.Visible := <VARF_PAGINAR>;  '
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxGER_OSERV_030_002_AGetValue
    Left = 452
    Top = 360
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSTCsemEMP
        DataSetName = 'frxDsSTCsemEMP'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 68.031540000000010000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Lugares sem data de refer'#234'ncia para gera'#231#227'o de OSs autom'#225'ticas (' +
              'Padr'#245'es de OSs filhas)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 317.480520000000000000
          Top = 49.133890000000010000
          Width = 317.480314960000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lugar')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 49.133890000000010000
          Width = 317.480520000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 634.961040000000000000
          Top = 49.133890000000010000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 147.401670000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSTCsemEMP
        DataSetName = 'frxDsSTCsemEMP'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 317.480520000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'SiapTerCad'
          DataSet = frxDsSTCsemEMP
          DataSetName = 'frxDsSTCsemEMP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCsemEMP."SiapTerCad"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'Cliente'
          DataSet = frxDsSTCsemEMP
          DataSetName = 'frxDsSTCsemEMP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCsemEMP."Cliente"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 60.472480000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          DataField = 'NO_CLIENTE'
          DataSet = frxDsSTCsemEMP
          DataSetName = 'frxDsSTCsemEMP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSTCsemEMP."NO_CLIENTE"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 634.961040000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'Empresa'
          DataSet = frxDsSTCsemEMP
          DataSetName = 'frxDsSTCsemEMP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCsemEMP."Empresa"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 377.953000000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          DataField = 'Nome'
          DataSet = frxDsSTCsemEMP
          DataSetName = 'frxDsSTCsemEMP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSTCsemEMP."Nome"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrNaoRatif: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT cab.Codigo, cab.SiapterCad,'
      'stc.Nome NO_STC, cab.Entidade, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLI'
      'FROM osmonrec omr'
      'LEFT JOIN oscab cab ON cab.Codigo=omr.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapterCad'
      'WHERE omr.RatifUso=0 '
      'AND cab.DtaExeFim > "1900-01-01"')
    Left = 384
    Top = 412
    object QrNaoRatifCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNaoRatifSiapterCad: TIntegerField
      FieldName = 'SiapterCad'
    end
    object QrNaoRatifNO_STC: TWideStringField
      FieldName = 'NO_STC'
      Size = 100
    end
    object QrNaoRatifEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrNaoRatifNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
  end
  object frxGER_OSERV_030_001_C: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  //MePagina.Visible := <VARF_PAGINAR>;  '
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxGER_OSERV_030_002_AGetValue
    Left = 452
    Top = 412
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNaoRatif
        DataSetName = 'frxDsNaoRatif'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 68.031540000000010000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'OSs com f'#243'rmulas de monitoramento com consumos n'#227'o ratificados')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 362.834880000000000000
          Top = 49.133890000000010000
          Width = 317.480314960000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lugar')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 45.354360000000000000
          Top = 49.133890000000010000
          Width = 317.480520000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Top = 49.133890000000010000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OS')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 147.401670000000000000
        Width = 680.315400000000000000
        DataSet = frxDsNaoRatif
        DataSetName = 'frxDsNaoRatif'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 362.834880000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'SiapterCad'
          DataSet = frxDsNaoRatif
          DataSetName = 'frxDsNaoRatif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNaoRatif."SiapterCad"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 45.354360000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'Entidade'
          DataSet = frxDsNaoRatif
          DataSetName = 'frxDsNaoRatif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNaoRatif."Entidade"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 105.826840000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          DataField = 'NO_CLI'
          DataSet = frxDsNaoRatif
          DataSetName = 'frxDsNaoRatif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNaoRatif."NO_CLI"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'Codigo'
          DataSet = frxDsNaoRatif
          DataSetName = 'frxDsNaoRatif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNaoRatif."Codigo"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 423.307360000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          DataField = 'NO_STC'
          DataSet = frxDsNaoRatif
          DataSetName = 'frxDsNaoRatif'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNaoRatif."NO_STC"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsNaoRatif: TfrxDBDataset
    UserName = 'frxDsNaoRatif'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'SiapterCad=SiapterCad'
      'NO_STC=NO_STC'
      'Entidade=Entidade'
      'NO_CLI=NO_CLI')
    DataSet = QrNaoRatif
    BCDToCurrency = False
    Left = 412
    Top = 412
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppc.MaxDesco, ppc.Parcelas,'
      'ppc.Codigo, ppc.CodUsu, ppc.Nome'
      'FROM pediprzcab ppc'
      'WHERE ppc.Codigo=3')
    Left = 164
    Top = 480
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
  end
  object QrOSCabAlv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT oca.*, IF(oca.Praga_Z <> 0, '#39'E'#39', '#39'G'#39') NO_NIVEL, '
      'IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) NO_PRAGA'
      'FROM oscabalv oca'
      'LEFT JOIN praga_a pca ON pca.Codigo=oca.Praga_A'
      'LEFT JOIN praga_z pcz ON pcz.Codigo=oca.Praga_Z'
      'WHERE oca.Codigo=:P0'
      'ORDER BY oca.Ordem')
    Left = 160
    Top = 528
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSCabAlvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabAlvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSCabAlvOrdem: TSmallintField
      FieldName = 'Ordem'
    end
    object QrOSCabAlvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCabAlvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCabAlvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCabAlvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCabAlvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCabAlvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCabAlvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCabAlvPraga_A: TIntegerField
      FieldName = 'Praga_A'
    end
    object QrOSCabAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
    end
    object QrOSCabAlvNO_NIVEL: TWideStringField
      FieldName = 'NO_NIVEL'
      Required = True
      Size = 1
    end
    object QrOSCabAlvNO_PRAGA: TWideStringField
      FieldName = 'NO_PRAGA'
      Size = 60
    end
  end
  object QrPIP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pip.OSMonCab, pip.Codigo, pip.Ordem,'
      'pip.PrgLstCab'
      'FROM pipcad pip'
      'LEFT JOIN osmoncab omc ON omc.PipCad=pip.Codigo'
      'WHERE omc.Conta=1353')
    Left = 672
    Top = 4
    object QrPIPOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrPIPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPIPOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPIPPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 608
    Top = 19
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 580
    Top = 19
  end
end
