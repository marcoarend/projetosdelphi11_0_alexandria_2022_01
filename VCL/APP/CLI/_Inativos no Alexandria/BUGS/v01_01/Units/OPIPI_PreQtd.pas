unit OPIPI_PreQtd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, UnDmkProcFunc, UnDmkEnums;

type
  TFmOPIPI_PreQtd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    TbOPIPI_PreQtd: TmySQLTable;
    DBGrid1: TDBGrid;
    DsOPIPI_PreQtd: TDataSource;
    TbOPIPI_PreQtdConta: TLargeintField;
    TbOPIPI_PreQtdNO_PIP: TWideStringField;
    TbOPIPI_PreQtdNO_Pergunta: TWideStringField;
    TbOPIPI_PreQtdRespQtd: TIntegerField;
    TbOPIPI_PreQtdCodigo: TIntegerField;
    TbOPIPI_PreQtdFiliacao: TIntegerField;
    TbOPIPI_PreQtdPipCad: TIntegerField;
    QrOSPipIts: TmySQLQuery;
    QrOSPipItsCodigo: TIntegerField;
    QrOSPipItsControle: TIntegerField;
    QrOSPipItsConta: TLargeintField;
    QrOSPipItsPrgLstCab: TIntegerField;
    QrOSPipItsPrgLstIts: TIntegerField;
    QrOSPipItsSobreOrd: TIntegerField;
    QrOSPipItsOrdem: TIntegerField;
    QrOSPipItsSubOrdem: TIntegerField;
    QrOSPipItsTabela: TSmallintField;
    QrOSPipItsTabIdx: TLargeintField;
    QrOSPipItsGraGruX: TIntegerField;
    QrOSPipItsFuncoes: TSmallintField;
    QrOSPipItsDependente: TSmallintField;
    QrOSPipItsPrgAtrCad: TIntegerField;
    QrOSPipItsFiliacao: TIntegerField;
    QrOSPipItsRelacao: TIntegerField;
    QrOSPipItsNivel: TIntegerField;
    QrOSPipItsNivSeq: TIntegerField;
    QrOSPipItsPergunta: TIntegerField;
    QrOSPipItsBinarCad0: TIntegerField;
    QrOSPipItsBinarCad1: TIntegerField;
    QrOSPipItsAcaoPrd: TSmallintField;
    QrOSPipItsRespondido: TSmallintField;
    QrOSPipItsRespBin: TSmallintField;
    QrOSPipItsRespQtd: TFloatField;
    QrOSPipItsRespAtrCad: TIntegerField;
    QrOSPipItsRespAtrIts: TIntegerField;
    QrOSPipItsRespAtrTxt: TWideStringField;
    QrOSPipItsRespTxtLvr: TWideMemoField;
    QrOSPipItsRespPrdBin: TSmallintField;
    QrOSPipItsLk: TIntegerField;
    QrOSPipItsDataCad: TDateField;
    QrOSPipItsDataAlt: TDateField;
    QrOSPipItsUserCad: TIntegerField;
    QrOSPipItsUserAlt: TIntegerField;
    QrOSPipItsAlterWeb: TSmallintField;
    QrOSPipItsAtivo: TSmallintField;
    QrOSPipItsLupForma: TSmallintField;
    QrOSPipItsLupQtdVzs: TSmallintField;
    QrOSPipItsLupSeqRsp: TIntegerField;
    QrOSPipItsLupInfVzs: TSmallintField;
    QrOSPipItsRespMetodo: TSmallintField;
    QrOSPipItsSuperOrd: TIntegerField;
    TbOPIPI_PreQtdPrgLstIts: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure TbOPIPI_PreQtdBeforePost(DataSet: TDataSet);
    procedure TbOPIPI_PreQtdAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmOPIPI_PreQtd: TFmOPIPI_PreQtd;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UMySQLModule;

{$R *.DFM}

procedure TFmOPIPI_PreQtd.BtOKClick(Sender: TObject);
  function InserePerguntaPipIts(SuperSub, LupSeqRsp: Integer): Boolean;
  var
    Codigo, Controle, PrgLstCab, PrgLstIts, Ordem, Funcoes, Dependente,
    PrgAtrCad, Filiacao, Relacao, Nivel, NivSeq, Pergunta, BinarCad0, BinarCad1,
    AcaoPrd, LupForma, LupQtdVzs, SuperOrd, SobreOrd, SubOrdem, Tabela, GraGruX
    : Integer;
    Conta, TabIdx: Int64;
  begin
    //
    Codigo         := QrOsPipItsCodigo.Value;
    Controle       := QrOsPipItsControle.Value;
    Conta          := 0;
    PrgLstCab      := QrOsPipItsPrgLstCab.Value;
    PrgLstIts      := QrOsPipItsPrgLstIts.Value;
    Ordem          := QrOsPipItsOrdem.Value;
    //SubOrdem       := ;
    //Tabela         := ;
    //GraGruX        := ;
    Funcoes        := QrOsPipItsFuncoes.Value;
    Dependente     := QrOsPipItsDependente.Value;
    PrgAtrCad      := QrOsPipItsPrgAtrCad.Value;
    Filiacao       := QrOsPipItsFiliacao.Value;
    Relacao        := QrOsPipItsRelacao.Value;
    Nivel          := QrOsPipItsNivel.Value;
    NivSeq         := QrOsPipItsNivSeq.Value;
    Pergunta       := QrOsPipItsPergunta.Value;
    BinarCad0      := QrOsPipItsBinarCad0.Value;
    BinarCad1      := QrOsPipItsBinarCad1.Value;
    AcaoPrd        := QrOsPipItsAcaoPrd.Value;
    TabIdx         := QrOsPipItsTabIdx.Value;
    LupForma       := QrOsPipItsLupForma.Value;
    LupQtdVzs      := QrOsPipItsLupQtdVzs.Value;
    SuperOrd       := QrOsPipItsSuperOrd.Value;
    SobreOrd       := QrOsPipItsSobreOrd.Value;
    SubOrdem       := QrOsPipItsSubOrdem.Value;
    Tabela         := QrOsPipItsTabela.Value;
    GraGruX        := QrOsPipItsGraGruX.Value;
    //
    Conta := UMyMod.BPGS1I64_Reaproveita('ospipits', 'Conta', '', '', tsPos, stIns, Conta);
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ospipits', False, [
    'Codigo', 'Controle', 'PrgLstCab',
    'PrgLstIts', 'SuperOrd', 'SobreOrd',
    'Ordem', 'SubOrdem', 'Tabela',
    'GraGruX', 'Funcoes', 'Dependente',
    'PrgAtrCad', 'Filiacao', 'Relacao',
    'Nivel', 'NivSeq', 'Pergunta',
    'BinarCad0', 'BinarCad1', 'AcaoPrd',
    'TabIdx', 'LupForma', 'LupQtdVzs',
    'LupSeqRsp', 'SuperSub'], [
    'Conta'], [
    Codigo, Controle, PrgLstCab,
    PrgLstIts, SuperOrd, SobreOrd,
    Ordem, SubOrdem, Tabela,
    GraGruX, Funcoes, Dependente,
    PrgAtrCad, Filiacao, Relacao,
    Nivel, NivSeq, Pergunta,
    BinarCad0, BinarCad1, AcaoPrd,
    TabIdx, LupForma, LupQtdVzs,
    LupSeqRsp, SuperSub], [
    Conta], True);
  end;
  //
  procedure AtualizaSeqPrimeiro(OldSeq, NewSeq: Integer);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ospipits opi, ospipmon opm ',
    'SET opi.LupSeqRsp=' + Geral.FF0(NewSeq),
    ', opi.SuperSub=' + Geral.FF0(NewSeq),
    'WHERE opi.Controle=opm.Controle ',
    'AND opi.Codigo=' + Geral.FF0(TbOPIPI_PreQtdCodigo.Value),
    'AND opi.Filiacao=' + Geral.FF0(TbOPIPI_PreQtdPrgLstIts.Value),
    'AND opm.PipCad=' + Geral.FF0(TbOPIPI_PreQtdPipCad.Value),
    'AND opi.LupSeqRsp=' + Geral.FF0(OldSeq),
    '']);
  end;
const
  LupInfVzs = 1;
var
  RespQtd, OldSeq, NewSeq, I, SuperSub: Integer;
  Conta: Int64;
begin
  TbOPIPI_PreQtd.DisableControls;
  TbOPIPI_PreQtd.First;
  while not TbOPIPI_PreQtd.Eof do
  begin
    RespQtd := TbOPIPI_PreQtdRespQtd.Value;
    Conta   := TbOPIPI_PreQtdConta.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ospipits', False, [
    'RespQtd', 'LupInfVzs'], [
    'Conta'], [
    RespQtd, LupInfVzs], [
    Conta], True);
    //
    if RespQtd = 0 then
    begin
      OldSeq := 1;
      NewSeq := 0;
      AtualizaSeqPrimeiro(OldSeq, NewSeq);
    end else
    begin
      OldSeq := 0;
      NewSeq := 1;
      AtualizaSeqPrimeiro(OldSeq, NewSeq);
      //
      if RespQtd > 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrOSPipIts, Dmod.MyDB, [
        'SELECT opi.*',
        'FROM ospipits opi ',
        'LEFT JOIN ospipmon  opm ON opm.Controle=opi.Controle ',
        'LEFT JOIN pipcad    pip ON pip.Codigo=opm.PipCad  ',
        'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta ',
        'WHERE opi.Codigo=' + Geral.FF0(TbOPIPI_PreQtdCodigo.Value),
        'AND opi.Filiacao=' + Geral.FF0(TbOPIPI_PreQtdPrgLstIts.Value),
        'AND opm.PipCad=' + Geral.FF0(TbOPIPI_PreQtdPipCad.Value),
        'AND opi.LupSeqRsp=1',
        '',
        'ORDER BY opi.Controle, opi.SobreOrd, opi.Ordem, opi.SubOrdem',
        '']);
        for I := 2 to RespQtd do
        begin
          QrOSPipIts.First;
          while not QrOSPipIts.Eof  do
          begin
            InserePerguntaPipIts(I, I);
            //
            QrOSPipIts.Next;
          end;
        end;
      end
    end;
    TbOPIPI_PreQtd.Next;
  end;
  //
  Close;
end;

procedure TFmOPIPI_PreQtd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOPIPI_PreQtd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOPIPI_PreQtd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreTable(TbOPIPI_PreQtd, DmodG.MyPID_DB);
end;

procedure TFmOPIPI_PreQtd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOPIPI_PreQtd.TbOPIPI_PreQtdAfterPost(DataSet: TDataSet);
begin
  if TbOPIPI_PreQtdConta.Value = 0 then
    TbOPIPI_PreQtd.Delete;
end;

procedure TFmOPIPI_PreQtd.TbOPIPI_PreQtdBeforePost(DataSet: TDataSet);
const
  MaxVal = 10;
begin
  if TbOPIPI_PreQtdRespQtd.Value > MaxVal then
  begin
    TbOPIPI_PreQtdRespQtd.Value := MaxVal;
    //
    Geral.MB_Aviso('A quantidade m�xima de itens permitidos � 10!');
  end;
end;

end.
