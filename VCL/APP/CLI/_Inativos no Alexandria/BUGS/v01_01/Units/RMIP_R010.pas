unit RMIP_R010;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  dmkGeral, UnInternalConsts, frxClass, Data.DB, mySQLDbTables, frxDBSet;

type
  TFmRMIP_R010 = class(TForm)
    frxReport010A: TfrxReport;
    Qr010Uso: TmySQLQuery;
    Qr010UsoGraGru1: TIntegerField;
    Qr010UsoNO_GG1: TWideStringField;
    Qr010UsoSIGLA_USO: TWideStringField;
    Qr010UsoUsoQtd: TFloatField;
    frxDs010Uso: TfrxDBDataset;
    procedure FormCreate(Sender: TObject);
    procedure frxReport010AGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FRMIP_010Uso: String;
  public
    { Public declarations }
    FEmpresa, FCliente, FItemRel: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    //
    procedure GeraImp_TotaisProdutosUsados();
    procedure frxReport000GetValue(frxReport: TfrxReport;
              const VarName: string; var Value: Variant);
  end;

var
  FmRMIP_R010: TFmRMIP_R010;

implementation

uses CreateBugs, ModuleGeral, DmkDAC_PF, UnDmkProcFunc, UnMyObjects;

{$R *.dfm}

procedure TFmRMIP_R010.FormCreate(Sender: TObject);
begin
  FRMIP_010Uso :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_RMIP_010Uso, DmodG.QrUpdPID1, False);
end;

procedure TFmRMIP_R010.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
begin
//
end;

procedure TFmRMIP_R010.frxReport010AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport010A, VarName, Value);
end;

procedure TFmRMIP_R010.GeraImp_TotaisProdutosUsados();
var
  SQL_Cli, SQL_Dta, SQL: String;
  Ini, Fim: String;
  Continua: Boolean;
begin
  Ini := Geral.FDT(FDtaIni, 1);
  Fim := Geral.FDT(FDtaFIm, 1);
  SQL_Dta := 'AND cab.DtaExeFim BETWEEN "' + Ini + '" AND "' + Fim + '" ';
  if FCliente = 0 then
    SQL_Cli := ''
  else
    SQL_Cli := 'AND cab.Entidade=' + Geral.FF0(FCliente);
  //
  SQL := Geral.ATS([
  'DELETE FROM  ' + FRMIP_010Uso + ';',
  'INSERT INTO ' + FRMIP_010Uso,
  '',
  'SELECT 1 ApMo, rec.Codigo, rec.Controle,  ',
  'rec.Conta, rec.IDIts,  ',
  'ggx.GraGru1, ggx.Controle GraGruX,  ',
  'ofc.QtdTot, Diluente, rec.UsoQtd UsoQtd, EhDiluente,  ',
  'gg1.Nome NO_GG1, me1.Sigla SIGLA_USO ',
  'FROM ' + TMeuDB + '.osfrmrec rec  ',
  'LEFT JOIN ' + TMeuDB + '.oscab     cab ON cab.Codigo=rec.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.osfrmcab  ofc ON ofc.Conta=rec.Conta  ',
  'LEFT JOIN ' + TMeuDB + '.gragrux   ggx ON ggx.Controle=rec.GraGruX  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.unidmed   me1 ON me1.Codigo=gg1.UnidMed ',
  'WHERE rec.UsoQtd > 0 ',
  SQL_Dta,
  SQL_Cli,
  '  ',
  'UNION  ',
  '  ',
  'SELECT 2 ApMo, rec.Codigo, rec.Controle,  ',
  'rec.Conta, rec.IDIts,  ',
  'ggx.GraGru1, ggx.Controle GraGruX,  ',
  'ofc.QtdTot, Diluente, rec.UsoQtd UsoQtd, EhDiluente,  ',
  'gg1.Nome NO_GG1, me1.Sigla SIGLA_USO ',
  'FROM ' + TMeuDB + '.osmonrec rec  ',
  'LEFT JOIN ' + TMeuDB + '.oscab     cab ON cab.Codigo=rec.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.osmoncab  ofc ON ofc.Conta=rec.Conta  ',
  'LEFT JOIN ' + TMeuDB + '.gragrux   ggx ON ggx.Controle=rec.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.unidmed   me1 ON me1.Codigo=gg1.UnidMed ',
  'WHERE rec.UsoQtd > 0 ',
  SQL_Dta,
  SQL_Cli,
  '  ',
  'UNION  ',
  '  ',
  'SELECT 1 ApMo, rec.Codigo, rec.Controle,  ',
  'rec.Conta, rec.IDIts, ',
  'ggx.GraGru1, ggx.Controle GraGruX,  ',
  '0 QtdTot, ofc.Diluente, rec.UsoQtd UsoQtd, EhDiluente,  ',
  'gg1.Nome NO_GG1, me1.Sigla SIGLA_USO ',
  'FROM ' + TMeuDB + '.ospipitspr rec  ',
  'LEFT JOIN ' + TMeuDB + '.oscab     cab ON cab.Codigo=rec.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.ospipmon  opm ON opm.Controle=rec.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.pipcad    pip ON pip.Codigo=opm.PipCad  ',
  'LEFT JOIN ' + TMeuDB + '.osmoncab  ofc ON ofc.Conta=pip.OSMonCab  ',
  'LEFT JOIN ' + TMeuDB + '.gragrux   ggx ON ggx.Controle=rec.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.unidmed   me1 ON me1.Codigo=gg1.UnidMed ',
  'WHERE rec.UsoQtd > 0 ',
  SQL_Dta,
  SQL_Cli,
  ';']);
  //Geral.MB_Aviso(SQL);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  //
  UnDmkDAC_PF.AbreQuery(Qr010Uso, DModG.MyPID_DB);
  //
  frxReport010A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport010A.Variables['VARF_DATA']    := FDtaImp;
  frxReport010A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport010A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  frxReport010A.Variables['VARF_PERIODO']  := QuotedStr(dmkPF.PeriodoImp(FDtaIni,
    FDtaFim, 0, 0, True, True, False, False, '', ''));
  //
  MyObjects.frxDefineDataSets(frxReport010A, [
  frxDs010Uso
  ]);
  //MyObjects.frxPrepara(frxReport010A, 'Quantidades Totais de Produtos Utilizados');
end;

end.
