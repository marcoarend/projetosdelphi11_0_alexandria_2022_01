object FmRMIP_R012: TFmRMIP_R012
  Left = 0
  Top = 0
  Caption = 'RMIP 012'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Qr012OSPrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prv.*, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, '
      'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_Agente, '
      'eco.Nome NO_Contato, fct.Nome NO_FormContat  '
      'FROM osprv prv '
      'LEFT JOIN entidades cli ON cli.Codigo=prv.Cliente '
      'LEFT JOIN entidades age ON age.Codigo=prv.Agente '
      'LEFT JOIN enticontat eco ON eco.Controle=prv.Contato '
      'LEFT JOIN formcontat fct ON fct.Codigo=prv.FormContat '
      ' '
      ' ')
    Left = 40
    Top = 12
    object Qr012OSPrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr012OSPrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr012OSPrvDtHrContat: TDateTimeField
      FieldName = 'DtHrContat'
    end
    object Qr012OSPrvFormContat: TIntegerField
      FieldName = 'FormContat'
    end
    object Qr012OSPrvCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object Qr012OSPrvContato: TIntegerField
      FieldName = 'Contato'
    end
    object Qr012OSPrvAgente: TIntegerField
      FieldName = 'Agente'
    end
    object Qr012OSPrvNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object Qr012OSPrvNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object Qr012OSPrvNO_Agente: TWideStringField
      FieldName = 'NO_Agente'
      Size = 100
    end
    object Qr012OSPrvNO_Contato: TWideStringField
      FieldName = 'NO_Contato'
      Size = 30
    end
    object Qr012OSPrvNO_FormContat: TWideStringField
      FieldName = 'NO_FormContat'
      Size = 60
    end
    object Qr012OSPrvNO_PrvStatus: TWideStringField
      FieldName = 'NO_PrvStatus'
      Size = 60
    end
  end
  object frxReport012A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    Left = 40
    Top = 108
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDs012OSPrv
        DataSetName = 'frxDs012OSPrv'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 158.740260000000000000
          Top = 18.897650000000000000
          Width = 362.834880000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Provid'#234'ncias')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 521.575140000000100000
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente: [VARF_CLIENTE]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 332.598640000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 37.795278040000000000
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        DataSet = frxDs012OSPrv
        DataSetName = 'frxDs012OSPrv'
        RowCount = 0
        Stretched = True
        object Memo8: TfrxMemoView
          Left = 45.354360000000000000
          Top = 3.779530000000051000
          Width = 185.196970000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_Cliente'
          DataSet = frxDs012OSPrv
          DataSetName = 'frxDs012OSPrv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs012OSPrv."NO_Cliente"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 230.551330000000000000
          Top = 3.779530000000051000
          Width = 71.811070000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'DtHrContat'
          DataSet = frxDs012OSPrv
          DataSetName = 'frxDs012OSPrv'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs012OSPrv."DtHrContat"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 302.362400000000000000
          Top = 3.779529999999994000
          Width = 79.370130000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_FormContat'
          DataSet = frxDs012OSPrv
          DataSetName = 'frxDs012OSPrv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs012OSPrv."NO_FormContat"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 381.732530000000000000
          Top = 3.779530000000051000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_Contato'
          DataSet = frxDs012OSPrv
          DataSetName = 'frxDs012OSPrv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs012OSPrv."NO_Contato"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 457.323130000000000000
          Top = 3.779530000000051000
          Width = 94.488250000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_PrvStatus'
          DataSet = frxDs012OSPrv
          DataSetName = 'frxDs012OSPrv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs012OSPrv."NO_PrvStatus"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 551.811380000000000000
          Top = 3.779529999999994000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDs012OSPrv
          DataSetName = 'frxDs012OSPrv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs012OSPrv."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 612.283860000000000000
          Top = 3.779530000000051000
          Width = 68.031496060000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_Agente'
          DataSet = frxDs012OSPrv
          DataSetName = 'frxDs012OSPrv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs012OSPrv."NO_Agente"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Top = 3.779530000000051000
          Width = 45.354360000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Cliente'
          DataSet = frxDs012OSPrv
          DataSetName = 'frxDs012OSPrv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs012OSPrv."Cliente"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Top = 20.787404019999880000
          Width = 680.315400000000000000
          Height = 17.007874015748030000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Nome'
          DataSet = frxDs012OSPrv
          DataSetName = 'frxDs012OSPrv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs012OSPrv."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 20.787404020000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          Top = 3.779530000000051000
          Width = 230.551330000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 230.551330000000000000
          Top = 3.779530000000051000
          Width = 71.811070000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 302.362400000000000000
          Top = 3.779530000000051000
          Width = 79.370130000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma de contato')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 381.732530000000000000
          Top = 3.779530000000051000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Contato')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 457.323130000000000000
          Top = 3.779530000000051000
          Width = 94.488250000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 551.811380000000000000
          Top = 3.779529999999994000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'OS Atrelada')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 612.283860000000000000
          Top = 3.779530000000051000
          Width = 68.031496060000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        Height = 3.779530000000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDs012OSPrv: TfrxDBDataset
    UserName = 'frxDs012OSPrv'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'DtHrContat=DtHrContat'
      'FormContat=FormContat'
      'Cliente=Cliente'
      'Contato=Contato'
      'Agente=Agente'
      'Nome=Nome'
      'NO_Cliente=NO_Cliente'
      'NO_Agente=NO_Agente'
      'NO_Contato=NO_Contato'
      'NO_FormContat=NO_FormContat'
      'NO_PrvStatus=NO_PrvStatus')
    DataSet = Qr012OSPrv
    BCDToCurrency = False
    Left = 40
    Top = 60
  end
end
