unit OSSrvPre;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables;

type
  TFmOSSrvPre = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrDesServico: TmySQLQuery;
    QrDesServicoCodigo: TIntegerField;
    QrDesServicoNome: TWideStringField;
    DsDesServico: TDataSource;
    Label2: TLabel;
    EdDesServico: TdmkEditCB;
    CBDesServico: TdmkDBLookupComboBox;
    QrPreSrv: TmySQLQuery;
    QrPreSrvCodigo: TIntegerField;
    QrPreSrvControle: TIntegerField;
    QrPreSrvNome: TWideStringField;
    DsPreSrv: TDataSource;
    Label1: TLabel;
    EdPreSrv: TdmkEditCB;
    CBPreSrv: TdmkDBLookupComboBox;
    QrPreSrvGarantiaDd: TIntegerField;
    QrPreSrvHrEvacuar: TIntegerField;
    QrPreSrvHrExecutar: TFloatField;
    QrPreSrvValCalc: TFloatField;
    QrPreSrvValInfo: TFloatField;
    QrPreSrvValDesc: TFloatField;
    QrPreSrvValTota: TFloatField;
    QrPreSrvDetalhes: TWideMemoField;
    QrPreSrvMoniDdTotl: TIntegerField;
    QrPreSrvLk: TIntegerField;
    QrPreSrvDataCad: TDateField;
    QrPreSrvDataAlt: TDateField;
    QrPreSrvUserCad: TIntegerField;
    QrPreSrvUserAlt: TIntegerField;
    QrPreSrvAlterWeb: TSmallintField;
    QrPreSrvAtivo: TSmallintField;
    QrPreAlv: TmySQLQuery;
    QrPreAlvPraga_Z: TIntegerField;
    QrFormulas: TmySQLQuery;
    QrPreMon: TmySQLQuery;
    QrPreMonCodigo: TIntegerField;
    QrPreMonControle: TIntegerField;
    QrPreMonConta: TIntegerField;
    QrPreMonFormula: TIntegerField;
    QrPreMonLk: TIntegerField;
    QrPreMonDataCad: TDateField;
    QrPreMonDataAlt: TDateField;
    QrPreMonUserCad: TIntegerField;
    QrPreMonUserAlt: TIntegerField;
    QrPreMonAlterWeb: TSmallintField;
    QrPreMonAtivo: TSmallintField;
    QrPreMonNO_FORMULA: TWideStringField;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasLk: TIntegerField;
    QrFormulasDataCad: TDateField;
    QrFormulasDataAlt: TDateField;
    QrFormulasUserCad: TIntegerField;
    QrFormulasUserAlt: TIntegerField;
    QrFormulasAlterWeb: TSmallintField;
    QrFormulasAtivo: TSmallintField;
    QrFormulasEquipAplic: TIntegerField;
    QrFormulasQtdTot: TFloatField;
    QrFormulasQtdQSP: TFloatField;
    QrFormulasCusTot: TFloatField;
    QrFormulasAplicacao: TIntegerField;
    QrFormulasDiluente: TSmallintField;
    QrFormulasTipoAplica: TIntegerField;
    QrFormulasUnidMed: TIntegerField;
    QrPreSrvMoniDdIntv: TIntegerField;
    SpeedButton6: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdDesServicoChange(Sender: TObject);
    procedure EdDesServicoEnter(Sender: TObject);
    procedure EdDesServicoExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
  private
    { Private declarations }
    FlastDesServico: Integer;
    function  InsereOSSrv(var Controle: Integer): Boolean;
    procedure InserePragasAlvo(Controle: Integer);
    procedure InsereFrmAplic(Controle: Integer);
    procedure InsereFrmMonit(Controle: Integer);
    procedure ReopenPreSrv(Controle: Integer);
  public
    FOSCab: Integer;
    FQrOsSrv, FQrOSCab, FQrOSFrmCab, FQrOSFrmRec, FQrOSMonCab, FQrOSMonRec: TmySQLQuery;
    { Public declarations }
  end;

  var
  FmOSSrvPre: TFmOSSrvPre;

implementation

uses UnMyObjects, Module, DmkDAC_PF, OSSrv, MyDBCheck, UMySQLModule, OSFrmCab,
OSFrmRec, OSMonCabIns, UnOSApp_PF, Principal;

{$R *.DFM}

procedure TFmOSSrvPre.BtOKClick(Sender: TObject);
var
  PreSrv, Controle: Integer;
begin
  PreSrv := EdPreSrv.ValueVariant;

  if MyObjects.FIC(PreSrv = 0, EdPreSrv, 'Informe um servi�o base!') then
    Exit;
  // Prevenir interferencia do usuario durante insercoes!
  GroupBox1.Enabled := False;
  try
    ///
    ///////////////  INSERIR SERVI�O ///////////////////////////////////////////
    ///
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando servi�o');
    if InsereOSSrv(Controle) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo pragas alvo');
      InserePragasAlvo(Controle);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo f�rmulas de aplica��o');
      InsereFrmAplic(Controle);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo f�rmulas de monitoramento');
      InsereFrmMonit(Controle);
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Close;
  except
    GroupBox1.Enabled := True;
    raise;
  end;
end;

procedure TFmOSSrvPre.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSSrvPre.EdDesServicoChange(Sender: TObject);
begin
  if not EdDesServico.Focused then
    ReopenPreSrv(0);
end;

procedure TFmOSSrvPre.EdDesServicoEnter(Sender: TObject);
begin
  FlastDesServico := EdDesServico.ValueVariant;
end;

procedure TFmOSSrvPre.EdDesServicoExit(Sender: TObject);
begin
  if FlastDesServico <> EdDesServico.ValueVariant then
  begin
    FlastDesServico := EdDesServico.ValueVariant;
    ReopenPreSrv(0);
  end;
end;

procedure TFmOSSrvPre.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSSrvPre.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrDesServico, Dmod.MyDB);
end;

procedure TFmOSSrvPre.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSSrvPre.InsereFrmAplic(Controle: Integer);
const
  SQLType = stIns;
  //
  procedure MostraOSFrmRec(Conta: Integer);
  begin
    if DBCheck.CriaFm(TFmOSFrmRec, FmOSFrmRec, afmoNegarComAviso) then
    begin
      //FmOSFrmRec.ImgTipo.SQLType := SQLType;
      FmOSFrmRec.FQrOSFrmRec := FQrOSFrmRec;
      FmOSFrmRec.FQrOSFrmCab := FQrOSFrmCab;
      //FmOSFrmRec.FQrOSSrv := QrOSSrv;
      FmOSFrmRec.FCodigo := FOSCab;
      FmOSFrmRec.FControle := Controle;
      FmOSFrmRec.FConta := Conta; // QrOSFrmCabConta.Value;
      //
      FmOSFrmRec.QrOSSrv.Close;
      FmOSFrmRec.QrOSSrv.SQL.Text := FQrOSSrv.SQL.Text;
      FmOSFrmRec.QrOSSrv.Params := FQrOSSrv.Params;
      UMyMod.AbreQuery(FmOSFrmRec.QrOSSrv, Dmod.MyDB);
      //
      FmOSFrmRec.QrOSFrmCab.Close;
      FmOSFrmRec.QrOSFrmCab.SQL.Text := FQrOSFrmCab.SQL.Text;
      FmOSFrmRec.QrOSFrmCab.Params := FQrOSFrmCab.Params;
      UMyMod.AbreQuery(FmOSFrmRec.QrOSFrmCab, Dmod.MyDB);
      FmOSFrmRec.QrOSFrmCab.Locate('Conta', Conta (*QrOSFrmCabConta.Value*), []);
      //
      FmOSFrmRec.ShowModal;
      FmOSFrmRec.Destroy;
    end;
  end;

  procedure InsereAtual();
  var
    Conta: Integer;
  begin
    //Conta := 0;
    if DBCheck.CriaFm(TFmOSFrmCab, FmOSFrmCab, afmoNegarComAviso) then
    begin
      FmOSFrmCab.ImgTipo.SQLType  := SQLType;
      FmOSFrmCab.FQrOSFrmCab      := FQrOSFrmCab;
      FmOSFrmCab.FControle        := Controle; //QrOSSrvControle.Value;
      FmOSFrmCab.FConta           := 0;//QrOSFrmCabConta.Value;
      FmOSFrmCab.FCodigo          := FOSCab; //QrOSCabCodigo.Value;
      //
      FmOSFrmCab.QrOSSrv.Close;
      FmOSFrmCab.QrOSSrv.SQL.Text := FQrOSSrv.SQL.Text;
      FmOSFrmCab.QrOSSrv.Params   := FQrOSSrv.Params;
      UMyMod.AbreQuery(FmOSFrmCab.QrOSSrv, Dmod.MyDB);
      //
      // Inicio copia
      FmOSFrmCab.EdConta.ValueVariant      := 0; //QrFormulasConta.Value;
      FmOSFrmCab.EdNome.Text               := QrFormulasNome.Value;
      //
      FmOSFrmCab.EdFormula.ValueVariant    := QrFormulasCodigo.Value; //QrFormulasFormula.Value;
      FmOSFrmCab.CBFormula.KeyValue        := QrFormulasCodigo.Value; //QrFormulasFormula.Value;
      //
      FmOSFrmCab.EdEquipAplic.ValueVariant := QrFormulasEquipAplic.Value;
      FmOSFrmCab.CBEquipAplic.KeyValue     := QrFormulasEquipAplic.Value;
      //
      FmOSFrmCab.EdTipoAplica.ValueVariant := QrFormulasTipoAplica.Value;
      FmOSFrmCab.CBTipoAplica.KeyValue     := QrFormulasTipoAplica.Value;
      //
      FmOSFrmCab.EdQtdTot.ValueVariant     := QrFormulasQtdTot.Value;
      FmOSFrmCab.RGDiluente.ItemIndex      := QrFormulasDiluente.Value;
(*
////////////////// Especificos /////////////////////////////////////////////////
      FmOSFrmCab.RGIndicUso.ItemIndex      := QrFormulasIndicUso.Value;
      //FmOSFrmCab.EdQtdQSP.ValueVariant     := QrFormulasQtdQSP.Value;
      //
      FmOSFrmCab.EdSigla.Text              := QrFormulasSIGLA.Value;
      FmOSFrmCab.EdUnidMed.ValueVariant    := QrFormulasCU_UnidMed.Value;
      FmOSFrmCab.CBUnidMed.KeyValue        := QrFormulasCU_UnidMed.Value;
*)
      // FIM Copia
      //

      FmOSFrmCab.ShowModal;
      Conta := FmOSFrmCab.FConta;
      FmOSFrmCab.Destroy;
      //
      if (Conta <> 0) and (FQrOSFrmCab.State <> dsInactive)
      and (FQrOSFrmCab.FieldByName('Conta').AsInteger = Conta) then
       MostraOSFrmRec(Conta);
    end;
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT frm.* ',
  'FROM prefrm prf  ',
  'LEFT JOIN formulas frm ON frm.Codigo=prf.Formula  ',
  'WHERE prf.Controle=' + Geral.FF0(QrPreSrvControle.Value),
  '']);
  QrFormulas.First;
  while not QrFormulas.Eof do
  begin
    InsereAtual();
    //
    QrFormulas.Next;
  end;
end;

procedure TFmOSSrvPre.InsereFrmMonit(Controle: Integer);
  procedure InsereAtual();
  const
    SQLType = stIns;
  begin
    if DBCheck.CriaFm(TFmOSMonCabIns, FmOSMonCabIns, afmoNegarComAviso) then
    begin
      FmOSMonCabIns.ImgTipo.SQLType := SQLType;
      FmOSMonCabIns.FQrOSMonCab := FQrOSMonCab;
      FmOSMonCabIns.FQrOSMonRec := FQrOSMonRec;
      FmOSMonCabIns.FCodigo     := FOSCAb; //QrOSCabCodigo.Value;
      FmOSMonCabIns.FControle   := Controle; //QrOSSrvControle.Value;
      //FmOSMonCabIns.FPipCad     := QrOSMonCabPipCad.Value;
      FmOSMonCabIns.FSiapterCad := FQrOSCab.FieldByName('SiapTerCad').AsInteger;
      FmOSMonCabIns.FEntidade   := FQrOSCab.FieldByName('Entidade').AsInteger;
      //
      FmOSMonCabIns.QrOSSrv.Close;
      FmOSMonCabIns.QrOSSrv.SQL.Text := FQrOSSrv.SQL.Text;
      FmOSMonCabIns.QrOSSrv.Params := FQrOSSrv.Params;
      UMyMod.AbreQuery(FmOSMonCabIns.QrOSSrv, Dmod.MyDB);
      //
//////// Pre-configurado ///////////////////////////////////////////////////////
      FmOSMonCabIns.EdFormula.ValueVariant := QrFormulasCodigo.Value;
      FmOSMonCabIns.CBFormula.KeyValue     := QrFormulasCodigo.Value;
////////////////////////////////////////////////////////////////////////////////
      FmOSMonCabIns.ShowModal;
      //Conta := FmOSMonCabIns.FConta;
      FmOSMonCabIns.Destroy;
      //
      OSApp_PF.ReopenOSMonRec(FQrOSMonRec, FQrOSMonCab.FieldByName('Conta').AsInteger, 0);
    end;
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT mon.* ',
  'FROM premon prf  ',
  'LEFT JOIN formulas mon ON mon.Codigo=prf.Formula  ',
  'WHERE prf.Controle=' + Geral.FF0(QrPreSrvControle.Value),
  '']);
  QrFormulas.First;
  while not QrFormulas.Eof do
  begin
    InsereAtual();
    //
    QrFormulas.Next;
  end;
end;

function TFmOSSrvPre.InsereOSSrv(var Controle: Integer): Boolean;
const
  SQLType = stIns;
begin
  Result := False;
  //
  if DBCheck.CriaFm(TFmOsSrv, FmOsSrv, afmoNegarComAviso) then
  begin
    FmOsSrv.ImgTipo.SQLType := SQLType;
    FmOsSrv.FQrInsUpd := FQrOsSrv;
    //
    FmOsSrv.QrOSCab.Close;
    FmOsSrv.QrOSCab.SQL.Text := FQrOSCab.SQL.Text;
    FmOsSrv.QrOSCab.Params := FQrOSCab.Params;
    UnDmkDAC_PF.AbreQuery(FmOsSrv.QrOSCab, Dmod.MyDB);
    //
    // In�cio c�pia
    FmOsSrv.EdGarantiaDd.ValueVariant := QrPreSrvGarantiaDd.Value;
    FmOsSrv.EdHrEvacuar.ValueVariant  := QrPreSrvHrEvacuar.Value;
    FmOsSrv.EdHrExecutar.ValueVariant := QrPreSrvHrExecutar.Value;
    //
    FmOsSrv.EdValCalc.ValueVariant    := QrPreSrvValCalc.Value;
    FmOsSrv.EdValInfo.ValueVariant    := QrPreSrvValInfo.Value;
    FmOsSrv.EdValDesc.ValueVariant    := QrPreSrvValDesc.Value;
    FmOsSrv.EdValTota.ValueVariant    := QrPreSrvValTota.Value;
    //
    FmOsSrv.EdMoniDdTotl.ValueVariant := QrPreSrvMoniDdTotl.Value;
    FmOSSrv.EdMoniDdIntv.ValueVariant := QrPreSrvMoniDdIntv.Value;
    //
    // Espec�ficos:
    FmOsSrv.EdDesServico.ValueVariant := QrPreSrvCodigo.Value;
    FmOsSrv.CBDesServico.KeyValue     := QrPreSrvCodigo.Value;
    FmOSSrv.MeDetalhes.Text           := QrPreSrvDetalhes.Value;
    // Fim C�pia
    //
    FmOsSrv.RGAutorizado.ItemIndex    := 0;
    FmOsSrv.RGTudoFeitoM.ItemIndex    := 0;
    FmOsSrv.EdControle.ValueVariant   := 0;
    FmOsSrv.FCriou := True;
    //FmOsSrv.CalculaTotal();
    FmOsSrv.ShowModal;
    Result := FmOsSrv.FConfirmou;
    Controle := FmOsSrv.EdControle.ValueVariant;
    FmOsSrv.Destroy;
(*
    PCGeral.ActivePageIndex := 1;
    //
    LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
      QrOSCabCodigo.Value);
    //
    if SQLType = stUpd then
    begin
      if Codigo = QrOSCabCodigo.Value then
        OSsFuturas()
      else
        Geral.MB_Aviso('Localizador n�o confere! [?] Avise a DERMATEK');
    end;
*)
  end;
end;

procedure TFmOSSrvPre.InserePragasAlvo(Controle: Integer);
var
  Codigo, Conta, Praga_Z: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPreAlv, Dmod.MyDB, [
  'SELECT osa.Praga_Z ',
  'FROM prealv osa ',
  'WHERE osa.Controle=' + Geral.FF0(QrPreSrvControle.Value),
  '']);
  QrPreAlv.First;
  while not QrPreAlv.Eof do
  begin
    Codigo         := FOSCab;
    //Controle       := ;
    Conta          := 0;
    Praga_Z        := QrPreAlvPraga_Z.Value;
    //
    Conta := UMyMod.BPGS1I32_Reaproveita('osalv', 'Conta', '', '', tsDef, stIns, 0);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osalv', False, [
    'Codigo', 'Controle', 'Praga_Z'], [
    'Conta'], [
    Codigo, Controle, Praga_Z], [
    Conta], True);
    //
    QrPreAlv.Next;
  end;
end;

procedure TFmOSSrvPre.ReopenPreSrv(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPreSrv, Dmod.MyDB, [
  'SELECT *',
  'FROM presrv',
  'WHERE Codigo=' + Geral.FF0(EdDesServico.ValueVariant),
  '']);
  //
  QrPreSrv.Locate('Controle', Controle, []);
end;

procedure TFmOSSrvPre.SpeedButton6Click(Sender: TObject);
var
  DesServico: Integer;
begin
  VAR_CADASTRO := 0;
  DesServico   := EdDesServico.ValueVariant;

  FmPrincipal.MostraServicoDesServico(DesServico);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdDesServico, CBDesServico, QrDesServico, VAR_CADASTRO);

    EdDesServico.SetFocus;
  end;
end;

end.
