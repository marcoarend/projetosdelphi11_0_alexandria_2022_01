unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UMySQLModule, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*)
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, WinSkinStore, WinSkinData, Mask, DBCtrls, Tabs,
  DockTabSet, AdvToolBar, AdvGlowButton, AdvMenus, dmkGeral, AdvToolBarStylers,
  AdvOfficeHint, AdvPreviewMenu, AdvShapeButton, unDmkProcFunc, dmkDBGrid,
  dmkEdit, CfgCadLista, ModOS, DMKpnfsConversao, frxClass, Variants,
  dmkPageControl, UnBugs_Tabs, UnDmkEnums, UnProjGroup_Consts, UnFinanceiro,
  ShellAPI, Vcl.Imaging.pngimage, ABSMain, IdUDPBase, IdUDPClient, IdSNTP,
  IdBaseComponent, IdComponent, IdRawBase, IdRawClient, IdIcmpClient,
  dmkDBLookupComboBox, dmkEditCB, UnitNotificacoes, UnitNotificacoesEdit,
  UnMLAGeral, UnXXe_PF {,
  //
  TDI, dmkTDIContainer};

type
  (*
  TTipoMaterialNF = (tnfMateriaPrima, tnfUsoEConsumo);
  TcpCalc = (cpJurosMes, cpMulta);
  *)
  TFmPrincipal = class(TForm)
    Timer1: TTimer;
    PMProdImp: TPopupMenu;
    Estoque1: TMenuItem;
    Histrico1: TMenuItem;
    Vendas1: TMenuItem;
    StatusBar: TStatusBar;
    PMPlaCtas: TPopupMenu;
    Saldoinicialdeconta1: TMenuItem;
    Listagem2: TMenuItem;
    Simples2: TMenuItem;
    Transfernciaentrecontas1: TMenuItem;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasDIFERENCA: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTIPOPRAZO: TWideStringField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TSmallintField;
    QrCarteirasUserAlt: TSmallintField;
    QrCarteirasNOMEPAGREC: TWideStringField;
    QrCarteirasPagRec: TSmallintField;
    QrCarteirasNOMEBANCO: TWideStringField;
    QrCarteirasFuturoC: TFloatField;
    QrCarteirasFuturoD: TFloatField;
    QrCarteirasFuturoS: TFloatField;
    DsCarteiras: TDataSource;
    QrCartSum: TmySQLQuery;
    QrCartSumSALDO: TFloatField;
    QrCartSumFuturoC: TFloatField;
    QrCartSumFuturoD: TFloatField;
    QrCartSumFuturoS: TFloatField;
    QrCartSumEmCaixa: TFloatField;
    QrCartSumDifere: TFloatField;
    QrCartSumSDO_FUT: TFloatField;
    DsCartSum: TDataSource;
    TimerIdle: TTimer;
    OpenDialog1: TOpenDialog;
    AdvToolBarPager1: TAdvToolBarPager;
    ATPFinanceiro: TAdvPage;
    AdvToolBarPager12: TAdvPage;
    AdvToolBarPager13: TAdvPage;
    AdvToolBar1: TAdvToolBar;
    AGBFinancas2: TAdvGlowButton;
    AdvToolBar2: TAdvToolBar;
    AdvGlowButton1: TAdvGlowButton;
    AdvToolBar5: TAdvToolBar;
    AdvToolBar6: TAdvToolBar;
    AGBPlanoCtaNiveis: TAdvGlowButton;
    AGBPlanoCtaContas: TAdvGlowButton;
    AGBPlanoCtaSubgrupos: TAdvGlowButton;
    AGBPlanoCtaGrupos: TAdvGlowButton;
    AGBPlanoCtaConjuntos: TAdvGlowButton;
    AGBPlanoCtaPlanos: TAdvGlowButton;
    AdvToolBar7: TAdvToolBar;
    AGBPlanoCtaLista: TAdvGlowButton;
    AGBPlanoCtaSaldo: TAdvGlowButton;
    AdvToolBar8: TAdvToolBar;
    AdvToolBar9: TAdvToolBar;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    AdvToolBarButton2: TAdvToolBarButton;
    ATBVerificaNovaVersao: TAdvToolBarButton;
    AdvToolBarButton4: TAdvToolBarButton;
    AdvPreviewMenu1: TAdvPreviewMenu;
    AdvToolBar10: TAdvToolBar;
    AdvGlowButton17: TAdvGlowButton;
    AdvToolBar11: TAdvToolBar;
    AdvGlowMenuButton3: TAdvGlowMenuButton;
    AdvGlowMenuButton4: TAdvGlowMenuButton;
    AdvOfficeHint1: TAdvOfficeHint;
    AGBMatriz: TAdvGlowButton;
    AGMBBackup: TAdvGlowButton;
    AGMBVerifiBD: TAdvGlowButton;
    AGBBancos: TAdvGlowButton;
    AGBCarteiras: TAdvGlowButton;
    AdvGlowButton7: TAdvGlowButton;
    Limpar1: TMenuItem;
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    AGBFiliais: TAdvGlowButton;
    AGBFolhaFunci: TAdvGlowButton;
    APMListasDiversas: TAdvPopupMenu;
    Atividades1: TMenuItem;
    HowFound1: TMenuItem;
    Caractersticas1: TMenuItem;
    Objetos1: TMenuItem;
    Residentes1: TMenuItem;
    Dependncias1: TMenuItem;
    Cuidados1: TMenuItem;
    CaixasDgua1: TMenuItem;
    Materias1: TMenuItem;
    Formas1: TMenuItem;
    Finalidades1: TMenuItem;
    LocaisdeAplicao1: TMenuItem;
    iposdeConstruo1: TMenuItem;
    AGBCunsCad1: TAdvGlowButton;
    ATBBFavoritos: TAdvToolBarButton;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    TimerAlphaBlend: TTimer;
    AdvPage2: TAdvPage;
    AdvToolBar13: TAdvToolBar;
    AGBQrCodePrint: TAdvGlowButton;
    Vistoria1: TMenuItem;
    Servios1: TMenuItem;
    Fatosgeradores1: TMenuItem;
    Abrangncias1: TMenuItem;
    iposdedependncias1: TMenuItem;
    AdvGlowButton3: TAdvGlowButton;
    AGBPediPrzCab_A: TAdvGlowButton;
    AdvGlowMenuButton1: TAdvGlowMenuButton;
    Atendimento1: TMenuItem;
    Formadecontato1: TMenuItem;
    Dependncias2: TMenuItem;
    Pragas1: TMenuItem;
    Pragas2: TMenuItem;
    Gruposdepragas1: TMenuItem;
    Frmulas1: TMenuItem;
    AdvGlowButton8: TAdvGlowButton;
    AdvGlowButton16: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvToolBar12: TAdvToolBar;
    AGBABDPragas: TAdvGlowButton;
    AGBListaCSIRO: TAdvGlowButton;
    AdvPage3: TAdvPage;
    AdvToolBar14: TAdvToolBar;
    AGBCNAB_Cfg: TAdvGlowButton;
    AGBBloArre: TAdvGlowButton;
    AGBBloOpcoes: TAdvGlowButton;
    AdvToolBar15: TAdvToolBar;
    AdvGlowButton28: TAdvGlowButton;
    AdvGlowButton30: TAdvGlowButton;
    AdvGlowButton31: TAdvGlowButton;
    AdvGlowButton23: TAdvGlowButton;
    AGVContratos: TAdvGlowButton;
    AGVIGPM: TAdvGlowButton;
    AdvPMVerificaBD: TAdvPopupMenu;
    VerificaBDServidor1: TMenuItem;
    VerificaTabelasterceiros1: TMenuItem;
    AdvGlowMenuButton2: TAdvGlowMenuButton;
    AdvPage4: TAdvPage;
    AdvToolBar16: TAdvToolBar;
    AGBFatDivGer: TAdvGlowButton;
    AdvToolBar38: TAdvToolBar;
    AGBFatPedNFs: TAdvGlowButton;
    AGBNFeLEnc: TAdvGlowButton;
    AGBNfe_Pesq: TAdvGlowButton;
    AGBNFeInut: TAdvGlowButton;
    AGBNFeSteps_0: TAdvGlowButton;
    AGBNFeEventos: TAdvGlowButton;
    AGBConsultaNFe: TAdvGlowButton;
    AdvToolBar40: TAdvToolBar;
    AGBNFeJust: TAdvGlowButton;
    AGBNFeInfCpl: TAdvGlowButton;
    AdvToolBar39: TAdvToolBar;
    AdvToolBarButton6: TAdvToolBarButton;
    AGBXML_No_BD: TAdvGlowButton;
    AGBExportaXML: TAdvGlowButton;
    AGBValidaXML: TAdvGlowButton;
    AdvPage5: TAdvPage;
    AdvToolBar19: TAdvToolBar;
    AGBFisRegCad: TAdvGlowButton;
    AGBCFOP2003: TAdvGlowButton;
    AGBNCMs: TAdvGlowButton;
    AGBModelosImp: TAdvGlowButton;
    APGerenciamento: TAdvPage;
    AdvToolBar26: TAdvToolBar;
    AGBDiaEven: TAdvGlowButton;
    AGBDiaGer: TAdvGlowButton;
    AGBDiaAss: TAdvGlowButton;
    AdvToolBar3: TAdvToolBar;
    AGBOS: TAdvGlowButton;
    AGBSAC_01: TAdvGlowButton;
    AdvPage6: TAdvPage;
    ATBTemAtualizacao: TAdvToolBarButton;
    AdvToolBar4: TAdvToolBar;
    AGBPraga_Z: TAdvGlowButton;
    AGBPragas_A: TAdvGlowButton;
    AdvToolBar20: TAdvToolBar;
    AGBNFSe_Edit: TAdvGlowButton;
    AGBNFSeLRpsC: TAdvGlowButton;
    AGBNFSe_RPSPesq: TAdvGlowButton;
    AGBNFSe_NFSePesq: TAdvGlowButton;
    AGBNFSeFatCab: TAdvGlowButton;
    AdvToolBar18: TAdvToolBar;
    AGBNFSeSrvCad: TAdvGlowButton;
    AGBNFSeMenCab: TAdvGlowButton;
    AdvToolBar21: TAdvToolBar;
    AGBTabsSPEDEFD: TAdvGlowButton;
    AdvToolBar22: TAdvToolBar;
    AGBNFSeStepLote: TAdvGlowButton;
    AGBNFSE_LoadXML_0201: TAdvGlowButton;
    AGBDesServico: TAdvGlowButton;
    ATBBMenuGeral: TAdvToolBarButton;
    AGBDTB: TAdvGlowButton;
    Produtos1: TMenuItem;
    PrincpiosAtivos1: TMenuItem;
    GruposQumicos1: TMenuItem;
    AGBFormulas: TAdvGlowButton;
    QrClientes: TmySQLQuery;
    QrClientesNOMEENT: TWideStringField;
    QrClientesEntidade: TIntegerField;
    QrClientesCODIGO: TIntegerField;
    DsClientes: TDataSource;
    Geral1: TMenuItem;
    extosGenricos1: TMenuItem;
    CheckList1: TMenuItem;
    AGBOpcoesFili: TAdvGlowButton;
    MotivosdedestivdePIPs1: TMenuItem;
    Monitoramento1: TMenuItem;
    ListasdePerguntas1: TMenuItem;
    Atributosdeperguntas1: TMenuItem;
    ipodeAplicaes1: TMenuItem;
    Button3: TButton;
    ATBAgenda: TAdvToolBar;
    AGBAgendaGer: TAdvGlowButton;
    AGBCorrigeFatID: TAdvGlowButton;
    GBAvisos1: TGroupBox;
    Panel14: TPanel;
    LaAvisoA1: TLabel;
    LaAvisoA2: TLabel;
    PB1: TProgressBar;
    QrOSs: TmySQLQuery;
    AGBAgenGrCab: TAdvGlowButton;
    PageControl1: TdmkPageControl;
    Timer2: TTimer;
    Agendaavulsa1: TMenuItem;
    Atividadesaseremexecutadas1: TMenuItem;
    Fatosgeradoresdoscompromissos1: TMenuItem;
    AGBAgendaEve: TAdvGlowButton;
    Clientes1: TMenuItem;
    Statusdefotosrelativas1: TMenuItem;
    AGBEntidade2Tabed: TAdvGlowButton;
    EstatusdeOSs1: TMenuItem;
    QrProvisorio1: TmySQLQuery;
    TimerPingServer: TTimer;
    AGBDirectTerminal: TAdvGlowButton;
    NCTCAC1: TMenuItem;
    GerenciaContatos1: TMenuItem;
    PrCadastros1: TMenuItem;
    PsVenda1: TMenuItem;
    AGBOSPrv: TAdvGlowButton;
    AdvToolBar24: TAdvToolBar;
    AdvGlowButton25: TAdvGlowButton;
    AdvGlowButton26: TAdvGlowButton;
    AGBPipCad: TAdvGlowButton;
    StatusdeProvidncias1: TMenuItem;
    Memo3: TMemo;
    AGBFilhas: TAdvGlowButton;
    Feriados1: TMenuItem;
    AGBOSPesq: TAdvGlowButton;
    Gruposdeservios1: TMenuItem;
    Gerencia1: TMenuItem;
    Autogera1: TMenuItem;
    Novos1: TMenuItem;
    Regeratodo1: TMenuItem;
    AGBAgeEqiCab: TAdvGlowButton;
    ATBBLastWork: TAdvToolBarButton;
    ATBBATBAutoExp: TAdvToolBarButton;
    Questionrios1: TMenuItem;
    Cadastrodeperguntas1: TMenuItem;
    BtArrumaPrg: TButton;
    extosbinrios1: TMenuItem;
    AGBCunsCad2: TAdvGlowButton;
    ATBBLehCodBarra: TAdvToolBarButton;
    AdvToolBar28: TAdvToolBar;
    AGBOSProtSai1: TAdvGlowButton;
    AGBOSProtSai2: TAdvGlowButton;
    APMOSProtSai1: TAdvPopupMenu;
    Novajanela1: TMenuItem;
    N2: TMenuItem;
    Existente1: TMenuItem;
    APMOSProtSai2: TAdvPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    AGBOSProtPsq: TAdvGlowButton;
    ATBImp: TAdvToolBarButton;
    VerificaBDWeb1: TMenuItem;
    AGBPosVdaGer: TAdvGlowButton;
    Statusdecompromissos1: TMenuItem;
    APMNFSeTabed: TAdvPopupMenu;
    GerenciaNFSe1: TMenuItem;
    DeclaraodeServio1: TMenuItem;
    FaturamentoMensal1: TMenuItem;
    Configuraodeemissesmensais1: TMenuItem;
    AGBNFSeTabed: TAdvGlowMenuButton;
    AdvPage7: TAdvPage;
    AdvToolBar29: TAdvToolBar;
    AdvGlowButton38: TAdvGlowButton;
    AdvGlowButton35: TAdvGlowButton;
    AGBGraGruMoW: TAdvGlowButton;
    AdvToolBar25: TAdvToolBar;
    AdvGlowButton42: TAdvGlowButton;
    TmSuporte: TTimer;
    TySuporte: TTrayIcon;
    PMGeral: TPopupMenu;
    Chamadasrecebidas1: TMenuItem;
    Chamadasatendidas1: TMenuItem;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    APMContratos: TAdvPopupMenu;
    Cadastros1: TMenuItem;
    Pesquisaemgrade1: TMenuItem;
    AGBContratos2: TAdvGlowMenuButton;
    AGBOSPrn: TAdvGlowButton;
    ATBOSCab: TAdvToolBarButton;
    AdvToolBar30: TAdvToolBar;
    AGBRMIPCfgCab: TAdvGlowButton;
    AGBRMIP_Cad: TAdvGlowButton;
    Shape1: TShape;
    mySQLQuery1: TmySQLQuery;
    mySQLQuery1USERID: TIntegerField;
    mySQLQuery1Numero: TLargeintField;
    AdvGlowButton24: TAdvGlowButton;
    Rotas1: TMenuItem;
    Arenovar1: TMenuItem;
    AdvPage8: TAdvPage;
    AdvToolBar33: TAdvToolBar;
    AdvGlowButton50: TAdvGlowButton;
    Mobilidade1: TMenuItem;
    Mobilidade2: TMenuItem;
    Automoo1: TMenuItem;
    AGBStqBalCad: TAdvGlowButton;
    AGBStqManCad: TAdvGlowButton;
    Motivospsgarantia1: TMenuItem;
    AGBGarantRnw: TAdvGlowButton;
    AdvGlowButton51: TAdvGlowButton;
    AdvGlowButton52: TAdvGlowButton;
    Image1: TImage;
    AGBGraImpLista: TAdvGlowButton;
    AGBStqCenCab: TAdvGlowButton;
    IdIcmpClient1: TIdIcmpClient;
    BalloonHint1: TBalloonHint;
    AdvToolBarButton10: TAdvToolBarButton;
    AdvGlowButton37: TAdvGlowButton;
    AdvPMCadastrosWEB: TAdvPopupMenu;
    Avisos1: TMenuItem;
    Perfis2: TMenuItem;
    N4: TMenuItem;
    Janelas1: TMenuItem;
    AdvGlowMenuButton5: TAdvGlowMenuButton;
    IdSNTP1: TIdSNTP;
    AGBNfeCabA: TAdvGlowButton;
    AGBCntngnc: TAdvGlowButton;
    AGBNFeDest: TAdvGlowButton;
    AGBNFeLoad_Inn: TAdvGlowButton;
    AGBNFeDesDowC: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    AdvGlowButton43: TAdvGlowButton;
    AdvPage9: TAdvPage;
    AdvToolBar23: TAdvToolBar;
    AdvGlowButton6: TAdvGlowButton;
    AdvToolBar17: TAdvToolBar;
    AdvWSuporte: TAdvGlowButton;
    AdvGlowButton4: TAdvGlowButton;
    AdvPMTabela: TAdvPopupMenu;
    Custos1: TMenuItem;
    Precos1: TMenuItem;
    AdvPMProd: TAdvPopupMenu;
    GradeTamanhos1: TMenuItem;
    Reduzidodeprodutos1: TMenuItem;
    N5: TMenuItem;
    ProdutosaAplicar1: TMenuItem;
    ProdutosaMonitorar1: TMenuItem;
    EquipamentosaAplicar1: TMenuItem;
    EquipamentoMonitorar1: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    MarcasdeEmpresas1: TMenuItem;
    OpesGrade1: TMenuItem;
    AdvToolBar27: TAdvToolBar;
    AdvGlowButton118: TAdvGlowButton;
    AdvGlowButton105: TAdvGlowButton;
    AdvGlowButton113: TAdvGlowButton;
    AdvGlowMenuButton6: TAdvGlowMenuButton;
    AGBStqCenCad: TAdvGlowButton;
    AdvGlowMenuButton7: TAdvGlowMenuButton;
    AdvPMFin: TAdvPopupMenu;
    Prlanamentos1: TMenuItem;
    N8: TMenuItem;
    Listadascontas1: TMenuItem;
    Centrodecustos1: TMenuItem;
    N9: TMenuItem;
    Ajustes1: TMenuItem;
    Reiniciacomemisses1: TMenuItem;
    Corrigeerrosemisses1: TMenuItem;
    AdvToolBar31: TAdvToolBar;
    AdvGlowButton5: TAdvGlowButton;
    AdvGlowButton9: TAdvGlowButton;
    AdvGlowButton10: TAdvGlowButton;
    AdvToolBar32: TAdvToolBar;
    AdvGlowButton11: TAdvGlowButton;
    AdvGlowButton12: TAdvGlowButton;
    AGBBloProtoGer: TAdvGlowButton;
    AGBBloEnPr: TAdvGlowButton;
    AdvGlowButton55: TAdvGlowButton;
    AdvGlowButton13: TAdvGlowButton;
    AGBBloGeren: TAdvGlowButton;
    AdvGlowButton14: TAdvGlowButton;
    TmVersao: TTimer;
    AdvGlowButton15: TAdvGlowButton;
    AdvGlowButton19: TAdvGlowButton;
    BtSVG2: TButton;
    Formadeapresentaodosprodutos1: TMenuItem;
    AdvGlowButton167: TAdvGlowButton;
    AdvPage10: TAdvPage;
    AdvToolBar34: TAdvToolBar;
    AdvGlowButton18: TAdvGlowButton;
    AdvToolBar35: TAdvToolBar;
    AdvGlowButton40: TAdvGlowButton;
    AdvGlowButton20: TAdvGlowButton;
    AdvToolBar36: TAdvToolBar;
    AdvGlowButton21: TAdvGlowButton;
    AdvToolBar37: TAdvToolBar;
    AdvGlowButton46: TAdvGlowButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Listagem2Click(Sender: TObject);
    procedure Simples2Click(Sender: TObject);
    procedure Transfernciaentrecontas1Click(Sender: TObject);
    procedure Saldodecontas1Click(Sender: TObject);
    procedure Verificanovaverso1Click(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure Atualizasaldodecontas1Click(Sender: TObject);
    procedure Gerenciafinancas1Click(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure AGBFinancas2Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AGBPlanoCtaPlanosClick(Sender: TObject);
    procedure AGBPlanoCtaConjuntosClick(Sender: TObject);
    procedure AGBPlanoCtaGruposClick(Sender: TObject);
    procedure AGBPlanoCtaSubgruposClick(Sender: TObject);
    procedure AGBPlanoCtaContasClick(Sender: TObject);
    procedure AGBPlanoCtaNiveisClick(Sender: TObject);
    procedure AGBPlanoCtaListaClick(Sender: TObject);
    procedure AGBPlanoCtaSaldoClick(Sender: TObject);
    procedure AGBCarteirasClick(Sender: TObject);
    procedure AdvToolBarButton4Click(Sender: TObject);
    procedure ATBVerificaNovaVersaoClick(Sender: TObject);
    procedure AdvToolBarButton2Click(Sender: TObject);
    procedure AGBMatrizClick(Sender: TObject);
    procedure AGMBBackupClick(Sender: TObject);
    procedure AGBBancosClick(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure AdvGlowButton7Click(Sender: TObject);
    procedure Limpar1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AGBFiliaisClick(Sender: TObject);
    procedure AGBTabsSPEDEFDClick(Sender: TObject);
    procedure AGBFolhaFunciClick(Sender: TObject);
    procedure Atividades1Click(Sender: TObject);
    procedure Materias1Click(Sender: TObject);
    procedure Formas1Click(Sender: TObject);
    procedure Caractersticas1Click(Sender: TObject);
    procedure Objetos1Click(Sender: TObject);
    procedure Cuidados1Click(Sender: TObject);
    procedure HowFound1Click(Sender: TObject);
    procedure Finalidades1Click(Sender: TObject);
    procedure Residentes1Click(Sender: TObject);
    procedure iposdeConstruo1Click(Sender: TObject);
    procedure AGBCunsCad1Click(Sender: TObject);
    procedure ATBBFavoritosClick(Sender: TObject);
    procedure TimerAlphaBlendTimer(Sender: TObject);
    procedure AGBDiaEvenClick(Sender: TObject);
    procedure AGBDiaGerClick(Sender: TObject);
    procedure AGBDiaAssClick(Sender: TObject);
    procedure AGBQrCodePrintClick(Sender: TObject);
    procedure Servios1Click(Sender: TObject);
    procedure Fatosgeradores1Click(Sender: TObject);
    procedure Abrangncias1Click(Sender: TObject);
    procedure iposdedependncias1Click(Sender: TObject);
    procedure AGBPediPrzCab_AClick(Sender: TObject);
    procedure AGBDirectTerminalClick(Sender: TObject);
    procedure AGBSAC_01Click(Sender: TObject);
    procedure Formadecontato1Click(Sender: TObject);
    procedure Dependncias2Click(Sender: TObject);
    procedure Pragas2Click(Sender: TObject);
    procedure Gruposdepragas1Click(Sender: TObject);
    procedure Frmulas1Click(Sender: TObject);
    procedure AdvGlowButton8Click(Sender: TObject);
    procedure AdvGlowButton16Click(Sender: TObject);
    procedure AGBOSClick(Sender: TObject);
    procedure AGBABDPragasClick(Sender: TObject);
    procedure AGBListaCSIROClick(Sender: TObject);
    procedure AGBBloArreClick(Sender: TObject);
    procedure AGBCNAB_CfgClick(Sender: TObject);
    procedure AGBBloGerenClick(Sender: TObject);
    procedure AGBBloOpcoesClick(Sender: TObject);
    procedure AGBBloEnPrClick(Sender: TObject);
    procedure AGBBloProtoGerClick(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton30Click(Sender: TObject);
    procedure AdvGlowButton31Click(Sender: TObject);
    procedure AdvGlowButton23Click(Sender: TObject);
    procedure AGVContratosClick(Sender: TObject);
    procedure AGVIGPMClick(Sender: TObject);
    procedure VerificaTabelasterceiros1Click(Sender: TObject);
    procedure VerificaBDServidor1Click(Sender: TObject);
    procedure AGBFatDivGerClick(Sender: TObject);
    procedure AGBCntngncClick(Sender: TObject);
    procedure AGBNfeCabAClick(Sender: TObject);
    procedure AGBFatPedNFsClick(Sender: TObject);
    procedure AGBNFeLEncClick(Sender: TObject);
    procedure AGBNfe_PesqClick(Sender: TObject);
    procedure AGBConsultaNFeClick(Sender: TObject);
    procedure AGBNFeSteps_0Click(Sender: TObject);
    procedure AGBNFeEventosClick(Sender: TObject);
    procedure AGBNFeInutClick(Sender: TObject);
    procedure AGBNFeJustClick(Sender: TObject);
    procedure AGBNFeInfCplClick(Sender: TObject);
    procedure AGBXML_No_BDClick(Sender: TObject);
    procedure AGBExportaXMLClick(Sender: TObject);
    procedure AGBValidaXMLClick(Sender: TObject);
    procedure AGBFisRegCadClick(Sender: TObject);
    procedure AGBCFOP2003Click(Sender: TObject);
    procedure AGBNCMsClick(Sender: TObject);
    procedure AGBModelosImpClick(Sender: TObject);
    procedure AGBListServClick(Sender: TObject);
    procedure ATBTemAtualizacaoClick(Sender: TObject);
    procedure AdvWSuporteClick(Sender: TObject);
    procedure AGBPraga_ZClick(Sender: TObject);
    procedure AGBPragas_AClick(Sender: TObject);
    procedure AGBNFSe_EditClick(Sender: TObject);
    procedure AGBNFSe_NFSePesqClick(Sender: TObject);
    procedure AGBNFSe_RPSPesqClick(Sender: TObject);
    procedure AGBNFSeFatCabClick(Sender: TObject);
    procedure AGBNFSeLRpsCClick(Sender: TObject);
    procedure AGBNFSeSrvCadClick(Sender: TObject);
    procedure AGBNFSeMenCabClick(Sender: TObject);
    procedure AGBNFSeStepLoteClick(Sender: TObject);
    procedure AGBNFSE_LoadXML_0201Click(Sender: TObject);
    procedure AGBDesServicoClick(Sender: TObject);
    procedure ATBBMenuGeralClick(Sender: TObject);
    procedure AGBDTBClick(Sender: TObject);
    procedure GruposQumicos1Click(Sender: TObject);
    procedure PrincpiosAtivos1Click(Sender: TObject);
    procedure AGBFormulasClick(Sender: TObject);
    procedure extosGenricos1Click(Sender: TObject);
    procedure CheckList1Click(Sender: TObject);
    procedure AGBOpcoesFiliClick(Sender: TObject);
    procedure MotivosdedestivdePIPs1Click(Sender: TObject);
    procedure ListasdePerguntas1Click(Sender: TObject);
    procedure Atributosdeperguntas1Click(Sender: TObject);
    procedure ipodeAplicaes1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure AGBAgendaGerClick(Sender: TObject);
    procedure AGBCorrigeFatIDClick(Sender: TObject);
    procedure AdvToolBarPager1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PageControl1Enter(Sender: TObject);
    procedure PageControl1MouseEnter(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Atividadesaseremexecutadas1Click(Sender: TObject);
    procedure Fatosgeradoresdoscompromissos1Click(Sender: TObject);
    procedure AGBAgendaEveClick(Sender: TObject);
    procedure AGBAgenGrCabClick(Sender: TObject);
    procedure Statusdefotosrelativas1Click(Sender: TObject);
    procedure PageControl1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AGBEntidade2TabedClick(Sender: TObject);
    procedure EstatusdeOSs1Click(Sender: TObject);
    procedure AdvGlowButton25Click(Sender: TObject);
    procedure AdvGlowButton26Click(Sender: TObject);
    procedure TimerPingServerTimer(Sender: TObject);
    procedure NCTCAC1Click(Sender: TObject);
    procedure GerenciaContatos1Click(Sender: TObject);
    procedure PsVenda1Click(Sender: TObject);
    procedure AGBOSPrvClick(Sender: TObject);
    procedure AGBPipCadClick(Sender: TObject);
    procedure StatusdeProvidncias1Click(Sender: TObject);
    procedure AGBFilhasClick(Sender: TObject);
    procedure Feriados1Click(Sender: TObject);
    procedure AGBOSPesqClick(Sender: TObject);
    procedure Gerencia1Click(Sender: TObject);
    procedure Novos1Click(Sender: TObject);
    procedure Regeratodo1Click(Sender: TObject);
    procedure ATBBATBAutoExpClick(Sender: TObject);
    procedure Cadastrodeperguntas1Click(Sender: TObject);
    procedure BtArrumaPrgClick(Sender: TObject);
    procedure extosbinrios1Click(Sender: TObject);
    procedure ATBBLehCodBarraClick(Sender: TObject);
    procedure Novajanela1Click(Sender: TObject);
    procedure Existente1Click(Sender: TObject);
    procedure AGBOSProtPsqClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ATBImpClick(Sender: TObject);
    procedure VerificaBDWeb1Click(Sender: TObject);
    procedure AGBPosVdaGerClick(Sender: TObject);
    procedure Statusdecompromissos1Click(Sender: TObject);
    procedure GerenciaNFSe1Click(Sender: TObject);
    procedure DeclaraodeServio1Click(Sender: TObject);
    procedure FaturamentoMensal1Click(Sender: TObject);
    procedure Configuraodeemissesmensais1Click(Sender: TObject);
    procedure AGBContratos2Click(Sender: TObject);
    procedure AGBGraGruMoWClick(Sender: TObject);
    procedure AdvGlowButton38Click(Sender: TObject);
    procedure Chamadasatendidas1Click(Sender: TObject);
    procedure Cadastros1Click(Sender: TObject);
    procedure Pesquisaemgrade1Click(Sender: TObject);
    procedure ATBBLastWorkClick(Sender: TObject);
    procedure AGBOSPrnClick(Sender: TObject);
    procedure AdvGlowButton42Click(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure ATBOSCabClick(Sender: TObject);
    procedure AGBRMIP_CadClick(Sender: TObject);
    procedure AGBRMIPCfgCabClick(Sender: TObject);
    procedure Rotas1Click(Sender: TObject);
    procedure AdvGlowButton24Click(Sender: TObject);
    procedure Arenovar1Click(Sender: TObject);
    procedure AdvGlowButton50Click(Sender: TObject);
    procedure Automoo1Click(Sender: TObject);
    procedure Mobilidade2Click(Sender: TObject);
    procedure AGBStqBalCadClick(Sender: TObject);
    procedure AGBStqManCadClick(Sender: TObject);
    procedure AGBStqCenCadClick(Sender: TObject);
    procedure AGBGraImpListaClick(Sender: TObject);
    procedure Motivospsgarantia1Click(Sender: TObject);
    procedure AGBGarantRnwClick(Sender: TObject);
    procedure AdvGlowButton51Click(Sender: TObject);
    procedure AdvGlowButton52Click(Sender: TObject);
    procedure AGBStqCenCabClick(Sender: TObject);
    procedure AdvGlowButton35Click(Sender: TObject);
    procedure AdvToolBarButton10Click(Sender: TObject);
    procedure AdvGlowButton37Click(Sender: TObject);
    procedure Avisos1Click(Sender: TObject);
    procedure Perfis2Click(Sender: TObject);
    procedure Janelas1Click(Sender: TObject);
    procedure Chamadasrecebidas1Click(Sender: TObject);
    procedure AGBNFeDestClick(Sender: TObject);
    procedure AGBNFeLoad_InnClick(Sender: TObject);
    procedure AGBNFeDesDowCClick(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AdvGlowButton43Click(Sender: TObject);
    procedure AdvGlowButton4Click(Sender: TObject);
    procedure Custos1Click(Sender: TObject);
    procedure Precos1Click(Sender: TObject);
    procedure GradeTamanhos1Click(Sender: TObject);
    procedure Reduzidodeprodutos1Click(Sender: TObject);
    procedure ProdutosaAplicar1Click(Sender: TObject);
    procedure ProdutosaMonitorar1Click(Sender: TObject);
    procedure EquipamentosaAplicar1Click(Sender: TObject);
    procedure EquipamentoMonitorar1Click(Sender: TObject);
    procedure MarcasdeEmpresas1Click(Sender: TObject);
    procedure OpesGrade1Click(Sender: TObject);
    procedure Prlanamentos1Click(Sender: TObject);
    procedure Listadascontas1Click(Sender: TObject);
    procedure Centrodecustos1Click(Sender: TObject);
    procedure Reiniciacomemisses1Click(Sender: TObject);
    procedure Corrigeerrosemisses1Click(Sender: TObject);
    procedure AdvGlowButton5Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure AdvGlowButton10Click(Sender: TObject);
    procedure AdvGlowButton113Click(Sender: TObject);
    procedure AdvGlowButton105Click(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AdvGlowButton12Click(Sender: TObject);
    procedure AGBAgeEqiCabClick(Sender: TObject);
    procedure AGBOSProtSai2Click(Sender: TObject);
    procedure AGBOSProtSai1Click(Sender: TObject);
    procedure AdvGlowButton13Click(Sender: TObject);
    procedure AdvGlowButton55Click(Sender: TObject);
    procedure AdvGlowButton14Click(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure sd1FormSkin(Sender: TObject; aName: string; var DoSkin: Boolean);
    procedure AdvGlowButton15Click(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure BtSVG2Click(Sender: TObject);
    procedure Formadeapresentaodosprodutos1Click(Sender: TObject);
    procedure AdvGlowButton167Click(Sender: TObject);
    procedure AdvGlowButton18Click(Sender: TObject);
    procedure AdvGlowButton40Click(Sender: TObject);
    procedure AdvGlowButton20Click(Sender: TObject);
    procedure AdvGlowButton21Click(Sender: TObject);
    procedure AdvGlowButton46Click(Sender: TObject);
  private
    { Private declarations }
    FBorda, FCursorPosX, FCursorPosY: Integer;
    FALiberar: Boolean;
    //
    procedure AtzSdoContas;
    procedure GerenciaFinancas;
    //
    procedure MostraBackup3;
    procedure MostraOpcoes;
    procedure MostraLogoff;
    procedure MostraMatriz();
    procedure MostraParamsEmp();
    procedure MostraAnotacoes();
    procedure SkinMenu(Index: integer);
    //
    //procedure RecriaTiposDeProdutoPadrao();
    procedure SincronizarTabelasWeb();
    procedure MostraAgeEqiCab(Codigo: Integer);
  public
    { Public declarations }
    {FTDI: TTDI;}
    //
    FDuplicata, FCheque, FVerArqSCX, FEntInt, FTipoNovoEnti, FCliInt: Integer;
    FLDataIni, FLDataFim: TDateTime;
    FTipoEntradTitu: String;
    FImportPath: String;
    FTipoEntradaDig, FTipoEntradaNFe, FTipoEntradaEFD, FCliIntUnico: Integer;

    procedure MostraServicoDesServico(Codigo: Integer);
    procedure MostraMobiliCad(Codigo: Integer);
    procedure AcoesIniciaisDoAplicativo();
    procedure ShowHint(Sender: TObject);
    procedure RetornoCNAB;
    function PreparaMinutosSQL: Boolean;
    function AdicionaMinutosSQL(HI, HF: TTime): Boolean;

    procedure CadastroDeCarteiras(TipoCarteira, ItemCarteira, x: Integer);
    procedure CadastroEntidades(Entidade: Integer; AbrirEmAba: Boolean;
              EntTipo: TUnEntTipo = uetNenhum);

    procedure SelecionaImagemdefundo;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
              Entidade: Integer; AbrirEmAba: Boolean);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade1: TStringGrid);
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    procedure CadastroDeContasSdoLista();
    function CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
    procedure TransfereEntreContas(Entidade, CtaOrig, CtaDest: Integer;
              Data: TDateTime; Valor: Double);
    procedure MostraCarteiras(Carteira: Integer);

    procedure MostraCRO();
    function  MostraFormCunsCad(Tabed: Boolean; Codigo, SiapTerCad: Integer;
              Unique: Boolean = False; Aba: Integer = 0;
              AbreSiapTerFlh: Boolean = False; AbreCroqui: Boolean = False): TForm;
    procedure MostraFormDescanso();
    procedure MostraFormObjetos();
    procedure MostraFormFormulas(QrOSFrmRec: TmySQLQuery; Codigo, Conta: Integer);
    procedure MostraFormFinalidads();
    procedure MostraFormTpConstrus();
    procedure MostraFormCxaFormas();
    procedure MostraFormCxaMaters();
    procedure MostraFormDependType();
    procedure MostraFormPraga_A(Codigo: Integer);
    procedure MostraFormPraga_Z();
    procedure MostraFormDependenci();
    procedure MostraFormGraFabCad(Controle: Integer);
    procedure MostraFormPipRapido(Codigo: Integer; SQLType: TSQLType;
              Entidade, Equipamento: Integer; PermiteDesativInutili: Boolean);
    procedure MostraFormPipVarios(SQLType: TSQLType; Equipamento: Integer);
    procedure MostraFormPrgLstCab(Codigo, Controle: Integer);
    procedure MostraFormTxtGeneric(Codigo: Integer);
    procedure MostraFormCunsImgNCT(Codigo: Integer);
    procedure MostraFormPreEmail(Codigo: Integer);
    procedure MostraRMIP();
    procedure MostraMotDesativ(Codigo: Integer);
    procedure MostraFrmApres(Codigo: Integer);

    procedure DefParams;
    procedure ReabreCarteiras(Cliente, LocCart: Integer);
    procedure LocCod(Atual, Codigo: Integer);
    // Cashier
    function CartaoDeFatura: Integer;
    function CompensacaoDeFatura: String;
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    function VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    procedure CadastroDeContasNiv();
    procedure CadastroBancos();
    procedure ReCaptionComponentesDeForm(Form: TForm(*; Memo: TMemo*));
    //
    procedure DefineVarsCliInt(Empresa: Integer);
    procedure BalanceteConfiguravel(TabLctA: String);
    function  PreparaPesquisaFinanceira(TabLctA: String): Boolean;
    procedure ExtratosFinanceirosEmpresaUnica(TabLctA: String);
    //
    // NFe
(*
    function CriaFormEntradaCab(MaterialNF: TTipoMaterialNF;
             ShowForm: Boolean; IDCtrl: Integer): Boolean;
*)
    //
    // Estoque
    {
    procedure MostraFormGraG1EqAp();
    procedure MostraFormGraG1EqMo();
    procedure MostraFormGraG1PrAp();
    procedure MostraFormGraG1PrMo();
    }
    procedure MostraFormGraG1Prod(Servico: TGraBugsServi; Nivel1: Integer);
    procedure MostraFormGraG1Equi(Servico: TGraBugsServi; Nivel1: Integer);

    // Fim Estoque
    procedure MostraFormContratos(Codigo: Integer);
    procedure MostraFormTipoAplica(Codigo: Integer);
    procedure MostraFormStatusOSs();
    procedure MostraFormStatusProvidencias();
    procedure MostraFormDiarioGer2(Assunto, Empresa, Entidade, Terceiro,
              Interlocutor, Acao, UserGenero, Depto: Integer; TemUserGenero,
              SohInterloctrRelacionado: Boolean; DataIni, DataFim: TDateTime;
              ParteTexto: String; ExecutaPesquisa: Boolean);
    procedure MostraFormOSProtSai(Tabed, NewSession: Boolean);
    //procedure MostraFormMobiliUni();
    procedure MostraFormPipCad(Codigo: Integer; Entidade: Integer = 0);
    procedure VerificaListasPerguntas();
    function  AcaoEspecificaDeApp(Servico: String): Boolean;
    procedure MostraOSCabByOSSrv(OSSrv: Integer);
    procedure MostraOSCab(AbrirEmAba: Boolean; Codigo, Grupo, Lugar,
              Opcao: Integer; OSSrv: Integer = 0);
    function  SelecionaImg(DirPadrao, ArquivoSel: String; Ext: TStringList;
              TravaDir: Boolean = False): String;
    function  FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;
    //Notifica��es
    function  NotificacoesVerifica(QueryNotifi: TmySQLQuery; DataHora: TDateTime): Boolean;
    procedure NotificacoesJanelas(TipoNotifi: TNotifi; DataHora: TDateTime);
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses //ContasSdoAll, ContasHistAtz2, ContasHistSdo, ContasSdoUni,
  //ContasSdoTrf
  UnMyObjects, Module, Entidades, MyGlyfs, MyDBCheck, Bugstrol_Dmk,
  Opcoes, MyListas, MyVCLSkin, UCreate, Backup3, CalcPercent, ContasNiv,
  ModuleGeral, Entidade2, Matriz, Anotacoes, Bancos, LinkRankSkin, UnDmkWeb,
  ModuleFin, ParamsEmp, SPED_EFD_Tabelas, IBGE_LoadTabs, FPFunci, CunsCad,
  FavoritosG, DiarioAss, (*DiarioAdd_02,*) DiarioGer2, Extratos2, ModuleLct2,
  CashBal, QRCodeImp, GetValor, GraTamCad, GraGruReduzido,
  (*EntradaCab,*) UnPraz_PF, DirectTerminal, SAC_01, {OSCab,} Dependenci,
  GSM_Serial, Descanso, GraFabCad (*, GraG1PrMo, GraG1EqAp, GraG1EqMo*),
  (*GraG1PrAp, GraG1EqAp,*) UnAppListas, ABD_Mod, ABD_Pragas, DownCSIRO,
  Praga_Z, ModuleBloGeren, IGPM, BloSemLot, PreEmail, Contratos, ModPediVda,
  CNAE21Cad, UnDmkWeb_Jan, NFeLEnC_0200, NFeSteps_0200, ModuleNFe_0000,
  NFSe_0000_Module, (*NFSe_Steps_0201,*) NFSe_PF_0000, AgenGrCab, UnGFat_Jan,
  NFSe_PF_0201, NFSeFatCab, NFSeMenCab, NFSe_LoadXML_0201, DesServico, GraG1,
  IBGE_DTB, Formulas, DmkDAC_PF, LctGer2, TxtGeneric, PipRapido, PipVarios,
  PrgLstCab, CfgAtributos, OSCab2, AgendaEve, CunsImgNCT, ContatosEnt, PosVdaCab,
  UnOSApp_PF, OSPrvGer, PipCad, OSPsq, AgeEqiCab, DocsCab, Feriados, SelfGer2,
  UnBloquetos,
  // Provis�rio
  ModAgenda, EntiSel, UnMyVCLRef, UnBloqGerl_Jan, UnGrl_Vars,
  // Fim Provisorio
  CodBarraRead, CreateBugs, OSProtSai, OSProtPsq, (*EstatusOSs,*) OSPs2,
  CadAnyStatus, PosVdaGer, NFSe_Edit_0201, NFSe_NFSPesq, GraGruMoW, ContratPsq2,
  OSPrn, UnEntities, UnWUsersJan, UnFTPJan, RMIP_Cab, RMIPCfgCad, FluxCxaDia2,
  ReIniLctsSoComOpnEmiss, PreLctCab, ReceDesp2, ContasPlanoLista, CentroCusto,
  RotaLatLon, UnContratUnit, AllToRenew, (*MobiliUni,*) MobiliCad, WShowMemo,
  NFe_PF, OSFlhAdm, ModCRO, AgendaGer, UnAgendaGerApp, UnAgendaGerAll,
  UnOSAll_PF, UnBina_PF, UnGrade_Jan, UnChekLstJan, About, UnProtocoUnit,
  SelImg, UnFinanceiroJan, UnFinanceiro2Jan, CorrigeCidadeEntidade,
  DmkSVGEditor, UnFixBugs, UnAppPF, UMySQLDB, UnDmkWeb2_Jan, UnALL_Jan;

const
  FAltLin = CO_POLEGADA / 2.16;

var
  FAdvToolBarPager_Hei_Max: Integer;

{$R *.DFM}

function TFmPrincipal.SelecionaImg(DirPadrao, ArquivoSel: String;
  Ext: TStringList; TravaDir: Boolean = False): String;
begin
  Result := '';
  //
  if DBCheck.CriaFm(TFmSelImg, FmSelImg, afmoLiberado) then
  begin
    FmSelImg.FArquivoSel              := ArquivoSel;
    FmSelImg.FTravaDir                := TravaDir;
    FmSelImg.FExt                     := Ext;
    FmSelImg.EdDiretorio.ValueVariant := DirPadrao;//ExtractFileDir(DirPadrao);
    FmSelImg.ShowModal;
    //
    Result := FmSelImg.FArquivo;
    //
    FmSelImg.Destroy;
  end;
end;

procedure TFmPrincipal.Fatosgeradores1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'fatogeradr', 60, ncGerlSeq1,
  'Cadastro de Fatos Geradores (Motivo da OS)',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.Fatosgeradoresdoscompromissos1Click(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgenFgCad();
end;

procedure TFmPrincipal.FaturamentoMensal1Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeFatCab(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.Feriados1Click(Sender: TObject);
begin
  //MyObjects.FormShow(TFmFeriados, FmFeriados);
  if DBCheck.CriaFm(TFmFeriados, FmFeriados, afmoNegarComAviso) then
  begin
    FmFeriados.ShowModal;
    FmFeriados.Destroy;
  end;
end;

procedure TFmPrincipal.Finalidades1Click(Sender: TObject);
begin
  MostraFormFinalidads();
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then Timer1.Enabled := True;
end;

procedure TFmPrincipal.MostraFrmApres(Codigo: Integer);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'FrmApres', 100, ncGerlSeq1,
    'Forma de apresenta��o do produto', [], False, Null, [], [], False,
    Codigo);
end;

procedure TFmPrincipal.Formadeapresentaodosprodutos1Click(Sender: TObject);
begin
  MostraFrmApres(0);
end;

procedure TFmPrincipal.Formadecontato1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'formcontat', 60, ncGerlSeq1,
    'Cadastro de Forma de Contato', [], False, Null, [], [], False);
end;

procedure TFmPrincipal.Formas1Click(Sender: TObject);
begin
  MostraFormCxaFormas();
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
begin
  VAR_USA_MODULO_CRO := True;
  //
  VAR_TemContratoMensalidade_FldCodigo := 'Codigo';
  VAR_TemContratoMensalidade_FldNome := 'Nome';
  VAR_TemContratoMensalidade_TabNome := 'contratos';
  //
  dmkPF.AcoesAntesDeIniciarApp_dmk();
  //
  FBorda := (Width - ClientWidth) div 2;
  //
  VAR_TIPO_TAB_LCT := 1;
  VAR_MULTIPLAS_TAB_LCT := True;
  //
  GERAL_MODELO_FORM_ENTIDADES := fmcadEntidade2;
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_TYPE_LOG := ttlFiliLog;
  FEntInt := -1;
  VAR_USA_TAG_BITBTN := True;
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  VAR_KIND_DEPTO := kdOS1;
  VAR_LA_PRINCIPAL1   := LaAviso1;
  VAR_LA_PRINCIPAL2   := LaAviso1;
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  SkinMenu(MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  //////////////////////////////////////////////////////////////////////////////
  FLDataIni := Date - Geral.ReadAppKey('Dias', Application.Title,
    ktInteger, 60, HKEY_LOCAL_MACHINE);
  FLDataFim := Date;
  //////////////////////////////////////////////////////////////////////////////
  //
  Application.OnHint      := ShowHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  //2017-07-19 => N�o ativar por causa da c�mera Application.OnIdle := AppIdle;
  TimerIdle.Interval := VAR_TIMERIDLEITERVAL;
  TimerIdle.Enabled  := True;

  // Deixar invis�vel
  AlphaBlendValue := 0;
  AlphaBlend := True;
  //
  {
  // Ini teste TDI
  FTDI := TTDI.Create(Self, TFmDescanso);
  FTDI.MostrarMenuPopup := True;
  // Fim teste TDI
  }
  PageControl1.Align := alClient;
  Width := 1600;
  Height := 870;
  FAdvToolBarPager_Hei_Max := AdvToolBarPager1.Height; // 225
  //
  //  Atendimento
  //MyObjects.FormTDICria(TFmSAC_01, PageControl1, AdvToolBarPager1);
  //  Di�rio
  FmPrincipal.WindowState := wsMaximized;
  //MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1);
  // d� erro!! vou abrir no AcoesIniciaisDoAplicativo();
  //MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
  AdvToolBarPager1.Collaps;
end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
  if VAR_WEB_CONECTADO = 100 then
    DmkWeb.DesconectarUsuarioWEB;
end;

procedure TFmPrincipal.FormResize(Sender: TObject);
var
  Empresa: String;
begin
{
  if DModG.QrFiliLog.State <> dsInactive then
  begin
    if DModG.QrFiliLog.RecordCount = 1 then
      Empresa := Geral.FF0(DModG.QrFiliLogNom) + ' - ' +
        DModG.QrFiliLogNomeEmp.Value;
  end else
    Empresa := '';
}
  Empresa := Geral.FF0(VAR_LIB_EMPRESA_SEL) + ' - ' + VAR_LIB_EMPRESA_SEL_TXT;
  MyObjects.Informa2(LaAviso2, LaAviso1, False, Empresa +
  '  {L: ' + Geral.FF0(Width) + ' - A: ' + Geral.FF0(Height) + '}');
end;

procedure TFmPrincipal.Frmulas1Click(Sender: TObject);
begin
  MostraFormFormulas(nil, 0, 0);
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso, PageControl1);
end;

procedure TFmPrincipal.Servios1Click(Sender: TObject);
begin
{
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'desservico', 60, ncGerlSeq1,
  'Cadastro de Servi�os');
}
  UnCfgCadLista.MostraCadRegistroSimples(Dmod.MyDB, DmModOS.TbDesServico,
    DmModOS.DsDesServico, ncGerlSeq1, 'Cadastro de Servi�os');
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ZZTerminate := True;
    (*DmDados.TbMaster.Open;
    DmDados.TbMaster.Edit;
    if Trunc(Date) > DmDados.TbMasterLA.Value then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value +
      (Trunc(Date) - Trunc(DmDados.TbMasterLA.Value));
    if Trunc(Date) < DmDados.TbMasterLA.Value then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 3;
    if (Trunc(Date) = DmDados.TbMasterLA.Value) and
       (Time < DmDados.TbMasterLH.Value)  then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 1;
    DmDados.TbMasterLA.Value := Trunc(Date);
    DmDados.TbMasterLH.Value := Time;
    DmDados.TbMaster.Post;
    DmDados.TbMaster.Close;
    FmSkin.Close;
    FmSkin.Destroy;*)
  Application.Terminate;
end;

procedure TFmPrincipal.Pragas2Click(Sender: TObject);
begin
  MostraFormPraga_Z();
end;

function TFmPrincipal.PreparaMinutosSQL: Boolean;
//var
//  i: Integer;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM Ocupacao');
    Dmod.QrUpdL.ExecSQL;
    ///
(*    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO Ocupacao SET Min=:P0');
    for i := 1 to 1440 do
    begin
      Dmod.QrUpdL.Params[0].AsInteger := I;
      Dmod.QrUpdL.ExecSQL;
    end;*)
  except
    Result := False
  end;
end;

function TFmPrincipal.PreparaPesquisaFinanceira(TabLctA: String): Boolean;
var
  MyCursor: TCursor;
begin
  Result := True;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  //
 { TODO : VER SE PRECISA ALGO! }
{
  UMyMod.AbreQuery(DmLot.QrLot);
  if DmLot.QrLot.RecordCount > 0 then
  begin
    if Geral.MensagemBox('Existem ' + Geral.FF0(DmLot.QrLot.RecordCount) +
    ' lotes de border�s n�o encerrados que poder�o distorcer valores financeiros!'
    + 'Deseja continuar assim mesmo?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      Result := False;
      Screen.Cursor := MyCursor;
      Exit;
    end;
  end;
  UFinanceiro.LancamentosComProblemas(TabLctA);
  //
  if AtualizaDefinicaoDeFatID_0301() then
    ReopenFatID_0301();
  //
}

  Screen.Cursor := MyCursor;
  //
end;

procedure TFmPrincipal.PrincpiosAtivos1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadItemGrupo(Dmod.MyDB, 'G1PriAtiI',
    AppListas.ListaNiveisPrincipiosAtivos(), ncGerlSeq1);
end;

procedure TFmPrincipal.Prlanamentos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreLctCab, FmPreLctCab, afmoNegarComAviso) then
  begin
    FmPreLctCab.ShowModal;
    FmPreLctCab.Destroy;
  end;
end;

procedure TFmPrincipal.ProdutosaAplicar1Click(Sender: TObject);
begin
  MostraFormGraG1Prod(gbsAplica, 0);
end;

procedure TFmPrincipal.ProdutosaMonitorar1Click(Sender: TObject);
begin
  MostraFormGraG1Prod(gbsMonitora, 0);
  //MostraFormGraG1PrMo();
end;

procedure TFmPrincipal.PsVenda1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPosVdaCab, FmPosVdaCab, afmoNegarComAviso) then
  begin
    FmPosVdaCab.ShowModal;
    FmPosVdaCab.Destroy;
  end;
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO Ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := I;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal.AGBPlanoCtaNiveisClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano();
end;

procedure TFmPrincipal.AGBPlanoCtaContasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.AGBPlanoCtaSubgruposClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.AGBPlanoCtaGruposClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.AGBPlanoCtaConjuntosClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.AGBPlanoCtaPlanosClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.AdvGlowButton105Click(Sender: TObject);
begin
  Grade_Jan.MostraFormPrdGrupTip(0);
end;

procedure TFmPrincipal.AdvGlowButton10Click(Sender: TObject);
begin
  MostraFormFormulas(nil, 0, 0);
end;

procedure TFmPrincipal.AdvGlowButton113Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruN(0);
end;

procedure TFmPrincipal.AdvGlowButton11Click(Sender: TObject);
begin
  MostraServicoDesServico(0);
end;

procedure TFmPrincipal.AdvGlowButton12Click(Sender: TObject);
begin
  MostraAgeEqiCab(0);
end;

procedure TFmPrincipal.AdvGlowButton13Click(Sender: TObject);
begin
  UBloquetos.MostraBloGerenInadImp(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton14Click(Sender: TObject);
begin
  UnNotificacoes.MostraFmNotificacoes(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton15Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  Dmod.AtualizaGraTabAppProdutos();
end;

procedure TFmPrincipal.AdvGlowButton167Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal.AdvGlowButton16Click(Sender: TObject);
begin
  Dmod.MostraFormOpcoesBugs(0);
end;

procedure TFmPrincipal.AdvGlowButton17Click(Sender: TObject);
begin
  MostraOpcoes;
end;

procedure TFmPrincipal.AdvGlowButton18Click(Sender: TObject);
begin
  Financeiro2Jan.MostraFormCadastroPlanoCtas();
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
begin
  Application.CreateForm(TFmCorrigeCidadeEntidade, FmCorrigeCidadeEntidade);
  FmCorrigeCidadeEntidade.ShowModal;
  FmCorrigeCidadeEntidade.Destroy;
end;

procedure TFmPrincipal.AGBSAC_01Click(Sender: TObject);
begin
  //FmSAC_01.Show;
  MyObjects.FormTDICria(TFmSAC_01, PageControl1, AdvToolBarPager1, False);
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
begin
  CadastroEntidades(0, True);
end;

procedure TFmPrincipal.AGBCNAB_CfgClick(Sender: TObject);
begin
  UBloqGerl_Jan.CadastroCNAB_Cfg;
  (* Usado no BLQ v02_01
  UBloquetos.CadastroCNAB_Cfg;
  *)
end;

procedure TFmPrincipal.AGBBloArreClick(Sender: TObject);
begin
  UBloquetos.MostraBloArre(0, 0);
end;

procedure TFmPrincipal.AGBBloGerenClick(Sender: TObject);
begin
  UBloquetos.MostraBloGeren(-1, 0, 0, 0, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton20Click(Sender: TObject);
begin
  Financeiro2Jan.MostraFormCadastroCarteiras(0);
end;

procedure TFmPrincipal.AdvGlowButton21Click(Sender: TObject);
begin
  Financeiro2Jan.MostraFormLctGer3(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton23Click(Sender: TObject);
begin
  MostraFormPreEmail(0);
end;

procedure TFmPrincipal.AdvGlowButton24Click(Sender: TObject);
(*const
  Max = 896;
var
  I, J: Integer;
  Row:  pRGBTripleArray;
  Bitmap: TBitmap;
*)
begin
(*
  //Geral.MB_Aviso(Geral.FF0(Dmod.MyDB.Ping));
  Bitmap := TBitmap.Create;
  try
    Bitmap.PixelFormat := pf24bit;
    Bitmap.Width  := Max;
    Bitmap.Height := 30;

    for J := 0 to Bitmap.Height - 1 do
    begin
      Row := Bitmap.Scanline[J];
      for I := 0 to Bitmap.Width - 1 do
      begin
        with Row[I] do
        begin
          // Red
          if I < 256 then
            rgbtRed := I
          else
          if I > 768 then
            rgbtRed := Max - I + 127
          else
            rgbtRed := 255;
          // Green
          if I <= 255 then
            rgbtGreen := 255
          else
          if I > 511 then
            rgbtGreen := 0
          else
            rgbtGreen := 511 - I;
          // Blue
          if I <= 512 then
            rgbtBlue := 0
          else
          if I >= 768 then
            rgbtBlue := 255
          else
            rgbtBlue := I - 512
        end;
      end;
    end;;
    // Display on screen
    Image1.Picture.Graphic := Bitmap;
    Image1.Picture.Bitmap.SaveToFile('C:\Dermatek\SpectroBug.bmp');
  finally
    Bitmap.Free
  end;
*)
end;

procedure TFmPrincipal.AdvGlowButton25Click(Sender: TObject);
begin
  (*
  if DBCheck.CriaFm(TFmTesteCreateDir, FmTesteCreateDir, afmoLiberado) then
  begin
    FmTesteCreateDir.ShowModal;
    FmTesteCreateDir.Destroy;
  end;
  *)
end;

procedure TFmPrincipal.AdvGlowButton26Click(Sender: TObject);
var
  I, N, Codigo, Controle, Ordem: Integer;
  Qry: TmySQLQuery;
  Cod: array of Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    GBAvisos1.Visible := True;
    MyObjects.Informa2(LaAvisoA1, LaAvisoA2, True, 'Iniciando ordena��o!');
    //
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DISTINCT Codigo ',
      'FROM cunsimgcab ',
      'ORDER BY Codigo ',
      '']);
      //
      N := Qry.RecordCount;
      if N > 0 then
      begin
        SetLength(Cod, N);
        Qry.First;
        while not Qry.Eof do
        begin
          I := Qry.RecNo -1;
          Cod[I] := Qry.FieldByName('Codigo').AsInteger;
          //
          Qry.Next;
        end;
        for I := 0 to N - 1 do
        begin
          Codigo := Cod[I];
          MyObjects.Informa2(LaAvisoA1, LaAvisoA2, True, 'Ordenando cliente ' +
            Geral.FF0(Codigo));
          Ordem := 0;
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Controle ',
          'FROM cunsimgcab ',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          'ORDER BY Controle ',
          '']);
          Qry.First;
          while not Qry.Eof do
          begin
            Controle       := Qry.FieldByName('Controle').AsInteger;
            Ordem          := Ordem + 1;
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cunsimgcab', False, [
            'Ordem'], [
            'Controle'], [
            Ordem], [
            Controle], True);
            //
            Qry.Next;
          end;
        end;
      end;
      //
      MyObjects.Informa2(LaAvisoA1, LaAvisoA2, False, '...');
      GBAvisos1.Visible := False;
    finally
      Qry.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  ProtocoUnit.MostraFormProtocolos(0, 0);
end;

procedure TFmPrincipal.AdvGlowButton2Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormLayoutNFe();
end;

procedure TFmPrincipal.AGBFinancas2Click(Sender: TObject);
var
  DmLctX: TDataModule;
begin
  if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
    TDmLct2(DmLctX).GerenciaEmpresa(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBBancosClick(Sender: TObject);
begin
  CadastroBancos();
end;

procedure TFmPrincipal.AdvGlowButton30Click(Sender: TObject);
begin
  ProtocoUnit.MostraFormProtocoMot();
end;

procedure TFmPrincipal.AdvGlowButton31Click(Sender: TObject);
begin
  ProtocoUnit.MostraFormProtocoOco()
end;

procedure TFmPrincipal.AdvGlowButton35Click(Sender: TObject);
begin
  SincronizarTabelasWeb();
end;

procedure TFmPrincipal.AGBBloOpcoesClick(Sender: TObject);
begin
  UBloquetos.CadastroDeBloOpcoes;
end;

procedure TFmPrincipal.AdvGlowButton37Click(Sender: TObject);
begin
  UWUsersJan.MostraWUsers(0);
end;

procedure TFmPrincipal.AdvGlowButton38Click(Sender: TObject);
begin
  DmkWeb2_Jan.MostraWOpcoes(Dmod.MyDBn);
end;

procedure TFmPrincipal.AdvGlowButton3Click(Sender: TObject);
begin
  MostraAnotacoes();
end;

procedure TFmPrincipal.AdvGlowButton40Click(Sender: TObject);
begin
  Financeiro2Jan.MostraFormCadastroCentroCusto(0);
end;

procedure TFmPrincipal.AdvGlowButton42Click(Sender: TObject);
begin
  UFTPJan.MostraFTPWebArq(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton43Click(Sender: TObject);
var
  Versao: Integer;
  ArqNome: String;
begin
  DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_SVG, 'SVG',
    Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), 0, 0, DModG.ObtemAgora(), nil,
    dtImag, Versao, ArqNome, False);
end;

procedure TFmPrincipal.AdvGlowButton46Click(Sender: TObject);
begin
  Financeiro2Jan.MostraFormOpcoes();
end;

procedure TFmPrincipal.AdvGlowButton4Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AGBPlanoCtaListaClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmPrincipal.AGBABDPragasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmABD_Pragas, FmABD_Pragas, afmoNegarComAviso) then
  begin
    FmABD_Pragas.ShowModal;
    FmABD_Pragas.Destroy;
  end;
end;

procedure TFmPrincipal.AGBAgeEqiCabClick(Sender: TObject);
begin
  MostraAgeEqiCab(0);
end;

procedure TFmPrincipal.AGBAgendaEveClick(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgendaEve(0, 0, False, 0, 0, 0, 0, []);
end;

procedure TFmPrincipal.AGBAgendaGerClick(Sender: TObject);
begin
  //FmPrincipal.WindowState := wsMaximized;
  //MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1, False);
  AgendaGerAll.MostraFormAgendaGer(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBAgenGrCabClick(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgenGrCab(0);
end;

procedure TFmPrincipal.AGBCFOP2003Click(Sender: TObject);
begin
  Grade_Jan.MostraFormCFOP2003();
end;

procedure TFmPrincipal.AGBCntngncClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCntngnc();
end;

procedure TFmPrincipal.AGBConsultaNFeClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeConsulta();
end;

procedure TFmPrincipal.AGBContratos2Click(Sender: TObject);
begin
  MyObjects.FormTDICria(TFmContratos, PageControl1, AdvToolBarPager1, True);
end;

procedure TFmPrincipal.AGBCorrigeFatIDClick(Sender: TObject);
var
  FatID, IDIts, S, N: Integer;
  Qry: TmySQLQuery;
begin
  S := 0;
  N := 0;
{
  GBAvisos1.Visible := True;
  DmProd.CorrigeFatID_Venda(PB1, LaAvisoA1, LaAvisoA2);
  GBAvisos1.Visible := True;
}
  GBAvisos1.Visible := True;
  GBAvisos1.Visible := True;
  PB1.Position := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT 4101 FatID, IDIts   ',
    'FROM osfrmrec ',
    'WHERE RatifUso=1 ',
    'AND SMI_IDCtrl = 0  ',
    ' ',
    'UNION ',
    ' ',
    'SELECT 4102 FatID, IDIts   ',
    'FROM osmonrec ',
    'WHERE RatifUso=1 ',
    'AND SMI_IDCtrl = 0  ',
    ' ',
    'UNION ',
    ' ',
    'SELECT 4102 + Acao FatID, IDIts   ',
    'FROM ospipitspr ',
    'WHERE SMI_IDCtrl = 0  ',
    'AND Acao IN (1,2)',
    ' ',
    '']);
    PB1.Max := Qry.RecordCount;
    //
    Qry.First;
    while not Qry.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      FatID  := Qry.FieldByName('FatID').AsInteger;
      IDIts  := Qry.FieldByName('IDIts').AsInteger;
      //
      try
        DmModOS.BaixaInsumoDeOS(FatID, IDIts, DModG.ObtemAgora());
        S := S + 1;
      except
        N := N + 1;
      end;
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  PB1.Position := 0;
  Geral.MB_Info('Verifica��o finalizada:' + sLineBreak + sLineBreak +
  Geral.FF0(S) + ' itens foram baixados!' + sLineBreak +
  Geral.FF0(N) + ' itens N�O foram baixados por problemas!');
end;

procedure TFmPrincipal.AGBCunsCad1Click(Sender: TObject);
begin
  MostraFormCunsCad(True, 0, 0);
end;

procedure TFmPrincipal.AGBDesServicoClick(Sender: TObject);
begin
  MostraServicoDesServico(0);
end;

procedure TFmPrincipal.AGBDiaAssClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDiarioAss, FmDiarioAss, afmoNegarComAviso) then
  begin
    FmDiarioAss.ShowModal;
    FmDiarioAss.Destroy;
  end;
end;

procedure TFmPrincipal.AGBDiaEvenClick(Sender: TObject);
begin
{ N�o usa o DiarioAdd original!
  if DBCheck.CriaFm(TFmDiarioAdd, FmDiarioAdd, afmoNegarComAviso) then
  begin
    FmDiarioAdd.FChamou        := -1;
    FmDiarioAdd.ImgTipo.SQLType := stIns;
    FmDiarioAdd.ShowModal;
    FmDiarioAdd.Destroy;
  end;
}
end;

procedure TFmPrincipal.AGBDiaGerClick(Sender: TObject);
{
var
  Assunto, Empresa, Entidade,
  Terceiro, Interlocutor, Acao, UserGenero, Depto: Integer;
  TemUserGenero, SohInterloctrRelacionado: Boolean; DataIni, DataFim: TDateTime;
  ParteTexto: String;
}
begin
{
  Assunto                  := 0;
  Empresa                  := 0;
  Entidade                 := 0;
  Terceiro                 := 0;
  Interlocutor             := 0;
  Acao                     := 0;
  UserGenero               := 0;
  Depto                    := 0;
  TemUserGenero            := False;
  SohInterloctrRelacionado := True;
  DataIni                  := 0;
  DataFim                  := 0;
  ParteTexto               := '';
  //
  FmPrincipal.MostraFormDiarioGer2(Assunto, Empresa, Entidade, Terceiro,
    Interlocutor, Acao, UserGenero, Depto, TemUserGenero,
    SohInterloctrRelacionado, DataIni, DataFim, ParteTexto, False);
}
  MyObjects.FormTDICria(TFmDiarioGer2, PageControl1, AdvToolBarPager1, False);
end;

procedure TFmPrincipal.AGBDirectTerminalClick(Sender: TObject);
begin
  FmDirectTerminal.Show;
  FmDirectTerminal.EdData.Text := FormatDateTime('mmdd', Date);
  FmDirectTerminal.EdHora.Text := FormatDateTime('hhnn', Time);
  FmDirectTerminal.EdFone.SetFocus;
end;

procedure TFmPrincipal.AGBDTBClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmIBGE_DTB, FmIBGE_DTB, afmoSoAdmin) then
  begin
    FmIBGE_DTB.ShowModal;
    FmIBGE_DTB.Destroy;
  end;
end;

procedure TFmPrincipal.AGBEntidade2TabedClick(Sender: TObject);
begin
  CadastroEntidades(0, True);
end;

procedure TFmPrincipal.AGBExportaXMLClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeExportaXML();
end;

procedure TFmPrincipal.AGBFatDivGerClick(Sender: TObject);
begin
  GFat_Jan.MostraFormFatDivGer();
end;

procedure TFmPrincipal.AGBFatPedNFsClick(Sender: TObject);
const
  Cliente       = 0;
  CU_PediVda    = 0;
  ForcaCriarXML = False;
var
  EMP_FILIAL: Integer;
begin
  EMP_FILIAL := DmodG.QrFiliLogFilial.Value;
  //
  UnNFe_PF.MostraFormFatPedNFs(EMP_FILIAL, Cliente, CU_PediVda, ForcaCriarXML);
end;

procedure TFmPrincipal.AGBFilhasClick(Sender: TObject);
begin
  DmModOS.VerificaFormulasFilhas(True, True, True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBFiliaisClick(Sender: TObject);
begin
  MostraParamsEmp();
end;

procedure TFmPrincipal.AGBFisRegCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormFisRegCad(0);
end;

procedure TFmPrincipal.AGBFolhaFunciClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFpFunci, FmFpFunci, afmoNegarComAviso) then
  begin
    FmFpFunci.ShowModal;
    FmFpFunci.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFormulasClick(Sender: TObject);
begin
  MostraFormFormulas(nil, 0, 0);
end;

procedure TFmPrincipal.AGBGarantRnwClick(Sender: TObject);
begin
  DmModOS.GarantiasAVencer(DModG.ObtemAgora(), True);
end;

procedure TFmPrincipal.AGBGraGruMoWClick(Sender: TObject);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmGraGruMoW, FmGraGruMoW, afmoNegarComAviso) then
    begin
      FmGraGruMoW.ShowModal;
      FmGraGruMoW.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.AGBGraImpListaClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraImpLista();
end;

procedure TFmPrincipal.AGBListaCSIROClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDownCSIRO, FmDownCSIRO, afmoNegarComAviso) then
  begin
    FmDownCSIRO.ShowModal;
    FmDownCSIRO.Destroy;
  end;
end;

procedure TFmPrincipal.AGBListServClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormListServ();
end;

procedure TFmPrincipal.AGBMatrizClick(Sender: TObject);
begin
  MostraMatriz();
end;

procedure TFmPrincipal.AGBModelosImpClick(Sender: TObject);
begin
  Grade_Jan.MostraFormModelosImp();
end;

procedure TFmPrincipal.AGBNCMsClick(Sender: TObject);
begin
  Grade_Jan.MostraFormNCMs();
end;

procedure TFmPrincipal.AGBNfeCabAClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabA();
end;

procedure TFmPrincipal.AGBNFeDesDowCClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCnfDowC_0100();
end;

procedure TFmPrincipal.AGBNFeDestClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeDistDFeInt();
end;

procedure TFmPrincipal.AGBNFeEventosClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeEveRLoE(0);
end;

procedure TFmPrincipal.AGBNFeInfCplClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeInfCpl();
end;

procedure TFmPrincipal.AGBNFeInutClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeInut();
end;

procedure TFmPrincipal.AGBNFeJustClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeJust();
end;

procedure TFmPrincipal.AGBNFeLEncClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLEnc(0);
end;

procedure TFmPrincipal.AGBNFeLoad_InnClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Inn();
end;

procedure TFmPrincipal.AGBNFeSteps_0Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormStepsNFe_StatusServico();
end;

procedure TFmPrincipal.AGBNfe_PesqClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFePesq(True, PageControl1, AdvToolBarPager1, 0);
end;

procedure TFmPrincipal.AGBNFSeFatCabClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeFatCab(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBNFSeLRpsCClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeLRpsC_0201(0);
end;

procedure TFmPrincipal.AGBNFSeMenCabClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeMenCab(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBNFSeSrvCadClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeSrvCad();
end;

procedure TFmPrincipal.AGBNFSeStepLoteClick(Sender: TObject);
begin
{
  case DModG.VersaoNFSe(Geral.IMV(VAR_LIB_EMPRESAS)) of
    201:
    if DBCheck.CriaFm(TFmNFSe_Steps_0201, FmNFSe_Steps_0201, afmoSoMaster) then
    begin
      //FmNFSe_Steps_0201.PnLoteEnv.Visible  := True;
      FmNFSe_Steps_0201.RGAcao.Enabled     := True;
      FmNFSe_Steps_0201.PnAbrirXML.Visible := True;
      FmNFSe_Steps_0201.PnAbrirXML.Enabled := True;
      FmNFSe_Steps_0201.CkSoLer.Enabled    := True;
      //FmNFSe_Steps_0201.EdchNFe.ReadOnly   := False;
      FmNFSe_Steps_0201.PnConfirma.Visible := True;

      FmNFSe_Steps_0201.EdEmpresa.ValueVariant := -11;

      FmNFSe_Steps_0201.ShowModal;
      FmNFSe_Steps_0201.Destroy;
    end;
  end;
}
end;

procedure TFmPrincipal.AGBNFSe_EditClick(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;
  Valor         = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
  //
  TabedForm = nil;
var
  Prestador: Integer;
  SerieNF: String;
  NumNF: Integer;
begin
  Prestador := DmodG.QrFiliLogFilial.Value;
  UnNFSe_PF_0201.MostraFormNFSe(SQLType,
  Prestador, Tomador, Intermediario, MeuServico, ItemListSrv,
  Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico, nil, Valor,
  SerieNF, NumNF, TabedForm);
end;

procedure TFmPrincipal.AGBNFSE_LoadXML_0201Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFSe_LoadXML_0201, FmNFSe_LoadXML_0201, afmoSoMaster) then
  begin
    FmNFSe_LoadXML_0201.ShowModal;
    FmNFSe_LoadXML_0201.Destroy;
  end;
end;

procedure TFmPrincipal.AGBNFSe_NFSePesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(False, nil, nil);
end;

procedure TFmPrincipal.AGBNFSe_RPSPesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_RPSPesq(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBOpcoesFiliClick(Sender: TObject);
begin
  Dmod.MostraFormOpcoesFili();
end;

procedure TFmPrincipal.MostraOSCab(AbrirEmAba: Boolean; Codigo, Grupo,
  Lugar, Opcao: Integer; OSSrv: Integer = 0);
var
  Form: TForm;
begin
  // Provis�rio
  UnDmkDAC_PF.AbreMySQLQuery0(QrProvisorio1, DMod.MyDB, [
    'SELECT * ',
    'FROM oscab ',
    'WHERE Grupo=0 ',
    '']);
  // Fim provis�rio
  //
  if QrProvisorio1.RecordCount > 0 then
  begin
    Geral.MB_Aviso(
    'Clique no bot�o "Corrigir OSs j� Criadas!" na guia "Provis�rio" da aba "Gerenciamento".'
    + 'Existem ' + Geral.FF0(QrProvisorio1.RecordCount) + ' OSs orf�s de grupo!')
  end else
  begin
    if not AbrirEmAba then
    begin
      if DBCheck.CriaFm(TFmOSCab2, FmOSCab2, afmoNegarComAviso) then
      begin
        if Codigo <> 0 then
          FmOSCab2.LocCod(Grupo, Grupo, dispIndefinido, Lugar, Opcao, True, Codigo);
        FmOSCab2.FOSSrv := OSSrv;
        //
        FmOSCab2.ShowModal;
        FmOSCab2.Destroy;
      end;
    end else
    begin
      Form := MyObjects.FormTDICria(TFmOSCab2, PageControl1, AdvToolBarPager1, True, True);
      //
      if Codigo <> 0 then
        TFmOSCab2(Form).LocCod(Grupo, Grupo, dispIndefinido, Lugar, Opcao, True, Codigo);
      TFmOSCab2(Form).FOSSrv := OSSrv;
    end;
  end;
end;

procedure TFmPrincipal.MostraOSCabByOSSrv(OSSrv: Integer);
var
  Qry: TmySQLQuery;
  Codigo, Grupo, SiapTerCad, Opcao: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT osc.Codigo, osc.Grupo, ',
      'osc.SiapTerCad, osc.Opcao ',
      'FROM ossrv oss ',
      'LEFT JOIN oscab osc ON osc.Codigo = oss.Codigo ',
      'WHERE oss.Controle=' + Geral.FF0(OSSrv),
      '']);
    if Qry.RecordCount > 0 then
    begin
      Codigo     := Qry.FieldByName('Codigo').AsInteger;
      Grupo      := Qry.FieldByName('Grupo').AsInteger;
      SiapTerCad := Qry.FieldByName('SiapTerCad').AsInteger;
      Opcao      := Qry.FieldByName('Opcao').AsInteger;
    end;
  finally
    Qry.Free;
  end;
  if Codigo <> 0 then
    MostraOSCab(True, Codigo, Grupo, SiapTerCad, Opcao, OSSrv);
end;

procedure TFmPrincipal.AGBOSClick(Sender: TObject);
begin
  MostraOSCab(True, 0, 0, 0, 0);
end;

procedure TFmPrincipal.AGBOSPesqClick(Sender: TObject);
begin
  MyObjects.FormTDICria(TFmOSPsq, PageControl1, AdvToolBarPager1, True);
end;

procedure TFmPrincipal.AGBOSPrnClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOSPrn, FmOSPrn, afmoNegarComAviso) then
  begin
    FmOSPrn.ShowModal;
    FmOSPrn.Destroy;
  end;
end;

procedure TFmPrincipal.AGBOSProtPsqClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOSProtPsq, FmOSProtPsq, afmoNegarComAviso) then
  begin
    FmOSProtPsq.ShowModal;
    FmOSProtPsq.Destroy;
  end;
end;

procedure TFmPrincipal.AGBOSProtSai1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(TPopupMenu(APMOSProtSai1), AGBOSProtSai1);
end;

procedure TFmPrincipal.AGBOSProtSai2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(TPopupMenu(APMOSProtSai2), AGBOSProtSai2);
end;

procedure TFmPrincipal.AGBOSPrvClick(Sender: TObject);
begin
  MyObjects.FormTDICria(TFmOSPrvGer, PageControl1, AdvToolBarPager1, True);
end;

procedure TFmPrincipal.AGBPediPrzCab_AClick(Sender: TObject);
begin
  Praz_PF.MostraFormPediPrzCab1(0);
end;

procedure TFmPrincipal.AGBPipCadClick(Sender: TObject);
begin
  MostraFormPipCad(0);
end;

procedure TFmPrincipal.AGBPosVdaGerClick(Sender: TObject);
begin
  MyObjects.FormTDICria(TFmPosVdaGer, PageControl1, AdvToolBarPager1);
{
  if DBCheck.CriaFm(TFmPosVdaGer, FmPosVdaGer, afmoNegarComAviso) then
  begin
    FmPosVdaGer.ShowModal;
    FmPosVdaGer.Destroy;
  end;
}
end;

procedure TFmPrincipal.AGBPragas_AClick(Sender: TObject);
begin
  MostraFormPraga_A(0);
end;

procedure TFmPrincipal.AGBPraga_ZClick(Sender: TObject);
begin
  MostraFormPraga_Z();
end;

procedure TFmPrincipal.AGBQrCodePrintClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmQRCodeImp, FmQRCodeImp, afmoNegarComAviso) then
  begin
    FmQRCodeImp.ShowModal;
    FmQRCodeImp.Destroy;
  end;
end;

procedure TFmPrincipal.AGBRMIPCfgCabClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRMIPCfgCad, FmRMIPCfgCad, afmoNegarComAviso) then
  begin
    FmRMIPCfgCad.ShowModal;
    FmRMIPCfgCad.Destroy;
  end;
end;

procedure TFmPrincipal.AGBRMIP_CadClick(Sender: TObject);
begin
  MostraRMIP();
end;

procedure TFmPrincipal.AGBStqBalCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqBalCad(0);
end;

procedure TFmPrincipal.AGBStqCenCabClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqCenCab();
end;

procedure TFmPrincipal.AGBStqCenCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqCenCad(0);
end;

procedure TFmPrincipal.AGBStqManCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqManCad(0);
end;

procedure TFmPrincipal.AGBTabsSPEDEFDClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPED_EFD_Tabelas, FmSPED_EFD_Tabelas, afmoNegarComAviso) then
  begin
    FmSPED_EFD_Tabelas.ShowModal;
    FmSPED_EFD_Tabelas.Destroy;
  end;
end;

procedure TFmPrincipal.AGBValidaXMLClick(Sender: TObject);
begin
  UnNFe_PF.ValidaXML_NFe('');
end;

procedure TFmPrincipal.AGBXML_No_BDClick(Sender: TObject);
begin
  DmNFe_0000.AtualizaXML_No_BD_Tudo(True);
end;

procedure TFmPrincipal.MostraMatriz();
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton50Click(Sender: TObject);
begin
  Grade_Jan.MostraFormStqInnCad(0);
end;

procedure TFmPrincipal.AdvGlowButton51Click(Sender: TObject);
(*
var
  Qry: TmySQLQuery;
  Texto: String;
*)
begin
(*
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT eco.Nome NO_ENTICONTAT, cab.*,  ',
    'fge.Nome NO_FatoGeradr, sta.Nome NO_ESTATUS, ',
    'stc.Nome NO_SiapTerCad, stc.LstCusPrd, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
    'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT, ',
    'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG, ',
    'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR, ',
    'car.Nome NO_CART, ppc.Nome NO_PRZ, ',
    'tg1.Nome NO_ExeTxtCli1, tg2.Nome NO_ExeTxtCli2, ',
    'aec.Nome NO_AgeEqiCab, moc.Nome NO_MobiliCad ',
    'FROM oscab cab ',
    'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
    'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante ',
    'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat ',
    'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
    'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
    'LEFT JOIN carteiras car ON car.Codigo=cab.CartEmis ',
    'LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondicaoPg ',
    'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat ',
    'LEFT JOIN txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1  ',
    'LEFT JOIN txtgeneric tg2 ON tg2.Codigo=cab.ExeTxtCli2  ',
    'LEFT JOIN ageeqicab aec ON aec.Codigo=cab.AgeEqiCab  ',
    'LEFT JOIN mobilicad moc ON moc.Codigo=cab.MobiliCad ',
    'WHERE cab.PosGerou=1 ',
    'ORDER BY cab.Codigo ',
    '']);
    if Qry.RecordCount = 0 then
      Texto := 'N�o h� OSs bloqueadas para gera��o de OSs filhas'
    else
      Texto := 'Num OS - Codigo / Nome Cliente' + sLineBreak;
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Texto := Texto +
      Geral.FFF(Qry.FieldByName('Codigo').AsInteger, 6) + '  -  ' +
      Geral.FFF(Qry.FieldByName('Entidade').AsInteger, 6) + '  -  ' +
      Qry.FieldByName('NO_ENT').AsString + sLineBreak;
      //
      Qry.Next;
    end;
    Geral.MB_Aviso(Texto);
  finally
    Qry.Free;
  end;
*)

  if DBCheck.CriaFm(TFmOSFlhAdm, FmOSFlhAdm, afmoNegarComAviso) then
  begin
    FmOSFlhAdm.ShowModal;
    FmOSFlhAdm.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton52Click(Sender: TObject);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWShowMemo, FmWShowMemo, afmoNegarComAviso) then
    begin
(*
SELECT Nivel1, VetorArq
FROM grag1eqmo
WHERE VetorSvg <> ''
*)
      FmWShowMemo.FTabela  := 'grag1eqmo';
      FmWShowMemo.FFldCodi := 'Nivel1';
      FmWShowMemo.FFldNome := 'VetorArq';
      FmWShowMemo.FFldMemo := 'VetorSVG';
      //
      FmWShowMemo.ReopenTabela();
      //
      FmWShowMemo.ShowModal;
      FmWShowMemo.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.AdvGlowButton55Click(Sender: TObject);
begin
  UBloqGerl_Jan.MostraBloCNAB_Ret;
end;

procedure TFmPrincipal.AdvGlowButton5Click(Sender: TObject);
begin
  MostraFormPraga_Z();
end;

procedure TFmPrincipal.AGBPlanoCtaSaldoClick(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas(0);
end;

procedure TFmPrincipal.AGBBloEnPrClick(Sender: TObject);
begin
  ProtocoUnit.MostraProEnPr(0);
end;

procedure TFmPrincipal.AGBBloProtoGerClick(Sender: TObject);
begin
  ProtocoUnit.MostraProtoGer(True, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPager1, 0, 0);
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvGlowButton7Click(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.AdvGlowButton8Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGSM_Serial, FmGSM_Serial, afmoSoBoss) then
  begin
    FmGSM_Serial.ShowModal;
    FmGSM_Serial.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  MostraFormPraga_A(0);
end;

procedure TFmPrincipal.AGBCarteirasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCarteiras(0);
end;

procedure TFmPrincipal.AdvToolBarButton10Click(Sender: TObject);
begin
  SincronizarTabelasWeb();
end;

procedure TFmPrincipal.AdvToolBarButton2Click(Sender: TObject);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.ATBVerificaNovaVersaoClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvToolBarButton4Click(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.ATBTemAtualizacaoClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte;
  {$ENDIF}
end;

procedure TFmPrincipal.AdvToolBarPager1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
  begin
    if AdvToolBarPager1.Expanded then
    begin
      if AdvToolBarPager1.Height < FAdvToolBarPager_Hei_Max then
      begin
        AdvToolBarPager1.Collaps;
        AdvToolBarPager1.Expand;
        AdvToolBarPager1.Height := FAdvToolBarPager_Hei_Max;
      end;
      if Y > FAdvToolBarPager_Hei_Max - 5 then
        AdvToolBarPager1.Collaps;
    end else
      AdvToolBarPager1.Expand;
  end;
end;

procedure TFmPrincipal.AdvWSuporteClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte;
  {$ENDIF}
end;

procedure TFmPrincipal.AGMBBackupClick(Sender: TObject);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.AGVContratosClick(Sender: TObject);
begin
  MostraFormContratos(0);
  //[TAG_NIV_ACOES_A_SEREM_DESENV]
end;

procedure TFmPrincipal.AGVIGPMClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmIGPM, FmIGPM, afmoNegarComAviso) then
  begin
    FmIGPM.ShowModal;
    FmIGPM.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroEntidades(Entidade: Integer; AbrirEmAba: Boolean;
  EntTipo: TUnEntTipo = uetNenhum);
(*
var
  Form: TForm;
*)
begin
  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, False,
    AbrirEmAba, PageControl1, AdvToolBarPager1, False, EntTipo);
  (*
  if AbrirEmAba then
  begin
    Form := MyObjects.FormTDICria(TFmEntidade2, PageControl1, AdvToolBarPager1, True);

    if Entidade > 0 then
      TFmEntidade2(Form).LocCod(Entidade, Entidade);
  end else
  begin
    DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, False);
  end;
  *)
end;

procedure TFmPrincipal.Cadastros1Click(Sender: TObject);
begin
  MyObjects.FormTDICria(TFmContratos, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmBugstrol_Dmk.Show;
  Enabled := False;
  FmBugstrol_Dmk.Refresh;
  FmBugstrol_Dmk.EdSenha.Text := FmBugstrol_Dmk.EdSenha.Text+'*';
  FmBugstrol_Dmk.EdSenha.Refresh;
  FmBugstrol_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
    (* Se precisar mudar caption dos componentes!
    FDmodCriado := True;
    ReCaptionComponentesDeForm(FmPrincipal);
    AdvToolBarPager1.Visible := True;
    *)
    // Tornar vis�vel
    TimerAlphaBlend.Enabled := True;
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  try
    Application.CreateForm(TDmPediVda, DmPediVda);
  except
    Application.MessageBox(PChar('Imposs�vel criar M�dulo de vendas'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmBugstrol_Dmk.EdSenha.Text := FmBugstrol_Dmk.EdSenha.Text+'*';
  FmBugstrol_Dmk.EdSenha.Refresh;
  FmBugstrol_Dmk.ReloadSkin;
  FmBugstrol_Dmk.EdLogin.Text := '';
  FmBugstrol_Dmk.EdLogin.PasswordChar := 'l';
  FmBugstrol_Dmk.EdSenha.Text := '';
  FmBugstrol_Dmk.EdSenha.Refresh;
  FmBugstrol_Dmk.EdLogin.ReadOnly := False;
  FmBugstrol_Dmk.EdSenha.ReadOnly := False;
  FmBugstrol_Dmk.EdLogin.SetFocus;
  //FmBugstrol_Dmk.ReloadSkin;
  FmBugstrol_Dmk.Refresh;
end;

procedure TFmPrincipal.Timer2Timer(Sender: TObject);
var
  ptCursor: TPoint;
  Fim: Integer;
begin
  // Caso esteja finalizando o aplicativo...
  if ZZTerminate then
    Exit;
  // .. caso nao esteja, continua.
  try
    if Dmod = nil then
      Exit;

    if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
    begin
      GetCursorPos(ptCursor);
      //
      if (ptCursor.X <> FCursorPosX)
      or (ptCursor.Y <> FCursorPosY) then
      begin
        Fim := AdvToolBarPager1.Top + FAdvToolBarPager_Hei_Max +
        FmPrincipal.Top + FmPrincipal.Height - FmPrincipal.ClientHeight + FBorda;
        FCursorPosX := ptCursor.X;
        FCursorPosY := ptCursor.Y;
        //
        if AdvToolBarPager1.Expanded then
        begin
          if FCursorPosY > Fim then
            AdvToolBarPager1.Collaps;
        end;
      end;
    end else
    begin
      Timer2.Enabled := False;
      MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
    end;
  except
    Timer2.Enabled := False;
    // TODO > Implementar erro no idle
    raise Exception.Create('N�o foi poss�vel executar "FmPrincipal.Timer2()"');
  end;
end;


procedure TFmPrincipal.TimerAlphaBlendTimer(Sender: TObject);
begin
  if AlphaBlendValue < 255 then
    AlphaBlendValue := AlphaBlendValue + 1
  else begin
    TimerAlphaBlend.Enabled := False;
    AlphaBlend := False;
  end;
end;

procedure TFmPrincipal.CadastroDeCarteiras(TipoCarteira, ItemCarteira, x: Integer);
begin
  VAR_CARTEIRA := TipoCarteira;
  //
  FinanceiroJan.CadastroDeCarteiras(x);
end;

procedure TFmPrincipal.MostraLogoff;
begin
  FmPrincipal.Enabled := False;
  //
  FmBugstrol_Dmk.Show;
  FmBugstrol_Dmk.EdLogin.Text   := '';
  FmBugstrol_Dmk.EdSenha.Text   := '';
  FmBugstrol_Dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal.MostraOpcoes;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraParamsEmp();
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

procedure TFmPrincipal.MostraRMIP();
begin
  if DBCheck.CriaFm(TFmRMIP_Cab, FmRMIP_Cab, afmoNegarComAviso) then
  begin
    FmRMIP_Cab.ShowModal;
    FmRMIP_Cab.Destroy;
  end;
end;

procedure TFmPrincipal.MostraServicoDesServico(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmDesServico, FmDesServico, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmDesServico.LocCod(Codigo, Codigo);
    FmDesServico.ShowModal;
    FmDesServico.Destroy;
  end;
end;

procedure TFmPrincipal.MotivosdedestivdePIPs1Click(Sender: TObject);
begin
  MostraMotDesativ(0);
end;

procedure TFmPrincipal.Motivospsgarantia1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'motposgar', 60, ncGerlSeq1,
  'Motivos de P�s Garantia',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.NCTCAC1Click(Sender: TObject);
begin
  MostraFormCunsImgNCT(0);
end;

procedure TFmPrincipal.Novajanela1Click(Sender: TObject);
begin
  MostraFormOSProtSai(True, True);
end;

procedure TFmPrincipal.Novos1Click(Sender: TObject);
begin
  DmModOS.VerificaMulServico(GBAvisos1, LaAvisoA1, LaAvisoA2, PB1);
end;

procedure TFmPrincipal.Objetos1Click(Sender: TObject);
begin
  MostraFormObjetos();
end;

procedure TFmPrincipal.OpesGrade1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormOpcoesGrad();
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
end;

procedure TFmPrincipal.EquipamentoMonitorar1Click(Sender: TObject);
begin
  MostraFormGraG1Equi(gbsMonitora, 0);
  //MostraFormGraG1EqMo();
end;

procedure TFmPrincipal.EquipamentosaAplicar1Click(Sender: TObject);
begin
  MostraFormGraG1Equi(gbsAplica, 0);
  //MostraFormGraG1EqAp();
end;

procedure TFmPrincipal.EstatusdeOSs1Click(Sender: TObject);
begin
  MostraFormStatusOSs();
end;

procedure TFmPrincipal.Existente1Click(Sender: TObject);
begin
  MostraFormOSProtSai(True, False);
end;

procedure TFmPrincipal.extosbinrios1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'PrgBinCad', 60, ncGerlSeq1,
  'Textos Bin�rios',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.extosGenricos1Click(Sender: TObject);
begin
  MostraFormTxtGeneric(0);
end;

procedure TFmPrincipal.ExtratosFinanceirosEmpresaUnica(TabLctA: String);
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if not PreparaPesquisaFinanceira(TabLctA) then
    Exit;
  //
  if DBCheck.CriaFm(TFmExtratos2, FmExtratos2, afmoNegarComAviso) then
  begin
    FmExtratos2.ShowModal;
    FmExtratos2.Destroy;
  end;
end;

function TFmPrincipal.AcaoEspecificaDeApp(Servico: String): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; AbrirEmAba: Boolean);
var
  UltimoNovo: Integer;
begin
  UltimoNovo := 0;
  if OSAll_PF.AchouNovosClientes(UltimoNovo) then
  begin
    if UltimoNovo > 0 then
      if Geral.MB_Pergunta('Deseja informar dados de terrenos do cliente n� ' +
      Geral.FF0(UltimoNovo) + '?') = ID_YES
    then
      MostraFormCunsCad(AbrirEmAba, UltimoNovo, 0, True);
  end;
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade1: TStringGrid);
begin
   // Compatibilidade
end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;

procedure TFmPrincipal.Reduzidodeprodutos1Click(Sender: TObject);
var
  GraGruX: Variant;
  //Cod: Integer;
begin
  GraGruX := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, GraGruX, 0, 0,
  '', '', True, 'Reduzido', 'Informe o reduzido do produto: ', 0, GraGruX) then
  begin
    if DBCheck.CriaFm(TFmGraGruReduzido, FmGraGruReduzido, afmoSoAdmin) then
    begin
      FmGraGruReduzido.FGraGruX := GraGruX;
      FmGraGruReduzido.QrGraGruX.Close;
      FmGraGruReduzido.QrGraGruX.Params[0].AsInteger := GraGruX;
      FmGraGruReduzido.QrGraGruX.Open;
      FmGraGruReduzido.ShowModal;
      FmGraGruReduzido.Destroy;
    end;
  end;
end;

(* Cria direto no UnGrade_Tabas
procedure TFmPrincipal.RecriaTiposDeProdutoPadrao();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM prdgruptip ',
    'WHERE Codigo > 0',
    '']);
  if Dmod.QrAux.RecordCount = 0 then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
      'CodUsu', 'Nome', 'NivCad', 'ImpedeCad', 'TipPrd', 'MadeBy', 'Fracio',
      'Gradeado', 'Customizav', 'TitNiv1', 'TitNiv2', 'TitNiv3', 'TitNiv4',
      'TitNiv5', 'LstPrcFisc'], ['Codigo'], [1, 'Produto de aplica��o', 2, 0,
      1, 2, 1, 0, 0, 'Produto', 'Grupo', '', '', '', 4], [1], False);

    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
      'CodUsu', 'Nome', 'NivCad', 'ImpedeCad', 'TipPrd', 'MadeBy', 'Fracio',
      'Gradeado', 'Customizav', 'TitNiv1', 'TitNiv2', 'TitNiv3', 'TitNiv4',
      'TitNiv5', 'LstPrcFisc'], ['Codigo'], [2, 'Equipamentos de aplica��o', 2,
      0, 3, 2, 1, 0, 0, 'Produto', 'Grupo', '', '', '', 4], [2], False);

    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
      'CodUsu', 'Nome', 'NivCad', 'ImpedeCad', 'TipPrd', 'MadeBy', 'Fracio',
      'Gradeado', 'Customizav', 'TitNiv1', 'TitNiv2', 'TitNiv3', 'TitNiv4',
      'TitNiv5', 'LstPrcFisc'], ['Codigo'], [3, 'Produtos de monitoramento', 2,
      0, 1, 2, 1, 0, 0, 'Produto', 'Grupo', '', '', '', 4], [3], False);

    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
      'CodUsu', 'Nome', 'NivCad', 'ImpedeCad', 'TipPrd', 'MadeBy', 'Fracio',
      'Gradeado', 'Customizav', 'TitNiv1', 'TitNiv2', 'TitNiv3', 'TitNiv4',
      'TitNiv5', 'LstPrcFisc'], ['Codigo'], [4, 'Equipamentos de monitoramento',
      2, 0, 3, 2, 1, 0, 0, 'Produto', 'Grupo', '', '', '', 4], [4], False);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB,
      ['UPDATE controle SET PrdGrupTip=4', '']);
  end;
end;
*)

procedure TFmPrincipal.Regeratodo1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DELETE FROM mulservico',
  ''
  ]);
  DmModOS.VerificaMulServico(GBAvisos1, LaAvisoA1, LaAvisoA2, PB1);
end;

procedure TFmPrincipal.Reiniciacomemisses1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  if DBCheck.CriaFm(TFmReIniLctsSoComOpnEmiss, FmReIniLctsSoComOpnEmiss, afmoSoAdmin) then
  begin
    FmReIniLctsSoComOpnEmiss.ShowModal;
    FmReIniLctsSoComOpnEmiss.Destroy;
  end;
end;

procedure TFmPrincipal.Residentes1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'residentes', 60, ncGerlSeq1,
    'Cadastro de Residentes em locais de Aplica��o',
    [], False, Null, [], [], False);
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKey('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.PageControl1Enter(Sender: TObject);
begin
  if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
  begin
    AdvToolBarPager1.Collaps;
  end;
end;

procedure TFmPrincipal.PageControl1MouseEnter(Sender: TObject);
begin
  if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
  begin
    AdvToolBarPager1.Collaps;
  end;
end;

procedure TFmPrincipal.PageControl1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbRight) then
  begin
    //ShowMessage('X=' + IntToStr(X) + ' Y=' + intToStr(Y));
    //ShowMessage('Qtd Compos: ' + IntToStr(PageControl1.ActivePage.ComponentCount));
    if PageControl1.ActivePage.ComponentCount = 0 then
    begin
      if Geral.MB_Pergunta('Deseja excluir a aba "' +
      PageControl1.ActivePage.Caption + '"?') = ID_YES then
        PageControl1.ActivePage.Free;
    end;
  end;
end;

procedure TFmPrincipal.Perfis2Click(Sender: TObject);
begin
  UWUsersJan.MostraWPerfis(0);
end;

procedure TFmPrincipal.Pesquisaemgrade1Click(Sender: TObject);
begin
  MyObjects.FormTDICria(TFmContratPsq2, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  //Apenas compatibilidade usado no Syndi2
end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

procedure TFmPrincipal.sd1FormSkin(Sender: TObject; aName: string;
  var DoSkin: Boolean);
begin
  if aName = VAR_FORMTDI_NAME then
    DoSkin := False;
end;

procedure TFmPrincipal.Abrangncias1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'abrangicie', 60, ncGerlSeq1,
    'Cadastro de Abrang�ncias (superf�cies de aplica��o)',
    [], False, Null, [], [], False);
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.Arenovar1Click(Sender: TObject);
begin
  ContratUnit.ContratosAVencer(DModG.ObtemAgora(), True);
end;

procedure TFmPrincipal.ATBBATBAutoExpClick(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal.ATBBFavoritosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    DModG.CriaFavoritos(AdvToolBarPager1, LaAviso2, LaAviso1, AGBOS, FmPrincipal);
  end;
end;

procedure TFmPrincipal.ATBBLastWorkClick(Sender: TObject);
(*
const
  CasasValI = 2;
  CasasValF = 2;
  CasasPerc = 6;
var
  ValorValI, ValorValF, ValorPerc: Double;
begin
  ValorValI := 400;
  ValorValF := 0;
  ValorPerc := 5;
  if MyVCLRef.CalculaPercentual(TFmCalculaPercentual, FmCalculaPercentual,
  fcpercDesconto,
  CasasValI, CasasValF, CasasPerc,
  ValorValI, ValorValF, ValorPerc) then
    ShowMessage(FloatToStr(ValorPerc));
*)
(*
var
  Texto: Variant;
*)
begin
  MostraCRO();
{
  OSApp_PF.OSRapida();
}
{
SELECT AgeEqiCab, SUM(
((UNIX_TIMESTAMP(DtaExeFim) -
UNIX_TIMESTAMP(DtaExeIni)) /60/60) -
(
(TO_DAYS(DtaExeFim) - TO_DAYS(DtaExeIni)) * 16
)) Horas, aec.Nome NO_EQUIPE

FROM oscab cab
LEFT JOIN ageeqicab aec ON aec.Codigo=cab.AgeEqiCab
WHERE (DtaExeIni > "1900-01-01"
OR DtaExeFim > "1900-01-01")

AND DtaExeFim BETWEEN "2014-10-1" AND "2014-10-31"

GROUP BY AgeEqiCab
}
(*
  OSApp_PF.MostraFormOSCab2(mfoisCodigo, 4293);
*)
(*
  MostraFormCunsCad(True, 285);
*)
(*
var
  Host: String;
begin
  Host := '127.0.0.1';
  if InputQuery('Host', 'Defina o Host', Host) then
  begin
    if MyObjects.Ping(Host) then
      Geral.MB_Info('Ping OK: ' + Host)
    else
      Geral.MB_Aviso('Ping falhou: ' + Host);
  end;
*)
(*
end;
var
  Form: TForm;
begin
  Form := MostraFormCunsCad(True, 1433);
  TFmCunsCad(Form).PGDadosLocaisAplic.ActivePageIndex := 8;
  TFmCunsCad(Form).DBGSiapImaSVGDblClick(TFmCunsCad(Form).DBGSiapImaSVG);
*)
(*

  Geral.MB_Aviso(
    'Vermelho = ' + IntToStr($00513FE6) + slineBreak + //  5324774
    'Laranja  = ' + IntToStr($002267F2) + slineBreak + //  2254834
    'Amarelo  = ' + IntToStr($0000BAFF) + slineBreak + //    47871
    'Azul     = ' + IntToStr($00C68645) + slineBreak + // 13010501
    'Roxo     = ' + IntToStr($0075479D) + slineBreak + //  7686045
    'Verde    = ' + IntToStr($0040C086) + slineBreak + //  4243590
  '');
*)
(*
  //FmPrincipal.ExtratosFinanceirosEmpresaUnica('lct0001a');
  if DBCheck.CriaFm(TFmReceDesp2, FmReceDesp2, afmoLiberado) then
  begin
    FmReceDesp2.EdEmpresa.ValueVariant := 1;
    FmReceDesp2.CBEmpresa.KeyValue     := 1;
    FmReceDesp2.TPDataIni01.Date := EncodeDate(2014, 01, 01);
    FmReceDesp2.TPDataFim01.Date := EncodeDate(2014, 01, 01);
    //FmReceDesp2.TPDataIni02.Date := 0;
    //FmReceDesp2.TPDataFim02.Date := 0;
    //
    FmReceDesp2.RGNivel.ItemIndex := 0;//5;
    FmReceDesp2.EdNivelSel1.ValueVariant := 0;//4;
    FmReceDesp2.CBNivelSel1.KeyValue     := 0;//4;
    FmReceDesp2.EdNivelSel2.ValueVariant := 0;//3;
    FmReceDesp2.CBNivelSel2.KeyValue     := 0;//3;
    //
    FmReceDesp2.CkNaoAgruparNada.Checked := True;
    FmReceDesp2.RGPagRec.ItemIndex := 1;
    FmReceDesp2.CkCentroRes.Checked := True;
    FmReceDesp2.CGCentroRes.Value := 2;
    //
    FmReceDesp2.ShowModal;
    FmReceDesp2.Destroy;
  end;
*)
(*
  if Geral.GET_Compo_Val(Self, Memo1, 'Memo1', 'Lines', TMemo, Texto) then
  Geral.MB_Info(String(Texto));
*)
end;

procedure TFmPrincipal.ATBBLehCodBarraClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCodBarraRead, FmCodBarraRead, afmoLiberado) then
  begin
    FmCodBarraRead.ShowModal;
    FmCodBarraRead.Destroy;
  end;
end;

procedure TFmPrincipal.ATBBMenuGeralClick(Sender: TObject);
begin
  MyObjects.MostraPopupGeral();
end;

procedure TFmPrincipal.ATBImpClick(Sender: TObject);
begin
  MostraRMIP();
end;

procedure TFmPrincipal.ATBOSCabClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOSPs2, FmOSPs2, afmoNegarComAviso) then
  begin
    FmOSPs2.ShowModal;
    FmOSPs2.Destroy;
  end;
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.MarcasdeEmpresas1Click(Sender: TObject);
begin
  MostraFormGraFabCad(0);
end;

procedure TFmPrincipal.Materias1Click(Sender: TObject);
begin
  MostraFormCxaMaters();
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.MostraMobiliCad(Codigo: integer);
begin
  if DBCheck.CriaFm(TFmMobiliCad, FmMobiliCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmMobiliCad.LocCod(Codigo, Codigo);
    FmMobiliCad.ShowModal;
    FmMobiliCad.Destroy;
  end;
end;

procedure TFmPrincipal.MostraMotDesativ(Codigo: Integer);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'motdesativ', 60, ncGerlSeq1,
    'Motivos de Desativa��o / Inutiliza��o de PIPs', [], False, Null, [], [],
    False, Codigo);
end;

procedure TFmPrincipal.Mobilidade2Click(Sender: TObject);
begin
  MostraMobiliCad(0);
end;

procedure TFmPrincipal.MostraAgeEqiCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmAgeEqiCab, FmAgeEqiCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmAgeEqiCab.LocCod(Codigo, Codigo);
    FmAgeEqiCab.ShowModal;
    FmAgeEqiCab.Destroy;
  end;
end;

procedure TFmPrincipal.MostraAnotacoes;
begin
  if DBCheck.CriaFm(TFmAnotacoes, FmAnotacoes, afmoNegarComAviso) then
  begin
    FmAnotacoes.ShowModal;
    FmAnotacoes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraBackup3;
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.Limpar1Click(Sender: TObject);
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso,
    PageControl1, True);
end;

procedure TFmPrincipal.Listadascontas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasPlanoLista, FmContasPlanoLista, afmoNegarComAviso) then
  begin
    FmContasPlanoLista.ShowModal;
    FmContasPlanoLista.Destroy;
  end;
end;

procedure TFmPrincipal.Listagem2Click(Sender: TObject);
begin
  CadastroDeContasSdoLista();
end;

procedure TFmPrincipal.ListasdePerguntas1Click(Sender: TObject);
begin
  MostraFormPrgLstCab(0, 0);
end;

procedure TFmPrincipal.Simples2Click(Sender: TObject);
begin
  CadastroDeContasSdoSimples(0, 0);
end;

procedure TFmPrincipal.SincronizarTabelasWeb;
begin
  Geral.MB_Aviso('Tempor�riamente indispon�vel!');
  (*
  DmkWeb_Jan.MostraWSincro(CO_DMKID_APP, CO_Versao,
    DModG.QrMasterHabilModulos.Value, DmodG.QrOpcoesGerl, Dmod.MyDBn,
    Dmod.MyDB, DModG.QrControle.FieldByName('DtaSincro').AsDateTime, TMeuDB,
    VAR_AllID_DB_NOME, PB1, [''], VAR_VERIFI_DB_CANCEL);
  *)
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
{var
  Indice : Integer;}
begin
  {if Index >= 0 then
    Indice := Index
  else
    Indice := Geral.ReadAppKey('MenuStyle', Application.Title,
      ktInteger, 1, HKEY_LOCAL_MACHINE);}
  case Index of
    0: AdvToolBarOfficeStyler1.Style := bsOffice2003Blue;
    1: AdvToolBarOfficeStyler1.Style := bsOffice2003Classic;
    2: AdvToolBarOfficeStyler1.Style := bsOffice2003Olive;
    3: AdvToolBarOfficeStyler1.Style := bsOffice2003Silver;
    4: AdvToolBarOfficeStyler1.Style := bsOffice2007Luna;
    5: AdvToolBarOfficeStyler1.Style := bsOffice2007Obsidian;
    6: AdvToolBarOfficeStyler1.Style := bsOffice2007Silver;
    7: AdvToolBarOfficeStyler1.Style := bsOfficeXP;
    8: AdvToolBarOfficeStyler1.Style := bsWhidbeyStyle;
    9: AdvToolBarOfficeStyler1.Style := bsWindowsXP;
  end;
end;

procedure TFmPrincipal.Statusdecompromissos1Click(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgendaStatus();
end;

procedure TFmPrincipal.Statusdefotosrelativas1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadItemGrupo(Dmod.MyDB, 'CunsImgSit',
    AppListas.ListaNiveisSituacoesFotosClientes(), ncGerlSeq1);
end;

procedure TFmPrincipal.StatusdeProvidncias1Click(Sender: TObject);
begin
  MostraFormStatusProvidencias();
end;

procedure TFmPrincipal.Transfernciaentrecontas1Click(Sender: TObject);
begin
  TransfereEntreContas(0, 0, 0, Date, 0);
end;

procedure TFmPrincipal.Cadastrodeperguntas1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadRegistroSimples(Dmod.MyDB, DmModOS.TbPrgCadPrg,
    DmModOS.DsPrgCadPrg, ncGerlSeq1, 'Cadastro de Perguntas');
end;

procedure TFmPrincipal.ipodeAplicaes1Click(Sender: TObject);
begin
  MostraFormTipoAplica(0);
end;

procedure TFmPrincipal.iposdeConstruo1Click(Sender: TObject);
begin
  MostraFormTpConstrus();
end;

procedure TFmPrincipal.iposdedependncias1Click(Sender: TObject);
begin
  MostraFormDependType();
end;

procedure TFmPrincipal.Janelas1Click(Sender: TObject);
begin
  UWUsersJan.MostraWPerfJan(True);
end;

procedure TFmPrincipal.CadastroDeContasSdoLista();
begin
{ N�o usar! Ver o que usa no novo!
  if DBCheck.CriaFm(TFmContasSdoAll, FmContasSdoAll, afmoNegarComAviso) then
  begin
    FmContasSdoAll.ShowModal;
    FmContasSdoAll.Destroy;
  end;
}
end;

function TFmPrincipal.CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
begin
{ N�o usar! Ver o que usa no novo
  if DBCheck.CriaFm(TFmContasSdoUni, FmContasSdoUni, afmoNegarComAviso) then
  begin
    if Entidade <> 0 then
    begin
      FmContasSdoUni.EdEntidade.Text := IntToStr(Entidade);
      FmContasSdoUni.CBEntidade.KeyValue := Entidade;
    end;
    if Conta <> 0 then
    begin
      FmContasSdoUni.EdCodigo.Text := IntToStr(Conta);
      FmContasSdoUni.CBCodigo.KeyValue := Conta;
    end;
    //
    FmContasSdoUni.ShowModal;
    //
    Result := FmContasSdoUni.FExecutou;
    //
    FmContasSdoUni.Destroy;
  end else
    Result := False;
}
  Result := True;
end;

procedure TFmPrincipal.TransfereEntreContas(Entidade, CtaOrig, CtaDest: Integer;
  Data: TDateTime; Valor: Double);
begin
{ N�o usar este! Ver como � o novo!
  if DBCheck.CriaFm(TFmContasSdoTrf, FmContasSdoTrf, afmoNegarComAviso) then
  begin
    if (Entidade > 0)
    and (FmContasSdoTrf.QrEntidades.Locate('Codigo', Entidade, [])) then
    begin
      FmContasSdoTrf.EdEntidade.Text := IntToStr(Entidade);
      FmContasSdoTrf.CBEntidade.KeyValue := Entidade;
    end;
    if CtaOrig > 0 then
    begin
      FmContasSdoTrf.EdCtaOrig.Text := IntToStr(CtaOrig);
      FmContasSdoTrf.CBCtaOrig.KeyValue := CtaOrig;
    end;
    if CtaDest > 0 then
    begin
      FmContasSdoTrf.EdCtaDest.Text := IntToStr(CtaDest);
      FmContasSdoTrf.CBCtaDest.KeyValue := CtaDest;
    end;
    if Data > 0 then FmContasSdoTrf.TPDataT.Date := Data;
    //
    FmContasSdoTrf.ShowModal;
    FmContasSdoTrf.Destroy;
  end;
}
end;

procedure TFmPrincipal.MostraCarteiras(Carteira: Integer);
begin
  // n�o � necess�rio
end;

procedure TFmPrincipal.MostraCRO();
begin
  if DBCheck.CriaFm(TFmModCRO, FmModCRO, afmoNegarComAviso) then
  begin
    FmModCRO.ShowModal;
    FmModCRO.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormFinalidads();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'finalidads', 60, ncGerlSeq1,
  'Cadastro de Finalidades dos Locais de Aplica��o',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.MostraFormFormulas(QrOSFrmRec: TmySQLQuery; Codigo,
  Conta: Integer);
begin
  if DBCheck.CriaFm(TFmFormulas, FmFormulas, afmoNegarComAviso) then
  begin
    FmFormulas.FQrOSFrmRec := QrOSFrmRec;
    FmFormulas.FConta      := Conta;
    //
    if (Conta = 0) and (Codigo <> 0) then
      FmFormulas.LocCod(Codigo, Codigo);
    //
    FmFormulas.ShowModal;
    FmFormulas.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormGraFabCad(Controle: Integer);
begin
  if DBCheck.CriaFm(TFmGraFabCad, FmGraFabCad, afmoNegarComAviso) then
  begin
    if Controle <> 0 then
      FmGraFabCad.LocCodByControle(Controle);
    FmGraFabCad.ShowModal;
    FmGraFabCad.Destroy;
  end;
end;

(*
procedure TFmPrincipal.MostraFormMobiliUni();
begin
  if DBCheck.CriaFm(TFmMobiliUni, FmMobiliUni, afmoNegarComAviso) then
  begin
    FmMobiliUni.ShowModal;
    FmMobiliUni.Destroy;
  end;
end;
*)

procedure TFmPrincipal.MostraFormContratos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmContratos, FmContratos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmContratos.LocCod(Codigo, Codigo);
    FmContratos.ShowModal;
    FmContratos.Destroy;
  end;
end;

function TFmPrincipal.MostraFormCunsCad(Tabed: Boolean; Codigo,
  SiapTerCad: Integer; Unique: Boolean = False; Aba: Integer = 0;
  AbreSiapTerFlh: Boolean = False; AbreCroqui: Boolean = False): TForm;
begin
  Result := AgendaGerApp.MostraFormCunsCad(Tabed, Codigo, SiapTerCad, Unique,
            Aba, AbreSiapTerFlh, AbreCroqui);
end;

procedure TFmPrincipal.MostraFormCunsImgNCT(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCunsImgNCT, FmCunsImgNCT, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCunsImgNCT.LocCod(Codigo, Codigo);
    FmCunsImgNCT.ShowModal;
    FmCunsImgNCT.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormCxaFormas();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'cxaformas', 60, ncGerlSeq1,
  'Formatos de Caixa D`�gua',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.MostraFormCxaMaters();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'cxamaters', 60, ncGerlSeq1,
  'Material de Caixa D`�gua',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.MostraFormDependenci();
begin
  if DBCheck.CriaFm(TFmDependenci, FmDependenci, afmoNegarComAviso) then
  begin
    FmDependenci.ShowModal;
    FmDependenci.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormDependType;
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'dependtype', 60, ncGerlSeq1,
  'Tipos de depend�ncias',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.MostraFormDescanso;
begin
  MyObjects.FormTDICria(TFmDescanso, PageControl1, AdvToolBarPager1, False, True, afmoLiberado);
end;

procedure TFmPrincipal.MostraFormDiarioGer2(Assunto, Empresa, Entidade,
  Terceiro, Interlocutor, Acao, UserGenero, Depto: Integer;
  TemUserGenero, SohInterloctrRelacionado: Boolean; DataIni, DataFim: TDateTime;
  ParteTexto: String; ExecutaPesquisa: Boolean);
begin
  if DBCheck.CriaFm(TFmDiarioGer2, FmDiarioGer2, afmoNegarComAviso) then
  begin
    FmDiarioGer2.EdDiarioAss.ValueVariant  := Assunto;
    FmDiarioGer2.CBDiarioAss.KeyValue      := Assunto;
    FmDiarioGer2.EdCliInt.ValueVariant     := Empresa;
    FmDiarioGer2.CBCliInt.KeyValue         := Empresa;
    FmDiarioGer2.EdEntidade.ValueVariant   := Entidade;
    FmDiarioGer2.CBEntidade.KeyValue       := Entidade;
    FmDiarioGer2.EdTerceiro01.ValueVariant := Terceiro;
    FmDiarioGer2.CBTerceiro01.KeyValue     := Terceiro;
    FmDiarioGer2.EdInterloctr.ValueVariant := Interlocutor;
    FmDiarioGer2.CBInterloctr.KeyValue     := Interlocutor;
    FmDiarioGer2.CkUserGenero.Checked      := TemUserGenero;
    FmDiarioGer2.RGUserGenero.ItemIndex    := UserGenero;
    FmDiarioGer2.EdDeptoCB.ValueVariant    := Depto;
    FmDiarioGer2.CBDepto.KeyValue          := Depto;
    FmDiarioGer2.CkInterloctr.Checked      := SohInterloctrRelacionado;
    FmDiarioGer2.EdNome.Text               := ParteTexto;
    if DataIni > 2 then
      FmDiarioGer2.TPDataIni.Date          := DataIni;
    if DataFim > 2 then
      FmDiarioGer2.TPDataFim.Date          := DataFim;
    //
    if ExecutaPesquisa then
       FmDiarioGer2.ReopenDiario(0);
    FmDiarioGer2.ShowModal;
    FmDiarioGer2.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormStatusOSs();
begin
  if DBCheck.CriaFm(TFmCadAnyStatus, FmCadAnyStatus, afmoNegarComAviso,
  'OSS-STATU-001 :: Status de OSs') then
  begin
    FmCadAnyStatus.TbCad.TableName := 'estatusoss';
    FmCadAnyStatus.ShowModal;
    FmCadAnyStatus.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormStatusProvidencias();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'OSPrvSta', 60, ncGerlSeq1,
  'Status de Provid�ncias',
  [], True, 2000, [], [], False);
end;

procedure TFmPrincipal.MostraFormObjetos();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'objetos', 60, ncGerlSeq1,
  'Cadastro de Objetos de Aplica��o',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.MostraFormOSProtSai(Tabed, NewSession: Boolean);
const
  Base = '_ProtoAddIts';
const
  Aviso   = '...';
  Caption = 'Sele��o de data/hora';
  Prompt  = 'Informe a data / hora';
  Campo   = 'Descricao';
var
  Form: TForm;
  //Existe: Boolean;
  Sessao: TDateTime;
  Forms: TArrForms;
  I: Integer;
  Lista: String;
  //Itens: array of TDateTime;
  Qry: TmySQLQuery;
  Res: Variant;
  Data: String;
begin
  Form := nil;
  if not USQLDB.TabelaExiste(Base, DModG.MyPID_DB) then
    UnCreateBugs.RecriaTempTableNovo(ntrtt_ProtoAddIts, DModG.QrUpdPID1,
      False);
  //
  if NewSession = False then
  begin
    Lista := '';
    SetLength(Forms, 0);
    Forms := MyObjects.LocalizaFormsIguais(TFmOSProtSai);
    for I := 0 to Length(Forms) - 1 do
      Lista := Lista + ', "' +
        Geral.FDT(TFmOSProtSai(Forms[I]).FSessionDT, 109) + '"';
    if Lista <> '' then
      Lista := 'WHERE Session NOT IN (' + Copy(Lista, 3) + ') ';
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
      'SELECT DISTINCT Session ',
      'FROM ' + Base,
      Lista,
      '']);
      if Qry.RecordCount > 0 then
      begin
        Res := DBCheck.EscolheTextoUnico(Aviso, Caption, Prompt, nil, nil, Campo, Null, [
        'SELECT DISTINCT DATE_FORMAT(Session, "%d/%m/%Y %H:%i:%S") ' + Campo,
        'FROM _protoaddits',
        Lista,
        'ORDER BY Session',
        ''], DModG.MyPID_DB, False, 'CodTxt', 'Nome');
        if Res <> Null then
        begin
          Data := String(Res);
          Sessao := Geral.ValidaDataBR(Copy(Data, 1, 10), False, False) +
                    StrToTime(Copy(Data, 12));

        end else
          Exit;
      end else
      begin
        Geral.MB_Aviso('N�o existe sele��o iniciada ou todas est�o reiniciadas!');
        Exit;
      end;
    finally
      Qry.Free;
    end;
  end else
    Sessao := DModG.ObtemAgora();
  if Tabed then
  begin
    Form := MyObjects.FormTDICria(TFmOSProtSai, PageControl1, AdvToolBarPager1);
    TFmOSProtSai(Form).FSessionDT  := Sessao;
    TFmOSProtSai(Form).FProtAddIts := Base;
    //
    TFmOSProtSai(Form).ReopenProtAddIts();
  end else
  begin
    if DBCheck.CriaFm(TFmOSProtSai, FmOSProtSai, afmoNegarComAviso) then
    begin
      TFmOSProtSai(Form).FSessionDT := Sessao;
      FmOSProtSai.FProtAddIts := Base;
      //
      FmOSProtSai.ReopenProtAddIts();
      FmOSProtSai.ShowModal;
      FmOSProtSai.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.MostraFormPipCad(Codigo: Integer; Entidade: Integer = 0);
begin
  if DBCheck.CriaFm(TFmPipCad, FmPipCad, afmoNegarComAviso) then
  begin
    FmPipCad.FCodigo   := Codigo;
    FmPipCad.FEntidade := Entidade;
    FmPipCad.LocCod(Codigo, Codigo);
    FmPipCad.ShowModal;
    FmPipCad.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormPipRapido(Codigo: Integer; SQLType: TSQLType;
  Entidade, Equipamento: Integer; PermiteDesativInutili: Boolean);
var
  //Qry: TmySQLQuery;
  PrgLstCab: Integer;
begin
  if DBCheck.CriaFm(TFmPipRapido, FmPipRapido, afmoNegarComAviso) then
  begin
    FmPipRapido.ImgTipo.SQLType := SQLType;
    FmPipRapido.FEntidade       := Entidade;

    FmPipRapido.ReopenSiapImaDep(Entidade);

    if SQLType = stIns then
    begin
      FmPipRapido.EdEquipamento.ValueVariant := Equipamento;
      FmPipRapido.CBEquipamento.KeyValue     := Equipamento;
      //
      PrgLstCab := OSAll_PF.ObtemListaDePergunta(Equipamento);
      //
      FmPipRapido.EdPrgLstCab.ValueVariant := PrgLstCab;
      FmPipRapido.CBPrgLstCab.KeyValue     := PrgLstCab;
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrPIPCad, Dmod.MyDB, [
        'SELECT * ',
        'FROM pipcad ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      //
      FmPipRapido.EdCodigo.ValueVariant      :=  DmModOS.QrPipCadCodigo.Value;
      FmPipRapido.EdNome.ValueVariant        :=  DmModOS.QrPipCadNome.Value;
      FmPipRapido.EdEquipamento.ValueVariant :=  DmModOS.QrPipCadEquipamento.Value;
      FmPipRapido.CBEquipamento.KeyValue     :=  DmModOS.QrPipCadEquipamento.Value;
      //FmPipRapido.EdOSMonCab.ValueVariant    :=  DmModOS.QrPipCadOSMonCab.Value;
      FmPipRapido.TPDtaAquis.Date            :=  DmModOS.QrPipCadDtaAquis.Value;
      FmPipRapido.EdPrgLstCab.ValueVariant   :=  DmModOS.QrPipCadPrgLstCab.Value;
      FmPipRapido.CBPrgLstCab.KeyValue       :=  DmModOS.QrPipCadPrgLstCab.Value;
      FmPipRapido.EdDependenci.ValueVariant  :=  DmModOS.QrPipCadDependenci.Value;
      FmPipRapido.CBDependenci.KeyValue      :=  DmModOS.QrPipCadDependenci.Value;

      if PermiteDesativInutili then
      begin
        FmPipRapido.EdMotDesativ.ValueVariant  :=  DmModOS.QrPipCadMotDesativ.Value;
        FmPipRapido.CBMotDesativ.KeyValue      :=  DmModOS.QrPipCadMotDesativ.Value;
        FmPipRapido.TPDtaDesativ.Date          :=  DmModOS.QrPipCadDtaDesativ.Value;
        //
        FmPipRapido.EdMotInutili.ValueVariant  :=  DmModOS.QrPipCadMotInutili.Value;
        FmPipRapido.CBMotInutili.KeyValue      :=  DmModOS.QrPipCadMotInutili.Value;
        FmPipRapido.TPDtaInutili.Date          :=  DmModOS.QrPipCadDtaInutili.Value;
        //
        FmPipRapido.Pndesativa.Visible         := True;
      end;
    end;
    //
    FmPipRapido.ShowModal;
    FmPipRapido.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormPipVarios(SQLType: TSQLType; Equipamento: Integer);
var
  PrgLstCab: Integer;
begin
  if DBCheck.CriaFm(TFmPipVarios, FmPipVarios, afmoNegarComAviso) then
  begin
    FmPipVarios.ImgTipo.SQLType            := SQLType;
    FmPipVarios.EdEquipamento.ValueVariant := Equipamento;
    FmPipVarios.CBEquipamento.KeyValue     := Equipamento;
    //
    PrgLstCab := OSAll_PF.ObtemListaDePergunta(Equipamento);
    //
    FmPipVarios.EdPrgLstCab.ValueVariant := PrgLstCab;
    FmPipVarios.CBPrgLstCab.KeyValue     := PrgLstCab;
    //
    FmPipVarios.ShowModal;
    FmPipVarios.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormPraga_A(Codigo: Integer);
begin
  UnCfgCadLista.MostraCadItemGrupo(Dmod.MyDB, 'praga_a',
    AppListas.ListaNiveisPragas(), ncGerlSeq1, Codigo);
end;

procedure TFmPrincipal.MostraFormPraga_Z;
begin
  if DBCheck.CriaFm(TFmPraga_Z, FmPraga_Z, afmoNegarComAviso) then
  begin
    FmPraga_Z.ShowModal;
    FmPraga_Z.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormPreEmail(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPreEmail.LocCod(Codigo, Codigo);
    //
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormPrgLstCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmPrgLstCab, FmPrgLstCab, afmoNegarComAviso) then
  begin
    if Controle <> 0 then
      FmPrgLstCab.LocCodByControle(Controle)
    else
    if Codigo <> 0 then
      FmPrgLstCab.LocCod(Codigo, Codigo);
    FmPrgLstCab.ShowModal;
    FmPrgLstCab.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormTipoAplica(Codigo: Integer);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'tipoaplica', 60, ncGerlSeq1,
    'Cadastro de Tipos de Aplica��es',
    [], False, Null, [], [], False, Codigo);
end;

procedure TFmPrincipal.MostraFormTpConstrus();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'tpconstrus', 60, ncGerlSeq1,
    'Cadastro de Tipos de Constru��o',
    [], False, Null, [], [], False);
end;

procedure TFmPrincipal.MostraFormTxtGeneric(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTxtGeneric, FmTxtGeneric, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmTxtGeneric.LocCod(Codigo, Codigo);
    FmTxtGeneric.ShowModal;
    FmTxtGeneric.Destroy;
  end;
end;

{
procedure TFmPrincipal.MostraFormGraG1EqAp();
begin
  if DBCheck.CriaFm(TFmGraG1EqAp, FmGraG1EqAp, afmoNegarComAviso) then
  begin
    FmGraG1EqAp.FiltraItensAExibir(gbsAplica);
    FmGraG1EqAp.ShowModal;
    FmGraG1EqAp.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormGraG1EqMo();
begin
  if DBCheck.CriaFm(TFmGraG1EqAp, FmGraG1EqAp, afmoNegarComAviso) then
  begin
    FmGraG1EqAp.FiltraItensAExibir(gbsMonitora);
    FmGraG1EqAp.ShowModal;
    FmGraG1EqAp.Destroy;
  end;
end;
}

procedure TFmPrincipal.MostraFormGraG1Equi(Servico: TGraBugsServi; Nivel1: Integer);
begin
  if DBCheck.CriaFm(TFmGraG1, FmGraG1, afmoNegarComAviso) then
  begin
    case Servico of
      gbsAplica:
      begin
        FmGraG1.EdGraTabApp.ValueVariant := CO_COD_GraTabApp_GraG1EqAp;
        FmGraG1.CBGraTabApp.KeyValue     := CO_COD_GraTabApp_GraG1EqAp;
      end;
      gbsMonitora:
      begin
        FmGraG1.EdGraTabApp.ValueVariant := CO_COD_GraTabApp_GraG1EqMo;
        FmGraG1.CBGraTabApp.KeyValue     := CO_COD_GraTabApp_GraG1EqMo;
      end;
    end;
    if Nivel1 <> 0 then
      FmGraG1.ReopenGraGru1(Nivel1);
    FmGraG1.ShowModal;
    FmGraG1.Destroy;
  end;
end;

{
procedure TFmPrincipal.MostraFormGraG1PrAp();
begin
  if DBCheck.CriaFm(TFmGraG1PrAp, FmGraG1PrAp, afmoNegarComAviso) then
  begin
    FmGraG1PrAp.FiltraItensAExibir(gbsAplica);
    FmGraG1PrAp.ShowModal;
    FmGraG1PrAp.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormGraG1PrMo();
begin
  if DBCheck.CriaFm(TFmGraG1PrAp, FmGraG1PrAp, afmoNegarComAviso) then
  begin
    FmGraG1PrAp.FiltraItensAExibir(gbsMonitora);
    FmGraG1PrAp.ShowModal;
    FmGraG1PrAp.Destroy;
  end;
end;
}

procedure TFmPrincipal.MostraFormGraG1Prod(Servico: TGraBugsServi; Nivel1: Integer);
begin
  if DBCheck.CriaFm(TFmGraG1, FmGraG1, afmoNegarComAviso) then
  begin
    case Servico of
      gbsAplica:
      begin
        FmGraG1.EdGraTabApp.ValueVariant := CO_COD_GraTabApp_GraG1PrAp;
        FmGraG1.CBGraTabApp.KeyValue     := CO_COD_GraTabApp_GraG1PrAp;
      end;
      gbsMonitora:
      begin
        FmGraG1.EdGraTabApp.ValueVariant := CO_COD_GraTabApp_GraG1PrMo;
        FmGraG1.CBGraTabApp.KeyValue     := CO_COD_GraTabApp_GraG1PrMo;
      end;
    end;
    if Nivel1 <> 0 then
      FmGraG1.ReopenGraGru1(Nivel1);
    FmGraG1.ShowModal;
    FmGraG1.Destroy;
  end;
end;

procedure TFmPrincipal.DeclaraodeServio1Click(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;
  Valor         = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
var
  Prestador: Integer;
  SerieNF: String;
  NumNF: Integer;
  //
  Form: TForm;
begin
  Prestador := DmodG.QrFiliLogFilial.Value;
  Form := MyObjects.FormTDICria(TFmNFSe_Edit_0201, FmPrincipal.PageControl1, AdvToolBarPager1);
  if Form <> nil then
  begin
    UnNFSe_PF_0201.MostraFormNFSe(SQLType,
    Prestador, Tomador, Intermediario, MeuServico, ItemListSrv,
    Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico, nil, Valor,
    SerieNF, NumNF, Form);
  end;
end;

procedure TFmPrincipal.DefineVarsCliInt(Empresa: Integer);
begin
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := Empresa;
  DmodG.QrCliIntUni.Open;
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  (*
  � configurado na hora de fazer o login
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  *)
  //
{:::
  DmodFin.QrCarts.Close;
  DmodFin.QrLctos.Close;
}
end;

procedure TFmPrincipal.DefParams;
var
  Carteira: Integer;
begin
  if QrCarteiras.State <> dsBrowse then Carteira := 0 else Carteira := QrCarteirasCodigo.Value;
  ReabreCarteiras(FEntInt, Carteira);
end;

procedure TFmPrincipal.Dependncias2Click(Sender: TObject);
begin
  MostraFormDependenci();
end;

procedure TFmPrincipal.ReabreCarteiras(Cliente, LocCart: Integer);
begin
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := Cliente;
  QrCarteiras.Open;
  //
  QrCarteiras.Locate('Codigo', LocCart, []);
  //
  QrCartSum.Close;
  QrCartSum.Params[0].AsInteger := Cliente;
  QrCartSum.Open;
  //
end;

procedure TFmPrincipal.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  if not QrCarteiras.Locate('Codigo', Codigo, [])
  then   QrCarteiras.Locate('Codigo', Atual, []);
end;

procedure TFmPrincipal.Gerenciafinancas1Click(Sender: TObject);
begin
  GerenciaFinancas;
end;

procedure TFmPrincipal.GerenciaNFSe1Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.GradeTamanhos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraTamCad, FmGraTamCad, afmoNegarComAviso) then
  begin
    FmGraTamCad.ShowModal;
    FmGraTamCad.Destroy;
  end;
end;

procedure TFmPrincipal.Gruposdepragas1Click(Sender: TObject);
begin
  MostraFormPraga_A(0);
end;

procedure TFmPrincipal.GruposQumicos1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'G1PriAtiG', 60, ncGerlSeq1,
    'Cadastro de Grupos Qu�micos', [], False, Null, [], [], False);
end;

procedure TFmPrincipal.HowFound1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'howfounded', 60, ncGerlSeq1,
    'Cadastro de Tipos de Marketing', [], False, Null, [], [], False);
end;

procedure TFmPrincipal.Gerencia1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'mulservico', 4096, ncGerlSeq1,
    'Cadastro de Conjuntos de Servi�os', [], False, 0, [], [], True);
end;

procedure TFmPrincipal.GerenciaContatos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContatosEnt, FmContatosEnt, afmoNegarComAviso) then
  begin
    FmContatosEnt.ShowModal;
    FmContatosEnt.Destroy;
  end;
end;

procedure TFmPrincipal.GerenciaFinancas;
begin
  DmLct2.GerenciaEmpresa(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if DModG <> nil then
    begin
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando Module Geral');
      DModG.MyPID_DB_Cria();
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando favoritos');
      DModG.CriaFavoritos(AdvToolBarPager1, LaAviso2, LaAviso1, AGBOS, FmPrincipal);
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando Module Anterior');
      DmABD_Mod.MyABD_Cria();
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Atualizando atrelamentos de contatos');
      DModG.AtualizaEntiConEnt();
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Setando ping ao servidor');
      //TimerPingServer.Enabled := VAR_SERVIDOR = 2; 2017-05-24 => Foi criada um fun��o que executa no OnIdle para substituir
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando feriados futuros');
      UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados);
      //
      //  Descanso
      MostraFormDescanso();
      //
      // Deve ser depois da paleta de cores! > Dmod.PoeEmMemoryCoresStatusOS();
      if Dmod.QrOpcoesBugsSWTAgenda.Value = 1 then
      begin
        MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Carregando paletas de cores de listas de status');
        AgendaGerAll.PoeEmMemoryCoresStatusAvul();
        AgendaGerAll.PoeEmMemoryCoresStatusOS();
        //
        MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando configurando agenda em guia (aba)');
        //MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1, False, True);
        AgendaGerAll.MostraFormAgendaGer(PageControl1, AdvToolBarPager1);
      end;
      //
      // Provisorio
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando listas de perguntas');
      VerificaListasPerguntas();
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando MD5 de grupos de agentes');
      DmModOS.MD5_VerificaMD5DeGrupoDeAgentes();
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando integridade de GraGruX');
      DModG.CorrigeCorETamInexistentes();
      // FIM Provisorio
      //

      // Deixar mais para o final!!
      //MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando a��es e renova��es');
      //DmModOS.VerificaFormulasFilhas(False);

      (*
      if DBCheck.CriaFm(TFmAllToRenew, FmAllToRenew, afmoNegarComAviso) then
      begin
        if FmAllToRenew.ItensAbertos() > 0 then
          FmAllToRenew.ShowModal;
        FmAllToRenew.Destroy;
      end;
      *)
      DefineVarsCliInt(VAR_LIB_EMPRESA_SEL);
      //
      DmkWeb2_Jan.ConfiguraDBn(Dmod.MyDB, Dmod.MyDBn, DmodG.QrWebParams);
      //
      //RecriaTiposDeProdutoPadrao;
      //
      UFixBugs.MostraFixBugs([
        (*
        J� executadas nos clientes migradas apenas para testes
        'Atualizar Grupos de OSs e opc��es',
        'Atualizar Servi�o em OSs M�ltiplas',
        'Atualizar equipes de agentes em OSs'
        *)
        'Atualiza textos padr�o',
        'Atualiza Respons�vel t�cnico',
        'Atualiza dados de relat�rios espec�ficos']);
      //
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      //TmVersao.Enabled := True;
      //
      DmodG.VerificaHorVerao();
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
    end;
  finally
    // Ver com MCW quando desmarcar
    //TmSuporte.Enabled := True;
    MyObjects.Informa2(LaAviso2, LaAviso1, False,
      Geral.FF0(VAR_LIB_EMPRESA_SEL) + ' - ' + VAR_LIB_EMPRESA_SEL_TXT);
    Screen.Cursor := crDefault;
  end;
end;

function TFmPrincipal.FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;
begin
  Result := True;
  try
    (* J� executadas
    if FixBug = 0 then
      AppPF.AtualizarOSGruposEOpcoes()
    else if FixBug = 1 then
      AppPF.AtualizarOSMulServico()
    else if FixBug = 2 then
      AppPF.AtualizarOSCabAgeEqiCab()
    *)
    if FixBug = 0 then
      AppPF.AtualizaTextosGenericos()
    else
    if FixBug = 1 then
      AppPF.AtualizaRespTec()
    else
    if FixBug = 2 then
      AppPF.AtualizaRelatiosEspecificos()
    else
      Result := False;
  except
    Result := False;
  end;
end;

procedure TFmPrincipal.CadastroBancos();
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

procedure TFmPrincipal.Caractersticas1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'caracteris', 60, ncGerlSeq1,
  'Cadastro de Caracter�sticas do Local de Aplica��o',
  [], False, Null, [], [], False);
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  Result := 0;//FmFaturas.QrFaturasCodigo.Value;
end;

procedure TFmPrincipal.Centrodecustos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCentroCusto, FmCentroCusto, afmoNegarComAviso) then
  begin
    FmCentroCusto.ShowModal;
    FmCentroCusto.Destroy;
  end;
end;

procedure TFmPrincipal.Chamadasatendidas1Click(Sender: TObject);
begin
  Bina_PF.MostraFormBinaLigouB();
end;

procedure TFmPrincipal.Chamadasrecebidas1Click(Sender: TObject);
begin
  Bina_PF.MostraFormBinaLigouA();
end;

procedure TFmPrincipal.CheckList1Click(Sender: TObject);
begin
  ChekLstJan.MostraChekLstCab();
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  Result := '';//FormatDateTime(VAR_FORMATDATE, FmFaturas.QrFaturasData.Value);
end;

procedure TFmPrincipal.Custos1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCusPrc(0);
end;

procedure TFmPrincipal.Configuraodeemissesmensais1Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeMenCab(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.Corrigeerrosemisses1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  TabLctA, Data: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  PB1.Visible := True;
  TabLctA     := 'lct0001a';
  Qry         := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT lct.Data, lct.Tipo, lct.Carteira, lct.Controle, lct.Sub ',
    'FROM ' + TabLctA + ' lct ',
    'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira ',
    'WHERE car.Tipo=2 ',
    'AND Controle <> 0',
    'AND Sub=0 <> 0',
    '']);
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    if Qry.RecordCount > 0 then
    begin
      Qry.First;
      while not Qry.Eof do
      begin
        MyObjects.UpdPB(PB1, nil, nil);
        UFinanceiro.AtualizaEmissaoMasterExtra_Novo(
          Qry.FieldByName('Controle').AsInteger, Qry, nil, nil, nil, nil, '0',
          TabLctA, False);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
    PB1.Visible := False;
  end;
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

(*
function TFmPrincipal.CriaFormEntradaCab(MaterialNF: TTipoMaterialNF;
  ShowForm: Boolean; IDCtrl: Integer): Boolean;
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  case MaterialNF of
    tnfMateriaPrima:
    begin
      FTipoEntradaDig := VAR_FATID_0113;
      FTipoEntradaNFE := VAR_FATID_0013;
      FTipoEntradaEFD := VAR_FATID_0213;
      FTipoEntradTitu := 'Mat�ria-prima';
    end;
    tnfUsoEConsumo:
    begin
      FTipoEntradaDig := VAR_FATID_0151;
      FTipoEntradaNFe := VAR_FATID_0051;
      FTipoEntradaEFD := VAR_FATID_0251;
      FTipoEntradTitu := 'Uso e Consumo';
    end;
  end;
  Result := DBCheck.CriaFm(TFmEntradaCab, FmEntradaCab, afmoNegarComAviso);
  if Result and ShowForm then
  begin
    if IDCtrl <> 0 then
    begin
      FmEntradaCab.LocCod(IDCtrl, IDCtrl);
      if FmEntradaCab.QrNFeCabAIDCtrl.Value <> IDCtrl then
        Geral.MensagemBox('N�o foi poss�vel localizar o lan�amento solicitado!',
        'Aviso', MB_OK+MB_ICONINFORMATION);
    end;
    FmEntradaCab.ShowModal;
    FmEntradaCab.Destroy;
  end;
end;
*)

procedure TFmPrincipal.Cuidados1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'cuidados', 60, ncGerlSeq1,
    'Cuidados em Locais de Aplica��o',
    [], False, Null, [], [], False);
end;

procedure TFmPrincipal.Saldodecontas1Click(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas(0);
end;

procedure TFmPrincipal.Precos1Click(Sender: TObject);
begin
  GFat_Jan.MostraFormTabePrcCab(0);
end;

procedure TFmPrincipal.VerificaBDServidor1Click(Sender: TObject);
begin
  ALL_Jan.MostraFormVerifiDB(False);
end;

procedure TFmPrincipal.VerificaBDWeb1Click(Sender: TObject);
begin
  DmkWeb_Jan.MostraVerifiDBi(DmodG.QrWebParams, Dmod.MyDBn, VAR_VERIFI_DB_CANCEL);
end;

// Provis�rio
procedure TFmPrincipal.VerificaListasPerguntas();
var
  Qry1: TmySQLQuery;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT * FROM prglstits ',
    'WHERE Pergunta=0 OR Nome<>"" OR Binario0<>"" OR Binario1<>""',
    '']);
    //
    BtArrumaPrg.Visible := Qry1.RecordCount > 0;
  finally
    Qry1.Free;
  end;
end;
// FIM Provis�rio

procedure TFmPrincipal.NotificacoesJanelas(TipoNotifi: TNotifi; DataHora: TDateTime);
begin
  if TipoNotifi = ntfBugsOSsFilhas then
    DmModOS.VerificaFormulasFilhas(True, False)
  //
  else if TipoNotifi = ntfBugsOSsGarantias then
    DmModOS.GarantiasAVencer(DataHora, True);
end;

function TFmPrincipal.NotificacoesVerifica(QueryNotifi: TmySQLQuery;
  DataHora: TDateTime): Boolean;
begin
  Result := False;
  //
  if not QueryNotifi.Locate('Codigo', Integer(ntfBugsOSsFilhas), []) then
  begin
    if DmModOS.VerificaFormulasFilhas(False, False) <> 0 then
      UnNotificacoesEdit.CriaNotificacaoPublica(Dmod.MyDB, ntfBugsOSsFilhas,
        DmodG.ObtemAgora(True));
    Result := True;
  end;

  if not QueryNotifi.Locate('Codigo', Integer(ntfBugsOSsGarantias), []) then
  begin
    if DmModOS.GarantiasAVencer(DataHora, False) <> 0 then
      UnNotificacoesEdit.CriaNotificacaoPublica(Dmod.MyDB, ntfBugsOSsGarantias,
        DmodG.ObtemAgora(True));
    Result := True;
  end;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
var
  Versao: Integer;
  ArqNome: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'Bugstrol',
    'Bugstrol', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExec, Versao, ArqNome, False,
    ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.Verificanovaverso1Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.VerificaTabelasterceiros1Click(Sender: TObject);
begin
  ALL_Jan.MostraFormVerifiDBTerceiros(False);
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
(*
var
  Dia: Integer;
*)
begin
  (*
  TimerIdle.Enabled := False;
  Dia := Geral.ReadAppKey('VeriNetVersao', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    if not VerificaNovasVersoes then Application.Terminate;
  end else
    Application.Terminate;
  *)
  if DModG <> nil then
    DmodG.ExecutaPing(FmBugstrol_Dmk, [Dmod.MyDB, Dmod.MyDBn, DModG.MyPID_DB,
      DModG.AllID_DB]);
end;

procedure TFmPrincipal.TimerPingServerTimer(Sender: TObject);
(*
var
  Qry: TmySQLQuery;
*)
begin
  (* 2017-05-24 => Foi criada um fun��o que executa no OnIdle para substituir
  // Ping no mysql a cada 30 minutos (10800000 )
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.Database := Dmod.MyDB;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT 1 Um');
    Qry.Open;
    {
    if Qry.FieldByName('Um').AsInteger = 1 then
      Memo1.Text := 'Ping ' + Geral.FDT(Now(), 109) + #13#10 + Memo1.Text;
    }
    Qry.Close;
  finally
    Qry.Free;
  end;
  *)
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    ATBTemAtualizacao, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection() then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(ATBVerificaNovaVersao,
      BalloonHint1, 'H� uma nova vers�o!', 'Clique aqui para atualizar!');
  end;
end;

procedure TFmPrincipal.AtzSdoContas;
begin
{ N�o usar! Ver o que usa no novo
  if DBCheck.CriaFm(TFmContasHistAtz2, FmContasHistAtz2, afmoNegarComAviso) then
  begin
    FmContasHistAtz2.FEntiCod := 0;
    FmContasHistAtz2.FGenero  := 0;
    FmContasHistAtz2.FPeriodo := -23999;
    //
    FmContasHistAtz2.ShowModal;
    FmContasHistAtz2.Destroy;
  end;
}
end;

procedure TFmPrincipal.Automoo1Click(Sender: TObject);
begin
 // MostraFormMobiliUni();
end;

procedure TFmPrincipal.Avisos1Click(Sender: TObject);
var
  Msg, Link: String;
begin
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg) then
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      Link := 'http://bugstrolweb.dermatek.net.br/' +
                DModG.QrOpcoesGerl.FieldByName('WebId').AsString + '.php?page=avisos';
      //
      ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
    end;
  end else
    Geral.MB_Aviso(Msg);
end;

procedure TFmPrincipal.BalanceteConfiguravel(TabLctA: String);
begin
  //
  if not PreparaPesquisaFinanceira(TabLctA) then
    Exit;
  //
  //
  if DBCheck.CriaFm(TFmCashBal, FmCashBal, afmoNegarComAviso) then
  begin
    FmCashBal.ShowModal;
    FmCashBal.Destroy;
  end;
end;

// Provis�rio
procedure TFmPrincipal.BtArrumaPrgClick(Sender: TObject);
var
  Qry1, Qry2: TmySQLQuery;
  //
var
  Nome: String;
  Codigo, Controle, Pergunta: Integer;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT * FROM prglstits ',
      'WHERE Pergunta=0 AND Nome<>"" ',
      'ORDER BY Controle ',
      '']);
      //
      Qry1.First;
      while not Qry1.Eof do
      begin
        Codigo         := 0;
        Nome           := Qry1.FieldByName('Nome').AsString;
        //
        Codigo := UMyMod.BPGS1I32('prgcadprg', 'Codigo', '', '', tsPos, stIns, Codigo);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prgcadprg', False, [
        'Nome', 'Sigla'], [
        'Codigo'], [
        Nome, Geral.FF0(Codigo)], [
        Codigo], True) then
        begin
          Pergunta := Codigo;
          Controle := Qry1.FieldByName('Controle').AsInteger;
          Nome := '';
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'prglstits', False, [
          'Nome', 'Pergunta'], [
          'Controle'], [
          Nome, Pergunta], [
          Controle], True);
        end;
        //
        Qry1.Next;
      end;




      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT * FROM prglstits ',
      'WHERE Binario0<>"" or Binario1<>""',
      'ORDER BY Controle ',
      '']);
      //
      Qry1.First;
      while not Qry1.Eof do
      begin
        Nome := Qry1.FieldByName('Binario0').AsString;
        if Nome <> '' then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
          'SELECT Codigo FROM prgbincad ',
          'WHERE Nome="' + Nome + '" ',
          '']);
          Codigo := Qry2.FieldByName('Codigo').AsInteger;
          if Codigo = 0 then
          begin
            Codigo := UMyMod.BPGS1I32('PrgBinCad', 'Codigo', '', '', tsPos, stIns, Codigo);
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'PrgBinCad', False, [
            'Nome'], [
            'Codigo'], [
            Nome], [
            Codigo], True);
          end;
          //
          begin
            //Pergunta := Codigo;
            Controle := Qry1.FieldByName('Controle').AsInteger;
            Nome := '';
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'prglstits', False, [
            'Binario0', 'BinarCad0'], [
            'Controle'], [
            '', Codigo], [
            Controle], True);
          end;
        end;
        //
        Nome := Qry1.FieldByName('Binario1').AsString;
        if Nome <> '' then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
          'SELECT Codigo FROM prgbincad ',
          'WHERE Nome="' + Nome + '" ',
          '']);
          Codigo := Qry2.FieldByName('Codigo').AsInteger;
          if Codigo = 0 then
          begin
            Codigo := UMyMod.BPGS1I32('PrgBinCad', 'Codigo', '', '', tsPos, stIns, Codigo);
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'PrgBinCad', False, [
            'Nome'], [
            'Codigo'], [
            Nome], [
            Codigo], True);
          end;
          //
          begin
            //Pergunta := Codigo;
            Controle := Qry1.FieldByName('Controle').AsInteger;
            Nome := '';
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'prglstits', False, [
            'Binario1', 'BinarCad1'], [
            'Controle'], [
            '', Codigo], [
            Controle], True);
          end;
        end;
        //
        Qry1.Next;
      end;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
  VerificaListasPerguntas();
end;
// FIM Provis�rio

procedure TFmPrincipal.BtSVG2Click(Sender: TObject);

  procedure CriaSVG(var SvgImg: array of TRSSVGImageObj; const Idx, Id: Integer;
    const Nome, ImgArq: String);
  var
    Img: TPicture;
  begin
    SvgImg[Idx].Id     := Id;
    SvgImg[Idx].Nome   := Nome;
    SvgImg[Idx].ImgArq := ImgArq;
    SvgImg[Idx].EmUso  := False;
    SvgImg[Idx].W      := 0;
    SvgImg[Idx].H      := 0;
    SvgImg[Idx].X      := 0;
    SvgImg[Idx].Y      := 0;
  end;
begin
  Application.CreateForm(TFmDmkSVGEditor, FmDmkSVGEditor);
  //
  SetLength(FmDmkSVGEditor.FSvgImgNPosi, 9);
  //
  CriaSVG(FmDmkSVGEditor.FSvgImgNPosi, 0, 1, 'PMV 01', 'C:\Dermatek\SVG\Croquis\Adesivo_de_visita_A.svg');
  CriaSVG(FmDmkSVGEditor.FSvgImgNPosi, 1, 2, 'PMV 02', 'C:\Dermatek\SVG\Croquis\Armadilha_Bola_B.svg');
  CriaSVG(FmDmkSVGEditor.FSvgImgNPosi, 2, 3, 'PMV 03', 'C:\Dermatek\SVG\Croquis\Bloco_de_Notas_A.svg');
  CriaSVG(FmDmkSVGEditor.FSvgImgNPosi, 3, 4, 'PMV 03', 'C:\Dermatek\SVG\Croquis\PMV_Mosca_B.svg');
  CriaSVG(FmDmkSVGEditor.FSvgImgNPosi, 4, 5, 'PMV 03', 'C:\Dermatek\SVG\Croquis\PMV_Porta_cola_B.svg');
  CriaSVG(FmDmkSVGEditor.FSvgImgNPosi, 5, 6, 'PMV 03', 'C:\Dermatek\SVG\Croquis\PMV_Roedores_B.svg');
  CriaSVG(FmDmkSVGEditor.FSvgImgNPosi, 6, 7, 'PMV 03', 'C:\Dermatek\SVG\Croquis\PV_Ponto_de_verifica��o_B.svg');
  CriaSVG(FmDmkSVGEditor.FSvgImgNPosi, 7, 8, 'PMV 03', 'C:\Dermatek\SVG\Croquis\Quadro_negro_A.svg');
  CriaSVG(FmDmkSVGEditor.FSvgImgNPosi, 8, 9, 'PMV 03', 'C:\Dermatek\SVG\Croquis\Ratoeira_target_B.svg');
  //
  SetLength(FmDmkSVGEditor.FSvgImgPosi, 0);
  //
  FmDmkSVGEditor.ShowModal;
  FmDmkSVGEditor.Destroy;
end;

procedure TFmPrincipal.Button3Click(Sender: TObject);
begin
(*
type
TForm1 = class(TForm)
Button1: TButton;
procedure Button1MouseDown(Sender: TObject; Button: TMouseButton;
Shift: TShiftState; X, Y: Integer);
procedure Button1MouseMove(Sender: TObject; Shift: TShiftState; X,
Y: Integer);
procedure Button1MouseUp(Sender: TObject; Button: TMouseButton;
Shift: TShiftState; X, Y: Integer);
private
{ Private declarations }
public
{ Public declarations }
MouseDownSpot : TPoint;
Capturing : bool;
end;
var
Form1: TForm1;

implementation
{$R *.DFM}

// Evento OnMouseDown do Form
procedure TForm1.Button1MouseDown(Sender: TObject; Button: TMouseButton;
Shift: TShiftState; X, Y: Integer);
begin
if ssCtrl in Shift then begin
SetCapture(Button1.Handle);
Capturing := true;
MouseDownSpot.X := x;
MouseDownSpot.Y := Y;
end;
end;

// Evento OnMouseMove do Form
procedure TForm1.Button1MouseMove(Sender:
TObject; Shift: TShiftState; X, Y: Integer);
begin
if Capturing then begin
Button1.Left:= Button1.Left-(MouseDownSpot.x-x);
Button1.Top:= Button1.Top - (MouseDownSpot.-y);
end;
end;

// Evento OnMouseUp do Form
procedure TForm1.Button1MouseUp(Sender: TObject; Button: TMouseButton;
Shift: TShiftState; X, Y: Integer);
begin
if Capturing then begin
ReleaseCapture;
Capturing := false;
Button1.Left := Button1.Left - (MouseDownSpot.x -x);
Button1.Top := Button1.Top - (MouseDownSpot.y - y);
end;
end;
*)
end;

procedure TFmPrincipal.Atividades1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'atividades', 60, ncGerlSeq1,
  'Cadastro de Atividade',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.Atividadesaseremexecutadas1Click(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgenExCad();
end;

procedure TFmPrincipal.Atributosdeperguntas1Click(Sender: TObject);
const
  QrAtrCad = nil;
  QrAtrIts = nil;
  QrCad    = nil;
  QrIts    = nil;
  EdAtrCad = nil;
  EdAtrIts = nil;
  CBAtrCad = nil;
  CBAtrIts = nil;
begin
  UnCfgAtributos.CadastraAtrCad(tiptbCAB, 'PrgAtrCad', 'PrgAtrIts', '',
  QrAtrCad, QrAtrIts, QrCad, QrIts, EdAtrCad, EdAtrIts, CBAtrCad, CBAtrIts,
  False);
end;

procedure TFmPrincipal.Atualizasaldodecontas1Click(Sender: TObject);
begin
  AtzSdoContas;
end;

procedure TFmPrincipal.CadastroDeContasNiv();
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;

procedure TFmPrincipal.RetornoCNAB;
begin
  // Compatibilidade
end;

procedure TFmPrincipal.Rotas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRotaLatLon, FmRotaLatLon, afmoNegarComAviso) then
  begin
    FmRotaLatLon.ShowModal;
    FmRotaLatLon.Destroy;
  end;
end;

//http://animaldiversity.ummz.umich.edu/accounts/Animalia/pictures/

(*

SELECT sic.*
FROM siapimacad sic
LEFT JOIN siaptercad stc ON sic.SiapImater=stc.codigo
WHERE stc.Cliente=1433
*)

end.
