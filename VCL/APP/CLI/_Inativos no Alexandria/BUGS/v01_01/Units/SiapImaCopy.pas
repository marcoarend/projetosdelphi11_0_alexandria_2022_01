unit SiapImaCopy;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, dmkEdit, dmkEditCB,
  Variants, dmkDBLookupComboBox, dmkImage, UnDmkEnums;

type
  TFmSiapImaCopy = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_ENT: TWideStringField;
    QrSiapTerCad: TmySQLQuery;
    DsSiapTerCad: TDataSource;
    QrSiapTerCadCodigo: TIntegerField;
    QrSiapTerCadNome: TWideStringField;
    GroupBox1: TGroupBox;
    CkExportar: TCheckBox;
    PnExportar: TPanel;
    Label1: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdLugar: TdmkEditCB;
    CBLugar: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CkExportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSiapImaTer: Integer;
    FContinua: Boolean;
    //
    procedure ReopenEntidades();
    procedure ReopenSiapTerCad(Entidade: Integer);
  end;

  var
  FmSiapImaCopy: TFmSiapImaCopy;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmSiapImaCopy.BtOKClick(Sender: TObject);
begin
  if PNExportar.Visible and CkExportar.Checked then
  begin
    FSiapImaTer := EdLugar.ValueVariant;
    if MyObjects.FIC(FSiapImaTer=0, EdLugar, 'Informe o lugar!') then
      Exit;
  end;
  FContinua := FSiapImaTer <> 0;
  if FContinua then
    Close;
end;

procedure TFmSiapImaCopy.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSiapImaCopy.CkExportarClick(Sender: TObject);
begin
  PnExportar.Visible := CkExportar.Checked;
end;

procedure TFmSiapImaCopy.EdEntidadeChange(Sender: TObject);
var
  Entidade: Integer;
begin
  QrSiapterCad.Close;
  Entidade := EdEntidade.ValueVariant;
  if Entidade <> 0 then
    ReopenSiapTerCad(Entidade);
end;

procedure TFmSiapImaCopy.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSiapImaCopy.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSiapImaTer := 0;
  FContinua := False;
  //
  CBEntidade.LocF7SQLMasc := CO_JOKE_SQL;
  CBEntidade.LocF7SQLText.Text := Geral.ATS([
  'SELECT ent.Codigo _Codigo, CONCAT(',
  'CASE WHEN ent.RazaoSocial<>"" THEN CONCAT(" [R] ", ent.RazaoSocial) ELSE "" END, ',
  'CASE WHEN ent.Nome<>"" THEN CONCAT(" [N] ", ent.Nome) ELSE "" END, ',
  'CASE WHEN ent.Fantasia<>"" THEN CONCAT(" [F] ", ent.Fantasia) ELSE "" END, ',
  'CASE WHEN ent.Apelido<>"" THEN CONCAT(" [A] ", ent.Apelido) ELSE "" END',
  ') _Nome',
  'FROM cunscad cun',
  'LEFT JOIN entidades ent ON cun.Codigo=ent.Codigo',
  'WHERE (',
  '     ent.RazaoSocial LIKE "%' + CO_JOKE_SQL + '%"',
  'OR',
  '     ent.Nome LIKE "%' + CO_JOKE_SQL + '%"',
  'OR',
  '     ent.Fantasia LIKE "%' + CO_JOKE_SQL + '%"',
  'OR',
  '     ent.Apelido LIKE "%' + CO_JOKE_SQL + '%"',
  ')',
  'ORDER BY _Nome',
  '']);
  //
  ReopenEntidades();
end;

procedure TFmSiapImaCopy.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSiapImaCopy.ReopenSiapTerCad(Entidade: Integer);
var
  SQL_WHERE: String;
begin
  if Entidade <> 0 then
    SQL_WHERE := 'WHERE Cliente=' + Geral.FF0(Entidade)
  else
    SQL_WHERE := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapTerCad, Dmod.MyDB, [
  'SELECT Codigo, Nome  ',
  'FROM siaptercad ',
  SQL_WHERE,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmSiapImaCopy.ReopenEntidades();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
  'SELECT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM cunscad cun ',
  'LEFT JOIN entidades ent ON cun.Codigo=ent.Codigo ',
  'ORDER BY NO_ENT ',
  '']);
end;

end.
