unit PrgLstIts2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkRadioGroup,
  Variants;

type
  TListaTip = (lsttPerguntas = 1, lsttBinarNeg = 2, lsttBinarPos = 3);
  TFmPrgLstIts2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrPrgCadPrg: TmySQLQuery;
    QrPrgCadPrgCodigo: TIntegerField;
    QrPrgCadPrgNome: TWideStringField;
    PnDados: TPanel;
    RGFuncoes: TdmkRadioGroup;
    LaPergunta: TLabel;
    CBPergunta: TComboBox;
    SbPrgCadPrg: TSpeedButton;
    PnFuncao: TPanel;
    PCFuncoes: TPageControl;
    TS1: TTabSheet;
    TS2: TTabSheet;
    TS3: TTabSheet;
    TS4: TTabSheet;
    TS5: TTabSheet;
    Panel5: TPanel;
    RGCasasQtde: TdmkRadioGroup;
    RGAcaoPrd: TdmkRadioGroup;
    CBBinarCad0: TComboBox;
    LaBinarCad0: TLabel;
    SBBinarCad0: TSpeedButton;
    LaBinarCad1: TLabel;
    SBBinarCad1: TSpeedButton;
    CBBinarCad1: TComboBox;
    QrPrgBinCad0: TmySQLQuery;
    QrPrgBinCad0Codigo: TIntegerField;
    QrPrgBinCad0Nome: TWideStringField;
    QrPrgBinCad1: TmySQLQuery;
    QrPrgBinCad1Codigo: TIntegerField;
    QrPrgBinCad1Nome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbPrgCadPrgClick(Sender: TObject);
    procedure CBPerguntaExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGFuncoesClick(Sender: TObject);
    procedure SBBinarCad1Click(Sender: TObject);
    procedure SBBinarCad0Click(Sender: TObject);
    procedure CBBinarCad0Exit(Sender: TObject);
    procedure CBBinarCad1Exit(Sender: TObject);
  private
    { Private declarations }
    FPergCodi, FPergNome, FBinNCodi, FBinNNome, FBinPCodi, FBinPNome: TStringList;
    function  ComboBoxObtemCodigo(ComboBox: TComboBox; ListaCodi: TStringList): Integer;
    procedure ComboBoxCriaLista();
    procedure ComboBoxConfigura(Lista: TListaTip; Valor: Integer);
    procedure ComboBoxSetaCodigoPesquisado(ComboBox: TComboBox; Valor: Integer;
              Lista: TListaTip);
    procedure ComboBoxCadastraItem(Lista: TListaTip);
    procedure MostraEdicao(SQLTipo: TSQLType);
    procedure MostraFormPrgBinCad(ComboBox: TComboBox; Cod: TStringList;
              Lista: TListaTip);
  public
    { Public declarations }
  end;

  var
  FmPrgLstIts2: TFmPrgLstIts2;

implementation

uses UnMyObjects, Module, DmkDAC_PF, CfgCadLista, ModOS, UnProjGroup_Consts,
  UMySQLModule;

{$R *.DFM}

procedure TFmPrgLstIts2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrgLstIts2.CBBinarCad0Exit(Sender: TObject);
begin
  ComboBoxCadastraItem(lsttBinarNeg);
end;

procedure TFmPrgLstIts2.CBBinarCad1Exit(Sender: TObject);
begin
  ComboBoxCadastraItem(lsttBinarPos);
end;

procedure TFmPrgLstIts2.CBPerguntaExit(Sender: TObject);
begin
  ComboBoxCadastraItem(lsttPerguntas);
end;

procedure TFmPrgLstIts2.ComboBoxCadastraItem(Lista: TListaTip);

  procedure SalvaDB(Nom: TStringList; ComboBox: TComboBox; Tabela, Pergunta: String);
  var
    Continua: Boolean;
    Codigo: Integer;
    Nome: String;
  begin
    if (ComboBox.Text <> '') and (Nom.IndexOf(Trim(ComboBox.Text)) < 0) then
    begin
      if Geral.MB_Pergunta(Pergunta) = ID_YES then
      begin
        Continua := False;
        Codigo   := UMyMod.BPGS1I32(Tabela, 'Codigo', '', '', tsPos, stIns, 0);
        Nome     := ComboBox.Text;
        //
        if Lista = lsttPerguntas then
        begin
          Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False,
                        ['Nome', 'Sigla'], ['Codigo'], [Nome, Nome], [Codigo], True);
        end else
        begin
          Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False,
                        ['Nome'], ['Codigo'], [Nome, Nome], [Codigo], True);
        end;
        if Continua then
          ComboBoxConfigura(Lista, Codigo);
      end else
      begin
        ComboBox.ItemIndex := -1;
        ComboBox.Text      := '';
        ComboBox.SetFocus;
      end;
    end;
  end;

begin
  case Lista of
    lsttPerguntas:
    begin
      SalvaDB(FPergNome, CBPergunta, 'prgcadprg', 'Esta pergunta n�o est� cadastrada!' +
        sLineBreak + 'Deseja cadastr�-la?');
    end;
    lsttBinarNeg:
    begin
      SalvaDB(FBinNNome, CBBinarCad0, 'prgbincad', 'Este texto n�o est� cadastrado!' +
        sLineBreak + 'Desaja cadastr�-lo?');
    end;
    lsttBinarPos:
    begin
      SalvaDB(FBinPCodi, CBBinarCad1, 'prgbincad', 'Este texto n�o est� cadastrado!' +
        sLineBreak + 'Desaja cadastr�-lo?');
    end;
  end;
end;

procedure TFmPrgLstIts2.ComboBoxConfigura(Lista: TListaTip; Valor: Integer);

  procedure Configura(Cod, Nom: TStringList; ComboBox: TComboBox;
    Query: TmySQLQuery);
  var
    Codigo, Idx: Integer;
    Nome: String;
  begin
    Cod.Clear;
    Nom.Clear;
    ComboBox.Items.Clear;
    //
    UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
    //
    if Query.RecordCount > 0 then
    begin
      while not Query.Eof do
      begin
        Codigo := Query.FieldByName('Codigo').AsInteger;
        Nome   := Query.FieldByName('Nome').AsString;
        //
        Cod.Add(Geral.FF0(Codigo));
        Nom.Add(Nome);
        //
        ComboBox.Items := Nom;
        //
        QrPrgCadPrg.Next;
      end;
    end;
    Idx := Cod.IndexOf(Geral.FF0(Valor));
    //
    ComboBox.ItemIndex := Idx;
  end;

begin
  case Lista of
    lsttPerguntas:
    begin
      Screen.Cursor := crHourGlass;
      try
        Configura(FPergCodi, FPergNome, CBPergunta, QrPrgCadPrg);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    lsttBinarNeg:
    begin
      Screen.Cursor := crHourGlass;
      try
        Configura(FBinNCodi, FBinNNome, CBBinarCad0, QrPrgBinCad0);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    lsttBinarPos:
    begin
      Screen.Cursor := crHourGlass;
      try
        Configura(FBinPCodi, FBinPNome, CBBinarCad1, QrPrgBinCad1);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmPrgLstIts2.ComboBoxCriaLista;
begin
  FPergCodi := TStringList.Create;
  FPergNome := TStringList.Create;
  //
  FBinNCodi := TStringList.Create;
  FBinNNome := TStringList.Create;
  FBinPCodi := TStringList.Create;
  FBinPNome := TStringList.Create;
end;

procedure TFmPrgLstIts2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrgLstIts2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGFuncoes, sListaFuncaoPrgAtrCab, 2, -1);
  //
  ComboBoxCriaLista;
end;

procedure TFmPrgLstIts2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrgLstIts2.FormShow(Sender: TObject);
begin
  MostraEdicao(stIns);
end;

procedure TFmPrgLstIts2.MostraEdicao(SQLTipo: TSQLType);
begin
  //Configura PageControl
  if PCFuncoes.PageCount <> MaxFuncaoPrgAtrCab then
    Geral.MB_Erro(
      'A quantidade de fun��es difere da quantidade de fun��es implementadas!' + sLineBreak +
      'Fun��es de perguntas: ' + Geral.FF0(MaxFuncaoPrgAtrCab) + sLineBreak +
      'Fun��es implementadas: ' + Geral.FF0(PCFuncoes.PageCount) + sLineBreak +
      'AVISE a Dermatek!');
  //
  TS1.Caption := sListaFuncaoPrgAtrCab[1];
  TS2.Caption := sListaFuncaoPrgAtrCab[2];
  TS3.Caption := sListaFuncaoPrgAtrCab[3];
  TS4.Caption := sListaFuncaoPrgAtrCab[4];
  TS5.Caption := sListaFuncaoPrgAtrCab[5];
  //
  if SQLTipo = stIns then
  begin
    ComboBoxConfigura(lsttPerguntas, 0);
    //
    RGFuncoes.ItemIndex := -1;
    PnFuncao.Visible    := False;
  end else
  begin

  end;
end;

procedure TFmPrgLstIts2.RGFuncoesClick(Sender: TObject);
begin
  PnFuncao.Visible := RGFuncoes.ItemIndex > 0;
end;

procedure TFmPrgLstIts2.MostraFormPrgBinCad(ComboBox: TComboBox;
  Cod: TStringList; Lista: TListaTip);
var
  PrgBinCad: Integer;
begin
  VAR_CADASTRO := 0;
  PrgBinCad    := ComboBoxObtemCodigo(ComboBox, Cod);
  //
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'PrgBinCad', 60, ncGerlSeq1,
    'Textos Bin�rios', [], False, Null, [], [], False, PrgBinCad);
  //
  if VAR_CADASTRO <> 0 then
    ComboBoxSetaCodigoPesquisado(ComboBox, VAR_CADASTRO, Lista);
end;

procedure TFmPrgLstIts2.SBBinarCad0Click(Sender: TObject);
begin
  MostraFormPrgBinCad(CBBinarCad0, FBinNCodi, lsttBinarNeg);
end;

procedure TFmPrgLstIts2.SBBinarCad1Click(Sender: TObject);
begin
  MostraFormPrgBinCad(CBBinarCad1, FBinPCodi, lsttBinarPos);
end;

procedure TFmPrgLstIts2.SbPrgCadPrgClick(Sender: TObject);
var
  Pergunta: Integer;
begin
  VAR_CADASTRO := 0;
  Pergunta     := ComboBoxObtemCodigo(CBPergunta, FPergCodi);
  //
  UnCfgCadLista.MostraCadRegistroSimples(Dmod.MyDB, DmModOS.TbPrgCadPrg,
    DmModOS.DsPrgCadPrg, ncGerlSeq1, 'Cadastro de Perguntas', Pergunta);
  //
  if VAR_CADASTRO <> 0 then
    ComboBoxSetaCodigoPesquisado(CBPergunta, VAR_CADASTRO, lsttPerguntas);
end;

procedure TFmPrgLstIts2.ComboBoxSetaCodigoPesquisado(ComboBox: TComboBox;
  Valor: Integer; Lista: TListaTip);
begin
  ComboBoxConfigura(Lista, Valor);
  ComboBox.SetFocus;
end;

function TFmPrgLstIts2.ComboBoxObtemCodigo(ComboBox: TComboBox; ListaCodi: TStringList): Integer;
begin
  if ComboBox.ItemIndex > -1 then
    Result := Geral.IMV(ListaCodi[ComboBox.ItemIndex])
  else
    Result := 0;
end;

end.
