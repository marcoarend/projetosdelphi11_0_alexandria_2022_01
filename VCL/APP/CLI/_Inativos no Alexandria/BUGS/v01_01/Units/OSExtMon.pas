unit OSExtMon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmOSExtMon = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    TPDtaFinal: TdmkEditDateTimePicker;
    Label1: TLabel;
    EdPerioDd: TdmkEdit;
    Label2: TLabel;
    Label4: TLabel;
    EdTotalDd: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure TPDtaFinalChange(Sender: TObject);
    procedure EdPerioDdChange(Sender: TObject);
    procedure TPDtaFinalClick(Sender: TObject);
    procedure EdPerioDdExit(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenOSExtMon(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    FAtuDiasMax: Integer;
    FMinData, FDtaExeFim: TDateTime;
    //
    procedure CalculaDataEDias(Kind: Integer);
  end;

  var
  FmOSExtMon: TFmOSExtMon;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
UnOSApp_PF;

{$R *.DFM}

procedure TFmOSExtMon.BtOKClick(Sender: TObject);
var
  DtaFinal: String;
  Codigo, Controle, PerioDd, TotalDd: Integer;
begin
  if MyObjects.FIC(TPDtaFinal.Date < FMinData, TPDtaFinal, 'Data inv�lida!') then
    Exit;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  DtaFinal       := Geral.FDT(TPDtaFinal.Date, 1);
  PerioDd        := EdPerioDd.ValueVariant;
  TotalDd        := EdTotalDd.ValueVariant;
  //
  Controle := UMyMod.BPGS1I32('osextmon', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osextmon', False, [
  'Codigo', 'DtaFinal', 'PerioDd',
  'TotalDd'], [
  'Controle'], [
  Codigo, DtaFinal, PerioDd,
  TotalDd], [
  Controle], True) then
  begin
    OSApp_PF.AtualizaExtenDdOS(Codigo, True);
    //
    ReopenOSExtMon(Controle);
    Close;
  end;
end;

procedure TFmOSExtMon.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSExtMon.CalculaDataEDias(Kind: Integer);
var
  Data: TDateTime;
  Dias: Integer;
begin
  if Kind = 0 then // Data
  begin
    Data := TPDtaFinal.Date;
    Dias := Trunc(Data - FDtaExeFim);
    EdTotalDd.ValueVariant := Dias;
    Dias := Dias - FAtuDiasMax;
    if EdPerioDd.ValueVariant <> Dias then
      EdPerioDd.ValueVariant := Dias;
  end else
  begin
    Dias := EdPerioDd.ValueVariant;
    Data := FDtaExeFim + Dias + FAtuDiasMax;
    if TPDtaFinal.Date <> Data then
      TPDtaFinal.Date := Data;
    EdTotalDd.ValueVariant := Trunc(Data - FDtaExeFim);
  end;
end;

procedure TFmOSExtMon.EdPerioDdChange(Sender: TObject);
begin
  if EdPerioDd.Focused then
    CalculaDataEDias(1);
end;

procedure TFmOSExtMon.EdPerioDdExit(Sender: TObject);
begin
    CalculaDataEDias(1);
end;

procedure TFmOSExtMon.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOSExtMon.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmOSExtMon.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSExtMon.ReopenOSExtMon(Controle: Integer);
begin
(* Nao precisa! Vai reabrir toda OS!
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
*)
end;

procedure TFmOSExtMon.TPDtaFinalChange(Sender: TObject);
begin
  if not EdPerioDd.Focused then
    CalculaDataEDias(0);
end;

procedure TFmOSExtMon.TPDtaFinalClick(Sender: TObject);
begin
  if not EdPerioDd.Focused then
    CalculaDataEDias(0);
end;

end.
