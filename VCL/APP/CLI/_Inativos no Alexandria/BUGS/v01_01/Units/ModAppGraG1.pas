unit ModAppGraG1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, dmkEdit,
  System.Classes, Vcl.Graphics, Vcl.StdCtrls, Vcl.Controls, Vcl.Forms,
  Vcl.Dialogs, Data.DB, mySQLDbTables, UnProjGroup_Consts, dmkGeral, MyDBCheck,
  UnDmkEnums, UnMyObjects;

type
  TDfModAppGraG1 = class(TForm)
    QrGraG1EqAp: TmySQLQuery;
    QrGraG1EqApNivel1: TIntegerField;
    QrGraG1EqApMarca: TIntegerField;
    QrGraG1EqApObservacao: TWideMemoField;
    QrGraG1EqMo: TmySQLQuery;
    QrGraG1EqMoNivel1: TIntegerField;
    QrGraG1EqMoMarca: TIntegerField;
    QrGraG1EqMoObservacao: TWideMemoField;
    QrGraG1EqMoPrgLstCab: TIntegerField;
    QrGraG1EqMoNaoUsaPrdt: TSmallintField;
    QrGraG1EqMoVetorArq: TWideStringField;
    QrGraG1EqMoVetorSVG: TWideMemoField;
    QrGraG1EqMoPrgICorSVG: TIntegerField;
    QrGraG1EqMoDiasCorSVG: TIntegerField;
    QrGraG1EqMoQtCorSVGgg: TIntegerField;
    QrGraG1EqMoQtCorSVGgr: TIntegerField;
    QrGraG1EqMoQtCorSVGrg: TIntegerField;
    QrGraG1EqMoQtCorSVGrr: TIntegerField;
    QrGraG1EqMoQtCorSVGrb: TIntegerField;
    QrGraG1EqMoQtCorSVGbr: TIntegerField;
    QrGraG1EqMoSVGLegenda: TWideStringField;
    QrGraG1EqMoZumPadr: TFloatField;
    QrGraG1PrAp: TmySQLQuery;
    QrGraG1PrApNivel1: TIntegerField;
    QrGraG1PrApRegMinSaud: TWideStringField;
    QrGraG1PrApMarca: TIntegerField;
    QrGraG1PrApImpInOrca: TSmallintField;
    QrGraG1PrApObservacao: TWideMemoField;
    QrGraG1PrApFoneCIT: TWideStringField;
    QrGraG1PrApFoneCEATOX: TWideStringField;
    QrGraG1PrApToxicidade: TWideStringField;
    QrGraG1PrApConcentrac: TWideStringField;
    QrGraG1PrApAcaoToxica: TWideStringField;
    QrGraG1PrApAntidoto: TWideStringField;
    QrGraG1PrApUnidMed: TIntegerField;
    QrGraG1PrApSIGLA: TWideStringField;
    QrGraG1PrApCU_UnidMed: TIntegerField;
    QrGraG1PrApAtuNumLot: TWideStringField;
    QrGraG1PrApAtuNumLaud: TWideStringField;
    QrGraG1PrMo: TmySQLQuery;
    QrGraG1PrMoNivel1: TIntegerField;
    QrGraG1PrMoRegMinSaud: TWideStringField;
    QrGraG1PrMoMarca: TIntegerField;
    QrGraG1PrMoImpInOrca: TSmallintField;
    QrGraG1PrMoObservacao: TWideMemoField;
    QrGraG1PrMoFoneCIT: TWideStringField;
    QrGraG1PrMoFoneCEATOX: TWideStringField;
    QrGraG1PrMoToxicidade: TWideStringField;
    QrGraG1PrMoConcentrac: TWideStringField;
    QrGraG1PrMoAcaoToxica: TWideStringField;
    QrGraG1PrMoAntidoto: TWideStringField;
    QrGraG1PrMoValCliDd: TIntegerField;
    QrGraG1PrMoUnidMed: TIntegerField;
    QrGraG1PrMoSIGLA: TWideStringField;
    QrGraG1PrMoCU_UnidMed: TIntegerField;
    QrGraG1PrMoAtuNumLot: TWideStringField;
    QrGraG1PrMoAtuNumLaud: TWideStringField;
    QrGraG1Veic: TmySQLQuery;
    QrGraG1VeicNivel1: TIntegerField;
    QrGraG1VeicMarca: TIntegerField;
    QrGraG1VeicPlaca: TWideStringField;
    QrGraG1VeicCustoKm: TFloatField;
    QrGraG1VeicUnidMed: TIntegerField;
    QrGraG1VeicNCM: TWideStringField;
    QrGraG1PrMoNCM: TWideStringField;
    QrGraG1PrApNCM: TWideStringField;
    QrGraG1EqApNCM: TWideStringField;
    QrGraG1EqApUnidMed: TIntegerField;
    QrGraG1EqMoNCM: TWideStringField;
    QrGraG1EqMoUnidMed: TIntegerField;
    QrGraG1EqApReferencia: TWideStringField;
    QrGraG1EqMoReferencia: TWideStringField;
    QrGraG1PrApReferencia: TWideStringField;
    QrGraG1PrMoReferencia: TWideStringField;
    QrGraG1VeicReferencia: TWideStringField;
    QrGraG1PrApFrmApres: TIntegerField;
    QrGraG1EqMoFrmApres: TIntegerField;
    QrGraG1PrMoFrmApres: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AlteraProdutoTabelaPersonalizada(GraTabApp, Nivel1: Integer;
              Nome: String; QueryNivel: TMySQLQuery; PreCadastra: Boolean = False);
    function  InsAltUsoConsumoIts(FatID, FatNum, Empresa, nItem, OriCtrl:
              Integer; GraGruX, Volumes: Integer; PesoVB, PesoVL, ValorItem,
              IPI_pIPI, IPI_vIPI, TotalCusto, TotalPeso: Double; prod_CFOP:
              Integer; SQLType: TSQLType): Boolean;
    function  InsAltUsoConsumoExcluiItem(FatID, FatNum, Empresa, nItem:
              Integer): Boolean;
    function  InsAltUsoConsumoCalculaDiferencas(tPag: TAquemPag; Codigo:
              Integer; TabLctA: String): Double;
    function  InsAltUsoConsumoFinalizaEntrada(Pergunta: Boolean; SQLType:
              TSQLType; Codigo: Integer): Boolean;
    function  PreparaUpdUsoConsumoCab(FatID, FatNUm, Empresa: Integer;
              EdNF_RP, EdNF_CC, EdConhecimento, EdFrete, EdPesoL, EdPesoB:
              TdmkEdit): Boolean;
    function  InsAltUsoConsumoCab(Data, DataE, refNFe: String; UpdCodigo, IQ, CI,
              Transportadora, NF, Conhecimento, Pedido, TipoNF, modNF, Serie,
              NF_RP, NF_CC, HowLoad: Integer; Frete, PesoB, PesoL, ValorNF,
              ICMS: Double; SQLType: TSQLType; FatID, FatNum,
              Empresa: Integer): Boolean;
  end;

var
  DfModAppGraG1: TDfModAppGraG1;

implementation

uses
  DmkDAC_PF, Module,
  GraGru1, GraG1Equi, GraG1Apli, GraG1Veic;
{$R *.dfm}

{ TFmModAppGraG1 }

procedure TDfModAppGraG1.AlteraProdutoTabelaPersonalizada(GraTabApp,
  Nivel1: Integer; Nome: String; QueryNivel: TMySQLQuery;
  PreCadastra: Boolean = False);

  procedure ReabreQuery(Nivel1: Integer);
  begin
    if QueryNivel <> nil then
    begin
      QueryNivel.Close;
      UnDmkDAC_PF.AbreQuery(QueryNivel, Dmod.MyDB);
      QueryNivel.Locate('Nivel1', Nivel1, []);
    end;
  end;

begin
  case GraTabApp of
    CO_COD_GraTabApp_GraG1EqAp: //24001: // Equipamentos de Aplica��o
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraG1EqAp, Dmod.MyDB, [
        'SELECT gg1.UnidMed, gg1.NCM, gg1.Referencia, gga.* ',
        'FROM grag1eqap gga ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=gga.Nivel1 ',
        'WHERE gga.Nivel1=' + Geral.FF0(Nivel1),
        '']);
      if DBCheck.CriaFm(TFmGraG1Equi, FmGraG1Equi, afmoNegarComAviso) then
      begin
        with FmGraG1Equi do
        begin
          FiltraItensAExibir(gbsAplica);
          //
          EdNivel1.ValueVariant     := Nivel1;
          EdNome.ValueVariant       := Nome;
          EdMarca.ValueVariant      := QrGraG1EqApMarca.Value;
          CBMarca.KeyValue          := QrGraG1EqApMarca.Value;
          EdUnidMed.ValueVariant    := QrGraG1EqApUnidMed.Value;
          CBUnidMed.KeyValue        := QrGraG1EqApUnidMed.Value;
          EdNCM.ValueVariant        := QrGraG1EqApNCM.Value;
          EdReferencia.ValueVariant := QrGraG1EqApReferencia.Value;
          //
          MyObjects.DefineTextoRichEdit(ReObservacao, QrGraG1EqApObservacao.Value);
          //
          if PreCadastra then
            BtOKClick(Self)
          else
            ShowModal;
          //
          Destroy;
          //
          ReabreQuery(Nivel1);
        end;
      end;
    end;
    CO_COD_GraTabApp_GraG1EqMo: // 24002: // Equipamentos de Monitoramento
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraG1EqMo, Dmod.MyDB, [
        'SELECT gg1.NCM, gg1.UnidMed, gg1.Referencia, gem.* ',
        'FROM grag1eqmo gem ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=gem.Nivel1 ',
        'WHERE gem.Nivel1=' + Geral.FF0(Nivel1),
        '']);
      if DBCheck.CriaFm(TFmGraG1Equi, FmGraG1Equi, afmoNegarComAviso) then
      begin
        with FmGraG1Equi do
        begin
          FiltraItensAExibir(gbsMonitora);
          //
          EdNivel1.ValueVariant     := Nivel1;
          EdNome.ValueVariant       := Nome;
          EdMarca.ValueVariant      := QrGraG1EqMoMarca.Value;
          CBMarca.KeyValue          := QrGraG1EqMoMarca.Value;
          EdPrgLstCab.ValueVariant  := QrGraG1EqMoPrgLstCab.Value;
          CBPrgLstCab.KeyValue      := QrGraG1EqMoPrgLstCab.Value;
          CkNaoUsaPrdt.Checked      := Geral.IntToBool(QrGraG1EqMoNaoUsaPrdt.Value);
          EdUnidMed.ValueVariant    := QrGraG1EqMoUnidMed.Value;
          CBUnidMed.KeyValue        := QrGraG1EqMoUnidMed.Value;
          EdNCM.ValueVariant        := QrGraG1EqMoNCM.Value;
          EdReferencia.ValueVariant := QrGraG1EqMoReferencia.Value;
          //
          MyObjects.DefineTextoRichEdit(ReObservacao, QrGraG1EqMoObservacao.Value);
          //
          EdVetorArq.Text           := QrGraG1EqMoVetorArq.Value;
          MeVetorSVG.CharCase       := ecNormal;
          MeVetorSVG.Text           := QrGraG1EqMoVetorSVG.Value;
          EdPrgICorSVG.ValueVariant := QrGraG1EqMoPrgICorSVG.Value;
          CBPrgICorSVG.KeyValue     := QrGraG1EqMoPrgICorSVG.Value;
          EdDiasCorSVG.ValueVariant := QrGraG1EqMoDiasCorSVG.Value;
          EdQtCorSVGgg.ValueVariant := QrGraG1EqMoQtCorSVGgg.Value;
          EdQtCorSVGgr.ValueVariant := QrGraG1EqMoQtCorSVGgr.Value;
          EdQtCorSVGrg.ValueVariant := QrGraG1EqMoQtCorSVGrg.Value;
          EdQtCorSVGrr.ValueVariant := QrGraG1EqMoQtCorSVGrr.Value;
          EdQtCorSVGrb.ValueVariant := QrGraG1EqMoQtCorSVGrb.Value;
          EdQtCorSVGbr.ValueVariant := QrGraG1EqMoQtCorSVGbr.Value;
          EdSVGLegenda.ValueVariant := QrGraG1EqMoSVGLegenda.Value;
          EdZumPadr.ValueVariant    := QrGraG1EqMoZumPadr.Value;
          //
          if PreCadastra then
            BtOKClick(Self)
          else
            ShowModal;
          //
          Destroy;
          //
          ReabreQuery(Nivel1);
        end;
      end;
    end;
    CO_COD_GraTabApp_GraG1PrAp: // 24003: // Produtos de Aplica��o
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraG1PrAp, Dmod.MyDB, [
        'SELECT pap.*, gg1.NCM, gg1.UnidMed, gg1.Referencia, med.SIGLA, ',
        'med.CodUsu CU_UnidMed ',
        'FROM grag1prap pap ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pap.Nivel1 ',
        'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
        'WHERE pap.Nivel1=' + Geral.FF0(Nivel1),
        '']);
      if DBCheck.CriaFm(TFmGraG1Apli, FmGraG1Apli, afmoNegarComAviso) then
      begin
        with FmGraG1Apli do
        begin
          FiltraItensAExibir(gbsAplica);
          //
          EdNivel1.ValueVariant       := Nivel1;
          EdNome.ValueVariant         := Nome;
          EdMarca.ValueVariant        := QrGraG1PrApMarca.Value;
          CBMarca.KeyValue            := QrGraG1PrApMarca.Value;
          EdRegMinSaud.Text           := QrGraG1PrApRegMinSaud.Value;
          RGImpInOrca.ItemIndex       := QrGraG1PrApImpInOrca.Value;
          EdFoneCIT.ValueVariant      := QrGraG1PrApFoneCIT.Value;
          EdFoneCEATOX.ValueVariant   := QrGraG1PrApFoneCEATOX.Value;
          EdToxicidade.ValueVariant   := QrGraG1PrApToxicidade.Value;
          EdConcentrac.ValueVariant   := QrGraG1PrApConcentrac.Value;
          EdAcaoToxica.ValueVariant   := QrGraG1PrApAcaoToxica.Value;
          EdAntidoto.ValueVariant     := QrGraG1PrApAntidoto.Value;
          EdNCM.ValueVariant          := QrGraG1PrApNCM.Value;
          EdReferencia.ValueVariant   := QrGraG1PrApReferencia.Value;
          EdFrmApres.ValueVariant     := QrGraG1PrApFrmApres.Value;
          CBFrmApres.KeyValue         := QrGraG1PrApFrmApres.Value;
          //
          MyObjects.DefineTextoRichEdit(ReObservacao, QrGraG1PrApObservacao.Value);
          //
          EdSigla.Text           := QrGraG1PrApSIGLA.Value;
          EdUnidMed.ValueVariant := QrGraG1PrApCU_UnidMed.Value;
          CBUnidMed.KeyValue     := QrGraG1PrApCU_UnidMed.Value;
          EdNCM.ValueVariant     := QrGraG1PrApNCM.Value;
          EdAtuNumLot.Text       := QrGraG1PrApAtuNumLot.Value;
          EdAtuNumLaud.Text      := QrGraG1PrApAtuNumLaud.Value;
          //
          if PreCadastra then
            BtOKClick(Self)
          else
            ShowModal;
          //
          Destroy;
          //
          ReabreQuery(Nivel1);
        end;
      end;
    end;
    CO_COD_GraTabApp_GraG1PrMo: // 24004: // Produtos de Monitoramento
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraG1PrMo, Dmod.MyDB, [
        'SELECT pmo.* , gg1.NCM, gg1.UnidMed, gg1.Referencia, med.SIGLA, ',
        'med.CodUsu CU_UnidMed ',
        'FROM grag1prmo pmo ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pmo.Nivel1 ',
        'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
        'WHERE pmo.Nivel1=' + Geral.FF0(Nivel1),
        '']);
      if DBCheck.CriaFm(TFmGraG1Apli, FmGraG1Apli, afmoNegarComAviso) then
      begin
        with FmGraG1Apli do
        begin
          FiltraItensAExibir(gbsMonitora);
          //
          EdNivel1.ValueVariant       := Nivel1;
          EdNome.ValueVariant         := Nome;
          EdMarca.ValueVariant        := QrGraG1PrMoMarca.Value;
          CBMarca.KeyValue            := QrGraG1PrMoMarca.Value;
          EdRegMinSaud.Text           := QrGraG1PrMoRegMinSaud.Value;
          RGImpInOrca.ItemIndex       := QrGraG1PrMoImpInOrca.Value;
          EdFoneCIT.ValueVariant      := QrGraG1PrMoFoneCIT.Value;
          EdFoneCEATOX.ValueVariant   := QrGraG1PrMoFoneCEATOX.Value;
          EdToxicidade.ValueVariant   := QrGraG1PrMoToxicidade.Value;
          EdConcentrac.ValueVariant   := QrGraG1PrMoConcentrac.Value;
          EdAcaoToxica.ValueVariant   := QrGraG1PrMoAcaoToxica.Value;
          EdAntidoto.ValueVariant     := QrGraG1PrMoAntidoto.Value;
          EdValCliDd.ValueVariant     := QrGraG1PrMoValCliDd.Value;
          EdNCM.ValueVariant          := QrGraG1PrMoNCM.Value;
          EdReferencia.ValueVariant   := QrGraG1PrMoReferencia.Value;
          EdFrmApres.ValueVariant     := QrGraG1PrMoFrmApres.Value;
          CBFrmApres.KeyValue         := QrGraG1PrMoFrmApres.Value;
          //
          MyObjects.DefineTextoRichEdit(ReObservacao, QrGraG1PrMoObservacao.Value);
          //
          EdSigla.Text           := QrGraG1PrMoSIGLA.Value;
          EdUnidMed.ValueVariant := QrGraG1PrMoCU_UnidMed.Value;
          CBUnidMed.KeyValue     := QrGraG1PrMoCU_UnidMed.Value;
          EdNCM.ValueVariant     := QrGraG1PrMoNCM.Value;
          EdAtuNumLot.Text       := QrGraG1PrMoAtuNumLot.Value;
          EdAtuNumLaud.Text      := QrGraG1PrMoAtuNumLaud.Value;
          //
          if PreCadastra then
            BtOKClick(Self)
          else
            ShowModal;
          //
          Destroy;
          //
          ReabreQuery(Nivel1);
        end;
      end;
    end;
    CO_COD_GraTabApp_GraG1Veic: // 24005: // Ve�culos
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraG1Veic, Dmod.MyDB, [
        //'SELECT pmo.* , gg1.UnidMed, med.SIGLA, ',
        //'med.CodUsu CU_UnidMed ',
        'SELECT gg1.UnidMed, gg1.NCM, gg1.Referencia, pmo.* ',
        'FROM GraG1Veic pmo ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pmo.Nivel1 ',
        'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
        'WHERE pmo.Nivel1=' + Geral.FF0(Nivel1),
        '']);
      if DBCheck.CriaFm(TFmGraG1Veic, FmGraG1Veic, afmoNegarComAviso) then
      begin
        with FmGraG1Veic do
        begin
          FiltraItensAExibir(gbsIndef);
          //
          EdNivel1.ValueVariant     := Nivel1;
          EdNome.ValueVariant       := Nome;
          EdMarca.ValueVariant      := QrGraG1VeicMarca.Value;
          CBMarca.KeyValue          := QrGraG1VeicMarca.Value;
          EdUnidMed.ValueVariant    := QrGraG1VeicUnidMed.Value;
          CBUnidMed.KeyValue        := QrGraG1VeicUnidMed.Value;
          EdNCM.ValueVariant        := QrGraG1VeicNCM.Value;
          EdPlaca.Text              := QrGraG1VeicPlaca.Value;
          EdCustoKm.ValueVariant    := QrGraG1VeicCustoKm.Value;
          EdReferencia.ValueVariant := QrGraG1VeicReferencia.Value;
          //
          if PreCadastra then
            BtOKClick(Self)
          else
            ShowModal;
          //
          Destroy;
          //
          ReabreQuery(Nivel1);
        end;
      end;
    end;
    else
    Geral.MB_ERRO('Janela de cadastro de Produto n�o implementada: ' +
    Geral.FF0(GraTabApp) +
    sListaGraTabAppTxt[Integer(sListaGraTabAppCod[GraTabApp])]);
  end;
end;

function TDfModAppGraG1.InsAltUsoConsumoCab(Data, DataE, refNFe: String;
  UpdCodigo, IQ, CI, Transportadora, NF, Conhecimento, Pedido, TipoNF, modNF,
  Serie, NF_RP, NF_CC, HowLoad: Integer; Frete, PesoB, PesoL, ValorNF,
  ICMS: Double; SQLType: TSQLType; FatID, FatNum, Empresa: Integer): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoCalculaDiferencas(tPag: TAquemPag;
  Codigo: Integer; TabLctA: String): Double;
begin
  Result := 0;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoExcluiItem(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoFinalizaEntrada(Pergunta: Boolean;
  SQLType: TSQLType; Codigo: Integer): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoIts(FatID, FatNum, Empresa, nItem,
  OriCtrl, GraGruX, Volumes: Integer; PesoVB, PesoVL, ValorItem, IPI_pIPI,
  IPI_vIPI, TotalCusto, TotalPeso: Double; prod_CFOP: Integer;
  SQLType: TSQLType): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.PreparaUpdUsoConsumoCab(FatID, FatNUm, Empresa: Integer;
  EdNF_RP, EdNF_CC, EdConhecimento, EdFrete, EdPesoL,
  EdPesoB: TdmkEdit): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

end.
