unit OSPos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, dmkRadioGroup,
  UnAppListas, UnDmkEnums, UnProjGroup_Consts;

type
  TFmOSPos = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    Label7: TLabel;
    GBEdita: TGroupBox;
    Label1: TLabel;
    Label9: TLabel;
    EdControle: TdmkEdit;
    EdDias: TdmkEdit;
    QrPreEmail: TmySQLQuery;
    QrPreEmailCodigo: TIntegerField;
    QrPreEmailNome: TWideStringField;
    DsPreEmail: TDataSource;
    RGAplicacao: TdmkRadioGroup;
    Label2: TLabel;
    EdAplicID: TdmkEditCB;
    CBAplicID: TdmkDBLookupComboBox;
    SBAplicID: TSpeedButton;
    CkContinuar: TCheckBox;
    EdCodigo: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBAplicIDClick(Sender: TObject);
  private
    { Private declarations }
    //F_OS_Alv_: String;
    //

  public
    { Public declarations }
    //FQrOSPOs: TmySQLQuery;

  end;

  var
  FmOSPos: TFmOSPos;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral,
  (*OSCab, ModOS, *)Principal, UnOSApp_PF, UnBugs_Tabs;

{$R *.DFM}

procedure TFmOSPos.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Dias, Aplicacao, AplicID: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Dias           := EdDias.ValueVariant;
  //
  Aplicacao      := RGAplicacao.ItemIndex;
  AplicID        := EdAplicID.ValueVariant;
  //
  if Aplicacao = CO_OS_POS_APLICACAO_002_ENVMAIL(*2*) then
    if MyObjects.FIC(AplicID = 0, EdAplicID, 'Informe o Pr� Email!') then
      Exit;

  Controle := EdControle.ValueVariant;
  Controle := UMyMod.BPGS1I32('ospos', 'Controle', '', '',
    tsDef, ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ospos', False, [
  'Codigo', 'Dias', 'Aplicacao',
  'AplicID'], [
  'Controle'], [
  Codigo, Dias, Aplicacao,
  AplicID], [
  Controle], True) then
  begin
    //if FQrOSPOs <> nil then
    //OSApp_PF.ReopenOSPOs(FQrOSPOs, Codigo, Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdControle.ValueVariant := 0;
      EdAplicID.ValueVariant   := 0;
      CBAplicID.KeyValue       := Null;
      RGAplicacao.ItemIndex    := CO_OS_POS_APLICACAO_000_NENHUMA(*0*);
      EdDias.ValueVariant := 0;
      EdDias.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmOSPos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSPos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGAplicacao, sListaAcaoPosVda, 3, 0);
  //
  UnDmkDAC_PF.AbreQuery(QrPreEmail, Dmod.MyDB);
end;

procedure TFmOSPos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPos.SBAplicIDClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormPreEmail(EdAplicID.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdAplicID, CBAplicID, QrPreEmail, VAR_CADASTRO);
end;

end.
