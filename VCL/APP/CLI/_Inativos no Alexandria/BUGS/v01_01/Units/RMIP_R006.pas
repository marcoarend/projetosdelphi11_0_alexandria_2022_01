unit RMIP_R006;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Data.DB, mySQLDbTables, dmkGeral, UnDmkEnums, frxClass,
  frxDBSet, frxChart, UnProjGroup_Consts, UnInternalConsts, UnMyObjects,
  UnDmkProcFunc, VCLTee.Chart, VCLTee.Series;

type
  TFmRMIP_R006 = class(TForm)
    Qr006A_PrgLstIts: TmySQLQuery;
    frxReport006A: TfrxReport;
    Qr006A_PrgLstItsPrgLstCab: TIntegerField;
    Qr006A_PrgLstItsPrgLstIts: TIntegerField;
    Qr006A_PrgLstItsNO_LST: TWideStringField;
    Qr006A_PrgLstItsSigla: TWideStringField;
    Qr006A_PrgLstItsNome: TWideStringField;
    Qr006A_PrgLstItsFuncoes: TSmallintField;
    Qr006Grupos: TmySQLQuery;
    Qr006NResp: TmySQLQuery;
    IntegerField1: TIntegerField;
    Qr006XVals: TmySQLQuery;
    Qr006Itens: TmySQLQuery;
    Qr006ItensITENS: TFloatField;
    Qr006ItensDtaExeFim: TDateField;
    Qr006XValsDtaExeFim: TDateField;
    Qr006XValsDIAS: TIntegerField;
    Qr006ItensDIAS: TIntegerField;
    Qr006Dados: TmySQLQuery;
    Qr006DadosCorPizza: TIntegerField;
    Qr006GruposResposta: TWideStringField;
    Qr006A_OSs: TmySQLQuery;
    Qr006A_OSsCodigo: TIntegerField;
    Qr006A_OSsEntidade: TIntegerField;
    Qr006A_OSsSiapTerCad: TIntegerField;
    Qr006A_OSsEstatus: TIntegerField;
    Qr006A_OSsDtaExeFim: TDateTimeField;
    Qr006A_OSsNumContrat: TIntegerField;
    Qr006A_OSsGrupo: TIntegerField;
    Qr006A_OSsNO_FatoGeradr: TWideStringField;
    Qr006A_OSsNO_ESTATUS: TWideStringField;
    Qr006A_OSsNO_SiapTerCad: TWideStringField;
    Qr006A_OSsNO_ENT: TWideStringField;
    frxDs006A_OSs: TfrxDBDataset;
    frxDs006A_PrgLstIts: TfrxDBDataset;
    Qr006SubGrupos: TmySQLQuery;
    Qr006SubGruposDependenci: TIntegerField;
    Qr006DadosNO_DEPENDENCI: TWideStringField;
    frxDs006Grupos: TfrxDBDataset;
    procedure FormCreate(Sender: TObject);
    procedure Qr006A_PrgLstItsAfterScroll(DataSet: TDataSet);
    procedure frxReport006AGetValue(const VarName: string; var Value: Variant);
    procedure Qr006SubGruposAfterOpen(DataSet: TDataSet);
    procedure Qr006GruposAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    F_RMIP_006Atr: String;
    //
    function  ObtemNomeArr(Nome: String; Index: Integer): String;
    procedure Reopen004A_PrgLstIts();
    procedure R006_GeraDados(PrgLstIts, Cliente: Integer; DtaIni, DtaFim:
              TDateTime; Funcoes: Integer);
  public
    { Public declarations }
    FCliente, FAplicacao: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL, FAplic_Tit: String;
    FItemRMIP: TItensRMIP;
    //
    procedure GeraImp_LinhaEvolucaoHistorico((*FmRMIP: TForm;*));
    procedure frxReport000GetValue(frxReport: TfrxReport; const VarName: string; var Value: Variant);
  end;

var
  FmRMIP_R006: TFmRMIP_R006;

implementation

uses Module, ModuleRMIP, DmkDAC_PF, ModuleGeral, CreateBugs;

var
  FSeries: Integer = 0;
  FYCount: Integer = 0;
  FNResp: Boolean;
  FXSource: String;
  FXValues: array of String;
  FYValues: array of array of String;
  FTitles: array of String;
  FCores: array of TColor;

{$R *.dfm}

procedure TFmRMIP_R006.FormCreate(Sender: TObject);
var
  I: Integer;
  Chart1: TfrxChartView;
begin
  F_RMIP_006Atr := UnCreateBugs.RecriaTempTableNovo(ntrtt_RMIP_006Atr,
    DmodG.QrUpdPID1, False);
  //
  //////////// Saber (registrar) as mudancas para outros gr�ficos
  Chart1 := frxReport006A.FindObject('Chart006A') as TfrxChartView;
  Chart1.Chart.LeftAxis.Axis.Color := clSilver;
  Chart1.Chart.LeftAxis.Axis.Width := 1;
  Chart1.Chart.LeftAxis.Grid.Color := clSilver;
  Chart1.Chart.LeftAxis.Grid.Width := 1;
  Chart1.Chart.LeftAxis.Grid.SmallDots := True;
  Chart1.Chart.BottomAxis.Axis.Color := clSilver;
  Chart1.Chart.BottomAxis.Axis.Width := 1;
  Chart1.Chart.BottomAxis.Grid.Color := clSilver;
  Chart1.Chart.BottomAxis.Grid.Width := 1;
  Chart1.Chart.BottomAxis.Grid.SmallDots := True;
  Chart1.Chart.Legend.LegendStyle := lsSeries;  // VCLTee.Chart
  // FIM Chart 1
end;

procedure TFmRMIP_R006.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
var
  Chart1: TfrxChartView;
  //
  YSource: String;
  I, J: Integer;
begin
  if VarName = 'VAR_LINE_TEE_006' then
  begin
    Value := True;
    //
    Chart1 := frxReport.FindObject('Chart006A') as TfrxChartView;
    while Chart1.SeriesData.Count > 0 do
      Chart1.SeriesData[0].Free;
    while Chart1.Chart.SeriesCount > 0 do
      Chart1.Chart.Series[0].Free;
    //
    for I := 0 to FSeries - 1 do
    begin
      YSource := '';
      for J := 0 to FYCount - 1 do
      begin
        YSource := YSource + ';' + FYValues[I][J];
      end;
      if Length(YSource) > 0 then
        YSource := Copy(YSource, 2);

      Chart1.AddSeries(TfrxChartSeries.csLine);

      // do VCLTee.Chart.CustomChart:
      Chart1.Chart.Series[I].Pen.Width := 4;
      Chart1.Chart.Series[I].LegendTitle := FTitles[I];
      //Chart1.Chart.Series[I].Color := FCores[I];

      // do frxChat
      Chart1.SeriesData[I].DataType := dtFixedData;
      Chart1.SeriesData[I].XSource := FXSource; //'value1; value2; value3';
      Chart1.SeriesData[I].YSource := YSource; //'31.5;28.54;31.58';
      Chart1.SeriesData[I].SortOrder := soNone;
      Chart1.SeriesData[I].TopN := 0;
      Chart1.SeriesData[I].XType := xtText;
    end;
  end;
end;

procedure TFmRMIP_R006.frxReport006AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport006A, VarName, Value);
end;

procedure TFmRMIP_R006.GeraImp_LinhaEvolucaoHistorico();
const
  OSCab = 0;
  Sim = 1;
  //
var
  Aviso: String;
  I, J, PrgLstIts: Integer;
  Qryes: array of TmySQLQuery;
  //Chart1: TfrxChartView;
  //
begin
  Qr006A_PrgLstIts.Close;
  Aviso := ' ';
  Reopen004A_PrgLstIts();
  //
  if Qr006A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso :=
      'Question�rios dos monitoramentos pesquisados sem perguntas geradoras do gr�fico "' +
      FAplic_Tit + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Per�odo: ' + Geral.FDT(FDtaIni, 3) + ' a ' + Geral.FDT(FDtaFim, 2);
  //
  frxReport006A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport006A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport006A.Variables['VARF_DATA']    := FDtaImp;
  frxReport006A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport006A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  //
  MyObjects.frxDefineDataSets(frxReport006A, [
  frxDs006A_PrgLstIts,
  frxDs006A_OSs,
  frxDs006Grupos
  ]);
  //MyObjects.frxPrepara(frxReport006A, FAplic_Tit);
end;

function TFmRMIP_R006.ObtemNomeArr(Nome: String; Index: Integer): String;
begin
  Result := Nome + FormatFloat('00', Index);
end;

procedure TFmRMIP_R006.Qr006SubGruposAfterOpen(DataSet: TDataSet);
  procedure DefineDadosInformados(Item: Integer);
  begin
    Qr006Itens.First;
    while not Qr006Itens.Eof do
    begin
      if Qr006XVals.Locate('DIAS', Qr006ItensDIAS.Value, []) then
      begin
        FYValues[Item][Qr006XVals.RecNo - 1] := Geral.FFI(Qr006ItensITENS.Value);
      end else
      begin
        raise Exception.Create('Erro: Data n�o localizada (1)!');
      end;
      //
      Qr006Itens.Next;
    end;
  end;
var
  I, J, Item: Integer;
begin
  // Manter TODAS datas!
  UnDmkDAC_PF.AbreMySQLQuery0(Qr006XVals, DModG.MyPID_DB, [
  'SELECT DISTINCT DtaExeFim, ',
  'TO_DAYS(DtaExeFim) DIAS ',
  'FROM ' + F_RMIP_006Atr,
  'ORDER BY DtaExeFim ',
  '']);
  FYCount := Qr006XVals.RecordCount;
  SetLength(FXValues, FYCount);
  Qr006XVals.First;
  FXSource := '';
  while not Qr006XVals.Eof do
  begin
    FXSource := FXSource + ';' + Geral.FDT(Qr006XValsDtaExeFim.Value, 3);
    FXValues[Qr006XVals.RecNo - 1] := Geral.FDT(Qr006XValsDtaExeFim.Value, 3);
    //
    Qr006XVals.Next;
  end;
  if Length(FXSource) > 1 then
    FXSource := Copy(FXSource, 2);
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrNResp, DModG.MyPID_DB, [
  'SELECT RespAtrIts ',
  'FROM ' + F_RMIP_006Atr,
  'WHERE Respondido = 0 ',
  //'AND Resposta="' + Qr006Grupos?Resposta.Value + '" ',
  '']);
  FNResp := QrNResp.RecordCount > 0;
  if FNResp then
    FSeries := 1
  else*)
    FSeries := 0;
  FSeries := FSeries + Qr006SubGrupos.RecordCount;
  SetLength(FYValues, FSeries);
  SetLength(FTitles, FSeries);
  SetLength(FCores, FSeries);
  for I := 0 to FSeries - 1 do
  begin
    SetLength(FYValues[I], FYCount);
    for J := 0 to FYCount - 1 do
      FYValues[I][J] := '0';
  end;
  Item := -1;
///////////////////////// Nao respondidos
(*
  if FNResp then
  begin
    Item := Item + 1;
    FTitles[Item] := CO_NAO_RESPONDIDO;
    FCores[Item]  := 8421504; // Cinza
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr006Itens, DModG.MyPID_DB, [
    'SELECT DtaExeFim, SUM(ITENS) + 0.000 ITENS, ',
    'TO_DAYS(DtaExeFim) DIAS ',
    'FROM ' + F_RMIP_006Atr,
    'WHERE Respondido = 0 ',
    'GROUP BY DtaExeFim ',
    'ORDER BY DtaExeFim ',
    '']);
    DefineDadosInformados(Item);
  end;
*)
///////////////////////// Respondidos
  Qr006SubGrupos.First;
  while not Qr006SubGrupos.Eof do
  begin
    Item := Item +1;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qr006Dados, DmodG.MyPID_DB, [
    'SELECT NO_DEPENDENCI, CorPizza ',
    'FROM ' + F_RMIP_006Atr,
    'WHERE Respondido = 1 ',
    'AND Resposta="' + Qr006GruposResposta.Value + '" ',
    'AND Dependenci=' + Geral.FF0(Qr006SubGruposDependenci.Value),
    '']);

    FTitles[Item] := Qr006DadosNO_DEPENDENCI.Value;
    FCores[Item] := Qr006DadosCorPizza.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr006Itens, DModG.MyPID_DB, [
    'SELECT DtaExeFim, SUM(ITENS) + 0.000 ITENS, ',
    'TO_DAYS(DtaExeFim) DIAS ',
    'FROM ' + F_RMIP_006Atr,
    'WHERE Respondido = 1 ',
    'AND Resposta="' + Qr006GruposResposta.Value + '" ',
    'AND Dependenci=' + Geral.FF0(Qr006SubGruposDependenci.Value),
    'GROUP BY DtaExeFim ',
    'ORDER BY DtaExeFim ',
    '']);
    DefineDadosInformados(Item);
    //
    Qr006SubGrupos.Next;
  end;
end;

procedure TFmRMIP_R006.Qr006A_PrgLstItsAfterScroll(DataSet: TDataSet);
var
  PrgLstIts, Funcoes: Integer;
begin
  if Qr006A_PrgLstIts.RecordCount > 0 then
  begin
    PrgLstIts := Qr006A_PrgLstIts.FieldByName('PrgLstIts').AsInteger;
    Funcoes   := Qr006A_PrgLstIts.FieldByName('Funcoes').AsInteger;
    R006_GeraDados(PrgLstIts, FCliente, FDtaIni, FDtaFim, Funcoes);
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qr006Grupos, DmodG.MyPID_DB, [
    'SELECT DISTINCT Resposta ',
    'FROM ' + F_RMIP_006Atr,
    //'WHERE Respondido=' + Geral.FF0(Sim),
    '']);
  end else
  begin
    Qr006Grupos.Close;
    // For�ar erro se o FR tentar abrir
    Qr006Grupos.SQL.Clear;
  end;
end;

procedure TFmRMIP_R006.Qr006GruposAfterScroll(DataSet: TDataSet);
const
  Sim = 1;
begin
  UnDMkDAC_PF.AbreMySQLQuery0(Qr006SubGrupos, DmodG.MyPID_DB, [
  'SELECT DISTINCT Dependenci ',
  'FROM ' + F_RMIP_006Atr,
  'WHERE Respondido=' + Geral.FF0(Sim),
  'AND Resposta="' + Qr006GruposResposta.Value + '" ',
  'ORDER BY NO_DEPENDENCI ',
  '']);
end;

procedure TFmRMIP_R006.R006_GeraDados(PrgLstIts, Cliente: Integer; DtaIni,
  DtaFim: TDateTime; Funcoes: Integer);
var
  SQL_Exec, SQL_Extra, SQL_Qtde, SQLPadrao, FldAgrup,
  Resposta, CorPizza, LeftJoin, GroupBy, OrderBy: String;
begin
  if Cliente <> 0 then
    SQL_Extra := 'AND cab.Entidade=' + Geral.FF0(Cliente)
  else
    SQL_Extra := '';
  //
  SQL_Extra := Geral.ATS([
    SQL_Extra,
    dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True)
  ]);
  if not DmRMIP.SQL_Funcoes(PrgLstIts, Funcoes, Resposta, CorPizza, LeftJoin,
  OrderBy, GroupBy, False) then
    Exit;
  //
  SQL_Exec := Geral.ATS([
  'DELETE FROM ' + F_RMIP_006Atr + '; ',
  'INSERT INTO ' + F_RMIP_006Atr,
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  Resposta,
  CorPizza,
  ', pip.Codigo, pip.Dependenci, sid.Dependenci, dep.Nome ',
  ', 1 Ativo ',
  LeftJoin,
  'LEFT JOIN ' + TMeuDB + '.ospipmon mon ON mon.Controle=opi.Controle ',
  'LEFT JOIN ' + TMeuDB + '.pipcad pip ON pip.Codigo=mon.PipCad ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=pip.Dependenci ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dep ON dep.Codigo=sid.Dependenci ',
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  GroupBy + ', sid.Dependenci',
  '']);
  //if Funcoes = CO_PRG_LST_BINARIO then
    //Geral.MB_Info(SQL_Exec);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL_Exec);

  // OSs afetadas:
  DmRMIP.ReopenOSsAfetadas(Qr006A_OSs, PrgLstIts, LeftJoin, SQL_Extra);
end;

procedure TFmRMIP_R006.Reopen004A_PrgLstIts;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr006A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(FAplicacao),
  dmkPF.SQL_Periodo('AND cab.DtaExeFim', FDtaIni, FDtaFim, True, True),
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
end;

end.
