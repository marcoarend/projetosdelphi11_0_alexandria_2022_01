unit OSFrmFlhDd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  UndmkProcFunc, dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmOSFrmFlhDd = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdIDIt2: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdOrdem: TdmkEdit;
    Label1: TLabel;
    EdDias: TdmkEdit;
    DBEdControle: TdmkDBEdit;
    Label2: TLabel;
    DBEdConta: TdmkDBEdit;
    Label4: TLabel;
    Label8: TLabel;
    DBEdIDIts: TdmkDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenOSFrmFlhDd(IDIt2: Integer);
    function  ProximaOrdem(Codigo, Controle, Conta, IDIts: Integer): Integer;
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOSFrmFlhDd: TFmOSFrmFlhDd;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
Formulas;

{$R *.DFM}

procedure TFmOSFrmFlhDd.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, IDIts, IDIt2, Ordem, Dias: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := Geral.IMV(DBEdConta.Text);
  IDIts          := Geral.IMV(DBEdIDIts.Text);
  IDIt2          := EdIDIt2.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  Dias           := EdDias.ValueVariant;
  //
  if MyObjects.FIC(Dias < 1, EdDias, 'Informe o intervalo em dias!') then
    Exit;
  IDIt2 := UMyMod.BPGS1I32('osfrmflhdd', 'IDIt2', '', '', tsPos, ImgTipo.SQLType, IDIt2);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osfrmflhdd', False, [
  'Codigo', 'Controle', 'Conta',
  'IDIts', 'Ordem', 'Dias'], [
  'IDIt2'], [
  Codigo, Controle, Conta,
  IDIts, Ordem, Dias], [
  IDIt2], True) then
  begin
    ReopenOSFrmFlhDd(IDIt2);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdIDIt2.ValueVariant     := 0;
      EdOrdem.ValueVariant     := ProximaOrdem(Codigo, Controle, Conta, IDIts);
      EdDias.ValueVariant      := 0;
      //
      EdOrdem.SetFocus;
    end else Close;
  end;
end;

procedure TFmOSFrmFlhDd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSFrmFlhDd.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCab;
  DBEdControle.DataSource := FDsCab;
  DBEdConta.DataSource    := FDsCab;
  DBEdIDIts.DataSource    := FDsCab;
  DBEdNome.DataSource     := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmOSFrmFlhDd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSFrmFlhDd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmOSFrmFlhDd.ProximaOrdem(Codigo, Controle, Conta, IDIts: Integer): Integer;
const
  Incremento = 1;
  Base       = 0;
begin
  Result := UnDmkDAC_PF.ObtemProximaOrdem(Dmod.MyDB, 'osfrmflhdd', 'Ordem',
              vpLast, dmktfInteger, Incremento, Base, ['Codigo', 'Controle',
              'Conta', 'IDITs'], [Codigo, Controle, Conta, IDIts]);
end;

procedure TFmOSFrmFlhDd.ReopenOSFrmFlhDd(IDIt2: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('IDIts').AsInteger;
    FQrIts.Open;
    //
    if IDIt2 <> 0 then
      FQrIts.Locate('IDIt2', IDIt2, []);
  end;
end;

end.
