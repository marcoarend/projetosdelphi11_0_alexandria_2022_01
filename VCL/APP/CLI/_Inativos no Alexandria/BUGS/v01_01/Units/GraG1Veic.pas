unit GraG1Veic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkRichEdit, UnBugs_Tabs, UnDmkEnums, dmkCheckBox, Variants,
  dmkValUsu;

type
  TFmGraG1Veic = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrMarcas: TmySQLQuery;
    QrMarcasControle: TIntegerField;
    QrMarcasNO_MARCA_FABR: TWideStringField;
    DsMarcas: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    SbMarcas: TSpeedButton;
    EdNivel1: TdmkEdit;
    EdNome: TdmkEdit;
    EdMarca: TdmkEditCB;
    CBMarca: TdmkDBLookupComboBox;
    GBOutros: TGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    EdPlaca: TdmkEdit;
    EdCustoKm: TdmkEdit;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    VUUnidMed: TdmkValUsu;
    CBUnidMed: TdmkDBLookupComboBox;
    SBUnidMed: TSpeedButton;
    EdSigla: TdmkEdit;
    EdUnidMed: TdmkEditCB;
    Label6: TLabel;
    EdNCM: TdmkEdit;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    Label17: TLabel;
    EdReferencia: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbMarcasClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNCMExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //FFrmNome,
    FTabNome: String;
    //FGraBugs: TGraBugsServi;
    //
    procedure FiltraItensAExibir(GraBugs: TGraBugsServi);

  end;

  var
  FmGraG1Veic: TFmGraG1Veic;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, DmkDAC_PF, UnDmkProcFunc,
  UnUMedi_PF, ModProd, UnMySQLCuringa, UnGrade_Jan;

{$R *.DFM}


procedure TFmGraG1Veic.BtOKClick(Sender: TObject);

  function InsUpdG1PrAp(SQLType: TSQLType): Boolean;
  var
    Placa: String;
    CustoKm: Double;
    Nivel1, Marca: Integer;
    SQLCampos: TdmkArrStr;
    ValCampos: TdmkArrVar;
  begin
    Nivel1         := EdNivel1.ValueVariant;
    Marca          := EdMarca.ValueVariant;
    Placa          := EdPlaca.ValueVariant;
    CustoKm        := EdCustoKm.ValueVariant;

    //
    SQLCampos := Geral.ATA_Str(['Placa', 'CustoKm', 'Marca']);
    ValCampos := Geral.ATA_Var([Placa ,  CustoKm ,  Marca]);
(*
    case FGraBugs of
      gbsAplica  : ; // nada
      gbsMonitora:
      begin
        // Somente Monitoramento
        PrgLstCab  := EdPrgLstCab.ValueVariant;
        NaoUsaPrdt := Geral.BoolToInt(CkNaoUsaPrdt.Checked);
        SQLCampos := Geral.ASA_Str(SQLCampos, ['PrgLstCab', 'NaoUsaPrdt']);
        ValCampos := Geral.ASA_Var(ValCampos, [ PrgLstCab, NaoUsaPrdt ]);
      end;
    end;
*)
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTabNome, False,
                SQLCampos, ['Nivel1'], ValCampos, [Nivel1], True);
  end;
var
  Nivel1, UnidMed: Integer;
  Nome, NCM, Referencia: String;
  SQLType: TSQLType;
begin
  Nome       := EdNome.ValueVariant;
  UnidMed    := VUUnidMed.ValueVariant;
  NCM        := EdNCM.ValueVariant;
  Referencia := EdReferencia.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Nivel1 := EdNivel1.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False,
    ['Nome', 'UnidMed', 'NCM', 'Referencia'], ['Nivel1'],
    [Nome, UnidMed, NCM, Referencia], [Nivel1], True) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Nivel1 ',
      'FROM ' + FTabNome,
      'WHERE Nivel1=' + Geral.FF0(Nivel1),
      '']);
    if Nivel1 = Dmod.QrAux.FieldByName('Nivel1').AsInteger then
      SQLType := stUpd
    else
      SQLType := stIns;
    if InsUpdG1PrAp(SQLType) then
    begin
      {
      LocCod(Nivel1,Nivel1);
      ImgTipo.SQLType := stlok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
      }
      Close;
    end;
  end;
end;

procedure TFmGraG1Veic.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraG1Veic.CBUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraG1Veic.EdNCMExit(Sender: TObject);
begin
  EdNCM.Text := Geral.FormataNCM(EdNCM.Text);
end;

procedure TFmGraG1Veic.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraG1Veic.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraG1Veic.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmGraG1Veic.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmGraG1Veic.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraG1Veic.FiltraItensAExibir(GraBugs: TGraBugsServi);
begin
  FTabNome := 'grag1veic';
(*
  FGraBugs := GraBugs;
  case GraBugs of
    gbsAplica  : FTabNome := 'grag1eqap';
    gbsMonitora: FTabNome := 'grag1eqmo';
    else FTabNome := 'grag1eq??';
  end;
  //
  case GraBugs of
    gbsAplica  : FFrmNome := 'Aplica��o';
    gbsMonitora: FFrmNome := 'Monitoramento';
    else FFrmNome := '? ? ? ? ?';
  end;
*)
end;

procedure TFmGraG1Veic.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraG1Veic.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrMarcas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmGraG1Veic.FormResize(Sender: TObject);
begin
(*
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], 'PRD-GRUPO-025 :: Equipamentos de ' + FFrmNome, True, taCenter, 2, 10, 20);
*)
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraG1Veic.SbMarcasClick(Sender: TObject);
var
  Marca: Integer;
begin
  VAR_CADASTRO2 := 0;
  Marca         := EdMarca.ValueVariant;

  FmPrincipal.MostraFormGraFabCad(Marca);

  if VAR_CADASTRO2 <> 0 then
    UMyMod.SetaCodigoPesquisado(EdMarca, CBMarca, QrMarcas, VAR_CADASTRO2, 'Controle');
end;

procedure TFmGraG1Veic.SBUnidMedClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := VUUnidMed.ValueVariant;
  VAR_CADASTRO := 0;
  //
  UMedi_PF.MostraUnidMed(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    CBUnidMed.SetFocus;
  end;
end;

procedure TFmGraG1Veic.SpeedButton1Click(Sender: TObject);
var
  NCMSel: String;
begin
  VAR_CADASTRO := 0;
  NCMSel       := Grade_Jan.MostraFormClasFisc();
  //
  if NCMSel <> '' then
    EdNCM.Texto := NCMSel;
end;

end.
