object FmOSPipIts: TFmOSPipIts
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-032 :: Respostas de Perguntas de Monitoramento'
  ClientHeight = 629
  ClientWidth = 876
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 876
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 828
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 780
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 525
        Height = 32
        Caption = 'Respostas de Perguntas de Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 525
        Height = 32
        Caption = 'Respostas de Perguntas de Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 525
        Height = 32
        Caption = 'Respostas de Perguntas de Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 876
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 876
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 876
        Height = 467
        Align = alClient
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 223
          Height = 450
          Align = alLeft
          DataSource = DsOSPipIts
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_PIP'
              Title.Caption = 'PMV'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SubOrdem'
              Width = 40
              Visible = True
            end>
        end
        object CategoryPanelGroup1: TCategoryPanelGroup
          Left = 225
          Top = 15
          Width = 649
          Height = 450
          VertScrollBar.Tracking = True
          Align = alClient
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clWindowText
          HeaderFont.Height = -11
          HeaderFont.Name = 'Tahoma'
          HeaderFont.Style = []
          TabOrder = 1
          object CategoryPanel6: TCategoryPanel
            Top = 453
            Height = 30
            Caption = 'CategoryPanel6'
            Collapsed = True
            TabOrder = 0
          end
          object CategoryPanel5: TCategoryPanel
            Top = 423
            Height = 30
            Caption = 'Texto Livre'
            Collapsed = True
            TabOrder = 1
            ExpandedHeight = 269
            object MeRespTxtLvr: TMemo
              Left = 0
              Top = 0
              Width = 628
              Height = 195
              Align = alClient
              TabOrder = 0
            end
            object Panel6: TPanel
              Left = 0
              Top = -48
              Width = 628
              Height = 48
              Align = alBottom
              TabOrder = 1
              object BtOK_5: TBitBtn
                Tag = 14
                Left = 4
                Top = 4
                Width = 120
                Height = 40
                Caption = '&OK'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtOK_5Click
              end
            end
          end
          object CategoryPanel4: TCategoryPanel
            Top = 393
            Height = 30
            Caption = 'Uso de Atributo'
            Collapsed = True
            TabOrder = 2
            ExpandedHeight = 267
            object CategoryPanelGroup3: TCategoryPanelGroup
              Left = 0
              Top = 0
              Width = 628
              Height = 0
              VertScrollBar.Tracking = True
              Align = alClient
              HeaderFont.Charset = DEFAULT_CHARSET
              HeaderFont.Color = clWindowText
              HeaderFont.Height = -11
              HeaderFont.Name = 'Tahoma'
              HeaderFont.Style = []
              TabOrder = 0
              object CategoryPanel4_2: TCategoryPanel
                Top = 30
                Height = 30
                Caption = 'Item de Atributo (texto  livre)'
                Collapsed = True
                TabOrder = 0
                object Panel5: TPanel
                  Left = 0
                  Top = -48
                  Width = 624
                  Height = 48
                  Align = alBottom
                  TabOrder = 0
                  object BtOK_4_2: TBitBtn
                    Tag = 14
                    Left = 4
                    Top = 4
                    Width = 120
                    Height = 40
                    Caption = '&OK'
                    NumGlyphs = 2
                    TabOrder = 0
                    OnClick = BtOK_4_2Click
                  end
                end
                object MeRespAtrTxt: TMemo
                  Left = 0
                  Top = 0
                  Width = 624
                  Height = 126
                  Align = alClient
                  TabOrder = 1
                end
              end
              object CategoryPanel4_1: TCategoryPanel
                Top = 0
                Height = 30
                Caption = 'Item de atributo (pr'#233'-definido)'
                Collapsed = True
                TabOrder = 1
                ExpandedHeight = 181
                object LaRespAtrIts: TLabel
                  Left = 8
                  Top = 9
                  Width = 76
                  Height = 13
                  Caption = 'Item do atributo:'
                end
                object BtOK_4_1: TBitBtn
                  Tag = 14
                  Left = 8
                  Top = 100
                  Width = 120
                  Height = 40
                  Caption = '&OK'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtOK_4_1Click
                end
                object EdRespAtrIts: TdmkEditCB
                  Left = 8
                  Top = 25
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdCampo = 'Embalagem'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBRespAtrIts
                  IgnoraDBLookupComboBox = False
                end
                object CBRespAtrIts: TdmkDBLookupComboBox
                  Left = 64
                  Top = 25
                  Width = 485
                  Height = 21
                  KeyField = 'CodUsu'
                  ListField = 'NOme'
                  ListSource = DsPrgAtrIts
                  TabOrder = 2
                  dmkEditCB = EdRespAtrIts
                  UpdType = utYes
                  LocF7CodiFldName = 'Nivel1'
                  LocF7NameFldName = 'Nome'
                  LocF7TableName = 'gragru1'
                  LocF7SQLMasc = '$#'
                end
              end
            end
          end
          object CategoryPanel3: TCategoryPanel
            Top = 60
            Height = 333
            Caption = 'Adi/reti. Produto'
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 3
            object CategoryPanelGroup2: TCategoryPanelGroup
              Left = 0
              Top = 0
              Width = 628
              Height = 307
              VertScrollBar.Tracking = True
              Align = alClient
              HeaderFont.Charset = DEFAULT_CHARSET
              HeaderFont.Color = clWindowText
              HeaderFont.Height = -11
              HeaderFont.Name = 'Tahoma'
              HeaderFont.Style = []
              TabOrder = 0
              object CategoryPanel3_3: TCategoryPanel
                Top = 230
                Height = 30
                Caption = 'Remove?'
                Collapsed = True
                Ctl3D = False
                ParentCtl3D = False
                TabOrder = 0
                object RGAcaoPrd_Tira: TRadioGroup
                  Left = 20
                  Top = 32
                  Width = 521
                  Height = 105
                  Items.Strings = (
                    'N'#227'o'
                    'Sim')
                  TabOrder = 0
                  OnClick = RGAcaoPrd_TiraClick
                end
              end
              object CategoryPanel3_2: TCategoryPanel
                Top = 200
                Height = 30
                Caption = 'Substitui?'
                Collapsed = True
                Ctl3D = False
                ParentCtl3D = False
                TabOrder = 1
                object RGAcaoPrd_Troc: TRadioGroup
                  Left = 22
                  Top = 32
                  Width = 95
                  Height = 105
                  Items.Strings = (
                    'N'#227'o'
                    'Sim')
                  TabOrder = 0
                  OnClick = RGAcaoPrd_TrocClick
                end
                object Panel3_2_Sim: TPanel
                  Left = 124
                  Top = 36
                  Width = 425
                  Height = 101
                  BevelKind = bkSoft
                  BevelOuter = bvNone
                  TabOrder = 1
                  Visible = False
                  object Label6: TLabel
                    Left = 8
                    Top = 8
                    Width = 58
                    Height = 13
                    Caption = 'Quantidade:'
                  end
                  object Label9: TLabel
                    Left = 108
                    Top = 8
                    Width = 75
                    Height = 13
                    Caption = 'N'#250'mero do lote:'
                  end
                  object Label10: TLabel
                    Left = 8
                    Top = 48
                    Width = 84
                    Height = 13
                    Caption = 'N'#250'mero do laudo:'
                  end
                  object EdQtdUso_3_2_Sim: TdmkEdit
                    Left = 8
                    Top = 24
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 3
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.000000000000000000
                    ValWarn = False
                    OnChange = EdUsoQtdChange
                  end
                  object BtOK_3_2_Sim: TBitBtn
                    Tag = 14
                    Left = 296
                    Top = 45
                    Width = 120
                    Height = 40
                    Caption = '&OK'
                    NumGlyphs = 2
                    TabOrder = 3
                    OnClick = BtOK_3_2_SimClick
                  end
                  object EdNumLote_3_2_Sim: TdmkEdit
                    Left = 108
                    Top = 24
                    Width = 164
                    Height = 21
                    TabOrder = 1
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 3
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                    OnChange = EdUsoQtdChange
                    OnEnter = EdNumLote_3_2_SimEnter
                    OnExit = EdNumLote_3_2_SimExit
                  end
                  object EdNumLaudo_3_2_Sim: TdmkEdit
                    Left = 8
                    Top = 64
                    Width = 265
                    Height = 21
                    TabOrder = 2
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 6
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                    OnChange = EdUsoCusUniChange
                    OnEnter = EdNumLaudo_3_2_SimEnter
                    OnExit = EdNumLaudo_3_2_SimExit
                  end
                  object UpDown2: TUpDown
                    Left = 88
                    Top = 22
                    Width = 17
                    Height = 25
                    TabOrder = 4
                    OnClick = UpDown2Click
                  end
                end
              end
              object CategoryPanel3_1: TCategoryPanel
                Top = 0
                Caption = 'Adi'#231#227'o de produto'
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 2
                object Label4: TLabel
                  Left = 8
                  Top = 9
                  Width = 89
                  Height = 13
                  Caption = 'Produto (reduzido):'
                end
                object Label1: TLabel
                  Left = 8
                  Top = 52
                  Width = 58
                  Height = 13
                  Caption = 'Quantidade:'
                end
                object Label2: TLabel
                  Left = 100
                  Top = 52
                  Width = 67
                  Height = 13
                  Caption = 'Custo unit'#225'rio:'
                end
                object Label3: TLabel
                  Left = 196
                  Top = 52
                  Width = 53
                  Height = 13
                  Caption = 'Custo total:'
                  Enabled = False
                end
                object Label5: TLabel
                  Left = 308
                  Top = 72
                  Width = 188
                  Height = 13
                  Caption = 'Dias de validade do produto no cliente: '
                end
                object Label7: TLabel
                  Left = 8
                  Top = 92
                  Width = 75
                  Height = 13
                  Caption = 'N'#250'mero do lote:'
                end
                object Label8: TLabel
                  Left = 196
                  Top = 92
                  Width = 84
                  Height = 13
                  Caption = 'N'#250'mero do laudo:'
                end
                object EdGraGruX: TdmkEditCB
                  Left = 8
                  Top = 25
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdCampo = 'Embalagem'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdGraGruXChange
                  OnEnter = EdGraGruXEnter
                  OnExit = EdGraGruXExit
                  DBLookupComboBox = CBGraGruX
                  IgnoraDBLookupComboBox = False
                end
                object CBGraGruX: TdmkDBLookupComboBox
                  Left = 64
                  Top = 25
                  Width = 485
                  Height = 19
                  KeyField = 'GraGruX'
                  ListField = 'NO_PRD_TAM_COR'
                  ListSource = DsGraGruX
                  TabOrder = 1
                  dmkEditCB = EdGraGruX
                  UpdType = utYes
                  LocF7CodiFldName = 'Nivel1'
                  LocF7NameFldName = 'Nome'
                  LocF7TableName = 'gragru1'
                  LocF7SQLMasc = '$#'
                end
                object BtOK_3_1: TBitBtn
                  Tag = 14
                  Left = 8
                  Top = 132
                  Width = 120
                  Height = 40
                  Caption = '&OK'
                  NumGlyphs = 2
                  TabOrder = 8
                  OnClick = BtOK_3_1Click
                end
                object EdUsoQtd: TdmkEdit
                  Left = 8
                  Top = 68
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1.000000000000000000
                  ValWarn = False
                  OnChange = EdUsoQtdChange
                end
                object EdUsoCusUni: TdmkEdit
                  Left = 100
                  Top = 68
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 6
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdUsoCusUniChange
                end
                object EdUsoCusTot: TdmkEdit
                  Left = 196
                  Top = 68
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 6
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdValCliDd: TdmkEdit
                  Left = 496
                  Top = 68
                  Width = 53
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'ValCliDd'
                  UpdCampo = 'ValCliDd'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdNumLote: TdmkEdit
                  Left = 8
                  Top = 108
                  Width = 180
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdUsoQtdChange
                  OnEnter = EdNumLoteEnter
                  OnExit = EdNumLoteExit
                end
                object EdNumLaudo: TdmkEdit
                  Left = 196
                  Top = 108
                  Width = 353
                  Height = 21
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 6
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdUsoCusUniChange
                  OnEnter = EdNumLaudoEnter
                  OnExit = EdNumLaudoExit
                end
                object UpDown1: TUpDown
                  Left = 80
                  Top = 66
                  Width = 17
                  Height = 25
                  TabOrder = 9
                  OnClick = UpDown1Click
                end
              end
            end
          end
          object CategoryPanel2: TCategoryPanel
            Top = 30
            Height = 30
            Caption = 'Quantitativo'
            Collapsed = True
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 4
            ExpandedHeight = 247
            object LaRespQtd: TLabel
              Left = 24
              Top = 16
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
            end
            object EdRespQtd: TdmkEdit
              Left = 24
              Top = 32
              Width = 105
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object BtOK_2: TBitBtn
              Tag = 14
              Left = 24
              Top = 60
              Width = 120
              Height = 40
              Caption = '&OK'
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtOK_2Click
            end
          end
          object CategoryPanel1: TCategoryPanel
            Top = 0
            Height = 30
            Caption = 'Bin'#225'rio'
            Collapsed = True
            TabOrder = 5
            ExpandedHeight = 205
            object RGRespBin: TRadioGroup
              Left = 20
              Top = 32
              Width = 521
              Height = 105
              TabOrder = 0
              OnClick = RGRespBinClick
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 876
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 872
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 876
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 730
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 728
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
        OnClick = BtOKClick
      end
      object CkPreencher: TCheckBox
        Left = 152
        Top = 16
        Width = 357
        Height = 17
        Caption = 'Preencher dados de perguntas j'#225' respondidas.'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
  end
  object QrOSPipIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrOSPipItsAfterScroll
    SQL.Strings = (
      'SELECT pip.Nome NO_PIP, pac.Nome NO_PrgAtrCad, pac.AtrTyp, '
      'pbs.Nome BinarSIM, pbn.Nome BinarNAO, '
      'pcp.Nome NO_PERGUNTA, ggx.GraGru1, opi.* '
      'FROM ospipits opi '
      'LEFT JOIN ospipmon  opm ON opm.Controle=opi.Controle'
      'LEFT JOIN pipcad    pip ON pip.Codigo=opm.PipCad '
      'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta '
      'LEFT JOIN prgbincad pbs ON pbs.Codigo=opi.BinarCad1 '
      'LEFT JOIN prgbincad pbn ON pbn.Codigo=opi.BinarCad0 '
      'LEFT JOIN prgatrcad pac ON pac.Codigo=opi.PrgAtrCad'
      'LEFT JOIN gragrux ggx ON ggx.Controle=opi.GraGruX'
      'WHERE opi.Codigo=804'
      'AND opi.Respondido=0 ')
    Left = 36
    Top = 52
    object QrOSPipItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPipItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSPipItsConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrOSPipItsPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrOSPipItsPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
    object QrOSPipItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSPipItsSubOrdem: TIntegerField
      FieldName = 'SubOrdem'
    end
    object QrOSPipItsTabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrOSPipItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOSPipItsFuncoes: TSmallintField
      FieldName = 'Funcoes'
    end
    object QrOSPipItsDependente: TSmallintField
      FieldName = 'Dependente'
    end
    object QrOSPipItsPrgAtrCad: TIntegerField
      FieldName = 'PrgAtrCad'
    end
    object QrOSPipItsFiliacao: TIntegerField
      FieldName = 'Filiacao'
    end
    object QrOSPipItsRelacao: TIntegerField
      FieldName = 'Relacao'
    end
    object QrOSPipItsNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrOSPipItsNivSeq: TIntegerField
      FieldName = 'NivSeq'
    end
    object QrOSPipItsPergunta: TIntegerField
      FieldName = 'Pergunta'
    end
    object QrOSPipItsBinarCad0: TIntegerField
      FieldName = 'BinarCad0'
    end
    object QrOSPipItsBinarCad1: TIntegerField
      FieldName = 'BinarCad1'
    end
    object QrOSPipItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSPipItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSPipItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSPipItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSPipItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSPipItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSPipItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSPipItsAcaoPrd: TSmallintField
      FieldName = 'AcaoPrd'
    end
    object QrOSPipItsRespondido: TSmallintField
      FieldName = 'Respondido'
    end
    object QrOSPipItsRespBin: TSmallintField
      FieldName = 'RespBin'
    end
    object QrOSPipItsRespQtd: TFloatField
      FieldName = 'RespQtd'
    end
    object QrOSPipItsRespAtrCad: TIntegerField
      FieldName = 'RespAtrCad'
    end
    object QrOSPipItsRespAtrIts: TIntegerField
      FieldName = 'RespAtrIts'
    end
    object QrOSPipItsRespTxtLvr: TWideMemoField
      FieldName = 'RespTxtLvr'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSPipItsBinarSIM: TWideStringField
      FieldName = 'BinarSIM'
      Size = 60
    end
    object QrOSPipItsBinarNAO: TWideStringField
      FieldName = 'BinarNAO'
      Size = 60
    end
    object QrOSPipItsNO_PERGUNTA: TWideStringField
      FieldName = 'NO_PERGUNTA'
      Size = 100
    end
    object QrOSPipItsNO_PrgAtrCad: TWideStringField
      FieldName = 'NO_PrgAtrCad'
      Size = 30
    end
    object QrOSPipItsAtrTyp: TSmallintField
      FieldName = 'AtrTyp'
    end
    object QrOSPipItsTabIdx: TLargeintField
      FieldName = 'TabIdx'
    end
    object QrOSPipItsNO_PIP: TWideStringField
      FieldName = 'NO_PIP'
      Size = 30
    end
    object QrOSPipItsRespAtrTxt: TWideStringField
      FieldName = 'RespAtrTxt'
      Size = 60
    end
    object QrOSPipItsRespPrdBin: TSmallintField
      FieldName = 'RespPrdBin'
    end
    object QrOSPipItsSobreOrd: TIntegerField
      FieldName = 'SobreOrd'
    end
    object QrOSPipItsLupForma: TSmallintField
      FieldName = 'LupForma'
    end
    object QrOSPipItsLupQtdVzs: TSmallintField
      FieldName = 'LupQtdVzs'
    end
    object QrOSPipItsLupSeqRsp: TIntegerField
      FieldName = 'LupSeqRsp'
    end
    object QrOSPipItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
  object DsOSPipIts: TDataSource
    DataSet = QrOSPipIts
    Left = 36
    Top = 96
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle GraGruX, ggv.Controle CtrlGGV, '
      'ggv.CustoPreco, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragruval ggv ON ggv.GraGruX=ggx.Controle'
      '  AND ggv.GraCusPrc=4'
      '  AND ggv.Entidade=-11'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 32
    Top = 144
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXCtrlGGV: TIntegerField
      FieldName = 'CtrlGGV'
    end
    object QrGraGruXCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 32
    Top = 192
  end
  object QrPrgAtrIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu, Controle, Nome'
      'FROM prgatrits'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 32
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrgAtrItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrgAtrItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPrgAtrItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPrgAtrIts: TDataSource
    DataSet = QrPrgAtrIts
    Left = 32
    Top = 288
  end
  object QrOri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ValCliDd, UsoQtd, UsoCusUni, UsoCusTot'
      'FROM ospipitspr')
    Left = 32
    Top = 336
    object QrOriValCliDd: TIntegerField
      FieldName = 'ValCliDd'
    end
    object QrOriUsoQtd: TFloatField
      FieldName = 'UsoQtd'
    end
    object QrOriUsoCusUni: TFloatField
      FieldName = 'UsoCusUni'
    end
    object QrOriUsoCusTot: TFloatField
      FieldName = 'UsoCusTot'
    end
  end
  object QrMesmoNiv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta'
      'FROM ospipits'
      'WHERE Controle=1277'
      'AND Nivel=0'
      'ORDER BY Ordem, Controle ')
    Left = 32
    Top = 384
    object QrMesmoNivConta: TLargeintField
      FieldName = 'Conta'
    end
  end
  object QrG1Any: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pmo.ValCliDd '
      'FROM grag1prmo pmo '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pmo.Nivel1 '
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=pmo.Nivel1 '
      'WHERE ggx.Controle=:P0 '
      '')
    Left = 156
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrG1AnyValCliDd: TIntegerField
      FieldName = 'ValCliDd'
    end
    object QrG1AnyAtuNumLot: TWideStringField
      FieldName = 'AtuNumLot'
      Size = 60
    end
    object QrG1AnyAtuNumLaud: TWideStringField
      FieldName = 'AtuNumLaud'
      Size = 60
    end
  end
  object QrOPI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ospipits')
    Left = 160
    Top = 88
    object QrOPICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOPIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOPIConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrOPIPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrOPIPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
    object QrOPISobreOrd: TIntegerField
      FieldName = 'SobreOrd'
    end
    object QrOPIOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOPISubOrdem: TIntegerField
      FieldName = 'SubOrdem'
    end
    object QrOPITabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrOPITabIdx: TLargeintField
      FieldName = 'TabIdx'
    end
    object QrOPIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOPIFuncoes: TSmallintField
      FieldName = 'Funcoes'
    end
    object QrOPIDependente: TSmallintField
      FieldName = 'Dependente'
    end
    object QrOPIPrgAtrCad: TIntegerField
      FieldName = 'PrgAtrCad'
    end
    object QrOPIFiliacao: TIntegerField
      FieldName = 'Filiacao'
    end
    object QrOPIRelacao: TIntegerField
      FieldName = 'Relacao'
    end
    object QrOPINivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrOPINivSeq: TIntegerField
      FieldName = 'NivSeq'
    end
    object QrOPIPergunta: TIntegerField
      FieldName = 'Pergunta'
    end
    object QrOPIBinarCad0: TIntegerField
      FieldName = 'BinarCad0'
    end
    object QrOPIBinarCad1: TIntegerField
      FieldName = 'BinarCad1'
    end
    object QrOPIAcaoPrd: TSmallintField
      FieldName = 'AcaoPrd'
    end
    object QrOPIRespondido: TSmallintField
      FieldName = 'Respondido'
    end
    object QrOPIRespBin: TSmallintField
      FieldName = 'RespBin'
    end
    object QrOPIRespQtd: TFloatField
      FieldName = 'RespQtd'
    end
    object QrOPIRespAtrCad: TIntegerField
      FieldName = 'RespAtrCad'
    end
    object QrOPIRespAtrIts: TIntegerField
      FieldName = 'RespAtrIts'
    end
    object QrOPIRespAtrTxt: TWideStringField
      FieldName = 'RespAtrTxt'
      Size = 60
    end
    object QrOPIRespTxtLvr: TWideMemoField
      FieldName = 'RespTxtLvr'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOPIRespPrdBin: TSmallintField
      FieldName = 'RespPrdBin'
    end
    object QrOPISuperOrd: TIntegerField
      FieldName = 'SuperOrd'
    end
    object QrOPISuperSub: TIntegerField
      FieldName = 'SuperSub'
    end
    object QrOPILupForma: TSmallintField
      FieldName = 'LupForma'
    end
    object QrOPILupQtdVzs: TSmallintField
      FieldName = 'LupQtdVzs'
    end
    object QrOPILupSeqRsp: TIntegerField
      FieldName = 'LupSeqRsp'
    end
    object QrOPILupInfVzs: TSmallintField
      FieldName = 'LupInfVzs'
    end
    object QrOPIRespMetodo: TSmallintField
      FieldName = 'RespMetodo'
    end
  end
  object QrAntOPIP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ospipitspr')
    Left = 160
    Top = 184
    object QrAntOPIPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAntOPIPControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAntOPIPConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrAntOPIPIDIts: TLargeintField
      FieldName = 'IDIts'
    end
    object QrAntOPIPAcao: TSmallintField
      FieldName = 'Acao'
    end
    object QrAntOPIPGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrAntOPIPUsoQtd: TFloatField
      FieldName = 'UsoQtd'
    end
    object QrAntOPIPUsoCusUni: TFloatField
      FieldName = 'UsoCusUni'
    end
    object QrAntOPIPUsoCusTot: TFloatField
      FieldName = 'UsoCusTot'
    end
    object QrAntOPIPUsoPrc: TFloatField
      FieldName = 'UsoPrc'
    end
    object QrAntOPIPUsoVal: TFloatField
      FieldName = 'UsoVal'
    end
    object QrAntOPIPUsoDec: TFloatField
      FieldName = 'UsoDec'
    end
    object QrAntOPIPUsoTot: TFloatField
      FieldName = 'UsoTot'
    end
    object QrAntOPIPValCliDd: TIntegerField
      FieldName = 'ValCliDd'
    end
    object QrAntOPIPEhDiluente: TSmallintField
      FieldName = 'EhDiluente'
    end
    object QrAntOPIPDesativado: TSmallintField
      FieldName = 'Desativado'
    end
    object QrAntOPIPNumLote: TWideStringField
      FieldName = 'NumLote'
      Size = 60
    end
    object QrAntOPIPNumLaudo: TWideStringField
      FieldName = 'NumLaudo'
      Size = 60
    end
    object QrAntOPIPLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAntOPIPDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAntOPIPDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAntOPIPUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAntOPIPUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAntOPIPAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAntOPIPAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAntOPIPSMI_IDCtrl: TLargeintField
      FieldName = 'SMI_IDCtrl'
    end
  end
  object QrOPIPR: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ospipitspr')
    Left = 160
    Top = 136
    object QrOPIPRCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOPIPRControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOPIPRConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrOPIPRIDIts: TLargeintField
      FieldName = 'IDIts'
    end
    object QrOPIPRAcao: TSmallintField
      FieldName = 'Acao'
    end
    object QrOPIPRGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOPIPRUsoQtd: TFloatField
      FieldName = 'UsoQtd'
    end
    object QrOPIPRUsoCusUni: TFloatField
      FieldName = 'UsoCusUni'
    end
    object QrOPIPRUsoCusTot: TFloatField
      FieldName = 'UsoCusTot'
    end
    object QrOPIPRUsoPrc: TFloatField
      FieldName = 'UsoPrc'
    end
    object QrOPIPRUsoVal: TFloatField
      FieldName = 'UsoVal'
    end
    object QrOPIPRUsoDec: TFloatField
      FieldName = 'UsoDec'
    end
    object QrOPIPRUsoTot: TFloatField
      FieldName = 'UsoTot'
    end
    object QrOPIPRValCliDd: TIntegerField
      FieldName = 'ValCliDd'
    end
    object QrOPIPREhDiluente: TSmallintField
      FieldName = 'EhDiluente'
    end
    object QrOPIPRDesativado: TSmallintField
      FieldName = 'Desativado'
    end
    object QrOPIPRNumLote: TWideStringField
      FieldName = 'NumLote'
      Size = 60
    end
    object QrOPIPRNumLaudo: TWideStringField
      FieldName = 'NumLaudo'
      Size = 60
    end
  end
  object QrSMIA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM stqmovitsa'
      'WHERE IDCtrl=?')
    Left = 160
    Top = 232
    object QrSMIADataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrSMIAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrSMIATipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSMIAOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrSMIAOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrSMIAOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrSMIAOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrSMIAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSMIAStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrSMIAGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSMIAQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSMIAPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSMIAPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSMIAAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSMIAAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSMIAFatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrSMIAQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
    end
    object QrSMIARetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrSMIAParTipo: TIntegerField
      FieldName = 'ParTipo'
    end
    object QrSMIAParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
    object QrSMIADebCtrl: TIntegerField
      FieldName = 'DebCtrl'
    end
    object QrSMIASMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
    end
    object QrSMIACustoAll: TFloatField
      FieldName = 'CustoAll'
    end
    object QrSMIAValorAll: TFloatField
      FieldName = 'ValorAll'
    end
    object QrSMIAGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
    object QrSMIABaixa: TSmallintField
      FieldName = 'Baixa'
    end
    object QrSMIAAntQtde: TFloatField
      FieldName = 'AntQtde'
    end
    object QrSMIAUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrSMIAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSMIAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSMIAStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrSMIAValiStq: TIntegerField
      FieldName = 'ValiStq'
    end
  end
end
