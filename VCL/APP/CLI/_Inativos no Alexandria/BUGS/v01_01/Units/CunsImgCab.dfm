object FmCunsImgCab: TFmCunsImgCab
  Left = 339
  Top = 185
  Caption = 'CAD-SUBCL-008 :: Cadastro de Fotos Relacionadas ao Cliente'
  ClientHeight = 591
  ClientWidth = 652
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 453
    Width = 652
    Height = 24
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 652
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label4: TLabel
      Left = 12
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 96
      Top = 20
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object DBEdCodigo: TDBEdit
      Left = 12
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object EdNO_ENT: TdmkEdit
      Left = 96
      Top = 36
      Width = 545
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'EdNO_ENT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'EdNO_ENT'
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 652
    Height = 341
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 648
      Height = 170
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label6: TLabel
        Left = 8
        Top = 0
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label7: TLabel
        Left = 92
        Top = 0
        Width = 28
        Height = 13
        Caption = 'Nome'
      end
      object Label2: TLabel
        Left = 8
        Top = 40
        Width = 39
        Height = 13
        Caption = 'Arquivo:'
      end
      object SBCaminho: TSpeedButton
        Left = 616
        Top = 56
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBCaminhoClick
      end
      object Label1: TLabel
        Left = 8
        Top = 80
        Width = 67
        Height = 13
        Caption = 'Depend'#234'ncia:'
      end
      object Label9: TLabel
        Left = 580
        Top = 80
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object EdControle: TdmkEdit
        Left = 8
        Top = 16
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 92
        Top = 16
        Width = 545
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCaminho: TdmkEdit
        Left = 8
        Top = 56
        Width = 605
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Caminho'
        UpdCampo = 'Caminho'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdDependencia: TdmkEditCB
        Left = 8
        Top = 96
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Dependencia'
        UpdCampo = 'Dependencia'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBDependencia
        IgnoraDBLookupComboBox = False
      end
      object CBDependencia: TdmkDBLookupComboBox
        Left = 64
        Top = 96
        Width = 513
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_COMPLETO'
        ListSource = DsDependencias
        TabOrder = 4
        dmkEditCB = EdDependencia
        QryCampo = 'Dependencia'
        UpdType = utYes
        LocF7CodiFldName = 'Codigo'
        LocF7NameFldName = 'Nome'
        LocF7TableName = 'dependenci'
        LocF7SQLMasc = '#'
      end
      object RGAplicacao: TdmkRadioGroup
        Left = 8
        Top = 120
        Width = 629
        Height = 45
        Caption = ' Aplica'#231#227'o: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Texto Livre'
          'N.C.T./C.A.C.')
        TabOrder = 5
        OnClick = RGAplicacaoClick
        QryCampo = 'Aplicacao'
        UpdCampo = 'Aplicacao'
        UpdType = utYes
        OldValor = 0
      end
      object EdOrdem: TdmkEdit
        Left = 580
        Top = 96
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
    object PageControl1: TPageControl
      Left = 2
      Top = 185
      Width = 648
      Height = 113
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Texto livre'
        object MeObserv: TdmkMemo
          Left = 0
          Top = 0
          Width = 640
          Height = 85
          Align = alClient
          TabOrder = 0
          QryCampo = 'Observ'
          UpdCampo = 'Observ'
          UpdType = utYes
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'N.C.T.'
        ImageIndex = 1
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 640
          Height = 85
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label8: TLabel
            Left = 4
            Top = 4
            Width = 173
            Height = 13
            Caption = 'N.C.T. (N'#227'o Conformidade T'#233'cnica):'
          end
          object SbCunsImgNCT: TSpeedButton
            Left = 611
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbCunsImgNCTClick
          end
          object EdCodiNCT: TdmkEditCB
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CunsImgNCT'
            UpdCampo = 'CunsImgNCT'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBCodiNCT
            IgnoraDBLookupComboBox = False
          end
          object CBCodiNCT: TdmkDBLookupComboBox
            Left = 60
            Top = 20
            Width = 549
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCunsImgNCT
            TabOrder = 1
            dmkEditCB = EdCodiNCT
            QryCampo = 'CunsImgNCT'
            UpdType = utYes
            LocF7SQLMasc = '#'
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 298
      Width = 648
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'Panel5'
      TabOrder = 2
      object SbStatus: TSpeedButton
        Left = 616
        Top = 16
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbStatusClick
      end
      object Label5: TLabel
        Left = 8
        Top = 0
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object CBStatus: TdmkDBLookupComboBox
        Left = 64
        Top = 16
        Width = 549
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsStatus
        TabOrder = 0
        dmkEditCB = EdStatus
        QryCampo = 'Status'
        UpdType = utYes
        LocF7SQLMasc = '#'
      end
      object EdStatus: TdmkEditCB
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Status'
        UpdCampo = 'Status'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBStatus
        IgnoraDBLookupComboBox = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 652
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 604
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 556
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 530
        Height = 32
        Caption = 'Cadastro de Fotos Relacionadas ao Cliente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 530
        Height = 32
        Caption = 'Cadastro de Fotos Relacionadas ao Cliente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 530
        Height = 32
        Caption = 'Cadastro de Fotos Relacionadas ao Cliente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 477
    Width = 652
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 648
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 521
    Width = 652
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 506
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 504
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrStatus: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cunsimgsit'
      'ORDER BY Ordem')
    Left = 8
    Top = 12
    object QrStatusCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStatusNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsStatus: TDataSource
    DataSet = QrStatus
    Left = 36
    Top = 12
  end
  object QrDependencias: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT CONCAT(stc.Nome, " (", sic.SCompl2, '#39') '#39', dep.Nome) NO_CO' +
        'MPLETO,'
      'stc.Nome NO_LUGAR, sic.SCompl2 NO_LOCAL, '
      'dep.Nome NO_DEP, sid.Controle, sid.Dependenci '
      'FROM siapimadep sid'
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci'
      'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo'
      'LEFT JOIN siaptercad stc ON stc.Codigo=sic.SiapImaTer'
      'WHERE stc.Cliente>0')
    Left = 92
    Top = 12
    object QrDependenciasNO_COMPLETO: TWideStringField
      FieldName = 'NO_COMPLETO'
      Size = 224
    end
    object QrDependenciasControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsDependencias: TDataSource
    DataSet = QrDependencias
    Left = 120
    Top = 12
  end
  object QrCunsImgNCT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cunsimgnct'
      'ORDER BY Nome')
    Left = 148
    Top = 12
    object QrCunsImgNCTCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCunsImgNCTNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCunsImgNCT: TDataSource
    DataSet = QrCunsImgNCT
    Left = 176
    Top = 12
  end
end
