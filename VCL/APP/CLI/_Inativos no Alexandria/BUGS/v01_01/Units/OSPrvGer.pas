unit OSPrvGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEditDateTimePicker, dmkEdit,
  dmkEditCB, dmkDBLookupComboBox, mySQLDbTables, dmkCheckGroup, dmkCompoStore,
  frxClass, frxDBSet, dmkDBGridZTO, UnDmkEnums;

type
  TFmOSPrvGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    QrFormContat: TmySQLQuery;
    QrFormContatCodigo: TIntegerField;
    QrFormContatNome: TWideStringField;
    DsFormContat: TDataSource;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_ENT: TWideStringField;
    DsEntidades: TDataSource;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    DsEntiContat: TDataSource;
    QrAgentes: TmySQLQuery;
    QrAgentesCodigo: TIntegerField;
    QrAgentesNome: TWideStringField;
    DsAgentes: TDataSource;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    CBCliente: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    EdContato: TdmkEditCB;
    CBContato: TdmkDBLookupComboBox;
    EdAgente: TdmkEditCB;
    CBAgente: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    EdFormContat: TdmkEditCB;
    CBFormContat: TdmkDBLookupComboBox;
    Panel6: TPanel;
    GroupBox5: TGroupBox;
    Panel7: TPanel;
    TPDtHrContat_Ini: TdmkEditDateTimePicker;
    TPDtHrContat_Fim: TdmkEditDateTimePicker;
    RGOrfaos: TRadioGroup;
    DBGrid1: TdmkDBGridZTO;
    BtOK: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    CkDtHrContat_Ini: TCheckBox;
    CkDtHrContat_Fim: TCheckBox;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    CSTabSheetChamou: TdmkCompoStore;
    QrPesqCodigo: TIntegerField;
    QrPesqControle: TIntegerField;
    QrPesqDtHrContat: TDateTimeField;
    QrPesqFormContat: TIntegerField;
    QrPesqCliente: TIntegerField;
    QrPesqContato: TIntegerField;
    QrPesqAgente: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqNO_Cliente: TWideStringField;
    QrPesqNO_Agente: TWideStringField;
    QrPesqNO_Contato: TWideStringField;
    QrPesqNO_FormContat: TWideStringField;
    QrOSPrvStatus: TmySQLQuery;
    QrOSPrvStatusCodigo: TIntegerField;
    QrOSPrvStatusNome: TWideStringField;
    DsOSPrvStatus: TDataSource;
    EdPrvStatus: TdmkEditCB;
    CBPrvStatus: TdmkDBLookupComboBox;
    CkPrvStatus: TCheckBox;
    QrPesqPrvStatus: TIntegerField;
    SbImprime: TBitBtn;
    frxGER_OSERV_027_001: TfrxReport;
    frxDsPesq: TfrxDBDataset;
    QrPesqNO_PrvStatus: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdContatoChange(Sender: TObject);
    procedure EdAgenteChange(Sender: TObject);
    procedure TPDtHrContat_IniChange(Sender: TObject);
    procedure TPDtHrContat_IniClick(Sender: TObject);
    procedure TPDtHrContat_FimClick(Sender: TObject);
    procedure TPDtHrContat_FimChange(Sender: TObject);
    procedure EdFormContatChange(Sender: TObject);
    procedure RGOrfaosClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdPrvStatusChange(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxGER_OSERV_027_001GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    procedure FechaPesquisa();
    procedure MostraFormOsPrvAdd(SQLType: TSQLType);
    procedure RefazPesquisa();

  public
    { Public declarations }
  end;

  var
  FmOSPrvGer: TFmOSPrvGer;

implementation

uses UnMyObjects, Module, UnOSApp_PF, ModuleGeral, MyGlyfs, Principal,
UMySQLModule, UnDmkProcFunc, DmkDAC_PF;

{$R *.DFM}

procedure TFmOSPrvGer.BtAlteraClick(Sender: TObject);
begin
  MostraFormOSPrvAdd(stUpd);
  RefazPesquisa();
end;

procedure TFmOSPrvGer.BtExcluiClick(Sender: TObject);
begin
  if QrPesqCodigo.Value <> 0 then
  else
  begin
    if UMyMod.ExcluiRegistroInt1(
    'Confirma a exclusão da solicitação de providência atual?',
    'osprv', 'Controle', QrPesqControle.Value, Dmod.MyDB) = ID_YES then
      RefazPesquisa();
  end;
end;

procedure TFmOSPrvGer.BtIncluiClick(Sender: TObject);
begin
  MostraFormOsPrvAdd(stIns);
end;

procedure TFmOSPrvGer.BtOKClick(Sender: TObject);
begin
  RefazPesquisa();
end;

procedure TFmOSPrvGer.BtSaidaClick(Sender: TObject);
begin
  if TFmOSPrvGer(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmOSPrvGer(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmOSPrvGer.EdAgenteChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOSPrvGer.EdClienteChange(Sender: TObject);
begin
  DModG.ReopenEntiContat(QrEntiContat, EdCliente.ValueVariant, False);
  FechaPesquisa();
end;

procedure TFmOSPrvGer.EdContatoChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOSPrvGer.EdFormContatChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOSPrvGer.EdPrvStatusChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOSPrvGer.FechaPesquisa();
begin
  QrPesq.Close;
end;

procedure TFmOSPrvGer.FormActivate(Sender: TObject);
begin
  if TFmOSPrvGer(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmOSPrvGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UMyMod.AbreQuery(QrFormContat, DMod.MyDB);
  UMyMod.AbreQuery(QrEntidades, DMod.MyDB);
  UMyMod.AbreQuery(QrAgentes, DMod.MyDB);
  UMyMod.AbreQuery(QrOSPrvStatus, DMod.MyDB);
end;

procedure TFmOSPrvGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPrvGer.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmOSPrvGer.frxGER_OSERV_027_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_DATA' then Value := Now()
  else
  if VarName = 'VARF_PERIODO' then Value := dmkPF.PeriodoImp(
    TPDtHrContat_Ini.Date, TPDtHrContat_Fim.Date,
    0, 0, CkDtHrContat_Ini.Checked, CkDtHrContat_Fim.Checked, False, False, '', '')
  else
{  if VarName = 'VARF_EMPRESA' then
    Value := ?;
  else
}
  if VarName = 'VARF_CLIENTE' then
    Value := dmkPF.ParValueCodTxt('', CBCliente.Text, EdCliente.ValueVariant)
  else
  if VarName = 'VARF_CONTATO' then
    Value := dmkPF.ParValueCodTxt('', CBContato.Text, EdContato.ValueVariant)
  else
  if VarName = 'VARF_AGENTE' then
    Value := dmkPF.ParValueCodTxt('', CBAgente.Text, EdAgente.ValueVariant)
  else
  if VarName = 'VARF_NO_PRVSTATUS' then
  begin
    if CkPrvStatus.Checked then
      Value := CBPrvStatus.Text
    else
      Value := 'TODOS';
  end
  else
  if VarName = 'VARF_FORMACONTATO' then
    Value := dmkPF.ParValueCodTxt('', CBFormContat.Text, EdFormContat.ValueVariant)
  else
  if VarName = 'VARF_ORFAOS' then
    Value := RGOrfaos.Items[RGOrfaos.ItemIndex]
  else
end;

procedure TFmOSPrvGer.MostraFormOsPrvAdd(SQLType: TSQLType);
const
  Ins_Cliente    = 0;
  Ins_Contato    = 0;
  Ins_Agente     = 0;
  Ins_FormContat = 0;
var
  Codigo, Controle: Integer;
begin
  if SQLtype = stIns then
  begin
    Codigo   := 0;
    Controle := 0;
  end else
  begin
    Codigo   := QrPesqCodigo.Value;
    Controle := QrPesqControle.Value;
  end;
  OSApp_PF.MostraFormOSPrvAdd(SQLType, Codigo, Controle, QrPesq,
  Ins_Cliente, Ins_Contato, Ins_Agente, Ins_FormContat);
  //
  RefazPesquisa();
end;

procedure TFmOSPrvGer.QrPesqAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrPesq.RecordCount > 0;
  //
  BtAltera.Enabled := Habilita;
  BtExclui.Enabled := Habilita;
end;

procedure TFmOSPrvGer.QrPesqBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
  BtExclui.Enabled := False;
end;

procedure TFmOSPrvGer.RefazPesquisa();
var
  Codigo, DtHrContat: String;
  FormContat, Cliente, Contato, Agente, PrvStatus: Integer;
begin
  case RGOrfaos.ItemIndex of
    0: Codigo := 'AND prv.Codigo <> 0 ';
    1: Codigo := 'AND prv.Codigo = 0 ';
    else Codigo := '';
  end;
  DtHrContat := dmkPF.SQL_Periodo('WHERE prv.DtHrContat ', TPDtHrContat_Ini.Date,
    TPDtHrContat_Fim.Date, CkDtHrContat_Ini.Checked, CkDtHrContat_Fim.Checked);
  //
  FormContat     := EdFormContat.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  Contato        := EdContato.ValueVariant;
  Agente         := EdAgente.ValueVariant;
  PrvStatus      := EdPrvStatus.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT ops.Nome NO_PrvStatus, prv.*, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ',
  'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_Agente, ',
  'eco.Nome NO_Contato, fct.Nome NO_FormContat  ',
  'FROM osprv prv ',
  'LEFT JOIN entidades cli ON cli.Codigo=prv.Cliente ',
  'LEFT JOIN entidades age ON age.Codigo=prv.Agente ',
  'LEFT JOIN enticontat eco ON eco.Controle=prv.Contato ',
  'LEFT JOIN formcontat fct ON fct.Codigo=prv.FormContat ',
  'LEFT JOIN osprvsta ops ON ops.Codigo=prv.PrvStatus ',
  DtHrContat,
  Codigo,
  Geral.ATS_if(FormContat <> 0, ['AND prv.FormContat=' + Geral.FF0(FormContat)]),
  Geral.ATS_if(Cliente <> 0, ['AND prv.Cliente=' + Geral.FF0(Cliente)]),
  Geral.ATS_if(Contato <> 0, ['AND prv.Contato=' + Geral.FF0(Contato)]),
  Geral.ATS_if(Agente <> 0, ['AND prv.Agente=' + Geral.FF0(Agente)]),
  Geral.ATS_if(CkPrvStatus.Checked, ['AND prv.PrvStatus=' + Geral.FF0(PrvStatus)]),
  'ORDER BY prv.DtHrContat, prv.Controle',
  '']);
end;

procedure TFmOSPrvGer.RGOrfaosClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOSPrvGer.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxGER_OSERV_027_001, [
    DModG.frxDsDono,
    frxDsPesq
  ]);
  MyObjects.frxMostra(frxGER_OSERV_027_001, 'Pesquisa de Gerenciamento de Providências');
end;

procedure TFmOSPrvGer.TPDtHrContat_FimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOSPrvGer.TPDtHrContat_FimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOSPrvGer.TPDtHrContat_IniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOSPrvGer.TPDtHrContat_IniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
