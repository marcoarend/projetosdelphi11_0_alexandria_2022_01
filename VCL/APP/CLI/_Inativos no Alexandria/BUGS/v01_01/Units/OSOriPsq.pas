unit OSOriPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, UnDmkEnums;

type
  TFmOSOriPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBGOSCab: TDBGrid;
    QrOSCab: TmySQLQuery;
    DsOSCab: TDataSource;
    QrOSCabCodigo: TIntegerField;
    QrOSCabGrupo: TIntegerField;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    QrOSSrvGarantiaDd: TIntegerField;
    QrOSSrvHrEvacuar: TIntegerField;
    QrOSSrvHrExecutar: TFloatField;
    QrOSSrvValCalc: TFloatField;
    QrOSSrvValInfo: TFloatField;
    QrOSSrvValDesc: TFloatField;
    QrOSSrvValTota: TFloatField;
    QrOSSrvAutorizado: TSmallintField;
    QrOSSrvVAL_CALCeINFO: TFloatField;
    QrOSSrvAUTORIZADO_BOOL: TBooleanField;
    QrOSSrvNO_SIGLA: TWideStringField;
    QrOSSrvDetalhes: TWideMemoField;
    QrOSSrvTudoFeitoA: TSmallintField;
    QrOSSrvTudoFeitoM: TSmallintField;
    QrOSSrvTUDOFEITO: TFloatField;
    QrOSSrvMoniDdTotl: TIntegerField;
    QrOSSrvMoniDdIntv: TIntegerField;
    DsOSSrv: TDataSource;
    DBGOSSrv: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrOSCabBeforeClose(DataSet: TDataSet);
    procedure QrOSCabAfterScroll(DataSet: TDataSet);
    procedure DBGOSSrvDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtOKClick(Sender: TObject);
    procedure QrOSSrvCalcFields(DataSet: TDataSet);
    procedure DBGOSSrvDblClick(Sender: TObject);
    procedure DBGOSCabDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FLocalizador: Integer;
    //
  end;

  var
  FmOSOriPsq: TFmOSOriPsq;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnOSApp_PF, MyVCLSkin;

{$R *.DFM}

procedure TFmOSOriPsq.BtOKClick(Sender: TObject);
begin
  if (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0) then
  begin
    FLocalizador := QrOSCabCodigo.Value;
    //
    Close;
  end;
end;

procedure TFmOSOriPsq.BtSaidaClick(Sender: TObject);
begin
  FLocalizador := 0;
  Close;
end;

procedure TFmOSOriPsq.DBGOSCabDblClick(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmOSOriPsq.DBGOSSrvDblClick(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmOSOriPsq.DBGOSSrvDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Autorizado' then
    MeuVCLSkin.DrawGrid(DBGOSSrv, Rect, 1, QrOSSrvAutorizado.Value);
  if Column.FieldName = 'TUDOFEITO' then
    MeuVCLSkin.DrawGrid(DBGOSSrv, Rect, 1, Trunc(QrOSSrvTUDOFEITO.Value));
end;

procedure TFmOSOriPsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSOriPsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FLocalizador := 0;
end;

procedure TFmOSOriPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSOriPsq.QrOSCabAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSSrv(QrOSSrv, QrOSCabCodigo.Value, 0);
end;

procedure TFmOSOriPsq.QrOSCabBeforeClose(DataSet: TDataSet);
begin
  QrOSSrv.Close;
end;

procedure TFmOSOriPsq.QrOSSrvCalcFields(DataSet: TDataSet);
begin
  QrOSSrvVAL_CALCeINFO.Value := QrOSSrvValCalc.Value + QrOSSrvValInfo.Value;
  QrOSSrvAUTORIZADO_BOOL.Value := Geral.IntToBool(QrOSSrvAutorizado.Value);
end;

end.
