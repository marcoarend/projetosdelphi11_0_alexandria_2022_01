unit OSImp2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  frxClass, frxDBSet, DmkDAC_PF, Mask, UnDmkProcFunc, UnAppListas,
  dmkDBGrid, dmkDBGridDAC, Variants, dmkImage, UnDmkEnums,
  UnProjGroup_Consts, UnProjGroup_Vars;

type
  TFmOSImp2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel5: TPanel;
    TabSheet2: TTabSheet;
    BtOK: TBitBtn;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvAtivo: TSmallintField;
    QrOSSrvGarantiaDd: TIntegerField;
    QrOSSrvHrEvacuar: TIntegerField;
    QrOSSrvHrExecutar: TFloatField;
    QrOSSrvValCalc: TFloatField;
    QrOSSrvValInfo: TFloatField;
    QrOSSrvValDesc: TFloatField;
    QrOSSrvValTota: TFloatField;
    QrOSSrvAutorizado: TSmallintField;
    QrOSSrvVAL_CALCeINFO: TFloatField;
    QrOSSrvAUTORIZADO_BOOL: TBooleanField;
    QrOSSrvNO_SIGLA: TWideStringField;
    QrOSAlv: TmySQLQuery;
    QrOSAlvCodigo: TIntegerField;
    QrOSAlvControle: TIntegerField;
    QrOSAlvConta: TIntegerField;
    QrOSAlvNO_Praga_Z: TWideStringField;
    QrOSAlvPraga_Z: TIntegerField;
    QrOSFrmCab: TmySQLQuery;
    QrOSFrmCabCodigo: TIntegerField;
    QrOSFrmCabControle: TIntegerField;
    QrOSFrmCabConta: TIntegerField;
    QrOSFrmCabNome: TWideStringField;
    QrOSFrmCabFormula: TIntegerField;
    QrOSFrmCabEquipAplic: TIntegerField;
    QrOSFrmCabQtdTot: TFloatField;
    QrOSFrmCabQtdQSP: TFloatField;
    QrOSFrmCabCusTot: TFloatField;
    QrOSFrmCabNO_FORMULA: TWideStringField;
    QrOSFrmCabNO_EquipAplic: TWideStringField;
    QrOSFrmRec: TmySQLQuery;
    QrOSFrmRecNO_GG1: TWideStringField;
    QrOSFrmRecCodigo: TIntegerField;
    QrOSFrmRecControle: TIntegerField;
    QrOSFrmRecConta: TIntegerField;
    QrOSFrmRecIDIts: TIntegerField;
    QrOSFrmRecGraGruX: TIntegerField;
    QrOSFrmRecPrvQtd: TFloatField;
    QrOSFrmRecPrvPrc: TFloatField;
    QrOSFrmRecPrvVal: TFloatField;
    QrOSFrmRecUsoQtd: TFloatField;
    QrOSFrmRecUsoPrc: TFloatField;
    QrOSFrmRecUsoVal: TFloatField;
    QrOSFrmRecUsoDec: TFloatField;
    QrOSFrmRecUsoTot: TFloatField;
    QrOSFrmRecOrdem: TIntegerField;
    QrOSFrmRecReordem: TIntegerField;
    QrOSMonCab: TmySQLQuery;
    QrOSMonCabCodigo: TIntegerField;
    QrOSMonCabControle: TIntegerField;
    QrOSMonCabConta: TIntegerField;
    QrOSMonCabNome: TWideStringField;
    QrOSMonCabFormula: TIntegerField;
    QrOSMonCabEquipAplic: TIntegerField;
    QrOSMonCabQtdTot: TFloatField;
    QrOSMonCabQtdQSP: TFloatField;
    QrOSMonCabCusTot: TFloatField;
    QrOSMonCabNO_FORMULA: TWideStringField;
    QrOSMonCabNO_EquipAplic: TWideStringField;
    QrOSMonRec: TmySQLQuery;
    QrOSMonRecNO_GG1: TWideStringField;
    QrOSMonRecCodigo: TIntegerField;
    QrOSMonRecControle: TIntegerField;
    QrOSMonRecConta: TIntegerField;
    QrOSMonRecIDIts: TIntegerField;
    QrOSMonRecGraGruX: TIntegerField;
    QrOSMonRecPrvQtd: TFloatField;
    QrOSMonRecPrvPrc: TFloatField;
    QrOSMonRecPrvVal: TFloatField;
    QrOSMonRecUsoQtd: TFloatField;
    QrOSMonRecUsoPrc: TFloatField;
    QrOSMonRecUsoVal: TFloatField;
    QrOSMonRecUsoDec: TFloatField;
    QrOSMonRecUsoTot: TFloatField;
    QrOSMonRecOrdem: TIntegerField;
    QrOSMonRecReordem: TIntegerField;
    frxDsOSMonRec: TfrxDBDataset;
    frxDsOSMonCab: TfrxDBDataset;
    frxDsOSFrmRec: TfrxDBDataset;
    frxDsOSFrmCab: TfrxDBDataset;
    frxDsOSAlv: TfrxDBDataset;
    frxDsOSSrv: TfrxDBDataset;
    QrOSAlvAtivo: TSmallintField;
    QrOSFrmCabAtivo: TSmallintField;
    QrOSFrmRecAtivo: TSmallintField;
    QrOSMonCabAtivo: TSmallintField;
    QrOSMonRecAtivo: TSmallintField;
    frxDsOSCab_OSImp: TfrxDBDataset;
    QrOSCab: TmySQLQuery;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabNO_FatoGeradr: TWideStringField;
    QrOSCabNO_ESTATUS: TWideStringField;
    QrOSCabNO_ENT: TWideStringField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabNO_SiapTerCad: TWideStringField;
    QrOSCabTXTVisPrv: TWideStringField;
    QrOSCabTXTContat: TWideStringField;
    QrOSCabTXTVisExe: TWideStringField;
    QrOSCabTXTExePrv: TWideStringField;
    QrOSCabTXTExeIni: TWideStringField;
    QrOSCabTXTExeFim: TWideStringField;
    QrOSCabValorTotal: TFloatField;
    QrOSCabDdsPosVda: TIntegerField;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    QrOSCabEntContrat: TIntegerField;
    QrOSCabEmpresa: TIntegerField;
    QrOSCabValorServi: TFloatField;
    QrOSCabValorDesco: TFloatField;
    Panel7: TPanel;
    Label9: TLabel;
    QrOSCabAlv: TmySQLQuery;
    QrOSCabAlvCodigo: TIntegerField;
    QrOSCabAlvControle: TIntegerField;
    QrOSCabAlvOrdem: TSmallintField;
    QrOSCabAlvAtivo: TSmallintField;
    QrOSCabAlvPraga_A: TIntegerField;
    QrOSCabAlvPraga_Z: TIntegerField;
    QrOSCabAlvNO_NIVEL: TWideStringField;
    QrOSCabAlvNO_PRAGA: TWideStringField;
    QrOSPrz: TmySQLQuery;
    QrOSPrzCodigo: TIntegerField;
    QrOSPrzControle: TIntegerField;
    QrOSPrzCondicao: TIntegerField;
    QrOSPrzNO_CONDICAO: TWideStringField;
    QrOSPrzDescoPer: TFloatField;
    QrOSPrzEscolhido: TSmallintField;
    frxDsOSCabAlv: TfrxDBDataset;
    frxDsOSPrz: TfrxDBDataset;
    Label12: TLabel;
    EdCliCod: TdmkEdit;
    EdCliNom: TdmkEdit;
    QrOC: TmySQLQuery;
    QrOCCodigo: TIntegerField;
    QrOCEntidade: TIntegerField;
    QrOCEstatus: TIntegerField;
    QrOCFatoGeradr: TIntegerField;
    QrOCDtaContat: TDateTimeField;
    QrOCDtaVisPrv: TDateTimeField;
    QrOCDtaVisExe: TDateTimeField;
    QrOCDtaExePrv: TDateTimeField;
    QrOCDtaExeIni: TDateTimeField;
    QrOCDtaExeFim: TDateTimeField;
    QrOCAtivo: TSmallintField;
    QrOCNO_FatoGeradr: TWideStringField;
    QrOCNO_ESTATUS: TWideStringField;
    QrOCNO_ENT: TWideStringField;
    QrOCSiapTerCad: TIntegerField;
    QrOCNO_SiapTerCad: TWideStringField;
    QrOCTXTVisPrv: TWideStringField;
    QrOCTXTContat: TWideStringField;
    QrOCTXTVisExe: TWideStringField;
    QrOCTXTExePrv: TWideStringField;
    QrOCTXTExeIni: TWideStringField;
    QrOCTXTExeFim: TWideStringField;
    QrOCValorTotal: TFloatField;
    QrOCDdsPosVda: TIntegerField;
    QrOCEntiContat: TIntegerField;
    QrOCNumContrat: TIntegerField;
    QrOCEntPagante: TIntegerField;
    QrOCEntContrat: TIntegerField;
    QrOCEmpresa: TIntegerField;
    QrOCValorServi: TFloatField;
    QrOCValorDesco: TFloatField;
    QrOCA: TmySQLQuery;
    QrOCACodigo: TIntegerField;
    QrOCAControle: TIntegerField;
    QrOCAOrdem: TSmallintField;
    QrOCALk: TIntegerField;
    QrOCADataCad: TDateField;
    QrOCADataAlt: TDateField;
    QrOCAUserCad: TIntegerField;
    QrOCAUserAlt: TIntegerField;
    QrOCAAlterWeb: TSmallintField;
    QrOCAAtivo: TSmallintField;
    QrOCAPraga_A: TIntegerField;
    QrOCAPraga_Z: TIntegerField;
    QrOCANO_NIVEL: TWideStringField;
    QrOCANO_PRAGA: TWideStringField;
    QrOP: TmySQLQuery;
    QrOPCodigo: TIntegerField;
    QrOPControle: TIntegerField;
    QrOPCondicao: TIntegerField;
    QrOPDescoPer: TFloatField;
    QrOPEscolhido: TSmallintField;
    QrOPNO_CONDICAO: TWideStringField;
    QrOPBaseVal: TFloatField;
    QrOPDescoVal: TFloatField;
    QrOPTotaVal: TFloatField;
    QrOS: TmySQLQuery;
    QrOSNO_DesServico: TWideStringField;
    QrOSNO_SIGLA: TWideStringField;
    QrOSCodigo: TIntegerField;
    QrOSControle: TIntegerField;
    QrOSDesServico: TIntegerField;
    QrOSGarantiaDd: TIntegerField;
    QrOSHrEvacuar: TIntegerField;
    QrOSHrExecutar: TFloatField;
    QrOSValCalc: TFloatField;
    QrOSValInfo: TFloatField;
    QrOSValDesc: TFloatField;
    QrOSValTota: TFloatField;
    QrOSAutorizado: TSmallintField;
    QrOFD: TmySQLQuery;
    QrOFDCodigo: TIntegerField;
    QrOFDControle: TIntegerField;
    QrOFDConta: TIntegerField;
    QrOFDTabela: TSmallintField;
    QrOFDCadastro: TIntegerField;
    QrOFDSIGLA: TWideStringField;
    QrOFDNO_Campo: TWideStringField;
    QrOA: TmySQLQuery;
    QrOANO_Praga_Z: TWideStringField;
    QrOACodigo: TIntegerField;
    QrOAControle: TIntegerField;
    QrOAConta: TIntegerField;
    QrOAPraga_Z: TIntegerField;
    QrOFC: TmySQLQuery;
    QrOFR: TmySQLQuery;
    QrOFCCodigo: TIntegerField;
    QrOFCControle: TIntegerField;
    QrOFCConta: TIntegerField;
    QrOFCNome: TWideStringField;
    QrOFCFormula: TIntegerField;
    QrOFCEquipAplic: TIntegerField;
    QrOFCQtdTot: TFloatField;
    QrOFCQtdQSP: TFloatField;
    QrOFCCusTot: TFloatField;
    QrOFCNO_FORMULA: TWideStringField;
    QrOFCNO_EquipAplic: TWideStringField;
    QrOFRNO_GG1: TWideStringField;
    QrOFRCodigo: TIntegerField;
    QrOFRControle: TIntegerField;
    QrOFRConta: TIntegerField;
    QrOFRIDIts: TIntegerField;
    QrOFRGraGruX: TIntegerField;
    QrOFRPrvQtd: TFloatField;
    QrOFRPrvPrc: TFloatField;
    QrOFRPrvVal: TFloatField;
    QrOFRUsoQtd: TFloatField;
    QrOFRUsoPrc: TFloatField;
    QrOFRUsoVal: TFloatField;
    QrOFRUsoDec: TFloatField;
    QrOFRUsoTot: TFloatField;
    QrOFROrdem: TIntegerField;
    QrOFRReordem: TIntegerField;
    QrOMC: TmySQLQuery;
    QrOMR: TmySQLQuery;
    QrOMRNO_GG1: TWideStringField;
    QrOMRCodigo: TIntegerField;
    QrOMRControle: TIntegerField;
    QrOMRConta: TIntegerField;
    QrOMRIDIts: TIntegerField;
    QrOMRGraGruX: TIntegerField;
    QrOMRPrvQtd: TFloatField;
    QrOMRPrvPrc: TFloatField;
    QrOMRPrvVal: TFloatField;
    QrOMRUsoQtd: TFloatField;
    QrOMRUsoPrc: TFloatField;
    QrOMRUsoVal: TFloatField;
    QrOMRUsoDec: TFloatField;
    QrOMRUsoTot: TFloatField;
    QrOMROrdem: TIntegerField;
    QrOMRReordem: TIntegerField;
    QrOMCCodigo: TIntegerField;
    QrOMCControle: TIntegerField;
    QrOMCConta: TIntegerField;
    QrOMCNome: TWideStringField;
    QrOMCFormula: TIntegerField;
    QrOMCEquipAplic: TIntegerField;
    QrOMCQtdTot: TFloatField;
    QrOMCQtdQSP: TFloatField;
    QrOMCCusTot: TFloatField;
    QrOMCNO_FORMULA: TWideStringField;
    QrOMCNO_EquipAplic: TWideStringField;
    Panel6: TPanel;
    Panel8: TPanel;
    GroupBox2: TGroupBox;
    Panel9: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    EdQtdOSPrz: TdmkEdit;
    EdQtdOSCabAlv: TdmkEdit;
    CkPreVistoria: TCheckBox;
    GroupBox3: TGroupBox;
    Panel10: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdQtdOSSrv: TdmkEdit;
    EdQtdOSAlv: TdmkEdit;
    EdQtdOSFrmCab: TdmkEdit;
    EdQtdOSFrmRec: TdmkEdit;
    EdQtdOSMonCab: TdmkEdit;
    EdQtdOSMonRec: TdmkEdit;
    CkVistoria: TCheckBox;
    QrOFDAtivo: TSmallintField;
    QrAnalise: TmySQLQuery;
    QrOSPrzAtivo: TSmallintField;
    QrOAAtivo: TSmallintField;
    QrOFCAtivo: TSmallintField;
    QrOFRAtivo: TSmallintField;
    QrOMCAtivo: TSmallintField;
    QrOMRAtivo: TSmallintField;
    QrOPAtivo: TSmallintField;
    QrOSAtivo: TSmallintField;
    Label14: TLabel;
    TabSheet3: TTabSheet;
    Panel12: TPanel;
    QrSiapTerCad: TmySQLQuery;
    QrSiapTerCadCodigo: TIntegerField;
    QrSiapTerCadNome: TWideStringField;
    QrSiapTerCadCliente: TIntegerField;
    QrSiapTerCadSLograd: TSmallintField;
    QrSiapTerCadSRua: TWideStringField;
    QrSiapTerCadSNumero: TIntegerField;
    QrSiapTerCadSCompl: TWideStringField;
    QrSiapTerCadSBairro: TWideStringField;
    QrSiapTerCadSCidade: TWideStringField;
    QrSiapTerCadSUF: TWideStringField;
    QrSiapTerCadSCEP: TIntegerField;
    QrSiapTerCadSPais: TWideStringField;
    QrSiapTerCadSEndeRef: TWideStringField;
    QrSiapTerCadSCodMunici: TIntegerField;
    QrSiapTerCadSCodiPais: TIntegerField;
    QrSiapTerCadM2Constru: TFloatField;
    QrSiapTerCadM2NaoBuild: TFloatField;
    QrSiapTerCadM2Terreno: TFloatField;
    QrSiapTerCadM2Total: TFloatField;
    QrSiapTerCadNOMELOGRAD: TWideStringField;
    QrSiapImaCad: TmySQLQuery;
    QrSiapImaCadTpConstru: TIntegerField;
    QrSiapImaCadSCompl2: TWideStringField;
    QrSiapImaCadNO_OBJETO: TWideStringField;
    QrSiapImaCadNO_FINALID: TWideStringField;
    QrSiapImaCadNO_TPCONSTRU: TWideStringField;
    QrSiapImaCadCodigo: TIntegerField;
    QrSiapImaCadSiapImaTer: TIntegerField;
    QrSiapImaCadObjeto: TIntegerField;
    QrSiapImaCadFinalidade: TIntegerField;
    QrSiapImaCadM2Constru: TFloatField;
    QrSiapImaCadM2NaoBuild: TFloatField;
    QrSiapImaCadM2Terreno: TFloatField;
    QrSiapImaCadM2Total: TFloatField;
    QrSiapImaDep: TmySQLQuery;
    QrSiapImaDepNO_DEPENDENCI: TWideStringField;
    QrSiapImaDepCodigo: TIntegerField;
    QrSiapImaDepControle: TIntegerField;
    QrSiapImaDepDependenci: TIntegerField;
    QrSiapImaDepMLarg: TFloatField;
    QrSiapImaDepMComp: TFloatField;
    QrSiapImaDepMAltu: TFloatField;
    QrSiapImaCdi: TmySQLQuery;
    QrSiapImaCdiNO_CARACTERIS: TWideStringField;
    QrSiapImaCdiCodigo: TIntegerField;
    QrSiapImaCdiControle: TIntegerField;
    QrSiapImaCdiCaracteris: TIntegerField;
    QrSiapImaCav: TmySQLQuery;
    QrSiapImaCavNO_CARACTERIS: TWideStringField;
    QrSiapImaCavCodigo: TIntegerField;
    QrSiapImaCavControle: TIntegerField;
    QrSiapImaCavCaracteris: TIntegerField;
    QrSiapImaRes: TmySQLQuery;
    QrSiapImaResNO_RESIDENTE: TWideStringField;
    QrSiapImaResNO_CUIDADO: TWideStringField;
    QrSiapImaResCodigo: TIntegerField;
    QrSiapImaResControle: TIntegerField;
    QrSiapImaResResidente: TIntegerField;
    QrSiapImaResNome: TWideStringField;
    QrSiapImaResAnoNatal: TIntegerField;
    QrSiapImaResCuidado: TIntegerField;
    QrSiapImaResObservacao: TWideStringField;
    QrSiapImaCui: TmySQLQuery;
    QrSiapImaCuiNO_CUIDADO: TWideStringField;
    QrSiapImaCuiCodigo: TIntegerField;
    QrSiapImaCuiControle: TIntegerField;
    QrSiapImaCuiCuidado: TIntegerField;
    QrSiapImaAti: TmySQLQuery;
    QrSiapImaAtiNO_ATIVIDADE: TWideStringField;
    QrSiapImaAtiCodigo: TIntegerField;
    QrSiapImaAtiControle: TIntegerField;
    QrSiapImaAtiAtividade: TIntegerField;
    QrSiapImaCxa: TmySQLQuery;
    QrSiapImaCxaNO_MATERIAL: TWideStringField;
    QrSiapImaCxaNOME_FORMA: TWideStringField;
    QrSiapImaCxaCodigo: TIntegerField;
    QrSiapImaCxaControle: TIntegerField;
    QrSiapImaCxaMatersCxa: TIntegerField;
    QrSiapImaCxaFormasCxa: TIntegerField;
    QrSiapImaCxaVolumeL: TFloatField;
    QrSiapImaCxaLocal: TWideStringField;
    QrSiapImaCxaAcesso: TWideStringField;
    QrSiapImaCxaMedidas: TWideStringField;
    QrAtrSICxDef: TmySQLQuery;
    QrAtrSICxDefID_Item: TIntegerField;
    QrAtrSICxDefID_Sorc: TIntegerField;
    QrAtrSICxDefAtrCad: TIntegerField;
    QrAtrSICxDefAtrIts: TIntegerField;
    QrAtrSICxDefCU_CAD: TIntegerField;
    QrAtrSICxDefCU_ITS: TIntegerField;
    QrAtrSICxDefNO_CAD: TWideStringField;
    QrAtrSICxDefNO_ITS: TWideStringField;
    QrAtrSICdDef: TmySQLQuery;
    QrAtrSICdDefID_Item: TIntegerField;
    QrAtrSICdDefID_Sorc: TIntegerField;
    QrAtrSICdDefAtrCad: TIntegerField;
    QrAtrSICdDefAtrIts: TIntegerField;
    QrAtrSICdDefCU_CAD: TIntegerField;
    QrAtrSICdDefCU_ITS: TIntegerField;
    QrAtrSICdDefNO_CAD: TWideStringField;
    QrAtrSICdDefNO_ITS: TWideStringField;
    QrSiapTerCadSTe1: TWideStringField;
    DsSiapTerCad: TDataSource;
    Panel13: TPanel;
    CkLugar: TCheckBox;
    GroupBox5: TGroupBox;
    Panel14: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    EdQtdSImaCad: TdmkEdit;
    EdQtdSImaDep: TdmkEdit;
    EdQtdSImaCdi: TdmkEdit;
    EdQtdSImaCav: TdmkEdit;
    EdQtdSImaRes: TdmkEdit;
    EdQtdSImaCui: TdmkEdit;
    EdQtdSImaAti: TdmkEdit;
    CkImoveis: TCheckBox;
    QrSiapImaCadAtivo: TSmallintField;
    QrSiapImaDepAtivo: TSmallintField;
    QrSiapImaCdiAtivo: TSmallintField;
    QrSiapImaCavAtivo: TSmallintField;
    QrSiapImaResAtivo: TSmallintField;
    QrSiapImaCuiAtivo: TSmallintField;
    QrSiapImaAtiAtivo: TSmallintField;
    QrSiapImaCxaAtivo: TSmallintField;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    EdQtdSImaCxa: TdmkEdit;
    EdQtdSICxDef: TdmkEdit;
    EdQtdSICdDef: TdmkEdit;
    QrAtrSICdDefAtivo: TSmallintField;
    QrAtrSICxDefAtivo: TSmallintField;
    QrSiapTerCadAtivo: TSmallintField;
    Panel15: TPanel;
    Label25: TLabel;
    EdQtdSMovCad: TdmkEdit;
    Label26: TLabel;
    EdQtdSMovDef: TdmkEdit;
    QrMovAmovCad: TmySQLQuery;
    QrMovAmovCadCodigo: TIntegerField;
    QrMovAmovCadNome: TWideStringField;
    QrMovAmovCadTipo: TSmallintField;
    QrMovAmovCadCliente: TIntegerField;
    QrAtrAMovDef: TmySQLQuery;
    QrAtrAMovDefID_Item: TIntegerField;
    QrAtrAMovDefID_Sorc: TIntegerField;
    QrAtrAMovDefAtrCad: TIntegerField;
    QrAtrAMovDefAtrIts: TIntegerField;
    QrAtrAMovDefCU_CAD: TIntegerField;
    QrAtrAMovDefCU_ITS: TIntegerField;
    QrAtrAMovDefNO_CAD: TWideStringField;
    QrAtrAMovDefNO_ITS: TWideStringField;
    QrAtrAMovDefAtivo: TSmallintField;
    frxGER_OSERV_014_001_A: TfrxReport;
    QrCunsCad: TmySQLQuery;
    QrCunsCadCodigo: TIntegerField;
    QrCunsCadAtivPrinc: TIntegerField;
    QrCunsCadHowFind: TIntegerField;
    QrCunsCadAccount: TIntegerField;
    QrCunsCadDataCon: TDateField;
    QrCunsCadNO_AtivPrinc: TWideStringField;
    QrCunsCadNO_HowFind: TWideStringField;
    QrCunsCadNO_Account: TWideStringField;
    frxDsCunsCad: TfrxDBDataset;
    QrSTerCad: TmySQLQuery;
    QrSTerCadCodigo: TIntegerField;
    QrSTerCadNome: TWideStringField;
    QrSTerCadCliente: TIntegerField;
    QrSTerCadSLograd: TSmallintField;
    QrSTerCadSRua: TWideStringField;
    QrSTerCadSNumero: TIntegerField;
    QrSTerCadSCompl: TWideStringField;
    QrSTerCadSBairro: TWideStringField;
    QrSTerCadSCidade: TWideStringField;
    QrSTerCadSUF: TWideStringField;
    QrSTerCadSCEP: TIntegerField;
    QrSTerCadSPais: TWideStringField;
    QrSTerCadSEndeRef: TWideStringField;
    QrSTerCadSCodMunici: TIntegerField;
    QrSTerCadSCodiPais: TIntegerField;
    QrSTerCadSTe1: TWideStringField;
    QrSTerCadM2Constru: TFloatField;
    QrSTerCadM2NaoBuild: TFloatField;
    QrSTerCadM2Terreno: TFloatField;
    QrSTerCadM2Total: TFloatField;
    QrSTerCadMyOrd: TIntegerField;
    QrSTerCadAtivo: TSmallintField;
    QrSTerCadNO_LOGRAD: TWideStringField;
    frxDsSTerCad: TfrxDBDataset;
    QrSTerCadNUM_TXT: TWideStringField;
    QrSTerCadCEP_TXT: TWideStringField;
    QrSTerCadCOD_TXT: TWideStringField;
    QrSTerCadTXT_CONSTRU: TWideStringField;
    QrSTerCadTXT_NAOBUILD: TWideStringField;
    QrSTerCadTXT_TERRENO: TWideStringField;
    QrSTerCadTXT_M2TOTAL: TWideStringField;
    frxDsSImaCad: TfrxDBDataset;
    QrSImaCad: TmySQLQuery;
    QrSImaCadNO_FIN: TWideStringField;
    QrSImaCadNO_OBJ: TWideStringField;
    QrSImaCadNO_TPC: TWideStringField;
    QrSImaCadCodigo: TIntegerField;
    QrSImaCadSiapImaTer: TIntegerField;
    QrSImaCadObjeto: TIntegerField;
    QrSImaCadFinalidade: TIntegerField;
    QrSImaCadTpConstru: TIntegerField;
    QrSImaCadSCompl2: TWideStringField;
    QrSImaCadM2Constru: TFloatField;
    QrSImaCadM2NaoBuild: TFloatField;
    QrSImaCadM2Terreno: TFloatField;
    QrSImaCadM2Total: TFloatField;
    QrSImaCadMyOrd: TIntegerField;
    QrSImaCadAtivo: TSmallintField;
    QrSImaDep: TmySQLQuery;
    frxDsSImaDep: TfrxDBDataset;
    QrSImaCdi: TmySQLQuery;
    frxDsSImaCdi: TfrxDBDataset;
    QrSImaCav: TmySQLQuery;
    frxDsSImaCav: TfrxDBDataset;
    QrSImaRes: TmySQLQuery;
    frxDsSImaRes: TfrxDBDataset;
    QrSImaCui: TmySQLQuery;
    frxDsSImaCui: TfrxDBDataset;
    QrSImaAti: TmySQLQuery;
    frxDsSImaAti: TfrxDBDataset;
    QrSImaCxa: TmySQLQuery;
    frxDsSImaCxa: TfrxDBDataset;
    QrSImaDepNO_DEP: TWideStringField;
    QrSImaDepCodigo: TIntegerField;
    QrSImaDepControle: TIntegerField;
    QrSImaDepDependenci: TIntegerField;
    QrSImaDepMLarg: TFloatField;
    QrSImaDepMComp: TFloatField;
    QrSImaDepMAltu: TFloatField;
    QrSImaDepMyOrd: TIntegerField;
    QrSImaDepAtivo: TSmallintField;
    QrSImaCdiNO_CARAC: TWideStringField;
    QrSImaCdiCodigo: TIntegerField;
    QrSImaCdiControle: TIntegerField;
    QrSImaCdiCaracteris: TIntegerField;
    QrSImaCdiMyOrd: TIntegerField;
    QrSImaCdiAtivo: TSmallintField;
    QrSImaCavNO_CARAC: TWideStringField;
    QrSImaCavCodigo: TIntegerField;
    QrSImaCavControle: TIntegerField;
    QrSImaCavCaracteris: TIntegerField;
    QrSImaCavMyOrd: TIntegerField;
    QrSImaCavAtivo: TSmallintField;
    QrSImaResNO_CUI: TWideStringField;
    QrSImaResCodigo: TIntegerField;
    QrSImaResControle: TIntegerField;
    QrSImaResResidente: TIntegerField;
    QrSImaResNome: TWideStringField;
    QrSImaResAnoNatal: TIntegerField;
    QrSImaResCuidado: TIntegerField;
    QrSImaResObservacao: TWideStringField;
    QrSImaResMyOrd: TIntegerField;
    QrSImaResAtivo: TSmallintField;
    QrSImaCuiNO_CUI: TWideStringField;
    QrSImaCuiCodigo: TIntegerField;
    QrSImaCuiControle: TIntegerField;
    QrSImaCuiCuidado: TIntegerField;
    QrSImaCuiMyOrd: TIntegerField;
    QrSImaCuiAtivo: TSmallintField;
    QrSImaAtiNO_ATI: TWideStringField;
    QrSImaAtiCodigo: TIntegerField;
    QrSImaAtiControle: TIntegerField;
    QrSImaAtiAtividade: TIntegerField;
    QrSImaAtiMyOrd: TIntegerField;
    QrSImaAtiAtivo: TSmallintField;
    QrSICxDef: TmySQLQuery;
    frxDsSICxDef: TfrxDBDataset;
    QrSICdDef: TmySQLQuery;
    frxDsSICdDef: TfrxDBDataset;
    QrSMovCad: TmySQLQuery;
    frxDsSMovCad: TfrxDBDataset;
    QrSMovDef: TmySQLQuery;
    frxDsSMovDef: TfrxDBDataset;
    QrSImaCxaNO_MATER: TWideStringField;
    QrSImaCxaNO_FORMA: TWideStringField;
    QrSImaCxaCodigo: TIntegerField;
    QrSImaCxaControle: TIntegerField;
    QrSImaCxaMatersCxa: TIntegerField;
    QrSImaCxaFormasCxa: TIntegerField;
    QrSImaCxaVolumeL: TFloatField;
    QrSImaCxaLocal: TWideStringField;
    QrSImaCxaAcesso: TWideStringField;
    QrSImaCxaMedidas: TWideStringField;
    QrSImaCxaMyOrd: TIntegerField;
    QrSImaCxaAtivo: TSmallintField;
    QrSICxDefID_Item: TIntegerField;
    QrSICxDefID_Sorc: TIntegerField;
    QrSICxDefAtrCad: TIntegerField;
    QrSICxDefAtrIts: TIntegerField;
    QrSICxDefCU_CAD: TIntegerField;
    QrSICxDefCU_ITS: TIntegerField;
    QrSICxDefNO_CAD: TWideStringField;
    QrSICxDefNO_ITS: TWideStringField;
    QrSICdDefID_Item: TIntegerField;
    QrSICdDefID_Sorc: TIntegerField;
    QrSICdDefAtrCad: TIntegerField;
    QrSICdDefAtrIts: TIntegerField;
    QrSICdDefCU_CAD: TIntegerField;
    QrSICdDefCU_ITS: TIntegerField;
    QrSICdDefNO_CAD: TWideStringField;
    QrSICdDefNO_ITS: TWideStringField;
    QrSMovCadCodigo: TIntegerField;
    QrSMovCadNome: TWideStringField;
    QrSMovCadTipo: TSmallintField;
    QrSMovCadCliente: TIntegerField;
    QrSMovCadMyOrd: TIntegerField;
    QrSMovCadAtivo: TSmallintField;
    QrSMovDefID_Item: TIntegerField;
    QrSMovDefID_Sorc: TIntegerField;
    QrSMovDefAtrCad: TIntegerField;
    QrSMovDefAtrIts: TIntegerField;
    QrSMovDefCU_CAD: TIntegerField;
    QrSMovDefCU_ITS: TIntegerField;
    QrSMovDefNO_CAD: TWideStringField;
    QrSMovDefNO_ITS: TWideStringField;
    QrSImaCadTXT_CONSTRU: TWideStringField;
    QrSImaCadTXT_NAOBUILD: TWideStringField;
    QrSImaCadTXT_TERRENO: TWideStringField;
    QrSImaCadTXT_M2TOTAL: TWideStringField;
    GroupBox6: TGroupBox;
    Panel16: TPanel;
    Panel17: TPanel;
    CkColore: TCheckBox;
    Label13: TLabel;
    EdAltLinOS: TdmkEdit;
    RGBrushStyle: TRadioGroup;
    QrSImaDepTXT_Comp: TWideStringField;
    QrSImaDepTXT_Larg: TWideStringField;
    QrSImaDepTXT_Altu: TWideStringField;
    QrSImaResNO_RES: TWideStringField;
    QrSImaResTXT_NATAL: TWideStringField;
    QrSImaResOBS_EM_UMA_LIN: TWideStringField;
    QrSImaCxaACESSO_EM_UMA_LIN: TWideStringField;
    QrSMovCadNO_TIPO: TWideStringField;
    QrSImaCxaTXT_VOL: TWideStringField;
    QrSMovCadTXT_TIPO: TWideStringField;
    QrOSFrmAbr: TmySQLQuery;
    frxDsOSFrmAbr: TfrxDBDataset;
    QrOSFrmAbrNO_ABRANGE: TWideStringField;
    QrOSFrmAbrCodigo: TIntegerField;
    QrOSFrmAbrControle: TIntegerField;
    QrOSFrmAbrConta: TIntegerField;
    QrOSFrmAbrIDIts: TIntegerField;
    QrOSFrmAbrAbrangicie: TIntegerField;
    Label27: TLabel;
    EdQtdOSFrmAbr: TdmkEdit;
    QrOFA: TmySQLQuery;
    QrOFANO_ABRANGE: TWideStringField;
    QrOFACodigo: TIntegerField;
    QrOFAControle: TIntegerField;
    QrOFAConta: TIntegerField;
    QrOFAIDIts: TIntegerField;
    QrOFAAbrangicie: TIntegerField;
    QrOFAAtivo: TSmallintField;
    QrOSFrmAbrAtivo: TSmallintField;
    QrOSCabTel_ENT: TWideStringField;
    QrOSCabTXTTel_ENT: TWideStringField;
    QrEntiTel: TmySQLQuery;
    frxDsEntiTel: TfrxDBDataset;
    QrET: TmySQLQuery;
    QrETNOMEETC: TWideStringField;
    QrETCodigo: TIntegerField;
    QrETTelefone: TWideStringField;
    QrETRamal: TWideStringField;
    QrETTEL_TXT: TWideStringField;
    TabSheet4: TTabSheet;
    Panel11: TPanel;
    QrOSCabNO_ENTICONTAT: TWideStringField;
    QrOSCabTxtVisExePos: TWideStringField;
    QrOAObservacao: TWideMemoField;
    QrOSAlvObservacao: TWideMemoField;
    QrOSAlvNivSup: TIntegerField;
    QrOSAlvEspecie: TWideStringField;
    QrOSAlvArqDir: TWideStringField;
    QrOSAlvCopyright: TWideStringField;
    QrOSAlvArqImg: TWideStringField;
    QrOSAlvMyOrd: TIntegerField;
    QrOFDIDIts: TIntegerField;
    QrOMD: TmySQLQuery;
    QrOMDCodigo: TIntegerField;
    QrOMDControle: TIntegerField;
    QrOMDConta: TIntegerField;
    QrOMDTabela: TSmallintField;
    QrOMDCadastro: TIntegerField;
    QrOMDSIGLA: TWideStringField;
    QrOMDNO_Campo: TWideStringField;
    QrOMDAtivo: TSmallintField;
    QrOMDIDIts: TIntegerField;
    Label2: TLabel;
    EdQtdOSFrmDep: TdmkEdit;
    Label28: TLabel;
    _Ed_Qtd_OS_Mon_Dep_: TdmkEdit;
    QrOSFrmDep: TmySQLQuery;
    frxDsOSFrmDep: TfrxDBDataset;
    QrOSAllDep: TmySQLQuery;
    QrOSAllDepSIGLA: TWideStringField;
    QrOSAllDepNO_Campo: TWideStringField;
    QrDesServico: TmySQLQuery;
    frxDsDesServico: TfrxDBDataset;
    QrDesServicoCodigo: TIntegerField;
    QrDesServicoNome: TWideStringField;
    QrDesServicoSigla: TWideStringField;
    QrDesServicoObservacao: TWideMemoField;
    QrAnaliseN1: TFloatField;
    QrAnaliseID: TFloatField;
    QrAnaliseATU: TFloatField;
    QrAnaliseMAX: TFloatField;
    QrOSPrzVALOR_BRUTO: TFloatField;
    QrOSPrzVALOR_PARCELA: TFloatField;
    QrOSPrzVALOR_TOTAL: TFloatField;
    QrOPParcelas: TIntegerField;
    QrOSPrzParcelas: TIntegerField;
    QrOSCabOrcamTotal: TFloatField;
    QrOCDtaLibFat: TDateTimeField;
    QrOCDtaFimFat: TDateTimeField;
    QrOCValorOutrs: TFloatField;
    QrOCCondicaoPg: TIntegerField;
    QrOCCartEmis: TIntegerField;
    QrOCSerNF: TWideStringField;
    QrOCNumNF: TIntegerField;
    QrOCInvalServi: TFloatField;
    QrOCInvalDesco: TFloatField;
    QrOCInvalOutrs: TFloatField;
    QrOCInvalTotal: TFloatField;
    QrOCOrcamServi: TFloatField;
    QrOCOrcamDesco: TFloatField;
    QrOCOrcamOutrs: TFloatField;
    QrOCOrcamTotal: TFloatField;
    QrOCValiDdOrca: TIntegerField;
    QrOSCabValiDdOrca: TIntegerField;
    QrOSAreDep: TmySQLQuery;
    QrOSAreDepMLarg: TFloatField;
    QrOSAreDepMComp: TFloatField;
    QrOSAreDepMAltu: TFloatField;
    QrETNO_CONTATO: TWideStringField;
    QrETNATAL_TXT: TWideStringField;
    QrETDtaNatal: TDateField;
    QrETNO_CARGO: TWideStringField;
    QrEntiTelCodigo: TIntegerField;
    QrEntiTelNO_CONTATO: TWideStringField;
    QrEntiTelNO_CARGO: TWideStringField;
    QrEntiTelDtaNatal: TDateField;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelRamal: TWideStringField;
    QrEntiTelNATAL_TXT: TWideStringField;
    QrEntiTelTEL_TXT: TWideStringField;
    QrEntiTelMyOrd: TIntegerField;
    QrEntiTelAtivo: TSmallintField;
    QrEM: TmySQLQuery;
    QrEntiEma: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField8: TWideStringField;
    StringField9: TWideStringField;
    DateField2: TDateField;
    StringField10: TWideStringField;
    StringField13: TWideStringField;
    IntegerField3: TIntegerField;
    SmallintField1: TSmallintField;
    frxDsEntiEma: TfrxDBDataset;
    QrEMCodigo: TIntegerField;
    QrEMNO_CONTATO: TWideStringField;
    QrEMNO_CARGO: TWideStringField;
    QrEMDtaNatal: TDateField;
    QrEMNOMEETC: TWideStringField;
    QrEMEMail: TWideStringField;
    QrEntiEmaEMail: TWideStringField;
    QrEMNATAL_TXT: TWideStringField;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    Panel18: TPanel;
    Panel19: TPanel;
    QrOSFPS: TmySQLQuery;
    frxDsOSFPS: TfrxDBDataset;
    QrOSFPSCodigo: TIntegerField;
    QrOSFPSControle: TIntegerField;
    QrOSFPSCondicao: TIntegerField;
    QrOSFPSNO_CONDICAO: TWideStringField;
    QrOSFPSDescoPer: TFloatField;
    QrOSFPSEscolhido: TSmallintField;
    QrOSFPSParcelas: TIntegerField;
    QrOSFPSAtivo: TSmallintField;
    QrOSFPSVALOR_BRUTO: TFloatField;
    QrOSFPSVALOR_PARCELA: TFloatField;
    QrOSFPSVALOR_TOTAL: TFloatField;
    QrOSCabExeTxtCli1: TIntegerField;
    QrOSCabNO_ExeTxtCli1: TWideStringField;
    QrOCExeTxtCli2: TIntegerField;
    QrOSCabTXT_ExeTxtCli1: TWideMemoField;
    QrOSCabInvalTotal: TFloatField;
    QrOSCabInvalServi: TFloatField;
    QrOSCabInvalDesco: TFloatField;
    QrOSCabOrcamServi: TFloatField;
    QrOSCabOrcamDesco: TFloatField;
    QrOSCabMyOrd: TIntegerField;
    QrOSCabNO_CONTRATANTE: TWideStringField;
    QrOSCabNO_PAGANTE: TWideStringField;
    QrOSCxa: TmySQLQuery;
    QrOSCxaCodigo: TIntegerField;
    QrOSCxaControle: TIntegerField;
    QrOSCxaCaixa: TIntegerField;
    QrOSCxaGarantiaDd: TIntegerField;
    QrOSCxaHrEvacuar: TIntegerField;
    QrOSCxaHrExecutar: TFloatField;
    QrOSCxaValCalc: TFloatField;
    QrOSCxaValInfo: TFloatField;
    QrOSCxaValDesc: TFloatField;
    QrOSCxaValTota: TFloatField;
    QrOSCxaAutorizado: TSmallintField;
    QrOSCxaAtivo: TSmallintField;
    QrOSCxaNO_MATERIAL: TWideStringField;
    QrOSCxaNOME_FORMA: TWideStringField;
    QrOSCxaMatersCxa: TIntegerField;
    QrOSCxaFormasCxa: TIntegerField;
    QrOSCxaVolumeL: TFloatField;
    QrOSCxaLocal: TWideStringField;
    QrOSCxaAcesso: TWideStringField;
    QrOSCxaMedidas: TWideStringField;
    frxDsOSCxa: TfrxDBDataset;
    QrOSCxaAUTORIZADO_BOOL: TBooleanField;
    QrOSCxaVAL_CALCeINFO: TFloatField;
    QrOSCabOperacao: TIntegerField;
    QrOCOperacao: TIntegerField;
    QrOCValorPre: TFloatField;
    QrOX: TmySQLQuery;
    QrOXCodigo: TIntegerField;
    QrOXControle: TIntegerField;
    QrOXCaixa: TIntegerField;
    QrOXGarantiaDd: TIntegerField;
    QrOXHrEvacuar: TIntegerField;
    QrOXHrExecutar: TFloatField;
    QrOXValCalc: TFloatField;
    QrOXValInfo: TFloatField;
    QrOXValDesc: TFloatField;
    QrOXValTota: TFloatField;
    QrOXAutorizado: TSmallintField;
    QrOXAtivo: TSmallintField;
    Panel20: TPanel;
    GroupBox7: TGroupBox;
    Panel21: TPanel;
    Label29: TLabel;
    EdQtdOSCxa: TdmkEdit;
    QrChekLstIts: TmySQLQuery;
    frxDsChekLstIts: TfrxDBDataset;
    QrOSCxaChekLstCab: TIntegerField;
    QrOXChekLstCab: TIntegerField;
    QrChekLstItsNO_ChekLstCab: TWideStringField;
    QrChekLstItsCodigo: TIntegerField;
    QrChekLstItsOrdem: TIntegerField;
    QrChekLstItsReordem: TIntegerField;
    QrChekLstItsControle: TIntegerField;
    QrChekLstItsNome: TWideStringField;
    QrOSCabObsExecuta: TWideMemoField;
    QrOCObsExecuta: TWideMemoField;
    QrOSSrvDATA_GARANTIA: TDateField;
    QrOSSrvTXT_DATA_GARANTIA: TWideStringField;
    frxDsOSCxI: TfrxDBDataset;
    QrOSCxI: TmySQLQuery;
    QrOSCxICodigo: TIntegerField;
    QrOSCxIControle: TIntegerField;
    QrOSCxIFotoCxa: TWideStringField;
    QrOSCxIDetalhes: TWideMemoField;
    GroupBox8: TGroupBox;
    Memo1: TMemo;
    QrOSAge: TmySQLQuery;
    frxDsOSAge: TfrxDBDataset;
    QrOG: TmySQLQuery;
    QrOGCodigo: TIntegerField;
    QrOGControle: TIntegerField;
    QrOGAgente: TIntegerField;
    QrOGResponsa: TSmallintField;
    QrOGNO_RESPONSA: TWideStringField;
    QrOGNO_Agente: TWideStringField;
    QrOGAtivo: TSmallintField;
    QrItens: TmySQLQuery;
    QrItensApMo: TLargeintField;
    QrItensCodigo: TIntegerField;
    QrItensControle: TIntegerField;
    QrItensNO_Diluente: TWideStringField;
    QrItensNO_EQUIP: TWideStringField;
    QrItensNO_TIPOAPLICA: TWideStringField;
    QrItensQtdTot: TFloatField;
    QrItensDiluente: TSmallintField;
    QrItensUsoQtd: TFloatField;
    QrItensEhDiluente: TSmallintField;
    QrItensFoneCIT: TWideStringField;
    QrItensFoneCEATOX: TWideStringField;
    QrItensToxicidade: TWideStringField;
    QrItensConcentrac: TWideStringField;
    QrItensAcaoToxica: TWideStringField;
    QrItensAntidoto: TWideStringField;
    QrItensNO_GG1: TWideStringField;
    QrItensTIPO: TWideStringField;
    QrItensRegMinSaud: TWideStringField;
    QrItensObservacao: TWideMemoField;
    QrItensNO_MARCA: TWideStringField;
    QrItensNO_FABRICA: TWideStringField;
    QrGraG1PrPA: TmySQLQuery;
    QrItensGraGru1: TIntegerField;
    QrItensGraGruX: TIntegerField;
    QrGraG1PrPAPRINCIPIO_ATIVO: TWideStringField;
    QrGraG1PrPAGRUPO_QUIMICO: TWideStringField;
    QrOSTox5: TmySQLQuery;
    frxDsOSTox5: TfrxDBDataset;
    QrOSAgeCodigo: TIntegerField;
    QrOSAgeOrdem: TIntegerField;
    QrOSAgeControle: TIntegerField;
    QrOSAgeAgente: TIntegerField;
    QrOSAgeResponsa: TIntegerField;
    QrOSAgeMyOrd: TIntegerField;
    QrOSAgeAtivo: TSmallintField;
    QrOSAgeNO_RESPONSA: TWideStringField;
    QrOSAgeNO_Agente: TWideStringField;
    QrOSTox5ApMo: TSmallintField;
    QrOSTox5Codigo: TIntegerField;
    QrOSTox5Controle: TIntegerField;
    QrOSTox5Conta: TIntegerField;
    QrOSTox5IDIts: TIntegerField;
    QrOSTox5TIPO: TWideStringField;
    QrOSTox5NO_Diluente: TWideStringField;
    QrOSTox5NO_EQUIP: TWideStringField;
    QrOSTox5NO_TIPOAPLICA: TWideStringField;
    QrOSTox5QtdTot: TFloatField;
    QrOSTox5Diluente: TSmallintField;
    QrOSTox5UsoQtd: TFloatField;
    QrOSTox5EhDiluente: TSmallintField;
    QrOSTox5RegMinSaud: TWideStringField;
    QrOSTox5FoneCIT: TWideStringField;
    QrOSTox5FoneCEATOX: TWideStringField;
    QrOSTox5Toxicidade: TWideStringField;
    QrOSTox5Concentrac: TWideStringField;
    QrOSTox5AcaoToxica: TWideStringField;
    QrOSTox5Antidoto: TWideStringField;
    QrOSTox5NO_GG1: TWideStringField;
    QrOSTox5NO_MARCA: TWideStringField;
    QrOSTox5NO_FABRICA: TWideStringField;
    QrOSTox5Observacao: TWideMemoField;
    QrOSTox5MyOrd: TIntegerField;
    QrOSTox5Ativo: TSmallintField;
    QrOSToz: TmySQLQuery;
    frxDsOSToz: TfrxDBDataset;
    QrOSTozApMo: TSmallintField;
    QrOSTozCodigo: TIntegerField;
    QrOSTozControle: TIntegerField;
    QrOSTozConta: TIntegerField;
    QrOSTozIDIts: TIntegerField;
    QrOSTozMyOrd: TIntegerField;
    QrOSTozAtivo: TSmallintField;
    QrOSTozPRINCIPIO_ATIVO: TWideStringField;
    QrOSTozGRUPO_QUIMICO: TWideStringField;
    QrOSTox5NO_MARCA_E_FAB: TWideStringField;
    QrItensSIGLA_TOT: TWideStringField;
    QrItensSIGLA_USO: TWideStringField;
    QrOSTox5SIGLA_USO: TWideStringField;
    QrOSTox5SIGLA_TOT: TWideStringField;
    QrLoc1: TmySQLQuery;
    QrLoc1GraGruX: TIntegerField;
    QrOSCxaDetalhes: TWideMemoField;
    QrOXDetalhes: TWideMemoField;
    QrOSAlvPrevencao: TWideMemoField;
    QrCdi: TmySQLQuery;
    QrCdiNO_CARACTERIS: TWideStringField;
    QrLoc: TmySQLQuery;
    QrLocCodigo: TIntegerField;
    QrLocSCompl2: TWideStringField;
    QrDep: TmySQLQuery;
    QrDepSIGLA: TWideStringField;
    QrDepo4Ter: TLargeintField;
    QrDepc4Ter: TLargeintField;
    QrDepn4Ter: TWideStringField;
    QrDepa4Ter: TLargeintField;
    QrDepo3Loc: TLargeintField;
    QrDepc3Loc: TLargeintField;
    QrDepn3Loc: TWideStringField;
    QrDepa3Loc: TLargeintField;
    QrDepo2Typ: TLargeintField;
    QrDepc2Typ: TIntegerField;
    QrDepn2Typ: TWideStringField;
    QrDepa2Typ: TLargeintField;
    QrDepo1Dep: TLargeintField;
    QrDepc1dep: TIntegerField;
    QrDepn1Dep: TWideStringField;
    QrDepa1Dep: TLargeintField;
    QrDepTabela: TLargeintField;
    QrDepAtivo: TLargeintField;
    QrCdiControle: TIntegerField;
    QrCdiCaracteris: TIntegerField;
    frxDsLoc: TfrxDBDataset;
    frxDsCdi: TfrxDBDataset;
    QrCav: TmySQLQuery;
    frxDsCav: TfrxDBDataset;
    QrCavNO_CARACTERIS: TWideStringField;
    QrCavControle: TIntegerField;
    QrCavCaracteris: TIntegerField;
    QrRes: TmySQLQuery;
    frxDsRes: TfrxDBDataset;
    QrResNome: TWideStringField;
    QrResIDADE: TWideStringField;
    QrResNO_CUI: TWideStringField;
    QrResObservacao: TWideStringField;
    QrCui: TmySQLQuery;
    frxDsCui: TfrxDBDataset;
    QrAti: TmySQLQuery;
    frxDsAti: TfrxDBDataset;
    QrAtr: TmySQLQuery;
    frxDsAtr: TfrxDBDataset;
    QrCuiNO_CUIDADO: TWideStringField;
    QrAtiNO_ATIVIDADE: TWideStringField;
    QrAtrNO_CAD: TWideStringField;
    QrAtrNO_ITS: TWideStringField;
    QrOSTox3: TmySQLQuery;
    frxDsOSTox3: TfrxDBDataset;
    QrOSTox3RegMinSaud: TWideStringField;
    QrOSTox3NO_GG1: TWideStringField;
    QrOSTox3NO_MARCA_E_FAB: TWideStringField;
    QrOSTox3NO_MARCA: TWideStringField;
    QrOSTox3NO_FABRICA: TWideStringField;
    QrOSFrmRecSIGLA: TWideStringField;
    QrOSMonRecSIGLA: TWideStringField;
    QrOSCabExeTxtCli2: TIntegerField;
    QrOSCabNO_ExeTxtCli2: TWideStringField;
    QrOSCabTXT_ExeTxtCli2: TWideMemoField;
    QrOCExeTxtCli1: TIntegerField;
    QrOSCxaAtr: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    StringField1: TWideStringField;
    StringField2: TWideStringField;
    frxDsOSCxaAtr: TfrxDBDataset;
    QrOSCxaTXT_DATA_GARANTIA: TWideStringField;
    QrOSCxaDATA_GARANTIA: TDateField;
    QrOSCxaTIT_ChekLstCab: TWideStringField;
    frxGER_OSERV_014_003_A: TfrxReport;
    QrDepsA: TmySQLQuery;
    frxDsDepsA: TfrxDBDataset;
    QrDepsAc3Loc: TFloatField;
    QrDepsAn3Loc: TWideStringField;
    QrDepsAn2Typ: TWideStringField;
    QrDepsASIGLA: TWideStringField;
    QrDepsAn1Dep: TWideStringField;
    QrOSFrmDepc3Loc: TFloatField;
    QrOSFrmDepn3Loc: TWideStringField;
    QrOSFrmDepn2Typ: TWideStringField;
    QrOSFrmDepSIGLA: TWideStringField;
    QrOSFrmDepn1Dep: TWideStringField;
    QrOSFrmDepAtivo: TSmallintField;
    QrOSFrmDepSIGLA_E_LOC: TWideStringField;
    frxGER_OSERV_014_006_A: TfrxReport;
    QrOSChk: TmySQLQuery;
    frxDsOSChk: TfrxDBDataset;
    QrOSChkCodigo: TIntegerField;
    QrOSChkControle: TIntegerField;
    QrOSChkChekLstIts: TIntegerField;
    QrOSChkNO_LstIts: TWideStringField;
    QrDepsAc2Typ: TFloatField;
    QrOSFrmDepc2Typ: TFloatField;
    Label8: TLabel;
    EdGrupo: TdmkEdit;
    Panel22: TPanel;
    PnLugarOpcao: TPanel;
    DBGLugar: TdmkDBGridDAC;
    DBGOpcao: TDBGrid;
    QrLugares: TmySQLQuery;
    QrLugaresLugar: TIntegerField;
    QrLugaresNO_Lugar: TWideStringField;
    DsLugares: TDataSource;
    QrOpcoes: TmySQLQuery;
    QrOpcoesOpcao: TIntegerField;
    DsOpcoes: TDataSource;
    QrLugaresAtivo: TSmallintField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrLugaresOSCab: TIntegerField;
    SpLugarOpcao: TSplitter;
    frxDsSelLug: TfrxDBDataset;
    QrSelLug: TmySQLQuery;
    QrSelLugLugar: TIntegerField;
    QrSelLugNO_Lugar: TWideStringField;
    QrSelLugAtivo: TSmallintField;
    QrSelLugOSCab: TIntegerField;
    frxReport1: TfrxReport;
    frxReport2: TfrxReport;
    frxReport3: TfrxReport;
    QrSelSTC: TmySQLQuery;
    QrSelSTCLugar: TIntegerField;
    QrSelSTCNO_Lugar: TWideStringField;
    QrSelSTCAtivo: TSmallintField;
    QrSelSTCOSCab: TIntegerField;
    QrSTerCadLugInfo: TIntegerField;
    Label30: TLabel;
    EdQtdOSAge: TdmkEdit;
    RGOSStartNewPage: TRadioGroup;
    frxReport4: TfrxReport;
    QrLocais: TmySQLQuery;
    QrLocaisc3Loc: TFloatField;
    QrLocaisn3Loc: TWideStringField;
    QrTipos: TmySQLQuery;
    QrTiposc2Typ: TFloatField;
    QrTiposn2Typ: TWideStringField;
    QrDeptos: TmySQLQuery;
    QrDeptosSIGLA: TWideStringField;
    QrDeptosn1Dep: TWideStringField;
    frxDsLocais: TfrxDBDataset;
    frxDsTipos: TfrxDBDataset;
    frxDsDeptos: TfrxDBDataset;
    QrLocaisTABELA: TSmallintField;
    QrTiposTypLabel: TWideStringField;
    QrSumOS: TmySQLQuery;
    QrSumOSOrcamTotal: TFloatField;
    QrSumPrz: TmySQLQuery;
    QrSumPrzCondicao: TIntegerField;
    QrSumPrzNO_CONDICAO: TWideStringField;
    QrSumPrzDescoPer: TFloatField;
    QrSumPrzEscolhido: TSmallintField;
    QrSumPrzParcelas: TIntegerField;
    QrSumPrzAtivo: TSmallintField;
    QrSumPrzVALOR_BRUTO: TFloatField;
    QrSumPrzVALOR_PARCELA: TFloatField;
    QrSumPrzVALOR_TOTAL: TFloatField;
    frxDsSumPrz: TfrxDBDataset;
    QrDiarioAdd: TmySQLQuery;
    QrDiarioAddCodigo: TIntegerField;
    QrDiarioAddData: TDateField;
    QrDiarioAddHora: TTimeField;
    QrDiarioAddNome: TWideStringField;
    frxDsDiarioAdd: TfrxDBDataset;
    frxGER_OSERV_014_002_A: TfrxReport;
    QrDepsB: TmySQLQuery;
    frxDsDepsB: TfrxDBDataset;
    QrDepsBc3Loc: TFloatField;
    QrDepsBn3Loc: TWideStringField;
    QrDepsBn2Typ: TWideStringField;
    QrDepsBSIGLA: TWideStringField;
    QrDepsBn1Dep: TWideStringField;
    QrDepsBc2Typ: TFloatField;
    EdTel_ENT: TdmkEdit;
    Label31: TLabel;
    QrOSSta: TmySQLQuery;
    QrOSStaNO_STATUS: TWideStringField;
    QrOSStaCodigo: TIntegerField;
    QrOSStaControle: TIntegerField;
    QrOSStaStatus: TIntegerField;
    QrOSStaFeito: TSmallintField;
    QrSumOSOrcamDesco: TFloatField;
    TabSheet7: TTabSheet;
    Panel23: TPanel;
    frxGER_OSERV_014_005_B: TfrxReport;
    QrOSPipMon: TmySQLQuery;
    QrOSPipMonNO_PIP: TWideStringField;
    QrOSPipMonCodigo: TIntegerField;
    QrOSPipMonControle: TIntegerField;
    QrOSPipMonPipCad: TIntegerField;
    frxDsOSPipMon: TfrxDBDataset;
    QrOSPipMonPrgLstCab: TIntegerField;
    QrPrgLstIts: TmySQLQuery;
    CkDoLocalize: TCheckBox;
    QrSumOSOrcamServi: TFloatField;
    QrSumOSValorServi: TFloatField;
    QrCunsCadImpImaInfo: TSmallintField;
    CkImpImaInfo: TCheckBox;
    QrOSSrvMyOrd: TIntegerField;
    QrOSMoniDdTotl: TIntegerField;
    QrOSMoniDdIntv: TIntegerField;
    QrOSSrvPERIODO_MONIT: TWideStringField;
    QrOSSrvMoniDdTotl: TIntegerField;
    QrOSSrvMoniDdIntv: TIntegerField;
    QrOSCabXtr: TmySQLQuery;
    QrOSCabXtrCodigo: TIntegerField;
    QrOSCabXtrControle: TIntegerField;
    QrOSCabXtrDtHrIni: TDateTimeField;
    QrOSCabXtrDtHrFim: TDateTimeField;
    frxDsOSCabXtr: TfrxDBDataset;
    QrOSPipMonNO_PrgLstCab: TWideStringField;
    QrOSPipMonOrdem: TIntegerField;
    QrOSPipMonReordem: TIntegerField;
    QrOSPipMonLst: TmySQLQuery;
    QrOSPipMonLstCodigo: TIntegerField;
    QrOSPipMonLstNome: TWideStringField;
    QrOSPipMonIts: TmySQLQuery;
    QrOSPipMonItsNO_PERGUNTA: TWideStringField;
    QrOSPipMonItsSigla: TWideStringField;
    QrOSPipMonItsCodigo: TIntegerField;
    QrOSPipMonItsControle: TIntegerField;
    QrOSPipMonItsConta: TLargeintField;
    QrOSPipMonItsPrgLstCab: TIntegerField;
    QrOSPipMonItsPrgLstIts: TIntegerField;
    QrOSPipMonItsOrdem: TIntegerField;
    QrOSPipMonItsSubOrdem: TIntegerField;
    QrOSPipMonItsTabela: TSmallintField;
    QrOSPipMonItsGraGruX: TIntegerField;
    QrOSPipMonItsFuncoes: TSmallintField;
    QrOSPipMonItsDependente: TSmallintField;
    QrOSPipMonItsPrgAtrCad: TIntegerField;
    QrOSPipMonItsFiliacao: TIntegerField;
    QrOSPipMonItsRelacao: TIntegerField;
    QrOSPipMonItsNivel: TIntegerField;
    QrOSPipMonItsNivSeq: TIntegerField;
    QrOSPipMonItsPergunta: TIntegerField;
    QrOSPipMonItsBinarCad0: TIntegerField;
    QrOSPipMonItsBinarCad1: TIntegerField;
    frxDsOSPipML: TfrxDBDataset;
    frxDsOSPipMI: TfrxDBDataset;
    QrOSPipMonItsNO_PIP: TWideStringField;
    QrOSPipMonItsNO_ITEM: TWideStringField;
    QrPrgCadPrg: TmySQLQuery;
    QrPrgCadPrgSigla: TWideStringField;
    QrPrgCadPrgLargura: TIntegerField;
    QrSemRef: TmySQLQuery;
    frxGER_OSERV_014_005_Err1: TfrxReport;
    frxDsSemRef: TfrxDBDataset;
    QrSemRefNivel1: TIntegerField;
    QrSemRefNome: TWideStringField;
    QrOSPipMonItsLupForma: TSmallintField;
    QrOSPipMonItsLupQtdVzs: TSmallintField;
    QrOSPipMonItsLupSeqRsp: TIntegerField;
    QrOSPipMonItsSobreOrd: TIntegerField;
    QrOSPipMonItsTabIdx: TLargeintField;
    QrOSPipMonItsAcaoPrd: TSmallintField;
    QrOSPipMonSub: TmySQLQuery;
    QrOSPipMonSubNO_PERGUNTA: TWideStringField;
    QrOSPipMonSubSigla: TWideStringField;
    QrOSPipMonSubCodigo: TIntegerField;
    QrOSPipMonSubControle: TIntegerField;
    QrOSPipMonSubConta: TLargeintField;
    QrOSPipMonSubPrgLstCab: TIntegerField;
    QrOSPipMonSubPrgLstIts: TIntegerField;
    QrOSPipMonSubOrdem: TIntegerField;
    QrOSPipMonSubSubOrdem: TIntegerField;
    QrOSPipMonSubTabela: TSmallintField;
    QrOSPipMonSubGraGruX: TIntegerField;
    QrOSPipMonSubFuncoes: TSmallintField;
    QrOSPipMonSubDependente: TSmallintField;
    QrOSPipMonSubPrgAtrCad: TIntegerField;
    QrOSPipMonSubFiliacao: TIntegerField;
    QrOSPipMonSubRelacao: TIntegerField;
    QrOSPipMonSubNivel: TIntegerField;
    QrOSPipMonSubNivSeq: TIntegerField;
    QrOSPipMonSubPergunta: TIntegerField;
    QrOSPipMonSubBinarCad0: TIntegerField;
    QrOSPipMonSubBinarCad1: TIntegerField;
    QrOSPipMonSubNO_PIP: TWideStringField;
    QrOSPipMonSubNO_ITEM: TWideStringField;
    QrOSPipMonSubLupForma: TSmallintField;
    QrOSPipMonSubLupQtdVzs: TSmallintField;
    QrOSPipMonSubLupSeqRsp: TIntegerField;
    QrOSPipMonSubSobreOrd: TIntegerField;
    QrOSPipMonSubTabIdx: TLargeintField;
    QrOSPipMonSubAcaoPrd: TSmallintField;
    QrOSPipMI: TmySQLQuery;
    QrOSPipML: TmySQLQuery;
    QrOSPipMLCodigo: TIntegerField;
    QrOSPipMLNome: TWideStringField;
    QrOSPipMINO_PIP: TWideStringField;
    QrOSPipMINO_ITEM: TWideStringField;
    QrOSPipMISigla: TWideStringField;
    QrOSPipMITabela: TSmallintField;
    QrOSPipMICodigo: TIntegerField;
    QrOSPipMIControle: TIntegerField;
    QrOSPipMIConta: TLargeintField;
    QrOSPipMIOrdem: TIntegerField;
    QrOSPipMISubOrdem: TIntegerField;
    QrOSPipMISobreOrd: TIntegerField;
    QrOSPipMIPrgLstCab: TIntegerField;
    QrOSPipMIPrgLstIts: TIntegerField;
    QrOSPipMILupForma: TSmallintField;
    QrOSPipMILupQtdVzs: TSmallintField;
    QrOSPipMILupSeqRsp: TIntegerField;
    QrOSPipMIMyOrd: TIntegerField;
    QrOSPipMIAtivo: TSmallintField;
    QrOSPipMISeq: TIntegerField;
    frxDsOSPrv: TfrxDBDataset;
    QrOSPrv: TmySQLQuery;
    QrOSPrvCodigo: TIntegerField;
    QrOSPrvControle: TIntegerField;
    QrOSPrvDtHrContat: TDateTimeField;
    QrOSPrvFormContat: TIntegerField;
    QrOSPrvCliente: TIntegerField;
    QrOSPrvContato: TIntegerField;
    QrOSPrvAgente: TIntegerField;
    QrOSPrvNome: TWideStringField;
    QrOSPrvNO_Cliente: TWideStringField;
    QrOSPrvNO_Agente: TWideStringField;
    QrOSPrvNO_Contato: TWideStringField;
    QrOSPrvNO_FormContat: TWideStringField;
    QrItensConta: TLargeintField;
    QrItensIDIts: TLargeintField;
    QrOSPipMonSubRESPOSTA: TWideStringField;
    QrOSPipMonItsRESPOSTA: TWideStringField;
    QrOSPipMIRESPOSTA: TWideMemoField;
    QrOSPipMonItsSuperOrd: TIntegerField;
    QrOSPipMonItsSuperSub: TIntegerField;
    QrOSPipMISuperOrd: TIntegerField;
    QrOSPipMISuperSub: TIntegerField;
    QrOSPipMonSubSuperOrd: TIntegerField;
    QrOSPipMonSubSuperSub: TIntegerField;
    QrOSRespMI: TmySQLQuery;
    frxDsOSRespMI: TfrxDBDataset;
    QrOSRespMINO_PIP: TWideStringField;
    QrOSRespMIControle: TIntegerField;
    QrOSRespOK: TmySQLQuery;
    frxDsOSRespOK: TfrxDBDataset;
    QrOSRespOKNO_PIP: TWideStringField;
    QrOSRespOKNO_ITEM: TWideStringField;
    QrOSRespOKSigla: TWideStringField;
    QrOSRespOKTabela: TSmallintField;
    QrOSRespOKCodigo: TIntegerField;
    QrOSRespOKControle: TIntegerField;
    QrOSRespOKConta: TLargeintField;
    QrOSRespOKSuperOrd: TIntegerField;
    QrOSRespOKSuperSub: TIntegerField;
    QrOSRespOKOrdem: TIntegerField;
    QrOSRespOKSubOrdem: TIntegerField;
    QrOSRespOKSobreOrd: TIntegerField;
    QrOSRespOKPrgLstCab: TIntegerField;
    QrOSRespOKPrgLstIts: TIntegerField;
    QrOSRespOKLupForma: TSmallintField;
    QrOSRespOKLupQtdVzs: TSmallintField;
    QrOSRespOKLupSeqRsp: TIntegerField;
    QrOSRespOKRESPOSTA: TWideMemoField;
    QrOSRespOKSeq: TIntegerField;
    QrOSRespOKMyOrd: TIntegerField;
    QrOSRespOKAtivo: TSmallintField;
    QrOSRespOKPERGUNTA: TWideStringField;
    QrOSTox6: TmySQLQuery;
    frxDsOSTox6: TfrxDBDataset;
    QrOSTox6NO_Diluente: TWideStringField;
    QrOSTox6NO_EQUIP: TWideStringField;
    QrOSTox6NO_TIPOAPLICA: TWideStringField;
    QrOSTox6QtdTot: TFloatField;
    QrOSTox6UsoQtd: TFloatField;
    QrOSTox6RegMinSaud: TWideStringField;
    QrOSTox6FoneCIT: TWideStringField;
    QrOSTox6FoneCEATOX: TWideStringField;
    QrOSTox6Toxicidade: TWideStringField;
    QrOSTox6Concentrac: TWideStringField;
    QrOSTox6AcaoToxica: TWideStringField;
    QrOSTox6Antidoto: TWideStringField;
    QrOSTox6NO_GG1: TWideStringField;
    QrOSTox6NO_MARCA: TWideStringField;
    QrOSTox6NO_FABRICA: TWideStringField;
    QrOSTox6NO_MARCA_E_FAB: TWideStringField;
    QrOSTox6SIGLA_USO: TWideStringField;
    QrOSTox6SIGLA_TOT: TWideStringField;
    QrOSToz6: TmySQLQuery;
    frxDsOSToz6: TfrxDBDataset;
    QrOSToz6ApMo: TSmallintField;
    QrOSToz6Codigo: TIntegerField;
    QrOSToz6Controle: TIntegerField;
    QrOSToz6Conta: TIntegerField;
    QrOSToz6IDIts: TIntegerField;
    QrOSToz6MyOrd: TIntegerField;
    QrOSToz6Ativo: TSmallintField;
    QrOSToz6PRINCIPIO_ATIVO: TWideStringField;
    QrOSToz6GRUPO_QUIMICO: TWideStringField;
    QrOSTox6ApMo: TSmallintField;
    QrOSTox6Codigo: TIntegerField;
    QrOSTox6Controle: TIntegerField;
    QrOSTox6Conta: TIntegerField;
    QrOSTox6IDIts: TIntegerField;
    QrOSFrmRecNO_MARCA_FABR: TWideStringField;
    QrOSFrmRecRegMinSaud: TWideStringField;
    QrOSMonRecNO_MARCA_FABR: TWideStringField;
    QrOSMonRecRegMinSaud: TWideStringField;
    QrItensNumLote: TWideStringField;
    QrItensNumLaudo: TWideStringField;
    QrOSTox5NumLote: TWideStringField;
    QrOSTox5NumLaudo: TWideStringField;
    QrOSTox6NumLote: TWideStringField;
    QrOSTox6NumLaudo: TWideStringField;
    QrItensReferencia: TWideStringField;
    QrOSTox6Referencia: TWideStringField;
    QrOSPipMonItsNO_DEPENDENCI: TWideStringField;
    QrOSPipMonItsSiapImaDep: TIntegerField;
    QrOSPipMonItsSiapImaCad: TIntegerField;
    QrOSPipMonItsNO_LOCAL: TWideStringField;
    QrOSRespMINO_LOCAL: TWideStringField;
    QrOSRespMINO_DEPENDENCI: TWideStringField;
    QrOSPipMonSubNO_DEPENDENCI: TWideStringField;
    QrOSPipMonSubNO_LOCAL: TWideStringField;
    QrOSPipMonSubSiapImaCad: TIntegerField;
    QrOSPipMonSubSiapImaDep: TIntegerField;
    QrItensPrgLstCab: TLargeintField;
    QrOSFrmCabCALC_FEITO: TFloatField;
    QrOSSrvTUDOFEITO: TFloatField;
    QrOSFrmCabPercFeito: TFloatField;
    QrOFCPercFeito: TFloatField;
    QrOSTudoFeitoM: TSmallintField;
    QrOSTudoFeitoA: TSmallintField;
    RGModeloOrcamento: TRadioGroup;
    frxGER_OSERV_014_003_B: TfrxReport;
    QrOrcSrv: TmySQLQuery;
    QrOrcSrvCodigo: TIntegerField;
    QrOrcSrvNome: TWideStringField;
    QrOrcSrvSigla: TWideStringField;
    QrOrcSrvObservacao: TWideMemoField;
    frxDsOrcSrv: TfrxDBDataset;
    QrLocalizad1: TmySQLQuery;
    QrLocalizad1Codigo: TIntegerField;
    QrOrcAlv: TmySQLQuery;
    QrOrcAlvNO_Praga_Z: TWideStringField;
    QrOrcAlvNivSup: TIntegerField;
    QrOrcAlvEspecie: TWideStringField;
    QrOrcAlvObservacao: TWideMemoField;
    QrOrcAlvArqDir: TWideStringField;
    QrOrcAlvCopyright: TWideStringField;
    QrOrcAlvArqImg: TWideStringField;
    frxDsOrcAlv: TfrxDBDataset;
    QrOrcTox: TmySQLQuery;
    QrOrcToxGraGru1: TIntegerField;
    QrOrcToxGraGruX: TIntegerField;
    QrOrcToxRegMinSaud: TWideStringField;
    QrOrcToxNO_GG1: TWideStringField;
    QrOrcToxNO_MARCA: TWideStringField;
    QrOrcToxNO_FABRICA: TWideStringField;
    frxDsOrcTox: TfrxDBDataset;
    QrOrcToxNO_MARCA_E_FAB: TWideStringField;
    QrOSChkNO_FEITO: TWideStringField;
    QrOSCxaTudoFeito: TSmallintField;
    QrOXTudoFeito: TSmallintField;
    QrOSCabObsGaranti: TWideMemoField;
    QrOCObsGaranti: TWideMemoField;
    QrOSPipMonItsPipCad: TIntegerField;
    QrValiIsca: TmySQLQuery;
    QrValiIscaDtaPutPMV: TDateTimeField;
    QrValiIscaValCliDd: TIntegerField;
    QrOSPipMonSubPipCad: TIntegerField;
    QrOSPipMISiapImaCad: TIntegerField;
    QrOSPipMISiapImaDep: TIntegerField;
    QrOSPipMINO_LOCAL: TWideStringField;
    QrOSPipMINO_DEPENDENCI: TWideStringField;
    QrOSPipMIPERGUNTA: TWideStringField;
    QrOSPipMIDtaPutPMV: TDateTimeField;
    QrOSPipMIValCliDd: TIntegerField;
    QrOSPipMIDtaValPMV: TDateTimeField;
    QrOSPipMIRelacao: TIntegerField;
    BtConfigura: TBitBtn;
    PB1: TProgressBar;
    PB2: TProgressBar;
    frxGER_OSERV_014_007_A: TfrxReport;
    QrFAESLocais: TmySQLQuery;
    StringField3: TWideStringField;
    frxDsFAESLocais: TfrxDBDataset;
    QrFAESLocaisM2Total: TFloatField;
    QrLocaisM2Constru: TFloatField;
    QrLocaisM2NaoBuild: TFloatField;
    QrLocaisM2Terreno: TFloatField;
    QrLocaisM2Total: TFloatField;
    TabSheet8: TTabSheet;
    QrFAESSSImaCav: TmySQLQuery;
    StringField4: TWideStringField;
    frxDsFAESSSImaCav: TfrxDBDataset;
    QrFAESOSAge: TmySQLQuery;
    QrFAESOSAgeNome: TWideStringField;
    frxDsFAESOSAge: TfrxDBDataset;
    QrFAESOSTox5: TmySQLQuery;
    frxDsFAESOSTox5: TfrxDBDataset;
    QrOSCabMapPMV: TIntegerField;
    QrOCMapPMV: TSmallintField;
    QrFAESOSTox5ApMo: TSmallintField;
    QrFAESOSTox5Codigo: TIntegerField;
    QrFAESOSTox5Controle: TIntegerField;
    QrFAESOSTox5Conta: TIntegerField;
    QrFAESOSTox5IDIts: TIntegerField;
    QrFAESOSTox5TIPO: TWideStringField;
    QrFAESOSTox5NO_Diluente: TWideStringField;
    QrFAESOSTox5NO_EQUIP: TWideStringField;
    QrFAESOSTox5NO_TIPOAPLICA: TWideStringField;
    QrFAESOSTox5QtdTot: TFloatField;
    QrFAESOSTox5Diluente: TSmallintField;
    QrFAESOSTox5UsoQtd: TFloatField;
    QrFAESOSTox5EhDiluente: TSmallintField;
    QrFAESOSTox5RegMinSaud: TWideStringField;
    QrFAESOSTox5FoneCIT: TWideStringField;
    QrFAESOSTox5FoneCEATOX: TWideStringField;
    QrFAESOSTox5Toxicidade: TWideStringField;
    QrFAESOSTox5Concentrac: TWideStringField;
    QrFAESOSTox5AcaoToxica: TWideStringField;
    QrFAESOSTox5Antidoto: TWideStringField;
    QrFAESOSTox5NO_GG1: TWideStringField;
    QrFAESOSTox5NO_MARCA: TWideStringField;
    QrFAESOSTox5NO_FABRICA: TWideStringField;
    QrFAESOSTox5Observacao: TWideMemoField;
    QrFAESOSTox5MyOrd: TIntegerField;
    QrFAESOSTox5Ativo: TSmallintField;
    QrFAESOSTox5NO_MARCA_E_FAB: TWideStringField;
    QrFAESOSTox5SIGLA_USO: TWideStringField;
    QrFAESOSTox5SIGLA_TOT: TWideStringField;
    QrFAESOSTox5NumLote: TWideStringField;
    QrFAESOSTox5NumLaudo: TWideStringField;
    QrFAESOSTox5GRUPO_QUIMICO: TWideStringField;
    QrTermoFAES: TmySQLQuery;
    QrTermoFAESTexto: TWideMemoField;
    frxDsTermoFAES: TfrxDBDataset;
    QrFAESOSAgeAssDados: TWideStringField;
    QrFAESOSAgeAssImagem: TWideStringField;
    QrFAESOSAgeAssNome: TWideStringField;
    QrFAESOSAgeCodigo: TIntegerField;
    QrOSCabLauInfesInt: TWideMemoField;
    QrOSCabLauInfesExt: TWideMemoField;
    QrOSCabLauParTec: TWideMemoField;
    QrOCLauInfesInt: TWideMemoField;
    QrOCLauInfesExt: TWideMemoField;
    QrOCLauParTec: TWideMemoField;
    QrItensFormaApresen: TWideStringField;
    QrFAESOSTox5FormaApresen: TWideStringField;
    QrFAESOSTox5GraGru1: TIntegerField;
    Label32: TLabel;
    EdQtdLObsFaes: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOSSrvBeforeClose(DataSet: TDataSet);
    procedure QrOSSrvAfterScroll(DataSet: TDataSet);
    procedure QrOSFrmCabBeforeClose(DataSet: TDataSet);
    procedure QrOSMonCabBeforeClose(DataSet: TDataSet);
    procedure QrOSFrmCabAfterScroll(DataSet: TDataSet);
    procedure QrOSMonCabAfterScroll(DataSet: TDataSet);
    procedure EdOSCabChange(Sender: TObject);
    procedure QrOSCabBeforeClose(DataSet: TDataSet);
    procedure QrOSCabAfterScroll(DataSet: TDataSet);
    procedure QrOSCabCalcFields(DataSet: TDataSet);
    procedure QrOCCalcFields(DataSet: TDataSet);
    procedure QrOSSrvCalcFields(DataSet: TDataSet);
    procedure frxGER_OSERV_014_001_AGetValue(const VarName: string;
      var Value: Variant);
    procedure QrSTerCadCalcFields(DataSet: TDataSet);
    procedure QrSTerCadBeforeClose(DataSet: TDataSet);
    procedure QrSTerCadAfterScroll(DataSet: TDataSet);
    procedure QrSImaCadCalcFields(DataSet: TDataSet);
    procedure QrSImaDepCalcFields(DataSet: TDataSet);
    procedure QrSImaCadBeforeClose(DataSet: TDataSet);
    procedure QrSImaCadAfterScroll(DataSet: TDataSet);
    procedure QrSImaResCalcFields(DataSet: TDataSet);
    procedure QrSImaCxaBeforeClose(DataSet: TDataSet);
    procedure QrSImaCxaCalcFields(DataSet: TDataSet);
    procedure QrSImaCxaAfterScroll(DataSet: TDataSet);
    procedure QrSMovCadBeforeClose(DataSet: TDataSet);
    procedure QrSMovCadAfterScroll(DataSet: TDataSet);
    procedure QrSMovCadCalcFields(DataSet: TDataSet);
    procedure QrETCalcFields(DataSet: TDataSet);
    procedure QrOSPrzCalcFields(DataSet: TDataSet);
    procedure QrEMCalcFields(DataSet: TDataSet);
    procedure QrOSFPSCalcFields(DataSet: TDataSet);
    procedure QrOSCxaCalcFields(DataSet: TDataSet);
    procedure QrOSCxaBeforeClose(DataSet: TDataSet);
    procedure QrOSCxaAfterScroll(DataSet: TDataSet);
    procedure QrOSTox5CalcFields(DataSet: TDataSet);
    procedure QrOSTox5BeforeClose(DataSet: TDataSet);
    procedure QrOSTox5AfterScroll(DataSet: TDataSet);
    procedure QrLocAfterScroll(DataSet: TDataSet);
    procedure QrLocBeforeClose(DataSet: TDataSet);
    procedure QrOSTox3CalcFields(DataSet: TDataSet);
    procedure QrOSFrmDepCalcFields(DataSet: TDataSet);
    //procedure Qr_OS_Mon_Dep_CalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure QrOpcoesBeforeClose(DataSet: TDataSet);
    procedure QrOpcoesAfterScroll(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure QrSelLugBeforeClose(DataSet: TDataSet);
    procedure QrSelLugAfterScroll(DataSet: TDataSet);
    procedure QrSelSTCAfterScroll(DataSet: TDataSet);
    procedure QrSelSTCBeforeClose(DataSet: TDataSet);
    procedure QrLocaisBeforeClose(DataSet: TDataSet);
    procedure QrLocaisAfterScroll(DataSet: TDataSet);
    procedure QrTiposAfterScroll(DataSet: TDataSet);
    procedure QrTiposBeforeClose(DataSet: TDataSet);
    procedure QrSumPrzCalcFields(DataSet: TDataSet);
    procedure QrOSPipMonBeforeClose(DataSet: TDataSet);
    procedure QrOSPipMonAfterScroll(DataSet: TDataSet);
    procedure EdCliCodChange(Sender: TObject);
    procedure QrOSPipMLAfterScroll(DataSet: TDataSet);
    procedure QrOSPipMLBeforeClose(DataSet: TDataSet);
    procedure QrOSRespMIBeforeClose(DataSet: TDataSet);
    procedure QrOSRespMIAfterScroll(DataSet: TDataSet);
    procedure QrOSTox6AfterScroll(DataSet: TDataSet);
    procedure QrOSTox6BeforeClose(DataSet: TDataSet);
    procedure QrOSTox6CalcFields(DataSet: TDataSet);
    procedure QrOSFrmCabCalcFields(DataSet: TDataSet);
    procedure QrOrcSrvAfterScroll(DataSet: TDataSet);
    procedure QrOrcToxCalcFields(DataSet: TDataSet);
    procedure BtConfiguraClick(Sender: TObject);
    procedure frxGER_OSERV_014_005_BGetValue(const VarName: string;
      var Value: Variant);
    procedure frxGER_OSERV_014_002_AGetValue(const VarName: string;
      var Value: Variant);
    procedure frxGER_OSERV_014_003_AGetValue(const VarName: string;
      var Value: Variant);
    procedure frxGER_OSERV_014_003_BGetValue(const VarName: string;
      var Value: Variant);
    procedure frxGER_OSERV_014_006_AGetValue(const VarName: string;
      var Value: Variant);
    procedure frxGER_OSERV_014_007_AGetValue(const VarName: string;
      var Value: Variant);
    procedure frxGER_OSERV_014_007_AClickObject(Sender: TfrxView;
      Button: TMouseButton; Shift: TShiftState; var Modified: Boolean);
  private
    { Private declarations }
    FLastOpenOSCab: Integer;
    FSeqStc: Integer;
    //
    F_OS_Imp_: String;
    FRegSID_AlturaLoc, FRegSID_AlturaTyp: Double;
    FPageIndex: Integer;
    // FImp_OSDep,
    FImp_OSCab, FImp_OSAge, FImp_OSCabAlv, FImp_OSSrv, FImp_OSAlv,
    FImp_OSFrmCab, FImp_OSFrmRec, FImp_OSFrmAbr, FImp_OSFrmDep,
    FImp_OSMonCab, FImp_OSMonRec, (*FImp__OS_Mon_Dep_,*)
    FImp_OSPrz, FImp_OSTox, FImp_OSToz, FImp_OSCxa,
    FImp_STerCad, FImp_SImaCad, FImp_SImaDep,
    FImp_SImaCdi, FImp_SImaCav, FImp_SImaRes, FImp_SImaCui,
    FImp_SImaAti, FImp_SImaCxa, FImp_SICxDef, FImp_SICdDef,
    FImp_SMovCad, FImp_SMovDef, FImp_EntiTel, FImp_EntiEma,
    FImp_OSPipML, FImp_OSPipMI: String;
    //FOSCab(*, FSTC*): Integer;
    FPreVistoria, FVistoria: Boolean;
    //
    procedure AtualizaOSPrn(GruImp: Integer; Agora: TDateTime);
    procedure AtualizaTodos(Ativo: Integer);

    procedure DefineCores();
    procedure DefineCorMemos(frx: TfrxReport);
    procedure DefineAlturasA(frx: TfrxReport);
    procedure DefineAlturasB(frx: TfrxReport);

    function  GetImaCad(Current: Integer): Integer;

    function  GG1_SemReferencia(): Boolean;

    procedure ImprimeVistoria();
    procedure ImprimeOrcamento();
    procedure ImprimeExecucao();
    function  ImprimeGarantia(): Boolean;
    procedure ImprimeMonitoramento();
    procedure Info(Txt: String);
    //
    procedure InsereOSCab(Codigo: Integer);
    procedure InsereOSAge(OSCab: Integer);
    procedure InsereOSCabAlv(OSCab: Integer);
    procedure InsereOSSrv(OSCab: Integer);
    procedure InsereOSAlv(OSCab: Integer);
    procedure InsereOSFrmCab(OSCab: Integer);
    procedure InsereOSFrmRec(OSCab: Integer);
    procedure InsereOSFrmAbr(OSCab: Integer);
    procedure InsereOSFrmDep(OSCab: Integer);
    procedure InsereOSMonCab(OSCab: Integer);
    procedure InsereOSMonRec(OSCab: Integer);
    //procedure Insere_OS_Mon_Dep_(OSCab: Integer);
    procedure InsereOSPipML_MI(OSCab: Integer);
    procedure InsereOSPipML_MI2(OSCab: Integer);
    procedure InsereOSPrz(OSCab: Integer);
    procedure InsereOSCxa(OSCab: Integer);
    //
    procedure InsereEntiTel(Entidades: String);
    procedure InsereEntiEma(Entidades: String);
    //
    procedure InsereSMovCad();
    procedure InsereSMovDef();
    procedure InsereSTerCad();
    procedure InsereSImaCad();
    procedure InsereSImaDep();
    procedure InsereSImaCdi();
    procedure InsereSImaCav();
    procedure InsereSImaRes();
    procedure InsereSImaCui();
    procedure InsereSImaAti();
    procedure InsereSImaCxa();
    procedure InsereSICxDef();
    procedure InsereSICdDef();

    procedure InsereVariaveisMonitPIP(frxReport: TfrxReport);

    function  ObtemBrushStyle(Ativo: Integer): TBrushStyle;

    function  Obtem_LOCALIZADORES_SERVICOS_OFERECIDOS(): String;

    function  OSStartNewPage(): Boolean;
    //
    function  QtdOSAge(): Integer;
    function  QtdOSCabAlv(): Integer;
    function  QtdOSSrv(): Integer;
    function  QtdOSAlv(): Integer;
    function  QtdOSFrmCab(): Integer;
    function  QtdOSFrmRec(): Integer;
    function  QtdOSFrmAbr(): Integer;
    function  QtdOSFrmDep(): Integer;
    function  QtdOSMonCab(): Integer;
    function  QtdOSMonRec(): Integer;
    //function  Qtd_OS_Mon_Dep_(): Integer;
    function  QtdOSPrz(): Integer;
    function  QtdOSCxa(): Integer;
    //
    function  QtdEntiEma(): Integer;
    function  QtdEntiTel(): Integer;
    //
    function  QtdSMovCad(): Integer;
    function  QtdSMovDef(): Integer;
    function  QtdSImaCad(): Integer;
    function  QtdSImaDep(): Integer;
    function  QtdSImaCdi(): Integer;
    function  QtdSImaCav(): Integer;
    function  QtdSImaRes(): Integer;
    function  QtdSImaCui(): Integer;
    function  QtdSImaAti(): Integer;
    function  QtdSImaCxa(): Integer;
    function  QtdSICxDef(): Integer;
    function  QtdSICdDef(): Integer;
    //
    procedure ReopenAnalise(Tabela, Grupo1, Campo: String; Colunas: Integer);
    //
    procedure ReopenCunsCad();
    //
    procedure ReopenDesServico();
    //
    procedure ReopenOrcAlv();
    procedure ReopenOrcSrv();
    procedure ReopenOrcTox();


    procedure ReopenOSCab(Tabela: String; Qry: TmySQLQuery; DB: TmySQLDatabase;
              Codigo: Integer);
    procedure ReopenOSAge(OSCab: Integer);
    procedure ReopenOSCabAlv(OSCab: Integer);
    procedure ReopenOSCabXtr(OSCab: Integer);
    procedure ReopenOSSrv(OSCab: Integer);
    procedure ReopenOSAlv();
    procedure ReopenOSFrmCab();
    procedure ReopenOSFrmRec();
    procedure ReopenOSFrmAbr();
    procedure ReopenOSFrmDep();
    procedure ReopenOSMonCab();
    procedure ReopenOSMonRec();
    //procedure Reopen_OS_Mon_Dep_();
    procedure ReopenOSPrz(OSCab: Integer);
    procedure ReopenOSFPS(OSCab: Integer);
    procedure ReopenOSCxa(OSCab: Integer);
    procedure ReopenOSCxI(OSCab: Integer);
    procedure ReopenOSTox3();
    procedure ReopenOSTox5();
    procedure ReopenOSTox6();
    procedure ReopenOSToz5();
    procedure ReopenOSToz6();
    procedure ReopenSumOS();
    procedure ReopenSumPrz();
    //
    procedure ReopenSiapTerCad();
    procedure ReopenEntiEma();
    procedure ReopenEntiTel();
    //
    procedure ReopenSTerCad();
    procedure ReopenSImaCad();
    procedure ReopenSImaDep();
    procedure ReopenSImaCdi();
    procedure ReopenSImaCav();
    procedure ReopenSImaRes();
    procedure ReopenSImaCui();
    procedure ReopenSImaAti();
    procedure ReopenSImaCxa();
    procedure ReopenSICxDef();
    procedure ReopenSICdDef();
    procedure ReopenSMovCad();
    procedure ReopenSMovDef();

    procedure ReopenCdi();
    procedure ReopenCav();
    procedure ReopenRes();
    procedure ReopenCui();
    procedure ReopenAti();
    procedure ReopenAtr();

    procedure ReopenDepsA();
    procedure ReopenDepsB();
    procedure ReopenLocais();
    procedure ReopenTipos(Qry: TmySQLQuery; ByOS: Boolean = False);
    procedure ReopenDeptos(Qry: TmySQLQuery; ByOS: Boolean = False);

    procedure ReopenOpcoes();
    procedure ReopenLugares(Opcao, Lugar: Integer);
    procedure ReopenSelLug(Qry: TmySQLQuery);

    procedure ReopenDiarioAdd(Grupo: Integer);

    procedure ReopenOSPipMonLst(OSCab: Integer);
    procedure ReopenOSPipMonIts(OSCab, PrgLstCab: Integer);
    procedure ReopenOSPipMonSub(OSCab, PrgLstIts: Integer);
    //
    procedure ReopenOSPipML();
    procedure ReopenOSPipMI();
    procedure ReopenOSRespMI();
    procedure ReopenOSRespOK();

    function  ImprimeOS(frx: TfrxReport; Titulo: String): Boolean;
    procedure ImprimeLugar();
    procedure ImprimeTerreno();
    procedure Imprime002(Titulo: String);

    function  InsereOSPrn(Relatorio: Integer; Agora: TDateTime): Integer;

    function  ObtemContratante_E_Pagante(): String;
    function  OSCxIFotoCxa(): String;
    function  ObtemAbrangencias(): String;
    //procedure ColunaFinalOSDep(var Coluna, Linha: Integer);
    procedure RecriaDadosGarantias();
    //
    procedure CriaTabelasTemporarias();
    //
    function ObtemContrato(): String;
    function ObtemEntidade(): String;
    function ObtemLugar(): String;
    function EnderecoMontado(UmaLinha: Boolean = False): String;
    //
    //FAES
    procedure ImprimeFAES();
    procedure ReopenFAESLocais();
    procedure ReopenFAESSImaCav();
    procedure ReopenFAESOSAge();
    procedure ReopenFAESOSTox5();
    function  ObtemListaPragas(Encontradas: Boolean): String;
    function  ObtemListaEPIs(ApMo, Conta: Integer): String;
  public
    { Public declarations }
    FCliente, FContratante, FPagante: Integer;
    FApenasGera: Boolean;
    FDoLocDefined: Boolean;
    //
    procedure RecriaTabsTempOS();
    procedure RecriaTabsTempTerreno();
    function  ObtemDependencias_Nomes(): String;
    function  ObtemDependencias_Area(): Double;
    function  ObtemPragasAlvoServico(): String;
    function  ObtemDeptosDoTypAtual(Qry: TmySQLQuery): String;
    function  CaminhoArqPraga(): String;
    function  CaminhoArqPragaOrc(): String;
    procedure ReabreClienteTabs();
    procedure ReopenOSChk();
    procedure PreparaselacaoDeOSs(Grupo: Integer);
    procedure frxGER_OSERV_014_000_AGetValue(frxReport: TfrxReport;
              const VarName: string; var Value: Variant);
    function  GeraImpressao(): Boolean;
  end;

  var
  FmOSImp2: TFmOSImp2;

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleGeral, CreateBugs, UMySQLModule,
 OSFrmCab, ModOS, UnBugs_Tabs, Entidade2ImpOne, UnOSApp_PF, UnEntities,
 UnGrade_Jan;

{$R *.DFM}

{
const
  CorFraca = $00F2E4D7; // $00E3E3E3
  CorMedia = $00E2C5A9; // $00C8C8C8
  CorForte = $00CD9B69; // $00646464
  //
  CorTitul = clWhite;
  CorFundo = clCream;
  CorVazio = clWhite;
  CorInfBk = clInfoBk;
  CorTitSe = $00C4DFC4; // Verde money
}
var
  CorFraca,
  CorMedia,
  CorForte,
  //
  CorTitul,
  CorFundo,
  CorVazio,
  CorInfBk,
  CorTitSe,
  CorBloco: TColor;

const
  FMyAlturaLinha = 400;
  FPage_Cadastro_Entidade    = 0;
  FPage_Ficha_Lugar          = 1;
  FPage_Ficha_Vistoria       = 2;
  FPage_Orcamento            = 3;
  FPage_Ficha_Execucao       = 4;
  FPage_Monitoramento        = 5;
  FPage_Comprovante_Execucao = 6;
  FPage_FAES                 = 7;
  //FPage_   = ;
  CO_SEM_DILUENTE = 'Sem diluente';

procedure TFmOSImp2.AtualizaOSPrn(GruImp: Integer; Agora: TDateTime);
var
  DtHrFim: String;
begin
  if GruImp = 0 then
    Exit;
  DtHrFim := Geral.FDT(Agora, 109);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osprn', False, [
  'DtHrFim'], [
  'GruImp'], [
  DtHrFim], [
  GruImp], True);
end;

procedure TFmOSImp2.AtualizaTodos(Ativo: Integer);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'UPDATE ' + F_OS_Imp_ + ' SET Ativo=' + Geral.FF0(Ativo),
  'WHERE Opcao=' + Geral.FF0(QrOpcoesOpcao.Value),
  '']);
  ReopenLugares(QrOpcoesOpcao.Value, QrLugaresLugar.Value);
end;

procedure TFmOSImp2.BtConfiguraClick(Sender: TObject);
begin
  Dmod.MostraFormOpcoesBugs(3);
end;

procedure TFmOSImp2.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmOSImp2.BtOKClick(Sender: TObject);
begin
  GeraImpressao();
end;

procedure TFmOSImp2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSImp2.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

function TFmOSImp2.CaminhoArqPraga(): String;
var
  //, ArqPath
  Host, SDir, RDir, DirDest: String;
  Empresa: Integer;
begin
  Result  := '';
  Host    := Dmod.QrOpcoesBugsImgIPv4.Value;
  SDir    := Dmod.QrOpcoesBugsImgSDir.Value;
  Empresa := QrOSCabEmpresa.Value;
  RDir    := QrOSAlvArqDir.Value;
  //
  if CkDoLocalize.Checked then
  begin
    if MyObjects.TentaDefinirDiretorio(Host, SDir, Empresa, RDir, LaAviso1, LaAviso2, False, Memo1, DirDest) then
      Result := DmkPF.CaminhoArquivo(DirDest, QrOSAlvArqImg.Value, '')
    else
      Result := '';
  end else
    Result := '';
end;

function TFmOSImp2.CaminhoArqPragaOrc(): String;
var
  //, ArqPath
  Host, SDir, RDir, DirDest: String;
  Empresa: Integer;
begin
  Result := '';
  Host := Dmod.QrOpcoesBugsImgIPv4.Value;
  SDir := Dmod.QrOpcoesBugsImgSDir.Value;
  Empresa := QrOSCabEmpresa.Value;
  RDir := QrOrcAlvArqDir.Value;
  //
  if CkDoLocalize.Checked then
  begin
    if MyObjects.TentaDefinirDiretorio(Host, SDir, Empresa, RDir, LaAviso1, LaAviso2, False, Memo1, DirDest) then
      Result := DmkPF.CaminhoArquivo(DirDest, QrOrcAlvArqImg.Value, '')
    else
      Result := '';
  end else
    Result := '';
end;

procedure TFmOSImp2.CriaTabelasTemporarias();
var
  OSCab: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
  RecriaTabsTempOS();
  //
  if QrSelSTC.RecordCount > 0 then
  begin
    PB1.Position := 0;
    PB1.Max      := QrSelSTC.RecordCount;
    //
    QrSelSTC.First;
    while not QrSelSTC.Eof do
    begin
      OSCab := QrSelSTCOSCab.Value;
      //
      Info('OSCab');
      InsereOSCab(OSCab);

      Info('OSCab > OSAge');
      InsereOSAge(OSCab);

      Info('OSCab > OSCabAlv');
      InsereOSCabAlv(OSCab);

      Info('OSCab > OSPrz');
      InsereOSPrz(OSCab);

      Info('OSCab > OSSrv');
      InsereOSSrv(OSCab);

      Info('OSCab > OSSrv > OSAlv');
      InsereOSAlv(OSCab);

      Info('OSCab > OSSrv > OSFrmCab');
      InsereOSFrmCab(OSCab);

      Info('OSCab > OSSrv > OSFrmCab > OSFrmRec');
      InsereOSFrmRec(OSCab);

      Info('OSCab > OSSrv > OSFrmCab > OSFrmDep');
      InsereOSFrmDep(OSCab);

      Info('OSCab > OSSrv > OSFrmCab > OSFrmAbr');
      InsereOSFrmAbr(OSCab);

      Info('OSCab > OSSrv > OSMonCab');
      InsereOSMonCab(OSCab);

      Info('OSCab > OSSrv > OSFrmCab > OSMonRec');
      InsereOSMonRec(OSCab);

  {
      Info('OSCab > OSSrv > OSFrmCab > _OS_Mon_Dep_');
      Insere_OS_Mon_Dep_(OSCab);
  }

      Info('OSCab > OSCxa');
      InsereOSCxa(OSCab);

      (*
      if FPageIndex in ([5, 6]) then
      begin
        Info('OSPipML / OSPipMI');
        InsereOSPipML_MI(OSCab);
      end;
      *)
      if FPageIndex = 5 then
      begin
        Info('OSPipML / OSPipMI');
        InsereOSPipML_MI(OSCab);
      end;

      if FPageIndex = 6 then
      begin
        Info('OSPipML / OSPipMI');
        InsereOSPipML_MI2(OSCab);
      end;
      //
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      QrSelSTC.Next;
    end;
    //
    //
    //ReopenOSCab(FImp_OSCab, QrOSCab, DModG.MyPID_DB, OSCab);
  end;
  PB1.Position := 0;
  PB2.Position := 0;
end;

{
procedure TFmOSImp.ColunaFinalOSDep(var Coluna, Linha: Integer);
begin
  Linha := EdQtdOSDep.ValueVariant div CO_IMP_COLS_DEP;
  Coluna := EdQtdOSDep.ValueVariant mod CO_IMP_COLS_DEP;
end;
}

procedure TFmOSImp2.DefineAlturasA(frx: TfrxReport);
var
  I, AltLinOS: Integer;
  Compo: TfrxComponent;
begin
  AltLinOS := EdAltLinOS.ValueVariant;
  //if not EdAlturaMemosOS.ValueVariant <> FMyAlturaLinha then
  begin
    for I := 0 to frx.ComponentCount -1 do
    begin
      if frx.Components[I] is TfrxComponent then
      begin
        Compo := TfrxComponent(frx.Components[I]);
        if Compo.Tag > 0 then
          Compo.Height := (Compo.Tag + AltLinOS - FMyAlturaLinha) / VAR_frCM;
      end;
    end;
  end;
end;

procedure TFmOSImp2.DefineAlturasB(frx: TfrxReport);
var
  I, P, AltLinOS: Integer;
  Compo: TfrxComponent;
  Defs: String;
  QL, TL, NL: Integer;
  //MyTop: Double;
begin
  AltLinOS := EdAltLinOS.ValueVariant;
  //if not EdAlturaMemosOS.ValueVariant <> FMyAlturaLinha then
  begin
    for I := 0 to frx.ComponentCount -1 do
    begin
      if frx.Components[I] is TfrxComponent then
      begin
        Compo := TfrxComponent(frx.Components[I]);
        Defs := Compo.Description;
        if Defs <> '' then
        begin
          P := pos('QL', Defs);
          if P = 0 then
            QL := 1
          else
            QL := Geral.IMV(Copy(Defs, P + 2, 2));
          P := pos('TL', Defs);
          if P <> 0 then
          begin
            TL := Geral.IMV(Copy(Defs, P + 2, 5));
            P := pos('NL', Defs);
            if P > 0 then
              NL := Geral.IMV(Copy(Defs, P + 2, 2)) - 1
            else NL := 0;
            //
            Compo.Top := ((AltLinOS * NL) - (FMyAlturaLinha * NL) + TL) / VAR_frCM;
          end;
          if Compo.Tag = 0 then
            Geral.MB_Erro('Componente com QL mas sem tag: ' + Compo.Name)
          else begin
            Compo.Height := (Compo.Tag + (AltLinOS * QL) - (FMyAlturaLinha * QL)) / VAR_frCM;
          end;
        end else
        if Compo.Tag > 0 then
          Compo.Height := (Compo.Tag + AltLinOS - FMyAlturaLinha) / VAR_frCM;

      end;
    end;
  end;
end;

procedure TFmOSImp2.DefineCores();
begin
  if CkColore.Checked then
  begin
    CorFraca := $00F2E4D7; // $00E3E3E3
    CorMedia := $00E2C5A9; // $00C8C8C8
    CorForte := $00CD9B69; // $00646464
    //
    CorFundo := clCream;
    CorVazio := clWhite;
    CorInfBk := clInfoBk;
    CorTitul := $00ADE9AD; // Verde claro
    CorTitSe := $0055B850; // Verde m�dio
    CorBloco := clRed;
  end else
  begin
    CorFraca := clWhite;
    CorMedia := clWhite;
    CorForte := clWhite;
    //
    CorFundo := clWhite;
    CorVazio := clWhite;
    CorInfBk := clWhite;
    CorTitul := clWhite;
    CorTitSe := clWhite;
    CorBloco := clWhite;
  end;
end;

procedure TFmOSImp2.DefineCorMemos(frx: TfrxReport);
var
  I: Integer;
  Nome, Memo: String;
begin
  for I := 0 to frx.ComponentCount -1 do
  begin
    Nome := TComponent(frx.Components[I]).Name;
    Memo := UpperCase(Copy(Nome, 1, 8));
    if Memo = 'MEFRACO_' then
      TfrxMemoView(frx.Components[I]).Color := CorFraca
    else
    if Memo = 'MEMEDIO_' then
      TfrxMemoView(frx.Components[I]).Color := CorMedia
    else
    if Memo = 'MEFORTE_' then
      TfrxMemoView(frx.Components[I]).Color := CorForte
    else
    if Memo = 'MEFUNDO_' then
      TfrxMemoView(frx.Components[I]).Color := CorFundo
    else
    if Memo = 'MEVAZIO_' then
      TfrxMemoView(frx.Components[I]).Color := CorVazio
    else
    if Memo = 'MEINFBK_' then
      TfrxMemoView(frx.Components[I]).Color := CorInfBk
    else
    if Memo = 'METITSE_' then
      TfrxMemoView(frx.Components[I]).Color := CorTitSe
    else
    if Memo = 'METITUL_' then
      TfrxMemoView(frx.Components[I]).Color := CorTitul
    else
    if Memo = 'MEBLOCO_' then
      TfrxMemoView(frx.Components[I]).Color := CorBloco
    else
    //if Components[I] is TfrxMemoView then
      //TfrxMemoView(frx.Components[I]).Color := clFuchsia
  end;
end;

procedure TFmOSImp2.EdCliCodChange(Sender: TObject);
begin
  ReopenCunsCad();
  CkImpImaInfo.Checked := QrCunscadImpImaInfo.Value = 1;
end;

procedure TFmOSImp2.EdOSCabChange(Sender: TObject);
begin
{
  if EdOSCab.ValueVariant = 0 then
  begin
    CkPreVistoria.Checked := True;
    CkPreVistoria.Enabled := False;
  end else
  begin
    CkPreVistoria.Checked := False;
    CkPreVistoria.Enabled := True;
  end;
}
end;

function TFmOSImp2.EnderecoMontado(UmaLinha: Boolean = False): String;
begin
  Result := Entities.MontaEndereco(
              DmModOS.QrSTCNOMELOGRAD.Value,
              DmModOS.QrSTCSRua.Value,
              DmModOS.QrSTCSNumero.Value,
              DmModOS.QrSTCSCompl.Value,
              DmModOS.QrSTCSBairro.Value,
              DmModOS.QrSTCSCidade.Value,
              DmModOS.QrSTCSUF.Value,
              DmModOS.QrSTCSPais.Value,
              DmModOS.QrSTCSCEP.Value,
              UmaLinha)
end;

procedure TFmOSImp2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSImp2.FormClose(Sender: TObject; var Action: TCloseAction);
  procedure GravaReg(Tab: String; Valor: Integer);
  begin
    Geral.WriteAppKeyCU('QtdOS' + Tab,
      Application.Title + '\OSImpEmpty', Valor, ktInteger);
  end;
begin
  GravaReg('CabAlv', EdQtdOSCabAlv.ValueVariant);
  GravaReg('Age', EdQtdOSAge.ValueVariant);
  GravaReg('Prz', EdQtdOSPrz.ValueVariant);
  GravaReg('Srv', EdQtdOSSrv.ValueVariant);
  GravaReg('Alv', EdQtdOSAlv.ValueVariant);
  GravaReg('FrmCab', EdQtdOSFrmCab.ValueVariant);
  GravaReg('FrmRec', EdQtdOSFrmRec.ValueVariant);
  GravaReg('FrmAbr', EdQtdOSFrmAbr.ValueVariant);
  GravaReg('FrmDep', EdQtdOSFrmDep.ValueVariant);
  GravaReg('MonCab', EdQtdOSMonCab.ValueVariant);
  GravaReg('MonRec', EdQtdOSMonRec.ValueVariant);
  //GravaReg('MonDep', EdQtd_OS_Mon_Dep_.ValueVariant);
  //
  GravaReg('SImaCad', EdQtdSImaCad.ValueVariant);
  GravaReg('SImaDep', EdQtdSImaDep.ValueVariant);
  GravaReg('SImaCdi', EdQtdSImaCdi.ValueVariant);
  GravaReg('SImaCav', EdQtdSImaCav.ValueVariant);
  GravaReg('SImaRes', EdQtdSImaRes.ValueVariant);
  GravaReg('SImaCui', EdQtdSImaCui.ValueVariant);
  GravaReg('SImaAti', EdQtdSImaAti.ValueVariant);
  GravaReg('SImaCxa', EdQtdSImaCxa.ValueVariant);
  GravaReg('SICxDef', EdQtdSICxDef.ValueVariant);
  GravaReg('SICdDef', EdQtdSICdDef.ValueVariant);
  GravaReg('SMovCad', EdQtdSMovCad.ValueVariant);
  GravaReg('SMovDef', EdQtdSMovDef.ValueVariant);
  //
  GravaReg('AltLin', EdAltLinOS.ValueVariant);
  //
  //
  GravaReg('OSCxa', EdQtdOSCxa.ValueVariant);
  //
  GravaReg('Colore', Geral.BoolToInt(CkColore.Checked));
  //
end;

procedure TFmOSImp2.FormCreate(Sender: TObject);
  procedure LeReg(Tab: String; Ed: TdmkEdit; Padrao: Integer);
  begin
    Ed.ValueVariant := Geral.ReadAppKeyCU('QtdOS' + Tab,
      Application.Title + '\OSImpEmpty', ktInteger, Padrao);
  end;
var
  Colore, I: Integer;
begin
  FDoLocDefined   := False;
  FApenasGera     := False;
  FCliente        := 0;
  FContratante    := 0;
  FPagante        := 0;
  ImgTipo.SQLType := stLok;
  FLastOpenOSCab  := 0;
  //
  Dmod.frxDsOpcoesBugs.DataSet := Dmod.QrOpcoesBugs;
  //
  frxGER_OSERV_014_002_A.AddFunction(
  'function CorMemoMatrix(Tabela: String; Coluna, IdxCampo: Integer): Integer');
  frxGER_OSERV_014_003_A.AddFunction(
  'function CorMemoMatrix(Tabela: String; Coluna, IdxCampo: Integer): Integer');
  //
  LeReg('CabAlv', EdQtdOSCabAlv, 1);
  LeReg('Age', EdQtdOSAge, 1);
  LeReg('Prz', EdQtdOSPrz, 1);
  LeReg('Srv', EdQtdOSSrv, 1);
  LeReg('Alv', EdQtdOSAlv, 1);
  LeReg('FrmCab', EdQtdOSFrmCab, 1);
  LeReg('FrmRec', EdQtdOSFrmRec, 1);
  LeReg('FrmAbr', EdQtdOSFrmAbr, 1);
  LeReg('FrmDep', EdQtdOSFrmDep, 1);
  LeReg('MonCab', EdQtdOSMonCab, 1);
  LeReg('MonRec', EdQtdOSMonRec, 1);
  //LeReg('MonDep', EdQtd_OS_Mon_Dep_, 1);
  //
  LeReg('AltLin', EdAltLinOS, FMyAlturaLinha);
  //
  LeReg('SImaCad', EdQtdSImaCad, 1);
  LeReg('SImaDep', EdQtdSImaDep, 1);
  LeReg('SImaCdi', EdQtdSImaCdi, 1);
  LeReg('SImaCav', EdQtdSImaCav, 1);
  LeReg('SImaRes', EdQtdSImaRes, 1);
  LeReg('SImaCui', EdQtdSImaCui, 1);
  LeReg('SImaAti', EdQtdSImaAti, 1);
  LeReg('SImaCxa', EdQtdSImaCxa, 1);
  LeReg('SICxDef', EdQtdSICxDef, 1);
  LeReg('SICdDef', EdQtdSICdDef, 1);
  LeReg('SMovCad', EdQtdSMovCad, 1);
  LeReg('SMovDef', EdQtdSMovDef, 1);
  //
  LeReg('OSCxa', EdQtdOSCxa, 1);
  //
  Colore := Geral.ReadAppKeyCU('QtdOSColore',
      Application.Title + '\OSImpEmpty', ktInteger, 0);
  CkColore.Checked := Colore = 1;
  //
{ N�o muda o tempo!
  for I := 0 to ComponentCount -1 do
  begin
    if Components[I] is TfrxDBDataSet then
      TfrxDBDataSet(Components[I]).OpenDataSource := False;
  end;
}
end;

procedure TFmOSImp2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSImp2.FormShow(Sender: TObject);
begin
  PreparaSelacaoDeOSs(EdGrupo.ValueVariant);
end;

function TFmOSImp2.GeraImpressao: Boolean;
var
  GruImp, Codigo: Integer;
begin
  Result       := False;
  FPageIndex   := PageControl1.ActivePageIndex;
  BtOk.Enabled := False;
  try
    // Verifica se escolheu pelo menos um lugar!
    if QrLugares.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Selecione pelo menos um lugar!');
      Exit;
    end;
    if not FDoLocDefined then
      if dmkPF.NaoQueroLocalizarAsImagens(CkDoLocalize.Checked) then
        Exit;
    //
    GruImp := InsereOSPrn(FPageIndex, DModG.ObtemAgora());
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
    RecriaTabsTempTerreno();
    //
    ReopenSelLug(QrSelSTC);
    // Fim verifica
    //
    Memo1.Lines.Clear;
    //
    DefineCores();
    case FPageIndex of
      (*0*)FPage_Cadastro_Entidade:
      begin
        if DBCheck.CriaFm(TFmEntidade2ImpOne, FmEntidade2ImpOne, afmoNegarComAviso) then
        begin
          FmEntidade2ImpOne.EdEntidade.ValueVariant := EdCliCod.ValueVariant;
          FmEntidade2ImpOne.CBEntidade.KeyValue     := EdCliCod.ValueVariant;
          FmEntidade2ImpOne.ShowModal;
          FmEntidade2ImpOne.Destroy;
        end;
      end;
      (*1*)FPage_Ficha_Lugar:                    ImprimeTerreno();
      (*2*)FPage_Ficha_Vistoria:                 ImprimeVistoria();
      (*3*)FPage_Orcamento:                      ImprimeOrcamento();
      (*4*)FPage_Ficha_Execucao:                 ImprimeExecucao();
      (*5*)FPage_Monitoramento:                  ImprimeMonitoramento();
      (*6*)FPage_Comprovante_Execucao: Result := ImprimeGarantia();
      (*7*)FPage_FAES:                           ImprimeFAES();
      else Geral.MensagemBox('Impress�o n�o implementada para a guia selecionada',
      'Aviso', MB_OK+MB_ICONINFORMATION);
    end;
    AtualizaOSPrn(GruImp, DModG.ObtemAgora());
  finally
    BtOk.Enabled := True;
  end;
end;

function TFmOSImp2.GetImaCad(Current: Integer): Integer;
begin
  Result := Current + (FSeqStc * QtdSImaCad());
end;

function TFmOSImp2.GG1_SemReferencia(): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSemRef, Dmod.MyDB, [
  'SELECT gg1p.Nivel1, gg1p.Nome ',
  'FROM ospipits opi ',
  'LEFT JOIN gragrux ggxp ON ggxp.Controle=opi.GraGruX ',
  'LEFT JOIN gragru1 gg1p ON gg1p.Nivel1=ggxp.GraGru1 ',
  'WHERE gg1p.Nivel1 <> 0',
  'AND (gg1p.Referencia IS NULL ',
  'OR gg1p.Referencia="")',
  ' ',
  'UNION ',
  ' ',
  'SELECT gg1e.Nivel1, gg1e.Nome ',
  'FROM pipcad pip ',
  'LEFT JOIN gragrux ggxe ON ggxe.Controle=pip.Equipamento ',
  'LEFT JOIN gragru1 gg1e ON gg1e.Nivel1=ggxe.GraGru1 ',
  'WHERE gg1e.Nivel1 <> 0',
  'AND (gg1e.Referencia IS NULL ',
  'OR gg1e.Referencia="")',
  '',
  '']);
  //
  Result := QrSemRef.RecordCount > 0;
  //
  if Result then
  begin
    if Geral.MB_Pergunta('O produtos '+ QrSemRef.FieldByName('Nome').AsString +
      ' n�o possui refer�ncia definida!' + sLineBreak +
      'Deseja definir agora?') = ID_YES then
    begin
      Grade_Jan.MostraFormGraGruN(QrSemRef.FieldByName('Nivel1').AsInteger);
      Exit;
    end;
    //
    MyObjects.frxDefineDataSets(frxGER_OSERV_014_005_Err1, [
    DModG.frxDsDono,
    frxDsSemRef
    ]);
    //
    MyObjects.frxMostra(frxGER_OSERV_014_005_Err1,
      'Produtos sem refer�ncia definida');
  end;
end;

procedure TFmOSImp2.frxGER_OSERV_014_000_AGetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
var
  ALin, Lins: Integer;
  Area: Double;
begin
  try
  // Ganha s� um segundo se desmarcar aqui!
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando impress�o: ' + VarName);
  if VarName = 'VARF_REL_GEREN_008' then
  begin
    VAR_REL_GEREN := 8;
    Value := True;
  end else
  //  Muito requisitados
  if VarName = 'VARF_GRUPO_OS' then
    Value := Geral.FFN(EdGrupo.ValueVariant, 3)
  else
  if VarName = 'VAR_ATIVO_OSAGE' then
    Value := ObtemBrushStyle(QrOSAgeAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSCABALV' then
    Value := ObtemBrushStyle(QrOSCabAlvAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSALV' then
    Value := ObtemBrushStyle(QrOSAlvAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSFRMREC' then
    Value := ObtemBrushStyle(QrOSFrmRecAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSFRMABR' then
    Value := ObtemBrushStyle(QrOSFrmAbrAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSFRMDEP' then
    Value := ObtemBrushStyle(QrOSFrmDepAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSMONREC' then
    Value := ObtemBrushStyle(QrOSMonRecAtivo.Value)
  else
{
  if VarName = 'VAR_ATIVO__OS_Mon_Dep_' then
    Value := ObtemBrushStyle(Qr_OS_Mon_Dep_Ativo.Value)
  else
}
  if VarName = 'VAR_ATIVO_OSPRZ' then
    Value := ObtemBrushStyle(QrOSPrzAtivo.Value)
  //

  else
  if VarName = 'VARF_PAGINAR' then
    Value := True
  else
  if VarName = 'VARF_FATO_GERADOR' then
    Value := QrOSCabNO_FatoGeradr.Value
  else
  if VarName = 'VARF_OS_ORIGEM' then
  begin
(*
    if QrOSCabOSOrigem.Value <> 0 then
      Value := 'OS de origem: ' + Geral.FFN(QrOSCabOSOrigem.Value, 3)
    else
*)
      Value := ' ';
  end
  else
  if VarName = 'VARF_CONTRATO' then
    Value := ObtemContrato()
  else
  if VarName = 'VARF_LUGAR' then
    Value := ObtemLugar()
  else
  if VarName = 'VARF_ENDERECO_MONTADO' then
    Value := EnderecoMontado()
  else
  if VarName = 'VARF_ENDERECO_MONTADO2' then
    Value := EnderecoMontado(True)
  else
  if VarName = 'VARF_ENDER_REFER' then
    Value := DmModOS.QrSTCSEndeRef.Value
  else
  if VarName = 'VARF_ENTIDADE' then
    Value := ObtemEntidade()


  // CORES
  else
  if VarName = 'VARF_MEDIA' then
    Value := CorMedia
  else
  if VarName = 'VARF_CLARA' then
    Value := CorFraca
  else
  if VarName = 'VARF_TITUL' then
    Value := CorTitul
  else
  if VarName = 'VARF_FUNDO' then
    Value := CorFundo
  else
  if VarName = 'VARF_VAZIO' then
    Value := CorVazio
  //
  //
  // FIM CORES


{
  else
  if VarName = 'VARF_LastColOSDep' then
    Value := QrOSDep.RecordCount mod CO_IMP_COLS_DEP
}
  else
  if VarName = 'VARF_LastColOSAlv' then
  begin
    Lins := (QrOSAlv.RecordCount + CO_IMP_COLS_ALV - 1) div CO_IMP_COLS_ALV;
    ALin := (QrOSAlv.RecNo       + CO_IMP_COLS_ALV - 1) div CO_IMP_COLS_ALV;
    if ALin < Lins then
      Value := 0
    else
      Value := QrOSAlv.RecordCount mod CO_IMP_COLS_ALV;
  end

  // ...


  else
  if VarName = 'VARF_STATUS_COD' then
    Value := QrOSCabEstatus.Value
  else
  if VarName = 'VARF_STATUS_NOM' then
    Value := QrOSCabNO_ESTATUS.Value
  else
  if VarName = 'LogoTitRExiste' then
    Value := MyObjects.ArquivoExiste(Dmod.QrOpcoesFiliImgTitRelR.Value, Memo1, CkDoLocalize.Checked)
  else
  if VarName = 'LogoTitRPath' then
    Value := Dmod.QrOpcoesFiliImgTitRelR.Value
  else
  if VarName = 'LogoTitLExiste' then
    Value := MyObjects.ArquivoExiste(Dmod.QrOpcoesFiliImgTitRelL.Value, Memo1, CkDoLocalize.Checked)
  else
  if VarName = 'LogoTitLPath' then
    Value := Dmod.QrOpcoesFiliImgTitRelL.Value
  else
  if VarName = 'VARF_AREA_TOTAL' then
  begin
    Area := ObtemDependencias_Area();
    if Area >= 0.01 then
      Value := Geral.FFT(Area, 2, siPositivo) + ' m�'
    else
      Value := '';
  end else
  if VarName = 'VARF_DEPENDENCIAS' then
    Value := ObtemDependencias_Nomes()
  else
  if VarName = 'VARF_PRAGASALVO' then
    Value := ObtemPragasAlvoServico()
  else
  if VarName = 'ImgPragaExiste' then
    Value := MyObjects.ArquivoExiste(CaminhoArqPraga(), Memo1, CkDoLocalize.Checked)
  else
  if VarName = 'ImgPragaExisteOrc' then
    Value := MyObjects.ArquivoExiste(CaminhoArqPragaOrc(), Memo1, CkDoLocalize.Checked)
  else
  if VarName = 'ImgPragaPath' then
    Value := CaminhoArqPraga()
  else
  if VarName = 'ImgPragaPathOrc' then
    Value := CaminhoArqPragaOrc()
  //
  else
  if VarName = 'VARF_PAGE_INDEX' then
    Value := FPageIndex
  else
  if VarName = 'VARF_CONTRATANTE_PAGANTE' then
    Value := ObtemContratante_E_Pagante()
  else
  if VarName = 'VARF_NumLicOper' then
    Value := Dmod.QrOpcoesFiliNumLicOper.Value
  else
  if VarName = 'ImgCaixaExiste' then
    Value := MyObjects.ArquivoExiste(OSCxIFotoCxa(), Memo1, CkDoLocalize.Checked)
  else
  if VarName = 'ImgCaixaPath' then
    Value := OSCxIFotoCxa()
  else
  if VarName = 'VARF_AG_S' then
  begin
    if QrOSAge.RecordCount > 1 then
      Value := 's'
    else
      Value := ''
  end else
  if VarName = 'VARF_OP_S' then
  begin
    if QrOSAge.RecordCount > 1 then
      Value := 'is'
    else
      Value := 'l'
  end else
  if VarName = 'VARF_CorImpPrdBkg' then
    Value := Dmod.QrOpcoesBugsCorImpPrdBkg.Value
  else
  if VarName = 'VARF_CorImpPrdTxt' then
    Value := Dmod.QrOpcoesBugsCorImpPrdTxt.Value
  else
  if VarName = 'VARF_CorImpPrdBld' then
    Value := Dmod.QrOpcoesBugsCorImpPrdBld.Value
  else
  if VarName = 'VARF_CorImpPrdBkg' then
    Value := Dmod.QrOpcoesBugsCorImpPrdBkg.Value
  else
  if VarName = 'VARF_CorImpPrdTxt' then
    Value := Dmod.QrOpcoesBugsCorImpPrdTxt.Value
  else
  //  Previs�o de execu��o
  if VarName = 'VARF_CorImpExePrvTxt' then
  begin
    if QrOSSta.Locate('Status', CO_BUG_STATUS_0550_AGEN_OK_, []) then
      Value := Dmod.QrOpcoesBugsCorImpPrdTxt.Value
    else
      Value := clBlack;
  end else
  if VarName = 'VARF_CorImpExePrvBkg' then
  begin
    if QrOSSta.Locate('Status', CO_BUG_STATUS_0550_AGEN_OK_, []) then
      Value := Dmod.QrOpcoesBugsCorImpPrdBkg.Value
    else
      Value := clNone;
  end else
  // Alturas dependencias!
  if VarName = 'VARF_ALTURA_LOC_A' then
  begin
    if QrDepsA.RecNo = 1 then
      FRegSID_AlturaLoc := 0;
    //
    if FRegSID_AlturaLoc = QrDepsAc3Loc.Value then
      Value := 0
    else begin
      Value := 0.5 * fr1cm;
      FRegSID_AlturaLoc := QrDepsAc3Loc.Value;
    end;
  end
  else
  if VarName = 'VARF_ALTURA_TYP_A' then
  begin
    if FRegSID_AlturaTyp = QrDepsAc2Typ.Value then
      Value := 0
    else begin
      Value := 0.5 * fr1cm;
      FRegSID_AlturaTyp := QrDepsAc2Typ.Value;
    end;
  end
  else
  if VarName = 'VARF_STRING_LOC_A' then
    Value := QrDepsAn3Loc.Value
  else
  if VarName = 'VARF_STRING_TYP_A' then
    Value := QrDepsAn2Typ.Value
  else
  // Alturas dependencias!
  if VarName = 'VARF_ALTURA_LOC_B' then
  begin
    if QrDepsB.RecNo = 1 then
      FRegSID_AlturaLoc := 0;
    //
    if FRegSID_AlturaLoc = QrDepsBc3Loc.Value then
      Value := 0
    else begin
      Value := 0.5 * fr1cm;
      FRegSID_AlturaLoc := QrDepsBc3Loc.Value;
    end;
  end
  else
  if VarName = 'VARF_ALTURA_TYP_B' then
  begin
    if FRegSID_AlturaTyp = QrDepsBc2Typ.Value then
      Value := 0
    else begin
      Value := 0.5 * fr1cm;
      FRegSID_AlturaTyp := QrDepsBc2Typ.Value;
    end;
  end
  else
  if VarName = 'VARF_STRING_LOC_B' then
    Value := QrDepsBn3Loc.Value
  else
  if VarName = 'VARF_STRING_TYP_B' then
    Value := QrDepsBn2Typ.Value
  else
  if VarName = 'VARF_OS_START_NEW_PAGE' then
    Value := OSStartNewPage()
  else
  if VarName = 'VARF_OPCAO' then
    Value := Geral.FF0(QrOpcoesOpcao.Value)
  else
  if VarName = 'VARF_DEPENDENCIAS_POR_TYP' then
    Value := ObtemDeptosDoTypAtual(QrDeptos)
  else
  if VarName = 'VARF_SUM_OS' then
    Value := Geral.FFT(QrSumOSOrcamTotal.Value, 2, siNegativo)
  else
  if VarName = 'VARF_LUGAR_DE_LUGARES' then
  begin
    if QrSelLug.RecordCount < 2 then
      Value := ''
    else
      Value := ' para o lugar ' + Geral.FFN(QrOSCabSiapTerCad.Value, 3) +
        ' - ' + QrOSCabNO_SiapTerCad.Value;
  end else
  if VarName = 'VARF_TITULO' then
    Value := PageControl1.Pages[FPageIndex].Caption
  {
  begin
    case FPageIndex of
      2: Value := 'FICHA DE VISTORIA';
      4: Value := 'ORDEM DE EXECU��O';
      else Value := '? ? ? ? ?';
    end;
  end} else
  if VarName = 'VARF_ABRANGENCIAS' then
    Value := ObtemAbrangencias()
  //frxDsOSFrmAbr."NO_ABRANGE"
  else
  if VarName = 'LogoCarimboEmpExiste' then
    Value := MyObjects.ArquivoExiste(Dmod.QrOpcoesFiliCarimboEmp.Value, Memo1, CkDoLocalize.Checked)
  else
  if VarName = 'LogoCarimboEmpPath' then
    Value := Dmod.QrOpcoesFiliCarimboEmp.Value
  else
  if VarName = 'LogoRespAge1Existe' then
    Value := MyObjects.ArquivoExiste(QrFAESOSAgeAssImagem.Value, Memo1, CkDoLocalize.Checked)
  else
  if VarName = 'LogoRespAge1Path' then
    Value := QrFAESOSAgeAssImagem.Value
  else
  if VarName = 'LogoRespTec1Existe' then
    Value := MyObjects.ArquivoExiste(Dmod.QrOpcoesFiliRespTec1.Value, Memo1, CkDoLocalize.Checked)
  else
  if VarName = 'LogoRespTec1Path' then
    Value := Dmod.QrOpcoesFiliRespTec1.Value
  else
  if VarName = 'VARF_MostraQuadro2Dep' then
    Value := Dmod.QrOpcoesBugsInfQdr2Dep.Value = 1
  else
  if VarName = 'VARF_ImpImaInfo' then
    Value := CkImpImaInfo.Checked
  else
  if VarName = 'VARF_Cros1BordB' then
    Value := Dmod.QrOpcoesBugsCros1BordB.Value
  else
  if VarName = 'VARF_Cros1Per1B' then
    Value := Dmod.QrOpcoesBugsCros1Per1B.Value
  else
  if VarName = 'VARF_Cros1Res1B' then
    Value := Dmod.QrOpcoesBugsCros1Res1B.Value
  else
  if VarName = 'VARF_Cros1Res2B' then
    Value := Dmod.QrOpcoesBugsCros1Res2B.Value
  else
  if VarName = 'VARF_Cros1Res3B' then
    Value := Dmod.QrOpcoesBugsCros1Res3B.Value
  else
  if VarName = 'VARF_SERVICO_REALIZADO' then
    Value := QrOSFrmCabCALC_FEITO.Value > 99.99
  else
  if VarName = 'VARF_LCD_REALIZADA' then
    Value := QrOSCxaTudoFeito.Value = 1
  else
  if VarName = 'VARF_LOCALIZADORES_SERVICOS_OFERECIDOS' then
    Value := Obtem_LOCALIZADORES_SERVICOS_OFERECIDOS()
  else
  if VarName = 'VAR_TESTE' then
    Value := 'Teste 1' + sLineBreak + 'Teste 2' + sLineBreak + 'Teste 3' + sLineBreak + 'Teste 4' + sLineBreak + 'Teste 5'
  else
  if VarName = 'VARF_FAES_M2Total' then
  begin
    if QrFAESLocaisM2Total.Value <> 0 then
      Value := dmkPF.FFP(QrFAESLocaisM2Total.Value, 2)
    else
      Value := '';
  end else
  if VarName = 'VARF_FAES_LOCAIS' then
    Value := QrFAESLocais.RecordCount
  else
  if VarName = 'VARF_FAES_VICI' then
    Value := QrSImaCav.RecordCount
  else
  if VarName = 'VARF_FAES_PRAGASREC' then
    Value := ObtemListaPragas(False)
  else
  if VarName = 'VARF_FAES_PRAGASENC' then
    Value := ObtemListaPragas(True)
  else
  if VarName = 'VARF_FAES_VISEXE' then
  begin
    if QrOSCabDtaVisExe.Value > 2 then
      Value := QrOSCabTXTVisExe.Value
    else
      Value := '___ / ___ / ______';
  end else
  if VarName = 'VARF_FAES_EXEINI' then
  begin
    if QrOSCabDtaExeIni.Value > 2 then
      Value := QrOSCabTXTExeIni.Value
    else
      Value := '___ / ___ / ______';
  end else
  if VarName = 'VARF_FAES_EXEFIM' then
  begin
    if QrOSCabDtaExeFim.Value > 2 then
      Value := QrOSCabTXTExeFim.Value
    else
      Value := '___ / ___ / ______';
  end else
  if VarName = 'VARF_FAES_MAPPMV' then
    Value := QrOSCabMapPMV.Value
  else
  if VarName = 'VARF_FAES_FONECIT' then
    Value := Geral.FormataTelefone_TT_Curto(QrFAESOSTox5FoneCIT.Value)
  else
  if VarName = 'VARF_FAES_FONECEATOX' then
    Value := Geral.FormataTelefone_TT_Curto(QrFAESOSTox5FoneCEATOX.Value)
  else
  if VarName = 'VARF_OSSrvGarantia' then
  begin
    if QrOSSrvGarantiaDd.Value > 0 then
      Value := FormatFloat('000;-000; ', QrOSSrvGarantiaDd.Value) + ' dia(s)'
    else
      Value := '';
  end else
  if VarName = 'VARF_OSSrvEvacuar' then
  begin
    if QrOSSrvHrEvacuar.Value > 0 then
      Value := FormatFloat('000;-000; ', QrOSSrvHrEvacuar.Value) + ' hora(s)'
    else
      Value := '';
  end else
  if VarName = 'VARF_OSSrvExecucao' then
  begin
    if QrOSSrvHrExecutar.Value > 0 then
      Value := FormatFloat('#,###,###,##0.000;-#,###,###,##0.000; ', QrOSSrvHrExecutar.Value) + ' hora(s)'
    else
      Value := '';
  end else
  if VarName = 'VARF_OSCxaGarantia' then
  begin
    if QrOSCxaGarantiaDd.Value > 0 then
      Value := FormatFloat('000;-000; ', QrOSCxaGarantiaDd.Value) + ' dia(s)'
    else
      Value := '';
  end else
  if VarName = 'VARF_OSCxaDesligar' then
  begin
    if QrOSCxaHrEvacuar.Value > 0 then
      Value := FormatFloat('000;-000; ', QrOSCxaHrEvacuar.Value) + ' hora(s)'
    else
      Value := '';
  end else
  if VarName = 'VARF_OSCxaExecucao' then
  begin
    if QrOSCxaHrExecutar.Value > 0 then
      Value := FormatFloat('#,###,###,##0.000;-#,###,###,##0.000; ', QrOSCxaHrExecutar.Value) + ' hora(s)'
    else
      Value := '';
  end else
  if VarName = 'VARF_HOJE' then
    Value := Trunc(DModG.ObtemAgora())
  else
  if VarName = 'VARF_FAES_EPI' then
    Value := ObtemListaEPIs(QrFAESOSTox5ApMo.Value, QrFAESOSTox5Conta.Value)
  else
  if VarName = 'VARF_OSSRV_DATA_GARANTIA' then
  begin
    if Dmod.QrOpcoesBugsDataGarantiaFaes.Value = 1 then
      Value := QrOSSrvTXT_DATA_GARANTIA.Value
    else
      Value := '';
  end
  else
  if VarName = 'VARF_OSCXA_DATA_GARANTIA' then
  begin
    if Dmod.QrOpcoesBugsDataGarantiaFaes.Value = 1 then
      Value := QrOSCxaTXT_DATA_GARANTIA.Value
    else
      Value := '';
  end else
  if VarName = 'VARF_QTDLOBSFAES' then
    Value := EdQtdLObsFaes.ValueVariant;
  //...
  //MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando impress�o');
  except
    ShowMessage(VarName);
  end
end;

procedure TFmOSImp2.frxGER_OSERV_014_001_AGetValue(const VarName: string;
  var Value: Variant);
{
var
  ALin, Lins: Integer;
}
begin
{  //  Muito requisitados
  if VarName = 'VAR_ATIVO_OSCABALV' then
    Value := ObtemBrushStyle(QrOSCabAlvAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSDEP' then
    Value := ObtemBrushStyle(QrOSDepAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSALV' then
    Value := ObtemBrushStyle(QrOSAlvAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSFRMREC' then
    Value := ObtemBrushStyle(QrOSFrmRecAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSMONREC' then
    Value := ObtemBrushStyle(QrOSMonRecAtivo.Value)
  else
  if VarName = 'VAR_ATIVO_OSPRZ' then
    Value := ObtemBrushStyle(QrOSPrzAtivo.Value)
  //

  else
  if VarName = 'VARF_FATO_GERADOR' then
    Value := QrOSCabNO_FatoGeradr.Value
  else
  if VarName = 'VARF_OS_ORIGEM' then
  begin
    if QrOSCabOSOrigem.Value <> 0 then
      Value := 'OS de origem: ' + Geral.FFN(QrOSCabOSOrigem.Value, 3)
    else
      Value := ' ';
  end
  else
  if VarName = 'VARF_CONTRATO' then
  begin
    if QrOSCabNumContrat.Value <> 0 then
      Value := 'Contrato: ' + Geral.FFN(QrOSCabNumContrat.Value, 3)
    else
      Value := ' ';
  end
  else
}
  if VarName = 'VARF_LUGAR' then
  begin
    if QrOSCabSiapTerCad.Value <> 0 then
      Value := Geral.FFN(QrOSCabSiapTerCad.Value, 3) + ' - ' + QrOSCabNO_SiapTerCad.Value
    else
      Value := ' ';
  end
  else
  if VarName = 'VARF_ENDERECO_MONTADO' then
    Value := EnderecoMontado()
  else
  if VarName = 'VARF_ENDER_REFER' then
    Value := DmModOS.QrSTCSEndeRef.Value
  else
  if VarName = 'VARF_ENTIDADE' then
  begin
{
    if QrOSCabEntidade.Value <> 0 then
      Value := Geral.FFN(QrOSCabEntidade.Value, 3) + ' - ' + QrOSCabNO_ENT.Value +
      '  -  Tel: ' + QrOSCabTXTTel_ENT.Value
}
    if EdCliCod.ValueVariant <> 0 then
      Value := Geral.FFN(EdCliCod.ValueVariant, 3) + ' - ' + EdCliNom.Text +
      '  -  Tel: ' + EdTel_ENT.Text
    else
      Value := 'Sub-cliente: ';
  end


{
  // CORES
  else
  if VarName = 'VARF_MEDIA' then
    Value := CorMedia
  else
  if VarName = 'VARF_CLARA' then
    Value := CorFraca
  else
  if VarName = 'VARF_TITUL' then
    Value := CorTitul
  else
  if VarName = 'VARF_FUNDO' then
    Value := CorFundo
  else
  if VarName = 'VARF_VAZIO' then
    Value := CorVazio
  //
  //
  // FIM CORES


   else
  if VarName = 'VARF_LastColOSDep' then
    Value := QrOSDep.RecordCount mod CO_IMP_COLS_DEP
   else
  if VarName = 'VARF_LastColOSAlv' then
  begin
    Lins := (QrOSAlv.RecordCount + CO_IMP_COLS_ALV - 1) div CO_IMP_COLS_ALV;
    ALin := (QrOSAlv.RecNo       + CO_IMP_COLS_ALV - 1) div CO_IMP_COLS_ALV;
    if ALin < Lins then
      Value := 0
    else
      Value := QrOSAlv.RecordCount mod CO_IMP_COLS_ALV;
  end

  // ...


  else
  if VarName = 'VARF_STATUS_COD' then
    Value := QrOSCabEstatus.Value
  else
  if VarName = 'VARF_STATUS_NOM' then
    Value := QrOSCabNO_ESTATUS.Value
}
  else
  if VarName = 'VARF_PAGINAR' then
    Value := True
end;

procedure TFmOSImp2.frxGER_OSERV_014_002_AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxGER_OSERV_014_000_AGetValue(frxGER_OSERV_014_002_A, VarName, Value);
end;

procedure TFmOSImp2.frxGER_OSERV_014_003_AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxGER_OSERV_014_000_AGetValue(frxGER_OSERV_014_003_A, VarName, Value);
end;

procedure TFmOSImp2.frxGER_OSERV_014_003_BGetValue(const VarName: string;
  var Value: Variant);
begin
  frxGER_OSERV_014_000_AGetValue(frxGER_OSERV_014_003_B, VarName, Value);
end;

procedure TFmOSImp2.frxGER_OSERV_014_005_BGetValue(const VarName: string;
  var Value: Variant);
begin
  frxGER_OSERV_014_000_AGetValue(frxGER_OSERV_014_002_A, VarName, Value);
end;

procedure TFmOSImp2.frxGER_OSERV_014_006_AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxGER_OSERV_014_000_AGetValue(frxGER_OSERV_014_006_A, VarName, Value);
end;

procedure TFmOSImp2.frxGER_OSERV_014_007_AClickObject(Sender: TfrxView;
  Button: TMouseButton; Shift: TShiftState; var Modified: Boolean);
var
  Entidade, GraGru1: Integer;
begin
  Entidade := 0;
  GraGru1  := 0;
  //
  if (Sender.Name = 'PictureRespAge1') or (Sender.Name = 'PictureRespAge2') or
    (Sender.Name = 'Memo63') or (Sender.Name = 'Memo121') or
    (Sender.Name = 'PictureRespTec1') or (Sender.Name = 'PictureRespTec2') or
    (Sender.Name = 'Memo69') or (Sender.Name = 'Memo122')
  then
    Entidade := Geral.IMV(Sender.TagStr);
  //
  if Entidade <> 0 then
  begin
    Entities.MostraFormEntiAssina(Entidade);
    //
    if frxGER_OSERV_014_007_A.Preview <> nil then
      TForm(frxGER_OSERV_014_007_A.Preview.Parent).Close;
  end;
  //
  if (Sender.Name = 'Memo75') then
    GraGru1 := Geral.IMV(Sender.TagStr);
  //
  if GraGru1 <> 0 then
  begin
    Grade_Jan.MostraFormGraGruN(GraGru1);
    //
    if frxGER_OSERV_014_007_A.Preview <> nil then
      TForm(frxGER_OSERV_014_007_A.Preview.Parent).Close;
  end;
end;

procedure TFmOSImp2.frxGER_OSERV_014_007_AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxGER_OSERV_014_000_AGetValue(frxGER_OSERV_014_003_A, VarName, Value);
end;

procedure TFmOSImp2.Imprime002(Titulo: String);
begin
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando impress�o');
  MyObjects.frxDefineDatasets(frxGER_OSERV_014_002_A, [
  frxDsChekLstIts,

  frxDsSelLug,
  frxDsAti,
  frxDsAtr,
  frxDsCav,
  frxDsCdi,
  frxDsCui,

  DModG.frxDsDono,
  frxDsOSCab_OSImp,
  frxDsOSCabAlv,
  frxDsOSChk,
  frxDsOSPrz,
  frxDsOSFPS,
  //
  frxDsOSSrv,
  frxDsOSAlv,
  frxDsOSFrmCab,
  frxDsOSFrmRec,
  frxDsOSFrmDep,
  frxDsOSFrmAbr,
  frxDsOSMonCab,
  //frxDs_OS_Mon_Dep_,
  frxDsOSMonRec,
  frxDsOSCxa,
  //
  frxDsLoc,
  frxDsRes,
  frxDsOSAge,
  //
  frxDsOsCxaAtr,
  frxDsDiarioAdd,
  //
  frxDsDepsB,
  //
  frxDsOSCabXtr,
  //
  frxDsOSPrv
  ]);
  //
  ImprimeOS(frxGER_OSERV_014_002_A, Titulo);
end;

procedure TFmOSImp2.ImprimeExecucao();
var
  Cliente, Proforma: Integer;
  Tempo: TDateTime;
begin
  Tempo := Now();
  Cliente  := EdCliCod.ValueVariant;
  Proforma := EdGrupo.ValueVariant;
  //
  if OSApp_PF.TemProvidenciaOrfa(Cliente) then
  begin
    if Geral.MB_Pergunta('Este cliente tem provid�ncias orf�s!' + #13#10 +
    'Deseja imprimir assim mesmo?') <> ID_YES then
      Exit;
  end;
  //
  FPreVistoria := False;
  FVistoria := False;
  //
  CriaTabelasTemporarias();
  //
  Tempo := Now() - Tempo;
  //ShowMessage(FormatDateTime('hh:nn:ss:zzz', Tempo));
  VAR_TEMPO_SHOW_FRX := Now();
  ReopenSelLug(QrSelLug);
  Imprime002('Ficha de Execu��o');
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  MyObjects.Informa2(LaAviso1, LaAviso2, False, FormatDateTime('hh:nn:ss:zzz', VAR_TEMPO_SHOW_FRX));
end;

function TFmOSImp2.ImprimeGarantia(): Boolean;
begin
  Result := False;
  if not
    OSApp_PF.ContinuaSemDefinicaoDeParzoDePagamento(EdGrupo.ValueVariant, True)
    then
      Exit;
  // Evitar campos de impress�o vazios
  FPreVistoria := False;
  FVistoria := False;
  // Fim Evitar vazios
  //

  CriaTabelasTemporarias();
  //
  // Somente para or�amento e certificado de garantia
  // Deve ser antes da OSSrv!
  RecriaDadosGarantias();
  //
  ReopenCunsCad(); // AtivPrinc
  //
  //...
  //
  ReopenSelLug(QrSelLug);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando impress�o');
  frxGER_OSERV_014_006_A.Variables['VARF_PAGINAR']          := True;
  frxGER_OSERV_014_006_A.Variables['VARF_NumLicOper']       := QuotedStr(Dmod.QrOpcoesFiliNumLicOper.Value);
  frxGER_OSERV_014_006_A.Variables['VARF_OS_ORIGEM']        := QuotedStr('  ');
  frxGER_OSERV_014_006_A.Variables['VARF_CONTRATO']         := QuotedStr(ObtemContrato());
  frxGER_OSERV_014_006_A.Variables['VARF_ENTIDADE']         := QuotedStr(ObtemEntidade());
  frxGER_OSERV_014_006_A.Variables['VARF_LUGAR']            := QuotedStr(ObtemLugar());
  frxGER_OSERV_014_006_A.Variables['VARF_ENDER_REFER']      := QuotedStr(DmModOS.QrSTCSEndeRef.Value);
  frxGER_OSERV_014_006_A.Variables['VARF_ENDERECO_MONTADO'] := QuotedStr(EnderecoMontado);
  frxGER_OSERV_014_006_A.Variables['SloganFooter']          := QuotedStr(VAR_SLOGAN_FOOTER);
  //
  MyObjects.frxDefineDatasets(frxGER_OSERV_014_006_A, [
  frxDsChekLstIts,
  frxDsCunsCad,
  frxDsDesServico,
  DModG.frxDsDono,
  frxDsLocais,
  Dmod.frxDsOpcoesBugs,
  Dmod.frxDsOpcoesFili,
  frxDsOSAge,
  frxDsOSAlv,
  frxDsOSCab_OSImp,
  frxDsOSCabAlv,
  frxDsOSCxa,
  frxDsOSCxI,
  frxDsOSFrmAbr,
  frxDsOSFrmCab,
  frxDsOSFrmDep,
  frxDsOSFrmRec,
  frxDsOSMonCab,
  frxDsOSMonRec,
  frxDsOSPipML,
  frxDsOSPipMI,
  frxDsOSRespMI,
  frxDsOSRespOK,
  frxDsOSSrv,
  frxDsOSTox5,
  frxDsOSTox6,
  frxDsOSToz,
  frxDsOSToz6,
  frxDsSelLug,
  frxDsTipos
  ]);
  //
  InsereVariaveisMonitPIP(frxGER_OSERV_014_006_A);

  Result := ImprimeOS(frxGER_OSERV_014_006_A, 'Comprovante de Execu��o');
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
end;

procedure TFmOSImp2.ImprimeLugar;
begin
  DefineAlturasB(frxGER_OSERV_014_001_A);
  DefineCorMemos(frxGER_OSERV_014_001_A);
  //
  if FApenasGera then
    MyObjects.frxPrepara(frxGER_OSERV_014_001_A, 'Ficha de Lugar')
  else
    MyObjects.frxMostra(frxGER_OSERV_014_001_A, 'Ficha de Lugar');
end;

procedure TFmOSImp2.ImprimeMonitoramento();
begin
  if GG1_SemReferencia() then
    Exit;
  //
  CriaTabelasTemporarias();
  //
  ReopenSelLug(QrSelLug);
  //
  MyObjects.frxDefineDataSets(frxGER_OSERV_014_005_B, [
    DModG.frxDsDono,
    frxDsOSCab_OSImp,
    frxDsOSPipML,
    frxDsOSPipMI
    ]);
  //
  InsereVariaveisMonitPIP(frxGER_OSERV_014_005_B);
  //>> DtaPutPMV + ValCliDd = DtaValPMV
  MyObjects.frxMostra(frxGER_OSERV_014_005_B, 'Monitoramento');
end;

procedure TFmOSImp2.ImprimeFAES();
begin
  CriaTabelasTemporarias();
  //
  RecriaDadosGarantias();
  //
  ReopenSelLug(QrSelLug);
  ReopenFAESLocais();
  ReopenFAESSImaCav();
  ReopenFAESOSAge;
  ReopenFAESOSTox5;
  UMyMod.AbreQuery(QrTermoFAES, Dmod.MyDB);
  //
  MyObjects.frxDefineDataSets(frxGER_OSERV_014_007_A, [
    DmodG.frxDsDono,
    frxDsFAESLocais,
    frxDsFAESOSAge,
    frxDsFAESOSTox5,
    frxDsFAESSSImaCav,
    frxDsLoc,
    Dmod.frxDsOpcoesFili,
    frxDsOSCab_OSImp,
    frxDsOSCabAlv,
    frxDsOSCabXtr,
    frxDsOSCxa,
    frxDsOSFPS,
    frxDsOSSrv,
    frxDsSelLug,
    frxDsTermoFAES,
    frxDsOSAlv
    ]);
  //
  MyObjects.frxMostra(frxGER_OSERV_014_007_A, 'FAES');
end;

procedure TFmOSImp2.ImprimeOrcamento();
begin
  // Evitar campos de impress�o vazios
  FPreVistoria := False;
  FVistoria := False;
  // Fim Evitar vazios
  //
  CriaTabelasTemporarias();
  //
  // Somente para or�amento e certificado de garantia
  // Deve ser antes da OSSrv!
  RecriaDadosGarantias();
  //
  ReopenSelLug(QrSelLug);
  // Deve ser antes do ReopenSumPrz()!!!
  ReopenSumOS();
  ReopenSumPrz();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando impress�o');
  case RGModeloOrcamento.ItemIndex of
    0:
    begin
      MyObjects.frxDefineDatasets(frxGER_OSERV_014_003_A, [
      frxDsChekLstIts,
      frxDsDepsA,
      frxDsDeptos,
      frxDsDesServico,
      DModG.frxDsDono,
      frxDsLocais,
      Dmod.frxDsOpcoesBugs,
      frxDsOSAlv,
      frxDsOSCab_OSImp,
      frxDsOSCabAlv,
      frxDsOSCxa,
      frxDsOSCxI,
      frxDsOSFrmCab,
      frxDsOSFrmRec,
      frxDsOSFrmAbr,
      frxDsOSFrmDep,
      frxDsOSMonCab,
      //frxDs_OS_Mon_Dep_,
      frxDsOSMonRec,
      frxDsOSSrv,
      frxDsOSTox3,
      frxDsSelLug,
      frxDsSumPrz,
      frxDsTipos
      ]);
      ImprimeOS(frxGER_OSERV_014_003_A, 'Or�amento');
    end;
    1:
    begin
      ReopenOrcSrv();
      //
      MyObjects.frxDefineDatasets(frxGER_OSERV_014_003_B, [
      frxDsChekLstIts,
      frxDsDepsA,
      frxDsDeptos,
      frxDsDesServico,
      DModG.frxDsDono,
      frxDsLocais,
      Dmod.frxDsOpcoesBugs,
      frxDsOSAlv,
      frxDsOSCab_OSImp,
      frxDsOSCabAlv,
      frxDsOSCxa,
      frxDsOSCxI,
      frxDsOSFrmCab,
      frxDsOSFrmRec,
      frxDsOSFrmAbr,
      frxDsOSFrmDep,
      frxDsOSMonCab,
      //frxDs_OS_Mon_Dep_,
      frxDsOSMonRec,
      frxDsOSSrv,
      frxDsOSTox3,
      frxDsSelLug,
      frxDsSumPrz,
      frxDsTipos,
      // Novos no B
      frxDsOrcSrv,
      frxDsOrcAlv,
      frxDsOrcTox
      ]);
      ImprimeOS(frxGER_OSERV_014_003_B, 'Or�amento');
    end;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

function TFmOSImp2.ImprimeOS(frx: TfrxReport; Titulo: String): Boolean;
begin
  Result := False;
  DefineAlturasA(frx);
  DefineCorMemos(frx);
  //
  if QrOSCabOperacao.Value in ([1,3]) then
  begin
    if MyObjects.FIC(QrOSCabAlv.RecordCount = 0, nil,
    '� obrigat�rio a informa��o de pelo menos uma praga alvo de pr�-atendimento na OS '
    + Geral.FF0(QrOSCabCodigo.Value) + '  !!!') then
    Exit;
  end;
  //

  if FApenasGera then
    MyObjects.frxPrepara(frx, Titulo)
  else
    MyObjects.frxMostra(frx, Titulo);
  //
  Result := True;
end;

procedure TFmOSImp2.ImprimeTerreno();
var
  Entidades: String;
begin
{
  FOSCab := EdOSCab.ValueVariant;
  if CkLugar.Checked then
    FSTC := EdCliCod.ValueVariant
  else
    FSTC := 0;
}
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
  RecriaTabsTempTerreno();
  //

  //
{
  Info('OSCab');
  InsereOSCab(FOSCab);
}

  Entidades := '';
  if FCliente <> 0 then
    Entidades := Entidades + ', ' + Geral.FF0(FCliente);
  if FContratante <> 0 then
    Entidades := Entidades + ', ' + Geral.FF0(FContratante);
  if FPagante <> 0 then
    Entidades := Entidades + ', ' + Geral.FF0(FPagante);
  Entidades := Copy(Entidades, 3);

  Info('EntiEma');
  InsereEntiEma(Entidades);

  Info('EntiTel');
  InsereEntiTel(Entidades);

  QrSelSTC.First;
  FSeqSTC := -1;
  while not QrSelSTC.Eof do
  begin
    FSeqSTC := FSeqSTC + 1;

    Info('SiapTerCad');
    InsereSTerCad();

    Info('SiapImaCad');
    InsereSImaCad();

    Info('SiapImaDep');
    InsereSImaDep();

    Info('SiapImaCdi');
    InsereSImaCdi();

    Info('SiapImaCav');
    InsereSImaCav();

    Info('SiapImaRes');
    InsereSImaRes();

    Info('SiapImaCui');
    InsereSImaCui();

    Info('SiapImaAti');
    InsereSImaAti();

    Info('SiapImaCxa');
    InsereSImaCxa();

    Info('AtrSICxDef');
    InsereSICxDef();

    Info('AtrSICdDef');
    InsereSICdDef();

    //
    QrSelSTC.Next;
  end;

  Info('MovAmovCad');
  InsereSMovCad();

  Info('AtrAMovDef');
  InsereSMovDef();


  //
  ReopenSelLug(QrSelLug);
  //
  ReopenCunsCad();
  //ReopenSTerCad();
  ReopenEntiEma();
  ReopenEntiTel();
  //
  //
  // Precisa?
  //ReopenOSCab(FImp_OSCab, QrOSCab, DModG.MyPID_DB, FOSCab);
  //
  //...
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando impress�o');
  MyObjects.frxDefineDatasets(frxGER_OSERV_014_001_A, [
  DModG.frxDsDono,
  frxDsCunsCad,
  frxDsEntiEma,
  frxDsEntiTel,
  frxDsSTerCad,
  frxDsSImaCad,
  frxDsSImaDep,
  frxDsSImaCdi,
  frxDsSImaCav,
  frxDsSImaRes,
  frxDsSImaCui,
  frxDsSImaAti,
  frxDsSImaCxa,
  frxDsSICxDef,
  frxDsSICdDef,
  frxDsSMovCad,
  frxDsSMovDef
  ]);
  ImprimeLugar();
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmOSImp2.ImprimeVistoria();
begin
  FPreVistoria := CkPreVistoria.Checked;
  FVistoria := CkVistoria.Checked;
  //
  // Compatibilidade!
  //ReabreClienteTabs(-999999999);
  {
  if FPreVistoria then
    FOSCab := 0
  else
    FOSCab := EdOSCab.ValueVariant;
  }
  //
  CriaTabelasTemporarias();
  ReopenSelLug(QrSelLug);
  Imprime002('Ficha de Vistoria');
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmOSImp2.Info(Txt: String);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Inserindo dados da tabela de "' + Txt + '"');
end;

procedure TFmOSImp2.InsereSICdDef();
var
  I, J, N, Ativo,
  ID_Item, ID_Sorc, AtrCad, AtrIts, MyOrd: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SICdDef, True, [
    'ID_Item', 'ID_Sorc', 'AtrCad',
    'AtrIts', 'MyOrd', 'Ativo'], [
    ], [
    ID_Item, ID_Sorc, AtrCad,
    AtrIts, MyOrd, Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSImaCad() do
    begin
      for J := 1 to QtdSICdDef() do
      begin
        ID_Sorc        := GetImaCad(I);
        ID_Item        := J;
        AtrCad         := 0;
        AtrIts         := 0;
        //
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin
    QrAtrSICdDef.Close;
    QrAtrSICdDef.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
    UMyMod.AbreQuery(QrAtrSICdDef, Dmod.MyDB);
    //
    QrAtrSICdDef.First;
    while not QrAtrSICdDef.Eof do
    begin
        ID_Sorc        := QrAtrSICdDefID_Sorc.Value;
        ID_Item        := QrAtrSICdDefID_Item.Value;
        AtrCad         := QrAtrSICdDefAtrCad.Value;
        AtrIts         := QrAtrSICdDefAtrIts.Value;
        Ativo          := QrAtrSICdDefAtivo.Value;
      //
      InsereAtual();
      //
      QrAtrSICdDef.Next;
    end;
    //
    ReopenAnalise(FImp_SICdDef, 'ID_Sorc', 'ID_Item', CO_IMP_COLS_SICdDef);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          ID_Sorc        := Trunc(QrAnaliseN1.Value);
          ID_Item        := Trunc(QrAnaliseID.Value);
          AtrCad         := 0;
          AtrIts         := 0;
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSICxDef();
var
  I, J, N, Ativo,
  ID_Item, ID_Sorc, AtrCad, AtrIts, MyOrd: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SICxDef, True, [
    'ID_Item', 'ID_Sorc', 'AtrCad',
    'AtrIts', 'MyOrd', 'Ativo'], [
    ], [
    ID_Item, ID_Sorc, AtrCad,
    AtrIts, MyOrd, Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSImaCad() do
    begin
      for J := 1 to QtdSICxDef() do
      begin
        ID_Sorc        := GetImaCad(I);
        ID_Item        := J;
        AtrCad         := 0;
        AtrIts         := 0;
        //
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin
    QrAtrSICxDef.Close;
    QrAtrSICxDef.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
    UMyMod.AbreQuery(QrAtrSICxDef, Dmod.MyDB);
    //
    QrAtrSICxDef.First;
    while not QrAtrSICxDef.Eof do
    begin
        ID_Sorc        := QrAtrSICxDefID_Sorc.Value;
        ID_Item        := QrAtrSICxDefID_Item.Value;
        AtrCad         := QrAtrSICxDefAtrCad.Value;
        AtrIts         := QrAtrSICxDefAtrIts.Value;
        Ativo          := QrAtrSICxDefAtivo.Value;
      //
      InsereAtual();
      //
      QrAtrSICxDef.Next;
    end;
    //
    ReopenAnalise(FImp_SICxDef, 'ID_Sorc', 'ID_Item', CO_IMP_COLS_SICxDef);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          ID_Sorc        := Trunc(QrAnaliseN1.Value);
          ID_Item        := Trunc(QrAnaliseID.Value);
          AtrCad         := 0;
          AtrIts         := 0;
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereEntiEma(Entidades: String);
var
  NO_CONTATO, NO_CARGO, NATAL_TXT, NOMEETC, EMail: String;
  I, N,
  Codigo, MyOrd: Integer;
  DtaNatal: TDateTime;
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_EntiEma, False, [
    'Codigo', 'NO_CONTATO', 'NO_CARGO',
    'DtaNatal', 'EMail',
    'NOMEETC', 'NATAL_TXT',
    'MyOrd'], [
    ], [
    Codigo, NO_CONTATO, NO_CARGO,
    DtaNatal, EMail,
    NOMEETC, NATAL_TXT,
    MyOrd], [
    ], False);
  end;
begin
  MyOrd := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEM, Dmod.MyDB, [
  'SELECT DISTINCT con.Codigo, con.Nome NO_CONTATO, ',
  'crg.Nome NO_CARGO, con.DtaNatal, ',
  'etc.Nome NOMEETC, emt.EMail ',
  'FROM enticontat con ',
  'LEFT JOIN enticonent ece ON ece.Controle=con.Controle ',
  'LEFT JOIN enticargos crg ON crg.Codigo=con.Cargo ',
  'LEFT JOIN entimail emt ON emt.Controle=con.Controle ',
  'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto ',
  'WHERE ece.Codigo IN (' + Entidades + ') ',
  '']);
  QrEM.First;
  while not QrEM.Eof do
  begin
    Codigo         := QrEMCodigo.Value;
    NO_CONTATO     := QrEMNO_CONTATO.Value;
    NO_CARGO       := QrEMNO_CARGO.Value;
    DtaNatal       := QrEMDtaNatal.Value;
    NOMEETC        := QrEMNOMEETC.Value;
    EMail          := QrEMEMail.Value;
    NATAL_TXT      := QrEMNATAL_TXT.Value;
    //
    InsereAtual();
    //
    QrEM.Next;
  end;
  N := QrEM.RecordCount;
  if N < QtdEntiEma() then
    N := QtdEntiEma();
  N := CO_IMP_ENTI_EMA * Trunc((N + CO_IMP_ENTI_EMA - 1) / CO_IMP_ENTI_EMA);
  for I := QrEM.RecordCount + 1 to N do
  begin
    Codigo         := QrEMCodigo.Value;
    NO_CONTATO     := '';
    NO_CARGO       := '';
    DtaNatal       := 0;
    NOMEETC        := '';
    EMail          := '';
    NATAL_TXT      := '';
    //
    InsereAtual();
    //
    QrEM.Next;
  end;
end;

procedure TFmOSImp2.InsereEntiTel(Entidades: String);
var
  NO_CONTATO, NO_CARGO, TEL_TXT, NATAL_TXT, NOMEETC, Telefone, Ramal: String;
  I, N,
  Codigo, MyOrd: Integer;
  DtaNatal: TDateTime;
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_EntiTel, False, [
    'Codigo', 'NO_CONTATO', 'NO_CARGO',
    'DtaNatal', 'Telefone', 'Ramal',
    'NOMEETC', 'TEL_TXT', 'NATAL_TXT',
    'MyOrd'], [
    ], [
    Codigo, NO_CONTATO, NO_CARGO,
    DtaNatal, Telefone, Ramal,
    NOMEETC, TEL_TXT, NATAL_TXT,
    MyOrd], [
    ], False);
  end;
begin
  MyOrd := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrET, Dmod.MyDB, [
  'SELECT DISTINCT con.Codigo, con.Nome NO_CONTATO, ',
  'crg.Nome NO_CARGO, con.DtaNatal, ',
  'etc.Nome NOMEETC, emt.Telefone, emt.Ramal ',
  'FROM enticontat con ',
  'LEFT JOIN enticonent ece ON ece.Controle=con.Controle ',
  'LEFT JOIN enticargos crg ON crg.Codigo=con.Cargo ',
  'LEFT JOIN entitel emt ON emt.Controle=con.Controle ',
  'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto ',
  'WHERE ece.Codigo IN (' + Entidades + ') ',
  '']);
  QrET.First;
  while not QrET.Eof do
  begin
    Codigo         := QrETCodigo.Value;
    NO_CONTATO     := QrETNO_CONTATO.Value;
    NO_CARGO       := QrETNO_CARGO.Value;
    DtaNatal       := QrETDtaNatal.Value;
    NOMEETC        := QrETNOMEETC.Value;
    Telefone       := QrETTelefone.Value;
    Ramal          := QrETRamal.Value;
    NATAL_TXT      := QrETNATAL_TXT.Value;
    TEL_TXT        := QrETTEL_TXT.Value;
    //
    InsereAtual();
    //
    QrET.Next;
  end;
  N := QrET.RecordCount;
  if N < QtdEntiTel() then
    N := QtdEntiTel();
  N := CO_IMP_ENTI_TEL * Trunc((N + CO_IMP_ENTI_TEL - 1) / CO_IMP_ENTI_TEL);
  for I := QrET.RecordCount + 1 to N do
  begin
    Codigo         := QrETCodigo.Value;
    NO_CONTATO     := '';
    NO_CARGO       := '';
    DtaNatal       := 0;
    NOMEETC        := '';
    Telefone       := '';
    Ramal          := '';
    NATAL_TXT      := '';
    TEL_TXT        := '';
    //
    InsereAtual();
    //
    QrET.Next;
  end;
end;

procedure TFmOSImp2.InsereOSAge(OSCab: Integer);
var
  MyOrd, (*Ordem,*) Codigo, Controle, I, N, Agente, Responsa, Ativo: Integer;
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSAge, False, [
    'Codigo', (*'Ordem',*) 'Agente',
    'Responsa', 'MyOrd', 'Ativo'], [
    'Controle'], [
    Codigo, (*Ordem,*) Agente,
    Responsa, MyOrd, Ativo], [
    Controle], False);
  end;
begin
  MyOrd := 0;
  if FPreVistoria then
  begin
    PB2.Position := 0;
    PB2.Max      := QtdOSAge();
    //
    for I := 1 to QtdOSAge() do
    begin
      Codigo         := OSCab;
      Controle       := I;
      //Ordem          := 0;
      Agente         := 0;
      Responsa       := 0;
      Ativo          := 1;
      //
      InsereAtual();
      //
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
    end;
  end else
  begin
    QrOG.Close;
    QrOG.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOG, Dmod.MyDB);
    //
    if QrOG.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOG.RecordCount;
      //
      QrOG.First;
      while not QrOG.Eof do
      begin
        Codigo         := QrOGCodigo.Value;
        Controle       := QrOGControle.Value;
        //Ordem          := QrOGOrdem.Value;
        Agente         := QrOGAgente.Value;
        Responsa       := QrOGResponsa.Value;
        Ativo          := QrOGAtivo.Value;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOG.Next;
      end;
    end;
    //
    if FPageIndex <> (*4*)FPage_Ficha_Execucao then
    begin
      ReopenAnalise(FImp_OSAge, 'Codigo', 'Codigo', CO_IMP_COLS_AGE);
      //
      if QrAnalise.RecordCount > 0 then
      begin
        PB2.Position := 0;
        PB2.Max      := QrAnalise.RecordCount;
        //
        QrAnalise.First;
        while not QrAnalise.Eof do
        begin
          if (Trunc(QrAnaliseATU.Value) > 0) then
          begin
            for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
            begin
              Codigo         := Trunc(QrAnaliseID.Value);
              Controle       := 0;
              //Ordem          := 0;
              Agente         := 0;
              Responsa       := 0;
              Ativo          := 0;
              //
              InsereAtual();
            end;
          end;
          PB2.Position := PB2.Position + 1;
          PB2.Update;
          Application.ProcessMessages;
          //
          QrAnalise.Next;
        end;
      end;
    end;
  end;
end;

procedure TFmOSImp2.InsereOSAlv(OSCab: Integer);
var
  MyOrd, I, J, N, Codigo, Controle, Conta, Praga_Z, Ativo: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSAlv, False, [
    'Codigo', 'Controle', 'Conta',
    'Praga_Z', 'Ativo'], ['MyOrd'
    ], [
    Codigo, Controle, Conta,
    Praga_Z, Ativo], [MyOrd
    ], False);
  end;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    PB2.Position := 0;
    PB2.Max      := QtdOSSrv();
    //
    for I := 1 to QtdOSSrv() do
    begin
      for J := 1 to QtdOSAlv() do
      begin
        Codigo         := OSCab;
        Controle       := I;
        Conta          := J;
        Praga_Z        := 0;
        Ativo          := 1;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
      end;
    end;
  end else
  begin
    QrOA.Close;
    QrOA.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOA, Dmod.MyDB);
    //
    if QrOA.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOA.RecordCount;
      //
      QrOA.First;
      while not QrOA.Eof do
      begin
        Codigo         := QrOACodigo.Value;
        Controle       := QrOAControle.Value;
        Conta          := QrOAConta.Value;
        Praga_Z        := QrOAPraga_Z.Value;
        Ativo          := QrOAAtivo.Value;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOA.Next;
      end;
    end;
    //
    ReopenAnalise(FImp_OSAlv, 'Controle', 'Controle', CO_IMP_COLS_ALV);
    //
    if QrAnalise.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrAnalise.RecordCount;
      //
      QrAnalise.First;
      while not QrAnalise.Eof do
      begin
        if (Trunc(QrAnaliseATU.Value) > 0) then
        begin
          for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
          begin
            Codigo         := QrOACodigo.Value;
            Controle       := Trunc(QrAnaliseID.Value);
            Conta          := 0;
            Praga_Z        := 0;
            Ativo          := 0;
            //
            InsereAtual();
          end;
        end;
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrAnalise.Next;
      end;
    end;
  end;
end;

procedure TFmOSImp2.InsereOSCab(Codigo: Integer);
var
  DtaContat, DtaVisPrv, DtaVisExe, DtaExePrv, DtaExeIni, DtaExeFim: String;
  Entidade, SiapTerCad, Estatus, FatoGeradr, (*OSOrigem,*) DdsPosVda, EntiContat,
  MyOrd, NumContrat, EntPagante, EntContrat, Empresa, ValiDdOrca,
  ExeTxtCli1, ExeTxtCli2, Operacao, MapPMV: Integer;
  ValorTotal, ValorServi, ValorDesco,
  InvalTotal, InvalServi, InvalDesco,
  OrcamTotal, OrcamServi, OrcamDesco,
  ValorPre: Double;
  ObsGaranti, ObsExecuta, LauInfesInt, LauInfesExt, LauParTec: String;
begin
  ValorTotal := 0;
  MyOrd      := 1; // Vai usar no futuro?
  //
  if FPreVistoria then
  begin
    Entidade       := 0;
    SiapTerCad     := 0;
    Estatus        := CO_BUG_STATUS_0000_ABERTURA;
    FatoGeradr     := 0;
    //OSOrigem       := 0;
    DtaContat      := '1889-12-30';
    DtaVisPrv      := '1889-12-30';
    DtaVisExe      := '1889-12-30';
    DtaExePrv      := '1889-12-30';
    DtaExeIni      := '1889-12-30';
    DtaExeFim      := '1889-12-30';
    DdsPosVda      := 0;
    EntiContat     := 0;
    NumContrat     := 0;
    EntPagante     := 0;
    EntContrat     := 0;
    Empresa        := 0;
    ValorTotal     := 0;
    ValorServi     := 0;
    ValorDesco     := 0;
    InvalTotal     := 0;
    InvalServi     := 0;
    InvalDesco     := 0;
    OrcamTotal     := 0;
    OrcamServi     := 0;
    OrcamDesco     := 0;
    ValiDdOrca     := 0;
    ExeTxtCli1     := 0;
    ExeTxtCli2     := 0;
    Operacao       := 0;
    ValorPre       := 0;
    ObsGaranti     := '';
    ObsExecuta     := '';
    MapPMV         := -1;
    LauInfesInt    := '';
    LauInfesExt    := '';
    LauParTec      := '';
  end else
  begin
    ReopenOSCab('oscab', QrOC, Dmod.MyDB, Codigo);
    //
    Entidade       := QrOCEntidade.Value;
    SiapTerCad     := QrOCSiapTerCad.Value;
    Estatus        := QrOCEstatus.Value;
    FatoGeradr     := QrOCFatoGeradr.Value;
    //OSOrigem       := QrOCOSOrigem.Value;
    DtaContat      := Geral.FDT(QrOCDtaContat.Value, 109);
    DtaVisPrv      := Geral.FDT(QrOCDtaVisPrv.Value, 109);
    DtaVisExe      := Geral.FDT(QrOCDtaVisExe.Value, 109);
    DtaExePrv      := Geral.FDT(QrOCDtaExePrv.Value, 109);
    DtaExeIni      := Geral.FDT(QrOCDtaExeIni.Value, 109);
    DtaExeFim      := Geral.FDT(QrOCDtaExeFim.Value, 109);
    DdsPosVda      := QrOCDdsPosVda.Value;
    EntiContat     := QrOCEntiContat.Value;
    NumContrat     := QrOCNumContrat.Value;
    EntPagante     := QrOCEntPagante.Value;
    EntContrat     := QrOCEntContrat.Value;
    Empresa        := QrOCEmpresa.Value;
    ValorServi     := QrOCValorServi.Value;
    ValorDesco     := QrOCValorDesco.Value;
    InvalServi     := QrOCInvalServi.Value;
    InvalDesco     := QrOCInvalDesco.Value;
    InvalTotal     := QrOCInvalTotal.Value;
    OrcamTotal     := QrOCOrcamTotal.Value;
    OrcamServi     := QrOCOrcamServi.Value;
    OrcamDesco     := QrOCOrcamDesco.Value;
    ValiDdOrca     := QrOCValiDdOrca.Value;
    ExeTxtCli1     := QrOCExeTxtCli1.Value;
    ExeTxtCli2     := QrOCExeTxtCli2.Value;
    Operacao       := QrOCOperacao.Value;
    ValorPre       := QrOCValorPre.Value;
    ObsGaranti     := QrOCObsGaranti.Value;
    ObsExecuta     := QrOCObsExecuta.Value;
    MapPMV         := QrOCMapPMV.Value;
    LauInfesInt    := QrOCLauInfesInt.Value;
    LauInfesExt    := QrOCLauInfesExt.Value;
    LauParTec      := QrOCLauParTec.Value;
  end;
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSCab, False, [
  'Entidade', 'SiapTerCad', 'Estatus',
  'FatoGeradr', (*'OSOrigem',*) 'DtaContat',
  'DtaVisPrv', 'DtaVisExe', 'DtaExePrv',
  'DtaExeIni', 'DtaExeFim', 'ValiDdOrca',
  'DdsPosVda', 'EntiContat', 'NumContrat',
  'EntPagante', 'EntContrat', 'Empresa',
  'ValorTotal', 'ValorServi', 'ValorDesco',
  'InvalTotal', 'InvalServi', 'InvalDesco',
  'OrcamTotal', 'OrcamServi', 'OrcamDesco',
  'ExeTxtCli1', 'Operacao', 'ValorPre',
  'ObsGaranti', 'ObsExecuta', 'MapPMV',
  'ExeTxtCli2', 'MyOrd', 'LauInfesInt',
  'LauInfesExt', 'LauParTec'], [
  'Codigo'], [
  Entidade, SiapTerCad, Estatus,
  FatoGeradr, (*OSOrigem,*) DtaContat,
  DtaVisPrv, DtaVisExe, DtaExePrv,
  DtaExeIni, DtaExeFim, ValiDdOrca,
  DdsPosVda, EntiContat, NumContrat,
  EntPagante, EntContrat, Empresa,
  ValorTotal, ValorServi, ValorDesco,
  InvalTotal, InvalServi, InvalDesco,
  OrcamTotal, OrcamServi, OrcamDesco,
  ExeTxtCli1, Operacao, ValorPre,
  ObsGaranti, ObsExecuta, MapPMV,
  ExeTxtCli2, MyOrd, LauInfesInt,
  LauInfesExt, LauParTec], [
  Codigo], False);
end;

procedure TFmOSImp2.InsereOSCabAlv(OSCab: Integer);
var
  MyOrd, Ordem, Codigo, Controle, I, N, Praga_A, Praga_Z, Ativo: Integer;
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSCabAlv, False, [
    'Codigo', 'Ordem', 'Praga_A',
    'Praga_Z', 'MyOrd', 'Ativo'], [
    'Controle'], [
    Codigo, Ordem, Praga_A,
    Praga_Z, MyOrd, Ativo], [
    Controle], False);
  end;
begin
  MyOrd := 0;
  if FPreVistoria then
  begin
    PB2.Position := 0;
    PB2.Max      := QtdOSCabAlv();
    //
    for I := 1 to QtdOSCabAlv() do
    begin
      Codigo         := OSCab;
      Controle       := I;
      Ordem          := 0;
      Praga_A        := -1;  // "."
      Praga_Z        := 0;
      Ativo          := 1;
      //
      InsereAtual();
      //
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
    end;
  end else
  begin
    QrOCA.Close;
    QrOCA.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOCA, Dmod.MyDB);
    //
    if QrOCA.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOCA.RecordCount;
      //
      QrOCA.First;
      while not QrOCA.Eof do
      begin
        Codigo         := QrOCACodigo.Value;
        Controle       := QrOCAControle.Value;
        Ordem          := QrOCAOrdem.Value;
        Praga_A        := QrOCAPraga_A.Value;
        Praga_Z        := QrOCAPraga_Z.Value;
        Ativo          := QrOCAAtivo.Value;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOCA.Next;
      end;
    end;
    //
    ReopenAnalise(FImp_OSCabAlv, 'Codigo', 'Codigo', CO_IMP_COLS_CAB_ALV);
    //
    if QrAnalise.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrAnalise.RecordCount;
      //
      QrAnalise.First;
      while not QrAnalise.Eof do
      begin
        if (Trunc(QrAnaliseATU.Value) > 0) then
        begin
          for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
          begin
            Codigo         := Trunc(QrAnaliseID.Value);
            Controle       := 0;
            Ordem          := 0;
            Praga_A        := -1;  // "."
            Praga_Z        := 0;
            Ativo          := 0;
            //
            InsereAtual();
            //
            PB2.Position := PB2.Position + 1;
            PB2.Update;
            Application.ProcessMessages;
          end;
        end;
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrAnalise.Next;
      end;
    end;
  end;
end;

procedure TFmOSImp2.InsereOSCxa(OSCab: Integer);
var
  MyOrd, I, Ativo,
  Codigo, Controle, GarantiaDd, HrEvacuar,
  Autorizado, Caixa, ChekLstCab, TudoFeito: Integer;
  HrExecutar, ValCalc, ValInfo, ValDesc, ValTota: Double;
  Detalhes: String;
  //
  Continua: Boolean;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSCxa, False, [
    'Codigo', 'Controle',
    'GarantiaDd', 'HrEvacuar', 'HrExecutar',
    'ValCalc', 'ValInfo', 'ValDesc',
    'ValTota', 'Autorizado', 'Caixa',
    'ChekLstCab', 'Detalhes', 'TudoFeito'], [
    'MyOrd', 'Ativo'], [
    Codigo, Controle,
    GarantiaDd, HrEvacuar, HrExecutar,
    ValCalc, ValInfo, ValDesc,
    ValTota, Autorizado, Caixa,
    ChekLstCab, Detalhes, TudoFeito], [
    MyOrd, Ativo], False);
  end;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    PB2.Position := 0;
    PB2.Max      := QtdOSCxa();
    //
    for I := 1 to QtdOSCxa() do
    begin
      Codigo         := OSCab;
      Controle       := I;
      Caixa          := 0;
      GarantiaDd     := 0;
      HrEvacuar      := 0;
      HrExecutar     := 0;
      ValCalc        := 0;
      ValInfo        := 0;
      ValDesc        := 0;
      ValTota        := 0;
      Autorizado     := 0;
      ChekLstCab     := 0;
      Detalhes       := '';
      TudoFeito      := 0;
      Ativo          := 0;
      //
      InsereAtual();
      //
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
    end;
  end else
  begin
    QrOX.Close;
    QrOX.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOX, Dmod.MyDB);
    //
    if QrOX.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOX.RecordCount;
      //
      QrOX.First;
      while not QrOX.Eof do
      begin
        Continua := (FPageIndex < (*4*)FPage_Ficha_Execucao) or (QrOXAutorizado.Value = 1);
        if Continua then
        begin
          Codigo         := QrOXCodigo.Value;
          Controle       := QrOXControle.Value;
          Caixa          := QrOXCaixa.Value;
          GarantiaDd     := QrOXGarantiaDd.Value;
          HrEvacuar      := QrOXHrEvacuar.Value;
          HrExecutar     := QrOXHrExecutar.Value;
          ValCalc        := QrOXValCalc.Value;
          ValInfo        := QrOXValInfo.Value;
          ValDesc        := QrOXValDesc.Value;
          ValTota        := QrOXValTota.Value;
          Autorizado     := QrOXAutorizado.Value;
          ChekLstCab     := QrOXChekLstCab.Value;
          Detalhes       := QrOXDetalhes.Value;
          TudoFeito      := QrOXTudoFeito.Value;
          Ativo          := QrOXAtivo.Value;
          //
          InsereAtual();
        end;
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOX.Next;
      end;
    end;
  end;
end;

{
procedure TFmOSImp.InsereOSDep(OSCab: Integer);
var
  MyOrd, I, J, N, C, Codigo, Controle, Conta, Tabela, Cadastro, Ativo: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSDep, False, [
    'Codigo', 'Controle', 'Conta',
    'Tabela', 'Cadastro', 'Ativo'
    ], ['MyOrd'
    ], [
    Codigo, Controle, Conta,
    Tabela, Cadastro, Ativo], [
    MyOrd], False);
  end;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    for I := 1 to QtdOSSrv() do
    begin
      for J := 1 to QtdOSDep() do
      begin
        Codigo         := OSCab;
        Controle       := I;
        Conta          := J;
        Tabela         := 0;
        Cadastro       := 0;
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin
    QrOD.Close;
    QrOD.Params[0].AsInteger := OSCab;
    /
    UMyMod.AbreQuery(QrOD, Dmod.MyDB);
    //
    QrOD.First;
    Controle := 0;
    while not QrOD.Eof do
    begin
      Codigo         := QrODCodigo.Value;
      Controle       := QrODControle.Value;
      Conta          := QrODConta.Value;
      Tabela         := QrODTabela.Value;
      Cadastro       := QrODCadastro.Value;
      Ativo          := QrODAtivo.Value;
      //
      InsereAtual();
      //
      QrOD.Next;
    end;
    //
    ReopenAnalise(FImp_OSDep, 'Controle', 'Controle', CO_IMP_COLS_DEP);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          Codigo         := QrODCodigo.Value;
          Controle       := Trunc(QrAnaliseID.Value);
          Conta          := 0;
          Tabela         := 0;
          Cadastro       := 0;
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
    //
  end;
end;
}

procedure TFmOSImp2.InsereOSFrmAbr(OSCab: Integer);
var
  MyOrd, I, J, K, N, Codigo, Controle, Conta, IDIts, Ativo: Integer;
  Abrangicie: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSFrmAbr, False, [
    'Codigo', 'Controle', 'Conta',
    'IDIts', 'Abrangicie',
    'Ativo'], [
    'MyOrd'
    ], [
    Codigo, Controle, Conta,
    IDIts, Abrangicie,
    Ativo], [
    MyOrd], False);
  end;
  var
    JJ: Integer;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    JJ           := 0;
    PB2.Position := 0;
    PB2.Max      := QtdOSSrv();
    //
    for I := 1 to QtdOSSrv() do
    begin
      for J := 1 to QtdOSFrmCab() do
      begin
        JJ := JJ + 1;
        for K := 1 to QtdOSFrmAbr() do
        begin
          Codigo         := OSCab;
          Controle       := I;
          Conta          := JJ;
          IDIts          := K;
          Abrangicie     := 0;
          Ativo          := 1;
          //
          InsereAtual();
        end;
      end;
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
    end;
  end else
  begin
    QrOFA.Close;
    QrOFA.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOFA, Dmod.MyDB);
    //
    if QrOFA.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOFA.RecordCount;
      //
      QrOFA.First;
      while not QrOFA.Eof do
      begin
        Codigo         := QrOFACodigo.Value;
        Controle       := QrOFAControle.Value;
        Conta          := QrOFAConta.Value;
        IDIts          := QrOFAIDIts.Value;
        Abrangicie     := QrOFAAbrangicie.Value;
        Ativo          := QrOFAAtivo.Value;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOFA.Next;
      end;
    end;
    //
    ReopenAnalise(FImp_OSFrmAbr, 'Controle', 'Conta', CO_IMP_COLS_FRM_ABR);
    //
    if QrAnalise.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrAnalise.RecordCount;
      //
      QrAnalise.First;
      while not QrAnalise.Eof do
      begin
        if (Trunc(QrAnaliseATU.Value) > 0) then
        begin
          for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
          begin
            Codigo         := QrOFACodigo.Value;
            Controle       := Trunc(QrAnaliseN1.Value);
            Conta          := Trunc(QrAnaliseID.Value);
            IDIts          := 0;
            Abrangicie     := 0;
            Ativo          := 0;
            //
            InsereAtual();
          end;
        end;
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrAnalise.Next;
      end;
    end;
  end;
end;

procedure TFmOSImp2.InsereOSFrmCab(OSCab: Integer);
var
  MyOrd, I, J, Ativo,
  Codigo, Controle, Conta: Integer;
  Nome: String;
  Formula, EquipAplic: Integer;
  QtdTot, QtdQSP, CusTot, PercFeito: Double;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSFrmCab, False, [
    'Codigo', 'Controle', 'Conta',
    'Nome', 'Formula', 'EquipAplic',
    'QtdTot', 'QtdQSP', 'CusTot',
    'PercFeito'], [
    'MyOrd', 'Ativo'
    ], [
    Codigo, Controle, Conta,
    Nome, Formula, EquipAplic,
    QtdTot, QtdQSP, CusTot,
    PercFeito], [
    MyOrd, Ativo
    ], False);
  end;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    PB2.Position := 0;
    PB2.Max      := QtdOSSrv();
    //
    for I := 1 to QtdOSSrv() do
    begin
      for J := 1 to QtdOSFrmCab() do
      begin
        Codigo         := OSCab;
        Controle       := I;
        Conta          := J;
        Nome           := '';
        Formula        := 0;
        EquipAplic     := 0;
        QtdTot         := 0;
        QtdQSP         := 0;
        CusTot         := 0;
        PercFeito      := 0;
        Ativo          := 1;
        //
        InsereAtual();
      end;
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
    end;
  end else
  begin
    QrOFC.Close;
    QrOFC.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOFC, Dmod.MyDB);
    //
    if QrOFC.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOFC.RecordCount;
      //
      QrOFC.First;
      while not QrOFC.Eof do
      begin
        Codigo         := QrOFCCodigo.Value;
        Controle       := QrOFCControle.Value;
        Conta          := QrOFCConta.Value;
        Nome           := QrOFCNome.Value;
        Formula        := QrOFCFormula.Value;
        EquipAplic     := QrOFCEquipAplic.Value;
        QtdTot         := QrOFCQtdTot.Value;
        QtdQSP         := QrOFCQtdQSP.Value;
        CusTot         := QrOFCCusTot.Value;
        PercFeito      := QrOFCPercFeito.Value;
        Ativo          := QrOFCAtivo.Value;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOFC.Next;
      end;
    end;
  end;
end;

procedure TFmOSImp2.InsereOSFrmDep(OSCab: Integer);
var
  MyOrd, I, J, K, N, Codigo, Controle, Conta, IDIts, Ativo: Integer;
  Tabela, Cadastro: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSFrmDep, False, [
    'Codigo', 'Controle', 'Conta',
    'IDIts', 'Tabela', 'Cadastro',
    'Ativo'], [
    'MyOrd'
    ], [
    Codigo, Controle, Conta,
    IDIts, Tabela, Cadastro,
    Ativo], [
    MyOrd], False);
  end;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    PB2.Position := 0;
    PB2.Max      := QtdOSSrv();
    //
    for I := 1 to QtdOSSrv() do
    begin
      for J := 1 to QtdOSFrmCab() do
      begin
        for K := 1 to QtdOSFrmDep() do
        begin
          Codigo         := OSCab;
          Controle       := I;
          Conta          := J;
          IDIts          := K;
          Tabela         := 0;
          Cadastro       := 0;
          Ativo          := 1;
          //
          InsereAtual();
        end;
      end;
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
    end;
  end else
  begin
    QrOFD.Close;
    QrOFD.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOFD, Dmod.MyDB);
    //
    if QrOFD.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOFD.RecordCount;
      //
      QrOFD.First;
      while not QrOFD.Eof do
      begin
        Codigo         := QrOFDCodigo.Value;
        Controle       := QrOFDControle.Value;
        Conta          := QrOFDConta.Value;
        IDIts          := QrOFDIDIts.Value;
        Tabela         := QrOFDTabela.Value;
        Cadastro       := QrOFDCadastro.Value;
        Ativo          := QrOFDAtivo.Value;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOFD.Next;
      end;
    end;
    //
    ReopenAnalise(FImp_OSFrmDep, 'Controle', 'Conta', CO_IMP_COLS_FRM_DEP);
    //
    if QrAnalise.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrAnalise.RecordCount;
      //
      QrAnalise.First;
      while not QrAnalise.Eof do
      begin
        if (Trunc(QrAnaliseATU.Value) > 0) then
        begin
          for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
          begin
            Codigo         := QrOFDCodigo.Value;
            Controle       := Trunc(QrAnaliseN1.Value);
            Conta          := Trunc(QrAnaliseID.Value);
            IDIts          := 0;
            Tabela         := 0;
            Cadastro       := 0;
            Ativo          := 0;
            //
            InsereAtual();
          end;
        end;
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrAnalise.Next;
      end;
    end;
  end;
end;

procedure TFmOSImp2.InsereOSFrmRec(OSCab: Integer);
var
  MyOrd, Ordem, I, J, K, N, Codigo, Controle, Conta, IDIts, Ativo: Integer;
  GraGruX, Reordem: Integer;
  PrvQtd, PrvPrc, PrvVal, UsoQtd, UsoPrc, UsoVal, UsoDec, UsoTot: Double;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSFrmRec, False, [
    'Codigo', 'Controle', 'Conta',
    'IDIts', 'GraGruX', 'PrvQtd',
    'PrvPrc', 'PrvVal', 'UsoQtd',
    'UsoPrc', 'UsoVal', 'UsoDec',
    'UsoTot', 'Ordem', 'Reordem',
    'Ativo'], [
    'MyOrd'
    ], [
    Codigo, Controle, Conta,
    IDIts, GraGruX, PrvQtd,
    PrvPrc, PrvVal, UsoQtd,
    UsoPrc, UsoVal, UsoDec,
    UsoTot, Ordem, Reordem,
    Ativo], [
    MyOrd], False);
  end;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    for I := 1 to QtdOSSrv() do
    begin
      PB2.Position := 0;
      PB2.Max      := QtdOSSrv();
      //
      for J := 1 to QtdOSFrmCab() do
      begin
        for K := 1 to QtdOSFrmRec() do
        begin
          Codigo         := OSCab;
          Controle       := I;
          Conta          := J;
          IDIts          := K;
          GraGruX        := 0;
          PrvQtd         := 0;
          PrvPrc         := 0;
          PrvVal         := 0;
          UsoQtd         := 0;
          UsoPrc         := 0;
          UsoVal         := 0;
          UsoDec         := 0;
          UsoTot         := 0;
          Ordem          := 0;
          Reordem        := 0;
          Ativo          := 1;
          //
          InsereAtual();
          //
          PB2.Position := PB2.Position + 1;
          PB2.Update;
          Application.ProcessMessages;
        end;
      end;
    end;
  end else
  begin
    QrOFR.Close;
    QrOFR.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOFR, Dmod.MyDB);
    //
    if QrOFR.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOFR.RecordCount;
      //
      QrOFR.First;
      while not QrOFR.Eof do
      begin
        Codigo         := QrOFRCodigo.Value;
        Controle       := QrOFRControle.Value;
        Conta          := QrOFRConta.Value;
        IDIts          := QrOFRIDIts.Value;
        GraGruX        := QrOFRGraGruX.Value;
        PrvQtd         := QrOFRPrvQtd.Value;
        PrvPrc         := QrOFRPrvPrc.Value;
        PrvVal         := QrOFRPrvVal.Value;
        UsoQtd         := QrOFRUsoQtd.Value;
        UsoPrc         := QrOFRUsoPrc.Value;
        UsoVal         := QrOFRUsoVal.Value;
        UsoDec         := QrOFRUsoDec.Value;
        UsoTot         := QrOFRUsoTot.Value;
        Ordem          := QrOFROrdem.Value;
        Reordem        := QrOFRReordem.Value;
        Ativo          := QrOFRAtivo.Value;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOFR.Next;
      end;
    end;
    //
    ReopenAnalise(FImp_OSFrmRec, 'Controle', 'Conta', CO_IMP_COLS_FRM_REC);
    //
    if QrAnalise.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrAnalise.RecordCount;
      //
      QrAnalise.First;
      while not QrAnalise.Eof do
      begin
        if (Trunc(QrAnaliseATU.Value) > 0) then
        begin
          for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
          begin
            Codigo         := QrOFRCodigo.Value;
            Controle       := Trunc(QrAnaliseN1.Value);
            Conta          := Trunc(QrAnaliseID.Value);
            IDIts          := 0;
            GraGruX        := 0;
            PrvQtd         := 0;
            PrvPrc         := 0;
            PrvVal         := 0;
            UsoQtd         := 0;
            UsoPrc         := 0;
            UsoVal         := 0;
            UsoDec         := 0;
            UsoTot         := 0;
            Ordem          := 999999999;
            Reordem        := 0;
            Ativo          := 0;
            //
            InsereAtual();
          end;
        end;
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrAnalise.Next;
      end;
    end;
  end;
end;

procedure TFmOSImp2.InsereOSMonCab(OSCab: Integer);
var
  MyOrd, I, J, Ativo,
  Codigo, Controle, Conta: Integer;
  Nome: String;
  Formula, EquipAplic: Integer;
  QtdTot, QtdQSP, CusTot: Double;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSMonCab, False, [
    'Codigo', 'Controle', 'Conta',
    'Nome', 'Formula', 'EquipAplic',
    'QtdTot', 'QtdQSP', 'CusTot'], [
    'MyOrd', 'Ativo'], [
    Codigo, Controle, Conta,
    Nome, Formula, EquipAplic,
    QtdTot, QtdQSP, CusTot], [
    MyOrd, Ativo], False);
  end;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    PB2.Position := 0;
    PB2.Max      := QtdOSSrv();
    //
    for I := 1 to QtdOSSrv() do
    begin
      for J := 1 to QtdOSMonCab() do
      begin
        Codigo         := OSCab;
        Controle       := I;
        Conta          := J;
        Nome           := '';
        Formula        := 0;
        EquipAplic     := 0;
        QtdTot         := 0;
        QtdQSP         := 0;
        CusTot         := 0;
        Ativo          := 1;
        //
        InsereAtual();
      end;
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
    end;
  end else
  begin
    QrOMC.Close;
    QrOMC.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOMC, Dmod.MyDB);
    //
    if QrOMC.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOMC.RecordCount;
      //
      QrOMC.First;
      while not QrOMC.Eof do
      begin
        Codigo         := QrOMCCodigo.Value;
        Controle       := QrOMCControle.Value;
        Conta          := QrOMCConta.Value;
        Nome           := QrOMCNome.Value;
        Formula        := QrOMCFormula.Value;
        EquipAplic     := QrOMCEquipAplic.Value;
        QtdTot         := QrOMCQtdTot.Value;
        QtdQSP         := QrOMCQtdQSP.Value;
        CusTot         := QrOMCCusTot.Value;
        Ativo          := QrOMCAtivo.Value;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOMC.Next;
      end;
    end;
  end;
end;

{
procedure TFmOSImp.Insere_OS_Mon_Dep_(OSCab: Integer);
var
  MyOrd, I, J, K, N, Codigo, Controle, Conta, IDIts, Ativo: Integer;
  Tabela, Cadastro: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp__OS_Mon_Dep_, False, [
    'Codigo', 'Controle', 'Conta',
    'IDIts', 'Tabela', 'Cadastro',
    'Ativo'], [
    'MyOrd'
    ], [
    Codigo, Controle, Conta,
    IDIts, Tabela, Cadastro,
    Ativo], [
    MyOrd], False);
  end;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    for I := 1 to QtdOSSrv() do
    begin
      for J := 1 to QtdOSMonCab() do
      begin
        for K := 1 to Qtd_OS_Mon_Dep_() do
        begin
          Codigo         := OSCab;
          Controle       := I;
          Conta          := J;
          IDIts          := K;
          Tabela         := 0;
          Cadastro       := 0;
          Ativo          := 1;
          //
          InsereAtual();
        end;
      end;
    end;
  end else
  begin
    QrOMD.Close;
    QrOMD.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOMD, Dmod.MyDB);
    //
    QrOMD.First;
    while not QrOMD.Eof do
    begin
      Codigo         := QrOMDCodigo.Value;
      Controle       := QrOMDControle.Value;
      Conta          := QrOMDConta.Value;
      IDIts          := QrOMDIDIts.Value;
      Tabela         := QrOMDTabela.Value;
      Cadastro       := QrOMDCadastro.Value;
      Ativo          := QrOMDAtivo.Value;
      //
      InsereAtual();
      //
      QrOMD.Next;
    end;
    //
    ReopenAnalise(FImp__OS_Mon_Dep_, 'Controle', 'Conta', CO_IMP_COLS_MON_DEP);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          Codigo         := QrOMDCodigo.Value;
          Controle       := Trunc(QrAnaliseN1.Value);
          Conta          := Trunc(QrAnaliseID.Value);
          IDIts          := 0;
          Tabela         := 0;
          Cadastro       := 0;
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
    //
  end;
end;
}

procedure TFmOSImp2.InsereOSMonRec(OSCab: Integer);
var
  MyOrd, Ordem, I, J, K, N, Ativo, Codigo, Controle, Conta, IDIts: Integer;
  GraGruX, Reordem: Integer;
  PrvQtd, PrvPrc, PrvVal, UsoQtd, UsoPrc, UsoVal, UsoDec, UsoTot: Double;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSMonRec, False, [
    'Codigo', 'Controle', 'Conta',
    'IDIts', 'GraGruX', 'PrvQtd',
    'PrvPrc', 'PrvVal', 'UsoQtd',
    'UsoPrc', 'UsoVal', 'UsoDec',
    'UsoTot', 'Ordem', 'Reordem',
    'Ativo'], [
    'MyOrd'], [
    Codigo, Controle, Conta,
    IDIts, GraGruX, PrvQtd,
    PrvPrc, PrvVal, UsoQtd,
    UsoPrc, UsoVal, UsoDec,
    UsoTot, Ordem, Reordem,
    Ativo], [
    MyOrd], False);
  end;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    PB2.Position := 0;
    PB2.Max      := QtdOSSrv();
    //
    for I := 1 to QtdOSSrv() do
    begin
      for J := 1 to QtdOSMonCab() do
      begin
        for K := 1 to QtdOSMonRec() do
        begin
          Codigo         := OSCab;
          Controle       := I;
          Conta          := J;
          IDIts          := K;
          GraGruX        := 0;
          PrvQtd         := 0;
          PrvPrc         := 0;
          PrvVal         := 0;
          UsoQtd         := 0;
          UsoPrc         := 0;
          UsoVal         := 0;
          UsoDec         := 0;
          UsoTot         := 0;
          Ordem          := 0;
          Reordem        := 0;
          Ativo          := 1;
          //
          InsereAtual();
        end;
      end;
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
    end;
  end else
  begin
    QrOMR.Close;
    QrOMR.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOMR, Dmod.MyDB);
    //
    if QrOMR.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOMR.RecordCount;
      //
      QrOMR.First;
      while not QrOMR.Eof do
      begin
        Codigo         := QrOMRCodigo.Value;
        Controle       := QrOMRControle.Value;
        Conta          := QrOMRConta.Value;
        IDIts          := QrOMRIDIts.Value;
        GraGruX        := QrOMRGraGruX.Value;
        PrvQtd         := QrOMRPrvQtd.Value;
        PrvPrc         := QrOMRPrvPrc.Value;
        PrvVal         := QrOMRPrvVal.Value;
        UsoQtd         := QrOMRUsoQtd.Value;
        UsoPrc         := QrOMRUsoPrc.Value;
        UsoVal         := QrOMRUsoVal.Value;
        UsoDec         := QrOMRUsoDec.Value;
        UsoTot         := QrOMRUsoTot.Value;
        Ordem          := QrOMROrdem.Value;
        Reordem        := QrOMRReordem.Value;
        Ativo          := QrOMRAtivo.Value;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOMR.Next;
      end;
    end;
    //
    ReopenAnalise(FImp_OSMonRec, 'Controle', 'Conta', CO_IMP_COLS_MON_REC);
    //
    if QrAnalise.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrAnalise.RecordCount;
      //
      QrAnalise.First;
      while not QrAnalise.Eof do
      begin
        if (Trunc(QrAnaliseATU.Value) > 0) then
        begin
          for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
          begin
            Codigo         := QrOMRCodigo.Value;
            Controle       := Trunc(QrAnaliseN1.Value);
            Conta          := Trunc(QrAnaliseID.Value);
            IDIts          := 0;
            GraGruX        := 0;
            PrvQtd         := 0;
            PrvPrc         := 0;
            PrvVal         := 0;
            UsoQtd         := 0;
            UsoPrc         := 0;
            UsoVal         := 0;
            UsoDec         := 0;
            UsoTot         := 0;
            Ordem          := 999999999;
            Reordem        := 0;
            Ativo          := 0;
            //
            InsereAtual();
          end;
        end;
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrAnalise.Next;
      end;
    end;
  end;
end;

procedure TFmOSImp2.InsereOSPipML_MI(OSCab: Integer);
var
  ItsMyOrd: Integer;

  procedure InsereOSPipML();
  var
    Nome: String;
    Codigo, MyOrd: Integer;
  begin
    Codigo         := QrOSPipMonLstCodigo.Value;
    Nome           := QrOSPipMonLstNome.Value;
    MyOrd          := QrOSPipMonLst.RecNo;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSPipML, False, [
    'Codigo', 'Nome', 'MyOrd'], [
    ], [
    Codigo, Nome, MyOrd], [
    ], False);
  end;

  procedure InsereOSPipMI(Qry: TmySQLQuery; Seq: Integer);
  var
    NO_PIP, NO_ITEM, Sigla, PERGUNTA, RESPOSTA, NO_LOCAL, NO_DEPENDENCI,
    DtaPutPMV, DtaValPMV: String;
    Tabela, Codigo, Controle, Ordem, SubOrdem, SobreOrd, LupForma, LupQtdVzs,
    LupSeqRsp, PrgLstCab, PrgLstIts, MyOrd, SuperOrd, SuperSub, SiapImaDep,
    SiapImaCad, ValCliDd, Relacao: Integer;
    Conta: Int64;
  begin
    ItsMyOrd := ItsMyOrd + 1;
    //
    NO_PIP         := Qry.FieldByName('NO_PIP').AsString;
    NO_ITEM        := Qry.FieldByName('NO_ITEM').AsString;
    Sigla          := Qry.FieldByName('Sigla').AsString;
    Tabela         := Qry.FieldByName('Tabela').AsInteger;
    Codigo         := Qry.FieldByName('Codigo').AsInteger;
    Controle       := Qry.FieldByName('Controle').AsInteger;
    Conta          := Qry.FieldByName('Conta').AsLargeInt;
    Ordem          := Qry.FieldByName('Ordem').AsInteger;
    SubOrdem       := Qry.FieldByName('SubOrdem').AsInteger;
    SobreOrd       := Qry.FieldByName('SobreOrd').AsInteger;
    LupForma       := Qry.FieldByName('LupForma').AsInteger;
    LupQtdVzs      := Qry.FieldByName('LupQtdVzs').AsInteger;
    LupSeqRsp      := Qry.FieldByName('LupSeqRsp').AsInteger;
    PrgLstCab      := Qry.FieldByName('PrgLstCab').AsInteger;
    PrgLstIts      := Qry.FieldByName('PrgLstIts').AsInteger;
    SuperOrd       := Qry.FieldByName('SuperOrd').AsInteger;
    SuperSub       := Qry.FieldByName('SuperSub').AsInteger;
    PERGUNTA       := Qry.FieldByName('NO_PERGUNTA').AsString;
    RESPOSTA       := Qry.FieldByName('RESPOSTA').AsString;
    SiapImaDep     := Qry.FieldByName('SiapImaDep').AsInteger;
    SiapImaCad     := Qry.FieldByName('SiapImaCad').AsInteger;
    NO_LOCAL       := Qry.FieldByName('NO_LOCAL').AsString;
    NO_DEPENDENCI  := Qry.FieldByName('NO_DEPENDENCI').AsString;
    Relacao        := Qry.FieldByName('Relacao').AsInteger;
    //
    MyOrd          := ItsMyOrd;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrValiIsca, Dmod.MyDb, [
    'SELECT cab.DtaExeFim DtaPutPMV, ipr.ValCliDd ',
    'FROM ospipitspr ipr ',
    'LEFT JOIN ospipmon opm ON opm.Controle=ipr.Controle ',
    'LEFT JOIN oscab cab ON cab.Codigo=opm.Codigo ',
    'WHERE opm.PipCad=' + Geral.FF0(Qry.FieldByName('PipCad').AsInteger),
    'ORDER BY DtaPutPMV DESC ',
    '']);
    DtaPutPMV := Geral.FDT(QrValiIscaDtaPutPMV.Value, 109);
    ValCliDd  := QrValiIscaValCliDd.Value;
    if Relacao = CO_PrgLstItsRelacao_COD_Iscas then
      DtaValPMV := Geral.FDT(QrValiIscaDtaPutPMV.Value + QrValiIscaValCliDd.Value, 109)
    else
      DtaValPMV := '0000-00-00';
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSPipMI, False, [
    'NO_PIP', 'NO_ITEM', 'Sigla',
    'Tabela', 'Codigo', 'Controle',
    'Conta', 'Ordem', 'SubOrdem',
    'SobreOrd', 'LupForma', 'LupQtdVzs',
    'LupSeqRsp', 'PrgLstCab', 'PrgLstIts',
    'SuperOrd', 'SuperSub',
    'PERGUNTA', 'RESPOSTA',
    'SiapImadep', 'SiapImaCad',
    'NO_LOCAL', 'NO_DEPENDENCI',
    'DtaPutPMV', 'ValCliDd', 'DtaValPMV',
    'MyOrd', 'Seq', 'Relacao'], [
    ], [
    NO_PIP, NO_ITEM, Sigla,
    Tabela, Codigo, Controle,
    Conta, Ordem, SubOrdem,
    SobreOrd, LupForma, LupQtdVzs,
    LupSeqRsp, PrgLstCab, PrgLstIts,
    SuperOrd, SuperSub,
    PERGUNTA, RESPOSTA,
    SiapImadep, SiapImaCad,
    NO_LOCAL, NO_DEPENDENCI,
    DtaPutPMV, ValCliDd, DtaValPMV,
    MyOrd, Seq, Relacao], [
    ], False);
  end;

  var
    I, N: Integer;
begin
  ReopenOSPipMonLst(OSCab);
  //
  if QrOSPipMonLst.RecordCount > 0 then
  begin
    PB2.Position := 0;
    PB2.Max      := QrOSPipMonLst.RecordCount;
    //
    QrOSPipMonLst.First;
    while not QrOSPipMonLst.Eof do
    begin
      InsereOSPipML();
      //
      ReopenOSPipMonIts(OSCab, QrOSPipMonLstCodigo.Value);
      QrOSPipMonIts.First;
      while not QrOSPipMonIts.Eof do
      begin
        InsereOSPipMI(QrOSPipMonIts, 0);
        //
        if (QrOSPipMonItsLupForma.Value = CO_LupForma_COD_PorLoop) then
        begin
          ReopenOSPipMonSub(OSCab, QrOSPipMonItsPrgLstIts.Value);
          QrOSPipMonSub.First;
          while not QrOSPipMonSub.Eof do
          begin
            // Parei aqui!! Ver o que fazer!
            N := QrOSPipMonItsLupQtdVzs.Value;
            if N = 0 then
              N := 1;
            for I := 1 to N do
            begin
              InsereOSPipMI(QrOSPipMonSub, I);
            end;
            QrOSPipMonSub.Next;
          end;
        end;
        QrOSPipMonIts.Next;
      end;
      //
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
      //
      QrOSPipMonLst.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereOSPipML_MI2(OSCab: Integer);
var
  ItsMyOrd: Integer;

  procedure ReopenOSPipMonIt(OSCab: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrOSPipMonIts, Dmod.MyDB, [
    'SELECT sid.Controle SiapImaDep, sic.Codigo SiapImaCad, ',
    'sic.SCompl2 NO_LOCAL, dep.Nome NO_DEPENDENCI, ',
    '',
    'IF(opi.Respondido<>1, "", ',
    '  ELT(Funcoes, ',
    '  ELT(RespBin + 1, pb0.Nome, pb1.Nome, ""),',
    '  TRIM(TRAILING ",000" FROM REPLACE(RespQtd, ".", ",")), ',
    '  IF(ppr.GraGruX IS NULL, "", CONCAT(',
    '    TRIM(TRAILING ",000" FROM REPLACE(ppr.UsoQtd, ".", ",")),',
    '    " unidade", IF(RespQtd > 1, "s", ""), " da isca ", gg1i.Nome)),',
    '  pai.Nome,',
    '  LEFT(RespTxtLvr, 512))) RESPOSTA, ',
    '',
    'pip.Nome NO_PIP, ELT(opi.Relacao, gg1e.Referencia, ',
    'gg1p.Referencia) NO_ITEM, pcp.Nome NO_PERGUNTA, pcp.Sigla, ',
    'dep.Nome NO_DEPENDENCI, ',
    'opm.PipCad, ',
    'opi.* ',
    'FROM ospipits opi ',
    'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta ',
    'LEFT JOIN ospipmon opm ON opm.Controle=opi.Controle',
    'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad',
    'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts',
    'LEFT JOIN ospipitspr ppr ON ppr.Conta=opi.Conta',
    'LEFT JOIN prgbincad pb0 ON pb0.Codigo=opi.BinarCad0',
    'LEFT JOIN prgbincad pb1 ON pb1.Codigo=opi.BinarCad1',
    'LEFT JOIN gragrux ggxp ON ggxp.Controle=opi.GraGruX',
    'LEFT JOIN gragru1 gg1p ON gg1p.Nivel1=ggxp.GraGru1',
    'LEFT JOIN gragrux ggxe ON ggxe.Controle=pip.Equipamento',
    'LEFT JOIN gragru1 gg1e ON gg1e.Nivel1=ggxe.GraGru1',
    'LEFT JOIN gragrux ggxi ON ggxi.Controle=ppr.GraGruX',
    'LEFT JOIN gragru1 gg1i ON gg1i.Nivel1=ggxi.GraGru1',
    'LEFT JOIN siapimadep sid ON sid.Controle=pip.Dependenci ',
    'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo ',
    'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
    '',
    'WHERE opi.Codigo=' + Geral.FF0(OSCab),
    'ORDER BY opi.Ordem, opi.SubOrdem ',
    '']);
  end;

  procedure InsereOSPipML();
  var
    Nome: String;
    Codigo, MyOrd: Integer;
  begin
    Codigo         := QrOSPipMonLstCodigo.Value;
    Nome           := QrOSPipMonLstNome.Value;
    MyOrd          := QrOSPipMonLst.RecNo;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSPipML, False, [
    'Codigo', 'Nome', 'MyOrd'], [],
    [Codigo, Nome, MyOrd], [], False);
  end;

  procedure InsereOSPipMI(Qry: TmySQLQuery; Seq: Integer);
  var
    NO_PIP, NO_ITEM, Sigla, PERGUNTA, RESPOSTA, NO_LOCAL, NO_DEPENDENCI,
    DtaPutPMV, DtaValPMV: String;
    Tabela, Codigo, Controle, Ordem, SubOrdem, SobreOrd, LupForma, LupQtdVzs,
    LupSeqRsp, PrgLstCab, PrgLstIts, MyOrd, SuperOrd, SuperSub, SiapImaDep,
    SiapImaCad, ValCliDd, Relacao: Integer;
    Conta: Int64;
  begin
    ItsMyOrd := ItsMyOrd + 1;
    //
    NO_PIP         := Qry.FieldByName('NO_PIP').AsString;
    NO_ITEM        := Qry.FieldByName('NO_ITEM').AsString;
    Sigla          := Qry.FieldByName('Sigla').AsString;
    Tabela         := Qry.FieldByName('Tabela').AsInteger;
    Codigo         := Qry.FieldByName('Codigo').AsInteger;
    Controle       := Qry.FieldByName('Controle').AsInteger;
    Conta          := Qry.FieldByName('Conta').AsLargeInt;
    Ordem          := Qry.FieldByName('Ordem').AsInteger;
    SubOrdem       := Qry.FieldByName('SubOrdem').AsInteger;
    SobreOrd       := Qry.FieldByName('SobreOrd').AsInteger;
    LupForma       := Qry.FieldByName('LupForma').AsInteger;
    LupQtdVzs      := Qry.FieldByName('LupQtdVzs').AsInteger;
    LupSeqRsp      := Qry.FieldByName('LupSeqRsp').AsInteger;
    PrgLstCab      := Qry.FieldByName('PrgLstCab').AsInteger;
    PrgLstIts      := Qry.FieldByName('PrgLstIts').AsInteger;
    SuperOrd       := Qry.FieldByName('SuperOrd').AsInteger;
    SuperSub       := Qry.FieldByName('SuperSub').AsInteger;
    PERGUNTA       := Qry.FieldByName('NO_PERGUNTA').AsString;
    RESPOSTA       := Qry.FieldByName('RESPOSTA').AsString;
    SiapImaDep     := Qry.FieldByName('SiapImaDep').AsInteger;
    SiapImaCad     := Qry.FieldByName('SiapImaCad').AsInteger;
    NO_LOCAL       := Qry.FieldByName('NO_LOCAL').AsString;
    NO_DEPENDENCI  := Qry.FieldByName('NO_DEPENDENCI').AsString;
    Relacao        := Qry.FieldByName('Relacao').AsInteger;
    //
    MyOrd          := ItsMyOrd;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrValiIsca, Dmod.MyDb, [
    'SELECT cab.DtaExeFim DtaPutPMV, ipr.ValCliDd ',
    'FROM ospipitspr ipr ',
    'LEFT JOIN ospipmon opm ON opm.Controle=ipr.Controle ',
    'LEFT JOIN oscab cab ON cab.Codigo=opm.Codigo ',
    'WHERE opm.PipCad=' + Geral.FF0(Qry.FieldByName('PipCad').AsInteger),
    'ORDER BY DtaPutPMV DESC ',
    '']);
    DtaPutPMV := Geral.FDT(QrValiIscaDtaPutPMV.Value, 109);
    ValCliDd  := QrValiIscaValCliDd.Value;
    if Relacao = CO_PrgLstItsRelacao_COD_Iscas then
      DtaValPMV := Geral.FDT(QrValiIscaDtaPutPMV.Value + QrValiIscaValCliDd.Value, 109)
    else
      DtaValPMV := '0000-00-00';
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSPipMI, False, [
    'NO_PIP', 'NO_ITEM', 'Sigla',
    'Tabela', 'Codigo', 'Controle',
    'Conta', 'Ordem', 'SubOrdem',
    'SobreOrd', 'LupForma', 'LupQtdVzs',
    'LupSeqRsp', 'PrgLstCab', 'PrgLstIts',
    'SuperOrd', 'SuperSub',
    'PERGUNTA', 'RESPOSTA',
    'SiapImadep', 'SiapImaCad',
    'NO_LOCAL', 'NO_DEPENDENCI',
    'DtaPutPMV', 'ValCliDd', 'DtaValPMV',
    'MyOrd', 'Seq', 'Relacao'], [
    ], [
    NO_PIP, NO_ITEM, Sigla,
    Tabela, Codigo, Controle,
    Conta, Ordem, SubOrdem,
    SobreOrd, LupForma, LupQtdVzs,
    LupSeqRsp, PrgLstCab, PrgLstIts,
    SuperOrd, SuperSub,
    PERGUNTA, RESPOSTA,
    SiapImadep, SiapImaCad,
    NO_LOCAL, NO_DEPENDENCI,
    DtaPutPMV, ValCliDd, DtaValPMV,
    MyOrd, Seq, Relacao], [
    ], False);
  end;

begin
  ReopenOSPipMonLst(OSCab);
  //
  if QrOSPipMonLst.RecordCount > 0 then
  begin
    PB2.Position := 0;
    PB2.Max      := QrOSPipMonLst.RecordCount;
    //
    QrOSPipMonLst.First;
    while not QrOSPipMonLst.Eof do
    begin
      InsereOSPipML();
      //
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
      //
      QrOSPipMonLst.Next;
    end;
  end;
  //
  ReopenOSPipMonIt(OSCab);
  //
  if QrOSPipMonIts.RecordCount > 0 then
  begin
    PB2.Position := 0;
    PB2.Max      := QrOSPipMonIts.RecordCount;
    //
    QrOSPipMonIts.First;
    while not QrOSPipMonIts.Eof do
    begin
      InsereOSPipMI(QrOSPipMonIts, 0);
      //
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
      //
      QrOSPipMonIts.Next;
    end;
  end;
end;

function TFmOSImp2.InsereOSPrn(Relatorio: Integer; Agora: TDateTime):
  Integer;
var
  DtHrIni, DtHrFim: String;
  Codigo, Controle, GruImp: Integer;
begin
  Controle       := 0;
  DtHrIni        := Geral.FDT(Agora, 109);
  DtHrFim        := '0000-00-00 00:00:00';

  //
  GruImp := UMyMod.BPGS1I32('osprn', 'GruImp', '', '', tsPos, stIns, 0);
  QrLugares.First;
  while not QrLugares.Eof do
  begin
    Codigo := QrLugaresOSCab.Value;
    Controle := UMyMod.BPGS1I32('osprn', 'Controle', '', '', tsPos, stIns, Controle);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osprn', False, [
    'Codigo', 'Relatorio', 'DtHrIni',
    'DtHrFim', 'GruImp'], [
    'Controle'], [
    Codigo, Relatorio, DtHrIni,
    DtHrFim, GruImp], [
    Controle], True);
    //
    QrLugares.Next;
  end;
  QrLugares.First;
  Result := GruImp;
end;

procedure TFmOSImp2.InsereOSPrz(OSCab: Integer);
var
  MyOrd, I, N, Ativo, Codigo, Controle, Condicao, Escolhido, Parcelas: Integer;
  DescoPer, BaseVal, DescoVal, TotaVal: Double;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSPrz, False, [
    'Codigo', 'Controle', 'Condicao',
    'DescoPer', 'BaseVal', 'DescoVal',
    'TotaVal', 'Escolhido', 'Parcelas', 
    'Ativo'], [
    'MyOrd'], [
    Codigo, Controle, Condicao,
    DescoPer, BaseVal, DescoVal,
    TotaVal, Escolhido, Parcelas,
    Ativo], [
    MyOrd], False);
  end;
begin
  MyOrd := 0;
  if FPreVistoria then
  begin
    PB2.Position := 0;
    PB2.Max      := QtdOSPrz();
    //
    for I := 1 to QtdOSPrz() do
    begin
      Codigo         := OSCab;
      Controle       := I;
      Condicao       := 0;
      DescoPer       := 0;
      BaseVal        := 0;
      DescoVal       := 0;
      TotaVal        := 0;
      Escolhido      := 0;
      Parcelas       := 0;
      Ativo          := 1;
      //
      InsereAtual();
      //
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
    end;
  end else
  begin
    QrOP.Close;
    QrOP.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOP, Dmod.MyDB);
    //
    if QrOP.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOP.RecordCount;
      //
      QrOP.First;
      while not QrOP.Eof do
      begin
        Codigo         := QrOPCodigo.Value;
        Controle       := QrOPControle.Value;
        Condicao       := QrOPCondicao.Value;
        DescoPer       := QrOPDescoPer.Value;
        BaseVal        := QrOPBaseVal.Value;
        DescoVal       := QrOPDescoVal.Value;
        TotaVal        := QrOPTotaVal.Value;
        Escolhido      := QrOPEscolhido.Value;
        Parcelas       := QrOPParcelas.Value;
        Ativo          := QrOPAtivo.Value;
        //
        InsereAtual();
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOP.Next;
      end;
    end;
    //
    ReopenAnalise(FImp_OSPrz, 'Codigo', 'Codigo', CO_IMP_COLS_PRZ);
    //
    if QrAnalise.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrAnalise.RecordCount;
      //
      QrAnalise.First;
      while not QrAnalise.Eof do
      begin
        if (Trunc(QrAnaliseATU.Value) > 0) then
        begin
          for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
          begin
            Codigo         := Trunc(QrAnaliseID.Value);
            Controle       := 0;
            Condicao       := 0;
            DescoPer       := 0;
            BaseVal        := 0;
            DescoVal       := 0;
            TotaVal        := 0;
            Escolhido      := 0;
            Parcelas       := 0;
            Ativo          := 0;
            //
            InsereAtual();
          end;
        end;
        //
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrAnalise.Next;
      end;
    end;
  end;
end;

procedure TFmOSImp2.InsereOSSrv(OSCab: Integer);
var
  Continua: Boolean;
var
  MyOrd, I, Ativo,
  Codigo, Controle, DesServico, GarantiaDd, HrEvacuar, Autorizado, MoniDdTotl,
  MoniDdIntv, TudoFeitoM, TudoFeitoA: Integer;
  HrExecutar,ValCalc, ValInfo, ValDesc, ValTota: Double;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_OSSrv, False, [
    'Codigo', 'Controle', 'DesServico',
    'GarantiaDd', 'HrEvacuar', 'HrExecutar',
    'ValCalc', 'ValInfo', 'ValDesc',
    'ValTota', 'Autorizado', 'MoniDdTotl',
    'MoniDdIntv', 'TudoFeitoM', 'TudoFeitoA'
    ], [
    'MyOrd', 'Ativo'], [
    Codigo, Controle, DesServico,
    GarantiaDd, HrEvacuar, HrExecutar,
    ValCalc, ValInfo, ValDesc,
    ValTota, Autorizado, MoniDdTotl,
    MoniDdIntv, TudoFeitoM, TudoFeitoA], [
    MyOrd, Ativo], False);
  end;
begin
  MyOrd := 0;
  if FVistoria then
  begin
    PB2.Position := 0;
    PB2.Max      := QtdOSSrv();
    //
    for I := 1 to QtdOSSrv() do
    begin
      Codigo         := OSCab;
      Controle       := I;
      DesServico     := 0;
      GarantiaDd     := 0;
      HrEvacuar      := 0;
      HrExecutar     := 0;
      ValCalc        := 0;
      ValInfo        := 0;
      ValDesc        := 0;
      ValTota        := 0;
      Autorizado     := 0;
      Ativo          := 0;
      MoniDdTotl     := 0;
      MoniDdIntv     := 0;
      TudoFeitoM     := 0;
      TudoFeitoA     := 0;
      //
      InsereAtual();
      //
      PB2.Position := PB2.Position + 1;
      PB2.Update;
      Application.ProcessMessages;
    end;
  end else
  begin
    QrOS.Close;
    QrOS.Params[0].AsInteger := OSCab;
    UMyMod.AbreQuery(QrOS, Dmod.MyDB);
    //
    if QrOS.RecordCount > 0 then
    begin
      PB2.Position := 0;
      PB2.Max      := QrOS.RecordCount;
      //
      QrOS.First;
      while not QrOS.Eof do
      begin
        Continua := (FPageIndex < (*4*)FPage_Ficha_Execucao) or (QrOSAutorizado.Value = 1);
        if Continua then
        begin
          Codigo         := QrOSCodigo.Value;
          Controle       := QrOSControle.Value;
          DesServico     := QrOSDesServico.Value;
          GarantiaDd     := QrOSGarantiaDd.Value;
          HrEvacuar      := QrOSHrEvacuar.Value;
          HrExecutar     := QrOSHrExecutar.Value;
          ValCalc        := QrOSValCalc.Value;
          ValInfo        := QrOSValInfo.Value;
          ValDesc        := QrOSValDesc.Value;
          ValTota        := QrOSValTota.Value;
          Autorizado     := QrOSAutorizado.Value;
          MoniDdTotl     := QrOSMoniDdTotl.Value;
          MoniDdIntv     := QrOSMoniDdIntv.Value;
          TudoFeitoM     := QrOSTudoFeitoM.Value;
          TudoFeitoA     := QrOSTudoFeitoA.Value;
          //
          Ativo          := QrOSAtivo.Value;
          //
          InsereAtual();
          //
        end;
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        QrOS.Next;
      end;
    end;
  end;
end;

procedure TFmOSImp2.InsereSImaAti();
var
  I, J, N, Ativo,
  Codigo, Controle, Atividade, MyOrd: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SImaAti, True, [
    'Codigo', 'Controle', 'Atividade',
    'MyOrd', 'Ativo'], [
    ], [
    Codigo, Controle, Atividade,
    MyOrd, Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSImaCad() do
    begin
      for J := 1 to QtdSImaAti() do
      begin
        Codigo         := GetImaCad(I);
        Controle       := J;
        Atividade     := 0;
        //
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin
    QrSiapImaAti.Close;
    QrSiapImaAti.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
    UMyMod.AbreQuery(QrSiapImaAti, Dmod.MyDB);
    //
    QrSiapImaAti.First;
    Controle := 0;
    while not QrSiapImaAti.Eof do
    begin
      Codigo         := QrSiapImaAtiCodigo.Value;
      Controle       := QrSiapImaAtiControle.Value;
      Atividade      := QrSiapImaAtiAtividade.Value;
      Ativo          := QrSiapImaAtiAtivo.Value;
      //
      InsereAtual();
      //
      QrSiapImaAti.Next;
    end;
    //
    ReopenAnalise(FImp_SImaAti, 'Codigo', 'Codigo', CO_IMP_COLS_SImaAti);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          Codigo         := Trunc(QrAnaliseN1.Value);
          Controle       := Trunc(QrAnaliseID.Value);
          Atividade     := 0;
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSImaCad();
var
  SCompl2: String;
  I, Ativo, Codigo, SiapImaTer, Objeto, Finalidade, TpConstru, MyOrd: Integer;
  M2Constru, M2NaoBuild, M2Terreno, M2Total: Double;
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, Fimp_simacad, False, [
    'Codigo', 'SiapImaTer', 'Objeto',
    'Finalidade', 'TpConstru', 'SCompl2',
    'M2Constru', 'M2NaoBuild', 'M2Terreno',
    'M2Total', 'MyOrd', 'Ativo'], [
    ], [
    Codigo, SiapImaTer, Objeto,
    Finalidade, TpConstru, SCompl2,
    M2Constru, M2NaoBuild, M2Terreno,
    M2Total, MyOrd, Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSImaCad() do
    begin
      Codigo         := GetImaCad(I);
      SiapImaTer     := QrSelSTCLugar.Value;
      Objeto         := 0;
      Finalidade     := 0;
      TpConstru      := 0;
      SCompl2        := '';
      M2Constru      := 0;
      M2NaoBuild     := 0;
      M2Terreno      := 0;
      M2Total        := 0;
      Ativo          := 1;
      //
      InsereAtual();
    end;
  end else
  begin
    QrSiapImaCad.Close;
    QrSiapImaCad.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
    UMyMod.AbreQuery(QrSiapImaCad, Dmod.MyDB);
    //
    QrSiapImaCad.First;
    while not QrSiapImaCad.Eof do
    begin
      Codigo         := QrSiapImaCadCodigo.Value;
      SiapImaTer     := QrSiapImaCadSiapImaTer.Value;
      Objeto         := QrSiapImaCadObjeto.Value;
      Finalidade     := QrSiapImaCadFinalidade.Value;
      TpConstru      := QrSiapImaCadTpConstru.Value;
      SCompl2        := QrSiapImaCadSCompl2.Value;
      M2Constru      := QrSiapImaCadM2Constru.Value;
      M2NaoBuild     := QrSiapImaCadM2NaoBuild.Value;
      M2Terreno      := QrSiapImaCadM2Terreno.Value;
      M2Total        := QrSiapImaCadM2Total.Value;
      Ativo          := QrSiapImaCadAtivo.Value;
      //
      InsereAtual();
      //
      QrSiapImaCad.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSImaCav();
var
  I, J, N, Ativo,
  Codigo, Controle, Caracteris, MyOrd: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SImaCav, True, [
    'Codigo', 'Controle', 'Caracteris',
    'MyOrd', 'Ativo'], [
    ], [
    Codigo, Controle, Caracteris,
    MyOrd, Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSImaCad() do
    begin
      for J := 1 to QtdSImaCav() do
      begin
        Codigo         := GetImaCad(I);
        Controle       := J;
        Caracteris     := 0;
        //
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin
    QrSiapImaCav.Close;
    QrSiapImaCav.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
    UMyMod.AbreQuery(QrSiapImaCav, Dmod.MyDB);
    //
    QrSiapImaCav.First;
    Controle := 0;
    while not QrSiapImaCav.Eof do
    begin
      Codigo         := QrSiapImaCavCodigo.Value;
      Controle       := QrSiapImaCavControle.Value;
      Caracteris     := QrSiapImaCavCaracteris.Value;
      Ativo          := QrSiapImaCavAtivo.Value;
      //
      InsereAtual();
      //
      QrSiapImaCav.Next;
    end;
    //
    ReopenAnalise(FImp_SImaCav, 'Codigo', 'Codigo', CO_IMP_COLS_SImaCav);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          Codigo         := Trunc(QrAnaliseN1.Value);
          Controle       := Trunc(QrAnaliseID.Value);
          Caracteris     := 0;
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSImaCdi();
var
  I, J, N, Ativo,
  Codigo, Controle, Caracteris, MyOrd: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SImaCdi, True, [
    'Codigo', 'Controle', 'Caracteris',
    'MyOrd', 'Ativo'], [
    ], [
    Codigo, Controle, Caracteris,
    MyOrd, Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSImaCad() do
    begin
      for J := 1 to QtdSImaCdi() do
      begin
        Codigo         := GetImaCad(I);
        Controle       := J;
        Caracteris     := 0;
        //
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin
    QrSiapImaCdi.Close;
    QrSiapImaCdi.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
    UMyMod.AbreQuery(QrSiapImaCdi, Dmod.MyDB);
    //
    QrSiapImaCdi.First;
    Controle := 0;
    while not QrSiapImaCdi.Eof do
    begin
      Codigo         := QrSiapImaCdiCodigo.Value;
      Controle       := QrSiapImaCdiControle.Value;
      Caracteris     := QrSiapImaCdiCaracteris.Value;
      Ativo          := QrSiapImaCdiAtivo.Value;
      //
      InsereAtual();
      //
      QrSiapImaCdi.Next;
    end;
    //
    ReopenAnalise(FImp_SImaCdi, 'Codigo', 'Codigo', CO_IMP_COLS_SImaCdi);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          Codigo         := Trunc(QrAnaliseN1.Value);
          Controle       := Trunc(QrAnaliseID.Value);
          Caracteris     := 0;
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSImaCui();
var
  I, J, N, Ativo,
  Codigo, Controle, Cuidado, MyOrd: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SImaCui, True, [
    'Codigo', 'Controle', 'Cuidado',
    'MyOrd', 'Ativo'], [
    ], [
    Codigo, Controle, Cuidado,
    MyOrd, Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSImaCad() do
    begin
      for J := 1 to QtdSImaCui() do
      begin
        Codigo         := GetImaCad(I);
        Controle       := J;
        Cuidado        := 0;
        //
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin
    QrSiapImaCui.Close;
    QrSiapImaCui.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
    UMyMod.AbreQuery(QrSiapImaCui, Dmod.MyDB);
    //
    QrSiapImaCui.First;
    Controle := 0;
    while not QrSiapImaCui.Eof do
    begin
      Codigo         := QrSiapImaCuiCodigo.Value;
      Controle       := QrSiapImaCuiControle.Value;
      Cuidado        := QrSiapImaCuiCuidado.Value;
      Ativo          := QrSiapImaCuiAtivo.Value;
      //
      InsereAtual();
      //
      QrSiapImaCui.Next;
    end;
    //
    ReopenAnalise(FImp_SImaCui, 'Codigo', 'Codigo', CO_IMP_COLS_SImaCui);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          Codigo         := Trunc(QrAnaliseN1.Value);
          Controle       := Trunc(QrAnaliseID.Value);
          Cuidado        := 0;
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSImaCxa();
var
  Local, Acesso, Medidas: String;
  I, J, N, Ativo,
  Codigo, Controle, MatersCxa, FormasCxa, MyOrd: Integer;
  VolumeL: Double;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SImaCxa, True, [
    'Codigo', 'Controle', 'MatersCxa',
    'FormasCxa', 'VolumeL', 'Local',
    'Acesso', 'Medidas', 'MyOrd',
    'Ativo'], [
    ], [
    Codigo, Controle, MatersCxa,
    FormasCxa, VolumeL, Local,
    Acesso, Medidas, MyOrd,
    Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSImaCad() do
    begin
      for J := 1 to QtdSImaCxa() do
      begin
        Codigo         := GetImaCad(I);
        Controle       := J;
        MatersCxa      := 0;
        FormasCxa      := 0;
        VolumeL        := 0;
        Local          := '';
        Acesso         := '';
        Medidas        := '';
        //
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin
    QrSiapImaCxa.Close;
    QrSiapImaCxa.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
    UMyMod.AbreQuery(QrSiapImaCxa, Dmod.MyDB);
    //
    QrSiapImaCxa.First;
    Controle := 0;
    while not QrSiapImaCxa.Eof do
    begin
      Codigo         := QrSiapImaCxaCodigo.Value;
      Controle       := QrSiapImaCxaControle.Value;
      MatersCxa      := QrSiapImaCxaMatersCxa.Value;
      FormasCxa      := QrSiapImaCxaFormasCxa.Value;
      VolumeL        := QrSiapImaCxaVolumeL.Value;
      Local          := QrSiapImaCxaLocal.Value;
      Acesso         := QrSiapImaCxaAcesso.Value;
      Medidas        := QrSiapImaCxaMedidas.Value;
      //
      Ativo          := QrSiapImaCxaAtivo.Value;
      //
      InsereAtual();
      //
      QrSiapImaCxa.Next;
    end;
    //
    ReopenAnalise(FImp_SImaCxa, 'Codigo', 'Codigo', CO_IMP_COLS_SImaCxa);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          Codigo         := Trunc(QrAnaliseN1.Value);
          Controle       := Trunc(QrAnaliseID.Value);
          MatersCxa      := 0;
          FormasCxa      := 0;
          VolumeL        := 0;
          Local          := '';
          Acesso         := '';
          Medidas        := '';
          //
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSImaDep();
var
  I, J, N, Ativo, Codigo, Controle, Dependenci, MyOrd: Integer;
  MLarg, MComp, MAltu: Double;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SImaDep, True, [
    'Codigo', 'Controle', 'Dependenci',
    'MLarg', 'MComp', 'MAltu',
    'MyOrd', 'Ativo'], [
    ], [
    Codigo, Controle, Dependenci,
    MLarg, MComp, MAltu,
    MyOrd, Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSImaCad() do
    begin
      for J := 1 to QtdSImaDep() do
      begin
        Codigo         := GetImaCad(I);
        Controle       := J;
        Dependenci     := 0;
        MLarg          := 0;
        MComp          := 0;
        MAltu          := 0;
        //
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin

    QrSiapImaDep.Close;
    QrSiapImaDep.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
    UMyMod.AbreQuery(QrSiapImaDep, Dmod.MyDB);
    //
    QrSiapImaDep.First;
    Controle := 0;
    while not QrSiapImaDep.Eof do
    begin
      Codigo         := QrSiapImaDepCodigo.Value;
      Controle       := QrSiapImaDepControle.Value;
      Dependenci     := QrSiapImaDepDependenci.Value;
      MLarg          := QrSiapImaDepMLarg.Value;
      MComp          := QrSiapImaDepMComp.Value;
      MAltu          := QrSiapImaDepMAltu.Value;
      Ativo          := QrSiapImaDepAtivo.Value;
      //
      InsereAtual();
      //
      QrSiapImaDep.Next;
    end;
    //
    ReopenAnalise(FImp_SImaDep, 'Codigo', 'Codigo', CO_IMP_COLS_SIMADEP);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          Codigo         := Trunc(QrAnaliseN1.Value);
          Controle       := Trunc(QrAnaliseID.Value);
          Dependenci     := 0;
          MLarg          := 0;
          MComp          := 0;
          MAltu          := 0;
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSImaRes();
var
  Nome, Observacao: String;
  I, J, N, Ativo,
  Codigo, Controle, Residente, AnoNatal, Cuidado, MyOrd: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SImaRes, True, [
    'Codigo', 'Controle', 'Residente',
    'Nome', 'AnoNatal', 'Cuidado',
    'Observacao', 'MyOrd', 'Ativo'], [
    ], [
    Codigo, Controle, Residente,
    Nome, AnoNatal, Cuidado,
    Observacao, MyOrd, Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSImaCad() do
    begin
      for J := 1 to QtdSImaRes() do
      begin
        Codigo         := GetImaCad(I);
        Controle       := J;
        Residente      := 0;
        Nome           := '';
        AnoNatal       := 0;
        Cuidado        := 0;
        Observacao     := '';
        //
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin
    QrSiapImaRes.Close;
    QrSiapImaRes.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
    UMyMod.AbreQuery(QrSiapImaRes, Dmod.MyDB);
    //
    QrSiapImaRes.First;
    Controle := 0;
    while not QrSiapImaRes.Eof do
    begin
      Codigo         := QrSiapImaResCodigo.Value;
      Controle       := QrSiapImaResControle.Value;
      Residente      := QrSiapImaResResidente.Value;
      Nome           := QrSiapImaResNome.Value;
      AnoNatal       := QrSiapImaResAnoNatal.Value;
      Cuidado        := QrSiapImaResCuidado.Value;
      Observacao     := QrSiapImaResObservacao.Value;
      Ativo          := QrSiapImaResAtivo.Value;
      //
      InsereAtual();
      //
      QrSiapImaRes.Next;
    end;
    //
    ReopenAnalise(FImp_SImaRes, 'Codigo', 'Codigo', CO_IMP_COLS_SImaRes);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          Codigo         := Trunc(QrAnaliseN1.Value);
          Controle       := Trunc(QrAnaliseID.Value);
          Residente      := 0;
          Nome           := '';
          AnoNatal       := 0;
          Cuidado        := 0;
          Observacao     := '';
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSMovCad();
var
  Nome: String;
  I, N, MyOrd, Ativo, Codigo, Tipo, Cliente: Integer;
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SMovCad, False, [
    'Nome', 'Tipo', 'Cliente',
    'MyOrd', 'Ativo'], [
    'Codigo'], [
    Nome, Tipo, Cliente,
    MyOrd, Ativo], [
    Codigo], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSMovCad() do
    begin
      Codigo         := I;
      Nome           := '';
      Tipo           := 0;
      Cliente        := 0;
      //
      Ativo          := 1;
      InsereAtual();
    end;
  end else
  begin
    QrMovAmovCad.Close;
    QrMovAmovCad.Params[0].AsInteger := EdCliCod.ValueVariant;
    UMyMod.AbreQuery(QrMovAmovCad, Dmod.MyDB);

    QrMovAmovCad.First;
    while not QrMovAmovCad.Eof do
    begin
      Codigo         := QrMovAmovCadCodigo.Value;
      Nome           := QrMovAmovCadNome.Value;
      Tipo           := QrMovAmovCadTipo.Value;
      Cliente        := QrMovAmovCadCliente.Value;
      //
      Ativo          := 1;
      InsereAtual();
      //
      QrMovAmovCad.Next;
    end;
    //
    ReopenAnalise(FImp_SMovCad, 'Cliente', 'Cliente', CO_IMP_COLS_SMOVCAD);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          Codigo         := Trunc(QrAnaliseID.Value);
          Nome           := '';
          Tipo           := 0;
          Cliente        := 0;
          //
          Ativo          := 0;
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSMovDef();
var
  I, J, N, Ativo,
  ID_Item, ID_Sorc, AtrCad, AtrIts, MyOrd: Integer;
  //
  procedure InsereAtual();
  begin
    MyOrd := MyOrd + 1;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_SMovDef, True, [
    'ID_Item', 'ID_Sorc', 'AtrCad',
    'AtrIts', 'MyOrd', 'Ativo'], [
    ], [
    ID_Item, ID_Sorc, AtrCad,
    AtrIts, MyOrd, Ativo], [
    ], False);
  end;
begin
  MyOrd := 0;
  if CkImoveis.Checked then
  begin
    for I := 1 to QtdSMovCad() do
    begin
      for J := 1 to QtdSMovDef() do
      begin
        ID_Sorc        := I;
        ID_Item        := J;
        AtrCad         := 0;
        AtrIts         := 0;
        //
        Ativo          := 1;
        //
        InsereAtual();
      end;
    end;
  end else
  begin
    QrAtrAMovDef.Close;
    QrAtrAMovDef.Params[0].AsInteger := EdCliCod.ValueVariant;
    UMyMod.AbreQuery(QrAtrAMovDef, Dmod.MyDB);
    //
    QrAtrAMovDef.First;
    while not QrAtrAMovDef.Eof do
    begin
      ID_Sorc        := QrAtrAMovDefID_Sorc.Value;
      ID_Item        := QrAtrAMovDefID_Item.Value;
      AtrCad         := QrAtrAMovDefAtrCad.Value;
      AtrIts         := QrAtrAMovDefAtrIts.Value;
      Ativo          := QrAtrAMovDefAtivo.Value;
      //
      InsereAtual();
      //
      QrAtrAMovDef.Next;
    end;
    //
    ReopenAnalise(FImp_SMovDef, 'ID_Sorc', 'ID_Item', CO_IMP_COLS_SMovDef);
    QrAnalise.First;
    while not QrAnalise.Eof do
    begin
      if (Trunc(QrAnaliseATU.Value) > 0) then
      begin
        for N := Trunc(QrAnaliseATU.Value) + 1 to Trunc(QrAnaliseMAX.Value) do
        begin
          ID_Sorc        := Trunc(QrAnaliseN1.Value);
          ID_Item        := Trunc(QrAnaliseID.Value);
          AtrCad         := 0;
          AtrIts         := 0;
          Ativo          := 0;
          //
          InsereAtual();
        end;
      end;
      //
      QrAnalise.Next;
    end;
  end;
end;

procedure TFmOSImp2.InsereSTerCad();
var
  Nome, SRua, SCompl, SBairro, SCidade, SUF, SPais, SEndeRef, STe1: String;
  Ativo, Codigo, Cliente, SLograd, SNumero, SCEP, SCodMunici, SCodiPais,
  MyOrd, LugInfo: Integer;
  M2Constru, M2NaoBuild, M2Terreno, M2Total: Double;
begin
  MyOrd := 1; // Vai usar no futuro?
  Codigo := QrSiapTerCadCodigo.Value;
  if not CkLugar.Checked then
  begin
    LugInfo        := 0;
    Nome           := '';
    Cliente        := 0;
    SLograd        := 0;
    SRua           := '';
    SNumero        := 0;
    SCompl         := '';
    SBairro        := '';
    SCidade        := '';
    SUF            := '';
    SCEP           := 0;
    SPais          := '';
    SEndeRef       := '';
    SCodMunici     := 0;
    SCodiPais      := 0;
    STe1           := '';
    M2Constru      := 0;
    M2NaoBuild     := 0;
    M2Terreno      := 0;
    M2Total        := 0;
    Ativo          := 1;
  end else
  begin
    LugInfo        := Codigo;
    Nome           := QrSiapTerCadNome.Value;
    Cliente        := QrSiapTerCadCliente.Value;
    SLograd        := QrSiapTerCadSLograd.Value;
    SRua           := QrSiapTerCadSRua.Value;
    SNumero        := QrSiapTerCadSNumero.Value;
    SCompl         := QrSiapTerCadSCompl.Value;
    SBairro        := QrSiapTerCadSBairro.Value;
    SCidade        := QrSiapTerCadSCidade.Value;
    SUF            := QrSiapTerCadSUF.Value;
    SCEP           := QrSiapTerCadSCEP.Value;
    SPais          := QrSiapTerCadSPais.Value;
    SEndeRef       := QrSiapTerCadSEndeRef.Value;
    SCodMunici     := QrSiapTerCadSCodMunici.Value;
    SCodiPais      := QrSiapTerCadSCodiPais.Value;
    STe1           := QrSiapTerCadSTe1.Value;
    M2Constru      := QrSiapTerCadM2Constru.Value;
    M2NaoBuild     := QrSiapTerCadM2NaoBuild.Value;
    M2Terreno      := QrSiapTerCadM2Terreno.Value;
    M2Total        := QrSiapTerCadM2Total.Value;
    Ativo          := QrSiapTerCadAtivo.Value;
  end;
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FImp_STerCad, False, [
  'Codigo', 'Nome', 'Cliente',
  'SLograd', 'SRua', 'SNumero',
  'SCompl', 'SBairro', 'SCidade',
  'SUF', 'SCEP', 'SPais',
  'SEndeRef', 'SCodMunici', 'SCodiPais',
  'STe1', 'M2Constru', 'M2NaoBuild',
  'M2Terreno', 'M2Total', 'MyOrd',
  'LugInfo',
  'Ativo'], [
  ], [
  Codigo, Nome, Cliente,
  SLograd, SRua, SNumero,
  SCompl, SBairro, SCidade,
  SUF, SCEP, SPais,
  SEndeRef, SCodMunici, SCodiPais,
  STe1, M2Constru, M2NaoBuild,
  M2Terreno, M2Total, MyOrd,
  LugInfo,
  Ativo], [
  ], False);
end;

procedure TFmOSImp2.InsereVariaveisMonitPIP(frxReport: TfrxReport);
begin
  frxReport.Variables.Clear;
  //
  frxReport.Variables[' ' + 'Perguntas'] := Null;
  //
  //frxReport.Variables['My Variable 1'] := 10;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrgCadPrg, Dmod.MyDB, [
  'SELECT Sigla, Largura ',
  'FROM prgcadprg ',
  '']);
  //
  if QrPrgCadPrg.RecordCount > 0 then
  begin
    PB1.Position := 0;
    PB1.Max      := QrPrgCadPrg.RecordCount;
    //
    QrPrgCadPrg.First;
    while not QrPrgCadPrg.Eof do
    begin
      frxReport.Variables[QrPrgCadPrgSigla.Value] := QrPrgCadPrgLargura.Value;
      //
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      QrPrgCadPrg.Next;
    end;
  end;
  PB1.Position := 0;
end;

function TFmOSImp2.ObtemAbrangencias(): String;
begin
  Result := '';
  QrOSFrmAbr.First;
  while not QrOSFrmAbr.Eof do
  begin
    Result := Result + ', ' + QrOSFrmAbrNO_ABRANGE.Value;
    //
    QrOSFrmAbr.Next;
  end;
  if Result <> '' then
    Result := Copy(Result, 3);
end;

function TFmOSImp2.ObtemBrushStyle(Ativo: Integer): TBrushStyle;
begin
  Result := bsSolid;
  if Ativo = 0 then
  begin
    case RGBrushStyle.ItemIndex of
      0: Result := bsSolid;
      1: Result := bsClear;
      2: Result := bsHorizontal;
      3: Result := bsVertical;
      4: Result := bsFDiagonal;
      5: Result := bsBDiagonal;
      6: Result := bsCross;
      7: Result := bsDiagCross;
    end;
  end;
end;

function TFmOSImp2.ObtemContratante_E_Pagante(): String;
begin
  Result := '';
  if QrOSCabEntContrat.Value = QrOSCabEntPagante.Value then
  begin
    Result := 'Contratante e pagante: ';
    if QrOSCabEntContrat.Value = 0 then
      Result := Result + 'N�o definido!'
    else
    if QrOSCabEntContrat.Value = QrOSCabEntidade.Value then
      Result := Result + 'O mesmo.'
    else
      Result := Result + QrOSCabNO_CONTRATANTE.Value;
  end else
  begin
    Result := 'Contratante: ';
    if QrOSCabEntContrat.Value = 0 then
      Result := Result + 'N�o definido!'
{   OS Dmk n� 1750
    else
    if QrOSCabEntContrat.Value = QrOSCabEntidade.Value then
      Result := Result + 'O mesmo.'
}
    else
      Result := Result + QrOSCabNO_CONTRATANTE.Value;


    Result := Result + '  |  Pagante: ';


    if QrOSCabEntPagante.Value = 0 then
      Result := Result + 'N�o definido!'
{   OS Dmk n� 1750
    else
    if QrOSCabEntPagante.Value = QrOSCabEntidade.Value then
      Result := Result + 'O mesmo.'
}
    else
      Result := Result + QrOSCabNO_PAGANTE.Value;
  end;
end;

function TFmOSImp2.ObtemContrato(): String;
begin
  if QrOSCabNumContrat.Value <> 0 then
    Result := 'Contrato: ' + Geral.FFN(QrOSCabNumContrat.Value, 3)
  else
    Result := ' ';
end;

function TFmOSImp2.ObtemDependencias_Area(): Double;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSAreDep, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _imp_osdepareas; ',
  ' ',
  'CREATE TABLE _imp_osdepareas ',
  ' ',
  'SELECT sid.MLarg, sid.MComp, sid.MAltu',
  'FROM _imp_osfrmdep osd ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro ',
  'WHERE osd.Controle=' + Geral.FF0(QrOSSrvControle.Value),
  ' ',
{
  'UNION ',
  ' ',
  'SELECT sid.MLarg, sid.MComp, sid.MAltu',
  'FROM _imp__OS_Mon_Dep_ osd ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro ',
  'WHERE osd.Controle=' + Controle,
}
  '; ',
  ' ',
  'SELECT * ',
  'FROM _imp_osdepareas',
  'WHERE MLarg + MComp + MAltu > 0;',
  ' ',
  //'DROP TABLE IF EXISTS _imp_osdepareas; ',
  '']);
  QrOSAreDep.First;
  while not QrOSAreDep.Eof do
  begin
    Result := Result + (QrOSAreDepMComp.Value * QrOSAreDepMLarg.Value);
    //
    QrOSAreDep.Next;
  end;
end;

function TFmOSImp2.ObtemDependencias_Nomes(): String;
var
  Controle, Extra: String;
begin
  Result := '';
  Controle := Geral.FF0(QrOSSrvControle.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSAllDep, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _imp_osdepnomes; ',
  ' ',
  'CREATE TABLE _imp_osdepnomes ',
  ' ',
  'SELECT osd.*, ELT(osd.Tabela, "I", "M", "?") SIGLA, ',
  'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") NO_Campo ',
  'FROM _imp_osfrmdep osd ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci ',
  'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro ',
  'WHERE osd.Controle=' + Controle,
  ' ',
(*
  'UNION ',
  ' ',
  'SELECT osd.*, ELT(osd.Tabela, "I", "M", "?") SIGLA, ',
  'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") NO_Campo ',
  'FROM _imp__OS_Mon_Dep_ osd ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci ',
  'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro ',
  'WHERE osd.Controle=' + Controle,
*)
  '; ',
  ' ',
  'SELECT DISTINCT SIGLA, NO_Campo ',
  'FROM _imp_osdepnomes ',
  'WHERE NO_CAMPO <> ""; ',
  ' ',
  'DROP TABLE IF EXISTS _imp_osdepnomes; ',
  '']);
  QrOSAllDep.First;
  while not QrOSAllDep.Eof do
  begin
    if QrOSAllDepNO_Campo.Value <> '' then
    begin
      if Dmod.QrOpcoesBugsInfoImoMov.Value = 1 then
        Extra := ' (' + QrOSAllDepSIGLA.Value + ')'
      else Extra := '';
      //
      Result := Result + ', ' + QrOSAllDepNO_Campo.Value + Extra;
    end;
    //
    QrOSAllDep.Next;
  end;
  if Length(Result) > 2 then
    Result := Copy(Result, 3);
  //
end;

function TFmOSImp2.ObtemDeptosDoTypAtual(Qry: TmySQLQuery): String;
var
  Extra: String;
begin
  Result := '';
  Qry.First;
  while not Qry.Eof do
  begin
    if Dmod.QrOpcoesBugsInfoImoMov.Value = 1 then
      Extra := '(' + Qry.FieldByName('SIGLA').AsString + ') '
    else Extra := '';
    Result := Result + ', ' + Extra + Qry.FieldByName('n1Dep').AsString;
    //
    Qry.Next;
  end;
  Result := Copy(Result, 3);
  if Qry.RecordCount < 2 then
    Result := 'Item: ' + Result
  else
    Result := 'Itens: ' + Result;
end;

function TFmOSImp2.ObtemEntidade(): String;
begin
  if QrOSCabEntidade.Value <> 0 then
    Result := Geral.FFN(QrOSCabEntidade.Value, 3) + ' - ' + QrOSCabNO_ENT.Value +
    '  -  Tel: ' + QrOSCabTXTTel_ENT.Value
  else
    Result := 'Sub-cliente: ';
end;

function TFmOSImp2.ObtemListaEPIs(ApMo, Conta: Integer): String;
var
  Qry: TmySQLQuery;
  Tabela, Txt: String;
begin
  Result := '';
  //
  if (ApMo = 1) or (ApMo = 2) then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      if ApMo = 1 then //osfrmrec
        Tabela := 'osfrmepi'
      else
        Tabela := 'osmonepi';
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT epi.Controle, ggx.Controle GraGruX, ',
        'gg1.GraTabApp, gg1.Nivel1, gg1.Nome ',
        'FROM ' + Tabela + ' epi ',
        'LEFT JOIN gragrux ggx ON ggx.Controle = epi.GraGruX ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
        'WHERE epi.Conta=' + Geral.FF0(Conta),
        'ORDER BY gg1.Nome ',
        '']);
      //
      if Qry.RecordCount > 0 then
      begin
        while not Qry.Eof do
        begin
          Txt := Txt + Qry.FieldByName('Nome').AsString;
          //
          if Qry.RecNo <> Qry.RecordCount then
            Txt := Txt + ', '
          else
            Txt := Txt + ';';
          Qry.Next;
        end;
        Result := Txt;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

function TFmOSImp2.ObtemListaPragas(Encontradas: Boolean): String;
var
  Qry: TmySQLQuery;
  Txt: String;
begin
  Result := '';
  Qry    := TmySQLQuery.Create(Dmod);
  try
    if Encontradas then
    begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT prc.Nome ',
      'FROM osalv osa ',
      'LEFT JOIN praga_z prc ON prc.Codigo=osa.Praga_Z ',
      'WHERE osa.Codigo=' + Geral.FF0(QrSelLugOSCab.Value),
      '']);
    end else
    begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) Nome ',
      'FROM oscabalv oca ',
      'LEFT JOIN praga_a pca ON pca.Codigo=oca.Praga_A ',
      'LEFT JOIN praga_z pcz ON pcz.Codigo=oca.Praga_Z ',
      'WHERE oca.Codigo=' + Geral.FF0(QrSelLugOSCab.Value),
      'ORDER BY oca.Ordem ',
      '']);
    end;
    //
    if Qry.RecordCount > 0 then
    begin
      while not Qry.Eof do
      begin
        Txt := Txt + Qry.FieldByName('Nome').AsString;
        //
        if Qry.RecNo <> Qry.RecordCount then
          Txt := Txt + ', '
        else
          Txt := Txt + ';';
        Qry.Next;
      end;
      Result := Txt;
    end;
  finally
    Qry.Free;
  end;
end;

function TFmOSImp2.ObtemLugar(): String;
begin
  if QrOSCabSiapTerCad.Value <> 0 then
    Result := Geral.FFN(QrOSCabSiapTerCad.Value, 3) + ' - ' + QrOSCabNO_SiapTerCad.Value
  else
    Result := ' ';
end;

function TFmOSImp2.ObtemPragasAlvoServico(): String;
begin
  QrOSAlv.First;
  while not QrOSAlv.Eof do
  begin
    if QrOSAlvNO_Praga_Z.Value <> '' then
      Result := Result + ', ' + QrOSAlvNO_Praga_Z.Value;
    QrOSAlv.Next;
  end;
  if Length(Result) > 2 then
    Result := Copy(Result, 3);
end;

function TFmOSImp2.Obtem_LOCALIZADORES_SERVICOS_OFERECIDOS: String;
begin
  Result := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocalizad1, DModG.MyPID_DB, [
  'SELECT DISTINCT Codigo ',
  'FROM _imp_ossrv srv ',
  'WHERE DesServico=' + Geral.FF0(QrOrcSrvCodigo.Value),
  'ORDER BY Codigo ',
  '']);
  QrLocalizad1.First;
  while not QrLocalizad1.Eof do
  begin
    Result := Result + ', ' + Geral.FF0(QrLocalizad1Codigo.Value);
    //
    QrLocalizad1.Next;
  end;
  Result := Copy(Result, 3);
end;

function TFmOSImp2.OSCxIFotoCxa(): String;
var
  Host, SDir, RDir, DirDest: String;
  Empresa: Integer;
begin
  Host := Dmod.QrOpcoesBugsCxaIPv4.Value;
  SDir := Dmod.QrOpcoesBugsCxaSDir.Value;
  Empresa := 0;
  RDir := '';
  //
  if CkDoLocalize.Checked then
  begin
    if MyObjects.TentaDefinirDiretorio(
    Host, SDir, Empresa, RDir, LaAviso1, LaAviso2, False, Memo1, DirDest) then
      Result := dmkPF.CaminhoArquivo(DirDest, QrOSCxIFotoCxa.Value, '')
    else
      Result := '';
  end else
    Result := '';
end;

function TFmOSImp2.OSStartNewPage(): Boolean;
begin
{
selecionar op��es de StartNewPage do Header13 do  frxGER_OSERV_014_002_A:
  0: Autom�tico
  1: Sim StartNewPage
  2: N�o StartNewPage
}
  case RGOSStartNewPage.ItemIndex of
    0: Result := QrLugares.RecordCount > 1;
    1: Result := True;
    else Result := False;
  end;
end;

procedure TFmOSImp2.PreparaselacaoDeOSs(Grupo: Integer);
var
  Cli: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando sele��o');
  //
  F_OS_Imp_ :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_OS_Imp_, DmodG.QrUpdPID1, False);
  //
  // Se foi chamado pela janela de OSs (FmOSCab)...
  if Grupo <> 0 then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'DELETE FROM _os_imp_; ',
    'INSERT INTO _os_imp_ ',
    'SELECT cab.Grupo, cab.SiapTerCad Lugar, cab.Opcao,  ',
    'cab.Codigo OSCab, stc.Nome NO_Lugar, 1 Ativo ',
    'FROM ' + TMeuDB + '.oscab cab ',
    'LEFT JOIN ' + TMeuDB + '.siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
    'WHERE Grupo=' + Geral.FF0(Grupo),
    '']);
    //
    ReopenOpcoes();
    //
    ReopenDiarioAdd(Grupo);
  end else
  //  ... caso contr�rio deve ter sido chamado pelo cadastro de cliente (FmCunsCad)
  begin
    DBGOpcao.Visible := False;
    //
    Cli := Geral.FF0(EdCliCod.ValueVariant);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'DELETE FROM _os_imp_; ',
    'INSERT INTO _os_imp_ ',
    'SELECT 0 Grupo, stc.Codigo Lugar, 0 Opcao,  ',
    '0 OSCab, stc.Nome NO_Lugar, 1 Ativo ',
    'FROM ' + TMeuDB + '.siaptercad stc ',
    'WHERE stc.Cliente=' + Cli,
    '']);
    //
    ReopenOpcoes();
    //ReopenLugares(Geral.IMV(Cli), 0);
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmOSImp2.QrEMCalcFields(DataSet: TDataSet);
begin
  QrEMNATAL_TXT.Value := Geral.FDT(QrEMDtaNatal.Value, 3);
end;

procedure TFmOSImp2.QrETCalcFields(DataSet: TDataSet);
begin
  QrETTEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrETTelefone.Value);
  QrETNATAL_TXT.Value := Geral.FDT(QrETDtaNatal.Value, 3);
end;

procedure TFmOSImp2.QrLocAfterScroll(DataSet: TDataSet);
begin
  ReopenCdi();
  ReopenCav();
  ReopenRes();
  ReopenCui();
  ReopenAti();
  ReopenAtr();
end;

procedure TFmOSImp2.QrLocaisAfterScroll(DataSet: TDataSet);
begin
  ReopenTipos(QrTipos);
end;

procedure TFmOSImp2.QrLocaisBeforeClose(DataSet: TDataSet);
begin
  QrTipos.Close;
end;

procedure TFmOSImp2.QrLocBeforeClose(DataSet: TDataSet);
begin
  QrCdi.Close;
  QrCav.Close;
  QrRes.Close;
  QrCui.Close;
  QrAti.Close;
  QrAtr.Close;
end;

procedure TFmOSImp2.QrOCCalcFields(DataSet: TDataSet);
begin
  QrOCTXTContat.Value := Geral.FDT(QrOCDtaContat.Value, 0, True);
  //
  QrOCTXTVisPrv.Value := Geral.FDT(QrOCDtaVisPrv.Value, 0, True);
  QrOCTXTVisExe.Value := Geral.FDT(QrOCDtaVisExe.Value, 0, True);
  //
  QrOCTXTExePrv.Value := Geral.FDT(QrOCDtaExePrv.Value, 0, True);
  QrOCTXTExeIni.Value := Geral.FDT(QrOCDtaExeIni.Value, 0, True);
  QrOCTXTExeFim.Value := Geral.FDT(QrOCDtaExeFim.Value, 0, True);
end;

procedure TFmOSImp2.QrOpcoesAfterScroll(DataSet: TDataSet);
begin
  ReopenLugares(QrOpcoesOpcao.Value, 0);
end;

procedure TFmOSImp2.QrOpcoesBeforeClose(DataSet: TDataSet);
begin
  QrLugares.Close;
end;

procedure TFmOSImp2.QrOrcSrvAfterScroll(DataSet: TDataSet);
begin
  ReopenOrcAlv();
  ReopenOrcTox();
end;

procedure TFmOSImp2.QrOrcToxCalcFields(DataSet: TDataSet);
begin
  if QrOrcToxNO_MARCA.Value <> '' then
  begin
    QrOrcToxNO_MARCA_E_FAB.Value := QrOrcToxNO_MARCA.Value;
    if QrOrcToxNO_FABRICA.Value <> '' then
      QrOrcToxNO_MARCA_E_FAB.Value :=
      QrOrcToxNO_MARCA_E_FAB.Value + ' [' +
      QrOrcToxNO_FABRICA.Value + ']';
  end else
  if QrOrcToxNO_FABRICA.Value <> '' then
      QrOrcToxNO_MARCA_E_FAB.Value :=
      QrOrcToxNO_FABRICA.Value
  else
    QrOrcToxNO_MARCA_E_FAB.Value := '';
end;

procedure TFmOSImp2.QrOSCabAfterScroll(DataSet: TDataSet);
begin
  ReopenOSAge(QrOSCabCodigo.Value);
  ReopenOSCabAlv(QrOSCabCodigo.Value);
  ReopenOSSrv(QrOSCabCodigo.Value);
  ReopenOSFPS(QrOSCabCodigo.Value);
  ReopenOSCxa(QrOSCabCodigo.Value);
  ReopenOSCxI(QrOSCabCodigo.Value);
  OSApp_PF.ReopenOSSta(QrOSSta, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSPrv(QrOSPrv, QrOSCabCodigo.Value, 0);
  Dmod.ReopenOpcoesFiliPelaFilial(QrOSCabEmpresa.Value);
  //
  case FPageIndex of
    (*1*)FPage_Ficha_Lugar: // Ficha do lugar
    begin
    end;
    (*2*)FPage_Ficha_Vistoria: // Ficha de vistoria
    begin
      ReopenOSPrz(QrOSCabCodigo.Value);
      ReopenOSCabXtr(-999999999);
    end;
    (*3*)FPage_Orcamento: // Or�amento
    begin
    end;
    (*4*)FPage_Ficha_Execucao: // Ficha de execu��o
    begin
      ReopenOSPrz(QrOSCabCodigo.Value);
      ReopenOSCabXtr(QrOSCabCodigo.Value);
    end;
    (*5*) FPage_Monitoramento: // Monitoramento
    begin
      //OSApp_PF.ReopenOSPipMon(QrOSPipMon, QrOSCabCodigo.Value, 0);
      ReopenOSPipML();
    end;
    (*6*)FPage_Comprovante_Execucao: // Certificado de garantia
    begin
      ReopenOSPipML();
    end;
    (*7*)FPage_FAES: //FAES
    begin
      ReopenOSPrz(QrOSCabCodigo.Value);
      ReopenOSCabXtr(QrOSCabCodigo.Value);
    end;
  end;
end;

procedure TFmOSImp2.QrOSCabBeforeClose(DataSet: TDataSet);
begin
  QrOSAge.Close;
  QrOSCabAlv.Close;
  QrOSSrv.Close;
  QrOSPrz.Close;
  QrOSFPS.Close;
  QrOSCxa.Close;
  QrOSCxI.Close;
  QrOSSta.Close;
  QrOSPipML.Close;
end;

procedure TFmOSImp2.QrOSCabCalcFields(DataSet: TDataSet);
begin
  QrOSCabTXTContat.Value := Geral.FDT(QrOSCabDtaContat.Value, 0, True);
  //
  QrOSCabTXTVisPrv.Value := Geral.FDT(QrOSCabDtaVisPrv.Value, 0, True);
  QrOSCabTXTVisExe.Value := Geral.FDT(QrOSCabDtaVisExe.Value, 0, True);
  //
  QrOSCabTXTExePrv.Value := Geral.FDT(QrOSCabDtaExePrv.Value, 0, True);
  QrOSCabTXTExeIni.Value := Geral.FDT(QrOSCabDtaExeIni.Value, 0, True);
  QrOSCabTXTExeFim.Value := Geral.FDT(QrOSCabDtaExeFim.Value, 0, True);
  //
  QrOSCabTXTTel_ENT.Value := Geral.FormataTelefone_TT_Curto(QrOSCabTel_ENT.Value);
  //
  if QrOSCabDtaVisExe.Value > 2 then
    QrOSCabTXTVisExePos.Value := QrOSCabTXTVisExe.Value
  else
    QrOSCabTXTVisExePos.Value := 'N�o realizada';
end;

procedure TFmOSImp2.QrOSCxaAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrChekLstIts, Dmod.MyDB, [
  'SELECT clc.Nome NO_ChekLstCab, cli.* ',
  'FROM cheklstits cli ',
  'LEFT JOIN cheklstcab clc ON clc.Codigo=cli.Codigo ',
  'WHERE cli.Codigo=' + Geral.FF0(QrOSCxaChekLstCab.Value),
  'ORDER BY cli.Ordem, cli.Controle ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCxaAtr, Dmod.MyDB, [
  'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts, ',
  'cad.CodUsu CU_CAD, its.CodUsu CU_ITS, ',
  'cad.Nome NO_CAD, its.Nome NO_ITS ',
  'FROM atrsicxdef def ',
  'LEFT JOIN atrsicxits its ON its.Controle=def.AtrIts ',
  'LEFT JOIN atrsicxcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrOSCxaControle.Value),
  'ORDER BY NO_CAD, NO_ITS ',
  '']);
end;

procedure TFmOSImp2.QrOSCxaBeforeClose(DataSet: TDataSet);
begin
  QrChekLstIts.Close;
  QrOSCxaAtr.Close;
end;

procedure TFmOSImp2.QrOSCxaCalcFields(DataSet: TDataSet);
begin
  QrOSCxaVAL_CALCeINFO.Value := QrOSCxaValCalc.Value + QrOSCxaValInfo.Value;
  QrOSCxaAUTORIZADO_BOOL.Value := Geral.IntToBool(QrOSCxaAutorizado.Value);
  //
  if (QrOSCxaGarantiaDd.Value > 0) and (QrOSCabDtaExeIni.Value > 2) then
  begin
    QrOSCxaDATA_GARANTIA.Value :=
      QrOSCabDtaExeIni.Value + QrOSCxaGarantiaDd.Value;
    QrOSCxaTXT_DATA_GARANTIA.Value :=
      'at� ' + Geral.FDT(QrOSCxaDATA_GARANTIA.Value, 2);
  end else
  begin
    QrOSCxaDATA_GARANTIA.Value := 0;
    //
    if (QrOSCxaGarantiaDd.Value > 0) then
      QrOSCxaTXT_DATA_GARANTIA.Value := 'Data de execu��o n�o informada!'
    else
      QrOSCxaTXT_DATA_GARANTIA.Value := '';
  end;
end;

procedure TFmOSImp2.QrOSFPSCalcFields(DataSet: TDataSet);
var
  ValTotal, ValParcela: Double;
begin
  case FPageIndex of
    (*3*)FPage_Orcamento: ValTotal := QrOSCabOrcamServi.Value;
    (*4*)FPage_Ficha_Execucao: ValTotal := QrOSCabValorServi.Value;
    (*7*)FPage_FAES: ValTotal := QrOSCabValorServi.Value;
    else ValTotal := 0;
  end;
  QrOSFPSVALOR_BRUTO.Value := ValTotal;
  if QrOSFPSParcelas.Value = 0 then
    ValParcela := 0
  else
    ValParcela := Geral.RoundC((QrOSFPSVALOR_BRUTO.Value - (QrOSFPSDescoPer.Value *
    QrOSFPSVALOR_BRUTO.Value / 100)) / QrOSFPSParcelas.Value, 2);
  //
  QrOSFPSVALOR_TOTAL.Value := ValParcela * QrOSFPSParcelas.Value;
  QrOSFPSVALOR_PARCELA.Value := ValParcela;
end;

procedure TFmOSImp2.QrOSFrmCabAfterScroll(DataSet: TDataSet);
begin
  ReopenOSFrmRec();
  ReopenOSFrmAbr();
  ReopenOSFrmDep();
  ReopenDepsB();
end;

procedure TFmOSImp2.QrOSFrmCabBeforeClose(DataSet: TDataSet);
begin
  QrOSFrmRec.Close;
  QrOSFrmAbr.Close;
  QrOSFrmDep.Close;
  QrDepsB.Close;
end;

procedure TFmOSImp2.QrOSFrmCabCalcFields(DataSet: TDataSet);
begin
  if QrOSSrvTUDOFEITO.Value = 1 then
    QrOSFrmCabCALC_FEITO.Value := 100
  else
    QrOSFrmCabCALC_FEITO.Value := QrOSFrmCabPercFeito.Value;

end;

procedure TFmOSImp2.QrOSFrmDepCalcFields(DataSet: TDataSet);
var
  Extra: String;
begin
  if Dmod.QrOpcoesBugsInfoImoMov.Value = 1 then
  begin
    if Dmod.QrOpcoesBugsInfoImoMov.Value = 1 then
      Extra := '(' + QrOSFrmDepSIGLA.Value + ') '
    else Extra := '';
    //
    QrOSFrmDepSIGLA_E_LOC.Value := Extra + QrOSFrmDepn3Loc.Value
  end else
    QrOSFrmDepSIGLA_E_LOC.Value := QrOSFrmDepn3Loc.Value;
end;

procedure TFmOSImp2.QrOSMonCabAfterScroll(DataSet: TDataSet);
begin
  ReopenOSMonRec();
  //Reopen_OS_Mon_Dep_();
end;

procedure TFmOSImp2.QrOSMonCabBeforeClose(DataSet: TDataSet);
begin
  QrOSMonRec.Close;
  //Qr_OS_Mon_Dep_.Close;
end;

{
procedure TFmOSImp.Qr_OS_Mon_Dep_CalcFields(DataSet: TDataSet);
var
  Extra: String;
begin
  if Dmod.QrOpcoesBugsInfoImoMov.Value = 1 then
  begin
    if Dmod.QrOpcoesBugsInfoImoMov.Value = 1 then
      Extra := '(' + Qr_OS_Mon_Dep_SIGLA.Value + ') '
    else Extra := '';
    //
    Qr_OS_Mon_Dep_SIGLA_E_LOC.Value := Extra + Qr_OS_Mon_Dep_n3Loc.Value
  end else
    Qr_OS_Mon_Dep_SIGLA_E_LOC.Value := Qr_OS_Mon_Dep_n3Loc.Value;
end;
}

procedure TFmOSImp2.QrOSPipMLAfterScroll(DataSet: TDataSet);
begin
  case FPageIndex of
    (*5*)FPage_Monitoramento:
    begin
      ReopenOSPipMI;
    end;
    (*6*)FPage_Comprovante_Execucao:
    begin
      ReopenOSRespMI();
      ReopenOSTox6();
    end;
  end;
end;

procedure TFmOSImp2.QrOSPipMLBeforeClose(DataSet: TDataSet);
begin
  QrOSPipMI.Close;
  QrOSRespMI.Close;
end;

procedure TFmOSImp2.QrOSPipMonAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrgLstIts, Dmod.MyDB, [
  'SELECT ELT(pli.Funcoes + 1, ' +
  dmkPF.SQL_DeLista(sListaFuncaoPrgAtrCab, MaxFuncaoPrgAtrCab) +
  ') NO_FUNCAO,',
  'pli.*, plc.Nome NO_LISTA',
  'FROM prglstits pli',
  'LEFT JOIN prglstcab plc ON plc.Codigo=pli.Codigo',
  '']);
end;

procedure TFmOSImp2.QrOSPipMonBeforeClose(DataSet: TDataSet);
begin
  QrPrgLstIts.Close;
end;

procedure TFmOSImp2.QrOSPrzCalcFields(DataSet: TDataSet);
var
  ValTotal, ValParcela: Double;
begin
  case FPageIndex of
    (*3*)FPage_Orcamento: ValTotal := QrOSCabOrcamServi.Value;
    (*4*)FPage_Ficha_Execucao: ValTotal := QrOSCabValorServi.Value;
    else ValTotal := 0;
  end;
  QrOSPrzVALOR_BRUTO.Value := ValTotal;
  if QrOSPrzParcelas.Value = 0 then
    ValParcela := 0
  else
    ValParcela := Geral.RoundC((QrOSPrzVALOR_BRUTO.Value - (QrOSPrzDescoPer.Value *
    QrOSPrzVALOR_BRUTO.Value / 100)) / QrOSPrzParcelas.Value, 2);
  //
  QrOSPrzVALOR_TOTAL.Value := ValParcela * QrOSPrzParcelas.Value;
  QrOSPrzVALOR_PARCELA.Value := ValParcela;
end;

procedure TFmOSImp2.QrOSRespMIAfterScroll(DataSet: TDataSet);
begin
  ReopenOSRespOK();
end;

procedure TFmOSImp2.QrOSRespMIBeforeClose(DataSet: TDataSet);
begin
  QrOSRespOK.Close;
end;

procedure TFmOSImp2.QrOSSrvAfterScroll(DataSet: TDataSet);
begin
  ReopenOSAlv();
  ReopenOSFrmCab();
  ReopenOSMonCab();
  ReopenDesServico();
  //
  case FPageIndex of
    (*2*)FPage_Ficha_Vistoria: ReopenDepsA(); // Dando erro, mesmo sem usar!
    (*3*)FPage_Orcamento:
    begin
      ReopenOSTox3();
      ReopenDepsA();
    end;
    (*4*)FPage_Ficha_Execucao: ReopenDepsA(); // Dando erro, mesmo sem usar!
    (*6*)FPage_Comprovante_Execucao:
    begin
      ReopenOSTox5();
      //ReopenOSTox6();  eh por lista de perguntas (por equipamento)
    end;
  end;
  ReopenLocais();
end;

procedure TFmOSImp2.QrOSSrvBeforeClose(DataSet: TDataSet);
begin
  QrOSAlv.Close;
  QrOSFrmCab.Close;
  QrOSMonCab.Close;
  QrDesServico.Close;
  QrOSTox3.Close;
  QrOSTox5.Close;
end;

procedure TFmOSImp2.QrOSSrvCalcFields(DataSet: TDataSet);
begin
  QrOSSrvVAL_CALCeINFO.Value := QrOSSrvValCalc.Value + QrOSSrvValInfo.Value;
  QrOSSrvAUTORIZADO_BOOL.Value := Geral.IntToBool(QrOSSrvAutorizado.Value);
  //
  if (QrOSSrvGarantiaDd.Value > 0) and (QrOSCabDtaExeIni.Value > 2) then
  begin
    QrOSSrvDATA_GARANTIA.Value     := QrOSCabDtaExeIni.Value + QrOSSrvGarantiaDd.Value;
    QrOSSrvTXT_DATA_GARANTIA.Value := 'at� ' + Geral.FDT(QrOSSrvDATA_GARANTIA.Value, 2);
  end else
  begin
    QrOSSrvDATA_GARANTIA.Value := 0;
    //
    if (QrOSSrvGarantiaDd.Value > 0) then
      QrOSSrvTXT_DATA_GARANTIA.Value := 'Data de execu��o n�o informada!'
    else
      QrOSSrvTXT_DATA_GARANTIA.Value := '';
  end;
  //
  QrOSSrvPERIODO_MONIT.Value := dmkPF.HRDT_PeriodoEIntervalos(
    QrOSSrvMoniDdTotl.Value, QrOSSrvMoniDdIntv.Value, 'Sem Monitoramento');
end;

procedure TFmOSImp2.QrOSTox3CalcFields(DataSet: TDataSet);
begin
  if QrOSTox3NO_MARCA.Value <> '' then
  begin
    QrOSTox3NO_MARCA_E_FAB.Value := QrOSTox3NO_MARCA.Value;
    if QrOSTox3NO_FABRICA.Value <> '' then
      QrOSTox3NO_MARCA_E_FAB.Value :=
      QrOSTox3NO_MARCA_E_FAB.Value + ' [' +
      QrOSTox3NO_FABRICA.Value + ']';
  end else
  if QrOSTox3NO_FABRICA.Value <> '' then
      QrOSTox3NO_MARCA_E_FAB.Value :=
      QrOSTox3NO_FABRICA.Value
  else
    QrOSTox3NO_MARCA_E_FAB.Value := '';
end;

procedure TFmOSImp2.QrOSTox5AfterScroll(DataSet: TDataSet);
begin
  ReopenOSToz5();
end;

procedure TFmOSImp2.QrOSTox5BeforeClose(DataSet: TDataSet);
begin
  QrOSToz.Close;
end;

procedure TFmOSImp2.QrOSTox5CalcFields(DataSet: TDataSet);
begin
  (*
  if QrOSTox5NO_MARCA.Value <> '' then
  begin
    QrOSTox5NO_MARCA_E_FAB.Value := QrOSTox5NO_MARCA.Value;
    if QrOSTox5NO_FABRICA.Value <> '' then
      QrOSTox5NO_MARCA_E_FAB.Value :=
      QrOSTox5NO_MARCA_E_FAB.Value + ' [' +
      QrOSTox5NO_FABRICA.Value + ']';
  end else
  if QrOSTox5NO_FABRICA.Value <> '' then
      QrOSTox5NO_MARCA_E_FAB.Value :=
      QrOSTox5NO_FABRICA.Value
  else
    QrOSTox5NO_MARCA_E_FAB.Value := '';
  *)
end;

procedure TFmOSImp2.QrOSTox6AfterScroll(DataSet: TDataSet);
begin
  ReopenOSToz6();
end;

procedure TFmOSImp2.QrOSTox6BeforeClose(DataSet: TDataSet);
begin
  QrOSToz6.Close;
end;

procedure TFmOSImp2.QrOSTox6CalcFields(DataSet: TDataSet);
begin
  if QrOSTox6NO_MARCA.Value <> '' then
  begin
    QrOSTox6NO_MARCA_E_FAB.Value := QrOSTox6NO_MARCA.Value;
    if QrOSTox6NO_FABRICA.Value <> '' then
      QrOSTox6NO_MARCA_E_FAB.Value :=
      QrOSTox6NO_MARCA_E_FAB.Value + ' [' +
      QrOSTox6NO_FABRICA.Value + ']';
  end else
  if QrOSTox6NO_FABRICA.Value <> '' then
      QrOSTox6NO_MARCA_E_FAB.Value :=
      QrOSTox6NO_FABRICA.Value
  else
    QrOSTox6NO_MARCA_E_FAB.Value := '';
end;

procedure TFmOSImp2.QrSelLugAfterScroll(DataSet: TDataSet);
begin
  ReabreClienteTabs();
  ReopenSTerCad();
  DmModOS.ReopenSTC_Unico(QrSelLugLugar.Value);
  //if FPageIndex > (*1*)FPage_Ficha_Lugar then
  if FImp_OSCab <> '' then
    ReopenOSCab(FImp_OSCab, QrOSCab, DModG.MyPID_DB, QrSelLugOSCab.Value);
  ReopenOSChk(); 
end;

procedure TFmOSImp2.QrSelLugBeforeClose(DataSet: TDataSet);
begin
  QrSTerCad.Close;
  QrLoc.Close;
  QrOSChk.Close;
end;

procedure TFmOSImp2.QrSelSTCAfterScroll(DataSet: TDataSet);
begin
  ReopenSiapTerCad();
end;

procedure TFmOSImp2.QrSelSTCBeforeClose(DataSet: TDataSet);
begin
  QrSiapTerCad.Close;
end;

procedure TFmOSImp2.QrSImaCadAfterScroll(DataSet: TDataSet);
begin
  ReopenSImaDep();
  ReopenSImaCdi();
  ReopenSImaCav();
  ReopenSImaRes();
  ReopenSImaCui();
  ReopenSImaAti();
  ReopenSImaCxa();
  ReopenSICdDef();
end;

procedure TFmOSImp2.QrSImaCadBeforeClose(DataSet: TDataSet);
begin
  QrSImaDep.Close;
  QrSImaCdi.Close;
  QrSImaCav.Close;
  QrSImaRes.Close;
  QrSImaCui.Close;
  QrSImaAti.Close;
  QrSImaCxa.Close;
  QrSICdDef.Close;
end;

procedure TFmOSImp2.QrSImaCadCalcFields(DataSet: TDataSet);
begin
  QrSImaCadTXT_CONSTRU.Value := Geral.FFT_Null(QrSImaCadM2Constru.Value, 2, siPositivo);
  QrSImaCadTXT_NAOBUILD.Value := Geral.FFT_Null(QrSImaCadM2NaoBuild.Value, 2, siPositivo);
  QrSImaCadTXT_TERRENO.Value := Geral.FFT_Null(QrSImaCadM2Terreno.Value, 2, siPositivo);
  QrSImaCadTXT_M2TOTAL.Value := Geral.FFT_Null(QrSImaCadM2Total.Value, 2, siPositivo);
end;

procedure TFmOSImp2.QrSImaCxaAfterScroll(DataSet: TDataSet);
begin
  ReopenSICxDef();
end;

procedure TFmOSImp2.QrSImaCxaBeforeClose(DataSet: TDataSet);
begin
  QrSICxDef.Close;
end;

procedure TFmOSImp2.QrSImaCxaCalcFields(DataSet: TDataSet);
var
  Txt: String;
begin
  Txt := Geral.Substitui(QrSImaCxaAcesso.Value, #10, ' ');
  QrSImaCxaACESSO_EM_UMA_LIN.Value := Geral.Substitui(Txt, #13, '');
  //
  if QrSImaCxaVolumeL.Value = 0 then
    QrSImaCxaTXT_VOL.Value := ''
  else
    QrSImaCxaTXT_VOL.Value := Geral.FFT(QrSImaCxaVolumeL.Value, 0, siPositivo);
end;

procedure TFmOSImp2.QrSImaDepCalcFields(DataSet: TDataSet);
begin
  QrSImaDepTXT_Comp.Value := Geral.FFT_Null(QrSImaDepMComp.Value, 2, siPositivo);
  QrSImaDepTXT_Larg.Value := Geral.FFT_Null(QrSImaDepMLarg.Value, 2, siPositivo);
  QrSImaDepTXT_Altu.Value := Geral.FFT_Null(QrSImaDepMAltu.Value, 2, siPositivo);
end;

procedure TFmOSImp2.QrSImaResCalcFields(DataSet: TDataSet);
var
  Txt: String;
begin
  if QrSImaResAnoNatal.Value = 0 then
    QrSImaResTXT_NATAL.Value := ''
  else
    QrSImaResTXT_NATAL.Value := Geral.FF0(QrSImaResAnoNatal.Value);
  Txt := Geral.Substitui(QrSImaResObservacao.Value, #10, ' ');
  QrSImaResOBS_EM_UMA_LIN.Value := Geral.Substitui(Txt, #13, ' ');
end;

procedure TFmOSImp2.QrSMovCadAfterScroll(DataSet: TDataSet);
begin
  ReopenSMovDef();
end;

procedure TFmOSImp2.QrSMovCadBeforeClose(DataSet: TDataSet);
begin
  QrSMovDef.Close;
end;

procedure TFmOSImp2.QrSMovCadCalcFields(DataSet: TDataSet);
begin
  if QrSMovCadTipo.Value = 0 then
    QrSMovCadTXT_TIPO.Value := ''
  else
    QrSMovCadTXT_TIPO.Value := QrSMovCadNO_TIPO.Value;
end;

procedure TFmOSImp2.QrSTerCadAfterScroll(DataSet: TDataSet);
begin
  ReopenSImaCad();
  ReopenSMovCad();
end;

procedure TFmOSImp2.QrSTerCadBeforeClose(DataSet: TDataSet);
begin
  QrSImaCad.Close;
end;

procedure TFmOSImp2.QrSTerCadCalcFields(DataSet: TDataSet);
begin
  if QrSTerCadCodigo.Value <> 0 then
  begin
    if QrSTerCadSNumero.Value = 0 then
    begin
      if Trim(QrSTerCadSRua.Value) = '' then
        QrSTerCadNUM_TXT.Value := ''
      else
        QrSTerCadNUM_TXT.Value := 'S/N';
    end else
       QrSTerCadNUM_TXT.Value :=
       FloatToStr(QrSTerCadSNumero.Value);
    //
    QrSTerCadCEP_TXT.Value := Geral.FormataCEP_NT(QrSTerCadSCEP.Value);
    QrSTerCadCOD_TXT.Value := Geral.FFN(QrSTerCadLugInfo.Value, 6);
    QrSTerCadTXT_CONSTRU.Value := Geral.FFT_Null(QrSTerCadM2Constru.Value, 2, siPositivo);
    QrSTerCadTXT_NAOBUILD.Value := Geral.FFT_Null(QrSTerCadM2NaoBuild.Value, 2, siPositivo);
    QrSTerCadTXT_TERRENO.Value := Geral.FFT_Null(QrSTerCadM2Terreno.Value, 2, siPositivo);
    QrSTerCadTXT_M2TOTAL.Value := Geral.FFT_Null(QrSTerCadM2Total.Value, 2, siPositivo);
  end else
  begin
    QrSTerCadNUM_TXT.Value := '';
    QrSTerCadCEP_TXT.Value := '';
    QrSTerCadCOD_TXT.Value := '';
    QrSTerCadTXT_CONSTRU.Value := '';
    QrSTerCadTXT_NAOBUILD.Value := '';
    QrSTerCadTXT_TERRENO.Value := '';
    QrSTerCadTXT_M2TOTAL.Value := '';
  end;
end;

procedure TFmOSImp2.QrSumPrzCalcFields(DataSet: TDataSet);
var
  ValTotal, ValParcela: Double;
begin
  case FPageIndex of
    (*3*)FPage_Orcamento: ValTotal := QrSumOSOrcamServi.Value;
    (*4*)FPage_Ficha_Execucao: ValTotal := QrSumOSValorServi.Value;
    else ValTotal := 0;
  end;
  QrSumPrzVALOR_BRUTO.Value := ValTotal;
  if QrSumPrzParcelas.Value = 0 then
    ValParcela := 0
  else
    ValParcela := Geral.RoundC((QrSumPrzVALOR_BRUTO.Value - (QrSumPrzDescoPer.Value *
    QrSumPrzVALOR_BRUTO.Value / 100)) / QrSumPrzParcelas.Value, 2);
  //
  QrSumPrzVALOR_TOTAL.Value := ValParcela * QrSumPrzParcelas.Value;
  QrSumPrzVALOR_PARCELA.Value := ValParcela;
end;

procedure TFmOSImp2.QrTiposAfterScroll(DataSet: TDataSet);
begin
  ReopenDeptos(QrDeptos);
end;

procedure TFmOSImp2.QrTiposBeforeClose(DataSet: TDataSet);
begin
  QrDeptos.Close;
end;

function TFmOSImp2.QtdSICdDef: Integer;
begin
  Result := EdQtdSICdDef.ValueVariant * CO_IMP_COLS_SICdDef;
end;

function TFmOSImp2.QtdSICxDef: Integer;
begin
  Result := EdQtdSICxDef.ValueVariant * CO_IMP_COLS_SICxDef;
end;

function TFmOSImp2.QtdEntiEma: Integer;
begin
  Result := 1 * CO_IMP_ENTI_EMA;
end;

function TFmOSImp2.QtdEntiTel(): Integer;
begin
  Result := 2 * CO_IMP_ENTI_TEL;
end;

function TFmOSImp2.QtdOSAge(): Integer;
begin
  Result := EdQtdOSAge.ValueVariant * CO_IMP_COLS_AGE;
end;

function TFmOSImp2.QtdOSAlv(): Integer;
begin
  Result := EdQtdOSAlv.ValueVariant * CO_IMP_COLS_ALV;
end;

function TFmOSImp2.QtdOSCabAlv(): Integer;
begin
  Result := EdQtdOSCabAlv.ValueVariant * CO_IMP_COLS_CAB_ALV;
end;

function TFmOSImp2.QtdOSCxa: Integer;
begin
  Result := EdQtdOSCxa.ValueVariant * CO_IMP_COLS_CXA;
end;

function TFmOSImp2.QtdOSFrmAbr(): Integer;
begin
  Result := EdQtdOSFrmAbr.ValueVariant * CO_IMP_COLS_FRM_ABR;
end;

function TFmOSImp2.QtdOSFrmCab(): Integer;
begin
  Result := EdQtdOSFrmCab.ValueVariant * CO_IMP_COLS_FRM_CAB;
end;

function TFmOSImp2.QtdOSFrmDep(): Integer;
begin
  Result := EdQtdOSFrmDep.ValueVariant * CO_IMP_COLS_FRM_DEP;
end;

function TFmOSImp2.QtdOSFrmRec(): Integer;
begin
  Result := EdQtdOSFrmRec.ValueVariant * CO_IMP_COLS_FRM_REC;
end;

function TFmOSImp2.QtdOSMonCab(): Integer;
begin
  Result := EdQtdOSMonCab.ValueVariant * CO_IMP_COLS_MON_CAB;
end;

{
function TFmOSImp.Qtd_OS_Mon_Dep_(): Integer;
begin
  Result := EdQtd_OS_Mon_Dep_.ValueVariant * CO_IMP_COLS_MON_DEP;
end;
}

function TFmOSImp2.QtdOSMonRec(): Integer;
begin
  Result := EdQtdOSMonRec.ValueVariant * CO_IMP_COLS_MON_REC;
end;

function TFmOSImp2.QtdOSPrz(): Integer;
begin
  Result := EdQtdOSPrz.ValueVariant * CO_IMP_COLS_PRZ;
end;

function TFmOSImp2.QtdOSSrv(): Integer;
begin
  Result := EdQtdOSSrv.ValueVariant * CO_IMP_COLS_SRV;
end;

function TFmOSImp2.QtdSImaAti: Integer;
begin
  Result := EdQtdSImaAti.ValueVariant * CO_IMP_COLS_SIMAATI;
end;

function TFmOSImp2.QtdSImaCad(): Integer;
begin
  Result := EdQtdSImaCad.ValueVariant * CO_IMP_COLS_SIMACAD;
end;

function TFmOSImp2.QtdSImaCav: Integer;
begin
  Result := EdQtdSImaCav.ValueVariant * CO_IMP_COLS_SIMACAV;
end;

function TFmOSImp2.QtdSImaCdi: Integer;
begin
  Result := EdQtdSImaCdi.ValueVariant * CO_IMP_COLS_SIMACDI;
end;

function TFmOSImp2.QtdSImaCui: Integer;
begin
  Result := EdQtdSImaCui.ValueVariant * CO_IMP_COLS_SIMACUI;
end;

function TFmOSImp2.QtdSImaCxa: Integer;
begin
  Result := EdQtdSImaCxa.ValueVariant * CO_IMP_COLS_SIMACXA;
end;

function TFmOSImp2.QtdSImaDep(): Integer;
begin
  Result := EdQtdSImaDep.ValueVariant * CO_IMP_COLS_SIMADEP;
end;

function TFmOSImp2.QtdSImaRes: Integer;
begin
  Result := EdQtdSImaRes.ValueVariant * CO_IMP_COLS_SIMARES;
end;

function TFmOSImp2.QtdSMovCad(): Integer;
begin
  Result := EdQtdSMovCad.ValueVariant * CO_IMP_COLS_SMOVCAD;
end;

function TFmOSImp2.QtdSMovDef(): Integer;
begin
  Result := EdQtdSMovDef.ValueVariant * CO_IMP_COLS_SMOVDEF;
end;

procedure TFmOSImp2.ReabreClienteTabs();
begin
  Info('Abrindo tabelas auxiliares');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
  'SELECT DISTINCT sic.Codigo, sic.SCompl2',
  'FROM ' + TMeuDB + '.osfrmdep osd',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo',
  'WHERE osd.Codigo=' + Geral.FF0(QrSelLugOSCab.Value),
  'AND osd.Tabela=' + Geral.FF0(CO_TABELA_TIPO_LUGAR_001_SIAPIMADEP),
  '',
{
  'UNION',
  '',
  'SELECT DISTINCT sic.Codigo, sic.SCompl2',
  'FROM ' + TMeuDB + '._OS_Mon_Dep_ osd',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo',
  'WHERE osd.Codigo=' + Geral.FF0(QrSelLugOSCab.Value),
  'AND osd.Tabela=' + Geral.FF0(CO_TABELA_TIPO_LUGAR_001_SIAPIMADEP),
}
  '']);
  //
end;

procedure TFmOSImp2.RecriaDadosGarantias();
var
  TIPO, NO_Diluente, NO_EQUIP, NO_TIPOAPLICA, RegMinSaud, FoneCIT, FoneCEATOX,
  Toxicidade, Concentrac, AcaoToxica, Antidoto, NO_GG1, SIGLA_USO, SIGLA_TOT,
  NO_MARCA, NO_FABRICA, Observacao, Tabela, Referencia: String;
  ApMo, Codigo, Controle, Conta, IDIts, Diluente, EhDiluente, MyOrd,
  GraGru1, GraGruX: Integer;
  QtdTot, UsoQtd: Double;
  //
  PRINCIPIO_ATIVO, GRUPO_QUIMICO, NumLote, NumLaudo, FormaApresen: String;
  //
  OSCab, PrgLstCab, GGX_Dilu: Integer;
begin
  Info('Gerando infoma��es dos produtos');
  //
  if QrSelSTC.RecordCount > 0 then
  begin
    PB1.Position := 0;
    PB1.Max      := QrSelSTC.RecordCount;
    //
    QrSelSTC.First;
    while not QrSelSTC.Eof do
    begin
      OSCab := QrSelSTCOSCab.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrItens, Dmod.MyDB, [
        'SELECT 1 ApMo, rec.Codigo, rec.Controle, ',
        'rec.Conta, rec.IDIts, "?" NO_Diluente, ',
        'rec.Numlote, rec.NumLaudo, ',
        'ggx.GraGru1, ggx.Controle GraGruX, ',
        '0 PrgLstCab, g1b.Nome NO_EQUIP, apl.Nome NO_TIPOAPLICA, ',
        'ofc.QtdTot, Diluente, rec.UsoQtd UsoQtd, EhDiluente, ',
        'IF(pmo.Nivel1 IS NULL, fap.Nome, fao.Nome) FormaApresen,',
        'IF(pmo.Nivel1 IS NULL, pap.FoneCIT, pmo.FoneCIT) FoneCIT,',
        'IF(pmo.Nivel1 IS NULL, pap.FoneCEATOX, pmo.FoneCEATOX) FoneCEATOX, ',
        'IF(pmo.Nivel1 IS NULL, pap.Toxicidade, pmo.Toxicidade) Toxicidade,',
        'IF(pmo.Nivel1 IS NULL, pap.Concentrac, pmo.Concentrac) Concentrac, ',
        'IF(pmo.Nivel1 IS NULL, pap.AcaoToxica, pmo.AcaoToxica) AcaoToxica,',
        'IF(pmo.Nivel1 IS NULL, pap.Antidoto, pmo.Antidoto) Antidoto,',
        'gg1.Nome NO_GG1, me1.Sigla SIGLA_USO, ',
        'IF(pmo.Nivel1 IS NULL, "AP", "MO") TIPO, ',
        'IF(pmo.Nivel1 IS NULL, pap.RegMinSaud, pmo.RegMinSaud) RegMinSaud, ',
        'IF(pmo.Nivel1 IS NULL, pap.Observacao, ',
        'pmo.Observacao) Observacao, ',
        'mar.Nome NO_MARCA, fab.Nome NO_FABRICA, ',
        'me2.Sigla SIGLA_TOT, gg1.Referencia ',
        'FROM osfrmrec rec ',
        'LEFT JOIN osfrmcab  ofc ON ofc.Conta=rec.Conta ',
        'LEFT JOIN gragrux   ggx ON ggx.Controle=rec.GraGruX ',
        'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN grag1prap pap ON pap.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN frmapres frp ON frp.Codigo=pap.FrmApres ',
        'LEFT JOIN grag1prmo pmo ON pmo.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN grafabmar mar ON mar.Controle= ',
        '    IF(pmo.Nivel1 IS NULL, pap.Marca, pmo.Marca) ',
        'LEFT JOIN grafabcad fab ON fab.Codigo=mar.Codigo ',
        'LEFT JOIN tipoaplica apl ON apl.Codigo=ofc.TipoAplica ',
        'LEFT JOIN unidmed   me1 ON me1.Codigo=gg1.UnidMed ',
        'LEFT JOIN unidmed   me2 ON me2.Codigo=ofc.UnidMed ',
        'LEFT JOIN gragrux   gxb ON gxb.Controle=ofc.EquipAplic ',
        'LEFT JOIN gragru1   g1b ON g1b.Nivel1=gxb.GraGru1 ',
        'LEFT JOIN grag1eqap eap ON eap.Nivel1=gxb.GraGru1 ',
        'LEFT JOIN grag1prmo emo ON emo.Nivel1=gxb.GraGru1 ',
        'LEFT JOIN frmapres fap ON fap.Codigo=pap.FrmApres ',
        'LEFT JOIN frmapres fao ON fao.Codigo=pmo.FrmApres ',
        'WHERE rec.Codigo=' + Geral.FF0(OSCab),
        ' ',
        'UNION ',
        ' ',
        'SELECT 2 ApMo, rec.Codigo, rec.Controle, ',
        'rec.Conta, rec.IDIts, "?" NO_Diluente, ',
        'rec.Numlote, rec.NumLaudo, ',
        'ggx.GraGru1, ggx.Controle GraGruX, ',
        '0 PrgLstCab, g1b.Nome NO_EQUIP, apl.Nome NO_TIPOAPLICA, ',
        'ofc.QtdTot, Diluente, rec.UsoQtd UsoQtd, EhDiluente, ',
        'IF(pmo.Nivel1 IS NULL, fap.Nome, fao.Nome) FormaApresen,',
        'IF(pmo.Nivel1 IS NULL, pap.FoneCIT, pmo.FoneCIT) FoneCIT,',
        'IF(pmo.Nivel1 IS NULL, pap.FoneCEATOX, pmo.FoneCEATOX) FoneCEATOX, ',
        'IF(pmo.Nivel1 IS NULL, pap.Toxicidade, pmo.Toxicidade) Toxicidade,',
        'IF(pmo.Nivel1 IS NULL, pap.Concentrac, pmo.Concentrac) Concentrac, ',
        'IF(pmo.Nivel1 IS NULL, pap.AcaoToxica, pmo.AcaoToxica) AcaoToxica,',
        'IF(pmo.Nivel1 IS NULL, pap.Antidoto, pmo.Antidoto) Antidoto,',
        'gg1.Nome NO_GG1, me1.Sigla SIGLA_USO, "MO" TIPO,',
        'IF(pmo.Nivel1 IS NULL, pap.RegMinSaud, pmo.RegMinSaud) RegMinSaud,',
        'IF(pmo.Nivel1 IS NULL, pap.Observacao, pmo.Observacao) Observacao,',
        'mar.Nome NO_MARCA, fab.Nome NO_FABRICA, ',
        'me2.Sigla SIGLA_TOT, gg1.Referencia ',
        'FROM osmonrec rec ',
        'LEFT JOIN osmoncab  ofc ON ofc.Conta=rec.Conta ',
        'LEFT JOIN gragrux   ggx ON ggx.Controle=rec.GraGruX ',
        'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN grag1prap pap ON pap.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN frmapres fap ON fap.Codigo=pap.FrmApres ',
        'LEFT JOIN grag1prmo pmo ON pmo.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN frmapres fao ON fao.Codigo=pmo.FrmApres ',
        'LEFT JOIN grafabmar mar ON mar.Controle= ',
        '    IF(pmo.Nivel1 IS NULL, pap.Marca, pmo.Marca) ',
        'LEFT JOIN grafabcad fab ON fab.Codigo=mar.Codigo ',
        'LEFT JOIN tipoaplica apl ON apl.Codigo=ofc.TipoAplica ',
        'LEFT JOIN unidmed   me1 ON me1.Codigo=gg1.UnidMed ',
        'LEFT JOIN unidmed   me2 ON me2.Codigo=ofc.UnidMed ',
        'LEFT JOIN gragrux   gxb ON gxb.Controle=ofc.EquipAplic ',
        'LEFT JOIN gragru1   g1b ON g1b.Nivel1=gxb.GraGru1 ',
        'LEFT JOIN grag1eqap eap ON eap.Nivel1=gxb.GraGru1 ',
        'LEFT JOIN grag1prmo emo ON emo.Nivel1=gxb.GraGru1 ',
        'WHERE rec.Codigo=' + Geral.FF0(OSCab),
        ' ',
        ' ',
        'UNION ',
        ' ',
        'SELECT 3 ApMo, rec.Codigo, rec.Controle, ',
        'rec.Conta, rec.IDIts, "' + CO_SEM_DILUENTE + '" NO_Diluente, ',
        'rec.Numlote, rec.NumLaudo, ',
        'ggx.GraGru1, ggx.Controle GraGruX, ',
        'opm.PrgLstCab, g1b.Nome NO_EQUIP,  apl.Nome NO_TIPOAPLICA, ',
        '0 QtdTot, ofc.Diluente, rec.UsoQtd UsoQtd, EhDiluente, ',
        'IF(pmo.Nivel1 IS NULL, fap.Nome, fao.Nome) FormaApresen,',
        'IF(pmo.Nivel1 IS NULL, pap.FoneCIT, pmo.FoneCIT) FoneCIT,',
        'IF(pmo.Nivel1 IS NULL, pap.FoneCEATOX, pmo.FoneCEATOX) FoneCEATOX, ',
        'IF(pmo.Nivel1 IS NULL, pap.Toxicidade, pmo.Toxicidade) Toxicidade,',
        'IF(pmo.Nivel1 IS NULL, pap.Concentrac, pmo.Concentrac) Concentrac, ',
        'IF(pmo.Nivel1 IS NULL, pap.AcaoToxica, pmo.AcaoToxica) AcaoToxica,',
        'IF(pmo.Nivel1 IS NULL, pap.Antidoto, pmo.Antidoto) Antidoto,',
        'gg1.Nome NO_GG1, me1.Sigla SIGLA_USO, "MO" TIPO,',
        'IF(pmo.Nivel1 IS NULL, pap.RegMinSaud, pmo.RegMinSaud) RegMinSaud,',
        'IF(pmo.Nivel1 IS NULL, pap.Observacao, pmo.Observacao) Observacao,',
        'mar.Nome NO_MARCA, fab.Nome NO_FABRICA, ',
        '"" SIGLA_TOT, gg1.Referencia ',
        'FROM ospipitspr rec ',
        'LEFT JOIN ospipmon  opm ON opm.Controle=rec.Controle ',
        'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad ',
        'LEFT JOIN osmoncab  ofc ON ofc.Conta=pip.OSMonCab ',
        'LEFT JOIN gragrux   ggx ON ggx.Controle=rec.GraGruX ',
        'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN grag1prap pap ON pap.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN frmapres fap ON fap.Codigo=pap.FrmApres ',
        'LEFT JOIN grag1prmo pmo ON pmo.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN frmapres fao ON fao.Codigo=pmo.FrmApres ',
        'LEFT JOIN tipoaplica apl ON apl.Codigo=ofc.TipoAplica ',
        'LEFT JOIN grafabmar mar ON mar.Controle= ',
        '    IF(pmo.Nivel1 IS NULL, pap.Marca, pmo.Marca) ',
        'LEFT JOIN grafabcad fab ON fab.Codigo=mar.Codigo ',
        'LEFT JOIN unidmed   me1 ON me1.Codigo=gg1.UnidMed ',
        'LEFT JOIN unidmed   me2 ON me2.Codigo=ofc.UnidMed ',
        'LEFT JOIN gragrux   gxb ON gxb.Controle=ofc.EquipAplic  ',
        'LEFT JOIN gragru1   g1b ON g1b.Nivel1=gxb.GraGru1  ',
        'LEFT JOIN grag1eqap eap ON eap.Nivel1=gxb.GraGru1  ',
        'LEFT JOIN grag1prmo emo ON emo.Nivel1=gxb.GraGru1  ',
        'WHERE rec.Codigo=' + Geral.FF0(OSCab),
        '']);
      //
      MyOrd := 0;
      if QrItens.RecordCount > 0 then
      begin
        PB2.Position := 0;
        PB2.Max      := QrItens.RecordCount;
        //
        QrItens.First;
        while not QrItens.Eof do
        begin
          ApMo     := QrItensApMo.Value;
          Codigo   := QrItensCodigo.Value;
          Controle := QrItensControle.Value;
          Conta    := QrItensConta.Value;
          Diluente := QrItensDiluente.Value;
      {
          UnDmkDac_PF.AbreMySQLQuery0(QrLoc1, DModG.MyPID_DB, [
          'SELECT GraGru1, Gragrux ',
          'FROM _imp_ostox ',
          'WHERE EhDiluente=1 ',
          'AND ApMo=' + Geral.FF0(ApMo),
          'AND Codigo=' + Geral.FF0(Codigo),
          'AND Controle=' + Geral.FF0(Controle),
          'AND Conta=' + Geral.FF0(Conta),
          '']);
      }
          if ApMo < 3 then
          begin
            if Diluente = CO_COD_BUGS_DILUENTE_003 then
            begin
              if ApMo = 1 then
                Tabela := 'osfrmrec'
              else
                Tabela := 'osmonrec';
              UnDmkDac_PF.AbreMySQLQuery0(QrLoc1, Dmod.MyDB, [
                'SELECT GraGruX ',
                'FROM ' + Tabela,
                'WHERE EhDiluente=1 ',
                'AND Codigo=' + Geral.FF0(Codigo),
                'AND Controle=' + Geral.FF0(Controle),
                'AND Conta=' + Geral.FF0(Conta),
                '']);
              GGX_Dilu := QrLoc1GraGruX.Value;
            end else
              GGX_Dilu := 0;
            NO_Diluente  := DmModOS.NomeDiluente_GGX(Diluente, GGX_Dilu, True);
          end else
            NO_Diluente  := CO_SEM_DILUENTE;
          //
          IDIts          := QrItensIDIts.Value;
          TIPO           := QrItensTIPO.Value;
          Diluente       := QrItensDiluente.Value;
          GraGru1        := QrItensGraGru1.Value;
          GraGruX        := QrItensGraGruX.Value;
          NO_EQUIP       := QrItensNO_EQUIP.Value;
          NO_TIPOAPLICA  := QrItensNO_TIPOAPLICA.Value;
          QtdTot         := QrItensQtdTot.Value;
          SIGLA_TOT      := QrItensSIGLA_TOT.Value;
          UsoQtd         := QrItensUsoQtd.Value;
          EhDiluente     := QrItensEhDiluente.Value;
          RegMinSaud     := QrItensRegMinSaud.Value;
          FoneCIT        := QrItensFoneCIT.Value;
          if FoneCIT <> '' then
            FoneCIT        := Geral.FormataTelefone_TT_Curto(FoneCIT);
          FoneCEATOX     := QrItensFoneCEATOX.Value;
          if FoneCEATOX <> '' then
          FoneCEATOX     := Geral.FormataTelefone_TT_Curto(FoneCEATOX);
          Toxicidade     := QrItensToxicidade.Value;
          Concentrac     := QrItensConcentrac.Value;
          AcaoToxica     := QrItensAcaoToxica.Value;
          Antidoto       := QrItensAntidoto.Value;
          NO_GG1         := QrItensNO_GG1.Value;
          Referencia     := QrItensReferencia.Value;
          SIGLA_USO      := QrItensSIGLA_USO.Value;
          NO_MARCA       := QrItensNO_MARCA.Value;
          NO_FABRICA     := QrItensNO_FABRICA.Value;
          Observacao     := QrItensObservacao.Value;
          MyOrd          := MyOrd + 1;
          NumLote        := QrItensNumLote.Value;
          NumLaudo       := QrItensNumLaudo.Value;
          PrgLstCab      := QrItensPrgLstCab.Value;
          FormaApresen   := QrItensFormaApresen.Value;
          //
          if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, '_imp_ostox', False, [
          'ApMo', 'Codigo', 'Controle',
          'Conta', 'IDIts', 'TIPO',
          'NO_Diluente', 'NO_EQUIP', 'NO_TIPOAPLICA',
          'QtdTot', 'SIGLA_TOT', 'Diluente', 'UsoQtd',
          'EhDiluente', 'RegMinSaud', 'FoneCIT',
          'FoneCEATOX', 'Toxicidade', 'Concentrac',
          'AcaoToxica', 'Antidoto', 'NO_GG1',
          'SIGLA_USO', 'NO_MARCA', 'NO_FABRICA',
          'Observacao', 'GraGru1', 'graGruX',
          'NumLote', 'NumLaudo', 'Referencia',
          'PrgLstCab', 'FormaApresen', 'MyOrd'], [
          ], [
          ApMo, Codigo, Controle,
          Conta, IDIts, TIPO,
          NO_Diluente, NO_EQUIP, NO_TIPOAPLICA,
          QtdTot, SIGLA_TOT, Diluente, UsoQtd,
          EhDiluente, RegMinSaud, FoneCIT,
          FoneCEATOX, Toxicidade, Concentrac,
          AcaoToxica, Antidoto, NO_GG1,
          SIGLA_USO, NO_MARCA, NO_FABRICA,
          Observacao, GraGru1, graGruX,
          NumLote, NumLaudo, Referencia,
          PrgLstCab, FormaApresen, MyOrd], [
          ], False) then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrGraG1PrPA, Dmod.MyDB, [
              'SELECT pai.Nome PRINCIPIO_ATIVO, pag.Nome GRUPO_QUIMICO ',
              'FROM grag1prpa ppa ',
              'LEFT JOIN g1priatii pai ON pai.Codigo=ppa.G1PriAtiI ',
              'LEFT JOIN g1priatig pag ON pag.Codigo=pai.NivSup ',
              'WHERE ppa.Nivel1=' + Geral.FF0(GraGru1),
              '']);
            QrGraG1PrPA.First;
            while not QrGraG1PrPA.Eof do
            begin
              PRINCIPIO_ATIVO := QrGraG1PrPAPRINCIPIO_ATIVO.Value;
              GRUPO_QUIMICO   := QrGraG1PrPAGRUPO_QUIMICO.Value;
              //
              UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, '_imp_ostoz', False, [
                'ApMo', 'Codigo', 'Controle',
                'Conta', 'IDIts',
                'PRINCIPIO_ATIVO', 'GRUPO_QUIMICO',
                'MyOrd'], [
                ], [
                ApMo, Codigo, Controle,
                Conta, IDIts,
                PRINCIPIO_ATIVO, GRUPO_QUIMICO,
                MyOrd], [
                ], False);
              //
              QrGraG1PrPA.Next;
            end;
          end;
          PB2.Position := PB2.Position + 1;
          PB2.Update;
          Application.ProcessMessages;
          //
          QrItens.Next;
        end;
      end;
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      QrSelSTC.Next;
    end;
  end;
  PB1.Position := 0;
  PB2.Position := 0;
end;

procedure TFmOSImp2.RecriaTabsTempOS();
begin
  FImp_OSCab :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSCab, DmodG.QrUpdPID1, False);
  FImp_OSAge :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSAge, DmodG.QrUpdPID1, False);
  FImp_OSCabAlv :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSCabAlv, DmodG.QrUpdPID1, False);
  FImp_OSSrv :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSSrv, DmodG.QrUpdPID1, False);
  FImp_OSAlv :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSAlv, DmodG.QrUpdPID1, False);
  FImp_OSFrmCab :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSFrmCab, DmodG.QrUpdPID1, False);
  FImp_OSFrmRec :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSFrmRec, DmodG.QrUpdPID1, False);
  FImp_OSFrmAbr :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSFrmAbr, DmodG.QrUpdPID1, False);
  FImp_OSFrmDep :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSFrmDep, DmodG.QrUpdPID1, False);
  FImp_OSMonCab :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSMonCab, DmodG.QrUpdPID1, False);
  FImp_OSMonRec :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSMonRec, DmodG.QrUpdPID1, False);
{
  FImp__OS_Mon_Dep_ :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp__OS_Mon_Dep_, DmodG.QrUpdPID1, False);
}
  FImp_OSPrz :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSPrz, DmodG.QrUpdPID1, False);
  FImp_OSCxa :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSCxa, DmodG.QrUpdPID1, False);
  //
  //  GARANTIA
  FImp_OSTox :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSTox, DmodG.QrUpdPID1, False);
  FImp_OSToz :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSToz, DmodG.QrUpdPID1, False);
  //
  if FPageIndex in ([5, 6]) then
  begin
    FImp_OSPipML :=
      UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSPipML, DmodG.QrUpdPID1, False);
    FImp_OSPipMI :=
      UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSPipMI, DmodG.QrUpdPID1, False);
  end;
end;

procedure TFmOSImp2.RecriaTabsTempTerreno();

  function CriaTmpTable(Tabela: TNomeTabRecriaTempTableBugs): String;
  begin
    Result := UnCreateBugs.RecriaTempTableNovo(Tabela, DmodG.QrUpdPID1, False);
    //
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
  end;

const
  CO_QtdTabs = 15;
begin
  PB1.Position := 0;
  PB1.Max      := CO_QtdTabs;
  //
  //FImp_OSCab :=   UnCreateBugs.RecriaTempTableNovo(ntrtt_Imp_OSCab,   DmodG.QrUpdPID1, False);
  FImp_EntiEma := CriaTmpTable(ntrtt_Imp_EntiEma);
  FImp_EntiTel := CriaTmpTable(ntrtt_Imp_EntiTel);
  FImp_SMovCad := CriaTmpTable(ntrtt_Imp_SMovCad);
  FImp_SMovDef := CriaTmpTable(ntrtt_Imp_SMovDef);
  FImp_STerCad := CriaTmpTable(ntrtt_Imp_STerCad);
  FImp_SImaCad := CriaTmpTable(ntrtt_Imp_SImaCad);
  FImp_SImaDep := CriaTmpTable(ntrtt_Imp_SImaDep);
  FImp_SImaCdi := CriaTmpTable(ntrtt_Imp_SImaCdi);
  FImp_SImaCav := CriaTmpTable(ntrtt_Imp_SImaCav);
  FImp_SImaRes := CriaTmpTable(ntrtt_Imp_SImaRes);
  FImp_SImaCui := CriaTmpTable(ntrtt_Imp_SImaCui);
  FImp_SImaAti := CriaTmpTable(ntrtt_Imp_SImaAti);
  FImp_SImaCxa := CriaTmpTable(ntrtt_Imp_SImaCxa);
  FImp_SICxDef := CriaTmpTable(ntrtt_Imp_SICxDef);
  FImp_SICdDef := CriaTmpTable(ntrtt_Imp_SICdDef);
  //
  PB1.Position := 0;
end;

procedure TFmOSImp2.ReopenAnalise(Tabela, Grupo1, Campo: String; Colunas: Integer);
var
  Col: String;
begin
  case FPageIndex of
    (*1*)FPage_Ficha_Lugar          ,
    //
    (*2*)FPage_Ficha_Vistoria       ,
    (*4*)FPage_Ficha_Execucao       ,
    (*5*)FPage_Monitoramento        ,
    (*7*)FPage_FAES                 :
    begin
      Col := Geral.FF0(Colunas);
      UnDmkDAC_PF.AbreMySQLQuery0(QrAnalise, DModG.MyPID_DB, [
        'SELECT ' + Grupo1 + ' + 0.000 N1, ' +
        Campo + ' + 0.000 ID, MOD(COUNT(' + Campo + '), ' +
        Col + ') + 0.000  ATU, ' + Col + ' + 0.000 MAX ',
        'FROM ' + Tabela,
        'GROUP BY N1, ID',
        '']);
    end;
    (*3*)FPage_Orcamento            ,
    (*6*)FPage_Comprovante_Execucao :
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAnalise, DModG.MyPID_DB, [
        'SELECT 0.000 N1, 0.000 ID, 0.000 ATU, 0.000 MAX ',
        '']);
    end;
    else Geral.MB_Erro('"ReopenAnalise" n�o definido para impress�o selecionada');
  end;
end;

procedure TFmOSImp2.ReopenAti();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAti, Dmod.MyDB, [
    'SELECT ati.Nome NO_ATIVIDADE ',
    'FROM siapimaati sia ',
    'LEFT JOIN atividades ati ON ',
    '  ati.Codigo=sia.Atividade ',
    'WHERE sia.Codigo=' + Geral.FF0(QrLocCodigo.Value),
    '']);
end;

procedure TFmOSImp2.ReopenAtr();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtr, Dmod.MyDB, [
    'SELECT cad.Nome NO_CAD, its.Nome NO_ITS ',
    'FROM atrsicddef def ',
    'LEFT JOIN atrsicdits its ON its.Controle=def.AtrIts ',
    'LEFT JOIN atrsicdcad cad ON cad.Codigo=def.AtrCad ',
    'WHERE def.ID_Sorc=' + Geral.FF0(QrLocCodigo.Value),
    'ORDER BY NO_CAD, NO_ITS ',
    '']);
end;

procedure TFmOSImp2.ReopenCav();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCav, Dmod.MyDB, [
    'SELECT car.Nome NO_CARACTERIS, ',
    'cav.Controle, cav.Caracteris ',
    'FROM siapimacav cav ',
    'LEFT JOIN Caracteris car ON car.Codigo=cav.Caracteris ',
    'WHERE cav.Codigo=' + Geral.FF0(QrLocCodigo.Value),
    'ORDER BY NO_CARACTERIS ',
    '']);
end;

procedure TFmOSImp2.ReopenCdi();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCdi, Dmod.MyDB, [
    'SELECT car.Nome NO_CARACTERIS, ',
    'cdi.Controle, cdi.Caracteris ',
    'FROM siapimacdi cdi ',
    'LEFT JOIN Caracteris car ON car.Codigo=cdi.Caracteris ',
    'WHERE cdi.Codigo=' + Geral.FF0(QrLocCodigo.Value),
    'ORDER BY NO_CARACTERIS ',
    '']);
end;

procedure TFmOSImp2.ReopenCui();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCui, Dmod.MyDB, [
    'SELECT cui.Nome NO_CUIDADO ',
    'FROM siapimacui sic ',
    'LEFT JOIN cuidados cui ON ',
    '  cui.Codigo=sic.Cuidado ',
    'WHERE sic.Codigo=' + Geral.FF0(QrLocCodigo.Value),
    '']);
end;

procedure TFmOSImp2.ReopenCunsCad();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCunsCad, Dmod.MyDB, [
    'SELECT ati.Nome NO_AtivPrinc, how.Nome NO_HowFind, ',
    'IF(acc.Tipo=0, RazaoSocial, acc.Nome) NO_Account, cun.* ',
    'FROM cunscad cun ',
    'LEFT JOIN atividades ati ON ati.Codigo=cun.AtivPrinc ',
    'LEFT JOIN howfounded how ON how.Codigo=cun.HowFind ',
    'LEFT JOIN entidades acc ON acc.Codigo=cun.Account ',
    'WHERE cun.Codigo=' + Geral.FF0(EdCliCod.ValueVariant),
    '']);
end;

procedure TFmOSImp2.ReopenDepsA();
var
  CtrlTxt: String;
{
 TEMP_TAB: String;
  Codigo, Controle, Conta, IDIts, Tabela, Cadastro, SeqLug, SeqLoc, SeqTyp,
  SeqDep, TotLug, TotLoc, TotTyp, TotDep, UltLug, UltLoc, UltTyp, UltDep: Integer;
}
begin
  FRegSID_AlturaLoc := 0;
  FRegSID_AlturaTyp := 0;
  //
  CtrlTxt := Geral.FF0(QrOSSrvControle.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDepsA, DModG.MyPID_DB, [
    'SELECT DISTINCT  ',
    'IF(Tabela=1, sic.Codigo, 0) + 0.000 c3Loc, ',
    'IF(Tabela=1, sic.SCompl2, " ") n3Loc, ',
    'IF(Tabela=1, det.Codigo, mac.Tipo) + 0.000 c2Typ, ',
    'IF(Tabela=1, det.Nome,  ',
    '     ELT(mac.Tipo, "M�vel", "Autom�vel", ',
    '      "Animal", "Outro", "???")) n2Typ, ',
    'ELT(osd.Tabela, "I", "M", "?") SIGLA,  ',
    'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") n1Dep ',
    'FROM _imp_osfrmdep osd  ',
    'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro  ',
    'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo  ',
    'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci  ',
    'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType  ',
    'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro  ',
    'WHERE osd.Controle=' + CtrlTxt,
    ' ',
(*
  'UNION  ',
  '  ',
  'SELECT DISTINCT  ',
  'IF(Tabela=1, sic.Codigo, 0) + 0.000 c3Loc, ',
  'IF(Tabela=1, sic.SCompl2, " ") n3Loc,  ',
  'IF(Tabela=1, det.Codigo, mac.Tipo) + 0.000 c2Typ, ',
  'IF(Tabela=1, det.Nome,  ',
  '     ELT(mac.Tipo, "M�vel", "Autom�vel", ',
  '      "Animal", "Outro", "???")) n2Typ, ',
  'ELT(osd.Tabela, "I", "M", "?") SIGLA,  ',
  'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") n1Dep ',
  'FROM _imp__OS_Mon_Dep_ osd  ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro  ',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo  ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci  ',
  'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType  ',
  'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro  ',
  'WHERE osd.Controle=' + CtrlTxt,
  '  ',
*)
    'ORDER BY n3Loc, n2Typ, SIGLA, n1Dep ',
    ' ',
    '; ',
    '']);
  //
(*
  UltLug := 0;
  UltLoc := 0;
  UltTyp := 0;
  UltDep := 0;
  //
  QrDepsA.First;
  while not QrDepsA.Eof do
  begin
// locbugst__1
    Codigo      := QrDepsACodigo.Value;
    Controle    := QrDepsAControle.Value;
    Conta       := QrDepsAConta.Value;
    IDIts       := QrDepsAIDIts.Value;
    Tabela      := QrDepsATabela.Value;
    Cadastro    := QrDepsACadastro.Value;
    SeqLug      := 1;
    if QrDepsASiapImaDep.Value = UltLoc then
    begin
      SeqLoc    := SeqLoc + 1;
    end else begin
      SeqLoc    := 1;
    end;
    if QrDepsADependType.Value = UltTyp then
    begin
      SeqTyp    := SeqTyp + 1;
    end else begin
      SeqTyp    := 1;
    end;
    SeqDep      := SeqDep + 1;
    //QrDepsA.
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, '_imp_osfrmdep', False, [
    'SeqLug', 'SeqLoc', 'SeqTyp',
    'SeqDep'], [
    'Codigo', 'Controle', 'Conta',
    'IDIts', 'Tabela', 'Cadastro'
    ], [
    SeqLug, SeqLoc, SeqTyp,
    SeqDep], [
    Codigo, Controle, Conta,
    IDIts, Tabela, Cadastro
    ], True);
    //
    UltLoc := QrDepsASiapImaDep.Value;
    UltTyp := QrDepsADependType.Value;
    //
    QrDepsA.Next;
  end;
  //

  //
*)
end;

procedure TFmOSImp2.ReopenDepsB();
var
  CtaTxt: String;
{
  CtaTxt, TEMP_TAB: String;
  Codigo, Controle, Conta, IDIts, Tabela, Cadastro, SeqLug, SeqLoc, SeqTyp,
  SeqDep, TotLug, TotLoc, TotTyp, TotDep, UltLug, UltLoc, UltTyp, UltDep: Integer;
}
begin
  FRegSID_AlturaLoc := 0;
  FRegSID_AlturaTyp := 0;
  //
  CtaTxt := Geral.FF0(QrOSFrmCabConta.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDepsB, DModG.MyPID_DB, [
    'SELECT DISTINCT  ',
    'IF(Tabela=1, sic.Codigo, 0) + 0.000 c3Loc, ',
    'IF(Tabela=1, sic.SCompl2, " ") n3Loc, ',
    'IF(Tabela=1, det.Codigo, mac.Tipo) + 0.000 c2Typ, ',
    'IF(Tabela=1, det.Nome,  ',
    '     ELT(mac.Tipo, "M�vel", "Autom�vel", ',
    '      "Animal", "Outro", "???")) n2Typ, ',
    'ELT(osd.Tabela, "I", "M", "?") SIGLA,  ',
    'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") n1Dep ',
    'FROM _imp_osfrmdep osd  ',
    'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro  ',
    'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo  ',
    'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci  ',
    'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType  ',
    'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro  ',
    'WHERE osd.Conta=' + CtaTxt,
    ' ',
(*
  'UNION  ',
  '  ',
  'SELECT DISTINCT  ',
  'IF(Tabela=1, sic.Codigo, 0) + 0.000 c3Loc, ',
  'IF(Tabela=1, sic.SCompl2, " ") n3Loc,  ',
  'IF(Tabela=1, det.Codigo, mac.Tipo) + 0.000 c2Typ, ',
  'IF(Tabela=1, det.Nome,  ',
  '     ELT(mac.Tipo, "M�vel", "Autom�vel", ',
  '      "Animal", "Outro", "???")) n2Typ, ',
  'ELT(osd.Tabela, "I", "M", "?") SIGLA,  ',
  'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") n1Dep ',
  'FROM _imp__OS_Mon_Dep_ osd  ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro  ',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo  ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci  ',
  'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType  ',
  'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro  ',
  'WHERE osd.Conta=' + CtaTxt,
  '  ',
*)
    'ORDER BY n3Loc, n2Typ, SIGLA, n1Dep ',
    ' ',
    '; ',
    '']);
end;

procedure TFmOSImp2.ReopenDeptos(Qry: TmySQLQuery; ByOS: Boolean = False);
var
  SQLCompl, CtrlTxt, LocTxt, TypTxt: String;
begin
  if not ByOS then
  begin
    CtrlTxt  := Geral.FF0(QrOSSrvControle.Value);
    SQLCompl := 'WHERE osd.Controle=' + CtrlTxt;
  end else
  begin
    CtrlTxt := Geral.FF0(QrSelLugOSCab.Value);
    SQLCompl := 'WHERE osd.Codigo=' + CtrlTxt;
  end;
  LocTxt := Geral.FFI(QrLocaisc3Loc.Value);
  TypTxt := Geral.FFI(QrTiposc2Typ.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'SELECT DISTINCT  ',
    'ELT(osd.Tabela, "I", "M", "?") SIGLA,  ',
    'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") n1Dep ',
    'FROM _imp_osfrmdep osd  ',
    'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro  ',
    'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo  ',
    'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci  ',
    'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType  ',
    'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro  ',
    SQLCompl,
    'AND IF(Tabela=1, sic.Codigo, 0)=' + LocTxt,
    'AND IF(Tabela=1, det.Codigo, mac.Tipo)=' + TypTxt,
    ' ',
(*
  'UNION  ',
  '  ',
  'SELECT DISTINCT  ',
  'ELT(osd.Tabela, "I", "M", "?") SIGLA,  ',
  'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") n1Dep ',
  'FROM _imp__OS_Mon_Dep_ osd  ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro  ',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo  ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci  ',
  'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType  ',
  'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro  ',
  'WHERE osd.Controle=' + CtrlTxt,
  'AND IF(Tabela=1, sic.Codigo, 0)=' + LocTxt,
  'AND IF(Tabela=1, det.Codigo, mac.Tipo)=' + TypTxt,
  '  ',
*)
    'ORDER BY SIGLA, n1Dep ',
    ' ',
    '']);
end;

procedure TFmOSImp2.ReopenDesServico();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDesServico, Dmod.MyDB, [
    'SELECT * ',
    'FROM desservico ',
    'WHERE Codigo=' + Geral.FF0(QrOSSrvDesServico.Value),
    '']);
end;

procedure TFmOSImp2.ReopenDiarioAdd(Grupo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDiarioAdd, Dmod.MyDB, [
    'SELECT Codigo, Data, Hora, Nome',
    'FROM diarioadd ',
    'WHERE PrintOS=1',
    'AND Depto IN ( ',
    '     SELECT Codigo ',
    '     FROM oscab  ',
    '     WHERE Grupo=' + Geral.FF0(Grupo) + ') ',
    'ORDER BY Data, Hora',
    '']);
end;

procedure TFmOSImp2.ReopenEntiEma();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiEma, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM _imp_entiema ',
    'ORDER BY MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenEntiTel();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiTel, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM _imp_entitel ',
    'ORDER BY MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenFAESLocais();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFAESLocais, DMod.MyDB, [
    'SELECT DISTINCT ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") n3Loc, ',
    '(sid.MLarg * sid.MComp) M2Total ',
    'FROM osfrmdep osd ',
    'LEFT JOIN siapimadep sid ON sid.Controle=osd.Cadastro ',
    'LEFT JOIN dependenci dpd ON dpd.Codigo=sid.Dependenci ',
    'LEFT JOIN movamovcad mac ON mac.Codigo=osd.Cadastro ',
    'WHERE osd.Codigo=' + Geral.FF0(QrSelLugOSCab.Value),
    '']);
end;

procedure TFmOSImp2.ReopenLocais();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocais, DModG.MyPID_DB, [
    'SELECT DISTINCT TABELA, ',
    'IF(Tabela=1, sic.Codigo, 0) + 0.000 c3Loc, ',
    'IF(Tabela=1, sic.SCompl2, "M�veis e im�veis") n3Loc, ',
    'sic.M2Constru, sic.M2NaoBuild, sic.M2Terreno, sic.M2Total ',
    'FROM _imp_osfrmdep osd  ',
    'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro  ',
    'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo  ',
    'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci  ',
    'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType  ',
    'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro  ',
    'WHERE osd.Controle=' + Geral.FF0(QrOSSrvControle.Value),
    ' ',
(*
  'UNION  ',
  '  ',
  'SELECT DISTINCT TABELA, ',
  'IF(Tabela=1, sic.Codigo, 0) + 0.000 c3Loc, ',
  'IF(Tabela=1, sic.SCompl2, "M�veis e im�veis") n3Loc ',
  'FROM _imp__OS_Mon_Dep_ osd  ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro  ',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo  ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci  ',
  'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType  ',
  'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro  ',
  'WHERE osd.Controle=' + CtrlTxt,
*)
    '  ',
    'ORDER BY TABELA DESC, n3Loc ',
    ' ',
    '']);
end;

procedure TFmOSImp2.ReopenLugares(Opcao, Lugar: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLugares, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + F_os_imp_,
    'WHERE Opcao=' + Geral.FF0(Opcao),
    // FIM 2013-08-12
    //'ORDER BY NO_Lugar ',
    'ORDER BY OSCab ',
    // FIM 2013-08-12
    '']);
  //
  QrLugares.Locate('Lugar', Lugar, []);
end;

procedure TFmOSImp2.ReopenOpcoes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOpcoes, DModG.MyPID_DB, [
    'SELECT DISTINCT Opcao ',
    'FROM ' + F_os_imp_,
    'ORDER BY Opcao ',
    '']);
end;

procedure TFmOSImp2.ReopenOrcAlv();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrcAlv, DModG.MyPID_DB, [
    'SELECT DISTINCT prc.Nome NO_Praga_Z, prc.NivSup, prc.Especie, ',
    'prc.Observacao, prc.ArqDir, ',
    'prc.Copyright, prc.ArqImg ',
    'FROM _imp_ossrv oss ',
    'LEFT JOIN _imp_osalv osa ON osa.Controle=oss.Controle ',
    'LEFT JOIN ' + TMeuDB + '.praga_z prc ON prc.Codigo=osa.Praga_Z ',
    'WHERE oss.DesServico=' + Geral.FF0(QrOrcSrvCodigo.Value),
    '']);
end;

procedure TFmOSImp2.ReopenOrcSrv();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrcSrv, DModG.MyPID_DB, [
    'SELECT DISTINCT des.* ',
    'FROM _imp_ossrv srv ',
    'LEFT JOIN ' + TMeuDB + '.desservico des ON des.Codigo=srv.DesServico ',
    '']);
end;

procedure TFmOSImp2.ReopenOrcTox();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrcTox, DModG.MyPID_DB, [
(*
  'SELECT DISTINCT ot.TIPO, ot.NO_Diluente, ot.GraGru1, ot.GraGruX, ',
  'ot.NO_EQUIP, ot.NO_TIPOAPLICA, ot.QtdTot, ot.SIGLA_TOT, ',
  'ot.Diluente, ot.RegMinSaud, ot.FoneCIT, ot.FoneCEATOX, ',
  'ot.Toxicidade, ot.Concentrac, ot.AcaoToxica, ot.Antidoto, ',
  'ot.NO_GG1, ot.Referencia, ot.SIGLA_USO, ot.NO_MARCA, ',
  'ot.NO_FABRICA, ot.Observacao, ot.NumLaudo, ot.NumLote ',
  'FROM _imp_ostox ot ',
  'LEFT JOIN _imp_ossrv oss ON oss.Controle=ot.Controle ',
  'WHERE oss.DesServico=' + Geral.FF0(QrOrcSrvCodigo.Value),
*)
    'SELECT DISTINCT ot.graGru1, ot.GraGruX, ot.NO_GG1, ',
    'ot.NO_MARCA, ot.NO_FABRICA, ot.RegMinSaud ',
    'FROM _imp_ostox ot ',
    'LEFT JOIN _imp_ossrv oss ON oss.Controle=ot.Controle ',
    'WHERE oss.DesServico=' + Geral.FF0(QrOrcSrvCodigo.Value),
    '']);
end;

procedure TFmOSImp2.ReopenOSAge(OSCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSAge, DModG.MyPID_DB, [
    'SELECT og.*, IF(og.Responsa = 1, "R", " ") NO_RESPONSA, ',
    'IF(og.Agente <> 0, ent.Nome, "") NO_Agente ',
    'FROM _imp_osage og ',
    'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=og.Agente ',
    'WHERE og.Codigo=' + Geral.FF0(OSCab),
    'ORDER BY ent.Nome ',
    '']);
end;

procedure TFmOSImp2.ReopenOSAlv();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSAlv, DModG.MyPID_DB, [
    'SELECT prc.Nome NO_Praga_Z, prc.NivSup, prc.Especie, ',
    'prc.Observacao, prc.Prevencao, prc.ArqDir, ',
    'prc.Copyright, prc.ArqImg, osa.* ',
    'FROM _imp_osalv osa ',
    'LEFT JOIN ' + TMeuDB + '.praga_z prc ON prc.Codigo=osa.Praga_Z ',
    'WHERE osa.Codigo=' + Geral.FF0(QrOSSrvCodigo.Value),
    'AND osa.Controle=' + Geral.FF0(QrOSSrvControle.Value),
    'ORDER BY MyOrd',
    '']);
end;

procedure TFmOSImp2.ReopenOSCab(Tabela: String; Qry: TmySQLQuery;
DB: TmySQLDatabase; Codigo: Integer);
begin
//  if FLastOpenOSCab = Codigo then
  //  Exit;
  //
  if UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
    'SELECT cab.*, ',
    'fge.Nome NO_FatoGeradr, ',
    'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
    'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT, ',
    'eco.Nome NO_ENTICONTAT, ',
    'tg1.Nome NO_ExeTxtCli1, tg1.Texto TXT_ExeTxtCli1, ',
    'tg2.Nome NO_ExeTxtCli2, tg2.Texto TXT_ExeTxtCli2, ',
    'IF(enc.Tipo=0, enc.RazaoSocial, enc.Nome) NO_CONTRATANTE, ',
    'IF(enp.Tipo=0, enp.RazaoSocial, enp.Nome) NO_PAGANTE ',
    'FROM ' + Tabela + ' cab ',
    'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=cab.Entidade ',
    'LEFT JOIN ' + TMeuDB + '.fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
    'LEFT JOIN ' + TMeuDB + '.estatusoss sta ON sta.Codigo=cab.Estatus ',
    'LEFT JOIN ' + TMeuDB + '.siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
    'LEFT JOIN ' + TMeuDB + '.enticontat eco ON eco.Controle=cab.EntiContat ',
    'LEFT JOIN ' + TMeuDB + '.txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1 ',
    'LEFT JOIN ' + TMeuDB + '.txtgeneric tg2 ON tg2.Codigo=cab.ExeTxtCli2 ',
    'LEFT JOIN ' + TMeuDB + '.entidades enc ON enc.Codigo=cab.EntContrat ',
    'LEFT JOIN ' + TMeuDB + '.entidades enp ON enp.Codigo=cab.EntPagante ',
    'WHERE cab.Codigo=' + Geral.FF0(Codigo),
    //'ORDER BY MyOrd', // d� erro na tabela "oscab"
    // 2013-08-12
    'ORDER BY Codigo ',
    // FIM 2013-08-12
    ''])
  then
    FLastOpenOScab := Codigo;
end;

procedure TFmOSImp2.ReopenOSCabAlv(OSCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCabAlv, DModG.MyPID_DB, [
    'SELECT oca.*, IF(oca.Praga_Z <> 0, "E", "G") NO_NIVEL, ',
    'IF(oca.Praga_Z <> 0, pcz.Nome, ',
    'IF(oca.Praga_A <> -1, pca.Nome, "")) NO_PRAGA ',
    'FROM _imp_oscabalv oca ',
    'LEFT JOIN ' + TMeuDB + '.praga_a pca ON pca.Codigo=oca.Praga_A ',
    'LEFT JOIN ' + TMeuDB + '.praga_z pcz ON pcz.Codigo=oca.Praga_Z ',
    'WHERE oca.Codigo=' + Geral.FF0(OSCab),
    'ORDER BY oca.Ordem, MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenOSCabXtr(OSCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCabXtr, Dmod.MyDB, [
    'SELECT * ',
    'FROM oscabxtr ',
    'WHERE Codigo=' + Geral.FF0(OSCab),
    'ORDER BY DtHrIni ',
    '']);
end;

procedure TFmOSImp2.ReopenOSChk();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSChk, Dmod.MyDB, [
    'SELECT chk.*, lsi.Nome NO_LstIts, ',
    'ELT(chk.Feito+1, "   ", "SIM", "???") NO_FEITO ',
    'FROM oschk chk ',
    'LEFT JOIN cheklstits lsi ON lsi.Controle=chk.ChekLstIts ',
    'WHERE chk.Codigo=' + Geral.FF0(QrSelLugOSCab.Value),
    '']);
end;

procedure TFmOSImp2.ReopenOSCxa(OSCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCxa, DModG.MyPID_DB, [
    'SELECT cxa.*, cxm.Nome NO_MATERIAL, ',
    'cxf.Nome NOME_FORMA, ',
    'six.MatersCxa, six.FormasCxa, six.VolumeL, ',
    'six.Local, six.Acesso, six.Medidas, ',
    'clc.TituloImp TIT_ChekLstCab ',
    'FROM _imp_oscxa cxa ',
    'LEFT JOIN ' + TMeuDB + '.siapimacxa six ON six.Controle=cxa.Caixa ',
    'LEFT JOIN ' + TMeuDB + '.cxamaters cxm ON ',
    '  cxm.Codigo=six.MatersCxa ',
    'LEFT JOIN ' + TMeuDB + '.cxaformas cxf ON ',
    '  cxf.Codigo=six.FormasCxa ',
    'LEFT JOIN ' + TMeuDB + '.cheklstcab clc ON ',
    '  clc.Codigo=cxa.ChekLstCab ',
    'WHERE cxa.Codigo=' + Geral.FF0(OSCab),
    'ORDER BY MyOrd',
    '']);
end;

procedure TFmOSImp2.ReopenOSCxI(OSCab: Integer);
const
  Imprimir = '1';
var
  SQLx: String;
begin
  case FPageIndex of
    (*3*)FPage_Orcamento            : SQLx := 'AND Aplicacao & 1';
    (*6*)FPage_Comprovante_Execucao : SQLx := 'AND Aplicacao & 2';
    else SQLx := '';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCxI, DMod.MyDB, [
    'SELECT cxi.*  ',
    'FROM oscxi cxi ',
    'WHERE cxi.Codigo=' + Geral.FF0(OSCab),
    'AND Imprimir=' + Imprimir,
    SQLx,
    '']);
end;

procedure TFmOSImp2.ReopenOSFrmCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSFrmCab, DModG.MyPID_DB, [
    'SELECT ofc.*, frm.Nome NO_FORMULA, gg1.Nome NO_EquipAplic ',
    'FROM _imp_osfrmcab ofc ',
    'LEFT JOIN ' + TMeuDB + '.formulas frm ON frm.Codigo=ofc.Formula ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=ofc.EquipAplic ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON ggx.Gragru1=gg1.Nivel1 ',
    'WHERE ofc.Codigo=' + Geral.FF0(QrOSSrvCodigo.Value),
    'AND ofc.Controle=' + Geral.FF0(QrOSSrvControle.Value),
    'ORDER BY MyOrd',
    '']);
end;

procedure TFmOSImp2.ReopenOSFrmDep();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSFrmDep, DModG.MyPID_DB, [
(*
  'SELECT osd.*, ELT(osd.Tabela, "I", "M", "?") SIGLA, ',
  'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") NO_Campo ',
  'FROM _imp_osfrmdep osd ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci ',
  'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro ',
  //'WHERE osd.Controle=' + Geral.FF0(QrOSSrvControle.Value),
  'WHERE osd.Conta=' + Geral.FF0(QrOSFrmCabConta.Value),
  'ORDER BY MyOrd',
*)
(*
  'SELECT osd.*, ELT(osd.Tabela, "I", "M", "?") SIGLA,',
  'sic.SCompl2 NO_LOCAL, det.Nome NO_TIPO,',
  'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") NO_Campo',
  'FROM _imp_osfrmdep osd',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro',
  'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci',
  'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo',
  'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType',
  'WHERE osd.Conta=' + Geral.FF0(QrOSFrmCabConta.Value),
  'ORDER BY MyOrd',
*)
    'SELECT  osd.Ativo, ',
    'IF(Tabela=1, sic.Codigo, 0) + 0.000 c3Loc,  ',
    'IF(Tabela=1, sic.SCompl2, " ") n3Loc,   ',
    'IF(Tabela=1, det.Codigo, mac.Tipo) + 0.000 c2Typ,  ',
    'IF(Tabela=1, det.Nome,   ',
    '     ELT(mac.Tipo, "M�vel", "Autom�vel",   ',
    '     "Animal", "Outro", "???")) n2Typ,  ',
    'ELT(osd.Tabela, "I", "M", "?") SIGLA,   ',
    'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") n1Dep ',
    'FROM _imp_osfrmdep osd ',
    'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro ',
    'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci ',
    'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro ',
    'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType ',
    'WHERE osd.Codigo=' + Geral.FF0(QrOSSrvCodigo.Value),
    'AND osd.Controle=' + Geral.FF0(QrOSSrvControle.Value),
    'AND osd.Conta=' + Geral.FF0(QrOSFrmCabConta.Value),
    'ORDER BY MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenOSFrmRec();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSFrmRec, DModG.MyPID_DB, [
    'SELECT med.SIGLA, gg1.Nome NO_GG1, ',

    'IF(pap.RegMinSaud <> "", pap.RegMinSaud, ',
    'pmo.RegMinSaud) RegMinSaud, ',
    'CONCAT(IF(gm1.Nome<>"", gm1.Nome, gm2.Nome), " [", ',
    'IF(gm1.Nome<>"", gc1.Nome, gc2.Nome), "]") NO_MARCA_FABR, ',

    'ofr.* ',
    'FROM _imp_osfrmrec ofr ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx on ggx.Controle=ofr.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 on gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed med on med.Codigo=gg1.UnidMed ',


    'LEFT JOIN ' + TMeuDB + '.grag1prap pap ON pap.Nivel1=gg1.Nivel1 ',
    'LEFT JOIN ' + TMeuDB + '.grafabmar gm1 ON gm1.Controle=pap.Marca ',
    'LEFT JOIN ' + TMeuDB + '.grafabcad gc1 ON gm1.Codigo=gc1.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.grag1prmo pmo ON pmo.Nivel1=gg1.Nivel1 ',
    'LEFT JOIN ' + TMeuDB + '.grafabmar gm2 ON gm2.Controle=pmo.Marca ',
    'LEFT JOIN ' + TMeuDB + '.grafabcad gc2 ON gm2.Codigo=gc2.Codigo ',

    'WHERE ofr.Codigo=' + Geral.FF0(QrOSFrmCabCodigo.Value),
    'AND ofr.Controle=' + Geral.FF0(QrOSFrmCabControle.Value),
    'AND ofr.Conta=' + Geral.FF0(QrOSFrmCabConta.Value),
    'ORDER BY Ordem, MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenOSFPS(OSCab: Integer);
begin
// FPS Forma de Pagamento selecionada!
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSFPS, DModG.MyPID_DB, [
    'SELECT opz.Codigo, opz.Controle, opz.Condicao, opz.Parcelas, ',
    'opz.DescoPer, opz.Escolhido, opz.Ativo, ppc.Nome NO_CONDICAO ',
    'FROM _imp_osprz opz ',
    'LEFT JOIN ' + TMeuDB + '.pediprzcab ppc ON ppc.Codigo=opz.Condicao ',
    'WHERE opz.Codigo=' + Geral.FF0(OSCab),
    'AND opz.Escolhido=1',
    '']);
end;

procedure TFmOSImp2.ReopenOSFrmAbr();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSFrmAbr, DModG.MyPID_DB, [
    'SELECT abr.Nome NO_ABRANGE, ofa.* ',
    'FROM _imp_osfrmabr ofa ',
    'LEFT JOIN ' + TMeuDB + '.abrangicie abr ON abr.Codigo=ofa.Abrangicie ',
    'WHERE ofa.Codigo=' + Geral.FF0(QrOSFrmCabCodigo.Value),
    'AND ofa.Controle=' + Geral.FF0(QrOSFrmCabControle.Value),
    'AND ofa.Conta=' + Geral.FF0(QrOSFrmCabConta.Value),
    //'ORDER BY Ordem, MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenOSMonCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSMonCab, DModG.MyPID_DB, [
    'SELECT ofc.*, mon.Nome NO_FORMULA, gg1.Nome NO_EquipAplic ',
    'FROM _imp_osmoncab ofc ',
    'LEFT JOIN ' + TMeuDB + '.formulas mon ON mon.Codigo=ofc.Formula ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=ofc.EquipAplic ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON ggx.Gragru1=gg1.Nivel1 ',
    'WHERE ofc.Codigo=' + Geral.FF0(QrOSSrvCodigo.Value),
    'AND ofc.Controle=' + Geral.FF0(QrOSSrvControle.Value),
    'ORDER BY MyOrd',
    '']);
end;

(*
procedure TFmOSImp.Reopen_OS_Mon_Dep_();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr_OS_Mon_Dep_, DModG.MyPID_DB, [
  'SELECT  osd.Ativo, ',
  'IF(Tabela=1, sic.Codigo, 0) + 0.000 c3Loc,  ',
  'IF(Tabela=1, sic.SCompl2, " ") n3Loc,   ',
  'IF(Tabela=1, det.Codigo, mac.Tipo) + 0.000 c2Typ,  ',
  'IF(Tabela=1, det.Nome,   ',
  '     ELT(mac.Tipo, "M�vel", "Autom�vel",   ',
  '     "Animal", "Outro", "???")) n2Typ,  ',
  'ELT(osd.Tabela, "I", "M", "?") SIGLA,   ',
  'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") n1Dep ',
  'FROM _imp__OS_Mon_Dep_ osd',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro',
  'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci',
  'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo',
  'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType',
  'WHERE osd.Codigo=' + Geral.FF0(QrOSSrvCodigo.Value),
  'AND osd.Controle=' + Geral.FF0(QrOSSrvControle.Value),
  'AND osd.Conta=' + Geral.FF0(QrOSMonCabConta.Value),
  'ORDER BY MyOrd',
  //
  '']);
end;
*)

procedure TFmOSImp2.ReopenOSMonRec();
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSMonRec, DModG.MyPID_DB, [
  'SELECT med.SIGLA, gg1.Nome NO_GG1, ofr.* ',
  'FROM _imp_osmonrec ofr ',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx on ggx.Controle=ofr.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 on gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed med on med.Codigo=gg1.UnidMed ',
  'WHERE ofr.Codigo=' + Geral.FF0(QrOSSrvCodigo.Value),
  'AND ofr.Controle=' + Geral.FF0(QrOSSrvControle.Value),
  'AND ofr.Conta=' + Geral.FF0(QrOSMonCabConta.Value),
  'ORDER BY Ordem, MyOrd ',
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSMonRec, DModG.MyPID_DB, [
    'SELECT med.SIGLA, gg1.Nome NO_GG1, ',

    'IF(pmo.RegMinSaud <> "", pmo.RegMinSaud, ',
    'pap.RegMinSaud) RegMinSaud, ',
    'CONCAT(IF(gm2.Nome<>"", gm2.Nome, gm1.Nome), " [", ',
    'IF(gm2.Nome<>"", gc2.Nome, gc1.Nome), "]") NO_MARCA_FABR, ',

    'ofr.* ',
    'FROM _imp_osmonrec ofr ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx on ggx.Controle=ofr.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 on gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed med on med.Codigo=gg1.UnidMed ',


    'LEFT JOIN ' + TMeuDB + '.grag1prap pap ON pap.Nivel1=gg1.Nivel1 ',
    'LEFT JOIN ' + TMeuDB + '.grafabmar gm1 ON gm1.Controle=pap.Marca ',
    'LEFT JOIN ' + TMeuDB + '.grafabcad gc1 ON gm1.Codigo=gc1.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.grag1prmo pmo ON pmo.Nivel1=gg1.Nivel1 ',
    'LEFT JOIN ' + TMeuDB + '.grafabmar gm2 ON gm2.Controle=pmo.Marca ',
    'LEFT JOIN ' + TMeuDB + '.grafabcad gc2 ON gm2.Codigo=gc2.Codigo ',

    'WHERE ofr.Codigo=' + Geral.FF0(QrOSMonCabCodigo.Value),
    'AND ofr.Controle=' + Geral.FF0(QrOSMonCabControle.Value),
    'AND ofr.Conta=' + Geral.FF0(QrOSMonCabConta.Value),
    'ORDER BY Ordem, MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenOSPrz(OSCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPrz, DModG.MyPID_DB, [
    'SELECT opz.Codigo, opz.Controle, opz.Condicao, opz.Parcelas, ',
    'opz.DescoPer, opz.Escolhido, opz.Ativo, ppc.Nome NO_CONDICAO ',
    'FROM _imp_osprz opz ',
    'LEFT JOIN ' + TMeuDB + '.pediprzcab ppc ON ppc.Codigo=opz.Condicao ',
    'WHERE opz.Codigo=' + Geral.FF0(OSCab),
    'ORDER BY MyOrd, opz.DescoPer DESC',
    '']);
end;

procedure TFmOSImp2.ReopenOSRespMI();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSRespMI, DModG.MyPID_DB, [
    'SELECT DISTINCT Controle, NO_PIP, ',
    'NO_LOCAL, NO_DEPENDENCI ',
    'FROM _imp_ospipmi ',
    'WHERE Codigo=' + Geral.FF0(QrOSCabCodigo.Value),
    'AND PrgLstCab=' + Geral.FF0(QrOSPipMLCodigo.Value),
    'AND Resposta <> "" ',
    'ORDER BY NO_PIP',
    '']);
end;

procedure TFmOSImp2.ReopenOSRespOK();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSRespOK, DModG.MyPID_DB, [
    'SELECT * FROM _imp_ospipmi ',
    'WHERE Codigo=' + Geral.FF0(QrOSCabCodigo.Value),
    'AND PrgLstCab=' + Geral.FF0(QrOSPipMLCodigo.Value),
    'AND Controle=' + Geral.FF0(QrOSRespMIControle.Value),
    'AND Resposta <> "" ',
    'ORDER BY Controle, ',
    'SuperOrd, SuperSub, SobreOrd, Ordem, SubOrdem ',
    '']);
end;

procedure TFmOSImp2.ReopenOSSrv(OSCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSSrv, DModG.MyPID_DB, [
    'SELECT des.Nome NO_DesServico, ',
    'des.Sigla NO_SIGLA, IF(srv.TudoFeitoA=1, 1,',
    'IF(srv.TudoFeitoM=1, 1, 0)) + 0.000 TUDOFEITO, srv.* ',
    'FROM _imp_ossrv srv ',
    'LEFT JOIN ' + TMeuDB + '.desservico des ON des.Codigo=srv.DesServico ',
    'WHERE srv.Codigo=' + Geral.FF0(OSCab),
    'ORDER BY MyOrd',
    '']);
end;

procedure TFmOSImp2.ReopenOSTox3();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSTox3, DModG.MyPID_DB, [
    'SELECT DISTINCT NO_GG1, NO_MARCA, NO_FABRICA, RegMinSaud',
    'FROM _imp_ostox ot ',
    'WHERE ot.Codigo=' + Geral.FF0(QrOSSrvCodigo.Value),
    'AND ot.Controle=' + Geral.FF0(QrOSSrvControle.Value),
    'AND ot.EhDiluente=0 ',
    'ORDER BY NO_FABRICA, NO_MARCA, NO_GG1 ',
    '']);
end;

procedure TFmOSImp2.ReopenFAESOSTox5();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFAESOSTox5, DModG.MyPID_DB, [
    'SELECT pag.Nome GRUPO_QUIMICO, ot.* ',
    'FROM _imp_ostox ot ',
    'LEFT JOIN ' + TMeuDB + '.grag1prpa ppa ON ppa.Nivel1=ot.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.g1priatii pai ON pai.Codigo=ppa.G1PriAtiI ',
    'LEFT JOIN ' + TMeuDB + '.g1priatig pag ON pag.Codigo=pai.NivSup ',
    'WHERE ot.Codigo=' + Geral.FF0(QrOSSrvCodigo.Value),
    'AND ot.EhDiluente=0 ',
    'AND ot.ApMo<>3 ',
    'ORDER BY Conta, IDIts ',
    '']);
end;

procedure TFmOSImp2.ReopenOSTox5();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSTox5, DModG.MyPID_DB, [
    'SELECT ot.* ',
    'FROM _imp_ostox ot ',
    'WHERE ot.Codigo=' + Geral.FF0(QrOSSrvCodigo.Value),
    'AND ot.Controle=' + Geral.FF0(QrOSSrvControle.Value),
    'AND ot.EhDiluente=0 ',
    'AND ot.ApMo<>3 ',
    'ORDER BY Conta, IDIts ',
    '']);
end;

procedure TFmOSImp2.ReopenOSTox6();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSTox6, DModG.MyPID_DB, [
    'SELECT ot.ApMo, ot.Codigo, ot.Controle, ot.Conta, ot.IDIts, ',
    'ot.NO_GG1, ot.NO_MArca, ot.NO_FABRICA, ot.RegMinSaud, ',
    'SUM(ot.UsoQtd) UsoQtd, ot.SIGLA_USO, ot.Concentrac,  ',
    'SUM(ot.UsoQtd) QtdTot, ot.SIGLA_USO Sigla_TOT,  ',
    'ot.NO_TIPOAPLICA, ot.NO_DILUENTE, ot.NO_EQUIP, ot.FoneCEATOX,  ',
    'ot.FoneCIT, ot.Toxicidade, ot.AcaoToxica, ot.Antidoto, ',
    'ot.GraGruX, ot.NumLote, ot.NumLaudo, ot.Referencia ',
    'FROM _imp_ostox ot  ',
    'WHERE ot.Codigo=' + Geral.FF0(QrOSSrvCodigo.Value),
    'AND PrgLstCab=' + Geral.FF0(QrOSPipMLCodigo.Value),
    'AND ot.EhDiluente=0  ',
    'AND ot.ApMo=3  ',
    'GROUP BY ot.GraGruX, ot.NumLote, ot.NumLaudo ',
    'ORDER BY Conta, IDIts  ',
    '']);
end;

procedure TFmOSImp2.ReopenOSToz5();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSToz, DModG.MyPID_DB, [
    'SELECT oz.* ',
    'FROM _imp_ostoz oz ',
    'WHERE oz.ApMo=' + Geral.FF0(QrOSTox5ApMo.Value),
    'AND oz.Codigo=' + Geral.FF0(QrOSTox5Codigo.Value),
    'AND oz.Controle=' + Geral.FF0(QrOSTox5Controle.Value),
    'AND oz.Conta=' + Geral.FF0(QrOSTox5Conta.Value),
    'AND oz.IDIts=' + Geral.FF0(QrOSTox5IDIts.Value),
    'ORDER BY PRINCIPIO_ATIVO ',
    '']);
end;

procedure TFmOSImp2.ReopenOSToz6();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSToz6, DModG.MyPID_DB, [
    'SELECT oz.* ',
    'FROM _imp_ostoz oz ',
    'WHERE oz.ApMo=' + Geral.FF0(QrOSTox6ApMo.Value),
    'AND oz.Codigo=' + Geral.FF0(QrOSTox6Codigo.Value),
    'AND oz.Controle=' + Geral.FF0(QrOSTox6Controle.Value),
    'AND oz.Conta=' + Geral.FF0(QrOSTox6Conta.Value),
    'AND oz.IDIts=' + Geral.FF0(QrOSTox6IDIts.Value),
    'ORDER BY PRINCIPIO_ATIVO ',
    '']);
end;

procedure TFmOSImp2.ReopenRes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRes, Dmod.MyDB, [
    'SELECT res.Nome, IF(res.AnoNatal=0, " ", ',
    'CONCAT(YEAR(SYSDATE()) - res.AnoNatal, "")) IDADE, ',
    'cui.Nome NO_CUI, res.Observacao ',
    'FROM siapimares res ',
    'LEFT JOIN cuidados cui ON cui.Codigo=res.Cuidado ',
    'WHERE res.Codigo=' + Geral.FF0(QrLocCodigo.Value),
    '']);
end;

procedure TFmOSImp2.ReopenSelLug(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + F_os_imp_,
    'WHERE Ativo=1 ',
    'AND Opcao=' + Geral.FF0(QrOpcoesOpcao.Value),
    //'ORDER BY NO_Lugar ',
    // 2013-08-12
    //'ORDER BY NO_Lugar ',
    'ORDER BY OSCab ',
    // FIM 2013-08-12
    '']);
end;

procedure TFmOSImp2.ReopenSiapTerCad();
begin
  QrSiapTerCad.Close;
  if QrSelSTCLugar.Value <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSiapTerCad, Dmod.MyDB, [
      'SELECT llo.Nome NOMELOGRAD, stc.* ',
      'FROM siaptercad stc ',
      'LEFT JOIN listalograd llo ON llo.Codigo=stc.SLograd ',
      'WHERE stc.Codigo=' + Geral.FF0(QrSelSTCLugar.Value),
      '']);
  end;
end;

procedure TFmOSImp2.ReopenSICdDef;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSICdDef, DModG.MyPID_DB, [
    'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts, ',
    'cad.CodUsu CU_CAD, its.CodUsu CU_ITS, ',
    'cad.Nome NO_CAD, its.Nome NO_ITS ',
    'FROM _imp_sicddef def ',
    'LEFT JOIN ' + TMeuDB + '.atrsicdits its ON its.Controle=def.AtrIts ',
    'LEFT JOIN ' + TMeuDB + '.atrsicdcad cad ON cad.Codigo=def.AtrCad ',
    'WHERE def.ID_Sorc=' + Geral.FF0(QrSImaCadCodigo.Value),
    'ORDER BY NO_CAD, NO_ITS ',
    '']);
end;

procedure TFmOSImp2.ReopenSICxDef();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSICxDef, DModG.MyPID_DB, [
    'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts, ',
    'cad.CodUsu CU_CAD, its.CodUsu CU_ITS, ',
    'cad.Nome NO_CAD, its.Nome NO_ITS ',
    'FROM _imp_sicxdef def ',
    'LEFT JOIN ' + TMeuDB + '.atrsicxits its ON its.Controle=def.AtrIts ',
    'LEFT JOIN ' + TMeuDB + '.atrsicxcad cad ON cad.Codigo=def.AtrCad ',
    'WHERE def.ID_Sorc=' + Geral.FF0(QrSImaCxaControle.Value),
    'ORDER BY NO_CAD, NO_ITS ',
    '']);
end;

procedure TFmOSImp2.ReopenSImaAti();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSImaAti, DModG.MyPID_DB, [
    'SELECT ode.Nome NO_ATI, dis.* ',
    'FROM _imp_simaati dis ',
    'LEFT JOIN ' + TMeuDB + '.atividades ode ON ode.Codigo=dis.Atividade ',
    'WHERE dis.Codigo=' + Geral.FF0(QrSImaCadCodigo.Value),
    'ORDER  BY dis.MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenSImaCad();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSImaCad, DModG.MyPID_DB, [
    'SELECT fin.Nome NO_FIN, obj.Nome NO_OBJ, ',
    'tpc.Nome NO_TPC, sic.* ',
    'FROM _imp_simacad sic ',
    'LEFT JOIN ' + TMeuDB + '.objetos obj ON obj.Codigo=sic.Objeto ',
    'LEFT JOIN ' + TMeuDB + '.finalidads fin ON fin.Codigo=sic.Finalidade ',
    'LEFT JOIN ' + TMeuDB + '.tpconstrus tpc ON tpc.Codigo=sic.TpConstru ',
    'WHERE sic.SiapImaTer=' + Geral.FF0(QrSTerCadCodigo.Value),
    //'WHERE SiapImaTer=' + Geral.FF0(QrSterCadCliente.Value),
    'ORDER  BY sic.MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenFAESOSAge;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFAESOSAge, Dmod.MyDB, [
    'SELECT ent.Codigo, ent.Nome, ass.Dados AssDados, ass.Imagem AssImagem, ',
    'IF(ass.Nome <> "", ass.Nome, ent.Nome) AssNome ',
    'FROM osage osa ',
    'LEFT JOIN entidades ent ON ent.Codigo = osa.Agente ',
    'LEFT JOIN entiassina ass ON ass.Entidade = ent.Codigo ',
    'WHERE osa.Responsa = 1 ',
    'AND osa.Codigo=' + Geral.FF0(QrSelLugOSCab.Value),
    '']);
end;

procedure TFmOSImp2.ReopenFAESSImaCav();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFAESSSImaCav, Dmod.MyDB, [
    'SELECT DISTINCT ode.Nome NO_CARAC ',
    'FROM siapimacav scv ',
    'LEFT JOIN siapimacad sic ON sic.Codigo = scv.Codigo ',
    'LEFT JOIN siaptercad stc ON stc.Codigo = sic.SiapImaTer ',
    'LEFT JOIN caracteris ode ON ode.Codigo = scv.Caracteris ',
    'WHERE stc.Codigo=' + Geral.FF0(QrSelLugLugar.Value),
    '']);
end;

procedure TFmOSImp2.ReopenSImaCav();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSImaCav, DModG.MyPID_DB, [
    'SELECT ode.Nome NO_CARAC, dis.* ',
    'FROM _imp_simacav dis ',
    'LEFT JOIN ' + TMeuDB + '.caracteris ode ON ode.Codigo=dis.Caracteris ',
    'WHERE dis.Codigo=' + Geral.FF0(QrSImaCadCodigo.Value),
    'ORDER  BY dis.MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenSImaCdi();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSImaCdi, DModG.MyPID_DB, [
    'SELECT ode.Nome NO_CARAC, dis.* ',
    'FROM _imp_simacdi dis ',
    'LEFT JOIN ' + TMeuDB + '.caracteris ode ON ode.Codigo=dis.Caracteris ',
    'WHERE dis.Codigo=' + Geral.FF0(QrSImaCadCodigo.Value),
    'ORDER  BY dis.MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenSImaCui();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSImaCui, DModG.MyPID_DB, [
    'SELECT ode.Nome NO_CUI, dis.* ',
    'FROM _imp_simacui dis ',
    'LEFT JOIN ' + TMeuDB + '.cuidados ode ON ode.Codigo=dis.Cuidado ',
    'WHERE dis.Codigo=' + Geral.FF0(QrSImaCadCodigo.Value),
    'ORDER  BY dis.MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenSImaCxa();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSImaCxa, DModG.MyPID_DB, [
    'SELECT od1.Nome NO_MATER, ',
    'od2.Nome NO_FORMA, dis.* ',
    'FROM _imp_simacxa dis ',
    'LEFT JOIN ' + TMeuDB + '.cxamaters od1 ON od1.Codigo=dis.MatersCxa ',
    'LEFT JOIN ' + TMeuDB + '.cxaformas od2 ON od2.Codigo=dis.FormasCxa ',
    'WHERE dis.Codigo=' + Geral.FF0(QrSImaCadCodigo.Value),
    'ORDER  BY dis.MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenSImaDep();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSImaDep, DModG.MyPID_DB, [
    'SELECT IF(dis.Dependenci=0, "", dep.Nome) NO_DEP, dis.*',
    'FROM _imp_simadep dis ',
    'LEFT JOIN ' + TMeuDB + '.dependenci dep ON dep.Codigo=dis.Dependenci ',
    'WHERE dis.Codigo=' + Geral.FF0(QrSImaCadCodigo.Value),
    'ORDER  BY dis.MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenSImaRes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSImaRes, DModG.MyPID_DB, [
    'SELECT od1.Nome NO_CUI, ',
    'od2.Nome NO_RES, dis.* ',
    'FROM _imp_simares dis ',
    'LEFT JOIN ' + TMeuDB + '.cuidados od1 ON od1.Codigo=dis.Cuidado ',
    'LEFT JOIN ' + TMeuDB + '.residentes od2 ON od2.Codigo=dis.Residente ',
    'WHERE dis.Codigo=' + Geral.FF0(QrSImaCadCodigo.Value),
    'ORDER  BY dis.MyOrd ',
    '']);
end;

procedure TFmOSImp2.ReopenSMovCad();
var
  Tipos: String;
begin
  Tipos :=
    dmkPF.ArrayToTexto('mac.Tipo', 'NO_TIPO', pvNo, True, sListaTiposMoveis);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSMovCad, DModG.MyPID_DB, [
    'SELECT mac.*, ',
    Tipos,
    'FROM _imp_smovcad mac ',
    '']);
end;

procedure TFmOSImp2.ReopenSMovDef();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSMovDef, DModG.MyPID_DB, [
    'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts, ',
    'cad.CodUsu CU_CAD, its.CodUsu CU_ITS, ',
    'cad.Nome NO_CAD, its.Nome NO_ITS ',
    'FROM _imp_smovdef def ',
    'LEFT JOIN ' + TMeuDB + '.atramovits its ON its.Controle=def.AtrIts ',
    'LEFT JOIN ' + TMeuDB + '.atramovcad cad ON cad.Codigo=def.AtrCad ',
    'WHERE def.ID_Sorc=' + Geral.FF0(QrSMovCadCodigo.Value),
    'ORDER BY NO_CAD, NO_ITS ',
    '']);
end;

procedure TFmOSImp2.ReopenSTerCad();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSTerCad, DModG.MyPID_DB, [
    'SELECT stc.*, lls.Nome NO_LOGRAD ',
    'FROM _imp_stercad stc ',
    'LEFT JOIN ' + TMeuDB + '.ListaLograd lls ON lls.Codigo=stc.SLograd ',
    'WHERE stc.Codigo=' + Geral.FF0(QrSelLugLugar.Value),
    'ORDER  BY stc.Nome ',
    '']);
end;

procedure TFmOSImp2.ReopenSumOS();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumOS, DModG.MyPID_DB, [
    'SELECT SUM(OrcamTotal) OrcamTotal, ',
    'SUM(OrcamDesco) OrcamDesco,',
    'SUM(OrcamServi) OrcamServi, ',
    'SUM(Valorservi) ValorServi',
    'FROM _imp_oscab ',
    '']);
end;

procedure TFmOSImp2.ReopenSumPrz();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPrz, DModG.MyPID_DB, [
    'SELECT DISTINCT opz.Parcelas,  opz.Condicao,  ',
    'opz.DescoPer, opz.Escolhido, opz.Ativo, ppc.Nome NO_CONDICAO  ',
    'FROM _imp_osprz opz  ',
    'LEFT JOIN ' + TMeuDB + '.pediprzcab ppc ON ppc.Codigo=opz.Condicao  ',
    //'GROUP BY opz.DescoPer ', ERRO!
    'ORDER BY MyOrd, opz.DescoPer DESC ',
    '']);
end;

procedure TFmOSImp2.ReopenTipos(Qry: TmySQLQuery; ByOS: Boolean = False);
var
  SQLCompl, CtrlTxt, LocTxt: String;
begin
  if not ByOS then
  begin
    CtrlTxt  := Geral.FF0(QrOSSrvControle.Value);
    SQLCompl := 'WHERE osd.Controle=' + CtrlTxt;
  end else
  begin
    CtrlTxt  := Geral.FF0(QrSelLugOSCab.Value);
    SQLCompl := 'WHERE osd.Codigo=' + CtrlTxt;
  end;
  LocTxt  := Geral.FFI(QrLocaisc3Loc.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'SELECT DISTINCT  ',
    'IF(Tabela=1, "Tipo de local", "Tipo de objeto") TypLabel, ',
    'IF(Tabela=1, det.Codigo, mac.Tipo) + 0.000 c2Typ, ',
    'IF(Tabela=1, det.Nome,  ',
    '     ELT(mac.Tipo, "M�vel", "Autom�vel", ',
    '      "Animal", "Outro", "???")) n2Typ ',
    'FROM _imp_osfrmdep osd  ',
    'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro  ',
    'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo  ',
    'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci  ',
    'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType  ',
    'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro  ',
    SQLCompl,
    'AND IF(Tabela=1, sic.Codigo, 0)=' + LocTxt,
    ' ',
(*
  'UNION  ',
  '  ',
  'SELECT DISTINCT  ',
  'IF(Tabela=1, "Tipo de local", "Tipo de objeto") TypLabel, ',
  'IF(Tabela=1, det.Codigo, mac.Tipo) c2Typ, ',
  'IF(Tabela=1, det.Nome,  ',
  '     ELT(mac.Tipo, "M�vel", "Autom�vel", ',
  '      "Animal", "Outro", "???")) n2Typ ',
  'FROM _imp__OS_Mon_Dep_ osd  ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro  ',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo  ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci  ',
  'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dpd.DependType  ',
  'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro  ',
  'WHERE osd.Controle=' + CtrlTxt,
  'AND IF(Tabela=1, sic.Codigo, 0)=' + LocTxt,
  '  ',
*)
    'ORDER BY n2Typ ',
    '']);
end;

procedure TFmOSImp2.ReopenOSPipMI;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPipMI, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + FImp_OSPipMI,
    'WHERE Codigo=' + Geral.FF0(QrOSCabCodigo.Value),
    'AND PrgLstCab=' + Geral.FF0(QrOSPipMLCodigo.Value),
    //
    'ORDER BY NO_PIP, MyOrd ',
    //
    '']);
end;

procedure TFmOSImp2.ReopenOSPipML();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPipML, DModG.MyPID_DB, [
    'SELECT * FROM ' + FImp_OSPipML,
    //WHERE Codigo da OS = QrOSCabCodigo.Value
    '']);
end;

procedure TFmOSImp2.ReopenOSPipMonIts(OSCab, PrgLstCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPipMonIts, Dmod.MyDB, [
    'SELECT sid.Controle SiapImaDep, sic.Codigo SiapImaCad, ',
    'sic.SCompl2 NO_LOCAL, dep.Nome NO_DEPENDENCI, ',
    '',
    'IF(opi.Respondido<>1, "", ',
    '  ELT(Funcoes, ',
    '  ELT(RespBin + 1, pb0.Nome, pb1.Nome, ""),',
    '  TRIM(TRAILING ",000" FROM REPLACE(RespQtd, ".", ",")), ',
    '  IF(ppr.GraGruX IS NULL, "", CONCAT(',
    '    TRIM(TRAILING ",000" FROM REPLACE(ppr.UsoQtd, ".", ",")),',
    '    " unidade", IF(RespQtd > 1, "s", ""), " da isca ", gg1i.Nome)),',
    '  pai.Nome,',
    '  LEFT(RespTxtLvr, 512))) RESPOSTA, ',
    '',
    'pip.Nome NO_PIP, ELT(opi.Relacao, gg1e.Referencia, ',
    'gg1p.Referencia) NO_ITEM, pcp.Nome NO_PERGUNTA, pcp.Sigla, ',
    'dep.Nome NO_DEPENDENCI, ',
    'opm.PipCad, ',
    'opi.* ',
    'FROM ospipits opi ',
    'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta ',
    'LEFT JOIN ospipmon opm ON opm.Controle=opi.Controle',
    'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad',
    'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts',
    'LEFT JOIN ospipitspr ppr ON ppr.Conta=opi.Conta',
    'LEFT JOIN prgbincad pb0 ON pb0.Codigo=opi.BinarCad0',
    'LEFT JOIN prgbincad pb1 ON pb1.Codigo=opi.BinarCad1',
    'LEFT JOIN gragrux ggxp ON ggxp.Controle=opi.GraGruX',
    'LEFT JOIN gragru1 gg1p ON gg1p.Nivel1=ggxp.GraGru1',
    'LEFT JOIN gragrux ggxe ON ggxe.Controle=pip.Equipamento',
    'LEFT JOIN gragru1 gg1e ON gg1e.Nivel1=ggxe.GraGru1',
    'LEFT JOIN gragrux ggxi ON ggxi.Controle=ppr.GraGruX',
    'LEFT JOIN gragru1 gg1i ON gg1i.Nivel1=ggxi.GraGru1',
    'LEFT JOIN siapimadep sid ON sid.Controle=pip.Dependenci ',
    'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo ',
    'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
    '',
    'WHERE opi.Codigo=' + Geral.FF0(OSCab),
    'AND opi.PrgLstCab=' + Geral.FF0(PrgLstCab),
    'AND NOT (opi.Filiacao IN',
    '  (',
    '  SELECT Controle',
    '  FROM prglstits',
    '  WHERE Codigo=' + Geral.FF0(PrgLstCab),
    '  AND LupForma=' + Geral.FF0(CO_LupForma_COD_PorLoop),
    '  )',
    ')',
    'ORDER BY opi.Ordem, opi.SubOrdem ',
    '']);
(*
  'SELECT pip.Nome NO_PIP, ELT(opi.Relacao, gg1e.Referencia, ',
  'gg1p.Referencia) NO_ITEM, pcp.Nome NO_PERGUNTA, pcp.Sigla, ',
  'opi.* ',
  'FROM ospipits opi ',
  'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta ',
  'LEFT JOIN ospipmon opm ON opm.Controle=opi.Controle',
  'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad',
  'LEFT JOIN gragrux ggxp ON ggxp.Controle=opi.GraGruX',
  'LEFT JOIN gragru1 gg1p ON gg1p.Nivel1=ggxp.GraGru1',
  'LEFT JOIN gragrux ggxe ON ggxe.Controle=pip.Equipamento',
  'LEFT JOIN gragru1 gg1e ON gg1e.Nivel1=ggxe.GraGru1',
  'WHERE opi.Codigo=' + Geral.FF0(OSCab),
  'AND opi.PrgLstCab=' + Geral.FF0(PrgLstCab),
  'AND NOT (opi.Filiacao IN',
  '  (',
  '  SELECT Controle',
  '  FROM prglstits',
  '  WHERE Codigo=' + Geral.FF0(PrgLstCab),
  '  AND LupForma=' + Geral.FF0(CO_LupForma_COD_PorLoop),
  '  )',
  ')',
  'ORDER BY opi.Ordem, opi.SubOrdem ',
  '']);
*)
end;

procedure TFmOSImp2.ReopenOSPipMonLst(OSCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPipMonLst, Dmod.MyDB, [
    'SELECT DISTINCT plc.Codigo, plc.Nome ',
    'FROM ospipits opi ',
    'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
    'WHERE opi.Codigo=' + Geral.FF0(OsCab),
    'ORDER BY opi.Ordem  ',
    '']);
end;

procedure TFmOSImp2.ReopenOSPipMonSub(OSCab, PrgLstIts: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPipMonSub, Dmod.MyDB, [
    'SELECT sid.Controle SiapImaDep, sic.Codigo SiapImaCad, ',
    'sic.SCompl2 NO_LOCAL, dep.Nome NO_DEPENDENCI, ',
    '',
    '',
    'IF(opi.Respondido<>1, "", ',
    '  ELT(Funcoes, ',
    '  ELT(RespBin + 1, pb0.Nome, pb1.Nome, ""),',
    '  TRIM(TRAILING ",000" FROM REPLACE(RespQtd, ".", ",")), ',
    '  IF(ppr.GraGruX IS NULL, "", CONCAT(',
    '    TRIM(TRAILING ",000" FROM REPLACE(ppr.UsoQtd, ".", ",")),',
    '    " unidade", IF(RespQtd > 1, "s", ""), " da isca ", gg1i.Nome)),',
    '  pai.Nome,',
    '  LEFT(RespTxtLvr, 512))) RESPOSTA, ',
    '',
    'pip.Nome NO_PIP, ELT(opi.Relacao, gg1e.Referencia, ',
    'gg1p.Referencia) NO_ITEM, pcp.Nome NO_PERGUNTA, pcp.Sigla, ',
    'opm.PipCad, ',
    'opi.* ',
    'FROM ospipits opi ',
    'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta ',
    'LEFT JOIN ospipmon opm ON opm.Controle=opi.Controle',
    'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad',
    'LEFT JOIN ospipitspr ppr ON ppr.Conta=opi.Conta',
    'LEFT JOIN prgbincad pb0 ON pb0.Codigo=opi.BinarCad0',
    'LEFT JOIN prgbincad pb1 ON pb1.Codigo=opi.BinarCad1',
    'LEFT JOIN gragrux ggxp ON ggxp.Controle=opi.GraGruX',
    'LEFT JOIN gragru1 gg1p ON gg1p.Nivel1=ggxp.GraGru1',
    'LEFT JOIN gragrux ggxe ON ggxe.Controle=pip.Equipamento',
    'LEFT JOIN gragru1 gg1e ON gg1e.Nivel1=ggxe.GraGru1',
    'LEFT JOIN gragrux ggxi ON ggxi.Controle=ppr.GraGruX',
    'LEFT JOIN gragru1 gg1i ON gg1i.Nivel1=ggxi.GraGru1',
    'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts',
    'LEFT JOIN siapimadep sid ON sid.Controle=pip.Dependenci ',
    'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo ',
    'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
    '',
    'WHERE opi.Codigo=' + Geral.FF0(OSCab),
    'AND (opi.Filiacao IN',
    '  (',
    '  SELECT Controle',
    '  FROM prglstits',
    '  WHERE Controle=' + Geral.FF0(PrgLstIts),
    '  )',
    ')',
    'ORDER BY opi.Ordem, opi.SubOrdem ',
    '']);
(*
  'SELECT pip.Nome NO_PIP, ELT(opi.Relacao, gg1e.Referencia, ',
  'gg1p.Referencia) NO_ITEM, pcp.Nome NO_PERGUNTA, pcp.Sigla, ',
  'opi.* ',
  'FROM ospipits opi ',
  'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta ',
  'LEFT JOIN ospipmon opm ON opm.Controle=opi.Controle',
  'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad',
  'LEFT JOIN gragrux ggxp ON ggxp.Controle=opi.GraGruX',
  'LEFT JOIN gragru1 gg1p ON gg1p.Nivel1=ggxp.GraGru1',
  'LEFT JOIN gragrux ggxe ON ggxe.Controle=pip.Equipamento',
  'LEFT JOIN gragru1 gg1e ON gg1e.Nivel1=ggxe.GraGru1',
  'WHERE opi.Codigo=' + Geral.FF0(OSCab),
  'AND (opi.Filiacao IN',
  '  (',
  '  SELECT Controle',
  '  FROM prglstits',
  '  WHERE Controle=' + Geral.FF0(PrgLstIts),
  '  )',
  ')',
  'ORDER BY opi.Ordem, opi.SubOrdem ',
  '']);
*)
end;

{
object frxDs_OS_Mon_Dep_: TfrxDBDataset
  UserName = 'frxDs_OS_Mon_Dep_'
  CloseDataSource = False
  FieldAliases.Strings = (
    'c3Loc=c3Loc'
    'n3Loc=n3Loc'
    'c2Typ=c2Typ'
    'n2Typ=n2Typ'
    'SIGLA=SIGLA'
    'n1Dep=n1Dep'
    'Ativo=Ativo'
    'SIGLA_E_LOC=SIGLA_E_LOC')
  DataSet = Qr_OS_Mon_Dep_
  BCDToCurrency = False
  Left = 752
  Top = 12
end
object Qr_OS_Mon_Dep_: TmySQLQuery
  Database = Dmod.ZZDB
  OnCalcFields = Qr_OS_Mon_Dep_CalcFields
  SQL.Strings = (
    'SELECT osd.*, '
    'IF(Tabela=1, sic.Codigo, 0) + 0.000 c3Loc, '
    'IF(Tabela=1, sic.SCompl2, " ") n3Loc,  '
    'IF(Tabela=1, det.Codigo, mac.Tipo) +0.000 c2Typ, '
    'IF(Tabela=1, det.Nome,  '
    '     ELT(mac.Tipo, "M'#243'vel", "Autom'#243'vel",  '
    '      "Animal", "Outro", "???")) n2Typ, '
    'ELT(osd.Tabela, "I", "M", "?") SIGLA,  '
    'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") n1Dep'
    'FROM _imp_osdep osd'
    'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=osd.Cadastro'
    'LEFT JOIN ' + TMeuDB + '.dependenci dpd ON dpd.Codigo=sid.Dependenci'
    'LEFT JOIN ' + TMeuDB + '.movamovcad mac ON mac.Codigo=osd.Cadastro'
    'WHERE osd.Controle=:P0')
  Left = 724
  Top = 12
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end>
  object Qr_OS_Mon_Dep_c3Loc: TFloatField
    FieldName = 'c3Loc'
  end
  object Qr_OS_Mon_Dep_n3Loc: TWideStringField
    FieldName = 'n3Loc'
    Size = 60
  end
  object Qr_OS_Mon_Dep_n2Typ: TWideStringField
    FieldName = 'n2Typ'
    Size = 60
  end
  object Qr_OS_Mon_Dep_SIGLA: TWideStringField
    FieldName = 'SIGLA'
    Size = 1
  end
  object Qr_OS_Mon_Dep_n1Dep: TWideStringField
    FieldName = 'n1Dep'
    Size = 100
  end
  object Qr_OS_Mon_Dep_Ativo: TSmallintField
    FieldName = 'Ativo'
  end
  object Qr_OS_Mon_Dep_SIGLA_E_LOC: TWideStringField
    FieldKind = fkCalculated
    FieldName = 'SIGLA_E_LOC'
    Size = 255
    Calculated = True
  end
  object Qr_OS_Mon_Dep_c2Typ: TFloatField
    FieldName = 'c2Typ'
  end
end
}

//[frxDsOSTox5."NO_Diluente"]

//OSTox5NO_Diluente


end.
