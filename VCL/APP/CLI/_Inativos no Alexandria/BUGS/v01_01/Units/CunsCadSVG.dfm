object FmCunsCadSVG: TFmCunsCadSVG
  Left = 0
  Top = 0
  Caption = 'CAD-SUBCL-014 :: Croqui'
  ClientHeight = 503
  ClientWidth = 931
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 629
    Top = 0
    Height = 503
    Align = alRight
    ExplicitLeft = 612
    ExplicitTop = 28
  end
  object PageControl1: TPageControl
    Left = 632
    Top = 0
    Width = 299
    Height = 503
    ActivePage = TabSheet4
    Align = alRight
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Vetor'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 291
        Height = 475
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object TreeView1: TTreeView
          Left = 1
          Top = 22
          Width = 289
          Height = 452
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HideSelection = False
          Indent = 19
          ParentFont = False
          TabOrder = 0
        end
        object Edit1: TEdit
          Left = 1
          Top = 1
          Width = 289
          Height = 21
          Align = alTop
          TabOrder = 1
          TextHint = 'ID name to search'
          OnChange = Edit1Change
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'PMVs deste croqui'
      ImageIndex = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 291
        Height = 475
        Align = alClient
        DataSource = DsPMVsSim
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Visible = True
          end>
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Outros PMVs'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 291
        Height = 475
        Align = alClient
        DataSource = DsPMVsNao
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = DBGrid2DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Visible = True
          end>
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
      object BitBtn1: TBitBtn
        Left = 100
        Top = 124
        Width = 75
        Height = 25
        Caption = 'BitBtn1'
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object Button1: TButton
        Left = 108
        Top = 164
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 1
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 84
        Top = 240
        Width = 75
        Height = 25
        Caption = 'Button2'
        TabOrder = 2
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 16
        Top = 24
        Width = 75
        Height = 25
        Caption = 'Button3'
        TabOrder = 3
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 92
        Top = 284
        Width = 75
        Height = 25
        Caption = 'Button4'
        TabOrder = 4
        OnClick = Button4Click
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'TabSheet5'
      ImageIndex = 4
      object Image1: TImage
        Left = 0
        Top = 0
        Width = 291
        Height = 475
        Align = alClient
        ExplicitLeft = 96
        ExplicitTop = 200
        ExplicitWidth = 105
        ExplicitHeight = 105
      end
    end
  end
  object ScrollBox1: TScrollBox
    Left = 0
    Top = 0
    Width = 629
    Height = 503
    Align = alClient
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 1
    object RSSVGImage1: TRSSVGImage
      Left = 0
      Top = 0
      Width = 625
      Height = 499
      Opacity = 1.000000000000000000
      SVGDocument = RSSVGDocument1
      WrapMode = iwOriginal
      ScaleOriginal = 1.000000000000000000
      Align = alClient
      ExplicitLeft = -132
      ExplicitTop = -4
      ExplicitWidth = 743
      ExplicitHeight = 503
    end
  end
  object RSSVGDocument1: TRSSVGDocument
    DefaultTextRendering = txrnOptimizeLegibility
    PreferredLanguage = 'EN'
    Left = 68
    Top = 236
  end
  object QrSiapImaSVG: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM siapimasvg '
      'WHERE Controle>0')
    Left = 72
    Top = 188
    object QrSiapImaSVGCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaSVGControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapImaSVGNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrSiapImaSVGNoArq: TWideStringField
      FieldName = 'NoArq'
      Required = True
      Size = 255
    end
    object QrSiapImaSVGArquivo: TWideMemoField
      FieldName = 'Arquivo'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSiapImaSVGLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSiapImaSVGDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSiapImaSVGDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSiapImaSVGUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSiapImaSVGUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSiapImaSVGAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSiapImaSVGAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrPMVsSim: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mot.Nome NO_MOTDESAT, gg1.Nome NO_EQUI, gg1.Nivel1, '
      'plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA, '
      'IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT, '
      'IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT, '
      'cad.* '
      'FROM pipcad cad '
      'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ '
      'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab '
      'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci '
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci '
      'LEFT JOIN osmoncab omc ON omc.Conta=cad.OsMonCab '
      'LEFT JOIN oscab osc ON osc.Codigo=omc.Codigo '
      'WHERE osc.Entidade=1433'
      'AND (cad.MotInutili = 0 AND cad.MotInutili = 0) '
      ''
      'ORDER BY Nome ')
    Left = 72
    Top = 52
    object QrPMVsSimNO_MOTDESAT: TWideStringField
      FieldName = 'NO_MOTDESAT'
      Size = 60
    end
    object QrPMVsSimNO_EQUI: TWideStringField
      FieldName = 'NO_EQUI'
      Size = 120
    end
    object QrPMVsSimNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPMVsSimNO_LISTA: TWideStringField
      FieldName = 'NO_LISTA'
      Size = 100
    end
    object QrPMVsSimNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrPMVsSimDtaAquis_TXT: TWideStringField
      FieldName = 'DtaAquis_TXT'
      Required = True
      Size = 10
    end
    object QrPMVsSimDtaDesativ_TXT: TWideStringField
      FieldName = 'DtaDesativ_TXT'
      Required = True
      Size = 10
    end
    object QrPMVsSimCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPMVsSimNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPMVsSimEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrPMVsSimOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrPMVsSimMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrPMVsSimDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrPMVsSimDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrPMVsSimLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPMVsSimDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPMVsSimDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPMVsSimUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPMVsSimUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPMVsSimAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPMVsSimAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPMVsSimPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrPMVsSimDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrPMVsSimIntrvMonDD: TIntegerField
      FieldName = 'IntrvMonDD'
    end
    object QrPMVsSimOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPMVsSimReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrPMVsSimMotInutili: TIntegerField
      FieldName = 'MotInutili'
    end
    object QrPMVsSimDtaInutili: TDateField
      FieldName = 'DtaInutili'
    end
    object QrPMVsSimSvgPosX: TFloatField
      FieldName = 'SvgPosX'
    end
    object QrPMVsSimSvgPosY: TFloatField
      FieldName = 'SvgPosY'
    end
    object QrPMVsSimSvgZumF: TFloatField
      FieldName = 'SvgZumF'
    end
    object QrPMVsSimSiapImaSVG: TIntegerField
      FieldName = 'SiapImaSVG'
    end
  end
  object DsPMVsSim: TDataSource
    DataSet = QrPMVsSim
    Left = 72
    Top = 100
  end
  object QrPMVsNao: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mot.Nome NO_MOTDESAT, gg1.Nome NO_EQUI, gg1.Nivel1, '
      'plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA, '
      'IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT, '
      'IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT, '
      'cad.* '
      'FROM pipcad cad '
      'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ '
      'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab '
      'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci '
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci '
      'LEFT JOIN osmoncab omc ON omc.Conta=cad.OsMonCab '
      'LEFT JOIN oscab osc ON osc.Codigo=omc.Codigo '
      'WHERE osc.Entidade=1433'
      'AND (cad.MotInutili = 0 AND cad.MotInutili = 0) '
      ''
      'ORDER BY Nome ')
    Left = 136
    Top = 52
    object QrPMVsNaoNO_MOTDESAT: TWideStringField
      FieldName = 'NO_MOTDESAT'
      Size = 60
    end
    object QrPMVsNaoNO_EQUI: TWideStringField
      FieldName = 'NO_EQUI'
      Size = 120
    end
    object QrPMVsNaoNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPMVsNaoNO_LISTA: TWideStringField
      FieldName = 'NO_LISTA'
      Size = 100
    end
    object QrPMVsNaoNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrPMVsNaoDtaAquis_TXT: TWideStringField
      FieldName = 'DtaAquis_TXT'
      Required = True
      Size = 10
    end
    object QrPMVsNaoDtaDesativ_TXT: TWideStringField
      FieldName = 'DtaDesativ_TXT'
      Required = True
      Size = 10
    end
    object QrPMVsNaoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPMVsNaoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPMVsNaoEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrPMVsNaoOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrPMVsNaoMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrPMVsNaoDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrPMVsNaoDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrPMVsNaoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPMVsNaoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPMVsNaoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPMVsNaoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPMVsNaoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPMVsNaoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPMVsNaoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPMVsNaoPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrPMVsNaoDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrPMVsNaoIntrvMonDD: TIntegerField
      FieldName = 'IntrvMonDD'
    end
    object QrPMVsNaoOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPMVsNaoReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrPMVsNaoMotInutili: TIntegerField
      FieldName = 'MotInutili'
    end
    object QrPMVsNaoDtaInutili: TDateField
      FieldName = 'DtaInutili'
    end
    object QrPMVsNaoSvgPosX: TFloatField
      FieldName = 'SvgPosX'
    end
    object QrPMVsNaoSvgPosY: TFloatField
      FieldName = 'SvgPosY'
    end
    object QrPMVsNaoSvgZumF: TFloatField
      FieldName = 'SvgZumF'
    end
    object QrPMVsNaoSiapImaSVG: TIntegerField
      FieldName = 'SiapImaSVG'
    end
  end
  object DsPMVsNao: TDataSource
    DataSet = QrPMVsNao
    Left = 136
    Top = 100
  end
  object QrGraG1EqMo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM grag1eqmo')
    Left = 72
    Top = 144
    object QrGraG1EqMoNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraG1EqMoMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraG1EqMoObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGraG1EqMoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraG1EqMoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraG1EqMoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraG1EqMoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraG1EqMoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraG1EqMoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraG1EqMoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraG1EqMoPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrGraG1EqMoNaoUsaPrdt: TSmallintField
      FieldName = 'NaoUsaPrdt'
    end
    object QrGraG1EqMoVetorSvg: TWideMemoField
      FieldName = 'VetorSvg'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGraG1EqMoVetorArq: TWideStringField
      FieldName = 'VetorArq'
      Required = True
      Size = 255
    end
    object QrGraG1EqMoPrgICorSVG: TIntegerField
      FieldName = 'PrgICorSVG'
    end
    object QrGraG1EqMoDiasCorSVG: TIntegerField
      FieldName = 'DiasCorSVG'
    end
    object QrGraG1EqMoQtCorSVGgg: TIntegerField
      FieldName = 'QtCorSVGgg'
    end
    object QrGraG1EqMoQtCorSVGgr: TIntegerField
      FieldName = 'QtCorSVGgr'
    end
    object QrGraG1EqMoQtCorSVGrg: TIntegerField
      FieldName = 'QtCorSVGrg'
    end
    object QrGraG1EqMoQtCorSVGrr: TIntegerField
      FieldName = 'QtCorSVGrr'
    end
    object QrGraG1EqMoQtCorSVGrb: TIntegerField
      FieldName = 'QtCorSVGrb'
    end
    object QrGraG1EqMoQtCorSVGbr: TIntegerField
      FieldName = 'QtCorSVGbr'
    end
    object QrGraG1EqMoZumPadr: TFloatField
      FieldName = 'ZumPadr'
    end
    object QrGraG1EqMoSVGLegenda: TWideStringField
      FieldName = 'SVGLegenda'
      Size = 50
    end
  end
  object PMPMV: TPopupMenu
    Left = 252
    Top = 260
    object Zoom1: TMenuItem
      Caption = 'Zoom'
      OnClick = Zoom1Click
    end
  end
end
