unit PipCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums;

type
  TFmPipCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPipCad: TmySQLQuery;
    DsPipCad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo_: TdmkEdit;
    Label9: TLabel;
    EdNome_: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma_: TBitBtn;
    Panel1: TPanel;
    BtDesiste_: TBitBtn;
    QrPipCadNO_MOTDESAT: TWideStringField;
    QrPipCadNO_EQUI: TWideStringField;
    QrPipCadNO_LISTA: TWideStringField;
    QrPipCadCodigo: TIntegerField;
    QrPipCadNome: TWideStringField;
    QrPipCadEquipamento: TIntegerField;
    QrPipCadOSMonCab: TIntegerField;
    QrPipCadMotDesativ: TIntegerField;
    QrPipCadDtaAquis: TDateField;
    QrPipCadDtaDesativ: TDateField;
    QrPipCadPrgLstCab: TIntegerField;
    Panel6: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    QrOrig: TmySQLQuery;
    DsOrig: TDataSource;
    GBDesat: TGroupBox;
    Panel7: TPanel;
    Label6: TLabel;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    GBOrig: TGroupBox;
    Panel8: TPanel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    QrOrigCodigo: TIntegerField;
    QrOrigSiapTerCad: TIntegerField;
    QrOrigNome: TWideStringField;
    QrOrigNO_CLIENTE: TWideStringField;
    Label11: TLabel;
    DBEdit10: TDBEdit;
    Label12: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    QrPipCadDependenci: TIntegerField;
    QrOrigEntidade: TSmallintField;
    QrPipCadDtaAquis_TXT: TWideStringField;
    QrPipCadDtaDesativ_TXT: TWideStringField;
    QrPipCadNO_DEPENDENCIA: TWideStringField;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Label14: TLabel;
    DBEdit15: TDBEdit;
    Label15: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    QrPipCadDtaInutili_TXT: TWideStringField;
    QrPipCadMotInutili: TIntegerField;
    QrPipCadNO_MOTINUTI: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    //procedure BtConfirmaClick(Sender: TObject);
    //procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPipCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPipCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrPipCadBeforeClose(DataSet: TDataSet);
    procedure QrPipCadAfterScroll(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenOrig();
  public
    { Public declarations }
    FCodigo, FEntidade: Integer;
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPipCad: TFmPipCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PipRapido, MyDBCheck, DmkDAC_PF, Principal, PipPesq;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPipCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPipCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPipCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPipCad.DefParams;
begin
  VAR_GOTOTABELA := 'pipcad';
  VAR_GOTOMYSQLTABLE := QrPipCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT mot.Nome NO_MOTDESAT, inu.Nome NO_MOTINUTI, ');
  VAR_SQLx.Add('gg1.Nome NO_EQUI, plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA, ');
(*
  VAR_SQLx.Add('IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT, ');
  VAR_SQLx.Add('IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT, ');
  VAR_SQLx.Add('IF(DtaInutili<= "30/12/1899", "", DtaInutili) DtaInutili_TXT, ');
*)
  VAR_SQLx.Add('IF(DtaAquis <= "1900-01-01", "", ');
  VAR_SQLx.Add('  DATE_FORMAT(DtaAquis, "%d/%m/%Y")) DtaAquis_TXT, ');
  VAR_SQLx.Add('IF(DtaDesativ<= "1900-01-01", "", ');
  VAR_SQLx.Add('  DATE_FORMAT(DtaDesativ, "%d/%m/%Y")) DtaDesativ_TXT, ');
  VAR_SQLx.Add('IF(DtaInutili<= "1900-01-01", "", ');
  VAR_SQLx.Add('  DATE_FORMAT(DtaInutili, "%d/%m/%Y")) DtaInutili_TXT, ');
  //
  VAR_SQLx.Add('cad.* ');
  VAR_SQLx.Add('FROM pipcad cad');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento');
  VAR_SQLx.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ');
  VAR_SQLx.Add('LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ');
  VAR_SQLx.Add('LEFT JOIN motdesativ inu ON inu.Codigo=cad.MotInutili');
  VAR_SQLx.Add('LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab');
  VAR_SQLx.Add('LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci');
  VAR_SQLx.Add('LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci');
  VAR_SQLx.Add('WHERE cad.Codigo<>0');
  VAR_SQL1.Add('AND cad.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND cad.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cad.Nome Like :P0');
  //
end;

procedure TFmPipCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPipCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPipCad.ReopenOrig();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrig, Dmod.MyDB, [
    'SELECT cab.Codigo, cab.Entidade, cab.SiapTerCad, stc.Nome, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE ',
    'FROM osmoncab omc ',
    'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo ',
    'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapterCad ',
    'WHERE omc.Conta=' + Geral.FF0(QrPipCadOSMonCab.Value),
    '']);
end;

procedure TFmPipCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPipCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPipCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPipCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPipCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPipCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPipCad.BtAlteraClick(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := QrOrigEntidade.Value;
  //
  if Entidade = 0 then
    Entidade := FEntidade;
(*
  if DBCheck.CriaFm(TFmPipRapido, FmPipRapido, afmoNegarComAviso) then
  begin
    FmPipRapido.ImgTipo.SQLType := stUpd;
    //
    FmPipRapido.FEntidade := Entidade;
    FmPipRapido.ReopenSiapImaDep(Entidade);
    //
    FmPipRapido.EdCodigo.ValueVariant      := QrPipCadCodigo.Value;
    FmPipRapido.EdNome.Text                := QrPipCadNome.Value;
    FmPipRapido.TPDtaAquis.Date            := QrPipCadDtaAquis.Value;
    FmPipRapido.EdEquipamento.ValueVariant := QrPipCadEquipamento.Value;
    FmPipRapido.CBEquipamento.KeyValue     := QrPipCadEquipamento.Value;
    FmPipRapido.EdPrgLstCab.ValueVariant   := QrPipCadPrgLstCab.Value;
    FmPipRapido.CBPrgLstCab.KeyValue       := QrPipCadPrgLstCab.Value;
    FmPipRapido.EdMotDesativ.ValueVariant  := QrPipCadMotDesativ.Value;
    FmPipRapido.CBMotDesativ.KeyValue      := QrPipCadMotDesativ.Value;
    FmPipRapido.TPDtaDesativ.Date          := QrPipCadDtaDesativ.Value;
    FmPipRapido.EdDependenci.ValueVariant  := QrPipCadDependenci.Value;
    FmPipRapido.CBDependenci.KeyValue      := QrPipCadDependenci.Value;
    //
    FmPipRapido.ShowModal;
    FmPipRapido.Destroy;
  end;
*)
  FmPrincipal.MostraFormPipRapido(QrPipCadCodigo.Value, stUpd, Entidade, 0, True);
    LocCod(QrPipCadCodigo.Value, QrPipCadCodigo.Value);
end;

procedure TFmPipCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPipCadCodigo.Value;
  Close;
end;

{
procedure TFmPipCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('pipcad', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrPipCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'pipcad', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;
procedure TFmPipCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'pipcad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;
}

procedure TFmPipCad.BtIncluiClick(Sender: TObject);
begin
{ Ver se d� para incluir por aqui
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPipCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'pipcad');
}
end;

procedure TFmPipCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDesat.Align   := alClient;
  FEntidade       := 0;
  //
  CriaOForm;
end;

procedure TFmPipCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPipCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPipCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmPipCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPipCad.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrPipCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPipCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPipCad.QrPipCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPipCad.QrPipCadAfterScroll(DataSet: TDataSet);
begin
  ReopenOrig();
end;

procedure TFmPipCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPipCad.SbQueryClick(Sender: TObject);
var
  Codigo: Integer;
begin
(*
  LocCod(QrPipCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'pipcad', Dmod.MyDB, CO_VAZIO));
*)
  if DBCheck.CriaFm(TFmPipPesq, FmPipPesq, afmoNegarComAviso) then
  begin
    FmPipPesq.ImgTipo.SQLType := stPsq;
    FmPipPesq.ShowModal;
    Codigo := FmPipPesq.FCodigoSel;
    FmPipPesq.Destroy;
    //
    if Codigo <> 0 then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPipCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPipCad.QrPipCadBeforeClose(DataSet: TDataSet);
begin
  QrOrig.Close;
end;

procedure TFmPipCad.QrPipCadBeforeOpen(DataSet: TDataSet);
begin
  QrPipCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

