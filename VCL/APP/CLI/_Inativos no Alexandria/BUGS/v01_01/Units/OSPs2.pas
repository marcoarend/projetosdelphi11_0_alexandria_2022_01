unit OSPs2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, dmkEdit, UnDmkEnums;

type
  TFmOSPs2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox9: TGroupBox;
    Label11: TLabel;
    RGDireto: TRadioGroup;
    EdDireto: TdmkEdit;
    QrLoc: TmySQLQuery;
    QrLocCodigo: TIntegerField;
    QrLocGrupo: TIntegerField;
    QrLocOpcao: TIntegerField;
    QrLocSiapTerCad: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGDiretoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmOSPs2: TFmOSPs2;

implementation

uses UnMyObjects, Module, DmkDAC_PF, OSCab2, Principal, OSPsq, UnOSApp_PF;

{$R *.DFM}

procedure TFmOSPs2.BtOKClick(Sender: TObject);
(*
const
  Atual = 0;
  Disposicao = dispIndefinido;
  ReabreOSCab = True;
*)
var
  //SQL: String;
  //Form: TForm;
  //Grupo, Opcao, Lugar, OSCab: Integer;
  IDIdx: Integer;
begin
  if MyObjects.FIC(RGDireto.ItemIndex = -1, RGDireto,
  'Defina o item de pesquisa "Por:"!') then
    Exit;
  IDIdx := EdDireto.ValueVariant;
  if MyObjects.FIC(EdDireto.ValueVariant = 0, EdDireto,
  'Defina o item de pesquisa "N�mero:"!') then
    Exit;
  case RGDireto.ItemIndex of
    0: OSApp_PF.MostraFormOSCab2(mfoisGrupo, IDIdx);
    1: OSApp_PF.MostraFormOSCab2(mfoisCodigo, IDIdx);
    2: OSApp_PF.MostraFormOSCab2(mfoisPesq, IDIdx);
  end;
  Close;
(*
  case RGDireto.ItemIndex of
    0: SQL := 'WHERE Grupo=' + Geral.FF0(EdDireto.ValueVariant);
    1: SQL := 'WHERE Codigo=' + Geral.FF0(EdDireto.ValueVariant);
    2:
    begin
      MyObjects.FormTDICria(TFmOSPsq, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
      Close;
      Exit;
    end
    else
    begin
      Geral.MB_Aviso('Defina o item de pesquisa "Por:"!');
      Exit;
    end;
  end;
  if EdDireto.ValueVariant = 0 then
  begin
    Geral.MB_Aviso('Defina o item de pesquisa "N�mero:"!');
    Exit;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
  'SELECT Codigo, Grupo, Opcao, SiapTerCad ',
  'FROM oscab ',
  SQL,
  '']);
  //
  if (QrLoc.State <> dsInactive) and (QrLoc.RecordCount > 0) then
  begin
    Grupo := QrLocGrupo.Value;
    Opcao := QrLocOpcao.Value;
    Lugar := QrLocSiapTerCad.Value;
    OSCab := QrLocCodigo.Value;
    //
    Form := MyObjects.FormTDICria(TFmOSCab2, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
    TFmOSCab2(Form).LocCod(Atual, Grupo, Disposicao, Lugar, Opcao, ReabreOSCab, OSCab);
    //
    Close;
  end;
*)
end;

procedure TFmOSPs2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPs2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSPs2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSPs2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPs2.RGDiretoClick(Sender: TObject);
begin
  BtOKClick(Sender);
end;

end.
