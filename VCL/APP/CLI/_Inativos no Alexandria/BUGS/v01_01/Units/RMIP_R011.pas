unit RMIP_R011;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, frxClass, frxDBSet,
  mySQLDbTables, UnInternalConsts, dmkGeral, Vcl.StdCtrls;

type
  TFmRMIP_R011 = class(TForm)
    frxReport011A: TfrxReport;
    Qr011CIC_Imp: TmySQLQuery;
    Qr011CIC_ImpCodigo: TIntegerField;
    Qr011CIC_ImpControle: TIntegerField;
    Qr011CIC_ImpNome: TWideStringField;
    Qr011CIC_ImpStatus: TIntegerField;
    Qr011CIC_ImpCaminho: TWideStringField;
    Qr011CIC_ImpNO_STATUS: TWideStringField;
    Qr011CIC_ImpDependencia: TIntegerField;
    Qr011CIC_ImpNO_DEPENDENCIA: TWideStringField;
    Qr011CIC_ImpCodiNCT: TIntegerField;
    Qr011CIC_ImpObserv: TWideMemoField;
    Qr011CIC_ImpAplicacao: TIntegerField;
    Qr011CIC_ImpNO_NCT: TWideStringField;
    Qr011CIC_ImpTxtCAC: TWideStringField;
    Qr011CIC_ImpNO_LOCAL: TWideStringField;
    Qr011CIC_ImpOrdem: TIntegerField;
    Qr011CIC_ImpNO_STC: TWideStringField;
    frxDs011CIC_Imp: TfrxDBDataset;
    Qr011MixRel: TmySQLQuery;
    Qr011MixRelFC: TFloatField;
    Qr011MixRelNCT: TFloatField;
    procedure frxReport011AGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    //
    procedure ReopenCIC_Imp(Codigo: Integer);
  public
    { Public declarations }
    FEmpresa, FCliente, FItemRel: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL, F_Cuns_Img_Cab: String;
    FMemo1: TMemo;
    //
    procedure GeraImp_NCTs();
    procedure frxReport000GetValue(frxReport: TfrxReport; const VarName: string; var Value: Variant);
  end;

var
  FmRMIP_R011: TFmRMIP_R011;

implementation

uses Module, UnMyObjects, ModuleGeral, UnDmkProcFunc, DmkDAC_PF;

{$R *.dfm}

procedure TFmRMIP_R011.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
  //
  function DefineArquivo(): String;
  begin
    Result := Dmod.DefineArquivo(Qr011CIC_ImpCaminho.Value, nil, nil, FMemo1);
  end;
begin
  if VarName = 'ImgNCTExiste' then
    Value := MyObjects.ArquivoExiste(DefineArquivo(), FMemo1)
  else
  if VarName = 'ImgNCTPath' then
    Value := DefineArquivo()
  else
  if VarName = 'VARF_TITULO' then
  begin
    if Qr011MixRelFC.Value >= 1 then
    begin
      if Qr011MixRelNCT.Value >= 1 then
        Value := 'RELAT�RIO DE N�O CONFORMIDADES T�CNICAS E FOTOS COMENTADAS'
      else
        Value := 'RELAT�RIO DE FOTOS COMENTADAS';
    end
    else
      Value := 'RELAT�RIO DE N�O CONFORMIDADES T�CNICAS';
  end
  else
  if VarName = 'VARF_FC_NCT' then
  begin
    if Qr011CIC_ImpAplicacao.Value = 0 then
      Value := 'FOTO COMENTADA'
    else
      Value := 'N�O CONFORMIDADE T�CNICA - N.C.T.';
  end
  else
  if VarName = 'VARF_DADOS_REL' then
  begin
    if Qr011CIC_ImpAplicacao.Value = 0 then
      Value := Qr011CIC_ImpObserv.Value
    else
      Value :=
      'N.C.T.: ' + Qr011CIC_ImpNO_NCT.Value + sLineBreak +
      'C.A.C.: ' + Qr011CIC_ImpTxtCAC.Value + sLineBreak +
      'Status: ' + Qr011CIC_ImpNO_STATUS.Value;
      // + sLineBreak + Qr011CIC_ImpObserv.Value;
  end
  else
end;

procedure TFmRMIP_R011.frxReport011AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport011A, VarName, Value);
end;

procedure TFmRMIP_R011.GeraImp_NCTs;
begin
  //F_Cuns_Img_Cab := '_RMIP_011';
  //ReopenCIC_Imp(QrCunsImgCabCodigo.Value);
  ReopenCIC_Imp(FCliente);
  MyObjects.frxDefineDatasets(frxReport011A, [
  DModG.frxDsDono,
  Dmod.frxDsOpcoesBugs,
  frxDs011CIC_Imp
  ]);
  //
  frxReport011A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport011A.Variables['VARF_DATA']    := FDtaImp;
  frxReport011A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport011A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  frxReport011A.Variables['VARF_PERIODO']  := dmkPF.PeriodoImp(FDtaIni,
    FDtaFim, 0, 0, True, True, False, False, '', '');
  //
  frxReport011A.Variables['VARF_ENTIDADE'] := QuotedStr(FNO_Cliente);
  frxReport011A.Variables['LogoTitRExiste'] := MyObjects.ArquivoExiste(Dmod.QrOpcoesFiliImgTitRelR.Value, FMemo1);
  frxReport011A.Variables['LogoTitRPath'] := Dmod.QrOpcoesFiliImgTitRelR.Value;
  frxReport011A.Variables['LogoTitLExiste'] := MyObjects.ArquivoExiste(Dmod.QrOpcoesFiliImgTitRelL.Value, FMemo1);
  frxReport011A.Variables['LogoTitLPath'] := Dmod.QrOpcoesFiliImgTitRelL.Value;
  //
  //MyObjects.frxPrepara(frxReport011A, 'NCT - CAC');
end;

procedure TFmRMIP_R011.ReopenCIC_Imp(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr011CIC_Imp, DModG.MyPID_DB, [
  'SELECT dep.Nome NO_DEPENDENCIA, cis.Nome NO_STATUS,  ',
  'nct.Nome NO_NCT, nct.TxtCAC, sic.SCompl2 NO_LOCAL, ',
  'stc.Nome NO_STC, cic.*  ',
  'FROM ' + F_Cuns_Img_Cab + ' cic  ',
  'LEFT JOIN ' + TMeuDB + '.cunsimgsit cis ON cis.Codigo=cic.Status  ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=cic.Dependencia ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dep ON dep.Codigo=sid.Dependenci ',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo',
  'LEFT JOIN ' + TMeuDB + '.siaptercad stc ON stc.Codigo=sic.SiapImaTer',
  'LEFT JOIN ' + TMeuDB + '.cunsimgnct nct ON nct.Codigo=cic.CodiNCT ',
  'WHERE cic.Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr011MixRel, DModG.MyPID_DB, [
  'SELECT SUM(IF(Aplicacao=0,1,0)) FC, ',
  'SUM(IF(Aplicacao=1,1,0)) NCT ',
  // 2014-12-22
  //'FROM _cuns_img_cab ',
  'FROM ' + F_Cuns_Img_Cab,
  // FIM 2014-12-22
  '']);
end;

end.
