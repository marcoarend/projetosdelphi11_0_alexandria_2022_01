unit PipRapido;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnBugs_Tabs, UnDmkEnums;

type
  TFmPipRapido = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEquipAplic: TmySQLQuery;
    QrEquipAplicControle: TIntegerField;
    QrEquipAplicNome: TWideStringField;
    DsEquipAplic: TDataSource;
    QrMotDesativ: TmySQLQuery;
    DsMotDesativ: TDataSource;
    QrMotDesativCodigo: TIntegerField;
    QrMotDesativNome: TWideStringField;
    PnGeral: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    TPDtaAquis: TdmkEditDateTimePicker;
    Label2: TLabel;
    EdEquipamento: TdmkEditCB;
    CBEquipamento: TdmkDBLookupComboBox;
    SbEquipAplic: TSpeedButton;
    PnDesativa: TPanel;
    CBMotDesativ: TdmkDBLookupComboBox;
    TPDtaDesativ: TdmkEditDateTimePicker;
    Label3: TLabel;
    EdMotDesativ: TdmkEditCB;
    Label4: TLabel;
    Label5: TLabel;
    EdPrgLstCab: TdmkEditCB;
    CBPrgLstCab: TdmkDBLookupComboBox;
    SbPrgLstCab: TSpeedButton;
    QrPrgLstCab: TmySQLQuery;
    QrPrgLstCabCodigo: TIntegerField;
    QrPrgLstCabNome: TWideStringField;
    DsPrgLstCab: TDataSource;
    DsSiapImaDep: TDataSource;
    QrSiapImaDep: TmySQLQuery;
    QrSiapImaDepNO_DEPENDENCI: TWideStringField;
    QrSiapImaDepControle: TIntegerField;
    Label6: TLabel;
    EdDependenci: TdmkEditCB;
    CBDependenci: TdmkDBLookupComboBox;
    Label11: TLabel;
    EdMotInutili: TdmkEditCB;
    CBMotInutili: TdmkDBLookupComboBox;
    Label10: TLabel;
    TPDtaInutili: TdmkEditDateTimePicker;
    QrMotInutili: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsMotInutili: TDataSource;
    SpeedButton1: TSpeedButton;
    QrEquipAplicNivel1: TIntegerField;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbEquipAplicClick(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SbPrgLstCabClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEntidade: Integer;
    //
    procedure ReopenSiapImaDep(SiapterCad: Integer);
  end;

  var
  FmPipRapido: TFmPipRapido;

implementation

uses UnMyObjects, Module, ModOS, UnGrade_Jan, UMySQLModule, DmkDAC_PF,
UnOSApp_PF, PrgLstCab, Principal;

{$R *.DFM}

procedure TFmPipRapido.BtOKClick(Sender: TObject);
var
  Nome, DtaAquis, DtaDesativ, DtaInutili: String;
  //OSMonCab,
  Codigo, Equipamento, MotDesativ, MotInutili, PrgLstCab, Dependenci,
  OSMonCab: Integer;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma referência!') then Exit;
  //
  PrgLstCab := EdPrgLstCab.ValueVariant;
  if MyObjects.FIC(PrgLstCab = 0, EdPrgLstCab, 'Informe a lista de perguntas!') then
    Exit;
  //
  DtaAquis     := Geral.FDT(TPDtaAquis.Date, 1);
  Equipamento  := EdEquipamento.ValueVariant;
  //OSMonCab     := 0;
  Dependenci   := EdDependenci.ValueVariant;
  //
  DtaDesativ   := Geral.FDT(TPDtaDesativ.Date, 1);
  MotDesativ   := EdMotDesativ.ValueVariant;
  //
  DtaInutili   := Geral.FDT(TPDtaInutili.Date, 1);
  MotInutili   := EdMotInutili.ValueVariant;
  //
  if (MotDesativ <> 0) or (TPDtaDesativ.Date > 1)  then
  begin
    if MyObjects.FIC(MotDesativ = 0, EdMotDesativ,
    'Informe o motivo da desativação!') then
      Exit;
    if MyObjects.FIC(TPDtaDesativ.Date < 2, TPDtaDesativ,
    'Informe a data da desativação!') then
      Exit;
  end;
  //
  if (MotInutili <> 0) or (TPDtaInutili.Date > 1)  then
  begin
    if MyObjects.FIC(MotInutili = 0, EdMotInutili,
    'Informe o motivo da Inutilização!') then
      Exit;
    if MyObjects.FIC(TPDtaInutili.Date < 2, TPDtaInutili,
    'Informe a data da Inutilização!') then
      Exit;
  end;
  if (MotDesativ <> 0) or (MotInutili <> 0) then
  begin
    if Geral.MB_Pergunta(
    'O PMV terá sua dependência e OS de origem zerada! ' + sLineBreak +
    'Deseja continuar assim mesmo?') <> ID_YES then
      Exit;
  end;
  //
  Codigo := EdCodigo.ValueVariant;
  Codigo := UMyMod.BPGS1I32('pipcad', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pipcad', False, [
  'Nome', 'Equipamento', (*'OSMonCab',*)
  'MotDesativ', 'DtaAquis', 'DtaDesativ',
  'PrgLstCab', 'Dependenci',
  'MotInutili', 'DtaInutili'
  ], [
  'Codigo'], [
  Nome, Equipamento, (*OSMonCab,*)
  MotDesativ, DtaAquis, DtaDesativ,
  PrgLstCab, Dependenci,
  MotInutili, DtaInutili], [
  Codigo], True) then
  begin
    //LocCod(Codigo, Codigo);
    if MotDesativ <> 0 then
    begin
      OSMonCab   := 0;
      Dependenci := 0;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False, [
      'OSMonCab', 'Dependenci'
      ], [
      'Codigo'], [
      OSMonCab, Dependenci], [
      Codigo], True);
    end;
    //
    VAR_CADASTRO := Codigo;
    Close;
  end;
end;

procedure TFmPipRapido.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPipRapido.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdNome.CharCase := ecNormal;
end;

procedure TFmPipRapido.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPipRapido.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrPrgLstCab, Dmod.MyDB);

  OSApp_PF.ReopenEquipAplic(QrEquipAplic, gbsMonitora);

  UnDmkDAC_PF.AbreMySQLQuery0(QrMotDesativ, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM motdesativ ',
    'ORDER BY Nome ',
    '']);

  UnDmkDAC_PF.AbreMySQLQuery0(QrMotInutili, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM motdesativ ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmPipRapido.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPipRapido.ReopenSiapImaDep(SiapterCad: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapImaDep, Dmod.MyDB, [
    'SELECT sid.Controle, CONCAT(sic.SCompl2, " => ", dep.Nome) NO_DEPENDENCI ',
    'FROM siapimadep sid ',
    'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
    'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=sic.SiapImaTer ',
    'WHERE stc.Cliente=' + Geral.FF0(SiapTerCad),
    'ORDER BY NO_DEPENDENCI ',
    ' ']);
end;

procedure TFmPipRapido.SbEquipAplicClick(Sender: TObject);
var
  Equipamento, Nivel1: Integer;
begin
  VAR_CADASTRO := 0;
  Equipamento  := EdEquipamento.ValueVariant;

  if Equipamento <> 0 then
    Nivel1 := QrEquipAplicNivel1.Value
  else
    Nivel1 := 0;

  Grade_Jan.MostraFormGraGruN(Nivel1);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEquipamento, CBEquipamento, QrEquipAplic, VAR_CADASTRO, 'Controle');
    EdEquipamento.SetFocus;
  end;
end;

procedure TFmPipRapido.SbPrgLstCabClick(Sender: TObject);
var
  PrgLstCab: Integer;
begin
  VAR_CADASTRO := 0;
  PrgLstCab    := EdPrgLstCab.ValueVariant;

  FmPrincipal.MostraFormPrgLstCab(PrgLstCab, 0);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPrgLstCab, CBPrgLstCab, QrPrgLstCab, VAR_CADASTRO);
    EdPrgLstCab.SetFocus;
  end;
end;

procedure TFmPipRapido.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;

  FmPrincipal.MostraFormCunsCad(False, FEntidade, 0);

  if VAR_CADASTRO <> 0 then
  begin
    ReopenSiapImaDep(FEntidade);
    CBDependenci.SetFocus;
  end;
end;

procedure TFmPipRapido.SpeedButton2Click(Sender: TObject);
var
  MotDesativ: Integer;
begin
  VAR_CADASTRO := 0;
  MotDesativ   := EdMotDesativ.ValueVariant;

  FmPrincipal.MostraMotDesativ(MotDesativ);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdMotDesativ, CBMotDesativ, QrMotDesativ, VAR_CADASTRO);
    EdMotDesativ.SetFocus;
  end;
end;

procedure TFmPipRapido.SpeedButton3Click(Sender: TObject);
var
  MotInutili: Integer;
begin
  VAR_CADASTRO := 0;
  MotInutili   := EdMotInutili.ValueVariant;

  FmPrincipal.MostraMotDesativ(MotInutili);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdMotInutili, CBMotInutili, QrMotInutili, VAR_CADASTRO);
    EdMotInutili.SetFocus;
  end;
end;

end.
