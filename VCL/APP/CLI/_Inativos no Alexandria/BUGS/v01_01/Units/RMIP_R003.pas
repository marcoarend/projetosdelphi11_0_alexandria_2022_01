unit RMIP_R003;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  mySQLDbTables, Data.DB, frxClass, frxDBSet, UnDmkEnums, DmkGeral,
  UnInternalConsts, UnProjGroup_Consts, UnDmkProcFunc, UnMyObjects, frxChart;

type
  TFmRMIP_R003 = class(TForm)
    frxReport003A: TfrxReport;
    Qr003A_PrgLstIts: TmySQLQuery;
    Qr003A_PrgLstItsPrgLstCab: TIntegerField;
    Qr003A_PrgLstItsPrgLstIts: TIntegerField;
    Qr003A_PrgLstItsNO_LST: TWideStringField;
    Qr003A_PrgLstItsSigla: TWideStringField;
    Qr003A_PrgLstItsNome: TWideStringField;
    Qr003A_PrgLstItsFuncoes: TIntegerField;
    Qr003A_PrgLstItsCodigo: TFloatField;
    frxDs003A_PrgLstIts: TfrxDBDataset;
    Qr003A_OSPipIts: TmySQLQuery;
    Qr003A_OSPipItsITENS: TLargeintField;
    Qr003A_OSPipItsRespondido: TSmallintField;
    Qr003A_OSPipItsRespAtrCad: TIntegerField;
    Qr003A_OSPipItsRespAtrIts: TIntegerField;
    Qr003A_OSPipItsResposta: TWideStringField;
    Qr003A_OSPipItsCorPizza: TFloatField;
    Qr003A_OSPipItsDtaExeFim: TDateTimeField;
    frxDs003A_OSPipIts: TfrxDBDataset;
    Qr003A_OSs: TmySQLQuery;
    Qr003A_OSsCodigo: TIntegerField;
    Qr003A_OSsEntidade: TIntegerField;
    Qr003A_OSsSiapTerCad: TIntegerField;
    Qr003A_OSsEstatus: TIntegerField;
    Qr003A_OSsDtaExeFim: TDateTimeField;
    Qr003A_OSsNumContrat: TIntegerField;
    Qr003A_OSsGrupo: TIntegerField;
    Qr003A_OSsNO_FatoGeradr: TWideStringField;
    Qr003A_OSsNO_ESTATUS: TWideStringField;
    Qr003A_OSsNO_SiapTerCad: TWideStringField;
    Qr003A_OSsNO_ENT: TWideStringField;
    frxDs003A_OSs: TfrxDBDataset;
    procedure Qr003A_PrgLstItsAfterScroll(DataSet: TDataSet);
    procedure frxReport003AGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FAplicacao: Integer;
    FAbrangencia: TRelAbrange;
    //
    procedure Reopen003A_PrgLstIts();
    procedure Reopen003A_OSPipIts(Localizador, PrgLstIts, Funcoes: Integer;
              DtaIni, DtaFim: TDateTime; Abrangencia: TRelAbrange; ItemRel: Integer);
  public
    { Public declarations }
    FOSCab, FEmpresa, FCliente, FItemRel: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    //
    procedure GeraImp_PizzaUltimaRevisaoPMV();
    procedure GeraImp_PizzaRevisaoPMVPeriodo();
    procedure frxReport000GetValue(frxReport: TfrxReport;
              const VarName: string; var Value: Variant);
  end;

var
  FmRMIP_R003: TFmRMIP_R003;

implementation

{$R *.dfm}

uses Module, ModuleGeral, ModuleRMIP, DmkDAC_PF;

{ TFmRMIP_R003 }

procedure TFmRMIP_R003.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
var
  Chart1: TfrxChartView;
  Funcoes: Integer;
begin
  if VarName = 'VAR_LINE_TEE_003' then
  begin
    Value := True;
    //
    Chart1 := frxReport.FindObject('Chart003A') as TfrxChartView;
  // Source3 = Cores!
    Funcoes     := Qr003A_PrgLstItsFuncoes.Value;
    if Funcoes in ([CO_PRG_LST_QUANTIT, CO_PRG_LST_BXAPROD, CO_PRG_LST_TXTLIVR]) then
      Chart1.SeriesData.Items[0].Source3 := ''
    else
      Chart1.SeriesData.Items[0].Source3 := 'frxDs003A_OSPipIts."CorPizza"';
  end;
end;

procedure TFmRMIP_R003.frxReport003AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport003A, VarName, Value);
end;

procedure TFmRMIP_R003.GeraImp_PizzaRevisaoPMVPeriodo();
const
  Cliente = 0;
var
  Aviso: String;
begin
  Qr003A_PrgLstIts.Close;
  Aviso := ' ';
  FOSCab := 0;
  FAbrangencia := relAbrangeEmpresa;
  FAplicacao := CO_GeraGrafRelatPMV_0001_COD_PizzaMonitsResp;
  Reopen003A_PrgLstIts();
  //
  if Qr003A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso :=
      'Question�rios dos monitoramentos pesquisados sem perguntas geradoras do gr�fico "' +
      CO_GeraGrafRelatPMV_0001_TXT_PizzaMonitsResp + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Per�odo: ' + Geral.FDT(FDtaIni, 3) + ' a ' + Geral.FDT(FDtaFim, 2);
  //
  frxReport003A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport003A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport003A.Variables['VARF_DATA']    := FDtaImp;
  frxReport003A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport003A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  MyObjects.frxDefineDataSets(frxReport003A, [
    frxDs003A_OSPipIts,
    frxDs003A_PrgLstIts,
    frxDs003A_OSs]);
  frxReport003A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
  //MyObjects.frxPrepara(frxReport003A, CO_GeraGrafRelatPMV_0001_TXT_PizzaMonitsResp);
end;

procedure TFmRMIP_R003.GeraImp_PizzaUltimaRevisaoPMV();
const
  DtaIni = 0;
  DtaFim = 0;
var
  //Qry: TmySQLQuery;
  Aviso: String;
begin
  Qr003A_PrgLstIts.Close;
  Aviso := ' ';
(*
  // Ultimo monitormento finalizado do cliente selecionado!
  FOSCab := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT cab.Codigo, cab.DtaExeFim ',
    'FROM oscab cab ',
    'LEFT JOIN ' + TMeuDB + '.ospipits opi ON opi.Codigo=cab.Codigo ',
    'WHERE cab.Empresa=' + Geral.FF0(FEmpresa),
    'AND cab.Entidade=' + Geral.FF0(FCliente),
    'AND cab.DtaExeFim > "1900-01-01" ',
    'AND cab.Operacao & ' + Geral.FF0(CO_OS_OPERACAO_SERVICO_MONITORAMENTO),
    'AND opi.Codigo IS NOT NULL ',
    /
    'ORDER BY cab.DtaExeFim DESC ',
    'LIMIT 1 ',
    '']);
    //
    FOSCab := Qry.FieldByName('Codigo').AsInteger;
  finally
    Qry.Free;
  end;
  //
  if FOSCab = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Cliente sem monitoramento respondido e finalizado!';
  end;
  *)
  FAplicacao := CO_GeraGrafRelatPMV_0001_COD_PizzaMonitsResp;
  FAbrangencia := relAbrangeCliente;
  Reopen003A_PrgLstIts();
  //
  if Qr003A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Question�rios do monitoramento do localizador ' +
      Geral.FF0(FOSCab) + ' sem perguntas geradoras do gr�fico "' +
      CO_GeraGrafRelatPMV_0001_TXT_PizzaMonitsResp + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Localizador: ' + Geral.FF0(FOSCab);

  frxReport003A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport003A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport003A.Variables['VARF_DATA']    := FDtaImp;
  frxReport003A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport003A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  //frxReport003A.Variables['VARF_OSCAB']   := FOSCab;
  MyObjects.frxDefineDataSets(frxReport003A, [
    frxDs003A_OSPipIts,
    frxDs003A_PrgLstIts,
    frxDs003A_OSs]);
  frxReport003A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
  //MyObjects.frxPrepara(frxReport003A, CO_GeraGrafRelatPMV_0001_TXT_PizzaMonitsResp);
end;

procedure TFmRMIP_R003.Qr003A_PrgLstItsAfterScroll(DataSet: TDataSet);
var
  Localizador, PrgLstIts, Funcoes: Integer;
  Chart1: TfrxChartView;
  Abrangencia: TRelAbrange;
begin
  Localizador := Trunc(Qr003A_PrgLstItsCodigo.Value);
  PrgLstIts   := Qr003A_PrgLstItsPrgLstIts.Value;
  Funcoes     := Qr003A_PrgLstItsFuncoes.Value;
  Abrangencia := DmRMIP.DefineAbrangencia(FCliente);
  Reopen003A_OSPipIts(Localizador, PrgLstIts, Funcoes, FDtaIni, FDtaFim,
  Abrangencia, FItemRel);
  Chart1 := frxReport003A.FindObject('Chart003A') as TfrxChartView;
  // Source3 = Cores!
  if Funcoes in ([CO_PRG_LST_QUANTIT, CO_PRG_LST_BXAPROD, CO_PRG_LST_TXTLIVR]) then
    Chart1.SeriesData.Items[0].Source3 := ''
  else
    Chart1.SeriesData.Items[0].Source3 := 'frxDs003A_OSPipIts."CorPizza"';
end;

procedure TFmRMIP_R003.Reopen003A_OSPipIts(Localizador, PrgLstIts,
  Funcoes: Integer; DtaIni, DtaFim: TDateTime;
  Abrangencia: TRelAbrange; ItemRel: Integer);
var
  Resposta, CorPizza, LeftJoin, OrderBy, GroupBy, SQL_Extra, Xtra_Group,
  SQL_Qtde, SQLPadrao, FldAgrup: String;
begin
  // Evitar mensagens desnecessarias
  if Qr003A_PrgLstIts.RecordCount = 0 then
  begin
    Qr003A_OSPipIts.Close;
    Qr003A_OSPipIts.SQL.Text := '';
    Exit;
  end;
  case TItensRMIP(ItemRel) of
    rmipLinhaEvoluHistoriResp: Xtra_Group := '';
    rmipPizzaMonitsResp: Xtra_Group := ', DtaExeFim';
    else Xtra_Group := ', ???';
  end;
  //
  if not DmRMIP.SQL_Funcoes(PrgLstIts, Funcoes, Resposta, CorPizza, LeftJoin, OrderBy,
  GroupBy, False) then
    Exit;
  //
  if (Abrangencia = relAbrangeCliente)
  and (TItensRMIP(ItemRel) = rmipPizzaMonitsResp) then
    SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(Localizador)
  else //relAbrangeEmpresa
    SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_OSPipIts, Dmod.MyDB, [
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  Resposta,
  CorPizza,
  LeftJoin,
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  GroupBy,
  OrderBy,
  '']);
  //Geral.MB_SQL(self, Qr003A_OSPipIts);
  //


  // OSs afetadas:
  DmRMIP.ReopenOSsAfetadas(Qr003A_OSs, PrgLstIts, LeftJoin, SQL_Extra);
end;

procedure TFmRMIP_R003.Reopen003A_PrgLstIts();
var
  SQL_Codigo, SQL_Extra, SQL_DtExe: String;
begin
  case FAbrangencia of
    relAbrangeCliente:
    begin
      SQL_Codigo := 'opi.Codigo + 0.000';
      SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(FOSCab);
      SQL_DtExe := '0 DtaExeFim, ';
    end;
    relAbrangeEmpresa:
    begin
      SQL_Codigo := '0.000';
      SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', FDtaIni, FDtaFim, True, True);
      SQL_DtExe := '0 DtaExeFim, ';
    end
    else
    begin
      SQL_Codigo := '???';
      SQL_Extra := '? ? ?';
      SQL_DtExe := '??? DtaExeFim, ';
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT ' +
  SQL_DtExe,
  SQL_Codigo + ' Codigo, opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN ' + TMeuDB + '.prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN ' + TMeuDB + '.prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(FAplicacao),
  SQL_Extra,
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
  //
  //Geral.MB_SQL(Self, Qr003A_PrgLstIts);
end;

end.
