unit MovAmovCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkRadioGroup, UnAppListas, dmkImage, UnDmkEnums, UnProjGroup_Consts;

type
  TFmMovAmovCad = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    RGTipo: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenIts(Codigo: Integer);
  public
    { Public declarations }
    FQrCab,
    FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmMovAmovCad: TFmMovAmovCad;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal,
  MyDBCheck, EntiCargos;

{$R *.DFM}

procedure TFmMovAmovCad.BtOKClick(Sender: TObject);
var
  Codigo, Tipo: Integer;
  Nome, Cliente: String;
begin
  Nome    := EdNome.Text;
  Tipo    := RGTipo.ItemIndex;
  Cliente := DBEdCodigo.Text;

  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Defina uma descri��o!') then Exit;

  Codigo := EdCodigo.ValueVariant;
  Codigo := UMyMod.BPGS1I32('movamovcad', 'Codigo', '', '', tsDef, ImgTipo.SQLType, Codigo);

  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'movamovcad', False,
    ['Nome', 'Tipo', 'Cliente'], ['Codigo'],
    [Nome, Tipo, Cliente], [Codigo], True) then
  begin
    ReopenIts(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType       := stIns;
      EdCodigo.ValueVariant := 0;
      RGTipo.ItemIndex      := 0;
      EdNome.ValueVariant   := '';
      EdNome.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmMovAmovCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMovAmovCad.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmMovAmovCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.ConfiguraRadioGroup(RGTipo, sListaTiposMoveis, 5, 0);
end;

procedure TFmMovAmovCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMovAmovCad.ReopenIts(Codigo: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    {
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    }
    FQrIts.Open;
    //
    if Codigo <> 0 then
      FQrIts.Locate('Codigo', Codigo, []);
  end;
end;

end.
