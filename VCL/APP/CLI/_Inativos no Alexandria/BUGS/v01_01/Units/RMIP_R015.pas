unit RMIP_R015;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Data.DB, mySQLDbTables, dmkGeral, UnDmkEnums, frxClass,
  frxDBSet, frxChart, UnProjGroup_Consts, UnInternalConsts, UnMyObjects,
  UnDmkProcFunc, VCLTee.Chart, VCLTee.Series;

type
  TFmRMIP_R015 = class(TForm)
    Qr015A_PrgLstIts: TmySQLQuery;
    frxReport015A: TfrxReport;
    Qr015A_PrgLstItsPrgLstCab: TIntegerField;
    Qr015A_PrgLstItsPrgLstIts: TIntegerField;
    Qr015A_PrgLstItsNO_LST: TWideStringField;
    Qr015A_PrgLstItsSigla: TWideStringField;
    Qr015A_PrgLstItsNome: TWideStringField;
    Qr015A_PrgLstItsFuncoes: TSmallintField;
    Qr015Grupos: TmySQLQuery;
    Qr015NResp: TmySQLQuery;
    IntegerField1: TIntegerField;
    Qr015XVals: TmySQLQuery;
    Qr015Itens: TmySQLQuery;
    Qr015ItensITENS: TFloatField;
    Qr015ItensDtaExeFim: TDateField;
    Qr015XValsDtaExeFim: TDateField;
    Qr015XValsDIAS: TIntegerField;
    Qr015ItensDIAS: TIntegerField;
    Qr015Dados: TmySQLQuery;
    Qr015DadosCorPizza: TIntegerField;
    Qr015GruposResposta: TWideStringField;
    Qr015A_OSs: TmySQLQuery;
    Qr015A_OSsCodigo: TIntegerField;
    Qr015A_OSsEntidade: TIntegerField;
    Qr015A_OSsSiapTerCad: TIntegerField;
    Qr015A_OSsEstatus: TIntegerField;
    Qr015A_OSsDtaExeFim: TDateTimeField;
    Qr015A_OSsNumContrat: TIntegerField;
    Qr015A_OSsGrupo: TIntegerField;
    Qr015A_OSsNO_FatoGeradr: TWideStringField;
    Qr015A_OSsNO_ESTATUS: TWideStringField;
    Qr015A_OSsNO_SiapTerCad: TWideStringField;
    Qr015A_OSsNO_ENT: TWideStringField;
    frxDs015A_OSs: TfrxDBDataset;
    frxDs015A_PrgLstIts: TfrxDBDataset;
    Qr015SubGrupos: TmySQLQuery;
    Qr015SubGruposDependenci: TIntegerField;
    Qr015DadosNO_DEPENDENCI: TWideStringField;
    frxDs015Grupos: TfrxDBDataset;
    procedure FormCreate(Sender: TObject);
    procedure Qr015A_PrgLstItsAfterScroll(DataSet: TDataSet);
    procedure frxReport015AGetValue(const VarName: string; var Value: Variant);
    procedure Qr015SubGruposAfterOpen(DataSet: TDataSet);
    procedure Qr015GruposAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    F_RMIP_015Atr: String;
    //
    function  ObtemNomeArr(Nome: String; Index: Integer): String;
    procedure Reopen004A_PrgLstIts();
    procedure R015_GeraDados(PrgLstIts, Cliente: Integer; DtaIni, DtaFim:
              TDateTime; Funcoes: Integer);
  public
    { Public declarations }
    FCliente, FAplicacao: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL, FAplic_Tit: String;
    FItemRMIP: TItensRMIP;
    //
    procedure GeraImp_LinhaEvolucaoHistorico((*FmRMIP: TForm;*));
    procedure frxReport000GetValue(frxReport: TfrxReport; const VarName: string; var Value: Variant);
  end;

var
  FmRMIP_R015: TFmRMIP_R015;

implementation

uses Module, ModuleRMIP, DmkDAC_PF, ModuleGeral, CreateBugs;

var
  FSeries: Integer = 0;
  FYCount: Integer = 0;
  FNResp: Boolean;
  FXSource: String;
  FXValues: array of String;
  FYValues: array of array of String;
  FTitles: array of String;
  FCores: array of TColor;

{$R *.dfm}

procedure TFmRMIP_R015.FormCreate(Sender: TObject);
var
  I: Integer;
  Chart1: TfrxChartView;
begin
  F_RMIP_015Atr := UnCreateBugs.RecriaTempTableNovo(ntrtt_RMIP_015Atr,
    DmodG.QrUpdPID1, False);
  //
  //////////// Saber (registrar) as mudancas para outros gr�ficos
  Chart1 := frxReport015A.FindObject('Chart015A') as TfrxChartView;
  Chart1.Chart.LeftAxis.Axis.Color := clSilver;
  Chart1.Chart.LeftAxis.Axis.Width := 1;
  Chart1.Chart.LeftAxis.Grid.Color := clSilver;
  Chart1.Chart.LeftAxis.Grid.Width := 1;
  Chart1.Chart.LeftAxis.Grid.SmallDots := True;
  Chart1.Chart.BottomAxis.Axis.Color := clSilver;
  Chart1.Chart.BottomAxis.Axis.Width := 1;
  Chart1.Chart.BottomAxis.Grid.Color := clSilver;
  Chart1.Chart.BottomAxis.Grid.Width := 1;
  Chart1.Chart.BottomAxis.Grid.SmallDots := True;
  Chart1.Chart.Legend.LegendStyle := lsSeries;  // VCLTee.Chart
  // FIM Chart 1
end;

procedure TFmRMIP_R015.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
var
  Chart1: TfrxChartView;
  //
  YSource: String;
  I, J: Integer;
begin
  if VarName = 'VAR_LINE_TEE_015' then
  begin
    Value := True;
    //
    Chart1 := frxReport.FindObject('Chart015A') as TfrxChartView;
    while Chart1.SeriesData.Count > 0 do
      Chart1.SeriesData[0].Free;
    while Chart1.Chart.SeriesCount > 0 do
      Chart1.Chart.Series[0].Free;
    //
    for I := 0 to FSeries - 1 do
    begin
      YSource := '';
      for J := 0 to FYCount - 1 do
      begin
        YSource := YSource + ';' + FYValues[I][J];
      end;
      if Length(YSource) > 0 then
        YSource := Copy(YSource, 2);

      Chart1.AddSeries(TfrxChartSeries.csBar);

      // do VCLTee.Chart.CustomChart:
      TCustomBarSeries(Chart1.Chart.Series[I]).BarPen.Style := psClear; //VCLTee.Series
      TCustomBarSeries(Chart1.Chart.Series[I]).BarPen.Width := 0; //VCLTee.Series
      TCustomBarSeries(Chart1.Chart.Series[I]).Marks.Visible := False; //VCLTee.Series
      Chart1.Chart.Series[I].LegendTitle := FTitles[I];

      // do frxChat
      Chart1.SeriesData[I].DataType := dtFixedData;
      Chart1.SeriesData[I].XSource := FXSource; //'value1; value2; value3';
      Chart1.SeriesData[I].YSource := YSource; //'31.5;28.54;31.58';
      Chart1.SeriesData[I].SortOrder := soNone;
      Chart1.SeriesData[I].TopN := 0;
      Chart1.SeriesData[I].XType := xtText;
    end;
  end;
end;

procedure TFmRMIP_R015.frxReport015AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport015A, VarName, Value);
end;

procedure TFmRMIP_R015.GeraImp_LinhaEvolucaoHistorico();
const
  OSCab = 0;
  Sim = 1;
  //
var
  Aviso: String;
  I, J, PrgLstIts: Integer;
  Qryes: array of TmySQLQuery;
  //Chart1: TfrxChartView;
  //
begin
  Qr015A_PrgLstIts.Close;
  Aviso := ' ';
  Reopen004A_PrgLstIts();
  //
  if Qr015A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso :=
      'Question�rios dos monitoramentos pesquisados sem perguntas geradoras do gr�fico "' +
      FAplic_Tit + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Per�odo: ' + Geral.FDT(FDtaIni, 3) + ' a ' + Geral.FDT(FDtaFim, 2);
  //
  frxReport015A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport015A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport015A.Variables['VARF_DATA']    := FDtaImp;
  frxReport015A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport015A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  //
  MyObjects.frxDefineDataSets(frxReport015A, [
  frxDs015A_PrgLstIts,
  frxDs015A_OSs,
  frxDs015Grupos
  ]);
  //MyObjects.frxPrepara(frxReport015A, FAplic_Tit);
end;

function TFmRMIP_R015.ObtemNomeArr(Nome: String; Index: Integer): String;
begin
  Result := Nome + FormatFloat('00', Index);
end;

procedure TFmRMIP_R015.Qr015SubGruposAfterOpen(DataSet: TDataSet);
  procedure DefineDadosInformados(Item: Integer);
  begin
    Qr015Itens.First;
    while not Qr015Itens.Eof do
    begin
      if Qr015XVals.Locate('DIAS', Qr015ItensDIAS.Value, []) then
      begin
        FYValues[Item][Qr015XVals.RecNo - 1] := Geral.FFI(Qr015ItensITENS.Value);
      end else
      begin
        raise Exception.Create('Erro: Data n�o localizada (1)!');
      end;
      //
      Qr015Itens.Next;
    end;
  end;
var
  I, J, Item: Integer;
begin
  // Manter TODAS datas!
  UnDmkDAC_PF.AbreMySQLQuery0(Qr015XVals, DModG.MyPID_DB, [
  'SELECT DISTINCT DtaExeFim, ',
  'TO_DAYS(DtaExeFim) DIAS ',
  'FROM ' + F_RMIP_015Atr,
  'ORDER BY DtaExeFim ',
  '']);
  FYCount := Qr015XVals.RecordCount;
  SetLength(FXValues, FYCount);
  Qr015XVals.First;
  FXSource := '';
  while not Qr015XVals.Eof do
  begin
    FXSource := FXSource + ';' + Geral.FDT(Qr015XValsDtaExeFim.Value, 3);
    FXValues[Qr015XVals.RecNo - 1] := Geral.FDT(Qr015XValsDtaExeFim.Value, 3);
    //
    Qr015XVals.Next;
  end;
  if Length(FXSource) > 1 then
    FXSource := Copy(FXSource, 2);
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrNResp, DModG.MyPID_DB, [
  'SELECT RespAtrIts ',
  'FROM ' + F_RMIP_015Atr,
  'WHERE Respondido = 0 ',
  //'AND Resposta="' + Qr015Grupos?Resposta.Value + '" ',
  '']);
  FNResp := QrNResp.RecordCount > 0;
  if FNResp then
    FSeries := 1
  else*)
    FSeries := 0;
  FSeries := FSeries + Qr015SubGrupos.RecordCount;
  SetLength(FYValues, FSeries);
  SetLength(FTitles, FSeries);
  SetLength(FCores, FSeries);
  for I := 0 to FSeries - 1 do
  begin
    SetLength(FYValues[I], FYCount);
    for J := 0 to FYCount - 1 do
      FYValues[I][J] := '0';
  end;
  Item := -1;
///////////////////////// Nao respondidos
(*
  if FNResp then
  begin
    Item := Item + 1;
    FTitles[Item] := CO_NAO_RESPONDIDO;
    FCores[Item]  := 8421504; // Cinza
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr015Itens, DModG.MyPID_DB, [
    'SELECT DtaExeFim, SUM(ITENS) + 0.000 ITENS, ',
    'TO_DAYS(DtaExeFim) DIAS ',
    'FROM ' + F_RMIP_015Atr,
    'WHERE Respondido = 0 ',
    'GROUP BY DtaExeFim ',
    'ORDER BY DtaExeFim ',
    '']);
    DefineDadosInformados(Item);
  end;
*)
///////////////////////// Respondidos
  Qr015SubGrupos.First;
  while not Qr015SubGrupos.Eof do
  begin
    Item := Item +1;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qr015Dados, DmodG.MyPID_DB, [
    'SELECT NO_DEPENDENCI, CorPizza ',
    'FROM ' + F_RMIP_015Atr,
    'WHERE Respondido = 1 ',
    'AND Resposta="' + Qr015GruposResposta.Value + '" ',
    'AND Dependenci=' + Geral.FF0(Qr015SubGruposDependenci.Value),
    '']);

    FTitles[Item] := Qr015DadosNO_DEPENDENCI.Value;
    FCores[Item] := Qr015DadosCorPizza.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr015Itens, DModG.MyPID_DB, [
    'SELECT DtaExeFim, SUM(ITENS) + 0.000 ITENS, ',
    'TO_DAYS(DtaExeFim) DIAS ',
    'FROM ' + F_RMIP_015Atr,
    'WHERE Respondido = 1 ',
    'AND Resposta="' + Qr015GruposResposta.Value + '" ',
    'AND Dependenci=' + Geral.FF0(Qr015SubGruposDependenci.Value),
    'GROUP BY DtaExeFim ',
    'ORDER BY DtaExeFim ',
    '']);
    DefineDadosInformados(Item);
    //
    Qr015SubGrupos.Next;
  end;
end;

procedure TFmRMIP_R015.Qr015A_PrgLstItsAfterScroll(DataSet: TDataSet);
var
  PrgLstIts, Funcoes: Integer;
begin
  if Qr015A_PrgLstIts.RecordCount > 0 then
  begin
    PrgLstIts := Qr015A_PrgLstIts.FieldByName('PrgLstIts').AsInteger;
    Funcoes   := Qr015A_PrgLstIts.FieldByName('Funcoes').AsInteger;
    R015_GeraDados(PrgLstIts, FCliente, FDtaIni, FDtaFim, Funcoes);
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qr015Grupos, DmodG.MyPID_DB, [
    'SELECT DISTINCT Resposta ',
    'FROM ' + F_RMIP_015Atr,
    //'WHERE Respondido=' + Geral.FF0(Sim),
    '']);
  end else
  begin
    Qr015Grupos.Close;
    // For�ar erro se o FR tentar abrir
    Qr015Grupos.SQL.Clear;
  end;
end;

procedure TFmRMIP_R015.Qr015GruposAfterScroll(DataSet: TDataSet);
const
  Sim = 1;
begin
  UnDMkDAC_PF.AbreMySQLQuery0(Qr015SubGrupos, DmodG.MyPID_DB, [
  'SELECT DISTINCT Dependenci ',
  'FROM ' + F_RMIP_015Atr,
  'WHERE Respondido=' + Geral.FF0(Sim),
  'AND Resposta="' + Qr015GruposResposta.Value + '" ',
  'ORDER BY NO_DEPENDENCI ',
  '']);
end;

procedure TFmRMIP_R015.R015_GeraDados(PrgLstIts, Cliente: Integer; DtaIni,
  DtaFim: TDateTime; Funcoes: Integer);
var
  SQL_Exec, SQL_Extra, SQL_Qtde, SQLPadrao, FldAgrup,
  Resposta, CorPizza, LeftJoin, GroupBy, OrderBy: String;
begin
  if Cliente <> 0 then
    SQL_Extra := 'AND cab.Entidade=' + Geral.FF0(Cliente)
  else
    SQL_Extra := '';
  //
  SQL_Extra := Geral.ATS([
    SQL_Extra,
    dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True)
  ]);
  if not DmRMIP.SQL_Funcoes(PrgLstIts, Funcoes, Resposta, CorPizza, LeftJoin,
  OrderBy, GroupBy, False) then
    Exit;
  //
  SQL_Exec := Geral.ATS([
  'DELETE FROM ' + F_RMIP_015Atr + '; ',
  'INSERT INTO ' + F_RMIP_015Atr,
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  Resposta,
  CorPizza,
  ', pip.Codigo, pip.Dependenci, sid.Dependenci, dep.Nome ',
  ', 1 Ativo ',
  LeftJoin,
  'LEFT JOIN ' + TMeuDB + '.ospipmon mon ON mon.Controle=opi.Controle ',
  'LEFT JOIN ' + TMeuDB + '.pipcad pip ON pip.Codigo=mon.PipCad ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=pip.Dependenci ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dep ON dep.Codigo=sid.Dependenci ',
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  GroupBy + ', sid.Dependenci',
  '']);
  //if Funcoes = CO_PRG_LST_BINARIO then
    //Geral.MB_Info(SQL_Exec);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL_Exec);

  // OSs afetadas:
  DmRMIP.ReopenOSsAfetadas(Qr015A_OSs, PrgLstIts, LeftJoin, SQL_Extra);
end;

procedure TFmRMIP_R015.Reopen004A_PrgLstIts;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr015A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(FAplicacao),
  dmkPF.SQL_Periodo('AND cab.DtaExeFim', FDtaIni, FDtaFim, True, True),
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
end;

end.
