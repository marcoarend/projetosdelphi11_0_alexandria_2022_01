object FmRMIP_R013: TFmRMIP_R013
  Left = 0
  Top = 0
  Caption = 'RMIP 013'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Qr013Pesq: TmySQLQuery
    Database = Dmod.MyDB
    Left = 56
    Top = 16
    object Qr013PesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object Qr013PesqQuestaoTyp: TIntegerField
      FieldName = 'QuestaoTyp'
      Required = True
    end
    object Qr013PesqQuestaoCod: TIntegerField
      FieldName = 'QuestaoCod'
      Required = True
    end
    object Qr013PesqQuestaoExe: TIntegerField
      FieldName = 'QuestaoExe'
      Required = True
    end
    object Qr013PesqInicio: TDateTimeField
      FieldName = 'Inicio'
      Required = True
    end
    object Qr013PesqTermino: TDateTimeField
      FieldName = 'Termino'
      Required = True
    end
    object Qr013PesqEmprEnti: TIntegerField
      FieldName = 'EmprEnti'
      Required = True
    end
    object Qr013PesqTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object Qr013PesqLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object Qr013PesqCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object Qr013PesqCption: TSmallintField
      FieldName = 'Cption'
      Required = True
    end
    object Qr013PesqNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object Qr013PesqNotas: TWideStringField
      FieldName = 'Notas'
      Required = True
      Size = 255
    end
    object Qr013PesqFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
      Required = True
    end
    object Qr013PesqNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
      Required = True
      Size = 60
    end
    object Qr013PesqNO_QuestaoExe: TWideStringField
      FieldName = 'NO_QuestaoExe'
      Required = True
      Size = 60
    end
    object Qr013PesqNO_Lugar: TWideStringField
      FieldName = 'NO_Lugar'
      Size = 100
    end
    object Qr013PesqNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object frxDs013Pesq: TfrxDBDataset
    UserName = 'frxDs013Pesq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'QuestaoTyp=QuestaoTyp'
      'QuestaoCod=QuestaoCod'
      'QuestaoExe=QuestaoExe'
      'Inicio=Inicio'
      'Termino=Termino'
      'EmprEnti=EmprEnti'
      'Terceiro=Terceiro'
      'Local=Local'
      'Cor=Cor'
      'Cption=Cption'
      'Nome=Nome'
      'Notas=Notas'
      'FatoGeradr=FatoGeradr'
      'NO_FatoGeradr=NO_FatoGeradr'
      'NO_QuestaoExe=NO_QuestaoExe'
      'NO_Lugar=NO_Lugar'
      'NO_ENT=NO_ENT')
    DataSet = Qr013Pesq
    BCDToCurrency = False
    Left = 56
    Top = 64
  end
  object frxReport013A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 41410.866535844910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxReport013AGetValue
    Left = 52
    Top = 112
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDs013Pesq
        DataSetName = 'frxDs013Pesq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object DadosMestre1: TfrxMasterData
        Height = 75.590600000000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDs013Pesq
        DataSetName = 'frxDs013Pesq'
        RowCount = 0
        Stretched = True
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Width = 71.811026060000000000
          Height = 20.787401570000000000
          ShowHint = False
          DataField = 'Inicio'
          DataSet = frxDs013Pesq
          DataSetName = 'frxDs013Pesq'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs013Pesq."Inicio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          Left = 71.811021180000000000
          Width = 71.811026060000000000
          Height = 20.787401570000000000
          ShowHint = False
          DataField = 'Termino'
          DataSet = frxDs013Pesq
          DataSetName = 'frxDs013Pesq'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs013Pesq."Termino"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 143.622042360000000000
          Width = 268.346454250000000000
          Height = 20.787401570000000000
          ShowHint = False
          DataField = 'NO_Lugar'
          DataSet = frxDs013Pesq
          DataSetName = 'frxDs013Pesq'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs013Pesq."NO_Lugar"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 0.000048820000000000
          Top = 20.787404020000000000
          Width = 226.771738980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_QuestaoExe'
          DataSet = frxDs013Pesq
          DataSetName = 'frxDs013Pesq'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs013Pesq."NO_QuestaoExe"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 226.881928820000000000
          Top = 20.787404020000000000
          Width = 453.543538980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_FatoGeradr'
          DataSet = frxDs013Pesq
          DataSetName = 'frxDs013Pesq'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs013Pesq."NO_FatoGeradr"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 3.779578820000000000
          Top = 37.795278029999990000
          Width = 672.756278980000000000
          Height = 37.795278030000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataSet = frxDs013Pesq
          DataSetName = 'frxDs013Pesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[VARF_DESCRI_ATIVI]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 411.968818820000000000
          Width = 268.346454250000000000
          Height = 20.787401570000000000
          ShowHint = False
          DataField = 'NO_ENT'
          DataSet = frxDs013Pesq
          DataSetName = 'frxDs013Pesq'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs013Pesq."NO_ENT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 120.944884330000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PARTICIPANTE]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 415.748300000000000000
          Top = 64.252010000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Top = 64.252010000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa: [VARF_NO_EMPRESA]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Top = 83.149660000000000000
          Width = 71.811026060000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'In'#237'cio')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 71.811021180000000000
          Top = 83.149660000000000000
          Width = 71.811026060000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'T'#233'rmino')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 143.622042360000000000
          Top = 83.149660000000000000
          Width = 268.346454250000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Local')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Top = 100.157480310000000000
          Width = 226.771738980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Atividade')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 226.881928820000000000
          Top = 100.157480310000000000
          Width = 453.543538980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pauta')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 411.968770000000000000
          Top = 83.149660000000000000
          Width = 268.346454250000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        Height = 3.779530000000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        RowCount = 1
      end
    end
  end
end
