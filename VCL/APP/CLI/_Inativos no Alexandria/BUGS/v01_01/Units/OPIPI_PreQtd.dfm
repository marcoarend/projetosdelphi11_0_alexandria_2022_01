object FmOPIPI_PreQtd: TFmOPIPI_PreQtd
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-034 :: Pr'#233'-defini'#231#227'o de Loops'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 278
        Height = 32
        Caption = 'Pr'#233'-defini'#231#227'o de Loops'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 278
        Height = 32
        Caption = 'Pr'#233'-defini'#231#227'o de Loops'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 278
        Height = 32
        Caption = 'Pr'#233'-defini'#231#227'o de Loops'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 808
          Height = 450
          Align = alClient
          DataSource = DsOPIPI_PreQtd
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Conta'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PIP'
              Title.Caption = 'Nome PMV'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Pergunta'
              Title.Caption = 'Pergunta'
              Width = 552
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RespQtd'
              Title.Caption = 'Loops'
              Width = 40
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object TbOPIPI_PreQtd: TmySQLTable
    Database = Dmod.ZZDB
    BeforePost = TbOPIPI_PreQtdBeforePost
    AfterPost = TbOPIPI_PreQtdAfterPost
    TableName = '_opipi_preqtd'
    Left = 72
    Top = 104
    object TbOPIPI_PreQtdConta: TLargeintField
      FieldName = 'Conta'
      ReadOnly = True
    end
    object TbOPIPI_PreQtdNO_PIP: TWideStringField
      FieldName = 'NO_PIP'
      ReadOnly = True
      Size = 30
    end
    object TbOPIPI_PreQtdNO_Pergunta: TWideStringField
      FieldName = 'NO_Pergunta'
      ReadOnly = True
      Size = 100
    end
    object TbOPIPI_PreQtdRespQtd: TIntegerField
      FieldName = 'RespQtd'
    end
    object TbOPIPI_PreQtdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbOPIPI_PreQtdFiliacao: TIntegerField
      FieldName = 'Filiacao'
    end
    object TbOPIPI_PreQtdPipCad: TIntegerField
      FieldName = 'PipCad'
    end
    object TbOPIPI_PreQtdPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
  end
  object DsOPIPI_PreQtd: TDataSource
    DataSet = TbOPIPI_PreQtd
    Left = 72
    Top = 152
  end
  object QrOSPipIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT opi.*'
      'FROM ospipits opi '
      'LEFT JOIN ospipmon  opm ON opm.Controle=opi.Controle '
      'LEFT JOIN pipcad    pip ON pip.Codigo=opm.PipCad  '
      'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta '
      'WHERE opi.Codigo=817 '
      'AND opi.Filiacao=17'
      'AND opm.PipCad=30'
      'AND opi.LupSeqRsp=0'
      ''
      'ORDER BY opi.Controle, opi.SobreOrd, opi.Ordem, opi.SubOrdem')
    Left = 280
    Top = 264
    object QrOSPipItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPipItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSPipItsConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrOSPipItsPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrOSPipItsPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
    object QrOSPipItsSobreOrd: TIntegerField
      FieldName = 'SobreOrd'
    end
    object QrOSPipItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSPipItsSubOrdem: TIntegerField
      FieldName = 'SubOrdem'
    end
    object QrOSPipItsTabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrOSPipItsTabIdx: TLargeintField
      FieldName = 'TabIdx'
    end
    object QrOSPipItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOSPipItsFuncoes: TSmallintField
      FieldName = 'Funcoes'
    end
    object QrOSPipItsDependente: TSmallintField
      FieldName = 'Dependente'
    end
    object QrOSPipItsPrgAtrCad: TIntegerField
      FieldName = 'PrgAtrCad'
    end
    object QrOSPipItsFiliacao: TIntegerField
      FieldName = 'Filiacao'
    end
    object QrOSPipItsRelacao: TIntegerField
      FieldName = 'Relacao'
    end
    object QrOSPipItsNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrOSPipItsNivSeq: TIntegerField
      FieldName = 'NivSeq'
    end
    object QrOSPipItsPergunta: TIntegerField
      FieldName = 'Pergunta'
    end
    object QrOSPipItsBinarCad0: TIntegerField
      FieldName = 'BinarCad0'
    end
    object QrOSPipItsBinarCad1: TIntegerField
      FieldName = 'BinarCad1'
    end
    object QrOSPipItsAcaoPrd: TSmallintField
      FieldName = 'AcaoPrd'
    end
    object QrOSPipItsRespondido: TSmallintField
      FieldName = 'Respondido'
    end
    object QrOSPipItsRespBin: TSmallintField
      FieldName = 'RespBin'
    end
    object QrOSPipItsRespQtd: TFloatField
      FieldName = 'RespQtd'
    end
    object QrOSPipItsRespAtrCad: TIntegerField
      FieldName = 'RespAtrCad'
    end
    object QrOSPipItsRespAtrIts: TIntegerField
      FieldName = 'RespAtrIts'
    end
    object QrOSPipItsRespAtrTxt: TWideStringField
      FieldName = 'RespAtrTxt'
      Size = 60
    end
    object QrOSPipItsRespTxtLvr: TWideMemoField
      FieldName = 'RespTxtLvr'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSPipItsRespPrdBin: TSmallintField
      FieldName = 'RespPrdBin'
    end
    object QrOSPipItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSPipItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSPipItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSPipItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSPipItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSPipItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSPipItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSPipItsLupForma: TSmallintField
      FieldName = 'LupForma'
    end
    object QrOSPipItsLupQtdVzs: TSmallintField
      FieldName = 'LupQtdVzs'
    end
    object QrOSPipItsLupSeqRsp: TIntegerField
      FieldName = 'LupSeqRsp'
    end
    object QrOSPipItsLupInfVzs: TSmallintField
      FieldName = 'LupInfVzs'
    end
    object QrOSPipItsRespMetodo: TSmallintField
      FieldName = 'RespMetodo'
    end
    object QrOSPipItsSuperOrd: TIntegerField
      FieldName = 'SuperOrd'
    end
  end
end
