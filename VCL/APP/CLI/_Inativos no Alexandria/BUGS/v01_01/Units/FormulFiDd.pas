unit FormulFiDd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFormulFiDd = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdOrdem: TdmkEdit;
    Label1: TLabel;
    EdDias: TdmkEdit;
    DbEdControle: TdmkDBEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFormulFiDD(Conta: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFormulFiDd: TFmFormulFiDd;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
Formulas;

{$R *.DFM}

procedure TFmFormulFiDd.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, Ordem, Dias: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := EdConta.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  Dias           := EdDias.ValueVariant;
  //
  if MyObjects.FIC(Dias < 1, EdDias, 'Informe o intervalo em dias!') then
    Exit;
  Conta := UMyMod.BPGS1I32('formulfidd', 'Conta', '', '', tsPos, ImgTipo.SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'formulfidd', False, [
  'Codigo', 'Controle',
  'Ordem', 'Dias'], [
  'Conta'], [
  Codigo, Controle,
  Ordem, Dias], [
  Conta], True) then
  begin
    ReopenFormulFiDd(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdConta.ValueVariant     := 0;
      EdOrdem.ValueVariant     := FmFormulas.ProximaOrdem();
      EdDias.ValueVariant      := 0;
      //
      EdOrdem.SetFocus;
    end else Close;
  end;
end;

procedure TFmFormulFiDd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulFiDd.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdControle.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFormulFiDd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmFormulFiDd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulFiDd.ReopenFormulFiDd(Conta: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Conta <> 0 then
      FQrIts.Locate('Conta', Conta, []);
  end;
end;

end.
