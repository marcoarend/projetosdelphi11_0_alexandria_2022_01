unit PrgLstIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, dmkCheckGroup, dmkRadioGroup, UnBugs_Tabs, UnDmkEnums,
  UnProjGroup_Consts, Vcl.ComCtrls;

type
  TFmPrgLstIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrPrgAtrCad: TmySQLQuery;
    DsPrgAtrCad: TDataSource;
    VUPrgAtrCad: TdmkValUsu;
    QrPrgAtrCadCodigo: TIntegerField;
    QrPrgAtrCadCodUsu: TIntegerField;
    QrPrgAtrCadNome: TWideStringField;
    RGFuncoes: TdmkRadioGroup;
    RGDependente: TdmkRadioGroup;
    EdOrdem: TdmkEdit;
    Label9: TLabel;
    Label4: TLabel;
    EdFiliacao: TdmkEditCB;
    CBFiliacao: TdmkDBLookupComboBox;
    QrPrgLstPai: TmySQLQuery;
    DsPrgLstPai: TDataSource;
    QrPrgLstPaiControle: TIntegerField;
    RGRelacao: TdmkRadioGroup;
    EdPergunta: TdmkEditCB;
    CBPergunta: TdmkDBLookupComboBox;
    SbPrgCadPrg: TSpeedButton;
    QrPrgCadPrg: TmySQLQuery;
    DsPrgCadPrg: TDataSource;
    QrPrgCadPrgCodigo: TIntegerField;
    QrPrgCadPrgNome: TWideStringField;
    QrPrgLstPaiNO_PERGUNTA: TWideStringField;
    QrPrgBinCad0: TmySQLQuery;
    DsPrgBinCad0: TDataSource;
    QrPrgBinCad1: TmySQLQuery;
    DsPrgBinCad1: TDataSource;
    QrPrgBinCad0Codigo: TIntegerField;
    QrPrgBinCad0Nome: TWideStringField;
    QrPrgBinCad1Codigo: TIntegerField;
    QrPrgBinCad1Nome: TWideStringField;
    GroupBox4: TGroupBox;
    RGLupForma: TdmkRadioGroup;
    Label10: TLabel;
    EdLupQtdVzs: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PCFuncao: TPageControl;
    TabSheet3: TTabSheet;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label8: TLabel;
    SbBinarCad0: TSpeedButton;
    SbBinarCad1: TSpeedButton;
    EdBinarCad0: TdmkEditCB;
    CBBinarCad0: TdmkDBLookupComboBox;
    EdBinarCad1: TdmkEditCB;
    CBBinarCad1: TdmkDBLookupComboBox;
    TabSheet4: TTabSheet;
    RGAcaoPrd: TdmkRadioGroup;
    TabSheet5: TTabSheet;
    Label1: TLabel;
    SBPrgAtrCad: TSpeedButton;
    EdPrgAtrCad: TdmkEditCB;
    CBPrgAtrCad: TdmkDBLookupComboBox;
    TabSheet6: TTabSheet;
    RGCasasQtde: TdmkRadioGroup;
    TabSheet2: TTabSheet;
    Label11: TLabel;
    CBCorPie: TColorBox;
    CGAplicacao: TdmkCheckGroup;
    TabSheet7: TTabSheet;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBPrgAtrCadClick(Sender: TObject);
    procedure SbPrgCadPrgClick(Sender: TObject);
    procedure SbBinarCad0Click(Sender: TObject);
    procedure SbBinarCad1Click(Sender: TObject);
    procedure RGFuncoesClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPrgLstIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure ConfiguraFuncao(Funcao: Integer);
    procedure ReopenPrgLstPai(Codigo: Integer);
  end;

  var
  FmPrgLstIts: TFmPrgLstIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, UnAppListas,
DmkDAC_PF, CfgAtributos, PrgLstCab, ModOS, CfgCadLista;

{$R *.DFM}

procedure TFmPrgLstIts.BtOKClick(Sender: TObject);
const
  Nome = '';
  Binario0 = '';
  Binario1 = '';
var
  BinarCad0, BinarCad1: String;
  Codigo, Controle, Funcoes, Dependente, PrgAtrCad, Ordem, Filiacao, CasasQtde,
  Relacao, Pergunta, AcaoPrd, LupForma, LupQtdVzs, Aplicacao, CorPie: Integer;
  SemAtributo, SemAcaoProd, ManutProdut, SemQtdVzs, SemLupFrm: Boolean;
begin
  Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
  Funcoes        := RGFuncoes.ItemIndex;
  Dependente     := RGDependente.ItemIndex;
  BinarCad0      := EdBinarCad0.ValueVariant;
  BinarCad1      := EdBinarCad1.ValueVariant;
  PrgAtrCad      := EdPrgAtrCad.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  Filiacao       := EdFiliacao.ValueVariant;
  Relacao        := RGRelacao.ItemIndex;
  Pergunta       := EdPergunta.ValueVariant;
  AcaoPrd        := RGAcaoPrd.ItemIndex;
  LupForma       := RGLupForma.ItemIndex;
  LupQtdVzs      := EdLupQtdVzs.ValueVariant;
  Aplicacao      := CGAplicacao.Value;
  CorPie         := CBCorPie.Selected;
  CasasQtde      := RGCasasQtde.ItemIndex;
  //
  SemAtributo := (PrgAtrCad = 0) and (Funcoes = CO_PRG_LST_ATRIBUT); // = 4
  SemAcaoProd := (AcaoPrd = 0) and (Funcoes = CO_PRG_LST_BXAPROD);   // = 3
  //ManutProdut := (Relacao <> 2) and (Funcoes = 3);
  SemQtdVzs := (LupForma = CO_LupForma_COD_PorLoop) and (LupQtdVzs < 2);
  //
  case LupForma of
    CO_LupForma_COD_NaoTem: SemLupFrm := False;
    CO_LupForma_COD_Binario: SemLupFrm := Funcoes <> CO_PRG_LST_BINARIO;
    CO_LupForma_COD_PorLoop: SemLupFrm := Funcoes <> CO_PRG_LST_QUANTIT;
    else SemLupFrm := True;
  end;

  if MyObjects.FIC(Pergunta = 0, EdPergunta, 'Informe uma Pergunta!') then
    Exit;
  //
  if MyObjects.FIC(SemQtdVzs, EdLupQtdVzs, 'Informe a quantidade de loops (deve ser no m�nimo 2)!') then
    Exit;
  //
  if MyObjects.FIC(SemLupFrm, RGLupForma,
    'A forma de gera��o de descendentes deve ser relacionado a fun��o da pergunta!') then
    Exit;
  //
  if SemAtributo then
  begin
    PCFuncao.ActivePageIndex := 3;
    //
    Geral.MB_Aviso('Informe uma Tipo de Atributo!');
    EdPrgAtrCad.SetFocus;
    Exit;
  end;
  {
  if MyObjects.FIC(ManutProdut, RGFuncoes,
  '" Fun��o desta pergunta" n�o combina com "Pergunta relacionada a"!') then
    Exit;
  }
  if MyObjects.FIC(Relacao=0, RGRelacao, 'Informe o item "' + RGRelacao.Caption + '"!') then
    Exit;
  //
  if SemAcaoProd then
  begin
    PCFuncao.ActivePageIndex := 2;
    //
    Geral.MB_Aviso('Informe o item obrigat�rio para a fun��o "Adi/reti. Produto"!');
    RGAcaoPrd.SetFocus;
    Exit;
  end;
  //
  Controle := EdControle.ValueVariant;
  Controle := UMyMod.BPGS1I32('prglstits', 'Controle', '', '', tsPos,
                ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'prglstits', False, [
    'Codigo', 'Nome', 'Funcoes',
    'Dependente', 'BinarCad0', 'BinarCad1',
    'PrgAtrCad', 'Ordem', 'Filiacao',
    'Relacao' (*,'Nivel'*), 'Pergunta',
    'AcaoPrd', 'LupForma', 'LupQtdVzs',
    'Aplicacao', 'CorPie', 'CasasQtde',
    'Binario0', 'Binario1'], [
    'Controle'], [
    Codigo, Nome, Funcoes,
    Dependente, BinarCad0, BinarCad1,
    PrgAtrCad, Ordem, Filiacao,
    Relacao (*,Nivel*), Pergunta,
    AcaoPrd, LupForma, LupQtdVzs,
    Aplicacao, CorPie, CasasQtde,
    Binario0, Binario1
    ], [
    Controle], True) then
  begin
    FmPrgLstCab.AtualizarNivSeq(Codigo);
    ReopenPrgLstIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      RGFuncoes.ItemIndex      := 0;
      RGDependente.ItemIndex   := 0;
      EdBinarCad0.ValueVariant := 0;
      CBBinarCad0.KeyValue     := 0;
      EdBinarCad1.ValueVariant := 0;
      CBBinarCad1.KeyValue     := 0;
      EdPrgAtrCad.ValueVariant := 0;
      CBPrgAtrCad.KeyValue     := Null;
      EdPergunta.ValueVariant  := '';
      CBPergunta.KeyValue      := Null;
      EdOrdem.ValueVariant     := EdOrdem.ValueVariant + 1;
      RGAcaoPrd.ItemIndex      := 0;
      RGLupForma.ItemIndex     := 0;
      EdLupQtdVzs.ValueVariant := 0;
      CGAplicacao.Value        := 0;
      CBCorPie.Selected        := 0;
      //
      EdFiliacao.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmPrgLstIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrgLstIts.ConfiguraFuncao(Funcao: Integer);
begin
  PageControl1.ActivePageIndex := 0;
  //
  case Funcao of
    CO_PRG_LST_BINARIO:
    begin
      PCFuncao.ActivePageIndex := 0;
      //
      if EdBinarCad0.ValueVariant = 0 then
      begin
        EdBinarCad0.ValueVariant := -1;
        CBBinarCad0.KeyValue     := -1;
      end;
      if EdBinarCad1.ValueVariant = 0 then
      begin
        EdBinarCad1.ValueVariant := -2;
        CBBinarCad1.KeyValue     := -2;
      end;
    end;
    CO_PRG_LST_QUANTIT:
      PCFuncao.ActivePageIndex := 1;
    CO_PRG_LST_BXAPROD:
      PCFuncao.ActivePageIndex := 2;
    CO_PRG_LST_ATRIBUT:
      PCFuncao.ActivePageIndex := 3;
    CO_PRG_LST_TXTLIVR:
      PCFuncao.ActivePageIndex := 4;
  end;
end;

procedure TFmPrgLstIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmPrgLstIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGFuncoes, sListaFuncaoPrgAtrCab, 2, -1);
  MyObjects.ConfiguraRadioGroup(RGRelacao, sListaPrgLstItsRelacao, 3, 0);
  MyObjects.ConfiguraCheckGroup(CGAplicacao, sListaGeraGrafRelatPMV, 3, 0, False);
  UnDmkDAC_PF.AbreQuery(QrPrgAtrCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrgCadPrg, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrgBinCad0, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrgBinCad1, Dmod.MyDB);
end;

procedure TFmPrgLstIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrgLstIts.ReopenPrgLstIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmPrgLstIts.ReopenPrgLstPai(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrgLstPai, Dmod.MyDB, [
    'SELECT pli.Controle, pcp.Nome NO_PERGUNTA',
    'FROM prglstits pli',
    'LEFT JOIN prgcadprg pcp ON pcp.Codigo=pli.Pergunta',
    'WHERE pli.Codigo=' + Geral.FF0(Codigo),
    //'AND pli.Funcoes=' + Geral.FF0(CO_PRG_LST_BINARIO),
    'AND pli.LupForma > 0 ',
    'ORDER BY NO_PERGUNTA ',
    '']);
end;

procedure TFmPrgLstIts.RGFuncoesClick(Sender: TObject);
begin
  ConfiguraFuncao(RGFuncoes.ItemIndex);
end;

procedure TFmPrgLstIts.SbBinarCad0Click(Sender: TObject);
var
  BinarCad0: Integer;
begin
  VAR_CADASTRO := 0;
  BinarCad0    := EdBinarCad0.ValueVariant;

  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'PrgBinCad', 60, ncGerlSeq1,
    'Textos Bin�rios', [], False, Null, [], [], False, BinarCad0);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdBinarCad0, CBBinarCad0, QrPrgBinCad0, VAR_CADASTRO);
    UnDmkDAC_PF.AbreQuery(QrPrgBinCad1, Dmod.MyDB);
    EdBinarCad0.SetFocus;
  end;
end;

procedure TFmPrgLstIts.SbBinarCad1Click(Sender: TObject);
var
  BinarCad1: Integer;
begin
  VAR_CADASTRO := 0;
  BinarCad1    := EdBinarCad1.ValueVariant;

  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'PrgBinCad', 60, ncGerlSeq1,
    'Textos Bin�rios', [], False, Null, [], [], False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdBinarCad1, CBBinarCad1, QrPrgBinCad1, VAR_CADASTRO);
    UnDmkDAC_PF.AbreQuery(QrPrgBinCad0, Dmod.MyDB);
    EdBinarCad1.SetFocus;
  end;
end;

procedure TFmPrgLstIts.SBPrgAtrCadClick(Sender: TObject);
const
  QrAtrIts = nil;
  QrCad    = nil;
  QrIts    = nil;
  EdAtrCad = nil;
  EdAtrIts = nil;
  CBAtrCad = nil;
  CBAtrIts = nil;
var
  PrgAtrCad: Integer;
begin
  VAR_CADASTRO := 0;
  PrgAtrCad    := EdPrgAtrCad.ValueVariant;

  UnCfgAtributos.CadastraAtrCad(tiptbCAB, 'PrgAtrCad', 'PrgAtrIts', '',
    QrPrgAtrCad, QrAtrIts, QrCad, QrIts, EdAtrCad, EdAtrIts, CBAtrCad, CBAtrIts,
    False, PrgAtrCad);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPrgAtrCad, CBPrgAtrCad, QrPrgAtrCad, VAR_CADASTRO);
    EdPrgAtrCad.SetFocus;
  end;
end;

procedure TFmPrgLstIts.SbPrgCadPrgClick(Sender: TObject);
var
  Pergunta: Integer;
begin
  VAR_CADASTRO := 0;
  Pergunta     := EdPergunta.ValueVariant;

  UnCfgCadLista.MostraCadRegistroSimples(Dmod.MyDB, DmModOS.TbPrgCadPrg,
    DmModOS.DsPrgCadPrg, ncGerlSeq1, 'Cadastro de Perguntas', Pergunta);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPergunta, CBPergunta, QrPrgCadPrg, VAR_CADASTRO);
    EdPergunta.SetFocus;
  end;
end;

end.
