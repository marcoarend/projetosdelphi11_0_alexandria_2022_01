object FmOSSrvPre: TFmOSSrvPre
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-039 :: Ordem de Servi'#231'o - Adi'#231#227'o por Pr'#233'-servi'#231'o'
  ClientHeight = 261
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 634
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 586
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 538
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 516
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Adi'#231#227'o por Pr'#233'-servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 516
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Adi'#231#227'o por Pr'#233'-servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 516
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Adi'#231#227'o por Pr'#233'-servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 634
    Height = 99
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 634
      Height = 99
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 634
        Height = 99
        Align = alClient
        TabOrder = 0
        object Label2: TLabel
          Left = 20
          Top = 8
          Width = 39
          Height = 13
          Caption = 'Servi'#231'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label1: TLabel
          Left = 20
          Top = 48
          Width = 65
          Height = 13
          Caption = 'Servi'#231'o base:'
          Color = clBtnFace
          ParentColor = False
        end
        object SpeedButton6: TSpeedButton
          Left = 599
          Top = 24
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton6Click
        end
        object EdDesServico: TdmkEditCB
          Left = 20
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdDesServicoChange
          OnEnter = EdDesServicoEnter
          OnExit = EdDesServicoExit
          DBLookupComboBox = CBDesServico
          IgnoraDBLookupComboBox = False
        end
        object CBDesServico: TdmkDBLookupComboBox
          Left = 76
          Top = 24
          Width = 520
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsDesServico
          TabOrder = 1
          dmkEditCB = EdDesServico
          QryCampo = 'DesServico'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPreSrv: TdmkEditCB
          Left = 20
          Top = 64
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPreSrv
          IgnoraDBLookupComboBox = False
        end
        object CBPreSrv: TdmkDBLookupComboBox
          Left = 76
          Top = 64
          Width = 544
          Height = 21
          KeyField = 'Controle'
          ListField = 'Nome'
          ListSource = DsPreSrv
          TabOrder = 3
          dmkEditCB = EdPreSrv
          QryCampo = 'DesServico'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 147
    Width = 634
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 630
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 191
    Width = 634
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 488
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 486
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrDesServico: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM desservico'
      'ORDER BY Nome')
    Left = 92
    Top = 48
    object QrDesServicoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDesServicoNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsDesServico: TDataSource
    DataSet = QrDesServico
    Left = 92
    Top = 92
  end
  object QrPreSrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM presrv'
      'WHERE Codigo=0'
      '')
    Left = 160
    Top = 48
    object QrPreSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPreSrvNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrPreSrvGarantiaDd: TIntegerField
      FieldName = 'GarantiaDd'
    end
    object QrPreSrvHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
    end
    object QrPreSrvHrExecutar: TFloatField
      FieldName = 'HrExecutar'
    end
    object QrPreSrvValCalc: TFloatField
      FieldName = 'ValCalc'
    end
    object QrPreSrvValInfo: TFloatField
      FieldName = 'ValInfo'
    end
    object QrPreSrvValDesc: TFloatField
      FieldName = 'ValDesc'
    end
    object QrPreSrvValTota: TFloatField
      FieldName = 'ValTota'
    end
    object QrPreSrvDetalhes: TWideMemoField
      FieldName = 'Detalhes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPreSrvMoniDdTotl: TIntegerField
      FieldName = 'MoniDdTotl'
    end
    object QrPreSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPreSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPreSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPreSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPreSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPreSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPreSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPreSrvMoniDdIntv: TIntegerField
      FieldName = 'MoniDdIntv'
    end
  end
  object DsPreSrv: TDataSource
    DataSet = QrPreSrv
    Left = 160
    Top = 92
  end
  object QrPreAlv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prc.Nome NO_Praga_Z, osa.*'
      'FROM osalv osa'
      'LEFT JOIN praga_z prc ON prc.Codigo=osa.Praga_Z'
      'WHERE osa.Controle=:P0')
    Left = 268
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPreAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
      Origin = 'osalv.Praga_Z'
    end
  end
  object QrFormulas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frm.*'
      'FROM prefrm prf '
      'LEFT JOIN formulas frm ON frm.Codigo=prf.Formula '
      'WHERE prf.Controle=3')
    Left = 268
    Top = 96
    object QrFormulasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFormulasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFormulasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulasEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrFormulasQtdTot: TFloatField
      FieldName = 'QtdTot'
    end
    object QrFormulasQtdQSP: TFloatField
      FieldName = 'QtdQSP'
    end
    object QrFormulasCusTot: TFloatField
      FieldName = 'CusTot'
    end
    object QrFormulasAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrFormulasDiluente: TSmallintField
      FieldName = 'Diluente'
    end
    object QrFormulasTipoAplica: TIntegerField
      FieldName = 'TipoAplica'
    end
    object QrFormulasUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
  end
  object QrPreMon: TmySQLQuery
    Database = Dmod.MyDB
    Left = 268
    Top = 144
    object QrPreMonCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreMonControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPreMonConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPreMonFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrPreMonLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPreMonDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPreMonDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPreMonUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPreMonUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPreMonAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPreMonAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPreMonNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
  end
end
