unit OSRapida;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  frxClass, frxDBSet;

type
  TFmOSRapida = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrExecucoes: TmySQLQuery;
    QrExecucoesCodigo: TIntegerField;
    QrExecucoesInicio: TDateTimeField;
    QrExecucoesFim: TDateTimeField;
    QrExecucoesEscalonamento: TFloatField;
    QrExecucoesDIAS: TFloatField;
    frxDsExecucoes: TfrxDBDataset;
    frxExecucoes: TfrxReport;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //
  end;

  var
  FmOSRapida: TFmOSRapida;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmOSRapida.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSRapida.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSRapida.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSRapida.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
