object FmRMIP_R006: TFmRMIP_R006
  Left = 0
  Top = 0
  Caption = 'OK'
  ClientHeight = 539
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Qr006A_PrgLstIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = Qr006A_PrgLstItsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts,  '
      'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , '
      'plp.Sigla, plp.Nome, pli.Funcoes '
      'FROM ospipits opi '
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo '
      'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab '
      'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts '
      'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta '
      'WHERE pli.Aplicacao & 2'
      'AND cab.DtaExeFim BETWEEN "2013-09-09" AND "2014-03-08 23:59:59"'
      'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ')
    Left = 196
    Top = 52
    object Qr006A_PrgLstItsPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object Qr006A_PrgLstItsPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
    object Qr006A_PrgLstItsNO_LST: TWideStringField
      FieldName = 'NO_LST'
      Size = 255
    end
    object Qr006A_PrgLstItsSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 100
    end
    object Qr006A_PrgLstItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Qr006A_PrgLstItsFuncoes: TSmallintField
      FieldName = 'Funcoes'
    end
  end
  object frxReport006A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Chart1OnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  OK: Boolean;                                      '
      'begin'
      '  OK := <VAR_LINE_TEE_006>;                    '
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxReport006AGetValue
    Left = 196
    Top = 4
    Datasets = <
      item
        DataSet = frxDs006A_OSs
        DataSetName = 'frxDs006A_OSs'
      end
      item
        DataSet = frxDs006A_PrgLstIts
        DataSetName = 'frxDs006A_PrgLstIts'
      end
      item
        DataSet = frxDs006Grupos
        DataSetName = 'frxDs006Grupos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        Height = 124.724463150000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 60.472480000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 41.574803150000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 83.149660000000000000
          Width = 680.315400000000000000
          Height = 41.574803150000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVISO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        Height = 37.795300000000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDs006A_PrgLstIts
        DataSetName = 'frxDs006A_PrgLstIts'
        KeepTogether = True
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_LST'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs006A_PrgLstIts."NO_LST"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Nome'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs006A_PrgLstIts."Nome"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 748.346940000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 37.795300000000000000
        Top = 574.488560000000000000
        Width = 680.315400000000000000
        object Memo6: TfrxMemoView
          Top = 18.897649999999880000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 139.842610000000000000
          Top = 18.897649999999880000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lugar')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 540.472790000000000000
          Top = 18.897649999999880000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status da OS')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 52.913420000000000000
          Top = 18.897649999999880000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Final da execu'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 442.205010000000000000
          Top = 18.897649999999880000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fato gerador')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 627.401980000000000000
          Top = 18.897649999999880000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID Contrato')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ordens de servi'#231'o envolvidas')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 11.338590000000000000
        Top = 676.535870000000000000
        Width = 680.315400000000000000
      end
      object DetailData1: TfrxDetailData
        Height = 287.244280000000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        DataSet = frxDs006Grupos
        DataSetName = 'frxDs006Grupos'
        RowCount = 0
        object Chart006A: TfrxChartView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 268.346630000000000000
          OnBeforePrint = 'Chart1OnBeforePrint'
          ShowHint = False
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C6508124C6567656E642E4672616D652E436F6C6F720708636C5369
            6C766572164C6567656E642E4672616D652E536D616C6C446F747309154C6567
            656E642E536861646F772E56697369626C65080D4672616D652E56697369626C
            65080656696577334408165669657733444F7074696F6E732E526F746174696F
            6E02000A426576656C4F75746572070662764E6F6E6505436F6C6F720707636C
            576869746511436F6C6F7250616C65747465496E646578020D0000}
          ChartElevation = 345
          SeriesData = <>
        end
        object Memo19: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Resposta'
          DataSet = frxDs006Grupos
          DataSetName = 'frxDs006Grupos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGrupos."Resposta"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 634.961040000000000000
        Width = 680.315400000000000000
        DataSet = frxDs006A_OSs
        DataSetName = 'frxDs006A_OSs'
        RowCount = 0
        object Memo9: TfrxMemoView
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDs006A_OSs
          DataSetName = 'frxDs006A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs006A_OSs."Codigo"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 139.842610000000000000
          Width = 302.362365830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_SiapTerCad'
          DataSet = frxDs006A_OSs
          DataSetName = 'frxDs006A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs006A_OSs."NO_SiapTerCad"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 540.472790000000000000
          Width = 86.929155830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_ESTATUS'
          DataSet = frxDs006A_OSs
          DataSetName = 'frxDs006A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs006A_OSs."NO_ESTATUS"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 52.913420000000000000
          Width = 86.929133859999990000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DtaExeFim'
          DataSet = frxDs006A_OSs
          DataSetName = 'frxDs006A_OSs'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs006A_OSs."DtaExeFim"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 442.205010000000000000
          Width = 98.267745830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_FatoGeradr'
          DataSet = frxDs006A_OSs
          DataSetName = 'frxDs006A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs006A_OSs."NO_FatoGeradr"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NumContrat'
          DataSet = frxDs006A_OSs
          DataSetName = 'frxDs006A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs006A_OSs."NumContrat"]')
          ParentFont = False
        end
      end
    end
  end
  object Qr006Grupos: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterScroll = Qr006GruposAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT Resposta'
      'FROM _rmip_004atr_')
    Left = 196
    Top = 148
    object Qr006GruposResposta: TWideStringField
      FieldName = 'Resposta'
      Size = 255
    end
  end
  object Qr006NResp: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Respondido'
      'FROM _rmip_004atr_'
      'WHERE Respondido = 0')
    Left = 196
    Top = 292
    object IntegerField1: TIntegerField
      FieldName = 'RespAtrIts'
    end
  end
  object Qr006XVals: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT DISTINCT DtaExeFim, '
      'TO_DAYS(DtaExeFim) DIAS'
      'FROM _rmip_004atr_'
      'ORDER BY DtaExeFim')
    Left = 196
    Top = 340
    object Qr006XValsDtaExeFim: TDateField
      FieldName = 'DtaExeFim'
    end
    object Qr006XValsDIAS: TIntegerField
      FieldName = 'DIAS'
    end
  end
  object Qr006Itens: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 196
    Top = 388
    object Qr006ItensITENS: TFloatField
      FieldName = 'ITENS'
    end
    object Qr006ItensDIAS: TIntegerField
      FieldName = 'DIAS'
    end
    object Qr006ItensDtaExeFim: TDateField
      FieldName = 'DtaExeFim'
    end
  end
  object Qr006Dados: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 196
    Top = 436
    object Qr006DadosNO_DEPENDENCI: TWideStringField
      FieldName = 'NO_DEPENDENCI'
      Size = 60
    end
    object Qr006DadosCorPizza: TIntegerField
      FieldName = 'CorPizza'
    end
  end
  object Qr006A_OSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, cab.Entidade, cab.SiapTerCad, cab.Estatus, '
      ' cab.DtaExeFim, cab.NumContrat, cab.Grupo,'
      'fge.Nome NO_FatoGeradr,'
      'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM oscab cab'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad'
      'WHERE cab.Codigo=0')
    Left = 52
    Top = 248
    object Qr006A_OSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr006A_OSsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object Qr006A_OSsSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object Qr006A_OSsEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object Qr006A_OSsDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object Qr006A_OSsNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object Qr006A_OSsGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object Qr006A_OSsNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
    end
    object Qr006A_OSsNO_ESTATUS: TWideStringField
      FieldName = 'NO_ESTATUS'
      Size = 60
    end
    object Qr006A_OSsNO_SiapTerCad: TWideStringField
      FieldName = 'NO_SiapTerCad'
      Size = 100
    end
    object Qr006A_OSsNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object frxDs006A_OSs: TfrxDBDataset
    UserName = 'frxDs006A_OSs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Entidade=Entidade'
      'SiapTerCad=SiapTerCad'
      'Estatus=Estatus'
      'DtaExeFim=DtaExeFim'
      'NumContrat=NumContrat'
      'Grupo=Grupo'
      'NO_FatoGeradr=NO_FatoGeradr'
      'NO_ESTATUS=NO_ESTATUS'
      'NO_SiapTerCad=NO_SiapTerCad'
      'NO_ENT=NO_ENT')
    DataSet = Qr006A_OSs
    BCDToCurrency = False
    Left = 52
    Top = 296
  end
  object frxDs006A_PrgLstIts: TfrxDBDataset
    UserName = 'frxDs006A_PrgLstIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PrgLstCab=PrgLstCab'
      'PrgLstIts=PrgLstIts'
      'NO_LST=NO_LST'
      'Sigla=Sigla'
      'Nome=Nome'
      'Funcoes=Funcoes')
    DataSet = Qr006A_PrgLstIts
    BCDToCurrency = False
    Left = 196
    Top = 100
  end
  object Qr006SubGrupos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = Qr006SubGruposAfterOpen
    SQL.Strings = (
      'SELECT Dependenci'
      'FROM _rmip_006atr_'
      'WHERE Respondido=1'
      'AND Resposta="SIM"')
    Left = 196
    Top = 244
    object Qr006SubGruposDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
  end
  object frxDs006Grupos: TfrxDBDataset
    UserName = 'frxDs006Grupos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Resposta=Resposta')
    DataSet = Qr006Grupos
    BCDToCurrency = False
    Left = 196
    Top = 196
  end
end
