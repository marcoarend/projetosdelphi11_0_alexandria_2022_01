unit OSCabFat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkDBEdit, dmkValUsu, DMKpnfsConversao, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmOSCabFat = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    PnEdita: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdPagante: TDBEdit;
    DBEdNO_PAG: TDBEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    Label21: TLabel;
    TPDataFat: TdmkEditDateTimePicker;
    EdHoraFat: TdmkEdit;
    EdValorServi: TdmkEdit;
    Label2: TLabel;
    Label10: TLabel;
    EdValorOutrs: TdmkEdit;
    Label11: TLabel;
    EdValorDesco: TdmkEdit;
    Label12: TLabel;
    EdValorTotal: TdmkEdit;
    Label7: TLabel;
    EdCartEmis: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    VuCondicaoPG: TdmkValUsu;
    EdNumNF: TdmkEdit;
    Label13: TLabel;
    EdSerNF: TdmkEdit;
    Label14: TLabel;
    Label8: TLabel;
    DBEdEmpresa: TDBEdit;
    Label9: TLabel;
    DBEdNO_EMP: TDBEdit;
    SbNFSe: TSpeedButton;
    Label15: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdValorServiChange(Sender: TObject);
    procedure EdValConsuChange(Sender: TObject);
    procedure EdValUsadoChange(Sender: TObject);
    procedure EdValorOutrsChange(Sender: TObject);
    procedure EdValorDescoChange(Sender: TObject);
    procedure EdNumNFChange(Sender: TObject);
    procedure SbNFSeClick(Sender: TObject);
    procedure DBEdEmpresaChange(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValores();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FCliente: Integer;
    FDataAbriu: TDateTime;
    FEncerra: Boolean;
  end;

  var
  FmOSCabFat: TFmOSCabFat;

implementation

uses UnMyObjects, Module, ModuleFatura, UMySQLModule, UnBugs_Tabs, NFSe_PF_0201,
  ModuleGeral, ModAgenda;

{$R *.DFM}

procedure TFmOSCabFat.BtOKClick(Sender: TObject);
const
  FatID = VAR_FATID_4001;
  FatGru = VAR_FATID_4001;
  TipoFatura = tfatServico; {TTipoFatura}
  FaturaDta = dfEncerramento; {TDataFatura}
  Financeiro = tfinCred; {TTipoFinanceiro}
  //
  // N�o usa ainda
  Represen = 0;
var
  DtaFimFat, SerNF: String;
  Codigo, Estatus, CondicaoPg, CartEmis, Conta, NumNF: Integer;
  ValorTotal, ValorServi, ValorDesco, ValorOutrs: Double;
  //
  Filial, Entidade, FatNum, Cliente, IDDuplicata, NumeroNF, TipoCart: Integer;
  DataAbriu, DataEncer: TDateTime;
  SerieNF: String;
  ValTotal: Double;
  //
  function AlteraOSCab(): Boolean;
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
    'Estatus', 'ValorTotal', 'ValorServi',
    'ValorDesco', 'DtaFimFat', 'ValorOutrs',
    'CondicaoPg', 'CartEmis', 'Conta', 'SerNF', 'NumNF'], [
    'Codigo'], [
    Estatus, ValorTotal, ValorServi,
    ValorDesco, DtaFimFat, ValorOutrs,
    CondicaoPg, CartEmis, Conta, SerNF, NumNF], [
    Codigo], True);
  end;
var
  QuestaoTyp: TAgendaQuestao;
  QuestaoCod: Integer;
begin
  Codigo         := Geral.IMV(Geral.SoNumero_TT(DBEdCodigo.Text));
  Estatus        := CO_BUG_STATUS_0800_FATURADO;
  ValorTotal     := EdValorTotal.ValueVariant;
  ValorServi     := EdValorServi.ValueVariant;
  ValorDesco     := EdValorDesco.ValueVariant;
  DtaFimFat      := Geral.FDT(TPDataFat.Date, 1) + ' ' + EdHoraFat.Text;
  ValorOutrs     := EdValorOutrs.ValueVariant;
  CondicaoPg     := EdCondicaoPG.ValueVariant;
  CartEmis       := EdCartEmis.ValueVariant;
  Conta          := EdConta.ValueVariant;
  SerNF          := EdSerNF.Text;
  NumNF          := EdNumNF.ValueVariant;
  //
  if MyObjects.FIC(CondicaoPG = 0, EdCondicaoPG, 'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(CartEmis = 0, EdCartEmis, 'Informe a carteira!') then Exit;
  if MyObjects.FIC(Conta = 0, EdConta, 'Informe a conta!') then Exit;
  if ValorTotal < 0.01 then
  begin
    if FEncerra then
    begin
      if Geral.MB_Pergunta('Valor inv�lido para faturamento!' + sLineBreak +
      'Deseja encerrar assim mesmo?') <> ID_YES then
        Exit;
    end else
    begin
      Geral.MB_Aviso('Valor inv�lido para faturamento!');
      Exit;
    end;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    if AlteraOSCab() then
    begin
      // N�o h� faturamento parcial: Controle e c�digo s�o o mesmo n�mero
      FatNum      := Codigo;
      IDDuplicata := Codigo;
      // FIM N�o h� faturamento parcial
      Filial      := Geral.IMV(Geral.SoNumero_TT(DBEdEmpresa.Text));
      Entidade    := DModG.ObtemEntidadeDeFilial(Filial);
      Cliente     := Geral.IMV(Geral.SoNumero_TT(DBEdPagante.Text));
      DataAbriu   := FDataAbriu;
      // 2013-06-18
      //DataEncer   := Now();
      DataEncer   := TPDataFat.Date;
      // FIM  2013-06-18
      NumeroNF    := NumNF;
      SerieNF     := SerNF;
      TipoCart    := DmFatura.QrCartEmisTipo.Value;
      ValTotal    := ValorTotal;
      //
      DmFatura.EmiteFaturas(Codigo, FatGru, Entidade, FatID, FatNum, Cliente,
        DataAbriu, DataEncer, IDDuplicata, NumeroNF, SerieNF, CartEmis,
        TipoCart, Conta, CondicaoPG, Represen, TipoFatura, FaturaDta, Financeiro,
        ValTotal);
      //
      QuestaoTyp := qagOSBgstrl;
      QuestaoCod := Codigo;
      DmModAgenda.AtualizaStatusTodosEventosMesmoTipoCodigo(Estatus,
        QuestaoTyp, QuestaoCod);

      if FQrIts <> nil then
      begin
        UMyMod.AbreQuery(FQrIts, Dmod.MyDB);
        //FQrIts.Locate('Controle', Controle, []);
      end;
      Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSCabFat.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSCabFat.CalculaValores();
var
  ValorServi, ValorOutrs, ValorDesco, ValorTotal: Double;
begin
  ValorServi := EdValorServi.ValueVariant;
  ValorOutrs := EdValorOutrs.ValueVariant;
  ValorDesco := EdValorDesco.ValueVariant;
  ValorTotal := ValorServi + ValorOutrs - ValorDesco;
  //
  EdValorTotal.ValueVariant := ValorTotal;
end;

procedure TFmOSCabFat.DBEdEmpresaChange(Sender: TObject);
var
  Empresa, Entidade: Integer;
begin
  if DBEDEmpresa.Text <> '' then
  begin
    Empresa  := Geral.IMV(DBEdEmpresa.Text);
    Entidade := DmodG.ObtemEntidadeDeFilial(Empresa);
    DmFatura.ReopenCartEmis(Entidade);
  end else
    DmFatura.QrCartEmis.Close;
end;

procedure TFmOSCabFat.EdNumNFChange(Sender: TObject);
begin
  SbNFSe.Enabled := EdNumNF.ValueVariant = 0;
end;

procedure TFmOSCabFat.EdValConsuChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmOSCabFat.EdValorDescoChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmOSCabFat.EdValorOutrsChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmOSCabFat.EdValorServiChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmOSCabFat.EdValUsadoChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmOSCabFat.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdNumNFChange(Self);
end;

procedure TFmOSCabFat.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DmFatura.ReopenPediPrzCab();
  DmFatura.ReopenContas(tfinCred);
  //
  CBCondicaoPG.ListSource := DmFatura.DsPediPrzCab;
  CBCartEmis.ListSource   := DmFatura.DsCartEmis;
  CBConta.ListSource      := DmFatura.DsContas;
end;

procedure TFmOSCabFat.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSCabFat.SbNFSeClick(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  //Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
  //
  TabedForm = nil;
var
  Prestador, Tomador: Integer;
  Valor: Double;
  SerieNF: String;
  NumNF: Integer;
begin
  Prestador := FEmpresa; //DmodG.QrFiliLogFilial.Value;
  Tomador   := FCliente;
  Valor     := EdValorTotal.ValueVariant;
  //
  UnNFSe_PF_0201.MostraFormNFSe(SQLType,
  Prestador, Tomador, Intermediario, MeuServico, ItemListSrv,
  Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico, nil, Valor,
  SerieNF, NumNF, TabedForm);
  //
  if NumNF <> 0  then
  begin
    EdSerNF.ValueVariant := SerieNF;
    EdNumNF.ValueVariant := NumNF;
  end;
end;

{ TODO : CONTRATO!!! }

end.
