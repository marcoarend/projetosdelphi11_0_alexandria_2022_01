object FmOSCab2: TFmOSCab2
  Left = 368
  Top = 194
  Caption = 'GER-OSERV-001 :: Ordem de Servi'#231'o'
  ClientHeight = 897
  ClientWidth = 1914
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  ParentFont = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnQrys: TPanel
    Left = 1742
    Top = 64
    Width = 172
    Height = 833
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alRight
    BevelOuter = bvLowered
    TabOrder = 2
    Visible = False
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1914
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1854
      Top = 0
      Width = 60
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 372
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtCunsCad: TBitBtn
        Tag = 279
        Left = 263
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtCunsCadClick
      end
      object BtNFSePesq: TBitBtn
        Tag = 533
        Left = 315
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtNFSePesqClick
      end
    end
    object GB_M: TGroupBox
      Left = 372
      Top = 0
      Width = 291
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 258
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 258
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 258
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBOperacao: TGroupBox
      Left = 663
      Top = 0
      Width = 419
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 3
      object LaOperacaoA: TLabel
        Left = 9
        Top = 11
        Width = 54
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaOperacaoB: TLabel
        Left = 11
        Top = 14
        Width = 54
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaOperacaoC: TLabel
        Left = 10
        Top = 12
        Width = 54
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 1082
      Top = 0
      Width = 772
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 4
      object Panel4: TPanel
        Left = 2
        Top = 18
        Width = 451
        Height = 44
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 16
          Top = 2
          Width = 15
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 15
          Top = 1
          Width = 15
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object PB1: TProgressBar
          Left = 0
          Top = 23
          Width = 451
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          TabOrder = 0
        end
      end
      object Panel6: TPanel
        Left = 453
        Top = 18
        Width = 317
        Height = 44
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object Label24: TLabel
          Left = 207
          Top = 7
          Width = 55
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Empresa:'
          Visible = False
        end
        object Label33: TLabel
          Left = 43
          Top = 7
          Width = 93
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Dias P'#243's Venda:'
          Visible = False
        end
        object DBEdit17: TDBEdit
          Left = 143
          Top = 5
          Width = 50
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Empresa'
          DataSource = DsOSCab
          TabOrder = 0
          Visible = False
        end
        object DBEdit23: TDBEdit
          Left = 268
          Top = 2
          Width = 45
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'DdsPosVda'
          DataSource = DsOSCab
          TabOrder = 1
          Visible = False
        end
      end
    end
  end
  object PnForm: TPanel
    Left = 0
    Top = 64
    Width = 1742
    Height = 833
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object SpLugarOpcao: TSplitter
      Left = 172
      Top = 0
      Width = 6
      Height = 833
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      OnMoved = SpLugarOpcaoMoved
    end
    object PnEdita: TPanel
      Left = 178
      Top = 0
      Width = 1564
      Height = 833
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      Visible = False
      object ScrollBox2: TScrollBox
        Left = 24
        Top = 2
        Width = 1564
        Height = 756
        BevelInner = bvNone
        BevelOuter = bvNone
        TabOrder = 1
        object GBEdita: TGroupBox
          Left = 0
          Top = 0
          Width = 1560
          Height = 535
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Label1: TLabel
            Left = 20
            Top = 20
            Width = 69
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Localizador:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label2: TLabel
            Left = 532
            Top = 20
            Width = 44
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label6: TLabel
            Left = 69
            Top = 69
            Width = 37
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Lugar:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label14: TLabel
            Left = 975
            Top = 20
            Width = 41
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Status:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label15: TLabel
            Left = 694
            Top = 69
            Width = 130
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fato gerador (Motivo):'
            Color = clBtnFace
            ParentColor = False
          end
          object Label34: TLabel
            Left = 950
            Top = 226
            Width = 49
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Contato:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label36: TLabel
            Left = 20
            Top = 226
            Width = 100
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Contratante [F4]:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label38: TLabel
            Left = 527
            Top = 226
            Width = 79
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pagante [F4]:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label25: TLabel
            Left = 94
            Top = 20
            Width = 55
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Empresa:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label41: TLabel
            Left = 20
            Top = 286
            Width = 102
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valid. or'#231'amento:'
          end
          object Label42: TLabel
            Left = 89
            Top = 310
            Width = 28
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Dias.'
          end
          object Label47: TLabel
            Left = 20
            Top = 335
            Width = 460
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'Texto para cliente na impress'#227'o de execu'#231#227'o: (cadastrado em text' +
              'os gen'#233'ricos)'
            Color = clBtnFace
            ParentColor = False
          end
          object Label50: TLabel
            Left = 20
            Top = 384
            Width = 468
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'Texto para cliente na impress'#227'o do or'#231'amento: (cadastrado em tex' +
              'tos gen'#233'ricos)'
            Color = clBtnFace
            ParentColor = False
          end
          object Label54: TLabel
            Left = 20
            Top = 69
            Width = 41
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Op'#231#227'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label35: TLabel
            Left = 433
            Top = 226
            Width = 90
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Contrato* [F4]:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label55: TLabel
            Left = 708
            Top = 433
            Width = 110
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Equipe de agentes:'
            Color = clBtnFace
            ParentColor = False
          end
          object SpeedButton5: TSpeedButton
            Left = 942
            Top = 39
            Width = 25
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton5Click
          end
          object SpeedButton6: TSpeedButton
            Left = 401
            Top = 246
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton6Click
          end
          object SpeedButton7: TSpeedButton
            Left = 918
            Top = 246
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton7Click
          end
          object SpeedButton8: TSpeedButton
            Left = 662
            Top = 89
            Width = 26
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton8Click
          end
          object Label63: TLabel
            Left = 20
            Top = 433
            Width = 254
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Forma de mobilidade (e centro de estoque):'
            Color = clBtnFace
            ParentColor = False
          end
          object SpeedButton9: TSpeedButton
            Left = 1200
            Top = 246
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton9Click
          end
          object SpeedButton10: TSpeedButton
            Left = 1200
            Top = 354
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton10Click
          end
          object SpeedButton11: TSpeedButton
            Left = 1200
            Top = 404
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton11Click
          end
          object SpeedButton12: TSpeedButton
            Left = 519
            Top = 453
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton12Click
          end
          object Label65: TLabel
            Left = 20
            Top = 480
            Width = 60
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vendedor:'
            Color = clBtnFace
            ParentColor = False
          end
          object SBVendedor: TSpeedButton
            Left = 544
            Top = 500
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SBVendedorClick
          end
          object EdCodigo: TdmkEdit
            Left = 20
            Top = 39
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdEntidade: TdmkEditCB
            Left = 532
            Top = 39
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Entidade'
            UpdCampo = 'Entidade'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEntidadeChange
            OnEnter = EdEntidadeEnter
            DBLookupComboBox = CBEntidade
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEntidade: TdmkDBLookupComboBox
            Left = 601
            Top = 39
            Width = 338
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NO_ENT'
            TabOrder = 4
            dmkEditCB = EdEntidade
            QryCampo = 'Entidade'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdSiapTerCad: TdmkEditCB
            Left = 69
            Top = 89
            Width = 69
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'SiapTerCad'
            UpdCampo = 'SiapTerCad'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnEnter = EdSiapTerCadEnter
            DBLookupComboBox = CBSiapTerCad
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBSiapTerCad: TdmkDBLookupComboBox
            Left = 138
            Top = 89
            Width = 523
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsSiapterCad
            TabOrder = 9
            OnEnter = CBSiapTerCadEnter
            dmkEditCB = EdSiapTerCad
            QryCampo = 'SiapTerCad'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEstatus: TdmkEditCB
            Left = 975
            Top = 39
            Width = 54
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Estatus'
            UpdCampo = 'Estatus'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEstatusChange
            DBLookupComboBox = CBEstatus
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEstatus: TdmkDBLookupComboBox
            Left = 1029
            Top = 39
            Width = 197
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEstatusOSs
            TabOrder = 6
            dmkEditCB = EdEstatus
            QryCampo = 'Estatus'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdFatoGeradr: TdmkEditCB
            Left = 694
            Top = 89
            Width = 54
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FatoGeradr'
            UpdCampo = 'FatoGeradr'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFatoGeradrChange
            DBLookupComboBox = CBFatoGeradr
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFatoGeradr: TdmkDBLookupComboBox
            Left = 748
            Top = 89
            Width = 478
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsFatoGeradr
            TabOrder = 11
            dmkEditCB = EdFatoGeradr
            QryCampo = 'FatoGeradr'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object GroupBox3: TGroupBox
            Left = 303
            Top = 123
            Width = 456
            Height = 100
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Data Vistoria: '
            TabOrder = 13
            object Label17: TLabel
              Left = 15
              Top = 39
              Width = 50
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Previsto:'
              Color = clBtnFace
              ParentColor = False
            end
            object Label18: TLabel
              Left = 15
              Top = 74
              Width = 60
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Realizado:'
              Color = clBtnFace
              ParentColor = False
            end
            object Label52: TLabel
              Left = 79
              Top = 20
              Width = 63
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'In'#237'cio: [F4]'
              Color = clBtnFace
              ParentColor = False
            end
            object Label53: TLabel
              Left = 266
              Top = 20
              Width = 81
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'T'#233'rmino: [F4]'
              Color = clBtnFace
              ParentColor = False
            end
            object TPDtaVisPrv: TdmkEditDateTimePicker
              Left = 79
              Top = 39
              Width = 133
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.639644131944805900
              Time = 0.639644131944805900
              TabOrder = 0
              OnExit = TPDtaVisPrvExit
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaVisPrv'
              UpdCampo = 'DtaVisPrv'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPDtaVisExe: TdmkEditDateTimePicker
              Left = 79
              Top = 69
              Width = 133
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.639644131944805900
              Time = 0.639644131944805900
              TabOrder = 4
              OnExit = TPDtaVisExeExit
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaVisExe'
              UpdCampo = 'DtaVisExe'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdDtaVisPrv: TdmkEdit
              Left = 212
              Top = 39
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 1
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'DtaVisPrv'
              UpdCampo = 'DtaVisPrv'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdDtaVisPrvExit
            end
            object EdDtaVisExe: TdmkEdit
              Left = 212
              Top = 69
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 5
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'DtaVisExe'
              UpdCampo = 'DtaVisExe'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdDtaVisExeExit
            end
            object TPFimVisPrv: TdmkEditDateTimePicker
              Left = 266
              Top = 39
              Width = 133
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.639644131944805900
              Time = 0.639644131944805900
              TabOrder = 2
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'FimVisPrv'
              UpdCampo = 'FimVisPrv'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdFimVisPrv: TdmkEdit
              Left = 399
              Top = 39
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 3
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'FimVisPrv'
              UpdCampo = 'FimVisPrv'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object TPFimVisExe: TdmkEditDateTimePicker
              Left = 266
              Top = 69
              Width = 133
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.639644131944805900
              Time = 0.639644131944805900
              TabOrder = 6
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'FimVisExe'
              UpdCampo = 'FimVisExe'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdFimVisExe: TdmkEdit
              Left = 399
              Top = 69
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 7
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'FimVisExe'
              UpdCampo = 'FimVisExe'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox4: TGroupBox
            Left = 768
            Top = 123
            Width = 459
            Height = 100
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Data Execu'#231#227'o: '
            TabOrder = 14
            object Label19: TLabel
              Left = 15
              Top = 44
              Width = 50
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Previsto:'
              Color = clBtnFace
              ParentColor = False
            end
            object Label20: TLabel
              Left = 79
              Top = 20
              Width = 63
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'In'#237'cio: [F4]'
              Color = clBtnFace
              ParentColor = False
            end
            object Label21: TLabel
              Left = 266
              Top = 20
              Width = 81
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'T'#233'rmino: [F4]'
              Color = clBtnFace
              ParentColor = False
            end
            object Label51: TLabel
              Left = 15
              Top = 74
              Width = 60
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Realizado:'
              Color = clBtnFace
              ParentColor = False
            end
            object TPDtaExePrv: TdmkEditDateTimePicker
              Left = 79
              Top = 39
              Width = 133
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.639644131944805900
              Time = 0.639644131944805900
              TabOrder = 0
              OnExit = TPDtaExePrvExit
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaExePrv'
              UpdCampo = 'DtaExePrv'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPDtaExeIni: TdmkEditDateTimePicker
              Left = 79
              Top = 69
              Width = 133
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.639644131944805900
              Time = 0.639644131944805900
              TabOrder = 4
              OnExit = TPDtaExeIniExit
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaExeIni'
              UpdCampo = 'DtaExeIni'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPDtaExeFim: TdmkEditDateTimePicker
              Left = 266
              Top = 69
              Width = 133
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.639644131944805900
              Time = 0.639644131944805900
              TabOrder = 6
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaExeFim'
              UpdCampo = 'DtaExeFim'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdDtaExePrv: TdmkEdit
              Left = 212
              Top = 39
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 1
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'DtaExePrv'
              UpdCampo = 'DtaExePrv'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdDtaExePrvExit
            end
            object EdDtaExeIni: TdmkEdit
              Left = 212
              Top = 69
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 5
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'DtaExeIni'
              UpdCampo = 'DtaExeIni'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdDtaExeIniExit
            end
            object EdDtaExeFim: TdmkEdit
              Left = 399
              Top = 69
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 7
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'DtaExeFim'
              UpdCampo = 'DtaExeFim'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object TPFimExePrv: TdmkEditDateTimePicker
              Left = 266
              Top = 39
              Width = 133
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.639644131944805900
              Time = 0.639644131944805900
              TabOrder = 2
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'FimExePrv'
              UpdCampo = 'FimExePrv'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdFimExePrv: TdmkEdit
              Left = 399
              Top = 39
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 3
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'FimExePrv'
              UpdCampo = 'FimExePrv'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox5: TGroupBox
            Left = 20
            Top = 123
            Width = 277
            Height = 100
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Data Contato: '
            TabOrder = 12
            object Label22: TLabel
              Left = 15
              Top = 74
              Width = 60
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Realizado:'
              Color = clBtnFace
              ParentColor = False
            end
            object TPDtaContat: TdmkEditDateTimePicker
              Left = 84
              Top = 69
              Width = 133
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.639644131944805900
              Time = 0.639644131944805900
              TabOrder = 0
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaContat'
              UpdCampo = 'DtaContat'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdDtaContat: TdmkEdit
              Left = 217
              Top = 69
              Width = 49
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 1
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              QryCampo = 'DtaContat'
              UpdCampo = 'DtaContat'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object EdEntiContat: TdmkEditCB
            Left = 950
            Top = 246
            Width = 54
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 20
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EntiContat'
            UpdCampo = 'EntiContat'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEntiContat
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEntiContat: TdmkDBLookupComboBox
            Left = 1004
            Top = 246
            Width = 191
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsEntiContat
            TabOrder = 21
            dmkEditCB = EdEntiContat
            QryCampo = 'EntiContat'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEntContrat: TdmkEditCB
            Left = 20
            Top = 246
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 15
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EntContrat'
            UpdCampo = 'EntContrat'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEntContratChange
            OnKeyDown = EdEntContratKeyDown
            DBLookupComboBox = CBEntContrat
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEntContrat: TdmkDBLookupComboBox
            Left = 89
            Top = 246
            Width = 310
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NO_ENT'
            ListSource = DsEntContrat
            TabOrder = 16
            OnKeyDown = CBEntContratKeyDown
            dmkEditCB = EdEntContrat
            QryCampo = 'EntContrat'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEntPagante: TdmkEditCB
            Left = 527
            Top = 246
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 18
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EntPagante'
            UpdCampo = 'EntPagante'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEntPaganteChange
            OnKeyDown = EdEntPaganteKeyDown
            DBLookupComboBox = CBEntPagante
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEntPagante: TdmkDBLookupComboBox
            Left = 596
            Top = 246
            Width = 320
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NO_ENT'
            ListSource = DsEntPagante
            TabOrder = 19
            OnKeyDown = CBEntPaganteKeyDown
            dmkEditCB = EdEntPagante
            QryCampo = 'EntPagante'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 162
            Top = 39
            Width = 366
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 2
            dmkEditCB = EdEmpresa
            QryCampo = 'Empresa'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEmpresa: TdmkEditCB
            Left = 94
            Top = 39
            Width = 68
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Empresa'
            UpdCampo = 'Empresa'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEntidadeChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdValiDdOrca: TdmkEdit
            Left = 20
            Top = 305
            Width = 65
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 22
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '1'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            QryCampo = 'ValiDdOrca'
            UpdCampo = 'ValiDdOrca'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object EdExeTxtCli1: TdmkEditCB
            Left = 20
            Top = 354
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 24
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ExeTxtCli1'
            UpdCampo = 'ExeTxtCli1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBExeTxtCli1
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBExeTxtCli1: TdmkDBLookupComboBox
            Left = 89
            Top = 354
            Width = 1106
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsExeTxtCli1
            TabOrder = 25
            dmkEditCB = EdExeTxtCli1
            QryCampo = 'ExeTxtCli1'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CGOperacao: TdmkCheckGroup
            Left = 133
            Top = 276
            Width = 1093
            Height = 55
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Opera'#231#227'o: '
            Columns = 2
            Items.Strings = (
              'Dedetiza'#231#227'o'
              'Caixa d'#39#225'gua')
            TabOrder = 23
            OnClick = CGOperacaoClick
            QryCampo = 'Operacao'
            UpdCampo = 'Operacao'
            UpdType = utYes
            Value = 0
            OldValor = 0
          end
          object EdExeTxtCli2: TdmkEditCB
            Left = 20
            Top = 404
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 26
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ExeTxtCli2'
            UpdCampo = 'ExeTxtCli2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBExeTxtCli2
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBExeTxtCli2: TdmkDBLookupComboBox
            Left = 89
            Top = 404
            Width = 1106
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsExeTxtCli2
            TabOrder = 27
            dmkEditCB = EdExeTxtCli2
            QryCampo = 'ExeTxtCli2'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdOpcao: TdmkEdit
            Left = 20
            Top = 89
            Width = 40
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '1'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            QryCampo = 'Opcao'
            UpdCampo = 'Opcao'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object EdNumContrat: TdmkEdit
            Left = 433
            Top = 246
            Width = 89
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 17
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'NumContrat'
            UpdCampo = 'NumContrat'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = EdNumContratKeyDown
          end
          object EdAgeEqiCab: TdmkEditCB
            Left = 708
            Top = 453
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 31
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'AgeEqiCab'
            UpdCampo = 'AgeEqiCab'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBAgeEqiCab
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBAgeEqiCab: TdmkDBLookupComboBox
            Left = 777
            Top = 453
            Width = 449
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsAgeEqiCab
            TabOrder = 32
            dmkEditCB = EdAgeEqiCab
            QryCampo = 'AgeEqiCab'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdMobiliCad: TdmkEditCB
            Left = 20
            Top = 453
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 28
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'MobiliCad'
            UpdCampo = 'MobiliCad'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBMobiliCad
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBMobiliCad: TdmkDBLookupComboBox
            Left = 91
            Top = 453
            Width = 424
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsMobiliCad
            TabOrder = 29
            dmkEditCB = EdMobiliCad
            QryCampo = 'MobiliCad'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CkPosGerou: TdmkCheckBox
            Left = 551
            Top = 458
            Width = 148
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'N'#227'o gerar OSs filhas.'
            TabOrder = 30
            QryCampo = 'PosGerou'
            UpdCampo = 'PosGerou'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object EdEstatusCor: TdmkEdit
            Left = 1233
            Top = 39
            Width = 49
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            Enabled = False
            ReadOnly = True
            TabOrder = 36
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdVendedor: TdmkEditCB
            Left = 20
            Top = 500
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 33
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Vendedor'
            UpdCampo = 'Vendedor'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBVendedor
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBVendedor: TdmkDBLookupComboBox
            Left = 89
            Top = 500
            Width = 453
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NO_ENT'
            ListSource = DsVendedores
            TabOrder = 34
            dmkEditCB = EdVendedor
            QryCampo = 'Vendedor'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object RGMapPMV: TdmkRadioGroup
            Left = 578
            Top = 482
            Width = 494
            Height = 45
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              '  Mapeamento de iscas (Caso SIM, obrigat'#243'rio apresentar layout d' +
              'o mapeamento): '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o informado'
              'N'#227'o '
              'Sim')
            TabOrder = 35
            QryCampo = 'MapPMV'
            UpdCampo = 'MapPMV'
            UpdType = utYes
            OldValor = 0
          end
        end
        object GBObservacoes: TGroupBox
          Left = 0
          Top = 535
          Width = 1560
          Height = 195
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' Obseva'#231#245'es: '
          TabOrder = 1
          object PnObsExecuta: TPanel
            Left = 2
            Top = 18
            Width = 1556
            Height = 80
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object dmkLabelRotate2: TdmkLabelRotate
              Left = 0
              Top = 0
              Width = 31
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Angle = ag90
              Caption = 'Execu'#231#227'o'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -17
              Font.Name = 'Arial'
              Font.Style = []
              Align = alLeft
              ExplicitHeight = 86
            end
            object MeObsExecuta: TdmkMemo
              Left = 31
              Top = 0
              Width = 1525
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabOrder = 0
              QryCampo = 'ObsExecuta'
              UpdCampo = 'ObsExecuta'
              UpdType = utYes
            end
          end
          object PnObsGaranti: TPanel
            Left = 2
            Top = 98
            Width = 1556
            Height = 95
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object dmkLabelRotate3: TdmkLabelRotate
              Left = 0
              Top = 0
              Width = 31
              Height = 95
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Angle = ag90
              Caption = 'Garantia'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -17
              Font.Name = 'Arial'
              Font.Style = []
              Align = alLeft
              ExplicitHeight = 82
            end
            object MeObsGaranti: TdmkMemo
              Left = 31
              Top = 0
              Width = 1525
              Height = 95
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabOrder = 0
              QryCampo = 'ObsGaranti'
              UpdCampo = 'ObsGaranti'
              UpdType = utYes
            end
          end
        end
      end
      object GBConfirma: TGroupBox
        Left = 0
        Top = 756
        Width = 1564
        Height = 77
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
        object Label57: TLabel
          Left = 187
          Top = 37
          Width = 277
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Contrato*: [F4] pesquisa [Del] ou [Delete] zera.'
          Color = clBtnFace
          ParentColor = False
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 21
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
        object Panel1: TPanel
          Left = 1392
          Top = 18
          Width = 170
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 9
            Top = 2
            Width = 147
            Height = 50
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
      end
    end
    object PnDados: TPanel
      Left = 178
      Top = 0
      Width = 1564
      Height = 833
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object PCGeral: TPageControl
        Left = 0
        Top = 26
        Width = 1564
        Height = 619
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TSLocalizador
        Align = alTop
        TabHeight = 25
        TabOrder = 1
        object TSLocalizador: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Localizador'
          object GBDados: TGroupBox
            Left = 0
            Top = 0
            Width = 1556
            Height = 388
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 0
            object Panel7: TPanel
              Left = 2
              Top = 18
              Width = 967
              Height = 368
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label7: TLabel
                Left = 5
                Top = 5
                Width = 69
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Localizador:'
                Color = clBtnFace
                ParentColor = False
              end
              object Label9: TLabel
                Left = 153
                Top = 5
                Width = 44
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cliente:'
                Color = clBtnFace
                ParentColor = False
              end
              object Label4: TLabel
                Left = 709
                Top = 5
                Width = 41
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status:'
                Color = clBtnFace
                ParentColor = False
              end
              object Label3: TLabel
                Left = 10
                Top = 113
                Width = 37
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Lugar:'
                Color = clBtnFace
                ParentColor = False
              end
              object Label5: TLabel
                Left = 10
                Top = 231
                Width = 130
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Fato gerador (Motivo):'
                Color = clBtnFace
                ParentColor = False
              end
              object Label001: TLabel
                Left = 364
                Top = 231
                Width = 51
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '$ Previa:'
                Color = clBtnFace
                ParentColor = False
              end
              object Label28: TLabel
                Left = 10
                Top = 143
                Width = 70
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'N'#186' contrato:'
                FocusControl = DBEdit20
              end
              object Label29: TLabel
                Left = 10
                Top = 172
                Width = 72
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Contratante:'
                FocusControl = DBEdit21
              end
              object Label30: TLabel
                Left = 10
                Top = 202
                Width = 51
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Pagante:'
                FocusControl = DBEdit25
              end
              object Label31: TLabel
                Left = 172
                Top = 143
                Width = 49
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Contato:'
                FocusControl = DBEdit27
              end
              object Label56: TLabel
                Left = 10
                Top = 261
                Width = 43
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Equipe:'
                FocusControl = DBEdit2
              end
              object Label64: TLabel
                Left = 433
                Top = 261
                Width = 66
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Mobilidade:'
                FocusControl = DBEdit49
              end
              object Label66: TLabel
                Left = 10
                Top = 290
                Width = 60
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Vendedor:'
                FocusControl = DBEdit50
              end
              object DBEdit15: TDBEdit
                Left = 79
                Top = 0
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Codigo'
                DataSource = DsOSCab
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object DBEdit4: TDBEdit
                Left = 202
                Top = 0
                Width = 44
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Entidade'
                DataSource = DsOSCab
                TabOrder = 1
                OnDblClick = DBEdit4DblClick
              end
              object DBEdit3: TDBEdit
                Left = 246
                Top = 0
                Width = 321
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NO_ENT'
                DataSource = DsOSCab
                TabOrder = 2
              end
              object DBEdit6: TDBEdit
                Left = 753
                Top = 0
                Width = 40
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Estatus'
                DataSource = DsOSCab
                TabOrder = 3
              end
              object DBEdit5: TDBEdit
                Left = 793
                Top = 0
                Width = 167
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NO_ESTATUS'
                DataSource = DsOSCab
                TabOrder = 4
              end
              object DBEdSiapTerCad: TDBEdit
                Left = 54
                Top = 108
                Width = 69
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'SiapTerCad'
                DataSource = DsOSCab
                TabOrder = 5
                OnDblClick = DBEdSiapTerCadDblClick
              end
              object DBEdit1: TDBEdit
                Left = 123
                Top = 108
                Width = 410
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NO_SiapTerCad'
                DataSource = DsOSCab
                TabOrder = 6
              end
              object DBEdit8: TDBEdit
                Left = 610
                Top = 30
                Width = 35
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'FatoGeradr'
                DataSource = DsOSCab
                TabOrder = 7
              end
              object DBEdit7: TDBEdit
                Left = 158
                Top = 226
                Width = 203
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NO_FatoGeradr'
                DataSource = DsOSCab
                TabOrder = 8
              end
              object DBEdit9: TDBEdit
                Left = 438
                Top = 226
                Width = 94
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'ValorPre'
                DataSource = DsOSCab
                TabOrder = 9
              end
              object GroupBox6: TGroupBox
                Left = 10
                Top = 30
                Width = 154
                Height = 75
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Data Contato: '
                TabOrder = 10
                object Label23: TLabel
                  Left = 10
                  Top = 20
                  Width = 60
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Realizado:'
                  Color = clBtnFace
                  ParentColor = False
                end
                object DBEdit16: TDBEdit
                  Left = 10
                  Top = 44
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'TXTContat'
                  DataSource = DsOSCab
                  TabOrder = 0
                end
              end
              object GroupBox1: TGroupBox
                Left = 167
                Top = 30
                Width = 297
                Height = 75
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Data Vistoria: '
                TabOrder = 11
                object Label8: TLabel
                  Left = 10
                  Top = 20
                  Width = 50
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Previsto:'
                  Color = clBtnFace
                  ParentColor = False
                end
                object Label10: TLabel
                  Left = 153
                  Top = 20
                  Width = 60
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Realizado:'
                  Color = clBtnFace
                  ParentColor = False
                end
                object DBEdit10: TDBEdit
                  Left = 10
                  Top = 39
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'TXTVisPrv'
                  DataSource = DsOSCab
                  TabOrder = 0
                end
                object DBEdit11: TDBEdit
                  Left = 153
                  Top = 39
                  Width = 137
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'TXTVisExe'
                  DataSource = DsOSCab
                  TabOrder = 1
                end
              end
              object GroupBox2: TGroupBox
                Left = 468
                Top = 30
                Width = 493
                Height = 75
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Data Execu'#231#227'o: '
                TabOrder = 12
                object Label11: TLabel
                  Left = 10
                  Top = 20
                  Width = 50
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Previsto:'
                  Color = clBtnFace
                  ParentColor = False
                end
                object Label12: TLabel
                  Left = 153
                  Top = 20
                  Width = 35
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'In'#237'cio:'
                  Color = clBtnFace
                  ParentColor = False
                end
                object Label13: TLabel
                  Left = 295
                  Top = 20
                  Width = 53
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'T'#233'rmino:'
                  Color = clBtnFace
                  ParentColor = False
                end
                object Label48: TLabel
                  Left = 438
                  Top = 20
                  Width = 37
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Texto:'
                  FocusControl = DBEdit36
                end
                object DBEdit12: TDBEdit
                  Left = 10
                  Top = 39
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'TXTExePrv'
                  DataSource = DsOSCab
                  TabOrder = 0
                end
                object DBEdit13: TDBEdit
                  Left = 154
                  Top = 39
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'TXTExeIni'
                  DataSource = DsOSCab
                  TabOrder = 1
                end
                object DBEdit14: TDBEdit
                  Left = 297
                  Top = 39
                  Width = 137
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'TXTExeFim'
                  DataSource = DsOSCab
                  TabOrder = 2
                end
                object DBEdit36: TDBEdit
                  Left = 438
                  Top = 39
                  Width = 44
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'ExeTxtCli1'
                  DataSource = DsOSCab
                  TabOrder = 3
                end
              end
              object DBEdit20: TDBEdit
                Left = 84
                Top = 138
                Width = 83
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NumContrat'
                DataSource = DsOSCab
                TabOrder = 13
              end
              object DBEdit21: TDBEdit
                Left = 84
                Top = 167
                Width = 74
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'EntContrat'
                DataSource = DsOSCab
                TabOrder = 14
                OnDblClick = DBEdit21DblClick
              end
              object DBEdit24: TDBEdit
                Left = 158
                Top = 167
                Width = 375
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NO_CTR'
                DataSource = DsOSCab
                TabOrder = 15
              end
              object DBEdit25: TDBEdit
                Left = 84
                Top = 197
                Width = 74
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'EntPagante'
                DataSource = DsOSCab
                TabOrder = 16
                OnDblClick = DBEdit25DblClick
              end
              object DBEdit26: TDBEdit
                Left = 158
                Top = 197
                Width = 375
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NO_PAG'
                DataSource = DsOSCab
                TabOrder = 17
              end
              object DBEdit27: TDBEdit
                Left = 226
                Top = 137
                Width = 69
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'EntiContat'
                DataSource = DsOSCab
                TabOrder = 18
              end
              object DBEdit28: TDBEdit
                Left = 295
                Top = 137
                Width = 238
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NO_ENTICONTAT'
                DataSource = DsOSCab
                TabOrder = 19
              end
              object DBEdit29: TDBEdit
                Left = 566
                Top = 0
                Width = 138
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'TXTTel_ENT'
                DataSource = DsOSCab
                TabOrder = 20
              end
              object GroupBox21: TGroupBox
                Left = 542
                Top = 108
                Width = 419
                Height = 144
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Valores:'
                TabOrder = 21
                object Label26: TLabel
                  Left = 98
                  Top = 20
                  Width = 58
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '$ Servi'#231'o:'
                  FocusControl = DBEdit18
                end
                object Label27: TLabel
                  Left = 202
                  Top = 20
                  Width = 68
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '$ Desconto:'
                  FocusControl = DBEdit19
                end
                object Label37: TLabel
                  Left = 308
                  Top = 20
                  Width = 49
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '$ Total: '
                  FocusControl = DBEdit22
                end
                object Label32: TLabel
                  Left = 10
                  Top = 44
                  Width = 59
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Aprovado:'
                  FocusControl = DBEdit18
                end
                object Label39: TLabel
                  Left = 10
                  Top = 74
                  Width = 84
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'N'#227'o aprovado:'
                  FocusControl = DBEdit30
                end
                object Label40: TLabel
                  Left = 10
                  Top = 103
                  Width = 68
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Or'#231'amento:'
                  FocusControl = DBEdit33
                end
                object DBEdit18: TDBEdit
                  Left = 98
                  Top = 39
                  Width = 99
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'ValorServi'
                  DataSource = DsOSCab
                  TabOrder = 0
                end
                object DBEdit19: TDBEdit
                  Left = 202
                  Top = 39
                  Width = 98
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'ValorDesco'
                  DataSource = DsOSCab
                  TabOrder = 1
                end
                object DBEdit22: TDBEdit
                  Left = 305
                  Top = 39
                  Width = 99
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'ValorTotal'
                  DataSource = DsOSCab
                  TabOrder = 2
                end
                object DBEdit30: TDBEdit
                  Left = 98
                  Top = 69
                  Width = 99
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'InvalServi'
                  DataSource = DsOSCab
                  TabOrder = 3
                end
                object DBEdit31: TDBEdit
                  Left = 202
                  Top = 69
                  Width = 98
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'InvalDesco'
                  DataSource = DsOSCab
                  TabOrder = 4
                end
                object DBEdit32: TDBEdit
                  Left = 305
                  Top = 69
                  Width = 99
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'InvalTotal'
                  DataSource = DsOSCab
                  TabOrder = 5
                end
                object DBEdit33: TDBEdit
                  Left = 98
                  Top = 98
                  Width = 99
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'OrcamServi'
                  DataSource = DsOSCab
                  TabOrder = 6
                end
                object DBEdit34: TDBEdit
                  Left = 202
                  Top = 98
                  Width = 98
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'OrcamDesco'
                  DataSource = DsOSCab
                  TabOrder = 7
                end
                object DBEdit35: TDBEdit
                  Left = 305
                  Top = 98
                  Width = 99
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'OrcamTotal'
                  DataSource = DsOSCab
                  TabOrder = 8
                end
              end
              object DBEdit2: TDBEdit
                Left = 64
                Top = 256
                Width = 49
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'AgeEqiCab'
                DataSource = DsOSCab
                TabOrder = 22
              end
              object DBEdit38: TDBEdit
                Left = 113
                Top = 256
                Width = 310
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NO_AgeEqiCab'
                DataSource = DsOSCab
                TabOrder = 23
              end
              object DBEdit48: TDBEdit
                Left = 556
                Top = 256
                Width = 404
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NO_MobiliCad'
                DataSource = DsOSCab
                TabOrder = 24
              end
              object DBEdit49: TDBEdit
                Left = 507
                Top = 256
                Width = 49
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'MobiliCad'
                DataSource = DsOSCab
                TabOrder = 25
              end
              object DBEdit50: TDBEdit
                Left = 80
                Top = 286
                Width = 49
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Vendedor'
                DataSource = DsOSCab
                TabOrder = 26
              end
              object DBEdit51: TDBEdit
                Left = 129
                Top = 286
                Width = 427
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'NO_Vendedor'
                DataSource = DsOSCab
                TabOrder = 27
              end
              object RGDBMapPMV: TDBRadioGroup
                Left = 10
                Top = 318
                Width = 546
                Height = 45
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 
                  '  Mapeamento de iscas (Caso SIM, obrigat'#243'rio apresentar layout d' +
                  'o mapeamento): '
                Columns = 3
                DataField = 'MapPMV'
                DataSource = DsOSCab
                Items.Strings = (
                  'N'#227'o informado'
                  'N'#227'o '
                  'Sim')
                ParentBackground = True
                TabOrder = 28
                Values.Strings = (
                  '-1'
                  '0'
                  '1')
              end
            end
            object Panel12: TPanel
              Left = 969
              Top = 18
              Width = 585
              Height = 368
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Splitter4: TSplitter
                Left = 0
                Top = 258
                Width = 585
                Height = 6
                Cursor = crVSplit
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
                ExplicitTop = 208
                ExplicitWidth = 475
              end
              object GroupBox10: TGroupBox
                Left = 0
                Top = 264
                Width = 585
                Height = 104
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
                Caption = ' Agentes: '
                TabOrder = 0
                object DBGOSAge: TDBGrid
                  Left = 2
                  Top = 18
                  Width = 581
                  Height = 84
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsOSAge
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                  PopupMenu = PMOSAge
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -13
                  TitleFont.Name = 'Tahoma'
                  TitleFont.Style = []
                  OnCellClick = DBGOSAgeCellClick
                  OnColEnter = DBGOSAgeColEnter
                  OnColExit = DBGOSAgeColExit
                  OnDrawColumnCell = DBGOSAgeDrawColumnCell
                  OnMouseUp = DBGOSAgeMouseUp
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Responsa'
                      Title.Caption = 'R'
                      Width = 17
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_AGENTE'
                      Title.Caption = 'Agente'
                      Width = 160
                      Visible = True
                    end>
                end
              end
              object GroupBox25: TGroupBox
                Left = 0
                Top = 0
                Width = 585
                Height = 258
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Caption = ' Pragas alvo (pr'#233'-atendimento): '
                TabOrder = 1
                object DBGOSCabAlv: TDBGrid
                  Left = 2
                  Top = 18
                  Width = 581
                  Height = 238
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsOSCabAlv
                  PopupMenu = PMOSCabAlv
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -13
                  TitleFont.Name = 'Tahoma'
                  TitleFont.Style = []
                  OnMouseUp = DBGOSCabAlvMouseUp
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_NIVEL'
                      Title.Caption = 'N'
                      Width = 17
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PRAGA'
                      Title.Caption = 'Praga'
                      Width = 160
                      Visible = True
                    end>
                end
              end
            end
          end
          object DBGrid1: TDBGrid
            Left = 0
            Top = 388
            Width = 1556
            Height = 196
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsOSPrn
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Relatorio'
                Title.Caption = 'Impress'#227'o'
                Width = 160
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrIni'
                Title.Caption = 'In'#237'cio'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrFim'
                Title.Caption = 'T'#233'rmino'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UserCad'
                Title.Caption = 'Usu'#225'rio'
                Width = 40
                Visible = True
              end>
          end
        end
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Operacional '
          object PCOpera: TPageControl
            Left = 0
            Top = 0
            Width = 1556
            Height = 584
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ActivePage = TabSheet4
            Align = alClient
            TabHeight = 25
            TabOrder = 0
            object TabSheet4: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Dedetiza'#231#227'o '
              object PnDedetizacao: TPanel
                Left = 0
                Top = 0
                Width = 1548
                Height = 549
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                Visible = False
                object Panel8: TPanel
                  Left = 0
                  Top = 0
                  Width = 369
                  Height = 549
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Splitter7: TSplitter
                    Left = 0
                    Top = 278
                    Width = 369
                    Height = 12
                    Cursor = crVSplit
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                  end
                  object GroupBox7: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 369
                    Height = 278
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    Caption = ' Servi'#231'os: '
                    TabOrder = 0
                    object LaServicoAviso: TLabel
                      Left = 2
                      Top = 18
                      Width = 365
                      Height = 18
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      Caption = 'A = Autorizado / Ok = 100% realizado'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clGreen
                      Font.Height = -15
                      Font.Name = 'Tahoma'
                      Font.Style = [fsBold]
                      ParentFont = False
                      ExplicitWidth = 291
                    end
                    object DBGOSSrv: TDBGrid
                      Left = 2
                      Top = 36
                      Width = 365
                      Height = 242
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      DataSource = DsOSSrv
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                      PopupMenu = PMOSSrv
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -13
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      OnCellClick = DBGOSSrvCellClick
                      OnColEnter = DBGOSSrvColEnter
                      OnColExit = DBGOSSrvColExit
                      OnDrawColumnCell = DBGOSSrvDrawColumnCell
                      OnMouseUp = DBGOSSrvMouseUp
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Autorizado'
                          Title.Caption = 'A'
                          Width = 17
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Controle'
                          Title.Caption = 'ID'
                          Width = 41
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_DesServico'
                          Title.Caption = 'Servi'#231'o'
                          Width = 100
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ValTota'
                          Title.Caption = 'Valor'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'TUDOFEITO'
                          Title.Caption = 'ok'
                          Width = 17
                          Visible = True
                        end>
                    end
                  end
                  object GroupBox9: TGroupBox
                    Left = 0
                    Top = 290
                    Width = 369
                    Height = 259
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    Caption = ' Pragas alvo: '
                    TabOrder = 1
                    object DBGOSAlv: TDBGrid
                      Left = 2
                      Top = 18
                      Width = 365
                      Height = 239
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -13
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      OnMouseUp = DBGOSAlvMouseUp
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'NO_Praga_Z'
                          Title.Caption = 'Nome'
                          Width = 120
                          Visible = True
                        end>
                    end
                  end
                end
                object PCDedetizacao: TPageControl
                  Left = 369
                  Top = 0
                  Width = 1179
                  Height = 549
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ActivePage = TabSheet15
                  Align = alClient
                  TabHeight = 25
                  TabOrder = 1
                  object TabSheet15: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'F'#243'rmulas deAplica'#231#227'o'
                    object Panel10: TPanel
                      Left = 0
                      Top = 0
                      Width = 1171
                      Height = 514
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object DBText2: TDBText
                        Left = 0
                        Top = 0
                        Width = 1171
                        Height = 24
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alTop
                        AutoSize = True
                        DataField = 'TITULO_FORMULAS'
                        DataSource = DsOSSrv
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -20
                        Font.Name = 'Tahoma'
                        Font.Style = []
                        ParentFont = False
                        ExplicitWidth = 77
                      end
                      object GroupBox11: TGroupBox
                        Left = 0
                        Top = 24
                        Width = 1171
                        Height = 490
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        Caption = ' Formula'#231#245'es de APLICA'#199#195'O:'
                        TabOrder = 0
                        object GroupBox12: TGroupBox
                          Left = 2
                          Top = 18
                          Width = 780
                          Height = 470
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          Caption = ' Cabe'#231'alhos e seus itens de F'#211'RMULAS da aplica'#231#227'o selecionada: '
                          TabOrder = 0
                          object Splitter2: TSplitter
                            Left = 2
                            Top = 282
                            Width = 776
                            Height = 6
                            Cursor = crVSplit
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Align = alTop
                            ExplicitWidth = 661
                          end
                          object Panel20: TPanel
                            Left = 2
                            Top = 288
                            Width = 776
                            Height = 180
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Align = alClient
                            BevelOuter = bvNone
                            ParentBackground = False
                            TabOrder = 0
                            object DBGOSFrmAbr: TDBGrid
                              Left = 0
                              Top = 0
                              Width = 193
                              Height = 180
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Align = alLeft
                              DataSource = DsOSFrmAbr
                              TabOrder = 0
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -13
                              TitleFont.Name = 'Tahoma'
                              TitleFont.Style = []
                              OnMouseUp = DBGOSFrmAbrMouseUp
                              Columns = <
                                item
                                  Expanded = False
                                  FieldName = 'NO_ABRANGE'
                                  Title.Caption = 'ABRANG'#202'NCIA'
                                  Width = 120
                                  Visible = True
                                end>
                            end
                            object DBGOSFrmEvo: TDBGrid
                              Left = 193
                              Top = 0
                              Width = 583
                              Height = 180
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Align = alClient
                              DataSource = DsOSFrmEvo
                              PopupMenu = PMOSFrmEvo
                              TabOrder = 1
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -13
                              TitleFont.Name = 'Tahoma'
                              TitleFont.Style = []
                              OnDrawColumnCell = DBGOSFrmEvoDrawColumnCell
                              OnMouseUp = DBGOSFrmEvoMouseUp
                              Columns = <
                                item
                                  Expanded = False
                                  FieldName = 'DataHora'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'PercFeito'
                                  Title.Caption = '%'
                                  Width = 17
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'Texto'
                                  Width = 600
                                  Visible = True
                                end>
                            end
                          end
                          object Panel21: TPanel
                            Left = 2
                            Top = 18
                            Width = 776
                            Height = 264
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Align = alTop
                            BevelOuter = bvNone
                            ParentBackground = False
                            TabOrder = 1
                            object DBGOSFrmCab: TDBGrid
                              Left = 0
                              Top = 0
                              Width = 295
                              Height = 264
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Align = alLeft
                              DataSource = DsOSFrmCab
                              PopupMenu = PMOSFrmCab
                              TabOrder = 0
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -13
                              TitleFont.Name = 'Tahoma'
                              TitleFont.Style = []
                              OnCellClick = DBGOSFrmCabCellClick
                              OnColEnter = DBGOSFrmCabColEnter
                              OnColExit = DBGOSFrmCabColExit
                              OnDrawColumnCell = DBGOSFrmCabDrawColumnCell
                              OnMouseUp = DBGOSFrmCabMouseUp
                              Columns = <
                                item
                                  Expanded = False
                                  FieldName = 'Conta'
                                  Title.Caption = 'ID F. Aplic.'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'Nome'
                                  Title.Caption = 'APLICA'#199#195'O'
                                  Width = 120
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'CALC_FEITO'
                                  Title.Alignment = taCenter
                                  Title.Caption = '?'
                                  Width = 17
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'Codigo'
                                  Title.Caption = 'Localizador'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'Controle'
                                  Title.Caption = 'ID Servi'#231'o'
                                  Visible = True
                                end>
                            end
                            object PCFrmIts: TPageControl
                              Left = 295
                              Top = 0
                              Width = 481
                              Height = 264
                              ActivePage = TabSheet13
                              Align = alClient
                              TabHeight = 25
                              TabOrder = 1
                              object TabSheet13: TTabSheet
                                Caption = 'Itens'
                                object DBGOSFrmRec: TDBGrid
                                  Left = 0
                                  Top = 0
                                  Width = 473
                                  Height = 229
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  DataSource = DsOSFrmRec
                                  PopupMenu = PMOSFrmCab
                                  TabOrder = 0
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -13
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  OnMouseUp = DBGOSFrmRecMouseUp
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'Ordem'
                                      Title.Caption = 'Seq.'
                                      Width = 26
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_GG1'
                                      Title.Caption = 'Produto'
                                      Width = 128
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'PrvQtd'
                                      Title.Caption = 'Qtde'
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'SIGLAUNIDMED'
                                      Title.Caption = 'Unidade'
                                      Width = 44
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_RatifUso'
                                      Title.Caption = 'R?'
                                      Width = 20
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'UsoQtd'
                                      Title.Caption = 'Qtd. uso'
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'UsoCusUni'
                                      Title.Caption = 'Prc. Unit.'
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'UsoCusTot'
                                      Title.Caption = 'Cus. Tot.'
                                      Visible = True
                                    end>
                                end
                              end
                              object TabSheet17: TTabSheet
                                Caption = 'EPIs'
                                ImageIndex = 1
                                object Panel30: TPanel
                                  Left = 0
                                  Top = 0
                                  Width = 473
                                  Height = 63
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alTop
                                  ParentBackground = False
                                  TabOrder = 0
                                  object BtOSFrmEPI: TBitBtn
                                    Tag = 237
                                    Left = 4
                                    Top = 4
                                    Width = 53
                                    Height = 53
                                    Margins.Left = 4
                                    Margins.Top = 4
                                    Margins.Right = 4
                                    Margins.Bottom = 4
                                    NumGlyphs = 2
                                    TabOrder = 0
                                    OnClick = BtOSFrmEPIClick
                                  end
                                end
                                object DBGOSFrmEPI: TDBGrid
                                  Left = 0
                                  Top = 63
                                  Width = 473
                                  Height = 166
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  Color = clWhite
                                  DataSource = DsOSFrmEPI
                                  TabOrder = 1
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -13
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'GraGruX'
                                      Title.Alignment = taRightJustify
                                      Title.Caption = 'C'#243'd '
                                      Width = 73
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'Nome'
                                      Title.Caption = 'Descri'#231#227'o do Produto'
                                      Width = 220
                                      Visible = True
                                    end>
                                end
                              end
                            end
                          end
                        end
                        object GroupBox8: TGroupBox
                          Left = 977
                          Top = 18
                          Width = 192
                          Height = 470
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alRight
                          Caption = ' Comodos / Bens: '
                          TabOrder = 1
                          object DBGOSFrmDep: TDBGrid
                            Left = 2
                            Top = 18
                            Width = 188
                            Height = 450
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Align = alClient
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -13
                            TitleFont.Name = 'Tahoma'
                            TitleFont.Style = []
                            OnMouseUp = DBGOSFrmDepMouseUp
                            Columns = <
                              item
                                Alignment = taCenter
                                Expanded = False
                                FieldName = 'SIGLA'
                                Title.Alignment = taCenter
                                Title.Caption = 'T'
                                Width = 16
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_Campo'
                                Title.Caption = 'Descri'#231#227'o'
                                Width = 100
                                Visible = True
                              end>
                          end
                        end
                        object GroupBox17: TGroupBox
                          Left = 782
                          Top = 18
                          Width = 195
                          Height = 470
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alRight
                          Caption = ' Monitoramento: '
                          TabOrder = 2
                          object DBGOSFrmFlhCb: TDBGrid
                            Left = 2
                            Top = 18
                            Width = 191
                            Height = 121
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Align = alTop
                            DataSource = DsOSFrmFlhCb
                            PopupMenu = PMOSFrmFlhCb
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -13
                            TitleFont.Name = 'Tahoma'
                            TitleFont.Style = []
                            OnMouseUp = DBGOSFrmFlhCbMouseUp
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'Periodd'
                                Title.Caption = 'Dias'
                                Width = 26
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_FORMULA'
                                Title.Caption = 'F'#243'rmula'
                                Visible = True
                              end>
                          end
                          object DBGOSFrmFlhDd: TDBGrid
                            Left = 2
                            Top = 139
                            Width = 191
                            Height = 329
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Align = alClient
                            DataSource = DsOSFrmFlhDd
                            PopupMenu = PMOSFrmFlhDd
                            TabOrder = 1
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -13
                            TitleFont.Name = 'Tahoma'
                            TitleFont.Style = []
                            OnMouseUp = DBGOSFrmFlhDdMouseUp
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'Ordem'
                                Title.Caption = 'Seq.'
                                Width = 26
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Dias'
                                Title.Caption = 'Dias interv.'
                                Width = 59
                                Visible = True
                              end>
                          end
                        end
                      end
                    end
                  end
                  object TabSheet16: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'F'#243'rmulas de Monitoramento'
                    ImageIndex = 1
                    object Panel14: TPanel
                      Left = 0
                      Top = 0
                      Width = 1171
                      Height = 514
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object Panel19: TPanel
                        Left = 0
                        Top = 0
                        Width = 1171
                        Height = 514
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        BevelOuter = bvNone
                        TabOrder = 0
                        object DBText1: TDBText
                          Left = 0
                          Top = 0
                          Width = 1171
                          Height = 24
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alTop
                          AutoSize = True
                          DataField = 'TITULO_FORMULAS'
                          DataSource = DsOSSrv
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -20
                          Font.Name = 'Tahoma'
                          Font.Style = []
                          ParentFont = False
                          ExplicitWidth = 77
                        end
                        object GroupBox13: TGroupBox
                          Left = 0
                          Top = 24
                          Width = 1171
                          Height = 490
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          Caption = 'Formula'#231#245'es de MONITORAMENTO:'
                          TabOrder = 0
                          object Splitter8: TSplitter
                            Left = 469
                            Top = 18
                            Width = 4
                            Height = 470
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            ExplicitHeight = 383
                          end
                          object DBGOSMonCab: TDBGrid
                            Left = 2
                            Top = 18
                            Width = 467
                            Height = 470
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Align = alLeft
                            DataSource = DsOSMonCab
                            PopupMenu = PMOSMonCab
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -13
                            TitleFont.Name = 'Tahoma'
                            TitleFont.Style = []
                            OnMouseUp = DBGOSMonCabMouseUp
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'Conta'
                                Title.Caption = 'ID F. Mon.'
                                Width = 58
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Nome'
                                Title.Caption = 'MONITORAMENTO'
                                Width = 120
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PIP'
                                Title.Caption = 'PMV'
                                Width = 54
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_DEPENDENCI'
                                Title.Caption = 'Depend'#234'ncia'
                                Width = 70
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DdPostero'
                                Title.Caption = 'Interv.'
                                Width = 36
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_prglstcab'
                                Title.Caption = 'Lista de perguntas'
                                Width = 102
                                Visible = True
                              end>
                          end
                          object GroupBox14: TGroupBox
                            Left = 593
                            Top = 18
                            Width = 576
                            Height = 470
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Align = alClient
                            Caption = ' Itens do MONITORAMENTO selecionado: '
                            TabOrder = 1
                            object PCMonIts: TPageControl
                              Left = 2
                              Top = 18
                              Width = 572
                              Height = 450
                              ActivePage = TabSheet18
                              Align = alClient
                              TabHeight = 25
                              TabOrder = 0
                              object TabSheet18: TTabSheet
                                Caption = 'Itens'
                                object DBGOSMonRec: TDBGrid
                                  Left = 0
                                  Top = 0
                                  Width = 564
                                  Height = 415
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  DataSource = DsOSMonRec
                                  PopupMenu = PMOSMonCab
                                  TabOrder = 0
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -13
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  OnMouseUp = DBGOSMonRecMouseUp
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'Ordem'
                                      Title.Caption = 'Seq.'
                                      Width = 26
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'PrvQtd'
                                      Title.Caption = 'Qtde'
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'SIGLAUNIDMED'
                                      Title.Caption = 'Unidade'
                                      Width = 44
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_GG1'
                                      Title.Caption = 'Produto'
                                      Width = 250
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'ValCliDd'
                                      Title.Caption = 'Val. dias'
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'GraGruX'
                                      Title.Caption = 'Reduzido'
                                      Width = 50
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_RatifUso'
                                      Title.Caption = 'R?'
                                      Width = 20
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'UsoQtd'
                                      Title.Caption = 'Qtd. Uso'
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'UsoCusUni'
                                      Title.Caption = 'Cus. Unit.'
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'UsoCusTot'
                                      Title.Caption = 'Custo Tot.'
                                      Visible = True
                                    end>
                                end
                              end
                              object TabSheet20: TTabSheet
                                Caption = 'EPIs'
                                ImageIndex = 1
                                object Panel31: TPanel
                                  Left = 0
                                  Top = 0
                                  Width = 564
                                  Height = 63
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alTop
                                  ParentBackground = False
                                  TabOrder = 0
                                  object BtOSMonEPI: TBitBtn
                                    Tag = 237
                                    Left = 4
                                    Top = 4
                                    Width = 53
                                    Height = 53
                                    Margins.Left = 4
                                    Margins.Top = 4
                                    Margins.Right = 4
                                    Margins.Bottom = 4
                                    NumGlyphs = 2
                                    TabOrder = 0
                                    OnClick = BtOSMonEPIClick
                                  end
                                end
                                object DBGOSMonEPI: TDBGrid
                                  Left = 0
                                  Top = 63
                                  Width = 564
                                  Height = 352
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  Color = clWhite
                                  DataSource = DsOSMonEPI
                                  TabOrder = 1
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -13
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'GraGruX'
                                      Title.Alignment = taRightJustify
                                      Title.Caption = 'C'#243'd '
                                      Width = 73
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'Nome'
                                      Title.Caption = 'Descri'#231#227'o do Produto'
                                      Width = 220
                                      Visible = True
                                    end>
                                end
                              end
                            end
                          end
                          object GroupBox20: TGroupBox
                            Left = 473
                            Top = 18
                            Width = 120
                            Height = 470
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Align = alLeft
                            Caption = ' Intervalos: '
                            TabOrder = 2
                            object DBGOSMonPipDd: TDBGrid
                              Left = 2
                              Top = 18
                              Width = 116
                              Height = 450
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Align = alClient
                              DataSource = DsOSMonPipDd
                              PopupMenu = PMOSMonPipDd
                              TabOrder = 0
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -13
                              TitleFont.Name = 'Tahoma'
                              TitleFont.Style = []
                              Columns = <
                                item
                                  Expanded = False
                                  FieldName = 'Ordem'
                                  Title.Caption = 'Seq.'
                                  Width = 26
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'Dias'
                                  Width = 30
                                  Visible = True
                                end>
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end
            object TabSheet5: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Caixas d'#39#225'gua  '
              ImageIndex = 1
              object PnCaixaDAgua: TPanel
                Left = 0
                Top = 0
                Width = 1548
                Height = 549
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                Visible = False
                object PageControl3: TPageControl
                  Left = 0
                  Top = 0
                  Width = 1548
                  Height = 549
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  TabOrder = 0
                  Visible = False
                end
                object GroupBox22: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 1548
                  Height = 549
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  Caption = 'Caixas d'#39#225'gua'
                  TabOrder = 1
                  object Panel27: TPanel
                    Left = 2
                    Top = 18
                    Width = 1544
                    Height = 529
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object DBGOSCxaAtrib: TdmkDBGrid
                      Left = 929
                      Top = 0
                      Width = 615
                      Height = 529
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'CU_CAD'
                          Title.Caption = 'C'#243'digo'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_CAD'
                          Title.Caption = 'Atributo'
                          Width = 112
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'CU_ITS'
                          Title.Caption = 'C'#243'digo'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_ITS'
                          Title.Caption = 'Descri'#231#227'o do item do atributo'
                          Width = 236
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ID_Item'
                          Title.Caption = 'ID'
                          Width = 44
                          Visible = True
                        end>
                      Color = clWindow
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -13
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'CU_CAD'
                          Title.Caption = 'C'#243'digo'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_CAD'
                          Title.Caption = 'Atributo'
                          Width = 112
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'CU_ITS'
                          Title.Caption = 'C'#243'digo'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_ITS'
                          Title.Caption = 'Descri'#231#227'o do item do atributo'
                          Width = 236
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ID_Item'
                          Title.Caption = 'ID'
                          Width = 44
                          Visible = True
                        end>
                    end
                    object Panel11: TPanel
                      Left = 415
                      Top = 0
                      Width = 514
                      Height = 529
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alLeft
                      BevelOuter = bvNone
                      TabOrder = 1
                      object GroupBox23: TGroupBox
                        Left = 0
                        Top = 0
                        Width = 514
                        Height = 208
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alTop
                        Caption = ' Dados da caixa d'#39#225'gua selecionada: '
                        TabOrder = 0
                        object Panel28: TPanel
                          Left = 2
                          Top = 18
                          Width = 510
                          Height = 188
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          BevelOuter = bvNone
                          ParentBackground = False
                          TabOrder = 0
                          object Label43: TLabel
                            Left = 5
                            Top = 0
                            Width = 42
                            Height = 16
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Forma:'
                            FocusControl = DBEdNOME_FORMA
                          end
                          object Label44: TLabel
                            Left = 256
                            Top = 0
                            Width = 51
                            Height = 16
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Material:'
                            FocusControl = DBEdNO_MATERIAL
                          end
                          object Label45: TLabel
                            Left = 5
                            Top = 49
                            Width = 72
                            Height = 16
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Vol. (Litros):'
                            FocusControl = DBEdVolumeL
                          end
                          object Label46: TLabel
                            Left = 84
                            Top = 49
                            Width = 52
                            Height = 16
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Medidas:'
                            FocusControl = DBEdMedidas
                          end
                          object dmkLabelRotate1: TdmkLabelRotate
                            Left = 0
                            Top = 108
                            Width = 26
                            Height = 70
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Angle = ag90
                            Caption = 'Acesso:'
                            Font.Charset = ANSI_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -17
                            Font.Name = 'Arial'
                            Font.Style = []
                          end
                          object DBEdNOME_FORMA: TDBEdit
                            Left = 5
                            Top = 20
                            Width = 246
                            Height = 24
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NOME_FORMA'
                            TabOrder = 0
                          end
                          object DBEdNO_MATERIAL: TDBEdit
                            Left = 256
                            Top = 20
                            Width = 246
                            Height = 24
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NO_MATERIAL'
                            TabOrder = 1
                          end
                          object DBEdVolumeL: TDBEdit
                            Left = 5
                            Top = 69
                            Width = 74
                            Height = 24
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'VolumeL'
                            TabOrder = 2
                          end
                          object DBEdMedidas: TDBEdit
                            Left = 84
                            Top = 69
                            Width = 418
                            Height = 24
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'Medidas'
                            TabOrder = 3
                          end
                          object DBMeAcesso: TDBMemo
                            Left = 25
                            Top = 103
                            Width = 477
                            Height = 78
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'Acesso'
                            TabOrder = 4
                          end
                        end
                      end
                      object GroupBox24: TGroupBox
                        Left = 0
                        Top = 208
                        Width = 514
                        Height = 321
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        Caption = ' Fotos de caixas d'#39#225'gua: '
                        TabOrder = 1
                        object Splitter5: TSplitter
                          Left = 2
                          Top = 244
                          Width = 510
                          Height = 6
                          Cursor = crVSplit
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alBottom
                          ExplicitTop = 164
                        end
                        object DBGOSCxI: TDBGrid
                          Left = 2
                          Top = 18
                          Width = 510
                          Height = 226
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          DataSource = DsOSCxI
                          PopupMenu = PMOSCxI
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -13
                          TitleFont.Name = 'Tahoma'
                          TitleFont.Style = []
                          OnMouseUp = DBGOSCxIMouseUp
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'NO_APLICACAO'
                              Title.Caption = 'Mostrar'
                              Width = 46
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'FotoCxa'
                              Width = 331
                              Visible = True
                            end>
                        end
                        object DBMeOSCxI: TDBMemo
                          Left = 2
                          Top = 250
                          Width = 510
                          Height = 69
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alBottom
                          DataField = 'Detalhes'
                          DataSource = DsOSCxI
                          TabOrder = 1
                        end
                      end
                    end
                    object Panel13: TPanel
                      Left = 0
                      Top = 0
                      Width = 415
                      Height = 529
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alLeft
                      BevelOuter = bvNone
                      TabOrder = 2
                      object Splitter6: TSplitter
                        Left = 0
                        Top = 401
                        Width = 415
                        Height = 6
                        Cursor = crVSplit
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alBottom
                        ExplicitTop = 321
                      end
                      object DBGOSCxa: TdmkDBGridZTO
                        Left = 0
                        Top = 0
                        Width = 415
                        Height = 401
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        DataSource = DsOSCxa
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                        PopupMenu = PMOSCxa
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -13
                        TitleFont.Name = 'Tahoma'
                        TitleFont.Style = []
                        RowColors = <>
                        OnCellClick = DBGOSCxaCellClick
                        OnColEnter = DBGOSCxaColEnter
                        OnDrawColumnCell = DBGOSCxaDrawColumnCell
                        OnMouseUp = DBGOSCxaMouseUp
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Autorizado'
                            Title.Caption = 'A'
                            Width = 17
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Controle'
                            Title.Caption = 'ID'
                            Width = 40
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Local'
                            Title.Caption = 'Local da caixa'
                            Width = 150
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'ValTota'
                            Title.Caption = 'Valor'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'TudoFeito'
                            Title.Caption = '?'
                            Width = 17
                            Visible = True
                          end>
                      end
                      object DBMeOSCxa: TDBMemo
                        Left = 0
                        Top = 407
                        Width = 415
                        Height = 122
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alBottom
                        DataField = 'Detalhes'
                        DataSource = DsOSCxa
                        TabOrder = 1
                      end
                    end
                  end
                end
              end
            end
            object TabSheet6: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Monitoramento '
              ImageIndex = 2
              object PnMonitoramento: TPanel
                Left = 0
                Top = 0
                Width = 1548
                Height = 549
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object DBGOSPipMon: TDBGrid
                  Left = 0
                  Top = 36
                  Width = 577
                  Height = 513
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  DataSource = DsOSPipMon
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  PopupMenu = PMOSPipMon
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -13
                  TitleFont.Name = 'Tahoma'
                  TitleFont.Style = []
                  OnMouseUp = DBGOSPipMonMouseUp
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PipCad'
                      Title.Caption = 'ID PMV'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PIP'
                      Title.Caption = 'Refer'#234'ncia'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PrgLstCab'
                      Title.Caption = 'Lista de perguntas'
                      Width = 200
                      Visible = True
                    end>
                end
                object DBGOSPipIts: TdmkDBGridZTO
                  Left = 577
                  Top = 36
                  Width = 971
                  Height = 513
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsOSPipIts
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  PopupMenu = PMOSPipIts
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -13
                  TitleFont.Name = 'Tahoma'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Conta'
                      Title.Caption = 'ID'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Isca'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PERGUNTA'
                      Title.Caption = 'Pergunta'
                      Width = 360
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'RESPOSTA'
                      Title.Caption = 'Resposta'
                      Visible = True
                    end>
                end
                object Panel16: TPanel
                  Left = 0
                  Top = 0
                  Width = 1548
                  Height = 36
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 2
                  object Label16: TLabel
                    Left = 5
                    Top = 10
                    Width = 111
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = #218'ltimo upload web:'
                    FocusControl = DBEdit39
                  end
                  object DBEdit39: TDBEdit
                    Left = 148
                    Top = 5
                    Width = 138
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'LstUplWeb'
                    DataSource = DsOSCab
                    TabOrder = 0
                  end
                end
              end
            end
            object TabSheet14: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Monitoramentos Extras '
              ImageIndex = 3
              object DBGrid2: TDBGrid
                Left = 0
                Top = 0
                Width = 1548
                Height = 549
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsOSExtMon
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                PopupMenu = PMOSExtMon
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -13
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                OnMouseUp = DBGOSPipMonMouseUp
              end
            end
            object TabSheet11: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Croqui'
              ImageIndex = 4
              object BtCroquGer: TBitBtn
                Tag = 10206
                Left = 12
                Top = 12
                Width = 163
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Gerenciar croqui'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtCroquGerClick
              end
            end
            object TabSheet12: TTabSheet
              Caption = 'Laudos'
              ImageIndex = 5
              object Panel24: TPanel
                Left = 63
                Top = 0
                Width = 1485
                Height = 549
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Splitter1: TSplitter
                  Left = 0
                  Top = 330
                  Width = 1485
                  Height = 10
                  Cursor = crVSplit
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  ExplicitWidth = 1548
                end
                object GroupBox27: TGroupBox
                  Left = 0
                  Top = 340
                  Width = 1485
                  Height = 209
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  Caption = ' Parecer do respons'#225'vel t'#233'cnico:'
                  TabOrder = 0
                  object Panel25: TPanel
                    Left = 2
                    Top = 18
                    Width = 1481
                    Height = 189
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object MeDBLauParTec: TDBMemo
                      Left = 0
                      Top = 0
                      Width = 1481
                      Height = 189
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      DataField = 'LauParTec'
                      DataSource = DsOSCab
                      TabOrder = 0
                    end
                  end
                end
                object GroupBox28: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 1485
                  Height = 330
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Caption = ' Grau de infesta'#231#227'o das pragas e vetores:'
                  TabOrder = 1
                  object Splitter13: TSplitter
                    Left = 2
                    Top = 168
                    Width = 1481
                    Height = 10
                    Cursor = crVSplit
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    ExplicitTop = 188
                    ExplicitWidth = 1544
                  end
                  object Panel26: TPanel
                    Left = 2
                    Top = 18
                    Width = 1481
                    Height = 150
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object dmkLabelRotate8: TdmkLabelRotate
                      Left = 0
                      Top = 0
                      Width = 31
                      Height = 150
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Angle = ag90
                      Caption = #193'rea interna'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -17
                      Font.Name = 'Arial'
                      Font.Style = []
                      Align = alLeft
                      ExplicitHeight = 86
                    end
                    object MeDBLauInfesInt: TDBMemo
                      Left = 31
                      Top = 0
                      Width = 1450
                      Height = 150
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      DataField = 'LauInfesInt'
                      DataSource = DsOSCab
                      TabOrder = 0
                    end
                  end
                  object Panel29: TPanel
                    Left = 2
                    Top = 178
                    Width = 1481
                    Height = 150
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 1
                    object dmkLabelRotate9: TdmkLabelRotate
                      Left = 0
                      Top = 0
                      Width = 31
                      Height = 150
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Angle = ag90
                      Caption = #193'rea externa'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -17
                      Font.Name = 'Arial'
                      Font.Style = []
                      Align = alLeft
                      ExplicitHeight = 82
                    end
                    object MeDBLauInfesExt: TDBMemo
                      Left = 31
                      Top = 0
                      Width = 1450
                      Height = 150
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      DataField = 'LauInfesExt'
                      DataSource = DsOSCab
                      TabOrder = 0
                    end
                  end
                end
              end
              object Panel47: TPanel
                Left = 0
                Top = 0
                Width = 63
                Height = 549
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                ParentBackground = False
                TabOrder = 1
                object BtLaudoIns: TBitBtn
                  Tag = 10
                  Left = 5
                  Top = 5
                  Width = 53
                  Height = 53
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtLaudoInsClick
                end
                object BtLaudoDel: TBitBtn
                  Tag = 12
                  Left = 5
                  Top = 63
                  Width = 53
                  Height = 52
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = BtLaudoDelClick
                end
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Op'#231#245'es de pagamento'
          ImageIndex = 1
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 1556
            Height = 584
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox15: TGroupBox
              Left = 0
              Top = 0
              Width = 980
              Height = 584
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              Caption = ' Condi'#231#245'es de pagamento: '
              TabOrder = 0
              object DBGOSPrz: TDBGrid
                Left = 2
                Top = 18
                Width = 976
                Height = 564
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsOSPrz
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                PopupMenu = PMOSPrz
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -13
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                OnCellClick = DBGOSPrzCellClick
                OnColEnter = DBGOSPrzColEnter
                OnDrawColumnCell = DBGOSPrzDrawColumnCell
                OnMouseUp = DBGOSPrzMouseUp
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Escolhido'
                    Title.Caption = 'E'
                    Width = 17
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_CONDICAO'
                    Title.Caption = 'Descri'#231#227'o da condi'#231#227'o de pagamento'
                    Width = 214
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DescoPer'
                    Title.Caption = '% Desconto'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ORC_COM_DESCO'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Title.Caption = '$ Or'#231'ado'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'INV_COM_DESCO'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = 4227327
                    Font.Height = -11
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Title.Caption = '$ N'#227'o Aprov.'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VAL_COM_DESCO'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlue
                    Font.Height = -11
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Title.Caption = '$ Aprovado'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Parcelas'
                    Title.Caption = 'Parc.'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ORC_PARCELA'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Title.Caption = '$ Parc. Or'#231'.'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'INV_PARCELA'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = 4227327
                    Font.Height = -11
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Title.Caption = '$ Parc. N'#227'o'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VAL_PARCELA'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlue
                    Font.Height = -11
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Title.Caption = '$ Parc. Aprov.'
                    Width = 68
                    Visible = True
                  end>
              end
            end
            object GroupBox16: TGroupBox
              Left = 980
              Top = 0
              Width = 576
              Height = 584
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = ' Parcelas do faturamento selecionado: '
              TabOrder = 1
              object DBGLctFatRef: TDBGrid
                Left = 2
                Top = 78
                Width = 572
                Height = 504
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsLctFatRef
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -13
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'PARCELA'
                    Title.Caption = 'Parc.'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Lancto'
                    Width = 82
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencto'
                    Width = 56
                    Visible = True
                  end>
              end
              object Panel23: TPanel
                Left = 2
                Top = 18
                Width = 572
                Height = 60
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                TabOrder = 1
                object BtAlteraVct: TBitBtn
                  Tag = 11
                  Left = 5
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtAlteraVctClick
                end
              end
            end
          end
        end
        object TabSheet3: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Dados para p'#243's-venda'
          ImageIndex = 2
          object GroupBox18: TGroupBox
            Left = 0
            Top = 0
            Width = 769
            Height = 584
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' Telefone e e-mails'
            TabOrder = 0
            object Splitter3: TSplitter
              Left = 2
              Top = 198
              Width = 765
              Height = 6
              Cursor = crVSplit
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
            end
            object DBGEntiTel: TDBGrid
              Left = 2
              Top = 18
              Width = 765
              Height = 180
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              DataSource = DsEntiTel
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -13
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMEETC'
                  Title.Caption = 'Tipo de telefone'
                  Width = 84
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TEL_TXT'
                  Title.Caption = 'Telefone'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ramal'
                  Width = 37
                  Visible = True
                end>
            end
            object DBGEntiMail: TDBGrid
              Left = 2
              Top = 204
              Width = 765
              Height = 378
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsEntiMail
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -13
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMEETC'
                  Title.Caption = 'Tipo de e-mail'
                  Width = 84
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EMail'
                  Title.Caption = 'E-mail'
                  Width = 496
                  Visible = True
                end>
            end
          end
          object GroupBox19: TGroupBox
            Left = 769
            Top = 0
            Width = 787
            Height = 584
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' Alertas comerciais e monitoramentos: '
            TabOrder = 1
            object DBGOSPos: TDBGrid
              Left = 2
              Top = 18
              Width = 783
              Height = 564
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsOSPos
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -13
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              OnMouseUp = DBGOSPosMouseUp
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Dias'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_Aplicacao'
                  Title.Caption = 'A'#231#227'o a ser tomada'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_AplicID'
                  Title.Caption = 'Especifica'#231#245'es'
                  Width = 579
                  Visible = True
                end>
            end
          end
        end
        object TabSheet7: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Particularidades da OS'
          ImageIndex = 3
          object DBGOSChk: TDBGrid
            Left = 0
            Top = 0
            Width = 1556
            Height = 584
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsOSChk
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnCellClick = DBGOSChkCellClick
            OnDrawColumnCell = DBGOSChkDrawColumnCell
            OnMouseUp = DBGOSChkMouseUp
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_FEITO'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Title.Caption = 'Feito'
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ITEM'
                Title.Caption = 'Textos de itens de lista'
                Width = 960
                Visible = True
              end>
          end
        end
        object TabSheet8: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Conversas interlig. '#224' proforma'
          ImageIndex = 4
          object DBGDiarioAdd: TDBGrid
            Left = 0
            Top = 0
            Width = 369
            Height = 584
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            DataSource = DsDiarioAdd
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnDrawColumnCell = DBGDiarioAddDrawColumnCell
            OnMouseUp = DBGDiarioAddMouseUp
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'IMP'
                Title.Alignment = taCenter
                Title.Caption = 'I?'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'Tahoma'
                Title.Font.Style = [fsBold]
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Depto'
                Title.Caption = 'Localizador'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Hora'
                Width = 56
                Visible = True
              end>
          end
          object DBMemo1: TDBMemo
            Left = 369
            Top = 0
            Width = 1187
            Height = 584
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataField = 'Nome'
            DataSource = DsDiarioAdd
            TabOrder = 1
          end
        end
        object TabSheet9: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Escalon. / Status conf.'
          ImageIndex = 5
          object DBGOSSta: TDBGrid
            Left = 405
            Top = 0
            Width = 1151
            Height = 584
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsOSSta
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnMouseUp = DBGOSStaMouseUp
            Columns = <
              item
                Expanded = False
                FieldName = 'Status'
                Width = 41
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_STATUS'
                Title.Caption = 'Descri'#231#227'o'
                Width = 286
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataCad'
                Title.Caption = 'Lan'#231'ado em'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UserCad'
                Title.Caption = 'Usu'#225'rio'
                Visible = True
              end>
          end
          object DBGOSCabXtr: TDBGrid
            Left = 0
            Top = 0
            Width = 405
            Height = 584
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            DataSource = DsOSCabXtr
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnMouseUp = DBGOSCabXtrMouseUp
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrIni'
                Title.Caption = 'In'#237'cio'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrFim'
                Title.Caption = 'Final'
                Visible = True
              end>
          end
        end
        object TabSheet10: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Provid'#234'ncias no localiz.'
          ImageIndex = 6
          object DBGOSPrv: TDBGrid
            Left = 0
            Top = 0
            Width = 1556
            Height = 584
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsOSPrv
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnMouseUp = DBGOSPrvMouseUp
            Columns = <
              item
                Expanded = False
                FieldName = 'DtHrContat'
                Title.Caption = 'Data / hora contato'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FormContat'
                Title.Caption = 'Forma de contato'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Contato'
                Title.Caption = 'Contato'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Agente'
                Title.Caption = 'Agente'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PrvStatus'
                Title.Caption = 'Status'
                Width = 112
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Solicita'#231#227'o de provid'#234'ncia'
                Width = 980
                Visible = True
              end>
          end
        end
        object TabSheet19: TTabSheet
          Caption = 'Observa'#231#245'es'
          ImageIndex = 8
          object GridPanel1: TGridPanel
            Left = 0
            Top = 0
            Width = 1556
            Height = 584
            Align = alClient
            Caption = 'GridPanel1'
            ColumnCollection = <
              item
                Value = 100.000000000000000000
              end>
            ControlCollection = <
              item
                Column = 0
                Control = Panel17
                Row = 0
              end
              item
                Column = 0
                Control = Panel18
                Row = 1
              end>
            RowCollection = <
              item
                Value = 50.000000000000000000
              end
              item
                Value = 50.000000000000000000
              end>
            TabOrder = 0
            object Panel17: TPanel
              Left = 1
              Top = 1
              Width = 1554
              Height = 291
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object dmkLabelRotate4: TdmkLabelRotate
                Left = 0
                Top = 0
                Width = 31
                Height = 291
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Angle = ag90
                Caption = 'Execu'#231#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Arial'
                Font.Style = []
                Align = alLeft
                ExplicitHeight = 86
              end
              object DBMemo3: TDBMemo
                Left = 31
                Top = 0
                Width = 1523
                Height = 291
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataField = 'ObsExecuta'
                DataSource = DsOSCab
                TabOrder = 0
              end
            end
            object Panel18: TPanel
              Left = 1
              Top = 292
              Width = 1554
              Height = 291
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object dmkLabelRotate5: TdmkLabelRotate
                Left = 0
                Top = 0
                Width = 31
                Height = 291
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Angle = ag90
                Caption = 'Garantia'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'Arial'
                Font.Style = []
                Align = alLeft
                ExplicitHeight = 62
              end
              object DBMemo2: TDBMemo
                Left = 31
                Top = 0
                Width = 1523
                Height = 291
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataField = 'ObsGaranti'
                DataSource = DsOSCab
                TabOrder = 0
              end
            end
          end
        end
      end
      object GBCntrl: TGroupBox
        Left = 0
        Top = 754
        Width = 1564
        Height = 79
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 18
          Width = 212
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object SpeedButton4: TBitBtn
            Tag = 4
            Left = 158
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = SpeedButton4Click
          end
          object SpeedButton3: TBitBtn
            Tag = 3
            Left = 108
            Top = 5
            Width = 50
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = SpeedButton3Click
          end
          object SpeedButton2: TBitBtn
            Tag = 2
            Left = 59
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = SpeedButton2Click
          end
          object SpeedButton1: TBitBtn
            Tag = 1
            Left = 10
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = SpeedButton1Click
          end
        end
        object LaRegistro: TStaticText
          Left = 214
          Top = 18
          Width = 442
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelInner = bvLowered
          BevelKind = bkFlat
          Caption = '[N]: 0'
          TabOrder = 2
        end
        object Panel3: TPanel
          Left = 656
          Top = 18
          Width = 906
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object Panel2: TPanel
            Left = 780
            Top = 0
            Width = 126
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            Alignment = taRightJustify
            BevelOuter = bvNone
            TabOrder = 6
            object BtSaida: TBitBtn
              Tag = 13
              Left = 5
              Top = 5
              Width = 111
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BtOSCab: TBitBtn
            Tag = 539
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&OS'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtOSCabClick
          end
          object BtOSSrv: TBitBtn
            Left = 306
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Servi'#231'o'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            Visible = False
            OnClick = BtOSSrvClick
          end
          object BtOSFrmCab: TBitBtn
            Left = 533
            Top = 5
            Width = 110
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Aplica'#231#227'o'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            Visible = False
            OnClick = BtOSFrmCabClick
          end
          object BtOSAlv: TBitBtn
            Left = 419
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Praga'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            Visible = False
            OnClick = BtOSAlvClick
          end
          object BtOSMonCab: TBitBtn
            Left = 646
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Monitora'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            Visible = False
            OnClick = BtOSMonCabClick
          end
          object BtFat: TBitBtn
            Tag = 414
            Left = 155
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Faturamento'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtFatClick
          end
        end
      end
      object Panel22: TPanel
        Left = 0
        Top = 0
        Width = 1564
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label58: TLabel
          Left = 5
          Top = 5
          Width = 69
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Localizador:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label59: TLabel
          Left = 153
          Top = 5
          Width = 44
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cliente:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label60: TLabel
          Left = 709
          Top = 5
          Width = 41
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Status:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label61: TLabel
          Left = 1182
          Top = 5
          Width = 60
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Prev.Exec:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label62: TLabel
          Left = 965
          Top = 5
          Width = 55
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Prev.Vist:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdit40: TDBEdit
          Left = 79
          Top = 0
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Codigo'
          DataSource = DsOSCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit41: TDBEdit
          Left = 202
          Top = 0
          Width = 44
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Entidade'
          DataSource = DsOSCab
          TabOrder = 1
          OnDblClick = DBEdit4DblClick
        end
        object DBEdit42: TDBEdit
          Left = 246
          Top = 0
          Width = 321
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NO_ENT'
          DataSource = DsOSCab
          TabOrder = 2
        end
        object DBEdit43: TDBEdit
          Left = 566
          Top = 0
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'TXTTel_ENT'
          DataSource = DsOSCab
          TabOrder = 3
        end
        object DBEdit44: TDBEdit
          Left = 753
          Top = 0
          Width = 40
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Estatus'
          DataSource = DsOSCab
          TabOrder = 4
        end
        object DBEdit45: TDBEdit
          Left = 793
          Top = 0
          Width = 167
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NO_ESTATUS'
          DataSource = DsOSCab
          TabOrder = 5
        end
        object DBEdit46: TDBEdit
          Left = 1255
          Top = 0
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'TXTExePrv'
          DataSource = DsOSCab
          TabOrder = 6
        end
        object DBEdit47: TDBEdit
          Left = 1034
          Top = 0
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'TXTVisPrv'
          DataSource = DsOSCab
          TabOrder = 7
        end
      end
    end
    object PnLugarOpcao: TPanel
      Left = 0
      Top = 0
      Width = 172
      Height = 833
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object Panel15: TPanel
        Left = 0
        Top = 0
        Width = 172
        Height = 90
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label49: TLabel
          Left = 5
          Top = 10
          Width = 58
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Proforma:'
          Color = clBtnFace
          ParentColor = False
        end
        object BtAlterna: TBitBtn
          Left = 5
          Top = 36
          Width = 162
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Alternar Disposi'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtAlternaClick
        end
        object DBEdit37: TDBEdit
          Left = 69
          Top = 5
          Width = 98
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Grupo'
          DataSource = DsGrupos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object DBGLugar: TDBGrid
        Left = 0
        Top = 90
        Width = 103
        Height = 743
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsLugares
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnMouseUp = DBGLugarMouseUp
        Columns = <
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Lugar'
            Visible = True
          end>
      end
      object DBGOpcao: TDBGrid
        Left = 103
        Top = 90
        Width = 69
        Height = 743
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        DataSource = DsOpcoes
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Opcao'
            Title.Caption = 'Or'#231'amento'
            Width = 20
            Visible = True
          end>
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtFat
    Left = 564
  end
  object QrOSCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrOSCabBeforeOpen
    BeforeClose = QrOSCabBeforeClose
    AfterClose = QrOSCabAfterClose
    BeforeScroll = QrOSCabBeforeScroll
    AfterScroll = QrOSCabAfterScroll
    OnCalcFields = QrOSCabCalcFields
    SQL.Strings = (
      'SELECT eco.Nome NO_ENTICONTAT, cab.*,'
      'fge.Nome NO_FatoGeradr,'
      'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT,'
      'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG,'
      'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR,'
      'car.Nome NO_CART, ppc.Nome NO_PRZ,'
      'tg1.Nome NO_ExeTxtCli1, moc.Nome NO_MobiliCad'
      'FROM oscab cab'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante'
      'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN carteiras car ON car.Codigo=cab.CartEmis'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondicaoPg'
      'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat'
      'LEFT JOIN txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1'
      'LEFT JOIN mobilicad moc ON moc.Codigo=cab.MobiliCad'
      ''
      'WHERE cab.Codigo>0')
    Left = 508
    object QrOSCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'oscab.Codigo'
    end
    object QrOSCabEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'oscab.Entidade'
    end
    object QrOSCabEstatus: TIntegerField
      FieldName = 'Estatus'
      Origin = 'oscab.Estatus'
    end
    object QrOSCabFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
      Origin = 'oscab.FatoGeradr'
    end
    object QrOSCabDtaContat: TDateTimeField
      FieldName = 'DtaContat'
      Origin = 'oscab.DtaContat'
    end
    object QrOSCabDtaVisPrv: TDateTimeField
      FieldName = 'DtaVisPrv'
      Origin = 'oscab.DtaVisPrv'
    end
    object QrOSCabDtaVisExe: TDateTimeField
      FieldName = 'DtaVisExe'
      Origin = 'oscab.DtaVisExe'
    end
    object QrOSCabDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
      Origin = 'oscab.DtaExePrv'
    end
    object QrOSCabDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
      Origin = 'oscab.DtaExeIni'
    end
    object QrOSCabDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
      Origin = 'oscab.DtaExeFim'
    end
    object QrOSCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'oscab.Lk'
    end
    object QrOSCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'oscab.DataCad'
    end
    object QrOSCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'oscab.UserCad'
    end
    object QrOSCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'oscab.DataAlt'
    end
    object QrOSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'oscab.UserAlt'
    end
    object QrOSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'oscab.AlterWeb'
    end
    object QrOSCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'oscab.Ativo'
    end
    object QrOSCabNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
      Origin = 'fatogeradr.Nome'
    end
    object QrOSCabNO_ESTATUS: TWideStringField
      FieldName = 'NO_ESTATUS'
      Origin = 'estatusoss.Nome'
      Size = 60
    end
    object QrOSCabNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrOSCabSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
      Origin = 'oscab.SiapTerCad'
    end
    object QrOSCabNO_SiapTerCad: TWideStringField
      FieldName = 'NO_SiapTerCad'
      Origin = 'siaptercad.Nome'
      Size = 100
    end
    object QrOSCabTXTVisPrv: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTVisPrv'
      Calculated = True
    end
    object QrOSCabTXTContat: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTContat'
      Calculated = True
    end
    object QrOSCabTXTVisExe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTVisExe'
      Calculated = True
    end
    object QrOSCabTXTExePrv: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExePrv'
      Calculated = True
    end
    object QrOSCabTXTExeIni: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExeIni'
      Calculated = True
    end
    object QrOSCabTXTExeFim: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExeFim'
      Calculated = True
    end
    object QrOSCabDdsPosVda: TIntegerField
      FieldName = 'DdsPosVda'
      Origin = 'oscab.DdsPosVda'
      DisplayFormat = '00'
    end
    object QrOSCabEntiContat: TIntegerField
      FieldName = 'EntiContat'
      Origin = 'oscab.EntiContat'
    end
    object QrOSCabNumContrat: TIntegerField
      FieldName = 'NumContrat'
      Origin = 'oscab.NumContrat'
    end
    object QrOSCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
      Origin = 'oscab.EntPagante'
    end
    object QrOSCabEntContrat: TIntegerField
      FieldName = 'EntContrat'
      Origin = 'oscab.EntContrat'
    end
    object QrOSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'oscab.Empresa'
    end
    object QrOSCabNO_PAG: TWideStringField
      FieldName = 'NO_PAG'
      Size = 100
    end
    object QrOSCabNO_CTR: TWideStringField
      FieldName = 'NO_CTR'
      Size = 100
    end
    object QrOSCabDtaLibFat: TDateTimeField
      FieldName = 'DtaLibFat'
      Origin = 'oscab.DtaLibFat'
    end
    object QrOSCabDtaFimFat: TDateTimeField
      FieldName = 'DtaFimFat'
      Origin = 'oscab.DtaFimFat'
    end
    object QrOSCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
      Origin = 'oscab.CondicaoPg'
    end
    object QrOSCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Origin = 'oscab.CartEmis'
    end
    object QrOSCabNO_CART: TWideStringField
      FieldName = 'NO_CART'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrOSCabNO_PRZ: TWideStringField
      FieldName = 'NO_PRZ'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrOSCabSerNF: TWideStringField
      FieldName = 'SerNF'
      Origin = 'oscab.SerNF'
      Size = 3
    end
    object QrOSCabNumNF: TIntegerField
      FieldName = 'NumNF'
      Origin = 'oscab.NumNF'
    end
    object QrOSCabNO_ENTICONTAT: TWideStringField
      FieldName = 'NO_ENTICONTAT'
      Origin = 'enticontat.Nome'
      Size = 30
    end
    object QrOSCabTel_ENT: TWideStringField
      FieldName = 'Tel_ENT'
    end
    object QrOSCabTXTTel_ENT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTTel_ENT'
      Size = 30
      Calculated = True
    end
    object QrOSCabValorServi: TFloatField
      FieldName = 'ValorServi'
      Origin = 'oscab.ValorServi'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabValorDesco: TFloatField
      FieldName = 'ValorDesco'
      Origin = 'oscab.ValorDesco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabValorOutrs: TFloatField
      FieldName = 'ValorOutrs'
      Origin = 'oscab.ValorOutrs'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabInvalServi: TFloatField
      FieldName = 'InvalServi'
      Origin = 'oscab.InvalServi'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabValorTotal: TFloatField
      FieldName = 'ValorTotal'
      Origin = 'oscab.ValorTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabInvalDesco: TFloatField
      FieldName = 'InvalDesco'
      Origin = 'oscab.InvalDesco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabInvalOutrs: TFloatField
      FieldName = 'InvalOutrs'
      Origin = 'oscab.InvalOutrs'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabInvalTotal: TFloatField
      FieldName = 'InvalTotal'
      Origin = 'oscab.InvalTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabOrcamServi: TFloatField
      FieldName = 'OrcamServi'
      Origin = 'oscab.OrcamServi'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabOrcamDesco: TFloatField
      FieldName = 'OrcamDesco'
      Origin = 'oscab.OrcamDesco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabOrcamOutrs: TFloatField
      FieldName = 'OrcamOutrs'
      Origin = 'oscab.OrcamOutrs'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabOrcamTotal: TFloatField
      FieldName = 'OrcamTotal'
      Origin = 'oscab.OrcamTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabValiDdOrca: TIntegerField
      FieldName = 'ValiDdOrca'
      Origin = 'oscab.ValiDdOrca'
    end
    object QrOSCabOperacao: TIntegerField
      FieldName = 'Operacao'
      Origin = 'oscab.Operacao'
    end
    object QrOSCabExeTxtCli1: TIntegerField
      FieldName = 'ExeTxtCli1'
      Origin = 'oscab.ExeTxtCli1'
    end
    object QrOSCabNO_ExeTxtCli1: TWideStringField
      FieldName = 'NO_ExeTxtCli1'
      Size = 100
    end
    object QrOSCabExeTxtCli2: TIntegerField
      FieldName = 'ExeTxtCli2'
    end
    object QrOSCabNO_ExeTxtCli2: TWideStringField
      FieldName = 'NO_ExeTxtCli2'
      Size = 100
    end
    object QrOSCabValorPre: TFloatField
      FieldName = 'ValorPre'
    end
    object QrOSCabNO_OPERACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_OPERACAO'
      Size = 255
      Calculated = True
    end
    object QrOSCabObsGaranti: TWideMemoField
      FieldName = 'ObsGaranti'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCabObsExecuta: TWideMemoField
      FieldName = 'ObsExecuta'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCabFimVisPrv: TDateTimeField
      FieldName = 'FimVisPrv'
    end
    object QrOSCabFimVisExe: TDateTimeField
      FieldName = 'FimVisExe'
    end
    object QrOSCabFimExePrv: TDateTimeField
      FieldName = 'FimExePrv'
    end
    object QrOSCabOptado: TSmallintField
      FieldName = 'Optado'
    end
    object QrOSCabGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrOSCabNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrOSCabOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrOSCabAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object QrOSCabNO_AgeEqiCab: TWideStringField
      FieldName = 'NO_AgeEqiCab'
      Size = 255
    end
    object QrOSCabSohInicial: TSmallintField
      FieldName = 'SohInicial'
    end
    object QrOSCabStPipAdPrg: TSmallintField
      FieldName = 'StPipAdPrg'
    end
    object QrOSCabHowGerou: TSmallintField
      FieldName = 'HowGerou'
    end
    object QrOSCabPosGerou: TSmallintField
      FieldName = 'PosGerou'
    end
    object QrOSCabOSFlhUltGe: TIntegerField
      FieldName = 'OSFlhUltGe'
    end
    object QrOSCabMulServico: TLargeintField
      FieldName = 'MulServico'
    end
    object QrOSCabOSFlhGrCab: TIntegerField
      FieldName = 'OSFlhGrCab'
    end
    object QrOSCabOSFlhGrIts: TIntegerField
      FieldName = 'OSFlhGrIts'
    end
    object QrOSCabLstCusPrd: TIntegerField
      FieldName = 'LstCusPrd'
    end
    object QrOSCabLstUplWeb: TDateTimeField
      FieldName = 'LstUplWeb'
      OnGetText = QrOSCabLstUplWebGetText
    end
    object QrOSCabConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSCabLCPUsed: TIntegerField
      FieldName = 'LCPUsed'
    end
    object QrOSCabDtIniMonGr: TDateField
      FieldName = 'DtIniMonGr'
    end
    object QrOSCabMobiliCad: TIntegerField
      FieldName = 'MobiliCad'
    end
    object QrOSCabNO_MobiliCad: TWideStringField
      FieldName = 'NO_MobiliCad'
      Size = 100
    end
    object QrOSCabAgeCorIni: TIntegerField
      FieldName = 'AgeCorIni'
    end
    object QrOSCabAgeCorFon: TIntegerField
      FieldName = 'AgeCorFon'
    end
    object QrOSCabCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object QrOSCabStatusExecucao: TIntegerField
      FieldName = 'StatusExecucao'
    end
    object QrOSCabVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrOSCabNO_Vendedor: TWideStringField
      FieldName = 'NO_Vendedor'
      Size = 100
    end
    object QrOSCabMapPMV: TSmallintField
      FieldName = 'MapPMV'
    end
    object QrOSCabLauInfesInt: TWideMemoField
      FieldName = 'LauInfesInt'
      BlobType = ftWideMemo
      Size = 10
    end
    object QrOSCabLauInfesExt: TWideMemoField
      FieldName = 'LauInfesExt'
      BlobType = ftWideMemo
      Size = 10
    end
    object QrOSCabLauParTec: TWideMemoField
      FieldName = 'LauParTec'
      BlobType = ftWideMemo
      Size = 10
    end
  end
  object DsOSCab: TDataSource
    DataSet = QrOSCab
    Left = 536
  end
  object PMOSSrv: TPopupMenu
    OnPopup = PMOSSrvPopup
    Left = 1764
    Top = 80
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      object Servioatual1: TMenuItem
        Caption = 'Servi'#231'o atual'
        OnClick = Servioatual1Click
      end
      object odosservios1: TMenuItem
        Caption = '&Todos servi'#231'os'
        OnClick = odosservios1Click
      end
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Ratificaconsumosprevistos2: TMenuItem
      Caption = 'Ratifica consumos previstos'
      object Aplicaes1: TMenuItem
        Caption = 'Aplica'#231#245'es'
        OnClick = Aplicaes1Click
      end
      object Monitoramentos1: TMenuItem
        Caption = 'Monitoramentos'
        OnClick = Monitoramentos1Click
      end
      object ApliceMonit1: TMenuItem
        Caption = 'Aplic. e Monit.'
        OnClick = ApliceMonit1Click
      end
      object odaOS2: TMenuItem
        Caption = 'Todos servi'#231'os do localizador'
        OnClick = odaOS2Click
      end
    end
    object N16: TMenuItem
      Caption = '-'
    end
    object Recalcula1: TMenuItem
      Caption = 'Recalcula progresso de aplica'#231#245'es'
      OnClick = Recalcula1Click
    end
    object AdicionanovosPMVsemOSsfuturas1: TMenuItem
      Caption = 'Adiciona PMVs em OSs futuras'
      OnClick = AdicionanovosPMVsemOSsfuturas1Click
    end
  end
  object PMOSCab: TPopupMenu
    OnPopup = PMOSCabPopup
    Left = 1652
    Top = 80
    object CabInclui1: TMenuItem
      Caption = '&Inclui nova proforma'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera localizador'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object Alteraproformadolocalizador1: TMenuItem
      Caption = 'Altera proforma do localizador'
      OnClick = Alteraproformadolocalizador1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui localizador'
      object Comsegurana1: TMenuItem
        Caption = '&Com seguran'#231'a'
        OnClick = Comsegurana1Click
      end
      object Incondicional1: TMenuItem
        Caption = '&Incondicional'
        object odaOS1: TMenuItem
          Caption = '&Tudo inclusive cabe'#231'alho'
          OnClick = odaOS1Click
        end
        object Somenteseusservios1: TMenuItem
          Caption = '&Somente seus servi'#231'os'
          OnClick = Somenteseusservios1Click
        end
      end
    end
    object N14: TMenuItem
      Caption = '-'
    end
    object Duplica1: TMenuItem
      Caption = 'Replica toda proforma'
      OnClick = Duplica1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ReplicaLugarOpo1: TMenuItem
      Caption = 'Copia localizador atual'
      OnClick = ReplicaLugarOpo1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Adiodenovooramento1: TMenuItem
      Caption = 'Adi'#231#227'o de novo or'#231'amento'
      OnClick = Adiodenovooramento1Click
    end
    object Adiodenovolugar1: TMenuItem
      Caption = 'Adi'#231#227'o de novo lugar'
      Enabled = False
      Visible = False
      OnClick = Adiodenovolugar1Click
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object Agentes1: TMenuItem
      Caption = '&Agentes'
      object Adicionaagentes1: TMenuItem
        Caption = '&Adiciona agente(s)'
        OnClick = Adicionaagentes1Click
      end
      object Retiraagente1: TMenuItem
        Caption = '&Retira agente'
        OnClick = Retiraagente1Click
      end
    end
    object N13: TMenuItem
      Caption = '-'
    end
    object Encerra1: TMenuItem
      Caption = 'Ence&rrar'
      OnClick = Encerra1Click
    end
    object N20: TMenuItem
      Caption = '-'
    end
    object UploadNuvem1: TMenuItem
      Caption = 'Upload Nuvem'
      OnClick = UploadNuvem1Click
    end
  end
  object PMOSFrmCab: TPopupMenu
    OnPopup = PMOSFrmCabPopup
    Left = 1820
    Top = 80
    object Cabealho4: TMenuItem
      Caption = '&Cabe'#231'alho'
      object ItsInclui4: TMenuItem
        Caption = '&Adiciona'
        Enabled = False
        OnClick = ItsInclui4Click
      end
      object ItsAltera4: TMenuItem
        Caption = '&Edita'
        Enabled = False
        OnClick = ItsAltera4Click
      end
      object ItsExclui4: TMenuItem
        Caption = '&Remove'
        Enabled = False
        OnClick = ItsExclui4Click
        object Atual3: TMenuItem
          Caption = '&Atual'
          OnClick = Atual3Click
        end
        object odos2: TMenuItem
          Caption = '&Todos'
          OnClick = odos2Click
        end
      end
    end
    object Itens4: TMenuItem
      Caption = '&Itens'
      OnClick = Itens4Click
    end
    object Abrangncia1: TMenuItem
      Caption = '&Abrang'#234'ncia'
      OnClick = Abrangncia1Click
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object Alteraodeconsumorealizado1: TMenuItem
      Caption = 'Altera'#231#227'o de consumo realizado'
      OnClick = Alteraodeconsumorealizado1Click
    end
    object Ratificaodeconsumosprevistos1: TMenuItem
      Caption = 'Ratifica'#231#227'o de consumos previstos'
      OnClick = Ratificaodeconsumosprevistos1Click
    end
    object N11: TMenuItem
      Caption = '-'
    end
  end
  object PMOSCabAlv: TPopupMenu
    OnPopup = PMOSCabAlvPopup
    Left = 592
    object IncluiOSCabAlv1: TMenuItem
      Caption = '&Adiciona, muda ou retira pragas alvo'
      OnClick = IncluiOSCabAlv1Click
    end
    object ExcluiOSCabAlv1: TMenuItem
      Caption = '&Retira a praga alvo selecionada'
      OnClick = ExcluiOSCabAlv1Click
    end
  end
  object PMOSMonCab: TPopupMenu
    OnPopup = PMOSMonCabPopup
    Left = 1848
    Top = 80
    object Cabecalho5: TMenuItem
      Caption = '&Cabe'#231'alho'
      object ItsInclui5: TMenuItem
        Caption = '&Adiciona'
        Enabled = False
        OnClick = ItsInclui5Click
      end
      object ItsAltera5: TMenuItem
        Caption = '&Edita'
        Enabled = False
        object Cabealho1: TMenuItem
          Caption = 'Cabe'#231'alho'
          OnClick = Cabealho1Click
        end
        object PIP1: TMenuItem
          Caption = 'PMV'
          OnClick = PIP1Click
        end
        object N21: TMenuItem
          Caption = '-'
        end
        object AlteradatadaltimaemissodeOSsfilhas1: TMenuItem
          Caption = 'Altera data da '#250'ltima emiss'#227'o de OSs filhas'
          OnClick = AlteradatadaltimaemissodeOSsfilhas1Click
        end
      end
      object ItsExclui5: TMenuItem
        Caption = '&Remove'
        object Frmulaoatual1: TMenuItem
          Caption = '&Formula'#231#227'o atual'
          OnClick = Frmulaoatual1Click
        end
        object odasformulaesdesteservio1: TMenuItem
          Caption = 'Todas formula'#231#245'es deste servi'#231'o'
          OnClick = odasformulaesdesteservio1Click
        end
      end
    end
    object Itens5: TMenuItem
      Caption = '&Itens'
      OnClick = Itens5Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Ratificaconsumosprevistos1: TMenuItem
      Caption = '&Ratifica consumos previstos'
      OnClick = Ratificaconsumosprevistos1Click
    end
    object Alteraodeconsumorealizado2: TMenuItem
      Caption = '&Altera'#231#227'o de consumo realizado'
      OnClick = Alteraodeconsumorealizado2Click
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object AdicionaPMVsemOSsFuturas1: TMenuItem
      Caption = 'Adiciona &PMVs em OSs Futuras'
      OnClick = AdicionaPMVsemOSsFuturas1Click
    end
    object N17: TMenuItem
      Caption = '-'
    end
    object ImprimeQRCode1: TMenuItem
      Caption = 'Imprime &QRCode'
      OnClick = ImprimeQRCode1Click
    end
  end
  object PMFat: TPopupMenu
    OnPopup = PMFatPopup
    Left = 1736
    Top = 80
    object Adicionanovacondiodepagamento1: TMenuItem
      Caption = '&Adiciona nova condi'#231#227'o de pagamento'
      OnClick = Adicionanovacondiodepagamento1Click
    end
    object Editaacondiodepagamentoatual1: TMenuItem
      Caption = '&Edita a condi'#231#227'o de pagamento atual'
      OnClick = Editaacondiodepagamentoatual1Click
    end
    object Removecondiaodepagamentoatual1: TMenuItem
      Caption = '&Remove condi'#231'ao de pagamento atual'
      OnClick = Removecondiaodepagamentoatual1Click
    end
    object N15: TMenuItem
      Caption = '-'
    end
    object Fatura1: TMenuItem
      Caption = '&Fatura todos servi'#231'os da OS'
      OnClick = Fatura1Click
    end
    object Gerabloqueto1: TMenuItem
      Caption = 'Emite boleto(s)'
      OnClick = Gerabloqueto1Click
    end
    object Visualizarbloquetos1: TMenuItem
      Caption = 'Visualizar bloquetos'
      OnClick = Visualizarbloquetos1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Excluifaturamento1: TMenuItem
      Caption = 'Exclui todo faturamento'
      OnClick = Excluifaturamento1Click
    end
  end
  object PMOSPos: TPopupMenu
    OnPopup = PMOSCabPopup
    Left = 1792
    Top = 80
    object Incluialertacomercial1: TMenuItem
      Caption = '&Inclui alerta comercial'
      object Incluiporprcadastro1: TMenuItem
        Caption = '&M'#250'ltiplos por pr'#233' cadastro'
        OnClick = Incluiporprcadastro1Click
      end
      object IncluiC3: TMenuItem
        Caption = '&'#218'nico manualmente'
        OnClick = IncluiC3Click
      end
    end
    object AlteraC3: TMenuItem
      Caption = '&Altera o alerta selecionado'
      OnClick = AlteraC3Click
    end
    object ExcluiC3: TMenuItem
      Caption = '&Exclui o alerta selecionado'
      OnClick = ExcluiC3Click
    end
  end
  object PMOSCxa: TPopupMenu
    OnPopup = PMOSCxaPopup
    Left = 1680
    Top = 80
    object ItsInclui2: TMenuItem
      Caption = '&Adiciona caixa d'#180#224'gua'
      OnClick = ItsInclui2Click
    end
    object ItsAltera2: TMenuItem
      Caption = '&Edita caixa d'#180#224'gua'
      Enabled = False
      OnClick = ItsAltera2Click
    end
    object ItsExclui2: TMenuItem
      Caption = '&Retira caixa d'#180#224'gua'
      Enabled = False
      OnClick = ItsExclui2Click
      object Atual1: TMenuItem
        Caption = '&Atual'
        OnClick = Atual1Click
      end
      object odas1: TMenuItem
        Caption = '&Todas'
        OnClick = odas1Click
      end
    end
  end
  object PMOSPrz: TPopupMenu
    OnPopup = PMOSPrzPopup
    Left = 1652
    Top = 124
    object OSPrz1_Inclui: TMenuItem
      Caption = '&Adiciona nova condi'#231#227'o de pagamento'
      Enabled = False
      OnClick = OSPrz1_IncluiClick
    end
    object OSPrz1_Altera: TMenuItem
      Caption = '&Edita a condi'#231#227'o de pagamento atual'
      Enabled = False
      OnClick = OSPrz1_AlteraClick
    end
    object OSPrz1_Exclui: TMenuItem
      Caption = '&Remove condi'#231'ao de pagamento atual'
      Enabled = False
      OnClick = OSPrz1_ExcluiClick
    end
  end
  object PMOSPipMon: TPopupMenu
    OnPopup = PMOSPipMonPopup
    Left = 1680
    Top = 124
    object AdicionaPIPsativos1: TMenuItem
      Caption = '&Adiciona PMVs'
      Enabled = False
      OnClick = AdicionaPIPsativos1Click
    end
    object RemovePIPs1: TMenuItem
      Caption = '&Remove PMVs'
      OnClick = RemovePIPs1Click
      object Atual2: TMenuItem
        Caption = '&Atual'
        OnClick = Atual2Click
      end
      object odos1: TMenuItem
        Caption = '&Todos'
        Enabled = False
        OnClick = odos1Click
      end
      object Selecionados1: TMenuItem
        Caption = '&Selecionados'
        OnClick = Selecionados1Click
      end
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Gerarperguntas1: TMenuItem
      Caption = 'Gerar perguntas'
      OnClick = Gerarperguntas1Click
    end
    object Excluirperguntasgeradas1: TMenuItem
      Caption = 'Excluir perguntas geradas'
      object Atual4: TMenuItem
        Caption = '&Atual'
        OnClick = Atual4Click
      end
      object odasdoPIPSelecionado1: TMenuItem
        Caption = 'Todas do PMV &Selecionado'
        OnClick = odasdoPIPSelecionado1Click
      end
      object odasdetodosPIPs1: TMenuItem
        Caption = '&Todas de todos PMVs'
        OnClick = odasdetodosPIPs1Click
      end
    end
    object N22: TMenuItem
      Caption = '-'
    end
    object Responderperguntas1: TMenuItem
      Caption = 'Responder perguntas'
      OnClick = Responderperguntas1Click
    end
    object N19: TMenuItem
      Caption = '-'
    end
    object CadastrodoPMV1: TMenuItem
      Caption = 'Cadastro do PMV'
      OnClick = CadastrodoPMV1Click
    end
    object Cadastrodeperguntas1: TMenuItem
      Caption = 'Cadastro de perguntas'
      OnClick = Cadastrodeperguntas1Click
    end
  end
  object PMOSCxI: TPopupMenu
    OnPopup = PMOSCxIPopup
    Left = 1708
    Top = 80
    object ItsInclui6: TMenuItem
      Caption = '&Inclui foto da caixa d'#39#225'gua'
      OnClick = ItsInclui6Click
    end
    object ItsAltera6: TMenuItem
      Caption = '&Altera foto da caixa d'#39#225'gua'
      OnClick = ItsAltera6Click
    end
    object ItsExclui6: TMenuItem
      Caption = '&Exclui foto da caixa d'#39#225'gua'
      OnClick = ItsExclui6Click
    end
  end
  object PMOSAge: TPopupMenu
    OnPopup = PMOSAgePopup
    Left = 620
    object IncluiOSAge1: TMenuItem
      Caption = '&Adiciona agente(s)'
      OnClick = IncluiOSAge1Click
    end
    object RemoveOSAge1: TMenuItem
      Caption = '&Retira agente'
      OnClick = RemoveOSAge1Click
    end
    object N24: TMenuItem
      Caption = '-'
    end
    object Cadastrodeagentes1: TMenuItem
      Caption = '&Cadastro de agentes'
      OnClick = Cadastrodeagentes1Click
    end
  end
  object QrLctFatRef: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctFatRefCalcFields
    SQL.Strings = (
      'SELECT lfr.* '
      'FROM lctfatref lfr '
      'WHERE lfr.Controle=0')
    Left = 1764
    Top = 168
    object QrLctFatRefPARCELA: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'PARCELA'
      Calculated = True
    end
    object QrLctFatRefCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLctFatRefControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctFatRefConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrLctFatRefLancto: TLargeintField
      FieldName = 'Lancto'
    end
    object QrLctFatRefValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctFatRefVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsLctFatRef: TDataSource
    DataSet = QrLctFatRef
    Left = 1792
    Top = 168
  end
  object QrOSAlv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prc.Nome NO_Praga_Z, osa.*'
      'FROM osalv osa'
      'LEFT JOIN praga_z prc ON prc.Codigo=osa.Praga_Z'
      'WHERE osa.Controle=:P0'
      '')
    Left = 1652
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSAlvCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osalv.Codigo'
    end
    object QrOSAlvControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osalv.Controle'
    end
    object QrOSAlvConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'osalv.Conta'
    end
    object QrOSAlvNO_Praga_Z: TWideStringField
      FieldName = 'NO_Praga_Z'
      Origin = 'praga_z.Nome'
      Size = 60
    end
    object QrOSAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
      Origin = 'osalv.Praga_Z'
    end
  end
  object DsOSAlv: TDataSource
    DataSet = QrOSAlv
    Left = 1680
    Top = 168
  end
  object QrOSSrv: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSSrvBeforeClose
    AfterScroll = QrOSSrvAfterScroll
    OnCalcFields = QrOSSrvCalcFields
    SQL.Strings = (
      'SELECT des.Nome NO_DesServico, '
      'des.Sigla NO_SIGLA, srv.* '
      'FROM ossrv srv '
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico '
      'WHERE srv.Codigo=:P0')
    Left = 1652
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSSrvNO_DesServico: TWideStringField
      FieldName = 'NO_DesServico'
      Size = 60
    end
    object QrOSSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSSrvDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrOSSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSSrvGarantiaDd: TIntegerField
      FieldName = 'GarantiaDd'
      DisplayFormat = '00'
    end
    object QrOSSrvHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
      DisplayFormat = '00'
    end
    object QrOSSrvHrExecutar: TFloatField
      FieldName = 'HrExecutar'
      DisplayFormat = '00'
    end
    object QrOSSrvValCalc: TFloatField
      FieldName = 'ValCalc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvValInfo: TFloatField
      FieldName = 'ValInfo'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvValDesc: TFloatField
      FieldName = 'ValDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvValTota: TFloatField
      FieldName = 'ValTota'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvAutorizado: TSmallintField
      FieldName = 'Autorizado'
      MaxValue = 1
    end
    object QrOSSrvVAL_CALCeINFO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_CALCeINFO'
      Calculated = True
    end
    object QrOSSrvAUTORIZADO_BOOL: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'AUTORIZADO_BOOL'
      Calculated = True
    end
    object QrOSSrvNO_SIGLA: TWideStringField
      FieldName = 'NO_SIGLA'
      Size = 10
    end
    object QrOSSrvDetalhes: TWideMemoField
      FieldName = 'Detalhes'
      BlobType = ftWideMemo
    end
    object QrOSSrvTudoFeitoA: TSmallintField
      FieldName = 'TudoFeitoA'
      MaxValue = 1
    end
    object QrOSSrvTudoFeitoM: TSmallintField
      FieldName = 'TudoFeitoM'
      MaxValue = 1
    end
    object QrOSSrvTUDOFEITO: TFloatField
      FieldName = 'TUDOFEITO'
    end
    object QrOSSrvMoniDdTotl: TIntegerField
      FieldName = 'MoniDdTotl'
    end
    object QrOSSrvMoniDdIntv: TIntegerField
      FieldName = 'MoniDdIntv'
    end
    object QrOSSrvTITULO_FORMULAS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TITULO_FORMULAS'
      Size = 512
      Calculated = True
    end
  end
  object DsOSSrv: TDataSource
    DataSet = QrOSSrv
    Left = 1680
    Top = 212
  end
  object QrOSCxa: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSCxaBeforeClose
    AfterScroll = QrOSCxaAfterScroll
    SQL.Strings = (
      'SELECT cxa.*, cxm.Nome NO_MATERIAL,'
      'cxf.Nome NOME_FORMA,'
      'six.MatersCxa, six.FormasCxa, six.VolumeL,'
      'six.Local, six.Acesso, six.Medidas'
      'FROM oscxa cxa'
      'LEFT JOIN siapimacxa six ON six.Controle=cxa.Caixa'
      'LEFT JOIN cxamaters cxm ON'
      '  cxm.Codigo=six.MatersCxa'
      'LEFT JOIN cxaformas cxf ON'
      '  cxf.Codigo=six.FormasCxa'
      'WHERE cxa.Codigo=:P0')
    Left = 1764
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSCxaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCxaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSCxaCaixa: TIntegerField
      FieldName = 'Caixa'
    end
    object QrOSCxaGarantiaDd: TIntegerField
      FieldName = 'GarantiaDd'
    end
    object QrOSCxaHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
    end
    object QrOSCxaHrExecutar: TFloatField
      FieldName = 'HrExecutar'
    end
    object QrOSCxaValCalc: TFloatField
      FieldName = 'ValCalc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCxaValInfo: TFloatField
      FieldName = 'ValInfo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCxaValDesc: TFloatField
      FieldName = 'ValDesc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCxaValTota: TFloatField
      FieldName = 'ValTota'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCxaAutorizado: TSmallintField
      FieldName = 'Autorizado'
    end
    object QrOSCxaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCxaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCxaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCxaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCxaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCxaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCxaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCxaNO_MATERIAL: TWideStringField
      FieldName = 'NO_MATERIAL'
      Size = 60
    end
    object QrOSCxaNOME_FORMA: TWideStringField
      FieldName = 'NOME_FORMA'
      Size = 60
    end
    object QrOSCxaMatersCxa: TIntegerField
      FieldName = 'MatersCxa'
    end
    object QrOSCxaFormasCxa: TIntegerField
      FieldName = 'FormasCxa'
    end
    object QrOSCxaVolumeL: TFloatField
      FieldName = 'VolumeL'
    end
    object QrOSCxaLocal: TWideStringField
      FieldName = 'Local'
      Size = 255
    end
    object QrOSCxaAcesso: TWideStringField
      FieldName = 'Acesso'
      Size = 255
    end
    object QrOSCxaMedidas: TWideStringField
      FieldName = 'Medidas'
      Size = 255
    end
    object QrOSCxaChekLstCab: TIntegerField
      FieldName = 'ChekLstCab'
    end
    object QrOSCxaDetalhes: TWideMemoField
      FieldName = 'Detalhes'
      BlobType = ftWideMemo
    end
    object QrOSCxaTudoFeito: TSmallintField
      FieldName = 'TudoFeito'
      MaxValue = 1
    end
  end
  object DsOSCxa: TDataSource
    DataSet = QrOSCxa
    Left = 1792
    Top = 212
  end
  object QrOSCxI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ELT(cxi.Aplicacao, "OR'#199'AM", "C.EXE", "AMBOS")'
      'NO_APLICACAO, cxi.* '
      'FROM oscxi cxi '
      'WHERE cxi.Codigo>0')
    Left = 1764
    Top = 256
    object QrOSCxICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCxIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSCxIFotoCxa: TWideStringField
      FieldName = 'FotoCxa'
      Size = 255
    end
    object QrOSCxIDetalhes: TWideMemoField
      FieldName = 'Detalhes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCxILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCxIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCxIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCxIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCxIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCxIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCxIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCxIAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrOSCxINO_APLICACAO: TWideStringField
      FieldName = 'NO_APLICACAO'
      Size = 5
    end
  end
  object DsOSCxI: TDataSource
    DataSet = QrOSCxI
    Left = 1792
    Top = 256
  end
  object QrOSFrmDep: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT osd.*, ELT(osd.Tabela, "I", "M", "?") SIGLA,'
      'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") NO_Campo'
      'FROM osfrmdep osd'
      'LEFT JOIN siapimadep sid ON sid.Controle=osd.Cadastro'
      'LEFT JOIN dependenci dpd ON dpd.Codigo=sid.Dependenci'
      'LEFT JOIN movamovcad mac ON mac.Codigo=osd.Cadastro'
      'WHERE osd.Conta=:P0')
    Left = 1652
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmDepCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmDepControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmDepConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmDepTabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrOSFrmDepCadastro: TIntegerField
      FieldName = 'Cadastro'
    end
    object QrOSFrmDepLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSFrmDepDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSFrmDepDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSFrmDepUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSFrmDepUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSFrmDepAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSFrmDepAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSFrmDepSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 1
    end
    object QrOSFrmDepNO_Campo: TWideStringField
      FieldName = 'NO_Campo'
      Size = 100
    end
    object QrOSFrmDepIDIts: TIntegerField
      FieldName = 'IDIts'
    end
  end
  object DsOSFrmDep: TDataSource
    DataSet = QrOSFrmDep
    Left = 1680
    Top = 256
  end
  object QrOSFrmCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSFrmCabBeforeClose
    AfterScroll = QrOSFrmCabAfterScroll
    OnCalcFields = QrOSFrmCabCalcFields
    SQL.Strings = (
      'SELECT ofc.*, frm.Nome NO_FORMULA, gg1.Nome NO_EquipAplic,'
      'med.CodUsu CU_UNIDMED,'
      'med.SIGLA, med.Nome NO_UNIDMED'
      'FROM osfrmcab ofc'
      'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ofc.EquipAplic'
      'LEFT JOIN gragru1 gg1 ON ggx.Gragru1=gg1.Nivel1'
      'LEFT JOIN unidmed med ON med.Codigo=ofc.UnidMed'
      'WHERE ofc.Controle=:P0'
      ''
      '')
    Left = 1652
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmCabConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOSFrmCabFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrOSFrmCabEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrOSFrmCabQtdTot: TFloatField
      FieldName = 'QtdTot'
    end
    object QrOSFrmCabQtdQSP: TFloatField
      FieldName = 'QtdQSP'
    end
    object QrOSFrmCabCusTot: TFloatField
      FieldName = 'CusTot'
    end
    object QrOSFrmCabNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrOSFrmCabNO_EquipAplic: TWideStringField
      FieldName = 'NO_EquipAplic'
      Size = 60
    end
    object QrOSFrmCabDiluente: TSmallintField
      FieldName = 'Diluente'
    end
    object QrOSFrmCabTipoAplica: TIntegerField
      FieldName = 'TipoAplica'
    end
    object QrOSFrmCabUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrOSFrmCabCU_UNIDMED: TIntegerField
      FieldName = 'CU_UNIDMED'
      Required = True
    end
    object QrOSFrmCabSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrOSFrmCabNO_UNIDMED: TWideStringField
      FieldName = 'NO_UNIDMED'
      Size = 30
    end
    object QrOSFrmCabPercFeito: TFloatField
      FieldName = 'PercFeito'
    end
    object QrOSFrmCabCALC_FEITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_FEITO'
      Calculated = True
    end
    object QrOSFrmCabIndicUso: TSmallintField
      FieldName = 'IndicUso'
    end
  end
  object DsOSFrmCab: TDataSource
    DataSet = QrOSFrmCab
    Left = 1680
    Top = 300
  end
  object QrOSFrmRec: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT med.Sigla SIGLAUNIDMED, gg1.Nome NO_GG1,'
      'ELT(RatifUso+1, "N", "S", "?") NO_RatifUso, pgt.Fracio,'
      'ofr.*'
      'FROM osfrmrec ofr'
      'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX'
      'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed med on med.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ofr.Conta=:P0'
      'ORDER BY Ordem'
      ''
      ''
      '')
    Left = 1652
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmRecSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrOSFrmRecNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrOSFrmRecCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmRecControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmRecConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmRecIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSFrmRecGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOSFrmRecPrvQtd: TFloatField
      FieldName = 'PrvQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSFrmRecPrvPrc: TFloatField
      FieldName = 'PrvPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSFrmRecPrvVal: TFloatField
      FieldName = 'PrvVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSFrmRecUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSFrmRecUsoPrc: TFloatField
      FieldName = 'UsoPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSFrmRecUsoVal: TFloatField
      FieldName = 'UsoVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSFrmRecUsoDec: TFloatField
      FieldName = 'UsoDec'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOSFrmRecUsoTot: TFloatField
      FieldName = 'UsoTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOSFrmRecOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSFrmRecReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrOSFrmRecNO_RatifUso: TWideStringField
      FieldName = 'NO_RatifUso'
      Size = 1
    end
    object QrOSFrmRecEhDiluente: TSmallintField
      FieldName = 'EhDiluente'
    end
    object QrOSFrmRecUsoCusUni: TFloatField
      FieldName = 'UsoCusUni'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSFrmRecUsoCusTot: TFloatField
      FieldName = 'UsoCusTot'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSFrmRecRatifUso: TSmallintField
      FieldName = 'RatifUso'
    end
    object QrOSFrmRecFracio: TSmallintField
      FieldName = 'Fracio'
    end
  end
  object DsOSFrmRec: TDataSource
    DataSet = QrOSFrmRec
    Left = 1680
    Top = 344
  end
  object QrOSFrmAbr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT abr.Nome NO_ABRANGE, ofa.*'
      'FROM osfrmabr ofa'
      'LEFT JOIN abrangicie abr ON abr.Codigo=ofa.Abrangicie'
      'WHERE ofa.Conta=:P0')
    Left = 1652
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmAbrNO_ABRANGE: TWideStringField
      FieldName = 'NO_ABRANGE'
      Size = 60
    end
    object QrOSFrmAbrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmAbrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmAbrConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmAbrIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSFrmAbrAbrangicie: TIntegerField
      FieldName = 'Abrangicie'
    end
  end
  object DsOSFrmAbr: TDataSource
    DataSet = QrOSFrmAbr
    Left = 1680
    Top = 388
  end
  object QrOSMonCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSMonCabBeforeClose
    AfterScroll = QrOSMonCabAfterScroll
    SQL.Strings = (
      
        'SELECT plc.Nome NO_prglstcab, dep.Nome NO_DEPENDENCI, ofc.*, mon' +
        '.Nome NO_FORMULA, '
      'gg1.Nome NO_EquipAplic, pip.Nome NO_PIP,'
      'med.CodUsu CU_UNIDMED,'
      'med.SIGLA, med.Nome NO_UNIDMED'
      'FROM osmoncab ofc'
      'LEFT JOIN formulas mon ON mon.Codigo=ofc.Formula'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ofc.EquipAplic'
      'LEFT JOIN gragru1 gg1 ON ggx.Gragru1=gg1.Nivel1'
      'LEFT JOIN pipcad  pip ON pip.Codigo=ofc.PipCad'
      'LEFT JOIN unidmed med ON med.Codigo=ofc.UnidMed'
      'LEFT JOIN prglstcab plc ON plc.Codigo=pip.PrgLstCab'
      'LEFT JOIN siapimadep sid ON sid.Controle=pip.Dependenci'
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci'
      'WHERE ofc.Controle=:P0')
    Left = 1652
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSMonCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osmoncab.Codigo'
    end
    object QrOSMonCabControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osmoncab.Controle'
    end
    object QrOSMonCabConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'osmoncab.Conta'
    end
    object QrOSMonCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'osmoncab.Nome'
      Size = 60
    end
    object QrOSMonCabFormula: TIntegerField
      FieldName = 'Formula'
      Origin = 'osmoncab.Formula'
    end
    object QrOSMonCabEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
      Origin = 'osmoncab.EquipAplic'
    end
    object QrOSMonCabQtdTot: TFloatField
      FieldName = 'QtdTot'
      Origin = 'osmoncab.QtdTot'
    end
    object QrOSMonCabQtdQSP: TFloatField
      FieldName = 'QtdQSP'
      Origin = 'osmoncab.QtdQSP'
    end
    object QrOSMonCabCusTot: TFloatField
      FieldName = 'CusTot'
      Origin = 'osmoncab.CusTot'
    end
    object QrOSMonCabNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Origin = 'formulas.Nome'
      Size = 60
    end
    object QrOSMonCabNO_EquipAplic: TWideStringField
      FieldName = 'NO_EquipAplic'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrOSMonCabPipCad: TIntegerField
      FieldName = 'PipCad'
      Origin = 'osmoncab.PipCad'
    end
    object QrOSMonCabNO_PIP: TWideStringField
      FieldName = 'NO_PIP'
      Origin = 'pipcad.Nome'
      Size = 30
    end
    object QrOSMonCabDiluente: TIntegerField
      FieldName = 'Diluente'
    end
    object QrOSMonCabTipoAplica: TIntegerField
      FieldName = 'TipoAplica'
    end
    object QrOSMonCabUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrOSMonCabCU_UNIDMED: TIntegerField
      FieldName = 'CU_UNIDMED'
      Required = True
    end
    object QrOSMonCabSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrOSMonCabNO_UNIDMED: TWideStringField
      FieldName = 'NO_UNIDMED'
      Size = 30
    end
    object QrOSMonCabPerioDd: TIntegerField
      FieldName = 'PerioDd'
    end
    object QrOSMonCabDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
    object QrOSMonCabNO_prglstcab: TWideStringField
      FieldName = 'NO_prglstcab'
      Size = 100
    end
    object QrOSMonCabNO_DEPENDENCI: TWideStringField
      FieldName = 'NO_DEPENDENCI'
      Size = 60
    end
    object QrOSMonCabEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
    end
  end
  object DsOSMonCab: TDataSource
    DataSet = QrOSMonCab
    Left = 1680
    Top = 432
  end
  object _Qr_OS_Mon_Dep_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT osd.*, ELT(osd.Tabela, "I", "M", "?") SIGLA,'
      'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") NO_Campo'
      'FROM osmondep osd'
      'LEFT JOIN siapimadep sid ON sid.Controle=osd.Cadastro'
      'LEFT JOIN dependenci dpd ON dpd.Codigo=sid.Dependenci'
      'LEFT JOIN movamovcad mac ON mac.Codigo=osd.Cadastro'
      'WHERE osd.Conta=:P0'
      '')
    Left = 1652
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object _Qr_OS_Mon_Dep_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object _Qr_OS_Mon_Dep_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object _Qr_OS_Mon_Dep_Conta: TIntegerField
      FieldName = 'Conta'
    end
    object _Qr_OS_Mon_Dep_Tabela: TSmallintField
      FieldName = 'Tabela'
    end
    object _Qr_OS_Mon_Dep_Cadastro: TIntegerField
      FieldName = 'Cadastro'
    end
    object _Qr_OS_Mon_Dep_Lk: TIntegerField
      FieldName = 'Lk'
    end
    object _Qr_OS_Mon_Dep_DataCad: TDateField
      FieldName = 'DataCad'
    end
    object _Qr_OS_Mon_Dep_DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object _Qr_OS_Mon_Dep_UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object _Qr_OS_Mon_Dep_UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object _Qr_OS_Mon_Dep_AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object _Qr_OS_Mon_Dep_Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object _Qr_OS_Mon_Dep_SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 1
    end
    object _Qr_OS_Mon_Dep_NO_Campo: TWideStringField
      FieldName = 'NO_Campo'
      Size = 100
    end
    object _Qr_OS_Mon_Dep_IDIts: TIntegerField
      FieldName = 'IDIts'
    end
  end
  object _Ds_OS_Mon_Dep_: TDataSource
    DataSet = _Qr_OS_Mon_Dep_
    Left = 1680
    Top = 476
  end
  object QrOSMonRec: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT med.Sigla SIGLAUNIDMED, gg1.Nome NO_GG1,'
      'ELT(RatifUso+1, "N", "S", "?") NO_RatifUso, pgt.Fracio,'
      'ofr.*'
      'FROM osmonrec ofr'
      'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX'
      'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed med on med.Codigo=gg1.UnidMed'#11
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ofr.Conta=:P0'
      'ORDER BY Ordem'
      '')
    Left = 1652
    Top = 520
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSMonRecNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrOSMonRecCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSMonRecControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSMonRecConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSMonRecIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSMonRecGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOSMonRecPrvQtd: TFloatField
      FieldName = 'PrvQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSMonRecPrvPrc: TFloatField
      FieldName = 'PrvPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSMonRecPrvVal: TFloatField
      FieldName = 'PrvVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSMonRecUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSMonRecUsoPrc: TFloatField
      FieldName = 'UsoPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSMonRecUsoVal: TFloatField
      FieldName = 'UsoVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrOSMonRecUsoDec: TFloatField
      FieldName = 'UsoDec'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOSMonRecUsoTot: TFloatField
      FieldName = 'UsoTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOSMonRecOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSMonRecReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrOSMonRecValCliDd: TIntegerField
      FieldName = 'ValCliDd'
    end
    object QrOSMonRecEhDiluente: TSmallintField
      FieldName = 'EhDiluente'
    end
    object QrOSMonRecNO_RatifUso: TWideStringField
      FieldName = 'NO_RatifUso'
      Size = 1
    end
    object QrOSMonRecRatifUso: TSmallintField
      FieldName = 'RatifUso'
    end
    object QrOSMonRecUsoCusUni: TFloatField
      FieldName = 'UsoCusUni'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSMonRecUsoCusTot: TFloatField
      FieldName = 'UsoCusTot'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSMonRecDesativado: TSmallintField
      FieldName = 'Desativado'
    end
    object QrOSMonRecSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrOSMonRecFracio: TSmallintField
      FieldName = 'Fracio'
    end
  end
  object DsOSMonRec: TDataSource
    DataSet = QrOSMonRec
    Left = 1680
    Top = 520
  end
  object QrOSPos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(Aplicacao=2, pre.Nome, "") NO_AplicID, '
      
        'ELT(Aplicacao+1, "Nenhum", "Visita (O.S.)", "Envio de Email") NO' +
        '_Aplicacao, '
      'osp.* '
      'FROM ospos osp'
      'LEFT JOIN preemail pre ON pre.Codigo=osp.AplicID'
      'WHERE osp.Codigo =:P0'
      'ORDER BY osp.Dias')
    Left = 1652
    Top = 564
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSPosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSPosDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrOSPosNO_AplicID: TWideStringField
      FieldName = 'NO_AplicID'
      Size = 100
    end
    object QrOSPosNO_Aplicacao: TWideStringField
      FieldName = 'NO_Aplicacao'
      Size = 14
    end
    object QrOSPosAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrOSPosAplicID: TIntegerField
      FieldName = 'AplicID'
    end
    object QrOSPosEmitido: TIntegerField
      FieldName = 'Emitido'
    end
    object QrOSPosExecutado: TIntegerField
      FieldName = 'Executado'
    end
    object QrOSPosExeCodi: TIntegerField
      FieldName = 'ExeCodi'
    end
    object QrOSPosExeCtrl: TIntegerField
      FieldName = 'ExeCtrl'
    end
  end
  object DsOSPos: TDataSource
    DataSet = QrOSPos
    Left = 1680
    Top = 564
  end
  object QrOSPrz: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOSPrzCalcFields
    SQL.Strings = (
      'SELECT opz.Parcelas, '
      'opz.Codigo, opz.Controle, opz.Condicao, '
      'opz.DescoPer, opz.Escolhido, ppc.Nome NO_CONDICAO'
      'FROM osprz opz'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=opz.Condicao'
      'WHERE opz.Codigo=:P0')
    Left = 1652
    Top = 608
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSPrzCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osprz.Codigo'
    end
    object QrOSPrzControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osprz.Controle'
    end
    object QrOSPrzCondicao: TIntegerField
      FieldName = 'Condicao'
      Origin = 'osprz.Condicao'
    end
    object QrOSPrzNO_CONDICAO: TWideStringField
      FieldName = 'NO_CONDICAO'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrOSPrzDescoPer: TFloatField
      FieldName = 'DescoPer'
      Origin = 'osprz.DescoPer'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrOSPrzEscolhido: TSmallintField
      FieldName = 'Escolhido'
      Origin = 'osprz.Escolhido'
      MaxValue = 1
    end
    object QrOSPrzParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrOSPrzVAL_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrOSPrzVAL_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrOSPrzORC_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ORC_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrOSPrzORC_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ORC_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrOSPrzINV_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'INV_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrOSPrzINV_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'INV_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
  end
  object DsOSPrz: TDataSource
    DataSet = QrOSPrz
    Left = 1680
    Top = 608
  end
  object QrOSAge: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Nome NO_AGENTE, age.* '
      'FROM osage age'
      'LEFT JOIN entidades ent on ent.Codigo=age.Agente'
      'WHERE age.Codigo=:P0')
    Left = 1764
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSAgeNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
    object QrOSAgeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSAgeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSAgeAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrOSAgeResponsa: TSmallintField
      FieldName = 'Responsa'
    end
  end
  object DsOSAge: TDataSource
    DataSet = QrOSAge
    Left = 1792
    Top = 300
  end
  object QrOSCabAlv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT oca.*, IF(oca.Praga_Z <> 0, '#39'E'#39', '#39'G'#39') NO_NIVEL, '
      'IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) NO_PRAGA'
      'FROM oscabalv oca'
      'LEFT JOIN praga_a pca ON pca.Codigo=oca.Praga_A'
      'LEFT JOIN praga_z pcz ON pcz.Codigo=oca.Praga_Z'
      'WHERE oca.Codigo=:P0'
      'ORDER BY oca.Ordem')
    Left = 1652
    Top = 652
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSCabAlvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabAlvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSCabAlvOrdem: TSmallintField
      FieldName = 'Ordem'
    end
    object QrOSCabAlvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCabAlvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCabAlvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCabAlvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCabAlvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCabAlvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCabAlvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCabAlvPraga_A: TIntegerField
      FieldName = 'Praga_A'
    end
    object QrOSCabAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
    end
    object QrOSCabAlvNO_NIVEL: TWideStringField
      FieldName = 'NO_NIVEL'
      Required = True
      Size = 1
    end
    object QrOSCabAlvNO_PRAGA: TWideStringField
      FieldName = 'NO_PRAGA'
      Size = 60
    end
  end
  object DsOSCabAlv: TDataSource
    DataSet = QrOSCabAlv
    Left = 1680
    Top = 652
  end
  object QrEntiMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto, ema.Ordem'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      'ORDER BY ema.Ordem, ema.Conta'#10)
    Left = 1708
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMailNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEntiMailConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrEntiMailEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
    object QrEntiMailOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsEntiMail: TDataSource
    DataSet = QrEntiMail
    Left = 1736
    Top = 168
  end
  object QrEntiTel: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiTelCalcFields
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 1708
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTelNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrEntiTelConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrEntiTelTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrEntiTelEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrEntiTelTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiTelRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object DsEntiTel: TDataSource
    DataSet = QrEntiTel
    Left = 1736
    Top = 212
  end
  object QrOSPipMon: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSPipMonBeforeClose
    AfterScroll = QrOSPipMonAfterScroll
    SQL.Strings = (
      'SELECT plc.Nome NO_PrgLstCab, pip.Nome NO_PIP, opm.* '
      'FROM ospipmon opm '
      'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad '
      'LEFT JOIN prglstcab plc ON plc.Codigo=opm.PrgLstCab'
      'WHERE opm.Codigo=:P0'
      ''
      ''
      ''
      '')
    Left = 1764
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSPipMonNO_PIP: TWideStringField
      FieldName = 'NO_PIP'
      Size = 30
    end
    object QrOSPipMonCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPipMonControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSPipMonPipCad: TIntegerField
      FieldName = 'PipCad'
    end
    object QrOSPipMonPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrOSPipMonNO_PrgLstCab: TWideStringField
      FieldName = 'NO_PrgLstCab'
      Size = 100
    end
    object QrOSPipMonOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSPipMonReordem: TIntegerField
      FieldName = 'Reordem'
    end
  end
  object DsOSPipMon: TDataSource
    DataSet = QrOSPipMon
    Left = 1792
    Top = 344
  end
  object QrOSCxaAtrib: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts,'
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,'
      'cad.Nome NO_CAD, its.Nome NO_ITS'
      'FROM atrsicxdef def'
      'LEFT JOIN atrsicxits its ON its.Controle=def.AtrIts'
      'LEFT JOIN atrsicxcad cad ON cad.Codigo=def.AtrCad'
      'WHERE def.ID_Sorc=:P0'
      'ORDER BY NO_CAD, NO_ITS')
    Left = 1764
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSCxaAtribID_Item: TIntegerField
      FieldName = 'ID_Item'
    end
    object QrOSCxaAtribID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
    end
    object QrOSCxaAtribAtrCad: TIntegerField
      FieldName = 'AtrCad'
    end
    object QrOSCxaAtribAtrIts: TIntegerField
      FieldName = 'AtrIts'
    end
    object QrOSCxaAtribCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
      Required = True
    end
    object QrOSCxaAtribCU_ITS: TIntegerField
      FieldName = 'CU_ITS'
      Required = True
    end
    object QrOSCxaAtribNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrOSCxaAtribNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 50
    end
  end
  object DsOSCxaAtrib: TDataSource
    DataSet = QrOSCxaAtrib
    Left = 1792
    Top = 388
  end
  object QrSiapTerCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM siaptercad'
      'WHERE Cliente=100')
    Left = 1708
    Top = 256
    object QrSiapTerCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapTerCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsSiapterCad: TDataSource
    DataSet = QrSiapTerCad
    Left = 1736
    Top = 256
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eco.Controle, eco.Nome'
      'FROM enticontat eco'
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE ece.Codigo=:P0')
    Left = 1708
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 1736
    Top = 300
  end
  object QrFatoGeradr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, NeedOS_Ori'
      'FROM fatogeradr'
      'ORDER BY Nome')
    Left = 1708
    Top = 344
    object QrFatoGeradrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFatoGeradrNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFatoGeradrNeedOS_Ori: TSmallintField
      FieldName = 'NeedOS_Ori'
    end
  end
  object DsFatoGeradr: TDataSource
    DataSet = QrFatoGeradr
    Left = 1736
    Top = 344
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntidadesBeforeClose
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 1708
    Top = 388
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 1736
    Top = 388
  end
  object QrEstatusOSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome,'
      'AgeCorIni, AgeCorFon'
      'FROM estatusoss'
      'ORDER BY Codigo')
    Left = 1708
    Top = 432
    object QrEstatusOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstatusOSsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrEstatusOSsAgeCorIni: TIntegerField
      FieldName = 'AgeCorIni'
    end
    object QrEstatusOSsAgeCorFon: TIntegerField
      FieldName = 'AgeCorFon'
    end
  end
  object DsEstatusOSs: TDataSource
    DataSet = QrEstatusOSs
    Left = 1736
    Top = 432
  end
  object QrEntPagante: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 1708
    Top = 520
    object QrEntPaganteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntPaganteNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object QrEntContrat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 1708
    Top = 476
    object QrEntContratCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntContratNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntContrat: TDataSource
    DataSet = QrEntContrat
    Left = 1736
    Top = 476
  end
  object DsEntPagante: TDataSource
    DataSet = QrEntPagante
    Left = 1736
    Top = 520
  end
  object QrExeTxtCli1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM txtgeneric'
      'WHERE Aplicacao & 1'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 1764
    Top = 432
    object QrExeTxtCli1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrExeTxtCli1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrExeTxtCli2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM txtgeneric'
      'WHERE Aplicacao & 2'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 1764
    Top = 476
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsExeTxtCli2: TDataSource
    DataSet = QrExeTxtCli2
    Left = 1792
    Top = 476
  end
  object DsExeTxtCli1: TDataSource
    DataSet = QrExeTxtCli1
    Left = 1792
    Top = 432
  end
  object QrAgentes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidades'
      'WHERE Nome <> '#39#39' '
      'AND Fornece2="V"'
      'ORDER BY Nome')
    Left = 1820
    Top = 168
    object QrAgentesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgentesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsAgentes: TDataSource
    DataSet = QrAgentes
    Left = 1848
    Top = 168
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 1708
    Top = 564
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 1736
    Top = 564
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 16
    Top = 16
  end
  object QrOSChk: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ELT(chk.Feito+1, "N'#195'O", "SIM", "???") NO_FEITO,'
      'chi.Nome NO_ITEM, chk.*'
      'FROM oschk chk'
      'LEFT JOIN cheklstits chi ON chi.Controle=chk.ChekLstIts'
      'WHERE chk.Codigo=:P0')
    Left = 1652
    Top = 696
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSChkNO_ITEM: TWideStringField
      FieldName = 'NO_ITEM'
      Size = 255
    end
    object QrOSChkCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSChkControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSChkChekLstIts: TIntegerField
      FieldName = 'ChekLstIts'
    end
    object QrOSChkFeito: TSmallintField
      FieldName = 'Feito'
    end
    object QrOSChkNO_FEITO: TWideStringField
      FieldName = 'NO_FEITO'
      Size = 3
    end
  end
  object DsOSChk: TDataSource
    DataSet = QrOSChk
    Left = 1680
    Top = 696
  end
  object PMOSChk: TPopupMenu
    Left = 1708
    Top = 124
    object Incluiitens1: TMenuItem
      Caption = '&Inclui itens'
      OnClick = Incluiitens1Click
    end
    object Removeitens1: TMenuItem
      Caption = '&Remove itens'
      OnClick = Removeitens1Click
    end
  end
  object QrLugares: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLugaresBeforeClose
    AfterScroll = QrLugaresAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT osc.SiapTerCad, stc.Nome'
      'FROM oscab osc'
      'LEFT JOIN siaptercad stc ON stc.Codigo=osc.SiapTerCad')
    Left = 4
    Top = 136
    object QrLugaresSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrLugaresNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsLugares: TDataSource
    DataSet = QrLugares
    Left = 32
    Top = 136
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGruposAfterOpen
    BeforeClose = QrGruposBeforeClose
    AfterScroll = QrGruposAfterScroll
    Left = 44
    Top = 16
    object QrGruposGrupo: TIntegerField
      FieldName = 'Grupo'
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 72
    Top = 16
  end
  object QrOpcoes: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOpcoesBeforeClose
    AfterScroll = QrOpcoesAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT Opcao'
      'FROM oscab')
    Left = 60
    Top = 136
    object QrOpcoesOpcao: TIntegerField
      FieldName = 'Opcao'
    end
  end
  object DsOpcoes: TDataSource
    DataSet = QrOpcoes
    Left = 88
    Top = 136
  end
  object PMOSImp: TPopupMenu
    Left = 100
    Top = 16
    object Cadastrodecliente1: TMenuItem
      Caption = 'Cadastro do cliente'
      OnClick = Cadastrodecliente1Click
    end
    object Fichadolugar1: TMenuItem
      Caption = 'Ficha dos lugares'
      object Preencherosdadosdoslugares1: TMenuItem
        Caption = 'Preencher os dados dos lugares'
        OnClick = Preencherosdadosdoslugares1Click
      end
      object Dadosdoslugaresembranco1: TMenuItem
        Caption = 'Dados dos lugares em branco'
        OnClick = Dadosdoslugaresembranco1Click
      end
    end
    object Fichasdevistoria1: TMenuItem
      Caption = 'Fichas de vistoria'
      OnClick = Fichasdevistoria1Click
    end
    object Oramento1: TMenuItem
      Caption = 'Or'#231'amento'
      OnClick = Oramento1Click
    end
    object Fichadeexecuo1: TMenuItem
      Caption = 'Ordem de execu'#231#227'o'
      OnClick = Fichadeexecuo1Click
    end
    object Monitoramento1: TMenuItem
      Caption = 'Monitoramento'
      OnClick = Monitoramento1Click
    end
    object ComprovantedeExecues1: TMenuItem
      Caption = 'Comprovante de Execu'#231#245'es'
      OnClick = ComprovantedeExecues1Click
    end
    object FAES1: TMenuItem
      Caption = 'FAES'
      OnClick = FAES1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Diriodestaempresacomestecliente1: TMenuItem
      Caption = 'Di'#225'rio desta empresa com este cliente'
      OnClick = Diriodestaempresacomestecliente1Click
    end
    object N23: TMenuItem
      Caption = '-'
    end
    object Listadeperguntas1: TMenuItem
      Caption = 'Lista de perguntas'
      OnClick = Listadeperguntas1Click
    end
  end
  object QrReplica: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  Grupo, Opcao, SiapTerCad, Codigo'
      'FROM oscab '
      'WHERE Grupo <> 0  '
      'AND Grupo=86 '
      'ORDER BY Opcao, SiapTerCad ')
    Left = 4
    Top = 164
    object QrReplicaGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrReplicaOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrReplicaSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrReplicaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrDiarioAdd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Depto, Data, Hora,  '
      'Interloctr, PrintOS, '
      'ELT(PrintOS + 2, " ", "N", "S", "?") IMP, Nome'
      'FROM diarioadd '
      'WHERE Depto IN ( '
      '     SELECT Codigo '
      '     FROM oscab  '
      '     WHERE Grupo=109) '
      'ORDER BY Codigo DESC ')
    Left = 4
    Top = 196
    object QrDiarioAddCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioAddDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDiarioAddData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddHora: TTimeField
      FieldName = 'Hora'
    end
    object QrDiarioAddInterloctr: TIntegerField
      FieldName = 'Interloctr'
    end
    object QrDiarioAddPrintOS: TIntegerField
      FieldName = 'PrintOS'
    end
    object QrDiarioAddNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrDiarioAddIMP: TWideStringField
      FieldName = 'IMP'
      Size = 1
    end
  end
  object DsDiarioAdd: TDataSource
    DataSet = QrDiarioAdd
    Left = 32
    Top = 196
  end
  object PMDiarioAdd: TPopupMenu
    Left = 60
    Top = 196
    object Sim1: TMenuItem
      Caption = 'Sim'
      OnClick = Sim1Click
    end
    object No1: TMenuItem
      Caption = 'N'#227'o'
      OnClick = No1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Novodilogo1: TMenuItem
      Caption = 'Novo di'#225'logo'
      OnClick = Novodilogo1Click
    end
  end
  object QrFaltaToImp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Codigo) ITENS'
      'FROM diarioadd '
      'WHERE PrintOS=-1'
      'AND Depto IN ( '
      '     SELECT Codigo '
      '     FROM oscab  '
      '     WHERE Grupo=109) '
      'ORDER BY Codigo DESC '
      '')
    Left = 88
    Top = 196
    object QrFaltaToImpITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object PMPesqNum: TPopupMenu
    Left = 128
    Top = 16
    object Proforma1: TMenuItem
      Caption = '&Proforma'
      OnClick = Proforma1Click
    end
    object Localizador1: TMenuItem
      Caption = '&Localizador'
      OnClick = Localizador1Click
    end
  end
  object QrOSSta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eos.Nome NO_STATUS, oss.*'
      'FROM ossta oss'
      'LEFT JOIN estatusoss eos ON eos.Codigo=oss.Status'
      'WHERE oss.Codigo>0'
      'ORDER BY eos.Ordem, eos.Codigo')
    Left = 1708
    Top = 696
    object QrOSStaNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrOSStaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSStaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSStaStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrOSStaFeito: TSmallintField
      FieldName = 'Feito'
    end
    object QrOSStaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSStaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSStaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSStaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsOSSta: TDataSource
    DataSet = QrOSSta
    Left = 1736
    Top = 696
  end
  object PMOSSta: TPopupMenu
    Left = 1792
    Top = 124
    object Adicionastatus1: TMenuItem
      Caption = '&Adiciona status'
      OnClick = Adicionastatus1Click
    end
  end
  object QrOSFrmEvo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM osfrmevo'
      'WHERE Conta=:P0'
      'ORDER BY DataHora DESC ')
    Left = 1708
    Top = 608
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmEvoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmEvoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmEvoConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmEvoIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSFrmEvoDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrOSFrmEvoPercFeito: TFloatField
      FieldName = 'PercFeito'
    end
    object QrOSFrmEvoTexto: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
  end
  object DsOSFrmEvo: TDataSource
    DataSet = QrOSFrmEvo
    Left = 1737
    Top = 608
  end
  object DsOSCabXtr: TDataSource
    DataSet = QrOSCabXtr
    Left = 1737
    Top = 652
  end
  object QrOSCabXtr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM oscabxtr'
      'WHERE Codigo=:P0'
      'ORDER BY DtHrIni DESC')
    Left = 1708
    Top = 652
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSCabXtrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabXtrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSCabXtrDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrOSCabXtrDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
  end
  object PMOSFrmEvo: TPopupMenu
    OnPopup = PMOSFrmEvoPopup
    Left = 1736
    Top = 124
    object Incluievoluodeaplicao1: TMenuItem
      Caption = '&Inclui evolu'#231#227'o de aplica'#231#227'o'
      OnClick = Incluievoluodeaplicao1Click
    end
    object Alteraevoluodeaplicao1: TMenuItem
      Caption = '&Altera evolu'#231#227'o de aplica'#231#227'o'
      OnClick = Alteraevoluodeaplicao1Click
    end
    object Excluievoluodeaplicao1: TMenuItem
      Caption = '&Exclui evolu'#231#227'o de aplica'#231#227'o'
      OnClick = Excluievoluodeaplicao1Click
    end
  end
  object PMOSCabXtr: TPopupMenu
    Left = 1764
    Top = 124
    object Incluiitemdeescalonamento1: TMenuItem
      Caption = '&Inclui item de escalonamento'
      OnClick = Incluiitemdeescalonamento1Click
    end
    object Alteraitemdeescalonamento1: TMenuItem
      Caption = '&Altera item de escalonamento'
      OnClick = Alteraitemdeescalonamento1Click
    end
    object Excluiitemdeescalonamento1: TMenuItem
      Caption = '&Exclui item de escalonamento'
      OnClick = Excluiitemdeescalonamento1Click
    end
  end
  object QrOSPrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prv.*, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, '
      'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_Agente, '
      'eco.Nome NO_Contato, fct.Nome NO_FormContat  '
      'FROM osprv prv '
      'LEFT JOIN entidades cli ON cli.Codigo=prv.Cliente '
      'LEFT JOIN entidades age ON age.Codigo=prv.Agente '
      'LEFT JOIN enticontat eco ON eco.Controle=prv.Contato '
      'LEFT JOIN formcontat fct ON fct.Codigo=prv.FormContat '
      ' '
      ' ')
    Left = 1764
    Top = 520
    object QrOSPrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSPrvDtHrContat: TDateTimeField
      FieldName = 'DtHrContat'
    end
    object QrOSPrvFormContat: TIntegerField
      FieldName = 'FormContat'
    end
    object QrOSPrvCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrOSPrvContato: TIntegerField
      FieldName = 'Contato'
    end
    object QrOSPrvAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrOSPrvNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrOSPrvNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrOSPrvNO_Agente: TWideStringField
      FieldName = 'NO_Agente'
      Size = 100
    end
    object QrOSPrvNO_Contato: TWideStringField
      FieldName = 'NO_Contato'
      Size = 30
    end
    object QrOSPrvNO_FormContat: TWideStringField
      FieldName = 'NO_FormContat'
      Size = 60
    end
    object QrOSPrvNO_PrvStatus: TWideStringField
      FieldName = 'NO_PrvStatus'
      Size = 60
    end
  end
  object DsOSPrv: TDataSource
    DataSet = QrOSPrv
    Left = 1792
    Top = 520
  end
  object PMOSPrv: TPopupMenu
    Left = 1820
    Top = 124
    object Adicionasolicitaesdeprovidncias1: TMenuItem
      Caption = '&Adiciona solicita'#231#245'es de provid'#234'ncias'
      OnClick = Adicionasolicitaesdeprovidncias1Click
    end
    object Removeasolicitaesdeprovidnciaselecionada1: TMenuItem
      Caption = '&Remove a solicita'#231#245'es de provid'#234'ncia selecionada'
      OnClick = Removeasolicitaesdeprovidnciaselecionada1Click
    end
  end
  object PMOSFrmFlhCb: TPopupMenu
    OnPopup = PMOSFrmFlhCbPopup
    Left = 1072
    Top = 512
    object Incluifrmulafilha1: TMenuItem
      Caption = '&Inclui f'#243'rmula filha'
      OnClick = Incluifrmulafilha1Click
    end
    object Alterafrmulafilha1: TMenuItem
      Caption = '&Altera f'#243'rmula filha'
      OnClick = Alterafrmulafilha1Click
    end
    object Excluifrmulafilha1: TMenuItem
      Caption = '&Exclui f'#243'rmula filha'
      OnClick = Excluifrmulafilha1Click
    end
    object N18: TMenuItem
      Caption = '-'
    end
    object Alteradatadaltimaemissodefrmulasfilhas1: TMenuItem
      Caption = 'Altera data da '#250'ltima emiss'#227'o de OSs filhas'
      OnClick = Alteradatadaltimaemissodefrmulasfilhas1Click
    end
  end
  object QrOSFrmFlhCb: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSFrmFlhCbBeforeClose
    AfterScroll = QrOSFrmFlhCbAfterScroll
    SQL.Strings = (
      'SELECT ffc.*, frm.Nome NO_FORMULA'
      'FROM osfrmflhcb ffc'
      'LEFT JOIN formulas frm ON frm.Codigo=ffc.Formula'
      'WHERE ffc.Conta=:P0')
    Left = 1764
    Top = 564
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmFlhCbCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmFlhCbControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmFlhCbConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmFlhCbIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSFrmFlhCbFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrOSFrmFlhCbPeriodd: TIntegerField
      FieldName = 'Periodd'
    end
    object QrOSFrmFlhCbLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSFrmFlhCbDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSFrmFlhCbDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSFrmFlhCbUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSFrmFlhCbUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSFrmFlhCbAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSFrmFlhCbAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSFrmFlhCbNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrOSFrmFlhCbDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
    object QrOSFrmFlhCbEmisStatus: TSmallintField
      FieldName = 'EmisStatus'
    end
    object QrOSFrmFlhCbEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
    end
  end
  object DsOSFrmFlhCb: TDataSource
    DataSet = QrOSFrmFlhCb
    Left = 1792
    Top = 564
  end
  object QrOSFrmFlhDd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ffd.* '
      'FROM osfrmflhdd ffd '
      'WHERE ffd.IDIts=:P0'
      'ORDER BY Ordem, Controle ')
    Left = 1764
    Top = 608
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmFlhDdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmFlhDdControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmFlhDdConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmFlhDdIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSFrmFlhDdIDIt2: TIntegerField
      FieldName = 'IDIt2'
    end
    object QrOSFrmFlhDdOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSFrmFlhDdDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrOSFrmFlhDdLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSFrmFlhDdDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSFrmFlhDdDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSFrmFlhDdUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSFrmFlhDdUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSFrmFlhDdAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSFrmFlhDdAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsOSFrmFlhDd: TDataSource
    DataSet = QrOSFrmFlhDd
    Left = 1792
    Top = 608
  end
  object PMOSFrmFlhDd: TPopupMenu
    OnPopup = PMOSFrmFlhDdPopup
    Left = 1072
    Top = 560
    object IncluiIntervalo1: TMenuItem
      Caption = '&Inclui Intervalo'
      OnClick = IncluiIntervalo1Click
    end
    object Alteraintervalo1: TMenuItem
      Caption = '&Altera intervalo'
      OnClick = Alteraintervalo1Click
    end
    object ExcluiIntervalo1: TMenuItem
      Caption = '&Exclui Intervalo'
      OnClick = ExcluiIntervalo1Click
    end
  end
  object QrAgeEqiCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ageeqicab '
      'ORDER BY Nome'
      ' ')
    Left = 4
    Top = 244
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgeEqiCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsAgeEqiCab: TDataSource
    DataSet = QrAgeEqiCab
    Left = 32
    Top = 244
  end
  object PMOSMonPipDd: TPopupMenu
    OnPopup = PMOSMonPipDdPopup
    Left = 1072
    Top = 604
    object IncluiIntervalo2: TMenuItem
      Caption = '&Inclui Intervalo'
      OnClick = IncluiIntervalo2Click
    end
    object AlteraIntervalo2: TMenuItem
      Caption = '&Altera intervalo'
      OnClick = AlteraIntervalo2Click
    end
    object ExcluiIntervalo2: TMenuItem
      Caption = '&Exclui Intervalo'
      OnClick = ExcluiIntervalo2Click
    end
  end
  object QrOSMonPipDd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM osmonpipdd'
      'WHERE Conta>0'
      'ORDER BY Ordem, IDIts')
    Left = 1764
    Top = 652
    object QrOSMonPipDdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSMonPipDdControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSMonPipDdConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSMonPipDdIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSMonPipDdOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSMonPipDdDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrOSMonPipDdLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSMonPipDdDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSMonPipDdDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSMonPipDdUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSMonPipDdUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSMonPipDdAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSMonPipDdAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsOSMonPipDd: TDataSource
    DataSet = QrOSMonPipDd
    Left = 1792
    Top = 652
  end
  object QrOSPipIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pcp.Sigla, pcp.Nome NO_PERGUNTA,'
      'IF(Respondido<>1, "",'
      '  ELT(Funcoes,'
      '  ELT(RespBin + 1, pb0.Nome, pb1.Nome, "???"),'
      '  RespQtd,'
      '  IF(ppr.GraGruX IS NULL, "", CONCAT(ppr.UsoQtd,'
      '    " unidades da isca ID ", ppr.GraGruX)),'
      '  pai.Nome,'
      '  LEFT(RespTxtLvr, 512))) RESPOSTA,'
      'opi.*'
      'FROM ospipits opi'
      'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta'
      ''
      'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts'
      'LEFT JOIN ospipitspr ppr ON ppr.Conta=opi.Conta'
      'LEFT JOIN prgbincad pb0 ON pb0.Codigo=opi.BinarCad0'
      'LEFT JOIN prgbincad pb1 ON pb1.Codigo=opi.BinarCad1'
      'WHERE opi.Controle=1277'
      'ORDER BY opi.Controle, opi.SobreOrd, opi.Ordem, opi.SubOrdem'
      '')
    Left = 1764
    Top = 696
    object QrOSPipItsNO_PERGUNTA: TWideStringField
      FieldName = 'NO_PERGUNTA'
      Size = 100
    end
    object QrOSPipItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPipItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSPipItsConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrOSPipItsPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
    object QrOSPipItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSPipItsSubOrdem: TIntegerField
      FieldName = 'SubOrdem'
    end
    object QrOSPipItsTabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrOSPipItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOSPipItsFuncoes: TSmallintField
      FieldName = 'Funcoes'
    end
    object QrOSPipItsDependente: TSmallintField
      FieldName = 'Dependente'
    end
    object QrOSPipItsPrgAtrCad: TIntegerField
      FieldName = 'PrgAtrCad'
    end
    object QrOSPipItsFiliacao: TIntegerField
      FieldName = 'Filiacao'
    end
    object QrOSPipItsRelacao: TIntegerField
      FieldName = 'Relacao'
    end
    object QrOSPipItsNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrOSPipItsNivSeq: TIntegerField
      FieldName = 'NivSeq'
    end
    object QrOSPipItsPergunta: TIntegerField
      FieldName = 'Pergunta'
    end
    object QrOSPipItsBinarCad0: TIntegerField
      FieldName = 'BinarCad0'
    end
    object QrOSPipItsBinarCad1: TIntegerField
      FieldName = 'BinarCad1'
    end
    object QrOSPipItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSPipItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSPipItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSPipItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSPipItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSPipItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSPipItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSPipItsSigla: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Sigla'
      Size = 10
      Calculated = True
    end
    object QrOSPipItsPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrOSPipItsSobreOrd: TIntegerField
      FieldName = 'SobreOrd'
    end
    object QrOSPipItsTabIdx: TLargeintField
      FieldName = 'TabIdx'
    end
    object QrOSPipItsRESPOSTA: TWideStringField
      FieldName = 'RESPOSTA'
      Size = 512
    end
    object QrOSPipItsAcaoPrd: TSmallintField
      FieldName = 'AcaoPrd'
    end
    object QrOSPipItsRespondido: TSmallintField
      FieldName = 'Respondido'
    end
    object QrOSPipItsRespBin: TSmallintField
      FieldName = 'RespBin'
    end
    object QrOSPipItsRespQtd: TFloatField
      FieldName = 'RespQtd'
    end
    object QrOSPipItsRespAtrCad: TIntegerField
      FieldName = 'RespAtrCad'
    end
    object QrOSPipItsRespAtrIts: TIntegerField
      FieldName = 'RespAtrIts'
    end
    object QrOSPipItsRespAtrTxt: TWideStringField
      FieldName = 'RespAtrTxt'
      Size = 60
    end
    object QrOSPipItsRespTxtLvr: TWideMemoField
      FieldName = 'RespTxtLvr'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSPipItsRespPrdBin: TSmallintField
      FieldName = 'RespPrdBin'
    end
    object QrOSPipItsIDIts: TFloatField
      FieldName = 'IDIts'
    end
    object QrOSPipItsSMI_IDCtrl: TFloatField
      FieldName = 'SMI_IDCtrl'
    end
  end
  object DsOSPipIts: TDataSource
    DataSet = QrOSPipIts
    Left = 1792
    Top = 696
  end
  object QrNaoRatif: TmySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 456
  end
  object QrSemCus: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT opi.GraGruX, ggv.Controle, gg1.Nome'
      'FROM ospipits opi '
      'LEFT JOIN gragruval ggv ON ggv.GraGruX=opi.GraGruX'
      '  AND ggv.GraCusPrc=4'
      '  AND Entidade=-11  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=opi.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE opi.GraGruX <> 0'
      'AND opi.Codigo>0'
      'AND opi.Respondido=0 '
      'AND ggv.Controle IS NULL'
      'ORDER BY Nome')
    Left = 32
    Top = 500
    object QrSemCusGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSemCusControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSemCusNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object frxGER_OSERV_001_Err_01: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  //MePagina.Visible := <VARF_PAGINAR>;  '
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    Left = 32
    Top = 588
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSemCus
        DataSetName = 'frxDsSemCus'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 109.606370000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Produtos sem pre'#231'o de custo definido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 105.826840000000000000
          Top = 45.354360000000000000
          Width = 574.488354960000100000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 45.354360000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 52.913420000000000000
          Top = 90.708720000000000000
          Width = 627.401774960000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Top = 90.708720000000000000
          Width = 52.913390710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 105.826840000000000000
          Top = 64.252010000000000000
          Width = 574.488354960000100000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_LISTA_VALORES]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Top = 64.252010000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lista de valores:')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSemCus
        DataSetName = 'frxDsSemCus'
        RowCount = 0
        object Memo11: TfrxMemoView
          Width = 52.913390710000000000
          Height = 18.897650000000000000
          DataField = 'GraGruX'
          DataSet = frxDsSemCus
          DataSetName = 'frxDsSemCus'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSemCus."GraGruX"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 52.913420000000000000
          Width = 627.401980000000000000
          Height = 18.897650000000000000
          DataField = 'Nome'
          DataSet = frxDsSemCus
          DataSetName = 'frxDsSemCus'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSemCus."Nome"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsSemCus: TfrxDBDataset
    UserName = 'frxDsSemCus'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Controle=Controle'
      'Nome=Nome')
    DataSet = QrSemCus
    BCDToCurrency = False
    Left = 32
    Top = 544
  end
  object QrPreQtd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pip.Nome NO_PIP, pcp.Nome NO_Pergunta,'
      'opi.RespQtd, opi.PrgLstIts'
      'FROM ospipits opi'
      'LEFT JOIN ospipmon  opm ON opm.Controle=opi.Controle'
      'LEFT JOIN pipcad    pip ON pip.Codigo=opm.PipCad'
      'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta'
      'WHERE opi.Codigo=817'
      'AND opi.LupForma=2'
      'AND opi.LupInfVzs=0'
      'ORDER BY NO_PIP, NO_Pergunta')
    Left = 32
    Top = 408
    object QrPreQtdNO_PIP: TWideStringField
      FieldName = 'NO_PIP'
      Size = 30
    end
    object QrPreQtdNO_Pergunta: TWideStringField
      FieldName = 'NO_Pergunta'
      Size = 100
    end
    object QrPreQtdRespQtd: TFloatField
      FieldName = 'RespQtd'
    end
    object QrPreQtdConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrPreQtdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreQtdPipCad: TIntegerField
      FieldName = 'PipCad'
    end
    object QrPreQtdFiliacao: TIntegerField
      FieldName = 'Filiacao'
    end
    object QrPreQtdPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
  end
  object QrOSPrn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM osprn'
      'WHERE Codigo=0')
    Left = 220
    Top = 388
    object QrOSPrnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPrnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSPrnRelatorio: TIntegerField
      FieldName = 'Relatorio'
    end
    object QrOSPrnDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrOSPrnDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrOSPrnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSPrnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSPrnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSPrnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSPrnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSPrnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSPrnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSPrnNO_Relatorio: TWideStringField
      FieldName = 'NO_Relatorio'
      Size = 100
    end
  end
  object DsOSPrn: TDataSource
    DataSet = QrOSPrn
    Left = 220
    Top = 436
  end
  object QrMobiliCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM mobilicad'
      'ORDER BY Nome')
    Left = 236
    Top = 500
    object QrMobiliCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMobiliCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMobiliCad: TDataSource
    DataSet = QrMobiliCad
    Left = 236
    Top = 548
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 328
    Top = 316
  end
  object PMOSPipIts: TPopupMenu
    Left = 800
    Top = 228
    object este1: TMenuItem
      Caption = '&Alterar resposta'
      OnClick = este1Click
    end
  end
  object QrOSExtMon: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM osextmon')
    Left = 916
    Top = 132
    object QrOSExtMonCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSExtMonControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSExtMonDtaFinal: TDateField
      FieldName = 'DtaFinal'
    end
    object QrOSExtMonPerioDd: TIntegerField
      FieldName = 'PerioDd'
    end
    object QrOSExtMonTotalDd: TIntegerField
      FieldName = 'TotalDd'
    end
    object QrOSExtMonLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSExtMonDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSExtMonDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSExtMonUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSExtMonUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSExtMonAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSExtMonAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsOSExtMon: TDataSource
    DataSet = QrOSExtMon
    Left = 916
    Top = 180
  end
  object PMOSExtMon: TPopupMenu
    OnPopup = PMOSExtMonPopup
    Left = 500
    Top = 284
    object Incluimonitoramentoextra1: TMenuItem
      Caption = '&Inclui monitoramento extra'
      OnClick = Incluimonitoramentoextra1Click
    end
    object Alteramonitoramentoextra1: TMenuItem
      Caption = '&Altera monitoramento extra'
      Enabled = False
      Visible = False
      OnClick = Alteramonitoramentoextra1Click
    end
    object Excluimonitoramentoextra1: TMenuItem
      Caption = '&Exclui monitoramento extra'
      OnClick = Excluimonitoramentoextra1Click
    end
  end
  object QrVendedores: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntidadesBeforeClose
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Fornece3 = "V"'
      'ORDER BY NO_ENT')
    Left = 1820
    Top = 388
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 1848
    Top = 388
  end
  object PMLaudoDel: TPopupMenu
    OnPopup = PMLaudoDelPopup
    Left = 328
    Top = 240
    object Graudeinfestaodaspragasevetores1: TMenuItem
      Caption = '&Grau de infesta'#231#227'o das pragas e vetores'
      object reainterna1: TMenuItem
        Caption = #193'rea &interna'
        OnClick = reainterna1Click
      end
      object reaexterna1: TMenuItem
        Caption = #193'rea &externa'
        OnClick = reaexterna1Click
      end
    end
    object Parecerdoreponsveltcnico1: TMenuItem
      Caption = '&Parecer do repons'#225'vel t'#233'cnico'
      OnClick = Parecerdoreponsveltcnico1Click
    end
  end
  object PMOSFrmEPI: TPopupMenu
    OnPopup = PMOSFrmEPIPopup
    Left = 776
    Top = 352
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
    object MenuItem1: TMenuItem
      Caption = '-'
    end
    object Cadatrodeprodutos1: TMenuItem
      Caption = '&Cadatro de produtos'
      OnClick = Cadatrodeprodutos1Click
    end
  end
  object PMOSMonEPI: TPopupMenu
    OnPopup = PMOSMonEPIPopup
    Left = 856
    Top = 352
    object MenuItem2: TMenuItem
      Caption = '&Inclui'
      OnClick = MenuItem2Click
    end
    object MenuItem3: TMenuItem
      Caption = '&Exclui'
      OnClick = MenuItem3Click
    end
    object MenuItem4: TMenuItem
      Caption = '-'
    end
    object MenuItem5: TMenuItem
      Caption = '&Cadatro de produtos'
      OnClick = MenuItem5Click
    end
  end
  object QrOSFrmEPI: TmySQLQuery
    Database = Dmod.MyDB
    Left = 1840
    Top = 252
    object IntegerField4: TIntegerField
      FieldName = 'Controle'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object IntegerField5: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrOSFrmEPIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOSFrmEPICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmEPIConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmEPIIDIts: TIntegerField
      FieldName = 'IDIts'
    end
  end
  object DsOSFrmEPI: TDataSource
    DataSet = QrOSFrmEPI
    Left = 1868
    Top = 252
  end
  object QrOSMonEPI: TmySQLQuery
    Database = Dmod.MyDB
    Left = 1840
    Top = 308
    object IntegerField6: TIntegerField
      FieldName = 'Controle'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object IntegerField7: TIntegerField
      FieldName = 'Nivel1'
    end
    object IntegerField8: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOSMonEPICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSMonEPIConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSMonEPIIDIts: TIntegerField
      FieldName = 'IDIts'
    end
  end
  object DsOSMonEPI: TDataSource
    DataSet = QrOSMonEPI
    Left = 1868
    Top = 308
  end
end
