unit RMIP_R009;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  mySQLDbTables, Data.DB, frxClass, frxDBSet, UnDmkEnums, DmkGeral,
  UnInternalConsts, UnProjGroup_Consts, UnDmkProcFunc, UnMyObjects;

type
  TFmRMIP_R009 = class(TForm)
    frxReport009A: TfrxReport;
    Qr009A_PrgLstIts: TmySQLQuery;
    Qr009A_PrgLstItsPrgLstCab: TIntegerField;
    Qr009A_PrgLstItsPrgLstIts: TIntegerField;
    Qr009A_PrgLstItsNO_LST: TWideStringField;
    Qr009A_PrgLstItsSigla: TWideStringField;
    Qr009A_PrgLstItsNome: TWideStringField;
    Qr009A_PrgLstItsFuncoes: TIntegerField;
    Qr009A_PrgLstItsCodigo: TFloatField;
    frxDs009A_PrgLstIts: TfrxDBDataset;
    Qr009A_OSPipIts: TmySQLQuery;
    Qr009A_OSPipItsITENS: TLargeintField;
    Qr009A_OSPipItsRespondido: TSmallintField;
    Qr009A_OSPipItsRespAtrCad: TIntegerField;
    Qr009A_OSPipItsRespAtrIts: TIntegerField;
    Qr009A_OSPipItsResposta: TWideStringField;
    Qr009A_OSPipItsCorPizza: TFloatField;
    Qr009A_OSPipItsDtaExeFim: TDateTimeField;
    frxDs009A_OSPipIts: TfrxDBDataset;
    Qr009A_OSs: TmySQLQuery;
    Qr009A_OSsCodigo: TIntegerField;
    Qr009A_OSsEntidade: TIntegerField;
    Qr009A_OSsSiapTerCad: TIntegerField;
    Qr009A_OSsEstatus: TIntegerField;
    Qr009A_OSsDtaExeFim: TDateTimeField;
    Qr009A_OSsNumContrat: TIntegerField;
    Qr009A_OSsGrupo: TIntegerField;
    Qr009A_OSsNO_FatoGeradr: TWideStringField;
    Qr009A_OSsNO_ESTATUS: TWideStringField;
    Qr009A_OSsNO_SiapTerCad: TWideStringField;
    Qr009A_OSsNO_ENT: TWideStringField;
    frxDs009A_OSs: TfrxDBDataset;
    Qr009A_OSPipSUM: TmySQLQuery;
    Qr009A_OSPipSUMITENS: TLargeintField;
    frxDs009A_OSPipSUM: TfrxDBDataset;
    procedure Qr009A_PrgLstItsAfterScroll(DataSet: TDataSet);
    procedure frxReport009AGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FOSCab, FAplicacao: Integer;
    FAbrangencia: TRelAbrange;
    //
    procedure Reopen003A_PrgLstIts();
    procedure Reopen003A_OSPipIts(Localizador, PrgLstIts, Funcoes: Integer;
              DtaIni, DtaFim: TDateTime; Abrangencia: TRelAbrange; ItemRel: Integer);
  public
    { Public declarations }
    FEmpresa, FCliente, FItemRel: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    //
    procedure GeraImp_ListaUltimaRevisaoPMV();
    procedure frxReport000GetValue(frxReport: TfrxReport;
              const VarName: string; var Value: Variant);
  end;

var
  FmRMIP_R009: TFmRMIP_R009;

implementation

{$R *.dfm}

uses Module, ModuleGeral, ModuleRMIP, DmkDAC_PF;

{ TFmRMIP_R003 }

procedure TFmRMIP_R009.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
begin
{
  if VarName = 'VARF_PERCENT_ITENS' then
  begin
    if Qr009A_OSPipSUMITENS.Value <=0 then
      Value := 0
    else
      Value := Qr003A_OSPipItsITENS.Value / Qr009A_OSPipSUMITENS.Value * 100;
  end else
}
end;

procedure TFmRMIP_R009.frxReport009AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport009A, VarName, Value);
end;

procedure TFmRMIP_R009.GeraImp_ListaUltimaRevisaoPMV();
const
  DtaIni = 0;
  DtaFim = 0;
var
  Qry: TmySQLQuery;
  Aviso: String;
begin
  Qr009A_PrgLstIts.Close;
  FOSCab := 0;
  Aviso := ' ';
  // Ultimo monitormento finalizado do cliente selecionado!
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT cab.Codigo, cab.DtaExeFim ',
    'FROM oscab cab ',
    'LEFT JOIN ' + TMeuDB + '.ospipits opi ON opi.Codigo=cab.Codigo ',
    'WHERE cab.Empresa=' + Geral.FF0(FEmpresa),
    'AND cab.Entidade=' + Geral.FF0(FCliente),
    'AND cab.DtaExeFim > "1900-01-01" ',
    'AND cab.Operacao & ' + Geral.FF0(CO_OS_OPERACAO_SERVICO_MONITORAMENTO),
    'AND opi.Codigo IS NOT NULL ',
    'ORDER BY cab.DtaExeFim DESC ',
    'LIMIT 1 ',
    '']);
    //
    FOSCab := Qry.FieldByName('Codigo').AsInteger;
  finally
    Qry.Free;
  end;
  //
  if FOSCab = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Cliente sem monitoramento respondido e finalizado!';
  end;
  FAplicacao := CO_GeraGrafRelatPMV_0001_COD_PizzaMonitsResp;
  FAbrangencia := relAbrangeCliente;
  Reopen003A_PrgLstIts();
  //
  if Qr009A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Questionários do monitoramento do localizador ' +
      Geral.FF0(FOSCab) + ' sem perguntas geradoras do gráfico "' +
      CO_GeraGrafRelatPMV_0001_TXT_PizzaMonitsResp + '"!';
  end;
  if Trim(Aviso) = '' then
  //  Pode ter varios localizadores!
    //Aviso := 'Localizador: ' + Geral.FF0(FOSCab);
    Aviso := QuotedStr(dmkPF.PeriodoImp(FDtaIni, FDtaFim, 0, 0,
      True, True, False, False, '', ''));
  frxReport009A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport009A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport009A.Variables['VARF_DATA']    := FDtaImp;
  frxReport009A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport009A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  MyObjects.frxDefineDataSets(frxReport009A, [
    frxDs009A_OSPipIts,
    frxDs009A_PrgLstIts,
    frxDs009A_OSPipSUM,
    frxDs009A_OSs]);
  frxReport009A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
  //MyObjects.frxPrepara(frxReport009A, CO_GeraGrafRelatPMV_0001_TXT_PizzaMonitsResp);
end;

procedure TFmRMIP_R009.Qr009A_PrgLstItsAfterScroll(DataSet: TDataSet);
var
  Localizador, PrgLstIts, Funcoes: Integer;
  Abrangencia: TRelAbrange;
begin
  Localizador := Trunc(Qr009A_PrgLstItsCodigo.Value);
  PrgLstIts   := Qr009A_PrgLstItsPrgLstIts.Value;
  Funcoes     := Qr009A_PrgLstItsFuncoes.Value;
  Abrangencia := DmRMIP.DefineAbrangencia(FCliente);
  Reopen003A_OSPipIts(Localizador, PrgLstIts, Funcoes, FDtaIni, FDtaFim,
  Abrangencia, FItemRel);
end;

procedure TFmRMIP_R009.Reopen003A_OSPipIts(Localizador, PrgLstIts,
  Funcoes: Integer; DtaIni, DtaFim: TDateTime;
  Abrangencia: TRelAbrange; ItemRel: Integer);
var
  Resposta, CorPizza, LeftJoin, OrderBy, GroupBy, SQL_Extra,
  SQL_Qtde, SQLPadrao, FldAgrup: String;
begin
  if not DmRMIP.SQL_Funcoes(PrgLstIts, Funcoes, Resposta, CorPizza, LeftJoin, OrderBy,
  GroupBy, True) then
    Exit;
  //
  //SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(Localizador);
  SQL_Extra := Geral.ATS([
    dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True),
    Geral.ATS_If(FCliente <> 0, ['AND cab.Entidade=' + Geral.FF0(FCliente)])]);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr009A_OSPipSUM, Dmod.MyDB, [
  'SELECT COUNT(opi.RespAtrIts) ITENS ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'WHERE cab.Empresa=' + Geral.FF0(FEmpresa),
  'AND opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr009A_OSPipIts, Dmod.MyDB, [
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  Resposta,
  CorPizza,
  LeftJoin,
  'WHERE cab.Empresa=' + Geral.FF0(FEmpresa),
  'AND opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  GroupBy,
  OrderBy,
  '']);
 // Geral.MB_SQL(self, Qr009A_OSPipIts);
  //

  // OSs afetadas:
  DmRMIP.ReopenOSsAfetadas(Qr009A_OSs, PrgLstIts, LeftJoin, SQL_Extra);
  //Geral.MB_SQL(Self, Qr009A_OSs);
end;

procedure TFmRMIP_R009.Reopen003A_PrgLstIts();
var
  SQL_Codigo, SQL_Extra, SQL_DtExe: String;
begin
  case FAbrangencia of
    relAbrangeCliente:
    begin
      SQL_Codigo := 'opi.Codigo + 0.000';
      SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(FOSCab);
      SQL_DtExe := '0 DtaExeFim, ';
    end;
    relAbrangeEmpresa:
    begin
      SQL_Codigo := '0.000';
      SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', FDtaIni, FDtaFim, True, True);
      SQL_DtExe := '0 DtaExeFim, ';
    end
    else
    begin
      SQL_Codigo := '???';
      SQL_Extra := '? ? ?';
      SQL_DtExe := '??? DtaExeFim, ';
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr009A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT ' +
  SQL_DtExe,
  SQL_Codigo + ' Codigo, opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN ' + TMeuDB + '.prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN ' + TMeuDB + '.prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(FAplicacao),
  SQL_Extra,
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
  //
  //Geral.MB_SQL(Self, Qr009A_PrgLstIts);
end;

end.
