unit AllToRenew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables;

type
  TFmAllToRenew = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    BtOSsFilhas: TBitBtn;
    BtContratos: TBitBtn;
    BtNFSeMenCab: TBitBtn;
    BtBloArreIts: TBitBtn;
    BtGarantiasOSs: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOSsFilhasClick(Sender: TObject);
    procedure BtContratosClick(Sender: TObject);
    procedure BtNFSeMenCabClick(Sender: TObject);
    procedure BtBloArreItsClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtGarantiasOSsClick(Sender: TObject);
  private
    { Private declarations }
    FHoje: TDateTime;
  public
    { Public declarations }
    function ItensAbertos(): Integer;
  end;

  var
  FmAllToRenew: TFmAllToRenew;

implementation

uses UnMyObjects, Module, ModOS, UnContratUnit, ModuleGeral, DmkDAC_PF,
UnDmkProcFunc, NFSe_0000_Module, BloArrItsRnw, UnBloquetos;

{$R *.DFM}

procedure TFmAllToRenew.BtNFSeMenCabClick(Sender: TObject);
begin
  DmNFSe_0000.EmissoesMensaisVencidas(FHoje, True);
end;

procedure TFmAllToRenew.BtOKClick(Sender: TObject);
begin
  ItensAbertos();
end;

procedure TFmAllToRenew.BtOSsFilhasClick(Sender: TObject);
begin
  DmModOS.VerificaFormulasFilhas(True, False);
end;

procedure TFmAllToRenew.BtGarantiasOSsClick(Sender: TObject);
begin
  DmModOS.GarantiasAVencer(FHoje, True);
end;

procedure TFmAllToRenew.BtBloArreItsClick(Sender: TObject);
begin
  UBloquetos.ArrecadacoesVencidas_e_AVencer(FHoje, True);
end;

procedure TFmAllToRenew.BtContratosClick(Sender: TObject);
begin
  ContratUnit.ContratosAVencer(FHoje, True);
end;

procedure TFmAllToRenew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAllToRenew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAllToRenew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FHoje := DModG.ObtemAgora();
end;

procedure TFmAllToRenew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmAllToRenew.ItensAbertos(): Integer;
var
  Itens: Integer;
begin
  Result := 0;

  //

  Itens := DmModOS.VerificaFormulasFilhas(False, False);
  if Itens > 0 then
  begin
    Result := Result + 1;
    BtOSsFilhas.Caption := '{ ' + Geral.FF0(Itens) + ' } OSs Filhas';
    BtOSsFilhas.Enabled := True;
  end;

  //

  Itens := ContratUnit.ContratosAVencer(FHoje, False);
  if Itens > 0 then
  begin
    BtContratos.Caption := '{ ' + Geral.FF0(Itens) + ' } Contratos';
    BtContratos.Enabled := True;
    Result := Result + 1;
  end;

  //

  Itens := DmNFSe_0000.EmissoesMensaisVencidas(FHoje, False);
  if Itens > 0 then
  begin
    BtNFSeMenCab.Caption := '{ ' + Geral.FF0(Itens) + ' } Configurações de emissões mensais de NFS-e';
    BtNFSeMenCab.Enabled := True;
    Result := Result + 1;
  end;

  //

  Itens := UBloquetos.ArrecadacoesVencidas_e_AVencer(FHoje, False);
  if Itens > 0 then
  begin
    BtBloArreIts.Caption := '{ ' + Geral.FF0(Itens) + ' } Itens de arrecadações';
    BtBloArreIts.Enabled := True;
    Result := Result + 1;
  end;

  //

  Itens := DmModOS.GarantiasAVencer(FHoje, False);
  if Itens > 0 then
  begin
    BtGarantiasOSs.Caption := '{ ' + Geral.FF0(Itens) + ' } Garantias de serviços';
    BtGarantiasOSs.Enabled := True;
    Result := Result + 1;
  end;

  //

end;

end.
