unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, FileCtrl, UMySQLModule, dmkEdit,
  mySQLDbTables, UnGOTOy, Winsock, MySQLBatch, frxClass, frxDBSet, dmkGeral,
  Variants, StdCtrls, ComCtrls, IdBaseComponent, IdComponent,
  IdIPWatch, UnBugs_Tabs, ZCF2, UnDmkEnums, UnProjGroup_Consts;

type
  TDmod = class(TDataModule)
    QrMaster: TMySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrUpdU: TmySQLQuery;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrMasterECEP: TIntegerField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    MyDB: TmySQLDatabase;
    MyLocDatabase: TmySQLDatabase;
    QrPriorNext: TmySQLQuery;
    QrControle: TmySQLQuery;
    QrMasterLogo2: TBlobField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrMasterLimite: TSmallintField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrMasterSolicitaSenha: TIntegerField;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrMasterENumero: TFloatField;
    QrTerminaisLicenca: TWideStringField;
    QrTransf: TmySQLQuery;
    QrTransfData: TDateField;
    QrTransfTipo: TSmallintField;
    QrTransfCarteira: TIntegerField;
    QrTransfControle: TIntegerField;
    QrTransfGenero: TIntegerField;
    QrTransfDebito: TFloatField;
    QrTransfCredito: TFloatField;
    QrTransfDocumento: TFloatField;
    frxDsMaster: TfrxDBDataset;
    QrControleContasU: TIntegerField;
    QrControleEntDefAtr: TIntegerField;
    QrControleEntAtrCad: TIntegerField;
    QrControleEntAtrIts: TIntegerField;
    QrControleBalTopoNom: TIntegerField;
    QrControleBalTopoTit: TIntegerField;
    QrControleBalTopoPer: TIntegerField;
    QrControleCasasProd: TSmallintField;
    QrControleNCMs: TIntegerField;
    QrControleParamsNFs: TIntegerField;
    QrControleCambioCot: TIntegerField;
    QrControleCambioMda: TIntegerField;
    QrControleMoedaBr: TIntegerField;
    QrControleSecuritStr: TWideStringField;
    QrControleLogoBig1: TWideStringField;
    QrControleEquiCom: TIntegerField;
    QrControleContasLnk: TIntegerField;
    QrControleLastBco: TIntegerField;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrControleEquiGru: TIntegerField;
    QrControleCNAB_Rem: TIntegerField;
    QrControleCNAB_Rem_I: TIntegerField;
    QrControlePreEmMsgIm: TIntegerField;
    QrControlePreEmMsg: TIntegerField;
    QrControlePreEmail: TIntegerField;
    QrControleContasMes: TIntegerField;
    QrControleMultiPgto: TIntegerField;
    QrControleImpObs: TIntegerField;
    QrControleProLaINSSr: TFloatField;
    QrControleProLaINSSp: TFloatField;
    QrControleVerSalTabs: TIntegerField;
    QrControleVerBcoTabs: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNAB_CaD: TIntegerField;
    QrControleCNAB_CaG: TIntegerField;
    QrControleAtzCritic: TIntegerField;
    QrControleContasTrf: TIntegerField;
    QrControleSomaIts: TIntegerField;
    QrControleContasAgr: TIntegerField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleLogoNF: TWideStringField;
    QrControleConfJanela: TIntegerField;
    QrControleDataPesqAuto: TSmallintField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleAtividad: TSmallintField;
    QrControleCidades: TSmallintField;
    QrControleChequez: TSmallintField;
    QrControlePaises: TSmallintField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrControleCambiosData: TDateField;
    QrControleCambiosUsuario: TIntegerField;
    QrControlereg10: TSmallintField;
    QrControlereg11: TSmallintField;
    QrControlereg50: TSmallintField;
    QrControlereg51: TSmallintField;
    QrControlereg53: TSmallintField;
    QrControlereg54: TSmallintField;
    QrControlereg56: TSmallintField;
    QrControlereg60: TSmallintField;
    QrControlereg75: TSmallintField;
    QrControlereg88: TSmallintField;
    QrControlereg90: TSmallintField;
    QrControleNumSerieNF: TSmallintField;
    QrControleSerieNF: TIntegerField;
    QrControleModeloNF: TSmallintField;
    QrControleMyPagTip: TSmallintField;
    QrControleMyPagCar: TSmallintField;
    QrControleControlaNeg: TSmallintField;
    QrControleFamilias: TSmallintField;
    QrControleFamiliasIts: TSmallintField;
    QrControleAskNFOrca: TSmallintField;
    QrControlePreviewNF: TSmallintField;
    QrControleOrcaRapido: TSmallintField;
    QrControleDistriDescoItens: TSmallintField;
    QrControleEntraSemValor: TSmallintField;
    QrControleMensalSempre: TSmallintField;
    QrControleBalType: TSmallintField;
    QrControleOrcaOrdem: TSmallintField;
    QrControleOrcaLinhas: TSmallintField;
    QrControleOrcaLFeed: TIntegerField;
    QrControleOrcaModelo: TSmallintField;
    QrControleOrcaRodaPos: TSmallintField;
    QrControleOrcaRodape: TSmallintField;
    QrControleOrcaCabecalho: TSmallintField;
    QrControleCoresRel: TSmallintField;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleCFiscalPadr: TWideStringField;
    QrControleSitTribPadr: TWideStringField;
    QrControleCFOPPadr: TWideStringField;
    QrControleAvisosCxaEdit: TSmallintField;
    QrControleTravaCidade: TSmallintField;
    QrControleChConfCab: TIntegerField;
    QrControleImpDOS: TIntegerField;
    QrControleUnidadePadrao: TIntegerField;
    QrControleProdutosV: TIntegerField;
    QrControleCartDespesas: TIntegerField;
    QrControleReserva: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleVersao: TIntegerField;
    QrControleVerWeb: TIntegerField;
    QrControleUFPadrao: TIntegerField;
    QrControleCidade: TWideStringField;
    QrControleDono: TIntegerField;
    QrControleSoMaiusculas: TWideStringField;
    QrControleMoeda: TWideStringField;
    QrControleErroHora: TIntegerField;
    QrControleSenhas: TIntegerField;
    QrControleSenhasIts: TIntegerField;
    QrControleSalarios: TIntegerField;
    QrControleEntidades: TIntegerField;
    QrControleEntiCtas: TIntegerField;
    QrControleUFs: TIntegerField;
    QrControleListaECivil: TIntegerField;
    QrControlePerfis: TIntegerField;
    QrControleUsuario: TIntegerField;
    QrControleContas: TIntegerField;
    QrControleCentroCusto: TIntegerField;
    QrControleDepartamentos: TIntegerField;
    QrControleDividas: TIntegerField;
    QrControleDividasIts: TIntegerField;
    QrControleDividasPgs: TIntegerField;
    QrControleCarteiras: TIntegerField;
    QrControleCarteirasU: TIntegerField;
    QrControleCartaG: TIntegerField;
    QrControleCartas: TIntegerField;
    QrControleConsignacao: TIntegerField;
    QrControleGrupo: TIntegerField;
    QrControleSubGrupo: TIntegerField;
    QrControleConjunto: TIntegerField;
    QrControlePlano: TIntegerField;
    QrControleInflacao: TIntegerField;
    QrControlekm: TIntegerField;
    QrControlekmMedia: TIntegerField;
    QrControlekmIts: TIntegerField;
    QrControleFatura: TIntegerField;
    QrControleLanctos: TLargeintField;
    QrControleLctoEndoss: TIntegerField;
    QrControleEntiGrupos: TIntegerField;
    QrControleEntiContat: TIntegerField;
    QrControleEntiCargos: TIntegerField;
    QrControleEntiMail: TIntegerField;
    QrControleEntiTel: TIntegerField;
    QrControleEntiTipCto: TIntegerField;
    QrControleAparencias: TIntegerField;
    QrControlePages: TIntegerField;
    QrControleMultiEtq: TIntegerField;
    QrControleEntiTransp: TIntegerField;
    QrControleEntiRespon: TIntegerField;
    QrControleEntiCfgRel: TIntegerField;
    QrControleExcelGru: TIntegerField;
    QrControleExcelGruImp: TIntegerField;
    QrControleMediaCH: TIntegerField;
    QrControleContraSenha: TWideStringField;
    QrControleImprime: TIntegerField;
    QrControleImprimeBand: TIntegerField;
    QrControleImprimeView: TIntegerField;
    QrControleComProdPerc: TIntegerField;
    QrControleComProdEdit: TIntegerField;
    QrControleComServPerc: TIntegerField;
    QrControleComServEdit: TIntegerField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrControlePadrPlacaCar: TWideStringField;
    QrControleServSMTP: TWideStringField;
    QrControleNomeMailOC: TWideStringField;
    QrControleDonoMailOC: TWideStringField;
    QrControleMailOC: TWideStringField;
    QrControleCorpoMailOC: TWideMemoField;
    QrControleConexaoDialUp: TWideStringField;
    QrControleMailCCCega: TWideStringField;
    QrControleContVen: TIntegerField;
    QrControleContCom: TIntegerField;
    QrControleCartVen: TIntegerField;
    QrControleCartCom: TIntegerField;
    QrControleCartDeS: TIntegerField;
    QrControleCartReS: TIntegerField;
    QrControleCartDeG: TIntegerField;
    QrControleCartReG: TIntegerField;
    QrControleCartCoE: TIntegerField;
    QrControleCartCoC: TIntegerField;
    QrControleCartEmD: TIntegerField;
    QrControleCartEmA: TIntegerField;
    QrControleMoedaVal: TFloatField;
    QrControleTela1: TIntegerField;
    QrControleChamarPgtoServ: TIntegerField;
    QrControleFormUsaTam: TIntegerField;
    QrControleFormHeight: TIntegerField;
    QrControleFormWidth: TIntegerField;
    QrControleFormPixEsq: TIntegerField;
    QrControleFormPixDir: TIntegerField;
    QrControleFormPixTop: TIntegerField;
    QrControleFormPixBot: TIntegerField;
    QrControleFormFoAlt: TIntegerField;
    QrControleFormFoPro: TFloatField;
    QrControleFormUsaPro: TIntegerField;
    QrControleFormSlides: TIntegerField;
    QrControleFormNeg: TIntegerField;
    QrControleFormIta: TIntegerField;
    QrControleFormSub: TIntegerField;
    QrControleFormExt: TIntegerField;
    QrControleFormFundoTipo: TIntegerField;
    QrControleFormFundoBMP: TWideStringField;
    QrControleServInterv: TIntegerField;
    QrControleServAntecip: TIntegerField;
    QrControleAdiLancto: TIntegerField;
    QrControleContaSal: TIntegerField;
    QrControleContaVal: TIntegerField;
    QrControlePronomeE: TWideStringField;
    QrControlePronomeM: TWideStringField;
    QrControlePronomeF: TWideStringField;
    QrControlePronomeA: TWideStringField;
    QrControleSaudacaoE: TWideStringField;
    QrControleSaudacaoM: TWideStringField;
    QrControleSaudacaoF: TWideStringField;
    QrControleSaudacaoA: TWideStringField;
    QrControleNiver: TSmallintField;
    QrControleNiverddA: TSmallintField;
    QrControleNiverddD: TSmallintField;
    QrControleLastPassD: TDateTimeField;
    QrControleMultiPass: TIntegerField;
    QrControleCodigo: TIntegerField;
    QrControleAlterWeb: TSmallintField;
    QrControleAtivo: TSmallintField;
    QrMasterUsaAccMngr: TSmallintField;
    QlLocal: TMySQLBatchExecute;
    QrUpdM: TmySQLQuery;
    QrSomaM: TmySQLQuery;
    QrSomaMValor: TFloatField;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrDuplicStrX: TmySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrDuplicIntX: TmySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrFields: TmySQLQuery;
    QrDelLogX: TmySQLQuery;
    QrInsLogX: TmySQLQuery;
    QrRecCountX: TmySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrSel: TmySQLQuery;
    QrSelMovix: TIntegerField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QrAuxL: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrOpcoesBugs: TmySQLQuery;
    QrOpcoesBugsGraNivEqAp: TSmallintField;
    QrOpcoesBugsGraCodEqAp: TIntegerField;
    QrOpcoesBugsGraNivEqMo: TSmallintField;
    QrOpcoesBugsGraCodEqMo: TIntegerField;
    QrOpcoesBugsGraNivPrAp: TSmallintField;
    QrOpcoesBugsGraCodPrAp: TIntegerField;
    QrOpcoesBugsGraNivPrMo: TSmallintField;
    QrOpcoesBugsGraCodPrMo: TIntegerField;
    QrOpcoesBugsImgIPv4: TWideStringField;
    QrOpcoesBugsImgSDir: TWideStringField;
    QrControleBLQ_TopoAvisoV: TIntegerField;
    QrControleBLQ_MEsqAvisoV: TIntegerField;
    QrControleBLQ_AltuAvisoV: TIntegerField;
    QrControleBLQ_LargAvisoV: TIntegerField;
    QrControleBLQ_TopoDestin: TIntegerField;
    QrControleBLQ_MEsqDestin: TIntegerField;
    QrControleBLQ_AltuDestin: TIntegerField;
    QrControleBLQ_LargDestin: TIntegerField;
    QrOpcoesBugsSloganFoot: TWideStringField;
    QrOpcoesBugsContrtIPv4: TWideStringField;
    QrOpcoesBugsContrtSDir: TWideStringField;
    QrLastPip: TmySQLQuery;
    QrLastPipConta: TIntegerField;
    QrOpcoesBugsCodigo: TIntegerField;
    QrOpcoesBugsCxaIPv4: TWideStringField;
    QrOpcoesBugsCxaSDir: TWideStringField;
    QrOpcoesBugsCorImpPrdBkg: TIntegerField;
    QrOpcoesBugsCorImpPrdTxt: TIntegerField;
    QrOpcoesBugsCorImpPrdBld: TSmallintField;
    QrOpcoesBugsInfoImoMov: TSmallintField;
    QrOpcoesBugsCunsIPv4: TWideStringField;
    QrOpcoesBugsCunsSDir: TWideStringField;
    frxDsOpcoesBugs: TfrxDBDataset;
    QrOpcoesBugsInfQdr2Dep: TSmallintField;
    QrOpcoesBugsAgeNomAnts: TSmallintField;
    QrOpcoesBugsIdxBDAnt: TSmallintField;
    QrOpcoesBugsSWTAgenda: TSmallintField;
    DsOpcoesBugs: TDataSource;
    QrOpcoesBugsDdRotaFlh: TIntegerField;
    QrOpcoesBugsDdRotaSab: TIntegerField;
    QrOpcoesBugsDdRotaDom: TIntegerField;
    QrOpcoesBugsDdRotaFer: TIntegerField;
    QrOpcoesBugsOSPrvSta: TIntegerField;
    QrOpcoesBugsOSPrvXtraD: TIntegerField;
    QrOpcoesBugsOSPrvETC: TIntegerField;
    QrOpcoesBugsATBAutoExp: TSmallintField;
    QrOpcoesBugsFlhAgeEqi: TSmallintField;
    QrOpcoesBugsFlhEntCtt: TSmallintField;
    QrOpcoesBugsFlhNumCtr: TSmallintField;
    QrOpcoesBugsFlhEntCtr: TSmallintField;
    QrOpcoesBugsFlhEntPag: TSmallintField;
    QrOpcoesBugsFlhCondPg: TSmallintField;
    QrOpcoesBugsFlhCrtEmi: TSmallintField;
    QrOpcoesBugsCros1BordB: TIntegerField;
    QrOpcoesBugsCros1Per1B: TIntegerField;
    QrOpcoesBugsCros1Res1B: TIntegerField;
    QrOpcoesBugsCros1Res2B: TIntegerField;
    QrOpcoesBugsCros1Res3B: TIntegerField;
    QrOpcoesBugsLstCusPrd: TIntegerField;
    QrUpd2: TmySQLQuery;
    MyDBn: TmySQLDatabase;
    QrWeb: TmySQLQuery;
    QrOpcoesBugsAssPosVda: TIntegerField;
    QrOpcoesBugsFrmCPosVda: TIntegerField;
    QrOpcoesBugsAgeCorBgst: TIntegerField;
    QrOpcoesBugsAgeCorAvul: TIntegerField;
    QrOpcoesBugsPdrMntsMon: TIntegerField;
    QrOpcoesBugsMediakmh1: TIntegerField;
    QrOpcoesBugsMediakmh2: TIntegerField;
    QrOpcoesBugsMediakmh3: TIntegerField;
    QrOpcoesBugsMediakmh4: TIntegerField;
    QrOpcoesBugsMediakmh5: TIntegerField;
    QrUpdN: TmySQLQuery;
    QrOpcoesBugsFlhPrePrg: TSmallintField;
    QrOpcoesBugsMinHAgeExe: TTimeField;
    QrOpcoesBugsAutReatPIP: TSmallintField;
    QrOpcoesBugsOSPrvStaUsa: TSmallintField;
    QrOpcoesBugsOSPrvStaCod: TIntegerField;
    QrOpcoesFili: TmySQLQuery;
    QrOpcoesFiliCNPJ_CPF: TWideStringField;
    QrOpcoesFiliCodigo: TIntegerField;
    QrOpcoesFiliNumLicOper: TWideStringField;
    QrOpcoesFiliEmergencia: TWideStringField;
    QrOpcoesFiliRespTecNome: TWideStringField;
    QrOpcoesFiliRespTecDocu: TWideStringField;
    QrOpcoesFiliNO_EMP: TWideStringField;
    QrOpcoesFiliCarimboEmp: TWideStringField;
    QrOpcoesFiliRespTec1: TWideStringField;
    QrOpcoesFiliAgenDGrCab: TIntegerField;
    frxDsOpcoesFili: TfrxDBDataset;
    QrAuxN: TmySQLQuery;
    QrOpcoesBugsOSPrvOTC: TIntegerField;
    QrNotifi: TmySQLQuery;
    QrOpcoesBugsTxtFAES: TIntegerField;
    QrOpcoesFiliRespTec: TIntegerField;
    QrOpcoesFiliImgTitRelR: TWideStringField;
    QrOpcoesFiliImgTitRelL: TWideStringField;
    QrOpcoesFiliTxtTitRelC: TWideMemoField;
    QrOpcoesBugsDataGarantiaFaes: TSmallintField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure TbMovCalcFields(DataSet: TDataSet);
    procedure TbMovBeforePost(DataSet: TDataSet);
    procedure TbMovBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrEnderecoCalcFields(DataSet: TDataSet);
    procedure QrDonoCalcFields(DataSet: TDataSet);
    procedure QrControleAfterOpen(DataSet: TDataSet);
    procedure QrOpcoesBugsAfterOpen(DataSet: TDataSet);
    procedure QrOpcoesFiliAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FOpcoesFili: Boolean;
    procedure ReopenOpcoesFili(SQLCompl: String);
  public
    { Public declarations }
    FFmtPrc, FProdDelete: Integer;
    FStrFmtPrc, FStrFmtCus: String;
    //
    function Privilegios(Usuario : Integer) : Boolean;
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
    procedure VerificaSenha(Index: Integer; Login, Senha: String);

    function ConcatenaRegistros(Matricula: Integer): String;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    function BuscaProximoMovix: Integer;
    procedure InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
              DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
              CliInt: Integer; Campos: String; DescrSubstit: Boolean);
    function  TabelasQueNaoQueroCriar(): String;
    procedure ReopenControle();
    procedure MostraFormOpcoesBugs(PageIndex: Integer);
    procedure MostraFormOpcoesFili();

    // Compatibilidade Estoque / NFe
    procedure AlteraSMIA(Qry: TmySQLQuery);
    procedure ExcluiItensEstqNFsCouroImportadas();
    procedure GeraEstoqueEm_2(Empresa_Txt: String; DataIni,
      DataFim: TDateTime; PGT, GG1, GG2, GG3, TipoPesq: Integer; GraCusPrc,
      TabePrcCab: Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
      GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer; QrPGT_SCC, QrAbertura2a,
      QrAbertura2b: TmySQLQuery; UsaTabePrcCab, SoPositivos: Boolean;
      LaAviso: TLabel; NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery; var TxtSQL_Part1,
      TxtSQL_Part2: String);
    // Fim Compatibilidade

    //  Exclusivos do B U G S T R O L
    //procedure CalculaM2Total(EdM2Constru, EdM2NaoBuild, EdM2Total: TdmkEdit);
    //function  AchouNovosClientes(var MaiorCodigo: Integer): Boolean;
    //procedure ReopenBinaLigouA();
    //procedure AtendeTelefone(Qry: TmySQLQuery);
    procedure ReopenOpcoesBugs();
    //
    procedure AtualizaPIP(Codigo: Integer);
(*
    procedure PoeEmMemoryCoresStatus();
    procedure PoeEmMemoryCoresStatusOS();
    procedure PoeEmMemoryCoresStatusAvul();
    procedure PoeEmMemoryCoresStatusXXX(var Lista: TArrayListaEstatus; Tabela: String);
    function  MQLDeMemoryCoresStatusOS(const Codigo: Integer; var AgeCorIni,
              AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer): Boolean;
    function  MQLDeMemoryCoresStatusAvul(const Codigo: Integer; var AgeCorIni,
              AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer): Boolean;
    function  MQLDeMemoryCoresStatusXXX(const Lista: TArrayListaEstatus;
              const Codigo: Integer; var AgeCorIni, AgeCorFim, AgeCorDir,
              AgeCorFon, AgeCorHin: Integer): Boolean;
*)
    function  DefineArquivo(Caminho: String; LaAviso1, LaAviso2: TLabel;
              Memo: TMemo): String;
    procedure ReopenOpcoesFiliPelaEmpresa(Codigo: Integer);
    procedure ReopenOpcoesFiliPelaFilial(Codigo: Integer);
    procedure ReopenParamsEspecificos(Empresa: Integer);
    procedure AtualizaGraTabAppProdutos;
  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;

implementation

uses UnMyObjects, Principal, SenhaBoss, Servidor, MyDBCheck, UnDiario_PF,
  UnALL_Jan, UMySQLDB, InstallMySQL41, Bugstrol_Dmk, MyListas, ModuleGeral,
  ModuleFin, UnLic_Dmk, DmkDAC_PF, OpcoesBugs, DirectTerminal, UnDmkProcFunc,
  OpcoesFili, UnDmkWeb;

{$R *.DFM}

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  Versao, VerZero, Resp: Integer;
  BD: String;
  Verifica, VerificaDBTerc: Boolean;
begin
  VerificaDBTerc := False;
  FOpcoesFili    := False;
  //
  if MyDB.Connected then
    Geral.MB_Aviso('MyDB est� connectado antes da configura��o!');
  if MyLocDataBase.Connected then
    Geral.MB_Aviso('MyLocDataBase est� connectado antes da configura��o!');
  if MyDBn.Connected then
    Geral.MB_Aviso('MyDBn est� connectado antes da configura��o!');

  MyDB.LoginPrompt := False;

  ZZDB.LoginPrompt := False;
  MyLocDatabase.LoginPrompt := False;

  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  VAR_BDSENHA := 'wkljweryhvbirt';
  if not GOTOy.OMySQLEstaInstalado(FmBugstrol_Dmk.LaAviso1,
    FmBugstrol_Dmk.LaAviso2, FmBugstrol_Dmk.ProgressBar1) then
  begin
    //raise EAbort.Create('');
    //
    MyObjects.Informa2(FmBugstrol_Dmk.LaAviso1, FmBugstrol_Dmk.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    FmBugstrol_Dmk.BtEntra.Visible := False;
    Exit;
  end;
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  ZZDB.Host         := VAR_SQLHOST;
  ZZDB.UserName     := VAR_SQLUSER;
  ZZDB.UserPassword := VAR_BDSENHA;
  ZZDB.Port         := VAR_PORTA;
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE User SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then Geral.MB_Aviso('Banco de dados teste n�o se conecta!');
    end;
  end;
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MB_Aviso('M�quina cliente sem rede.');
        //Application.Terminate;
      end;
    end;
  except
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
{
  MyDB.UserName     := VAR_SQLUSER;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host         := VAR_IP;
  MyDB.Port         := VAR_PORTA;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
}
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql',  VAR_IP, VAR_PORTA,  VAR_SQLUSER,
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  QrAux.Open;
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados ' + TMeuDB +
      ' n�o existe e deve ser criado. Confirma a cria��o?');
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      if MyDB.Connected then
        MyDB.Disconnect;
      MyDB.DatabaseName := TMeuDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MB_Info('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
  (*
  MyLocDatabase.UserName     := VAR_SQLUSER;
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.Port         := VAR_PORTA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  *)
  {
  //
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  QrAuxL.Open;
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?');
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MB_Info('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
  //
  }
  Versao   := USQLDB.ObtemVersaoAppDB(MyDB);
  Verifica := False;
  //
  if Versao < CO_VERSAO then
    Verifica := True;
  //
  if Versao = 0 then
  begin
    Resp := Geral.MB_Pergunta('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?');
    if Resp = ID_YES then
      Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  if Verifica then
    ALL_Jan.MostraFormVerifiDB(True);
  //
  try
    QrMaster.Open;
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      ALL_Jan.MostraFormVerifiDB(True);
      //
      VerificaDBTerc := True;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  //
  Mylist.ConfiguracoesIniciais(1, Application.Name);
  //
  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados Geral', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  //
  if VerificaDBTerc then
  begin
    All_Jan.MostraFormVerifiDBTerceiros(True);
    //
    VerificaDBTerc := False;
  end;
  //
  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo Financeiro', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  (*
  // Conex�o na base de dados na url
  // S� pode ser ap�s abrir a tabela controle!
  // ver se est� ativado s� ent�o fazer !
  try
    DmodG.ReopenOpcoesGerl;
    
    if DmkWeb.ConexaoRemota(MyDBn, DmodG.QrWebParams, 1) then
    begin
      MyDBn.Connect;
      if MyDBn.Connected and Verifica then
      begin
        if not VAR_VERIFI_DB_CANCEL then
        begin
          Application.CreateForm(TFmVerifiDBi, FmVerifiDBi);
          with FmVerifiDbi do
          begin
            BtSair.Enabled := False;
            FVerifi := True;
            ShowModal;
            FVerifi := False;
            Destroy;
          end;
        end;
      end;
    end;
  except
    Geral.MensagemBox('N�o foi poss�vel se conectar � base de dados remota!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  *)
////////////////////////////////////////////////////////////////////////////////
  Lic_Dmk.LiberaUso5;
  //Geral.MB_Aviso('"Uso5" desabilitado!');
////////////////////////////////////////////////////////////////////////////////
  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  VAR_DB           := MyDB.DatabaseName;
end;

procedure TDmod.ReopenControle();
begin
  UnDmkDAC_PF.AbreQuery(QrControle, Dmod.MyDB);
end;

procedure TDmod.ReopenOpcoesBugs();
begin
  UMyMod.AbreQuery(QrOpcoesBugs, MyDB);
end;

procedure TDmod.ReopenOpcoesFili(SQLCompl: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOpcoesFili, Dmod.MyDB, [
    'SELECT IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMP, ',
    'IF(ass.Nome<>"", ass.Nome, ent.Nome) RespTecNome, ',
    'ass.Dados RespTecDocu, ass.Imagem RespTec1, fil.* ',
    'FROM opcoesfili fil ',
    'LEFT JOIN entidades ent ON ent.Codigo=fil.Codigo ',
    'LEFT JOIN entiassina ass ON ass.Entidade=fil.RespTec',
    'LEFT JOIN enticliint eci ON eci.CodEnti=fil.Codigo',
    SQLCompl,
    '']);
end;

procedure TDmod.ReopenOpcoesFiliPelaEmpresa(Codigo: Integer);
var
  SQL: String;
begin
  FOpcoesFili := False;
  SQL         := 'WHERE fil.Codigo=' + Geral.FF0(Codigo);
  //
  ReopenOpcoesFili(SQL);
end;

procedure TDmod.ReopenOpcoesFiliPelaFilial(Codigo: Integer);
var
  SQL: String;
begin
  FOpcoesFili := True;
  SQL         := 'WHERE eci.CodCliInt=' + Geral.FF0(Codigo);
  //
  ReopenOpcoesFili(SQL);
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  //Compatibilidade
end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
var
  Saldo: Double;
begin
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM Carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM Lanctos');
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM Lanctos');
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE Carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
(*  if Localiza = 3 then
  begin
    if Dmod.QrLanctos.State in [dsBrowse] then
    if (Dmod.QrLanctosTipo.Value = Tipo)
    and (Dmod.QrLanctosCarteira.Value = Carteira) then
    Localiza := 1;
    Dmod.DefParams;
  end;
  if Localiza = 1 then GOTOx.LocalizaCodigo(Carteira, Carteira);*)
end;

procedure TDmod.VerificaSenha(Index: Integer; Login, Senha: String);
begin
  if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
  else
  begin
    if Index = 7 then
    begin
(*      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      QrSenha.Open;
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        QrPerfis.Open;
        if QrPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else Geral.MB_Aviso(0,'Acesso negado. Senha Inv�lida');
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;                      *)
    end else ShowMessage('Em constru��o');
  end;
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
{
CNPJ n�o definido ou Empresa n�o definida.
Algumas ferramentas do aplicativo poder�o ficar inacess�veis.
C�digo = ?
}
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  ShowMessage('CNPJ n�o definido ou Empresa n�o definida. '+Chr(13)+Chr(10)+
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value+' # '+
  QrMasterCNPJ.Value;
  //
  ReopenControle();
  //
  if QrControleSoMaiusculas.Value = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
  VAR_MOEDA           := QrControleMoeda.Value;
  //
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
(*  case QrMasterTipo.Value of
    0: QrMasterEm.Value := QrMasterRazaoSocial.Value;
    1: QrMasterEm.Value := QrMasterNome.Value;
    else QrMasterEm.Value := QrMasterRazaoSocial.Value;
  end;*)
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value :=Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

procedure TDmod.QrOpcoesBugsAfterOpen(DataSet: TDataSet);
begin
  VAR_ORDEM_TXT_POPUP_AGENDA := QrOpcoesBugsAgeNomAnts.Value;
  VAR_AgeCorBgst := QrOpcoesBugsAgeCorBgst.Value;
  VAR_AgeCorAvul := QrOpcoesBugsAgeCorAvul.Value;
  FmPrincipal.Timer2.Enabled := True;//QrOpcoesBugsATBAutoExp.Value = 1;
end;

procedure TDmod.QrOpcoesFiliAfterOpen(DataSet: TDataSet);
begin
  if FOpcoesFili = True then
  begin
    if (QrOpcoesFili.FieldByName('RespTecNome').AsString = '')
    or (QrOpcoesFili.FieldByName('RespTecDocu').AsString = '')
    or (QrOpcoesFili.FieldByName('Emergencia').AsString = '') then
    begin
      if Geral.MB_Pergunta('Par�metros espec�ficos da filial n�o definidos!' +
        slineBreak + 'Deseja configurar agora?') = ID_YES then
      begin
        MostraFormOpcoesFili;
      end;
    end;
  end;
end;

{
procedure TDmod.CalculaM2Total(EdM2Constru, EdM2NaoBuild, EdM2Total: TdmkEdit);
var
  Sim, Nao: Double;
begin
  Sim := 0;
  Nao := 0;
  if EdM2Constru.ValueVariant <> Null then
    Sim := EdM2Constru.ValueVariant;
  if EdM2NaoBuild.ValueVariant <> Null then
    Nao := EdM2NaoBuild.ValueVariant;
  EdM2Total.ValueVariant := Sim + Nao;
end;
}

function TDmod.ConcatenaRegistros(Matricula: Integer): String;
(*var
  Registros, Conta: Integer;
  Liga: String;*)
begin
(*  Qr??.Close;
  Qr??.Params[0].AsInteger := Matricula;
  Qr??.Open;
  //
  Registros := Qr??.RecordCount;
  if Registros > 1 then Result := 'Pagamento de '
  else if Registros = 1 then Result := 'Pagamento de ';
  Result := Result + Qr??NOMECURSO.Value;
  Qr??.Next;
  Conta := 1;
  Liga := ', ';
  while not Qr??.Eof do
  begin
    Conta := Conta + 1;
    if Conta = Registros then Liga := ' e ';
    Result := Result + Liga + Qr??NOMECURSO.Value;
    Qr?
    ?.Next;
  end;*)
end;


procedure TDmod.TbMovCalcFields(DataSet: TDataSet);
begin
(*  TbMovSUBT.Value := TbMovQtde.Value * TbMovPreco.Value;
  TbMovTOTA.Value := TbMovSUBT.Value - TbMovDesco.Value;
  case TbMovTipo.Value of
    -1: TbMovNOMETIPO.Value := 'Venda';
     1: TbMovNOMETIPO.Value := 'Compra';
    else TbMovNOMETIPO.Value := 'INDEFINIDO';
  end;
  case TbMovENTITIPO.Value of
    0: TbMovNOMEENTIDADE.Value := TbMovENTIRAZAO.Value;
    1: TbMovNOMEENTIDADE.Value := TbMovENTINOME.Value;
    else TbMovNOMEENTIDADE.Value := 'INDEFINIDO';
  end;
  if TbMovESTQQ.Value <> 0 then
  TbMovCUSTOPROD.Value := TbMovESTQV.Value / TbMovESTQQ.Value else
  TbMovCUSTOPROD.Value := 0;*)
end;

procedure TDmod.TbMovBeforePost(DataSet: TDataSet);
begin
(*  case TbMovTipo.Value of
   -1:
    begin
      TbMovTotalV.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
      TbMovTotalC.Value := TbMovQtde.Value * TbMovCUSTOPROD.Value;
    end;
    1:
    begin
      TbMovTotalV.Value := 0;
      TbMovTotalC.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
    end;
    else begin
      //
    end;
  end;*)
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  //
end;

procedure TDmod.TbMovBeforeDelete(DataSet: TDataSet);
begin
  //FProdDelete := TbMovProduto.Value;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  WSACleanup;
  //
  // Evitar Memory Leak!!!
  VAR_SQLx.Free;
  VAR_SQL1.Free;
  VAR_SQL2.Free;
  VAR_SQLa.Free;
  //
  //MAR_SQLy.Free;
  MAR_SQLx.Free;
  MAR_SQL1.Free;
  MAR_SQL2.Free;
  MAR_SQLa.Free;
  //
end;

(*
procedure TDmod.PoeEmMemoryCoresStatus();
begin
  PoeEmMemoryCoresStatusAvul();
  PoeEmMemoryCoresStatusOS();
end;

procedure TDmod.PoeEmMemoryCoresStatusAvul();
begin
  PoeEmMemoryCoresStatusXXX(FListaEstatusAvul, 'agenstats');
end;

procedure TDmod.PoeEmMemoryCoresStatusOS();
begin
  PoeEmMemoryCoresStatusXXX(FListaEstatusOSs, 'estatusoss');
end;

procedure TDmod.PoeEmMemoryCoresStatusXXX(var Lista: TArrayListaEstatus; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEOSs, Dmod.MyDB, [
  'SELECT Codigo, AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin ',
  'FROM ' + Tabela,
  '']);
  //
  SetLength(Lista, QrEOSs.RecordCount);
  QrEOSs.First;
  while not QrEOSs.Eof do
  begin
    Lista[QrEOSs.RecNo - 1][0] := QrEOSsCodigo.Value;
    Lista[QrEOSs.RecNo - 1][1] := QrEOSsAgeCorIni.Value;
    Lista[QrEOSs.RecNo - 1][2] := QrEOSsAgeCorFim.Value;
    Lista[QrEOSs.RecNo - 1][3] := QrEOSsAgeCorDir.Value;
    Lista[QrEOSs.RecNo - 1][4] := QrEOSsAgeCorFon.Value;
    Lista[QrEOSs.RecNo - 1][5] := QrEOSsAgeCorHin.Value;
    //
    QrEOSs.Next;
  end;
end;
*)

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  FM_MASTER := 'F';
  Dmod.QrPerfis.Params[0].AsInteger := Usuario;
  Dmod.QrPerfis.Open;
  if Dmod.QrPerfis.RecordCount > 0 then
  begin
    Result := True;
    //FM_EQUIPAMENTOS             := Dmod.QrPerfisEquipamentos.Value;
    //FM_MARCAS          := Dmod.QrPerfisMarcas.Value;
    //FM_MODELOS                := Dmod.QrPerfisModelos.Value;
    //FM_SERVICOS            := Dmod.QrPerfisServicos.Value;
    //FM_PRODUTOS            := Dmod.QrPerfisProdutos.Value;
  end;
  Dmod.QrPerfis.Close;
end;

procedure TDmod.QrEnderecoCalcFields(DataSet: TDataSet);
{
var
  Natal: TDateTime;
}
begin
{
  QrEnderecoTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEnderecoRua.Value, QrEnderecoNumero.Value, False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  //
  QrEnderecoECEP_TXT.Value := Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then Natal := QrEnderecoENatal.Value
  else Natal := QrEnderecoPNatal.Value;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
  if QrEnderecoTipo.Value = 0 then
  begin
    Natal := QrEnderecoENatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEnderecoPNatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CPF';
  end;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
}
end;

function TDmod.DefineArquivo(Caminho: String; LaAviso1, LaAviso2: TLabel; Memo: TMemo): String;
var
  //NoArq,
  Host, SDir, RDir, DirDest: String;
  Empresa: Integer;
begin
  Result := '';
  Host := Dmod.QrOpcoesBugsCunsIPv4.Value;
  SDir := Dmod.QrOpcoesBugsCunsSDir.Value;
  Empresa := 0;
  RDir := '';
  //NoArq := QrCIC_ImpCaminho.Value;
  //
  if MyObjects.TentaDefinirDiretorio(
  Host, SDir, Empresa, RDir, LaAviso1, LaAviso2, False, Memo, DirDest) then
    Result := dmkPF.CaminhoArquivo(DirDest, (*NoArq*)Caminho, '');
end;

procedure TDmod.DefParams;
begin
  FmPrincipal.DefParams;
end;

procedure TDmod.ExcluiItensEstqNFsCouroImportadas();
begin
  // Compatibilidade
end;

procedure TDmod.GeraEstoqueEm_2(Empresa_Txt: String; DataIni,
  DataFim: TDateTime; PGT, GG1, GG2, GG3, TipoPesq: Integer; GraCusPrc,
  TabePrcCab: Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
  GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer; QrPGT_SCC, QrAbertura2a,
  QrAbertura2b: TmySQLQuery; UsaTabePrcCab, SoPositivos: Boolean;
  LaAviso: TLabel; NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery; var TxtSQL_Part1,
  TxtSQL_Part2: String);
begin
  Geral.MB_Info('Gera��o de estoque n�o implementada!' + sLineBreak +
  'Solicite � DERMATEK!');
end;

procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: TMySQLQuery;
DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
CliInt: Integer; Campos: String; DescrSubstit: Boolean);
begin
  Geral.MB_Info(
  'Este balancete n�o possui dados industriais adicionais neste aplicativo!');
end;

procedure TDmod.LocCod(Atual, Codigo: Integer);
begin
  FmPrincipal.LocCod(Atual, Codigo);
end;

procedure TDmod.MostraFormOpcoesBugs(PageIndex: Integer);
begin
  ReopenOpcoesBugs();
  if UmyMod.FormInsUpd_Cria(TFmOpcoesBugs, FmOpcoesBugs, afmoNegarComAviso,
    QrOpcoesBugs, stUpd) then
  begin
    FmOpcoesBugs.CBCorImpPrdTxt.Selected := QrOpcoesBugsCorImpPrdTxt.Value;
    FmOpcoesBugs.CBCorImpPrdBkg.Selected := QrOpcoesBugsCorImpPrdBkg.Value;
    FmOpcoesBugs.CkCorImpPrdBld.Checked  := Geral.IntToBool(QrOpcoesBugsCorImpPrdBld.Value);
    //
    FmOpcoesBugs.CBCros1BordB.Selected := QrOpcoesBugsCros1BordB.Value;
    FmOpcoesBugs.CBCros1Per1B.Selected := QrOpcoesBugsCros1Per1B.Value;
    FmOpcoesBugs.CBCros1Res1B.Selected := QrOpcoesBugsCros1Res1B.Value;
    FmOpcoesBugs.CBCros1Res2B.Selected := QrOpcoesBugsCros1Res2B.Value;
    FmOpcoesBugs.CBCros1Res3B.Selected := QrOpcoesBugsCros1Res3B.Value;
    //
    FmOpcoesBugs.CBAgeCorAvul.Selected := QrOpcoesBugsAgeCorAvul.Value;
    FmOpcoesBugs.CBAgeCorBgst.Selected := QrOpcoesBugsAgeCorBgst.Value;
    //
    FmOpcoesBugs.CkDataGarantiaFaes.Checked := Geral.IntToBool(QrOpcoesBugsDataGarantiaFaes.Value);
    //
    FmOpcoesBugs.PageControl1.ActivePageIndex := PageIndex;
    //
    FmOpcoesBugs.ShowModal;
    FmOpcoesBugs.Destroy;
    //
    ReopenOpcoesBugs();
  end;
end;

procedure TDmod.MostraFormOpcoesFili;
begin
  if DBCheck.CriaFm(TFmOpcoesFili, FmOpcoesFili, afmoNegarComAviso) then
  begin
    FmOpcoesFili.ShowModal;
    FmOpcoesFili.Destroy;
  end;
end;

(*
function TDmod.MQLDeMemoryCoresStatusAvul(const Codigo: Integer; var AgeCorIni,
  AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer): Boolean;
begin
  MQLDeMemoryCoresStatusXXX(FListaEstatusAvul, Codigo,
    AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin);
end;

function TDmod.MQLDeMemoryCoresStatusOS(const Codigo: Integer; var AgeCorIni,
AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer): Boolean;
begin
  MQLDeMemoryCoresStatusXXX(FListaEstatusOSs, Codigo,
    AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin);
end;

function TDmod.MQLDeMemoryCoresStatusXXX(const Lista: TArrayListaEstatus;
  const Codigo: Integer; var AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon,
  AgeCorHin: Integer): Boolean;
var
  I: Integer;
begin
  AgeCorIni := clWhite;
  AgeCorFim := clWhite;
  AgeCorDir := 1;
  AgeCorFon := clBlack;
  AgeCorHin := clRed;
  //
  for I := 0 to High(Lista) do
  begin
    if Lista[I][0] = Codigo then
    begin
      AgeCorIni := Lista[I][1];
      AgeCorFim := Lista[I][2];
      AgeCorDir := Lista[I][3];
      AgeCorFon := Lista[I][4];
      AgeCorHin := Lista[I][5];
      //
      Result := True;
      Exit;
    end;
  end;
end;
*)

{
function TDmod.AchouNovosClientes(var MaiorCodigo: Integer): Boolean;
var
  Codigo: Integer;
begin
  Result := False;
  MaiorCodigo := 0;
  Codigo := 0;
  try
    UnDMkDAC_PF.AbreMySQLQuery0(QrNew, MyDB, [
    'SELECT Codigo ',
    'FROM entidades ',
    'WHERE Cliente1="V" ',
    'AND NOT (Codigo IN ( ',
    '     SELECT Codigo FROM cunscad ',
    '     ) ',
    ') ',
    '']);
    if QrNew.RecordCount > 0 then
    begin
      QrNew.First;
      while not QrNew.Eof do
      begin
        Codigo := QrNewCodigo.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cunscad', False, [
        (*'AtivPrinc', 'HowFind', 'Account',
        'DataCon'*)], [
        'Codigo'], [
        (*AtivPrinc, HowFind, Account,
        DataCon*)], [
        Codigo], True);
        //
        QrNew.Next;
      end;
      MaiorCodigo := Codigo;
      Result := True;
    end;
  except
    // nada
  end;
end;
}

procedure TDmod.AlteraSMIA(Qry: TmySQLQuery);
begin
  // Compatibilidade?
end;

procedure TDmod.AtualizaGraTabAppProdutos;
var
  PrdGrupTip, GraTabApp: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
      'SELECT DISTINCT gg1.PrdGrupTip, pgt.GraTabApp ',
      'FROM gragru1 gg1 ',
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo = gg1.PrdGrupTip ',
      '']);
    if QrAux.RecordCount > 0 then
    begin
      QrAux.First;
      //
      while not QrAux.EOF do
      begin
        PrdGrupTip := QrAux.FieldByName('PrdGrupTip').AsInteger;
        GraTabApp  := QrAux.FieldByName('GraTabApp').AsInteger;
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(QrUpd, Dmod.MyDB, [
          'UPDATE gragru1 ',
          'SET  GraTabApp=' + Geral.FF0(GraTabApp),
          'WHERE PrdGrupTip=' + Geral.FF0(PrdGrupTip),
          'AND GraTabApp<>' + Geral.FF0(GraTabApp),
          '']);
        //
        QrAux.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TDmod.AtualizaPIP(Codigo: Integer);
const
  MotDesativ = 0;
  DtaDesativ = '0000-00-00';
  MotInutili = 0;
  DtaInutili = '0000-00-00';
var
  OSMonCab: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLastPip, Dmod.MyDB, [
  'SELECT MAX(Conta) Conta ',
  'FROM osmoncab ',
  'WHERE PipCad=' + Geral.FF0(Codigo),
  '']);
  OSMonCab := QrLastPipConta.Value;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False, [
  'OSMonCab',
  'MotDesativ', 'DtaDesativ',
  'MotInutili', 'DtaInutili'
  ], ['Codigo'], [
  OSMonCab,
  MotDesativ, DtaDesativ,
  MotInutili, DtaInutili
  ], [Codigo], True);
end;

function TDmod.BuscaProximoMovix: Integer;
begin
  QrSel.Close;
  QrSel.Open;
  Result := QrSelMovix.Value;
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  FFmtPrc         := Dmod.QrControleCasasProd.Value;
  FStrFmtPrc      := dmkPF.StrFmt_Double(Dmod.QrControleCasasProd.Value);
  FStrFmtCus      := dmkPF.StrFmt_Double(4);
  VAR_MEULOGOPATH := QrControleMeuLogoPath.Value;
  //
  if FileExists(VAR_MEULOGOPATH) then
    VAR_MEULOGOEXISTE := True
  else
    VAR_MEULOGOEXISTE := False;
end;

procedure TDmod.QrDonoCalcFields(DataSet: TDataSet);
{
var
  Natal: TDateTime;
}
begin
{
  QrDonoTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoTe1.Value);
  QrDonoTE2_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoTe2.Value);
  QrDonoFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoFax.Value);
  QrDonoCEL_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoECel.Value);
  QrDonoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrDonoCNPJ_CPF.Value);
  //
  QrDonoNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrDonoRua.Value, Trunc(QrDonoNumero.Value), False);
  QrDonoE_LNR.Value := QrDonoNOMELOGRAD.Value;
  if Trim(QrDonoE_LNR.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ';
  QrDonoE_LNR.Value := QrDonoE_LNR.Value + QrDonoRua.Value;
  if Trim(QrDonoRua.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ', ' + QrDonoNUMERO_TXT.Value;
  if Trim(QrDonoCompl.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ' + QrDonoCompl.Value;
  if Trim(QrDonoBairro.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' - ' + QrDonoBairro.Value;
  //
  QrDonoE_LN2.Value := '';
  if Trim(QrDonoCidade.Value) <>  '' then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + ' - '+QrDonoCIDADE.Value;
  QrDonoE_LN2.Value := QrDonoE_LN2.Value + ' - '+QrDonoNOMEUF.Value;
  if QrDonoCEP.Value > 0 then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + '  CEP ' +Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  QrDonoE_CUC.Value := QrDonoE_LNR.Value + QrDonoE_LN2.Value;
  //
  QrDonoECEP_TXT.Value :=Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  if QrDonoTipo.Value = 0 then Natal := QrDonoENatal.Value
  else Natal := QrDonoPNatal.Value;
  if Natal < 2 then QrDonoNATAL_TXT.Value := ''
  else QrDonoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
  //
  if Trim(QrDonoTE1.Value) <> '' then
    QrDonoFONES.Value := QrDonoTE1_TXT.Value;
  if Trim(QrDonoTe2.Value) <> '' then
    QrDonoFONES.Value := QrDonoFONES.Value + ' - ' + QrDonoTE2_TXT.Value;
}
end;

{
N�o foi poss�vel a conex�o ao IP: [127.0.0.1]
Erro retornado: "Conex�o - Erro n�.10051"
function TUnGOTOy.ConectaAoServidor()
}

end.
