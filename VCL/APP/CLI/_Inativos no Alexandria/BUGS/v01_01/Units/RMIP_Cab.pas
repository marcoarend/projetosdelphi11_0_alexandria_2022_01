unit RMIP_Cab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkEditCB, Db,
  mySQLDbTables, DBCtrls, dmkDBLookupComboBox, ComCtrls, dmkEditDateTimePicker,
  frxClass, frxDBSet, dmkGeral, Grids, DBGrids, dmkCheckGroup, Math, dmkDBGrid,
  dmkDBGridZTO, dmkCheckBox, Mask, TypInfo, UnDmkProcFunc, DmkDAC_PF, dmkImage,
  frxExportPDF, frxDock, frxCtrls, UnDmkEnums, UnProjGroup_Consts, dmkDBGridDAC,
  UnMyObjects, frxChart, Vcl.OleCtrls, SHDocVw, UnProjGroup_Vars,
  // WebBrowser
  Winapi.ActiveX;

type
  TFmRMIP_Cab = class(TForm)
    QrLista: TmySQLQuery;
    DsLista: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel10: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel12: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    Label3: TLabel;
    EdTempo: TdmkEdit;
    BtSaida: TBitBtn;
    BtEnviar: TBitBtn;
    BtConfigura: TBitBtn;
    frxIMP_ReMIP_001_001: TfrxReport;
    QrSels: TmySQLQuery;
    QrSelsItens: TLargeintField;
    QrRMIPCfgCad: TmySQLQuery;
    QrRMIPCfgCadCodigo: TIntegerField;
    QrRMIPCfgCadNome: TWideStringField;
    DsRMIPCfgCad: TDataSource;
    QrListaCodigo: TIntegerField;
    QrListaNome: TWideStringField;
    QrListaControle: TIntegerField;
    QrListaItemRel: TIntegerField;
    QrListaOrdem: TIntegerField;
    QrListaAtivo: TIntegerField;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    QrListaNO_REL: TWideStringField;
    frxChartObject1: TfrxChartObject;
    frxIMP_ReMIP_001_R002: TfrxReport;
    frxIMP_ReMIP_001_R001: TfrxReport;
    QrClientesNO_CLIENTE: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel7: TPanel;
    Panel5: TPanel;
    Panel8: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Panel14: TPanel;
    Label7: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Panel15: TPanel;
    Label2: TLabel;
    GroupBox4: TGroupBox;
    Panel16: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    TPData: TdmkEditDateTimePicker;
    Panel17: TPanel;
    Label10: TLabel;
    EdTitulo: TEdit;
    Panel6: TPanel;
    Panel9: TPanel;
    CGResumido: TdmkCheckGroup;
    PnListaCompl: TPanel;
    Panel11: TPanel;
    RGOpcoesListas: TRadioGroup;
    BtOrdem: TBitBtn;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    DBGLista: TdmkDBGridZTO;
    PnCfgCad: TPanel;
    Label6: TLabel;
    EdCfgCab: TdmkEditCB;
    CBCfgCab: TdmkDBLookupComboBox;
    TabSheet2: TTabSheet;
    WebBrowser1: TWebBrowser;
    Imagem: TTabSheet;
    Image1: TImage;
    frxReport1: TfrxReport;
    Button1: TButton;
    Button2: TButton;
    EdWebRes: TdmkEdit;
    ProgressBar1: TProgressBar;
    CkDoLocalize: TCheckBox;
    GroupBox8: TGroupBox;
    Memo1: TMemo;
    PnSub0: TPanel;
    LaSub0B: TLabel;
    LaSub0A: TLabel;
    PnSub1: TPanel;
    LaSub1A: TLabel;
    LaSub1B: TLabel;
    PBSub1: TProgressBar;
    PnSub2: TPanel;
    LaSub2B: TLabel;
    LaSub2A: TLabel;
    PBSub2: TProgressBar;
    QrTxtGeneric: TmySQLQuery;
    QrTxtGenericCodigo: TIntegerField;
    QrTxtGenericNome: TWideStringField;
    QrTxtGenericTexto: TWideMemoField;
    QrTxtGenericAplicacao: TIntegerField;
    EdParecer: TdmkEditCB;
    Label11: TLabel;
    CBParecer: TdmkDBLookupComboBox;
    frxDsTxtGeneric: TfrxDBDataset;
    CkPaginar: TdmkCheckBox;
    Label4: TLabel;
    EdEntiRespon: TdmkEditCB;
    CBEntiRespon: TdmkDBLookupComboBox;
    QrResponsaveis: TmySQLQuery;
    DsResponsaveis: TDataSource;
    QrResponsaveisControle: TIntegerField;
    QrResponsaveisNome: TWideStringField;
    QrEntiRespon: TmySQLQuery;
    QrEntiResponCodigo: TIntegerField;
    QrEntiResponControle: TIntegerField;
    QrEntiResponNome: TWideStringField;
    QrEntiResponCargo: TIntegerField;
    QrEntiResponAssina: TSmallintField;
    QrEntiResponOrdemLista: TIntegerField;
    QrEntiResponObserv: TWideStringField;
    QrEntiResponNOME_CARGO: TWideStringField;
    QrEntiResponNO_ASSINA: TWideStringField;
    QrEntiResponMandatoIni: TDateField;
    QrEntiResponMandatoFim: TDateField;
    QrEntiResponMandatoIni_TXT: TWideStringField;
    QrEntiResponMandatoFim_TXT: TWideStringField;
    QrEntiResponTe1: TWideStringField;
    QrEntiResponCel: TWideStringField;
    QrEntiResponEmail: TWideStringField;
    QrEntiResponEntidade: TIntegerField;
    QrEntiResponTe1_TXT: TWideStringField;
    QrEntiResponCel_TXT: TWideStringField;
    frxDsEntiRespon: TfrxDBDataset;
    QrPareceres: TmySQLQuery;
    DsPareceres: TDataSource;
    QrPareceresCodigo: TIntegerField;
    QrPareceresNome: TWideStringField;
    CkPizzaUltima: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure RGFormatoClick(Sender: TObject);
    procedure RGOpcoesListasClick(Sender: TObject);
    procedure BtOrdemClick(Sender: TObject);
    procedure QrListaBeforeClose(DataSet: TDataSet);
    procedure QrListaAfterOpen(DataSet: TDataSet);
    procedure BtEnviarClick(Sender: TObject);
    procedure BtConfiguraClick(Sender: TObject);
    procedure EdCfgCabChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure frxIMP_ReMIP_001_R001GetValue(const VarName: string;
      var Value: Variant);
    procedure frxIMP_ReMIP_001_001GetValue(const VarName: string;
      var Value: Variant);
    procedure Button1Click(Sender: TObject);
    procedure frxReport1GetValue(const VarName: string; var Value: Variant);
    procedure Button2Click(Sender: TObject);
    procedure WebBrowser1ProgressChange(ASender: TObject; Progress,
      ProgressMax: Integer);
    procedure frxIMP_ReMIP_001_R002GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FDataI, FDataF: String;
    FAtzContasMensais2: Boolean ;
    F_RMIP_: String;
    FPeriodoAtu, FPeriodoAnt, FPeriodoIni, FPeriodoFim: Integer;
    FArre1, FArre2, FDataAnt, FDataIni, FDataBal, FDataAtu: String;
    FResumido_01, FResumido_02, FResumido_03,
    FResumido_04, FResumido_05, FResumido_06, FResumido_07: Boolean;
    FIni, FFim, FTempo: TDateTime;
    FCarregouSVG: Boolean;
    FRelSelecionado: array[0..MaxListaItensRMIP] of Boolean;

    //function PeriodoNaoDefinido(CliInt, Periodo: Integer): Boolean;
    procedure AtualizaItensAImprimir();
    function  DatasInsuficientes(var QtdDtas: Integer): Integer;
    procedure ImprimeRMIP(Exporta: Boolean);
    procedure InfoGerRel(Extra: String);
    procedure MultiImp_RMIP_R003_GeraImp_PizzaRevisaoPMV(CLR: Boolean);
    procedure MultiImp_RMIP_R005_GeraImp_PizzaRevisaoDep(CLR: Boolean);
    function  ObtemNomeClienteCompleto(): String;
    function  ObtemNomeClienteSimples(): String;
    procedure ReopentTxtGeneric();
    procedure ReopenEntiRespon();
    procedure ReopenPareceres();
(*
    procedure ExportaRMIP(Frx: TfrxReport; PrepRep: Boolean);
*)
    function EstatusOSsSemExecucaoDefinida(): Boolean;
    // WebBrowser
    procedure TesteCapturaImgSVG_WebBrowser();
    procedure WebBrowserScreenShot(const wb: TWebBrowser; const fileName: TFileName);
    procedure WBLoadHTML(WebBrowser: TWebBrowser; HTMLCode: string);
    // FIM WebBrowser
  public
    { Public declarations }
    //FPBB: Integer;
    FConfiguInicial, FPaginar: Boolean;
    FEntidade, FCliInt,
    FClienteInicial,
    FPeriodoInicial: Integer;
    FTabAriA, FTabCnsA, FTabPriA, FTabPrvA,
    FEnt_Txt, FTabLctA, FTabLctB, FTabLctD(*, FTabLctX*): String;
    //
    FEmpresa, FCliente: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    //
    function frxComposite(frxAtu: TfrxReport; var ClearLastReport: Boolean):
             Boolean;
  end;

  var
  FmRMIP_Cab: TFmRMIP_Cab;

implementation

uses UnInternalConsts, UnFinanceiro, ModuleGeral, ModuleFin, Module, UCreate,
  MeuFrx, Resmes, MyDBCheck, UMySQLModule,
  {$IfDef CO_DMKID_APP_0004}
  Principal,
  {$EndIf}
  MyListas, RMIPCfgCad, CreateBugs, ModOS, OSImp2, RMIP_R011_Pre,
  ModuleRMIP, RMIP_R003, RMIP_R004, RMIP_R005, RMIP_R006, RMIP_R007, RMIP_R008,
  RMIP_R009, RMIP_R010, RMIP_R011, RMIP_R012, RMIP_R013, RMIP_R014, RMIP_R015,
  RMIP_R016;

const
  FCaminhoEMail = CO_DIR_RAIZ_DMK + '\FTP\RMIP\';
  XML_SVG =
'<?xml version="1.0" standalone="no"?>' +
'<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"' +
'  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">' +
'<svg width="6cm" height="5cm" viewBox="0 0 600 500"' +
'     xmlns="http://www.w3.org/2000/svg" version="1.1">' +
'  <desc>Example script01 - invoke an ECMAScript function from an onclick event' +
'  </desc>' +
'  <!-- ECMAScript to change the radius with each click -->' +
'  <script type="application/ecmascript"> <![CDATA[' +
'    function circle_click(evt) {' +
'      var circle = evt.target;' +
'      var currentRadius = circle.getAttribute("r");' +
'      if (currentRadius == 100)' +
'        circle.setAttribute("r", currentRadius*2);' +
'      else' +
'        circle.setAttribute("r", currentRadius*0.5);' +
'    }' +
'  ]]> </script>' +
'' +
'  <!-- Outline the drawing area with a blue line -->' +
'  <rect x="1" y="1" width="598" height="498" fill="none" stroke="blue"/>' +
'' +
'  <!-- Act on each click event -->' +
'  <circle onclick="circle_click(evt)" cx="300" cy="225" r="100"' +
'          fill="red"/>' +
'' +
'  <text x="300" y="480"' +
'        font-family="Verdana" font-size="35" text-anchor="middle">' +
'' +
'    Click on circle to change its size' +
'  </text>' +
'</svg>';

{$R *.DFM}

procedure TFmRMIP_Cab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRMIP_Cab.BtTudoClick(Sender: TObject);
begin
  //case RGFormato.ItemIndex of
    //-1,
    //0:
    MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGLista), True);
    //1: CGResumido.SetMaxValue;
  //end;
end;

procedure TFmRMIP_Cab.Button1Click(Sender: TObject);
begin
  FCarregouSVG := False;
  WBLoadHTML(WebBrowser1, XML_SVG) ;
  while not FCarregouSVG do
    Sleep(500);
  MyObjects.frxMostra(frxReport1, 'teste bmp');
end;

procedure TFmRMIP_Cab.Button2Click(Sender: TObject);
begin
  WebBrowserScreenShot(WebBrowser1, 'c:\WebBrowserImage.jpg') ;
  MyObjects.frxMostra(frxReport1, 'teste bmp');
end;

function TFmRMIP_Cab.DatasInsuficientes(var QtdDtas: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  if QtdDtas = - 1 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT cab.DtaExeFim ',
    'FROM oscab cab ',
    dmkPF.SQL_Periodo('WHERE cab.DtaExeFim', FDtaIni, FDtaFim, True, True),
    'AND cab.Entidade=' + Geral.FF0(FCliente),
    '']);
    //
    QtdDtas := Qry.RecordCount;
  end;
  Result := QtdDtas;
end;

procedure TFmRMIP_Cab.EdCfgCabChange(Sender: TObject);
begin
  if RGOpcoesListas.ItemIndex = 0 then
    AtualizaItensAImprimir();
end;

procedure TFmRMIP_Cab.EdClienteChange(Sender: TObject);
begin
  if not EdCliente.Focused then
    AtualizaItensAImprimir();
end;

procedure TFmRMIP_Cab.EdClienteExit(Sender: TObject);
begin
  AtualizaItensAImprimir();
end;

procedure TFmRMIP_Cab.EdEmpresaChange(Sender: TObject);
begin
  if not EdEmpresa.Focused then
  begin
    AtualizaItensAImprimir();
    ReopenEntiRespon();
  end;
end;

procedure TFmRMIP_Cab.EdEmpresaExit(Sender: TObject);
begin
  AtualizaItensAImprimir();
  ReopenEntiRespon();
end;

function TFmRMIP_Cab.EstatusOSsSemExecucaoDefinida(): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM estatusoss ',
  'WHERE Execucao=0 ',
  '']);
  //
  Result := Qry.RecordCount > 0;
  if Result then
    Geral.MB_Aviso('Existem ' + IntToStr(Qry.RecordCount) +
    ' cadastros de "Status de OSs" sem defini��o de "Execu��o da OS"!' +
    sLineBreak +
    '� necess�rio corrigir oa cadastros para integridade dos relat�rios!');
end;

procedure TFmRMIP_Cab.FormActivate(Sender: TObject);
var
  Mes, Ano: Word;
begin
  MyObjects.CorIniComponente();
  if not FConfiguInicial then
  begin
    FConfiguInicial := True;
    if FClienteInicial <> 0 then
    begin
      if DModG.QrEmpresas.Locate('Codigo', FClienteInicial, []) then
      begin
        EdEmpresa.ValueVariant := DModG.QrEmpresasFilial.Value;
        CBEmpresa.KeyValue     := DModG.QrEmpresasFilial.Value;
      end;
    end;
    if FPeriodoInicial > 0 then
    begin
      dmkPF.PeriodoDecode(FPeriodoInicial, Ano, Mes);
      //CBAno.Text      := FormatFloat('0', Ano);
      //CBMes.ItemIndex := Mes - 1;
    end;
  end;
end;

procedure TFmRMIP_Cab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRMIP_Cab.QrListaAfterOpen(DataSet: TDataSet);
begin
  BtOrdem.Enabled := QrLista.RecordCount > 0;
end;

procedure TFmRMIP_Cab.QrListaBeforeClose(DataSet: TDataSet);
begin
  BtOrdem.Enabled := False;
end;

procedure TFmRMIP_Cab.RGOpcoesListasClick(Sender: TObject);
begin
  AtualizaItensAImprimir();
end;

procedure TFmRMIP_Cab.TesteCapturaImgSVG_WebBrowser;
var
   sHTML : string;
begin
  // Mostar SVG em WebBrowser e copiar para o FastReport via MemoryStream
  // Capturing a Screen Shot of a TWebBrowser Content (Web Page) in Delphi
  // Nao funciona!!
  Geral.MB_Info('http://delphi.about.com/od/vclusing/a/wb_scren_shot.htm');
  // How to load HTML directly to a WebBrowser
{
  Geral.MB_Info('http://delphi.about.com/cs/adptips2004/a/bltip0104_4.htm');
   sHTML := '<a href="http://delphi.about.com">GOTO</a>' +
            '<b>About Delphi Programming</b>';
   WBLoadHTML(WebBrowser1, sHTML) ;
}
  WebBrowser1.Navigate('https://www.google.com.br/') ;
(*

Capturing a Screen Shot of a TWebBrowser Content (Web Page) in Delphi

Taking a Web Site's Screen Shot

By Zarko Gajic
.

Ads: Delphi
 Capture a Screenshot
 Capture
 Taking Screenshot
 Desktop Screenshot






Ads

WPF WebBrowser Controlwww.essentialobjects.comIntegrate web pages in your WPF app Full C# and VB sample code

Quer Notebook p/ Empresa?www.dell.com/br/NotebookDell� Latitude 15 S�rie 3000 com Intel� Core� e HD500 em at� 10x!

Delphi Globalization Toolwww.tsilang.comGlobalize your Delphi applications easy, professionally and fast!

Top Related Searches�twebbrowser delphi
�delphi apps
�web page document
�web browser screen
�nbsp nbsp nbsp nbsp nbsp
�graphics file


The TWebBrowser Delphi control provides access to the Web browser functionality from your Delphi apps - to allow you to create a customized Web browsing application or to add Internet, file and network browsing, document viewing, and data downloading capabilities to your applications.

Web Page Screen Shot
You might need to programmatically grab the screen shot of the current page loaded in the web browser control.
A screen shot (screen capture) is a copy of the screen's contents that can be saved as a graphics file.
Web Browser screen shot is a graphics copy of the content on the web browser control - usually a web page (document).

The custom function WebBrowserScreenShot will capture the contents of a TWebBrower's client area into a JPEG image and save it to a specified file.
 uses ActiveX;

 procedure WebBrowserScreenShot(const wb: TWebBrowser; const fileName: TFileName) ;
 var
   viewObject : IViewObject;
   r : TRect;
   bitmap : TBitmap;
 begin
   if wb.Document <> nil then
   begin
     wb.Document.QueryInterface(IViewObject, viewObject) ;
     if Assigned(viewObject) then
     try
       bitmap := TBitmap.Create;
       try
         r := Rect(0, 0, wb.Width, wb.Height) ;

         bitmap.Height := wb.Height;
         bitmap.Width := wb.Width;

         viewObject.Draw(DVASPECT_CONTENT, 1, nil, nil, Application.Handle, bitmap.Canvas.Handle, @r, nil, nil, 0) ;

         with TJPEGImage.Create do
         try
           Assign(bitmap) ;
           SaveToFile(fileName) ;
         finally
           Free;
         end;
       finally
         bitmap.Free;
       end;
     finally
       viewObject._Release;
     end;
   end;
 end;
Note: JPEG images are smaller when compared to BMPs - this is why the BMP object is converted to a JPG before saving to the disk.
A sample usage: navigate to a web site in the form's OnCreate event, take the screen shot in the webbrowser's OnNavigateComplete2 event:
 procedure TForm1.FormCreate(Sender: TObject) ;
 begin
   WebBrowser1.Navigate('http://delphi.about.com') ;
 end;

 procedure TForm1.WebBrowser1NavigateComplete2(ASender: TObject; const pDisp: IDispatch; var URL: OleVariant) ;
 begin
   WebBrowserScreenShot(WebBrowser1,'c:\WebBrowserImage.jpg') ;
 end;
Note: the code above saves the "http://delphi.about.com" site's screen shot to a file named WebBrowserImage.jpg on the C drive.
*)
end;

procedure TFmRMIP_Cab.WBLoadHTML(WebBrowser: TWebBrowser; HTMLCode: string);
var
  sl: TStringList;
  ms: TMemoryStream;
  Res: HRESULT;
begin
  WebBrowser.Navigate('about:blank') ;
  while WebBrowser.ReadyState < READYSTATE_INTERACTIVE do
   Application.ProcessMessages;

  if Assigned(WebBrowser.Document) then
  begin
    sl := TStringList.Create;
    try
      ms := TMemoryStream.Create;
      try
        sl.Text := HTMLCode;
        sl.SaveToStream(ms) ;
        ms.Seek(0, 0) ;
        FCarregouSVG := False;
        Res :=  (WebBrowser.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms));
        EdWebRes.ValueVariant := Res;
        WebBrowser.Invalidate;
        WebBrowserScreenShot(WebBrowser1, 'c:\WebBrowserImage.jpg') ;
      finally
        ms.Free;
      end;
    finally
      sl.Free;
    end;
  end;
end;

procedure TFmRMIP_Cab.WebBrowser1ProgressChange(ASender: TObject; Progress,
  ProgressMax: Integer);
begin
  if Progress > 0 then
  begin
    ProgressBar1.Max      := ProgressMax;
    ProgressBar1.Position := Progress;
    if Progress >= ProgressMax then
      WebBrowserScreenShot(WebBrowser1, 'c:\WebBrowserImage.jpg') ;
  end else
  begin
    ProgressBar1.Position := 0
  end;

end;

procedure TFmRMIP_Cab.WebBrowserScreenShot(const wb: TWebBrowser;
  const fileName: TFileName);
var
  frxBmp: TfrxPictureView;
var
  viewObject : IViewObject;
  r : TRect;
  bitmap : TBitmap;
begin
  if wb.Document <> nil then
  begin
    wb.Document.QueryInterface(IViewObject, viewObject) ;
    if Assigned(viewObject) then
    try
      bitmap := TBitmap.Create;
      try
        r := Rect(0, 0, wb.Width, wb.Height) ;

        bitmap.Height := wb.Height;
        bitmap.Width := wb.Width;

        viewObject.Draw(DVASPECT_CONTENT, 1, nil, nil, Application.Handle, bitmap.Canvas.Handle, @r, nil, nil, 0) ;

frxBmp := frxReport1.FindObject('Picture1') as TfrxPictureView;
frxBmp.Picture.Bitmap.Assign(Bitmap);
FCarregouSVG := True;

        {
        with TJPEGImage.Create do
        try
          Assign(bitmap) ;
          SaveToFile(fileName) ;
        finally
          Free;
        end;
        }
        Image1.Picture.Assign(bitmap);
      finally
        bitmap.Free;
      end;
    finally
      viewObject._Release;
    end;
  end;
end;

procedure TFmRMIP_Cab.ReopenEntiRespon();
var
  Entidade: Integer;
begin
  if EdEmpresa.ValueVariant <> 0 then
    Entidade := DModG.QrEmpresasCodigo.Value
  else
    Entidade := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrResponsaveis, Dmod.MyDB, [
  'SELECT Controle, Nome ',
  'FROM entirespon ',
  'WHERE Codigo=' + Geral.FF0(Entidade),
  'ORDER BY Nome ',
  '']);
  EdEntiRespon.ValueVariant := 0;
  CBEntiRespon.KeyValue     := 0;
end;

procedure TFmRMIP_Cab.ReopenPareceres();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPareceres, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM txtgeneric ',
  'WHERE Aplicacao & 8 ',
  '']);
end;

procedure TFmRMIP_Cab.ReopentTxtGeneric();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTxtGeneric, Dmod.MyDB, [
  'SELECT * ',
  'FROM txtgeneric ',
  'WHERE Codigo=' + Geral.FF0(EdParecer.ValueVariant),
  '']);
end;

procedure TFmRMIP_Cab.RGFormatoClick(Sender: TObject);
begin
(*
  PnListaCompl.Visible   := False;
  CGResumido.Visible := False;
  case RGFormato.ItemIndex of
    0: PnListaCompl.Visible   := True;
    1: CGResumido.Visible := True;
  end;
*)
end;

procedure TFmRMIP_Cab.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  //
  UnDMkDAC_PF.AbreQuery(QrRMIPCfgCad, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  ReopenPareceres();
  //
  F_RMIP_       := UnCreateBugs.RecriaTempTableNovo(ntrtt_RMIP, DmodG.QrUpdPID1, False);
  DmRMIP.CriaTempTables();
  //
  TPIni.Date := Int(Date - 30);
  TPFim.Date := Int(Date);

  //////////////////////////////////////////////////////////////////////////////

  CGResumido.Align := alClient;
  CGResumido.SetMaxValue;
  //
  FClienteInicial := 0;
  FPeriodoInicial := -1000;
  FConfiguInicial := False;
  //MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaSub0A, LaSub0B, False, '...');
  MyObjects.Informa2(LaSub1A, LaSub1B, False, '...');
  MyObjects.Informa2(LaSub2A, LaSub2B, False, '...');
  //MyObjects.Informa2(LaSub3A, LaSub3B, False, '...');
  //
  TPData.Date := Date;
  //EdDtEncer.DataSource := DModG.DsLastEncer;
  //EdDtMorto.DataSource := DModG.DsLastMorto;
  //
  {$IfDef CO_DMKID_APP_0004}
    BtEnviar.Visible := True;
  {$Else}
    BtEnviar.Visible := False;
  {$EndIf}
  CBEmpresa.ListSource := DModG.DsEmpresas;
  EstatusOSsSemExecucaoDefinida();


  //////////////////////////////////////////////////////////////////////////////
  /// TEMPORARIO ///////////////////////////////////////////////////////////////
  EdEmpresa.ValueVariant := 1;
  CBEmpresa.KeyValue     := 1;
  EdCliente.ValueVariant := 285;
  CBCliente.KeyValue     := 285;
  EdCfgCab.ValueVariant  := 1;
  CBCfgCab.KeyValue      := 1;

  TPIni.Date := EncodeDate(2014, 11, 1);
  TPFim.Date := EncodeDate(2014, 11, 30);
  CkDoLocalize.Checked := False;
  EdParecer.ValueVariant := 7;
  CBParecer.KeyValue     := 7;
  EdEntiRespon.ValueVariant := 8;
  CBEntiRespon.KeyValue     := 8;
  RGOpcoesListas.ItemIndex := 1;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////


end;

procedure TFmRMIP_Cab.AtualizaItensAImprimir();
var
  CfgCad, I, N, Empresa, Cliente: Integer;
  SQL, Aplic: String;
  NO_ItemRel: String;
begin
  PnCfgCad.Visible := RGOpcoesListas.ItemIndex = 0;
  QrLista.Close;
  Empresa := EdEmpresa.ValueVariant;
  if Empresa = 0 then
    Exit;
  //
  CfgCad := EdCfgCab.ValueVariant;
  case RGOpcoesListas.ItemIndex of
    0:
    begin
      Cliente := EdCliente.ValueVariant;
      if Cliente <> 0 then
        Aplic := Geral.FF0(Integer(arciCliente))
      else
        Aplic := Geral.FF0(Integer(arciEmpresa));
      //
      NO_ItemRel := dmkPF.ArrayToTexto('its.ItemRel', 'Nome', pvPos, True,
        sListaItensRMIP);
      //
      SQL := Geral.ATS([
      'DROP TABLE IF EXISTS ' + F_RMIP_ + '; ',
      'CREATE TABLE ' + F_RMIP_ + ' ',
      'SELECT cad.Codigo, ' + NO_ItemRel,
      'its.Nome Titulo, its.Controle, ',
      'its.ItemRel, its.Ordem, 1 Ativo ',
      'FROM ' + TMeuDB + '.rmipcfgcad cad',
      'LEFT JOIN ' + TMeuDB + '.rmipcfgits its ON its.Codigo=cad.Codigo',
      'WHERE cad.Codigo=' + FormatFloat('0', CfgCad),
      'AND its.Aplicacao IN (' + Aplic + ', 3) ' +
      '; ',
      '']);
    end;
    1:
    begin
      SQL := 'DELETE FROM ' + F_RMIP_ + '; ';
      for I := 1 to MaxListaItensRMIP do
      begin
        SQL := SQL + sLineBreak + 'INSERT INTO ' + F_RMIP_ + ' VALUES(0, "' +
        sListaItensRMIP[sOrdemItensRMIP[I]] + '", "' +
        sListaItensRMIP[sOrdemItensRMIP[I]] + '", ' +
        Geral.FF0(I) + ', ' + Geral.FF0(sOrdemItensRMIP[I]) + ', ' + Geral.FF0(I) +', 1);';
      end;
    end;
    else SQL := '*** SQL n�o implementada! ***';
  end;
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLista, DModG.MyPID_DB, [
  'SELECT mip.*, ',
  'IF(mip.Titulo <> "", mip.Titulo, mip.Nome) NO_REL ',
  'FROM ' + F_RMIP_ + ' mip',
  'ORDER BY mip.Ordem, mip.Controle ',
  '']);
  //
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGLista), False);
end;

procedure TFmRMIP_Cab.BtConfiguraClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRMIPCfgCad, FmRMIPCfgCad, afmoNegarComAviso) then
  begin
    FmRMIPCfgCad.ShowModal;
    FmRMIPCfgCad.Destroy;
  end;
end;

procedure TFmRMIP_Cab.BtEnviarClick(Sender: TObject);
begin
(*
  if CO_DMKID_APP = 4 then //Apenas para o Syndi2
    ImprimeRMIP(True)
  else
    Geral.MB_Aviso('Esta op��o n�o est� dispon�vel para este aplicativo');
*)
end;

procedure TFmRMIP_Cab.BtNenhumClick(Sender: TObject);
begin
  //case RGFormato.ItemIndex of
    //-1,
    //0:
    MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGLista), False);
    //1: CGResumido.Value := 0;
  //end;
end;

procedure TFmRMIP_Cab.BtOKClick(Sender: TObject);
begin
  ImprimeRMIP(False);
end;

procedure TFmRMIP_Cab.BtOrdemClick(Sender: TObject);
  procedure AtualizaOrdemDoItem(Ordem, Controle: Integer);
  begin
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_RMIP_, False, [
    'Ordem'], ['Controle'], [Ordem], [Controle], False);
  end;
var
  Ordem: Double;
  I, N, K: Integer;
begin
  if dmkPF.ObtemValorDouble(Ordem, 0) then
  begin
    N := Trunc(Ordem);
    if (N < 1) or (N > QrLista.RecordCount) then
    begin
      Geral.MB_Aviso('A ordem ' + IntToStr(N) + ' n�o existe!');
      Exit;
    end;
    I := 0;
    K := QrListaControle.Value;
    //
    AtualizaOrdemDoItem(N, K);
    //
    QrLista.First;
    while not QrLista.Eof do
    begin
      if K <> QrListaControle.Value then
      begin
        I := I + 1;
        if (I = N) then
          I := I + 1;
        //
        AtualizaOrdemDoItem(I, QrListaControle.Value);
      end;
      QrLista.Next;
    end;
    //
    UnDmkDAC_PF.AbreQuery(QrLista, DModG.MyPID_DB);
    QrLista.Locate('Controle', K, []);
  end;
end;

function TFmRMIP_Cab.frxComposite(frxAtu: TfrxReport; var ClearLastReport:
Boolean): Boolean;
var
  s: TMemoryStream;
begin
{
frxReport1.LoadFromFile('1.fr3');
frxReport1.PrepareReport;
frxReport1.LoadFromFile('2.fr3');
frxReport1.PrepareReport(False);
frxReport1.ShowPreparedReport;
}
  Result := False;
  try
    if frxAtu <> nil then
    begin
      //frxIMP_ReMIP_001_001.OnGetValue := frxAtu.OnGetValue;
      frxIMP_ReMIP_001_001.OnUserFunction := frxAtu.OnUserFunction;
      frxIMP_ReMIP_001_001.ReportOptions.Name := 'RMIP';
      frxAtu.Variables['MeuLogoExiste'] := VAR_MEULOGOEXISTE;
      frxAtu.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frxAtu.Variables['LogoNFExiste'] := VAR_LOGONFEXISTE;
      frxAtu.Variables['LogoNFCaminho'] := QuotedStr(VAR_LOGONFPATH);
      frxAtu.Variables['LogoTitRPath'] := Dmod.QrOpcoesFiliImgTitRelR.Value;
      frxAtu.Variables['LogoTitLPath'] := Dmod.QrOpcoesFiliImgTitRelL.Value;

      frxAtu.Variables['LogoDuplExiste'] := VAR_LOGODUPLEXISTE;
      frxAtu.Variables['LogoDuplCaminho'] := QuotedStr(VAR_LOGODUPLPATH);
      frxAtu.Variables['LogoBig1Existe'] := VAR_LOGOBIG1EXISTE;
      frxAtu.Variables['LogoBig1Caminho'] := QuotedStr(VAR_LOGOBIG1PATH);
      frxAtu.Variables['SloganFooter'] := QuotedStr(VAR_SLOGAN_FOOTER);
      frxAtu.Variables['VARF_CODI_FRX'] := QuotedStr(Copy(frxAtu.Name, 4));

      if FRelSelecionado[8] = True then
      begin
        MyObjects.Informa2(LaSub2A, LaSub2B, True, 'Localizando imagens de titulo (rel tipo 8');
        frxAtu.Variables['LogoTitRExiste'] := MyObjects.ArquivoExiste(Dmod.QrOpcoesFiliImgTitRelR.Value, Memo1, CkDoLocalize.Checked);
        frxAtu.Variables['LogoTitLExiste'] := MyObjects.ArquivoExiste(Dmod.QrOpcoesFiliImgTitRelL.Value, Memo1, CkDoLocalize.Checked);
        MyObjects.Informa2(LaSub2A, LaSub2B, False, '...');
      end;
      // agregar frx selecionado ao frxBal (frx virtual que est� agrupando v�rios frx para imprimir tudo junto)
      s := TMemoryStream.Create;
      frxAtu.SaveToStream(s);
      s.Position := 0;
      frxIMP_ReMIP_001_001.LoadFromStream(s);
      frxIMP_ReMIP_001_001.PrepareReport(ClearLastReport);
      ClearLastReport := False;
      //
      Result := True;
    end else
      Geral.MB_Erro('ERRO. "frxCond?" n�o definido.');
  except
    raise;
  end;
end;

procedure TFmRMIP_Cab.frxIMP_ReMIP_001_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_DATA' then
    Value := TPData.Date
  else
  if VarName = 'VARF_EMPRESA' then
    Value := CBEmpresa.Text
  else
  if VarName = 'VARF_NO_CLIENTE' then
    Value := ObtemNomeClienteSimples
  else
  if VarName = 'VARF_RelTitu' then
    Value := EdTitulo.Text
  else
  if VarName = 'VARF_PERIODO_TXT' then
    Value := dmkPF.PeriodoImp2(TPIni.Date, TPFim.Date, True, True, '', '', '')
  else

////////////////////////////////////////////////////////////////////////////////

  if VarName = 'VAR_LINE_TEE_003' then
    FmRMIP_R003.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
  else
  if VarName = 'VAR_LINE_TEE_004' then
    FmRMIP_R004.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
  else
  if VarName = 'VAR_LINE_TEE_006' then
    FmRMIP_R006.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
///////////
  else if VAR_REL_GEREN = 07 then
  begin
    if VarName = 'VARF_DESCRI_ATIVI' then
      FmRMIP_R007.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
  end
  else if VAR_REL_GEREN = 8 then
(*
  if (VarName = 'VARF_PAGINAR')
  or (VarName = 'VARF_NumLicOper')
  or (VarName = 'VARF_OS_ORIGEM')
  or (VarName = 'VARF_CONTRATO')
  or (VarName = 'VARF_ENTIDADE')
  or (VarName = 'VARF_LUGAR')
  or (VarName = 'VARF_ENDER_REFER')
  or (VarName = 'VARF_ENDERECO_MONTADO')
  or (VarName = 'SloganFooter')
  or (VarName = 'VARF_AG_S')
  or (VarName = 'VARF_OP_S')
  or (VarName = 'VARF_AREA_TOTAL')
  or (VarName = 'VARF_PRAGASALVO')
  or (VarName = 'VARF_GRUPO_OS')
  or (VarName = 'VARF_DEPENDENCIAS')
  or (VarName = 'LogoCarimboEmpExiste')
  or (VarName = 'LogoCarimboEmpPath')
  or (VarName = 'LogoRespTec1Existe')
  or (VarName = 'LogoRespTec1Path')
  or (VarName = 'VARF_DEPENDENCIAS_POR_TYP') then
*)
    FmOSImp2.frxGER_OSERV_014_000_AGetValue(frxIMP_ReMIP_001_001, VarName, Value)

///////////
  else
  if VarName = 'VARF_REL_GEREN_008' then
    Value := True
///////////
  else if VAR_REL_GEREN = 11 then
  begin
    if VarName = 'ImgNCTExiste' then
      FmRMIP_R011.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
    else
    if VarName = 'ImgNCTPath' then
      FmRMIP_R011.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
    else
    if VarName = 'VARF_TITULO' then
      FmRMIP_R011.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
    else
    if VarName = 'VARF_FC_NCT' then
      FmRMIP_R011.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
    else
    if VarName = 'VARF_DADOS_REL' then
      FmRMIP_R011.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
  end
///////////
  else if VAR_REL_GEREN = 13 then
  begin
    if VarName = 'VARF_DESCRI_ATIVI' then
      FmRMIP_R013.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
  end;
///////////
  if VarName = 'VAR_LINE_TEE_014' then
    FmRMIP_R014.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
  else
  if VarName = 'VAR_LINE_TEE_015' then
    FmRMIP_R015.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
///////////
  else if VAR_REL_GEREN = 16 then
    FmRMIP_R016.frxReport000GetValue(frxIMP_ReMIP_001_001, VarName, Value)
///////////
end;

procedure TFmRMIP_Cab.frxIMP_ReMIP_001_R001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := CBEmpresa.Text
  else
  if VarName = 'VARF_NO_CLIENTE' then
    Value := ObtemNomeClienteSimples
  else
  if VarName = 'VARF_RelTitu' then
    Value := EdTitulo.Text
  else
  if VarName = 'VARF_PERIODO_TXT' then
    Value := dmkPF.PeriodoImp2(TPIni.Date, TPFim.Date, True, True, '', '', '')
  else

end;

procedure TFmRMIP_Cab.frxIMP_ReMIP_001_R002GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_RelTitu' then
    Value := EdTitulo.Text
  else
end;

procedure TFmRMIP_Cab.frxReport1GetValue(const VarName: string;
  var Value: Variant);
(*
var
  frxBmp: TfrxPictureView;
  Bmp: Graphics.TBitmap;
  PipCad: String;
*)
begin
  if VarName = 'VARF_OBTEM_BMP' then
  begin
    WebBrowserScreenShot(WebBrowser1, 'c:\WebBrowserImage.jpg') ;
    //
    Value := True;
  end;
end;

procedure TFmRMIP_Cab.ImprimeRMIP(Exporta: Boolean);
var
  I, Q: Integer;
  CLR: Boolean;
  FechaForm: array[0..MaxListaItensRMIP] of Boolean;
  Continua: Boolean;
  RelSel, QtdDtas: Integer;
  Janela: HWND;
  _RMIP_011: String;
  //
  function SemClienteDefinido(): Boolean;
  begin
    Result := FCliente = 0;
    if Result then
      Geral.MB_Aviso('O relat�rio "' + QrListaNO_REL.Value +
      '" exige defini��o de cliente!');
  end;
  function RelatorioMuitoGrande(): Boolean;
  begin
    Result := FCliente = 0;
    if Result then
      Geral.MB_Aviso('O relat�rio "' + QrListaNO_REL.Value +
      '" exige defini��o de cliente pois � muito demorado para todos clientes!!');
  end;
begin
  Memo1.Lines.Clear;
  //frxIMP_ReMIP_001_001.PreviewPages.Clear;
  QtdDtas := -1;
  // Monitorar qual e o relatorio sendo montado no composite report
  VAR_REL_GEREN := -1;
  //
  if dmkPF.NaoQueroLocalizarAsImagens(CkDoLocalize.Checked) then
    Exit;
  //
  for I := 0 to MaxListaItensRMIP do
  begin
    FechaForm[I] := False;
    FRelSelecionado[I] := False;
  end;
  CLR := True;
  FEmpresa := EdEmpresa.ValueVariant;
  if MyObjects.FIC(FEmpresa = 0, EdEmpresa, 'Informe a Empresa!') then
    Exit;
  if MyObjects.FIC(DBGLista.SelectedRows.Count = 0, nil,
  'Nenhum relat�rio foi selecionado!') then
    Exit;
  FCliente := EdCliente.ValueVariant;
  // Desmaracr aqui quando melhorar relatorio por empresa (todos clientes)
  if MyObjects.FIC(FCliente = 0, EdCliente, 'Informe o Cliente!') then
    Exit;
  //
  FDtaIni := Trunc(TPIni.Date);
  FDtaFim := Trunc(TPFim.Date);
  FDtaImp := Trunc(TPData.Date);
  //
  with DBGLista.DataSource.DataSet do
  begin
    for Q := 0 to DBGLista.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBGLista.SelectedRows.Items[Q]));
      RelSel := QrListaItemRel.Value;
      case TItensRMIP(RelSel) of
        //rmipResumoOSs: // 7
        rmipComprovantesExecucao: // 8
        if RelatorioMuitoGrande() then
          Exit;
        rmipListagemSituacoesPMVs: // 9
        if SemClienteDefinido() then
          Exit;
        rmipNCTs: // 11
        begin
          if SemClienteDefinido() then
            Exit;
          Application.CreateForm(TFmRMIP_R011_Pre, FmRMIP_R011_Pre);
          FmRMIP_R011_Pre.FEmpresa    := FEmpresa;
          FmRMIP_R011_Pre.FCliente    := FCliente;
          FmRMIP_R011_Pre.FDtaIni     := FDtaIni;
          FmRMIP_R011_Pre.FDtaFim     := FDtaFim;
          FmRMIP_R011_Pre.FDtaImp     := FDtaImp;
          FmRMIP_R011_Pre.FNO_Empresa := CBEmpresa.Text;
          FmRMIP_R011_Pre.FNO_CLiente := ObtemNomeClienteCompleto;
          FmRMIP_R011_Pre.FItemRel    := QrListaItemRel.Value;
          FmRMIP_R011_Pre.FNO_REL     := QrListaNO_REL.Value;
          FmRMIP_R011_Pre.FMemo1      := Memo1;
          //
          FmRMIP_R011_Pre.ReopenCunsImgCab();
          FmRMIP_R011_Pre.ShowModal;
          _RMIP_011 := FmRMIP_R011_Pre.F_Cuns_Img_Cab;
          FmRMIP_R011_Pre.Destroy;
        end;
        rmipSPs: // 12
        if SemClienteDefinido() then
          Exit;
      end;
    end;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    with DBGLista.DataSource.DataSet do
    begin
      for Q := 0 to DBGLista.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGLista.SelectedRows.Items[Q]));
        RelSel := QrListaItemRel.Value;
        VAR_REL_GEREN := RelSel;
        case TItensRMIP(RelSel) of
          rmipNenhum: ; //  0: Nada
          rmipCapa: //=1,
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            MyObjects.frxDefineDataSets(frxIMP_ReMIP_001_R001, []);
            //MyObjects.frxMostra(frxIMP_ReMIP_001_R001, 'Capa');
            frxComposite(frxIMP_ReMIP_001_R001, CLR); // Capa
          end;
          rmipParecer: //=2,
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrEntiRespon, Dmod.MyDB, [
            'SELECT ere.*, eca.Nome NOME_CARGO, ',
            'ELT(ere.Assina+1, "N�o", "Sim", "???") NO_ASSINA, ',
            'IF(ent.Tipo=0, ent.ETe1, PTe1) Te1, ',
            'IF(ent.Tipo=0, ent.ECel, PCel) Cel, ',
            'IF(ent.Tipo=0, ent.EEmail, PEmail) Email ',
            'FROM entirespon ere ',
            'LEFT JOIN enticargos eca ON eca.Codigo=ere.Cargo ',
            'LEFT JOIN entidades ent ON ent.Codigo = ere.Entidade ',
            'WHERE ere.Controle=' +  Geral.FF0(EdEntiRespon.ValueVariant),
            '']);
            //
            ReopentTxtGeneric();
            //
            MyObjects.frxDefineDataSets(frxIMP_ReMIP_001_R002, [
            frxDsEntiRespon,
            frxDsTxtGeneric]);
            //MyObjects.frxPrepara(frxIMP_ReMIP_001_R002, 'Parecer');
            frxComposite(frxIMP_ReMIP_001_R002, CLR); //
          end;
          rmipPizzaMonitsResp: // 3
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R003, FmRMIP_R003);
            FmRMIP_R003.FEmpresa    := FEmpresa;
            FmRMIP_R003.FCliente    := FCliente;
            FmRMIP_R003.FDtaIni     := FDtaIni;
            FmRMIP_R003.FDtaFim     := FDtaFim;
            FmRMIP_R003.FDtaImp     := FDtaImp;
            FmRMIP_R003.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R003.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R003.FItemRel    := QrListaItemRel.Value;
            FmRMIP_R003.FNO_REL     := QrListaNO_REL.Value;
            //
            if FCliente <> 0 then
              //FmRMIP_R003.GeraImp_PizzaUltimaRevisaoPMV()
              MultiImp_RMIP_R003_GeraImp_PizzaRevisaoPMV(CLR)
            else
            begin
              FmRMIP_R003.GeraImp_PizzaRevisaoPMVPeriodo();
              //
              //FmRMIP_R003.ShowModal;
              frxComposite(FmRMIP_R003.frxReport003A, CLR); //
              //FmRMIP_R003.Destroy;
            end;
            FechaForm[QrListaItemRel.Value] := True;
          end;
          rmipLinhaEvoluHistoriResp: // 4
          begin
            if DatasInsuficientes(QtdDtas) > 1 then
            begin
              FRelSelecionado[RelSel] := True;
              InfoGerRel('');
              Application.CreateForm(TFmRMIP_R004, FmRMIP_R004);
              FmRMIP_R004.FCliente    := FCliente;
              FmRMIP_R004.FDtaIni     := FDtaIni;
              FmRMIP_R004.FDtaFim     := FDtaFim;
              FmRMIP_R004.FDtaImp     := FDtaImp;
              FmRMIP_R004.FNO_Empresa := CBEmpresa.Text;
              FmRMIP_R004.FNO_CLiente := ObtemNomeClienteCompleto;
              FmRMIP_R004.FNO_REL     := QrListaNO_REL.Value;
              FmRMIP_R004.FAplicacao  := CO_GeraGrafRelatPMV_0002_COD_LinhaEvoluHistorResp;
              FmRMIP_R004.FAplic_Tit  := CO_GeraGrafRelatPMV_0002_TXT_LinhaEvoluHistorResp;
              //FmRMIP_R004.FMemo       := Memo1;
              //
              FmRMIP_R004.GeraImp_LinhaEvolucaoHistorico();
              //FmRMIP_R004.ShowModal;
              frxComposite(FmRMIP_R004.frxReport004A, CLR); //
              //FmRMIP_R004.Destroy;
              FechaForm[QrListaItemRel.Value] := True;
            end else
              Memo1.Lines.Add(CO_GeraGrafRelatPMV_0002_TXT_LinhaEvoluHistorResp
                + ' cancelado. Motivo: Apenas um item por s�rie!');
          end;
          rmipPizzaMonitsDep: // 5
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R005, FmRMIP_R005);
            FmRMIP_R005.FEmpresa    := FEmpresa;
            FmRMIP_R005.FCliente    := FCliente;
            FmRMIP_R005.FDtaIni     := FDtaIni;
            FmRMIP_R005.FDtaFim     := FDtaFim;
            FmRMIP_R005.FDtaImp     := FDtaImp;
            FmRMIP_R005.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R005.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R005.FItemRel    := QrListaItemRel.Value;
            FmRMIP_R005.FNO_REL     := QrListaNO_REL.Value;
            //
            if FCliente <> 0 then
              //FmRMIP_R005.GeraImp_PizzaUltimaRevisaoPMV()
              MultiImp_RMIP_R005_GeraImp_PizzaRevisaoDep(CLR)
            else
            begin
              FmRMIP_R005.GeraImp_PizzaRevisaoPMVPeriodo();
              //FmRMIP_R005.ShowModal;
              frxComposite(FmRMIP_R005.frxReport005A, CLR); //
              //FmRMIP_R005.Destroy;
            end;
            FechaForm[QrListaItemRel.Value] := True;
          end;
          rmipLinhaEvoluHistoriDep: // 6
          begin
            if DatasInsuficientes(QtdDtas) > 1 then
            begin
              FRelSelecionado[RelSel] := True;
              InfoGerRel('');
              Application.CreateForm(TFmRMIP_R006, FmRMIP_R006);
              FmRMIP_R006.FCliente    := FCliente;
              FmRMIP_R006.FDtaIni     := FDtaIni;
              FmRMIP_R006.FDtaFim     := FDtaFim;
              FmRMIP_R006.FDtaImp     := FDtaImp;
              FmRMIP_R006.FNO_Empresa := CBEmpresa.Text;
              FmRMIP_R006.FNO_CLiente := ObtemNomeClienteCompleto;
              FmRMIP_R006.FNO_REL     := QrListaNO_REL.Value;
              FmRMIP_R006.FItemRMIP   := TItensRMIP(RelSel);
              FmRMIP_R006.FAplicacao := CO_GeraGrafRelatPMV_0008_COD_LinhaEvoluHistorDep;
              FmRMIP_R006.FAplic_Tit := CO_GeraGrafRelatPMV_0008_TXT_LinhaEvoluHistorDep;
              //
              FmRMIP_R006.GeraImp_LinhaEvolucaoHistorico();
              //FmRMIP_R006.ShowModal;
              frxComposite(FmRMIP_R006.frxReport006A, CLR); //
              //FmRMIP_R006.Destroy;
              FechaForm[QrListaItemRel.Value] := True;
            end else
              Memo1.Lines.Add(CO_GeraGrafRelatPMV_0016_TXT_BarraEvoluHistorResp
                + ' cancelado. Motivo: Apenas um item por s�rie!');
          end;
          rmipResumoOSs: // 7
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R007, FmRMIP_R007);
            FmRMIP_R007.FEmpresa    := FEmpresa;
            FmRMIP_R007.FCliente    := FCliente;
            FmRMIP_R007.FDtaIni     := FDtaIni;
            FmRMIP_R007.FDtaFim     := FDtaFim;
            FmRMIP_R007.FDtaImp     := FDtaImp;
            FmRMIP_R007.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R007.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R007.FNO_REL     := QrListaNO_REL.Value;
            //
            FmRMIP_R007.GeraImp_ResumoOSs();
            //FmRMIP_R007.ShowModal;
            frxComposite(FmRMIP_R007.frxReport007A, CLR);
            //FmRMIP_R007.Destroy;
            FechaForm[QrListaItemRel.Value] := True;
          end;
          rmipComprovantesExecucao: // 8
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R008, FmRMIP_R008);
            FmRMIP_R008.FEmpresa    := FEmpresa;
            FmRMIP_R008.FCliente    := FCliente;
            FmRMIP_R008.FDtaIni     := FDtaIni;
            FmRMIP_R008.FDtaFim     := FDtaFim;
            FmRMIP_R008.FDtaImp     := FDtaImp;
            FmRMIP_R008.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R008.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R008.FNO_REL     := QrListaNO_REL.Value;
            FmRMIP_R008.FDoLocalize := CkDoLocalize.Checked;
            FmRMIP_R008.FCLR        := CLR;
            FmRMIP_R008.FPB1        := PBSub1;
            FmRMIP_R008.FLaAviso1   := LaSub1A;
            FmRMIP_R008.FLaAviso2   := LaSub1B;
            //
            Continua := FmRMIP_R008.GeraImp_ComprovantesDeExecucao();
            //FmRMIP_R008.ShowModal;
            //frxComposite(FmRMIP_R008.frxReport008A, CLR);
            //FmRMIP_R008.Destroy;
            FechaForm[QrListaItemRel.Value] := True;
            //
            MyObjects.Informa2(LaSub1A, LaSub1B, False, '...');
            if not Continua then
              Exit;
          end;
          rmipListagemSituacoesPMVs: // 9
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R009, FmRMIP_R009);
            FmRMIP_R009.FEmpresa    := FEmpresa;
            FmRMIP_R009.FCliente    := FCliente;
            FmRMIP_R009.FDtaIni     := FDtaIni;
            FmRMIP_R009.FDtaFim     := FDtaFim;
            FmRMIP_R009.FDtaImp     := FDtaImp;
            FmRMIP_R009.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R009.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R009.FItemRel    := QrListaItemRel.Value;
            FmRMIP_R009.FNO_REL     := QrListaNO_REL.Value;
            //
            if FCliente <> 0 then
              FmRMIP_R009.GeraImp_ListaUltimaRevisaoPMV()
            else
              //FmRMIP_R009.GeraImp_ListaRevisaoPMVPeriodo();
            Geral.MB_Aviso('Relat�rio n�o existe para : TItensRMIP.Item = ' +
            Geral.FF0(QrListaItemRel.Value) + ' e cliente n�o definido!');
            //
            //FmRMIP_R009.ShowModal;
            frxComposite(FmRMIP_R009.frxReport009A, CLR); //
            //FmRMIP_R009.Destroy;
            FechaForm[QrListaItemRel.Value] := True;
          end;
          rmipProdutosUtilizados: // 10
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R010, FmRMIP_R010);
            FmRMIP_R010.FEmpresa    := FEmpresa;
            FmRMIP_R010.FCliente    := FCliente;
            FmRMIP_R010.FDtaIni     := FDtaIni;
            FmRMIP_R010.FDtaFim     := FDtaFim;
            FmRMIP_R010.FDtaImp     := FDtaImp;
            FmRMIP_R010.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R010.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R010.FItemRel    := QrListaItemRel.Value;
            FmRMIP_R010.FNO_REL     := QrListaNO_REL.Value;
            //
            FmRMIP_R010.GeraImp_TotaisProdutosUsados();
            //
            //FmRMIP_R010.ShowModal;
            frxComposite(FmRMIP_R010.frxReport010A, CLR); //
            //FmRMIP_R010.Destroy;
            FechaForm[QrListaItemRel.Value] := True;
          end;
          rmipNCTs: // 11
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R011, FmRMIP_R011);
            FmRMIP_R011.FEmpresa    := FEmpresa;
            FmRMIP_R011.FCliente    := FCliente;
            FmRMIP_R011.FDtaIni     := FDtaIni;
            FmRMIP_R011.FDtaFim     := FDtaFim;
            FmRMIP_R011.FDtaImp     := FDtaImp;
            FmRMIP_R011.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R011.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R011.FItemRel    := QrListaItemRel.Value;
            FmRMIP_R011.FNO_REL     := QrListaNO_REL.Value;
            FmRMIP_R011.FMemo1      := Memo1;
            //
            FmRMIP_R011.F_Cuns_Img_Cab := _RMIP_011;
            //
            FmRMIP_R011.GeraImp_NCTs();
            //FmRMIP_R011.ShowModal;
            frxComposite(FmRMIP_R011.frxReport011A, CLR); //
            //FmRMIP_R011.Destroy;
            FechaForm[QrListaItemRel.Value] := True;
          end;
          rmipSPs: // 12
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R012, FmRMIP_R012);
            FmRMIP_R012.FEmpresa    := FEmpresa;
            FmRMIP_R012.FCliente    := FCliente;
            FmRMIP_R012.FDtaIni     := FDtaIni;
            FmRMIP_R012.FDtaFim     := FDtaFim;
            FmRMIP_R012.FDtaImp     := FDtaImp;
            FmRMIP_R012.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R012.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R012.FItemRel    := QrListaItemRel.Value;
            FmRMIP_R012.FNO_REL     := QrListaNO_REL.Value;
            //FmRMIP_R012.FMemo1      := Memo1;
            //
            FmRMIP_R012.GeraImp_SPs();
            //FmRMIP_R012.ShowModal;
            frxComposite(FmRMIP_R012.frxReport012A, CLR); //
            //FmRMIP_R012.Destroy;
            FechaForm[QrListaItemRel.Value] := True;
            VAR_REL_GEREN := -1;
          end;
          rmipAgendaOSsPrev: // 13
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R013, FmRMIP_R013);
            FmRMIP_R013.FEmpresa    := FEmpresa;
            FmRMIP_R013.FCliente    := FCliente;
            FmRMIP_R013.FDtaIni     := FDtaIni;
            FmRMIP_R013.FDtaFim     := FDtaFim;
            FmRMIP_R013.FDtaImp     := FDtaImp;
            FmRMIP_R013.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R013.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R013.FItemRel    := QrListaItemRel.Value;
            FmRMIP_R013.FNO_REL     := QrListaNO_REL.Value;
            //FmRMIP_R013.FMemo1      := Memo1;
            //
            FmRMIP_R013.GeraImp_Agenda();
            //FmRMIP_R013.ShowModal;
            frxComposite(FmRMIP_R013.frxReport013A, CLR); //
            //FmRMIP_R013.Destroy;
            FechaForm[QrListaItemRel.Value] := True;
            VAR_REL_GEREN := -1;
          end;
          rmipBarraEvoluHistoriResp: // 14
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R014, FmRMIP_R014);
            FmRMIP_R014.FCliente    := FCliente;
            FmRMIP_R014.FDtaIni     := FDtaIni;
            FmRMIP_R014.FDtaFim     := FDtaFim;
            FmRMIP_R014.FDtaImp     := FDtaImp;
            FmRMIP_R014.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R014.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R014.FNO_REL     := QrListaNO_REL.Value;
            FmRMIP_R014.FAplicacao  := CO_GeraGrafRelatPMV_0016_COD_BarraEvoluHistorResp;
            FmRMIP_R014.FAplic_Tit  := CO_GeraGrafRelatPMV_0016_TXT_BarraEvoluHistorResp;
            //FmRMIP_R014.FMemo       := Memo1;

            //
            FmRMIP_R014.GeraImp_LinhaEvolucaoHistorico();
            //FmRMIP_R014.ShowModal;
            frxComposite(FmRMIP_R014.frxReport014A, CLR); //
            //FmRMIP_R014.Destroy;
            FechaForm[QrListaItemRel.Value] := True;
          end;
          rmipBarraEvoluHistoriDep: // 15
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R015, FmRMIP_R015);
            FmRMIP_R015.FCliente    := FCliente;
            FmRMIP_R015.FDtaIni     := FDtaIni;
            FmRMIP_R015.FDtaFim     := FDtaFim;
            FmRMIP_R015.FDtaImp     := FDtaImp;
            FmRMIP_R015.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R015.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R015.FNO_REL     := QrListaNO_REL.Value;
            FmRMIP_R015.FItemRMIP   := TItensRMIP(RelSel);
            FmRMIP_R015.FAplicacao := CO_GeraGrafRelatPMV_0032_COD_BarraEvoluHistorDep;
            FmRMIP_R015.FAplic_Tit := CO_GeraGrafRelatPMV_0032_TXT_BarraEvoluHistorDep;
            //
            FmRMIP_R015.GeraImp_LinhaEvolucaoHistorico();
            //FmRMIP_R015.ShowModal;
            frxComposite(FmRMIP_R015.frxReport015A, CLR); //
            //FmRMIP_R015.Destroy;
            FechaForm[QrListaItemRel.Value] := True;
          end;
          rmipCroquiPMVSColoridos: // 16
          begin
            FRelSelecionado[RelSel] := True;
            InfoGerRel('');
            Application.CreateForm(TFmRMIP_R016, FmRMIP_R016);
            FmRMIP_R016.FSiapImaSvg := 0;
            FmRMIP_R016.FEmpresa    := FEmpresa;
            FmRMIP_R016.FCliente    := FCliente;
            FmRMIP_R016.FDtaIni     := FDtaIni;
            FmRMIP_R016.FDtaFim     := FDtaFim;
            FmRMIP_R016.FDtaImp     := FDtaImp;
            FmRMIP_R016.FNO_Empresa := CBEmpresa.Text;
            FmRMIP_R016.FNO_CLiente := ObtemNomeClienteCompleto;
            FmRMIP_R016.FNO_REL     := QrListaNO_REL.Value;
            FmRMIP_R016.FDoLocalize := CkDoLocalize.Checked;
            FmRMIP_R016.FCLR        := CLR;
            FmRMIP_R016.FPB1        := PBSub1;
            FmRMIP_R016.FLaAviso1   := LaSub1A;
            FmRMIP_R016.FLaAviso2   := LaSub1B;
            //
            Continua := FmRMIP_R016.GeraImp_CrroquiPMVsColoridos();
            //FmRMIP_R016.ShowModal;
            frxComposite(FmRMIP_R016.frxReport016A, CLR);
            //FmRMIP_R016.Destroy;
            FechaForm[QrListaItemRel.Value] := True;
            //
            MyObjects.Informa2(LaSub1A, LaSub1B, False, '...');
            if not Continua then
              Exit;
          end;
          else Geral.MB_Erro('Relat�rio n�o implementado: TItensRMIP.Item = ' +
          Geral.FF0(QrListaItemRel.Value));


(*
  UnDmkDAC_PF.AbreQuery(QrSels);
  if RGFormato.ItemIndex = 0 then
  begin
    if MyObjects.FIC(QrSelsItens.Value = 0, nil,
    'Selecione pelo menos um relat�rio!') then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmp, Dmod.MyDB, [
  'SELECT en.Codigo, uf.Nome NO_UF, en.CliInt,',
  'FLOOR(IF(en.Codigo<-10,en.Filial,en.CliInt)) CO_SHOW,',
  'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_EMPRESA,',
  'IF(en.Tipo=0,en.ECidade,en.PCidade) CIDADE',
  'FROM entidades en',
  'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)',
  'WHERE en.Codigo=' + FormatFloat('0', DModG.QrEmpresasCodigo.Value),
  '']);
  //
  FEntidade := DModG.QrEmpresasCodigo.Value;
  FCliInt   := EdEmpresa.ValueVariant;
  FEnt_Txt  := FormatFloat('0', FEntidade);
  FTabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, FCliInt);
  FTabLctB  := DModG.NomeTab(TMeuDB, ntLct, False, ttB, FCliInt);
  FTabLctD  := DModG.NomeTab(TMeuDB, ntLct, False, ttD, FCliInt);
  //FTabLctX  := Copy(FTabLctA, 1, Length(FTabLctA)-1);
  FTabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, FCliInt);
  FTabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, FCliInt);
  FTabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, FCliInt);
  FTabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, FCliInt);
  //
  MyObjects.Informa(LaAviso, True, 'Verificando lan�amentos sem Cliente Interno');
  if DmodFin.ExisteLancamentoSemCliInt(FTabLctA) then
  begin
    MyObjects.Informa(LaAviso, False, '...');
    Screen.Cursor := crDefault;
    Exit;
  end;
  MyObjects.Informa(LaAviso, True, 'Preparando consultas');
  //
  FPeriodoAtu := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
  FDataI := Geral.FDT(???.PrimeiroDiaDoPeriodo_Date(FPeriodoAtu), 1);
  FDataF := Geral.FDT(???.UltimoDiaDoPeriodo_Date(FPeriodoAtu), 1);
  // Abre tabela com dados espec�ficos do cliente interno selecionado!
  if DmodFin.ReopenCliInt(FEntidade) then
    PBB := DmodFin.QrCliIntPBB.Value
  else PBB := 1;
  //
  Ano := Geral.IMV(CBAno.Text);
  Mes := CBMes.ItemIndex + 1;
  Mez := ((Ano - 2000) * 100) + Mes;
  FDataAnt := Geral.FDT(EncodeDate(Ano-1,1,1), 1);
  FDataIni := Geral.FDT(EncodeDate(Ano  ,1,1), 1);
  FDataBal := Geral.FDT(EncodeDate(Ano, Mes, 1), 1);
  FPeriodoIni := (Ano - 2000) * 100 + 1;
  FPeriodoFim := (Ano - 2000) * 100 + 12;
  FPeriodoAnt := FPeriodoIni - 100;
  if Mes = 12 then
  begin
    Ano := Ano + 1;
    Mes := 1;
  end else Mes := Mes + 1;
  FDataAtu := Geral.FDT(EncodeDate(Ano, Mes, 1)-1, 1);
  //
  case RGFormato.ItemIndex of
    0: ImprimeCompleto(Mez, PBB, Exporta);
    1: ImprimeReduzido(Mez, Exporta);
    else Geral.MB_Erro('Formato de relat�rio n�o implementado!');
  end;
*)
  //
        end;
        MyObjects.Informa2(LaSub0A, LaSub0B, False, '...');
        QrLista.Next;
      end;
    end;
    if not Exporta then //Imprime
    begin
      MyObjects.Informa2(LaSub0A, LaSub0B, True, 'Preparando RMIP');
      //
      Application.CreateForm(TFmMeufrx, FmMeufrx);
      frxIMP_ReMIP_001_001.Preview := FmMeufrx.PvVer;
      //
      frxIMP_ReMIP_001_001.Variables['MeuLogoExiste'] := VAR_MEULOGOEXISTE;
      frxIMP_ReMIP_001_001.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frxIMP_ReMIP_001_001.Variables['LogoNFExiste'] := VAR_LOGONFEXISTE;
      frxIMP_ReMIP_001_001.Variables['LogoNFCaminho'] := QuotedStr(VAR_LOGONFPATH);
      frxIMP_ReMIP_001_001.Variables['LogoDuplExiste'] := VAR_LOGODUPLEXISTE;
      frxIMP_ReMIP_001_001.Variables['LogoDuplCaminho'] := QuotedStr(VAR_LOGODUPLPATH);
      frxIMP_ReMIP_001_001.Variables['LogoBig1Existe'] := VAR_LOGOBIG1EXISTE;
      frxIMP_ReMIP_001_001.Variables['LogoBig1Caminho'] := QuotedStr(VAR_LOGOBIG1PATH);
      frxIMP_ReMIP_001_001.Variables['SloganFooter'] := QuotedStr(VAR_SLOGAN_FOOTER);
      frxIMP_ReMIP_001_001.Variables['VARF_CODI_FRX'] := QuotedStr(Copy(frxIMP_ReMIP_001_001.Name, 4));
      //
      FmMeufrx.PvVer.OutlineWidth := frxIMP_ReMIP_001_001.PreviewOptions.OutlineWidth;
      FmMeufrx.PvVer.Zoom := frxIMP_ReMIP_001_001.PreviewOptions.Zoom;
      FmMeufrx.PvVer.ZoomMode := frxIMP_ReMIP_001_001.PreviewOptions.ZoomMode;
      FmMeufrx.UpdateZoom;
    end else
    begin  //Exporta
      MyObjects.Informa2(LaSub0A, LaSub0B, True, 'Exportando RMIP');
    end;
    FFim := Now();
    FTempo := FFim - FIni;
    EdTempo.ValueVariant := FTempo;
    //
    if not Exporta then //Imprime
    begin
      FmMeufrx.ShowModal;
      FmMeufrx.Destroy;
    end else
    begin //Exporta
      //ExportaRMIP(frxFIN_BALAN_001_001, False);
    end;
    MyObjects.Informa2(LaSub0A, LaSub0B, False, '...');
  finally
    Screen.Cursor := crDefault;
    for I := 0 to MaxListaItensRMIP do
    begin
      Janela := 0;
      if FechaForm[I] then
      begin
        case TItensRMIP(I) of
          //rmipNenhum=0,
          //rmipCapa: =1,
          //rmipParecer=2,
          rmipPizzaMonitsResp       : FmRMIP_R003.Destroy;
          rmipLinhaEvoluHistoriResp : FmRMIP_R004.Destroy;
          rmipPizzaMonitsDep        : FmRMIP_R005.Destroy;
          rmipLinhaEvoluHistoriDep  : FmRMIP_R006.Destroy;
          rmipResumoOSs             : FmRMIP_R007.Destroy;
          rmipComprovantesExecucao  : FmRMIP_R008.Destroy;
          rmipListagemSituacoesPMVs : FmRMIP_R009.Destroy;
          rmipProdutosUtilizados    : FmRMIP_R010.Destroy;
          rmipNCTs                  : FmRMIP_R011.Destroy;
          rmipSPs                   : FmRMIP_R012.Destroy;
          rmipAgendaOSsPrev         : FmRMIP_R013.Destroy;
          rmipBarraEvoluHistoriResp : FmRMIP_R014.Destroy;
          rmipBarraEvoluHistoriDep  : FmRMIP_R015.Destroy;
          rmipCroquiPMVSColoridos   : FmRMIP_R016.Destroy;
          //...
          else Geral.MB_Erro('Form sem chamada de fechamanto: ' + Geral.FF0(I));
        end;
      end;
      if Janela <> 0 then
        SendMessage(Janela, WM_SYSCOMMAND, SC_CLOSE, 0);
    end;
  end;
  VAR_REL_GEREN := -1;
end;

procedure TFmRMIP_Cab.InfoGerRel(Extra: String);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando relat�rio tipo ' +
  Geral.FF0(QrListaItemRel.Value) + ': ' + QrListaNO_REL.Value + ' ' + Extra);
end;

procedure TFmRMIP_Cab.MultiImp_RMIP_R003_GeraImp_PizzaRevisaoPMV(CLR: Boolean);
var
  Qry: TmySQLQuery;
  Aviso, SQL_Datas, Todos: String;
  OSCab: Integer;
  procedure GeraItemSelecionado();
  begin
    //OSCab := Qry.FieldByName('Codigo').AsInteger;
    FmRMIP_R003.FOSCab := Qry.FieldByName('Codigo').AsInteger;
    FmRMIP_R003.GeraImp_PizzaUltimaRevisaoPMV();
    //FmRMIP_R003.ShowModal;
    frxComposite(FmRMIP_R003.frxReport003A, CLR); //
    //FmRMIP_R003.Destroy;
  end;
begin
  SQL_Datas := dmkPF.SQL_Periodo('AND cab.DtaExeFim ', FDtaIni, FDtaFim, True, True);
  Aviso := ' ';
  // Ultimo monitormento finalizado do cliente selecionado!
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT cab.Codigo, cab.DtaExeFim ',
    'FROM oscab cab ',
    'LEFT JOIN ' + TMeuDB + '.ospipits opi ON opi.Codigo=cab.Codigo ',
    'WHERE cab.Empresa=' + Geral.FF0(FEmpresa),
    'AND cab.Entidade=' + Geral.FF0(FCliente),
    'AND cab.Operacao & ' + Geral.FF0(CO_OS_OPERACAO_SERVICO_MONITORAMENTO),
    'AND opi.Codigo IS NOT NULL ',
    'AND cab.DtaExeFim > "1900-01-01" ',
    SQL_Datas,
    'ORDER BY cab.DtaExeFim ',
    '']);
    if Qry.RecordCount = 0 then
      Geral.MB_Aviso('Cliente sem monitoramento respondido e finalizado!')
    else
    begin
      if CkPizzaUltima.Checked then
      begin
        Qry.Last;
        GeraItemSelecionado();
      end else
      begin
        Todos := ' de ' + Geral.FF0(Qry.RecordCount);
        Qry.First;
        while not Qry.Eof do
        begin
          InfoGerRel('Item ' + Geral.FF0(Qry.RecNo) + Todos);
          GeraItemSelecionado();
          Qry.Next;
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmRMIP_Cab.MultiImp_RMIP_R005_GeraImp_PizzaRevisaoDep(CLR: Boolean);
var
  Qry: TmySQLQuery;
  Aviso, SQL_Datas, Todos: String;
  OSCab: Integer;
  //
  procedure GeraItemSelecionado();
  begin
    //OSCab := Qry.FieldByName('Codigo').AsInteger;
    FmRMIP_R005.FOSCab := Qry.FieldByName('Codigo').AsInteger;
    FmRMIP_R005.GeraImp_PizzaUltimaRevisaoPMV();
    //FmRMIP_R005.ShowModal;
    frxComposite(FmRMIP_R005.frxReport005A, CLR); //
    //FmRMIP_R005.Destroy;
  end;
begin
  SQL_Datas := dmkPF.SQL_Periodo('AND cab.DtaExeFim ', FDtaIni, FDtaFim, True, True);
  Aviso := ' ';
  // Ultimo monitormento finalizado do cliente selecionado!
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT cab.Codigo, cab.DtaExeFim ',
    'FROM oscab cab ',
    'LEFT JOIN ' + TMeuDB + '.ospipits opi ON opi.Codigo=cab.Codigo ',
    'WHERE cab.Empresa=' + Geral.FF0(FEmpresa),
    'AND cab.Entidade=' + Geral.FF0(FCliente),
    'AND cab.Operacao & ' + Geral.FF0(CO_OS_OPERACAO_SERVICO_MONITORAMENTO),
    'AND opi.Codigo IS NOT NULL ',
    'AND cab.DtaExeFim > "1900-01-01" ',
    SQL_Datas,
    'ORDER BY cab.DtaExeFim ',
    '']);
    if Qry.RecordCount = 0 then
      Geral.MB_Aviso('Cliente sem monitoramento respondido e finalizado!')
    else
    begin
      if CkPizzaUltima.Checked then
      begin
        Qry.Last;
        GeraItemSelecionado();
      end else
      begin
        Todos := ' de ' + Geral.FF0(Qry.RecordCount);
        Qry.First;
        while not Qry.Eof do
        begin
          InfoGerRel('Item ' + Geral.FF0(Qry.RecNo) + Todos);
          GeraitemSelecionado();
          Qry.Next;
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TFmRMIP_Cab.ObtemNomeClienteCompleto(): String;
begin
  Result := dmkPF.ParValueCodTxt('', CBCliente.Text, EdCliente.ValueVariant,
    'TODOS CLIENTES')
end;

function TFmRMIP_Cab.ObtemNomeClienteSimples(): String;
begin
  if EdCliente.ValueVariant <> 0 then
    Result := QrClientesNO_CLIENTE.Value
  else
  Result := 'TODOS CLIENTES';
end;

(*
procedure TFmRMIP_Cab.ExportaRMIP(Frx: TfrxReport; PrepRep: Boolean);
var
  ArqExport, ArqExportNome: String;
begin
  ForceDirectories(FCaminhoEMail);
  //
  ArqExport     := FCaminhoEMail + 'RMIP_' + CBMes.Text + '_' + CBAno.Text + '.pdf';
  ArqExportNome := 'RMIP ' + CBMes.Text + ' ' + CBAno.Text;
  //
  if FileExists(ArqExport) then
    DeleteFile(ArqExport);
  //
  frxPDFExport.FileName := ArqExport;
  frxPDFExport.Title    := 'RMIP';
  //
  if PrepRep then
    Frx.PrepareReport();
  //
  Frx.Export(frxPDFExport);
  //
  {$IfDef CO_DMKID_APP_0004}
  //FmPrincipal.MostraUploads(EdEmpresa.ValueVariant, ArqExport, ArqExportNome, True);
  {$EndIf}
end;

Perguntas!

SELECT pcp.Nome NO_PERGUNTA, pli.*
FROM prglstits pli
LEFT JOIN prgcadprg pcp ON pcp.Codigo=pli.Pergunta
WHERE pli.Codigo=1
AND pli.Aplicacao & 1
ORDER BY pli.Ordem, pli.Controle

*)


end.

