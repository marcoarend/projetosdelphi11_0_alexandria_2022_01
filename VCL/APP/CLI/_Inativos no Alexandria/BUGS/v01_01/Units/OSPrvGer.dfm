object FmOSPrvGer: TFmOSPrvGer
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-027 :: Gerenciamento de Provid'#234'ncias'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 385
        Height = 32
        Caption = 'Gerenciamento de Provid'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 385
        Height = 32
        Caption = 'Gerenciamento de Provid'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 385
        Height = 32
        Caption = 'Gerenciamento de Provid'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 463
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 463
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 463
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 1004
          Height = 166
          Align = alTop
          Caption = ' Pesquisa: '
          TabOrder = 0
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 1000
            Height = 149
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox4: TGroupBox
              Left = 0
              Top = 0
              Width = 612
              Height = 149
              Align = alLeft
              TabOrder = 0
              object Label4: TLabel
                Left = 12
                Top = 12
                Width = 32
                Height = 13
                Caption = 'Cliente'
              end
              object Label5: TLabel
                Left = 12
                Top = 56
                Width = 40
                Height = 13
                Caption = 'Contato:'
              end
              object Label6: TLabel
                Left = 12
                Top = 100
                Width = 37
                Height = 13
                Caption = 'Agente:'
              end
              object CBCliente: TdmkDBLookupComboBox
                Left = 68
                Top = 28
                Width = 537
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NO_ENT'
                ListSource = DsEntidades
                TabOrder = 1
                dmkEditCB = EdCliente
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdCliente: TdmkEditCB
                Left = 12
                Top = 28
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdClienteChange
                DBLookupComboBox = CBCliente
                IgnoraDBLookupComboBox = False
              end
              object EdContato: TdmkEditCB
                Left = 12
                Top = 72
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdContatoChange
                DBLookupComboBox = CBContato
                IgnoraDBLookupComboBox = False
              end
              object CBContato: TdmkDBLookupComboBox
                Left = 68
                Top = 72
                Width = 537
                Height = 21
                KeyField = 'Controle'
                ListField = 'Nome'
                ListSource = DsEntiContat
                TabOrder = 3
                dmkEditCB = EdContato
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdAgente: TdmkEditCB
                Left = 12
                Top = 116
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdAgenteChange
                DBLookupComboBox = CBAgente
                IgnoraDBLookupComboBox = False
              end
              object CBAgente: TdmkDBLookupComboBox
                Left = 68
                Top = 116
                Width = 241
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsAgentes
                TabOrder = 5
                dmkEditCB = EdAgente
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdPrvStatus: TdmkEditCB
                Left = 312
                Top = 116
                Width = 32
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'PrvStatus'
                UpdCampo = 'PrvStatus'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdPrvStatusChange
                DBLookupComboBox = CBPrvStatus
                IgnoraDBLookupComboBox = False
              end
              object CBPrvStatus: TdmkDBLookupComboBox
                Left = 344
                Top = 116
                Width = 261
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOSPrvStatus
                TabOrder = 7
                dmkEditCB = EdPrvStatus
                QryCampo = 'PrvStatus'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object CkPrvStatus: TCheckBox
                Left = 312
                Top = 96
                Width = 121
                Height = 17
                Caption = 'Status da provid'#234'ncia:'
                Checked = True
                State = cbChecked
                TabOrder = 8
              end
            end
            object GroupBox3: TGroupBox
              Left = 612
              Top = 0
              Width = 388
              Height = 149
              Align = alClient
              TabOrder = 1
              object Label1: TLabel
                Left = 12
                Top = 100
                Width = 86
                Height = 13
                Caption = 'Forma de contato:'
                Color = clBtnFace
                ParentColor = False
              end
              object EdFormContat: TdmkEditCB
                Left = 12
                Top = 116
                Width = 32
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'FormContat'
                UpdCampo = 'FormContat'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdFormContatChange
                DBLookupComboBox = CBFormContat
                IgnoraDBLookupComboBox = False
              end
              object CBFormContat: TdmkDBLookupComboBox
                Left = 44
                Top = 116
                Width = 189
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsFormContat
                TabOrder = 1
                dmkEditCB = EdFormContat
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object Panel6: TPanel
                Left = 2
                Top = 15
                Width = 384
                Height = 78
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 2
                object GroupBox5: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 241
                  Height = 78
                  Align = alLeft
                  Caption = ' Per'#237'odo: '
                  TabOrder = 0
                  object Panel7: TPanel
                    Left = 2
                    Top = 15
                    Width = 237
                    Height = 61
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object TPDtHrContat_Ini: TdmkEditDateTimePicker
                      Left = 8
                      Top = 28
                      Width = 108
                      Height = 21
                      Date = 0.639644131944805900
                      Time = 0.639644131944805900
                      TabOrder = 0
                      OnClick = TPDtHrContat_IniClick
                      OnChange = TPDtHrContat_IniChange
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      UpdType = utYes
                    end
                    object TPDtHrContat_Fim: TdmkEditDateTimePicker
                      Left = 120
                      Top = 28
                      Width = 108
                      Height = 21
                      Date = 0.639644131944805900
                      Time = 0.639644131944805900
                      TabOrder = 1
                      OnClick = TPDtHrContat_FimClick
                      OnChange = TPDtHrContat_FimChange
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      UpdType = utYes
                    end
                    object CkDtHrContat_Ini: TCheckBox
                      Left = 8
                      Top = 8
                      Width = 97
                      Height = 17
                      Caption = 'DataInicial:'
                      TabOrder = 2
                    end
                    object CkDtHrContat_Fim: TCheckBox
                      Left = 120
                      Top = 8
                      Width = 97
                      Height = 17
                      Caption = 'DataFinal:'
                      TabOrder = 3
                    end
                  end
                end
                object RGOrfaos: TRadioGroup
                  Left = 241
                  Top = 0
                  Width = 143
                  Height = 78
                  Align = alClient
                  Caption = ' '#211'rf'#227'o de OS: '
                  ItemIndex = 1
                  Items.Strings = (
                    'N'#195'O'
                    'SIM'
                    'S+N')
                  TabOrder = 1
                  OnClick = RGOrfaosClick
                end
              end
              object BtOK: TBitBtn
                Tag = 18
                Left = 244
                Top = 100
                Width = 137
                Height = 40
                Caption = '&Pesquisa'
                NumGlyphs = 2
                TabOrder = 3
                OnClick = BtOKClick
              end
            end
          end
        end
        object DBGrid1: TdmkDBGridZTO
          Left = 2
          Top = 181
          Width = 1004
          Height = 280
          Align = alClient
          DataSource = DsPesq
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'N'#176' SP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'O.S. atrelada'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrContat'
              Title.Caption = 'Data / hora contato'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FormContat'
              Title.Caption = 'Forma de contato'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Cliente'
              Title.Caption = 'Cliente'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Contato'
              Title.Caption = 'Contato'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Agente'
              Title.Caption = 'Agente'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PrvStatus'
              Title.Caption = 'Status'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Solicita'#231#227'o de provid'#234'ncia'
              Width = 980
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 16
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 264
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
      end
    end
  end
  object QrFormContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM formcontat'
      'ORDER BY Nome')
    Left = 448
    Top = 12
    object QrFormContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFormContat: TDataSource
    DataSet = QrFormContat
    Left = 476
    Top = 12
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 500
    Top = 12
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 528
    Top = 12
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT eco.Controle, eco.Nome'
      'FROM enticontat eco'
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle'
      'WHERE eco.Codigo=100'
      'ORDER BY Nome')
    Left = 556
    Top = 12
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 584
    Top = 12
  end
  object QrAgentes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidades'
      'WHERE Nome <> '#39#39' '
      'AND Fornece2="V"'
      'ORDER BY Nome')
    Left = 612
    Top = 12
    object QrAgentesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgentesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsAgentes: TDataSource
    DataSet = QrAgentes
    Left = 640
    Top = 12
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    SQL.Strings = (
      'SELECT prv.*, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, '
      'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_Agente, '
      'eco.Nome NO_Contato, fct.Nome NO_FormContat  '
      'FROM osprv prv '
      'LEFT JOIN entidades cli ON cli.Codigo=prv.Cliente '
      'LEFT JOIN entidades age ON age.Codigo=prv.Agente '
      'LEFT JOIN enticontat eco ON eco.Controle=prv.Contato '
      'LEFT JOIN formcontat fct ON fct.Codigo=prv.FormContat '
      ' '
      ' ')
    Left = 100
    Top = 292
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      DisplayFormat = '#;-#;Nenhuma'
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqDtHrContat: TDateTimeField
      FieldName = 'DtHrContat'
    end
    object QrPesqFormContat: TIntegerField
      FieldName = 'FormContat'
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesqContato: TIntegerField
      FieldName = 'Contato'
    end
    object QrPesqAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrPesqNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrPesqNO_Agente: TWideStringField
      FieldName = 'NO_Agente'
      Size = 100
    end
    object QrPesqNO_Contato: TWideStringField
      FieldName = 'NO_Contato'
      Size = 30
    end
    object QrPesqNO_FormContat: TWideStringField
      FieldName = 'NO_FormContat'
      Size = 60
    end
    object QrPesqPrvStatus: TIntegerField
      FieldName = 'PrvStatus'
    end
    object QrPesqNO_PrvStatus: TWideStringField
      FieldName = 'NO_PrvStatus'
      Size = 60
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 128
    Top = 292
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 216
    Top = 44
  end
  object QrOSPrvStatus: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM osprvsta'
      'ORDER BY Nome')
    Left = 668
    Top = 12
    object QrOSPrvStatusCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPrvStatusNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOSPrvStatus: TDataSource
    DataSet = QrOSPrvStatus
    Left = 696
    Top = 12
  end
  object frxGER_OSERV_027_001: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxGER_OSERV_027_001GetValue
    Left = 100
    Top = 344
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Height = 166.299320000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 158.740260000000000000
          Top = 18.897650000000000000
          Width = 362.834880000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Provid'#234'ncias')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 521.575140000000100000
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente: [VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Top = 83.149660000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Contato: [VARF_CONTATO]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo16: TfrxMemoView
          Top = 105.826840000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Agente: [VARF_AGENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Top = 124.724490000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Status da provid'#234'ncia: [VARF_NO_PRVSTATUS]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Top = 147.401670000000000000
          Width = 517.795610000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma de contato: [VARF_FORMACONTATO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 453.543600000000000000
          Top = 147.401670000000000000
          Width = 226.771800000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #211'rf'#227'o de OS: [VARF_ORFAOS]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 415.748300000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 37.795278040000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
        RowCount = 0
        Stretched = True
        object Memo8: TfrxMemoView
          Left = 45.354360000000000000
          Top = 3.779530000000022000
          Width = 185.196970000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_Cliente'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_Cliente"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 230.551330000000000000
          Top = 3.779530000000022000
          Width = 71.811070000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'DtHrContat'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."DtHrContat"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 302.362400000000000000
          Top = 3.779530000000022000
          Width = 79.370130000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_FormContat'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_FormContat"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 381.732530000000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_Contato'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_Contato"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 457.323130000000000000
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_PrvStatus'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_PrvStatus"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 551.811380000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 612.283860000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_Agente'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_Agente"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Top = 3.779530000000022000
          Width = 45.354360000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Cliente'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Cliente"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Top = 20.787404020000000000
          Width = 680.315400000000000000
          Height = 17.007874015748030000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Nome'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 20.787404020000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          Top = 3.779530000000051000
          Width = 230.551330000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 230.551330000000000000
          Top = 3.779530000000051000
          Width = 71.811070000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 302.362400000000000000
          Top = 3.779530000000051000
          Width = 79.370130000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma de contato')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 381.732530000000000000
          Top = 3.779530000000051000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Contato')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 457.323130000000000000
          Top = 3.779530000000051000
          Width = 94.488250000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 551.811380000000000000
          Top = 3.779530000000051000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OS Atrelada')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 612.283860000000000000
          Top = 3.779530000000051000
          Width = 68.031496060000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Respons'#225'vel')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        Height = 3.779530000000000000
        Top = 351.496290000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsPesq: TfrxDBDataset
    UserName = 'frxDsPesq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'DtHrContat=DtHrContat'
      'FormContat=FormContat'
      'Cliente=Cliente'
      'Contato=Contato'
      'Agente=Agente'
      'Nome=Nome'
      'NO_Cliente=NO_Cliente'
      'NO_Agente=NO_Agente'
      'NO_Contato=NO_Contato'
      'NO_FormContat=NO_FormContat'
      'PrvStatus=PrvStatus'
      'NO_PrvStatus=NO_PrvStatus')
    DataSet = QrPesq
    BCDToCurrency = False
    Left = 156
    Top = 292
  end
end
