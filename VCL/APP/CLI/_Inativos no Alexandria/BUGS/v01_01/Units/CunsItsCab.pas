unit CunsItsCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Menus, Grids, DBGrids;

type
  TFmCadastro_Com_Itens_CAB = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrCadastro_Com_Itens_CAB: TmySQLQuery;
    QrCadastro_Com_Itens_CABLk: TIntegerField;
    QrCadastro_Com_Itens_CABDataCad: TDateField;
    QrCadastro_Com_Itens_CABDataAlt: TDateField;
    QrCadastro_Com_Itens_CABUserCad: TIntegerField;
    QrCadastro_Com_Itens_CABUserAlt: TIntegerField;
    QrCadastro_Com_Itens_CABCodigo: TSmallintField;
    QrCadastro_Com_Itens_CABNome: TWideStringField;
    DsCadastro_Com_Itens_CAB: TDataSource;
    QrCadastro_Com_Itens_ITS: TmySQLQuery;
    QrCadastro_Com_Itens_ITSCodigo: TIntegerField;
    QrCadastro_Com_Itens_ITSCNPJ_CPF: TWideStringField;
    QrCadastro_Com_Itens_ITSNome: TWideStringField;
    QrCadastro_Com_Itens_ITSCNPJ_CPF_TXT: TWideStringField;
    DsCadastro_Com_Itens_ITS: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    N1: TMenuItem;
    Nomeiagrupo1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCadastro_Com_Itens_CABAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCadastro_Com_Itens_CABBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrCadastro_Com_Itens_CABAfterScroll(DataSet: TDataSet);
    procedure QrCadastro_Com_Itens_ITSCalcFields(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure Nomeiagrupo1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenCadastro_Com_Itens_ITS();

  end;

var
  FmCadastro_Com_Itens_CAB: TFmCadastro_Com_Itens_CAB;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, Cadastro_Com_Itens_ITS;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCadastro_Com_Itens_CAB.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCadastro_Com_Itens_CAB.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrCadastro_Com_Itens_ITSCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrCadastro_Com_Itens_ITSNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
end;

procedure TFmCadastro_Com_Itens_CAB.Nomeiagrupo1Click(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
begin
  Nome := QrCadastro_Com_Itens_ITSNome.Value;
  Codigo := QrCadastro_Com_Itens_CABCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cadastro_com_itens_cab', False, [
  'Nome'], ['Codigo'], [Nome], [Codigo], False) then
    LocCod(Codigo, Codigo);
end;

procedure TFmCadastro_Com_Itens_CAB.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrCadastro_Com_Itens_CAB);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrCadastro_Com_Itens_CAB, QrCadastro_Com_Itens_ITS);
end;

procedure TFmCadastro_Com_Itens_CAB.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrCadastro_Com_Itens_CAB);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrCadastro_Com_Itens_ITS);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrCadastro_Com_Itens_ITS);
  //
  MyObjects.HabilitaMenuItemItsDel(Nomeiagrupo1, QrCadastro_Com_Itens_ITS);
end;

procedure TFmCadastro_Com_Itens_CAB.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCadastro_Com_Itens_CABCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCadastro_Com_Itens_CAB.DefParams;
begin
  VAR_GOTOTABELA := 'cadastro_com_itens_cab';
  VAR_GOTOMYSQLTABLE := QrCadastro_Com_Itens_CAB;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cadastro_com_itens_cab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCadastro_Com_Itens_CAB.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmCadastro_Com_Itens_CAB.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + #13#10 +
  Caption + #13#10 + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmCadastro_Com_Itens_CAB.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCadastro_Com_Itens_CAB.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCadastro_Com_Itens_CAB.ItsExclui1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma do sacado / emitente do grupo atual?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM cadastro_com_itens_its WHERE CNPJ_CPF=:P0');
    Dmod.QrUpd.SQL.Add('AND Codigo=:P1');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[0].AsString  := QrCadastro_Com_Itens_ITSCNPJ_CPF.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrCadastro_Com_Itens_ITSCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenCadastro_Com_Itens_ITS;
  end;
end;

procedure TFmCadastro_Com_Itens_CAB.ReopenCadastro_Com_Itens_ITS();
begin
  QrCadastro_Com_Itens_ITS.Close;
  QrCadastro_Com_Itens_ITS.Params[0].AsInteger := QrCadastro_Com_Itens_CABCodigo.Value;
  QrCadastro_Com_Itens_ITS.Open;
end;


procedure TFmCadastro_Com_Itens_CAB.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCadastro_Com_Itens_CAB.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCadastro_Com_Itens_CAB.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCadastro_Com_Itens_CAB.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCadastro_Com_Itens_CAB.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCadastro_Com_Itens_CAB.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCadastro_Com_Itens_CAB.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCadastro_Com_Itens_CABCodigo.Value;
  Close;
end;

procedure TFmCadastro_Com_Itens_CAB.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmCadastro_Com_Itens_CAB.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCadastro_Com_Itens_CAB, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'cadastro_com_itens_cab');
end;

procedure TFmCadastro_Com_Itens_CAB.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('cadastro_com_itens_cab', 'Codigo', ImgTipo.SQLType,
    QrCadastro_Com_Itens_CABCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmCadastro_Com_Itens_CAB, PnEdita,
    'cadastro_com_itens_cab', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmCadastro_Com_Itens_CAB.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'cadastro_com_itens_cab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cadastro_com_itens_cab', 'Codigo');
end;

procedure TFmCadastro_Com_Itens_CAB.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmCadastro_Com_Itens_CAB.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmCadastro_Com_Itens_CAB.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmCadastro_Com_Itens_CAB.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCadastro_Com_Itens_CABCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCadastro_Com_Itens_CAB.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCadastro_Com_Itens_CAB.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCadastro_Com_Itens_CABCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCadastro_Com_Itens_CAB.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCadastro_Com_Itens_CAB.QrCadastro_Com_Itens_CABAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCadastro_Com_Itens_CAB.QrCadastro_Com_Itens_CABAfterScroll(DataSet: TDataSet);
begin
  ReopenCadastro_Com_Itens_ITS();
end;

procedure TFmCadastro_Com_Itens_CAB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrCadastro_Com_Itens_CABCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmCadastro_Com_Itens_CAB.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCadastro_Com_Itens_CABCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadastro_com_itens_cab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCadastro_Com_Itens_CAB.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCadastro_Com_Itens_CAB.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCadastro_Com_Itens_CAB, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'cadastro_com_itens_cab');
end;

procedure TFmCadastro_Com_Itens_CAB.QrCadastro_Com_Itens_CABBeforeOpen(DataSet: TDataSet);
begin
  QrCadastro_Com_Itens_CABCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCadastro_Com_Itens_CAB.QrCadastro_Com_Itens_ITSCalcFields(DataSet: TDataSet);
begin
  QrCadastro_Com_Itens_ITSCNPJ_CPF_TXT.Value :=
    Geral.FormataCNPJ_TT(QrCadastro_Com_Itens_ITSCNPJ_CPF.Value);
end;

end.

