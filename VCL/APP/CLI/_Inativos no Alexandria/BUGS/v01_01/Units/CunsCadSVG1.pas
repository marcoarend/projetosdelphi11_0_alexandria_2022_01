unit CunsCadSVG1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, XMLIntf, dmkGeral, UnDmkEnums,
  RSSVGCtrls, RSGdiPlusCtrls, Data.DB, mySQLDbTables, RSSVG, RSSVGTypes,
  RSGraphics, RSGdiPlusGraphicsTypes, Vcl.Grids, Vcl.DBGrids, Vcl.Menus,
  Vcl.Buttons, UnApp_Consts, dmkDBGridZTO, frxClass, UnDmkImg, UnInternalConsts;

type
  THackRSSVGImage = class(TRSSVGImage);
  TPMV = record
   ID: Integer;
   Doc: TRSSVGDocument;
   Img: TRSSVGImage;
   Codigo: Integer;
   Nome: String;
  end;
  TFmCunsCadSVG1 = class(TForm)
    RSSVGDocument1: TRSSVGDocument;
    Splitter1: TSplitter;
    QrSiapImaSVG: TmySQLQuery;
    QrSiapImaSVGCodigo: TIntegerField;
    QrSiapImaSVGControle: TIntegerField;
    QrSiapImaSVGNome: TWideStringField;
    QrSiapImaSVGNoArq: TWideStringField;
    QrSiapImaSVGArquivo: TWideMemoField;
    QrSiapImaSVGLk: TIntegerField;
    QrSiapImaSVGDataCad: TDateField;
    QrSiapImaSVGDataAlt: TDateField;
    QrSiapImaSVGUserCad: TIntegerField;
    QrSiapImaSVGUserAlt: TIntegerField;
    QrSiapImaSVGAlterWeb: TSmallintField;
    QrSiapImaSVGAtivo: TSmallintField;
    QrPMVsSim: TmySQLQuery;
    DsPMVsSim: TDataSource;
    QrPMVsSimNO_MOTDESAT: TWideStringField;
    QrPMVsSimNO_EQUI: TWideStringField;
    QrPMVsSimNivel1: TIntegerField;
    QrPMVsSimNO_LISTA: TWideStringField;
    QrPMVsSimNO_DEPENDENCIA: TWideStringField;
    QrPMVsSimDtaAquis_TXT: TWideStringField;
    QrPMVsSimDtaDesativ_TXT: TWideStringField;
    QrPMVsSimCodigo: TIntegerField;
    QrPMVsSimNome: TWideStringField;
    QrPMVsSimEquipamento: TIntegerField;
    QrPMVsSimOSMonCab: TIntegerField;
    QrPMVsSimMotDesativ: TIntegerField;
    QrPMVsSimDtaAquis: TDateField;
    QrPMVsSimDtaDesativ: TDateField;
    QrPMVsSimLk: TIntegerField;
    QrPMVsSimDataCad: TDateField;
    QrPMVsSimDataAlt: TDateField;
    QrPMVsSimUserCad: TIntegerField;
    QrPMVsSimUserAlt: TIntegerField;
    QrPMVsSimAlterWeb: TSmallintField;
    QrPMVsSimAtivo: TSmallintField;
    QrPMVsSimPrgLstCab: TIntegerField;
    QrPMVsSimDependenci: TIntegerField;
    QrPMVsSimIntrvMonDD: TIntegerField;
    QrPMVsSimOrdem: TIntegerField;
    QrPMVsSimReordem: TIntegerField;
    QrPMVsSimMotInutili: TIntegerField;
    QrPMVsSimDtaInutili: TDateField;
    QrPMVsSimSvgPosX: TFloatField;
    QrPMVsSimSvgPosY: TFloatField;
    QrPMVsSimSvgZumF: TFloatField;
    QrPMVsSimSiapImaSVG: TIntegerField;
    QrPMVsNao: TmySQLQuery;
    DsPMVsNao: TDataSource;
    QrPMVsNaoNO_MOTDESAT: TWideStringField;
    QrPMVsNaoNO_EQUI: TWideStringField;
    QrPMVsNaoNivel1: TIntegerField;
    QrPMVsNaoNO_LISTA: TWideStringField;
    QrPMVsNaoNO_DEPENDENCIA: TWideStringField;
    QrPMVsNaoDtaAquis_TXT: TWideStringField;
    QrPMVsNaoDtaDesativ_TXT: TWideStringField;
    QrPMVsNaoCodigo: TIntegerField;
    QrPMVsNaoNome: TWideStringField;
    QrPMVsNaoEquipamento: TIntegerField;
    QrPMVsNaoOSMonCab: TIntegerField;
    QrPMVsNaoMotDesativ: TIntegerField;
    QrPMVsNaoDtaAquis: TDateField;
    QrPMVsNaoDtaDesativ: TDateField;
    QrPMVsNaoLk: TIntegerField;
    QrPMVsNaoDataCad: TDateField;
    QrPMVsNaoDataAlt: TDateField;
    QrPMVsNaoUserCad: TIntegerField;
    QrPMVsNaoUserAlt: TIntegerField;
    QrPMVsNaoAlterWeb: TSmallintField;
    QrPMVsNaoAtivo: TSmallintField;
    QrPMVsNaoPrgLstCab: TIntegerField;
    QrPMVsNaoDependenci: TIntegerField;
    QrPMVsNaoIntrvMonDD: TIntegerField;
    QrPMVsNaoOrdem: TIntegerField;
    QrPMVsNaoReordem: TIntegerField;
    QrPMVsNaoMotInutili: TIntegerField;
    QrPMVsNaoDtaInutili: TDateField;
    QrPMVsNaoSvgPosX: TFloatField;
    QrPMVsNaoSvgPosY: TFloatField;
    QrPMVsNaoSvgZumF: TFloatField;
    QrPMVsNaoSiapImaSVG: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    TreeView1: TTreeView;
    Edit1: TEdit;
    DBGrid2: TDBGrid;
    QrGraG1EqMo: TmySQLQuery;
    QrGraG1EqMoNivel1: TIntegerField;
    QrGraG1EqMoMarca: TIntegerField;
    QrGraG1EqMoObservacao: TWideMemoField;
    QrGraG1EqMoLk: TIntegerField;
    QrGraG1EqMoDataCad: TDateField;
    QrGraG1EqMoDataAlt: TDateField;
    QrGraG1EqMoUserCad: TIntegerField;
    QrGraG1EqMoUserAlt: TIntegerField;
    QrGraG1EqMoAlterWeb: TSmallintField;
    QrGraG1EqMoAtivo: TSmallintField;
    QrGraG1EqMoPrgLstCab: TIntegerField;
    QrGraG1EqMoNaoUsaPrdt: TSmallintField;
    QrGraG1EqMoVetorSvg: TWideMemoField;
    QrGraG1EqMoVetorArq: TWideStringField;
    QrGraG1EqMoPrgICorSVG: TIntegerField;
    QrGraG1EqMoDiasCorSVG: TIntegerField;
    QrGraG1EqMoQtCorSVGgg: TIntegerField;
    QrGraG1EqMoQtCorSVGgr: TIntegerField;
    QrGraG1EqMoQtCorSVGrg: TIntegerField;
    QrGraG1EqMoQtCorSVGrr: TIntegerField;
    QrGraG1EqMoQtCorSVGrb: TIntegerField;
    QrGraG1EqMoQtCorSVGbr: TIntegerField;
    QrGraG1EqMoZumPadr: TFloatField;
    QrGraG1EqMoSVGLegenda: TWideStringField;
    PMPMV: TPopupMenu;
    Zoom1: TMenuItem;
    TabSheet6: TTabSheet;
    QrAtaques: TmySQLQuery;
    QrAtaquesITENS: TFloatField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrItens: TmySQLQuery;
    DsItens: TDataSource;
    QrItensPipCad: TIntegerField;
    QrItensNome: TWideStringField;
    QrItensITENS: TFloatField;
    N1: TMenuItem;
    RemovePMVdocroqui1: TMenuItem;
    PMSim: TPopupMenu;
    Zoomeposio1: TMenuItem;
    QrGG1EM: TmySQLQuery;
    QrGG1EMNivel1: TIntegerField;
    QrGG1EMNome: TWideStringField;
    QrGG1EMVetorSvg: TWideMemoField;
    TabSheet4: TTabSheet;
    SbLegenda: TScrollBox;
    RSSVGImgLegenda: TRSSVGImage;
    QrGG1EMQtCorSVGgr: TIntegerField;
    QrGG1EMQtCorSVGrg: TIntegerField;
    QrGG1EMQtCorSVGrr: TIntegerField;
    QrGG1EMQtCorSVGrb: TIntegerField;
    QrGG1EMQtCorSVGbr: TIntegerField;
    QrGG1EMQtCorSVGgg: TIntegerField;
    QrGG1EMSVGLegenda: TWideStringField;
    SBCroqui: TScrollBox;
    RSSVGImgCroqui: TRSSVGImage;
    QrGG1EMDiasCorSVG: TIntegerField;
    TabSheet5: TTabSheet;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    Panel3: TPanel;
    BtOK: TBitBtn;
    N2: TMenuItem;
    DefiniroZoomatualcomopadro1: TMenuItem;
    LaAviso: TLabel;
    procedure Edit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure RSSVGImageMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RSSVGImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure RSSVGImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Zoom1Click(Sender: TObject);
    procedure RemovePMVdocroqui1Click(Sender: TObject);
    procedure Zoomeposio1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DefiniroZoomatualcomopadro1Click(Sender: TObject);
  private
    { Private declarations }
    FCroquiXML: String;
    // PMVs
    FPosIni: TPoint;
    FPMV_Qtds: Integer;
    FPMVs: array of TPMV;
    FLgds: array of TPMV;
(*
    FPMV_Docs: array of TRSSVGDocument;
    FPMV_Imgs: array of TRSSVGImage;
    FPMV_Cods: array of Integer;
*)
    FMovendo: Boolean;
    FCroquiCount, FTamLegenda: Integer;
    //
    //procedure PaintBox1Paint(Sender: TObject); Nao usei PaintBox!
    function  CriaSVGDocument(XML: String; Zum: Double; Codigo: Integer;
              Nome: String; Ataques: Integer): TRSSVGImage;
    procedure DocChanged(Sender: TObject);
    procedure ParsingNode(Sender: TObject; const Node: IXMLNode);
    procedure ParsedNode(Sender: TObject; const Element: TSVGElement);
    //function  ObtemPMV(const Id: Integer; var Codigo: Integer): Boolean;
    function  TraduzTagsXML(const XML: String; const PipCad: Integer;
              const NO_PMV: String; var Ataques: Integer): String;
    // FIM PMVs
    procedure ReopenSiapImaSVG(Controle: Integer);
    procedure MostraEquipamento(Nivel1: Integer);
  public
    { Public declarations }
    FSelected: TRSSVGImage;
    FCliente, FSiapTerCad, FSiapImaCad, FSiapImaSVG: Integer;
    //
    procedure CarregaDadosCliente(Cliente, SiapTerCad, SiapImaCad,
              SiapImaSVG: Integer);
    procedure CarregaLegenda();
    procedure CarregaSVGsPMVPosicionados();
    function  LimpaXML(XML: String): String;
    procedure LimpaPMVs();
    procedure ReopenPMVs(Qry: TMySQLQuery; Posicionado: Boolean; Codigo: Integer);
    function  SelecionaPMV_PeloCodigo(const Codigo: Integer; var PMV: TRSSVGImage): Boolean;
    procedure FillTreeView(RSSVGDocument: TRSSVGDocument);
    procedure RefazZoomPMV(Codigo: Integer; SvgZumF: Double; PMV: TRSSVGImage);
    function  ReopenGraG1EqMo(const Nivel1: Integer; var XML: String): Boolean;
  end;

var
  FmCunsCadSVG1: TFmCunsCadSVG1;

implementation

uses
  StrUtils, RSVclHelperFunctions, DmkDAC_PF, Module, UnDmkProcFunc, UnMyObjects,
  UMySQLModule, GetValor, CunsCadSVGZumPos, MyDBCheck, UnProjGroup_Consts,
  Principal;

{$R *.dfm}

const
  Preto        = '#000000';
  PageHeight   = 1080;
  TitleHeight  = 10;
  TitleGapVert = 6;
  ShapeGapVert = 1;
  LeftMargin   = 5;
  TextGapVert  = 2;
  TextGapHorz  = 4;
  FontSize     = 7;
  LeftImg      = 80;
  QTam         = 60;
  QCor         = 8;

procedure TFmCunsCadSVG1.BtOKClick(Sender: TObject);
var
  Equipamento: Integer;
begin
  if (QrPMVsNao.State <> dsInactive) and (QrPMVsNao.RecordCount > 0) then
    Equipamento := QrPMVsNaoEquipamento.Value
  else
    Equipamento := 0;
  //
  MostraEquipamento(Equipamento);
end;

procedure TFmCunsCadSVG1.Button1Click(Sender: TObject);
begin
  SBLegenda.Align := alNone;
  SBLegenda.Width  := RSSVGImgLegenda.Width;
  SBLegenda.Height := RSSVGImgLegenda.Height;
end;

procedure TFmCunsCadSVG1.Button2Click(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to Self.ComponentCount -1 do
  begin
    if Self.Components[I] is TShape then
    begin
      Geral.MB_Info(
      'Shape W: ' + Geral.FF0(TShape(Self.Components[I]).Width) + sLineBreak +
      'Shape H: ' + Geral.FF0(TShape(Self.Components[I]).Height) + sLineBreak +
      'Shape L: ' + Geral.FF0(TShape(Self.Components[I]).Left) + sLineBreak +
      'Shape T: ' + Geral.FF0(TShape(Self.Components[I]).Top) + sLineBreak +
      '');
    end;
  end;

end;

procedure TFmCunsCadSVG1.CarregaDadosCliente(Cliente, SiapTerCad, SiapImaCad,
  SiapImaSVG: Integer);
begin

  //EXIT;

  FCliente := Cliente;
  FSiapTerCad := SiapTerCad;
  FSiapImaCad := SiapImaCad;
  FSiapImaSVG := SiapImaSVG;
  ReopenSiapImaSVG(FSiapImaSVG);
  //
  FCroquiXML := DmkPF.XMLDecode(QrSiapImaSVGArquivo.Value);
  //
  RSSVGDocument1.SVG.LoadFromText(FCroquiXML);
  RSSVGImgCroqui.SVGRootID := '';
  RSSVGImgCroqui.WrapMode := iwOriginal;
  FillTreeView(RSSVGDocument1);
  //
  ReopenPMVs(QrPMVsSim, True,  0);
  CarregaSVGsPMVPosicionados();
  ReopenPMVs(QrPMVsNao, False, 0);

  //
  RSSVGImgCroqui.Width := THAckRSSVGImage(RSSVGImgCroqui).BitMap.Width;
  RSSVGImgCroqui.Height := THAckRSSVGImage(RSSVGImgCroqui).BitMap.Height;
  // Fazer manual! Automatico nao esta funcionando!!
  SBCroqui.HorzScrollBar.Range := RSSVGImgCroqui.Width;
  SBCroqui.VertScrollBar.Range := RSSVGImgCroqui.Height;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrItens, Dmod.MyDB, [
  'SELECT omc.PipCad, pmv.Nome, (COUNT(opi.Conta) + 0.000) ITENS  ',
  'FROM ospipits opi ',
  'LEFT JOIN ospipmon omc ON omc.Controle=opi.Controle ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN pipcad pmv ON pmv.Codigo=omc.PipCad ',
  'LEFT JOIN grag1eqmo emo ON emo.Nivel1=pmv.Equipamento ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstits ',
  'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts ',
  'WHERE cab.SiapTerCad=' + Geral.FF0(FSiapTerCad),
  'AND TO_DAYS(SYSDATE()) - TO_DAYS(cab.DtaExeFim) > 0  ',
  'AND TO_DAYS(SYSDATE()) - TO_DAYS(cab.DtaExeFim) <= emo.DiasCorSVG ',
  'AND opi.RespAtrIts>0  ',
  'AND (pai.Efeito=0 OR pai.Efeito IS NULL) ',
  'GROUP BY omc.PipCad',
  '']);
  // EXIT
(*
      with THackRSSVGImage(RSSVGImgLegenda).Bitmap do
      begin
        Canvas.Pen.Color := clBlack;
        Canvas.Font.Name := 'Arial';
        Canvas.Font.Color := clBlack;
        Canvas.Font.Size := 12;
        Canvas.Font.Style := [fsBold];
        Canvas.TextOut(100, 100, 'Form:  sdff ef ef ewf we gfe w  :.');
        Canvas.TextOut(100, 130, 'Genus:  qw rewrwerew ew twe twe tew t  :.');
      end;
*)
end;

procedure TFmCunsCadSVG1.CarregaLegenda();
var
  MyShape: TShape;
  X1, Y1, X2, Y2: Integer;
  //
  procedure CriaShape(Item: Integer; R, G, B: Word);
  begin
    MyShape := TShape.Create(Self);
    //MyShape.Owner := Self;
    //with MyShape do
    begin
      MyShape.Parent := SbLegenda;
      MyShape.Align := alNone;
      MyShape.Left   := 14; //X1;
      MyShape.Top    := Y1 + (Item * (Y2 + ShapeGapVert));
      MyShape.Width  := X2;
      MyShape.Height := Y2;
      MyShape.Pen.Color   := RGB(R, G, B);
      MyShape.Brush.Color := RGB(R, G, B);
      //MyShape.Anchors := [akLeft, akTop, akRight];
    end;
  end;
  procedure CriaLabel(Item, Qtd: Integer);
  var
    MyLabel: TLabel;
  begin
    MyLabel := TLabel.Create(Self);
    //with MyLabel do
    begin
      MyLabel.Parent := SbLegenda;
      MyLabel.Align := alNone;
      MyLabel.AutoSize := False;
      MyLabel.Font.Size := FontSize;
      MyLabel.Font.Color := clBlack;
      //MyLabel.Color := clRed;
      MyLabel.Transparent := True; //False;
      MyLabel.Left   := MyShape.Left + MyShape.Width + TextGapHorz;
      MyLabel.Top    := Y1 + (Item * (Y2 + ShapeGapVert)) - TextGapVert;
      MyLabel.Width  := LeftImg - MyLabel.Left - 4;
      //MyLabel.Width  := 1000;
      MyLabel.Height := QCor + 1; //Y2;
      //MyLabel.Anchors := [akLeft, akTop, akRight];
      MyLabel.Caption := Geral.FF0(Qtd);
    end;
  end;
  //
  procedure CriaTitles();
  var
    MyLabel: TLabel;
  begin
    MyLabel := TLabel.Create(Self);
    //with MyLabel do
    begin
      MyLabel.Parent := SbLegenda;
      MyLabel.Align := alNone;
      MyLabel.AutoSize := False;
      MyLabel.Font.Size := FontSize;
      MyLabel.Font.Color := clBlack;
      MyLabel.Color := clRed;
      MyLabel.Transparent := True; //False;
      MyLabel.Left   := X1;
      MyLabel.Top    := Y1 + TitleGapVert;
      MyLabel.Width  := X2;
      //MyLabel.Width  := 1000;
      MyLabel.Height := QCor + 2;//Y2;
      //MyLabel.Anchors := [akLeft, akTop, akRight];
      MyLabel.Caption := QrGG1EMSVGLegenda.Value + ' (' + Geral.FF0(
      QrGG1EMDiasCorSVG.Value) + ' dias) - ' + QrGG1EMNome.Value;
    end;
(*
    MyLabel := TLabel.Create(Self);
    with MyLabel do
    begin
      Font.Size := FontSize;
      Parent := SbLegenda;
      Left   := LeftImg;
      Top    := Y1 + TitleGapVert;
      Width  := X2;
      Height := Y2;
      Anchors := [akLeft, akTop, akRight];
      Caption := QrGG1EMNome.Value;
    end;
*)
  end;
  //
  function CarregaAtual(): TRSSVGImage;
  var
    SVGAlign: TSVGAlign;
    I, H, W, F, Codigo: Integer;
    XML, Nome: String;
    Zum: Double;
    p: TPoint;
  begin
    XML    := QrGG1EMVetorSVG.Value;
    if Trim(XML) <> '' then
    begin
      //I := QrGG1EM.RecNo - 1;
      I := FCroquiCount;
      FCroquiCount := FCroquiCount + 1;
      Codigo := QrGG1EMNivel1.Value;
      Nome   := QrGG1EMNome.Value;
      //
      FLgds[I].ID := I;
      FLgds[I].Doc := TRSSVGDocument.Create(Self);
      XML := LimpaXML(DmkPF.XMLDecode(XML));
      //XML := TraduzTagsXML(XML, QrPMVsSimCodigo.Value, QrPMVsSimNome.Value, Ataques);
      XML :=
        StringReplace(
        StringReplace(
        XML,
        CO_PMV_XML_CorPMV, Preto, [rfReplaceAll]),
        CO_PMV_XML_IdPMV,  Nome, [rfReplaceAll]);
      //
      FLgds[I].Doc.SVG.LoadFromText(XML);
      //
      FLgds[I].Img := TRSSVGImage.Create(Self);
      Result := FLgds[I].Img;
      Result.Tag := I;
      Result.Parent := SBLegenda;
      FLgds[I].Codigo := Codigo;
      FLgds[I].Nome := Nome;
      //
      Result.WrapMode := iwOriginal;
      Result.SVGDocument := FLgds[I].Doc;
      Result.SVGRootID := '';
      //
      H := THackRSSVGImage(Result).Bitmap.Height;
      W := THackRSSVGImage(Result).Bitmap.Width;
      // ver se tem tamanho nos pips!
      // se nao tiver eles ficam ocultos!
      if (H < 1 ) or (W < 1) then
      begin
        if H < 1 then
          H := 1000;
        if W < 1 then
          W := 1000;
      end;
      if H > W then
        F := H
      else
        F := W;
      if F = 0 then
        F := QTam;
      Zum := Qtam / F;
      Result.Height := Trunc(Trunc(H + 1) * Zum) + 1;
      Result.Width  := Trunc(Trunc(W + 1) * Zum) + 1;
      // colocar ataques?
      //Result.Hint := Nome + sLineBreak + 'Qtde: ' + Geral.FF0(Ataques);
      Result.ShowHint := True;
      Result.ScaleOriginal := Zum;
      Result.Left := LeftImg;
      Result.Top  := I * (QTam + TitleHeight) + TitleHeight + TitleGapVert;
      //
      FTamLegenda := Result.Top + Result.Height + TitleHeight;
      //

      X1 := LeftMargin;
      Y1 := (QTam + TitleHeight) * I;
      X2 := RSSVGImgLegenda.Width * 2;
      Y2 := QCor;
      //
      CriaTitles();
      //
      Y1 := Y1 + TitleHeight;
      //
      X2 := QCor;
      CriaShape(1, 000, 255, 000);    // Verde
      CriaShape(2, 255, 255, 000);    // Amarelo
      CriaShape(3, 255, 128, 000);    // Laranja
      CriaShape(4, 255, 000, 000);    // Vermelho
      CriaShape(5, 255, 000, 255);    // Magenta
      CriaShape(6, 128, 000, 255);    // Lil�z
      //
      X2 := LeftImg;
      CriaLabel(1, QrGG1EMQtCorSVGgg.Value);
      CriaLabel(2, QrGG1EMQtCorSVGgr.Value);
      CriaLabel(3, QrGG1EMQtCorSVGrg.Value);
      CriaLabel(4, QrGG1EMQtCorSVGrr.Value);
      CriaLabel(5, QrGG1EMQtCorSVGrb.Value);
      CriaLabel(6, QrGG1EMQtCorSVGbr.Value);

    end;
    // FIm XML
  end;
var
  x, y: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGG1EM, Dmod.MyDB, [
  'SELECT gg1.Nivel1, gg1.Nome, gem.VetorSvg, ',
  'gem.QtCorSVGgg, gem.QtCorSVGgr, gem.QtCorSVGrg, ',
  'gem.QtCorSVGrr, gem.QtCorSVGrb, gem.QtCorSVGbr, ',
  'gem.SVGLegenda, gem.DiasCorSVG ',
  'FROM gragru1 gg1 ',
  'LEFT JOIN grag1eqmo gem ON gem.Nivel1=gg1.Nivel1 ',
  'WHERE gg1.GraTabApp=' + Geral.FF0(CO_COD_GraTabApp_GraG1EqMo),
  'ORDER BY gg1.Nome ',
  '']);
  //
  SetLength(FLgds, QrGG1EM.RecordCount);
  FCroquiCount := 0;
  //
  RSSVGImgLegenda.Width := SbLegenda.Width;
  RSSVGImgLegenda.Height := PageHeight;
  with THackRSSVGImage(RSSVGImgLegenda).Bitmap do
  begin
    Width := RSSVGImgLegenda.Width;
    Height := RSSVGImgLegenda.Height;
    Canvas.Rectangle(0, 0, Width, Height);
  end;
  QrGG1EM.First;
  while not QrGG1EM.Eof do
  begin
    CarregaAtual();
    //
    QrGG1EM.Next;
  end;
  // Fazer manual! Automatico nao esta funcionando!!
  //RSSVGImgLegenda.Height := (QTam * FCroquiCount) + 10;
  RSSVGImgLegenda.Height := FTamLegenda;
  SbLegenda.HorzScrollBar.Range := RSSVGImgLegenda.Width;
  SbLegenda.VertScrollBar.Range := RSSVGImgLegenda.Height;
end;

procedure TFmCunsCadSVG1.CarregaSVGsPMVPosicionados();
  procedure CarregaAtual();
  var
    XML: String;
    SVGGRoup: TSVGGroup;
    N, Codigo, Ataques: Integer;
    RSSVGImage: TRSSVGImage;
    ZumF: Double;
  begin
    if ReopenGraG1EqMo(QrPMVsSimNivel1.Value, XML) then
    begin
      Codigo := QrPMVsSimCodigo.Value;
      //
      XML := LimpaXML(DmkPF.XMLDecode(XML));
      XML := TraduzTagsXML(XML, QrPMVsSimCodigo.Value, QrPMVsSimNome.Value,
             Ataques);
      //Memo1.Text := XML;
      //
      ZumF := QrPMVsSimSvgZumF.Value;
      RSSVGImage := CriaSVGDocument(XML, ZumF, QrPMVsSimCodigo.Value,
        QrPMVsSimNome.Value, Ataques);
      RSSVGImage.ScaleOriginal := ZumF;
      RSSVGImage.Left := (*FPositionX +*) Trunc(QrPMVsSimSvgPosX.Value);
      RSSVGImage.Top  := (*FPositionY +*) Trunc(QrPMVsSimSvgPosY.Value);
    end;
  end;
begin
  QrPMVsSim.First;
  while not QrPMVsSim.Eof do
  begin
    CarregaAtual();
    //
    QrPMVsSim.Next;
  end;
end;

function TFmCunsCadSVG1.CriaSVGDocument(XML: String; Zum: Double; Codigo:
Integer; Nome: String; Ataques: Integer): TRSSVGImage;
var
  SVGAlign: TSVGAlign;
  I, J, H, W: Integer;
begin
  I := FPMV_Qtds;
  J := I + 1;
  SetLength(FPMVs, J);
  FPMVs[I].ID := I;
  FPMVs[I].Doc := TRSSVGDocument.Create(Self);
  FPMVs[I].Doc.SVG.LoadFromText(XML);
  //
  FPMVs[I].Img := TRSSVGImage.Create(Self);
  Result := FPMVs[I].Img;
  Result.Tag := I;
  Result.Parent := SBCroqui;
  FPMVs[I].Codigo := Codigo;
  FPMVs[I].Nome := Nome;
  //
  Result.WrapMode := iwOriginal;
  Result.SVGDocument := FPMVs[I].Doc;
  Result.SVGRootID := '';
  //FillTreeView(RSSVGDocument2);
  //RSSVGImgCroqui.Visible := False;
  Result.OnMouseDown := RSSVGImageMouseDown;
  Result.OnMouseMove := RSSVGImageMouseMove;
  Result.OnMouseUp   := RSSVGImageMouseUp;
  //
  H := THackRSSVGImage(Result).Bitmap.Height;
  W := THackRSSVGImage(Result).Bitmap.Width;
  // ver se tem tamanho nos pips!
  // se nao tiver eles ficam ocultos!
  if (H < 1 ) or (W < 1) then
  begin
    if H < 1 then
      H := 1000;
    if W < 1 then
      W := 1000;
  end;
  Result.Height := Trunc(Trunc(H + 1) * Zum) + 1;
  Result.Width  := Trunc(Trunc(W + 1) * Zum) + 1;
  // colocar ataques?
  Result.Hint := Nome + sLineBreak + 'Qtde: ' + Geral.FF0(Ataques);
  Result.ShowHint := True;
  //
  FPMV_Qtds := J;
end;

procedure TFmCunsCadSVG1.DBGrid2DblClick(Sender: TObject);
var
  XML: String;
  N, Codigo, SvgPosX, SvgPosY, Ataques: Integer;
  SvgZumF: Double;
  RSSVGImage: TRSSVGImage;
begin
  if ReopenGraG1EqMo(QrPMVsNaoNivel1.Value, XML) then
  begin
    XML := LimpaXML(DmkPF.XMLDecode(XML));
    //
    SvgPosX := 10;
    SvgPosY := 10;
    SvgZumF := QrGraG1EqMoZumPadr.Value;
    try
      RSSVGImage := CriaSVGDocument(XML, SvgZumF, QrPMVsNaoCodigo.Value,
        QrPMVsNaoNome.Value, Ataques);
      RSSVGImage.ScaleOriginal := SvgZumF;
      RSSVGImage.Left := SvgPosX;
      RSSVGImage.Top  := SvgPosY;
      //
      Codigo := QrPMVsNaoCodigo.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False, [
      'SvgPosX', 'SvgPosY', 'SvgZumF',
      'SiapImaSVG'], ['Codigo'], [
      SvgPosX, SvgPosY, SvgZumF,
      FSiapImaSVG], [Codigo], True) then
      begin
        ReopenPMVs(QrPMVsSim, True,  Codigo);
        QrPMVsNao.Next;
        Codigo := QrPMVsNaoCodigo.Value;
        ReopenPMVs(QrPMVsNao, False, Codigo);
      end;
    except
      on E: Exception do
      begin
        Geral.MB_Erro(E.Message + slineBreak + XML)
      end;
    end;
  end;
end;

procedure TFmCunsCadSVG1.DefiniroZoomatualcomopadro1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Codigo, Equipamento: Integer;
  SvgZumF: Variant;
begin
  SvgZumF := TRSSVGImage(FSelected).ScaleOriginal;
  Codigo  := FPMVs[TRSSVGImage(FSelected).Tag].Codigo;
  //
  if Codigo <> 0 then
  begin
    Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Equipamento ',
        'FROM pipcad ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      Equipamento := Qry.FieldByName('Equipamento').AsInteger;

      if Equipamento <> 0 then
      begin
        if MyObjects.FIC(SvgZumF < 0.000001, nil, 'Valor inv�lido!') then
          Exit;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'grag1eqmo', False,
          ['ZumPadr'], ['Nivel1'], [SvgZumF], [Equipamento], True) then
        begin
          CarregaLegenda();
          ReopenPMVs(QrPMVsNao, False, 0);
        end;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmCunsCadSVG1.DocChanged(Sender: TObject);
begin
//
end;

procedure TFmCunsCadSVG1.Edit1Change(Sender: TObject);
(*
  function GetNodeByText
  (ATree : TTreeView; AValue:String;
   AVisible: Boolean): TTreeNode;
  var
      Node: TTreeNode;
  begin
    Result := nil;
    if ATree.Items.Count = 0 then Exit;
    Node := ATree.Items[0];
    while Node <> nil do
    begin
      if ContainsText(Node.Text, AValue) then
      begin
        Result := Node;
        if AVisible then
        begin
          result.Selected := True;
          Result.MakeVisible;
        end;
        Break;
      end;
      Node := Node.GetNext;
    end;
  end;
*)
begin
(*
  GetNodeByText(TreeView1, Edit1.Text, True);
*)
end;

procedure TFmCunsCadSVG1.FillTreeView(RSSVGDocument: TRSSVGDocument);
  procedure FillTreeViewItem( E: TSVGElement; tvItem: TTreeNode );
  var
    i: Integer;
    newItem: TTreeNode;
  begin
    for i := 0 to E.Items.Count - 1 do
    begin
      newItem := TreeView1.Items.AddChildObject(tvItem, E.Items[i].ID + '('+
        E.Items[i].SVGTypeName+')', E.Items[i]);
//      newItem.IsChecked := True;
      FillTreeViewItem( E.Items[i], newItem );
    end;
  end;
begin
  TreeView1.Items.BeginUpdate;
  try
    TreeView1.OnChange := nil;
    TreeView1.Items.Clear;
    FillTreeViewItem( RSSVGDocument.SVG, nil );
  finally
    //TreeView1.OnChange := TreeView1Change;
    TreeView1.Items.EndUpdate;
  end;
end;

procedure TFmCunsCadSVG1.FormCreate(Sender: TObject);
begin
  FSelected := nil;
  FMovendo  := False;
  FPMV_Qtds := 0;
  //
  LaAviso.Caption := 'De um duplo clique no item da grade ' + sLineBreak +
                     'para posicion�-lo no croqui';
  //
(*  Nao funciona para deixar o fundo branco!!!!
  RSSVGImgCroqui.Width := 1046;
  RSSVGImgCroqui.Height := 680;
  with THackRSSVGImage(RSSVGImgCroqui).Bitmap do
  begin
    Canvas.Brush.Color := clWhite;
    Canvas.Brush.BackColor := clBlack;
    Width := RSSVGImgCroqui.Width;
    Height := RSSVGImgCroqui.Height;
    Canvas.Rectangle(0, 0, Width, Height);
  end;
//*)
  CarregaLegenda();
  //PageControl1.ActivePageIndex := 0;
  //
  if (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)) then
    TabSheet5.TabVisible := True
  else
    TabSheet5.TabVisible := False;
end;

procedure TFmCunsCadSVG1.LimpaPMVs();
var
  I: Integer;
begin
  FSelected := nil;
  for I := Low(FPMVs) to High(FPMVs) do
  begin
    if FPMVs[I].Img <> nil then
    begin
      //TRSSVGImage(FPMVs[I].Img).Visible := False;
      TRSSVGImage(FPMVs[I].Img).Free;
      TRSSVGImage(FPMVs[I].Img) := nil;
      FPMVs[I].Img := nil;
    end;
  end;
  FPMV_Qtds := 0;
  SetLength(FPMVs, FPMV_Qtds);
end;

function TFmCunsCadSVG1.LimpaXML(XML: String): String;
const
  Xi = '<!--';
  Xf = '-->';
var
  Ini, Tam: Integer;
  Tmp: String;
begin
  Tmp := StringReplace(
    //StringReplace(
      XML,
//      '\"', '""',[rfReplaceAll]),
      '-- >', '-->',[rfReplaceAll]);
  //
  if pos(Xi, Tmp) > 0 then
  begin
    Result := '';
    Ini := 0;
    Tam := 0;
    //Tmp := XML;
    while Length(Tmp) > 0 do
    begin
      Tam := Pos(Xi, Tmp);
      if Tam > 0 then
        //Result := Result + Tmp.Substring(0, Tam - 1);
        Result := Result + Copy(Tmp, 1, Tam - 1);
      Tam := Pos(Xf, Tmp);
      //Tmp := Tmp.Substring(Tam + 2); // 3 - 1
      Tmp := Copy(Tmp, Tam + 3);
      if pos(Xi, Tmp) = 0 then
      begin
        Result := Result + Tmp;
        Tmp := '';
      end;
    end;

  end else
    Result := Tmp;
end;

{
function TFmCunsCadSVG.ObtemPMV(const Id: Integer; var Codigo: Integer): Boolean;
var
  I: Integer;
begin
  Result := True;
  Codigo := -1;
  for I := 0 to High(FPMVs) do
  begin
    if FPMVs[I].Img.Tag = I then
    begin
      Result := True;
      Codigo := FPMVs[I].Codigo;
      Exit;
    end;
  end;
end;
}

procedure TFmCunsCadSVG1.ParsedNode(Sender: TObject; const Element: TSVGElement);
begin
//
end;

procedure TFmCunsCadSVG1.ParsingNode(Sender: TObject; const Node: IXMLNode);
begin
//
end;

procedure TFmCunsCadSVG1.RefazZoomPMV(Codigo: Integer; SvgZumF: Double;
  PMV: TRSSVGImage);
var
  W, H: Integer;
  Zum: Variant;
begin
  Zum := PMV.ScaleOriginal;
  W := PMV.Width / Zum * SvgZumF;
  H := PMV.Height / Zum * SvgZumF;
  PMV.Width  := W;
  PMV.Height := H;
  PMV.ScaleOriginal := SvgZumF;
  //
  ReopenPMVs(QrPMVsSim, True,  Codigo);
end;

procedure TFmCunsCadSVG1.RemovePMVdocroqui1Click(Sender: TObject);
const
  SiapImaSVG = 0;
var
  Codigo: Integer;
begin
  if Geral.MB_Pergunta('Confirma a remo��o do PMV deste croqui?') = ID_YES then
  begin
    Codigo := FPMVs[TRSSVGImage(FSelected).Tag].Codigo;

    if Codigo <> 0 then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False,
        ['SiapImaSVG'], ['Codigo'], [SiapImaSVG], [Codigo], True) then
      begin
        TRSSVGImage(FSelected).Visible := False;
        TRSSVGImage(FSelected) := nil;
        ReopenPMVs(QrPMVsSim, True,  0);
        //CarregaSVGsPMVPosicionados();
        ReopenPMVs(QrPMVsNao, False, 0);
      end;
    end;
  end;
end;

procedure TFmCunsCadSVG1.MostraEquipamento(Nivel1: Integer);
begin
  FmPrincipal.MostraFormGraG1Equi(gbsMonitora, Nivel1);
  //
  CarregaLegenda();
  ReopenPMVs(QrPMVsNao, False, 0);
end;

function TFmCunsCadSVG1.ReopenGraG1EqMo(const Nivel1: Integer; var XML: String): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraG1EqMo, Dmod.MyDB, [
  'SELECT *  ',
  'FROM grag1eqmo ',
  'WHERE Nivel1=' + Geral.FF0(Nivel1),
  '']);
  if QrGraG1EqMo.RecordCount = 0 then
  begin
    Result := False;
    Geral.MB_Aviso('Item n�o encontrado nos equipamentos de monitoramento!');
  end else
  begin
    XML := QrGraG1EqMoVetorSvg.Value;
    Result := XML <> '';
    if not Result then
    begin
      if Geral.MB_Pergunta('Equipamento sem vetor (croqui) definido!' +
        sLineBreak + 'Deseja definir agora?') = ID_YES then
      begin
        MostraEquipamento(Nivel1);
      end;
    end;
  end;
end;

procedure TFmCunsCadSVG1.ReopenPMVs(Qry: TMySQLQuery; Posicionado: Boolean;
  Codigo: Integer);
var
  SQLPosicionado: String;
begin
  if Posicionado then
  begin
    //SQLPosicionado := 'AND cad.SiapImaSVG <> 0'
    SQLPosicionado := 'AND cad.SiapImaSVG=' + Geral.FF0(FSiapImaSVG)
  end else
  begin
    //SQLPosicionado := 'AND cad.SiapImaSVG = 0';
    SQLPosicionado := 'AND cad.SiapImaSVG<>' + Geral.FF0(FSiapImaSVG)
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT mot.Nome NO_MOTDESAT, gg1.Nome NO_EQUI, gg1.Nivel1, ',
  'plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA, ',
  'IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT, ',
  'IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT, ',
  'cad.* ',
  'FROM pipcad cad ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab ',
  'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci ',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
  'LEFT JOIN osmoncab omc ON omc.Conta=cad.OsMonCab ',
  'LEFT JOIN oscab osc ON osc.Codigo=omc.Codigo ',
  'WHERE osc.Entidade=' + Geral.FF0(FCliente),
  'AND (cad.MotInutili = 0 AND cad.MotInutili = 0) ',
  SQLPOsicionado,
  'ORDER BY Nome ',
  '']);
  //
  if Codigo <> 0 then
    Qry.Locate('Codigo', Codigo, []);
end;

procedure TFmCunsCadSVG1.ReopenSiapImaSVG(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapImaSVG, Dmod.MyDB, [
  'SELECT * ',
  'FROM siapimasvg ',
  'WHERE Controle=' + Geral.FF0(FSiapImaSVG),
  '']);
end;

procedure TFmCunsCadSVG1.RSSVGImageMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FSelected := TRSSVGImage(Sender);
  if ssLeft in Shift then
  begin
    FMovendo := True;
    FPosIni.X := X;
    FPosIni.Y := Y;
  end;
end;

procedure TFmCunsCadSVG1.RSSVGImageMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then
  begin
    if FMovendo then
    begin
      TRSSVGImage(Sender).Left := TRSSVGImage(Sender).Left - (FPosIni.X - X);
      TRSSVGImage(Sender).Top  := TRSSVGImage(Sender).Top - (FPosIni.Y - Y);
    end;
  end;
end;

procedure TFmCunsCadSVG1.RSSVGImageMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Item, Codigo, SvgPosX, SvgPosY: Integer;
begin
  if Button = mbLeft then
  begin
    if FMovendo then
    begin
      FMovendo := False;
      Item := TRSSVGImage(Sender).Tag;
      Codigo  := FPMVs[Item].Codigo;
      SvgPosX := TRSSVGImage(Sender).Left - (FPosIni.X - X);
      SvgPosY := TRSSVGImage(Sender).Top - (FPosIni.Y - Y);
      TRSSVGImage(Sender).Left := SvgPosX;
      TRSSVGImage(Sender).Top  := SvgPosY;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False, [
      'SvgPosX', 'SvgPosY'], ['Codigo'], [
      SvgPosX, SvgPosY], [Codigo], True);
    end;
  end else
  if Button = mbRight then
  begin
    MyObjects.MostraPopUpDeBotao(PMPMV, SBCroqui, TRSSVGImage(Sender));
  end;
end;

function TFmCunsCadSVG1.SelecionaPMV_PeloCodigo(const Codigo: Integer;
  var PMV: TRSSVGImage): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := Low(FPMVs) to High(FPMVs) do
  begin
    if FPMVs[I].Codigo = Codigo then
    begin
      Result := True;
      PMV := FPMVs[I].Img;
      FSelected := PMV;
      //
      Exit;
    end;
  end;
end;

function TFmCunsCadSVG1.TraduzTagsXML(const XML: String; const PipCad: Integer;
const NO_PMV: String; var Ataques: Integer): String;
var
  I, K, CorInt: Integer;
  Val, GG, GR, RG, RR, RB, BR: Double;
  CorTxt: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtaques, Dmod.MyDB, [
(*
  'SELECT COUNT(opi.Conta) + 0.000 ITENS ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstits ',
  'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts ',
  'LEFT JOIN ospipmon omc ON omc.Controle=opi.Controle ',
  'LEFT JOIN pipcad pmv ON pmv.Codigo=omc.PipCad ',
  'LEFT JOIN grag1eqmo emo ON emo.Nivel1=pmv.Equipamento ',
  'WHERE cab.Entidade=' + Geral.FF0(FCliente),
  'AND omc.PipCad=' + Geral.FF0(PipCad),
  'AND TO_DAYS(SYSDATE()) - TO_DAYS(cab.DtaExeFim) > 0 ',
  'AND TO_DAYS(SYSDATE()) - TO_DAYS(cab.DtaExeFim) <= emo.DiasCorSVG ',
  'AND opi.RespAtrIts>0 ',
  'AND pai.Efeito=0 ',
*)
  'SELECT (COUNT(opi.Conta) + 0.000) ITENS  ',
  'FROM ospipits opi ',
  'LEFT JOIN ospipmon omc ON omc.Controle=opi.Controle ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN pipcad pmv ON pmv.Codigo=omc.PipCad ',
  'LEFT JOIN grag1eqmo emo ON emo.Nivel1=pmv.Equipamento ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstits ',
  'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts ',
  'WHERE omc.PipCad=' + Geral.FF0(PipCad),
  'AND cab.SiapTerCad=' + Geral.FF0(FSiapTerCad),
  'AND TO_DAYS(SYSDATE()) - TO_DAYS(cab.DtaExeFim) > 0  ',
  'AND TO_DAYS(SYSDATE()) - TO_DAYS(cab.DtaExeFim) <= emo.DiasCorSVG ',
  'AND opi.RespAtrIts>0  ',
  'AND (pai.Efeito=0 OR pai.Efeito IS NULL) ',
  '']);
  //Geral.MB_SQL(Self, QrAtaques);
  Ataques := Trunc(QrAtaquesITENS.Value);
  //
  GG := QrGraG1EqMoQtCorSVGgg.Value;
  GR := QrGraG1EqMoQtCorSVGgr.Value;
  RG := QrGraG1EqMoQtCorSVGrg.Value;
  RR := QrGraG1EqMoQtCorSVGrr.Value;
  RB := QrGraG1EqMoQtCorSVGrb.Value;
  BR := QrGraG1EqMoQtCorSVGBr.Value;
  Val := QrAtaquesITENS.Value;
  CorInt := Trunc(dmkPF.CalculaCor(Val, GG, GR, RG, RR, RB, BR));
  CorTxt := '#' + DmkImg.TColorToHex(CorInt);
  Result := XML;
  Result :=
    StringReplace(
    StringReplace(
    Result,
    CO_PMV_XML_CorPMV, CorTxt, [rfReplaceAll]),
    CO_PMV_XML_IdPMV,  NO_PMV, [rfReplaceAll]);
////////////////////////////////////////////////////////////////////////////////
(*  Outras constantes!
    CO_PMV_XML_OnClick
    CO_PMV_XML_PosicaoPMVIni
    CO_PMV_XML_ZoomPMV
    CO_PMV_XML__
    CO_PMV_XML_PosicaoPMVFim
*)
end;

procedure TFmCunsCadSVG1.Zoom1Click(Sender: TObject);
var
  Codigo, W, H: Integer;
  SvgZumF, Zum: Variant;
begin
  //ID := TRSSVGImage(FSelected).Tag;
  //if ObtemPMV(Id, Codigo) then
  begin
    SvgZumF := TRSSVGImage(FSelected).ScaleOriginal;
    Codigo  :=   FPMVs[TRSSVGImage(FSelected).Tag].Codigo;
    //
    if Codigo <> 0 then
    begin
      if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, SvgZumF, 6, 0,
      '0,000001', '', True, 'Zoom', 'Fator zoom (x vezes): ', 0, SvgZumF) then
      begin
        if MyObjects.FIC(SvgZumF < 0.000001, nil, 'Valor inv�lido!') then
          Exit;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False, [
        'SvgZumF'], ['Codigo'], [
        SvgZumF], [Codigo], True);
        //
        RefazZoomPMV(Codigo, SvgZumF, TRSSVGImage(FSelected));
      end;
    end;
  end;
end;

procedure TFmCunsCadSVG1.Zoomeposio1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCunsCadSVGZumPos, FmCunsCadSVGZumPos, afmoNegarComAviso) then
  begin
    FmCunsCadSVGZumPos.ImgTipo.SQLType := stUpd;
    FmCunsCadSVGZumPos.EdCodigo.ValueVariant      := QrPMVsSimCodigo.Value;
    FmCunsCadSVGZumPos.EdSvgPosX.ValueVariant     := QrPMVsSimSvgPosX.Value;
    FmCunsCadSVGZumPos.EdSvgPosY.ValueVariant     := QrPMVsSimSvgPosY.Value;
    FmCunsCadSVGZumPos.EdSvgZumF.ValueVariant     := QrPMVsSimSvgZumF.Value;
    FmCunsCadSVGZumPos.EdSiapImaSVG.ValueVariant  := QrPMVsSimSiapImaSVG.Value;
    FmCunsCadSVGZumPos.EdEquipamento.ValueVariant := QrPMVsSimEquipamento.Value;
    //
    FmCunsCadSVGZumPos.ShowModal;
    FmCunsCadSVGZumPos.Destroy;
  end;
  RSSVGImgCroqui.Invalidate;
end;

end.
