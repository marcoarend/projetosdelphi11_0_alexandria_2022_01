unit SiapTerFlh;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, Vcl.ComCtrls, dmkEditDateTimePicker, dmkRadioGroup,
  UnDmkEnums;

type
  TFmSiapTerFlh = class(TForm)
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntPagante: TmySQLQuery;
    QrEntPaganteCodigo: TIntegerField;
    QrEntPaganteNO_ENT: TWideStringField;
    DsEntPagante: TDataSource;
    DsEntContrat: TDataSource;
    QrEntContrat: TmySQLQuery;
    QrEntContratCodigo: TIntegerField;
    QrEntContratNO_ENT: TWideStringField;
    DsEntiContat: TDataSource;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    DsAgeEqiCab: TDataSource;
    QrAgeEqiCab: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrAgeEqiCabNome: TWideStringField;
    Panel5: TPanel;
    Label36: TLabel;
    Label2: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label55: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    EdMonEntCtr: TdmkEditCB;
    CBMonEntCtr: TdmkDBLookupComboBox;
    EdMonNumCtr: TdmkEdit;
    EdMonEntPag: TdmkEditCB;
    CBMonEntPag: TdmkDBLookupComboBox;
    EdMonEntCtt: TdmkEditCB;
    CBMonEntCtt: TdmkDBLookupComboBox;
    EdMonAgeEqi: TdmkEditCB;
    CbMonAgeEqi: TdmkDBLookupComboBox;
    EdMonCondPg: TdmkEditCB;
    CBMonCondPg: TdmkDBLookupComboBox;
    EdMonCrtEmi: TdmkEditCB;
    CBMonCrtEmi: TdmkDBLookupComboBox;
    Label25: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label38: TLabel;
    TPDataSincOS: TdmkEditDateTimePicker;
    SbDataSincOS: TSpeedButton;
    Label49: TLabel;
    EdTrajetDist: TdmkEdit;
    RGTrajetKm_h: TdmkRadioGroup;
    Label28: TLabel;
    EdMinHAgeExe: TdmkEdit;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdMonNumCtrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMonEntCtrChange(Sender: TObject);
    procedure EdMonEntPagChange(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure SbDataSincOSClick(Sender: TObject);
    procedure EdMonEntCtrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMonEntPagKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenQryIts(Controle: Integer);

  public
    { Public declarations }
    FCliente: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure ReopenEntiContat();
  end;

  var
  FmSiapTerFlh: TFmSiapTerFlh;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral, ModuleFatura, UnContratUnit, ModOS;

{$R *.DFM}

procedure TFmSiapTerFlh.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Empresa, MonAgeEqi, MonEntCtt, MonNumCtr, MonEntCtr,
  MonEntPag, MonCondPg, MonCrtEmi, TrajetDist, TrajetKm_h: Integer;
  DataSincOS, MinHAgeExe: String;
  //
  Qry: TmySQLQuery;
begin
  Codigo   := Geral.IMV(DBEdCodigo.Text);
  Controle := EdControle.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  if ImgTipo.SQLType = stIns then
  begin
    if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
    'Informe uma empresa!') then
      Exit;
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Empresa ',
      'FROM siapterflh ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      if MyObjects.FIC(Qry.RecordCount >0, EdEmpresa,
      'Empresa j� cadastrada!') then
        Exit;
    finally
      Qry.Free;
    end;
  end;
  //
  if MyObjects.FIC(TPDataSincOS.Date < 2, TPDataSincOS,
  'Data de refer�ncia para gera��o de OSs autom�ticas n�o definida!') then
    Exit;
  //
  MonAgeEqi      := EdMonAgeEqi.ValueVariant;
  MonEntCtt      := EdMonEntCtt.ValueVariant;
  MonNumCtr      := EdMonNumCtr.ValueVariant;
  MonEntCtr      := EdMonEntCtr.ValueVariant;
  MonEntPag      := EdMonEntPag.ValueVariant;
  MonCondPg      := EdMonCondPg.ValueVariant;
  MonCrtEmi      := EdMonCrtEmi.ValueVariant;
  DataSincOS     := Geral.FDT(TPDataSincOS.Date, 1);
  TrajetDist     := EdTrajetDist.ValueVariant;
  TrajetKm_h     := RGTrajetKm_h.ItemIndex;
  MinHAgeExe     := EdMinHAgeExe.Text;
  //
  Controle := UMyMod.BPGS1I32('siapterflh', 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'siapterflh', False, [
  'Codigo', 'Empresa', 'MonAgeEqi',
  'MonEntCtt', 'MonNumCtr', 'MonEntCtr',
  'MonEntPag', 'MonCondPg', 'MonCrtEmi',
  'DataSincOS', 'TrajetDist', 'TrajetKm_h',
  'MinHAgeExe'
  ], [
  'Controle'], [
  Codigo, Empresa, MonAgeEqi,
  MonEntCtt, MonNumCtr, MonEntCtr,
  MonEntPag, MonCondPg, MonCrtEmi,
  DataSincOS, TrajetDist, TrajetKm_h,
  MinHAgeExe
  ], [
  Controle], True) then
  begin
    ReopenQryIts(Controle);
(*
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      Ed??.ValueVariant     := 0;
      CB??.KeyValue         := Null;
      //
      EdEmpresa.Enabled := True;
      EdEmpresa.SetFocus;
    end else
*)
    Close;
  end;
end;

procedure TFmSiapTerFlh.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSiapTerFlh.EdEmpresaChange(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := DmFatura.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
  //
  DmFatura.ReopenCartEmis(Entidade);
end;

procedure TFmSiapTerFlh.EdMonEntCtrChange(Sender: TObject);
begin
  ReopenEntiContat();
end;

procedure TFmSiapTerFlh.EdMonEntCtrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdMonEntCtr.ValueVariant := FCliente;
end;

procedure TFmSiapTerFlh.EdMonEntPagChange(Sender: TObject);
begin
  ReopenEntiContat();
end;

procedure TFmSiapTerFlh.EdMonEntPagKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdMonEntCtr.ValueVariant := FCliente;
end;

procedure TFmSiapTerFlh.EdMonNumCtrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Empresa, Contratante, Contrato: Integer;
begin
  if Key = VK_F4 then
  begin
    //Empresa     := EdEmpresa.ValueVariant;
    Empresa     := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
    //
      if ContratUnit.PesquisaNumeroDoContrato(Empresa, FCliente, Contrato) then
    EdMonNumCtr.ValueVariant := Contrato;
  end;
end;

procedure TFmSiapTerFlh.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmSiapTerFlh.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrAgeEqiCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntContrat, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntPagante, Dmod.MyDB);
  //
  DmFatura.ReopenPediPrzCab();
  CBMonCondPG.ListSource := DmFatura.DsPediPrzCab;
  CBMonCrtEmi.ListSource := DmFatura.DsCartEmis;
  CBEmpresa.ListSource   := DModG.DsEmpresas;
end;

procedure TFmSiapTerFlh.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSiapTerFlh.ReopenQryIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmSiapTerFlh.SbDataSincOSClick(Sender: TObject);
var
  Empresa, Entidade, SiapTerCad: Integer;
  Data: TDateTime;
begin
  Empresa    := EdEmpresa.ValueVariant;
  Entidade   := FCliente;
  SiapTerCad := Geral.IMV(DBEdCodigo.Text);
  if DmModOS.DataSincOS_1Acao(Empresa, Entidade, SiapTerCad, Data) then
    TPDataSincOS.Date := Trunc(Data);
end;

procedure TFmSiapTerFlh.ReopenEntiContat();
var
  SQL1: String;
  MonEntCtr, MonEntPag: Integer;
begin
  MonEntCtr := EdMonEntCtr.ValueVariant;
  MonEntPag := EdMonEntPag.ValueVariant;
  SQL1 := Geral.FF0(MonEntCtr) + ', ' +
    Geral.FF0(MonEntPag) + ', ' + Geral.FF0(FCliente);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiContat, Dmod.MyDB, [
  'SELECT eco.Controle, eco.Nome ',
  'FROM enticontat eco ',
  'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
  'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo ',
  'WHERE ece.Codigo <> 0 ',
  'AND ece.Codigo IN (' + SQL1 + ') ',
  '']);
  //
end;

end.
