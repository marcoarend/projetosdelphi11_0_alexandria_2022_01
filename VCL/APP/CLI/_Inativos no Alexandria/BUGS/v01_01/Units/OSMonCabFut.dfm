object FmOSMonCabFut: TFmOSMonCabFut
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-040 :: Monitoramento - Adi'#231#227'o PMV em OSs Futuras'
  ClientHeight = 629
  ClientWidth = 828
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 828
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 780
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 732
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 560
        Height = 32
        Caption = 'Monitoramento - Adi'#231#227'o PMV em OSs Futuras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 560
        Height = 32
        Caption = 'Monitoramento - Adi'#231#227'o PMV em OSs Futuras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 560
        Height = 32
        Caption = 'Monitoramento - Adi'#231#227'o PMV em OSs Futuras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 828
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 828
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GBQryes: TGroupBox
        Left = 0
        Top = 0
        Width = 828
        Height = 467
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 425
          Top = 15
          Width = 401
          Height = 450
          Align = alClient
          Caption = ' OSs futuras: '
          TabOrder = 0
          object DBGOSs: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 397
            Height = 385
            Align = alClient
            DataSource = DsOSsFuturas
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Localizador'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaExePrv'
                Title.Caption = 'Dt.Prev.exe.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DIAS_A'
                Title.Caption = 'Dias A'#185
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DIAS_B'
                Font.Charset = ANSI_CHARSET
                Font.Color = clSilver
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Title.Caption = 'Dias B'#178
                Title.Font.Charset = ANSI_CHARSET
                Title.Font.Color = clSilver
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 72
                Visible = True
              end>
          end
          object Panel6: TPanel
            Left = 2
            Top = 400
            Width = 397
            Height = 48
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object BtTodos_OSs: TBitBtn
              Tag = 127
              Left = 4
              Top = 4
              Width = 127
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Todos'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtTodos_OSsClick
            end
            object BtNenhum_OSs: TBitBtn
              Tag = 128
              Left = 132
              Top = 4
              Width = 127
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Nenhum'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtNenhum_OSsClick
            end
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 15
          Width = 423
          Height = 450
          Align = alLeft
          Caption = ' PMVs: '
          TabOrder = 1
          object DBGPMVs: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 419
            Height = 385
            Align = alClient
            DataSource = DsNovosPMVs
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PerioDd'
                Title.Caption = 'Per'#237'odo'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DdPostero'
                Title.Caption = 'Intervalo'
                Width = 48
                Visible = True
              end>
          end
          object Panel5: TPanel
            Left = 2
            Top = 400
            Width = 419
            Height = 48
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object BtTodos_PMV: TBitBtn
              Tag = 127
              Left = 4
              Top = 4
              Width = 127
              Height = 40
              Cursor = crHandPoint
              Caption = '&Todos'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtTodos_PMVClick
            end
            object BtNenhum_PMV: TBitBtn
              Tag = 128
              Left = 132
              Top = 4
              Width = 127
              Height = 40
              Cursor = crHandPoint
              Caption = '&Nenhum'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtNenhum_PMVClick
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 828
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 824
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 783
        Height = 16
        Caption = 
          '('#185') Dias A:  Dias desde a data considerada como inicial do monit' +
          'oramento da OS futura.   ('#178') Dias B: Dias desdea OS dos novos PM' +
          'Vs.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 783
        Height = 16
        Caption = 
          '('#185') Dias A:  Dias desde a data considerada como inicial do monit' +
          'oramento da OS futura.   ('#178') Dias B: Dias desdea OS dos novos PM' +
          'Vs.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 828
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 682
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 680
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrNovosPMVs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pip.*, '
      'omc.PerioDd, omc.DdPostero '
      'FROM pipcad pip '
      'LEFT JOIN osmoncab omc ON omc.PipCad=pip.Codigo '
      'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo '
      'WHERE omc.Controle=3972'
      'AND pip.DtaDesativ < "1900-01-01" ')
    Left = 112
    Top = 84
    object QrNovosPMVsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNovosPMVsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrNovosPMVsEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrNovosPMVsOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrNovosPMVsMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrNovosPMVsDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrNovosPMVsDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrNovosPMVsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNovosPMVsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNovosPMVsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNovosPMVsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNovosPMVsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNovosPMVsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNovosPMVsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNovosPMVsPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrNovosPMVsDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrNovosPMVsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNovosPMVsReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrNovosPMVsPerioDd: TIntegerField
      FieldName = 'PerioDd'
    end
    object QrNovosPMVsDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
  end
  object DsNovosPMVs: TDataSource
    DataSet = QrNovosPMVs
    Left = 112
    Top = 132
  end
  object QrOSsFuturas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT cab.Codigo, cab.DtaExePrv,'
      'TO_DAYS(cab.DtaExePrv) - TO_DAYS("2014-04-22") DIAS'
      'FROM ospipmon opm'
      'LEFT JOIN oscab cab ON cab.Codigo=opm.Codigo'
      'WHERE cab.SiapterCad=1759 '
      'AND cab.DtaExePrv > "2014-04-22"'
      'ORDER BY cab.DtaExePrv')
    Left = 208
    Top = 80
    object QrOSsFuturasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSsFuturasDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
    end
    object QrOSsFuturasSiapterCad: TIntegerField
      FieldName = 'SiapterCad'
    end
    object QrOSsFuturasDIAS_B: TFloatField
      FieldName = 'DIAS_B'
    end
    object QrOSsFuturasDIAS_A: TFloatField
      FieldName = 'DIAS_A'
    end
  end
  object DsOSsFuturas: TDataSource
    DataSet = QrOSsFuturas
    Left = 208
    Top = 128
  end
end
