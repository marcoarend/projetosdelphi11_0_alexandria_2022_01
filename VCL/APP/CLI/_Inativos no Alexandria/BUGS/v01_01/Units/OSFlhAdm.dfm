object FmOSFlhAdm: TFmOSFlhAdm
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-045 :: Administra'#231#227'o de OSs Prolepses'
  ClientHeight = 691
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 399
        Height = 32
        Caption = 'Administra'#231#227'o de OSs Prolepses'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 399
        Height = 32
        Caption = 'Administra'#231#227'o de OSs Prolepses'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 399
        Height = 32
        Caption = 'Administra'#231#227'o de OSs Prolepses'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 529
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 529
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 529
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 467
        object Splitter1: TSplitter
          Left = 2
          Top = 317
          Width = 1004
          Height = 3
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = 613
          ExplicitTop = 56
          ExplicitWidth = 308
        end
        object DBGrid1: TdmkDBGridZTO
          Left = 2
          Top = 56
          Width = 1004
          Height = 261
          Align = alClient
          DataSource = DsFrmFil
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDrawColumnCell = DBGrid1DrawColumnCell
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'PelaEmisUltDta'
              Title.Caption = 'Ult.Dta?'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PelaFormula'
              Title.Caption = 'F'#243'rmula?'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PeloFatoGeradr20'
              Title.Caption = 'FG 20?'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PeloPosGerou0'
              Title.Caption = 'Gerou?'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PelaDtaExeFim'
              Title.Caption = 'Dta. Exec?'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Entidade'
              Title.Caption = 'Cliente'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CLIENTE'
              Title.Caption = 'Cliente'
              Width = 341
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIGLA_TAB'
              Title.Caption = 'Tipo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_STC'
              Title.Caption = 'Lugar'
              Width = 255
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Localizador'
              Width = 60
              Visible = True
            end>
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 320
          Width = 1004
          Height = 207
          Align = alBottom
          DataSource = DsFrmIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID Servi'#231'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Title.Caption = 'ID F. A/M'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CO_FORMULA'
              Title.Caption = 'F'#243'rmula'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORMULA'
              Title.Caption = 'Descri'#231#227'o da f'#243'rmula'
              Width = 283
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EmisUltDta'
              Title.Caption = #218'lt. emis.'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EmisStatus'
              Title.Caption = 'Status'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Periodd'
              Title.Caption = 'Per'#237'odo dias'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ExtenDd'
              Title.Caption = 'Dias Extens'#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DdPostero'
              Title.Caption = 'Intervalo'
              Width = 48
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 41
          Align = alTop
          Caption = 'Fazer Pesquisa?????'
          TabOrder = 2
          Visible = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 577
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 621
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAcao: TBitBtn
        Tag = 10010
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&A'#231#227'o'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAcaoClick
      end
    end
  end
  object QrFrmIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT'
      'IF(xxx.EmisUltDta < SYSDATE() , "Sim", "N'#227'o") PelaEmisUltDta,'
      'IF(cab.DtaExeFim > "1900-01-01", "Sim", "N'#227'o") PelaDtaExeFim,'
      'IF(cab.FatoGeradr=20, "Sim", "N'#227'o") PeloFatoGeradr20,'
      'IF(cab.PosGerou, "Sim", "N'#227'o") PeloPosGerou0,'
      'IF('
      '          cab.NumContrat=0'
      '          AND'
      '          xxx.EmisStatus=0'
      ', "Sim (A)",'
      'IF('
      '('
      '     ('
      '          cab.NumContrat<>0'
      '          AND'
      '          ctr.DtaCntrFim < "1900-01-01"'
      '          AND'
      '          xxx.EmisStatus < 2'
      '     )'
      '), "Sim (C)", "N'#227'o") ) PelaFormula,'
      'cab.Empresa,'
      'xxx.Codigo, xxx.Controle, xxx.Conta, xxx.DdPostero,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,'
      
        ' ELT(xxx.EmisStatus + 1,"Aberto","Parcial","Total") NO_EmisStatu' +
        's,'
      
        'stc.Nome NO_STC, frm.Codigo CO_FORMULA, frm.Nome NO_FORMULA, cab' +
        '.Entidade,'
      'cab.SiapTerCad, cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw,'
      'xxx.EmisUltDta, xxx.Periodd, xxx.EmisStatus,'
      '1 Tabela,'
      '"APL" SIGLA_TAB,'
      'xxx.IDIts IDSuperior'
      'FROM osfrmflhcb xxx'
      'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo'
      'LEFT JOIN formulas frm ON frm.Codigo=xxx.Formula'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN SiapTerCad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat'
      'LEFT JOIN cunscad cun ON cun.Codigo=cab.Entidade'
      'WHERE xxx.Ativo > 0'
      'AND xxx.EmisUltDta < SYSDATE()'
      'AND cab.DtaExeFim > "1900-01-01"'
      'AND cab.FatoGeradr=20'
      'AND cab.PosGerou=0'
      'AND'
      '('
      '     ('
      '          cab.NumContrat=0'
      '          AND'
      '          xxx.EmisStatus=0'
      '     )'
      '     OR'
      '     ('
      '          cab.NumContrat<>0'
      '          AND'
      '          ctr.DtaCntrFim < "1900-01-01"'
      '          AND'
      '          xxx.EmisStatus < 2'
      '     )'
      ')'
      ''
      ''
      'UNION'
      ''
      ''
      'SELECT'
      'IF(xxx.EmisUltDta < SYSDATE() , "Sim", "N'#227'o") PelaEmisUltDta,'
      'IF(cab.DtaExeFim > "1900-01-01", "Sim", "N'#227'o") PelaDtaExeFim,'
      'IF(cab.FatoGeradr=20, "Sim", "N'#227'o") PeloFatoGeradr20,'
      'IF(cab.PosGerou, "Sim", "N'#227'o") PeloPosGerou0,'
      'IF('
      '          cab.NumContrat=0'
      '          AND'
      '          xxx.EmisStatus=0'
      ', "Sim (A)",'
      'IF('
      '('
      '          cab.NumContrat<>0'
      '          AND'
      '          ctr.DtaCntrFim < "1900-01-01"'
      '          AND'
      '          xxx.EmisStatus < 2'
      '), "Sim (C)", "N'#227'o")) PelaFormula,'
      'cab.Empresa,'
      'xxx.Codigo, xxx.Controle, xxx.Conta, xxx.DdPostero,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,'
      
        ' ELT(xxx.EmisStatus + 1,"Aberto","Parcial","Total") NO_EmisStatu' +
        's,'
      
        'stc.Nome NO_STC, frm.Codigo CO_FORMULA, frm.Nome NO_FORMULA, cab' +
        '.Entidade,'
      'cab.SiapTerCad, cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw,'
      'xxx.EmisUltDta, xxx.Periodd, xxx.EmisStatus,'
      '2 Tabela,'
      '"PIP" SIGLA_TAB,'
      'xxx.Conta IDSuperior'
      'FROM osmoncab xxx'
      'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo'
      'LEFT JOIN formulas frm ON frm.Codigo=xxx.Formula'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN SiapTerCad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat'
      'LEFT JOIN cunscad cun ON cun.Codigo=cab.Entidade'
      'WHERE xxx.Ativo > 0'
      'AND xxx.EmisUltDta < SYSDATE()'
      'AND cab.DtaExeFim > "1900-01-01"'
      'AND cab.FatoGeradr=20'
      'AND cab.PosGerou=0'
      'AND'
      '('
      '     ('
      '          cab.NumContrat=0'
      '          AND'
      '          xxx.EmisStatus=0'
      '     )'
      '     OR'
      '     ('
      '          cab.NumContrat<>0'
      '          AND'
      '          ctr.DtaCntrFim < "1900-01-01"'
      '          AND'
      '          xxx.EmisStatus < 2'
      '     )'
      ')'
      ''
      ''
      ''
      'ORDER BY NO_CLIENTE, SIGLA_TAB'
      '')
    Left = 84
    Top = 152
    object QrFrmItsNO_EmisStatus: TWideStringField
      FieldName = 'NO_EmisStatus'
      Size = 7
    end
    object QrFrmItsNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrFrmItsIDSuperior: TIntegerField
      FieldName = 'IDSuperior'
    end
    object QrFrmItsEmisUltDta: TDateField
      FieldName = 'EmisUltDta'
    end
    object QrFrmItsPeriodd: TIntegerField
      FieldName = 'Periodd'
    end
    object QrFrmItsEmisStatus: TSmallintField
      FieldName = 'EmisStatus'
    end
    object QrFrmItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrmItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFrmItsCO_FORMULA: TIntegerField
      FieldName = 'CO_FORMULA'
    end
    object QrFrmItsDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
    object QrFrmItsExtenDd: TIntegerField
      FieldName = 'ExtenDd'
    end
  end
  object DsFrmIts: TDataSource
    DataSet = QrFrmIts
    Left = 84
    Top = 200
  end
  object QrFrmFil: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFrmFilBeforeClose
    AfterScroll = QrFrmFilAfterScroll
    SQL.Strings = (
      'SELECT'
      'IF(xxx.EmisUltDta < SYSDATE() , "Sim", "N'#227'o") PelaEmisUltDta,'
      'IF(cab.DtaExeFim > "1900-01-01", "Sim", "N'#227'o") PelaDtaExeFim,'
      'IF(cab.FatoGeradr=20, "Sim", "N'#227'o") PeloFatoGeradr20,'
      'IF(cab.PosGerou, "Sim", "N'#227'o") PeloPosGerou0,'
      'IF('
      '          cab.NumContrat=0'
      '          AND'
      '          xxx.EmisStatus=0'
      ', "Sim (A)",'
      'IF('
      '('
      '     ('
      '          cab.NumContrat<>0'
      '          AND'
      '          ctr.DtaCntrFim < "1900-01-01"'
      '          AND'
      '          xxx.EmisStatus < 2'
      '     )'
      '), "Sim (C)", "N'#227'o") ) PelaFormula,'
      'cab.Empresa,'
      'xxx.Codigo, xxx.Controle, xxx.Conta, xxx.DdPostero,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,'
      
        ' ELT(xxx.EmisStatus + 1,"Aberto","Parcial","Total") NO_EmisStatu' +
        's,'
      
        'stc.Nome NO_STC, frm.Codigo CO_FORMULA, frm.Nome NO_FORMULA, cab' +
        '.Entidade,'
      'cab.SiapTerCad, cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw,'
      'xxx.EmisUltDta, xxx.Periodd, xxx.EmisStatus,'
      '1 Tabela,'
      '"APL" SIGLA_TAB,'
      'xxx.IDIts IDSuperior'
      'FROM osfrmflhcb xxx'
      'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo'
      'LEFT JOIN formulas frm ON frm.Codigo=xxx.Formula'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN SiapTerCad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat'
      'LEFT JOIN cunscad cun ON cun.Codigo=cab.Entidade'
      'WHERE xxx.Ativo > 0'
      'AND xxx.EmisUltDta < SYSDATE()'
      'AND cab.DtaExeFim > "1900-01-01"'
      'AND cab.FatoGeradr=20'
      'AND cab.PosGerou=0'
      'AND'
      '('
      '     ('
      '          cab.NumContrat=0'
      '          AND'
      '          xxx.EmisStatus=0'
      '     )'
      '     OR'
      '     ('
      '          cab.NumContrat<>0'
      '          AND'
      '          ctr.DtaCntrFim < "1900-01-01"'
      '          AND'
      '          xxx.EmisStatus < 2'
      '     )'
      ')'
      ''
      ''
      'UNION'
      ''
      ''
      'SELECT'
      'IF(xxx.EmisUltDta < SYSDATE() , "Sim", "N'#227'o") PelaEmisUltDta,'
      'IF(cab.DtaExeFim > "1900-01-01", "Sim", "N'#227'o") PelaDtaExeFim,'
      'IF(cab.FatoGeradr=20, "Sim", "N'#227'o") PeloFatoGeradr20,'
      'IF(cab.PosGerou, "Sim", "N'#227'o") PeloPosGerou0,'
      'IF('
      '          cab.NumContrat=0'
      '          AND'
      '          xxx.EmisStatus=0'
      ', "Sim (A)",'
      'IF('
      '('
      '          cab.NumContrat<>0'
      '          AND'
      '          ctr.DtaCntrFim < "1900-01-01"'
      '          AND'
      '          xxx.EmisStatus < 2'
      '), "Sim (C)", "N'#227'o")) PelaFormula,'
      'cab.Empresa,'
      'xxx.Codigo, xxx.Controle, xxx.Conta, xxx.DdPostero,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,'
      
        ' ELT(xxx.EmisStatus + 1,"Aberto","Parcial","Total") NO_EmisStatu' +
        's,'
      
        'stc.Nome NO_STC, frm.Codigo CO_FORMULA, frm.Nome NO_FORMULA, cab' +
        '.Entidade,'
      'cab.SiapTerCad, cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw,'
      'xxx.EmisUltDta, xxx.Periodd, xxx.EmisStatus,'
      '2 Tabela,'
      '"PIP" SIGLA_TAB,'
      'xxx.Conta IDSuperior'
      'FROM osmoncab xxx'
      'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo'
      'LEFT JOIN formulas frm ON frm.Codigo=xxx.Formula'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN SiapTerCad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat'
      'LEFT JOIN cunscad cun ON cun.Codigo=cab.Entidade'
      'WHERE xxx.Ativo > 0'
      'AND xxx.EmisUltDta < SYSDATE()'
      'AND cab.DtaExeFim > "1900-01-01"'
      'AND cab.FatoGeradr=20'
      'AND cab.PosGerou=0'
      'AND'
      '('
      '     ('
      '          cab.NumContrat=0'
      '          AND'
      '          xxx.EmisStatus=0'
      '     )'
      '     OR'
      '     ('
      '          cab.NumContrat<>0'
      '          AND'
      '          ctr.DtaCntrFim < "1900-01-01"'
      '          AND'
      '          xxx.EmisStatus < 2'
      '     )'
      ')'
      ''
      ''
      ''
      'ORDER BY NO_CLIENTE, SIGLA_TAB'
      '')
    Left = 20
    Top = 152
    object QrFrmFilNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrFrmFilNO_STC: TWideStringField
      FieldName = 'NO_STC'
      Size = 100
    end
    object QrFrmFilEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrFrmFilSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrFrmFilDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrFrmFilNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrFrmFilDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
    end
    object QrFrmFilTabela: TLargeintField
      FieldName = 'Tabela'
      Required = True
    end
    object QrFrmFilSIGLA_TAB: TWideStringField
      FieldName = 'SIGLA_TAB'
      Size = 3
    end
    object QrFrmFilCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrmFilEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFrmFilPelaEmisUltDta: TWideStringField
      FieldName = 'PelaEmisUltDta'
      Required = True
      Size = 3
    end
    object QrFrmFilPelaDtaExeFim: TWideStringField
      FieldName = 'PelaDtaExeFim'
      Required = True
      Size = 3
    end
    object QrFrmFilPeloFatoGeradr20: TWideStringField
      FieldName = 'PeloFatoGeradr20'
      Required = True
      Size = 3
    end
    object QrFrmFilPeloPosGerou0: TWideStringField
      FieldName = 'PeloPosGerou0'
      Required = True
      Size = 3
    end
    object QrFrmFilPelaFormula: TWideStringField
      FieldName = 'PelaFormula'
      Required = True
      Size = 7
    end
    object QrFrmFilGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrFrmFilOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrFrmFilPosGerou: TSmallintField
      FieldName = 'PosGerou'
    end
  end
  object DsFrmFil: TDataSource
    DataSet = QrFrmFil
    Left = 20
    Top = 200
  end
  object PMAcao: TPopupMenu
    OnPopup = PMAcaoPopup
    Left = 176
    Top = 636
    object GerouSim1: TMenuItem
      Caption = 'Gerou = Sim'
      OnClick = GerouSim1Click
    end
    object GerouNao1: TMenuItem
      Caption = 'Gerou = N'#227'o'
      OnClick = GerouNao1Click
    end
  end
end
