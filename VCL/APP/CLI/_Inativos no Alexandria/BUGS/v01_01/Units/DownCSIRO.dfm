object FmDownCSIRO: TFmDownCSIRO
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Download CSIRO Systematic Names'
  ClientHeight = 626
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 447
        Height = 32
        Caption = 'Download CSIRO Systematic Names'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 447
        Height = 32
        Caption = 'Download CSIRO Systematic Names'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 447
        Height = 32
        Caption = 'Download CSIRO Systematic Names'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 500
    Width = 784
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label3: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PBScan: TProgressBar
        Left = 0
        Top = 22
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 556
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 784
    Height = 452
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = 'Download'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 424
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 380
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 0
            Top = 0
            Width = 264
            Height = 16
            Align = alTop
            Alignment = taCenter
            Caption = 'Lista de erros no download e importa'#231#227'o:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Panel16: TPanel
            Left = 0
            Top = 279
            Width = 776
            Height = 101
            Align = alBottom
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object LaAviso1: TLabel
              Left = 8
              Top = 4
              Width = 41
              Height = 13
              Caption = '..........'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaAviso2: TLabel
              Left = 8
              Top = 44
              Width = 41
              Height = 13
              Caption = '..........'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object PB1: TProgressBar
              Left = 8
              Top = 20
              Width = 760
              Height = 17
              TabOrder = 0
            end
            object StatusBar: TStatusBar
              Left = 0
              Top = 82
              Width = 776
              Height = 19
              Panels = <
                item
                  Text = ' Posi'#231#227'o do cursor no texto gerado:'
                  Width = 192
                end
                item
                  Width = 100
                end
                item
                  Text = ' Arquivo salvo:'
                  Width = 96
                end
                item
                  Width = 50
                end>
            end
            object PB2: TProgressBar
              Left = 8
              Top = 60
              Width = 760
              Height = 17
              TabOrder = 2
            end
          end
          object Memo1: TMemo
            Left = 0
            Top = 16
            Width = 591
            Height = 263
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
          object Panel5: TPanel
            Left = 591
            Top = 16
            Width = 185
            Height = 263
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 2
            object Memo2: TMemo
              Left = 0
              Top = 0
              Width = 185
              Height = 263
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object Panel2: TPanel
          Left = 0
          Top = 376
          Width = 776
          Height = 48
          Align = alBottom
          TabOrder = 1
          object Label5: TLabel
            Left = 128
            Top = 8
            Width = 69
            Height = 13
            Caption = #218'ltimo registro:'
          end
          object CkForcaCad: TCheckBox
            Left = 260
            Top = 16
            Width = 381
            Height = 17
            Caption = 
              'For'#231'a o recadastramento dos registros mesmo que j'#225' estejam cadas' +
              'trados.'
            TabOrder = 0
          end
          object BtOK: TBitBtn
            Tag = 14
            Left = 20
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Pesquisa'
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtOKClick
          end
          object EdMaxReg: TdmkEdit
            Left = 128
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '4350'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 4350
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Carregar'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 424
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel20: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 93
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label6: TLabel
            Left = 4
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Diret'#243'rio:'
          end
          object SpeedButton1: TSpeedButton
            Left = 436
            Top = 20
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object Label8: TLabel
            Left = 192
            Top = 48
            Width = 62
            Height = 13
            Caption = 'Escaneados:'
          end
          object Label9: TLabel
            Left = 276
            Top = 48
            Width = 48
            Height = 13
            Caption = 'Presen'#231'a:'
          end
          object Label10: TLabel
            Left = 360
            Top = 48
            Width = 52
            Height = 13
            Caption = 'Incid'#234'ncia:'
          end
          object EdDiretorio: TdmkEdit
            Left = 4
            Top = 20
            Width = 429
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'C:\Dermatek\Taxonomia\Imagens\CSIRO\'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'C:\Dermatek\Taxonomia\Imagens\CSIRO\'
          end
          object Panel21: TPanel
            Left = 657
            Top = 1
            Width = 118
            Height = 91
            Align = alRight
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object StaticText3: TStaticText
              Left = 0
              Top = 0
              Width = 118
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 'Extens'#245'es de arquivos:'
              TabOrder = 0
              ExplicitWidth = 114
            end
            object MeExtensoes: TMemo
              Left = 0
              Top = 17
              Width = 118
              Height = 74
              Align = alClient
              Lines.Strings = (
                '*.htm'
                '*.html')
              TabOrder = 1
              WantReturns = False
            end
          end
          object BtArquivos: TBitBtn
            Tag = 28
            Left = 4
            Top = 48
            Width = 90
            Height = 40
            Caption = '&Arquivos'
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BtArquivosClick
          end
          object CkSub1: TCheckBox
            Left = 464
            Top = 22
            Width = 117
            Height = 17
            Caption = 'Incluir subdiret'#243'rios.'
            Checked = True
            State = cbChecked
            TabOrder = 3
          end
          object BtPesquisa: TBitBtn
            Tag = 18
            Left = 99
            Top = 48
            Width = 90
            Height = 40
            Caption = '&Pesquisa'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BtPesquisaClick
          end
          object Ed1Scaned: TdmkEdit
            Left = 192
            Top = 64
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object Ed1Present: TdmkEdit
            Left = 276
            Top = 64
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object Ed1Incident: TdmkEdit
            Left = 360
            Top = 64
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object BtCarregar: TBitBtn
            Tag = 14
            Left = 447
            Top = 48
            Width = 90
            Height = 40
            Caption = '&Carregar'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 8
            OnClick = BtCarregarClick
          end
        end
        object PageControl2: TPageControl
          Left = 0
          Top = 93
          Width = 776
          Height = 331
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 1
          object TabSheet3: TTabSheet
            Caption = 'Lista de Arquivos'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object ListBox1: TListBox
              Left = 0
              Top = 0
              Width = 768
              Height = 303
              Align = alClient
              ItemHeight = 13
              TabOrder = 0
              OnDblClick = ListBox1DblClick
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'Pesquisa de texto'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 768
              Height = 25
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Label4: TLabel
                Left = 4
                Top = 4
                Width = 30
                Height = 13
                Caption = 'Texto:'
              end
              object EdTexto: TdmkEdit
                Left = 40
                Top = 0
                Width = 621
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '<td width="57%" rowspan="10"><img src=../images'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '<td width="57%" rowspan="10"><img src=../images'
              end
              object CkCaseSensitive: TCheckBox
                Left = 668
                Top = 2
                Width = 97
                Height = 19
                Caption = 'Case sensitive.'
                TabOrder = 1
              end
            end
            object Panel9: TPanel
              Left = 0
              Top = 25
              Width = 768
              Height = 128
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object GradeA: TStringGrid
                Left = 185
                Top = 0
                Width = 583
                Height = 128
                Align = alClient
                ColCount = 3
                DefaultColWidth = 40
                DefaultRowHeight = 17
                RowCount = 2
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                TabOrder = 0
                ColWidths = (
                  40
                  61
                  453)
              end
              object Panel19: TPanel
                Left = 0
                Top = 0
                Width = 185
                Height = 128
                Align = alLeft
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object StaticText2: TStaticText
                  Left = 0
                  Top = 0
                  Width = 58
                  Height = 17
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Excess'#245'es:'
                  TabOrder = 0
                end
                object MeExcl: TMemo
                  Left = 0
                  Top = 17
                  Width = 185
                  Height = 111
                  Align = alLeft
                  TabOrder = 1
                  WantReturns = False
                end
              end
            end
            object Panel10: TPanel
              Left = 0
              Top = 153
              Width = 768
              Height = 20
              Align = alTop
              ParentBackground = False
              TabOrder = 2
              object Label7: TLabel
                Left = 4
                Top = 4
                Width = 9
                Height = 13
                Caption = '...'
              end
            end
            object ListBox3: TListBox
              Left = 647
              Top = 173
              Width = 121
              Height = 130
              Align = alRight
              ItemHeight = 13
              TabOrder = 3
            end
            object GradeB: TStringGrid
              Left = 185
              Top = 173
              Width = 462
              Height = 130
              Align = alClient
              DefaultColWidth = 40
              DefaultRowHeight = 17
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
              TabOrder = 4
              ColWidths = (
                40
                329
                69
                526
                404)
            end
            object Me1Txt: TMemo
              Left = 0
              Top = 173
              Width = 185
              Height = 130
              Align = alLeft
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
            end
          end
          object TabSheet5: TTabSheet
            Caption = 'Abre HTM sem Tags'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel11: TPanel
              Left = 0
              Top = 0
              Width = 768
              Height = 303
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object Panel12: TPanel
                Left = 1
                Top = 1
                Width = 766
                Height = 28
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Sb_1_2_Abre: TSpeedButton
                  Left = 484
                  Top = 2
                  Width = 41
                  Height = 22
                  Caption = 'Abre'
                  OnClick = Sb_1_2_AbreClick
                end
                object Ed_1_2_Arq: TdmkEdit
                  Left = 4
                  Top = 4
                  Width = 429
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
              end
              object Me_1_2_Txt: TMemo
                Left = 1
                Top = 29
                Width = 766
                Height = 273
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
              end
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Dados'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Grade_1_3_A: TStringGrid
              Left = 0
              Top = 0
              Width = 768
              Height = 303
              Align = alClient
              ColCount = 25
              DefaultColWidth = 40
              DefaultRowHeight = 17
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
              PopupMenu = PM_1_3_A
              TabOrder = 0
              ColWidths = (
                40
                329
                69
                526
                404
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40
                40)
            end
          end
          object TabSheet7: TTabSheet
            Caption = 'An'#225'lise'
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Me_1_4_Txt: TMemo
              Left = 0
              Top = 28
              Width = 768
              Height = 275
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 768
              Height = 28
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Sb_1_4_Lista: TSpeedButton
                Left = 716
                Top = 2
                Width = 41
                Height = 22
                Caption = 'Lista'
                OnClick = Sb_1_4_ListaClick
              end
              object Label11: TLabel
                Left = 4
                Top = 8
                Width = 65
                Height = 13
                Caption = 'Coluna inicial:'
              end
              object Label12: TLabel
                Left = 120
                Top = 8
                Width = 113
                Height = 13
                Caption = 'Quantidade de colunas:'
              end
              object Label13: TLabel
                Left = 280
                Top = 8
                Width = 100
                Height = 13
                Caption = 'Quando diferente de:'
              end
              object Ed_1_4_Col: TdmkEdit
                Left = 76
                Top = 4
                Width = 41
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object Ed_1_4_Cols: TdmkEdit
                Left = 236
                Top = 3
                Width = 41
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
              end
              object Ed_1_4_Txt: TdmkEdit
                Left = 384
                Top = 3
                Width = 329
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
            end
          end
        end
      end
    end
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProtocolVersion = pv1_0
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 156
    Top = 124
  end
  object QrVersao: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Versao FROM spedefdtabt ')
    Left = 348
    Top = 200
    object QrVersaoVersao: TWideStringField
      FieldName = 'Versao'
    end
  end
  object QrDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodTxt'
      'FROM tbspedefd001'
      'WHERE CodTxt="1"')
    Left = 532
    Top = 16
    object QrDuplCodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
  end
  object QrSorc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 676
    Top = 64
  end
  object QrDest: TmySQLQuery
    Database = Dmod.MyDB
    Left = 704
    Top = 64
  end
  object PM_1_3_A: TPopupMenu
    Left = 232
    Top = 320
    object Largurasdagrade1: TMenuItem
      Caption = '&Larguras da grade'
      OnClick = Largurasdagrade1Click
    end
  end
end
