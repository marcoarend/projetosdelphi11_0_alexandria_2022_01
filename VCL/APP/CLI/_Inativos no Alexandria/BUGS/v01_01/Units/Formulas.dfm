object FmFormulas: TFmFormulas
  Left = 368
  Top = 194
  Caption = 'FRM-APLIC-001 :: F'#243'rmulas Bases'
  ClientHeight = 802
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnEdita: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 684
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEditaA: TGroupBox
      Left = 0
      Top = 0
      Width = 1241
      Height = 302
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 20
        Top = 20
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 94
        Top = 20
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 773
        Top = 118
        Width = 101
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Quantidade total:'
      end
      object Label3: TLabel
        Left = 20
        Top = 69
        Width = 143
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Equipamento aplicador:'
      end
      object SBEquip: TSpeedButton
        Left = 847
        Top = 89
        Width = 26
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SBEquipClick
      end
      object Label8: TLabel
        Left = 20
        Top = 118
        Width = 113
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo de aplica'#231#227'o:'
      end
      object SBTipoAplica: TSpeedButton
        Left = 345
        Top = 138
        Width = 25
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SBTipoAplicaClick
      end
      object Label11: TLabel
        Left = 374
        Top = 118
        Width = 149
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Unidade de Medida [F3]:'
      end
      object SpeedButton5: TSpeedButton
        Left = 743
        Top = 138
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 94
        Top = 39
        Width = 779
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CGAplicacao: TdmkCheckGroup
        Left = 20
        Top = 236
        Width = 853
        Height = 56
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Aplicabilidade: '
        Columns = 2
        Items.Strings = (
          'Aplica'#231#227'o'
          'Monitoramento')
        TabOrder = 11
        QryCampo = 'Aplicacao'
        UpdCampo = 'Aplicacao'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object EdQtdTot: TdmkEdit
        Left = 773
        Top = 138
        Width = 100
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'QtdTot'
        UpdCampo = 'QtdTot'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdEquipAplic: TdmkEditCB
        Left = 20
        Top = 89
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EquipAplic'
        UpdCampo = 'EquipAplic'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEquipAplic
        IgnoraDBLookupComboBox = False
      end
      object CBEquipAplic: TdmkDBLookupComboBox
        Left = 94
        Top = 89
        Width = 750
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Controle'
        ListField = 'Nome'
        ListSource = DsEquipamentos
        TabOrder = 3
        dmkEditCB = EdEquipAplic
        QryCampo = 'EquipAplic'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGDiluente: TdmkRadioGroup
        Left = 20
        Top = 169
        Width = 853
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Diluente: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'N'#227'o tem'
          #193'gua'
          'Produto indicado')
        TabOrder = 10
        QryCampo = 'Diluente'
        UpdCampo = 'Diluente'
        UpdType = utYes
        OldValor = 0
      end
      object EdTipoAplica: TdmkEditCB
        Left = 20
        Top = 138
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TipoAplica'
        UpdCampo = 'TipoAplica'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTipoAplica
        IgnoraDBLookupComboBox = False
      end
      object CBTipoAplica: TdmkDBLookupComboBox
        Left = 89
        Top = 138
        Width = 252
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTipoAplica
        TabOrder = 5
        dmkEditCB = EdTipoAplica
        QryCampo = 'TipoAplica'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdUnidMed: TdmkEditCB
        Left = 374
        Top = 138
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdUnidMedChange
        DBLookupComboBox = CBUnidMed
        IgnoraDBLookupComboBox = False
      end
      object EdSigla: TdmkEdit
        Left = 443
        Top = 138
        Width = 49
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdSiglaChange
        OnExit = EdSiglaExit
      end
      object CBUnidMed: TdmkDBLookupComboBox
        Left = 492
        Top = 138
        Width = 248
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsUnidMed
        TabOrder = 8
        dmkEditCB = EdUnidMed
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 607
      Width = 1241
      Height = 77
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1059
        Top = 18
        Width = 180
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 15
          Top = 2
          Width = 147
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 684
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1241
      Height = 287
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 18
        Width = 880
        Height = 267
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label1: TLabel
          Left = 15
          Top = 5
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 89
          Top = 5
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label10: TLabel
          Left = 15
          Top = 54
          Width = 143
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Equipamento aplicador:'
        end
        object Label4: TLabel
          Left = 15
          Top = 103
          Width = 113
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tipo de aplica'#231#227'o:'
        end
        object Label12: TLabel
          Left = 369
          Top = 103
          Width = 149
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Unidade de Medida [F3]:'
        end
        object Label5: TLabel
          Left = 753
          Top = 103
          Width = 101
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Quantidade total:'
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 15
          Top = 25
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsFormulas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 89
          Top = 25
          Width = 777
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsFormulas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit4: TDBEdit
          Left = 15
          Top = 74
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'EquipAplic'
          DataSource = DsFormulas
          TabOrder = 2
        end
        object DBEdit5: TDBEdit
          Left = 84
          Top = 74
          Width = 784
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NO_EQUIAPLIC'
          DataSource = DsFormulas
          TabOrder = 3
        end
        object DBEdit2: TDBEdit
          Left = 15
          Top = 123
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'TipoAplica'
          DataSource = DsFormulas
          TabOrder = 4
        end
        object DBEdit1: TDBEdit
          Left = 84
          Top = 123
          Width = 282
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NO_TipoAplica'
          DataSource = DsFormulas
          TabOrder = 5
        end
        object DBEdit6: TDBEdit
          Left = 369
          Top = 123
          Width = 70
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CU_UNIDMED'
          DataSource = DsFormulas
          TabOrder = 6
        end
        object DBEdit7: TDBEdit
          Left = 438
          Top = 123
          Width = 51
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'SIGLA'
          DataSource = DsFormulas
          TabOrder = 7
        end
        object DBEdit8: TDBEdit
          Left = 487
          Top = 123
          Width = 263
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NO_UNIDMED'
          DataSource = DsFormulas
          TabOrder = 8
        end
        object DBEdit3: TDBEdit
          Left = 753
          Top = 123
          Width = 113
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'QtdTot'
          DataSource = DsFormulas
          TabOrder = 9
        end
        object DBRadioGroup1: TDBRadioGroup
          Left = 15
          Top = 158
          Width = 851
          Height = 45
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Diluente: '
          Columns = 4
          DataField = 'Diluente'
          DataSource = DsFormulas
          Items.Strings = (
            'Indefinido'
            'N'#227'o tem'
            #193'gua'
            'Produto indicado')
          ParentBackground = True
          TabOrder = 10
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
        object dmkCheckGroup1: TdmkDBCheckGroup
          Left = 15
          Top = 207
          Width = 851
          Height = 54
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Aplicabilidade: '
          Columns = 2
          DataField = 'Aplicacao'
          DataSource = DsFormulas
          Items.Strings = (
            'Aplica'#231#227'o'
            'Monitoramento')
          ParentBackground = False
          TabOrder = 11
        end
      end
      object Panel8: TPanel
        Left = 882
        Top = 18
        Width = 357
        Height = 267
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object PageControl1: TPageControl
          Left = 0
          Top = 0
          Width = 357
          Height = 267
          ActivePage = TabSheet2
          Align = alClient
          TabHeight = 25
          TabOrder = 0
          object TabSheet1: TTabSheet
            Caption = 'Intervalos'
            object DBGrid2: TDBGrid
              Left = 0
              Top = 133
              Width = 349
              Height = 99
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsFormulFiDd
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -15
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Title.Caption = 'Sequencia'
                  Width = 57
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Dias'
                  Title.Caption = 'Dias de intervalo'
                  Width = 85
                  Visible = True
                end>
            end
            object DBGrid3: TDBGrid
              Left = 0
              Top = 0
              Width = 349
              Height = 133
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              DataSource = DsFormulFiCb
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -15
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Periodd'
                  Title.Caption = 'Per'#237'odo'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_FORMULA'
                  Title.Caption = 'F'#243'rmula'
                  Title.Color = clBlack
                  Width = 400
                  Visible = True
                end>
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'EPIs'
            ImageIndex = 1
            object Panel47: TPanel
              Left = 0
              Top = 0
              Width = 349
              Height = 63
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object BtFormulEPI: TBitBtn
                Tag = 237
                Left = 5
                Top = 5
                Width = 53
                Height = 53
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtFormulEPIClick
              end
            end
            object DBGFormulEPI: TDBGrid
              Left = 0
              Top = 63
              Width = 349
              Height = 169
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Color = clWhite
              DataSource = DsFormulEPI
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -15
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'C'#243'd '
                  Width = 73
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o do Produto'
                  Width = 220
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 606
      Width = 1241
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 122
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 336
        Top = 18
        Width = 903
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 740
          Top = 0
          Width = 163
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 178
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&F'#243'rmula'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 10109
          Left = 153
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtAbr: TBitBtn
          Tag = 10108
          Left = 300
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Abrang'#234'ncia'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtAbrClick
        end
        object BtFormulFiCb: TBitBtn
          Tag = 178
          Left = 448
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Fil&has'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtFormulFiCbClick
        end
        object BtFormulFiDd: TBitBtn
          Tag = 10078
          Left = 596
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Inter&valos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtFormulFiDdClick
        end
      end
    end
    object PnGrades: TPanel
      Left = 0
      Top = 287
      Width = 1241
      Height = 192
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 192
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Itens da f'#243'rmula: '
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 2
          Top = 18
          Width = 961
          Height = 172
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Color = clWhite
          DataSource = DsFormulIF
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Title.Caption = 'Seq.'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIGLAUNIDMED'
              Title.Caption = 'Unidade'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PrvQtd'
              Title.Alignment = taRightJustify
              Title.Caption = 'Qtd de uso'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Alignment = taRightJustify
              Title.Caption = 'C'#243'd [F4]'
              Width = 73
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GG1'
              Title.Caption = 'Descri'#231#227'o do Produto [F7]'
              Width = 459
              Visible = True
            end>
        end
        object GroupBox17: TGroupBox
          Left = 963
          Top = 18
          Width = 276
          Height = 172
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Caption = ' Abrang'#234'ncia: '
          TabOrder = 1
          object DBGOSFrmAbr: TDBGrid
            Left = 2
            Top = 18
            Width = 272
            Height = 152
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsFormulIA
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_ABRANGE'
                Title.Caption = 'ABRANG'#202'NCIA'
                Width = 180
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 916
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 233
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'F'#243'rmulas Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 233
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'F'#243'rmulas Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 233
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'F'#243'rmulas Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrFormulas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFormulasBeforeOpen
    AfterOpen = QrFormulasAfterOpen
    BeforeClose = QrFormulasBeforeClose
    AfterScroll = QrFormulasAfterScroll
    SQL.Strings = (
      'SELECT tpa.Nome NO_TipoAplica,'
      'gg1.Nome NO_EQUIAPLIC, med.CodUsu CU_UNIDMED, '
      'med.SIGLA, med.Nome NO_UNIDMED, '
      'ffi.Nome NO_FormFilha, frm.* '
      'FROM formulas frm'
      'LEFT JOIN tipoaplica tpa ON tpa.Codigo = frm.TipoAplica'
      'LEFT JOIN gragrux ggx ON ggx.Controle = frm.EquipAplic'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed med ON med.Codigo=frm.UnidMed'
      'LEFT JOIN formulas ffi ON ffi.Codigo=frm.FormFilha'
      'WHERE frm.Codigo > 0'
      '')
    Left = 64
    Top = 65
    object QrFormulasNO_EQUIAPLIC: TWideStringField
      FieldName = 'NO_EQUIAPLIC'
      Size = 120
    end
    object QrFormulasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFormulasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFormulasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulasEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrFormulasQtdTot: TFloatField
      FieldName = 'QtdTot'
      DisplayFormat = '#,###,##0.000'
    end
    object QrFormulasQtdQSP: TFloatField
      FieldName = 'QtdQSP'
    end
    object QrFormulasCusTot: TFloatField
      FieldName = 'CusTot'
    end
    object QrFormulasAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrFormulasDiluente: TIntegerField
      FieldName = 'Diluente'
    end
    object QrFormulasNO_TipoAplica: TWideStringField
      FieldName = 'NO_TipoAplica'
      Size = 60
    end
    object QrFormulasTipoAplica: TIntegerField
      FieldName = 'TipoAplica'
    end
    object QrFormulasUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrFormulasSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrFormulasNO_UNIDMED: TWideStringField
      FieldName = 'NO_UNIDMED'
      Size = 30
    end
    object QrFormulasCU_UNIDMED: TIntegerField
      FieldName = 'CU_UNIDMED'
      Required = True
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 92
    Top = 65
  end
  object QrFormulIF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT med.Sigla SIGLAUNIDMED, gg1.Nome NO_GG1, ofr.*'
      'FROM formulif ofr'
      'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX'
      'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed med on med.Codigo=gg1.UnidMed'
      'WHERE ofr.Codigo=:P0'
      'ORDER BY Ordem')
    Left = 148
    Top = 65
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsFormulIF: TDataSource
    DataSet = QrFormulIF
    Left = 176
    Top = 65
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 412
    Top = 532
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrFormulIA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT abr.Nome NO_ABRANGE, ofa.*'
      'FROM formulia ofa'
      'LEFT JOIN abrangicie abr ON abr.Codigo=ofa.Abrangicie'
      'WHERE ofa.Codigo=:P0')
    Left = 204
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsFormulIA: TDataSource
    DataSet = QrFormulIA
    Left = 232
    Top = 64
  end
  object QrEquipamentos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Nome, gg1.Nivel1'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg2.Nivel3'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg3.PrdGrupTip')
    Left = 520
    Top = 12
    object QrEquipamentosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEquipamentosNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrEquipamentosNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object DsEquipamentos: TDataSource
    DataSet = QrEquipamentos
    Left = 548
    Top = 12
  end
  object PMEquip: TPopupMenu
    Left = 560
    Top = 468
    object Aplicao1: TMenuItem
      Caption = '&Aplica'#231#227'o'
      OnClick = Aplicao1Click
    end
    object Monitoramento1: TMenuItem
      Caption = '&Monitoramento'
      OnClick = Monitoramento1Click
    end
  end
  object QrTipoAplica: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tipoaplica'
      'ORDER BY Nome')
    Left = 520
    Top = 40
    object QrTipoAplicaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTipoAplicaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTipoAplica: TDataSource
    DataSet = QrTipoAplica
    Left = 548
    Top = 40
  end
  object QrUnidMed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 264
    Top = 64
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 292
    Top = 64
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdUnidMed
    Panel = PnEdita
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 320
    Top = 64
  end
  object PMFormulFiCb: TPopupMenu
    OnPopup = PMFormulFiCbPopup
    Left = 692
    Top = 556
    object Incluifrmulafilha1: TMenuItem
      Caption = '&Inclui f'#243'rmula filha'
      OnClick = Incluifrmulafilha1Click
    end
    object Alterafrmulafilha1: TMenuItem
      Caption = '&Altera f'#243'rmula filha'
      OnClick = Alterafrmulafilha1Click
    end
    object Excluifrmulafilha1: TMenuItem
      Caption = '&Exclui f'#243'rmula filha'
      OnClick = Excluifrmulafilha1Click
    end
  end
  object QrFormulFiCb: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFormulFiCbBeforeClose
    AfterScroll = QrFormulFiCbAfterScroll
    SQL.Strings = (
      'SELECT ffc.*, frm.Nome NO_FORMULA'
      'FROM formulficb ffc'
      'LEFT JOIN formulas frm ON frm.Codigo=ffc.Formula'
      'WHERE ffc.Codigo=:P0')
    Left = 352
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFormulFiCbCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulFiCbControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFormulFiCbFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrFormulFiCbPeriodd: TIntegerField
      FieldName = 'Periodd'
    end
    object QrFormulFiCbLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulFiCbDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulFiCbDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulFiCbUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulFiCbUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulFiCbAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFormulFiCbAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulFiCbNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrFormulFiCbDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
  end
  object DsFormulFiCb: TDataSource
    DataSet = QrFormulFiCb
    Left = 380
    Top = 64
  end
  object QrFormulFiDd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ffd.* '
      'FROM formulfidd ffd '
      'WHERE ffd.Controle=0'
      'ORDER BY Ordem, Controle ')
    Left = 408
    Top = 64
    object QrFormulFiDdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulFiDdControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFormulFiDdConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFormulFiDdOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFormulFiDdDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrFormulFiDdLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulFiDdDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulFiDdDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulFiDdUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulFiDdUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulFiDdAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFormulFiDdAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFormulFiDd: TDataSource
    DataSet = QrFormulFiDd
    Left = 436
    Top = 64
  end
  object PMFormulFiDd: TPopupMenu
    OnPopup = PMFormulFiDdPopup
    Left = 800
    Top = 556
    object Incluiintervalo1: TMenuItem
      Caption = '&Inclui intervalo'
      OnClick = Incluiintervalo1Click
    end
    object Alteraintervalo1: TMenuItem
      Caption = '&Altera intervalo'
      OnClick = Alteraintervalo1Click
    end
    object Excluiintervalo1: TMenuItem
      Caption = '&Exclui intervalo'
      OnClick = Excluiintervalo1Click
    end
  end
  object PMFormulEPI: TPopupMenu
    OnPopup = PMFormulEPIPopup
    Left = 944
    Top = 320
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Cadatrodeprodutos1: TMenuItem
      Caption = '&Cadatro de produtos'
      OnClick = Cadatrodeprodutos1Click
    end
  end
  object QrFormulEPI: TmySQLQuery
    Database = Dmod.MyDB
    Left = 696
    Top = 76
    object IntegerField1: TIntegerField
      FieldName = 'Controle'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object IntegerField2: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrFormulEPIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DsFormulEPI: TDataSource
    DataSet = QrFormulEPI
    Left = 724
    Top = 76
  end
end
