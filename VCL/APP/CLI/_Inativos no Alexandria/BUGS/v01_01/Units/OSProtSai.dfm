object FmOSProtSai: TFmOSProtSai
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-035 :: Protocolo de Sa'#237'da de OSs'
  ClientHeight = 494
  ClientWidth = 724
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 724
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 676
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 628
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 328
        Height = 32
        Caption = 'Protocolo de Sa'#237'da de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 328
        Height = 32
        Caption = 'Protocolo de Sa'#237'da de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 328
        Height = 32
        Caption = 'Protocolo de Sa'#237'da de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 724
    Height = 320
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 724
      Height = 320
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 724
        Height = 320
        Align = alClient
        TabOrder = 0
        object DBGOSsStat: TDBGrid
          Left = 2
          Top = 65
          Width = 720
          Height = 253
          Align = alClient
          DataSource = DsOSProtSai
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          Columns = <
            item
              Expanded = False
              FieldName = 'ID_Cod1'
              Title.Caption = 'Localizador'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Cod2'
              Title.Caption = 'Status'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ESTATUS'
              Title.Caption = 'Descri'#231#227'o Status'
              Width = 180
              Visible = True
            end>
        end
        object PnEmpresa: TPanel
          Left = 2
          Top = 15
          Width = 720
          Height = 50
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label9: TLabel
            Left = 8
            Top = 0
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 16
            Width = 53
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 63
            Top = 16
            Width = 330
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object GroupBox2: TGroupBox
            Left = 448
            Top = 0
            Width = 257
            Height = 42
            Caption = ' Per'#237'odo: '
            TabOrder = 2
            Visible = False
            object Label10: TLabel
              Left = 119
              Top = 20
              Width = 15
              Height = 13
              Caption = 'at'#233
            end
            object TPDataIni: TdmkEditDateTimePicker
              Left = 4
              Top = 16
              Width = 112
              Height = 21
              Date = 40125.821355567130000000
              Time = 40125.821355567130000000
              TabOrder = 0
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object TPDataFim: TdmkEditDateTimePicker
              Left = 140
              Top = 16
              Width = 112
              Height = 21
              Date = 40125.821355567130000000
              Time = 40125.821355567130000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 368
    Width = 724
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 720
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 720
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 724
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 578
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 576
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtOSPsq: TBitBtn
        Tag = 539
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Pesquisa'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtOSPsqClick
      end
    end
  end
  object TbOSProtSai: TmySQLTable
    Database = DModG.MyPID_DB
    BeforePost = TbOSProtSaiBeforePost
    AfterPost = TbOSProtSaiAfterPost
    AfterDelete = TbOSProtSaiAfterDelete
    AfterRefresh = TbOSProtSaiAfterRefresh
    TableName = 'osprotsai'
    Left = 44
    Top = 160
    object TbOSProtSaiID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
    end
    object TbOSProtSaiID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
    end
    object TbOSProtSaiID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
    end
    object TbOSProtSaiID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
    end
    object TbOSProtSaiID_Txt1: TWideStringField
      FieldName = 'ID_Txt1'
      Size = 100
    end
    object TbOSProtSaiID_Txt2: TWideStringField
      FieldName = 'ID_Txt2'
      Size = 100
    end
    object TbOSProtSaiID_Txt3: TWideStringField
      FieldName = 'ID_Txt3'
      Size = 100
    end
    object TbOSProtSaiID_Txt4: TWideStringField
      FieldName = 'ID_Txt4'
      Size = 100
    end
    object TbOSProtSaiNO_ESTATUS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_ESTATUS'
      LookupDataSet = QrEstatusOSs
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'ID_Cod2'
      Lookup = True
    end
    object TbOSProtSaiSession: TDateTimeField
      FieldName = 'Session'
    end
  end
  object DsOSProtSai: TDataSource
    DataSet = TbOSProtSai
    Left = 112
    Top = 160
  end
  object QrEstatusOSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM estatusoss'
      'ORDER BY Codigo')
    Left = 360
    Top = 224
    object QrEstatusOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstatusOSsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEstatusOSs: TDataSource
    DataSet = QrEstatusOSs
    Left = 360
    Top = 272
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM oscab'
      'WHERE Codigo=999999999')
    Left = 44
    Top = 212
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrProtPakIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 112
    Top = 212
    object QrProtPakItsConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 24
    Top = 12
  end
end
