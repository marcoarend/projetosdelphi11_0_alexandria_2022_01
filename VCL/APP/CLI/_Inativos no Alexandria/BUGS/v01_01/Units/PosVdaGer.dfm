object FmPosVdaGer: TFmPosVdaGer
  Left = 339
  Top = 185
  Caption = 'POS-VENDA-003 :: Gerenciamento de P'#243's Venda'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 361
        Height = 32
        Caption = 'Gerenciamento de P'#243's Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 361
        Height = 32
        Caption = 'Gerenciamento de P'#243's Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 361
        Height = 32
        Caption = 'Gerenciamento de P'#243's Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 269
          Width = 808
          Height = 3
          Cursor = crVSplit
          Align = alBottom
          ExplicitTop = 45
          ExplicitWidth = 227
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 45
          Width = 808
          Height = 224
          Align = alClient
          DataSource = DsOSPos
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'DATA_ACAO'
              Title.Caption = 'Vencimento da a'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Localizador'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Entidade'
              Title.Caption = 'Sub cliente'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENT'
              Title.Caption = 'Nome sub cliente'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CONTAT'
              Title.Caption = 'Contato'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_STC'
              Title.Caption = 'Lugar'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ACAO'
              Title.Caption = 'A'#231#227'o'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_AplicID'
              Title.Caption = 'Especificac'#227'o'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SERVICOS'
              Title.Caption = 'Servi'#231'os'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_AgeEqiCab'
              Title.Caption = 'Equipe de agentes'
              Width = 97
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Estatus'
              Title.Caption = 'Status'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_STATUS'
              Title.Caption = 'Status'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_FATOGERA'
              Title.Caption = 'Fato gerador'
              Width = 100
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object LaCliInt: TLabel
            Left = 4
            Top = 6
            Width = 70
            Height = 13
            Caption = 'Cliente interno:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 76
            Top = 2
            Width = 48
            Height = 22
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 124
            Top = 2
            Width = 508
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 272
          Width = 808
          Height = 193
          ActivePage = TabSheet1
          Align = alBottom
          TabOrder = 2
          object TabSheet1: TTabSheet
            Caption = 'Di'#225'rio'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGDiarioAdd: TDBGrid
              Left = 0
              Top = 0
              Width = 281
              Height = 165
              Align = alLeft
              DataSource = DsDiarioAdd
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Depto'
                  Title.Caption = 'Localizador'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Hora'
                  Width = 56
                  Visible = True
                end>
            end
            object DBMemo1: TDBMemo
              Left = 281
              Top = 0
              Width = 519
              Height = 165
              Align = alClient
              DataField = 'Nome'
              DataSource = DsDiarioAdd
              TabOrder = 1
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 382
        Height = 16
        Caption = 
          'Duplo clique no registro de p'#243's-venda executa a a'#231#227'o pr'#233'-definid' +
          'a!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 382
        Height = 16
        Caption = 
          'Duplo clique no registro de p'#243's-venda executa a a'#231#227'o pr'#233'-definid' +
          'a!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtReabre: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Reabrir'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtReabreClick
      end
      object BtDiario: TBitBtn
        Tag = 399
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Di'#225'rio'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtDiarioClick
      end
      object BtEncerrar: TBitBtn
        Tag = 506
        Left = 384
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Encerrar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtEncerrarClick
      end
      object BtEmail: TBitBtn
        Tag = 244
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Email'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
      end
    end
  end
  object QrOSPos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSPosBeforeClose
    AfterScroll = QrOSPosAfterScroll
    SQL.Strings = (
      'SELECT cab.Empresa, cab.Entidade, cab.SiapTerCad,  '
      'cab.DtaExeFim, cab.Estatus, cab.FatoGeradr,  '
      'cab.EntiContat, cab.MulServico, '
      'DATE_ADD(cab.DtaExeFim, INTERVAL osp.Dias DAY) DATA_ACAO,  '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,  '
      'stc.Nome NO_STC, sos.Nome NO_STATUS,  '
      'fge.Nome NOME_FATOGERA, ect.Nome NO_CONTAT,  '
      'mls.Nome NO_SERVICOS, aec.Nome NO_AgeEqiCab, '
      'osp.Codigo, osp.Controle, osp.Dias, osp.Aplicacao,  '
      'osp.AplicID, osp.Emitido, osp.Executado,  '
      'osp.ExeCodi, osp.ExeCtrl, '
      
        'ELT(osp.Aplicacao+1, "Nenhuma", "Telefonar", "Enviar email") NO_' +
        'ACAO, '
      'IF(Aplicacao=2, pre.Nome, "") NO_AplicID '
      'FROM ospos osp '
      'LEFT JOIN oscab cab ON cab.Codigo=osp.Codigo '
      'LEFT JOIN entidades ent ON ent.Codigo=cab.entidade '
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapterCad '
      'LEFT JOIN estatusoss sos ON sos.Codigo=cab.Estatus '
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr '
      'LEFT JOIN enticontat ect ON ect.Controle=cab.Enticontat '
      'LEFT JOIN mulservico mls ON mls.Codigo=cab.MulServico '
      'LEFT JOIN ageeqicab aec ON aec.Codigo=cab.AgeEqiCab '
      'LEFT JOIN preemail pre ON pre.Codigo=osp.AplicID '
      'WHERE Executado=0 '
      'AND DtaExeFim>"1900-01-01" '
      'AND DATE_ADD(cab.DtaExeFim, INTERVAL osp.Dias DAY) < NOW() '
      'ORDER BY DATA_ACAO ')
    Left = 264
    Top = 164
    object QrOSPosEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'oscab.Empresa'
    end
    object QrOSPosEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'oscab.Entidade'
    end
    object QrOSPosSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
      Origin = 'oscab.SiapTerCad'
    end
    object QrOSPosDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
      Origin = 'oscab.DtaExeFim'
    end
    object QrOSPosEstatus: TIntegerField
      FieldName = 'Estatus'
      Origin = 'oscab.Estatus'
    end
    object QrOSPosFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
      Origin = 'oscab.FatoGeradr'
    end
    object QrOSPosEntiContat: TIntegerField
      FieldName = 'EntiContat'
      Origin = 'oscab.EntiContat'
    end
    object QrOSPosMulServico: TLargeintField
      FieldName = 'MulServico'
      Origin = 'oscab.MulServico'
    end
    object QrOSPosDATA_ACAO: TDateTimeField
      FieldName = 'DATA_ACAO'
      Origin = 'DATA_ACAO'
    end
    object QrOSPosNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Origin = 'NO_ENT'
      Size = 100
    end
    object QrOSPosNO_STC: TWideStringField
      FieldName = 'NO_STC'
      Origin = 'siaptercad.Nome'
      Size = 100
    end
    object QrOSPosNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Origin = 'estatusoss.Nome'
      Size = 60
    end
    object QrOSPosNOME_FATOGERA: TWideStringField
      FieldName = 'NOME_FATOGERA'
      Origin = 'fatogeradr.Nome'
    end
    object QrOSPosNO_CONTAT: TWideStringField
      FieldName = 'NO_CONTAT'
      Origin = 'enticontat.Nome'
      Size = 30
    end
    object QrOSPosNO_SERVICOS: TWideStringField
      FieldName = 'NO_SERVICOS'
      Origin = 'mulservico.Nome'
      Size = 60
    end
    object QrOSPosNO_AgeEqiCab: TWideStringField
      FieldName = 'NO_AgeEqiCab'
      Origin = 'ageeqicab.Nome'
      Size = 255
    end
    object QrOSPosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ospos.Codigo'
    end
    object QrOSPosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'ospos.Controle'
    end
    object QrOSPosDias: TIntegerField
      FieldName = 'Dias'
      Origin = 'ospos.Dias'
    end
    object QrOSPosAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Origin = 'ospos.Aplicacao'
    end
    object QrOSPosAplicID: TIntegerField
      FieldName = 'AplicID'
      Origin = 'ospos.AplicID'
    end
    object QrOSPosEmitido: TIntegerField
      FieldName = 'Emitido'
      Origin = 'ospos.Emitido'
    end
    object QrOSPosExecutado: TIntegerField
      FieldName = 'Executado'
      Origin = 'ospos.Executado'
    end
    object QrOSPosExeCodi: TIntegerField
      FieldName = 'ExeCodi'
      Origin = 'ospos.ExeCodi'
    end
    object QrOSPosExeCtrl: TIntegerField
      FieldName = 'ExeCtrl'
      Origin = 'ospos.ExeCtrl'
    end
    object QrOSPosNO_ACAO: TWideStringField
      FieldName = 'NO_ACAO'
      Origin = 'NO_ACAO'
      Size = 12
    end
    object QrOSPosNO_AplicID: TWideStringField
      FieldName = 'NO_AplicID'
      Origin = 'NO_AplicID'
      Size = 100
    end
    object QrOSPosEntContrat: TIntegerField
      FieldName = 'EntContrat'
    end
  end
  object DsOSPos: TDataSource
    DataSet = QrOSPos
    Left = 264
    Top = 212
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 16
    Top = 16
  end
  object DsDiarioAdd: TDataSource
    DataSet = QrDiarioAdd
    Left = 352
    Top = 212
  end
  object QrDiarioAdd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Depto, Data, Hora,  '
      'Interloctr, PrintOS, '
      'ELT(PrintOS + 2, " ", "N", "S", "?") IMP, Nome'
      'FROM diarioadd '
      'WHERE Depto IN ( '
      '     SELECT Codigo '
      '     FROM oscab  '
      '     WHERE Grupo=109) '
      'ORDER BY Codigo DESC ')
    Left = 352
    Top = 164
    object QrDiarioAddCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioAddDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDiarioAddData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddHora: TTimeField
      FieldName = 'Hora'
    end
    object QrDiarioAddInterloctr: TIntegerField
      FieldName = 'Interloctr'
    end
    object QrDiarioAddPrintOS: TIntegerField
      FieldName = 'PrintOS'
    end
    object QrDiarioAddNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrDiarioAddIMP: TWideStringField
      FieldName = 'IMP'
      Size = 1
    end
  end
end
