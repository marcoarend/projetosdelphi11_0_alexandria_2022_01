unit OSGarantRnw;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables,
  Vcl.Menus;

type
  TFmOSGarantRnw = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtEncerra: TBitBtn;
    LaTitulo1C: TLabel;
    DBGGarant: TdmkDBGridZTO;
    QrGarant: TmySQLQuery;
    DsGarant: TDataSource;
    QrGarantNO_ENTICONTAT: TWideStringField;
    QrGarantNO_FatoGeradr: TWideStringField;
    QrGarantNO_SiapTerCad: TWideStringField;
    QrGarantNO_ENT: TWideStringField;
    QrGarantTel_ENT: TWideStringField;
    QrGarantNO_PAG: TWideStringField;
    QrGarantNO_CTR: TWideStringField;
    QrGarantCodigo: TIntegerField;
    QrGarantControle: TIntegerField;
    QrGarantNO_SERVICO: TWideStringField;
    QrGarantDtaExeIni: TDateTimeField;
    QrGarantGarantiaDd: TIntegerField;
    QrGarantDtGarantia: TDateTimeField;
    BtConversas: TBitBtn;
    QrGarantTabela: TWideStringField;
    QrGarantDiarGen: TFloatField;
    QrOSCab: TmySQLQuery;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEmpresa: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabDtaLibFat: TDateTimeField;
    QrOSCabDtaFimFat: TDateTimeField;
    QrOSCabValorServi: TFloatField;
    QrOSCabValorDesco: TFloatField;
    QrOSCabValorOutrs: TFloatField;
    QrOSCabValorTotal: TFloatField;
    QrOSCabDdsPosVda: TIntegerField;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    QrOSCabEntContrat: TIntegerField;
    QrOSCabCondicaoPg: TIntegerField;
    QrOSCabCartEmis: TIntegerField;
    QrOSCabSerNF: TWideStringField;
    QrOSCabNumNF: TIntegerField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabInvalServi: TFloatField;
    QrOSCabInvalDesco: TFloatField;
    QrOSCabInvalOutrs: TFloatField;
    QrOSCabInvalTotal: TFloatField;
    QrOSCabOrcamServi: TFloatField;
    QrOSCabOrcamDesco: TFloatField;
    QrOSCabOrcamOutrs: TFloatField;
    QrOSCabOrcamTotal: TFloatField;
    QrOSCabValiDdOrca: TIntegerField;
    QrOSCabOperacao: TIntegerField;
    QrOSCabExeTxtCli1: TIntegerField;
    QrOSCabValorPre: TFloatField;
    QrOSCabObsGaranti: TWideMemoField;
    QrOSCabExeTxtCli2: TIntegerField;
    QrOSCabFimVisPrv: TDateTimeField;
    QrOSCabFimVisExe: TDateTimeField;
    QrOSCabFimExePrv: TDateTimeField;
    QrOSCabGrupo: TIntegerField;
    QrOSCabNumero: TIntegerField;
    QrOSCabOpcao: TIntegerField;
    QrOSCabOptado: TSmallintField;
    QrOSCabHowGerou: TSmallintField;
    QrOSCabPosGerou: TSmallintField;
    QrOSCabOSFlhUltGe: TIntegerField;
    QrOSCabMulServico: TLargeintField;
    QrOSCabAgeEqiCab: TIntegerField;
    QrOSCabOSFlhGrCab: TIntegerField;
    QrOSCabOSFlhGrIts: TIntegerField;
    QrOSCabSohInicial: TIntegerField;
    QrOSCabStPipAdPrg: TSmallintField;
    QrOSCabLstUplWeb: TDateTimeField;
    QrOSCabLCPUsed: TIntegerField;
    QrOSCabConta: TIntegerField;
    QrOSCabObsExecuta: TWideMemoField;
    QrOSCabDtIniMonGr: TDateField;
    QrOSCabMobiliCad: TIntegerField;
    PMEncerra: TPopupMenu;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    LaTotal: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConversasClick(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure QrGarantAfterOpen(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure QrGarantAfterScroll(DataSet: TDataSet);
    procedure QrGarantBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FDataI, FDataF: String;
    procedure ReopenGarantias();
  end;

  var
  FmOSGarantRnw: TFmOSGarantRnw;

implementation

uses UnMyObjects, ModuleGeral, Module, DmkDAC_PF, OSGarantCls, MyDBCheck,
  {$IfNDef SNoti} UnitNotificacoesEdit, {$EndIf}
  UnDiario_Tabs, DiarioAdd2, UMySQLModule;

{$R *.DFM}

procedure TFmOSGarantRnw.BtEncerraClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOSGarantCls, FmOSGarantCls, afmoNegarComAviso) then
  begin
    FmOSGarantCls.FFldCtrl := 'Controle';
    FmOSGarantCls.FDBG := DBGGarant;
    FmOSGarantCls.FQry := QrGarant;
    FmOSGarantCls.ShowModal;
    FmOSGarantCls.Destroy;
    ReopenGarantias();
  end;
end;

procedure TFmOSGarantRnw.BtNenhumClick(Sender: TObject);
begin
  DBGGarant.SelectedRows.Clear;
end;

procedure TFmOSGarantRnw.BtConversasClick(Sender: TObject);
var
  Genero: Integer;
begin
  if DBGGarant.SelectedRows.Count > 1 then
  begin
    Geral.MB_Aviso('Somente um item deve estar selecionado para gerar conversa!');
    Exit;
  end;
  if DBCheck.CriaFm(TFmDiarioAdd2, FmDiarioAdd2, afmoNegarComAviso) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrOSCab, Dmod.MyDB, [
    'SELECT * ',
    'FROM oscab ',
    'WHERE Codigo=' + Geral.FF0(QrGarantCodigo.Value),
    '']);
    //
    FmDiarioAdd2.ImgTipo.SQLType := stIns;
    FmDiarioAdd2.FGenero := Trunc(QrGarantDiarGen.Value);;
    FmDiarioAdd2.FDepto  := QrGarantCodigo.Value;
    FmDiarioAdd2.FLink2  := QrGarantControle.Value;
    FmDiarioAdd2.FQry    := nil;// QrDiarioAdd;
    //
    FmDiarioAdd2.EdCliInt.ValueVariant := QrOSCabEmpresa.Value;
    FmDiarioAdd2.CBCliInt.KeyValue := QrOSCabEmpresa.Value;
    //
    FmDiarioAdd2.EdEntidade1.ValueVariant := QrOSCabEntidade.Value;
    FmDiarioAdd2.CBEntidade1.KeyValue := QrOSCabEntidade.Value;
    //
    FmDiarioAdd2.EdTerceiro01.ValueVariant := QrOSCabEntContrat.Value;
    FmDiarioAdd2.CBTerceiro01.KeyValue := QrOSCabEntContrat.Value;
    //
    FmDiarioAdd2.EdInterloctr.ValueVariant := QrOSCabEntiContat.Value;
    FmDiarioAdd2.CBInterloctr.KeyValue := QrOSCabEntiContat.Value;
    //
    UMyMod.SetaCodUsuDeCodigo(FmDiarioAdd2.EdDiarioAss,
      FmDiarioAdd2.CBDiarioAss, FmDiarioAdd2.QrDiarioAss,
      Dmod.QrOpcoesBugsAssPosVda.Value, 'Codigo', 'CodUsu');
    //
    FmDiarioAdd2.EdFormContat.ValueVariant := Dmod.QrOpcoesBugsFrmCPosVda.Value;
    FmDiarioAdd2.CBFormContat.KeyValue := Dmod.QrOpcoesBugsFrmCPosVda.Value;
    //
    FmDiarioAdd2.LaEntidade1.Caption := '[Sub]Cliente:';
    FmDiarioAdd2.LaEntidade2.Caption := 'COntratante:';

    FmDiarioAdd2.ShowModal;
    FmDiarioAdd2.Destroy;
    //
    ReopenGarantias();
  end;
end;

procedure TFmOSGarantRnw.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSGarantRnw.BtTodosClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrGarantControle.Value;
  QrGarant.First;
  while not QrGarant.Eof do
  begin
    DBGGarant.SelectedRows.CurrentRowSelected := True;
    QrGarant.Next;
  end;
  QrGarant.Locate('Controle', Controle, []);
end;

procedure TFmOSGarantRnw.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSGarantRnw.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSGarantRnw.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSGarantRnw.QrGarantAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrGarant.RecordCount > 0;
  BtEncerra.Enabled   := Habilita;
  BtConversas.Enabled := Habilita;
end;

procedure TFmOSGarantRnw.QrGarantAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrGarant.RecordCount);
end;

procedure TFmOSGarantRnw.QrGarantBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmOSGarantRnw.ReopenGarantias();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGarant, Dmod.MyDB, [
    'SELECT "ossrv" Tabela, eco.Nome NO_ENTICONTAT,  ',
    Geral.FF0(CO_DIARIO_ADD_GENERO_POSGSRV) + ' + 0.000 DiarGen, ',
    'fge.Nome NO_FatoGeradr, stc.Nome NO_SiapTerCad, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
    'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT, ',
    'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG, ',
    'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR, ',
    ' ',
    'cab.Codigo, srv.Controle, des.Nome NO_SERVICO, cab.DtaExeIni, srv.GarantiaDd, ',
    'DATE_ADD(cab.DtaExeIni, INTERVAL srv.GarantiaDd DAY) DtGarantia ',
    'FROM ossrv srv ',
    'LEFT JOIN oscab cab ON cab.Codigo=srv.Codigo ',
    'LEFT JOIN desservico des ON des.Codigo=srv.DesServico ',
    ' ',
    'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
    'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante ',
    'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat ',
    'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
    'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
    'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat ',
    ' ',
    'WHERE DATE_ADD(cab.DtaExeini, INTERVAL srv.GarantiaDd DAY) ',
    '  BETWEEN "' + FDataI + '" AND "' + FDataF + '" ',
    'AND srv.EncerraDta<"1900-01-01"',
    ' ',
    'UNION ',
    ' ',
    'SELECT "oscxa" Tabela, eco.Nome NO_ENTICONTAT,  ',
    Geral.FF0(CO_DIARIO_ADD_GENERO_POSGCXA) + ' + 0.000 DiarGen, ',
    'fge.Nome NO_FatoGeradr, stc.Nome NO_SiapTerCad, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
    'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT, ',
    'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG, ',
    'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR, ',
    ' ',
    'cab.Codigo, cxa.Controle, "LIMPEZA DE CAIXA D''�GUA" NO_SERVICO, cab.DtaExeIni, cxa.GarantiaDd, ',
    'DATE_ADD(cab.DtaExeIni, INTERVAL cxa.GarantiaDd DAY) DtGarantia ',
    'FROM oscxa cxa ',
    'LEFT JOIN oscab cab ON cab.Codigo=cxa.Codigo ',
    ' ',
    'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
    'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante ',
    'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat ',
    'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
    'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
    'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat ',
    ' ',
    'WHERE DATE_ADD(cab.DtaExeini, INTERVAL cxa.GarantiaDd DAY) ',
    '  BETWEEN "' + FDataI + '" AND "' + FDataF + '" ',
    'AND cxa.EncerraDta<"1900-01-01"',
    ' ',
    'ORDER BY DtGarantia ',
    ' ',
    ' ']);
  {$IfNDef SNoti}
  if QrGarant.RecordCount > 0 then
    UnNotificacoesEdit.CriaNotificacaoPublica(Dmod.MyDB, ntfBugsOSsGarantias,
      DmodG.ObtemAgora(True))
  else
    UnNotificacoesEdit.ExcluiNotificacao(Dmod.MyDB, Integer(ntfBugsOSsGarantias));
  {$EndIf}
end;

end.
