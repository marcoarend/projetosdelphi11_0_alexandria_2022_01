unit OSMonCabFut;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO;

type
  TFmOSMonCabFut = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBQryes: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrNovosPMVs: TmySQLQuery;
    DsNovosPMVs: TDataSource;
    QrNovosPMVsCodigo: TIntegerField;
    QrNovosPMVsNome: TWideStringField;
    QrNovosPMVsEquipamento: TIntegerField;
    QrNovosPMVsOSMonCab: TIntegerField;
    QrNovosPMVsMotDesativ: TIntegerField;
    QrNovosPMVsDtaAquis: TDateField;
    QrNovosPMVsDtaDesativ: TDateField;
    QrNovosPMVsLk: TIntegerField;
    QrNovosPMVsDataCad: TDateField;
    QrNovosPMVsDataAlt: TDateField;
    QrNovosPMVsUserCad: TIntegerField;
    QrNovosPMVsUserAlt: TIntegerField;
    QrNovosPMVsAlterWeb: TSmallintField;
    QrNovosPMVsAtivo: TSmallintField;
    QrNovosPMVsPrgLstCab: TIntegerField;
    QrNovosPMVsDependenci: TIntegerField;
    QrNovosPMVsOrdem: TIntegerField;
    QrNovosPMVsReordem: TIntegerField;
    QrNovosPMVsPerioDd: TIntegerField;
    QrNovosPMVsDdPostero: TIntegerField;
    QrOSsFuturas: TmySQLQuery;
    QrOSsFuturasCodigo: TIntegerField;
    QrOSsFuturasDtaExePrv: TDateTimeField;
    GroupBox2: TGroupBox;
    DBGOSs: TdmkDBGridZTO;
    GroupBox3: TGroupBox;
    DBGPMVs: TdmkDBGridZTO;
    DsOSsFuturas: TDataSource;
    Panel5: TPanel;
    BtTodos_PMV: TBitBtn;
    BtNenhum_PMV: TBitBtn;
    Panel6: TPanel;
    BtTodos_OSs: TBitBtn;
    BtNenhum_OSs: TBitBtn;
    QrOSsFuturasSiapterCad: TIntegerField;
    QrOSsFuturasDIAS_B: TFloatField;
    QrOSsFuturasDIAS_A: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTodos_PMVClick(Sender: TObject);
    procedure BtNenhum_PMVClick(Sender: TObject);
    procedure BtTodos_OSsClick(Sender: TObject);
    procedure BtNenhum_OSsClick(Sender: TObject);
  private
    { Private declarations }
    procedure SelecionaTodasOSs(Seleciona: Boolean);
    procedure SelecionaTodosPMVs(Seleciona: Boolean);
  public
    { Public declarations }
    procedure ReopenNovosPMVs(SrvControle: Integer);
    procedure ReopenOSsFuturas(SiapTerCad: Integer; DataIni: TDateTime;
              EstaOS: Integer);
  end;

  var
  FmOSMonCabFut: TFmOSMonCabFut;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnOSApp_PF;

{$R *.DFM}

procedure TFmOSMonCabFut.BtNenhum_OSsClick(Sender: TObject);
begin
  SelecionaTodasOSs(False);
end;

procedure TFmOSMonCabFut.BtNenhum_PMVClick(Sender: TObject);
begin
  SelecionaTodosPMVs(False);
end;

procedure TFmOSMonCabFut.BtOKClick(Sender: TObject);
var
  I, J, Q: Integer;
(*
  PMVs: array of Integer;
  procedure AddPMVLista();
  var
    N: Integer;
  begin
    N := Length(PMVs);
    SetLength(PMVs, N + 1);
    PMVs[N] := QrNovosPMVsCodigo.Value;
  end;
*)
  procedure IncluiPMVsNaOSAtual();
  var
    Codigo, SiapterCad,
    OSFMCbMae, PipCad, Ordem, PrgLstCab: Integer;
  begin
    Codigo     := QrOSsFuturasCodigo.Value;
    SiapterCad := QrOSsFuturasSiapTerCad.Value;
    //
    OSFMCbMae  := QrNovosPMVsOSMonCab.Value;
    PipCad     := QrNovosPMVsCodigo.Value;
    Ordem      := QrNovosPMVsOrdem.Value;
    PrgLstCab  := QrNovosPMVsPrgLstCab.Value;
    //
    OSApp_PF.IncluiOSMonPipAtual(Codigo, SiapterCad, OSFMCbMae,
      PipCad, Ordem, PrgLstCab);
    //
  end;
begin
  if MyObjects.FIC(DBGPMVs.SelectedRows.Count < 1, nil,
  'Selecione pelo menos um PMV!') then
    Exit;
  //
  if MyObjects.FIC(DBGOSs.SelectedRows.Count < 1, nil,
  'Selecione pelo menos uma OS!') then
    Exit;
  //
  if Geral.MB_Pergunta(
  'Confirma a inclus�o dos PMVs selecionados nas OSs selecionadas?') = ID_YES then
  begin
     Q := 0;
     GBQryes.Enabled := False;
     try
        (*
        with DBGPMVs.DataSource.DataSet do
        for I:= 0 to DBGPMVs.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGPMVs.SelectedRows.Items[I]));
          //
          AddPMVLista();
        end;
        *)
        //
        with DBGOSs.DataSource.DataSet do
        for I:= 0 to DBGOSs.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGOSs.SelectedRows.Items[I]));
          //
          with DBGPMVs.DataSource.DataSet do
          for J := 0 to DBGPMVs.SelectedRows.Count-1 do
          begin
            GotoBookmark(pointer(DBGPMVs.SelectedRows.Items[J]));
            //
            IncluiPMVsNaOSAtual();
            Q := Q + 1;
          end;
        end;
     finally
       GBQryes.Enabled := True;
     end;
     Geral.MB_Info(Geral.FF0(Q) + ' lan�amentos foram feitos!');
  end;
end;

procedure TFmOSMonCabFut.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSMonCabFut.BtTodos_OSsClick(Sender: TObject);
begin
  SelecionaTodasOSs(True);
end;

procedure TFmOSMonCabFut.BtTodos_PMVClick(Sender: TObject);
begin
  SelecionaTodosPMVs(True);
end;

procedure TFmOSMonCabFut.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSMonCabFut.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSMonCabFut.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSMonCabFut.ReopenNovosPMVs(SrvControle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNovosPMVs, Dmod.MyDB, [
  'SELECT DISTINCT pip.*, ',
  'omc.PerioDd, omc.DdPostero ',
  'FROM pipcad pip ',
  'LEFT JOIN osmoncab omc ON omc.PipCad=pip.Codigo ',
  'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo ',
  'WHERE omc.Controle=' + Geral.FF0(SrvControle),
  'AND pip.DtaDesativ < "1900-01-01" ',
  '']);
  //
  SelecionaTodosPMVs(True);
end;

procedure TFmOSMonCabFut.ReopenOSsFuturas(SiapTerCad: Integer;
  DataIni: TDateTime; EstaOS: Integer);
var
  Data: String;
begin
  Data := Geral.FDT(DataIni, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSsFuturas, Dmod.MyDB, [
  'SELECT DISTINCT cab.Codigo, cab.DtaExePrv, cab.SiapterCad, ',
  'TO_DAYS(cab.DtaExePrv) - TO_DAYS(cab.DtIniMonGr) + 0.0000 DIAS_A, ',
  'TO_DAYS(cab.DtaExePrv) - TO_DAYS("' + Data + '") + 0.0000 DIAS_B ',
  'FROM ospipmon opm ',
  'LEFT JOIN oscab cab ON cab.Codigo=opm.Codigo ',
  'WHERE cab.SiapterCad=' + Geral.FF0(SiapterCad),
  'AND cab.DtaExePrv > "' + Data + '" ',
  'AND cab.Codigo<>' + Geral.FF0(EstaOS),
  'ORDER BY cab.DtaExePrv ',
  '']);
end;

procedure TFmOSMonCabFut.SelecionaTodasOSs(Seleciona: Boolean);
begin
  QrOSsFuturas.First;
  while not QrOSsFuturas.Eof do
  begin
    DBGOSs.SelectedRows.CurrentRowSelected := Seleciona;
    //
    QrOSsFuturas.Next;
  end;
end;

procedure TFmOSMonCabFut.SelecionaTodosPMVs(Seleciona: Boolean);
begin
  QrNovosPMVs.First;
  while not QrNovosPMVs.Eof do
  begin
    DBGPMVs.SelectedRows.CurrentRowSelected := Seleciona;
    //
    QrNovosPMVs.Next;
  end;
end;

end.
