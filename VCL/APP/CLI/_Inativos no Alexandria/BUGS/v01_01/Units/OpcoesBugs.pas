unit OpcoesBugs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkCheckBox, Vcl.Menus, dmkValUsu, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdFTP, UnDmkEnums;

type
  TFmOpcoesBugs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEqAp: TmySQLQuery;
    DsEqAp: TDataSource;
    QrEqApNome: TWideStringField;
    QrEqApCODNIV: TIntegerField;
    QrEqMo: TmySQLQuery;
    DsEqMo: TDataSource;
    QrEqMoNome: TWideStringField;
    QrEqMoCODNIV: TIntegerField;
    QrPrAp: TmySQLQuery;
    StringField1: TWideStringField;
    IntegerField1: TIntegerField;
    DsPrAp: TDataSource;
    QrPrMo: TmySQLQuery;
    StringField2: TWideStringField;
    IntegerField2: TIntegerField;
    DsPrMo: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBEquipamentos: TGroupBox;
    GroupBox2: TGroupBox;
    RGGraNivEqAp: TdmkRadioGroup;
    Panel5: TPanel;
    Label1: TLabel;
    EdGraCodEqAp: TdmkEditCB;
    CBGraCodEqAp: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    RGGraNivEqMo: TdmkRadioGroup;
    Panel6: TPanel;
    Label2: TLabel;
    EdGraCodEqMo: TdmkEditCB;
    CBGraCodEqMo: TdmkDBLookupComboBox;
    GBProdutos: TGroupBox;
    GroupBox5: TGroupBox;
    RGGraNivPrAp: TdmkRadioGroup;
    Panel7: TPanel;
    Label3: TLabel;
    EdGraCodPrAp: TdmkEditCB;
    CBGraCodPrAp: TdmkDBLookupComboBox;
    GroupBox6: TGroupBox;
    RGGraNivPrMo: TdmkRadioGroup;
    Panel8: TPanel;
    Label4: TLabel;
    EdGraCodPrMo: TdmkEditCB;
    CBGraCodPrMo: TdmkDBLookupComboBox;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    GroupBox4: TGroupBox;
    Panel10: TPanel;
    Label5: TLabel;
    EdImgSDir: TdmkEdit;
    EdImgIPv4: TdmkEdit;
    Label6: TLabel;
    SbImgSDir: TSpeedButton;
    TabSheet3: TTabSheet;
    Panel11: TPanel;
    EdSloganFoot: TdmkEdit;
    Label7: TLabel;
    Panel12: TPanel;
    GroupBox7: TGroupBox;
    Panel13: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    SbContrtSDir: TSpeedButton;
    EdContrtSDir: TdmkEdit;
    EdContrtIPv4: TdmkEdit;
    GroupBox9: TGroupBox;
    Panel15: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    SbCxaSDir: TSpeedButton;
    EdCxaSDir: TdmkEdit;
    EdCxaIPv4: TdmkEdit;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Panel17: TPanel;
    GroupBox11: TGroupBox;
    Panel18: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    SbCunsSDir: TSpeedButton;
    EdCunsSDir: TdmkEdit;
    EdCunsIPv4: TdmkEdit;
    Panel16: TPanel;
    Panel20: TPanel;
    GroupBox10: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    CBCorImpPrdTxt: TColorBox;
    CBCorImpPrdBkg: TColorBox;
    CkCorImpPrdBld: TdmkCheckBox;
    CkInfoImoMov: TdmkCheckBox;
    CkInfQdr2Dep: TdmkCheckBox;
    CkAgeNomAnts: TdmkCheckBox;
    RGIdxBDAnt: TdmkRadioGroup;
    Panel22: TPanel;
    GroupBox12: TGroupBox;
    Panel19: TPanel;
    CkSWTAgenda: TdmkCheckBox;
    QrEstatusOSs: TmySQLQuery;
    QrEstatusOSsCodigo: TIntegerField;
    QrEstatusOSsNome: TWideStringField;
    DsEstatusOSs: TDataSource;
    DsExeTxtCli1: TDataSource;
    QrExeTxtCli1: TmySQLQuery;
    QrExeTxtCli1Codigo: TIntegerField;
    QrExeTxtCli1Nome: TWideStringField;
    GroupBox16: TGroupBox;
    Panel27: TPanel;
    CkATBAutoExp: TdmkCheckBox;
    TabSheet6: TTabSheet;
    Panel28: TPanel;
    Panel23: TPanel;
    GroupBox15: TGroupBox;
    RGDdRotaSab: TdmkRadioGroup;
    RgDdRotaDom: TdmkRadioGroup;
    RGDdRotaFer: TdmkRadioGroup;
    Panel24: TPanel;
    Panel25: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    EdOSPrvSta: TdmkEditCB;
    CBOSPrvSta: TdmkDBLookupComboBox;
    EdOSPrvXtraD: TdmkEdit;
    Panel29: TPanel;
    RGFlhAgeEqi: TdmkRadioGroup;
    RGFlhEntCtt: TdmkRadioGroup;
    RGFlhNumCtr: TdmkRadioGroup;
    RGFlhEntCtr: TdmkRadioGroup;
    RGFlhEntPag: TdmkRadioGroup;
    RGFlhCondPg: TdmkRadioGroup;
    RGFlhCrtEmi: TdmkRadioGroup;
    GroupBox14: TGroupBox;
    Label22: TLabel;
    CBCros1BordB: TColorBox;
    Label23: TLabel;
    CBCros1Per1B: TColorBox;
    Label24: TLabel;
    CBCros1Res1B: TColorBox;
    Label25: TLabel;
    CBCros1Res2B: TColorBox;
    Label26: TLabel;
    CBCros1Res3B: TColorBox;
    BtSugestoes: TBitBtn;
    PMSugestoes: TPopupMenu;
    Brancoepreto1: TMenuItem;
    LaranjaAzuleverde1: TMenuItem;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    GroupBox17: TGroupBox;
    Panel44: TPanel;
    Label27: TLabel;
    EdLstCusPrd: TdmkEditCB;
    CBLstCusPrd: TdmkDBLookupComboBox;
    dmkValUsu1: TdmkValUsu;
    GroupBox18: TGroupBox;
    TabSheet7: TTabSheet;
    Panel30: TPanel;
    Label29: TLabel;
    EdSecuritStr: TdmkEdit;
    QrDiarioAss: TmySQLQuery;
    QrDiarioAssCodigo: TIntegerField;
    QrDiarioAssCodUsu: TIntegerField;
    QrDiarioAssNome: TWideStringField;
    QrDiarioAssAplicacao: TIntegerField;
    DsDiarioAss: TDataSource;
    dmkValUsu2: TdmkValUsu;
    QrFormContat: TmySQLQuery;
    QrFormContatCodigo: TIntegerField;
    QrFormContatNome: TWideStringField;
    DsFormContat: TDataSource;
    Label41: TLabel;
    CBAgeCorAvul: TColorBox;
    Label42: TLabel;
    CBAgeCorBgst: TColorBox;
    EdPdrMntsMon: TdmkEdit;
    Label43: TLabel;
    RGFlhPrePrg: TdmkRadioGroup;
    Panel32: TPanel;
    Panel33: TPanel;
    GroupBox21: TGroupBox;
    Panel34: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    EdMediakmh1: TdmkEdit;
    EdMediakmh2: TdmkEdit;
    EdMediakmh3: TdmkEdit;
    EdMediakmh4: TdmkEdit;
    EdMediakmh5: TdmkEdit;
    Label28: TLabel;
    EdMinHAgeExe: TdmkEdit;
    Label30: TLabel;
    Panel21: TPanel;
    GroupBox13: TGroupBox;
    Panel35: TPanel;
    Label19: TLabel;
    EdDdRotaFlh: TdmkEdit;
    CkAutReatPIP: TdmkCheckBox;
    GroupBox20: TGroupBox;
    Panel31: TPanel;
    Label39: TLabel;
    Label40: TLabel;
    EdAssPosVda: TdmkEditCB;
    CBAssPosVda: TdmkDBLookupComboBox;
    EdFrmCPosVda: TdmkEditCB;
    CBFrmCPosVda: TdmkDBLookupComboBox;
    GroupBox19: TGroupBox;
    Panel36: TPanel;
    CkOSPrvStaUsa: TdmkCheckBox;
    Label31: TLabel;
    EdOSPrvStaCod: TdmkEditCB;
    CBOSPrvStaCod: TdmkDBLookupComboBox;
    QrOSPrvStatus: TmySQLQuery;
    QrOSPrvStatusCodigo: TIntegerField;
    QrOSPrvStatusNome: TWideStringField;
    DsOSPrvStatus: TDataSource;
    Panel37: TPanel;
    QrExeTxtCli2: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsExeTxtCli2: TDataSource;
    TabSheet8: TTabSheet;
    Label47: TLabel;
    EdOSPrvETC: TdmkEditCB;
    CBOSPrvETC: TdmkDBLookupComboBox;
    Label32: TLabel;
    EdOSPrvOTC: TdmkEditCB;
    CBOSPrvOTC: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    SpeedButton10: TSpeedButton;
    Label33: TLabel;
    EdTxtFAES: TdmkEditCB;
    CBTxtFAES: TdmkDBLookupComboBox;
    SpeedButton2: TSpeedButton;
    QrTxtFAES: TmySQLQuery;
    IntegerField4: TIntegerField;
    StringField4: TWideStringField;
    DsTxtFAES: TDataSource;
    CkDataGarantiaFaes: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGGraNivEqApClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGGraNivEqMoClick(Sender: TObject);
    procedure RGGraNivPrMoClick(Sender: TObject);
    procedure RGGraNivPrApClick(Sender: TObject);
    procedure SbImgSDirClick(Sender: TObject);
    procedure SbContrtSDirClick(Sender: TObject);
    procedure SbCxaSDirClick(Sender: TObject);
    procedure SbCunsSDirClick(Sender: TObject);
    procedure Brancoepreto1Click(Sender: TObject);
    procedure LaranjaAzuleverde1Click(Sender: TObject);
    procedure BtSugestoesClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //FTxtTitRelC: String;
  end;

  var
  FmOpcoesBugs: TFmOpcoesBugs;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral, UnFTP, Principal;

{$R *.DFM}

procedure TFmOpcoesBugs.Brancoepreto1Click(Sender: TObject);
begin
  CBCros1BordB.Selected := clBlack;
  CBCros1Per1B.Selected := clWhite;
  CBCros1Res1B.Selected := clWhite;
  CBCros1Res2B.Selected := clWhite;
  CBCros1Res3B.Selected := clWhite;
end;

procedure TFmOpcoesBugs.BtOKClick(Sender: TObject);
const
  Codigo = 1;
var
  GraNivEqAp, GraCodEqAp, GraNivEqMo, GraCodEqMo,
  GraNivPrAp, GraCodPrAp, GraNivPrMo, GraCodPrMo,
  CorImpPrdBkg, CorImpPrdTxt, CorImpPrdBld,
  InfoImoMov, InfQdr2Dep, AgeNomAnts, DdRotaFlh: Integer;
  ImgIPv4, ImgSDir, CxaIPv4, CxaSDir, ContrtIPv4, ContrtSDir, CunsIPv4,
  CunsSDir: String;
  // OpcoesGerl
  SloganFoot, ImgTitRelR, ImgTitRelL: String;
  //MemoryStream: TMemoryStream;
  IdxBDAnt, SWTAgenda, DdRotaSab, DdRotaDom, DdRotaFer, OSPrvSta, OSPrvXtraD,
  FlhAgeEqi, FlhEntCtt, FlhNumCtr, FlhEntCtr, FlhEntPag, FlhCondPg, FlhCrtEmi,
  FlhPrePrg, OSPrvETC, OSPrvOTC, ATBAutoExp, Cros1BordB, Cros1Per1B, Cros1Res1B,
  Cros1Res2B, Cros1Res3B, LstCusPrd, AutReatPIP: Integer;
  //
  SecuritStr: String;
  AssPosVda, FrmCPosVda, AgeCorAvul, AgeCorBgst, PdrMntsMon, Mediakmh1,
  Mediakmh2, Mediakmh3, Mediakmh4, Mediakmh5, OSPrvStaUsa, OSPrvStaCod,
  TxtFAES, DataGarantiaFaes: Integer;
  MinHAgeExe: String;
begin
  GraNivEqAp     := RgGraNivEqAp.ItemIndex;
  GraCodEqAp     := EdGraCodEqAp.ValueVariant;
  GraNivEqMo     := RgGraNivEqMo.ItemIndex;
  GraCodEqMo     := EdGraCodEqMo.ValueVariant;
  //
  GraNivPrAp     := RgGraNivPrAp.ItemIndex;
  GraCodPrAp     := EdGraCodPrAp.ValueVariant;
  GraNivPrMo     := RgGraNivPrMo.ItemIndex;
  GraCodPrMo     := EdGraCodPrMo.ValueVariant;
  ImgIPv4        := EdImgIPv4.ValueVariant;
  ImgSDir        := EdImgSDir.ValueVariant;
  CxaIPv4        := EdCxaIPv4.ValueVariant;
  CxaSDir        := EdCxaSDir.ValueVariant;
  //
  CorImpPrdBkg   := CBCorImpPrdBkg.Selected;
  CorImpPrdTxt   := CBCorImpPrdTxt.Selected;
  CorImpPrdBld   := Geral.BoolToInt(CkCorImpPrdBld.Checked);
  //
  InfoImoMov     := Geral.BoolToInt(CkInfoImoMov.Checked);
  InfQdr2Dep     := Geral.BoolToInt(CkInfQdr2Dep.Checked);
  AgeNomAnts     := Geral.BoolToInt(CkAgeNomAnts.Checked);
  //
  CunsIPv4       := EdCunsIPv4.ValueVariant;
  CunsSDir       := EdCunsSDir.ValueVariant;
  //
  IdxBDAnt       := RGIdxBDAnt.ItemIndex;
  SWTAgenda      := Geral.BoolToInt(CkSWTAgenda.Checked);
  //
  DdRotaFlh      := EdDdRotaFlh.ValueVariant;
  DdRotaSab      := RGDdRotaSab.ItemIndex;
  DdRotaDom      := RGDdRotaDom.ItemIndex;
  DdRotaFer      := RGDdRotaFer.ItemIndex;
  //
  OSPrvSta       := EdOSPrvSta.ValueVariant;
  OSPrvXtraD     := EdOSPrvXtraD.ValueVariant;
  OSPrvETC       := EdOSPrvETC.ValueVariant;
  OSPrvOTC       := EdOSPrvOTC.ValueVariant;
  //
  ATBAutoExp     := Geral.BoolToInt(CkATBAutoExp.Checked);
  //
  FlhAgeEqi      := RGFlhAgeEqi.ItemIndex;
  FlhEntCtt      := RGFlhEntCtt.ItemIndex;
  FlhNumCtr      := RGFlhNumCtr.ItemIndex;
  FlhEntCtr      := RGFlhEntCtr.ItemIndex;
  FlhEntPag      := RGFlhEntPag.ItemIndex;
  FlhCondPg      := RGFlhCondPg.ItemIndex;
  FlhCrtEmi      := RGFlhCrtEmi.ItemIndex;
  FlhPrePrg      := RGFlhPrePrg.ItemIndex;
  //
  Cros1BordB     := CBCros1BordB.Selected;
  Cros1Per1B     := CBCros1Per1B.Selected;
  Cros1Res1B     := CBCros1Res1B.Selected;
  Cros1Res2B     := CBCros1Res2B.Selected;
  Cros1Res3B     := CBCros1Res3B.Selected;
  //
  LstCusPrd      := EdLstCusPrd.ValueVariant;
  //
  SecuritStr     := EdSecuritStr.Text;
  //
  FrmCPosVda     := EdFrmCPosVda.ValueVariant;
  AgeCorAvul     := CBAgeCorAvul.Selected;
  AgeCorBgst     := CBAgeCorBgst.Selected;
  //
  PdrMntsMon     := EdPdrMntsMon.ValueVariant;
  //
  Mediakmh1      := EdMediakmh1.ValueVariant;
  Mediakmh2      := EdMediakmh2.ValueVariant;
  Mediakmh3      := EdMediakmh3.ValueVariant;
  Mediakmh4      := EdMediakmh4.ValueVariant;
  Mediakmh5      := EdMediakmh5.ValueVariant;
  //
  MinHAgeExe     := EdMinHAgeExe.Text;
  //
  AutReatPIP     := Geral.BoolToInt(CkAutReatPIP.Checked);
  //
  OSPrvStaUsa    := Geral.BoolToInt(CkOSPrvStaUsa.Checked);
  OSPrvStaCod    := EdOSPrvStaCod.ValueVariant;
  //
  TxtFAES        := EdTxtFAES.ValueVariant;
  //
  DataGarantiaFaes := Geral.BoolToInt(CkDataGarantiaFaes.Checked);
  //
  // CUIDADO com o CodUsu!
  UMyMod.ObtemCodigoDeCodUsu(EdLstCusPrd, LstCusPrd, '');
  UMyMod.ObtemCodigoDeCodUsu(EdAssPosVda, AssPosVda, '');
  //

  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoesbugs', False, [
  'GraNivEqAp', 'GraCodEqAp', 'GraNivEqMo', 'GraCodEqMo',
  'GraNivPrAp', 'GraCodPrAp', 'GraNivPrMo', 'GraCodPrMo',
  'ImgIPv4', 'ImgSDir', 'CxaIPv4', 'CxaSDir',
  'CorImpPrdBkg', 'CorImpPrdTxt', 'CorImpPrdBld',
  'InfoImoMov', 'CunsIPv4', 'CunsSDir',
  'InfQdr2Dep', 'AgeNomAnts', 'IdxBDAnt',
  'SWTAgenda', 'DdRotaFlh',
  'DdRotaSab', 'DdRotaDom', 'DdRotaFer',
  'OSPrvSta', 'OSPrvXtraD', 'OSPrvETC', 'OSPrvOTC',
  'ATBAutoExp',
  'FlhAgeEqi', 'FlhEntCtt', 'FlhNumCtr',
  'FlhEntCtr', 'FlhEntPag', 'FlhCondPg',
  'FlhCrtEmi', 'FlhPrePrg',
  'Cros1BordB', 'Cros1Per1B',
  'Cros1Res1B', 'Cros1Res2B', 'Cros1Res3B',
  'LstCusPrd', 'PdrMntsMon',
  'Mediakmh1', 'Mediakmh2',
  'Mediakmh3', 'Mediakmh4', 'Mediakmh5',
  'MinHAgeExe', 'AutReatPIP',
  'OSPrvStaUsa', 'OSPrvStaCod',
  'TxtFAES', 'DataGarantiaFaes'
  ], [
  'Codigo'], [
  GraNivEqAp, GraCodEqAp, GraNivEqMo, GraCodEqMo,
  GraNivPrAp, GraCodPrAp, GraNivPrMo, GraCodPrMo,
  ImgIPv4, ImgSDir, CxaIPv4, CxaSDir,
  CorImpPrdBkg, CorImpPrdTxt, CorImpPrdBld,
  InfoImoMov, CunsIPv4, CunsSDir,
  InfQdr2Dep, AgeNomAnts, IdxBDAnt,
  SWTAgenda, DdRotaFlh,
  DdRotaSab, DdRotaDom, DdRotaFer,
  OSPrvSta, OSPrvXtraD, OSPrvETC, OSPrvOTC,
  ATBAutoExp,
  FlhAgeEqi, FlhEntCtt, FlhNumCtr,
  FlhEntCtr, FlhEntPag, FlhCondPg,
  FlhCrtEmi, FlhPrePrg,
  Cros1BordB, Cros1Per1B,
  Cros1Res1B, Cros1Res2B, Cros1Res3B,
  LstCusPrd, PdrMntsMon,
  Mediakmh1, Mediakmh2,
  Mediakmh3, Mediakmh4, Mediakmh5,
  MinHAgeExe, AutReatPIP,
  OSPrvStaUsa, OSPrvStaCod,
  TxtFAES, DataGarantiaFaes
  ], [
  Codigo], True) then
  begin
    SloganFoot     := EdSloganFoot.Text;
    ContrtIPv4     := EdContrtIPv4.ValueVariant;
    ContrtSDir     := EdContrtSDir.ValueVariant;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoesgerl', False, [
    'SloganFoot', 'ContrtIPv4', 'ContrtSDir',
    (*'SecuritStr', *)'AssPosVda', 'FrmCPosVda',
    'AgeCorAvul', 'AgeCorBgst'
    ], [
    'Codigo'], [
    SloganFoot, ContrtIPv4, ContrtSDir,
    (*SecuritStr, *)AssPosVda, FrmCPosVda,
    AgeCorAvul, AgeCorBgst
    ], [
    Codigo], False) then
    begin
      Dmod.ReopenOpcoesBugs();
      DModG.ReopenOpcoesGerl();
      Close;
    end;
  end;
end;

procedure TFmOpcoesBugs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesBugs.BtSugestoesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSugestoes, BtSugestoes);
end;

procedure TFmOpcoesBugs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesBugs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  UnDmkDAC_PF.AbreQuery(QrEstatusOSs, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrExeTxtCli1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrExeTxtCli2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCusPrc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDiarioAss, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFormContat, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOSPrvStatus, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTxtFAES, Dmod.MyDB);
end;

procedure TFmOpcoesBugs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesBugs.LaranjaAzuleverde1Click(Sender: TObject);
begin
  CBCros1BordB.Selected := clWhite;
  CBCros1Per1B.Selected := $0000CCFF;
  CBCros1Res1B.Selected := $009BEBFF;
  CBCros1Res2B.Selected := $00FED3BA;
  CBCros1Res3B.Selected := $0069F5CD;
end;

procedure TFmOpcoesBugs.RGGraNivEqApClick(Sender: TObject);
var
  FldCod, Tabela: String;
begin
  QrEqAp.Close;
  EdGraCodEqAp.ValueVariant := 0;
  CBGraCodEqAp.KeyValue := 0;
  FldCod := '';
  Tabela := '';
  case RGGraNivEqAp.ItemIndex of
    0: ; // nada
    1: begin FldCod := 'Nivel1'; Tabela := 'gragru1';    end;
    2: begin FldCod := 'Nivel2'; Tabela := 'gragru2';    end;
    3: begin FldCod := 'Nivel3'; Tabela := 'gragru3';    end;
    4: begin FldCod := 'Nivel4'; Tabela := 'gragru4';    end;
    5: begin FldCod := 'Nivel5'; Tabela := 'gragru5';    end;
    6: begin FldCod := 'Codigo'; Tabela := 'prdgruptip'; end;
    else
    begin
      Geral.MensagemBox('N�vel n�o implementado!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  if Tabela <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEqAp, Dmod.MyDB, [
    'SELECT Nome, ' + FldCod + ' CODNIV ',
    'FROM ' + Tabela,
    'ORDER BY Nome ',
    '']);
  end;
end;

procedure TFmOpcoesBugs.RGGraNivEqMoClick(Sender: TObject);
var
  FldCod, Tabela: String;
begin
  QrEqMo.Close;
  EdGraCodEqMo.ValueVariant := 0;
  CBGraCodEqMo.KeyValue := 0;
  case RGGraNivEqMo.ItemIndex of
    0: begin FldCod := '??????'; Tabela := '???????';    end;
    1: begin FldCod := 'Nivel1'; Tabela := 'gragru1';    end;
    2: begin FldCod := 'Nivel2'; Tabela := 'gragru2';    end;
    3: begin FldCod := 'Nivel3'; Tabela := 'gragru3';    end;
    4: begin FldCod := 'Nivel4'; Tabela := 'gragru4';    end;
    5: begin FldCod := 'Nivel5'; Tabela := 'gragru5';    end;
    6: begin FldCod := 'Codigo'; Tabela := 'prdgruptip'; end;
    else
    begin
      Geral.MensagemBox('N�vel n�o implementado!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEqMo, Dmod.MyDB, [
  'SELECT Nome, ' + FldCod + ' CODNIV ',
  'FROM ' + Tabela,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmOpcoesBugs.RGGraNivPrApClick(Sender: TObject);
var
  FldCod, Tabela: String;
begin
  QrPrAp.Close;
  EdGraCodPrAp.ValueVariant := 0;
  CBGraCodPrAp.KeyValue := 0;
  FldCod := '';
  Tabela := '';
  case RGGraNivPrAp.ItemIndex of
    0: ; // nada
    1: begin FldCod := 'Nivel1'; Tabela := 'gragru1';    end;
    2: begin FldCod := 'Nivel2'; Tabela := 'gragru2';    end;
    3: begin FldCod := 'Nivel3'; Tabela := 'gragru3';    end;
    4: begin FldCod := 'Nivel4'; Tabela := 'gragru4';    end;
    5: begin FldCod := 'Nivel5'; Tabela := 'gragru5';    end;
    6: begin FldCod := 'Codigo'; Tabela := 'prdgruptip'; end;
    else
    begin
      Geral.MensagemBox('N�vel n�o implementado!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  if Tabela <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPrAp, Dmod.MyDB, [
    'SELECT Nome, ' + FldCod + ' CODNIV ',
    'FROM ' + Tabela,
    'ORDER BY Nome ',
    '']);
  end;
end;

procedure TFmOpcoesBugs.RGGraNivPrMoClick(Sender: TObject);
var
  FldCod, Tabela: String;
begin
  QrPrMo.Close;
  EdGraCodPrMo.ValueVariant := 0;
  CBGraCodPrMo.KeyValue := 0;
  case RGGraNivPrMo.ItemIndex of
    0: begin FldCod := '??????'; Tabela := '???????';    end;
    1: begin FldCod := 'Nivel1'; Tabela := 'gragru1';    end;
    2: begin FldCod := 'Nivel2'; Tabela := 'gragru2';    end;
    3: begin FldCod := 'Nivel3'; Tabela := 'gragru3';    end;
    4: begin FldCod := 'Nivel4'; Tabela := 'gragru4';    end;
    5: begin FldCod := 'Nivel5'; Tabela := 'gragru5';    end;
    6: begin FldCod := 'Codigo'; Tabela := 'prdgruptip'; end;
    else
    begin
      Geral.MensagemBox('N�vel n�o implementado!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrMo, Dmod.MyDB, [
  'SELECT Nome, ' + FldCod + ' CODNIV ',
  'FROM ' + Tabela,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmOpcoesBugs.SbImgSDirClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdImgSDir);
end;

procedure TFmOpcoesBugs.SpeedButton10Click(Sender: TObject);
var
  OSPrvETC: Integer;
begin
  OSPrvETC   := EdOSPrvOTC.ValueVariant;
  VAR_CADASTRO := 0;

  FmPrincipal.MostraFormTxtGeneric(OSPrvETC);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdOSPrvETC, CBOSPrvETC, QrExeTxtCli1, VAR_CADASTRO);

    EdOSPrvETC.SetFocus;
  end;
end;

procedure TFmOpcoesBugs.SpeedButton1Click(Sender: TObject);
var
  OSPrvOTC: Integer;
begin
  OSPrvOTC   := EdOSPrvOTC.ValueVariant;
  VAR_CADASTRO := 0;

  FmPrincipal.MostraFormTxtGeneric(OSPrvOTC);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdOSPrvOTC, CBOSPrvOTC, QrExeTxtCli2, VAR_CADASTRO);

    EdOSPrvOTC.SetFocus;
  end;
end;

procedure TFmOpcoesBugs.SpeedButton2Click(Sender: TObject);
var
  TxtFAES: Integer;
begin
  TxtFAES      := EdTxtFAES.ValueVariant;
  VAR_CADASTRO := 0;

  FmPrincipal.MostraFormTxtGeneric(TxtFAES);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTxtFAES, CBTxtFAES, QrTxtFAES, VAR_CADASTRO);

    EdTxtFAES.SetFocus;
  end;
end;

procedure TFmOpcoesBugs.SbContrtSDirClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdContrtSDir);
end;

procedure TFmOpcoesBugs.SbCunsSDirClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdCunsSDir);
end;

procedure TFmOpcoesBugs.SbCxaSDirClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdCxaSDir);
end;

end.
