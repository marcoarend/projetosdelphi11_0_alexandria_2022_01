unit OSCabLaudo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkLabelRotate, dmkMemo;

type
  TFmOSCabLaudo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox28: TGroupBox;
    Splitter13: TSplitter;
    Panel26: TPanel;
    dmkLabelRotate8: TdmkLabelRotate;
    MeLauInfesInt: TdmkMemo;
    Panel29: TPanel;
    dmkLabelRotate9: TdmkLabelRotate;
    MeLauInfesExt: TdmkMemo;
    GroupBox27: TGroupBox;
    Panel25: TPanel;
    MeLauParTec: TdmkMemo;
    Splitter1: TSplitter;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmOSCabLaudo: TFmOSCabLaudo;

implementation

uses UnMyObjects, UMySQLModule, Module;

{$R *.DFM}

procedure TFmOSCabLaudo.BtOKClick(Sender: TObject);
var
  LauInfesInt, LauInfesExt, LauParTec: String;
begin
  if MyObjects.FIC(FCodigo = 0, nil, 'C�digo n�o informado!') then Exit;  
  //
  LauInfesInt := MeLauInfesInt.Text;
  LauInfesExt := MeLauInfesExt.Text;
  LauParTec   := MeLauParTec.Text;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False,
    ['LauInfesInt', 'LauInfesExt', 'LauParTec'], ['Codigo'],
    [LauInfesInt, LauInfesExt, LauParTec], [FCodigo], True)
  then
    Close;
end;

procedure TFmOSCabLaudo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSCabLaudo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSCabLaudo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSCabLaudo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
