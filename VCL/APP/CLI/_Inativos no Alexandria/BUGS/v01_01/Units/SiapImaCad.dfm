object FmSiapImaCad: TFmSiapImaCad
  Left = 339
  Top = 185
  Caption = 'CAD-SUBCL-003 :: Cadastro de Local de Aplica'#231#227'o'
  ClientHeight = 431
  ClientWidth = 501
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 501
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 453
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 405
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 385
        Height = 32
        Caption = 'Cadastro de Local de Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 385
        Height = 32
        Caption = 'Cadastro de Local de Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 385
        Height = 32
        Caption = 'Cadastro de Local de Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 501
    Height = 269
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 501
      Height = 269
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 501
        Height = 269
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 497
          Height = 66
          Align = alTop
          Caption = ' Dados Gerais: '
          TabOrder = 0
          object Label2: TLabel
            Left = 12
            Top = 20
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            Enabled = False
          end
          object Label3: TLabel
            Left = 80
            Top = 20
            Width = 40
            Height = 13
            Caption = 'Terreno:'
            Enabled = False
            FocusControl = EdTer_Nome
          end
          object EdCodigo: TdmkEdit
            Left = 12
            Top = 36
            Width = 64
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdTer_Nome: TdmkEdit
            Left = 148
            Top = 36
            Width = 337
            Height = 21
            Enabled = False
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSiapImaTer: TdmkEdit
            Left = 80
            Top = 36
            Width = 64
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 81
          Width = 497
          Height = 186
          Align = alClient
          Caption = ' Dados do endere'#231'o: '
          TabOrder = 1
          object Panel8: TPanel
            Left = 2
            Top = 15
            Width = 493
            Height = 169
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label16: TLabel
              Left = 8
              Top = 0
              Width = 88
              Height = 13
              Caption = 'Complemento: [F4]'
              FocusControl = EdSCompl2
            end
            object Label4: TLabel
              Left = 152
              Top = 0
              Width = 55
              Height = 13
              Caption = 'Constru'#237'da:'
            end
            object Label6: TLabel
              Left = 236
              Top = 0
              Width = 77
              Height = 13
              Caption = 'N'#227'o constru'#237'da:'
            end
            object Label10: TLabel
              Left = 320
              Top = 0
              Width = 40
              Height = 13
              Caption = 'Terreno:'
            end
            object Label11: TLabel
              Left = 404
              Top = 0
              Width = 27
              Height = 13
              Caption = 'Total:'
              Enabled = False
            end
            object Label1: TLabel
              Left = 8
              Top = 40
              Width = 157
              Height = 13
              Caption = 'Objeto: [Tecla F7 para pesquisar]'
            end
            object Label5: TLabel
              Left = 8
              Top = 80
              Width = 174
              Height = 13
              Caption = 'Finalidade: [Tecla F7 para pesquisar]'
            end
            object Label7: TLabel
              Left = 8
              Top = 120
              Width = 218
              Height = 13
              Caption = 'Tipo de constru'#231#227'o: [Tecla F7 para pesquisar]'
            end
            object SbObjeto: TSpeedButton
              Left = 460
              Top = 56
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbObjetoClick
            end
            object SbFinalidade: TSpeedButton
              Left = 460
              Top = 96
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbFinalidadeClick
            end
            object SbTpConstru: TSpeedButton
              Left = 460
              Top = 136
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbTpConstruClick
            end
            object EdSCompl2: TdmkEdit
              Left = 8
              Top = 16
              Width = 141
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'SCompl'
              UpdCampo = 'SCompl'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnKeyDown = EdSCompl2KeyDown
            end
            object EdM2Constru: TdmkEdit
              Left = 152
              Top = 16
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'M2Constru'
              UpdCampo = 'M2Constru'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdM2ConstruChange
            end
            object EdM2NaoBuild: TdmkEdit
              Left = 236
              Top = 16
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'M2NaoBuild'
              UpdCampo = 'M2NaoBuild'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdM2NaoBuildChange
            end
            object EdM2Terreno: TdmkEdit
              Left = 320
              Top = 16
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'M2Terreno'
              UpdCampo = 'M2Terreno'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdM2Total: TdmkEdit
              Left = 404
              Top = 16
              Width = 80
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'M2Total'
              UpdCampo = 'M2Total'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdObjeto: TdmkEditCB
              Left = 8
              Top = 56
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBObjeto
              IgnoraDBLookupComboBox = False
            end
            object CBObjeto: TdmkDBLookupComboBox
              Left = 64
              Top = 56
              Width = 392
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsObjetos
              TabOrder = 6
              dmkEditCB = EdObjeto
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdFinalidade: TdmkEditCB
              Left = 8
              Top = 96
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFinalidade
              IgnoraDBLookupComboBox = False
            end
            object CBFinalidade: TdmkDBLookupComboBox
              Left = 64
              Top = 96
              Width = 392
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsFinalidads
              TabOrder = 8
              dmkEditCB = EdFinalidade
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdTpConstru: TdmkEditCB
              Left = 8
              Top = 136
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBTpConstru
              IgnoraDBLookupComboBox = False
            end
            object CBTpConstru: TdmkDBLookupComboBox
              Left = 64
              Top = 136
              Width = 392
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTpConstrus
              TabOrder = 10
              dmkEditCB = EdTpConstru
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 317
    Width = 501
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 497
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 361
    Width = 501
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 355
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 353
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrObjetos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM objetos'
      'ORDER BY Nome')
    Left = 96
    Top = 52
    object QrObjetosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrObjetosNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsObjetos: TDataSource
    DataSet = QrObjetos
    Left = 124
    Top = 52
  end
  object QrFinalidads: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM finalidads'
      'ORDER BY Nome')
    Left = 152
    Top = 52
    object QrFinalidadsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFinalidadsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFinalidads: TDataSource
    DataSet = QrFinalidads
    Left = 180
    Top = 52
  end
  object QrTpConstrus: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tpconstrus'
      'ORDER BY Nome')
    Left = 208
    Top = 52
    object QrTpConstrusCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTpConstrusNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTpConstrus: TDataSource
    DataSet = QrTpConstrus
    Left = 236
    Top = 52
  end
end
