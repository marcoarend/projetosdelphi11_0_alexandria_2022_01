unit CunsCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, Variants,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Menus, Grids, DBGrids, ComCtrls, dmkDBGrid, UnDmkProcFunc, dmkLabelRotate,
  dmkDBLookupComboBox, dmkEditCB, dmkEditDateTimePicker, frxClass, dmkCompoStore,
  frxDBSet, dmkCheckBox, PnAnotacoes, dmkDBGridZTO, ComObj, UnDmkEnums,
  UnProjGroup_Consts;

type
  TCampoMonit = (fldmonitDdPostero, fldmonitPerioDd);
  TFmCunsCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    QrCunsCad: TmySQLQuery;
    DsCunsCad: TDataSource;
    QrCunsIts: TmySQLQuery;
    DsCunsIts: TDataSource;
    PMCab: TPopupMenu;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    QrCunsCadCodigo: TIntegerField;
    QrCunsCadAtivPrinc: TIntegerField;
    QrCunsCadHowFind: TIntegerField;
    QrCunsCadAccount: TIntegerField;
    QrCunsCadDataCon: TDateField;
    QrCunsItsNO_CUNSGRU: TWideStringField;
    QrCunsItsCNPJ_CPF: TWideStringField;
    QrCunsItsCunsGru: TIntegerField;
    QrCunsItsCunsSub: TIntegerField;
    QrCunsItsControle: TIntegerField;
    GBDados: TGroupBox;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesRazaoSocial: TWideStringField;
    QrEntidadesFantasia: TWideStringField;
    QrEntidadesRespons1: TWideStringField;
    QrEntidadesRespons2: TWideStringField;
    QrEntidadesPai: TWideStringField;
    QrEntidadesMae: TWideStringField;
    QrEntidadesCNPJ: TWideStringField;
    QrEntidadesIE: TWideStringField;
    QrEntidadesNome: TWideStringField;
    QrEntidadesApelido: TWideStringField;
    QrEntidadesCPF: TWideStringField;
    QrEntidadesRG: TWideStringField;
    QrEntidadesERua: TWideStringField;
    QrEntidadesECompl: TWideStringField;
    QrEntidadesEBairro: TWideStringField;
    QrEntidadesECidade: TWideStringField;
    QrEntidadesEUF: TSmallintField;
    QrEntidadesEPais: TWideStringField;
    QrEntidadesETe1: TWideStringField;
    QrEntidadesETe2: TWideStringField;
    QrEntidadesETe3: TWideStringField;
    QrEntidadesECel: TWideStringField;
    QrEntidadesEFax: TWideStringField;
    QrEntidadesEEMail: TWideStringField;
    QrEntidadesEContato: TWideStringField;
    QrEntidadesENatal: TDateField;
    QrEntidadesPRua: TWideStringField;
    QrEntidadesPCompl: TWideStringField;
    QrEntidadesPBairro: TWideStringField;
    QrEntidadesPCidade: TWideStringField;
    QrEntidadesPUF: TSmallintField;
    QrEntidadesPPais: TWideStringField;
    QrEntidadesPTe1: TWideStringField;
    QrEntidadesPTe2: TWideStringField;
    QrEntidadesPTe3: TWideStringField;
    QrEntidadesPCel: TWideStringField;
    QrEntidadesPFax: TWideStringField;
    QrEntidadesPEMail: TWideStringField;
    QrEntidadesPContato: TWideStringField;
    QrEntidadesPNatal: TDateField;
    QrEntidadesSexo: TWideStringField;
    QrEntidadesResponsavel: TWideStringField;
    QrEntidadesProfissao: TWideStringField;
    QrEntidadesCargo: TWideStringField;
    QrEntidadesRecibo: TSmallintField;
    QrEntidadesDiaRecibo: TSmallintField;
    QrEntidadesAjudaEmpV: TFloatField;
    QrEntidadesAjudaEmpP: TFloatField;
    QrEntidadesCliente1: TWideStringField;
    QrEntidadesCliente2: TWideStringField;
    QrEntidadesCliente3: TWideStringField;
    QrEntidadesCliente4: TWideStringField;
    QrEntidadesFornece1: TWideStringField;
    QrEntidadesFornece2: TWideStringField;
    QrEntidadesFornece3: TWideStringField;
    QrEntidadesFornece4: TWideStringField;
    QrEntidadesFornece5: TWideStringField;
    QrEntidadesFornece6: TWideStringField;
    QrEntidadesFornece7: TWideStringField;
    QrEntidadesFornece8: TWideStringField;
    QrEntidadesTerceiro: TWideStringField;
    QrEntidadesCadastro: TDateField;
    QrEntidadesInformacoes: TWideStringField;
    QrEntidadesLogo: TBlobField;
    QrEntidadesVeiculo: TIntegerField;
    QrEntidadesMensal: TWideStringField;
    QrEntidadesObservacoes: TWideMemoField;
    QrEntidadesTipo: TSmallintField;
    QrEntidadesLk: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesNOMEEUF: TWideStringField;
    QrEntidadesNOMEPUF: TWideStringField;
    QrEntidadesPCPF_TXT: TWideStringField;
    QrEntidadesPTE1_TXT: TWideStringField;
    QrEntidadesPTE2_TXT: TWideStringField;
    QrEntidadesPTE3_TXT: TWideStringField;
    QrEntidadesPCEL_TXT: TWideStringField;
    QrEntidadesPFAX_TXT: TWideStringField;
    QrEntidadesETE1_TXT: TWideStringField;
    QrEntidadesETE2_TXT: TWideStringField;
    QrEntidadesETE3_TXT: TWideStringField;
    QrEntidadesECEL_TXT: TWideStringField;
    QrEntidadesEFAX_TXT: TWideStringField;
    QrEntidadesCNPJ_TXT: TWideStringField;
    QrEntidadesECEP: TIntegerField;
    QrEntidadesPCEP: TIntegerField;
    QrEntidadesGrupo: TIntegerField;
    QrEntidadesDataAlt: TDateField;
    QrEntidadesUserCad: TSmallintField;
    QrEntidadesUserAlt: TSmallintField;
    QrEntidadesCRua: TWideStringField;
    QrEntidadesCCompl: TWideStringField;
    QrEntidadesCBairro: TWideStringField;
    QrEntidadesCCidade: TWideStringField;
    QrEntidadesCUF: TSmallintField;
    QrEntidadesCCEP: TIntegerField;
    QrEntidadesCPais: TWideStringField;
    QrEntidadesCTel: TWideStringField;
    QrEntidadesCFax: TWideStringField;
    QrEntidadesCCel: TWideStringField;
    QrEntidadesCContato: TWideStringField;
    QrEntidadesLRua: TWideStringField;
    QrEntidadesLCompl: TWideStringField;
    QrEntidadesLBairro: TWideStringField;
    QrEntidadesLCidade: TWideStringField;
    QrEntidadesLUF: TSmallintField;
    QrEntidadesLCEP: TIntegerField;
    QrEntidadesLPais: TWideStringField;
    QrEntidadesLTel: TWideStringField;
    QrEntidadesLFax: TWideStringField;
    QrEntidadesLCel: TWideStringField;
    QrEntidadesLContato: TWideStringField;
    QrEntidadesComissao: TFloatField;
    QrEntidadesDataCad: TDateField;
    QrEntidadesECEP_TXT: TWideStringField;
    QrEntidadesPCEP_TXT: TWideStringField;
    QrEntidadesCCEP_TXT: TWideStringField;
    QrEntidadesLCEP_TXT: TWideStringField;
    QrEntidadesNOMECAD2: TWideStringField;
    QrEntidadesNOMEALT2: TWideStringField;
    QrEntidadesSituacao: TSmallintField;
    QrEntidadesNivel: TWideStringField;
    QrEntidadesCTEL_TXT: TWideStringField;
    QrEntidadesCFAX_TXT: TWideStringField;
    QrEntidadesCCEL_TXT: TWideStringField;
    QrEntidadesLTEL_TXT: TWideStringField;
    QrEntidadesLFAX_TXT: TWideStringField;
    QrEntidadesLCEL_TXT: TWideStringField;
    QrEntidadesNOMEENTIGRUPO: TWideStringField;
    QrEntidadesNOMECUF: TWideStringField;
    QrEntidadesNOMELUF: TWideStringField;
    QrEntidadesAccount: TIntegerField;
    QrEntidadesNOMEACCOUNT: TWideStringField;
    QrEntidadesLogo2: TBlobField;
    QrEntidadesELograd: TSmallintField;
    QrEntidadesPLograd: TSmallintField;
    QrEntidadesConjugeNome: TWideStringField;
    QrEntidadesConjugeNatal: TDateField;
    QrEntidadesNome1: TWideStringField;
    QrEntidadesNatal1: TDateField;
    QrEntidadesNome2: TWideStringField;
    QrEntidadesNatal2: TDateField;
    QrEntidadesNome3: TWideStringField;
    QrEntidadesNatal3: TDateField;
    QrEntidadesCreditosI: TIntegerField;
    QrEntidadesCreditosL: TIntegerField;
    QrEntidadesCreditosD: TDateField;
    QrEntidadesCreditosU: TDateField;
    QrEntidadesCreditosV: TDateField;
    QrEntidadesMotivo: TIntegerField;
    QrEntidadesQuantI1: TIntegerField;
    QrEntidadesQuantI2: TIntegerField;
    QrEntidadesQuantI3: TIntegerField;
    QrEntidadesQuantI4: TIntegerField;
    QrEntidadesAgenda: TWideStringField;
    QrEntidadesSenhaQuer: TWideStringField;
    QrEntidadesSenha1: TWideStringField;
    QrEntidadesNatal4: TDateField;
    QrEntidadesNome4: TWideStringField;
    QrEntidadesNOMEMOTIVO: TWideStringField;
    QrEntidadesCreditosF2: TFloatField;
    QrEntidadesNOMEELOGRAD: TWideStringField;
    QrEntidadesNOMEPLOGRAD: TWideStringField;
    QrEntidadesCLograd: TSmallintField;
    QrEntidadesLLograd: TSmallintField;
    QrEntidadesNOMECLOGRAD: TWideStringField;
    QrEntidadesNOMELLOGRAD: TWideStringField;
    QrEntidadesQuantN1: TFloatField;
    QrEntidadesQuantN2: TFloatField;
    QrEntidadesLimiCred: TFloatField;
    QrEntidadesDesco: TFloatField;
    QrEntidadesCasasApliDesco: TSmallintField;
    QrEntidadesCPF_Pai: TWideStringField;
    QrEntidadesSSP: TWideStringField;
    QrEntidadesCidadeNatal: TWideStringField;
    QrEntidadesCPF_PAI_TXT: TWideStringField;
    QrEntidadesUFNatal: TSmallintField;
    QrEntidadesNOMENUF: TWideStringField;
    QrEntidadesFatorCompra: TFloatField;
    QrEntidadesAdValorem: TFloatField;
    QrEntidadesDMaisC: TIntegerField;
    QrEntidadesDMaisD: TIntegerField;
    QrEntidadesDataRG: TDateField;
    QrEntidadesNacionalid: TWideStringField;
    QrEntidadesEmpresa: TIntegerField;
    QrEntidadesNOMEEMPRESA: TWideStringField;
    QrEntidadesFormaSociet: TWideStringField;
    QrEntidadesSimples: TSmallintField;
    QrEntidadesAtividade: TWideStringField;
    QrEntidadesEstCivil: TSmallintField;
    QrEntidadesNOMEECIVIL: TWideStringField;
    QrEntidadesCPF_Conjuge: TWideStringField;
    QrEntidadesCBE: TIntegerField;
    QrEntidadesSCB: TIntegerField;
    QrEntidadesCPF_Resp1: TWideStringField;
    QrEntidadesCPF_Resp1_TXT: TWideStringField;
    QrEntidadesENumero: TIntegerField;
    QrEntidadesPNumero: TIntegerField;
    QrEntidadesCNumero: TIntegerField;
    QrEntidadesLNumero: TIntegerField;
    QrEntidadesBanco: TIntegerField;
    QrEntidadesAgencia: TWideStringField;
    QrEntidadesContaCorrente: TWideStringField;
    QrEntidadesCartPref: TIntegerField;
    QrEntidadesNOMECARTPREF: TWideStringField;
    QrEntidadesRolComis: TIntegerField;
    QrEntidadesFilial: TIntegerField;
    QrEntidadesIEST: TWideStringField;
    QrEntidadesQuantN3: TFloatField;
    QrEntidadesTempA: TFloatField;
    QrEntidadesTempD: TFloatField;
    QrEntidadesPAtividad: TIntegerField;
    QrEntidadesEAtividad: TIntegerField;
    QrEntidadesPCidadeCod: TIntegerField;
    QrEntidadesECidadeCod: TIntegerField;
    QrEntidadesPPaisCod: TIntegerField;
    QrEntidadesEPaisCod: TIntegerField;
    QrEntidadesAntigo: TWideStringField;
    QrEntidadesCUF2: TWideStringField;
    QrEntidadesContab: TWideStringField;
    QrEntidadesMSN1: TWideStringField;
    QrEntidadesPastaTxtFTP: TWideStringField;
    QrEntidadesPastaPwdFTP: TWideStringField;
    QrEntidadesProtestar: TSmallintField;
    QrEntidadesMultaCodi: TSmallintField;
    QrEntidadesMultaDias: TSmallintField;
    QrEntidadesMultaValr: TFloatField;
    QrEntidadesMultaPerc: TFloatField;
    QrEntidadesMultaTiVe: TSmallintField;
    QrEntidadesJuroSacado: TFloatField;
    QrEntidadesCPMF: TFloatField;
    QrEntidadesCorrido: TIntegerField;
    QrEntidadesCliInt: TIntegerField;
    QrEntidadesAltDtPlaCt: TDateField;
    QrEntidadesAlterWeb: TSmallintField;
    QrEntidadesAtivo: TSmallintField;
    QrEntidadesEEndeRef: TWideStringField;
    QrEntidadesPEndeRef: TWideStringField;
    QrEntidadesCEndeRef: TWideStringField;
    QrEntidadesLEndeRef: TWideStringField;
    QrEntidadesPNUMERO_TXT: TWideStringField;
    QrEntidadesENUMERO_TXT: TWideStringField;
    QrEntidadesCNUMERO_TXT: TWideStringField;
    QrEntidadesLNUMERO_TXT: TWideStringField;
    QrEntidadesIE_TXT: TWideStringField;
    QrEntidadesDATARG_TXT: TWideStringField;
    QrEntidadesECodMunici: TIntegerField;
    QrEntidadesPCodMunici: TIntegerField;
    QrEntidadesCCodMunici: TIntegerField;
    QrEntidadesLCodMunici: TIntegerField;
    QrEntidadesCNAE: TWideStringField;
    QrEntidadesSUFRAMA: TWideStringField;
    QrEntidadesECodiPais: TIntegerField;
    QrEntidadesPCodiPais: TIntegerField;
    QrEntidadesL_CNPJ: TWideStringField;
    QrEntidadesL_Ativo: TSmallintField;
    QrEntidadesL_CNPJ_TXT: TWideStringField;
    QrEntidadesNO_DTB_EMUNICI: TWideStringField;
    QrEntidadesNO_DTB_PMUNICI: TWideStringField;
    QrEntidadesNO_DTB_CMUNICI: TWideStringField;
    QrEntidadesNO_DTB_LMUNICI: TWideStringField;
    QrEntidadesNO_BACEN_EPAIS: TWideStringField;
    QrEntidadesNO_BACEN_PPAIS: TWideStringField;
    QrEntidadesCNAE_Nome: TWideStringField;
    QrEntidadesNIRE: TWideStringField;
    QrEntidadesIEST_TXT: TWideStringField;
    QrEntidadesURL: TWideStringField;
    QrEntidadesCRT: TSmallintField;
    QrEntidadesCOD_PART: TWideStringField;
    DsEntidades: TDataSource;
    PCDados: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PainelDados: TPanel;
    Panel11: TPanel;
    PnShowEnd: TPanel;
    PCShowGer: TPageControl;
    TabSheet4: TTabSheet;
    PnShow2Pj: TPanel;
    Panel18: TPanel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    dmkDBEdit20: TdmkDBEdit;
    dmkDBEdit21: TdmkDBEdit;
    dmkDBEdit22: TdmkDBEdit;
    PCShowCom: TPageControl;
    TabSheet5: TTabSheet;
    Panel19: TPanel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label117: TLabel;
    Label118: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    Label126: TLabel;
    Label131: TLabel;
    Label132: TLabel;
    Label133: TLabel;
    Label136: TLabel;
    dmkDBEdit23: TdmkDBEdit;
    dmkDBEdit24: TdmkDBEdit;
    dmkDBEdit25: TdmkDBEdit;
    dmkDBEdit26: TdmkDBEdit;
    dmkDBEdit27: TdmkDBEdit;
    dmkDBEdit28: TdmkDBEdit;
    dmkDBEdit29: TdmkDBEdit;
    dmkDBEdit30: TdmkDBEdit;
    dmkDBEdit31: TdmkDBEdit;
    dmkDBEdit32: TdmkDBEdit;
    dmkDBEdit33: TdmkDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    Memo2: TMemo;
    DBEdCRT: TDBEdit;
    PnShow2PF: TPanel;
    Panel15: TPanel;
    Label28: TLabel;
    Label33: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label39: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit34: TdmkDBEdit;
    PCShowRes: TPageControl;
    TabSheet6: TTabSheet;
    Panel16: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label119: TLabel;
    Label120: TLabel;
    Label123: TLabel;
    dmkDBEdit8: TdmkDBEdit;
    dmkDBEdit9: TdmkDBEdit;
    dmkDBEdit10: TdmkDBEdit;
    dmkDBEdit11: TdmkDBEdit;
    dmkDBEdit12: TdmkDBEdit;
    dmkDBEdit13: TdmkDBEdit;
    dmkDBEdit14: TdmkDBEdit;
    dmkDBEdit15: TdmkDBEdit;
    dmkDBEdit16: TdmkDBEdit;
    dmkDBEdit17: TdmkDBEdit;
    dmkDBEdit18: TdmkDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit16: TDBEdit;
    TabSheet7: TTabSheet;
    Panel13: TPanel;
    Label128: TLabel;
    Label138: TLabel;
    Label140: TLabel;
    GroupBox1: TGroupBox;
    CkCliente1: TDBCheckBox;
    CkFornece1: TDBCheckBox;
    CkFornece2: TDBCheckBox;
    CkFornece3: TDBCheckBox;
    CkTerceiro: TDBCheckBox;
    CkFornece4: TDBCheckBox;
    CkCliente2: TDBCheckBox;
    CkFornece5: TDBCheckBox;
    CkFornece6: TDBCheckBox;
    CkCliente3: TDBCheckBox;
    CkCliente4: TDBCheckBox;
    CkFornece8: TDBCheckBox;
    CkFornece7: TDBCheckBox;
    dmkDBEdit57: TdmkDBEdit;
    dmkDBEdit58: TdmkDBEdit;
    dmkDBEdit59: TdmkDBEdit;
    TabSheet11: TTabSheet;
    Panel6: TPanel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label116: TLabel;
    dmkDBEdit35: TdmkDBEdit;
    dmkDBEdit36: TdmkDBEdit;
    dmkDBEdit37: TdmkDBEdit;
    dmkDBEdit38: TdmkDBEdit;
    dmkDBEdit39: TdmkDBEdit;
    dmkDBEdit40: TdmkDBEdit;
    dmkDBEdit41: TdmkDBEdit;
    dmkDBEdit42: TdmkDBEdit;
    dmkDBEdit43: TdmkDBEdit;
    dmkDBEdit44: TdmkDBEdit;
    dmkDBEdit45: TdmkDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    TabSheet12: TTabSheet;
    Panel14: TPanel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label114: TLabel;
    Label115: TLabel;
    dmkDBEdit46: TdmkDBEdit;
    dmkDBEdit47: TdmkDBEdit;
    dmkDBEdit48: TdmkDBEdit;
    dmkDBEdit49: TdmkDBEdit;
    dmkDBEdit50: TdmkDBEdit;
    dmkDBEdit51: TdmkDBEdit;
    dmkDBEdit52: TdmkDBEdit;
    dmkDBEdit53: TdmkDBEdit;
    dmkDBEdit54: TdmkDBEdit;
    dmkDBEdit55: TdmkDBEdit;
    dmkDBEdit56: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    PCXtraGer: TPageControl;
    TabSheet9: TTabSheet;
    PnContatos: TPanel;
    DBGEntiContat: TDBGrid;
    Panel10: TPanel;
    DBGEntiMail: TDBGrid;
    DBGEntiTel: TDBGrid;
    TabSheet10: TTabSheet;
    GradeDefAtr: TdmkDBGrid;
    TabSheet15: TTabSheet;
    DBGEntiRespon: TDBGrid;
    TabSheet16: TTabSheet;
    DBGEntiCtas: TDBGrid;
    DsEntDefAtr: TDataSource;
    QrEntDefAtr: TmySQLQuery;
    QrEntDefAtrEntAtrCad: TIntegerField;
    QrEntDefAtrEntAtrIts: TIntegerField;
    QrEntDefAtrCTRL_ATR: TIntegerField;
    QrEntDefAtrCTRL_ITS: TIntegerField;
    QrEntDefAtrCODUSU_ITS: TIntegerField;
    QrEntDefAtrNOME_ITS: TWideStringField;
    QrEntDefAtrCODUSU_CAD: TIntegerField;
    QrEntDefAtrNOME_CAD: TWideStringField;
    DsEntiTel: TDataSource;
    QrEntiTel: TmySQLQuery;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelConta: TIntegerField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelEntiTipCto: TIntegerField;
    QrEntiTelTEL_TXT: TWideStringField;
    QrEntiTelRamal: TWideStringField;
    DsEntiMail: TDataSource;
    QrEntiMail: TmySQLQuery;
    QrEntiMailNOMEETC: TWideStringField;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailEntiTipCto: TIntegerField;
    QrEntiMailOrdem: TIntegerField;
    DsEntiRespon: TDataSource;
    QrEntiRespon: TmySQLQuery;
    QrEntiResponCodigo: TIntegerField;
    QrEntiResponControle: TIntegerField;
    QrEntiResponNome: TWideStringField;
    QrEntiResponCargo: TIntegerField;
    QrEntiResponAssina: TSmallintField;
    QrEntiResponOrdemLista: TIntegerField;
    QrEntiResponObserv: TWideStringField;
    QrEntiResponNOME_CARGO: TWideStringField;
    QrEntiResponNO_ASSINA: TWideStringField;
    QrEntiResponMandatoIni: TDateField;
    QrEntiResponMandatoFim: TDateField;
    QrEntiResponMandatoIni_TXT: TWideStringField;
    QrEntiResponMandatoFim_TXT: TWideStringField;
    DsEntiContat: TDataSource;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    QrEntiContatCargo: TIntegerField;
    QrEntiContatNOME_CARGO: TWideStringField;
    QrEntiCtas: TmySQLQuery;
    QrEntiCtasCodigo: TIntegerField;
    QrEntiCtasControle: TIntegerField;
    QrEntiCtasOrdem: TIntegerField;
    QrEntiCtasBanco: TIntegerField;
    QrEntiCtasAgencia: TIntegerField;
    QrEntiCtasContaCor: TWideStringField;
    QrEntiCtasContaTip: TWideStringField;
    QrEntiCtasDAC_A: TWideStringField;
    QrEntiCtasDAC_C: TWideStringField;
    QrEntiCtasDAC_AC: TWideStringField;
    QrEntiCtasLk: TIntegerField;
    QrEntiCtasDataCad: TDateField;
    QrEntiCtasDataAlt: TDateField;
    QrEntiCtasUserCad: TIntegerField;
    QrEntiCtasUserAlt: TIntegerField;
    QrEntiCtasAlterWeb: TSmallintField;
    QrEntiCtasAtivo: TSmallintField;
    QrEntiCtasNOMEBANCO: TWideStringField;
    DsEntiCtas: TDataSource;
    PnTerrenos: TPanel;
    QrSiapTerCad: TmySQLQuery;
    DsSiapTerCad: TDataSource;
    QrSiapImaCad: TmySQLQuery;
    DsSiapImaCad: TDataSource;
    QrSiapImaDep: TmySQLQuery;
    DsSiapImaDep: TDataSource;
    QrSiapImaCdi: TmySQLQuery;
    DsSiapImaCdi: TDataSource;
    QrSiapImaCav: TmySQLQuery;
    DsSiapImaCav: TDataSource;
    QrSiapImaRes: TmySQLQuery;
    DsSiapImaRes: TDataSource;
    QrSiapTerCadCodigo: TIntegerField;
    QrSiapTerCadNome: TWideStringField;
    QrSiapTerCadCliente: TIntegerField;
    QrSiapTerCadSLograd: TSmallintField;
    QrSiapTerCadSRua: TWideStringField;
    QrSiapTerCadSNumero: TIntegerField;
    QrSiapTerCadSCompl: TWideStringField;
    QrSiapTerCadSBairro: TWideStringField;
    QrSiapTerCadSCidade: TWideStringField;
    QrSiapTerCadSUF: TWideStringField;
    QrSiapTerCadSCEP: TIntegerField;
    QrSiapTerCadSPais: TWideStringField;
    QrSiapTerCadSEndeRef: TWideStringField;
    QrSiapTerCadSCodMunici: TIntegerField;
    QrSiapTerCadSCodiPais: TIntegerField;
    QrSiapTerCadM2Constru: TFloatField;
    QrSiapTerCadM2NaoBuild: TFloatField;
    QrSiapTerCadM2Terreno: TFloatField;
    QrSiapTerCadM2Total: TFloatField;
    Panel7: TPanel;
    Panel8: TPanel;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Splitter1: TSplitter;
    DBGCunsIts: TDBGrid;
    DBGSiapTerCad: TDBGrid;
    QrSiapTerCadNOMELOGRAD: TWideStringField;
    QrSiapImaCadTpConstru: TIntegerField;
    QrSiapImaCadSCompl2: TWideStringField;
    QrSiapImaCadNO_OBJETO: TWideStringField;
    QrSiapImaCadNO_FINALID: TWideStringField;
    QrSiapImaCadNO_TPCONSTRU: TWideStringField;
    QrSiapImaCadCodigo: TIntegerField;
    QrSiapImaCadSiapImaTer: TIntegerField;
    QrSiapImaCadObjeto: TIntegerField;
    QrSiapImaCadFinalidade: TIntegerField;
    QrSiapImaCadM2Constru: TFloatField;
    QrSiapImaCadM2NaoBuild: TFloatField;
    QrSiapImaCadM2Terreno: TFloatField;
    QrSiapImaCadM2Total: TFloatField;
    QrSiapImaCui: TmySQLQuery;
    DsSiapImaCui: TDataSource;
    QrSiapImaAti: TmySQLQuery;
    DsSiapImaAti: TDataSource;
    QrSiapImaCxa: TmySQLQuery;
    DsSiapImaCxa: TDataSource;
    QrSiapImaDepNO_DEPENDENCI: TWideStringField;
    QrSiapImaDepCodigo: TIntegerField;
    QrSiapImaDepControle: TIntegerField;
    QrSiapImaDepDependenci: TIntegerField;
    QrSiapImaCdiNO_CARACTERIS: TWideStringField;
    QrSiapImaCdiCodigo: TIntegerField;
    QrSiapImaCdiControle: TIntegerField;
    QrSiapImaCdiCaracteris: TIntegerField;
    QrSiapImaCavNO_CARACTERIS: TWideStringField;
    QrSiapImaCavCodigo: TIntegerField;
    QrSiapImaCavControle: TIntegerField;
    QrSiapImaCavCaracteris: TIntegerField;
    QrSiapImaResNO_RESIDENTE: TWideStringField;
    QrSiapImaResCodigo: TIntegerField;
    QrSiapImaResControle: TIntegerField;
    QrSiapImaResResidente: TIntegerField;
    QrSiapImaCuiNO_CUIDADO: TWideStringField;
    QrSiapImaCuiCodigo: TIntegerField;
    QrSiapImaCuiControle: TIntegerField;
    QrSiapImaCuiCuidado: TIntegerField;
    QrSiapImaAtiNO_ATIVIDADE: TWideStringField;
    QrSiapImaAtiCodigo: TIntegerField;
    QrSiapImaAtiControle: TIntegerField;
    QrSiapImaAtiAtividade: TIntegerField;
    QrSiapImaCxaNO_MATERIAL: TWideStringField;
    QrSiapImaCxaNOME_FORMA: TWideStringField;
    QrSiapImaCxaCodigo: TIntegerField;
    QrSiapImaCxaControle: TIntegerField;
    QrSiapImaCxaMatersCxa: TIntegerField;
    QrSiapImaCxaFormasCxa: TIntegerField;
    QrSiapImaCxaVolumeL: TFloatField;
    QrSiapImaCxaLocal: TWideStringField;
    QrSiapImaCxaAcesso: TWideStringField;
    QrSiapImaCxaMedidas: TWideStringField;
    PMCunsIts: TPopupMenu;
    AdicionaContratante1: TMenuItem;
    RetiraContratante1: TMenuItem;
    PMSiapTerCad: TPopupMenu;
    PMSiapImaCad: TPopupMenu;
    PMSiapImaDep: TPopupMenu;
    PMSiapImaCdi: TPopupMenu;
    PMSiapImaCav: TPopupMenu;
    PMSiapImaRes: TPopupMenu;
    PMSiapImaCui: TPopupMenu;
    PMSiapImaAti: TPopupMenu;
    PMSiapImaCxa: TPopupMenu;
    IncluiTerCad1: TMenuItem;
    AlteraTerCad1: TMenuItem;
    ExcluiterCad1: TMenuItem;
    IncluiSiapImaCad1: TMenuItem;
    AlteraSiapImaCad1: TMenuItem;
    ExcluiSiapImaCad1: TMenuItem;
    IncluiSiapImaDep1: TMenuItem;
    AlteraSiapImaDep1: TMenuItem;
    ExcluiSiapImaDep1: TMenuItem;
    IncluiSiapImaCdi1: TMenuItem;
    AlteraSiapImaCdi1: TMenuItem;
    ExcluiSiapImaCdi1: TMenuItem;
    IncluiSiapImaCav1: TMenuItem;
    AlteraSiapImaCav1: TMenuItem;
    ExcluiSiapImaCav1: TMenuItem;
    IncluiSiapImaRes1: TMenuItem;
    AlteraSiapImaRes1: TMenuItem;
    ExcluiSiapImaRes1: TMenuItem;
    IncluiSiapImaCui1: TMenuItem;
    AlteraSiapImaCui1: TMenuItem;
    ExcluiSiapImaCui1: TMenuItem;
    IncluiSiapImaAti1: TMenuItem;
    AlteraSiapImaAti1: TMenuItem;
    ExcluiSiapImaAti1: TMenuItem;
    IncluiSiapImaCxa1: TMenuItem;
    AlteraSiapImaCxa1: TMenuItem;
    ExcluiSiapImaCxa1: TMenuItem;
    QrAtrSICxDef: TmySQLQuery;
    DsAtrSICxDef: TDataSource;
    QrAtrSICxDefID_Item: TIntegerField;
    QrAtrSICxDefID_Sorc: TIntegerField;
    QrAtrSICxDefAtrCad: TIntegerField;
    QrAtrSICxDefAtrIts: TIntegerField;
    QrAtrSICxDefCU_CAD: TIntegerField;
    QrAtrSICxDefCU_ITS: TIntegerField;
    QrAtrSICxDefNO_CAD: TWideStringField;
    QrAtrSICxDefNO_ITS: TWideStringField;
    PMAtrSICxDef: TPopupMenu;
    IncluiAtrSICxDef1: TMenuItem;
    AlteraAtrSICxDef1: TMenuItem;
    ExcluiAtrSICxDef1: TMenuItem;
    QrAtrSICdDef: TmySQLQuery;
    DsAtrSICdDef: TDataSource;
    QrAtrSICdDefID_Item: TIntegerField;
    QrAtrSICdDefID_Sorc: TIntegerField;
    QrAtrSICdDefAtrCad: TIntegerField;
    QrAtrSICdDefAtrIts: TIntegerField;
    QrAtrSICdDefCU_CAD: TIntegerField;
    QrAtrSICdDefCU_ITS: TIntegerField;
    QrAtrSICdDefNO_CAD: TWideStringField;
    QrAtrSICdDefNO_ITS: TWideStringField;
    PMAtrSICdDef: TPopupMenu;
    IncluiAtrSICdDef1: TMenuItem;
    AlteraAtrSICdDef1: TMenuItem;
    ExcluiAtrSICdDef1: TMenuItem;
    QrSiapImaResNome: TWideStringField;
    QrSiapImaResAnoNatal: TIntegerField;
    QrSiapImaResCuidado: TIntegerField;
    QrSiapImaResObservacao: TWideStringField;
    QrSiapImaResNO_CUIDADO: TWideStringField;
    Splitter2: TSplitter;
    QrCunsCadNO_AtivPrinc: TWideStringField;
    QrCunsCadNO_HowFind: TWideStringField;
    QrCunsCadNO_Account: TWideStringField;
    Panel31: TPanel;
    Panel32: TPanel;
    BtCunsCad: TBitBtn;
    BtCunsIts: TBitBtn;
    BtSiapTerCad: TBitBtn;
    BtSiapImaCad: TBitBtn;
    BtSiapImaDep: TBitBtn;
    BtSiapImaCdi: TBitBtn;
    BtSiapImaCav: TBitBtn;
    BtSiapImaRes: TBitBtn;
    BtAtrSICxDef: TBitBtn;
    BtSiapImaCxa: TBitBtn;
    BtSiapImaAti: TBitBtn;
    BtSiapImaCui: TBitBtn;
    BtAtrSICdDef: TBitBtn;
    EdAtivPrinc: TdmkEditCB;
    CBAtivPrinc: TdmkDBLookupComboBox;
    Label24: TLabel;
    QrAtividades: TmySQLQuery;
    DsAtividades: TDataSource;
    QrHowFounded: TmySQLQuery;
    DsHowFounded: TDataSource;
    QrAccounts: TmySQLQuery;
    DsAccounts: TDataSource;
    QrAtividadesCodigo: TIntegerField;
    QrAtividadesNome: TWideStringField;
    QrAccountsCodigo: TIntegerField;
    QrAccountsNome: TWideStringField;
    QrHowFoundedCodigo: TIntegerField;
    QrHowFoundedNome: TWideStringField;
    Label25: TLabel;
    EdAccount: TdmkEditCB;
    CBAccount: TdmkDBLookupComboBox;
    Label26: TLabel;
    EdHowFind: TdmkEditCB;
    CBHowFind: TdmkDBLookupComboBox;
    TPDataCon: TdmkEditDateTimePicker;
    Label27: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    PMQuery: TPopupMenu;
    PeloNome1: TMenuItem;
    Dados1: TMenuItem;
    CNPJ1: TMenuItem;
    N1: TMenuItem;
    QrSubCli: TmySQLQuery;
    DsSubCli: TDataSource;
    QrSubCliNO_SUBCLI: TWideStringField;
    QrSubCliCNPJ_CPF: TWideStringField;
    QrSubCliCunsGru: TIntegerField;
    QrSubCliCunsSub: TIntegerField;
    QrSubCliControle: TIntegerField;
    GroupBox7: TGroupBox;
    DBGMovAmovCad: TDBGrid;
    PCObjeto: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet8: TTabSheet;
    Panel9: TPanel;
    Panel29: TPanel;
    GBTerCadEnder: TGroupBox;
    MeTerCadEnder: TMemo;
    GroupBox8: TGroupBox;
    Label14: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit34: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    QrMovAmovCad: TmySQLQuery;
    DsMovAmovCad: TDataSource;
    QrMovAmovCadCodigo: TIntegerField;
    QrMovAmovCadNome: TWideStringField;
    QrMovAmovCadTipo: TSmallintField;
    QrMovAmovCadCliente: TIntegerField;
    TabSheet26: TTabSheet;
    DBGrid1: TDBGrid;
    Panel33: TPanel;
    GroupBox9: TGroupBox;
    DBGAtrAMovDef: TdmkDBGrid;
    QrAtrAMovDef: TmySQLQuery;
    DsAtrAMovDef: TDataSource;
    QrAtrAMovDefID_Item: TIntegerField;
    QrAtrAMovDefID_Sorc: TIntegerField;
    QrAtrAMovDefAtrCad: TIntegerField;
    QrAtrAMovDefAtrIts: TIntegerField;
    QrAtrAMovDefCU_CAD: TIntegerField;
    QrAtrAMovDefCU_ITS: TIntegerField;
    QrAtrAMovDefNO_CAD: TWideStringField;
    QrAtrAMovDefNO_ITS: TWideStringField;
    PMAtrAMovDef: TPopupMenu;
    IncluiAtrAMovDef1: TMenuItem;
    AlteraAtrAMovDef1: TMenuItem;
    ExcluiAtrAMovDef1: TMenuItem;
    PMMovAmovCad: TPopupMenu;
    Incluinovomvelautomvel1: TMenuItem;
    Alteramvelautomvel1: TMenuItem;
    Excluimvelautomvel1: TMenuItem;
    QrSiapImaDepMLarg: TFloatField;
    QrSiapImaDepMComp: TFloatField;
    QrSiapImaDepMAltu: TFloatField;
    N2: TMenuItem;
    DuplicSiapImaCxa1: TMenuItem;
    QrAT_Dep: TmySQLQuery;
    QrAT_DepPERC_TOT: TFloatField;
    QrAT_DepAREA: TFloatField;
    QrAT_DepVOLUME: TFloatField;
    DsAT_Dep: TDataSource;
    QrCunsImgCab: TmySQLQuery;
    DsCunsImgCab: TDataSource;
    QrCunsImgCabCodigo: TIntegerField;
    QrCunsImgCabControle: TIntegerField;
    QrCunsImgCabNome: TWideStringField;
    QrCunsImgCabStatus: TIntegerField;
    QrCunsImgCabCaminho: TWideStringField;
    PMCunsImgCab: TPopupMenu;
    IncluinovaFoto1: TMenuItem;
    AlteraFotoatual1: TMenuItem;
    Excluifotoatual1: TMenuItem;
    QrCunsImgCabNO_STATUS: TWideStringField;
    QrCunsImgCmt_: TmySQLQuery;
    DsCunsImgCmt_: TDataSource;
    QrCunsImgCmt_TXT_GENERIC: TWideMemoField;
    QrCunsImgCmt_Codigo: TIntegerField;
    QrCunsImgCmt_Controle: TIntegerField;
    QrCunsImgCmt_Conta: TIntegerField;
    QrCunsImgCmt_TxtGeneric: TIntegerField;
    QrCunsImgCmt_Nome: TWideStringField;
    PMCunsImgCmt_: TPopupMenu;
    IncluiNovoTexto1: TMenuItem;
    ExcluiTextoAtual1: TMenuItem;
    AlteraTextoAtual1: TMenuItem;
    CSTabSheetChamou: TdmkCompoStore;
    QrCunsImgCabDependencia: TIntegerField;
    QrCunsImgCabNO_DEPENDENCIA: TWideStringField;
    PCSiapTerCad: TPageControl;
    TabSheet25: TTabSheet;
    Panel38: TPanel;
    Panel17: TPanel;
    DBGSiapImaCad: TDBGrid;
    GroupBox6: TGroupBox;
    PGDadosLocaisAplic: TPageControl;
    TabSheet13: TTabSheet;
    Panel01: TPanel;
    DBGSiapImaDep: TDBGrid;
    Panel34: TPanel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    TabSheet14: TTabSheet;
    Panel22: TPanel;
    DBGSiapImaCdi: TDBGrid;
    TabSheet17: TTabSheet;
    Panel23: TPanel;
    DBGSiapImaCav: TDBGrid;
    TabSheet18: TTabSheet;
    Panel24: TPanel;
    DBGrid7: TDBGrid;
    DBGSiapImaRes: TDBGrid;
    TabSheet19: TTabSheet;
    Panel25: TPanel;
    DBGSiapImaCui: TDBGrid;
    TabSheet20: TTabSheet;
    Panel26: TPanel;
    DBGSiapImaAti: TDBGrid;
    TabSheet21: TTabSheet;
    Panel27: TPanel;
    DBGSiapImaCxa: TDBGrid;
    PGCaixaDAgua: TPageControl;
    TabSheet22: TTabSheet;
    TabSheet23: TTabSheet;
    DBGAtrSICxDef: TdmkDBGrid;
    TabSheet24: TTabSheet;
    DBGAtrSICdDef: TdmkDBGrid;
    TabSheet27: TTabSheet;
    QrAtrSTCdDef: TmySQLQuery;
    DsAtrSTCdDef: TDataSource;
    QrAtrSTCdDefID_Item: TIntegerField;
    QrAtrSTCdDefID_Sorc: TIntegerField;
    QrAtrSTCdDefAtrCad: TIntegerField;
    QrAtrSTCdDefATRITS: TFloatField;
    QrAtrSTCdDefCU_CAD: TIntegerField;
    QrAtrSTCdDefCU_ITS: TLargeintField;
    QrAtrSTCdDefAtrTxt: TWideStringField;
    QrAtrSTCdDefNO_CAD: TWideStringField;
    QrAtrSTCdDefNO_ITS: TWideStringField;
    QrAtrSTCdDefAtrTyp: TSmallintField;
    DBGAtrSTCdDef: TdmkDBGrid;
    PMAtrSTCdDef: TPopupMenu;
    IncluiAtrSTCdDef1: TMenuItem;
    AlteraAtrSTCdDef1: TMenuItem;
    ExcluiAtrSTCdDef1: TMenuItem;
    N3: TMenuItem;
    Duplicaritem1: TMenuItem;
    QrCunsImgCabCodiNCT: TIntegerField;
    QrCunsImgCabObserv: TWideMemoField;
    QrCunsImgCabAplicacao: TIntegerField;
    QrCunsImgCabNO_NCT: TWideStringField;
    QrCunsImgCabTxtCAC: TWideStringField;
    PMImprime: TPopupMenu;
    FichadoLugar1: TMenuItem;
    NCTCAC1: TMenuItem;
    frxCAD_SUBCL_001_001_A: TfrxReport;
    QrCIC_Imp: TmySQLQuery;
    QrCIC_ImpCodigo: TIntegerField;
    QrCIC_ImpControle: TIntegerField;
    QrCIC_ImpNome: TWideStringField;
    QrCIC_ImpStatus: TIntegerField;
    QrCIC_ImpCaminho: TWideStringField;
    QrCIC_ImpNO_STATUS: TWideStringField;
    QrCIC_ImpDependencia: TIntegerField;
    QrCIC_ImpNO_DEPENDENCIA: TWideStringField;
    QrCIC_ImpCodiNCT: TIntegerField;
    QrCIC_ImpObserv: TWideMemoField;
    QrCIC_ImpAplicacao: TIntegerField;
    QrCIC_ImpNO_NCT: TWideStringField;
    QrCIC_ImpTxtCAC: TWideStringField;
    frxDsCIC_Imp: TfrxDBDataset;
    QrCIC_ImpNO_LOCAL: TWideStringField;
    QrMixRel: TmySQLQuery;
    QrMixRelFC: TFloatField;
    QrMixRelNCT: TFloatField;
    QrSiapTerCadSTe1: TWideStringField;
    CkImpImaInfo: TdmkCheckBox;
    QrCunsCadImpImaInfo: TSmallintField;
    QrSubCliFANTASIA: TWideStringField;
    TabSheet30: TTabSheet;
    PnFmPnAnotacoes: TPanel;
    QrCunsImgCabOrdem: TIntegerField;
    QrCIC_ImpOrdem: TIntegerField;
    TabSheet31: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrSTCAgeEve: TmySQLQuery;
    DsSTCAgeEve: TDataSource;
    QrSTCAgeEveCodigo: TIntegerField;
    QrSTCAgeEveQuestaoTyp: TIntegerField;
    QrSTCAgeEveQuestaoCod: TIntegerField;
    QrSTCAgeEveQuestaoExe: TIntegerField;
    QrSTCAgeEveInicio: TDateTimeField;
    QrSTCAgeEveTermino: TDateTimeField;
    QrSTCAgeEveEmprEnti: TIntegerField;
    QrSTCAgeEveTerceiro: TIntegerField;
    QrSTCAgeEveLocal: TIntegerField;
    QrSTCAgeEveCor: TIntegerField;
    QrSTCAgeEveCption: TSmallintField;
    QrSTCAgeEveNome: TWideStringField;
    QrSTCAgeEveNotas: TWideStringField;
    QrSTCAgeEveFatoGeradr: TIntegerField;
    QrSTCAgeEveLk: TIntegerField;
    QrSTCAgeEveDataCad: TDateField;
    QrSTCAgeEveDataAlt: TDateField;
    QrSTCAgeEveUserCad: TIntegerField;
    QrSTCAgeEveUserAlt: TIntegerField;
    QrSTCAgeEveAlterWeb: TSmallintField;
    QrSTCAgeEveAtivo: TSmallintField;
    PMSTCAgeEve: TPopupMenu;
    Excluiragendamento1: TMenuItem;
    GroupBox2: TGroupBox;
    SbSelfGer2: TBitBtn;
    SbProvidencias: TBitBtn;
    BtOSs: TBitBtn;
    BtDiarioGer2: TBitBtn;
    TabSheet32: TTabSheet;
    PnFinancas: TPanel;
    TabSheet33: TTabSheet;
    Panel41: TPanel;
    GroupBox10: TGroupBox;
    Panel42: TPanel;
    DBCheckBox1: TDBCheckBox;
    TabSheet34: TTabSheet;
    DBGSiapTerFlh: TDBGrid;
    PMSiapTerFlh: TPopupMenu;
    IncluiDsSiapTerFlh1: TMenuItem;
    AlteraDsSiapTerFlh1: TMenuItem;
    ExcluiDsSiapTerFlh1: TMenuItem;
    QrSiapTerFlh: TmySQLQuery;
    QrSiapTerFlhCodigo: TIntegerField;
    QrSiapTerFlhControle: TIntegerField;
    QrSiapTerFlhEmpresa: TIntegerField;
    QrSiapTerFlhMonAgeEqi: TIntegerField;
    QrSiapTerFlhMonEntCtt: TIntegerField;
    QrSiapTerFlhMonNumCtr: TIntegerField;
    QrSiapTerFlhMonEntCtr: TIntegerField;
    QrSiapTerFlhMonEntPag: TIntegerField;
    QrSiapTerFlhMonCondPg: TIntegerField;
    QrSiapTerFlhMonCrtEmi: TIntegerField;
    DsSiapTerFlh: TDataSource;
    QrSiapTerFlhDataSincOS: TDateField;
    TabSheet35: TTabSheet;
    Panel43: TPanel;
    GroupBox11: TGroupBox;
    Panel44: TPanel;
    Label35: TLabel;
    QrSiapTerCadNO_LSTCUSPRD: TWideStringField;
    QrSiapTerCadLstCusPrd: TIntegerField;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    TabSheet36: TTabSheet;
    PnOSPrvGer: TPanel;
    TabSheet37: TTabSheet;
    PnOSs: TPanel;
    QrOSCab: TmySQLQuery;
    QrOSCabNO_AgeEqiCab: TWideStringField;
    QrOSCabNO_MUL_SRV: TWideStringField;
    QrOSCabEstatus_TXT: TWideStringField;
    QrOSCabNO_LUGAR: TWideStringField;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEmpresa: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabDtaLibFat: TDateTimeField;
    QrOSCabDtaFimFat: TDateTimeField;
    QrOSCabValorServi: TFloatField;
    QrOSCabValorDesco: TFloatField;
    QrOSCabValorOutrs: TFloatField;
    QrOSCabValorTotal: TFloatField;
    QrOSCabDdsPosVda: TIntegerField;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    QrOSCabEntContrat: TIntegerField;
    QrOSCabCondicaoPg: TIntegerField;
    QrOSCabCartEmis: TIntegerField;
    QrOSCabSerNF: TWideStringField;
    QrOSCabNumNF: TIntegerField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabInvalServi: TFloatField;
    QrOSCabInvalDesco: TFloatField;
    QrOSCabInvalOutrs: TFloatField;
    QrOSCabInvalTotal: TFloatField;
    QrOSCabOrcamServi: TFloatField;
    QrOSCabOrcamDesco: TFloatField;
    QrOSCabOrcamOutrs: TFloatField;
    QrOSCabOrcamTotal: TFloatField;
    QrOSCabValiDdOrca: TIntegerField;
    QrOSCabOperacao: TIntegerField;
    QrOSCabExeTxtCli1: TIntegerField;
    QrOSCabValorPre: TFloatField;
    QrOSCabObsGaranti: TWideMemoField;
    QrOSCabExeTxtCli2: TIntegerField;
    QrOSCabFimVisPrv: TDateTimeField;
    QrOSCabFimVisExe: TDateTimeField;
    QrOSCabFimExePrv: TDateTimeField;
    QrOSCabGrupo: TIntegerField;
    QrOSCabNumero: TIntegerField;
    QrOSCabOpcao: TIntegerField;
    QrOSCabOptado: TSmallintField;
    QrOSCabHowGerou: TSmallintField;
    QrOSCabPosGerou: TSmallintField;
    QrOSCabOSFlhUltGe: TIntegerField;
    QrOSCabMulServico: TLargeintField;
    QrOSCabAgeEqiCab: TIntegerField;
    QrOSCabOSFlhGrCab: TIntegerField;
    QrOSCabOSFlhGrIts: TIntegerField;
    QrOSCabSohInicial: TIntegerField;
    QrOSCabStPipAdPrg: TSmallintField;
    QrOSCabNO_EMPRESA: TWideStringField;
    QrOSCabNO_ENTCONTRAT: TWideStringField;
    QrOSCabNO_ENTPAGANTE: TWideStringField;
    QrOSCabAF_ENTCONTRAT: TWideStringField;
    QrOSCabAF_ENTPAGANTE: TWideStringField;
    QrOSCabNO_ENTICONTAT: TWideStringField;
    DsOSCab: TDataSource;
    DBGPsq: TdmkDBGridZTO;
    Di�rio: TTabSheet;
    PnDiarioGer2: TPanel;
    Contratos: TTabSheet;
    PnContratos: TPanel;
    QrContratos: TmySQLQuery;
    QrContratosCodigo: TIntegerField;
    QrContratosNome: TWideStringField;
    QrContratosDContrato: TDateField;
    QrContratosDInstal: TDateField;
    QrContratosDVencimento: TDateField;
    QrContratosDtaPrxRenw: TDateField;
    QrContratosDtaCntrFim: TDateField;
    QrContratosStatus: TSmallintField;
    QrContratosValorMes: TFloatField;
    QrContratosLugarNome: TWideStringField;
    QrContratosVersao: TFloatField;
    QrContratosTXT_STATUS: TWideStringField;
    dmkDBGridZTO2: TdmkDBGridZTO;
    DsContratos: TDataSource;
    TabSheet38: TTabSheet;
    PnDocsCab: TPanel;
    TsFotos: TTabSheet;
    Panel35: TPanel;
    Panel36: TPanel;
    Splitter3: TSplitter;
    Panel37: TPanel;
    Panel39: TPanel;
    ImgFoto: TImage;
    PG_NCT_CAC: TPageControl;
    TabSheet29: TTabSheet;
    DBMemo2: TDBMemo;
    TabSheet39: TTabSheet;
    Panel40: TPanel;
    Label32: TLabel;
    Label34: TLabel;
    DBMemo3: TDBMemo;
    DBMemo4: TDBMemo;
    DBGCunsImgCab: TdmkDBGridZTO;
    Memo1: TMemo;
    Splitter4: TSplitter;
    QrCIC_ImpNO_STC: TWideStringField;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel30: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    QrSiapTerCadPdrMntsMon: TIntegerField;
    QrSTCAgeEveTEXTO: TWideStringField;
    QrSTCAgeEveNO_FATOGERADR: TWideStringField;
    QrSiapTerFlhTrajetDist: TIntegerField;
    QrSiapTerFlhTrajetKm_h: TSmallintField;
    Panel12: TPanel;
    CkAgeSoDoLugar: TCheckBox;
    QrOSCabNO_OPERACOES: TWideStringField;
    TabSheet28: TTabSheet;
    QrPMVs: TmySQLQuery;
    DsPMVs: TDataSource;
    DBGPMVs: TdmkDBGridZTO;
    QrPMVsNO_MOTDESAT: TWideStringField;
    QrPMVsNO_EQUI: TWideStringField;
    QrPMVsNO_LISTA: TWideStringField;
    QrPMVsNO_DEPENDENCIA: TWideStringField;
    QrPMVsDtaAquis_TXT: TWideStringField;
    QrPMVsDtaDesativ_TXT: TWideStringField;
    QrPMVsCodigo: TIntegerField;
    QrPMVsNome: TWideStringField;
    QrPMVsEquipamento: TIntegerField;
    QrPMVsOSMonCab: TIntegerField;
    QrPMVsMotDesativ: TIntegerField;
    QrPMVsDtaAquis: TDateField;
    QrPMVsDtaDesativ: TDateField;
    QrPMVsLk: TIntegerField;
    QrPMVsDataCad: TDateField;
    QrPMVsDataAlt: TDateField;
    QrPMVsUserCad: TIntegerField;
    QrPMVsUserAlt: TIntegerField;
    QrPMVsAlterWeb: TSmallintField;
    QrPMVsAtivo: TSmallintField;
    QrPMVsPrgLstCab: TIntegerField;
    QrPMVsDependenci: TIntegerField;
    QrPMVsOrdem: TIntegerField;
    QrPMVsReordem: TIntegerField;
    PMPMVs: TPopupMenu;
    AlteraPMVatual1: TMenuItem;
    QrOSCabLstUplWeb: TDateTimeField;
    QrOSCabLCPUsed: TIntegerField;
    QrOSCabConta: TIntegerField;
    QrOSCabObsExecuta: TWideMemoField;
    QrOSCabNO_USER_CAD: TWideStringField;
    QrOSCabNO_USER_ALT: TWideStringField;
    QrOSCabNO_WOW_GEROU: TWideStringField;
    QrOSCabNO_POS_GEROU: TWideStringField;
    QrOSCabNO_SOH_INICIAL: TWideStringField;
    QrOSCabNO_ST_PIP_AD_PRG: TWideStringField;
    QrSiapTerFlhMinHAgeExe: TTimeField;
    Panel45: TPanel;
    TabSheet40: TTabSheet;
    DBGSiapImaSVG: TDBGrid;
    QrSiapImaSVG: TmySQLQuery;
    DsSiapImaSVG: TDataSource;
    QrSiapImaSVGCodigo: TIntegerField;
    QrSiapImaSVGControle: TIntegerField;
    QrSiapImaSVGNome: TWideStringField;
    PMSiapImaSVG: TPopupMenu;
    Carreganovoarquivo1: TMenuItem;
    Alteraadescrio1: TMenuItem;
    Recarrega1: TMenuItem;
    QrSiapImaSVGNoArq: TWideStringField;
    QrSiapTerCadLatitude: TFloatField;
    QrSiapTerCadLongitude: TFloatField;
    QrSiapTerCadRotaLatLon: TIntegerField;
    N4: TMenuItem;
    Visualizarcroqui1: TMenuItem;
    Alterarintervalospsteros1: TMenuItem;
    Intervalospsteros1: TMenuItem;
    Periododemonitoramento1: TMenuItem;
    QrPMVsDdPostero: TIntegerField;
    QrPMVsPerioDd: TIntegerField;
    Excluiarquivoatual1: TMenuItem;
    N5: TMenuItem;
    QrSiapImaSVGRotarGraus: TIntegerField;
    QrSiapImaSVGNO_RotarGraus: TWideStringField;
    GroupBox12: TGroupBox;
    MeANL: TMemo;
    TSWeb: TTabSheet;
    Panel46: TPanel;
    BtWUsers: TBitBtn;
    dmkDBGrid2: TdmkDBGrid;
    QrWUsers: TmySQLQuery;
    QrWUsersCodigo: TIntegerField;
    QrWUsersPerfil_TXT: TWideStringField;
    QrWUsersPersonalName: TWideStringField;
    QrWUsersUsername: TWideStringField;
    QrWUsersEmail: TWideStringField;
    QrWUsersAtivo: TSmallintField;
    DsWUsers: TDataSource;
    N6: TMenuItem;
    Inclui1: TMenuItem;
    LaContratanteAviso: TLabel;
    BtEntidade: TBitBtn;
    QrEntidadesCOMPLENT: TWideStringField;
    QrCunsCadNOMEENTIDADE: TWideStringField;
    Panel47: TPanel;
    BtCroMenu: TBitBtn;
    BtCroVis: TBitBtn;
    ScrollBox1: TScrollBox;
    Label13: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    dmkLabelRotate1: TdmkLabelRotate;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBMemo1: TDBMemo;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCunsCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCunsCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrCunsCadAfterScroll(DataSet: TDataSet);
    procedure BtCunsCadClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure QrEntidadesAfterScroll(DataSet: TDataSet);
    procedure QrEntiContatAfterScroll(DataSet: TDataSet);
    procedure QrEntiContatBeforeClose(DataSet: TDataSet);
    procedure QrEntiResponCalcFields(DataSet: TDataSet);
    procedure QrEntiTelCalcFields(DataSet: TDataSet);
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure QrEntidadesCalcFields(DataSet: TDataSet);
    procedure QrCunsCadBeforeClose(DataSet: TDataSet);
    procedure QrSiapTerCadBeforeClose(DataSet: TDataSet);
    procedure QrSiapTerCadAfterScroll(DataSet: TDataSet);
    procedure QrSiapImaCadBeforeClose(DataSet: TDataSet);
    procedure QrSiapImaCadAfterScroll(DataSet: TDataSet);
    procedure AdicionaContratante1Click(Sender: TObject);
    procedure RetiraContratante1Click(Sender: TObject);
    procedure IncluiTerCad1Click(Sender: TObject);
    procedure AlteraTerCad1Click(Sender: TObject);
    procedure PMCunsItsPopup(Sender: TObject);
    procedure PMSiapTerCadPopup(Sender: TObject);
    procedure ExcluiterCad1Click(Sender: TObject);
    procedure PMSiapImaCadPopup(Sender: TObject);
    procedure DBGSiapImaCadMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiSiapImaCad1Click(Sender: TObject);
    procedure AlteraSiapImaCad1Click(Sender: TObject);
    procedure PMSiapImaDepPopup(Sender: TObject);
    procedure IncluiSiapImaDep1Click(Sender: TObject);
    procedure AlteraSiapImaDep1Click(Sender: TObject);
    procedure ExcluiSiapImaDep1Click(Sender: TObject);
    procedure ExcluiSiapImaCad1Click(Sender: TObject);
    procedure PMSiapImaCdiPopup(Sender: TObject);
    procedure IncluiSiapImaCdi1Click(Sender: TObject);
    procedure AlteraSiapImaCdi1Click(Sender: TObject);
    procedure ExcluiSiapImaCdi1Click(Sender: TObject);
    procedure IncluiSiapImaCav1Click(Sender: TObject);
    procedure AlteraSiapImaCav1Click(Sender: TObject);
    procedure ExcluiSiapImaCav1Click(Sender: TObject);
    procedure PMSiapImaCavPopup(Sender: TObject);
    procedure IncluiSiapImaRes1Click(Sender: TObject);
    procedure AlteraSiapImaRes1Click(Sender: TObject);
    procedure ExcluiSiapImaRes1Click(Sender: TObject);
    procedure PMSiapImaResPopup(Sender: TObject);
    procedure IncluiSiapImaCui1Click(Sender: TObject);
    procedure AlteraSiapImaCui1Click(Sender: TObject);
    procedure ExcluiSiapImaCui1Click(Sender: TObject);
    procedure PMSiapImaCuiPopup(Sender: TObject);
    procedure PMSiapImaAtiPopup(Sender: TObject);
    procedure IncluiSiapImaAti1Click(Sender: TObject);
    procedure AlteraSiapImaAti1Click(Sender: TObject);
    procedure ExcluiSiapImaAti1Click(Sender: TObject);
    procedure PMSiapImaCxaPopup(Sender: TObject);
    procedure ExcluiSiapImaCxa1Click(Sender: TObject);
    procedure IncluiSiapImaCxa1Click(Sender: TObject);
    procedure AlteraSiapImaCxa1Click(Sender: TObject);
    procedure IncluiAtrSICxDef1Click(Sender: TObject);
    procedure AlteraAtrSICxDef1Click(Sender: TObject);
    procedure ExcluiAtrSICxDef1Click(Sender: TObject);
    procedure QrSiapImaCxaBeforeClose(DataSet: TDataSet);
    procedure QrSiapImaCxaAfterScroll(DataSet: TDataSet);
    procedure IncluiAtrSICdDef1Click(Sender: TObject);
    procedure AlteraAtrSICdDef1Click(Sender: TObject);
    procedure ExcluiAtrSICdDef1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCunsItsClick(Sender: TObject);
    procedure BtSiapImaCadClick(Sender: TObject);
    procedure BtSiapImaDepClick(Sender: TObject);
    procedure BtSiapImaCdiClick(Sender: TObject);
    procedure BtSiapImaCavClick(Sender: TObject);
    procedure BtSiapImaResClick(Sender: TObject);
    procedure BtSiapImaCuiClick(Sender: TObject);
    procedure BtSiapImaAtiClick(Sender: TObject);
    procedure BtSiapImaCxaClick(Sender: TObject);
    procedure BtAtrSICxDefClick(Sender: TObject);
    procedure BtAtrSICdDefClick(Sender: TObject);
    procedure BtSiapTerCadClick(Sender: TObject);
    procedure PGDadosLocaisAplicChange(Sender: TObject);
    procedure QrSiapImaCadAfterOpen(DataSet: TDataSet);
    procedure QrSiapTerCadAfterOpen(DataSet: TDataSet);
    procedure QrSiapImaCadAfterClose(DataSet: TDataSet);
    procedure QrSiapImaCxaAfterOpen(DataSet: TDataSet);
    procedure PeloNome1Click(Sender: TObject);
    procedure CNPJ1Click(Sender: TObject);
    procedure Dados1Click(Sender: TObject);
    procedure DBGSiapTerCadCellClick(Column: TColumn);
    procedure DBGMovAmovCadCellClick(Column: TColumn);
    procedure DBGAtrAMovDefMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiAtrAMovDef1Click(Sender: TObject);
    procedure AlteraAtrAMovDef1Click(Sender: TObject);
    procedure ExcluiAtrAMovDef1Click(Sender: TObject);
    procedure Incluinovomvelautomvel1Click(Sender: TObject);
    procedure Alteramvelautomvel1Click(Sender: TObject);
    procedure Excluimvelautomvel1Click(Sender: TObject);
    procedure QrMovAmovCadBeforeClose(DataSet: TDataSet);
    procedure QrMovAmovCadAfterScroll(DataSet: TDataSet);
    procedure DBGMovAmovCadMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGSiapTerCadMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGSiapTerCadEnter(Sender: TObject);
    procedure DBGMovAmovCadEnter(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure DuplicSiapImaCxa1Click(Sender: TObject);
    procedure QrSiapImaDepBeforeClose(DataSet: TDataSet);
    procedure QrSiapImaDepAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGCunsImgCabMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluinovaFoto1Click(Sender: TObject);
    procedure QrCunsImgCabBeforeClose(DataSet: TDataSet);
    procedure QrCunsImgCabAfterScroll(DataSet: TDataSet);
    procedure DBGCunsImgCmtMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiNovoTexto1Click(Sender: TObject);
    procedure AlteraFotoatual1Click(Sender: TObject);
    procedure Excluifotoatual1Click(Sender: TObject);
    procedure ExcluiTextoAtual1Click(Sender: TObject);
    procedure AlteraTextoAtual1Click(Sender: TObject);
    procedure PMCunsImgCabPopup(Sender: TObject);
    procedure PMCunsImgCmt_Popup(Sender: TObject);
    procedure DBGAtrSTCdDefMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiAtrSTCdDef1Click(Sender: TObject);
    procedure AlteraAtrSTCdDef1Click(Sender: TObject);
    procedure ExcluiAtrSTCdDef1Click(Sender: TObject);
    procedure PMAtrSTCdDefPopup(Sender: TObject);
    procedure Duplicaritem1Click(Sender: TObject);
    procedure QrCunsImgCabAfterOpen(DataSet: TDataSet);
    procedure NCTCAC1Click(Sender: TObject);
    procedure FichadoLugar1Click(Sender: TObject);
    procedure frxCAD_SUBCL_001_001_AGetValue(const VarName: string;
      var Value: Variant);
    procedure DBGSiapImaDepMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGSiapImaCdiMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGSiapImaCavMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGSiapImaResMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGSiapImaCuiMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGSiapImaAtiMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGAtrSICxDefMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGSiapImaCxaMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGAtrSICdDefMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Excluiragendamento1Click(Sender: TObject);
    procedure SbSelfGer2Click(Sender: TObject);
    procedure IncluiDsSiapTerFlh1Click(Sender: TObject);
    procedure AlteraDsSiapTerFlh1Click(Sender: TObject);
    procedure ExcluiDsSiapTerFlh1Click(Sender: TObject);
    procedure PMSiapTerFlhPopup(Sender: TObject);
    procedure QrSiapTerFlhDataSincOSGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure SbProvidenciasClick(Sender: TObject);
    procedure DBGPsqDblClick(Sender: TObject);
    procedure BtOSsClick(Sender: TObject);
    procedure PCDadosChange(Sender: TObject);
    procedure BtDiarioGer2Click(Sender: TObject);
    procedure dmkDBGridZTO2DblClick(Sender: TObject);
    procedure QrSTCAgeEveCalcFields(DataSet: TDataSet);
    procedure CkAgeSoDoLugarClick(Sender: TObject);
    procedure AlteraPMVatual1Click(Sender: TObject);
    procedure DBGPMVsCellClick(Column: TColumn);
    procedure Carreganovoarquivo1Click(Sender: TObject);
    procedure Recarrega1Click(Sender: TObject);
    procedure Alteraadescrio1Click(Sender: TObject);
    procedure DBGSiapImaSVGDblClick(Sender: TObject);
    procedure Visualizarcroqui1Click(Sender: TObject);
    procedure Intervalospsteros1Click(Sender: TObject);
    procedure Periododemonitoramento1Click(Sender: TObject);
    procedure Excluiarquivoatual1Click(Sender: TObject);
    procedure PMSiapImaSVGPopup(Sender: TObject);
    procedure BtWUsersClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtEntidadeClick(Sender: TObject);
    procedure BtCroMenuClick(Sender: TObject);
    procedure BtCroVisClick(Sender: TObject);
    procedure PMPMVsPopup(Sender: TObject);
  private
    FUsrWeb: Boolean;
    FMySelfGer2: TForm; //TFmSelfGer2;
    FOSPrvGer: TForm; //TFmOSPrvGer
    FDiarioGer2: TForm; //TFmDiarioGer2
    F_Cuns_Img_Cab: String;
    FThisFmPnAnotacoes: TForm;
    FThisFmDocsCab: TForm;
    //
    procedure AlterarIntervalosOuPeriodo(CampoMonit: TCampoMonit);
    procedure CarregaArquivoSVG(SQLType: TSQLType; Campo: String);
    procedure CarregaImagem();
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    function  MontaEndereco(): String;
    //
    procedure MostraFormSiapTerCad(SQLType: TSQLType);
    procedure MostraFormAtrSTCdDef(SQLType: TSQLType);
    procedure MostraFormSiapImaCad(SQLType: TSQLType);
    procedure MostraFormSiapImaDep(SQLType: TSQLType);
    procedure MostraFormSiapImaCdi(SQLType: TSQLType);
    procedure MostraFormSiapImaCav(SQLType: TSQLType);
    procedure MostraFormSiapImaRes(SQLType: TSQLType);
    procedure MostraFormSiapImaCui(SQLType: TSQLType);
    procedure MostraFormSiapImaAti(SQLType: TSQLType);
    procedure MostraFormSiapImaCxa(SQLType: TSQLType);
    procedure MostraFormSiapImaSVG(SQLType: TSQLType);
    procedure MostraFormMovAmovCad(SQLType: TSQLType);
    procedure MostraFormAtrAMovDef(SQLType: TSQLType);
    procedure MostraFormAtrSICdDef(SQLType: TSQLType);
    procedure MostraFormAtrSICxDef(SQLType: TSQLType);
    procedure MostraFormCunsImgCab(SQLType: TSQLType);
    //procedure MostraFormCunsImgCmt(SQLType: TSQLType);
    //
    procedure MostraPaneledDiarioGer2();
    procedure MostraPaneledOSPrvGer();
    procedure MostraPeneledSelfGer2();
    //
    procedure ReopenEntidade();
    procedure ReopenEntiContat(Controle: Integer);
    procedure ReopenEntiRespon(Controle: Integer);
    procedure ReopenEntiMail(Conta: Integer);
    procedure ReopenEntiTel(Conta: Integer);
    procedure ReopenEntDefAtr(Controle: Integer);
    procedure ReopenEntiCtas();

    procedure ReopenOSCab();
    procedure ReopenContratos();
    //
    function  Loc_SQLExtra(): String;
    procedure VerificaHabilita();
    procedure ReopenWUsers();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure LocCodBySiapTerCad(Codigo: Integer);
    //procedure InsAltSiapImaDep(SQLType: TSQLType);
    //
    procedure ReopenCunsIts(CunsGru: Integer);
    procedure ReopenCunsImgCab(Controle: Integer);
    //procedure ReopenCunsImgCmt(Controle: Integer);
    procedure ReopenMovAmovCad(Codigo: Integer);
    procedure ReopenSiapTerCad(Codigo: Integer);
    procedure ReopenSiapterFlh(Controle: Integer);
    procedure ReopenSiapImaCad(Codigo: Integer);
    procedure ReopenSiapImaDep(Controle: Integer);
    procedure ReopenSiapImaCdi(Controle: Integer);
    procedure ReopenSiapImaCav(Controle: Integer);
    procedure ReopenSiapImaRes(Controle: Integer);
    procedure ReopenSiapImaCui(Controle: Integer);
    procedure ReopenSiapImaAti(Controle: Integer);
    procedure ReopenSiapImaCxa(Controle: Integer);
    procedure ReopenSiapImaSVG(Controle: Integer);
    //
    procedure ReopenSubCli(CunsSub: Integer);
    //
    procedure ReopenAtrSTCdDef(ID_Item: Integer);
    procedure ReopenAtrAMovDef(ID_Item: Integer);
    procedure ReopenAtrSICdDef(ID_Item: Integer);
    procedure ReopenAtrSICxDef(ID_Item: Integer);
    procedure ReopenSTCAgeEve(Codigo: Integer);
    procedure ReopenPMVs(Codigo: Integer);
    //
    procedure MostraFormSiapTerFlh(SQLType: TSQLType);
    procedure ReopenCIC_Imp(Codigo: Integer);
    function  DefineArquivo(): String;
    //
    procedure ConfiguraAbreCroqui();
    procedure ConfiguraAbreSiapTerFlh();
  end;

var
  FmCunsCad: TFmCunsCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, CfgCadLista, SiapTerCad, SiapImaCad,
  SiapImaCxa, Principal, CfgAtributos, SiapImaRes, ModuleGeral, Entidade2Imp,
  MovAmovCad, SiapImaDep, ModOS, OsImp2, MyGlyfs, DmkDAC_PF, CunsImgCab,
  SiapImaCopy, CreateBugs, UnitAnotacoes, UnBugs_Tabs, SelfGer2, ModuleLct2,
  SiapTerFlh, OSPrvGer, OSCab2, DiarioGer2, Contratos, UnContrat_Tabs, DocsCab,
  UnitDocsCab, UnOSApp_PF, LugarPesq, CunsCadSVG1, GetValor, SiapImaSVG,
  UnDmkWeb, UnWUsersJan, UnEntities, UnAgendaGerAll, UnOSAll_PF, MyListas,
  UnAnotacoes_Tabs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCunsCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCunsCad.LocCodBySiapTerCad(Codigo: Integer);
var
  Query: TmySQLQuery;
  Cliente: Integer;
begin
  Query := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
      'SELECT Cliente ',
      'FROM siaptercad ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    if Query.RecordCount > 0 then
    begin
      Cliente := Query.FieldByName('Cliente').AsInteger;

      LocCod(Cliente, Cliente);
      ReopenSiapTerCad(Codigo);
    end;
  finally
    Query.Free;
  end;
end;

function TFmCunsCad.Loc_SQLExtra: String;
begin
  Result := Geral.ATS([
  'AND Codigo IN ( ',
  '   SELECT Codigo FROM cunscad ',
  ') ',
  '']);
end;

function TFmCunsCad.MontaEndereco(): String;
{
var
  E_ALL, NUMERO_TXT, Logr: String;
  I, LowerC, UpperC: Integer;
}
begin
  Result := Entities.MontaEndereco(QrSiapTerCadNOMELOGRAD.Value,
  QrSiapTerCadSRua.Value, QrSiapTerCadSNumero.Value, QrSiapTerCadSCompl.Value,
  QrSiapTerCadSBairro.Value, QrSiapTerCadSCidade.Value, QrSiapTerCadSUF.Value,
  QrSiapTerCadSPais.Value, QrSiapTerCadSCEP.Value);
{
  // Primeira linha:
  NUMERO_TXT :=
    MLAGeral.FormataNumeroDeRua(QrSiapTerCadSRua.Value, QrSiapTerCadSNumero.Value, False);
  E_ALL := '';
  //
  E_ALL := E_ALL + QrSiapTerCadSRua.Value;
  if Trim(QrSiapTerCadSRua.Value) <> '' then E_ALL :=
    E_ALL + ', ' + NUMERO_TXT;
  if Trim(QrSiapTerCadSCompl.Value) <>  '' then E_ALL :=
    E_ALL + ' ' + QrSiapTerCadSCompl.Value;
  E_ALL := E_ALL + sLineBreak;
  //
  // Segunda Linha
  if Trim(QrSiapTerCadSBairro.Value) <>  '' then E_ALL :=
    E_ALL + 'BAIRRO: ' + QrSiapTerCadSBairro.Value;
  E_ALL := E_ALL + sLineBreak;
  //
  // terceira Linha
  if QrSiapTerCadSCEP.Value > 0 then E_ALL :=
    E_ALL + 'CEP ' +Geral.FormataCEP_NT(QrSiapTerCadSCEP.Value);
  if Trim(QrSiapTerCadSCidade.Value) <>  '' then E_ALL :=
    E_ALL + '  ' + QrSiapTerCadSCidade.Value;
  if Trim(QrSiapTerCadSUF.Value) <>  '' then E_ALL :=
    E_ALL + ', ' + QrSiapTerCadSUF.Value;
  if Trim(QrSiapTerCadSPais.Value) <>  '' then E_ALL :=
    E_ALL + ' - ' + QrSiapTerCadSPais.Value;
  //
  Logr := Trim(QrSiapTerCadNOMELOGRAD.Value);
  if Logr <> '' then
  begin
    LowerC := 0;
    UpperC := 0;
    for I := 0 to Length(E_ALL) do
    begin
      if Ord(E_ALL[i]) in [65..90] then
        UpperC := UpperC + 1
      else
      if Ord(E_ALL[i]) in [97..122] then
        LowerC := LowerC + 1;
    end;
    if UpperC > LowerC then
      Logr := AnsiUpperCase(Logr);
    E_ALL := Logr + ' ' + E_ALL;
  end;
  Result := E_ALL;
}
end;

procedure TFmCunsCad.MostraFormAtrAMovDef(SQLType: TSQLType);
begin
  UnCfgAtributos.InsAltAtrDef_ITS(QrMovAmovCadCodigo.Value,
    QrAtrAMovDefID_Item.Value, 'atramovdef', 'atramovcad', 'atramovits', '',
    SQLType, QrAtrAMovDefAtrCad.Value, QrAtrAMovDefAtrIts.Value, '', QrAtrAMovDef,
    nil, False);
end;

procedure TFmCunsCad.MostraFormAtrSICdDef(SQLType: TSQLType);
begin
  UnCfgAtributos.InsAltAtrDef_ITS(QrSiapImaCadCodigo.Value,
    QrAtrSICdDefID_Item.Value, 'atrsicddef', 'atrsicdcad', 'atrsicdits', '',
    SQLType, QrAtrSICdDefAtrCad.Value, QrAtrSICdDefAtrIts.Value, '', QrAtrSICdDef,
    nil, False);
end;

procedure TFmCunsCad.MostraFormAtrSICxDef(SQLType: TSQLType);
begin
  UnCfgAtributos.InsAltAtrDef_ITS(QrSiapImaCxaControle.Value,
    QrAtrSICxDefID_Item.Value, 'atrsicxdef', 'atrsicxcad', 'atrsicxits', '',
    SQLType, QrAtrSICxDefAtrCad.Value, QrAtrSICxDefAtrIts.Value, '', QrAtrSICxDef,
    nil, False);
end;

procedure TFmCunsCad.MostraFormAtrSTCdDef(SQLType: TSQLType);
begin
  UnCfgAtributos.InsAltAtrDef_ITS(QrSiapTerCadCodigo.Value,
    QrAtrSTCdDefID_Item.Value, 'atrstcddef', 'atrstcdcad', 'atrstcdits',
    'atrstcdtxt', SQLType, QrAtrSTCdDefAtrCad.Value,
    Trunc(QrAtrSTCdDefAtrIts.Value), QrAtrSTCdDefAtrTxt.Value, QrAtrSTCdDef,
    QrAtrSTCdDef, True);
end;

procedure TFmCunsCad.MostraFormCunsImgCab(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCunsImgCab, FmCunsImgCab, afmoNegarComAviso) then
  begin
    FmCunsImgCab.ImgTipo.SQLType := SQLType;
    FmCunsImgCab.ReopenDependencias(QrCunsCadCodigo.Value);
    FmCunsImgCab.FQrCab          := QrCunsCad;
    FmCunsImgCab.FDsCab          := DsCunsCad;
    FmCunsImgCab.FQrIts          := QrCunsImgCab;
    FmCunsImgCab.EdNO_ENT.Text   := QrEntidadesNOMEENTIDADE.Value;
    //FmCunsImgCab.ReopenPrgLstPai(QrPrgLstCabCodigo.Value);
    //
    if SQLType = stIns then
    begin
      FmCunsImgCab.EdOrdem.ValueVariant       := DmModOS.ProximaOrdemCunsImgCab(QrCunsCadCodigo.Value);
    end else
    begin
      FmCunsImgCab.EdControle.ValueVariant    := QrCunsImgCabControle.Value;
      FmCunsImgCab.EdNome.ValueVariant        := QrCunsImgCabNome.Value;
      FmCunsImgCab.EdCaminho.ValueVariant     := QrCunsImgCabCaminho.Value;
      FmCunsImgCab.EdStatus.ValueVariant      := QrCunsImgCabStatus.Value;
      FmCunsImgCab.CBStatus.KeyValue          := QrCunsImgCabStatus.Value;
      FmCunsImgCab.EdDependencia.ValueVariant := QrCunsImgCabDependencia.Value;
      FmCunsImgCab.CBDependencia.KeyValue     := QrCunsImgCabDependencia.Value;
      FmCunsImgCab.RGAplicacao.ItemIndex      := QrCunsImgCabAplicacao.Value;
      FmCunsImgCab.MeObserv.Text              := QrCunsImgCabObserv.Value;
      FmCunsImgCab.EdCodiNCT.ValueVariant     := QrCunsImgCabCodiNCT.Value;
      FmCunsImgCab.CBCodiNCT.KeyValue         := QrCunsImgCabCodiNCT.Value;
      FmCunsImgCab.EdOrdem.ValueVariant       := QrCunsImgCabOrdem.Value;
    end;
    FmCunsImgCab.ShowModal;
    FmCunsImgCab.Destroy;
  end;
end;

{
procedure TFmCunsCad.MostraFormCunsImgCmt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCunsImgCmt, FmCunsImgCmt, afmoNegarComAviso) then
  begin
    FmCunsImgCmt.ImgTipo.SQLType := SQLType;
    FmCunsImgCmt.FQrCab          := QrCunsImgCab;
    FmCunsImgCmt.FDsCab          := DsCunsImgCab;
    FmCunsImgCmt.FQrIts          := QrCunsImgCmt;
    //FmCunsImgCmt.ReopenPrgLstPai(QrPrgLstCmtCodigo.Value);
    //
    if SQLType = stIns then
    begin
      //
    end else
    begin
      FmCunsImgCmt.EdConta.ValueVariant      := QrCunsImgCmtConta.Value;
      FmCunsImgCmt.EdNome.ValueVariant       := QrCunsImgCmtNome.Value;
      FmCunsImgCmt.EdTxtGeneric.ValueVariant := QrCunsImgCmtTxtGeneric.Value;
      FmCunsImgCmt.CBTxtGeneric.KeyValue     := QrCunsImgCmtTxtGeneric.Value;
    end;
    FmCunsImgCmt.ShowModal;
    FmCunsImgCmt.Destroy;
  end;
end;
}

procedure TFmCunsCad.MostraFormMovAmovCad(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmMovAmovCad, FmMovAmovCad, afmoNegarComAviso) then
  begin
    FmMovAmovCad.ImgTipo.SQLType := SQLType;
    //
    FmMovAmovCad.FQrCab := QrEntidades;
    FmMovAmovCad.FQrIts := QrMovAmovCad;
    FmMovAmovCad.FDsCab := DsEntidades;
    //
    if SQLTYpe = stUpd then
    begin
      FmMovAmovCad.EdCodigo.ValueVariant := QrMovAmovCadCodigo.Value;
      FmMovAmovCad.EdNome.Text := QrMovAmovCadNome.Value;
      FmMovAmovCad.RGTipo.ItemIndex := QrMovAmovCadTipo.Value;
    end;
    FmMovAmovCad.ShowModal;
    FmMovAmovCad.Destroy;
  end;
end;

procedure TFmCunsCad.MostraFormSiapImaAti(SQLType: TSQLType);
var
  _WHERE_: String;
  DefaultItem: Integer;
  DefaultValr: Double;
  Tamanho1, Tamanho2: Integer;
begin
  _WHERE_ := '';
  Tamanho1 := 0;
  Tamanho2 := 2; ; //casas decimais
  //
  if SQLType = stUpd then
  begin
    DefaultItem := QrSiapImaAtiAtividade.Value;
    DefaultValr := 0;
  end else
  begin
    DefaultItem := 0;
    DefaultValr := 0;
  end;
  UnCfgCadLista.InsAltItemLstEmRegTab('siapimaati', 'Atividade', 'Controle', '',
    'atividades', CO_CODIGO, CO_NOME, _WHERE_, DefaultItem,
    DefaultValr, dmktfInteger, dmktfNone, Tamanho1, Tamanho2,
    SQLType, 'Adi��o de Atividade', 'Atividade:', '', ncGerlSeq1, ncGerlSeq1,
    ['Codigo', 'Controle'], [QrSiapImaCadCodigo.Value, QrSiapImaAtiControle.Value],
    QrSiapImaAti);
end;

procedure TFmCunsCad.MostraFormSiapImaCad(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmSiapImaCad, FmSiapImaCad, afmoNegarComAviso) then
  begin
    FmSiapImaCad.ImgTipo.SQLType := SQLType;
    FmSiapImaCad.FForm := TFmCunsCad(Self);
    FmSiapImaCad.EdSiapImaTer.ValueVariant := QrSiapTerCadCodigo.Value;
    FmSiapImaCad.EdTer_Nome.Text           := QrSiapTerCadNome.Value;
    FmSiapImaCad.FCompl                    := QrSiapTerCadSCompl.Value;
    //
    if SQLType = stUpd then
    begin
      FmSiapImaCad.EdCodigo.ValueVariant     := QrSiapImaCadCodigo.Value;
      //
      FmSiapImaCad.EdSCompl2.ValueVariant    := QrSiapImaCadSCompl2.Value;
      FmSiapImaCad.EdM2Constru.ValueVariant  := QrSiapImaCadM2Constru.Value;
      FmSiapImaCad.EdM2NaoBuild.ValueVariant := QrSiapImaCadM2NaoBuild.Value;
      FmSiapImaCad.EdM2Terreno.ValueVariant  := QrSiapImaCadM2Terreno.Value;
      FmSiapImaCad.EdM2Total.ValueVariant    := QrSiapImaCadM2Total.Value;
      //
      FmSiapImaCad.EdObjeto.ValueVariant     := QrSiapImaCadObjeto.Value;
      FmSiapImaCad.CBObjeto.KeyValue         := QrSiapImaCadObjeto.Value;
      FmSiapImaCad.EdFinalidade.ValueVariant := QrSiapImaCadFinalidade.Value;
      FmSiapImaCad.CBFinalidade.KeyValue     := QrSiapImaCadFinalidade.Value;
      FmSiapImaCad.EdTpConstru.ValueVariant  := QrSiapImaCadTpConstru.Value;
      FmSiapImaCad.CBTpConstru.KeyValue      := QrSiapImaCadTpConstru.Value;
    end;
    //
    FmSiapImaCad.ShowModal;
    FmSiapImaCad.Destroy;
  end;
end;

procedure TFmCunsCad.MostraFormSiapImaCav(SQLType: TSQLType);
var
  _WHERE_: String;
  DefaultItem: Integer;
  DefaultValr: Double;
  Tamanho1, Tamanho2: Integer;
begin
  _WHERE_ := '';
  Tamanho1 := 0;
  Tamanho2 := 2; //casas decimais
  //
  if SQLType = stUpd then
  begin
    DefaultItem := QrSiapImaCavCaracteris.Value;
    DefaultValr := 0;
  end else
  begin
    DefaultItem := 0;
    DefaultValr := 0;
  end;
  UnCfgCadLista.InsAltItemLstEmRegTab('siapimacav', 'Caracteris', 'Controle', '',
  'caracteris', CO_CODIGO, CO_NOME, _WHERE_, DefaultItem,
  DefaultValr, dmktfInteger, dmktfNone, Tamanho1, Tamanho2,
  SQLType, 'Adi��o de Caracter�stica de �rea Vicinal', 'Caracter�stica:', '',
  ncGerlSeq1, ncGerlSeq1, ['Codigo', 'Controle'],
  [QrSiapImaCadCodigo.Value, QrSiapImaCavControle.Value], QrSiapImaCav);
end;

procedure TFmCunsCad.MostraFormSiapImaCdi(SQLType: TSQLType);
var
  _WHERE_: String;
  DefaultItem: Integer;
  DefaultValr: Double;
  Tamanho1, Tamanho2: Integer;
begin
  _WHERE_ := '';
  Tamanho1 := 0;
  Tamanho2 := 2; //casas decimais
  //
  if SQLType = stUpd then
  begin
    DefaultItem := QrSiapImaCdiCaracteris.Value;
    DefaultValr := 0;
  end else
  begin
    DefaultItem := 0;
    DefaultValr := 0;
  end;
  UnCfgCadLista.InsAltItemLstEmRegTab('siapimacdi', 'Caracteris', 'Controle', '',
  'caracteris', CO_CODIGO, CO_NOME, _WHERE_, DefaultItem,
  DefaultValr, dmktfInteger, dmktfNone, Tamanho1, Tamanho2,
  SQLType, 'Adi��o de Caracter�stica de Im�vel', 'Caracter�stica:', '',
  ncGerlSeq1, ncGerlSeq1, ['Codigo', 'Controle'],
  [QrSiapImaCadCodigo.Value, QrSiapImaCdiControle.Value], QrSiapImaCdi);
end;

procedure TFmCunsCad.MostraFormSiapImaCui(SQLType: TSQLType);
var
  _WHERE_: String;
  DefaultItem: Integer;
  DefaultValr: Double;
  Tamanho1, Tamanho2: Integer;
begin
  _WHERE_ := '';
  Tamanho1 := 0;
  Tamanho2 := 2; ; //casas decimais
  //
  if SQLType = stUpd then
  begin
    DefaultItem := QrSiapImaCuiCuidado.Value;
    DefaultValr := 0;
  end else
  begin
    DefaultItem := 0;
    DefaultValr := 0;
  end;
  UnCfgCadLista.InsAltItemLstEmRegTab('siapimacui', 'Cuidado', 'Controle', '',
  'cuidados', CO_CODIGO, CO_NOME, _WHERE_, DefaultItem,
  DefaultValr, dmktfInteger, dmktfNone, Tamanho1, Tamanho2,
  SQLType, 'Adi��o de Cuidado', 'Cuidado:', '', ncGerlSeq1, ncGerlSeq1,
  ['Codigo', 'Controle'], [QrSiapImaCadCodigo.Value, QrSiapImaCuiControle.Value],
  QrSiapImaCui);
end;

procedure TFmCunsCad.MostraFormSiapImaCxa(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmSiapImaCxa, FmSiapImaCxa, afmoNegarComAviso) then
  begin
    FmSiapImaCxa.FForm                       := TFmCunsCad(Self);
    FmSiapImaCxa.ImgTipo.SQLType             := SQLType;
    FmSiapImaCxa.EdCodigo.ValueVariant       := QrSiapImaCadCodigo.Value;
    FmSiapImaCxa.EdNome.Text                 := QrSiapImaCadSCompl2.Value;
    if SQLType = stUpd then
    begin
      FmSiapImaCxa.EdControle.ValueVariant   := QrSiapImaCxaControle.Value;
      //
      FmSiapImaCxa.EdLocal.Text              := QrSiapImaCxaLocal.Value;
      FmSiapImaCxa.EdVolumeL.ValueVariant    := QrSiapImaCxaVolumeL.Value;
      FmSiapImaCxa.EdMedidas.Text            := QrSiapImaCxaMedidas.Value;
      FmSiapImaCxa.MeAcesso.Text             := QrSiapImaCxaAcesso.Value;
      //
      FmSiapImaCxa.EdMatersCxa.ValueVariant  := QrSiapImaCxaMatersCxa.Value;
      FmSiapImaCxa.CBMatersCxa.KeyValue      := QrSiapImaCxaMatersCxa.Value;
      FmSiapImaCxa.EdFormasCxa.ValueVariant  := QrSiapImaCxaFormasCxa.Value;
      FmSiapImaCxa.CBFormasCxa.KeyValue      := QrSiapImaCxaFormasCxa.Value;
    end;
    //
    FmSiapImaCxa.ShowModal;
    FmSiapImaCxa.Destroy;
  end;
end;

procedure TFmCunsCad.MostraFormSiapImaDep(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmSiapImaDep, FmSiapImaDep, afmoNegarComAviso) then
  begin
    FmSiapImaDep.ImgTipo.SQLType := SQLType;
    FmSiapImaDep.FCodiAtual := QrSiapImaCadCodigo.Value;
    FmSiapImaDep.FQrySiapImaDep := QrSiapImaDep;
    if SQLType = stUpd then
    begin
      FmSiapImaDep.FCtrlAtual := QrSiapImaDepControle.Value;
      FmSiapImaDep.EdDependenci.ValueVariant := QrSiapImaDepDependenci.Value;
      FmSiapImaDep.CBDependenci.KeyValue := QrSiapImaDepDependenci.Value;
      //
      FmSiapImaDep.EdMLarg.ValueVariant := QrSiapImaDepMLarg.Value;
      FmSiapImaDep.EdMComp.ValueVariant := QrSiapImaDepMComp.Value;
      FmSiapImaDep.EdMAltu.ValueVariant := QrSiapImaDepMAltu.Value;
    end else begin
      FmSiapImaDep.FCtrlAtual := 0;
    end;
    FmSiapImaDep.ShowModal;
    FmSiapImaDep.Destroy;
  end;
end;

procedure TFmCunsCad.MostraFormSiapImaRes(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmSiapImaRes, FmSiapImaRes, afmoNegarComAviso) then
  begin
    FmSiapImaRes.ImgTipo.SQLType := SQLType;
    FmSiapImaRes.FCodiAtual := QrSiapImaCadCodigo.Value;
    FmSiapImaRes.FQrySiapImaRes := QrSiapImaRes;
    if SQLType = stUpd then
    begin
      FmSiapImaRes.FCtrlAtual := QrSiapImaResControle.Value;
      FmSiapImaRes.EdResidente.ValueVariant := QrSiapImaResResidente.Value;
      FmSiapImaRes.CBResidente.KeyValue := QrSiapImaResResidente.Value;
      //
      FmSiapImaRes.EdNome.Text := QrSiapImaResNome.Value;
      FmSiapImaRes.EdAnoNatal.ValueVariant := QrSiapImaResAnoNatal.Value;
      FmSiapImaRes.EdCuidado.ValueVariant := QrSiapImaResCuidado.Value;
      FmSiapImaRes.CBCuidado.KeyValue := QrSiapImaResCuidado.Value;
      //
      FmSiapImaRes.MeObservacao.Text := QrSiapImaResObservacao.Value;
    end else begin
      FmSiapImaRes.FCtrlAtual := 0;
    end;
    FmSiapImaRes.ShowModal;
    FmSiapImaRes.Destroy;
  end;
end;

procedure TFmCunsCad.MostraFormSiapImaSVG(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmSiapImaSVG, FmSiapImaSVG, afmoNegarComAviso) then
  begin
    FmSiapImaSVG.ImgTipo.SQLType := SQLType;
    FmSiapImaSVG.FCodiAtual := QrSiapImaCadCodigo.Value;
    FmSiapImaSVG.FQrySiapImaSVG := QrSiapImaSVG;
    if SQLType = stUpd then
    begin
      FmSiapImaSVG.FCtrlAtual := QrSiapImaSVGControle.Value;
      //
      FmSiapImaSVG.EdNome.ValueVariant := QrSiapImaSVGNome.Value;
      FmSiapImaSVG.RGRotarGraus.ItemIndex := QrSiapImaSVGRotarGraus.Value;
    end else begin
      FmSiapImaSVG.FCtrlAtual := 0;
    end;
    FmSiapImaSVG.ShowModal;
    FmSiapImaSVG.Destroy;
  end;
end;

procedure TFmCunsCad.MostraFormSiapterCad(SQLType: TSQLType);
var
  STC, Ent, LstCusPrd: Integer;
begin
  if SQLTYpe = stIns then
  begin
    if Geral.MB_Pergunta('Deseja pesquisar por lugares antes de incluir?') = ID_YES then
    begin
      if DBCheck.CriaFm(TFmLugarPesq, FmLugarPesq, afmoNegarComAviso) then
      begin
        FmLugarPesq.ShowModal;
        STC := FmLugarPesq.FCodigo;
        Ent := FmLugarPesq.FEntidade;
        FmLugarPesq.Destroy;
      end;
      if STC <> 0 then
      begin
        LocCod(Ent, Ent);
        QrSiapTerCad.Locate('Codigo', STC, []);
        //
        Exit;
      end;
    end;
  end;
  if DBCheck.CriaFm(TFmSiapTerCad, FmSiapTerCad, afmoNegarComAviso) then
  begin
    FmSiapTerCad.ImgTipo.SQLType := SQLType;
    FmSiapTerCad.FQrSiapterCad   := QrSiapTerCad;
    FmSiapTerCad.FCompl          := QrEntidadesCOMPLENT.Value;
    //
    if SQLType = stUpd then
    begin
      FmSiapTerCad.EdCodigo.ValueVariant     := QrSiapTerCadCodigo.Value;
      FmSiapTerCad.EdCliente.ValueVariant    := QrSiapTerCadCliente.Value;
      FmSiapTerCad.CBCliente.KeyValue        := QrSiapTerCadCliente.Value;
      //
      FmSiapTerCad.EdNome.ValueVariant       := QrSiapTerCadNome.Value;
      FmSiapTerCad.EdSCEP.ValueVariant       := QrSiapTerCadSCEP.Value;
      FmSiapTerCad.EdSLograd.ValueVariant    := QrSiapTerCadSLograd.Value;
      FmSiapTerCad.CBSLograd.KeyValue        := QrSiapTerCadSLograd.Value;
      FmSiapTerCad.EdSRua.ValueVariant       := QrSiapTerCadSRua.Value;
      FmSiapTerCad.EdSNumero.ValueVariant    := QrSiapTerCadSNumero.Value;
      FmSiapTerCad.EdSCompl.ValueVariant     := QrSiapTerCadSCompl.Value;
      FmSiapTerCad.EdSBairro.ValueVariant    := QrSiapTerCadSBairro.Value;
      FmSiapTerCad.EdSCidade.ValueVariant    := QrSiapTerCadSCidade.Value;
      FmSiapTerCad.EdSUF.ValueVariant        := QrSiapTerCadSUF.Value;
      FmSiapTerCad.EdSPais.ValueVariant      := QrSiapTerCadSPais.Value;
      FmSiapTerCad.EdSEndeRef.ValueVariant   := QrSiapTerCadSEndeRef.Value;
      FmSiapTerCad.EdSTe1.ValueVariant       := QrSiapTerCadSTe1.Value;
      FmSiapTerCad.EdSCodMunici.ValueVariant := QrSiapTerCadSCodMunici.Value;
      FmSiapTerCad.EdSCodiPais.ValueVariant  := QrSiapTerCadSCodiPais.Value;
      FmSiapTerCad.EdM2Constru.ValueVariant  := QrSiapTerCadM2Constru.Value;
      FmSiapTerCad.EdM2NaoBuild.ValueVariant := QrSiapTerCadM2NaoBuild.Value;
      FmSiapTerCad.EdM2Terreno.ValueVariant  := QrSiapTerCadM2Terreno.Value;
      FmSiapTerCad.EdM2Total.ValueVariant    := QrSiapTerCadM2Total.Value;
      FmSiapTerCad.EdPdrMntsMon.ValueVariant := QrSiapTerCadPdrMntsMon.Value;
      FmSiapTerCad.EdLatitude.ValueVariant   := QrSiapTerCadLatitude.Value;
      FmSiapTerCad.EdLongitude.ValueVariant  := QrSiapTerCadLongitude.Value;
      FmSiapTerCad.EdRotaLatLon.ValueVariant := QrSiapTerCadRotaLatLon.Value;
      FmSiapTerCad.CBRotaLatLon.KeyValue     := QrSiapTerCadRotaLatLon.Value;
      UMyMod.SetaCodUsuDeCodigo(FmSiapTerCad.EdLstCusPrd, FmSiapTerCad.CBLstCusPrd,
        FmSiapTerCad.QrGraCusPrc, QrSiapTerCadLstCusPrd.Value, 'Codigo', 'CodUsu');
      //
    end else
    begin
      LstCusPrd := Dmod.QrOpcoesBugsLstCusPrd.Value;
      //
      if LstCusPrd = 0 then
        LstCusPrd := 4; //�ltima compra
      //
      FmSiapTerCad.EdCliente.ValueVariant   := QrCunsCadCodigo.Value;
      FmSiapTerCad.CBCliente.KeyValue       := QrCunsCadCodigo.Value;
      FmSiapTerCad.EdLstCusPrd.ValueVariant := LstCusPrd;
      FmSiapTerCad.CBLstCusPrd.KeyValue     := LstCusPrd;
    end;
    //
    FmSiapTerCad.ShowModal;
    FmSiapTerCad.Destroy;
  end;
end;

procedure TFmCunsCad.MostraFormSiapTerFlh(SQLType: TSQLType);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmSiapTerFlh, FmSiapTerFlh, afmoNegarComAviso) then
  begin
    FmSiapTerFlh.ImgTipo.SQLType := SQLType;
    FmSiapTerFlh.FQrCab := QrSiapTerCad;
    FmSiapTerFlh.FDsCab := DsSiapTerCad;
    FmSiapTerFlh.FQrIts := QrSiapTerFlh;
    FmSiapTerFlh.FCliente := QrCunsCadCodigo.Value;
    FmSiapTerFlh.ReopenEntiContat();
    if SQLType = stIns then
    begin
      FmSiapTerFlh.EdEmpresa.ValueVariant   := DModG.QrFiliLogFilial.Value;
      FmSiapTerFlh.CBEmpresa.KeyValue       := DModG.QrFiliLogFilial.Value;
      FmSiapTerFlh.EdMonEntCtr.ValueVariant := QrCunsCadCodigo.Value;
      FmSiapTerFlh.CBMonEntCtr.KeyValue     := QrCunsCadCodigo.Value;
      FmSiapTerFlh.EdMonEntPag.ValueVariant := QrCunsCadCodigo.Value;
      FmSiapTerFlh.CBMonEntPag.KeyValue     := QrCunsCadCodigo.Value;
      FmSiapTerFlh.EdMonAgeEqi.ValueVariant := -1;
      FmSiapTerFlh.CbMonAgeEqi.KeyValue     := -1;
    end else
    begin
      FmSiapTerFlh.EdEmpresa.Enabled         := False;
      //
      FmSiapTerFlh.EdControle.ValueVariant   := QrSiapTerFlhControle.Value;
      FmSiapTerFlh.EdEmpresa.ValueVariant    := QrSiapTerFlhEmpresa.Value;
      FmSiapTerFlh.CBEmpresa.KeyValue        := QrSiapTerFlhEmpresa.Value;
      //
      FmSiapTerFlh.TPDataSincOS.Date         := QrSiapTerFlhDataSincOS.Value;
      //
      FmSiapTerFlh.EdMonAgeEqi.ValueVariant  := QrSiapTerFlhMonAgeEqi.Value;
      FmSiapTerFlh.CBMonAgeEqi.KeyValue      := QrSiapTerFlhMonAgeEqi.Value;
      FmSiapTerFlh.EdMonEntCtt.ValueVariant  := QrSiapTerFlhMonEntCtt.Value;
      FmSiapTerFlh.CBMonEntCtt.KeyValue      := QrSiapTerFlhMonEntCtt.Value;
      FmSiapTerFlh.EdMonNumCtr.ValueVariant  := QrSiapTerFlhMonNumCtr.Value;
      FmSiapTerFlh.EdMonEntCtr.ValueVariant  := QrSiapTerFlhMonEntCtr.Value;
      FmSiapTerFlh.CBMonEntCtr.KeyValue      := QrSiapTerFlhMonEntCtr.Value;
      FmSiapTerFlh.EdMonEntPag.ValueVariant  := QrSiapTerFlhMonEntPag.Value;
      FmSiapTerFlh.CBMonEntPag.KeyValue      := QrSiapTerFlhMonEntPag.Value;
      FmSiapTerFlh.EdMonCondPg.ValueVariant  := QrSiapTerFlhMonCondPg.Value;
      FmSiapTerFlh.CBMonCondPg.KeyValue      := QrSiapTerFlhMonCondPg.Value;
      FmSiapTerFlh.EdMonCrtEmi.ValueVariant  := QrSiapTerFlhMonCrtEmi.Value;
      FmSiapTerFlh.CBMonCrtEmi.KeyValue      := QrSiapTerFlhMonCrtEmi.Value;
      FmSiapTerFlh.EdTrajetDist.ValueVariant := QrSiapTerFlhTrajetDist.Value;
      FmSiapTerFlh.RGTrajetKm_h.ItemIndex    := QrSiapTerFlhTrajetKm_h.Value;
      FmSiapTerFlh.EdMinHAgeExe.ValueVariant := QrSiapTerFlhMinHAgeExe.Value;
    end;
    //
    FmSiapTerFlh.ShowModal;
    FmSiapTerFlh.Destroy;
  end;
end;

procedure TFmCunsCad.MostraPaneledDiarioGer2();
begin
  if FDiarioGer2 = nil then
  begin
    FDiarioGer2 := MyObjects.FormTDIPanel(TFmDiarioGer2, FmDiarioGer2, PnDiarioGer2);
    TFmDiarioGer2(FDiarioGer2).EdEntidade.ValueVariant := QrEntidadesCodigo.Value;
    TFmDiarioGer2(FDiarioGer2).CBEntidade.KeyValue     := QrEntidadesCodigo.Value;
    //
    TFmDiarioGer2(FDiarioGer2).EdEntidade.Enabled      := False;
    TFmDiarioGer2(FDiarioGer2).CBEntidade.Enabled      := False;
    //
    TFmDiarioGer2(FDiarioGer2).BtSaida.Visible         := False;
  end;
end;

procedure TFmCunsCad.MostraPaneledOSPrvGer();
{
const
  Fornecedor = 0;
  AdvToolBarPager1 = nil;
var
  DmLctX: TDataModule;
}
begin
  if FOSPrvGer = nil then
  begin
    FOSPrvGer := MyObjects.FormTDIPanel(TFmOSPrvGer, FmOSPrvGer, PnOSPrvGer);
    TFmOSPrvGer(FOSPrvGer).EdCliente.ValueVariant := QrEntidadesCodigo.Value;
    TFmOSPrvGer(FOSPrvGer).CBCliente.KeyValue     := QrEntidadesCodigo.Value;
    //
    TFmOSPrvGer(FOSPrvGer).EdCliente.Enabled      := False;
    TFmOSPrvGer(FOSPrvGer).CBCliente.Enabled      := False;
    //
    TFmOSPrvGer(FOSPrvGer).BtSaida.Visible        := False;
  end;
end;

procedure TFmCunsCad.MostraPeneledSelfGer2();
const
  Fornecedor = 0;
  AdvToolBarPager1 = nil;
var
  DmLctX: TDataModule;
begin
  if FMySelfGer2 = nil then
  begin
    if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
    begin
      FMySelfGer2 := TDmLct2(DmLctX).GerenciaTerceiro(PnFinancas, AdvToolBarPager1,
        QrEntidadesCodigo.Value, Fornecedor);
      //
      TFmSelfGer2(FMySelfGer2).BtSair.Enabled := False;
    end;
  end;
end;

procedure TFmCunsCad.NCTCAC1Click(Sender: TObject);
  procedure IncluiAtual();
  var
    Nome, Caminho, Observ: String;
    Codigo, Controle, Status, Dependencia, CodiNCT, Aplicacao, Ordem: Integer;
  begin
    Codigo         := QrCunsImgCabCodigo.Value;
    Controle       := QrCunsImgCabControle.Value;
    Nome           := QrCunsImgCabNome.Value;
    Status         := QrCunsImgCabStatus.Value;
    Caminho        := QrCunsImgCabCaminho.Value;
    Dependencia    := QrCunsImgCabDependencia.Value;
    CodiNCT        := QrCunsImgCabCodiNCT.Value;
    Observ         := QrCunsImgCabObserv.Value;
    Aplicacao      := QrCunsImgCabAplicacao.Value;
    Ordem          := QrCunsImgCabOrdem.Value;

    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_Cuns_Img_Cab, False, [
    'Codigo', 'Nome', 'Status',
    'Caminho', 'Dependencia', 'CodiNCT',
    'Observ', 'Aplicacao', 'Ordem'], [
    'Controle'], [
    Codigo, Nome, Status,
    Caminho, Dependencia, CodiNCT,
    Observ, Aplicacao, Ordem], [
    Controle], False);
  end;
var
  CtrlLoc, I: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando impress�o');
  //
  CtrlLoc := QrCunsImgCabControle.Value;
  F_Cuns_Img_Cab :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Cuns_Img_Cab, DmodG.QrUpdPID1, False);
  if DBGCunsImgCab.SelectedRows.Count > 1 then
  begin
    with DBGCunsImgCab.DataSource.DataSet do
    for I:= 0 to DBGCunsImgCab.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBGCunsImgCab.SelectedRows.Items[i]));
      IncluiAtual();
    end;
  end else
    IncluiAtual();
  //
  ReopenCIC_Imp(QrCunsImgCabCodigo.Value);
  MyObjects.frxDefineDatasets(frxCAD_SUBCL_001_001_A, [
  DModG.frxDsDono,
  Dmod.frxDsOpcoesBugs,
  frxDsCIC_Imp
  ]);
  MyObjects.frxMostra(frxCAD_SUBCL_001_001_A, 'NCT - CAC');
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
end;

procedure TFmCunsCad.PCDadosChange(Sender: TObject);
begin
  case PCDados.ActivePageIndex of
    3: MostraPeneledSelfGer2();
    4: MostraPaneledOSPrvGer();
    5:
    begin
      if QrOSCab.State = dsInactive then
        ReopenOSCab();
    end;
    6: MostraPaneledDiarioGer2();
    7:
    begin
      if QrContratos.State = dsInactive then
        ReopenContratos();
    end;
  end;
end;

procedure TFmCunsCad.ReopenWUsers;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWUsers, Dmod.MyDBn, [
    'SELECT wus.Codigo, wpe.Nome Perfil_TXT, ',
    'wus.PersonalName, wus.Username, ',
    'wus.Email, wus.Ativo ',
    'FROM wusers wus ',
    'LEFT JOIN wperfis wpe ON wpe.Codigo = wus.Perfil ',
    'WHERE wus.Entidade=' + Geral.FF0(QrEntidadesCodigo.Value),
    '']);
end;

procedure TFmCunsCad.PeloNome1Click(Sender: TObject);
begin
  LocCod(QrCunsCadCodigo.Value,
  CuringaLoc.CriaFormEnti(CO_CODIGO, 'RazaoSocial', CO_NOME, 'Fantasia',
  'Apelido', 'Entidades', 'Tipo', 0, Dmod.MyDB, Loc_SQLExtra()));
end;

procedure TFmCunsCad.Periododemonitoramento1Click(Sender: TObject);
begin
  AlterarIntervalosOuPeriodo(fldmonitPerioDd);
end;

procedure TFmCunsCad.PGDadosLocaisAplicChange(Sender: TObject);
begin
  VerificaHabilita();
end;

procedure TFmCunsCad.PMAtrSTCdDefPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrSTCdDef1, QrSiapTerCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrSTCdDef1, QrAtrSTCdDef);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrSTCdDef1, QrAtrSTCdDef);
end;

procedure TFmCunsCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrCunsCad);
  MyObjects.HabilitaMenuItemCabDelC1I2(
    CabExclui1, QrCunsCad, QrCunsIts, QrSiapTerCad);
end;

procedure TFmCunsCad.PMCunsImgCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluinovaFoto1, QrCunsCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraFotoAtual1, QrCunsImgCab);
  MyObjects.HabilitaMenuItemItsDel(ExcluiFotoAtual1, QrCunsImgCab);
end;

procedure TFmCunsCad.PMCunsImgCmt_Popup(Sender: TObject);
begin
{
  MyObjects.HabilitaMenuItemItsIns(IncluinovoTexto1, QrCunsImgCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraTextoAtual1, QrCunsImgCmt);
  MyObjects.HabilitaMenuItemItsDel(ExcluiTextoAtual1, QrCunsImgCmt);
}
end;

procedure TFmCunsCad.PMCunsItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(AdicionaContratante1, QrCunsCad);
  MyObjects.HabilitaMenuItemItsDel(RetiraContratante1, QrCunsIts);
end;

procedure TFmCunsCad.PMPMVsPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPMVs.State <> dsInactive) and (QrPMVs.RecordCount > 0);
  //
  AlteraPMVatual1.Enabled           := Enab;
  Alterarintervalospsteros1.Enabled := Enab;
end;

procedure TFmCunsCad.PMSiapImaAtiPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSiapImaAti1, QrSiapImaCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSiapImaAti1, QrSiapImaAti);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSiapImaAti1, QrSiapImaAti);
end;

procedure TFmCunsCad.PMSiapImaCadPopup(Sender: TObject);
{
var
  Qtd: Integer;
}
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSiapImaCad1, QrSiapTerCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSiapImaCad1, QrSiapImaCad);
  //
  MyObjects.HabilitaMenuItemItsDel(ExcluiSiapImaCad1, QrSiapImaCad);
  if ExcluiSiapImaCad1.Enabled then
  begin
    ExcluiSiapImaCad1.Enabled :=
    (QrSiapImaDep.RecordCount + QrSiapImaCdi.RecordCount +
    QrSiapImaCav.RecordCount + QrSiapImaRes.RecordCount +
    QrSiapImaCui.RecordCount + QrSiapImaAti.RecordCount +
    QrSiapImaCxa.RecordCount) = 0;
  end;
end;

procedure TFmCunsCad.PMSiapImaCavPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSiapImaCav1, QrSiapImaCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSiapImaCav1, QrSiapImaCav);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSiapImaCav1, QrSiapImaCav);
end;

procedure TFmCunsCad.PMSiapImaCdiPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSiapImaCdi1, QrSiapImaCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSiapImaCdi1, QrSiapImaCdi);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSiapImaCdi1, QrSiapImaCdi);
end;

procedure TFmCunsCad.PMSiapImaCuiPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSiapImaCui1, QrSiapImaCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSiapImaCui1, QrSiapImaCui);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSiapImaCui1, QrSiapImaCui);
end;

procedure TFmCunsCad.PMSiapImaCxaPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSiapImaCxa1, QrSiapImaCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSiapImaCxa1, QrSiapImaCxa);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSiapImaCxa1, QrSiapImaCxa);
  MyObjects.HabilitaMenuItemItsUpd(DuplicSiapImaCxa1, QrSiapImaCxa);
end;

procedure TFmCunsCad.PMSiapImaDepPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSiapImaDep1, QrSiapImaCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSiapImaDep1, QrSiapImaDep);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSiapImaDep1, QrSiapImaDep);
end;

procedure TFmCunsCad.PMSiapImaResPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSiapImaRes1, QrSiapImaCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSiapImaRes1, QrSiapImaRes);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSiapImaRes1, QrSiapImaRes);
end;

procedure TFmCunsCad.PMSiapImaSVGPopup(Sender: TObject);
begin
 MyObjects.HabilitaMenuItemItsIns(Carreganovoarquivo1, QrSiapImaCad);
 //
 MyObjects.HabilitaMenuItemItsUpd(Recarrega1, QrSiapImaSVG);
 MyObjects.HabilitaMenuItemItsUpd(Alteraadescrio1, QrSiapImaSVG);
 MyObjects.HabilitaMenuItemItsUpd(Visualizarcroqui1, QrSiapImaSVG);
 //
 MyObjects.HabilitaMenuItemItsDel(Excluiarquivoatual1, QrSiapImaSVG);
end;

procedure TFmCunsCad.PMSiapTerCadPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiTerCad1, QrCunsCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraTerCad1, QrSiapTerCad);
  MyObjects.HabilitaMenuItemCabDel(ExcluiTerCad1, QrSiapTerCad, QrSiapImaCad);
end;

procedure TFmCunsCad.PMSiapTerFlhPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiDsSiapTerFlh1, QrSiapTerCad);
  MyObjects.HabilitaMenuItemItsUpd(AlteraDsSiapTerFlh1, QrSiapTerFlh);
  MyObjects.HabilitaMenuItemItsDel(ExcluiDsSiapTerFlh1, QrSiapTerFlh);
end;

procedure TFmCunsCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCunsCadCodigo.Value, LaRegistro.Caption[2]);
end;
procedure TFmCunsCad.VerificaHabilita();
var
  Habilita: Boolean;
begin
  Habilita := (QrSiapImaCad.State <> dsInactive)
    and (QrSiapImaCad.RecordCount > 0);
  //
  BtSiapImaDep.Enabled := False;
  BtSiapImaCdi.Enabled := False;
  BtSiapImaCav.Enabled := False;
  BtSiapImaRes.Enabled := False;
  BtSiapImaCui.Enabled := False;
  BtSiapImaAti.Enabled := False;
  BtSiapImaCxa.Enabled := False;
  BtAtrSICdDef.Enabled := False;
  //
  BtSiapImaDep.Enabled := Habilita and (PGDadosLocaisAplic.ActivePageIndex = 0);
  BtSiapImaCdi.Enabled := Habilita and (PGDadosLocaisAplic.ActivePageIndex = 1);
  BtSiapImaCav.Enabled := Habilita and (PGDadosLocaisAplic.ActivePageIndex = 2);
  BtSiapImaRes.Enabled := Habilita and (PGDadosLocaisAplic.ActivePageIndex = 3);
  BtSiapImaCui.Enabled := Habilita and (PGDadosLocaisAplic.ActivePageIndex = 4);
  BtSiapImaAti.Enabled := Habilita and (PGDadosLocaisAplic.ActivePageIndex = 5);
  BtSiapImaCxa.Enabled := Habilita and (PGDadosLocaisAplic.ActivePageIndex = 6);
  BtAtrSICdDef.Enabled := Habilita and (PGDadosLocaisAplic.ActivePageIndex = 7);
  //
  BtAtrSICxDef.Enabled := BtSiapImaCxa.Enabled
    and (QrSiapImaCxa.State <> dsInactive)
    and (QrSiapImaCxa.RecordCount > 0);
end;

procedure TFmCunsCad.Visualizarcroqui1Click(Sender: TObject);
begin
  DBGSiapImaSVGDblClick(Self);
end;

procedure TFmCunsCad.DefParams;
begin
  VAR_GOTOTABELA := 'cunscad cun';
  VAR_GOTOMYSQLTABLE := QrCunsCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'cun.Codigo';
  VAR_GOTONOME := 'ent.Nome';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ati.Nome NO_AtivPrinc, how.Nome NO_HowFind, ');
  VAR_SQLx.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENTIDADE, ');
  VAR_SQLx.Add('IF(acc.Tipo=0, acc.RazaoSocial, acc.Nome) NO_Account, cun.* ');
  VAR_SQLx.Add('FROM cunscad cun');
  VAR_SQLx.Add('LEFT JOIN atividades ati ON ati.Codigo=cun.AtivPrinc');
  VAR_SQLx.Add('LEFT JOIN howfounded how ON how.Codigo=cun.HowFind');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=cun.Codigo');
  VAR_SQLx.Add('LEFT JOIN entidades acc ON acc.Codigo=cun.Account');
  VAR_SQLx.Add('WHERE cun.Codigo <> 0');
  //
  VAR_SQL1.Add('AND cun.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND cun.CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND cun.Nome Like :P0');
  VAR_SQLa.Add('AND (CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial LIKE:P0');
  VAR_SQLa.Add('ELSE ent.Nome LIKE:P1 END) ');
  //
end;

procedure TFmCunsCad.dmkDBGridZTO2DblClick(Sender: TObject);
var
  Form: TForm;
  Codigo: Integer;
begin
  if (QrContratos.State <> dsInactive) and (QrContratos.RecordCount > 0) then
  begin
    Codigo := QrContratosCodigo.Value;
    //
    Form := MyObjects.FormTDICria(TFmContratos, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPager1);
    TFmContratos(Form).LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCunsCad.DBGPMVsCellClick(Column: TColumn);
begin
  if Column.FieldName = 'OSMonCab' then
    OSApp_PF.MostraFormOSCab2(mfoisOSMonCab, QrPMVsOSMonCab.Value);
end;

procedure TFmCunsCad.DBGAtrSTCdDefMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMAtrSTCdDef, DBGAtrSTCdDef, X,Y);
  *)
end;

procedure TFmCunsCad.Duplicaritem1Click(Sender: TObject);
var
  Codigo, Controle, ID_Item, ID_Sorc, Cxa, SiapImaTer: Integer;
  SCompl2, SCx: String;
  Continua: Boolean;
begin
  SiapImaTer := 0;
  Continua := False;
  if DBCheck.CriaFm(TFmSiapImaCopy, FmSiapImaCopy, afmoNegarComAviso) then
  begin
    FmSiapImaCopy.FSiapImaTer := QrSiapTerCadCodigo.Value;
    FmSiapImaCopy.ShowModal;
    SiapImaTer := FmSiapImaCopy.FSiapImater;
    Continua := FmSiapImaCopy.FContinua;
    FmSiapImaCopy.Destroy;
  end;
  if not Continua then
    Exit;
  //
  if Geral.MB_Pergunta('Confirma a duplica��o (c�pia) do local selecionado?') =
  ID_YES then
  begin
    if QrSiapImaCxa.RecordCount > 0 then
    begin
      if QrSiapImaCxa.RecordCount = 1 then
        SCx := 'a'
      else
        Scx := 'as';
      Cxa := Geral.MB_Pergunta('Deseja duplicar ' + Scx + ' caixa' + Scx +
      ' d''�gua?');
    end else
      Cxa := ID_NO;
    if Cxa = ID_CANCEL then
      Exit;
    SCompl2 := QrSiapImaCadSCompl2.Value + ' (c�pia)';
    Codigo := UMyMod.BPGS1I32('siapimacad', 'Codigo', '', '', tsDef, stIns, 0);
    //
    if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'siapimacad', TMeuDB,
    ['Codigo'], [QrSiapImaCadCodigo.Value],
    ['Codigo', 'SCompl2', 'SiapImaTer'],
    [Codigo, SCompl2, SiapImaTer],
    '', True, LaAviso1, LaAviso2) then
    begin
      // SiapImaDep (Depend�ncias)
      QrSiapImaDep.First;
      while not QrSiapImaDep.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('SiapImaDep', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'SiapImaDep', TMeuDB,
        ['Controle'], [QrSiapImaDepControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrSiapImaDep.Next;
      end;

      //

      // SiapImaCdi (Caracter�sticas)
      QrSiapImaCdi.First;
      while not QrSiapImaCdi.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('SiapImaCdi', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'SiapImaCdi', TMeuDB,
        ['Controle'], [QrSiapImaCdiControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrSiapImaCdi.Next;
      end;

      //

      // SiapImaCav (�reas vicinais)
      QrSiapImaCav.First;
      while not QrSiapImaCav.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('SiapImaCav', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'SiapImaCav', TMeuDB,
        ['Controle'], [QrSiapImaCavControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrSiapImaCav.Next;
      end;

      //

      // SiapImaRes (Residentes)
      QrSiapImaRes.First;
      while not QrSiapImaRes.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('SiapImaRes', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'SiapImaRes', TMeuDB,
        ['Controle'], [QrSiapImaResControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrSiapImaRes.Next;
      end;

      //

      // SiapImaCui (Cuidados)
      QrSiapImaCui.First;
      while not QrSiapImaCui.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('SiapImaCui', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'SiapImaCui', TMeuDB,
        ['Controle'], [QrSiapImaCuiControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrSiapImaCui.Next;
      end;

      //

      // SiapImaAti (Atividades)
      QrSiapImaAti.First;
      while not QrSiapImaAti.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('SiapImaAti', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'SiapImaAti', TMeuDB,
        ['Controle'], [QrSiapImaAtiControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrSiapImaAti.Next;
      end;

      //

      // AtrSICdDef (Atributos [do lugar])
      QrAtrSICdDef.First;
      while not QrAtrSICdDef.Eof do
      begin
        ID_Item := UMyMod.BPGS1I32('AtrSICdDef', 'ID_Item', '', '', tsDef, stIns, 0);
        ID_Sorc := Codigo;
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'AtrSICdDef', TMeuDB,
        ['ID_Item'], [QrAtrSICdDefID_Item.Value],
        ['ID_Sorc', 'ID_Item'], [ID_Sorc, ID_Item], '', True, LaAviso1, LaAviso2);
        //
        QrAtrSICdDef.Next;
      end;

      //

      if Cxa = ID_YES then
      begin
        // SiapImaCxa (?)
        QrSiapImaCxa.First;
        while not QrSiapImaCxa.Eof do
        begin
          Controle  := UMyMod.BPGS1I32('SiapImaCxa', 'Controle', '', '', tsDef, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'SiapImaCxa', TMeuDB,
          ['Controle'], [QrSiapImaCxaControle.Value],
          ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          //////////////////////////////////////////////////////////////////////
          // AtrSICxDef (Atributos [do lugar])
          QrAtrSICxDef.First;
          while not QrAtrSICxDef.Eof do
          begin
            ID_Item := UMyMod.BPGS1I32('AtrSICxDef', 'ID_Item', '', '', tsDef, stIns, 0);
            ID_Sorc := Controle;
            //
            UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'AtrSICxDef', TMeuDB,
            ['ID_Item'], [QrAtrSICxDefID_Item.Value],
            ['ID_Sorc', 'ID_Item'], [ID_Sorc, ID_Item], '', True, LaAviso1, LaAviso2);
            //
            QrAtrSICxDef.Next;
          end;
          //////////////////////////////////////////////////////////////////////
          //
          QrSiapImaCxa.Next;
        end;
      end;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    ReopenSiapImaCad(Codigo);
  end;
end;

procedure TFmCunsCad.DuplicSiapImaCxa1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.DuplicaRegistro_IdxInt1(Dmod.QrAux, 'siapimacxa',
  QrSiapImaCxaControle.Value, 'Controle',  ncGerlSeq1, Controle) then
    ReopenSiapImaCxa(Controle);
end;

procedure TFmCunsCad.DBGAtrAMovDefMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMAtrAMovDef, DBGAtrAMovDef, X,Y);
  *)
end;

procedure TFmCunsCad.Dados1Click(Sender: TObject);
begin
  MyObjects.FormShow(TFmEntidade2Imp, FmEntidade2Imp);
  LocCod(VAR_ENTIDADE, VAR_ENTIDADE);
end;

procedure TFmCunsCad.DBGAtrSICdDefMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMAtrSICdDef, DBGAtrSICdDef, X,Y);
  *)
end;

procedure TFmCunsCad.DBGAtrSICxDefMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMAtrSICxDef, DBGAtrSICxDef, X,Y);
  *)
end;

procedure TFmCunsCad.Excluiarquivoatual1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM pipcad ',
    'WHERE SiapImaSvg=' + Geral.FF0(QrSiapImaSVGControle.Value),
    '']);
    //
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Este croqui n�o pode ser removido pois possui ' +
      Geral.FF0(Qry.RecordCount) + ' PMV(s) associado(s) a ele!');
      Exit;
    end else
    begin
      if UMyMod.ExcluiRegistroInt1(
      'Confirma a remo��o do croqui selecionado?',
      'siapimasvg', 'Controle', QrSiapImaSVGControle.Value, Dmod.MyDB) = ID_YES then
      begin
        Controle := GOTOy.LocalizaPriorNextIntQr(QrSiapImaSVG,
        QrSiapImaSVGControle, QrSiapImaSVGControle.Value);
        ReopenSiapImaSVG(Controle);
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmCunsCad.ExcluiAtrAMovDef1Click(Sender: TObject);
var
  ID_Item: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do atributo do m�vel / ve�culo selecionado?',
    'atramovdef', 'ID_Item', QrAtrAMovDefID_Item.Value, Dmod.MyDB) = ID_YES then
  begin
    ID_Item := GOTOy.LocalizaPriorNextIntQr(QrAtrAMovDef, QrAtrAMovDefID_Item,
    QrAtrAMovDefID_Item.Value);
    ReopenAtrAMovDef(ID_Item);
  end;
end;

procedure TFmCunsCad.ExcluiAtrSICdDef1Click(Sender: TObject);
var
  ID_Item: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do atributo de local de aplica��o selecionado?',
  'atrsicddef', 'ID_Item', QrAtrSICdDefID_Item.Value, Dmod.MyDB) = ID_YES then
  begin
    ID_Item := GOTOy.LocalizaPriorNextIntQr(QrAtrSICdDef, QrAtrSICdDefID_Item,
    QrAtrSICdDefID_Item.Value);
    ReopenAtrSICdDef(ID_Item);
  end;
end;

procedure TFmCunsCad.ExcluiAtrSICxDef1Click(Sender: TObject);
var
  ID_Item: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do atributo de caixa d`�gua selecionado?',
  'atrsicxdef', 'ID_Item', QrAtrSICxDefID_Item.Value, Dmod.MyDB) = ID_YES then
  begin
    ID_Item := GOTOy.LocalizaPriorNextIntQr(QrAtrSICxDef, QrAtrSICxDefID_Item,
    QrAtrSICxDefID_Item.Value);
    ReopenAtrSICxDef(ID_Item);
  end;
end;

procedure TFmCunsCad.ExcluiAtrSTCdDef1Click(Sender: TObject);
var
  ID_Item: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do atributo do m�vel / ve�culo selecionado?',
  'atrstcddef', 'ID_Item', QrAtrSTCdDefID_Item.Value, Dmod.MyDB) = ID_YES then
  begin
    ID_Item := GOTOy.LocalizaPriorNextIntQr(QrAtrSTCdDef, QrAtrSTCdDefID_Item,
    QrAtrSTCdDefID_Item.Value);
    ReopenAtrSTCdDef(ID_Item);
  end;
end;

procedure TFmCunsCad.Excluifotoatual1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o da foto selecionada?',
  'cunsimgcab', 'Controle', QrCunsImgCabControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCunsImgCab, QrCunsImgCabCodigo,
    QrCunsImgCabControle.Value);
    ReopenCunsImgCab(Controle);
  end;
end;

procedure TFmCunsCad.Excluimvelautomvel1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{ TODO : DESMARCAR EXCLUS�O :: SiapTerCad}
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do m�vel / ve�culo selecionado?',
  'movamovcad', 'Codigo', QrMovAmovCadCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrMovAmovCad, QrMovAmovCadCodigo,
    QrMovAmovCadCodigo.Value);
    ReopenMovAmovCad(Codigo);
  end;
end;

procedure TFmCunsCad.Excluiragendamento1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do agendamento selecionado?',
  'agendaeve', 'Codigo', QrSTCAgeEveCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrSTCAgeEve, QrSTCAgeEveCodigo,
    QrSTCAgeEveCodigo.Value);
    ReopenSTCAgeEve(Codigo);
  end;
end;

procedure TFmCunsCad.ExcluiSiapImaAti1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o da atividade selecionada?',
  'siapimaati', 'Controle', QrSiapImaAtiControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSiapImaAti, QrSiapImaAtiControle,
    QrSiapImaAtiControle.Value);
    ReopenSiapImaAti(Controle);
  end;
end;

procedure TFmCunsCad.ExcluiSiapImaCad1Click(Sender: TObject);
  function _CE(Qry: TmySQLQuery; Tabela: String): Boolean;
  begin
    Result := (*(Qry.State <> dsInactive) and*) (Qry.RecordCount > 0);
    if Result then
      Geral.MB_Aviso('Exclus�o de local cancelado!' + sLineBreak +
      'A tabela "' + Tabela + '" possui registros para esse lugar!');
  end;
var
  Codigo: Integer;
begin
  if _CE(QrSiapImaDep, 'Depend�ncias') then
    Exit;
  if _CE(QrSiapImaCdi, 'Caracter�sticas') then
    Exit;
  if _CE(QrSiapImaCav, '�reas vicinais') then
    Exit;
  if _CE(QrSiapImaRes, 'Residentes') then
    Exit;
  if _CE(QrSiapImaCui, 'Cuidados') then
    Exit;
  if _CE(QrSiapImaAti, 'Atividades') then
    Exit;
  if _CE(QrSiapImaCxa, 'Caixas d''�gua') then
    Exit;
  if _CE(QrAtrSICdDef, 'Atributos') then
    Exit;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do local de aplica��o selecionado?',
  'siapimacad', 'Codigo', QrSiapImaCadCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrSiapImaCad, QrSiapImaCadCodigo,
      QrSiapImaCadCodigo.Value);
    ReopenSiapImaCad(Codigo);
  end;
end;

procedure TFmCunsCad.ExcluiSiapImaCav1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o da caracter�stica de �rea vicinal selecionada?',
  'siapimacav', 'Controle', QrSiapImaCavControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSiapImaCav, QrSiapImaCavCodigo,
    QrSiapImaCavControle.Value);
    ReopenSiapImaCav(Controle);
  end;
end;

procedure TFmCunsCad.ExcluiSiapImaCdi1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o da caracter�stica de im�vel selecionada?',
  'siapimacdi', 'Controle', QrSiapImaCdiControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSiapImaCdi, QrSiapImaCdiCodigo,
    QrSiapImaCdiControle.Value);
    ReopenSiapImaCdi(Controle);
  end;
end;

procedure TFmCunsCad.ExcluiSiapImaCui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do cuidado selecionado?',
  'siapimacui', 'Controle', QrSiapImaCuiControle.Value, DMod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSiapImaCui, QrSiapImaCuiCodigo,
    QrSiapImaCuiControle.Value);
    ReopenSiapImaCui(Controle);
  end;
end;

procedure TFmCunsCad.ExcluiSiapImaCxa1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrAtrSICxDef.RecordCount > 0 then
  begin
    Geral.MB_Info('Esta caixa d`�agua n�o pode ser exclu�da por ter atributos cadastrados!');
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT * FROM oscxa ',
  'WHERE Caixa=' + Geral.FF0(QrSiapImaCxaControle.Value),
  '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Geral.MB_Info('Esta caixa d`�agua n�o pode ser exclu�da por j� ter OS!');
    Exit;
  end;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da caixa d`�gua selecionada?',
  'siapimacxa', 'Controle', QrSiapImaCxaControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSiapImaCxa, QrSiapImaCxaCodigo,
    QrSiapImaCxaControle.Value);
    ReopenSiapImaCxa(Controle);
  end;
end;

procedure TFmCunsCad.ExcluiSiapImaDep1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do departamento selecionado?',
  'siapimadep', 'Controle', QrSiapImaDepControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSiapImaDep, QrSiapImaDepCodigo,
    QrSiapImaDepControle.Value);
    ReopenSiapImaDep(Controle);
  end;
end;

procedure TFmCunsCad.ExcluiSiapImaRes1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do residente selecionado?',
  'siapimares', 'Controle', QrSiapImaResControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSiapImaRes, QrSiapImaResCodigo,
    QrSiapImaResControle.Value);
    ReopenSiapImaRes(Controle);
  end;
end;

procedure TFmCunsCad.ExcluiterCad1Click(Sender: TObject);
var
  Codigo: Integer;
  CodTXT: String;
  Qry: TmySQLQuery;

  function TemRegistro(Local: String): Boolean;
  var
    Qtd: Integer;
  begin
    Qtd := Qry.FieldByName('ITENS').AsInteger;
    if Qtd > 0 then
    begin
      Result := True;
      Geral.MB_Aviso('Exclus�o cancelada!' + sLineBreak + 'Existem ' +
      Geral.FF0(Qtd) + ' registros referenciados em:' + sLineBreak+ Local);
    end else
      Result := False;
  end;
begin
  Codigo := QrSiapTerCadCodigo.Value;
  CodTXT := Geral.FF0(Codigo);
  Qry := TmySQLQuery.Create(Dmod);
  try
    //
    // verifica se h� OS gerada para este lugar
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT COUNT(*) ITENS ',
    'FROM oscab ',
    'WHERE SiapTerCad=' + CodTXT,
    '']);
    if TemRegistro('Lugares de OSs (ordens de servi�o)') then
      Exit;
    //
    // Verifica se h� contratos com locais deste lugar
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT COUNT(*) ITENS ',
    'FROM contratloc ',
    'WHERE Tabela=' + Geral.FF0(CO_TABELA_TIPO_LUGAR_001_SIAPIMADEP),
    'AND Cadastro IN ( ',
    '  SELECT Codigo ',
    '  FROM SiapImaCad ',
    '  WHERE SiapImaTer=' + CodTXT,
    ') ',
    '']);
    if TemRegistro('Locais contemplados de contratos') then
      Exit;
    //
    // Verifica se existem agendamentos para o lugar
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT COUNT(*) ITENS ',
    'FROM agendaeve ',
    'WHERE Local=' + CodTXT,
    '']);
    if TemRegistro('Itens de agendamento') then
      Exit;
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do lugar selecionado?',
    'siaptercad', 'Codigo', QrSiapTerCadCodigo.Value, Dmod.MyDB) = ID_YES then
    begin
      Codigo := GOTOy.LocalizaPriorNextIntQr(QrSiapterCad, QrSiapterCadCodigo,
        QrSiapTerCadCodigo.Value);
      ReopenSiapTerCad(Codigo);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmCunsCad.ExcluiTextoAtual1Click(Sender: TObject);
{
var
  Conta: Integer;
}
begin
{
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do texto selecionado?',
  'cunsimgcmt', 'Conta', QrCunsImgCmtConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrCunsImgCmt, QrCunsImgCmtCodigo,
    QrCunsImgCmtConta.Value);
    //ReopenCunsImgCmt(Conta);
  end;
}
end;

procedure TFmCunsCad.Inclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0) then
    Codigo := QrEntidadesCodigo.Value
  else
    Codigo := 0;
  //
  FmPrincipal.CadastroEntidades(Codigo, True, uetCliente1);
end;

procedure TFmCunsCad.IncluiAtrAMovDef1Click(Sender: TObject);
begin
  MostraFormAtrAMovDef(stIns);
end;

procedure TFmCunsCad.IncluiAtrSICdDef1Click(Sender: TObject);
begin
  MostraFormAtrSICdDef(stIns);
end;

procedure TFmCunsCad.IncluiAtrSICxDef1Click(Sender: TObject);
begin
  MostraFormAtrSICxDef(stIns);
end;

procedure TFmCunsCad.IncluiAtrSTCdDef1Click(Sender: TObject);
begin
  MostraFormAtrSTCdDef(stIns);
end;

procedure TFmCunsCad.IncluiDsSiapTerFlh1Click(Sender: TObject);
begin
  MostraFormSiapTerFlh(stIns);
end;

procedure TFmCunsCad.IncluinovaFoto1Click(Sender: TObject);
begin
  MostraFormCunsImgCab(stIns);
end;

procedure TFmCunsCad.Incluinovomvelautomvel1Click(Sender: TObject);
begin
  MostraFormMovAmovCad(stIns);
end;

procedure TFmCunsCad.IncluiNovoTexto1Click(Sender: TObject);
begin
  //MostraFormCunsImgCmt(stIns);
end;

procedure TFmCunsCad.IncluiSiapImaAti1Click(Sender: TObject);
begin
  MostraFormSiapImaAti(stIns);
end;

procedure TFmCunsCad.IncluiSiapImaCad1Click(Sender: TObject);
begin
  MostraFormSiapImaCad(stIns);
end;

procedure TFmCunsCad.IncluiSiapImaCav1Click(Sender: TObject);
begin
  MostraFormSiapImaCav(stIns);
end;

procedure TFmCunsCad.IncluiSiapImaCdi1Click(Sender: TObject);
begin
  MostraFormSiapImaCdi(stIns);
end;

procedure TFmCunsCad.IncluiSiapImaCui1Click(Sender: TObject);
begin
  MostraFormSiapImaCui(stIns);
end;

procedure TFmCunsCad.IncluiSiapImaCxa1Click(Sender: TObject);
begin
  MostraFormSiapImaCxa(stIns);
end;

procedure TFmCunsCad.IncluiSiapImaDep1Click(Sender: TObject);
begin
  MostraFormSiapImaDep(stIns);
end;

procedure TFmCunsCad.IncluiSiapImaRes1Click(Sender: TObject);
begin
  MostraFormSiapImaRes(stIns);
end;

procedure TFmCunsCad.IncluiTerCad1Click(Sender: TObject);
begin
  MostraFormSiapterCad(stIns);
end;

procedure TFmCunsCad.Intervalospsteros1Click(Sender: TObject);
begin
  AlterarIntervalosOuPeriodo(fldmonitDdPostero);
end;

{
procedure TFmCunsCad.InsAltSiapImaDep(SQLType: TSQLType);
var
  DestTabela, DestFldItem, DestFldValr,
  SrcTabela, SrcFldCodi, SrcFldNome, _WHERE_: String;
  DefaultItem, DefaultValr: Variant;
  TipoItem, TipoValr: TAllFormat;
  Titulo, PromptItem, PromptValr: String;
  Tamanho1, Tamanho2: Integer;
  DestNovoCodigo, SrcNovoCodigo: TNovoCodigo;
begin
  DestTabela := 'siapimadep';
  DestFldItem := 'Dependenci';
  DestFldValr := 'M2Area';
  SrcTabela := 'dependenci';
  SrcFldCodi := 'Codigo';
  SrcFldNome := 'Nome';
  _WHERE_ := '';
  DefaultItem := QrSiapImaDepDependenci.Value;
  DefaultValr := QrSiapImaDepM2Area.Value;
  TipoItem := dmktfInteger;
  TipoValr := dmktfDouble;
  Tamanho1 := 15;
  Tamanho2 := 2;
  Titulo := 'Adi��o de Depend�ncia';
  PromptItem := 'Depend�ncia:';
  PromptValr := '�rea:';
  DestNovoCodigo := ncGerlSeq1;
  SrcNovoCodigo := ncGerlSeq1;
  //
  UnCfgCadLista.InsAltItemLstEmRegTab(DestTabela, DestFldItem, DestFldValr,
  SrcTabela, SrcFldCodi, SrcFldNome, _WHERE_, DefaultItem,
  DefaultValr, TipoItem, TipoValr, Tamanho1, Tamanho2, SQLType,
  Titulo, PromptItem, PromptValr, DestNovoCodigo, SrcNovoCodigo,
  (*SQLIndex*)['Codigo', 'Controle'],
  (*ValIndex*)[QrSiapImaCadCodigo.Value, QrSiapImaDepControle.Value],
  QrSiapImaDep);
end;
}

procedure TFmCunsCad.CabAltera1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    ImgTipo.SQLType := stUpd;
    QrAtividades.Close;
    UMyMod.AbreQuery(QrAtividades, Dmod.MyDB);
    //
    QrHowFounded.Close;
    UMyMod.AbreQuery(QrHowFounded, Dmod.MyDB);
    //
    QrAccounts.Close;
    UMyMod.AbreQuery(QrAccounts, Dmod.MyDB);
    //
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCunsCad,
    [PnDados], [PnEdita], EdAtivPrinc, ImgTipo, 'cunscad');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCunsCad.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := QrCunsCadCodigo.Value;
    if UMyMod.ExcluiRegistroInt1(
    'Confirma a retirada da entidade selecionada do rol de clientes?',
    'cunscad', 'Codigo', Codigo, Dmod.MyDB) = ID_YES then
    begin
      Dmod.MyDB.Execute('UPDATE entidades set Cliente1="F" WHERE Codigo=' +
        Geral.FF0(Codigo));
      LocCod(Codigo, Codigo);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCunsCad.CarregaArquivoSVG(SQLType: TSQLType; Campo: String);
const
  IniDir = 'C:\Dermatek\SVG\Croquis\';
var
  ExtValid: TStringList;
  Ext, Dir, Nome, NoArq, Arquivo: String;
  Texto: TStringList;
  Codigo, Controle: Integer;
  Continua: Boolean;
  Valor: String;
begin
  if SQLType = stIns then
  begin
    ForceDirectories(IniDir);
    Dir      := IniDir;
    Nome     := '';
    Arquivo  := '';
    NoArq    := '';
    Controle := 0;
  end else
  begin
    Dir      := ExtractFileDir(QrSiapImaSVGNoArq.Value);
    Nome     := QrSiapImaSVGNome.Value;
    NoArq    := QrSiapImaSVGNoArq.Value;
    Controle := QrSiapImaSVGControle.Value;
  end;
  if (SQLType = stIns) or (Campo = 'Arquivo') then
  begin
    Geral.MB_Info('Orienta��es sobre o arquivo a ser importado:' + sLineBreak +
    '1. Caso utilize imagens de fundo para desenhar em cima dele ou usar textos,' + sLineBreak +
    '   Salve uma c�pia do original para futuras edi��es e uma c�pia conforme ' + sLineBreak +
    '   orienta��es abaixo para importar aqui.' + sLineBreak +
    '2. O arquivo dever ser vetorial (extens�o svg).' + sLineBreak +
    '3. Utilize preferencialmente o aplicativo Inkscape� para desenhar os croquis.' + sLineBreak +
    '4. N�o utilize, ou remova ap�s uso, arquivos de imagem (jpeg, bmp, etc.)' + sLineBreak +
    '5. N�o utilize recursos avan�ados no arquivo.' + sLineBreak +
    '6. Ao terminar o desenho do croqui no Inkscape� converta tudo para caminho:' + sLineBreak +
    '     V� em "Camada (Layer)" > "Gerenciador de camadas [Shift + Ctrl + L]".' + sLineBreak +
    '       Desbloqueie todas camdas que estiverem bloqueadas"' + sLineBreak +
    '     V� em "Caminho (Path)" > "Converter em caminho [Shift + Ctrl + C]".' + sLineBreak +
    '     V� em "Caminho (Path)" > "Converter contorno em caminho [Ctrl + Alt + C]".' + sLineBreak +
    '7. Para otimizar o tamanho na impress�o RMIP fa�a o seguinte no Inkscape�: ' + sLineBreak +
    '     V� em "Arquivo" > "Propriedades do desenho".' + sLineBreak +
    '       Clique em "Resize page content".' + sLineBreak +
    '       Clique em "Resize page to drawing or selection".' + sLineBreak +
    '8. Para finalizar salve com outro nome conforme o item 1. ' + sLineBreak +
    '');
    (*
    Continua := MyObjects.FileOpenDialog(Self, Dir, NoArq,
      'Selecione o arquivo a ser carregado', '', [], NoArq);
    if Continua then
    *)
    ExtValid := TStringList.Create;
    try
      ExtValid.Add('.svg');
      //
      NoArq := FmPrincipal.SelecionaImg(Dir, NoArq, ExtValid, False);
    finally
      ExtValid.Free;
    end;

    if NoArq <> '' then
    begin
      Screen.Cursor := crHourGlass;
      try
        if FileExists(NoArq) then
        begin
          Continua := False;
          Nome    := ExtractFileName(NoArq);
          Ext     := ExtractFileExt(Nome);
          Nome    := Copy(Nome, 1, Length(Nome) - Length(Ext));
          //
          Texto   := TStringList.Create;
          try
            Texto.LoadFromFile(NoArq);
            Arquivo := Texto.Text;
            Arquivo := DmkPF.XMLEncode(Texto.Text);
            //Arquivo := Geral.WideStringToSQLString(Arquivo);
            Continua := True;
          finally
            Texto.Free;
          end;
          //
          Codigo  := QrSiapImaCadCodigo.Value;
        end else
          Geral.MB_Aviso('Arquivo n�o encontrado!');
      finally
        Screen.Cursor := crDefault;
      end;
    end else
      Exit;
  end else
  begin
    Continua :=
      InputQuery('Descri��o de Croqui', 'Informe a nova descri��o:', Nome);
  end;
  if Continua then
  begin
    Screen.Cursor := crHourGlass;
    try
      Controle := UMyMod.BPGS1I32('siapimasvg', 'Controle', '', '', tsPos,
        SQLType, Controle);
      if SQLType = stIns then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'siapimasvg', False, [
        'Codigo', 'Nome', 'Arquivo',
        'NoArq'], [
        'Controle'], [
        Codigo, Nome, Arquivo,
        NoArq], [
        Controle], True)
      end else
      begin
        if Campo = 'Arquivo' then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'siapimasvg', False, [
          'Arquivo', 'NoArq'], ['Controle'], [
          Arquivo, NoArq], [Controle], True);
        end else
        if Campo = 'Nome' then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'siapimasvg', False, [
          'Nome'], ['Controle'], [
          Nome], [Controle], True);
        end else
        begin
          Geral.MB_Erro('Update em campo n�o implementado: ' + Campo);
          Exit;
        end;
        //
(*
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Tentamdo salvar o svg no Banco de Dados!');
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'siapimasvg', False, [
        Campo], [
        'Controle'], [
        Valor], [
        Controle], True) then
*)
        Screen.Cursor := crDefault;
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      end;
      //
      ReopenSiapImaSVG(Controle);
      Screen.Cursor := crDefault;
    except
      on E: Exception do
      begin
        Screen.Cursor := crDefault;
        Geral.MB_Erro('Erro ao salvar o svg no Banco de Dados!' +
          sLineBreak + E.Message);
      end;
    end;
  end;
end;

procedure TFmCunsCad.CarregaImagem();
var
  //, ArqPath
  Host, SDir, RDir, DirDest, Caminho: String;
  Empresa: Integer;
begin
  Host := Dmod.QrOpcoesBugsCunsIPv4.Value;
  SDir := Dmod.QrOpcoesBugsCunsSDir.Value;
  Empresa := 0;
  RDir := '';
  //
  if MyObjects.TentaDefinirDiretorio(Host, SDir, Empresa, RDir,
  LaAviso1, LaAviso2, False, nil, DirDest) then
  begin
    Caminho := DmkPF.CaminhoArquivo(DirDest, QrCunsImgCabCaminho.Value, '');
    if FileExists(Caminho) then
      ImgFoto.Picture.LoadFromFile(Caminho)
    else
      ImgFoto.Picture := nil;
  end;
end;

procedure TFmCunsCad.Carreganovoarquivo1Click(Sender: TObject);
begin
  CarregaArquivoSVG(stIns, '');
end;

procedure TFmCunsCad.CkAgeSoDoLugarClick(Sender: TObject);
begin
  ReopenSTCAgeEve(0);
end;

procedure TFmCunsCad.CNPJ1Click(Sender: TObject);
begin
  LocCod(QrCunsCadCodigo.Value,
  CuringaLoc.CriaFormEnti(CO_CODIGO, 'CNPJ', 'CPF', '', '', 'entidades', 'Tipo',
    0, Dmod.MyDB, Loc_SQLExtra(), True));
end;

procedure TFmCunsCad.ConfiguraAbreCroqui();
begin
  PCDados.ActivePageIndex            := 0;
  PCObjeto.ActivePageIndex           := 0;
  PCSiapTerCad.ActivePageIndex       := 0;
  PGDadosLocaisAplic.ActivePageIndex := 8;
  //
  VerificaHabilita();
end;

procedure TFmCunsCad.ConfiguraAbreSiapTerFlh();
begin
  PCDados.ActivePageIndex  := 0;
  PCObjeto.ActivePageIndex := 5;
  //
  MostraFormSiapTerFlh(stIns);
end;

procedure TFmCunsCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCunsCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCunsCad.ExcluiDsSiapTerFlh1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada da empresa selecionada?',
  'siapterflh', 'Controle', QrSiapTerFlhControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSiapTerFlh, QrSiapTerFlhControle,
    QrSiapTerFlhControle.Value);
    ReopenSiapTerFlh(Controle);
  end;
end;

procedure TFmCunsCad.Recarrega1Click(Sender: TObject);
begin
  CarregaArquivoSVG(stUpd, 'Arquivo');
end;

procedure TFmCunsCad.ReopenAtrAMovDef(ID_Item: Integer);
begin
  QrAtrAMovDef.Close;
  QrAtrAMovDef.Params[0].AsInteger := QrMovAmovCadCodigo.Value;
  UMyMod.AbreQuery(QrAtrAMovDef, DMod.MyDB);
  //
  QrAtrAMovDef.Locate('ID_Item', ID_Item, []);
end;

procedure TFmCunsCad.ReopenAtrSICdDef(ID_Item: Integer);
begin
  QrAtrSICdDef.Close;
  QrAtrSICdDef.Params[0].AsInteger := QrSiapImaCadCodigo.Value;
  UMyMod.AbreQuery(QrAtrSICdDef, Dmod.MyDB);
  //
  QrAtrSICdDef.Locate('ID_Item', ID_Item, []);
end;

procedure TFmCunsCad.ReopenAtrSICxDef(ID_Item: Integer);
begin
  QrAtrSICxDef.Close;
  QrAtrSICxDef.Params[0].AsInteger := QrSiapImaCxaControle.Value;
  UMyMod.AbreQuery(QrAtrSICxDef, Dmod.MyDB);
  //
  QrAtrSICxDef.Locate('ID_Item', ID_Item, []);
end;

procedure TFmCunsCad.ReopenAtrSTCdDef(ID_Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrSTCdDef, Dmod.MyDB, [
  'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS, ',
  'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,',
  'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp',
  'FROM atrstcddef def',
  'LEFT JOIN atrstcdits its ON its.Controle=def.AtrIts ',
  'LEFT JOIN atrstcdcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrSiapTerCadCodigo.Value),
  ' ',
  'UNION  ',
  ' ',
  'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, ',
  'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,',
  'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp ',
  'FROM atrstcdtxt def ',
  'LEFT JOIN atrstcdcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrSiapTerCadCodigo.Value),
  ' ',
  'ORDER BY NO_CAD, NO_ITS ',
  '']);
end;

procedure TFmCunsCad.ReopenCIC_Imp(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCIC_Imp, DModG.MyPID_DB, [
  'SELECT dep.Nome NO_DEPENDENCIA, cis.Nome NO_STATUS,  ',
  'nct.Nome NO_NCT, nct.TxtCAC, sic.SCompl2 NO_LOCAL, ',
  'stc.Nome NO_STC, cic.*  ',
  'FROM _cuns_img_cab cic  ',
  'LEFT JOIN ' + TMeuDB + '.cunsimgsit cis ON cis.Codigo=cic.Status  ',
  'LEFT JOIN ' + TMeuDB + '.siapimadep sid ON sid.Controle=cic.Dependencia ',
  'LEFT JOIN ' + TMeuDB + '.dependenci dep ON dep.Codigo=sid.Dependenci ',
  'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo',
  'LEFT JOIN ' + TMeuDB + '.siaptercad stc ON stc.Codigo=sic.SiapImaTer',
  'LEFT JOIN ' + TMeuDB + '.cunsimgnct nct ON nct.Codigo=cic.CodiNCT ',
  'WHERE cic.Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMixRel, DModG.MyPID_DB, [
  'SELECT SUM(IF(Aplicacao=0,1,0)) FC, ',
  'SUM(IF(Aplicacao=1,1,0)) NCT ',
  'FROM _cuns_img_cab ',
  '']);
end;

procedure TFmCunsCad.ReopenContratos();
var
  Fonte, Status: String;
begin
  Fonte := 'IF(DtaCntrFim > "1900-01-01", ' +
    Geral.FF0(CO_CONTRAT_STATUS_CONTRATO_COD_RESCINDI) + ', ctr.Status)';
  { OS 2256
  Status := dmkPF.ArrayToTexto(Fonte, ''(*'TXT_STATUS'*), pvNo, False,
    sListaStatusContratos);}
  Status := dmkPF.ArrayToTexto(Fonte, ''(*'TXT_STATUS'*), pvNo, True,
    sListaStatusContratos);
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB, [
  'SELECT ctr.Codigo, ctr.Nome,  ',
  'ctr.DContrato, ctr.DInstal, ctr.DVencimento, ctr.DtaPrxRenw, ctr.DtaCntrFim,  ',
  'ctr.Status, ctr.ValorMes, ctr.LugarNome, ctr.Versao , ',
  Status + ' TXT_STATUS ',
  'FROM Contratos ctr ',
  'LEFT JOIN entidades en1 ON en1.Codigo= ctr.Contratada ',
  'WHERE Contratante=' + Geral.FF0(QrEntidadesCodigo.Value),
  '']);
end;

procedure TFmCunsCad.ReopenCunsImgCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCunsImgCab, Dmod.MyDB, [
{
  'SELECT dep.Nome NO_DEPENDENCIA, cis.Nome NO_STATUS,  ',
  'nct.Nome NO_NCT, nct.TxtCAC, cic.*  ',
  'FROM cunsimgcab cic  ',
  'LEFT JOIN cunsimgsit cis ON cis.Codigo=cic.Status  ',
  'LEFT JOIN siapimadep sid ON sid.Controle=cic.Dependencia ',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
  'LEFT JOIN cunsimgnct nct ON nct.Codigo=cic.CodiNCT ',
  'WHERE cic.Codigo=' + Geral.FF0(QrCunsCadCodigo.Value),
  'ORDER BY Ordem ',
}
  'SELECT CONCAT(sic.SCompl2, " -> ", dep.Nome) NO_DEPENDENCIA, ',
  'cis.Nome NO_STATUS, nct.Nome NO_NCT, nct.TxtCAC, cic.*  ',
  'FROM cunsimgcab cic  ',
  'LEFT JOIN cunsimgsit cis ON cis.Codigo=cic.Status  ',
  'LEFT JOIN siapimadep sid ON sid.Controle=cic.Dependencia ',
  'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo',
  'LEFT JOIN siaptercad stc ON stc.Codigo=sic.SiapImaTer',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
  'LEFT JOIN cunsimgnct nct ON nct.Codigo=cic.CodiNCT ',
  'WHERE cic.Codigo=' + Geral.FF0(QrCunsCadCodigo.Value),
  'AND stc.Codigo=' + Geral.FF0(QrSiapTerCadCodigo.Value),
  'ORDER BY cic.Ordem, cic.Codigo ',
  '']);
  //
  if Controle <> 0 then
    QrCunsImgCab.Locate('Controle', Controle, []);
end;

{
procedure TFmCunsCad.ReopenCunsImgCmt(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCunsImgCmt, Dmod.MyDB, [
  'SELECT txg.Texto TXT_GENERIC, cic.* ',
  'FROM cunsimgcmt cic',
  'LEFT JOIN txtgeneric txg ON txg.Codigo=cic.TxtGeneric',
  'WHERE cic.Controle=' + Geral.FF0(QrCunsImgCabControle.Value),
  '']);
end;
}

procedure TFmCunsCad.ReopenCunsIts(CunsGru: Integer);
begin
  QrCunsIts.Close;
  QrCunsIts.Params[0].AsInteger := QrCunsCadCodigo.Value;
  UMyMod.AbreQuery(QrCunsIts, Dmod.MyDB);
  //
  QrCunsIts.Locate('CunsGru', CunsGru, []);
end;

procedure TFmCunsCad.ReopenEntDefAtr(Controle: Integer);
begin
  QrEntDefAtr.Close;
  QrEntDefAtr.Params[00].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntDefAtr, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrEntDefAtr.Locate('CTRL_ATR', Controle, []);
end;

procedure TFmCunsCad.ReopenEntiContat(Controle: Integer);
begin
  QrEntiContat.Close;
  QrEntiContat.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntiContat, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrEntiContat.Locate('Controle', Controle, []);
end;

procedure TFmCunsCad.ReopenEntiCtas();
begin
  QrEntiCtas.Close;
  QrEntiCtas.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntiCtas, Dmod.MyDB);
end;

procedure TFmCunsCad.ReopenEntidade();
begin
  QrEntidades.Close;
  QrEntidades.Params[0].AsInteger := QrCunsCadCodigo.Value;
  UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmCunsCad.ReopenEntiMail(Conta: Integer);
begin
  QrEntiMail.Close;
  QrEntiMail.Params[0].AsInteger := QrEntiContatControle.Value;
  UMyMod.AbreQuery(QrEntiMail, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiMail.Locate('Conta', Conta, []);
end;

procedure TFmCunsCad.ReopenEntiRespon(Controle: Integer);
begin
  QrEntiRespon.Close;
  QrEntiRespon.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntiRespon, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrEntiRespon.Locate('Controle', Controle, []);
end;

procedure TFmCunsCad.ReopenEntiTel(Conta: Integer);
begin
  QrEntiTel.Close;
  QrEntiTel.Params[0].AsInteger := QrEntiContatControle.Value;
  UMyMod.AbreQuery(QrEntiTel, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiTel.Locate('Conta', Conta, []);
end;

procedure TFmCunsCad.ReopenMovAmovCad(Codigo: Integer);
begin
  QrMovAmovCad.Close;
  QrMovAmovCad.Params[0].AsInteger := QrCunsCadCodigo.Value;
  UMyMod.AbreQuery(QrMovAmovCad, Dmod.MyDB);
  //
  if Codigo <> 0 then
    QrMovAmovCad.Locate('Codigo', Codigo, []);
end;

procedure TFmCunsCad.ReopenOSCab();
var
  Operacoes: String;
begin
  Operacoes := dmkPF.ArrayToTexto('osc.Operacao', 'NO_OPERACOES', pvPos, True,
    sListaOperacoesBugstrol);
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCab, Dmod.MyDB, [
  'SELECT DISTINCT  aec.Nome NO_AgeEqiCab, msv.Nome NO_MUL_SRV,  ',
  Operacoes,
  'CONCAT(osc.Estatus, " - ", sta.Nome) Estatus_TXT,  ',
  'cun.Nome NO_LUGAR,  ',
  'osc.*,  ',
  'IF(en1.Tipo=0, en1.RazaoSocial, en1.Nome) NO_EMPRESA,  ',
  'IF(en2.Tipo=0, en2.RazaoSocial, en2.Nome) NO_ENTCONTRAT,  ',
  'IF(en3.Tipo=0, en3.RazaoSocial, en3.Nome) NO_ENTPAGANTE,  ',
  'IF(en2.Tipo=0, en2.Fantasia, en2.Apelido) AF_ENTCONTRAT,  ',
  'IF(en3.Tipo=0, en3.Fantasia, en3.Apelido) AF_ENTPAGANTE,  ',
  'enc.Nome NO_ENTICONTAT,  ',
  'se1.Login NO_USER_CAD, se2.Login NO_USER_ALT,  ',
  'ELT(osc.HowGerou+1, "Manual", "Clone", "OS M�e", "?????") NO_WOW_GEROU, ',
  'ELT(osc.PosGerou+1, "N�o", "Sim", "???") NO_POS_GEROU,  ',
  'ELT(osc.SohInicial+1, "N�o", "Sim", "???") NO_SOH_INICIAL,  ',
  'ELT(osc.StPipAdPrg+1, "N�o iniciado", "Em inclus�o", "Finalizado", ',
  '"?????") NO_ST_PIP_AD_PRG ',
  'FROM oscab osc  ',
  'LEFT JOIN enticliint eci ON eci.CodCliInt=osc.Empresa  ',
  'LEFT JOIN entidades en1 ON en1.Codigo=eci.CodEnti  ',
  'LEFT JOIN entidades en2 ON en2.Codigo=osc.EntContrat  ',
  'LEFT JOIN enticontat enc ON enc.Controle=osc.EntiContat  ',
  'LEFT JOIN estatusoss sta ON sta.Codigo=osc.Estatus  ',
  'LEFT JOIN mulservico msv ON msv.Codigo=osc.MulServico  ',
  'LEFT JOIN ageeqicab aec ON aec.Codigo=osc.AgeEqiCab  ',
  'LEFT JOIN entidades en3 ON en3.Codigo=osc.EntPagante  ',
  'LEFT JOIN siaptercad cun ON cun.Codigo=osc.SiapTerCad  ',
  'LEFT JOIN senhas se1 ON se1.Numero=osc.UserCad ',
  'LEFT JOIN senhas se2 ON se2.Numero=osc.UserAlt ',
  'WHERE osc.Entidade=' + Geral.FF0(QrEntidadesCodigo.Value),
  'ORDER BY Codigo DESC ',
  '']);
end;

procedure TFmCunsCad.ReopenPMVs(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPMVs, Dmod.MyDB, [
  'SELECT mot.Nome NO_MOTDESAT, gg1.Nome NO_EQUI, ',
  'plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA, ',
  'IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT, ',
  'IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT, ',
  'cad.*, omc.DdPostero, omc.PerioDd ',
  'FROM pipcad cad ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab ',
  'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci ',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
  'LEFT JOIN osmoncab omc ON omc.Conta=cad.OsMonCab ',
  'LEFT JOIN oscab osc ON osc.Codigo=omc.Codigo ',
  'WHERE osc.Entidade=' + Geral.FF0(QrCunsCadCodigo.Value),
  'AND (cad.MotInutili = 0 AND cad.MotInutili = 0) ',
  'ORDER BY Nome ',
  '']);
  //
  if Codigo <> 0 then
    QrPMVs.Locate('Codigo', Codigo, []);
end;

procedure TFmCunsCad.ReopenSiapImaAti(Controle: Integer);
begin
  QrSiapImaAti.Close;
  QrSiapImaAti.Params[0].AsInteger := QrSiapImaCadCodigo.Value;
  UMyMod.AbreQuery(QrSiapImaAti, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrSiapImaAti.Locate('Controle', Controle, []);
end;

procedure TFmCunsCad.ReopenSiapImaCad(Codigo: Integer);
begin
  QrSiapImaCad.Close;
  QrSiapImaCad.Params[0].AsInteger := QrSiapTerCadCodigo.Value;
  UMyMod.AbreQuery(QrSiapImaCad, Dmod.MyDB);
  //
  if Codigo <> 0 then
    QrSiapImaCad.Locate('Codigo', Codigo, []);
end;

procedure TFmCunsCad.ReopenSiapImaCav(Controle: Integer);
begin
  QrSiapImaCav.Close;
  QrSiapImaCav.Params[0].AsInteger := QrSiapImaCadCodigo.Value;
  UMyMod.AbreQuery(QrSiapImaCav, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrSiapImaCav.Locate('Controle', Controle, []);
end;

procedure TFmCunsCad.ReopenSiapImaCdi(Controle: Integer);
begin
  QrSiapImaCdi.Close;
  QrSiapImaCdi.Params[0].AsInteger := QrSiapImaCadCodigo.Value;
  UMyMod.AbreQuery(QrSiapImaCdi, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrSiapImaCdi.Locate('Controle', Controle, []);
end;

procedure TFmCunsCad.ReopenSiapImaCui(Controle: Integer);
begin
  QrSiapImaCui.Close;
  QrSiapImaCui.Params[0].AsInteger := QrSiapImaCadCodigo.Value;
  UMyMod.AbreQuery(QrSiapImaCui, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrSiapImaCui.Locate('Controle', Controle, []);
end;

procedure TFmCunsCad.ReopenSiapImaCxa(Controle: Integer);
begin
  QrSiapImaCxa.Close;
  QrSiapImaCxa.Params[0].AsInteger := QrSiapImaCadCodigo.Value;
  UMyMod.AbreQuery(QrSiapImaCxa, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrSiapImaCxa.Locate('Controle', Controle, []);
end;

procedure TFmCunsCad.ReopenSiapImaDep(Controle: Integer);
begin
  QrSiapImaDep.Close;
  QrSiapImaDep.Params[0].AsInteger := QrSiapImaCadCodigo.Value;
  UMyMod.AbreQuery(QrSiapImaDep, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrSiapImaDep.Locate('Codigo', Controle, []);
end;

procedure TFmCunsCad.ReopenSiapImaRes(Controle: Integer);
begin
  QrSiapImaRes.Close;
  QrSiapImaRes.Params[0].AsInteger := QrSiapImaCadCodigo.Value;
  UMyMod.AbreQuery(QrSiapImaRes, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrSiapImaRes.Locate('Controle', Controle, []);
end;

procedure TFmCunsCad.ReopenSiapImaSVG(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapImaSVG, Dmod.MyDB, [
  'SELECT svg.*, ELT(svg.RotarGraus + 1, "N�o", ',
  '"90�", "180�", "270�", "???") NO_RotarGraus  ',
  'FROM siapimasvg svg',
  'WHERE svg.Codigo=' + Geral.FF0(QrSiapImaCadCodigo.Value),
  '']);
  //
  if Controle <> 0 then
    QrSiapImaSVG.Locate('Controle', Controle, []);
end;

procedure TFmCunsCad.ReopenSiapTerCad(Codigo: Integer);
begin
  QrSiapTerCad.Close;
  QrSiapTerCad.Params[0].AsInteger := QrCunsCadCodigo.Value;
  UMyMod.AbreQuery(QrSiapTerCad, Dmod.MyDB);
  //
  if Codigo <> 0 then
    QrSiapTerCad.Locate('Codigo', Codigo, []);
end;

procedure TFmCunsCad.ReopenSiapterFlh(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapTerFlh, Dmod.MyDB, [
  'SELECT * ',
  'FROM siapterflh ',
  'WHERE Codigo=' + Geral.FF0(QrSiapTerCadCodigo.Value),
  '']);
  //
  if Controle <> 0 then
    QrSiapTerFlh.Locate('Controle', Controle, []);
end;

procedure TFmCunsCad.ReopenSTCAgeEve(Codigo: Integer);
var
  SQL_WHR: String;
begin
  if CkAgeSoDoLugar.Checked then
    SQL_WHR := 'WHERE age.Local=' + Geral.FF0(QrSiapTerCadCodigo.Value)
  else
    SQL_WHR := 'WHERE Terceiro=' + Geral.FF0(QrCunsCadCodigo.Value);
  UnDmkDAC_PF.AbreMySQLQuery0(QrSTCAgeEve, Dmod.MyDB, [
  'SELECT fgd.Nome NO_FATOGERADR, age.* ',
  'FROM agendaeve age ',
  'LEFT JOIN fatogeradr fgd ON fgd.Codigo=age.FatoGeradr ',
  SQL_WHR,
  'ORDER BY Inicio DESC',
  '']);
  //
  if Codigo <> 0 then
    QrSTCAgeEve.Locate('Codigo', Codigo, []);
end;

procedure TFmCunsCad.ReopenSubCli(CunsSub: Integer);
begin
  QrSubCli.Close;
  //QrSubCli.Params[0].AsInteger := QrSiapImaCadCodigo.Value;
  QrSubCli.Params[0].AsInteger := QrCunsCadCodigo.Value;
  UMyMod.AbreQuery(QrSubCli, Dmod.MyDB);
  //
  QrSubCli.Locate('CunsSub', CunsSub, []);
end;

procedure TFmCunsCad.RetiraContratante1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada do contratante selecionado?',
  'cunsits', 'Controle', QrCunsItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCunsIts, QrCunsItsControle,
    QrCunsItsControle.Value);
    ReopenCunsIts(Controle);
  end;
end;

procedure TFmCunsCad.DBGCunsImgCabMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMCunsImgCab, DBGCunsImgCab, X,Y);
end;

procedure TFmCunsCad.DBGMovAmovCadCellClick(Column: TColumn);
begin
  PCObjeto.ActivePageIndex := 1;
end;

procedure TFmCunsCad.DBGMovAmovCadEnter(Sender: TObject);
begin
  PCObjeto.ActivePageIndex := 1;
end;

procedure TFmCunsCad.DBGMovAmovCadMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMMovAmovCad, DBGMovAmovCad, X,Y);
  *)
end;

procedure TFmCunsCad.DBGPsqDblClick(Sender: TObject);
const
  Atual = 0;
  Disposicao = dispIndefinido;
  ReabreOSCab = True;
var
  Form: TForm;
  Grupo, Opcao, Lugar, OSCab: Integer;
begin
  if (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0) then
  begin
    Grupo := QrOSCabGrupo.Value;
    Opcao := QrOSCabOpcao.Value;
    Lugar := QrOSCabSiapTerCad.Value;
    OSCab := QrOSCabCodigo.Value;
    //
    Form := MyObjects.FormTDICria(TFmOSCab2, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
    TFmOSCab2(Form).LocCod(Atual, Grupo, Disposicao, Lugar, Opcao, ReabreOSCab, OSCab);
  end;
end;

procedure TFmCunsCad.DBGCunsImgCmtMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
{
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMCunsImgCmt, DBGCunsImgCmt, X,Y);
}
end;

procedure TFmCunsCad.DBGSiapImaAtiMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMSiapImaAti, DBGSiapImaAti, X,Y);
  *)
end;

procedure TFmCunsCad.DBGSiapImaCxaMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMSiapImaCxa, DBGSiapImaCxa, X,Y);
  *)
end;

procedure TFmCunsCad.DBGSiapImaCdiMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMSiapImaCdi, DBGSiapImaCdi, X,Y);
  *)
end;

procedure TFmCunsCad.DBGSiapImaCavMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMSiapImaCav, DBGSiapImaCav, X,Y);
  *)
end;

procedure TFmCunsCad.DBGSiapImaResMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMSiapImaRes, DBGSiapImaRes, X,Y);
  *)
end;

procedure TFmCunsCad.DBGSiapImaSVGDblClick(Sender: TObject);
var
  Cliente, SiapTerCad, SiapImaCad, SiapImaSVG: Integer;
begin
  if (QrSiapImaSVG.State <> dsInactive) and (QrSiapImaSVG.Recordcount > 0) then
  begin
    if DBCheck.CriaFm(TFmCunsCadSVG1, FmCunsCadSVG1, afmoNegarComAviso) then
    begin
      Cliente    := QrSiapTerCadCliente.Value;
      SiapTerCad := QrSiapTerCadCodigo.Value;
      SiapImaCad := QrSiapImaCadCodigo.Value;
      SiapImaSVG := QrSiapImaSVGControle.Value;
      //
      FmCunsCadSVG1.CarregaDadosCliente(Cliente, SiapTerCad, SiapImaCad, SiapImaSVG);
      FmCunsCadSVG1.ShowModal;
      //
      FmCunsCadSVG1.Destroy;
    end;
  end else
    Geral.MB_Aviso('N�o h� item para vizualizar!');
end;

procedure TFmCunsCad.DBGSiapImaCuiMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMSiapImaCui, DBGSiapImaCui, X,Y);
  *)
end;

procedure TFmCunsCad.DBGSiapTerCadCellClick(Column: TColumn);
begin
  PCObjeto.ActivePageIndex := 0;
end;

procedure TFmCunsCad.DBGSiapTerCadEnter(Sender: TObject);
begin
  PCObjeto.ActivePageIndex := 0;
end;

procedure TFmCunsCad.DBGSiapTerCadMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMSiapTerCad, DBGSiapTerCad, X,Y);
  *)
end;

procedure TFmCunsCad.DBGSiapImaCadMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
{
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMSiapImaCad, DBGSiapImaCad, X,Y);
}
end;

procedure TFmCunsCad.DBGSiapImaDepMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
{
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMSiapImaDep, DBGSiapImaDep, X,Y);
}
end;

function TFmCunsCad.DefineArquivo(): String;
(*
var
  Host, SDir, RDir, DirDest, NoArq: String;
  Empresa: Integer;
begin
  Result := '';
  Host := Dmod.QrOpcoesBugsCunsIPv4.Value;
  SDir := Dmod.QrOpcoesBugsCunsSDir.Value;
  Empresa := 0;
  RDir := '';
  NoArq := QrCIC_ImpCaminho.Value;
  //
  if MyObjects.TentaDefinirDiretorio(
  Host, SDir, Empresa, RDir, LaAviso1, LaAviso2, False, nil, DirDest) then
    Result := dmkPF.CaminhoArquivo(DirDest, NoArq, '');
*)
begin
  Result := Dmod.DefineArquivo(QrCIC_ImpCaminho.Value, LaAviso1, LaAviso2, MeANL);
end;

procedure TFmCunsCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCunsCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCunsCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCunsCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCunsCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCunsCad.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(QrAtividades.Database, 'atividades',
  QrAtividadesNome.Size, ncGerlSeq1, 'Cadastro de Atividades',
  [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(EdAtivPrinc, CBAtivPrinc, QrAtividades, VAR_CADASTRO);
end;

procedure TFmCunsCad.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  DModG.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, True);
  UMyMod.SetaCodigoPesquisado(EdAccount, CBAccount, QrAccounts, VAR_CADASTRO);
end;

procedure TFmCunsCad.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(QrHowFounded.Database, 'howfounded',
  QrHowFoundedNome.Size, ncGerlSeq1, 'Cadastro de Tipos de Marketing',
  [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(EdHowFind, CBHowFind, QrHowFounded, VAR_CADASTRO);
end;

procedure TFmCunsCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCunsCad.BtSiapTerCadClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapTerCad, BtSiapTerCad);
end;

procedure TFmCunsCad.BtWUsersClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrWUsers.State <> dsInactive) and (QrWUsers.RecordCount > 0) then
    Codigo := QrWUsersCodigo.Value
  else
    Codigo := 0;
  //
  UWUsersJan.MostraWUsers(Codigo);
  ReopenWUsers;
end;

procedure TFmCunsCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCunsCadCodigo.Value;
  VAR_CADASTRO2 := QrSiapTerCadCodigo.Value;
  //
  if TFmCunsCad(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmCunsCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrCunsCadCodigo.Value;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'cunscad', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmCunsCad.BtCroMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapImaSVG, BtCroMenu);
end;

procedure TFmCunsCad.BtCroVisClick(Sender: TObject);
begin
  DBGSiapImaSVGDblClick(Self);
end;

procedure TFmCunsCad.BtCunsItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCunsIts, BtCunsIts);
end;

procedure TFmCunsCad.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmCunsCad.BtDiarioGer2Click(Sender: TObject);
begin
  MostraPaneledDiarioGer2();
  //
  PCDados.ActivePageIndex := 6;
end;

procedure TFmCunsCad.BtEntidadeClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrCunsCad.State <> dsInactive) and (QrCunsCad.RecordCount > 0) then
    Codigo := QrCunsCadCodigo.Value
  else
    Codigo := 0;

  FmPrincipal.CadastroEntidades(Codigo, True);
end;

procedure TFmCunsCad.BtOSsClick(Sender: TObject);
begin
  ReopenOSCab();
  //
  PCDados.ActivePageIndex := 5;
end;

procedure TFmCunsCad.AdicionaContratante1Click(Sender: TObject);
const
  Controle = 0;
var
  _WHERE_: String;
begin
  _WHERE_ := 'WHERE Codigo <> ' + Geral.FF0(QrEntidadesCodigo.Value);
  //
{
  UnCfgCadLista.InsAltItemLstEmRegTab('cunsits', 'CunsGru', '', 'entidades',
  CO_CODIGO, CO_NOME_ENT, _WHERE_, 0, null, dmktfInteger, dmktfNone,
  0, 0, stIns, 'Adi��o de Contratante', 'Entidade contratante:',
  '', ncGerlSeq1, ncGerlSeq1, ['CunsSub', 'Controle'],
  [QrEntidadesCodigo.Value, QrCunsItsControle.Value], QrCunsIts);
}
  UnCfgCadLista.InsAltItemLstEmRegTab('cunsits', 'CunsGru', 'Controle', '',
    'entidades', CO_CODIGO, CO_NOME_ENT, _WHERE_, 0, null, dmktfInteger,
    dmktfNone, 0, 0, stIns, 'Adi��o de Contratante', 'Entidade contratante:',
    '', ncGerlSeq1, ncGerlSeq1, ['CunsSub', 'Controle'],
    [QrEntidadesCodigo.Value, Controle], QrCunsIts);
end;

procedure TFmCunsCad.Alteraadescrio1Click(Sender: TObject);
begin
  //CarregaArquivoSVG(stUpd, 'Nome');
  MostraFormSiapImaSVG(stUpd);
end;

procedure TFmCunsCad.AlteraAtrAMovDef1Click(Sender: TObject);
begin
  MostraFormAtrAMovDef(stUpd);
end;

procedure TFmCunsCad.AlteraAtrSICdDef1Click(Sender: TObject);
begin
  MostraFormAtrSICdDef(stUpd);
end;

procedure TFmCunsCad.AlteraAtrSICxDef1Click(Sender: TObject);
begin
  MostraFormAtrSICxDef(stUpd);
end;

procedure TFmCunsCad.AlteraAtrSTCdDef1Click(Sender: TObject);
begin
  MostraFormAtrSTCdDef(stUpd);
end;

procedure TFmCunsCad.AlteraDsSiapTerFlh1Click(Sender: TObject);
begin
  MostraFormSiapTerFlh(stUpd);
end;

procedure TFmCunsCad.AlteraFotoatual1Click(Sender: TObject);
begin
  MostraFormCunsImgCab(stUpd);
end;

procedure TFmCunsCad.Alteramvelautomvel1Click(Sender: TObject);
begin
  MostraFormMovAmovCad(stUpd);
end;

procedure TFmCunsCad.AlteraPMVatual1Click(Sender: TObject);
begin
  if QrPMVs.RecordCount > 0 then
  begin
    if DBGPMVs.SelectedRows.Count > 1 then
    begin
      Geral.MB_Aviso('Apenas um PMV deve estar selecionado!');
      Exit;
    end;
    FmPrincipal.MostraFormPipRapido(QrPMVsCodigo.Value, stUpd,
      QrCunsCadCodigo.Value, 0, True);
    //
    ReopenPMVs(QrPMVsCodigo.Value);
  end;
end;

procedure TFmCunsCad.AlterarIntervalosOuPeriodo(CampoMonit: TCampoMonit);
const
  ValorMin = '';
  ValorMax = '';
var
  Q: TSelType;
  WidthCaption, I, Valor: Integer;
  ValCaption, ValTitle, Fld: String;
  ValVar: Variant;
  //
  procedure AlteraItemAtual();
  var
    Conta: Integer;
  begin
    Conta := QrPMVsOSMonCab.Value;
    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osmoncab', False, [
    Fld], ['Conta'], [
    Valor], [Conta], True);
  end;
var
  PMV: Integer;
begin
  PMV := QrPMVsCodigo.Value;
  case CampoMonit of
    fldmonitDdPostero:
    begin
      fld := 'DdPostero';
      Valor := 28;
      ValTitle := 'Intervalos P�steros de Monitoramento';
      ValCaption := 'Intervalo em dias:';
    end;
    fldmonitPerioDd:
    begin
      fld := 'PerioDd';
      Valor := 90;
      ValTitle := 'Per�odo de Monitoramento';
      ValCaption := 'Total de dias:';
    end;
  end;
  WidthCaption := Length(ValCaption) * 7;
  if not MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, Valor, 3, 0,
  ValorMin, ValorMax, True, ValTitle, ValCaption, WidthCaption, ValVar) then
    Exit;
  //
  Valor := ValVar;
  //
  DBCheck.Quais_Selecionou(QrPMVs, TDBGrid(DBGPMVs), Q);
  case Q of
    istAtual: AlteraItemAtual();
    istSelecionados:
    begin
      with DBGPMVs.DataSource.DataSet do
      for I := 0 to DBGPMVs.SelectedRows.Count - 1 do
      begin
        GotoBookmark(pointer(DBGPMVs.SelectedRows.Items[I]));
        AlteraItemAtual();
      end;
    end;
    istTodos:
    begin
      QrPMVs.First;
      while not QrPMVs.Eof do
      begin
        AlteraItemAtual();
        //
        QrPMVs.Next;
      end;
    end;
  end;
  QrPMVs.EnableControls;
  //
  ReopenPMVs(PMV);
end;

procedure TFmCunsCad.AlteraSiapImaAti1Click(Sender: TObject);
begin
  MostraFormSiapImaAti(stUpd);
end;

procedure TFmCunsCad.AlteraSiapImaCad1Click(Sender: TObject);
begin
  MostraFormSiapImaCad(stUpd);
end;

procedure TFmCunsCad.AlteraSiapImaCav1Click(Sender: TObject);
begin
  MostraFormSiapImaCav(stUpd);
end;

procedure TFmCunsCad.AlteraSiapImaCdi1Click(Sender: TObject);
begin
  MostraFormSiapImaCdi(stUpd);
end;

procedure TFmCunsCad.AlteraSiapImaCui1Click(Sender: TObject);
begin
  MostraFormSiapImaCui(stUpd);
end;

procedure TFmCunsCad.AlteraSiapImaCxa1Click(Sender: TObject);
begin
  MostraFormSiapImaCxa(stUpd);
end;

procedure TFmCunsCad.AlteraSiapImaDep1Click(Sender: TObject);
begin
  MostraFormSiapImaDep(stUpd);
end;

procedure TFmCunsCad.AlteraSiapImaRes1Click(Sender: TObject);
begin
  MostraFormSiapImaRes(stUpd);
end;

procedure TFmCunsCad.AlteraTerCad1Click(Sender: TObject);
begin
  MostraFormSiapterCad(stUpd);
end;

procedure TFmCunsCad.AlteraTextoAtual1Click(Sender: TObject);
begin
  //MostraFormCunsImgCmt(stUpd);
end;

procedure TFmCunsCad.BtSiapImaCxaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapImaCxa, BtSiapImaCxa);
end;

procedure TFmCunsCad.BtSiapImaAtiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapImaAti, BtSiapImaAti);
end;

procedure TFmCunsCad.BtSiapImaCuiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapImaCui, BtSiapImaCui);
end;

procedure TFmCunsCad.SbSelfGer2Click(Sender: TObject);
begin
  MostraPeneledSelfGer2();
  //
  PCDados.ActivePageIndex := 3;
end;

procedure TFmCunsCad.BtAtrSICdDefClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAtrSICdDef, BtAtrSICdDef);
end;

procedure TFmCunsCad.BtSiapImaCadClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapImaCad, BtSiapImaCad);
end;

procedure TFmCunsCad.BtSiapImaDepClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapImaDep, BtSiapImaDep);
end;

procedure TFmCunsCad.BtSiapImaCdiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapImaCdi, BtSiapImaCdi);
end;

procedure TFmCunsCad.BtSiapImaCavClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapImaCav, BtSiapImaCav);
end;

procedure TFmCunsCad.BtSiapImaResClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapImaRes, BtSiapImaRes);
end;

procedure TFmCunsCad.BtAtrSICxDefClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAtrSICxDef, BtAtrSICxDef);
end;

procedure TFmCunsCad.BtCunsCadClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCunsCad);
end;

procedure TFmCunsCad.FormCreate(Sender: TObject);
const
  Txt = 'Criando janela de clientes... ';
var
  MaiorCodigo: Integer;
  Aba: TTabSheet;
  Msg: String;
  //
  procedure MudaCaption(Texto: String);
  begin
    if Aba <> nil then
    begin
      Aba.Caption := Txt + Texto;
      Application.ProcessMessages;
    end;
  end;
begin
  LaContratanteAviso.Caption := 'Preencher apenas se o contratante' + sLineBreak + 'for diferente da entidade';

  if TFmCunsCad(Self).Owner is TApplication then
    Aba := nil
  else
    Aba := TTabSheet(Self.Owner);
  //
  MudaCaption(Txt);
  //
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PCDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  PCDados.ActivePageIndex := 0;
  PCObjeto.ActivePageIndex := 0;
  // ENTIDADES
  //PCEditGer.ActivePageIndex := 0;
  PCShowGer.ActivePageIndex := 0;
  PCXtraGer.ActivePageIndex := 0;
  PCSiapTerCad.ActivePageIndex := 0;
  //
  //PCEditGer.Align    := alClient;
  //PageControl2.Align := alClient;
  //PageControl3.Align := alClient;
  //PnEdits.Align      := alClient;
  //PnEdit2.Align      := alClient;
  //PnEdit2PJ.Align    := alClient;
  //PnEdit2PF.Align    := alClient;
  //
  PnShow2PJ.Align    := alClient;
  PnShow2PF.Align    := alClient;
  PnShowEnd.Align    := alClient;
  PCShowCom.Align    := alClient;
  PCShowRes.Align    := alClient;
  PCXtraGer.Align    := alClient;
  // FIM ENTIDADES
  PGDadosLocaisAplic.ActivePageIndex := 0;
  PGCaixaDAgua.ActivePageIndex       := 0;
  PG_NCT_CAC.ActivePageIndex         := 0;
  //
  MudaCaption('Localizando novos clientes...');
  if OSAll_PF.AchouNovosClientes(MaiorCodigo) then
    LocCod(MaiorCodigo, MaiorCodigo);
  //
  // FmPnAnotacoes
  MudaCaption('Criando janela de anotacoes...');
  FThisFmPnAnotacoes :=
  MyObjects.FormTDIPanel(TFmPnAnotacoes, FmPnAnotacoes, PnFmPnAnotacoes);
  MudaCaption('Configurando janela de anotacoes 1...');
  FmPnAnotacoes.DefineFiltroLink([
    CO_ANOTACOES_TAB_0001_UNIVERSAL,
    CO_ANOTACOES_TAB_0001_ENTIDADES,
    CO_ANOTACOES_TAB_0002_SIAPTERCAD]
{
    ,
    //Tipo > 0 = DB.Tabela.Campo
    [TYP_ANOTACOES_TAB_0_TABELA,
    TYP_ANOTACOES_TAB_0_TABELA,
    TYP_ANOTACOES_TAB_0_TABELA],
    //  DBs
    [DMod.MyDB, DMod.MyDB, DMod.MyDB],
    // Tabelas
    ['', 'Entidades', 'SiapTerCad'],
    // Campos
    ['', 'Codigo', 'Codigo']
}
    );
{  TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1[CO_ANOTACOES_TAB_0001_ENTIDADES] :=
    QrCunsCadCodigo.Value;
  TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1[CO_ANOTACOES_TAB_0002_SIAPTERCAD] :=
    QrSiapTerCadCodigo.Value;
}
  MudaCaption('Configurando janela de anotacoes 2...');
  UnAnotacoes.DefineVariavelEmFormTDIPnl(FThisFmPnAnotacoes,
  (*TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1,*) CO_ANOTACOES_TAB_0001_ENTIDADES,
  QrCunsCadCodigo.Value, True);
  //
  MudaCaption('Configurando janela de anotacoes 3...');
  UnAnotacoes.DefineVariavelEmFormTDIPnl(FThisFmPnAnotacoes,
  (*TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1,*) CO_ANOTACOES_TAB_0002_SIAPTERCAD,
  QrSiapTerCadCodigo.Value, True);
  // FIM FmPnAnotacoes
  ///////////////////////////////////////////////////////////////////////////////
  // FmPnAnotacoes
  MudaCaption('Criando janela de documentos...');
  FThisFmDocsCab :=
  MyObjects.FormTDIPanel(TFmDocsCab, FmDocsCab, PnDocsCab);
  MudaCaption('Configurando janela de Documentos 1...');
  FmDocsCab.DefineFiltroLink([
    CO_ANOTACOES_TAB_0001_UNIVERSAL,
    CO_ANOTACOES_TAB_0001_ENTIDADES,
    CO_ANOTACOES_TAB_0002_SIAPTERCAD]
    );
  MudaCaption('Configurando janela de Documentos 2...');
  UnDocsCab.DefineVariavelEmFormTDIPnl(FThisFmDocsCab,
  CO_ANOTACOES_TAB_0001_ENTIDADES,
  QrCunsCadCodigo.Value, True);
  //
  MudaCaption('Configurando janela de Documentos 3...');
  UnDocsCab.DefineVariavelEmFormTDIPnl(FThisFmDocsCab,
  CO_ANOTACOES_TAB_0002_SIAPTERCAD,
  QrSiapTerCadCodigo.Value, True);
  // FIM FmDocsCab

  //
  MudaCaption('Finalizando config janela clientes...');
  //
  DBGCunsIts.PopupMenu    := PMCunsIts;
  DBGSiapImaSVG.PopupMenu := PMSiapImaSVG;
  DBGSiapTerCad.PopupMenu := PMSiapTerCad;
  DBGMovAmovCad.PopupMenu := PMMovAmovCad;
  DBGSiapImaCdi.PopupMenu := PMSiapImaCdi;
  DBGSiapImaCav.PopupMenu := PMSiapImaCav;
  DBGSiapImaRes.PopupMenu := PMSiapImaRes;
  DBGSiapImaCui.PopupMenu := PMSiapImaCui;
  DBGSiapImaAti.PopupMenu := PMSiapImaAti;
  DBGSiapImaCxa.PopupMenu := PMSiapImaCxa;
  DBGAtrSICxDef.PopupMenu := PMAtrSICxDef;
  DBGAtrSICdDef.PopupMenu := PMAtrSICdDef;
  DBGAtrSTCdDef.PopupMenu := PMAtrSTCdDef;
  DBGAtrAMovDef.PopupMenu := PMAtrAMovDef;
  dmkDBGridZTO1.PopupMenu := PMSTCAgeEve;
  DBGCunsImgCab.PopupMenu := PMCunsImgCab;
  DBGSiapImaCad.PopupMenu := PMSiapImaCad;
  DBGSiapImaDep.PopupMenu := PMSiapImaDep;
  DBGSiapTerFlh.PopupMenu := PMSiapTerFlh;
  DBGPMVs.PopupMenu       := PMPMVs;
  //
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMCunsIts, DBGCunsIts, X,Y);
  MudaCaption('OnCreate janela clientes ok...');
  //
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg) then
    FUsrWeb := True
  else
    FUsrWeb := False;
  //
  TSWeb.TabVisible      := FUsrWeb;
  dmkDBGrid2.DataSource := DsWUsers;
end;

procedure TFmCunsCad.SbNumeroClick(Sender: TObject);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Codigo(QrCunsCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCunsCad.SbProvidenciasClick(Sender: TObject);
begin
  MostraPaneledOSPrvGer();
  //
  PCDados.ActivePageIndex := 4;
end;

procedure TFmCunsCad.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmCunsCad.SbNomeClick(Sender: TObject);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.NomeRelacionado(LaRegistro.Caption);
end;

procedure TFmCunsCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCunsCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCunsCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCunsCad.QrCunsCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtCunsIts.Enabled    := QrCunsCad.RecordCount > 0;
  BtSiapTerCad.Enabled := QrCunsCad.RecordCount > 0;
end;

procedure TFmCunsCad.QrCunsCadAfterScroll(DataSet: TDataSet);
begin
  QrSTCAgeEve.Close;  // Peder estar setado para a entidade no CkAgeSoDoLugar
  //
  ReopenCunsIts(0);
  ReopenSiapTerCad(0);
  ReopenMovAmovCad(0);
  ReopenSubCli(0);
  ReopenEntidade();
  //
  if FThisFmPnAnotacoes <> nil then
  begin
    UnAnotacoes.DefineVariavelEmFormTDIPnl(FThisFmPnAnotacoes,
    (*TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1,*) CO_ANOTACOES_TAB_0001_ENTIDADES,
    QrCunsCadCodigo.Value, True);
    //
    UnAnotacoes.DefineVariavelEmFormTDIPnl(FThisFmPnAnotacoes,
    (*TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1,*) CO_ANOTACOES_TAB_0002_SIAPTERCAD,
    QrSiapTerCadCodigo.Value, True);
    //
    if (PCDados.ActivePageIndex = 0) and (PCObjeto.ActivePageIndex = 3) then
      TFmPnAnotacoes(FThisFmPnAnotacoes).ReabreAnot(0, 0)
    else
    begin
      TFmPnAnotacoes(FThisFmPnAnotacoes).CBLinkTabs.CheckAll(cbUnchecked);
      TFmPnAnotacoes(FThisFmPnAnotacoes).QrAnot.Close;
    end;
  end;
  // FIM FmPnAnotacoes
  //
  if FThisFmDocsCab <> nil then
  begin
    UnDocsCab.DefineVariavelEmFormTDIPnl(FThisFmDocsCab,
    CO_ANOTACOES_TAB_0001_ENTIDADES,
    QrCunsCadCodigo.Value, True);
    //
    UnDocsCab.DefineVariavelEmFormTDIPnl(FThisFmDocsCab,
    CO_ANOTACOES_TAB_0002_SIAPTERCAD,
    QrSiapTerCadCodigo.Value, True);
    //
    if (PCDados.ActivePageIndex = 0) and (PCObjeto.ActivePageIndex = 3) then
      TFmDocsCab(FThisFmDocsCab).ReabreDocsCab(0, 0)
    else
    begin
      TFmDocsCab(FThisFmDocsCab).CBLinkTabs.CheckAll(cbUnchecked);
      TFmDocsCab(FThisFmDocsCab).QrDocsCab.Close;
    end;
  end;
  // FIM FmDocsCab
  if CkAgeSoDoLugar.Checked = False then
    ReopenSTCAgeEve(0);
  ReopenPMVs(0);
end;

procedure TFmCunsCad.FichadoLugar1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOsImp2, FmOsImp2, afmoSoBoss) then
  begin
    //FmOsImp2.EdOSCab.ValueVariant  := 0;
    FmOsImp2.EdCliCod.ValueVariant := QrEntidadesCodigo.Value;
    FmOsImp2.EdCliNom.Text         := QrEntidadesNOMEENTIDADE.Value;
    FmOsImp2.PageControl1.ActivePageIndex := 1;
    //
    FmOsImp2.ShowModal;
    FmOsImp2.Destroy;
  end;
end;

procedure TFmCunsCad.FormActivate(Sender: TObject);
begin
  if TFmCunsCad(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
    //
    if FSeq = 1 then
      //CabInclui1Click(Self)
    else
    if (not FLocIni) and (FCabIni <> 0)  then
    begin
      LocCod(FCabIni, FCabIni);
      if QrCunsCadCodigo.Value <> FCabIni then Geral.MensagemBox(
      'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
      FLocIni := True;
    end;
  end;
end;

procedure TFmCunsCad.SbQueryClick(Sender: TObject);
begin
  DefParams;
  MyObjects.MostraPopUpDeBotao(PMQuery, SbQuery);
end;

procedure TFmCunsCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCunsCad.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  //
  if FSeq = 1 then
    //CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrCunsCadCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmCunsCad.frxCAD_SUBCL_001_001_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_CONTRATO' then
  begin
    {if QrOSCabNumContrat.Value <> 0 then
      Value := 'Contrato: ' + Geral.FFN(QrOSCabNumContrat.Value, 3)
    else
    }
      Value := 'CONTRATO ????? ';
  end
  else
  if VarName = 'VARF_ENTIDADE' then
    Value := DBEdCodigo.Text + ' - ' + DBEdNome.Text
  else  
  if VarName = 'LogoTitRExiste' then
    Value := MyObjects.ArquivoExiste(Dmod.QrOpcoesFiliImgTitRelR.Value, Memo1)
  else
  if VarName = 'LogoTitRPath' then
    Value := Dmod.QrOpcoesFiliImgTitRelR.Value
  else
  if VarName = 'LogoTitLExiste' then
    Value := MyObjects.ArquivoExiste(Dmod.QrOpcoesFiliImgTitRelL.Value, Memo1)
  else
  if VarName = 'LogoTitLPath' then
    Value := Dmod.QrOpcoesFiliImgTitRelL.Value
  else
  if VarName = 'ImgNCTExiste' then
    Value := MyObjects.ArquivoExiste(DefineArquivo(), Memo1)
  else
  if VarName = 'ImgNCTPath' then
    Value := DefineArquivo()
  else
  if VarName = 'VARF_TITULO' then
  begin
    if QrMixRelFC.Value >= 1 then
    begin
      if QrMixRelNCT.Value >= 1 then
        Value := 'RELAT�RIO DE N�O CONFORMIDADES T�CNICAS E FOTOS COMENTADAS'
      else
        Value := 'RELAT�RIO DE FOTOS COMENTADAS';
    end
    else
      Value := 'RELAT�RIO DE N�O CONFORMIDADES T�CNICAS';
  end
  else
  if VarName = 'VARF_FC_NCT' then
  begin
    if QrCIC_ImpAplicacao.Value = 0 then
      Value := 'FOTO COMENTADA'
    else
      Value := 'N�O CONFORMIDADE T�CNICA - N.C.T.';
  end
  else
  if VarName = 'VARF_DADOS_REL' then
  begin
    if QrCIC_ImpAplicacao.Value = 0 then
      Value := QrCIC_ImpObserv.Value
    else
      Value :=
      'N.C.T.: ' + QrCIC_ImpNO_NCT.Value + sLineBreak +
      'C.A.C.: ' + QrCIC_ImpTxtCAC.Value + sLineBreak +
      'Status: ' + QrCIC_ImpNO_STATUS.Value;
      // + sLineBreak + QrCIC_ImpObserv.Value;
  end
  else
end;

procedure TFmCunsCad.QrCunsCadBeforeClose(DataSet: TDataSet);
begin
  QrCunsIts.Close;
  QrSiapTerCad.Close;
  QrEntidades.Close;
  QrSubCli.Close;
  QrPMVs.Close;
  QrContratos.Close;
  //
  BtCunsIts.Enabled    := False;
  BtSiapTerCad.Enabled := False;
  //
{  if FThisFmPnAnotacoes <> nil then
    TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1[CO_ANOTACOES_TAB_0001_ENTIDADES] := Null;
}
  UnAnotacoes.DefineVariavelEmFormTDIPnl(FThisFmPnAnotacoes,
  (*TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1,*) CO_ANOTACOES_TAB_0001_ENTIDADES,
  Null, True);
  //
  UnDocsCab.DefineVariavelEmFormTDIPnl(FThisFmDocsCab,
  CO_ANOTACOES_TAB_0001_ENTIDADES,
  Null, True);
end;

procedure TFmCunsCad.QrCunsCadBeforeOpen(DataSet: TDataSet);
begin
  QrCunsCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCunsCad.QrCunsImgCabAfterOpen(DataSet: TDataSet);
begin
  PG_NCT_CAC.ActivePageIndex := QrCunsImgCabAplicacao.Value;
end;

procedure TFmCunsCad.QrCunsImgCabAfterScroll(DataSet: TDataSet);
begin
  //ReopenCunsImgCmt(0);
  CarregaImagem();
end;

procedure TFmCunsCad.QrCunsImgCabBeforeClose(DataSet: TDataSet);
begin
  //QrCunsImgCmt.Close;
  ImgFoto.Picture := nil;
end;

procedure TFmCunsCad.QrEntiContatAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiMail(0);
  ReopenEntiTel(0);
end;

procedure TFmCunsCad.QrEntiContatBeforeClose(DataSet: TDataSet);
begin
  QrEntiMail.Close;
  QrEntiTel.Close;
end;

procedure TFmCunsCad.QrEntidadesAfterScroll(DataSet: TDataSet);
begin
  case QrEntidadesTipo.Value of
    0:
    begin
      PnShow2PJ.Visible := True;
      PnShow2PF.Visible := False;
    end;
    1:
    begin
      PnShow2PF.Visible := True;
      PnShow2PJ.Visible := False;
    end;
  end;
  ReopenEntiContat(0);
  ReopenEntiRespon(0);
  ReopenEntDefAtr(0);
  ReopenEntiCtas;
  //
  if FMySelfGer2 <> nil then
  begin
    TFmSelfGer2(FMySelfGer2).EdCliente.ValueVariant := QrEntidadesCodigo.Value;
    TFmSelfGer2(FMySelfGer2).CBCliente.KeyValue     := QrEntidadesCodigo.Value;
  end;
  if FOSPrvGer <> nil then
  begin
    TFmOSPrvGer(FOSPrvGer).EdCliente.ValueVariant := QrEntidadesCodigo.Value;
    TFmOSPrvGer(FOSPrvGer).CBCliente.KeyValue     := QrEntidadesCodigo.Value;
  end;
  if QrOSCab.State <> dsInactive then
    ReopenOSCab();
  if FDiarioGer2 <> nil then
  begin
    TFmDiarioGer2(FDiarioGer2).EdEntidade.ValueVariant := QrEntidadesCodigo.Value;
    TFmDiarioGer2(FDiarioGer2).CBEntidade.KeyValue     := QrEntidadesCodigo.Value;
  end;
  if QrContratos.State <> dsInactive then
    ReopenContratos();
  //
  if FUsrWeb then
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
      ReopenWUsers();
  end else
    QrWUsers.Close;
end;

procedure TFmCunsCad.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  QrEntiContat.Close;
  QrEntiRespon.Close;
  QrEntDefAtr.Close;
  QrEntiCtas.Close;
  QrWUsers.Close;
end;

procedure TFmCunsCad.QrEntidadesCalcFields(DataSet: TDataSet);
begin
  QrEntidadesETE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe1.Value);
  QrEntidadesETE2_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe2.Value);
  QrEntidadesETE3_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe3.Value);
  QrEntidadesEFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesEFax.Value);
  QrEntidadesECEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesECel.Value);
  QrEntidadesCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCNPJ.Value);
  QrEntidadesL_CNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesL_CNPJ.Value);
  //
  QrEntidadesPTE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe1.Value);
  QrEntidadesPTE2_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe2.Value);
  QrEntidadesPTE3_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe3.Value);
  QrEntidadesPFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPFax.Value);
  QrEntidadesPCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPCel.Value);
  QrEntidadesPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF.Value);
  QrEntidadesCPF_PAI_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF_Pai.Value);
  QrEntidadesCPF_Resp1_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF_Resp1.Value);
  //
  QrEntidadesCTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCTel.Value);
  QrEntidadesCFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCFax.Value);
  QrEntidadesCCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCCel.Value);
  //
  QrEntidadesLTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLTel.Value);
  QrEntidadesLFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLFax.Value);
  QrEntidadesLCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLCel.Value);
  //
  QrEntidadesECEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesECEP.Value));
  QrEntidadesPCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesPCEP.Value));
  QrEntidadesCCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesCCEP.Value));
  QrEntidadesLCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesLCEP.Value));
  //
  QrEntidadesENUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesERua.Value, QrEntidadesENumero.Value, False);
  QrEntidadesPNUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesPRua.Value, QrEntidadesPNumero.Value, False);
  QrEntidadesCNUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesCRua.Value, QrEntidadesCNumero.Value, False);
  QrEntidadesLNUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesLRua.Value, QrEntidadesLNumero.Value, False);
  //
  QrEntidadesIE_TXT.Value := Geral.Formata_IE(QrEntidadesIE.Value, QrEntidadesEUF.Value, '??');
  QrEntidadesIEST_TXT.Value := Geral.Formata_IE(QrEntidadesIEST.Value, QrEntidadesEUF.Value, '??');
  //
  QrEntidadesDATARG_TXT.Value := dmkPF.FDT_NULO(QrEntidadesDataRG.Value, 2);
end;

procedure TFmCunsCad.QrEntiResponCalcFields(DataSet: TDataSet);
begin
  QrEntiResponMandatoIni_TXT.Value := Geral.FDT(QrEntiResponMandatoIni.Value, 3);
  QrEntiResponMandatoFim_TXT.Value := Geral.FDT(QrEntiResponMandatoFim.Value, 3);
end;

procedure TFmCunsCad.QrEntiTelCalcFields(DataSet: TDataSet);
begin
  QrEntiTelTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntiTelTelefone.Value);
end;

procedure TFmCunsCad.QrMovAmovCadAfterScroll(DataSet: TDataSet);
begin
  ReopenAtrAMovDef(0);
end;

procedure TFmCunsCad.QrMovAmovCadBeforeClose(DataSet: TDataSet);
begin
  QrAtrAMovDef.Close;
end;

procedure TFmCunsCad.QrSiapImaCadAfterClose(DataSet: TDataSet);
begin
  VerificaHabilita();
end;

procedure TFmCunsCad.QrSiapImaCadAfterOpen(DataSet: TDataSet);
begin
  VerificaHabilita();
end;

procedure TFmCunsCad.QrSiapImaCadAfterScroll(DataSet: TDataSet);
begin
  ReopenSiapImaDep(0);
  ReopenSiapImaCdi(0);
  ReopenSiapImaCav(0);
  ReopenSiapImaRes(0);
  ReopenSiapImaCui(0);
  ReopenSiapImaAti(0);
  ReopenSiapImaCxa(0);
  ReopenAtrSICdDef(0);
  ReopenSiapImaSVG(0);
end;

procedure TFmCunsCad.QrSiapImaCadBeforeClose(DataSet: TDataSet);
begin
  QrSiapImaDep.Close;
  QrSiapImaCdi.Close;
  QrSiapImaCav.Close;
  QrSiapImaRes.Close;
  QrSiapImaCui.Close;
  QrSiapImaAti.Close;
  QrSiapImaCxa.Close;
  QrAtrSICdDef.Close;
  QrSiapImaSVG.Close;
end;

procedure TFmCunsCad.QrSiapImaCxaAfterOpen(DataSet: TDataSet);
begin
  VerificaHabilita();
end;

procedure TFmCunsCad.QrSiapImaCxaAfterScroll(DataSet: TDataSet);
begin
  ReopenAtrSICxDef(0);
end;

procedure TFmCunsCad.QrSiapImaCxaBeforeClose(DataSet: TDataSet);
begin
  QrAtrSICxDef.Close;
  VerificaHabilita();
end;

procedure TFmCunsCad.QrSiapImaDepAfterOpen(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAT_Dep, Dmod.MyDB, [
  'SELECT IF(sic.M2Constru > 0, ',
  'SUM(sid.MLarg * sid.MComp) / sic.M2Constru * 100, 0) PERC_TOT, ',
  'SUM(sid.MLarg * sid.MComp) AREA, ',
  'SUM(sid.MLArg * sid.MComp * sid.MAltu) VOLUME ',
  'FROM siapimadep sid ',
  'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo ',
  'WHERE sid.Codigo=' + Geral.FF0(QrSiapImaCadCodigo.Value),
  '']);
end;

procedure TFmCunsCad.QrSiapImaDepBeforeClose(DataSet: TDataSet);
begin
  QrAT_Dep.Close;
end;

procedure TFmCunsCad.QrSiapTerCadAfterOpen(DataSet: TDataSet);
begin
  BtSiapImacad.Enabled := QrSiapTerCad.RecordCount > 0;
end;

procedure TFmCunsCad.QrSiapTerCadAfterScroll(DataSet: TDataSet);
var
  Valor: Variant;
begin
  MeTerCadEnder.Text := MontaEndereco();
  ReopenSiapImaCad(0);
  ReopenAtrSTCdDef(0);
  ReopenSTCAgeEve(0);
  ReopenSiapterFlh(0);
  ReopenCunsImgCab(0);

{
  if FThisFmPnAnotacoes <> nil then
  begin
    if QrSiapTerCadCodigo.Value = 0 then
      Valor := Null
    else
      Valor := QrSiapTerCadCodigo.Value;
    TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1[CO_ANOTACOES_TAB_0002_SIAPTERCAD] :=
    QrSiapTerCadCodigo.Value;
  end;
}
  UnAnotacoes.DefineVariavelEmFormTDIPnl(FThisFmPnAnotacoes,
  (*TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1,*) CO_ANOTACOES_TAB_0002_SIAPTERCAD,
  QrSiapTerCadCodigo.Value, True);
  //
  UnDocsCab.DefineVariavelEmFormTDIPnl(FThisFmDocsCab,
  CO_ANOTACOES_TAB_0002_SIAPTERCAD,
  QrSiapTerCadCodigo.Value, True);
end;

procedure TFmCunsCad.QrSiapTerCadBeforeClose(DataSet: TDataSet);
begin
  MeTerCadEnder.Lines.Clear;
  //
  QrSiapImaCad.Close;
  QrAtrSTCdDef.Close;
  QrSTCAgeEve.Close;
  QrSiapTerFlh.Close;
  QrCunsImgCab.Close;
  //
  BtSiapImaCad.Enabled := False;
  //
{
  if FThisFmPnAnotacoes <> nil then
    TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1[CO_ANOTACOES_TAB_0002_SIAPTERCAD] := Null;
}
  UnAnotacoes.DefineVariavelEmFormTDIPnl(FThisFmPnAnotacoes,
  (*TFmPnAnotacoes(FThisFmPnAnotacoes).FLinkID1,*) CO_ANOTACOES_TAB_0002_SIAPTERCAD,
  Null, True);
  //
  UnDocsCab.DefineVariavelEmFormTDIPnl(FThisFmDocsCab,
  CO_ANOTACOES_TAB_0002_SIAPTERCAD,
  Null, True);
end;

procedure TFmCunsCad.QrSiapTerFlhDataSincOSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrSiapTerFlhDataSincOS.Value, 2);
end;

procedure TFmCunsCad.QrSTCAgeEveCalcFields(DataSet: TDataSet);
const
  Terceiro = 0;
  NO_ENT = '';
var
  QuestaoCod, QuestaoExe, FatoGeradr: Integer;
  NO_FATOGERADR: String;
begin
  case TAgendaQuestao(QrSTCAgeEveQuestaoTyp.Value) of
    (*0*)qagIndefinido , // nada
    (*1*)qagAvulso     : QrSTCAgeEveTEXTO.Value := QrSTCAgeEveNome.Value;
    else
    (*2*)(*qagOSBgstrl*)
    begin
      QuestaoCod    := QrSTCAgeEveQuestaoCod.Value;
      QuestaoExe    := QrSTCAgeEveQuestaoExe.Value;
      FatoGeradr    := QrSTCAgeEveFatoGeradr.Value;
      NO_FATOGERADR := QrSTCAgeEveNO_FATOGERADR.Value;
      //
      QrSTCAgeEveTEXTO.Value := AgendaGerAll.MontagemTextoCompromissoApp(
        QuestaoCod, QuestaoExe, Terceiro, NO_ENT, FatoGeradr, NO_FATOGERADR);
    end;
  end;
end;

end.

