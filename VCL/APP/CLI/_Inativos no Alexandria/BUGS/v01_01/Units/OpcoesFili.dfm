object FmOpcoesFili: TFmOpcoesFili
  Left = 368
  Top = 194
  Caption = 'FER-OPCAO-004 :: Par'#226'metros Espec'#237'ficos de Filiais'
  ClientHeight = 651
  ClientWidth = 979
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 118
    Width = 979
    Height = 533
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PnDataCab: TPanel
      Left = 0
      Top = 0
      Width = 979
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      ParentBackground = False
      TabOrder = 0
      object Label1: TLabel
        Left = 10
        Top = 5
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
      end
      object Label2: TLabel
        Left = 84
        Top = 5
        Width = 31
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Filial:'
        FocusControl = DBEdit1
      end
      object Label3: TLabel
        Left = 158
        Top = 5
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object DBEdit1: TDBEdit
        Left = 158
        Top = 25
        Width = 792
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMEFILIAL'
        DataSource = DsOpcoesFili
        TabOrder = 0
      end
      object DBEdit5: TDBEdit
        Left = 84
        Top = 25
        Width = 69
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Filial'
        DataSource = DsOpcoesFili
        TabOrder = 1
      end
      object DBEdit6: TDBEdit
        Left = 10
        Top = 25
        Width = 69
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Codigo'
        DataSource = DsOpcoesFili
        TabOrder = 2
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 454
      Width = 979
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 122
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 336
        Top = 18
        Width = 641
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 507
          Top = 0
          Width = 134
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtFilial: TBitBtn
          Tag = 408
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Filial'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtFilialClick
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 59
      Width = 979
      Height = 270
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Certificado de Garantia: '
      TabOrder = 2
      object Panel4: TPanel
        Left = 2
        Top = 18
        Width = 975
        Height = 250
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label5: TLabel
          Left = 10
          Top = 10
          Width = 50
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Licen'#231'a:'
        end
        object GroupBox2: TGroupBox
          Left = 10
          Top = 59
          Width = 936
          Height = 86
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Respons'#225'vel T'#233'cnico: '
          TabOrder = 0
          object Panel6: TPanel
            Left = 2
            Top = 18
            Width = 932
            Height = 66
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label6: TLabel
              Left = 10
              Top = 10
              Width = 99
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Nome completo:'
            end
            object DBEdit7: TDBEdit
              Left = 10
              Top = 30
              Width = 911
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'RespTecNome'
              DataSource = DsOpcoesFili
              TabOrder = 0
            end
          end
        end
        object DBEdit4: TDBEdit
          Left = 10
          Top = 30
          Width = 936
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NumLicOper'
          DataSource = DsOpcoesFili
          TabOrder = 1
        end
        object GroupBox3: TGroupBox
          Left = 10
          Top = 152
          Width = 936
          Height = 80
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Imagens: '
          TabOrder = 2
          object Panel7: TPanel
            Left = 2
            Top = 18
            Width = 932
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label12: TLabel
              Left = 10
              Top = 5
              Width = 256
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Imagem do carimbo da empresa (3,7 x 8,5):'
            end
            object DBEdit10: TDBEdit
              Left = 10
              Top = 25
              Width = 906
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'CarimboEmp'
              DataSource = DsOpcoesFili
              TabOrder = 0
            end
          end
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 329
      Width = 979
      Height = 84
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Agenda: '
      TabOrder = 3
      object Panel8: TPanel
        Left = 2
        Top = 18
        Width = 975
        Height = 64
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label17: TLabel
          Left = 15
          Top = 5
          Width = 247
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Grupo de visualiza'#231#227'o da agenda do dia:'
        end
        object DBEdit12: TDBEdit
          Left = 84
          Top = 25
          Width = 434
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NO_GRCAB_D'
          DataSource = DsOpcoesFili
          TabOrder = 0
        end
        object DBEdit13: TDBEdit
          Left = 15
          Top = 25
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'AgenDGrCab'
          DataSource = DsOpcoesFili
          TabOrder = 1
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 118
    Width = 979
    Height = 533
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PnEditCab: TPanel
      Left = 0
      Top = 0
      Width = 979
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Label7: TLabel
        Left = 10
        Top = 5
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 84
        Top = 5
        Width = 31
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Filial:'
      end
      object Label9: TLabel
        Left = 158
        Top = 5
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object DBEdit2: TDBEdit
        Left = 158
        Top = 25
        Width = 792
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMEFILIAL'
        DataSource = DsOpcoesFili
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 84
        Top = 25
        Width = 69
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Filial'
        DataSource = DsOpcoesFili
        TabOrder = 1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 10
        Top = 25
        Width = 69
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Codigo'
        DataSource = DsOpcoesFili
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utInc
        Alignment = taLeftJustify
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 456
      Width = 979
      Height = 77
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 14
        Top = 21
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 844
        Top = 18
        Width = 133
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 59
      Width = 979
      Height = 397
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Certificado de Garantia: '
        object Label13: TLabel
          Left = 10
          Top = 10
          Width = 50
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Licen'#231'a:'
        end
        object Label4: TLabel
          Left = 10
          Top = 60
          Width = 435
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Dados para situa'#231#227'o de emerg'#234'ncia: (Nome e telefone de hospital,' +
            ' etc...)'
        end
        object Label19: TLabel
          Left = 10
          Top = 109
          Width = 199
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Imagem do carimbo da empresa:'
        end
        object SbCarimboEmp: TSpeedButton
          Left = 922
          Top = 129
          Width = 26
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbCarimboEmpClick
        end
        object EdNumLicOper: TdmkEdit
          Left = 10
          Top = 30
          Width = 938
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'NumLicOper'
          UpdCampo = 'NumLicOper'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEmergencia: TdmkEdit
          Left = 10
          Top = 80
          Width = 938
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Emergencia'
          UpdCampo = 'Emergencia'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCarimboEmp: TdmkEdit
          Left = 10
          Top = 129
          Width = 908
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CarimboEmp'
          UpdCampo = 'CarimboEmp'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object GroupBox9: TGroupBox
          Left = 10
          Top = 163
          Width = 535
          Height = 85
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Respons'#225'vel T'#233'cnico: '
          TabOrder = 3
          object Panel16: TPanel
            Left = 2
            Top = 18
            Width = 531
            Height = 65
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label14: TLabel
              Left = 10
              Top = 4
              Width = 57
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Entidade:'
            end
            object SpeedButton5: TSpeedButton
              Left = 493
              Top = 23
              Width = 26
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SpeedButton5Click
            end
            object EdEntidade: TdmkEditCB
              Left = 10
              Top = 23
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'RespTec'
              UpdCampo = 'RespTec'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEntidade
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEntidade: TdmkDBLookupComboBox
              Left = 80
              Top = 23
              Width = 409
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsEntidades
              TabOrder = 1
              dmkEditCB = EdEntidade
              QryCampo = 'RespTec'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Agenda: '
        ImageIndex = 1
        object Label18: TLabel
          Left = 13
          Top = 5
          Width = 247
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Grupo de visualiza'#231#227'o da agenda do dia:'
        end
        object SbAgenDGrCab: TSpeedButton
          Left = 496
          Top = 25
          Width = 26
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbAgenDGrCabClick
        end
        object EdAgenDGrCab: TdmkEditCB
          Left = 13
          Top = 25
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'AgenDGrCab'
          UpdCampo = 'AgenDGrCab'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CbAgenDGrCab
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CbAgenDGrCab: TdmkDBLookupComboBox
          Left = 82
          Top = 25
          Width = 410
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsAgenGrCabDia
          TabOrder = 1
          dmkEditCB = EdAgenDGrCab
          QryCampo = 'AgenDGrCab'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Relat'#243'rios espec'#237'ficos: '
        ImageIndex = 2
        object Label10: TLabel
          Left = 10
          Top = 5
          Width = 171
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Imagem lado direito do t'#237'tulo:'
        end
        object Label11: TLabel
          Left = 10
          Top = 54
          Width = 192
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Imagem lado esquerdo do t'#237'tulo:'
        end
        object Label15: TLabel
          Left = 10
          Top = 103
          Width = 203
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Identifica'#231#227'o da empresa no t'#237'tulo:'
        end
        object SbTxtTitRelC: TSpeedButton
          Left = 916
          Top = 123
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbTxtTitRelCClick
        end
        object SbImgTitRelL: TSpeedButton
          Left = 916
          Top = 74
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbImgTitRelLClick
        end
        object SbImgTitRelR: TSpeedButton
          Left = 916
          Top = 25
          Width = 26
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbImgTitRelRClick
        end
        object EdImgTitRelR: TdmkEdit
          Left = 10
          Top = 25
          Width = 902
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ImgTitRelR'
          UpdCampo = 'ImgTitRelR'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdImgTitRelL: TdmkEdit
          Left = 10
          Top = 74
          Width = 902
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ImgTitRelL'
          UpdCampo = 'ImgTitRelL'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RETxtTitRelC: TdmkRichEdit
          Left = 10
          Top = 123
          Width = 902
          Height = 109
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssBoth
          TabOrder = 2
          QryCampo = 'TxtTitRelC'
          UpdCampo = 'TxtTitRelC'
          UpdType = utYes
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 979
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 920
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 654
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 477
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Par'#226'metros Espec'#237'ficos de Filiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 477
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Par'#226'metros Espec'#237'ficos de Filiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 477
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Par'#226'metros Espec'#237'ficos de Filiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 979
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel47: TPanel
      Left = 2
      Top = 18
      Width = 975
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsOpcoesFili: TDataSource
    DataSet = QrOpcoesFili
    Left = 260
    Top = 36
  end
  object QrOpcoesFili: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrOpcoesFiliBeforeOpen
    AfterOpen = QrOpcoesFiliAfterOpen
    SQL.Strings = (
      'SELECT agc.Nome NO_GRCAB_D,'
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0,ent.CNPJ,ent.CPF) CNPJCPF_FILIAL,'
      'ent.Filial, ent.Codigo Entidade, ent.Nome RespTecNome,'
      'fil.*'
      'FROM entidades ent'
      'LEFT JOIN opcoesfili fil ON fil.Codigo=ent.Codigo'
      'LEFT JOIN entidades res ON res.Codigo = fil.RespTec'
      'LEFT JOIN agengrcab agc ON agc.Codigo=fil.AgenDGrCab'
      'WHERE ent.Codigo < -10'
      '')
    Left = 232
    Top = 36
    object QrOpcoesFiliNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
    object QrOpcoesFiliFilial: TIntegerField
      FieldName = 'Filial'
      Required = True
    end
    object QrOpcoesFiliEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrOpcoesFiliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOpcoesFiliCNPJCPF_FILIAL: TWideStringField
      FieldName = 'CNPJCPF_FILIAL'
      Size = 18
    end
    object QrOpcoesFiliNumLicOper: TWideStringField
      FieldName = 'NumLicOper'
      Size = 255
    end
    object QrOpcoesFiliEmergencia: TWideStringField
      FieldName = 'Emergencia'
      Size = 255
    end
    object QrOpcoesFiliCarimboEmp: TWideStringField
      FieldName = 'CarimboEmp'
      Size = 255
    end
    object QrOpcoesFiliAgenDGrCab: TIntegerField
      FieldName = 'AgenDGrCab'
    end
    object QrOpcoesFiliNO_GRCAB_D: TWideStringField
      FieldName = 'NO_GRCAB_D'
      Size = 50
    end
    object QrOpcoesFiliRespTec: TIntegerField
      FieldName = 'RespTec'
    end
    object QrOpcoesFiliRespTecNome: TWideStringField
      FieldName = 'RespTecNome'
      Size = 100
    end
    object QrOpcoesFiliImgTitRelR: TWideStringField
      FieldName = 'ImgTitRelR'
      Origin = 'opcoesgerl.ImgTitRelR'
      Size = 255
    end
    object QrOpcoesFiliImgTitRelL: TWideStringField
      FieldName = 'ImgTitRelL'
      Origin = 'opcoesgerl.ImgTitRelL'
      Size = 255
    end
    object QrOpcoesFiliTxtTitRelC: TWideMemoField
      FieldName = 'TxtTitRelC'
      Origin = 'opcoesgerl.TxtTitRelC'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtSaida
    CanUpd01 = Panel2
    Left = 288
    Top = 36
  end
  object PMFilial: TPopupMenu
    OnPopup = PMFilialPopup
    Left = 592
    Top = 408
    object Adicionafilial1: TMenuItem
      Caption = 'Ad&iciona filial(is)'
      OnClick = Adicionafilial1Click
    end
    object Alterafilialatual1: TMenuItem
      Caption = '&Altera filial atual'
      OnClick = Alterafilialatual1Click
    end
    object Retirafilialatual1: TMenuItem
      Caption = '&Retira filial atual'
      Enabled = False
    end
  end
  object QrAgenGrCabDia: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM agengrcab'
      'ORDER BY Nome')
    Left = 316
    Top = 36
    object QrAgenGrCabDiaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgenGrCabDiaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrAgenGrCabDiaColLarg: TIntegerField
      FieldName = 'ColLarg'
    end
    object QrAgenGrCabDiaInterMinut: TIntegerField
      FieldName = 'InterMinut'
    end
    object QrAgenGrCabDiaCorMul: TIntegerField
      FieldName = 'CorMul'
    end
  end
  object DsAgenGrCabDia: TDataSource
    DataSet = QrAgenGrCabDia
    Left = 344
    Top = 36
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidades'
      'WHERE Tipo = 1'
      'AND Ativo = 1'
      'AND Codigo > 0'
      'ORDER BY Nome')
    Left = 716
    Top = 388
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 744
    Top = 388
  end
end
