object FmPipPesq: TFmPipPesq
  Left = 339
  Top = 185
  Caption = 'PIP-GEREN-004 :: Pesquisa de PMV'
  ClientHeight = 585
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 214
        Height = 32
        Caption = 'Pesquisa de PMV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 214
        Height = 32
        Caption = 'Pesquisa de PMV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 214
        Height = 32
        Caption = 'Pesquisa de PMV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 423
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 423
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 423
        Align = alClient
        TabOrder = 0
        object LaTotal: TLabel
          Left = 2
          Top = 408
          Width = 780
          Height = 13
          Align = alBottom
          ExplicitWidth = 3
        end
        object PnGeral: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 82
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label9: TLabel
            Left = 16
            Top = 0
            Width = 96
            Height = 13
            Caption = 'Parte da refer'#234'ncia: '
            Color = clBtnFace
            ParentColor = False
          end
          object Label1: TLabel
            Left = 128
            Top = 0
            Width = 102
            Height = 13
            Caption = 'Periodo de aquisi'#231#227'o:'
          end
          object Label2: TLabel
            Left = 384
            Top = 0
            Width = 65
            Height = 13
            Caption = 'Equipamento:'
          end
          object Label5: TLabel
            Left = 16
            Top = 40
            Width = 177
            Height = 13
            Caption = 'Lista de perguntas no monitoramento:'
          end
          object Label6: TLabel
            Left = 428
            Top = 40
            Width = 188
            Height = 13
            Caption = 'Depend'#234'ncia onde o PMV se encontra:'
          end
          object Label7: TLabel
            Left = 244
            Top = 20
            Width = 15
            Height = 13
            Caption = 'at'#233
          end
          object EdNome: TdmkEdit
            Left = 16
            Top = 16
            Width = 109
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnKeyDown = EdNomeKeyDown
          end
          object TPDtaAquisIni: TdmkEditDateTimePicker
            Left = 128
            Top = 16
            Width = 112
            Height = 21
            Date = -3652.857134502316000000
            Time = -3652.857134502316000000
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdEquipamento: TdmkEditCB
            Left = 384
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EquipAplic'
            UpdCampo = 'EquipAplic'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEquipamento
            IgnoraDBLookupComboBox = False
          end
          object CBEquipamento: TdmkDBLookupComboBox
            Left = 440
            Top = 16
            Width = 333
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsEquipAplic
            TabOrder = 3
            dmkEditCB = EdEquipamento
            QryCampo = 'EquipAplic'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPrgLstCab: TdmkEditCB
            Left = 16
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EquipAplic'
            UpdCampo = 'EquipAplic'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPrgLstCab
            IgnoraDBLookupComboBox = False
          end
          object CBPrgLstCab: TdmkDBLookupComboBox
            Left = 72
            Top = 56
            Width = 353
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPrgLstCab
            TabOrder = 5
            dmkEditCB = EdPrgLstCab
            QryCampo = 'EquipAplic'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdDependenci: TdmkEditCB
            Left = 428
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Dependenci'
            UpdCampo = 'Dependenci'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBDependenci
            IgnoraDBLookupComboBox = False
          end
          object CBDependenci: TdmkDBLookupComboBox
            Left = 484
            Top = 56
            Width = 289
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_DEPENDENCI'
            ListSource = DsSiapImaDep
            TabOrder = 7
            dmkEditCB = EdDependenci
            QryCampo = 'Dependenci'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object TPDtaAquisFim: TdmkEditDateTimePicker
            Left = 268
            Top = 16
            Width = 112
            Height = 21
            Date = -3652.857134502316000000
            Time = -3652.857134502316000000
            TabOrder = 8
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
        object PnDesativa: TPanel
          Left = 2
          Top = 97
          Width = 780
          Height = 84
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label3: TLabel
            Left = 520
            Top = 0
            Width = 117
            Height = 13
            Caption = 'Per'#237'odo de desativa'#231#227'o:'
          end
          object Label4: TLabel
            Left = 16
            Top = 0
            Width = 111
            Height = 13
            Caption = 'Motivo da desativa'#231#227'o:'
          end
          object Label11: TLabel
            Left = 16
            Top = 44
            Width = 105
            Height = 13
            Caption = 'Motivo da inutiliza'#231#227'o:'
          end
          object Label10: TLabel
            Left = 520
            Top = 44
            Width = 111
            Height = 13
            Caption = 'Per'#237'odo de inutiliza'#231#227'o:'
          end
          object Label8: TLabel
            Left = 636
            Top = 64
            Width = 15
            Height = 13
            Caption = 'at'#233
          end
          object Label12: TLabel
            Left = 636
            Top = 20
            Width = 15
            Height = 13
            Caption = 'at'#233
          end
          object CBMotDesativ: TdmkDBLookupComboBox
            Left = 72
            Top = 16
            Width = 441
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsMotDesativ
            TabOrder = 1
            dmkEditCB = EdMotDesativ
            QryCampo = 'EquipAplic'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object TPDtaDesativIni: TdmkEditDateTimePicker
            Left = 520
            Top = 16
            Width = 112
            Height = 21
            Date = -3652.857134502316000000
            Time = -3652.857134502316000000
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdMotDesativ: TdmkEditCB
            Left = 16
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EquipAplic'
            UpdCampo = 'EquipAplic'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBMotDesativ
            IgnoraDBLookupComboBox = False
          end
          object EdMotInutili: TdmkEditCB
            Left = 16
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EquipAplic'
            UpdCampo = 'EquipAplic'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBMotInutili
            IgnoraDBLookupComboBox = False
          end
          object CBMotInutili: TdmkDBLookupComboBox
            Left = 72
            Top = 60
            Width = 441
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsMotDesativ
            TabOrder = 4
            dmkEditCB = EdMotInutili
            QryCampo = 'EquipAplic'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object TPDtaInutiliIni: TdmkEditDateTimePicker
            Left = 520
            Top = 60
            Width = 112
            Height = 21
            Date = -3652.857134502316000000
            Time = -3652.857134502316000000
            TabOrder = 5
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object TPDtaDesativFim: TdmkEditDateTimePicker
            Left = 660
            Top = 16
            Width = 112
            Height = 21
            Date = -3652.857134502316000000
            Time = -3652.857134502316000000
            TabOrder = 6
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object TPDtaInutiliFim: TdmkEditDateTimePicker
            Left = 660
            Top = 60
            Width = 112
            Height = 21
            Date = -3652.857134502316000000
            Time = -3652.857134502316000000
            TabOrder = 7
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 225
          Width = 780
          Height = 183
          Align = alClient
          DataSource = DsPipCad
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 2
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = dmkDBGridZTO1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID PMV'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Equipamento'
              Title.Caption = 'Equip.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EQUI'
              Title.Caption = 'Nome do equipamento'
              Width = 208
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_LISTA'
              Title.Caption = 'Lista de perguntas'
              Width = 208
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENTIDADE'
              Title.Caption = '[Sub]Cliente'
              Width = 208
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENTCONTRAT'
              Title.Caption = 'Contratante'
              Width = 208
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OSMonCab'
              Title.Caption = 'OS coloc.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Dependenci'
              Title.Caption = 'Depend'#234'ncia'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_DEPENDENCIA'
              Title.Caption = 'Nome depend'#234'ncia'
              Width = 208
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaAquis_TXT'
              Title.Caption = 'Dt aquis.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MOTDESAT'
              Title.Caption = 'Mot. desativ.'
              Width = 208
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaDesativ_TXT'
              Title.Caption = 'Dt desativ.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MOTINUTI'
              Title.Caption = 'Mot. inutiliz.'
              Width = 208
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaInutili_TXT'
              Title.Caption = 'Dt inutiliz.'
              Width = 56
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 181
          Width = 780
          Height = 44
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object Label13: TLabel
            Left = 16
            Top = 4
            Width = 60
            Height = 13
            Caption = '[Sub]Cliente:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label14: TLabel
            Left = 396
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Contratante:'
            Color = clBtnFace
            ParentColor = False
          end
          object EdEntidade: TdmkEditCB
            Left = 16
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Entidade'
            UpdCampo = 'Entidade'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEntidade
            IgnoraDBLookupComboBox = False
          end
          object CBEntidade: TdmkDBLookupComboBox
            Left = 72
            Top = 20
            Width = 319
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsEntContrat
            TabOrder = 1
            dmkEditCB = EdEntidade
            QryCampo = 'Entidade'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEntContrat: TdmkEditCB
            Left = 396
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Entidade'
            UpdCampo = 'Entidade'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEntContrat
            IgnoraDBLookupComboBox = False
          end
          object CBEntContrat: TdmkDBLookupComboBox
            Left = 452
            Top = 20
            Width = 319
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsEntContrat
            TabOrder = 3
            dmkEditCB = EdEntContrat
            QryCampo = 'Entidade'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 471
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 515
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 22
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEquipAplic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM equiaplic'
      'ORDER BY Nome')
    Left = 348
    Top = 352
    object QrEquipAplicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEquipAplicNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsEquipAplic: TDataSource
    DataSet = QrEquipAplic
    Left = 348
    Top = 400
  end
  object QrMotDesativ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM motdesativ'
      'ORDER BY Nome'
      '')
    Left = 96
    Top = 352
    object QrMotDesativCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMotDesativNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsMotDesativ: TDataSource
    DataSet = QrMotDesativ
    Left = 96
    Top = 404
  end
  object QrPrgLstCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM prglstcab '
      'ORDER BY Nome')
    Left = 16
    Top = 352
    object QrPrgLstCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgLstCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPrgLstCab: TDataSource
    DataSet = QrPrgLstCab
    Left = 16
    Top = 400
  end
  object DsSiapImaDep: TDataSource
    DataSet = QrSiapImaDep
    Left = 260
    Top = 400
  end
  object QrSiapImaDep: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sid.Controle, dep.Nome NO_DEPENDENCI'
      'FROM siapimadep sid'
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci'
      'WHERE sid.Codigo=:P0'
      'ORDER BY NO_DEPENDENCI')
    Left = 260
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapImaDepNO_DEPENDENCI: TWideStringField
      FieldName = 'NO_DEPENDENCI'
      Size = 60
    end
    object QrSiapImaDepControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrMotInutili: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM motdesativ'
      'ORDER BY Nome'
      '')
    Left = 176
    Top = 352
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsMotInutili: TDataSource
    DataSet = QrMotInutili
    Left = 176
    Top = 400
  end
  object QrPipCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPipCadBeforeClose
    AfterScroll = QrPipCadAfterScroll
    SQL.Strings = (
      'SELECT mot.Nome NO_MOTDESAT, gg1.Nome NO_EQUI,'
      'plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA,'
      'IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT,'
      'IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT,'
      'cad.*'
      'FROM pipcad cad'
      'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ'
      'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab'
      '/**/'
      'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci'
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci')
    Left = 424
    Top = 352
    object QrPipCadNO_MOTDESAT: TWideStringField
      FieldName = 'NO_MOTDESAT'
      Size = 60
    end
    object QrPipCadNO_EQUI: TWideStringField
      FieldName = 'NO_EQUI'
      Size = 120
    end
    object QrPipCadNO_LISTA: TWideStringField
      FieldName = 'NO_LISTA'
      Size = 100
    end
    object QrPipCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPipCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPipCadEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrPipCadOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrPipCadMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrPipCadDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrPipCadDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrPipCadPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrPipCadDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrPipCadDtaAquis_TXT: TWideStringField
      FieldName = 'DtaAquis_TXT'
      Size = 10
    end
    object QrPipCadDtaDesativ_TXT: TWideStringField
      FieldName = 'DtaDesativ_TXT'
      Size = 10
    end
    object QrPipCadNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrPipCadDtaInutili_TXT: TWideStringField
      FieldName = 'DtaInutili_TXT'
      Size = 10
    end
    object QrPipCadMotInutili: TIntegerField
      FieldName = 'MotInutili'
    end
    object QrPipCadNO_MOTINUTI: TWideStringField
      FieldName = 'NO_MOTINUTI'
      Size = 60
    end
    object QrPipCadNO_ENTIDADE: TWideStringField
      FieldName = 'NO_ENTIDADE'
      Size = 100
    end
    object QrPipCadNO_ENTCONTRAT: TWideStringField
      FieldName = 'NO_ENTCONTRAT'
      Size = 100
    end
  end
  object DsPipCad: TDataSource
    DataSet = QrPipCad
    Left = 424
    Top = 400
  end
  object QrEntContrat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 568
    Top = 352
    object QrEntContratCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntContratCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
    end
    object QrEntContratCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntContratNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 496
    Top = 352
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrClientesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 496
    Top = 400
  end
  object DsEntContrat: TDataSource
    DataSet = QrEntContrat
    Left = 568
    Top = 400
  end
end
