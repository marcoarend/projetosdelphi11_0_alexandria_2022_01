object FmOSOriPsq: TFmOSOriPsq
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-029 :: Pesquisa de OSs M'#227'es'
  ClientHeight = 415
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 640
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 592
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 544
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 293
        Height = 32
        Caption = ' Pesquisa de OSs M'#227'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 293
        Height = 32
        Caption = ' Pesquisa de OSs M'#227'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 293
        Height = 32
        Caption = ' Pesquisa de OSs M'#227'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 640
    Height = 253
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 640
      Height = 253
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 640
        Height = 253
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 812
        object DBGOSCab: TDBGrid
          Left = 2
          Top = 15
          Width = 199
          Height = 236
          Align = alLeft
          DataSource = DsOSCab
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGOSCabDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Grupo'
              Title.Caption = 'Proforma'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Localizador'
              Width = 80
              Visible = True
            end>
        end
        object DBGOSSrv: TDBGrid
          Left = 201
          Top = 15
          Width = 437
          Height = 236
          Align = alClient
          DataSource = DsOSSrv
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGOSSrvDrawColumnCell
          OnDblClick = DBGOSSrvDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Autorizado'
              Title.Caption = 'A'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_DesServico'
              Title.Caption = 'Servi'#231'o'
              Width = 257
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValTota'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TUDOFEITO'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 301
    Width = 640
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 345
    Width = 640
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 494
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 492
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOSCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOSCabBeforeClose
    AfterScroll = QrOSCabAfterScroll
    Left = 200
    Top = 140
    object QrOSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabGrupo: TIntegerField
      FieldName = 'Grupo'
    end
  end
  object DsOSCab: TDataSource
    DataSet = QrOSCab
    Left = 200
    Top = 188
  end
  object QrOSSrv: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOSSrvCalcFields
    SQL.Strings = (
      'SELECT des.Nome NO_DesServico, '
      'des.Sigla NO_SIGLA, srv.* '
      'FROM ossrv srv '
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico '
      'WHERE srv.Codigo=:P0')
    Left = 256
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSSrvNO_DesServico: TWideStringField
      FieldName = 'NO_DesServico'
      Size = 60
    end
    object QrOSSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSSrvDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrOSSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSSrvGarantiaDd: TIntegerField
      FieldName = 'GarantiaDd'
      DisplayFormat = '00'
    end
    object QrOSSrvHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
      DisplayFormat = '00'
    end
    object QrOSSrvHrExecutar: TFloatField
      FieldName = 'HrExecutar'
      DisplayFormat = '00'
    end
    object QrOSSrvValCalc: TFloatField
      FieldName = 'ValCalc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvValInfo: TFloatField
      FieldName = 'ValInfo'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvValDesc: TFloatField
      FieldName = 'ValDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvValTota: TFloatField
      FieldName = 'ValTota'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOSSrvAutorizado: TSmallintField
      FieldName = 'Autorizado'
      MaxValue = 1
    end
    object QrOSSrvVAL_CALCeINFO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_CALCeINFO'
      Calculated = True
    end
    object QrOSSrvAUTORIZADO_BOOL: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'AUTORIZADO_BOOL'
      Calculated = True
    end
    object QrOSSrvNO_SIGLA: TWideStringField
      FieldName = 'NO_SIGLA'
      Size = 10
    end
    object QrOSSrvDetalhes: TWideMemoField
      FieldName = 'Detalhes'
      BlobType = ftWideMemo
    end
    object QrOSSrvTudoFeitoA: TSmallintField
      FieldName = 'TudoFeitoA'
      MaxValue = 1
    end
    object QrOSSrvTudoFeitoM: TSmallintField
      FieldName = 'TudoFeitoM'
      MaxValue = 1
    end
    object QrOSSrvTUDOFEITO: TFloatField
      FieldName = 'TUDOFEITO'
    end
    object QrOSSrvMoniDdTotl: TIntegerField
      FieldName = 'MoniDdTotl'
    end
    object QrOSSrvMoniDdIntv: TIntegerField
      FieldName = 'MoniDdIntv'
    end
  end
  object DsOSSrv: TDataSource
    DataSet = QrOSSrv
    Left = 256
    Top = 188
  end
end
