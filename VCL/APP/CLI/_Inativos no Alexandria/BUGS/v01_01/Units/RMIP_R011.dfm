object FmRMIP_R011: TFmRMIP_R011
  Left = 0
  Top = 0
  Caption = 'FmRMIP_R011'
  ClientHeight = 391
  ClientWidth = 830
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 120
  TextHeight = 17
  object frxReport011A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41383.503409583300000000
    ReportOptions.LastChange = 41383.503409583300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Picture1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <ImgNCTExiste> = True then'
      '    Picture1.LoadFromFile(<ImgNCTPath>);  '
      'end;'
      ''
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      
        '  MeSloganFooter1.Text := <SloganFooter>;                       ' +
        '            '
      '  //'
      '  if <LogoTitRExiste> = True then'
      '  begin              '
      '    PictureTitR1.LoadFromFile(<LogoTitRPath>);'
      '    //PictureTitR2.LoadFromFile(<LogoTitRPath>);'
      '  end;                  '
      '  //'
      '  if <LogoTitLExiste> = True then'
      '  begin              '
      '    PictureTitL1.LoadFromFile(<LogoTitLPath>);'
      '   // PictureTitL2.LoadFromFile(<LogoTitLPath>);'
      '  end;'
      'end.')
    OnGetValue = frxReport011AGetValue
    Left = 52
    Top = 8
    Datasets = <
      item
        DataSet = frxDs011CIC_Imp
        DataSetName = 'frxDs011CIC_Imp'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsOpcoesBugs
        DataSetName = 'frxDsOpcoesBugs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 230.551330000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 151.181200000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Line_PH_01: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 132.283550000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 132.283550000000000000
          Width = 132.283486540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134200000000000000
          Top = 132.283550000000000000
          Width = 132.283486540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Top = 181.417440000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ENTIDADE]')
          ParentFont = False
        end
        object PictureTitR1: TfrxPictureView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 113.385826770000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Stretched = False
          HightQuality = True
          Transparent = True
          TransparentColor = clWhite
        end
        object PictureTitL1: TfrxPictureView
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 113.385826770000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Stretched = False
          HightQuality = True
          Transparent = True
          TransparentColor = clWhite
        end
        object Rich1: TfrxRichView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 113.385826770000000000
          DataField = 'TxtTitRelC'
          DataSet = Dmod.frxDsOpcoesFili
          DataSetName = 'frxDsOpcoesBugs'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C205461686F6D613B7D7D0D0A7B5C2A5C67656E657261746F7220
            52696368656432302031302E302E31363239397D5C766965776B696E64345C75
            6331200D0A5C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
        object Memo2: TfrxMemoView
          Top = 154.960730000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 68.031530240000000000
        Top = 929.764380000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Top = 52.913420000000200000
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Top = 52.913420000000200000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSloganFooter1: TfrxMemoView
          Top = 30.236240000000180000
          Width = 676.535870000000000000
          Height = 22.677170240000000000
          OnBeforePrint = 'MeSloganFooter1OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FALTA_SETAR_SLOGAN_FOOTER]!')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 7.559060000000045000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'Legenda: N.C.T. : N'#227'o Conformidade T'#233'cnica - C.A.C. : Comunicado' +
              ' de A'#231#227'o Corretiva.')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 559.370415590000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        DataSet = frxDs011CIC_Imp
        DataSetName = 'frxDs011CIC_Imp'
        RowCount = 0
        Stretched = True
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_FC_NCT] N'#186' [frxDs011CIC_Imp."Ordem"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Foto: [frxDs011CIC_Imp."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 37.795300000000000000
          Top = 37.795300000000000000
          Width = 604.724800000000000000
          Height = 340.157700000000000000
          OnBeforePrint = 'Picture1OnBeforePrint'
          Center = True
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          Top = 385.512060000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente: [VARF_ENTIDADE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Top = 423.307360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              'Local: [frxDs011CIC_Imp."NO_LOCAL"] : [frxDs011CIC_Imp."NO_DEPEN' +
              'DENCIA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Top = 442.205010000000000000
          Width = 680.315400000000000000
          Height = 22.677155590000000000
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_DADOS_REL]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 404.409710000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Lugar: [frxDs011CIC_Imp."NO_STC"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object Qr011CIC_Imp: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT dep.Nome NO_DEPENDENCIA, cis.Nome NO_STATUS,  '
      'nct.Nome NO_NCT, nct.TxtCAC, sic.SCompl2 NO_LOCAL, cic.*  '
      'FROM _cuns_img_cab cic  '
      'LEFT JOIN bugstrol.cunsimgsit cis ON cis.Codigo=cic.Status  '
      
        'LEFT JOIN bugstrol.siapimadep sid ON sid.Controle=cic.Dependenci' +
        'a '
      'LEFT JOIN bugstrol.dependenci dep ON dep.Codigo=sid.Dependenci '
      'LEFT JOIN bugstrol.siapimacad sic ON sic.Codigo=sid.Codigo'
      'LEFT JOIN bugstrol.cunsimgnct nct ON nct.Codigo=cic.CodiNCT '
      'WHERE cic.Codigo>0  ')
    Left = 52
    Top = 56
    object Qr011CIC_ImpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr011CIC_ImpControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr011CIC_ImpNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object Qr011CIC_ImpStatus: TIntegerField
      FieldName = 'Status'
    end
    object Qr011CIC_ImpCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
    object Qr011CIC_ImpNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object Qr011CIC_ImpDependencia: TIntegerField
      FieldName = 'Dependencia'
    end
    object Qr011CIC_ImpNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object Qr011CIC_ImpCodiNCT: TIntegerField
      FieldName = 'CodiNCT'
    end
    object Qr011CIC_ImpObserv: TWideMemoField
      FieldName = 'Observ'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object Qr011CIC_ImpAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object Qr011CIC_ImpNO_NCT: TWideStringField
      FieldName = 'NO_NCT'
      Size = 255
    end
    object Qr011CIC_ImpTxtCAC: TWideStringField
      FieldName = 'TxtCAC'
      Size = 255
    end
    object Qr011CIC_ImpNO_LOCAL: TWideStringField
      FieldName = 'NO_LOCAL'
      Size = 100
    end
    object Qr011CIC_ImpOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object Qr011CIC_ImpNO_STC: TWideStringField
      FieldName = 'NO_STC'
      Size = 100
    end
  end
  object frxDs011CIC_Imp: TfrxDBDataset
    UserName = 'frxDs011CIC_Imp'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Nome=Nome'
      'Status=Status'
      'Caminho=Caminho'
      'NO_STATUS=NO_STATUS'
      'Dependencia=Dependencia'
      'NO_DEPENDENCIA=NO_DEPENDENCIA'
      'CodiNCT=CodiNCT'
      'Observ=Observ'
      'Aplicacao=Aplicacao'
      'NO_NCT=NO_NCT'
      'TxtCAC=TxtCAC'
      'NO_LOCAL=NO_LOCAL'
      'Ordem=Ordem'
      'NO_STC=NO_STC')
    DataSet = Qr011CIC_Imp
    BCDToCurrency = False
    Left = 52
    Top = 108
  end
  object Qr011MixRel: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT SUM(IF(Aplicacao=0,1,0)) FC,'
      'SUM(IF(Aplicacao=1,1,0)) NCT'
      'FROM _cuns_img_cab')
    Left = 52
    Top = 156
    object Qr011MixRelFC: TFloatField
      FieldName = 'FC'
    end
    object Qr011MixRelNCT: TFloatField
      FieldName = 'NCT'
    end
  end
end
