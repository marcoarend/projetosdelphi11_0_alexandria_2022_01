unit CorrigeCidadeEntidade;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables;

type
  TFmCorrigeCidadeEntidade = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Grade1: TStringGrid;
    Panel5: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    LaAviso: TLabel;
    EdCaminho: TdmkEdit;
    PB1: TProgressBar;
    mySQLQuery1: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    function  LocalizaCodigoStringGrid(Codigo: Integer; var ECodMuniSG,
              PCodMuniSG: Integer): Integer;
  public
    { Public declarations }
  end;

  var
  FmCorrigeCidadeEntidade: TFmCorrigeCidadeEntidade;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

function TFmCorrigeCidadeEntidade.LocalizaCodigoStringGrid(Codigo: Integer;
  var ECodMuniSG, PCodMuniSG: Integer): Integer;
var
  I, SGCod: Integer;
begin
  Result := 0;
  //
  for I := 0 to Grade1.RowCount - 1 do
  begin
    SGCod := Geral.IMV(Geral.SoNumero_TT(Grade1.Cells[1, I]));
    //
    if SGCod = Codigo then
    begin
      ECodMuniSG := Geral.IMV(Geral.SoNumero_TT(Grade1.Cells[2, I]));
      PCodMuniSG := Geral.IMV(Geral.SoNumero_TT(Grade1.Cells[3, I]));
      Result     := SGCod;
      break;
    end;
  end;
end;

procedure TFmCorrigeCidadeEntidade.BtOKClick(Sender: TObject);
var
  Query, QueryUpd: TMySQLQuery;
  Codigo, CodigoSG, ECodMuniSG, PCodMuniSG: Integer;
begin
  (*
  Modo de usar:

  * Restaurar um backup que tenha as cidades
  * Rodar a seguinte SQL

SELECT
Codigo, ECodMunici, PCodMunici,
IF(Tipo=0, ECodMunici, PCodMunici) Cidade
FROM entidades
//WHERE IF(Tipo=0, ECodMunici, PCodMunici) = 0

  *Exportar o resultado para o Excel
  *Carregar neste form

  *)
  if MyObjects.Xls_To_StringGrid(Grade1, EdCaminho.Text, PB1, LaAviso1, LaAviso2) then
  begin
    Screen.Cursor := crHourGlass;
    //
    Query    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    QueryUpd := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
        'SELECT ',
        'Codigo, ECodMunici, PCodMunici, ',
        'IF(Tipo=0, ECodMunici, PCodMunici) Cidade ',
        'FROM entidades ',
        'WHERE IF(Tipo=0, ECodMunici, PCodMunici) = 0 ',
        '']);
      if Query.RecordCount > 0 then
      begin
        PB1.Position := 0;
        PB1.Max      := Query.RecordCount;
        //
        while not Query.EOF do
        begin
          Codigo   := Query.FieldByName('Codigo').AsInteger;
          CodigoSG := LocalizaCodigoStringGrid(Codigo, ECodMuniSG, PCodMuniSG);
          //
          if CodigoSG <>  0 then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Dmod.MyDB, [
              'UPDATE entidades ',
              'SET ECodMunici="' + Geral.FF0(ECodMuniSG) + '", PCodMunici="' +
              Geral.FF0(PCodMuniSG) + '" ',
              'WHERE Codigo="' + Geral.FF0(CodigoSG) + '"',
              '']);
          end;
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          //
          Query.Next;
        end;
      end;
    finally
      PB1.Position := 0;
      QueryUpd.Free;
      Query.Free;
      //
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmCorrigeCidadeEntidade.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCorrigeCidadeEntidade.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCorrigeCidadeEntidade.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCorrigeCidadeEntidade.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCorrigeCidadeEntidade.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := '';
  Arquivo := '';
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, 'Selecione o arquivo',
    '', [], Arquivo)
  then
    EdCaminho.Text := Arquivo;
end;

end.
