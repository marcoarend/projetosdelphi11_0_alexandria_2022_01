unit CunsCadSVG2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, XMLIntf, dmkGeral, UnDmkEnums,
  RSSVGCtrls, RSGdiPlusCtrls, Data.DB, mySQLDbTables, RSSVG, RSSVGTypes,
  RSGraphics, RSGdiPlusGraphicsTypes, Vcl.Grids, Vcl.DBGrids, Vcl.Menus,
  Vcl.Buttons;

type
  THackRSSVGPanel = class(TRSSVGPanel);
  TPMV = record
   ID: Integer;
   Doc: TRSSVGDocument;
   Img: TRSSVGPanel;
   Codigo: Integer;
   Nome: String;
  end;
  TFmCunsCadSVG2 = class(TForm)
    RSSVGDocument1: TRSSVGDocument;
    Splitter1: TSplitter;
    QrSiapImaSVG: TmySQLQuery;
    QrSiapImaSVGCodigo: TIntegerField;
    QrSiapImaSVGControle: TIntegerField;
    QrSiapImaSVGNome: TWideStringField;
    QrSiapImaSVGNoArq: TWideStringField;
    QrSiapImaSVGArquivo: TWideMemoField;
    QrSiapImaSVGLk: TIntegerField;
    QrSiapImaSVGDataCad: TDateField;
    QrSiapImaSVGDataAlt: TDateField;
    QrSiapImaSVGUserCad: TIntegerField;
    QrSiapImaSVGUserAlt: TIntegerField;
    QrSiapImaSVGAlterWeb: TSmallintField;
    QrSiapImaSVGAtivo: TSmallintField;
    QrPMVsSim: TmySQLQuery;
    DsPMVsSim: TDataSource;
    QrPMVsSimNO_MOTDESAT: TWideStringField;
    QrPMVsSimNO_EQUI: TWideStringField;
    QrPMVsSimNivel1: TIntegerField;
    QrPMVsSimNO_LISTA: TWideStringField;
    QrPMVsSimNO_DEPENDENCIA: TWideStringField;
    QrPMVsSimDtaAquis_TXT: TWideStringField;
    QrPMVsSimDtaDesativ_TXT: TWideStringField;
    QrPMVsSimCodigo: TIntegerField;
    QrPMVsSimNome: TWideStringField;
    QrPMVsSimEquipamento: TIntegerField;
    QrPMVsSimOSMonCab: TIntegerField;
    QrPMVsSimMotDesativ: TIntegerField;
    QrPMVsSimDtaAquis: TDateField;
    QrPMVsSimDtaDesativ: TDateField;
    QrPMVsSimLk: TIntegerField;
    QrPMVsSimDataCad: TDateField;
    QrPMVsSimDataAlt: TDateField;
    QrPMVsSimUserCad: TIntegerField;
    QrPMVsSimUserAlt: TIntegerField;
    QrPMVsSimAlterWeb: TSmallintField;
    QrPMVsSimAtivo: TSmallintField;
    QrPMVsSimPrgLstCab: TIntegerField;
    QrPMVsSimDependenci: TIntegerField;
    QrPMVsSimIntrvMonDD: TIntegerField;
    QrPMVsSimOrdem: TIntegerField;
    QrPMVsSimReordem: TIntegerField;
    QrPMVsSimMotInutili: TIntegerField;
    QrPMVsSimDtaInutili: TDateField;
    QrPMVsSimSvgPosX: TFloatField;
    QrPMVsSimSvgPosY: TFloatField;
    QrPMVsSimSvgZumF: TFloatField;
    QrPMVsSimSiapImaSVG: TIntegerField;
    QrPMVsNao: TmySQLQuery;
    DsPMVsNao: TDataSource;
    QrPMVsNaoNO_MOTDESAT: TWideStringField;
    QrPMVsNaoNO_EQUI: TWideStringField;
    QrPMVsNaoNivel1: TIntegerField;
    QrPMVsNaoNO_LISTA: TWideStringField;
    QrPMVsNaoNO_DEPENDENCIA: TWideStringField;
    QrPMVsNaoDtaAquis_TXT: TWideStringField;
    QrPMVsNaoDtaDesativ_TXT: TWideStringField;
    QrPMVsNaoCodigo: TIntegerField;
    QrPMVsNaoNome: TWideStringField;
    QrPMVsNaoEquipamento: TIntegerField;
    QrPMVsNaoOSMonCab: TIntegerField;
    QrPMVsNaoMotDesativ: TIntegerField;
    QrPMVsNaoDtaAquis: TDateField;
    QrPMVsNaoDtaDesativ: TDateField;
    QrPMVsNaoLk: TIntegerField;
    QrPMVsNaoDataCad: TDateField;
    QrPMVsNaoDataAlt: TDateField;
    QrPMVsNaoUserCad: TIntegerField;
    QrPMVsNaoUserAlt: TIntegerField;
    QrPMVsNaoAlterWeb: TSmallintField;
    QrPMVsNaoAtivo: TSmallintField;
    QrPMVsNaoPrgLstCab: TIntegerField;
    QrPMVsNaoDependenci: TIntegerField;
    QrPMVsNaoIntrvMonDD: TIntegerField;
    QrPMVsNaoOrdem: TIntegerField;
    QrPMVsNaoReordem: TIntegerField;
    QrPMVsNaoMotInutili: TIntegerField;
    QrPMVsNaoDtaInutili: TDateField;
    QrPMVsNaoSvgPosX: TFloatField;
    QrPMVsNaoSvgPosY: TFloatField;
    QrPMVsNaoSvgZumF: TFloatField;
    QrPMVsNaoSiapImaSVG: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    TreeView1: TTreeView;
    Edit1: TEdit;
    DBGrid2: TDBGrid;
    ScrollBox1: TScrollBox;
    RSSVGPanel1: TRSSVGPanel;
    QrGraG1EqMo: TmySQLQuery;
    QrGraG1EqMoNivel1: TIntegerField;
    QrGraG1EqMoMarca: TIntegerField;
    QrGraG1EqMoObservacao: TWideMemoField;
    QrGraG1EqMoLk: TIntegerField;
    QrGraG1EqMoDataCad: TDateField;
    QrGraG1EqMoDataAlt: TDateField;
    QrGraG1EqMoUserCad: TIntegerField;
    QrGraG1EqMoUserAlt: TIntegerField;
    QrGraG1EqMoAlterWeb: TSmallintField;
    QrGraG1EqMoAtivo: TSmallintField;
    QrGraG1EqMoPrgLstCab: TIntegerField;
    QrGraG1EqMoNaoUsaPrdt: TSmallintField;
    QrGraG1EqMoVetorSvg: TWideMemoField;
    QrGraG1EqMoVetorArq: TWideStringField;
    QrGraG1EqMoPrgICorSVG: TIntegerField;
    QrGraG1EqMoDiasCorSVG: TIntegerField;
    QrGraG1EqMoQtCorSVGgg: TIntegerField;
    QrGraG1EqMoQtCorSVGgr: TIntegerField;
    QrGraG1EqMoQtCorSVGrg: TIntegerField;
    QrGraG1EqMoQtCorSVGrr: TIntegerField;
    QrGraG1EqMoQtCorSVGrb: TIntegerField;
    QrGraG1EqMoQtCorSVGbr: TIntegerField;
    QrGraG1EqMoZumPadr: TFloatField;
    QrGraG1EqMoSVGLegenda: TWideStringField;
    PMPMV: TPopupMenu;
    Zoom1: TMenuItem;
    TabSheet4: TTabSheet;
    BitBtn1: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    TabSheet5: TTabSheet;
    Image1: TImage;
    Button4: TButton;
    procedure Edit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure RSSVGPanelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RSSVGPanelMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure RSSVGPanelMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Zoom1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
    FCliente, FSiapTerCad, FSiapImaCad, FSiapImaSVG: Integer;
    FCroquiXML: String;
    // PMVs
    FPosIni: TPoint;
    FPMV_Qtds: Integer;
    FPMVs: array of TPMV;
    FSelected: TRSSVGPanel;
(*
    FPMV_Docs: array of TRSSVGDocument;
    FPMV_Imgs: array of TRSSVGPanel;
    FPMV_Cods: array of Integer;
*)
    FMovendo: Boolean;
    //
    //procedure PaintBox1Paint(Sender: TObject); Nao usei PaintBox!
    function  CriaSVGDocument(XML: String; Zum: Double; Codigo: Integer;
              Nome: String): TRSSVGPanel;
    procedure DocChanged(Sender: TObject);
    procedure ParsingNode(Sender: TObject; const Node: IXMLNode);
    procedure ParsedNode(Sender: TObject; const Element: TSVGElement);
    //function  ObtemPMV(const Id: Integer; var Codigo: Integer): Boolean;
    // FIM PMVs
    procedure ReopenSiapImaSVG(Controle: Integer);
  public
    { Public declarations }
    procedure CarregaDadosCliente(Cliente, SiapTerCad, SiapImaCad,
              SiapImaSVG: Integer);
    procedure CarregaSVGsPMVPosicionados();
    function  LimpaXML(XML: String): String;
    procedure ReopenPMVs(Qry: TMySQLQuery; Posicionado: Boolean; Codigo: Integer);
    //
    procedure FillTreeView(RSSVGDocument: TRSSVGDocument);
    function  ReopenGraG1EqMo(const Nivel1: Integer; var XML: String): Boolean;


  end;

var
  FmCunsCadSVG2: TFmCunsCadSVG2;

implementation

uses
  StrUtils, RSVclHelperFunctions, DmkDAC_PF, Module, UnDmkProcFunc, UnMyObjects,
  UMySQLModule, GetValor;

{$R *.dfm}

procedure TFmCunsCadSVG2.BitBtn1Click(Sender: TObject);
procedure ScreenShot(DestBitmap : TBitmap) ;
var
  DC : HDC;
begin
  DC := GetDC (GetDesktopWindow) ;
  try
   DestBitmap.Width := GetDeviceCaps (DC, HORZRES) ;
   DestBitmap.Height := GetDeviceCaps (DC, VERTRES) ;
   BitBlt(DestBitmap.Canvas.Handle, 0, 0, DestBitmap.Width, DestBitmap.Height, DC, 0, 0, SRCCOPY) ;
  finally
   ReleaseDC (GetDesktopWindow, DC) ;
  end;
end;

//Note: the width and height of the screen shot are equal to the width and height of the physical screen.
//Usage:
 var
 b:TBitmap;
 begin
   b := TBitmap.Create;
   try
   ScreenShot(b) ;
   Image1.Picture.Bitmap.Assign(b) ;
   finally
   b.FreeImage;
   FreeAndNil(b) ;
 end;
end;

procedure TFmCunsCadSVG2.Button1Click(Sender: TObject);
procedure ScreenShot(activeWindow: bool; destBitmap : TBitmap) ;
 var
    w,h : integer;
    DC : HDC;
    hWin : Cardinal;
    r : TRect;
 begin
    if activeWindow then
    begin
      hWin := GetForegroundWindow;
      dc := GetWindowDC(hWin) ;
      GetWindowRect(hWin,r) ;
      w := r.Right - r.Left;
      h := r.Bottom - r.Top;
    end
    else
    begin
      hWin := GetDesktopWindow;
      dc := GetDC(hWin) ;
      w := GetDeviceCaps (DC, HORZRES) ;
      h := GetDeviceCaps (DC, VERTRES) ;
    end;

    try
     destBitmap.Width := w;
     destBitmap.Height := h;
     BitBlt(destBitmap.Canvas.Handle,
            0,
            0,
            destBitmap.Width,
            destBitmap.Height,
            DC,
            0,
            0,
            SRCCOPY) ;
    finally
     ReleaseDC(hWin, DC) ;
    end;
 end;
//Usage:  var
var
    b:TBitmap;
 begin
   b := TBitmap.Create;
   try
     ScreenShot(TRUE, b) ;
     Image1.Picture.Bitmap.Assign(b) ;
   finally
     b.FreeImage;
     FreeAndNil(b) ;
   end;
end;

procedure TFmCunsCadSVG2.CarregaDadosCliente(Cliente, SiapTerCad, SiapImaCad,
  SiapImaSVG: Integer);
begin
  FCliente := Cliente;
  FSiapTerCad := SiapTerCad;
  FSiapImaCad := SiapImaCad;
  FSiapImaSVG := SiapImaSVG;
  ReopenSiapImaSVG(FSiapImaSVG);
  //
  FCroquiXML := DmkPF.XMLDecode(QrSiapImaSVGArquivo.Value);
  //
  RSSVGDocument1.SVG.LoadFromText(FCroquiXML);
  RSSVGPanel1.SVGRootID := '';
  RSSVGPanel1.WrapMode := iwOriginal;
  FillTreeView(RSSVGDocument1);
  //
  ReopenPMVs(QrPMVsSim, True,  0);
  CarregaSVGsPMVPosicionados();
  ReopenPMVs(QrPMVsNao, False, 0);
  RSSVGPanel1.AutoSize := True;
  //RSSVGPanel1.BevelOuter := bvNone;
end;

procedure TFmCunsCadSVG2.CarregaSVGsPMVPosicionados();
  procedure CarregaAtual();
  var
    XML: String;
    SVGGRoup: TSVGGroup;
    N, Codigo: Integer;
    RSSVGPanel: TRSSVGPanel;
    ZumF: Double;
  begin
    if ReopenGraG1EqMo(QrPMVsSimNivel1.Value, XML) then
    begin
      Codigo := QrPMVsSimCodigo.Value;
      //
      XML := LimpaXML(DmkPF.XMLDecode(XML));
      //
      ZumF := QrPMVsSimSvgZumF.Value;
      RSSVGPanel := CriaSVGDocument(XML, ZumF, QrPMVsSimCodigo.Value,
        QrPMVsSimNome.Value);
      RSSVGPanel.ScaleOriginal := ZumF;
      RSSVGPanel.Left := (*FPositionX +*) Trunc(QrPMVsSimSvgPosX.Value);
      RSSVGPanel.Top  := (*FPositionY +*) Trunc(QrPMVsSimSvgPosY.Value);
    end;
  end;
begin
  QrPMVsSim.First;
  while not QrPMVsSim.Eof do
  begin
    CarregaAtual();
    //
    QrPMVsSim.Next;
  end;
end;

function TFmCunsCadSVG2.CriaSVGDocument(XML: String; Zum: Double; Codigo:
Integer; Nome: String): TRSSVGPanel;
var
  SVGAlign: TSVGAlign;
  I, J, H, W: Integer;
begin
  I := FPMV_Qtds;
  J := I + 1;
  SetLength(FPMVs, J);
  FPMVs[I].ID := I;
(*
  SetLength(FPMV_Docs, J);
  SetLength(FPMV_Imgs, J);
  SetLength(FPMV_Cods, J);
  //
  FPMV_Cods[I] := Codigo;
  //
  FPMV_Docs[I] := TRSSVGDocument.Create(Self);
  FPMV_Docs[I].SVG.LoadFromText(XML);
*)
  FPMVs[I].Doc := TRSSVGDocument.Create(Self);
  FPMVs[I].Doc.SVG.LoadFromText(XML);
  //
(*
  FPMV_Imgs[I] := TRSSVGPanel.Create(Self);
  Result := FPMV_Imgs[I];
  Result.Tag := I;
  Result.Parent := ScrollBox1;
*)
  FPMVs[I].Img := TRSSVGPanel.Create(Self);
  Result := FPMVs[I].Img;
  Result.Tag := Codigo;
  Result.Parent := ScrollBox1;
  FPMVs[I].Codigo := Codigo;
  FPMVs[I].Nome := Nome;
  //
  Result.WrapMode := iwOriginal;
  Result.SVGDocument := FPMVs[I].Doc;
  Result.SVGRootID := '';
  //FillTreeView(RSSVGDocument2);
  //RSSVGPanel1.Visible := False;
  Result.OnMouseDown := RSSVGPanelMouseDown;
  Result.OnMouseMove := RSSVGPanelMouseMove;
  Result.OnMouseUp   := RSSVGPanelMouseUp;
  //
  THackRSSVGPanel(Result).AutoSize := True;
  THackRSSVGPanel(Result).BevelOuter := bvNone;
(*
  W := THackRSSVGPanel(Result).Bitmap.Width;
  H := THackRSSVGPanel(Result).Bitmap.Height;
  W := THackRSSVGPanel(Result).Bitmap.Width;
*)
  H := THackRSSVGPanel(Result).Height;
  W := THackRSSVGPanel(Result).Width;
  Result.Height := Trunc(Trunc(H + 1) * Zum) + 1;
  Result.Width  := Trunc(Trunc(W + 1) * Zum) + 1;
  //
  FPMV_Qtds := J;
end;

procedure TFmCunsCadSVG2.DBGrid2DblClick(Sender: TObject);
var
  XML: String;
  N, Codigo, SvgPosX, SvgPosY: Integer;
  SvgZumF: Double;
  RSSVGPanel: TRSSVGPanel;
begin
  if ReopenGraG1EqMo(QrPMVsNaoNivel1.Value, XML) then
  begin
    XML := LimpaXML(DmkPF.XMLDecode(XML));
    //
    SvgPosX := 10;
    SvgPosY := 10;
    SvgZumF := QrGraG1EqMoZumPadr.Value;
    RSSVGPanel := CriaSVGDocument(XML, SvgZumF, QrPMVsNaoCodigo.Value,
      QrPMVsNaoNome.Value);
    RSSVGPanel.ScaleOriginal := SvgZumF;
    RSSVGPanel.Left := SvgPosX;
    RSSVGPanel.Top  := SvgPosY;
    //
    Codigo := QrPMVsNaoCodigo.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False, [
    'SvgPosX', 'SvgPosY', 'SvgZumF',
    'SiapImaSVG'], ['Codigo'], [
    SvgPosX, SvgPosY, SvgZumF,
    FSiapImaSVG], [Codigo], True) then
    begin
      ReopenPMVs(QrPMVsSim, True,  Codigo);
      QrPMVsNao.Next;
      Codigo := QrPMVsNaoCodigo.Value;
      ReopenPMVs(QrPMVsNao, False, Codigo);
    end;
  end;
end;

procedure TFmCunsCadSVG2.DocChanged(Sender: TObject);
begin
//
end;

procedure TFmCunsCadSVG2.Edit1Change(Sender: TObject);
(*
  function GetNodeByText
  (ATree : TTreeView; AValue:String;
   AVisible: Boolean): TTreeNode;
  var
      Node: TTreeNode;
  begin
    Result := nil;
    if ATree.Items.Count = 0 then Exit;
    Node := ATree.Items[0];
    while Node <> nil do
    begin
      if ContainsText(Node.Text, AValue) then
      begin
        Result := Node;
        if AVisible then
        begin
          result.Selected := True;
          Result.MakeVisible;
        end;
        Break;
      end;
      Node := Node.GetNext;
    end;
  end;
*)
begin
(*
  GetNodeByText(TreeView1, Edit1.Text, True);
*)
end;

procedure TFmCunsCadSVG2.FillTreeView(RSSVGDocument: TRSSVGDocument);
  procedure FillTreeViewItem( E: TSVGElement; tvItem: TTreeNode );
  var
    i: Integer;
    newItem: TTreeNode;
  begin
    for i := 0 to E.Items.Count - 1 do
    begin
      newItem := TreeView1.Items.AddChildObject(tvItem, E.Items[i].ID + '('+
        E.Items[i].SVGTypeName+')', E.Items[i]);
//      newItem.IsChecked := True;
      FillTreeViewItem( E.Items[i], newItem );
    end;
  end;
begin
  TreeView1.Items.BeginUpdate;
  try
    TreeView1.OnChange := nil;
    TreeView1.Items.Clear;
    FillTreeViewItem( RSSVGDocument.SVG, nil );
  finally
    //TreeView1.OnChange := TreeView1Change;
    TreeView1.Items.EndUpdate;
  end;
end;

procedure TFmCunsCadSVG2.FormCreate(Sender: TObject);
begin
  FSelected := nil;
  FMovendo  := False;
  FPMV_Qtds := 0;
end;

function TFmCunsCadSVG2.LimpaXML(XML: String): String;
const
  Xi = '<!--';
  Xf = '-->';
var
  Ini, Tam: Integer;
  Tmp: String;
begin
  Tmp := StringReplace(
    //StringReplace(
      XML,
//      '\"', '""',[rfReplaceAll]),
      '-- >', '-->',[rfReplaceAll]);
  //
  if pos(Xi, Tmp) > 0 then
  begin
    Result := '';
    Ini := 0;
    Tam := 0;
    //Tmp := XML;
    while Length(Tmp) > 0 do
    begin
      Tam := Pos(Xi, Tmp);
      if Tam > 0 then
        //Result := Result + Tmp.Substring(0, Tam - 1);
        Result := Result + Copy(Tmp, 1, Tam - 1);
      Tam := Pos(Xf, Tmp);
      //Tmp := Tmp.Substring(Tam + 2); // 3 - 1
      Tmp := Copy(Tmp, Tam + 3);
      if pos(Xi, Tmp) = 0 then
      begin
        Result := Result + Tmp;
        Tmp := '';
      end;
    end;

  end else
    Result := Tmp;
end;

{
function TFmCunsCadSVG.ObtemPMV(const Id: Integer; var Codigo: Integer): Boolean;
var
  I: Integer;
begin
  Result := True;
  Codigo := -1;
  for I := 0 to High(FPMVs) do
  begin
    if FPMVs[I].Img.Tag = I then
    begin
      Result := True;
      Codigo := FPMVs[I].Codigo;
      Exit;
    end;
  end;
end;
}

procedure TFmCunsCadSVG2.ParsedNode(Sender: TObject; const Element: TSVGElement);
begin
//
end;

procedure TFmCunsCadSVG2.ParsingNode(Sender: TObject; const Node: IXMLNode);
begin
//
end;

function TFmCunsCadSVG2.ReopenGraG1EqMo(const Nivel1: Integer; var XML: String): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraG1EqMo, Dmod.MyDB, [
  'SELECT *  ',
  'FROM grag1eqmo ',
  'WHERE Nivel1=' + Geral.FF0(Nivel1),
  '']);
  if QrGraG1EqMo.RecordCount = 0 then
  begin
    Result := False;
    Geral.MB_Aviso('Item n�o encontrado nos equipamentos de monitoramento!');
  end else
  begin
    XML := QrGraG1EqMoVetorSvg.Value;
    Result := XML <> '';
    if not Result then
      Geral.MB_Aviso('Equipamento sem vetor (croqui) definido!');
  end;
end;

procedure TFmCunsCadSVG2.ReopenPMVs(Qry: TMySQLQuery; Posicionado: Boolean;
Codigo: Integer);
var
  SQLPOsicionado: String;
begin
  if Posicionado then
    SQLPOsicionado := 'AND cad.SiapImaSVG <> 0'
  else
    SQLPOsicionado := 'AND cad.SiapImaSVG = 0';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT mot.Nome NO_MOTDESAT, gg1.Nome NO_EQUI, gg1.Nivel1, ',
  'plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA, ',
  'IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT, ',
  'IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT, ',
  'cad.* ',
  'FROM pipcad cad ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab ',
  'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci ',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
  'LEFT JOIN osmoncab omc ON omc.Conta=cad.OsMonCab ',
  'LEFT JOIN oscab osc ON osc.Codigo=omc.Codigo ',
  'WHERE osc.Entidade=' + Geral.FF0(FCliente),
  'AND (cad.MotInutili = 0 AND cad.MotInutili = 0) ',
  SQLPOsicionado,
  'ORDER BY Nome ',
  '']);
  //
  if Codigo <> 0 then
    Qry.Locate('Codigo', Codigo, []);
end;

procedure TFmCunsCadSVG2.ReopenSiapImaSVG(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapImaSVG, Dmod.MyDB, [
  'SELECT * ',
  'FROM siapimasvg ',
  'WHERE Controle=' + Geral.FF0(FSiapImaSVG),
  '']);
end;

procedure TFmCunsCadSVG2.RSSVGPanelMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FSelected := TRSSVGPanel(Sender);
  if ssLeft in Shift then
  begin
    FMovendo := True;
    FPosIni.X := X;
    FPosIni.Y := Y;
  end;
end;

procedure TFmCunsCadSVG2.RSSVGPanelMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then
  begin
    if FMovendo then
    begin
      TRSSVGPanel(Sender).Left := TRSSVGPanel(Sender).Left - (FPosIni.X - X);
      TRSSVGPanel(Sender).Top  := TRSSVGPanel(Sender).Top - (FPosIni.Y - Y);
    end;
  end;
end;

procedure TFmCunsCadSVG2.RSSVGPanelMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Item, Codigo, SvgPosX, SvgPosY: Integer;
begin
  if Button = mbLeft then
  begin
    if FMovendo then
    begin
      FMovendo := False;
      Item := TRSSVGPanel(Sender).Tag;
      Codigo  := FPMVs[Item].Codigo;
      SvgPosX := TRSSVGPanel(Sender).Left - (FPosIni.X - X);
      SvgPosY := TRSSVGPanel(Sender).Top - (FPosIni.Y - Y);
      TRSSVGPanel(Sender).Left := SvgPosX;
      TRSSVGPanel(Sender).Top  := SvgPosY;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False, [
      'SvgPosX', 'SvgPosY'], ['Codigo'], [
      SvgPosX, SvgPosY], [Codigo], True);
    end;
  end else
  if Button = mbRight then
  begin
    MyObjects.MostraPopUpDeBotao(PMPMV, ScrollBox1, TRSSVGPanel(Sender));
  end;
end;

procedure TFmCunsCadSVG2.Zoom1Click(Sender: TObject);
var
  //Id,
  Codigo, W, H: Integer;
  SvgZumF, Zum: Variant;
begin
  //ID := TRSSVGPanel(FSelected).Tag;
  //if ObtemPMV(Id, Codigo) then
  begin
    SvgZumF := TRSSVGPanel(FSelected).ScaleOriginal;
    Codigo  := TRSSVGPanel(FSelected).Tag;
    //
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, SvgZumF, 6, 0,
    '0,000001', '', True, 'Zoom', 'Fator zoom (x vezes): ', 0, SvgZumF) then
    begin
      if MyObjects.FIC(SvgZumF < 0.000001, nil, 'Valor inv�lido!') then
        Exit;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False, [
      'SvgZumF'], ['Codigo'], [
      SvgZumF], [Codigo], True);
      //
      Zum := TRSSVGPanel(FSelected).ScaleOriginal;
      W := TRSSVGPanel(FSelected).Width / Zum * SvgZumF;
      H := TRSSVGPanel(FSelected).Height / Zum * SvgZumF;
      TRSSVGPanel(FSelected).Width  := W;
      TRSSVGPanel(FSelected).Height := H;
      TRSSVGPanel(FSelected).ScaleOriginal := SvgZumF;
    end;
  end;
end;

procedure TFmCunsCadSVG2.Button2Click(Sender: TObject);
  procedure PrintControl(AControl :TWinControl;AOut:TBitmap);
  var
    DC: HDC;
  begin
    if not Assigned(AOut) then Exit;
    if not Assigned(AControl) then Exit;

    DC :=GetWindowDC(AControl.Handle);
    AOut.Width  :=AControl.Width;
    AOut.Height :=AControl.Height;
    with AOut do
    BitBlt(
      Canvas.Handle, 0, 0, Width, Height, DC, 0, 0, SrcCopy
    );

    ReleaseDC(AControl.Handle, DC);
  end;
 var
 b:TBitmap;
 begin
   b := TBitmap.Create;
   try
   PrintControl(ScrollBox1, b);
   Image1.Picture.Bitmap.Assign(b) ;
   finally
   b.FreeImage;
   FreeAndNil(b) ;
 end;
end;

procedure TFmCunsCadSVG2.Button4Click(Sender: TObject);
  function Control2Bitmap(Control_: TWinControl): TBitmap;
  begin
    Result := TBitmap.Create;
    with Result do begin
      Height := Control_.Height;
      Width  := Control_.Width;
      Canvas.Handle := CreateDC(nil, nil, nil, nil);
      Canvas.Lock;
      Control_.PaintTo(Canvas.Handle, 0, 0);
      Canvas.Unlock;
      DeleteDC(Canvas.Handle);
    end;
  end;
 var
 b:TBitmap;
begin
  b := Control2Bitmap(ScrollBox1);
  try
    Image1.Picture.Bitmap.Assign(b) ;
    Image1.Picture.SaveToFile('C:\Dermatek\RSSVGPanel1.bmp');
  finally
    b.FreeImage;
    FreeAndNil(b) ;
  end;
end;

end.
