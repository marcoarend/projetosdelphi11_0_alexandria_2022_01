unit Bugs_Tabs;
 // ""
{ Colocar no MyListas:

Uses Bugs_Tabs;
//
function TMyListas.CriaListaTabelas:
  UnBugs_Tabs.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
  UnBugs_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
  UnBugs_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
  UnBugs_Tabs.CarregaListaFRIndices(Tabela, FRIndices, FLIndices);
//
function TMyListas.CriaListaCampos:
  UnBugs_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos);
  UnBugs_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  UnBugs_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
}
interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral, MyListas,
  UnDmkListas;

type
  TGraBugsOpera = (gboNenhum, gboServi, gboCaixa);
  TGraBugsServi = (gbsIndef, gbsAplEMon, gbsAplica, gbsMonitora, gbsMonMulti);
  TGraBugsMater = (gbmIndef, gbmEquiEProd, gbmEquipam, gbmProduto);
  TBugs_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    //function CarregaListaTabsWeb(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(TabelaBase: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL2(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  UnBugs_Tabs: TBugs_Tabs;

const
  //
  CO_COD_TAXON_ITS_1  = '1';
  CO_COD_TAXON_ITS_2  = '2';
  CO_COD_TAXON_ITS_3  = '3';
  CO_COD_TAXON_ITS_4  = '4';
  CO_COD_TAXON_ITS_5  = '5';
  CO_COD_TAXON_ITS_6  = '6';
  CO_COD_TAXON_ITS_7  = '7';
  CO_COD_TAXON_ITS_8  = '8';
  CO_COD_TAXON_ITS_9  = '9';
  CO_COD_TAXON_ITS_10 = '10';
  CO_COD_TAXON_ITS_11 = '11';
  CO_COD_TAXON_ITS_12 = '12';
  CO_COD_TAXON_ITS_13 = '13';
  CO_COD_TAXON_ITS_14 = '14';
  CO_COD_TAXON_ITS_15 = '15';
  CO_COD_TAXON_ITS_16 = '16';
  CO_COD_TAXON_ITS_17 = '17';
  CO_COD_TAXON_ITS_18 = '18';
  CO_COD_TAXON_ITS_19 = '19';
  CO_COD_TAXON_ITS_20 = '20';
  CO_COD_TAXON_ITS_21 = '21';
  CO_COD_TAXON_ITS_22 = '22';
  CO_COD_TAXON_ITS_23 = '23';
  CO_COD_TAXON_ITS_24 = '24';
  CO_COD_TAXON_ITS_25 = '25';
  CO_COD_TAXON_ITS_26 = '26';
  CO_COD_TAXON_ITS_27 = '27';
  CO_COD_TAXON_ITS_28 = '28';
  CO_COD_TAXON_ITS_29 = '29';
  CO_COD_TAXON_ITS_30 = '30';
  CO_COD_TAXON_ITS_31 = '31';
  CO_COD_TAXON_ITS_32 = '32';
  CO_COD_TAXON_ITS_33 = '33';
  CO_COD_TAXON_ITS_34 = '34';
  CO_COD_TAXON_ITS_35 = '35';
  CO_COD_TAXON_ITS_36 = '36';
  CO_COD_TAXON_ITS_37 = '37';
  CO_COD_TAXON_ITS_38 = '38';
  CO_COD_TAXON_ITS_39 = '39';
  CO_COD_TAXON_ITS_40 = '40';
  CO_COD_TAXON_ITS_41 = '41';
  CO_COD_TAXON_ITS_42 = '42';
  CO_COD_TAXON_ITS_43 = '43';
  CO_COD_TAXON_ITS_44 = '44';
  CO_COD_TAXON_ITS_45 = '45';
  CO_COD_TAXON_ITS_46 = '46';
  CO_COD_TAXON_ITS_47 = '47';
  CO_COD_TAXON_ITS_48 = '48';
  CO_COD_TAXON_ITS_49 = '49';
  CO_COD_TAXON_ITS_50 = '50';
  CO_COD_TAXON_ITS_51 = '51';
  CO_COD_TAXON_ITS_52 = '52';
  CO_COD_TAXON_ITS_53 = '53';
  CO_COD_TAXON_ITS_54 = '54';
  CO_COD_TAXON_ITS_55 = '55';
  CO_COD_TAXON_ITS_56 = '56';
  CO_COD_TAXON_ITS_57 = '57';
  CO_COD_TAXON_ITS_58 = '58';
  CO_COD_TAXON_ITS_59 = '59';
  CO_COD_TAXON_ITS_60 = '60';
  CO_COD_TAXON_ITS_61 = '61';
  CO_COD_TAXON_ITS_62 = '62';
  CO_COD_TAXON_ITS_63 = '63';
  CO_COD_TAXON_ITS_64 = '64';
  CO_COD_TAXON_ITS_65 = '65';
  CO_COD_TAXON_ITS_66 = '66';
  CO_COD_TAXON_ITS_67 = '67';
  CO_COD_TAXON_ITS_68 = '68';
  CO_COD_TAXON_ITS_69 = '69';
  CO_COD_TAXON_ITS_70 = '70';
  CO_COD_TAXON_ITS_71 = '71';
  CO_COD_TAXON_ITS_72 = '72';
  CO_COD_TAXON_ITS_73 = '73';
  CO_COD_TAXON_ITS_74 = '74';
  CO_COD_TAXON_ITS_75 = '75';
  CO_COD_TAXON_ITS_76 = '76';
  CO_COD_TAXON_ITS_77 = '77';

  //

  CO_BUG_STATUS_0000_ABERTURA = 0000; //|"Abertura de OS"');
  CO_BUG_STATUS_0100_PRE_ATEN = 0100; //|"Pr�-atendimento"');
  CO_BUG_STATUS_0200_VISTORIA = 0200; //|"Vistoria"');
  CO_BUG_STATUS_0300_EM_ORCAM = 0300; //|"Em or�amento"');
  CO_BUG_STATUS_0400_ORCA_PAS = 0400; //|"Or�amento passado"');
  CO_BUG_STATUS_0450_ORCA_OK_ = 0450; //|"Or�amento aprovado"');
  CO_BUG_STATUS_0500_AGEN_EXE = 0500; //|"Agendado a execu��o"');
  CO_BUG_STATUS_0550_AGEN_OK_ = 0550; //|"Confirmado Agendamento"');
  CO_BUG_STATUS_0600_EM_EXECU = 0600; //|"Em execu��o "');
  CO_BUG_STATUS_0700_EXE_FINA = 0700; //|"Execu��o finalizada"');
  CO_BUG_STATUS_0750_CEX_ENVI = 0750; //|"Comprovante Enviado"');
  CO_BUG_STATUS_0800_FATURADO = 0800; //|"Faturado"');
  CO_BUG_STATUS_1000_POS_VEND = 1000; //|"P�s venda"');'
  CO_BUG_STATUS_2000_O_S_BAIX = 2000; //|"OS baixada"');


  //CO_COD_GraTabApp_NemhumGTA = 0; // para todos apps  > est� no UnDmkListas!
  CO_COD_GraTabApp_GraG1EqAp = CO_DMKID_APP * 1000 + 001;
  CO_COD_GraTabApp_GraG1EqMo = CO_DMKID_APP * 1000 + 002;
  CO_COD_GraTabApp_GraG1PrAp = CO_DMKID_APP * 1000 + 003;
  CO_COD_GraTabApp_GraG1PrMo = CO_DMKID_APP * 1000 + 004;

  CO_TABELA_TIPO_LUGAR_001_SIAPIMADEP = 1;
  CO_TABELA_TIPO_LUGAR_002_MOVAMOVCAD = 2;


  // 0=N�o informado, 1=N�o executado na OS origem, 2=Executado na OS origem
  CO_SRV_HOW_ESTAVA_NAO_INFO = 0;
  CO_SRV_HOW_ESTAVA_NAO_EXEC = 1;
  CO_SRV_HOW_ESTAVA_SIM_EXEC = 2;

  CO_COD_FatoGeradr_INDEF       = 00; // nenhuma a��o
  CO_COD_FatoGeradr_DEPRECADO   = 10; // Deprecado
  CO_COD_FatoGeradr_1aAcao      = 20; // 1� A��o
  CO_COD_FatoGeradr_Corretiva   = 30; // Corretiva
  CO_COD_FatoGeradr_Preventiva  = 40; // Preventiva
  CO_COD_FatoGeradr_Preditiva   = 50; // Preditiva


  CO_FORMUL_FILHA_INICIAL = 1;
  CO_FORMUL_FILHA_POSTERO = 2;
  //
  CO_FORMUL_FILHA_MONIT_EH_FILHA = 1;
  CO_FORMUL_FILHA_MONIT_EH_MONIT = 2;
  //
  CO_FORMUL_FILHA_MONIT_Txt_FILHA = 'APL';
  CO_FORMUL_FILHA_MONIT_Txt_MONIT = 'PIP';
  //
  CO_FORMUL_FILHA_MONIT_EmisStat_ABERTO  = 0;
  CO_FORMUL_FILHA_MONIT_EmisStat_PARCIAL = 1;
  CO_FORMUL_FILHA_MONIT_EmisStat_TOTAL   = 2;


  // 'Operacao' :: Opera��es realizadas em uma OS
  CO_OS_OPERACAO_SERVICO_DEDETIZACAO     = 1;
  CO_OS_OPERACAO_SERVICO_CAIXADAGUA      = 2;
  (*DEDETIZACAO + CAIXADAGUA             = 3; *)
  CO_OS_OPERACAO_SERVICO_MONITORAMENTO   = 4;
  (*DEDETIZACAO + MONITORAMENTO          = 5; *)
  (*CAIXADAGUA  + MONITORAMENTO          = 6; *)
  (*DEDETIZACAO + CAIXADAGUA + MONITORAM = 7; *)

  // 'HowGerou' :: Como Gerou: 0=Manual, 1=Clonado, 2=Gerada por f�rmulas filhas de OS m�e
  CO_OS_HOWGEROU_OS_MANUAL_USER          = 0;
  CO_OS_HOWGEROU_OS_CLONANDO_OS          = 1;
  CO_OS_HOWGEROU_OS_AUTO_PREVENT         = 2;

  //////////////////////////////  OS automaticas ///////////////////////////////
  //EntiContat, NumContrat, EntPagante, EntContrat, CondicaoPg, CartEmis
  CO_OSFlh_OPT_NAO_DEFINID = 0;
  CO_OSFlh_OPT_DEIXR_VAZIO = 1;
  CO_OSFlh_OPT_DEF_IN_LUGR = 2;
  CO_OSFlh_OPT_FROM_OS_INI = 3;
  //CO_OSFlh_OPT_AGR_AGE_OSs = 4;
  //

  ////////////////////////////  FIM OS automaticas //////////////////////////////

  CO_OSPipIts_SEM_TAB = 1; // Sem tabela, se refere ao pr�prio PIP
  CO_OSPipIts_OSMONREC = 2; // Produtos adicionados na coloca��o do PIP
  CO_OSPipIts_OSPIPITSPR = 3; // Produtos adicionados em monitoramento
  //
  CO_OSPipItsPr_INDEF = 0;
  CO_OSPipItsPr_TROCA = 1;
  CO_OSPipItsPr_INCLU = 2;
  //
  CO_StPipAdPrg_NONE = 0;
  CO_StPipAdPrg_INIT = 1;
  CO_StPipAdPrg_FINI = 2;
  //
  CO_AcaoPrd_Nada = 0;
  CO_AcaoPrd_Adic = 1;
  CO_AcaoPrd_Troc = 2;
  CO_AcaoPrd_Tira = 3;
  //
{ Definido na unit AppListas > CO_PRG_LST_...
  CO_PrgFuncao_Nenhuma = 0; //  1 = Nenhuma            Respondido
  CO_PrgFuncao_Binario = 1; //  2 = Bin�rio            RespBin
  CO_PrgFuncao_Quantit = 2; //  4 = Quantitativo       RespQtd
  CO_PrgFuncao_BxaProd = 3; //  8 = Baixa de Produto   ...multi itens na tabela OSPipItsPr
  CO_PrgFuncao_Atribut = 4; // 16 = Uso de Atributo    RespAtrCad + RespAtrIts + RespAtrTxt
  CO_PrgFuncao_TxtLivr = 5; // 32 = Texto Livre        RespTxtLvr
}
  //
  CO_Respondido_Nao = 0;
  CO_Respondido_SIM = 1;
  CO_Respondido_N_A = 2; // Nao aplicavel
  //
  CO_LupForma_COD_NaoTem  = 0;
  CO_LupForma_COD_Binario = 1;
  CO_LupForma_COD_PorLoop = 2;
  //
  CO_RespMetodo_COD_None = 0;
  CO_RespMetodo_COD_Manu = 1;
  CO_RespMetodo_COD_Load = 2;

implementation

uses UMySQLModule;

function TBugs_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('XXX') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"XXX"');
  end;
end;

function TBugs_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, 'AgeEqiCab'   , '');             // Cadastro de Equipes de agentes                        //////////
    MyLinguas.AdTbLst(Lista, False, 'AgeEqiIts'   , '');             // Itens (Agentes) de Equipes                            //////////
    //
    MyLinguas.AdTbLst(Lista, False, 'ContrBugsN'   , '');            // Cadastro de Tags de contratos - Pragas Inclusas       //////////
    MyLinguas.AdTbLst(Lista, False, 'ContrBugsS'   , 'ContrBugsN');  // Cadastro de Tags de contratos - Pragas Exclusas       //////////
    MyLinguas.AdTbLst(Lista, False, 'ContrServi'   , '');            // Cadastro de Tags de contratos - Servo�os Contemplados //////////
    MyLinguas.AdTbLst(Lista, False, 'CtrTagNiv3'   , 'CtrTagNiv1');  // Cadastro de Tags de contratos - N�vel 3               //////////
    MyLinguas.AdTbLst(Lista, False, 'CtrTagNiv2'   , 'CtrTagNiv1');  // Cadastro de Tags de contratos - N�vel 2               //////////
    MyLinguas.AdTbLst(Lista, False, 'CtrTagNiv1'   , '');            // Cadastro de Tags de contratos - N�vel 1               //////////
    MyLinguas.AdTbLst(Lista, False, 'CunsCad'   , '');  // Clientes                                                           //////////
    MyLinguas.AdTbLst(Lista, False, 'CunsIts'   , '');  // Contratantes e seus consumidores                                   //////////
    MyLinguas.AdTbLst(Lista, False, 'CunsImgCab', '');  // Fotos relacionadas a clientes                                      //////////
    MyLinguas.AdTbLst(Lista, False, 'CunsImgNCT', '');  // Cadastro de N�o Conformidades T�cnicas (N.C.T) e Comunicados de A��es Corretivas (C.A.C.)//////////
    //Linguas.AdTbLst(Lista, False, 'CunsImgCmt', '');  // Coment�rio pr�-cadastrados de fotos relacionadas a clientes        //////////
    MyLinguas.AdTbLst(Lista, False, 'CunsImgSit', '');  // Tipos de Status para as Fotos relacionadas a clientes              //////////
    MyLinguas.AdTbLst(Lista, False, 'TxtGeneric'   , '');  // Textos gen�ricos do aplicativo                                  //////////
    //Linguas.AdTbLst(Lista, False, 'CunsCtr'   , '');  // Contratos                                                        //////////
    // M�veis e Autom�veis                                                                                                  //////////
    MyLinguas.AdTbLst(Lista, False, 'MovAmovCad', '');  // M�veis e autom�veis                                                //////////
    // Locais de Aplicacao                                                                                                  //////////
    MyLinguas.AdTbLst(Lista, False, 'SiapTerCad', '');  // Terrenos                                                           //////////
    MyLinguas.AdTbLst(Lista, False, 'SiapTerFlh', '');  // Defaults para OSs filhas                                           //////////
    MyLinguas.AdTbLst(Lista, False, 'SiapImaCad', '');  // Im�veis, m�veis ou autom�veis                                      //////////
    MyLinguas.AdTbLst(Lista, False, 'SiapImaCav', '');  //   - Caracter�sticas das �reas vicinais                             //////////
    MyLinguas.AdTbLst(Lista, False, 'SiapImaCdi', '');  //   - Caracter�sticas do im�vel                                      //////////
    MyLinguas.AdTbLst(Lista, False, 'SiapImaRes', '');  //   - Residentes                                                     //////////
    MyLinguas.AdTbLst(Lista, False, 'SiapImaCui', '');  //   - Cuidados                                                       //////////
    MyLinguas.AdTbLst(Lista, False, 'SiapImaAti', '');  //   - Atividades                                                     //////////
    MyLinguas.AdTbLst(Lista, False, 'SiapImaDep', '');  //   - Depend�ncias                                                   //////////
    MyLinguas.AdTbLst(Lista, False, 'SiapImaCxa', '');  //   - Caixas D`�gua                                                  //////////
    ///////// Listas Simples                                                                                                //////////
    //Linguas.AdTbLst(Lista, False, 'Atividades', '_lst_sample');   //   - Venda de ..., Fabrica de....                     //////////
    MyLinguas.AdTbLst(Lista, False, 'Caracteris', '_lst_sample');   //   -                                                    //////////
    MyLinguas.AdTbLst(Lista, False, 'CxaMaters' , '_lst_sample');   //   - Materiais das caixas d`�gua                        //////////
    MyLinguas.AdTbLst(Lista, False, 'CxaFormas' , '_lst_sample');   //   - Formas das caixas d`�gua                           //////////
    MyLinguas.AdTbLst(Lista, False, 'Cuidados'  , '_lst_sample');   //   - Cuidados nas resid�ncias                           //////////
    MyLinguas.AdTbLst(Lista, False, 'Dependenci', ''           );   //   - C�modos das resid�ncias                            //////////
    MyLinguas.AdTbLst(Lista, False, 'DependType', '_lst_sample');   //   - Tipos de depend�ncias                              //////////
    MyLinguas.AdTbLst(Lista, False, 'Finalidads', '_lst_sample');   //   - Residencia, com�rcio, ind�stria, etc               //////////
    MyLinguas.AdTbLst(Lista, False, 'HowFounded', '_lst_sample');   //   - Propaganda que atingiu o cliente                   //////////
    MyLinguas.AdTbLst(Lista, False, 'MotDesativ', '_lst_sample');   //   - Motivos de desativa��o de PIP                      //////////
    MyLinguas.AdTbLst(Lista, False, 'Objetos'   , '_lst_sample');   //   - Im�veis, m�veis e autom�veis                       //////////
    MyLinguas.AdTbLst(Lista, False, 'Residentes', '_lst_sample');   //   - Moradores                                          //////////
    MyLinguas.AdTbLst(Lista, False, 'TpConstrus', '_lst_sample');   //   - Tipos de constru��o                                //////////
    // Vistoria                                                                                                             //////////
    MyLinguas.AdTbLst(Lista, False, 'Abrangicie', '_lst_sample');   //   - Abrang�ncia de aplica��o (Superf�cie de aplica��o  //////////
    MyLinguas.AdTbLst(Lista, False, 'DesServico', '');   //   - Tipo de Servi�o:  Des...alguma coisa               //////////
    MyLinguas.AdTbLst(Lista, False, 'MulServico', '_lst_sample');   //   - Combina��es de siglas de Servi�os:  Des...alguma coisa               //////////
    MyLinguas.AdTbLst(Lista, False, 'EstatusOSs', '');              //   - Status (passos) das OSs                            //////////
    MyLinguas.AdTbLst(Lista, False, 'FatoGeradr', ''           );   //   - Fato gerador > Motivo da OS                        //////////
    MyLinguas.AdTbLst(Lista, False, 'Formulas'  , '');   //   - F�rmulas                                                      //////////
    MyLinguas.AdTbLst(Lista, False, 'FormulIF'  , '');   //   - Itens de F�rmulas                                             //////////
    MyLinguas.AdTbLst(Lista, False, 'FormulIA'  , '');   //   - Abrang�ncias das F�rmulas                                     //////////
    MyLinguas.AdTbLst(Lista, False, 'FormulFiCb', '');   //   - Formulas filhas de monitramento                               //////////
    MyLinguas.AdTbLst(Lista, False, 'FormulFiDd', '');   //   - Intervalos em dias de visitas de monitramento                 //////////
    MyLinguas.AdTbLst(Lista, False, 'TipoAplica', '_lst_sample');   //   - Tipo de Aplica��o: Inje��o, pulveriza��o, etc ...  //////////
    //Linguas.AdTbLst(Lista, False, 'Praga sCab' , ''           );   //   - Praga s                                         //////////
    //Linguas.AdTbLst(Lista, False, 'Praga sGru' , '_lst_sample');   //   - Grupos de Praga s                               //////////
    //                                                                                                                      //////////
    // Atributos de M�veis e Autom�veis                                                                                     //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrAMovCad', '_atr_cad');      //   - Cadastro do atributo                               //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrAMovIts', '_atr_its');      //   - Cadastro do valores para cada atributo             //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrAMovDef', '_atr_def');      //   - Defini��o dos atributos e valores                  //////////
    //                                                                                                                      //////////
    // Atributos de Lugares
    MyLinguas.AdTbLst(Lista, False, 'AtrSTCdCad', '_atr_cad');      //   - Cadastro do atributo                               //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrSTCdIts', '_atr_its');      //   - Cadastro do valores para cada atributo             //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrSTCdDef', '_atr_def');      //   - Defini��o dos atributos e valores                  //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrSTCdTxt', '_atr_txt');      //   - Texto livre do atributo selecionado                //////////
    //                                                                                                                      //////////
    // Atributos de Im�veis                                                                                                 //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrSICdCad', '_atr_cad');      //   - Cadastro do atributo                               //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrSICdIts', '_atr_its');      //   - Cadastro do valores para cada atributo             //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrSICdDef', '_atr_def');      //   - Defini��o dos atributos e valores                  //////////
    // Atributos de Caixas d'�gua                                                                                                                     //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrSICxCad', '_atr_cad');      //   - Cadastro do atributo                               //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrSICxIts', '_atr_its');      //   - Cadastro do valores para cada atributo             //////////
    MyLinguas.AdTbLst(Lista, False, 'AtrSICxDef', '_atr_def');      //   - Defini��o dos atributos e valores                  //////////
    //
    MyLinguas.AdTbLst(Lista, False, 'OpcoesBugs', '');              //   - Op��es espec�ficas do Bugstrol                     //////////
    MyLinguas.AdTbLst(Lista, False, 'OpcoesFili', '');              //   - ParamsEmp espec�ficas do Bugstrol                  //////////
    //
    MyLinguas.AdTbLst(Lista, False, 'OSCab'     , '');              //   - Cabe�alho da OS                                    //////////
    MyLinguas.AdTbLst(Lista, False, 'OSChk'     , '');              //   - Check list de itens espec�ficos da OS              //////////
    MyLinguas.AdTbLst(Lista, False, 'OSDel'     , 'OSCab');         //   - Cabe�alho da OS Arquivada                          //////////
    MyLinguas.AdTbLst(Lista, False, 'OSAge'     , '');              //   - Agentes operacionais                               //////////
    MyLinguas.AdTbLst(Lista, False, 'OSCabAlv'  , '');              //   - Pragas alvo (pr�-atendimento)                      //////////
    MyLinguas.AdTbLst(Lista, False, 'OSCabXtr'  , '');              //   - Execu��es extras do servi�o (em outros dias)       //////////
    MyLinguas.AdTbLst(Lista, False, 'OSCxa'     , '');              //   - Limpeza de caixas d`�gua                           //////////
    MyLinguas.AdTbLst(Lista, False, 'OSCxI'     , '');              //   - Fotos de Caixas d`�gua para Or�amento / Comprovante//////////
    MyLinguas.AdTbLst(Lista, False, 'OSSrv'     , '');              //   - Servi�os                                           //////////
    //Linguas.AdTbLst(Lista, False, 'OSDep'     , '');              //   - Im�veis e m�veis do Servi�o                      //////////
    MyLinguas.AdTbLst(Lista, False, 'OSAlv'     , '');              //   - Pragas alvo do Servi�o                             //////////
    MyLinguas.AdTbLst(Lista, False, 'OSFrmCab'  , '');              //   - F�rmula��es do servi�o                             //////////
    MyLinguas.AdTbLst(Lista, False, 'OSFrmRec'  , '');              //   - Itens da receita da f�rrmula                       //////////
    MyLinguas.AdTbLst(Lista, False, 'OSFrmDep'  , '');              //   - Im�veis e m�veis da f�rmula                        //////////
    MyLinguas.AdTbLst(Lista, False, 'OSFrmAbr'  , '');              //   - Abrang�ncia da aplica��o                           //////////
    MyLinguas.AdTbLst(Lista, False, 'OSFrmEvo'  , '');              //   - Evolu��o do servi�o                                //////////
    MyLinguas.AdTbLst(Lista, False, 'OSFrmFlhCb', '');              //   - Formulas filhas de monitramento                    //////////
    MyLinguas.AdTbLst(Lista, False, 'OSFrmFlhDd', '');              //   - Intervalos iniciais em dias de visitas de monit.   //////////
    MyLinguas.AdTbLst(Lista, False, 'OSMonCab'  , '');              //   - Cabe�alho de monitoramento                         //////////
    MyLinguas.AdTbLst(Lista, False, 'OSMonRec'  , '');              //   - Itens de Monitoramento                             //////////
    //Linguas.AdTbLst(Lista, False, '_OS_Mon_Dep_'  , '');              //   - Im�veis e m�veis da f�rmula                  //////////
    MyLinguas.AdTbLst(Lista, False, 'OSMonPipDd', '');              //   - Intervalos iniciais em dias de monit. de PIPs      //////////
    MyLinguas.AdTbLst(Lista, False, 'OSPrz'     , '');              //   - Op�oes de prazos da OS                             //////////
    MyLinguas.AdTbLst(Lista, False, 'OSPipMon'  , '');              //   - PIP a serem monitorados                            //////////
    MyLinguas.AdTbLst(Lista, False, 'OSPipIts'  , '');              //   - Perguntas dos PIP a serem monitorados              //////////
    MyLinguas.AdTbLst(Lista, False, 'OSPipItsPr', '');              //   - Produtos substitu�dos / adicionados em itens de Monitoramento
    MyLinguas.AdTbLst(Lista, False, 'OSPos'     , '');              //   - P�s-venda                                          //////////
    MyLinguas.AdTbLst(Lista, False, 'OSPrv'     , '');              //   - Provid�ncias para tomar em O.S. futura             //////////
    MyLinguas.AdTbLst(Lista, False, 'OSPrvSta'  , '_lst_sample');   //   - Status das provid�ncias para tomar em O.S. futura  //////////
    MyLinguas.AdTbLst(Lista, False, 'OSSta'     , '');              //   - Status registrados da OS                           //////////
    MyLinguas.AdTbLst(Lista, False, 'OSFlhGer'  , '');              //   - Controle de gera��o de OSs filhas                  //////////
    //
    // Tabelas de Grade pr�prias do aplicativo:
    MyLinguas.AdTbLst(Lista, False, 'GraG1EqAp'  , '');            //   - Dados complementares do GraGru1: Equipam. de Aplica��o ///////
    MyLinguas.AdTbLst(Lista, False, 'GraG1EqMo'  , '');            //   - Dados complementares do GraGru1: Equipam. de Monitoramento ///
    MyLinguas.AdTbLst(Lista, False, 'GraG1PrAp'  , '');            //   - Dados complementares do GraGru1: Produtos de Aplica��o ///////
    MyLinguas.AdTbLst(Lista, False, 'GraG1PrMo'  , '');            //   - Dados complementares do GraGru1: Produtos de Monitoramento ///
    // Fim Grade
    MyLinguas.AdTbLst(Lista, False, 'GraG1PrPA'  , '');            //   - Dados complementares do GraGru1: Princ�pios Ativos          ///
    MyLinguas.AdTbLst(Lista, False, 'G1PriAtiI'  , '_lst_grupos'); //   - Cadastro de Princ�pios Ativos                               ///
    MyLinguas.AdTbLst(Lista, False, 'G1PriAtiG'  , '_lst_grupos'); //   - Cadastro de Grupos de Princ�pios Ativos                     ///
{
    MyLinguas.AdTbLst(Lista, False, 'Praga_6', '_lst_grupos');  // ??
    MyLinguas.AdTbLst(Lista, False, 'Praga_7', '_lst_grupos');  // Filo
    MyLinguas.AdTbLst(Lista, False, 'Praga_8', '_lst_grupos');  // Classe
    MyLinguas.AdTbLst(Lista, False, 'Praga_9', '_lst_grupos');  // Ordem
    MyLinguas.AdTbLst(Lista, False, 'Praga_A', '_lst_grupos');  // Praga
    MyLinguas.AdTbLst(Lista, False, 'Praga_B', '_lst_grupos');  // Sub-ordem
    MyLinguas.AdTbLst(Lista, False, 'Praga_C', '_lst_grupos');  // Super Fam�lia
    MyLinguas.AdTbLst(Lista, False, 'Praga_D', '_lst_grupos');  // Fam�lia
    MyLinguas.AdTbLst(Lista, False, 'Praga_E', '_lst_grupos');  // Sub-fam�lia
    MyLinguas.AdTbLst(Lista, False, 'Praga_F', '_lst_grupos');  // G�nero
    MyLinguas.AdTbLst(Lista, False, 'Praga_Z', '_lst_grupos');  // ... Esp�cie
    //
}
    //
    MyLinguas.AdTbLst(Lista, False, 'PipCad', '');                  //   - PIP - Ponto de Iscagem Permanente
    MyLinguas.AdTbLst(Lista, False, 'PosVdaCab', '');               //   - Tipos de p�s venda
    MyLinguas.AdTbLst(Lista, False, 'PosVdaIts', '');               //   - Itens de tipos de p�s venda
    MyLinguas.AdTbLst(Lista, False, 'PrgBinCad', '_lst_sample');    //   - Cadastro de textos Sim/Nao
    MyLinguas.AdTbLst(Lista, False, 'PrgCadPrg'  , '');             //   - Cadastro de Perguntas
    MyLinguas.AdTbLst(Lista, False, 'PrgLstCab'  , '');             //   - Listas de Perguntas
    MyLinguas.AdTbLst(Lista, False, 'PrgLstIts'  , '');             //   - Itens das Listas de Perguntas
    MyLinguas.AdTbLst(Lista, False, 'PrgAtrCad', '_atr_cad');       //   - Cadastro do atributo
    MyLinguas.AdTbLst(Lista, False, 'PrgAtrIts', '_atr_its');       //   - Cadastro do valores para cada atributo
    //  Vai usar?
    MyLinguas.AdTbLst(Lista, False, 'PrgLstDef', '_atr_def');       //   - Defini��o dos atributos e valores
    //
    MyLinguas.AdTbLst(Lista, False, 'Praga_A', '_lst_grupos');      //   - Gupos de pragas
    MyLinguas.AdTbLst(Lista, False, 'Praga_Z', '');                 //   - Esp�cies de pragas
    MyLinguas.AdTbLst(Lista, False, 'Praga_ZTax', '');              //   - Taxonomia da esp�cie
    MyLinguas.AdTbLst(Lista, False, 'TaxonGru'  , '');              //   - Cadastro Cfe Taxonomia de Lineu
    MyLinguas.AdTbLst(Lista, False, 'TaxonCad'  , '');              //   - Cadastro Cfe Taxonomia de Lineu
    MyLinguas.AdTbLst(Lista, False, 'TaxonIts'  , '');              //   - Cadastro Cfe Taxonomia de Lineu
    //
    MyLinguas.AdTbLst(Lista, False, 'TaxonNet'  , '');              //   - Dados da Web sobre esp�cies

    Result := True;
  except
    raise;
    Result := False;
  end;
end;

{
function TBugs_Tabs.CarregaListaTabsWeb(Lista: TList<TTabelas>): Boolean;
begin
  try
    BugMbl_Tabs.CarregaListaTabelas(
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;
}

function TBugs_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Abrangicie') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('1|"RODAP�"');
      FListaSQL.Add('2|"CANTOS"');
      FListaSQL.Add('3|"RODAFORROS"');
      FListaSQL.Add('4|"PAREDE TODA"');
      FListaSQL.Add('5|"TODA �REA INTERNA"');
      FListaSQL.Add('6|"CAIXAS DE ESGOTO"');
      FListaSQL.Add('7|"CANOS DE PIA"');
      FListaSQL.Add('8|"VASOS SANIT�RIOS"');
      FListaSQL.Add('9|"RALOS"');
      FListaSQL.Add('10|"CAIXAS DE �GUAS DE CHUVA"');
      FListaSQL.Add('11|"FRESTAS"');
      FListaSQL.Add('12|"EQUIPAMENTOS"');
      FListaSQL.Add('13|"LOCAIS INFESTADOS PELA PRAGA"');
      FListaSQL.Add('14|"PRATELEIRAS, ARM�RIOS, MESAS, PIAS E BALC�ES"');
      FListaSQL.Add('15|"NINHOS E CARREIROS"');
      FListaSQL.Add('16|"EMBAIXO E ATR�S DE M�VEIS"');
      FListaSQL.Add('17|"L�MPADAS"');
      FListaSQL.Add('18|"TETO"');
      FListaSQL.Add('19|"MUROS"');
      FListaSQL.Add('20|"GRAMADOS"');
      FListaSQL.Add('21|"PER�METRO"');
      FListaSQL.Add('22|"APLICA��O ESPACIAL"');
      FListaSQL.Add('23|"DENTRO DO TELHADO"');
      FListaSQL.Add('24|"CH�O"');
      FListaSQL.Add('25|"MADEIRAMENTO A VISTA"');
      FListaSQL.Add('26|"PONTOS DE ISCAGEM PERMANENTE (PIP�S)"');
      FListaSQL.Add('27|"MESAS/M�QUINAS DE CAF�"');
      FListaSQL.Add('28|"FOSSAS"');
      FListaSQL.Add('29|"BOEIROS"');
      FListaSQL.Add('30|"ARMADILHAS LUMINOSAS"');
      FListaSQL.Add('31|"CONTORNO DE PORTAS"');
      FListaSQL.Add('32|"CONTORNO DE JANELAS"');
      FListaSQL.Add('33|"TAMPAS DE SANEAMENTO DA CAL�ADA"');
      FListaSQL.Add('34|"ARMADILHAS BOLA"');
      FListaSQL.Add('35|"ACESSO-PAREDE"');
    end else
    if Uppercase(Tabela) = Uppercase('AgeEqiCab') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('-1|"***A definir***"');
    end else
    if Uppercase(Tabela) = Uppercase('Atividades') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('1|"SOFTWARE HOUSE"');
      FListaSQL.Add('2|"RESIDENCIAL"');
      FListaSQL.Add('3|"RESTAURANTE INDUSTRIAL"');
      FListaSQL.Add('4|"ESCOLA"');
      FListaSQL.Add('5|"CONSULT�RIO"');
      FListaSQL.Add('6|"CONSTRUTORA"');
      FListaSQL.Add('7|"ALOJAMENTO"');
      FListaSQL.Add('8|"ADMINISTRATIVO"');
      FListaSQL.Add('9|"REFEIT�RIO"');
      FListaSQL.Add('10|"EXAMES"');
      FListaSQL.Add('11|"RESTAURANTE"');
      FListaSQL.Add('12|"SHOPPING"');
      FListaSQL.Add('13|"ADVOCACIA"');
      FListaSQL.Add('14|"VENDAS"');
      FListaSQL.Add('15|"SHOW ROOM"');
      FListaSQL.Add('16|"ILUMINA��O DE AMBIENTES"');
      FListaSQL.Add('17|"DEP�SITO/ESTOQUE"');
      FListaSQL.Add('18|"A�OUGUE"');
      FListaSQL.Add('19|"SERVI�OS DE HOSPEDAGEM"');
      FListaSQL.Add('20|"IGREJA"');
      FListaSQL.Add('21|"SUPERMERCADO"');
      FListaSQL.Add('22|"CEMIT�RIO"');
      FListaSQL.Add('23|"F�RUM"');
      FListaSQL.Add('24|"COM�RCIO"');
      FListaSQL.Add('25|"FARM�CIA"');
      FListaSQL.Add('26|"IMOBILI�RIA"');
      FListaSQL.Add('27|"ESCRIT�RIO"');
      FListaSQL.Add('28|"SERVI�OS"');
      FListaSQL.Add('29|"PRODU��O DE ARMA��ES DE FERRO"');
      FListaSQL.Add('30|"GARAGEM"');
      FListaSQL.Add('31|"�RG�O P�BLICO"');
      FListaSQL.Add('32|"IND�STRIA MET�LICA"');
      FListaSQL.Add('33|"IND�STRIA"');
      FListaSQL.Add('34|"DEP�SITO"');
      FListaSQL.Add('35|"DANCETERIA"');
      FListaSQL.Add('36|"TRANSPORTADORA"');
      FListaSQL.Add('37|"OFICINA"');
      FListaSQL.Add('38|"COM�RCIO DE ROUPAS"');
      FListaSQL.Add('39|"LOCA��O DE ROUPAS"');
      FListaSQL.Add('40|"ATELIER"');
      FListaSQL.Add('41|"PADARIA"');
      FListaSQL.Add('42|"CONDOM�NIO"');
      FListaSQL.Add('43|"SEGURADORA"');
      FListaSQL.Add('44|"BANCO"');
      FListaSQL.Add('45|"LAZER"');
      FListaSQL.Add('46|"CONSTRUTORA"');
      FListaSQL.Add('47|"CONDOM�NIO INDUSTRIAL"');
      FListaSQL.Add('48|"OBRA"');
      FListaSQL.Add('49|"LABORAT�RIO"');
      FListaSQL.Add('50|"BUFFET INFANTIL"');
      FListaSQL.Add('51|"LOCA��O DE EQUIPAMENTOS"');
      FListaSQL.Add('52|"IND�STRIA FARMAC�UTICA"');
      FListaSQL.Add('53|"SORVETERIA"');
      FListaSQL.Add('54|"AGRICULTURA"');
      FListaSQL.Add('55|"MANUTEN��O DE EQUIPAMENTOS M�DICO-HOSPITALARES"');
      FListaSQL.Add('56|"ABRIGO/PROTE��O DE EQUIPAMENTOS"');
      FListaSQL.Add('57|"ASSISTENCIAL"');
      FListaSQL.Add('58|"PRODU��O DE ALIMENTOS"');
      FListaSQL.Add('59|"DISTRIBUIDORA DE MEDICAMENTOS"');
      FListaSQL.Add('60|"GAMES INFANTIS"');
      FListaSQL.Add('61|"FECULARIA"');
      FListaSQL.Add('62|"POSTO DE GASOLINA"');
      FListaSQL.Add('63|"CRIA��O DE CAVALOS"');
      FListaSQL.Add('64|"CURTUME"');
      FListaSQL.Add('65|"CL�NICA"');
      FListaSQL.Add('66|"TRANSPORTE P�BLICO"');
      FListaSQL.Add('67|"HOSPITAL"');
      FListaSQL.Add('68|"PRODU��O"');
      FListaSQL.Add('69|"ATERRO SANIT�RIO"');
      FListaSQL.Add('70|"EDITORA"');
      FListaSQL.Add('71|"DISTRIBUIDORA DE PE�AS AUTOMOTIVAS"');
      FListaSQL.Add('72|"COM�RCIO AGROPECU�RIO"');
      FListaSQL.Add('73|"ARROZEIRA"');
      FListaSQL.Add('74|"FRIGOR�FICO"');
      FListaSQL.Add('75|"COM�RCIO DE PRODUTOS QU�MICOS"');
    end else
    if Uppercase(Tabela) = Uppercase('Caracteris') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('1|"FORRO DE PVC"');
      FListaSQL.Add('2|"DIVIS�RIAS EUCATEX"');
      FListaSQL.Add('3|"PALETES DE MADEIRA"');
      FListaSQL.Add('4|"MESAS COM ESTRUTURA DE MADEIRA"');
      FListaSQL.Add('5|"PRATELEIRAS DE METAL"');
      FListaSQL.Add('6|"FRESTAS EM M�VEIS"');
      FListaSQL.Add('7|"FRESTAS EM PAREDES"');
      FListaSQL.Add('8|"FRESTAS EM PORTAS"');
      FListaSQL.Add('9|"SIF�ES INADEQUADOS"');
      FListaSQL.Add('10|"TERRENO BALDIO"');
      FListaSQL.Add('11|"RODOVIA"');
      FListaSQL.Add('12|"FORRO DO TETO DE MANTA T�RMICA"');
      FListaSQL.Add('13|"CONSTRU��O PROVIS�RIA"');
      FListaSQL.Add('14|"EM CONSTRU��O"');
      FListaSQL.Add('15|"ACABAMENTO EM GESSO"');
      FListaSQL.Add('16|"MUROS COM TREPADEIRAS"');
      FListaSQL.Add('17|"JARDINS COM PLANTAS"');
      FListaSQL.Add('18|"GRAMADO"');
      FListaSQL.Add('19|"TETO REBAIXADO"');
      FListaSQL.Add('20|"ARVORES FRUT�FERAS"');
      FListaSQL.Add('21|"CONSTRU�DA"');
      FListaSQL.Add('22|"PAREDES COMPARTILHADAS OU GEMINADA"');
      FListaSQL.Add('23|"FORRO DE MADEIRA"');
      FListaSQL.Add('24|"FORRO DE ISOPOR"');
      FListaSQL.Add('25|"REVESTIMENTO DE MADEIRA/MDF"');
      FListaSQL.Add('26|"DIVIS�RIAS GESSO ACARTONADO"');
      FListaSQL.Add('27|"MATA OU FLORESTA"');
      FListaSQL.Add('28|"N�O VISTORIADO"');
      FListaSQL.Add('29|"M�VEIS EMBUTIDOS"');
      FListaSQL.Add('30|"PISO DE MADEIRA"');
      FListaSQL.Add('31|"PISO LAMINADO"');
      FListaSQL.Add('32|"CONT�INERS USADOS COMO DEPEND�NCIAS"');
      FListaSQL.Add('33|"ALVENARIA"');
      FListaSQL.Add('34|"PER�METRO CASCALHADO"');
      FListaSQL.Add('35|"SEM IDENTIFICA��O NA FRENTE DO IM�VEL"');
      FListaSQL.Add('36|"MADEIRAMENTO A VISTA"');
      FListaSQL.Add('37|"TERRENO VAZIO"');
      FListaSQL.Add('38|"CONSTRU��O DE MADEIRA"');
      FListaSQL.Add('39|"OFICINA MEC�NICA"');
      FListaSQL.Add('40|"AMBIENTE CLIMATIZADO"');
      FListaSQL.Add('41|"LINHA F�RREA / TREM"');
      FListaSQL.Add('42|"FRESTAS NO CH�O EXTERNO"');
      FListaSQL.Add('43|"LOCAL EM DESUSO"');
      FListaSQL.Add('44|"CASCA DE PINUS/EUCALIPTO NO JARDIM"');
      //FListaSQL.Add('45|"INAUGURADO 08/2012"');
      FListaSQL.Add('46|"REC�M CONSTRU�DO"');
      FListaSQL.Add('47|"SHOPPING - LOJAS"');
      //FListaSQL.Add('48|"VISTORIADO PELO GOOGLE STREET VIEW"');
      FListaSQL.Add('49|"C�RREGOS, ALAGADOS OU CURSOS D��GUA"');
      FListaSQL.Add('50|"PONTOS DE APLICA��O ACIMA DE 10 M DE ALTURA"');
      FListaSQL.Add('51|"BARRAC�O DE ZINCO"');
      FListaSQL.Add('52|"N�O CONSTRU�DO"');
      FListaSQL.Add('53|"PER�METRO CAL�ADO"');
      FListaSQL.Add('54|"LATERAIS ABERTAS"');
      FListaSQL.Add('55|"MADEIREIRA"');
      FListaSQL.Add('56|"SEM DIVIS�RIAS"');
      FListaSQL.Add('57|"PER�METRO COM TERRA"');
      FListaSQL.Add('58|"PISO SUSPENSO"');
      FListaSQL.Add('59|"TETO DE GESSO"');
      FListaSQL.Add('60|"PER�METRO GRAMADO"');
      FListaSQL.Add('61|"PLANTAS ORNAMENTAIS"');
      FListaSQL.Add('62|"�REA RURAL"');
      FListaSQL.Add('63|"LAVOURAS"');
      //FListaSQL.Add('64|"IDEM"');
      FListaSQL.Add('65|"CRIA��O DE GALINHAS"');
      FListaSQL.Add('66|"CRIA��O DE ANIMAIS DOM�STICOS"');
      FListaSQL.Add('67|"SUPERMERCADOS"');
      FListaSQL.Add('68|"TETO COM MADEIRAMENTO A VISTA"');
      FListaSQL.Add('69|"BARRC�ES INDUSTRIAIS"');
      FListaSQL.Add('70|"CEREALISTA"');
      FListaSQL.Add('71|"FINO ACABAMENTO"');
      FListaSQL.Add('72|"RESID�NCIAS"');
      FListaSQL.Add('73|"CONSTRU��O ANTIGA"');
      FListaSQL.Add('74|"CRIA��O DE GADO"');
      FListaSQL.Add('75|"MATERIAIS RECICL�VEIS"');
      FListaSQL.Add('76|"ESTOQUE DE PRODUTOS DE TROCA"');
      FListaSQL.Add('77|"CULTIVOS"');
    end else
    if Uppercase(Tabela) = Uppercase('CxaFormas') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('1|"CIRCULAR"');
      FListaSQL.Add('2|"QUADRADA"');
      FListaSQL.Add('3|"TORRE CILINDRICA"');
      FListaSQL.Add('4|"TORRE QUADRADA"');
      FListaSQL.Add('5|"TA�A"');
    end else
    if Uppercase(Tabela) = Uppercase('CxaMaters') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('1|"PVC"');
      FListaSQL.Add('2|"FIBRA DE VIDRO"');
      FListaSQL.Add('3|"AMIANTO"');
      FListaSQL.Add('4|"CONCRETO"');
      FListaSQL.Add('5|"METAL"');
    end else
    if Uppercase(Tabela) = Uppercase('DependType') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"."');
      FListaSQL.Add('1|"�REA INTERNA"');
      FListaSQL.Add('2|"�REA EXTERNA"');
      FListaSQL.Add('3|"ESGOTO"');
      FListaSQL.Add('4|"�REA COMUM"');
    end else
    if Uppercase(Tabela) = Uppercase('DesServico') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('1|"DESINSETIZA��O"');
      FListaSQL.Add('2|"DESRATIZA��O"');
      FListaSQL.Add('3|"DESCUPINIZA��O"');
    end else
    if Uppercase(Tabela) = Uppercase('EstatusOSs') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0000_ABERTURA) + '|"Abertura de OS"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0100_PRE_ATEN) + '|"Pr�-atendimento"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0200_VISTORIA) + '|"Vistoria"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0300_EM_ORCAM) + '|"Em or�amento"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0400_ORCA_PAS) + '|"Or�amento passado"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0450_ORCA_OK_) + '|"Or�amento aprovado"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0500_AGEN_EXE) + '|"Agendado a execu��o"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0550_AGEN_OK_) + '|"Confirmado Agendamento"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0600_EM_EXECU) + '|"Em execu��o "');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0700_EXE_FINA) + '|"Execu��o finalizada"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0750_CEX_ENVI) + '|"Comprovante Enviado"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0800_FATURADO) + '|"Faturado"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_1000_POS_VEND) + '|"P�s venda"');
      FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_2000_O_S_BAIX) + '|"OS baixada"');
    end else
    if Uppercase(Tabela) = Uppercase('FatoGeradr') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome|NeedOS_Ori');
      //FListaSQL.Add('10|"Contratual"|0'); // Registro deprecado!
      FListaSQL.Add(Geral.FF0(CO_COD_FatoGeradr_1aAcao    ) + '|"1� a��o"|0');
      FListaSQL.Add(Geral.FF0(CO_COD_FatoGeradr_Corretiva ) + '|"Corretiva"|1');
      FListaSQL.Add(Geral.FF0(CO_COD_FatoGeradr_Preventiva) + '|"Preventiva"|1');
      FListaSQL.Add(Geral.FF0(CO_COD_FatoGeradr_Preditiva ) + '|"Preditiva"|1');
    end else
    if Uppercase(Tabela) = Uppercase('Finalidads') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('1|"RESIDENCIAL"');
      FListaSQL.Add('2|"ESCRIT�RIO"');
      FListaSQL.Add('3|"COZINHA INDUSTRIAL"');
      FListaSQL.Add('4|"INDEFINIDO"');
      FListaSQL.Add('5|"ALOJAMENTO/ADMINISTRATIVO"');
      FListaSQL.Add('6|"CONSULT�RIO"');
      FListaSQL.Add('7|"RESTAURANTE"');
      FListaSQL.Add('8|"ADMINISTRATIVO"');
      FListaSQL.Add('9|"LOJA"');
      FListaSQL.Add('10|"DEP�SITO"');
      FListaSQL.Add('11|"LAZER"');
      FListaSQL.Add('12|"GARAGEM"');
      FListaSQL.Add('13|"SUPERMERCADO"');
      FListaSQL.Add('14|"CEMIT�RIO"');
      FListaSQL.Add('15|"PRODU��O"');
      FListaSQL.Add('16|"�REA LIVRE"');
      FListaSQL.Add('17|"ESCOLA"');
      FListaSQL.Add('18|"SHOW ROOM"');
      FListaSQL.Add('19|"LABORAT�RIO"');
      FListaSQL.Add('20|"COM�RCIO DE MEDICAMENTOS"');
      FListaSQL.Add('21|"DESATIVADO"');
      FListaSQL.Add('22|"VESTI�RIO/DESCANSO"');
      FListaSQL.Add('23|"COM�RCIO DE COLCH�ES"');
      FListaSQL.Add('24|"COM�RCIO DE MATERIAIS M�DICO-HOSPITALARES"');
      FListaSQL.Add('25|"COM�RCIO DE COMBUST�VEIS"');
      FListaSQL.Add('26|"DANCETERIA/CASA DE SHOWS"');
      FListaSQL.Add('27|"OFICINA"');
      FListaSQL.Add('28|"ESTOQUE"');
      FListaSQL.Add('29|"INDUSTRIAL"');
      FListaSQL.Add('30|"PADARIA"');
      FListaSQL.Add('31|"CONDOM�NIO"');
      FListaSQL.Add('32|"BANCO"');
      FListaSQL.Add('33|"SERVI�OS DE COSTURA"');
      FListaSQL.Add('34|"GIN�SIO"');
      FListaSQL.Add('35|"�REA DE LOJAS"');
      FListaSQL.Add('36|"COBERTURA"');
      FListaSQL.Add('37|"LOCA��O DE EQUIPAMENTOS"');
      FListaSQL.Add('38|"CHOPPERIA"');
      FListaSQL.Add('39|"SORVETERIA"');
      FListaSQL.Add('40|"ABRIGO/PROTE��O DE EQUIPAMENTOS"');
      FListaSQL.Add('41|"DOCERIA"');
      FListaSQL.Add('42|"ALIMENT�CIO"');
      FListaSQL.Add('43|"TRASNPORTADORA"');
      FListaSQL.Add('44|"REFEIT�RIO"');
      FListaSQL.Add('45|"COM�RCIO"');
      FListaSQL.Add('46|"SERVI�OS EM INFORM�TICA"');
      FListaSQL.Add('47|"ALOJAMENTO"');
      FListaSQL.Add('48|"HOSPITAL"');
      FListaSQL.Add('49|"HOTEL/HOSPEDAGEM"');
      FListaSQL.Add('50|"ACESSO"');
      FListaSQL.Add('51|"ARMAZENAGEM"');
      FListaSQL.Add('52|"CONSTRU��O CIVIL"');
      FListaSQL.Add('53|"IMOBILI�RIA"');
    end else
    if Uppercase(Tabela) = Uppercase('Formulas') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|EquipAplic|Aplicacao|Diluente|TipoAplica|UnidMed|QtdTot|Nome');
      FListaSQL.Add('1|004|001|002|005|003|8.000|"PULVERIZA��O 1"');
      FListaSQL.Add('2|005|001|001|004|006|50.000|"POLVILHAMENTO REDE DE ESGOTO"');
      FListaSQL.Add('3|006|001|001|003|006|10.000|"APLICA��O DE GEL - BARATAS"');
    end else
    if Uppercase(Tabela) = Uppercase('FormulIF') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Controle|Codigo|GraGruX|PrvQtd|Ordem|Reordem');
      FListaSQL.Add('1|1|001|10.000|001|001');
      FListaSQL.Add('2|2|002|50.000|001|001');
      FListaSQL.Add('3|3|003|10.000|001|001');
    end else
    if Uppercase(Tabela) = Uppercase('FormulIA') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Controle|Codigo|Abrangicie');
      FListaSQL.Add('1|001|002');
      FListaSQL.Add('2|001|001');
      FListaSQL.Add('3|002|010');
      FListaSQL.Add('4|002|006');
      FListaSQL.Add('5|002|007');
      FListaSQL.Add('6|002|033');
      FListaSQL.Add('7|002|008');
      FListaSQL.Add('8|003|012');
      FListaSQL.Add('9|003|011');
      FListaSQL.Add('10|003|013');
      FListaSQL.Add('11|003|014');
    end else
    if Uppercase(Tabela) = Uppercase('PrdGrupTip') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|CodUsu|Nome|MadeBy|Fracio|Gradeado|TipPrd|NivCad|FaixaIni|FaixaFim|TitNiv1|TitNiv2|ImpedeCad');
      FListaSQL.Add('1|1|"???"|0|0|0|1|1|0|0|""|""|0');
    end else
    if Uppercase(Tabela) = Uppercase('GraGru1') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('CodUsu|Nivel1|Nivel2|Nivel3|Nivel4|Nivel5|Nome|PrdGrupTip|GraTamCad|GerBxaEstq');
      //FListaSQL.Add('Nivel1|CodUsu|PrdGrupTip|GraTamCad|UnidMed');
      FListaSQL.Add('1|001|0|0|0|0|"DELTAGARD"|1|1|1');
      FListaSQL.Add('2|002|0|0|0|0|"K-OTHRINE 2P"|1|1|1');
      FListaSQL.Add('3|003|0|0|0|0|"MAX FORCE"|1|1|1');
      FListaSQL.Add('4|004|0|0|0|0|"PULVERIZADOR INOX"|1|1|1');
      FListaSQL.Add('5|005|0|0|0|0|"POLVILHADEIRA"|1|1|1');
      FListaSQL.Add('6|006|0|0|0|0|"PISTOLA APLICADORA DE GEL"|1|1|1');
    end else
    if Uppercase(Tabela) = Uppercase('GraGruC') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Controle|Nivel1|GraCorCad');
      FListaSQL.Add('1|001|001');
      FListaSQL.Add('2|002|001');
      FListaSQL.Add('3|003|001');
      FListaSQL.Add('4|004|001');
      FListaSQL.Add('5|005|001');
      FListaSQL.Add('6|006|001');
    end else
    if Uppercase(Tabela) = Uppercase('GraGruX') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Controle|GraGruC|GraGru1|GraTamI');
      FListaSQL.Add('1|001|001|001');
      FListaSQL.Add('2|002|002|001');
      FListaSQL.Add('3|003|003|001');
      FListaSQL.Add('4|004|004|001');
      FListaSQL.Add('5|005|005|001');
      FListaSQL.Add('6|006|006|001');
    end else
    if Uppercase(Tabela) = Uppercase('GraTabApp') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Tabela|Nome');
      FListaSQL.Add(Geral.FF0(CO_COD_GraTabApp_NenhumGTA) + '|"NenhumGTA"|"Cadastro sem v�nculo"');
      FListaSQL.Add(Geral.FF0(CO_COD_GraTabApp_GraG1EqAp) + '|"GraG1EqAp"|"Equipamentos de Aplica��o"');
      FListaSQL.Add(Geral.FF0(CO_COD_GraTabApp_GraG1EqMo) + '|"GraG1EqMo"|"Equipamentos de Monitoramento"');
      FListaSQL.Add(Geral.FF0(CO_COD_GraTabApp_GraG1PrAp) + '|"GraG1PrAp"|"Produtos de Aplica��o"');
      FListaSQL.Add(Geral.FF0(CO_COD_GraTabApp_GraG1PrMo) + '|"GraG1PrMo"|"Produtos de Monitoramento"');
    end else
    if Uppercase(Tabela) = Uppercase('MovAmovCad') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"."');
    end else
    if Uppercase(Tabela) = Uppercase('OpcoesBugs') then
    begin
      FListaSQL.Add('Codigo');
      FListaSQL.Add('1');
    end else
{
    if Uppercase(Tabela) = Uppercase('OpcoesFili') then
    begin
      FListaSQL.Add('Codigo');
      FListaSQL.Add('-11');
    end else
}
    if Uppercase(Tabela) = Uppercase('TaxonGru') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Ordem|Nome');
      //http://pt.wikipedia.org/wiki/Taxonomia_de_Lineu#Esquema_Taxon.C3.B4mico
      FListaSQL.Add('1|1|"Reino"');
      FListaSQL.Add('2|2|"Filo"');
      FListaSQL.Add('3|3|"Classe"');
      FListaSQL.Add('4|4|"Ordem"');
      FListaSQL.Add('5|5|"Fam�lia"');
      FListaSQL.Add('6|6|"Genero"');
      FListaSQL.Add('7|7|"Esp�cie"');
    end else
    if Uppercase(Tabela) = Uppercase('TaxonCad') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Ordem|TaxonGru|Nome');
      FListaSQL.Add('1|01|1|"Imp�rio"');
      FListaSQL.Add('2|02|1|"Dom�nio"');
      FListaSQL.Add('3|03|1|"Reino"');
      FListaSQL.Add('4|04|2|"Filo"');
      FListaSQL.Add('5|05|3|"Coorte bot�nica"');
      FListaSQL.Add('6|06|3|"Classe"');
      FListaSQL.Add('7|07|3|"Divis�o"');
      FListaSQL.Add('8|08|3|"Legiao"');
      FListaSQL.Add('9|09|3|"Coorte zool�gica"');
      FListaSQL.Add('10|10|4|"Ordem"');
      FListaSQL.Add('11|11|5|"Se��o"');
      FListaSQL.Add('12|12|5|"Fam�lia"');
      FListaSQL.Add('13|13|5|"Tribo"');
      FListaSQL.Add('14|14|6|"Genero"');
      FListaSQL.Add('15|15|7|"Esp�cie"');
    end else
    if Uppercase(Tabela) = Uppercase('TaxonIts') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|TaxonGru|TaxonCad|Ordem|Nome');
      FListaSQL.Add(CO_COD_TAXON_ITS_1+'|1|01|01|"Imp�rio"');  // http://pt.wikipedia.org/wiki/Taxonomia_de_Lineu#Esquema_Taxon.C3.B4mico
      FListaSQL.Add(CO_COD_TAXON_ITS_2+'|1|02|02|"Superdom�nio"');
      FListaSQL.Add(CO_COD_TAXON_ITS_3+'|1|02|03|"Dom�nio"');
      FListaSQL.Add(CO_COD_TAXON_ITS_4+'|1|03|04|"Reino"');
      FListaSQL.Add(CO_COD_TAXON_ITS_5+'|1|03|05|"Subreino"');
      FListaSQL.Add(CO_COD_TAXON_ITS_6+'|1|03|06|"Ramo"');
      FListaSQL.Add(CO_COD_TAXON_ITS_7+'|1|03|07|"Infrareino"');
      FListaSQL.Add(CO_COD_TAXON_ITS_8+'|2|04|08|"Superfilo (ou Superdivis�o na bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_9+'|2|04|09|"Filo (ou Divis�o na bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_10+'|2|04|10|"Subfilo (ou Subdivis�o na bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_11+'|2|04|11|"Infrafilo (ou Infradivis�o na bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_12+'|2|04|12|"Microfilo"');
      FListaSQL.Add(CO_COD_TAXON_ITS_13+'|3|05|13|"Supercoorte (bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_14+'|3|05|14|"Coorte (bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_15+'|3|05|15|"Subcoorte (bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_16+'|3|05|16|"Infracoorte ( bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_17+'|3|06|17|"Superclasse"');
      FListaSQL.Add(CO_COD_TAXON_ITS_18+'|3|06|18|"Classe"');
      FListaSQL.Add(CO_COD_TAXON_ITS_19+'|3|06|19|"Subclasse"');
      FListaSQL.Add(CO_COD_TAXON_ITS_20+'|3|06|20|"Infraclasse"');
      FListaSQL.Add(CO_COD_TAXON_ITS_21+'|3|06|21|"Parvclasse"');
      FListaSQL.Add(CO_COD_TAXON_ITS_22+'|3|07|22|"Superdivis�o (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_23+'|3|07|23|"Divis�o (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_24+'|3|07|24|"Subdivis�o (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_25+'|3|07|25|"Infradivis�o (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_26+'|3|08|26|"Superlegi�o (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_27+'|3|08|27|"Legi�o (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_28+'|3|08|28|"Sublegi�o (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_29+'|3|08|29|"Infralegi�o (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_30+'|3|09|30|"Supercoorte (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_31+'|3|09|31|"Coorte (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_32+'|3|09|32|"Subcoorte (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_33+'|3|09|33|"Infracoorte (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_34+'|4|10|34|"Gigaordem (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_35+'|4|10|35|"Magnordem ou Megaordem (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_36+'|4|10|36|"Gradordem ou Capaxordem (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_37+'|4|10|37|"Mirorder ou Hiperordem (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_38+'|4|10|38|"Superordem"');
      FListaSQL.Add(CO_COD_TAXON_ITS_39+'|4|10|39|"S�rie (para peixes)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_40+'|4|10|40|"Ordem"');
      FListaSQL.Add(CO_COD_TAXON_ITS_41+'|4|10|41|"Parvordem (posi��o em algumas classifica��es zool�gicas)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_42+'|4|10|42|"Nanordem (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_43+'|4|10|43|"Hipoordem (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_44+'|4|10|44|"Minordem (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_45+'|4|10|45|"Subordem"');
      FListaSQL.Add(CO_COD_TAXON_ITS_46+'|4|10|46|"Infra-ordem"');
      FListaSQL.Add(CO_COD_TAXON_ITS_47+'|4|10|47|"Parvordem (posi��o usual) ou Microordem (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_48+'|5|11|48|"Se��o (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_49+'|5|11|49|"Subse��o (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_50+'|5|12|50|"Gigafam�lia (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_51+'|5|12|51|"Megafam�lia (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_52+'|5|12|52|"Gradfam�lia (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_53+'|5|12|53|"Hiperfam�lia (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_54+'|5|12|54|"Superfam�lia"');
      FListaSQL.Add(CO_COD_TAXON_ITS_55+'|5|12|55|"Epifam�lia (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_56+'|5|12|56|"S�ries (para os Lepidoptera)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_57+'|5|12|57|"Grupo (para os Lepidoptera)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_58+'|5|12|58|"Fam�lia"');
      FListaSQL.Add(CO_COD_TAXON_ITS_59+'|5|12|59|"Subfam�lia"');
      FListaSQL.Add(CO_COD_TAXON_ITS_60+'|5|12|60|"Infrafam�lia"');
      FListaSQL.Add(CO_COD_TAXON_ITS_61+'|5|13|61|"Supertribo"');
      FListaSQL.Add(CO_COD_TAXON_ITS_62+'|5|13|62|"Tribo"');
      FListaSQL.Add(CO_COD_TAXON_ITS_63+'|5|13|63|"Subtribo"');
      FListaSQL.Add(CO_COD_TAXON_ITS_64+'|5|13|64|"Infratribo"');
      FListaSQL.Add(CO_COD_TAXON_ITS_65+'|6|14|65|"G�nero"');
      FListaSQL.Add(CO_COD_TAXON_ITS_66+'|6|14|66|"Subg�nero"');
      FListaSQL.Add(CO_COD_TAXON_ITS_67+'|6|14|67|"Se��o (bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_68+'|6|14|68|"Subse��o (bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_69+'|6|14|69|"S�rie (bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_70+'|6|14|70|"Subs�rie (bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_71+'|7|15|71|"Superesp�cie"');
      FListaSQL.Add(CO_COD_TAXON_ITS_72+'|7|15|72|"Esp�cie"');
      FListaSQL.Add(CO_COD_TAXON_ITS_73+'|7|15|73|"Subesp�cie (ou Forma Especial para Fungos, ou Variedade para Bact�rias)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_74+'|7|15|74|"Variedade (Bot�nica) ou Forma/Morf (Zoologia)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_75+'|7|15|75|"Subvariedade (Bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_76+'|7|15|76|"Forma (Bot�nica)"');
      FListaSQL.Add(CO_COD_TAXON_ITS_77+'|7|15|77|"Subforma (Bot�nica)"');
    end else
    if Uppercase(Tabela) = Uppercase('Praga_A') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('-1|"."');
      FListaSQL.Add( '0|"[Outros]"');
      FListaSQL.Add('1|"Abelhas"');
      FListaSQL.Add('2|"�caros"');
      FListaSQL.Add('3|"Algas"');
      FListaSQL.Add('4|"Aranhas"');
      FListaSQL.Add('5|"Bact�rias"');
      FListaSQL.Add('6|"Barbeiros"');
      FListaSQL.Add('7|"Besouros"');
      FListaSQL.Add('8|"Baratas"');
      FListaSQL.Add('9|"Borboletas"');
      FListaSQL.Add('10|"Borrachudos"');
      FListaSQL.Add('11|"Brocas"');
      FListaSQL.Add('12|"Camundongos"');
      FListaSQL.Add('13|"Caramujos"');
      FListaSQL.Add('14|"Carrapatos"');
      FListaSQL.Add('15|"Carunchos"');
      FListaSQL.Add('16|"Centopeias"');
      FListaSQL.Add('17|"Cigarras"');
      FListaSQL.Add('18|"Cigarrinhas"');
      FListaSQL.Add('19|"Cochonilhas"');
      FListaSQL.Add('20|"Cupins"');
      FListaSQL.Add('21|"Escorpi�es"');
      FListaSQL.Add('22|"Esperan�as"');
      FListaSQL.Add('23|"Formigas"');
      FListaSQL.Add('24|"Fungos"');
      FListaSQL.Add('25|"Gafanhotos"');
      FListaSQL.Add('26|"Grilos"');
      FListaSQL.Add('27|"Gongolos (Piolhos-de-cobra)"');
      FListaSQL.Add('28|"Joaninhas"');
      FListaSQL.Add('29|"Grilos"');
      FListaSQL.Add('30|"Lagartas (Taturanas)"');
      FListaSQL.Add('31|"Lacraias"');
      FListaSQL.Add('32|"Lesmas"');
      FListaSQL.Add('33|"Mariposas"');
      FListaSQL.Add('34|"Morcegos"');
      FListaSQL.Add('35|"Moscas"');
      FListaSQL.Add('36|"Mosquitos"');
      FListaSQL.Add('37|"Mutucas"');
      FListaSQL.Add('38|"Percevejos"');
      FListaSQL.Add('39|"Pernilongos"');
      FListaSQL.Add('40|"Piolhos"');
      FListaSQL.Add('41|"Pombos"');
      FListaSQL.Add('42|"Protozo�rios"');
      FListaSQL.Add('43|"Pulgas"');
      FListaSQL.Add('44|"Pulg�es"');
      FListaSQL.Add('45|"Ratos"');
      FListaSQL.Add('46|"Serpentes (Cobras)"');
      FListaSQL.Add('47|"Tatuzinhos-de-jardim"');
      FListaSQL.Add('48|"Tra�as"');
      FListaSQL.Add('49|"Tripes"');
      FListaSQL.Add('50|"Vespas"');
    end else
    if Uppercase(Tabela) = Uppercase('Praga_Z') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|NivSup|Ordem|Especie|Nome');
      FListaSQL.Add('1|045|001|"RATTUS RATTUS"|"RATO PRETO"');
      FListaSQL.Add('2|008|002|"PERIPLANETA AMERICANA (LINNAEUS)"|"BARATA DE ESGOTO"');
      FListaSQL.Add('3|048|003|"LEPISMA SACCHARINA LINNAEUS"|"TRA�A"');
      FListaSQL.Add('4|023|004|"TAPINOMA MELANOCEPHALUM (FABRICIUS)"|"FORMIGA DOCEIRA"');
      FListaSQL.Add('5|004|005|"LYCOSA GODEFFROYI L. KOCH"|"ARANHA DE JARDIM"');
      FListaSQL.Add('6|008|006|"BLATTELLA GERMANICA (LINNAEUS)"|"BARATA ALEM�"');
      FListaSQL.Add('7|023|007|"CAMPONOTUS SPP."|"FORMIGA CARPINTEIRA"');
      FListaSQL.Add('8|004|008|"PHONEUTRIA SPP"|"ARANHA ARMADEIRA"');
      FListaSQL.Add('9|038|009|"CIMEX LECTULARIUS LINNAEUS"|"BED BUG - PERCEVEJO DE CAMA"');
      FListaSQL.Add('10|014|010|"RHIPICEPHALUS SANGUINEUS (LATREILLE)"|"CARRAPATO DE CACHORRO"');
      FListaSQL.Add('11|014|011|"AMBLYOMMA CAJENNENSE"|"CARRAPATO ESTRELA"');
      FListaSQL.Add('12|020|012|"CRYPTOTERMES BREVIS (WALKER)"|"CUPIM DE MADEIRA SECA"');
      FListaSQL.Add('13|020|013|"NASUTITERMES SPP"|"CUPIM ARBOR�COLA"');
      FListaSQL.Add('14|011|014|"LYCTUS BRUNNEUS (STEPHENS)"|"BROCA - LYCTIDAE"');
      FListaSQL.Add('15|011|015|"ANOBIUM PUNCTATUM (DE GEER)"|"BROCA - ANOBIIDAE"');
      FListaSQL.Add('16|011|016|"DINODERUS MINUTUS (FABRICIUS)"|"BROCA - BOSTRICHIDAE"');
      FListaSQL.Add('17|034|017|"DIVERSAS"|"MORCEGOS"');
      FListaSQL.Add('18|035|018|"MUSCA DOMESTICA"|"MOSCA"');
      FListaSQL.Add('19|039|019|"CULEX QUINQUEFASCIATUS SAY"|"PERNILONGO"');
      FListaSQL.Add('20|023|020|"Atta spp."|"FORMIGA CORTADEIRA"');
      FListaSQL.Add('21|021|021|"TITYUS SERRULATUS"|"ESCORPI�O"');
      FListaSQL.Add('22|007|022|"ANTHRENUS VERBASCI (LINNAEUS)"|"BESOURO DO TAPETE"');
      FListaSQL.Add('23|033|023|"BRASSOLIS SOPHORAE"|"LAGARTA DO COQUEIRO"');
      FListaSQL.Add('24|004|024|"PHOLCUS PHALANGIOIDES (FUESSLIN)"|"ARANHA ''PERNUDA''"');
      FListaSQL.Add('25|045|025|"RATTUS NORVEGICUS"|"RATAZANA"');
      FListaSQL.Add('26|045|026|"MUS MUSCULUS"|"CAMUNDONGO"');
      FListaSQL.Add('27|043|027|"CTENOCEPHALIDES CANIS (CURTIS)"|"PULGA"');
      FListaSQL.Add('28|031|028|"SCOLOPENDRA MORSITANS LINNAEUS"|"LACRAIA/CENTOP�IA"');
      FListaSQL.Add('29|027|029|"OMMATOIULUS MORELETI (LUCAS)"|"PIOLHO DE COBRA"');
      FListaSQL.Add('30|023|030|"PARATRECHINA LONGICORNIS (LATREILLE)"|"FORMIGA LOUCA"');
      FListaSQL.Add('31|040|031|"ORNITHONYSSUS SPP."|"PIOLHO DE POMBO"');
      FListaSQL.Add('32|050|032|"ROPALIDIA SPP."|"VESPAS"');
      FListaSQL.Add('33|015|033|"SITOPHILUS ZEAMAIS MOTSCHULSKY"|"CARUNCHO"');
      FListaSQL.Add('34|033|034|"DIVERSAS"|"MARIPOSAS"');
      FListaSQL.Add('35|023|035|"Solenopsis invicta Buren"|"FORMIGA LAVA-P�S"');
      FListaSQL.Add('36|000|036|"?"|"?"');
      FListaSQL.Add('37|001|037|"Apis mellifera Linnaeus"|"ABELHA"');
      FListaSQL.Add('38|000|038|"?"|"EXIG�NCIA VISA"');
    end else
    if Uppercase(Tabela) = Uppercase('Praga_ZTax') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|TaxonIts|Nome');
      FListaSQL.Add('2|9|"ARTHROPODA"');
      FListaSQL.Add('2|18|"HEXAPODA"');
      FListaSQL.Add('2|40|"Blattodea"');
      FListaSQL.Add('2|58|"Blattidae"');
      FListaSQL.Add('3|09|"ARTHROPODA"');
      FListaSQL.Add('3|18|"HEXAPODA"');
      FListaSQL.Add('3|40|"Thysanura"');
      FListaSQL.Add('3|58|"Lepismatidae"');
      FListaSQL.Add('4|9|"ARTHROPODA"');
      FListaSQL.Add('4|18|"HEXAPODA"');
      FListaSQL.Add('4|40|"Hymenoptera"');
      FListaSQL.Add('4|58|"Formicidae"');
      FListaSQL.Add('5|09|"ARTHROPODA"');
      FListaSQL.Add('5|18|"ARACHNIDA"');
      FListaSQL.Add('5|40|"Araneida"');
      FListaSQL.Add('5|58|"Lycosidae"');
      FListaSQL.Add('6|09|"ARTHROPODA"');
      FListaSQL.Add('6|18|"HEXAPODA"');
      FListaSQL.Add('6|40|"Blattodea"');
      FListaSQL.Add('6|58|"Blattellidae"');
      FListaSQL.Add('7|09|"ARTHROPODA"');
      FListaSQL.Add('7|18|"HEXAPODA"');
      FListaSQL.Add('7|40|"Hymenoptera"');
      FListaSQL.Add('7|58|"Formicidae"');
      FListaSQL.Add('8|09|"ARTHROPODA"');
      FListaSQL.Add('8|18|"ARACHNIDA"');
      FListaSQL.Add('8|40|"Araneida"');
      FListaSQL.Add('8|58|"Lycosidae"');
      FListaSQL.Add('9|09|"ARTHROPODA"');
      FListaSQL.Add('9|18|"HEXAPODA"');
      FListaSQL.Add('9|40|"Hemiptera"');
      FListaSQL.Add('9|58|"Cimicidae"');
      FListaSQL.Add('10|9|"ARTHROPODA"');
      FListaSQL.Add('10|18|"ARACHNIDA"');
      FListaSQL.Add('10|40|"Acarina"');
      FListaSQL.Add('10|58|"Ixodidae"');
      FListaSQL.Add('11|09|"ARTHROPODA"');
      FListaSQL.Add('11|18|"ARACHNIDA"');
      FListaSQL.Add('11|40|"Acarina"');
      FListaSQL.Add('11|58|"Ixodidae"');
      FListaSQL.Add('12|09|"ARTHROPODA"');
      FListaSQL.Add('12|18|"HEXAPODA"');
      FListaSQL.Add('12|40|"Isoptera"');
      FListaSQL.Add('12|58|"Kalotermitidae"');
      FListaSQL.Add('13|9|"ARTHROPODA"');
      FListaSQL.Add('13|18|"HEXAPODA"');
      FListaSQL.Add('13|40|"Isoptera"');
      FListaSQL.Add('13|58|"Termitidae"');
      FListaSQL.Add('14|09|"ARTHROPODA"');
      FListaSQL.Add('14|18|"HEXAPODA"');
      FListaSQL.Add('14|40|"Coleoptera"');
      FListaSQL.Add('14|58|"Bostrichidae"');
      FListaSQL.Add('15|09|"ARTHROPODA"');
      FListaSQL.Add('15|18|"HEXAPODA"');
      FListaSQL.Add('15|40|"Coleoptera"');
      FListaSQL.Add('15|58|"Anobiidae"');
      FListaSQL.Add('16|9|"ARTHROPODA"');
      FListaSQL.Add('16|18|"HEXAPODA"');
      FListaSQL.Add('16|40|"Coleoptera"');
      FListaSQL.Add('16|58|"Bostrichidae"');
      FListaSQL.Add('18|09|"ARTHROPODA"');
      FListaSQL.Add('18|18|"HEXAPODA"');
      FListaSQL.Add('18|40|"Diptera"');
      FListaSQL.Add('18|58|"Muscidae"');
      FListaSQL.Add('19|9|"ARTHROPODA"');
      FListaSQL.Add('19|18|"HEXAPODA"');
      FListaSQL.Add('19|40|"Diptera"');
      FListaSQL.Add('19|58|"Culicidae"');
      FListaSQL.Add('21|9|"ARTHROPODA"');
      FListaSQL.Add('21|18|"HEXAPODA"');
      FListaSQL.Add('21|40|"Hemiptera"');
      FListaSQL.Add('21|58|"Miridae"');
      FListaSQL.Add('22|9|"ARTHROPODA"');
      FListaSQL.Add('22|18|"HEXAPODA"');
      FListaSQL.Add('22|40|"Coleoptera"');
      FListaSQL.Add('22|58|"Dermestidae"');
      FListaSQL.Add('24|9|"ARTHROPODA"');
      FListaSQL.Add('24|18|"ARACHNIDA"');
      FListaSQL.Add('24|40|"Araneida"');
      FListaSQL.Add('24|58|"Pholcidae"');
      FListaSQL.Add('27|9|"ARTHROPODA"');
      FListaSQL.Add('27|18|"HEXAPODA"');
      FListaSQL.Add('27|40|"Siphonaptera"');
      FListaSQL.Add('27|58|"Pulicidae"');
      FListaSQL.Add('28|9|"ARTHROPODA"');
      FListaSQL.Add('28|18|"CHILOPODA"');
      FListaSQL.Add('28|40|"Scolopendromorpha"');
      FListaSQL.Add('28|58|"Scolopendridae"');
      FListaSQL.Add('29|9|"ARTHROPODA"');
      FListaSQL.Add('29|18|"DIPLOPODA"');
      FListaSQL.Add('29|40|"Julida"');
      FListaSQL.Add('29|58|"Julidae"');
      FListaSQL.Add('30|9|"ARTHROPODA"');
      FListaSQL.Add('30|18|"HEXAPODA"');
      FListaSQL.Add('30|40|"Hymenoptera"');
      FListaSQL.Add('30|58|"Formicidae"');
      FListaSQL.Add('31|9|"ARTHROPODA"');
      FListaSQL.Add('31|18|"ARACHNIDA"');
      FListaSQL.Add('31|40|"Acarina"');
      FListaSQL.Add('31|58|"Macronyssidae"');
      FListaSQL.Add('32|9|"ARTHROPODA"');
      FListaSQL.Add('32|18|"HEXAPODA"');
      FListaSQL.Add('32|40|"Hymenoptera"');
      FListaSQL.Add('32|58|"Vespidae"');
      FListaSQL.Add('33|9|"ARTHROPODA"');
      FListaSQL.Add('33|18|"HEXAPODA"');
      FListaSQL.Add('33|40|"Coleoptera"');
      FListaSQL.Add('33|58|"Curculionidae"');
      FListaSQL.Add('34|9|"ARTHROPODA"');
      FListaSQL.Add('34|18|"HEXAPODA"');
      FListaSQL.Add('34|40|"Lepidoptera"');
      FListaSQL.Add('34|58|"Agonoxenidae"');
      FListaSQL.Add('35|9|"ARTHROPODA"');
      FListaSQL.Add('35|18|"HEXAPODA"');
      FListaSQL.Add('35|40|"Hymenoptera"');
      FListaSQL.Add('35|58|"Formicidae"');
      FListaSQL.Add('37|9|"ARTHROPODA"');
      FListaSQL.Add('37|18|"HEXAPODA"');
      FListaSQL.Add('37|40|"Hymenoptera"');
      FListaSQL.Add('37|58|"Apidae"');
    end else
    if Uppercase(Tabela) = Uppercase('TipoAplica') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('1|"ATOMIZA��O"');
      FListaSQL.Add('2|"TERMONEBULIZA��O"');
      FListaSQL.Add('3|"ISCAGEM"');
      FListaSQL.Add('4|"POLVILHAMENTO"');
      FListaSQL.Add('5|"PULVERIZA��O"');
      FListaSQL.Add('6|"INJE��O"');
      FListaSQL.Add('7|"ARMADILHA"');
      FListaSQL.Add('8|"FIXA��O"');
    end else
    if Uppercase(Tabela) = Uppercase('TpConstrus') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('1|"ALVENARIA"');
      FListaSQL.Add('2|"TELHAS GALVANIZADAS"');
      FListaSQL.Add('3|"MADEIRA"');
      FListaSQL.Add('4|"L�PIDES"');
      FListaSQL.Add('5|"MISTA - ALVENARIA E MADEIRA"');
      FListaSQL.Add('6|"CONTEINER DE METAL"');
      FListaSQL.Add('7|"PROVIS�RIA"');
      FListaSQL.Add('8|"SEM CONSTRU��O"');
      FListaSQL.Add('9|"GESSO ACARTONADO"');
      FListaSQL.Add('10|"PR�-MOLDADO COM ACABAMENTO"');
      FListaSQL.Add('11|"PR�-MOLDADO SEM ACABAMENTO"');
      FListaSQL.Add('12|"DIVIDIDA POR ALAMBRADOS"');
      FListaSQL.Add('13|"DIVERSAS"');
      FListaSQL.Add('14|"BARRAC�O COM TELHAS GALVANIZADAS"');
    end else
    if Uppercase(Tabela) = Uppercase('UnidMed') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|CodUsu|Sequencia|Grandeza|Sigla|Nome');
      FListaSQL.Add('-1|0-1|000|003|"??"|"? ? ? ? ?"');
      FListaSQL.Add('0|000|000|003|""|""');
      FListaSQL.Add('1|001|000|000|"P�"|"PE�A"');
      FListaSQL.Add('2|002|000|000|"PCTE"|"PACOTE"');
      FListaSQL.Add('3|003|000|005|"L"|"LITROS"');
      FListaSQL.Add('4|004|000|005|"ML"|"MILILITROS"');
      FListaSQL.Add('5|005|000|000|"BL"|"BLOCO"');
      FListaSQL.Add('6|006|000|002|"G"|"GRAMAS"');
    end else
      ComplementaListaSQL2(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TBugs_Tabs.ComplementaListaSQL2(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Dependenci') then
    begin
      if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"."');
      FListaSQL.Add('1|"BWC I"');
      FListaSQL.Add('2|"RECEP��O"');
      FListaSQL.Add('3|"COZINHA INDUSTRIAL"');
      FListaSQL.Add('4|"VESTI�RIO FEMININO"');
      FListaSQL.Add('5|"BWC FEMININO"');
      FListaSQL.Add('6|"BWC MASCULINO"');
      FListaSQL.Add('7|"ESTOQUE DE EMBALAGENS"');
      FListaSQL.Add('8|"ESTOQUE DE HORTIFRUTI"');
      FListaSQL.Add('9|"ESTOQUE DE ALIMENTOS N�O PEREC�VEIS"');
      FListaSQL.Add('10|"BWC RECEP��O"');
      FListaSQL.Add('11|"ESTACIONAMENTO"');
      FListaSQL.Add('12|"DEP�SITO DE LIXO"');
      FListaSQL.Add('13|"CENTRAL GLP"');
      FListaSQL.Add('14|"RECEP��O/EXPEDI��O DE MERCADORIAS"');
      FListaSQL.Add('15|"ESCRIT�RIO"');
      FListaSQL.Add('16|"DESPENSA"');
      FListaSQL.Add('17|"COZINHA"');
      FListaSQL.Add('18|"REFEIT�RIO"');
      FListaSQL.Add('19|"LAVANDERIA"');
      FListaSQL.Add('20|"REDE DE ESGOTO"');
      FListaSQL.Add('21|"ALOJAMENTO"');
      FListaSQL.Add('22|"SALA DA ENGENHARIA"');
      FListaSQL.Add('23|"ALMOXARIFADO"');
      FListaSQL.Add('24|"SEGURAN�A DO TRABALHO"');
      FListaSQL.Add('25|"PER�METRO EXTERNO"');
      FListaSQL.Add('26|"JARDIM DE INVERNO"');
      FListaSQL.Add('27|"CONSULT�RIO"');
      FListaSQL.Add('28|"SALA DE AVALIA��O"');
      FListaSQL.Add('29|"SALA DE CURATIVOS"');
      FListaSQL.Add('30|"CORREDOR INTERNO"');
      FListaSQL.Add('31|"SALA DE ESPERA"');
      FListaSQL.Add('32|"BWC DESATIVADO"');
      FListaSQL.Add('33|"SALA DE ESTERILIZA��O"');
      FListaSQL.Add('34|"SAUNA DESATIVADA"');
      FListaSQL.Add('35|"PISCINA"');
      FListaSQL.Add('36|"CORREDOR EXTERNO"');
      FListaSQL.Add('37|"CASA DE M�QUINAS DA PISCINA"');
      FListaSQL.Add('38|"BUFFET"');
      FListaSQL.Add('39|"�REA DE MESAS"');
      FListaSQL.Add('40|"COZINHA DAS SALADAS"');
      FListaSQL.Add('41|"CHAPA"');
      FListaSQL.Add('42|"COZINHA DOS FORNOS"');
      FListaSQL.Add('43|"C�MARA FRIA"');
      FListaSQL.Add('44|"A�OUGUE"');
      FListaSQL.Add('45|"CAIXA"');
      FListaSQL.Add('46|"ESTOQUE"');
      FListaSQL.Add('47|"VESTI�RIO MASCULINO"');
      FListaSQL.Add('48|"ARQUIVO"');
      FListaSQL.Add('49|"�REA INTERNA DO IM�VEL"');
      FListaSQL.Add('50|"SHOW ROOM"');
      FListaSQL.Add('51|"EMBAIXO DA ESCADA"');
      FListaSQL.Add('52|"ESCADA"');
      FListaSQL.Add('53|"COPA"');
      FListaSQL.Add('54|"GRAMADO"');
      FListaSQL.Add('55|"CAL�ADA"');
      FListaSQL.Add('56|"ENTRADA"');
      FListaSQL.Add('57|"JARDIM"');
      FListaSQL.Add('58|"SALA DE REUNI�O I"');
      FListaSQL.Add('59|"SALA DE DESIGN"');
      FListaSQL.Add('60|".TODA �REA INTERNA DO IM�VEL"');
      FListaSQL.Add('61|".TODA �REA EXTERNA DO IM�VEL"');
      FListaSQL.Add('62|"PADARIA"');
      FListaSQL.Add('63|"�REA DE VENDAS"');
      FListaSQL.Add('64|"FEIRINHA"');
      FListaSQL.Add('65|"BWC FUNCION�RIOS"');
      FListaSQL.Add('66|"SALA DE ESTAR"');
      FListaSQL.Add('67|"QUARTO"');
      FListaSQL.Add('68|"SU�TE I"');
      FListaSQL.Add('69|"SU�TE COM SACADA"');
      FListaSQL.Add('70|"SU�TE COM CLOSET"');
      FListaSQL.Add('71|"�REA CAL�ADA FRENTE"');
      FListaSQL.Add('72|"QUINTAL FUNDOS"');
      FListaSQL.Add('73|"QUARTO EXTERNO"');
      FListaSQL.Add('74|"�RVORES"');
      FListaSQL.Add('75|"TELHADO"');
      FListaSQL.Add('76|"QUINTAL"');
      FListaSQL.Add('77|"SALA DE JANTAR"');
      FListaSQL.Add('78|"SALA DE TV"');
      FListaSQL.Add('79|"SALA DE ESTAR 1"');
      FListaSQL.Add('80|"PORTARIA"');
      FListaSQL.Add('81|"HALL SOCIAL"');
      FListaSQL.Add('82|"HALL DE SERVI�O"');
      FListaSQL.Add('83|"GARAGEM"');
      FListaSQL.Add('84|"BICICLET�RIO"');
      FListaSQL.Add('85|"SAL�O DE FESTAS"');
      FListaSQL.Add('86|"ENTRADA DA GARAGEM"');
      FListaSQL.Add('87|"CHURRASQUEIRA"');
      FListaSQL.Add('88|"QUADRA EXTERNA"');
      FListaSQL.Add('89|"�REA DE SERVI�O"');
      FListaSQL.Add('90|"SACADA I"');
      FListaSQL.Add('91|"BOEIROS"');
      FListaSQL.Add('92|"TAMPAS SANEAMENTO NA RUA/CAL�ADA"');
      FListaSQL.Add('93|"LAVABO"');
      FListaSQL.Add('94|"BWC SERVI�O"');
      FListaSQL.Add('95|"DEP�SITO"');
      FListaSQL.Add('96|"PER�METRO DOS CONT�INERS"');
      FListaSQL.Add('97|"TERRENO"');
      FListaSQL.Add('98|"MURO COM TREPADEIRA"');
      FListaSQL.Add('99|"SALA DO MESTRE DE OBRAS"');
      FListaSQL.Add('100|"ADMINISTRATIVO"');
      FListaSQL.Add('101|"SALA DO CONTROLE"');
      FListaSQL.Add('102|"GRUA"');
      FListaSQL.Add('103|"SALA DA EL�TRICA"');
      FListaSQL.Add('104|"SALA EMPREITEIRA"');
      FListaSQL.Add('105|"QUARTO 6"');
      FListaSQL.Add('106|"QUARTO 7"');
      FListaSQL.Add('107|"SALA DA MANUTEN��O"');
      FListaSQL.Add('108|"SALA DO ELEVADOR"');
      FListaSQL.Add('109|"SALA DO ZELADOR"');
      FListaSQL.Add('110|"SALA DA CARPINTARIA"');
      FListaSQL.Add('111|"PARQUINHO"');
      FListaSQL.Add('112|"SALA DE AULA"');
      FListaSQL.Add('113|"P�TEO EXTERNO"');
      FListaSQL.Add('114|"�REA CASCALHADA"');
      FListaSQL.Add('115|"BIBLIOTECA"');
      FListaSQL.Add('116|"SALA DE INFORM�TICA"');
      FListaSQL.Add('117|"SALA DE PROFESSORES"');
      FListaSQL.Add('118|"SALA DE M�SICA"');
      FListaSQL.Add('119|"BER��RIO"');
      FListaSQL.Add('120|"SALA DE V�DEO"');
      FListaSQL.Add('121|"SALA DE BRINQUEDO"');
      FListaSQL.Add('122|"ESPA�O LAZER"');
      FListaSQL.Add('123|"SALA 2 AMBIENTES"');
      FListaSQL.Add('124|"DRIVE-THRU"');
      FListaSQL.Add('125|"QUARTO 1"');
      FListaSQL.Add('126|"QUARTO 2"');
      FListaSQL.Add('127|"QUARTO 3"');
      FListaSQL.Add('128|"QUARTO 4"');
      FListaSQL.Add('129|"BWC SOCIAL"');
      FListaSQL.Add('130|"BWC SU�TE"');
      FListaSQL.Add('131|"SALA DO SERVIDOR"');
      FListaSQL.Add('132|"D.M.L."');
      FListaSQL.Add('133|"SALA DE ATENDIMENTO 1"');
      FListaSQL.Add('134|"SALA DE ATENDIMENTO 2"');
      FListaSQL.Add('135|"SALA DE ATENDIMENTO 3"');
      FListaSQL.Add('136|"SALA DE ATENDIMENTO 4"');
      FListaSQL.Add('137|"SALA DE ATENDIMENTO 5"');
      FListaSQL.Add('138|"SALA DE ATENDIMENTO 6"');
      FListaSQL.Add('139|"SALA DO RAIO-X"');
      FListaSQL.Add('140|"SALA VAZIA"');
      FListaSQL.Add('141|"LABORAT�RIO"');
      FListaSQL.Add('142|"SALA DE SELE��O"');
      FListaSQL.Add('143|"SALA DO M�DICO"');
      FListaSQL.Add('144|"ENFERMARIA"');
      FListaSQL.Add('145|"SALA DE TREINAMENTO"');
      FListaSQL.Add('146|"SALA DA CHEFIA"');
      FListaSQL.Add('147|"SALA DE ENTREVISTAS"');
      FListaSQL.Add('148|"�REA DESATIVADA"');
      FListaSQL.Add('149|"ORQUID�RIO"');
      FListaSQL.Add('150|"SALA DA EXPEDI��O"');
      FListaSQL.Add('151|"SALA DA DIRETORIA"');
      FListaSQL.Add('152|"RECEP��O DA DIRETORIA"');
      FListaSQL.Add('153|"SALA DE REUNI�O II"');
      FListaSQL.Add('154|"SALA DE REUNI�O III"');
      FListaSQL.Add('155|"SALA DE REUNI�O IV"');
      FListaSQL.Add('156|"SALA DE REUNI�O V"');
      FListaSQL.Add('157|"�REA DE ARM�RIOS MASCULINOS"');
      FListaSQL.Add('158|"�REA DE DESCANSO"');
      FListaSQL.Add('159|"�REA DE ARM�RIOS FEMININOS"');
      FListaSQL.Add('160|"RAMPA"');
      FListaSQL.Add('161|"SALA DE DEVOLU��O DOS TALHERES"');
      FListaSQL.Add('162|"SALA DE LAVAGEM DE TALHERES"');
      FListaSQL.Add('163|"RECEP��O DE ALIMENTOS - �REA SUJA"');
      FListaSQL.Add('164|"SALA DA(O) NUTRICIONISTA"');
      FListaSQL.Add('165|"GARAGEM -1"');
      FListaSQL.Add('166|".TODA �REA COMUM"');
      FListaSQL.Add('167|"LOJA"');
      FListaSQL.Add('168|".TODA �REA �TIL DO IM�VEL"');
      FListaSQL.Add('169|"VARANDA"');
      FListaSQL.Add('170|"SU�TE MASTER"');
      FListaSQL.Add('171|"SU�TE II"');
      FListaSQL.Add('172|"SU�TE III"');
      FListaSQL.Add('173|"SU�TE IV"');
      FListaSQL.Add('174|"SACADA II"');
      FListaSQL.Add('175|"SACADA III"');
      FListaSQL.Add('176|"MADEIRAMENTO A VISTA EXTERNO"');
      FListaSQL.Add('177|"RESID�NCIA NOS FUNDOS"');
      FListaSQL.Add('178|"ESTACIONAMENTO FUNDOS"');
      FListaSQL.Add('179|"BAR FUNDOS"');
      FListaSQL.Add('180|"PISTA DE DAN�A"');
      FListaSQL.Add('181|"CAMARIM"');
      FListaSQL.Add('182|"BWC CAMARIM"');
      FListaSQL.Add('183|"�REA VIP I"');
      FListaSQL.Add('184|"�REA VIP II"');
      FListaSQL.Add('185|"OFICINA USINAGEM"');
      FListaSQL.Add('186|"TRANSFORMADOR DE ENERGIA"');
      FListaSQL.Add('187|"MEZANINO"');
      FListaSQL.Add('188|"DEP�SITO I"');
      FListaSQL.Add('189|"DEP�SITO II"');
      FListaSQL.Add('190|"FOSSA"');
      FListaSQL.Add('191|"LAVAGEM DE PE�AS"');
      FListaSQL.Add('192|"DEPOSITO IV"');
      FListaSQL.Add('193|"DEPOSITO V"');
      FListaSQL.Add('194|"COMPRESSORES"');
      FListaSQL.Add('195|"DEP�SITO DE CARCA�AS"');
      FListaSQL.Add('196|"FORNO DE FUNDI��O"');
      FListaSQL.Add('197|"JATO DE AREIA"');
      FListaSQL.Add('198|"SALA DA LOG�STICA"');
      FListaSQL.Add('199|"SALA DA CONTABILIDADE"');
      FListaSQL.Add('200|"BWC II"');
      FListaSQL.Add('201|"OFICINA I"');
      FListaSQL.Add('202|"OFICINA II"');
      FListaSQL.Add('203|"BORRACHARIA"');
      FListaSQL.Add('204|"�REA DE RES�DUOS"');
      FListaSQL.Add('205|"CASA DOS MOTORISTAS"');
      FListaSQL.Add('206|"FUNILARIA E PINTURA"');
      FListaSQL.Add('207|"MEZANINO - ESCRIT�RIO"');
      FListaSQL.Add('208|"P�TEO DE TERRA"');
      FListaSQL.Add('209|"�REA E-COAT"');
      FListaSQL.Add('210|"SUB-ESTA��O DE ENERGIA 2"');
      FListaSQL.Add('211|"ALMOXARIFADO NOVO"');
      FListaSQL.Add('212|"ALMOXARIFADO ANTIGO"');
      FListaSQL.Add('213|"LAVANDERIA EXTERNA"');
      FListaSQL.Add('214|"FRENTE DA LOJA"');
      FListaSQL.Add('215|"ESTOQUE DE ROUPAS"');
      FListaSQL.Add('216|"PROVADOR I"');
      FListaSQL.Add('217|"PROVADOR II"');
      FListaSQL.Add('218|"SALA DAS COSTUREIRAS I"');
      FListaSQL.Add('219|"SALA DAS COSTUREIRAS II"');
      FListaSQL.Add('220|"�REA DE PRODU��O"');
      FListaSQL.Add('221|"MURO"');
      FListaSQL.Add('222|"�REA DOS CAIXAS ELETR�NICOS"');
      FListaSQL.Add('223|"�REA DE ATENDIMENTO AOS CLIENTES"');
      FListaSQL.Add('224|"BWC CLIENTES"');
      FListaSQL.Add('225|"ESCRIT�RIO FUNDOS"');
      FListaSQL.Add('226|"SALA T�CNICA"');
      FListaSQL.Add('227|"TESOURARIA"');
      FListaSQL.Add('228|"SALA ATR�S DOS CAIXAS ELETR�NICOS"');
      FListaSQL.Add('229|"ADEGA"');
      FListaSQL.Add('230|"�REA EXTERNA"');
      FListaSQL.Add('231|"SU�TE EMPREGADA"');
      FListaSQL.Add('232|"CAPELA"');
      FListaSQL.Add('233|"SALAS"');
      FListaSQL.Add('234|"BANHEIROS"');
      FListaSQL.Add('235|"QUARTOS"');
      FListaSQL.Add('236|"QUARTO 5"');
      FListaSQL.Add('237|"CORREDOR FUNDOS"');
      FListaSQL.Add('238|"�REA DO FORNO � LENHA"');
      FListaSQL.Add('239|"SALA DE SOBREMESAS"');
      FListaSQL.Add('240|"DEP�SITO DE BEBIDAS"');
      FListaSQL.Add('241|"SALA DE LAVAGEM DE PANELAS"');
      FListaSQL.Add('242|"SALAS DAS C�MARAS FRIAS"');
      FListaSQL.Add('243|"LAVANDERIA/ELEVADOR"');
      FListaSQL.Add('244|"PO�O DO ELEVADOR"');
      FListaSQL.Add('245|"ANTE-SALA DOS VESTI�RIOS"');
      FListaSQL.Add('246|"�REA DO AR CONDICIONADO"');
      FListaSQL.Add('247|"�REA DOS EXAUSTORES"');
      FListaSQL.Add('248|"FRENTE DO IM�VEL"');
      FListaSQL.Add('249|"SALAS DE AULA"');
      FListaSQL.Add('250|"SALA DE MUSCULA��O"');
      FListaSQL.Add('251|"BWC PISCINA"');
      FListaSQL.Add('252|"�REA DA EXPANS�O"');
      FListaSQL.Add('253|"LAVA-RAPIDO"');
      FListaSQL.Add('254|"ESCADA ROLANTE"');
      FListaSQL.Add('255|"VAGAS PARA VE�CULOS"');
      FListaSQL.Add('256|"SA�DA DA GARAGEM"');
      FListaSQL.Add('257|"QUARTINHO"');
      FListaSQL.Add('258|"ESTOQUE DE FARINHA"');
      FListaSQL.Add('259|"PER�METRO INTERNO"');
      FListaSQL.Add('260|"BEIRAL DO TELHADO"');
      FListaSQL.Add('261|"ESCRIT�RIO FRENTE"');
      FListaSQL.Add('262|"DEP�SITO DE EQUIPAMENTOS"');
      FListaSQL.Add('263|"FUNDOS DA LOJA"');
      FListaSQL.Add('264|"GUARITA"');
      FListaSQL.Add('265|"JARDIM FRENTE"');
      FListaSQL.Add('266|"JARDIM LATERAL"');
      FListaSQL.Add('267|"ESCADARIA GARAGEM"');
      FListaSQL.Add('268|"CASA CISTERNA"');
      FListaSQL.Add('269|"SAL�O DE REPOUSO"');
      FListaSQL.Add('270|"SAL�O DE BELEZA"');
      FListaSQL.Add('271|"SAUNA"');
      FListaSQL.Add('272|"ACADEMIA"');
      FListaSQL.Add('273|"SALA DE JOGOS"');
      FListaSQL.Add('274|"SAL�O INFANTIL"');
      FListaSQL.Add('275|"SAL�O GOURMET"');
      FListaSQL.Add('276|"SALA DO GERADOR"');
      FListaSQL.Add('277|"QUARTO EMBAIXO DA RAMPA"');
      FListaSQL.Add('278|"ESCADARIA DE EMERG�NGIA"');
      FListaSQL.Add('279|"CASA DE M�QUINAS ELEVADOR"');
      FListaSQL.Add('280|"SALA DE FRALDAS"');
      FListaSQL.Add('281|"SALA 1"');
      FListaSQL.Add('282|"SALA 2"');
      FListaSQL.Add('283|"SALA 3"');
      FListaSQL.Add('284|"SALA 4"');
      FListaSQL.Add('285|"SALA 5"');
      FListaSQL.Add('286|"SALA 6"');
      FListaSQL.Add('287|"SALA DE BOLSAS"');
      FListaSQL.Add('288|"BWC III"');
      FListaSQL.Add('289|"SECRETARIA"');
      FListaSQL.Add('290|"BAR"');
      FListaSQL.Add('291|"VESTI�RIO 1"');
      FListaSQL.Add('292|"VESTI�RIO 2"');
      FListaSQL.Add('293|"ENTRADA DE VE�CULOS"');
      FListaSQL.Add('294|"ARQUIBANCADA"');
      FListaSQL.Add('295|"ESCRIT�RIOS"');
      FListaSQL.Add('296|"SALA DO FUNDI�RIO"');
      FListaSQL.Add('297|"ESTOQUE MEZANINO"');
      FListaSQL.Add('298|"ESCRIT�RIO MEZANINO"');
      FListaSQL.Add('299|"PADARIA MEZANINO"');
      FListaSQL.Add('300|"�REA DAS G�NDOLAS"');
      FListaSQL.Add('301|"BALC�ES REFRIGERADOS FUNDOS"');
      FListaSQL.Add('302|"SHAFT"');
      FListaSQL.Add('303|"SA�DA DE EMERG�NCIA 1"');
      FListaSQL.Add('304|"SA�DA DE EMERG�NCIA 2"');
      FListaSQL.Add('305|"SA�DA DE EMERG�NCIA 3"');
      FListaSQL.Add('306|"ESCADA ACESSO 5� PISO"');
      FListaSQL.Add('307|"CORREDOR T�CNICO 1"');
      FListaSQL.Add('308|"CORREDOR T�CNICO 2"');
      FListaSQL.Add('309|"CORREDOR T�CNICO 3"');
      FListaSQL.Add('310|"ESCADA ROLANTE DESCE"');
      FListaSQL.Add('311|"ESCADA ROLANTE SOBE"');
      FListaSQL.Add('312|"FLOREIRAS"');
      FListaSQL.Add('313|"FORRO - �REA T�CNICA"');
      FListaSQL.Add('314|"LAVAT�RIO"');
      FListaSQL.Add('315|"�REA DE CIRCULA��O 1"');
      FListaSQL.Add('316|"BWC CADEIRANTES"');
      FListaSQL.Add('317|"FRENTE DAS LOJAS"');
      FListaSQL.Add('318|"BWC MASC. �REA T�CNICA"');
      FListaSQL.Add('319|"BWC FEM. �REA T�CNICA"');
      FListaSQL.Add('320|"CONFEITARIA"');
      FListaSQL.Add('321|"TELHADO DA PADARIA - FRENTE"');
      FListaSQL.Add('322|"CORREDOR LATERAL ESQUERDO"');
      FListaSQL.Add('323|"CORREDOR DO TANQUE"');
      FListaSQL.Add('324|"CANIL"');
      FListaSQL.Add('325|"P�TEO COBERTO"');
      FListaSQL.Add('326|"MEZANINO FRENTE"');
      FListaSQL.Add('327|"MEZANINO FUNDOS"');
      FListaSQL.Add('328|"COZINHA PRATOS QUENTES"');
      FListaSQL.Add('329|"CORREDOR LATERAL DIREITO"');
      FListaSQL.Add('330|"S�T�O"');
      FListaSQL.Add('331|"SALA DE MAT�RIA-PRIMA"');
      FListaSQL.Add('332|"SALA DE ROTULAGEM"');
      FListaSQL.Add('333|"ESTOQUE DE MATERIAL ACABADO"');
      FListaSQL.Add('334|"DEP�SITO DE CAIXAS"');
      FListaSQL.Add('335|"ATENDIMENTO AO CLIENTE"');
      FListaSQL.Add('336|"SALA DA FATIADORA DE FRIOS"');
      FListaSQL.Add('337|"SALA DE TROCAS"');
      FListaSQL.Add('338|"�REA DOS RECICL�VEIS"');
      FListaSQL.Add('339|"SAL�O DE MESAS 1"');
      FListaSQL.Add('340|"SAL�O DE MESAS 2"');
      FListaSQL.Add('341|"BWC FEMININO INTERNO"');
      FListaSQL.Add('342|"BWC FEMININO EXTERNO"');
      FListaSQL.Add('343|"BWC MASCULINO EXTERNO"');
      FListaSQL.Add('344|"BWC MASCULINO INTERNO"');
      FListaSQL.Add('345|"LANCHONETE"');
      FListaSQL.Add('346|"�REA EXTERNA FUNDOS"');
      FListaSQL.Add('347|"�REA EXTERNA FRENTE"');
      FListaSQL.Add('348|"C�MARA FRIA DESATIVADA 1"');
      FListaSQL.Add('349|"C�MARA FRIA DESATIVADA 2"');
      FListaSQL.Add('350|"RAMPA DESCIDA"');
      FListaSQL.Add('351|"RAMPA SUBIDA"');
      FListaSQL.Add('352|"DEP�SITO VI"');
      FListaSQL.Add('353|"DEP�SITO VII"');
      FListaSQL.Add('354|"CENTRAL DE ESGOTO"');
      FListaSQL.Add('355|"CAG"');
      FListaSQL.Add('356|"�REA DA PISCINA"');
      FListaSQL.Add('357|"CENTRAL DE IMAGENS"');
      FListaSQL.Add('358|"BALC�ES"');
      FListaSQL.Add('359|"QUIOSQUE"');
      FListaSQL.Add('360|"LABORAT�RIOS"');
      FListaSQL.Add('361|"FRITADEIRA"');
      FListaSQL.Add('362|"SALSICHARIA"');
      FListaSQL.Add('363|"FLV"');
      FListaSQL.Add('364|"PAS"');
      FListaSQL.Add('365|"PEIXARIA"');
      FListaSQL.Add('366|"PERFUMARIA"');
      FListaSQL.Add('367|"SECA"');
      FListaSQL.Add('368|"L�QUIDA"');
      FListaSQL.Add('369|"BAZAR"');
      FListaSQL.Add('370|"ELETRO"');
      FListaSQL.Add('371|"TEXTIL"');
      FListaSQL.Add('372|"CHECK OUT"');
      FListaSQL.Add('373|"FRENTE DE CAIXA"');
      FListaSQL.Add('374|"�REAS COMUNS"');
      FListaSQL.Add('375|"DEP�SITO VIII"');
      FListaSQL.Add('376|"DEP�SITO DO BAZAR"');
      FListaSQL.Add('377|"DEP�SITO DO ELETRO"');
      FListaSQL.Add('378|"P�TIOS EXTERNOS"');
      FListaSQL.Add('379|"DOCAS"');
      FListaSQL.Add('380|"GALERIAS"');
      FListaSQL.Add('381|"DEP�SITO IX"');
      FListaSQL.Add('382|"DEP�SITO X"');
      FListaSQL.Add('383|"DEP�SITO XI"');
      FListaSQL.Add('384|"CORREDOR ENTRE BARRAC�ES"');
      FListaSQL.Add('385|"MOINHO 1"');
      FListaSQL.Add('386|"MOINHO 2"');
      FListaSQL.Add('387|"MEC�NICA E TORNO"');
      FListaSQL.Add('388|"CONTROLE DE QUALIDADE"');
      FListaSQL.Add('389|"SALA DE MOLDES"');
      FListaSQL.Add('390|"ESTOQUE DE MAT�RIA PRIMA"');
      FListaSQL.Add('391|"HALL DE ENTRADA"');
      FListaSQL.Add('392|"RECEP��O DE PRODUTOS"');
      FListaSQL.Add('393|"DEPTO. COMERCIAL"');
      FListaSQL.Add('394|"SALA DA HIGIENIZA��O"');
      FListaSQL.Add('395|"BWC FEMININO - COPA"');
      FListaSQL.Add('396|"BWC MASCULINO - COPA"');
      FListaSQL.Add('397|"BWC FEMININO - SL. MANUTEN��O"');
      FListaSQL.Add('398|"BWC MASCULINO - SL. MANUTEN��O"');
      FListaSQL.Add('399|"EXPEDI��O DE PRODUTOS"');
      FListaSQL.Add('400|"DEPTO. FINANCEIRO"');
      FListaSQL.Add('401|"SALA DE TESTES/INSPE��O"');
      FListaSQL.Add('402|"SALA DE MATERIAIS DE MANUTEN��O"');
      FListaSQL.Add('403|"COMPRESSOR"');
      FListaSQL.Add('404|"P�TEO INTERNO"');
      FListaSQL.Add('405|"FOYER"');
      FListaSQL.Add('406|"SALA DE PROJE��O 1,2,3"');
      FListaSQL.Add('407|"SALA DE PROJE��O 4,5"');
      FListaSQL.Add('408|"SALA DE �CULOS 3D"');
      FListaSQL.Add('409|"BOMBONIERE"');
      FListaSQL.Add('410|"CINEMA - SALA 1"');
      FListaSQL.Add('411|"CINEMA - SALA 2"');
      FListaSQL.Add('412|"CINEMA - SALA 3"');
      FListaSQL.Add('413|"CINEMA - SALA 4"');
      FListaSQL.Add('414|"CINEMA - SALA 5"');
      FListaSQL.Add('415|"CINEMA - SALA 6"');
      FListaSQL.Add('416|"SALA DE MERENDA"');
      FListaSQL.Add('417|"DEP�SITO DE LENHA"');
      FListaSQL.Add('418|"EMBALAGEM DE PRODUTOS"');
      FListaSQL.Add('419|"ESTOQUE DE PRODUTO FINALIZADO"');
      FListaSQL.Add('420|"RECEP��O DE MAT�RIA PRIMA"');
      FListaSQL.Add('421|"�REA DAS CANALETAS DE AMIDO"');
      FListaSQL.Add('422|"BARRAC�O DA COLA"');
      FListaSQL.Add('423|"CALDEIRA"');
      FListaSQL.Add('424|"SUB-ESTA��O DE ENERGIA"');
      FListaSQL.Add('425|"�REA DE SECAGEM"');
      FListaSQL.Add('426|"�REA DOS TANQUES"');
      FListaSQL.Add('427|"LAGOA 1"');
      FListaSQL.Add('428|"LAGOA 2"');
      FListaSQL.Add('429|"LAGOA 3"');
      FListaSQL.Add('430|"LAGOA 4"');
      FListaSQL.Add('431|"BALAN�A"');
      FListaSQL.Add('432|"PISTA DE ABASTECIMENTO"');
      FListaSQL.Add('433|"GER�NCIA"');
      FListaSQL.Add('434|"ENTRADA PRINCIPAL DE SHOPPING"');
      FListaSQL.Add('435|"TRIAGEM DE LIXO RECICL�VEL"');
      FListaSQL.Add('436|"SALA 7"');
      FListaSQL.Add('437|"�REA DOS CONT�INERS DE LIXO NA RUA"');
      FListaSQL.Add('438|"CAIXAS DE GORDURA/ESGOTO"');
      FListaSQL.Add('439|"CORREDORES"');
      FListaSQL.Add('440|"QUADRA"');
      FListaSQL.Add('441|"GRAMADOS"');
      FListaSQL.Add('442|"BALC�O DE ATENDIMENTO"');
      FListaSQL.Add('443|"G�NDOLA"');
      FListaSQL.Add('444|"�REA DA FATIADORA DE FRIOS"');
      FListaSQL.Add('445|"BALC�O (CESTO) DE P�ES"');
      FListaSQL.Add('446|"BALC�O REFRIGERADO"');
      FListaSQL.Add('447|"GELADEIRA"');
      FListaSQL.Add('448|"ESTUFA DE DOCES"');
      FListaSQL.Add('449|"ESTUFA DE SALGADOS"');
      FListaSQL.Add('450|"PIA (FRENTE)"');
      FListaSQL.Add('451|"PIA (PRODU��O)"');
      FListaSQL.Add('452|"�REA DO FORNO MICROONDAS"');
      FListaSQL.Add('453|"�REA DE ESTAR"');
      FListaSQL.Add('454|"BALC�O DA CHOPEIRA/CHOPEIRA"');
      FListaSQL.Add('455|"GARAGEM INTERNA"');
      FListaSQL.Add('456|"CARAMELO STORE"');
      FListaSQL.Add('457|"ENTRADA DA RECEP��O"');
      FListaSQL.Add('458|"BARRAC�O 1"');
      FListaSQL.Add('459|"BARRAC�O 2"');
      FListaSQL.Add('460|"BARRAC�O 3"');
      FListaSQL.Add('461|"BARRAC�O 4"');
      FListaSQL.Add('462|"BARRAC�O 5"');
      FListaSQL.Add('463|"BARRAC�O 6"');
      FListaSQL.Add('464|"BARRAC�O 7"');
      FListaSQL.Add('465|"SUB-ESTA��O COMBATE A INC�NDIO"');
      FListaSQL.Add('466|"P.A.1"');
      FListaSQL.Add('467|"P.A.2"');
      FListaSQL.Add('468|"CENTRAL DE OPERA��ES"');
      FListaSQL.Add('469|"P.A.3"');
      FListaSQL.Add('470|"BWC MASC. ADMINISTRATIVO"');
      FListaSQL.Add('471|"BWC FEM. ADMINISTRATIVO"');
      FListaSQL.Add('472|"SALA DE REUNI�O DIRETORIA"');
      FListaSQL.Add('473|"SOLARIUM"');
      FListaSQL.Add('474|"CONSULT�RIOS"');
      FListaSQL.Add('475|"LEITOS"');
      FListaSQL.Add('476|"CARROSSEL"');
      FListaSQL.Add('477|"SALA DAS CHOPEIRAS"');
      FListaSQL.Add('478|"SALA DE BREAK"');
      FListaSQL.Add('479|"APARTAMENTOS DECORADOS"');
      FListaSQL.Add('480|"SALA DE CONSERTOS"');
      FListaSQL.Add('481|"ESTOQUE DE CARTUCHOS"');
      FListaSQL.Add('482|"TELEMARKETING"');
      FListaSQL.Add('483|"DEP�SITO DE ESPECIARIAS"');
      FListaSQL.Add('484|"ENVASE/ESTOQUE DE PRODUTOS"');
      FListaSQL.Add('485|"DEP�SITO DE LENHA (EXTERNO)"');
      FListaSQL.Add('486|"DEP�SITO DE LIXO ORG�NICO"');
      FListaSQL.Add('487|"DEP�SITO DE LIXO RECICL�VEL"');
      FListaSQL.Add('488|"COZINHA AUXILIAR"');
      FListaSQL.Add('489|"BWC MASC. - �REA DE LAZER"');
      FListaSQL.Add('490|"BWC FEM. - �REA DE LAZER"');
      FListaSQL.Add('491|"SALAS DE REUNI�O"');
      FListaSQL.Add('492|"CASA DE M�QUINAS DA HIDROMASSAGEM"');
      FListaSQL.Add('493|"COZINHA DESATIVADA"');
      FListaSQL.Add('494|"GOVERN�NCIA"');
      FListaSQL.Add('495|"DEP�SITO DE COLCH�ES"');
      FListaSQL.Add('496|"AUDIT�RIO"');
      FListaSQL.Add('497|"ESTOQUE DE �LEO"');
      FListaSQL.Add('498|"LOJA DE CONVENI�NCIA"');
      FListaSQL.Add('499|"TROCA DE �LEO"');
      FListaSQL.Add('500|"DEP�SITO DE INSUMOS P/ ENTREPOSTOS"');
      FListaSQL.Add('501|"MOEGA/SECADOR 1"');
      FListaSQL.Add('502|"MOEGA/SECADOR 2"');
      FListaSQL.Add('503|"SILO 1"');
      FListaSQL.Add('504|"SILO 2"');
      FListaSQL.Add('505|"SILO 3"');
      FListaSQL.Add('506|"SILO 4"');
      FListaSQL.Add('507|"MOEGA 1"');
      FListaSQL.Add('508|"MOEGA 2"');
      FListaSQL.Add('509|"ENSAQUE"');
      FListaSQL.Add('510|"SELECIONADORA DE ARROZ"');
      FListaSQL.Add('511|"RENDA DE ARROZ"');
      FListaSQL.Add('512|"EMPACOTADORA"');
      FListaSQL.Add('513|"SALA 8"');
      FListaSQL.Add('514|"SALA 9"');
      FListaSQL.Add('515|"SALA 10"');
      FListaSQL.Add('516|"SALA  11"');
      FListaSQL.Add('517|"SALA  12"');
      FListaSQL.Add('518|"P�TEO FUNDOS"');
      FListaSQL.Add('519|"DEPTO. RH"');
      FListaSQL.Add('520|"P�TEO CAL�ADO"');
      FListaSQL.Add('521|"�REA DE CONTEN��O EFLUENTES"');
      FListaSQL.Add('522|"ESTOQUE DE SULFETO DE S�DIO"');
      FListaSQL.Add('523|"�REA DE LAVAGEM DE EMBALAGENS"');
      FListaSQL.Add('524|"EMBAIXO DA PIA"');
      FListaSQL.Add('525|"ATR�S M�Q. SORVETE"');
      FListaSQL.Add('526|"ESTOQUE DE PRODUTOS QU�MICOS 1"');
      FListaSQL.Add('527|"ESTOQUE DE PRODUTOS QU�MICOS 2"');
      FListaSQL.Add('528|"HIDRANTES"');
      FListaSQL.Add('529|".TODOS ESCRIT�RIOS"');
      FListaSQL.Add('530|"SALA DA GARANTIA"');
      FListaSQL.Add('531|"FERRAMENTARIA"');
      FListaSQL.Add('532|"SALA DO MOTORISTA"');
      FListaSQL.Add('533|"REDE DE �GUAS PLUVIAIS (CHUVA)"');
      FListaSQL.Add('534|"DEP�SITO DE PE�AS"');
      FListaSQL.Add('535|"SALA DA ESTUFA"');
      FListaSQL.Add('536|"GARAGEM -2"');
      FListaSQL.Add('537|"CAIXAS DE ENERGIA"');
      FListaSQL.Add('538|"ESPELHO D��GUA"');
      FListaSQL.Add('539|"APARTAMENTO DECORADO I"');
      FListaSQL.Add('540|"APARTAMENTO DECORADO II"');
      FListaSQL.Add('541|"APARTAMENTO DECORADO III"');
      FListaSQL.Add('542|"PER�METRO DOS APTOS DECORADOS"');
      FListaSQL.Add('543|".TODOS QUARTOS"');
    end else
    if Uppercase(Tabela) = Uppercase('Objetos') then
    begin
      if FListaSQL.Count = 0 then
        FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('1|"T�RREO"');
      FListaSQL.Add('2|"1�PISO"');
      FListaSQL.Add('3|"MEZANINO"');
      FListaSQL.Add('4|"TELHADO"');
      FListaSQL.Add('5|"PAVIMENTO �NICO"');
      FListaSQL.Add('6|"�REA COMUM"');
      FListaSQL.Add('7|"SOBRELOJA"');
      FListaSQL.Add('8|"SUBSOLO -1"');
      FListaSQL.Add('9|"�REA GRAMADA"');
      FListaSQL.Add('10|"PAVIMENTO SUPERIOR"');
      FListaSQL.Add('11|"CONT�INER"');
      FListaSQL.Add('12|"2�PISO"');
      FListaSQL.Add('13|"SUBSOLO -2"');
      FListaSQL.Add('14|"TERRENO"');
      FListaSQL.Add('15|"PAVIMENTO INFERIOR"');
      FListaSQL.Add('16|"SOBRADO"');
      FListaSQL.Add('17|"BARRAC�O DE ZINCO"');
      FListaSQL.Add('18|"BARRAC�O COM MEZANINO"');
      FListaSQL.Add('19|"CASA T�RREA"');
      FListaSQL.Add('20|"LOJA COMERCIAL T�RREA"');
      FListaSQL.Add('21|"APARTAMENTO"');
      FListaSQL.Add('22|"LOJA DE SHOPPING"');
      FListaSQL.Add('23|"PR�DIO COM 2 PAVIMENTOS"');
      FListaSQL.Add('24|"LOJA COM MEZANINO"');
      FListaSQL.Add('25|"SUBSOLO -3"');
      FListaSQL.Add('26|"3�PISO"');
      FListaSQL.Add('27|"4�PISO"');
      FListaSQL.Add('28|"8�PISO"');
      FListaSQL.Add('29|"9�PISO"');
      FListaSQL.Add('30|"10�PISO"');
      FListaSQL.Add('31|"RESID�NCIA"');
      FListaSQL.Add('32|"APARTAMENTO DUPLEX"');
      FListaSQL.Add('33|"QUIOSQUE"');
      FListaSQL.Add('34|"6�PISO"');
      FListaSQL.Add('35|"5�PISO"');
      FListaSQL.Add('36|"PR�DIO COM 3 PAVIMENTOS"');
      FListaSQL.Add('37|"PR�DIO COM 10 PAVIMENTOS"');
      FListaSQL.Add('38|"PR�DIO COM 12 PAVIMENTOS"');
      FListaSQL.Add('39|"PR�DIO COM 4 PAVIMENTOS"');
    end;
  except
    raise;
    Result := False;
  end;
end;


function TBugs_Tabs.CarregaListaFRIndices(TabelaBase: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  if Uppercase(TabelaBase) = Uppercase('AgeEqiCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('AgeEqiIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('ContrBugsN') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('ContrBugsS') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('ContrServi') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('CtrTagNiv1') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('CtrTagNiv2') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('CtrTagNiv3') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('CunsCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('CunsIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CunsGru';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'CunsSub';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('CunsImgCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('CunsImgNCT') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('CunsImgCmt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('CunsImgSit') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('GraG1EqAp') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Nivel1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('GraG1EqMo') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Nivel1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('GraG1PrAp') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Nivel1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('GraG1PrMo') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Nivel1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('GraG1PrPA') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('MovAmovCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('Dependenci') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('DesServico') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('EstatusOSs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('FatoGeradr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('Formulas') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('Formulia') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('FormulFiCB') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('FormulFiDd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('Formulif') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  {
  end else
  if Uppercase(TabelaBase) = Uppercase('Praga sCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  }
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapTerCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapTerFlh') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapImaCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapImaCdi') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapImaCav') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapImaRes') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapImaCui') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapImaAti') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapImaDep') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapImaCxa') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('SiapImaCxaAtr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OpcoesBugs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OpcoesFili') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Grupo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'SiapTerCad';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Opcao';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSChk') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'ChekLstIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSAge') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSCabAlv') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSCabXtr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSCxa') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(TabelaBase) = Uppercase('OSCxI') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(TabelaBase) = Uppercase('OSSrv') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('OSDep') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('OSAlv') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSFrmCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSFrmAbr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'IDIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSFrmEvo') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'IDIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSFrmFlhCb') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'IDIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSFrmFlhDd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'IDIt2';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSFrmRec') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'IDIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSFrmDep') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'IDIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSMonCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSMonRec') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'IDIts';
    FLIndices.Add(FRIndices);
    //
(*
  end else
  if Uppercase(TabelaBase) = Uppercase('_OS_Mon_Dep_') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'IDIts';
    FLIndices.Add(FRIndices);
    //
*)
  end else
  if Uppercase(TabelaBase) = Uppercase('OSMonPipDd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'IDIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSPrz') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSPipMon') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'PipCad';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'PrgLstCab';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSPipIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSPipItsPr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'IDIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSPos') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSPrv') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSSta') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('OSFlhGer') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('Praga_Z') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('PipCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('PosVdaCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('PosVdaIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('PrgCadPrg') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Sigla';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('PrgLstCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('PrgLstIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('Praga_ZTax') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'TaxonIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TaxonGru') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TaxonCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TaxonIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TaxonNet') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Fonte';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TxtGeneric') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TBugs_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('AgeEqiCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MD5';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('AgeEqiIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EhLider';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ContrBugsN') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Praga_A';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Praga_Z';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ContrServi') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Servico';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CtrTagNiv1') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'NivSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tag';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conteudo';
      FRCampos.Tipo       := 'mediumtext';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '999';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CunsCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';  // mesmo c�digo da entidade
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AtivPrinc';  // Atividade principal
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HowFind';  // Como conheceu
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Account';  // Vendedor
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataCon';  // Data primeiro contatocontato
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpImaInfo';  // Imprime dados secundarios na OS?
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('MovAmovCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'tinyint(1)'; // 0=N/D 1=M�vel 2=Autom�vel
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';   // mesmo c�digo da Entidade.Codigo ou seja CunsCad.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CunsIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'CunsGru';  // mesmo c�digo da entidade contratante
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CunsSub';  // mesmo c�digo da entidade consumidora
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CunsImgCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caminho';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dependencia';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicacao'; // 0=Texto livre 1=NCT/CAC
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodiNCT';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '999';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CunsImgNCT') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtCAC'; // CAC-Comunicado de acao corretiva
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
    end else
    if Uppercase(Tabela) = Uppercase('CunsImgCmt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtGeneric';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else
    if Uppercase(Tabela) = Uppercase('CunsImgSit') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NivSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '999';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('Dependenci') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DependType';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('DesServico') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observacao';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EstatusOSs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '999999999';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeCorIni';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := IntToStr(clWhite);
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeCorFim';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := IntToStr(clWhite);
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeCorDir';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeCorFon';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := IntToStr(clBlack);
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeCorHin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := IntToStr(clRed);
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('FatoGeradr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NeedOS_Ori';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('Formulas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EquipAplic';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdTot';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdQSP';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Diluente';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoAplica';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMed';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('Formulia') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Abrangicie';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('FormulFiCB') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Formula';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodd';   // Periodo de monitoramento em dias
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdPostero';   // Intervalos p�steros aos dias da tabela "FormulFiDd" em dias
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('FormulFiDd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';  // sequencia na soma
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dias';   // intervalo em dias
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('Formulif') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrvQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrvPrc';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrvVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoPrc';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoDec';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Reordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EhDiluente';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    {
    end else
    if Uppercase(Tabela) = Uppercase('Praga sCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grupo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    }
    end else
    if Uppercase(Tabela) = Uppercase('SiapTerCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';   // mesmo c�digo da Entidade.Codigo ou seja CunsCad.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SLograd';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SRua';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SNumero';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SCompl';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SBairro';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SCidade';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUF';
      FRCampos.Tipo       := 'char(3)'; // tam 3 :> se por engano colocar o c�digo (-18 por exemplo)
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SCEP';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPais';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SEndeRef';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SCodMunici';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SCodiPais';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1058';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'STe1';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Constru';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2NaoBuild';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Terreno';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Total'; // Contru�do + n�o constru�do
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstCusPrd';  // Lista de custos de produtos
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PdrMntsMon';  // Tempo padrao de monitoramento em minutos
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // Zero usara das 'opcoesbugs'
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SiapTerFlh') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MonAgeEqi';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MonEntCtt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MonNumCtr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MonEntCtr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MonEntPag';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MonCondPg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MonCrtEmi';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataSincOS';  // Data para sincronismo de OS filhas e Monitoramentos
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TrajetDist';  // Dist�ncia em km da filial ao cliente
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TrajetKm_h';  // Item de vel. m�dia da filial ao cliente
      FRCampos.Tipo       := 'tinyint';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SiapImaCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SiapImaTer'; // Terreno associado
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Objeto';  // Im�vel, movel ou autom�vel
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Finalidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpConstru';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SCompl2';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Constru';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2NaoBuild';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Terreno';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Total'; // Contru�do + n�o constru�do
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SiapImaCdi') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caracteris';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SiapImaCav') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caracteris';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SiapImaRes') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Residente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoNatal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cuidado';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observacao';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SiapImaCui') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cuidado';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SiapImaAti') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Atividade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SiapImaDep') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dependenci';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MLarg';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MComp';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MAltu';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SiapImaCxa') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MatersCxa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FormasCxa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VolumeL';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Local';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acesso';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Medidas';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else

{ TODO : Fazer conforme entidades }
    // Fezer conforme entidades
    if Uppercase(Tabela) = Uppercase('SiapImaCxaAtr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Atributo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OpcoesBugs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraNivEqAp';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraCodEqAp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraNivEqMo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraCodEqMo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraNivPrAp';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraCodPrAp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraNivPrMo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraCodPrMo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImgIPv4';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '127.0.0.1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImgSDir';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '|C:|Dermatek|Taxonomia|Imagens';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaIPv4';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '127.0.0.1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CxaSDir';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '|C:|Dermatek|Imagens|CaixasDAgua';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CorImpPrdBkg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2147483647';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CorImpPrdTxt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CorImpPrdBld';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfoImoMov';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CunsIPv4';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '127.0.0.1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CunsSDir';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '|C:|Dermatek|Entidades|Imagens';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfQdr2Dep';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeNomAnts';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IdxBDAnt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SWTAgenda'; // Start With Tabbing Agenda
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdRotaFlh'; // Intervalo de agrupamento de OSs filhas por rota
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '6';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdRotaSab'; // O que fazer com OSs que caem em s�bado
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdRotaDom'; // O que fazer com OSs que caem em domingo
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdRotaFer'; // O que fazer com OSs que caem em feriado
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OSPrvSta'; // OSCab.Estatus (de OS preventiva)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OSPrvETC'; // > OSCab.ExeTxtCli1 (de OS preventiva)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OSPrvXtraD'; // > OSSrv.GarantiaDd (Dias extras de grantia dos servi�os de OS preventiva)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ATBAutoExp'; // > Auto expansao da AdvToolBarPager do form Principal
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlhAgeEqi'; // > FlhAgeEqi CO_OSFlh_OPT_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlhEntCtt'; // EntiContat > CO_OSFlh_OPT_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlhNumCtr'; // > NumContrat CO_OSFlh_OPT_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlhEntCtr'; // EntContrat > CO_OSFlh_OPT_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlhEntPag'; // EntPagante > CO_OSFlh_OPT_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlhCondPg'; // > CondicaoPg CO_OSFlh_OPT_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlhCrtEmi'; // CartEmis > CO_OSFlh_OPT_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cros1BordB';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // Preto
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cros1Per1B';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '16777215'; // Branco
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cros1Res1B';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '16777215'; // Branco
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cros1Res2B';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '16777215'; // Branco
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cros1Res3B';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '16777215'; // Branco
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstCusPrd';  // Lista de custos de produtos
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PdrMntsMon';  // Tempo padrao de monitoramento em
      FRCampos.Tipo       := 'int(11)';     // minutos para todos clientes.
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '30';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mediakmh1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '10';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mediakmh2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '20';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mediakmh3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '30';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mediakmh4';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '40';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mediakmh5';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '60';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OpcoesFili') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumLicOper';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Emergencia';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespTecNome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespTecDocu';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CarimboEmp';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespTec1';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgenDGrCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Grupo'; // Para varias OSs em uma
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Numero'; // N�mero da OS a ser impressa
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Opcao'; // Sequencial de or�amento: 1=1�, 2=2�, 3=3� etc...
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SiapTerCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatoGeradr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*  Desabilitado em 2013-08-06
      New(FRCampos);
      FRCampos.Field      := 'OSOrigem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'DtaContat';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaVisPrv';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaVisExe';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaExePrv';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaExeIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaExeFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaLibFat';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaFimFat';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorServi';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorDesco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorOutrs';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdsPosVda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntiContat'; // enticontat.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumContrat'; //
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntPagante';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntContrat';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondicaoPg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CartEmis';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerNF';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InvalServi';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InvalDesco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InvalOutrs';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InvalTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrcamServi';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrcamDesco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrcamOutrs';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrcamTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValiDdOrca';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Operacao';
      FRCampos.Tipo       := 'int(11)'; // 0=??, 1=Dedetiza��o, 2=Limpeza caixa d`�gua 4=... 8=.... 16=...
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ExeTxtCli1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ExeTxtCli2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorPre';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ObsGaranti';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FimVisPrv';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FimVisExe';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FimExePrv';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Optado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; //0=N�o 1=Sim
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HowGerou'; // Como Gerou: 0=Manual, 1=Clonado, 2=Gerada por f�rmulas filhas de OS m�e
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PosGerou';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OSFlhUltGe'; // �ltima OSFlhGer executada!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*    2013-08-06 - desabilitado
      New(FRCampos);
      FRCampos.Field      := 'OSMaeCod'; // C�digo da OS m�e
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
(*    2013-08-20 - desabilitado
      New(FRCampos);
      FRCampos.Field      := 'GTSROTDR'; // Gerou todos sub registros de outras tabelas deste registro ?
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';  // Sim autom�tico
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'MulServico'; // Somente para relat�rios
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeEqiCab'; // Equipe de agentes respons�vel
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OSFlhGrCab'; // Grupo de gera��o de OSs
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OSFlhGrIts'; // Item de gera��o de OSs no Grupo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SohInicial'; // Revis�o inicial de PIP > Marcar para n�o adicionar PIPs!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=N�o, 1=Sim
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StPipAdPrg'; // Status da adi��o de perguntas nos PIPs CO_StPipAdPrg...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstUplWeb'; // Ultimo upload web
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSChk') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ChekLstIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Feito';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSAge') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Agente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Responsa';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSCabAlv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Praga_A';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Praga_Z';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSCabXtr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSCxa') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caixa'; //   - SiapImaCxa.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GarantiaDd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HrEvacuar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HrExecutar';
      FRCampos.Tipo       := 'double(15,10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValCalc';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValInfo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValDesc';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Autorizado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ChekLstCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Detalhes';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSCxI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FotoCxa';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Detalhes';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicacao'; // 1=Or�amento 2=Comprovante
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Imprimir';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSSrv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DesServico'; //   - Tipo de Servi�o:  Des...alguma coisa
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GarantiaDd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HrEvacuar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HrExecutar';
      FRCampos.Tipo       := 'double(15,10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValCalc';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValInfo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValDesc';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Autorizado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Detalhes';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TudoFeitoM'; // Manual
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TudoFeitoA'; // Autom�tico
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HowEstav'; // 0=N�o informado, 1=N�o executado na OS origem, 2=Executado na OS origem
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HowOrige'; // OSSrv.Controle do servi�o de origem da
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MoniDdTotl';  // Dias Totais de monitoramento
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MoniDdIntv'; // intervalo de dias de monitoramento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
    end else
    if Uppercase(Tabela) = Uppercase('OSDep') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0-ND 1-Dependencia 2=DependType
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cadastro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'ReIns'; // Usado para controle no InsUpd
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
}
    end else
    if Uppercase(Tabela) = Uppercase('OSAlv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Praga_Z';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSFrmCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Formula';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EquipAplic';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdTot';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdQSP';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Diluente';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoAplica';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMed';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercFeito'; // Automatico
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OSFFCbMae'; // OSFrmFlhCb.IDIts que originou esta OS.Servico.Formula
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IndicUso'; // AppListas.CO_FRM_INDICUSO_....
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSFrmRec') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrvQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrvPrc';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrvVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoCusUni';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoCusTot';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoPrc';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoDec'; 
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Reordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EhDiluente';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RatifUso';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumLote';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumLaudo';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSFrmDep') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0-ND 1-Dependencia 2=DependType
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cadastro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSMonCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Formula';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EquipAplic';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdTot';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdQSP';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PipCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Diluente';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoAplica';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMed';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerioDd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
//    J� tem no cadastro do PIP
      New(FRCampos);
      FRCampos.Field      := 'DdPostero';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmisStatus';   // Status das emiss�es de formulas filhas ou monitoramento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // > CO_FORMUL_FILHA_MONIT_EmisStat_...
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmisUltDta';   // data de emiss�o
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IndicUso'; // AppListas.CO_FRM_INDICUSO_....
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2'; // Uso definitivo
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSMonRec') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrvQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrvPrc';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrvVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoCusUni';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoCusTot';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoPrc';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoDec';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Reordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValCliDd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      {
      //
      New(FRCampos);
      FRCampos.Field      := 'ValCliDt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      }
      //
      New(FRCampos);
      FRCampos.Field      := 'EhDiluente';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desativado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RatifUso';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
    end else
    if Uppercase(Tabela) = Uppercase('_OS_Mon_Dep_') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0-ND 1-Dependencia 2=DependType
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cadastro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      //
      New(FRCampos);
      FRCampos.Field      := 'NumLote';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumLaudo';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSMonPipDd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';  // sequencia na soma
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dias';   // intervalo em dias
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSFrmAbr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Abrangicie';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSFrmEvo') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercFeito';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSFrmFlhCb') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Formula';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodd';   // Periodo de monitoramento em dias
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmisStatus';   // Status das emiss�es de formulas filhas ou monitoramento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // > CO_FORMUL_FILHA_MONIT_EmisStat_...
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmisUltDta';   // data de emiss�o
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdPostero';   // Intervalos p�steros aos dias da tabela "OSFrmFlhDd" em dias
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSFrmFlhDd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIt2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';  // sequencia na soma
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dias';   // intervalo em dias
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSPrz') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Condicao';  // PediPrzCab
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parcelas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescoPer';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BaseVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescoVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TotaVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Escolhido';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSPipMon') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PipCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Reordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrgLstCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OSFMCbMae'; // OSMonCab.Conta que originou esta OS.OSPipMon
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Insercoes'; // ON_DUPLICATE_KEY
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSPipIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrgLstCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrgLstIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SuperOrd';  // Organizar Linhas de Placa de Monitoramento
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SuperSub';  // Organizar Linhas de Placa de Monitoramento
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SobreOrd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubOrdem';  // Organizar iscas
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'tinyint(1)'; // CO_OSPipIts_...
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TabIdx';  // Indice na tabela!
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      /// In�cio itens copiados do PrgLstIts
      //
      New(FRCampos);
      FRCampos.Field      := 'Funcoes';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dependente';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrgAtrCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Filiacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Relacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NivSeq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pergunta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BinarCad0';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BinarCad1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AcaoPrd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  //  CO_AcaoPrd_...
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LupForma';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  //  CO_LupForma_...
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LupQtdVzs';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      /// FIM itens copiados do PrgLstIts
      //
      //
      /// Respostas do PrgLstIts
      //
      New(FRCampos);
      FRCampos.Field      := 'Respondido'; // CO_Respondido_
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespBin';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';  // 2 = ??? - 1 = Sim - 0 = Nao
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespAtrCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespAtrIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespAtrTxt';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespTxtLvr';
      FRCampos.Tipo       := 'mediumtext';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespPrdBin';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';  // -1 = ??? - 1 = Sim - 0 = Nao
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LupSeqRsp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LupInfVzs';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RespMetodo'; // CO_RespMetodo_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSPipItsPr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDIts';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acao';  // 1=Trocar, 2=Substituir -> CO_OSPipItsPr_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoCusUni';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoCusTot';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoPrc';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoDec';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsoTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValCliDd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EhDiluente';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desativado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumLote';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumLaudo';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSPos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dias';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AplicID';  // C�digo do Pre email por exemplo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Emitido';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Executado';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ExeCodi';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ExeCtrl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Encerrado';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSPrv') then  // Provid�ncias
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo'; // 0 default! Setar depois ao definir.
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrContat';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FormContat';  // Fone, mail, sms, midia social
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente'; // Cliente que fez a solicita��o
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Contato'; // Contato do cliente que fez a solicita��o
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Agente'; // // Agente que recebeu a solicita��o
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';  // Texto da solicita��o
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrvStatus'; // // Status da providencia
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSSta') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Feito';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OSFlhGer') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('GraG1EqAp') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel1';  // GraGru1
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Marca';  // Marca do equipamento
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observacao';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
{
      //
      New(FRCampos);
      FRCampos.Field      := 'PrgLstCab';   // S� compatibilidade!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
}
      //
    end else
    if Uppercase(Tabela) = Uppercase('GraG1EqMo') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel1';  // GraGru1
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Marca';  // Marca do equipamento
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observacao';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrgLstCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('GraG1PrAp') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel1';  // GraGru1
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RegMinSaud'; // Registro no Minist�rio da Sa�de
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Marca';  // Marca do produto
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpInOrca'; // Imprime no or�amento?
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observacao';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FoneCIT';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FoneCEATOX';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Toxicidade';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Concentrac';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AcaoToxica';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Antidoto';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValCliDd'; // N�o usa - deixar por compatibilidade
      FRCampos.Tipo       := 'int(11)';  // com produto de monitoramento
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AtuNumLot';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AtuNumLaud';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('GraG1PrMo') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel1';  // GraGru1
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RegMinSaud'; // Registro no Minist�rio da Sa�de
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Marca';  // Marca do produto
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpInOrca'; // Imprime no or�amento?
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observacao';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      New(FRCampos);
      FRCampos.Field      := 'FoneCIT';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FoneCEATOX';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Toxicidade';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Concentrac';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AcaoToxica';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Antidoto';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValCliDd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AtuNumLot';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AtuNumLaud';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('GraG1PrPA') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'G1PriAtiI';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
    end else if Uppercase(Tabela) = Uppercase('PipCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Equipamento';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OSMonCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MotDesativ';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaAquis';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaDesativ';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrgLstCab'; // Lista de perguntas
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dependenci'; //Depend�ncia onde o PIP est�
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := Intrv Mon DD;  //Dias de intervalo de monitoramento do PIP
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'Ordem';  // Infelizmente deve ordenar aqui por causa dos PIP adiciondos p�steros
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '9999';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Reordem';  //
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PosVdaCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PosVdaIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dias';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AplicID';  // C�digo do Pre email por exemplo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PrgCadPrg') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';  // Para ganhar espa�o no frx
      FRCampos.Tipo       := 'varchar(100)'; // para usar como largura de coluna
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '?????';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);      // Nao deu certo!
      FRCampos.Field      := 'Largura';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '200'; // 2 cm
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PrgLstCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PrgLstIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pergunta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                            //  1 = Nenhuma
      FRCampos.Field      := 'Funcoes';         //  2 = Bin�rio
      FRCampos.Tipo       := 'tinyint(1)';      //  4 = Quantitativo
      FRCampos.Null       := '';                //  8 = Baixa de Produto
      FRCampos.Key        := '';                // 16 = Uso de Atributo
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dependente';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Binario0';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Binario1';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrgAtrCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '999';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Filiacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Relacao'; // 0=PIP(1x), 1=Iscas(n...x)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel';  // Para definir recuo no frx
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NivSeq';  // Para definir ordem no frx
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BinarCad0';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BinarCad1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AcaoPrd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  //  CO_AcaoPrd_...
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LupForma';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  //  CO_LupForma_...
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LupQtdVzs';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Praga_Z') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'NivSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '999';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Especie';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observacao';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArqDir';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Copyright';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArqImg';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Prevencao';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Praga_ZTax') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TaxonIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TaxonGru') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TaxonCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TaxonGru';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TaxonIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TaxonGru';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TaxonCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TaxonNet') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Fonte';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Especie';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Popular1';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Popular2';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Popular3';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Popular4';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Popular5';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Origem';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Filo';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Classe';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Familia';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Copyright';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observacao';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArqDir';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArqImg';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArqTxt';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TxtGeneric') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'mediumtext';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TBugs_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaSincro';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TBugs_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // AGE-EQUIP-001 :: Cadastro de Equipes de Agentes
  New(FRJanelas);
  FRJanelas.ID        := 'AGE-EQUIP-001';
  FRJanelas.Nome      := 'FmAgeEqiCab';
  FRJanelas.Descricao := 'Cadastro de Equipes de Agentes';
  FLJanelas.Add(FRJanelas);
  //
  // AGE-EQUIP-002 :: Adi��o de Agentes em Equipes
  New(FRJanelas);
  FRJanelas.ID        := 'AGE-EQUIP-002';
  FRJanelas.Nome      := 'FmAgeEqiIts';
  FRJanelas.Descricao := 'Adi��o de Agentes em Equipes';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-DEPEN-001 :: Cadastro de Depend�ncias
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-DEPEN-001';
  FRJanelas.Nome      := 'FmDependenci';
  FRJanelas.Descricao := 'Cadastro de Depend�ncias';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-PRAGA-001 :: Cadastro de Esp�cies de Pragas
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-PRAGA-001';
  FRJanelas.Nome      := 'FmPraga_Z';
  FRJanelas.Descricao := 'Cadastro de Esp�cies de Pragas';
  FLJanelas.Add(FRJanelas);
  //
 { J'a est'a implementado no unit Contrat_Tabs
  // CAD-CONTR-001 :: Cadastro de Contratos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-001';
  FRJanelas.Nome      := 'FmContratos';
  FRJanelas.Descricao := 'Cadastro de Contratos';
  FLJanelas.Add(FRJanelas);
  //
}
  // CAD-SUBCL-001 :: Cadastro de Clientes
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-001';
  FRJanelas.Nome      := 'FmCunsCad';
  FRJanelas.Descricao := 'Cadastro de Clientes';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-002 :: Cadastro de Lugar
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-002';
  FRJanelas.Nome      := 'FmSiapTerCad';
  FRJanelas.Descricao := 'Cadastro de Lugar';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-003 :: Cadastro de Local de Aplica��o
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-003';
  FRJanelas.Nome      := 'FmSiapImaCad';
  FRJanelas.Descricao := 'Cadastro de Local de Aplica��o';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-004 :: Cadastro de Residente
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-004';
  FRJanelas.Nome      := 'FmSiapImaRes';
  FRJanelas.Descricao := 'Cadastro de Residente';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-005 :: Cadastro de M�veis / Autom�veis
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-005';
  FRJanelas.Nome      := 'FmMovAmovCad';
  FRJanelas.Descricao := 'Cadastro de M�veis / Autom�veis';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-006 :: Cadastro (Adi��o) de Depend�ncia
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-006';
  FRJanelas.Nome      := 'FmSiapImaDep';
  FRJanelas.Descricao := 'Cadastro (Adi��o) de Depend�ncia';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-007 :: Cadastro de Caixa D`�gua
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-007';
  FRJanelas.Nome      := 'FmSiapImaCxa';
  FRJanelas.Descricao := 'Cadastro de Caixa D`�gua';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-008 :: Cadastro de Fotos Relacionadas ao Cliente
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-008';
  FRJanelas.Nome      := 'FmCunsImgCab';
  FRJanelas.Descricao := 'Cadastro de Fotos Relacionadas ao Cliente';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-009 :: Coment�rios de Fotos Relacionadas ao Cliente
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-009';
  FRJanelas.Nome      := 'FmCunsImgCmt';
  FRJanelas.Descricao := 'Coment�rios de Fotos Relacionadas ao Cliente';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-010 :: C�pia de Lugar
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-010';
  FRJanelas.Nome      := 'FmSiapImaCopy';
  FRJanelas.Descricao := 'C�pia de Lugar';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-011 :: NCT / CAC de Fotos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-011';
  FRJanelas.Nome      := 'FmCunsImgNCT';
  FRJanelas.Descricao := 'NCT / CAC de Fotos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-SUBCL-012 :: Padr�es de OSs Filhas
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-SUBCL-012';
  FRJanelas.Nome      := 'FmSiapTerFlh';
  FRJanelas.Descricao := 'Padr�es de OSs Filhas';
  FLJanelas.Add(FRJanelas);
  //
  // DES-SERVI-001 :: Cadastro de Servi�os
  New(FRJanelas);
  FRJanelas.ID        := 'DES-SERVI-001';
  FRJanelas.Nome      := 'FmDesServico';
  FRJanelas.Descricao := 'Cadastro de Servi�os';
  FLJanelas.Add(FRJanelas);
  //
  // FER-OPCAO-002 :: Op��es Espec�ficas do Aplicativo
  New(FRJanelas);
  FRJanelas.ID        := 'FER-OPCAO-002';
  FRJanelas.Nome      := 'FmOpcoesBugs';
  FRJanelas.Descricao := 'Op��es Espec�ficas do Aplicativo';
  FLJanelas.Add(FRJanelas);
  //
  // FRM-APLIC-001 :: F�rmulas Bases
  New(FRJanelas);
  FRJanelas.ID        := 'FRM-APLIC-001';
  FRJanelas.Nome      := 'FmFormulas';
  FRJanelas.Descricao := 'F�rmulas Bases';
  FLJanelas.Add(FRJanelas);
  //
  // FRM-APLIC-002 :: Itens de F�rmula de Aplica��o
  New(FRJanelas);
  FRJanelas.ID        := 'FRM-APLIC-002';
  FRJanelas.Nome      := 'FmFormulif';
  FRJanelas.Descricao := 'Itens de F�rmula de Aplica��o';
  FLJanelas.Add(FRJanelas);
  //
  // FRM-APLIC-003 :: Abrang�ncia de F�rmula de Aplica��o
  New(FRJanelas);
  FRJanelas.ID        := 'FRM-APLIC-003';
  FRJanelas.Nome      := 'FmFormulia';
  FRJanelas.Descricao := 'Abrang�ncia de F�rmula de Aplica��o';
  FLJanelas.Add(FRJanelas);
  //
  // FRM-APLIC-004 :: F�rmulas Filhas de Monitoramento
  New(FRJanelas);
  FRJanelas.ID        := 'FRM-APLIC-004';
  FRJanelas.Nome      := 'FmFormulFiCb';
  FRJanelas.Descricao := 'F�rmulas Filhas de Monitoramento';
  FLJanelas.Add(FRJanelas);
  //
  // FRM-APLIC-005 :: Intervalos de Visitas de Monitoramento
  New(FRJanelas);
  FRJanelas.ID        := 'FRM-APLIC-005';
  FRJanelas.Nome      := 'FmFormulFiDd';
  FRJanelas.Descricao := 'Intervalos de Visitas de Monitoramento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-001 :: Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-001';
  FRJanelas.Nome      := 'FmOSCab';
  FRJanelas.Descricao := 'Ordem de Servi�o';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-002 :: Ordem de Servi�o - Edita
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-002';
  FRJanelas.Nome      := 'FmOSCad';
  FRJanelas.Descricao := 'Ordem de Servi�o - Edita';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-003 :: Ordem de Servi�o - Servi�o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-003';
  FRJanelas.Nome      := 'FmOSSrv';
  FRJanelas.Descricao := 'Ordem de Servi�o - Servi�o';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-004 :: Ordem de Servi�o - Per�metro de Aplica��o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-004';
  FRJanelas.Nome      := 'FmOSFrmDep';
  FRJanelas.Descricao := 'Ordem de Servi�o - Per�metro de Aplica��o';
  FLJanelas.Add(FRJanelas);
  //
(*
  // GER-OSERV-005 :: Ordem de Servi�o - Per�metro de Monitoramento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-005';
  FRJanelas.Nome      := 'Fm_OS_Mon_Dep_';
  FRJanelas.Descricao := 'Ordem de Servi�o - Per�metro de Monitoramento';
  FLJanelas.Add(FRJanelas);
  //
*)
  // GER-OSERV-006 :: Pragas Alvo (Pr�-atendimento)
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-006';
  FRJanelas.Nome      := 'FmOSCabAlv';
  FRJanelas.Descricao := 'Pragas Alvo (Pr�-atendimento)';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-007 :: Ordem de Servi�o - Pragas Alvo
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-007';
  FRJanelas.Nome      := 'FmOSAlv';
  FRJanelas.Descricao := 'Ordem de Servi�o - Pragas Alvo';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-008 :: Ordem de Servi�o - Aplica��o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-008';
  FRJanelas.Nome      := 'FmOSFrmCab';
  FRJanelas.Descricao := 'Ordem de Servi�o - Aplica��o';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-009 :: Ordem de Servi�o - Item de Aplica��o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-009';
  FRJanelas.Nome      := 'FmOSFrmRec';
  FRJanelas.Descricao := 'Ordem de Servi�o - Item de Aplica��o';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-010 :: Ordem de Servi�o - Monitoramento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-010';
  FRJanelas.Nome      := 'FmOSMonCab';
  FRJanelas.Descricao := 'Ordem de Servi�o - Monitoramento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-011 :: Ordem de Servi�o - Item de Monitoramento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-011';
  FRJanelas.Nome      := 'FmOSMonRec';
  FRJanelas.Descricao := 'Ordem de Servi�o - Item de Monitoramento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-012 :: Ordem de Servi�o - Pesquisa
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-012';
  FRJanelas.Nome      := 'FmOSPsq';
  FRJanelas.Descricao := 'Ordem de Servi�o - Pesquisa';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-013 :: OS - Condi��es de Pagamento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-013';
  FRJanelas.Nome      := 'FmOSPrz';
  FRJanelas.Descricao := 'OS - Condi��es de Pagamento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-014 :: Impress�o de OS
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-014';
  FRJanelas.Nome      := 'FmOSImp';
  FRJanelas.Descricao := 'Impress�o de OS';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-015 :: Faturamento de Servi�o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-015';
  FRJanelas.Nome      := 'FmOSCabFat';
  FRJanelas.Descricao := 'Faturamento de Servi�o';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-016 :: Abrang�ncia da Aplica��o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-016';
  FRJanelas.Nome      := 'FmOSFrmAbr';
  FRJanelas.Descricao := 'Abrang�ncia da Aplica��o';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-017 :: P�s-venda
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-017';
  FRJanelas.Nome      := 'FmOSPos';
  FRJanelas.Descricao := 'P�s-venda';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-018 :: Limpeza de Caixa D`�gua
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-018';
  FRJanelas.Nome      := 'FmOSCxa';
  FRJanelas.Descricao := 'Limpeza de Caixa D`�gua';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-019 :: PIPs a Serem Conferidos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-019';
  FRJanelas.Nome      := 'FmOSPipMon';
  FRJanelas.Descricao := 'PIPs a Serem Conferidos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-020 :: Foto de Caixa D`�gua
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-020';
  FRJanelas.Nome      := 'FmOSCxI';
  FRJanelas.Descricao := 'Foto de Caixa D`�gua';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-021 :: Itens de Observa��es Espec�ficas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-021';
  FRJanelas.Nome      := 'FmOSChk';
  FRJanelas.Descricao := 'Itens de Observa��es Espec�ficas';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-022 :: Ordem de Servi�o - Multi-itens de Monitoramento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-022';
  FRJanelas.Nome      := 'FmOSFrmCabIns';
  FRJanelas.Descricao := 'Ordem de Servi�o - Multi-itens de Monitoramento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-023 :: Aplica��o - Progresso do Servi�o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-023';
  FRJanelas.Nome      := 'FmOSFrmEvo';
  FRJanelas.Descricao := 'Aplica��o - Progresso do Servi�o';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-024 :: O.S. - Escalonamento de Servi�os
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-024';
  FRJanelas.Nome      := 'OSCabXtr';
  FRJanelas.Descricao := 'O.S. - Escalonamento de Servi�os';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-025 :: Solicita��o de Provid�ncia
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-025';
  FRJanelas.Nome      := 'OSPrvAdd';
  FRJanelas.Descricao := 'Solicita��o de Provid�ncia';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-026 :: Sele��o de itens de Provid�ncias
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-026';
  FRJanelas.Nome      := 'OSPrvSel';
  FRJanelas.Descricao := 'Sele��o de itens de Provid�ncias';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-027 :: Gerenciamento de Provid�ncias
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-027';
  FRJanelas.Nome      := 'OSPrvGer';
  FRJanelas.Descricao := 'Gerenciamento de Provid�ncias';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-028 :: Aplica��o - Intervalos de Monitoramentos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-028';
  FRJanelas.Nome      := 'FmOSFrmDds';
  FRJanelas.Descricao := 'Aplica��o - Intervalos de Monitoramentos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-029 :: Pesquisa de OSs M�es
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-029';
  FRJanelas.Nome      := 'FmOSOriPsq';
  FRJanelas.Descricao := 'Pesquisa de OSs M�es';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-030 :: OSs Monitoramento de F�rmulas Filhas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-030';
  FRJanelas.Nome      := 'FmOSFlhGer';
  FRJanelas.Descricao := 'OSs Monitoramento de F�rmulas Filhas';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-031 :: Intervalos Iniciais de Monitoramento de PIPs
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-031';
  FRJanelas.Nome      := 'FmOSMonPipDd';
  FRJanelas.Descricao := 'Intervalos Iniciais de Monitoramento de PIPs';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-032 :: Respostas de Perguntas de Monitoramento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-032';
  FRJanelas.Nome      := 'FmOSPipIts';
  FRJanelas.Descricao := 'Respostas de Perguntas de Monitoramento';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-033 :: Altera��o de Consumo de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-033';
  FRJanelas.Nome      := 'FmOSAltRec';
  FRJanelas.Descricao := 'Altera��o de Consumo de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-034 :: Pr�-defini��o de Loops
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-034';
  FRJanelas.Nome      := 'FmOPIPI_PreQtd';
  FRJanelas.Descricao := 'Pr�-defini��o de Loops';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-035 :: Protocolo de Sa�da de OSs
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-035';
  FRJanelas.Nome      := 'FmOSProtSai';
  FRJanelas.Descricao := 'Protocolo de Sa�da de OSs';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-036 :: Pesquisa OSs x Protocolos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-036';
  FRJanelas.Nome      := 'FmOSProtPsq';
  FRJanelas.Descricao := 'Pesquisa OSs x Protocolos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-OSERV-037 :: Pesquisa Direta de OS
  New(FRJanelas);
  FRJanelas.ID        := 'GER-OSERV-037';
  FRJanelas.Nome      := 'FmOSPs2';
  FRJanelas.Descricao := 'Pesquisa Direta de OS';
  FLJanelas.Add(FRJanelas);
  //
  //  OSS-STATU-001 :: Status de OSs
  New(FRJanelas);
  FRJanelas.ID        := 'OSS-STATU-001';
  FRJanelas.Nome      := 'FmEstatusOSs';
  FRJanelas.Descricao := 'Status de OSs';
  FLJanelas.Add(FRJanelas);
  //
  //  PIP-GEREN-001 :: PIPs
  New(FRJanelas);
  FRJanelas.ID        := 'PIP-GEREN-001';
  FRJanelas.Nome      := 'FmPipCad';
  FRJanelas.Descricao := 'PIPs';
  FLJanelas.Add(FRJanelas);
  //
  //  PIP-GEREN-002 :: Cadastro de PIP
  New(FRJanelas);
  FRJanelas.ID        := 'PIP-GEREN-002';
  FRJanelas.Nome      := 'FmPipRapido';
  FRJanelas.Descricao := 'Cadastro de PIP';
  FLJanelas.Add(FRJanelas);
  //
  //  PIP-GEREN-003 :: Cadastro de V�rios PIPs
  New(FRJanelas);
  FRJanelas.ID        := 'PIP-GEREN-003';
  FRJanelas.Nome      := 'FmPipVarios';
  FRJanelas.Descricao := 'Cadastro de V�rios PIPs';
  FLJanelas.Add(FRJanelas);
  //
  //  PIP-RTVOS-001 :: Reativar PIP em OS
  New(FRJanelas);
  FRJanelas.ID        := 'PIP-RTVOS-001';
  FRJanelas.Nome      := 'FmReativaPIPemOS';
  FRJanelas.Descricao := 'Reativar PIP em OS';
  FLJanelas.Add(FRJanelas);
  //
  //  POS-VENDA-001 :: Pr� Cadastro de P�s Venda
  New(FRJanelas);
  FRJanelas.ID        := 'POS-VENDA-001';
  FRJanelas.Nome      := 'FmPosVdaCab';
  FRJanelas.Descricao := 'Pr� Cadastro de P�s Venda';
  FLJanelas.Add(FRJanelas);
  //
  //  POS-VENDA-002 :: Itens de Pr� Cadastro de P�s Venda
  New(FRJanelas);
  FRJanelas.ID        := 'POS-VENDA-002';
  FRJanelas.Nome      := 'FmPosVdaIts';
  FRJanelas.Descricao := 'Itens de Pr� Cadastro de P�s Venda';
  FLJanelas.Add(FRJanelas);
  //
  //  POS-VENDA-003 :: Gerenciamento de P�s Venda
  New(FRJanelas);
  FRJanelas.ID        := 'POS-VENDA-003';
  FRJanelas.Nome      := 'FmPosVdaGer';
  FRJanelas.Descricao := 'Gerenciamento de P�s Venda';
  FLJanelas.Add(FRJanelas);
  //
  //  PRD-GRUPO-023 :: Produtos de Aplica��o
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GRUPO-023';
  FRJanelas.Nome      := 'FmGraG1PrAp';
  FRJanelas.Descricao := 'Produtos de Aplica��o';
  FLJanelas.Add(FRJanelas);
  //
  //  PRD-GRUPO-024 :: Produtos de Monitoramento
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GRUPO-024';
  FRJanelas.Nome      := 'FmGraG1PrMo';
  FRJanelas.Descricao := 'Produtos de Monitoramento';
  FLJanelas.Add(FRJanelas);
  //
  //  PRD-GRUPO-025 :: Adi��o de Princ�pio Ativo
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GRUPO-025';
  FRJanelas.Nome      := 'FmGraG1PrPA';
  FRJanelas.Descricao := 'Adi��o de Princ�pio Ativo';
  FLJanelas.Add(FRJanelas);
  //
  //  PRD-GRUPO-026 :: Equipamentos de Monitoramento
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GRUPO-026';
  FRJanelas.Nome      := 'FmGraG1EqMo';
  FRJanelas.Descricao := 'Equipamentos de Monitoramento';
  FLJanelas.Add(FRJanelas);
  //
  //  PRD-GRUPO-027 :: Sele��o de Lugar e Op��o
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GRUPO-027';
  FRJanelas.Nome      := 'FmOSNew';
  FRJanelas.Descricao := 'Sele��o de Lugar e Op��o';
  FLJanelas.Add(FRJanelas);
  //
  //  PRD-GRUPO-028 :: Equipamentos de Aplica��o
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GRUPO-028';
  FRJanelas.Nome      := 'FmGraG1EqAp';
  FRJanelas.Descricao := 'Equipamentos de Aplica��o';
  FLJanelas.Add(FRJanelas);
  //
  //  PRD-GRUPO-029 :: Produtos de Monitoramento Mobile
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-GRUPO-029';
  FRJanelas.Nome      := 'FmGraGruMoW';
  FRJanelas.Descricao := 'Produtos de Monitoramento Mobile';
  FLJanelas.Add(FRJanelas);
  //
  //  PRG-LISTA-001 :: Listas de Perguntas
  New(FRJanelas);
  FRJanelas.ID        := 'PRG-LISTA-001';
  FRJanelas.Nome      := 'FmPrgLstCab';
  FRJanelas.Descricao := 'Listas de Perguntas';
  FLJanelas.Add(FRJanelas);
  //
  //  PRG-LISTA-002 :: Item de Lista de Pergunta
  New(FRJanelas);
  FRJanelas.ID        := 'PRG-LISTA-002';
  FRJanelas.Nome      := 'FmPrgLstIts';
  FRJanelas.Descricao := 'Item de Lista de Pergunta';
  FLJanelas.Add(FRJanelas);
  //
{
  //  PRG-LISTA-003 :: Cadastro de Perguntas
  New(FRJanelas);
  FRJanelas.ID        := 'PRG-LISTA-003';
  FRJanelas.Nome      := 'FmPrgCadPrg';
  FRJanelas.Descricao := 'Cadastro de Perguntas';
  FLJanelas.Add(FRJanelas);
  //
}
  //  TAG-CONTR-000 :: Gerenciamento de N�veis de Tags de Contrato
  New(FRJanelas);
  FRJanelas.ID        := 'TAG-CONTR-000';
  FRJanelas.Nome      := 'FmCtrTagNivCab';
  FRJanelas.Descricao := 'Gerenciamento de N�veis de Tags de Contrato';
  FLJanelas.Add(FRJanelas);
  //
  //  TAG-CONTR-999 :: N�vel X de Tags de Contrato
  New(FRJanelas);
  FRJanelas.ID        := 'TAG-CONTR-999';
  FRJanelas.Nome      := 'FmCtrTagNivX';
  FRJanelas.Descricao := 'N�vel X de Tags de Contrato';
  FLJanelas.Add(FRJanelas);
  //
  //  TXT-GENER-001 :: Cadastro de Textos Gen�ricos
  New(FRJanelas);
  FRJanelas.ID        := 'TXT-GENER-001';
  FRJanelas.Nome      := 'FmTxtGeneric';
  FRJanelas.Descricao := 'Cadastro de Textos Gen�ricos';
  FLJanelas.Add(FRJanelas);
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.

