unit OSCabXtr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, dmkValUsu, dmkCheckBox, UnDmkEnums;

type
  TFmOSCabXtr = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    QrOSCab: TmySQLQuery;
    DsOSCab: TDataSource;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit15: TDBEdit;
    EdControle: TdmkEdit;
    Label1: TLabel;
    CkContinuar: TCheckBox;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEmpresa: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabDtaLibFat: TDateTimeField;
    QrOSCabDtaFimFat: TDateTimeField;
    QrOSCabValorServi: TFloatField;
    QrOSCabValorDesco: TFloatField;
    QrOSCabValorOutrs: TFloatField;
    QrOSCabValorTotal: TFloatField;
    QrOSCabDdsPosVda: TIntegerField;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    QrOSCabEntContrat: TIntegerField;
    QrOSCabCondicaoPg: TIntegerField;
    QrOSCabCartEmis: TIntegerField;
    QrOSCabSerNF: TWideStringField;
    QrOSCabNumNF: TIntegerField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabInvalServi: TFloatField;
    QrOSCabInvalDesco: TFloatField;
    QrOSCabInvalOutrs: TFloatField;
    QrOSCabInvalTotal: TFloatField;
    QrOSCabOrcamServi: TFloatField;
    QrOSCabOrcamDesco: TFloatField;
    QrOSCabOrcamOutrs: TFloatField;
    QrOSCabOrcamTotal: TFloatField;
    QrOSCabValiDdOrca: TIntegerField;
    QrOSCabOperacao: TSmallintField;
    QrOSCabExeTxtCli1: TIntegerField;
    QrOSCabNO_FatoGeradr: TWideStringField;
    QrOSCabNO_ESTATUS: TWideStringField;
    QrOSCabNO_SiapTerCad: TWideStringField;
    QrOSCabNO_ENT: TWideStringField;
    TPDtHrIni: TdmkEditDateTimePicker;
    Label52: TLabel;
    EdDtHrIni: TdmkEdit;
    TPDtHrFim: TdmkEditDateTimePicker;
    Label6: TLabel;
    EdDtHrFim: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrInsUpd: TmySQLQuery;
    //
  end;

  var
  FmOSCabXtr: TFmOSCabXtr;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModAgenda;

{$R *.DFM}

procedure TFmOSCabXtr.BtOKClick(Sender: TObject);
var
  DtHrIni, DtHrFim: String;
  Codigo, Controle, Status: Integer;
  //
  Inicio, Termino: TDateTime;
  Nome, Notas: String;
  Cor: Integer;
  Cption: Byte;
  //
  Empresa, Entidade, SiapTerCad, FatoGeradr: Integer;
begin
  Codigo         := QrOSCabCodigo.Value;
  Controle       := EdControle.ValueVariant;
  DtHrIni        := Geral.FDT(TPDtHrIni.Date, 1) + ' ' + EdDtHrIni.Text + ':00';
  DtHrFim        := Geral.FDT(TPDtHrFim.Date, 1) + ' ' + EdDtHrFim.Text + ':00';
  Controle := UMyMod.BPGS1I32('oscabxtr', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'oscabxtr', False, [
  'Codigo', 'DtHrIni', 'DtHrFim'], [
  'Controle'], [
  Codigo, DtHrIni, DtHrFim], [
  Controle], True) then
  begin
////////////////////////////////////////////////////////////////////////////////
///    Criar compromisso na agenda!
////////////////////////////////////////////////////////////////////////////////
    Empresa    := QrOSCabEmpresa.Value;
    Entidade   := QrOSCabEntidade.Value;
    SiapTerCad := QrOSCabSiapTerCad.Value;
    FatoGeradr := QrOSCabFatoGeradr.Value;
    Status     := QrOSCabEstatus.Value;
///
    // Escalonamento
    Inicio  := Geral.STD_109(DtHrIni);
    Termino := Geral.STD_109(DtHrFim);
    Nome    := '';
    Notas   := '';
    Cor     := 0;
    Cption  := 0;
(*  2014-02-18 Mudado "QuestaoCod" do codigo para controle para permitir mais
    //         de um item na agenda!
    DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagEscalonamento, Codigo,
*)  //
    DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagEscalonamento, Controle,
    Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
    Cor, Cption, Status);
////////////////////////////////////////////////////////////////////////////////
///
    if FQrInsUpd <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrInsUpd, Dmod.MyDB);
      FQrInsUpd.Locate('Controle', Controle, []);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdControle.ValueVariant := 0;
      FmOSCabXtr.TPDtHrIni.Date := 0;
      FmOSCabXtr.EdDtHrIni.ValueVariant := 0;
      FmOSCabXtr.TPDtHrFim.Date := 0;
      FmOSCabXtr.EdDtHrFim.ValueVariant := 0;
    end else
      Close;
  end;
end;

procedure TFmOSCabXtr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSCabXtr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSCabXtr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

(*
object LaOSOrigem: TLabel
  Left = 608
  Top = 60
  Width = 54
  Height = 13
  Caption = 'OS Origem:'
  Color = clBtnFace
  Enabled = False
  ParentColor = False
end
object DBEdit9: TDBEdit
  Left = 664
  Top = 56
  Width = 105
  Height = 21
  TabStop = False
  DataField = 'OSOrigem'
  DataSource = DsOSCab
  TabOrder = 9
end

*)
end.
