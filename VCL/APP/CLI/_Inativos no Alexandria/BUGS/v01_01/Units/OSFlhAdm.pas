unit OSFlhAdm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables,
  Vcl.Menus;

type
  TFmOSFlhAdm = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAcao: TBitBtn;
    LaTitulo1C: TLabel;
    QrFrmIts: TmySQLQuery;
    QrFrmItsNO_EmisStatus: TWideStringField;
    QrFrmItsNO_FORMULA: TWideStringField;
    QrFrmItsIDSuperior: TIntegerField;
    QrFrmItsEmisUltDta: TDateField;
    QrFrmItsPeriodd: TIntegerField;
    QrFrmItsEmisStatus: TSmallintField;
    QrFrmItsControle: TIntegerField;
    QrFrmItsConta: TIntegerField;
    QrFrmItsCO_FORMULA: TIntegerField;
    QrFrmItsDdPostero: TIntegerField;
    DsFrmIts: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    QrFrmFil: TmySQLQuery;
    QrFrmFilNO_CLIENTE: TWideStringField;
    QrFrmFilNO_STC: TWideStringField;
    QrFrmFilEntidade: TIntegerField;
    QrFrmFilSiapTerCad: TIntegerField;
    QrFrmFilDtaExeFim: TDateTimeField;
    QrFrmFilNumContrat: TIntegerField;
    QrFrmFilDtaPrxRenw: TDateField;
    QrFrmFilTabela: TLargeintField;
    QrFrmFilSIGLA_TAB: TWideStringField;
    QrFrmFilCodigo: TIntegerField;
    QrFrmFilEmpresa: TIntegerField;
    QrFrmFilPelaEmisUltDta: TWideStringField;
    QrFrmFilPelaDtaExeFim: TWideStringField;
    QrFrmFilPeloFatoGeradr20: TWideStringField;
    QrFrmFilPeloPosGerou0: TWideStringField;
    QrFrmFilPelaFormula: TWideStringField;
    DsFrmFil: TDataSource;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Splitter1: TSplitter;
    QrFrmItsExtenDd: TIntegerField;
    Panel5: TPanel;
    QrFrmFilGrupo: TIntegerField;
    QrFrmFilOpcao: TIntegerField;
    PMAcao: TPopupMenu;
    GerouSim1: TMenuItem;
    GerouNao1: TMenuItem;
    QrFrmFilPosGerou: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrFrmFilBeforeClose(DataSet: TDataSet);
    procedure QrFrmFilAfterScroll(DataSet: TDataSet);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure GerouSim1Click(Sender: TObject);
    procedure GerouNao1Click(Sender: TObject);
    procedure PMAcaoPopup(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
  private
    { Private declarations }
    FOSProlepse: String;
    //
    procedure AlteraOSCab(Campo: String; Valor: Variant);
    procedure ReopenFrmFil(Codigo, Controle: Integer);
    procedure ReopenFrmIts();
  public
    { Public declarations }
    FGrupo, FOpcao, FLugar, FOSCab: Integer;
    //
  end;

  var
  FmOSFlhAdm: TFmOSFlhAdm;

implementation

uses UnMyObjects, Module, CreateBugs, ModuleGeral, ModOS, UnDmkProcFunc,
UnProjGroup_Consts, DmkDAC_PF, OSCab2, Principal, UMySQLModule;

{$R *.DFM}

procedure TFmOSFlhAdm.AlteraOSCab(Campo: String; Valor: Variant);
var
  Codigo, Controle: Integer;
begin
  Codigo   := QrFrmFilCodigo.Value;
  Controle := QrFrmItsControle.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
  Campo], ['Codigo'], [
  Valor], [Codigo], False) then
  begin
    ReopenFrmFil(Codigo, Controle);
  end;
end;

procedure TFmOSFlhAdm.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmOSFlhAdm.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSFlhAdm.DBGrid1DblClick(Sender: TObject);
const
  Atual = 0;
  Disposicao = dispIndefinido;
  ReabreOSCab = True;
var
  Form: TForm;
begin
(*
  if FMultiSel then
  begin
    Geral.MB_Aviso('Duplo clique desabilitado para multi sele��o');
    Exit;
  end;
*)
  if (QrFrmFil.State <> dsInactive) and (QrFrmFil.RecordCount > 0) then
  begin
    FGrupo := QrFrmFilGrupo.Value;
    FOpcao := QrFrmFilOpcao.Value;
    FLugar := QrFrmFilSiapTerCad.Value;
    FOSCab := QrFrmFilCodigo.Value;
    //
    if TFmOSFlhAdm(Self).Owner is TApplication then
    begin
        Form := MyObjects.FormTDICria(TFmOSCab2, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
        TFmOSCab2(Form).LocCod(Atual, FGrupo, Disposicao, FLugar, FOpcao,
        ReabreOSCab, FOSCab);
      Close;
    end
    else
    begin
      Form := MyObjects.FormTDICria(TFmOSCab2, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
      TFmOSCab2(Form).LocCod(Atual, FGrupo, Disposicao, FLugar, FOpcao,
        ReabreOSCab, FOSCab);
    end;
  end;
end;

procedure TFmOSFlhAdm.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
(*
  if Column.FieldName = ?? then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, Qr??.Value);
*)
end;

procedure TFmOSFlhAdm.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSFlhAdm.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenFrmFil(0, 0);
end;

procedure TFmOSFlhAdm.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSFlhAdm.GerouNao1Click(Sender: TObject);
begin
  AlteraOSCab('PosGerou', 0);
end;

procedure TFmOSFlhAdm.GerouSim1Click(Sender: TObject);
begin
  AlteraOSCab('PosGerou', 1);
end;

procedure TFmOSFlhAdm.PMAcaoPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrFrmFil.State <> dsInactive) and (QrFrmFil.RecordCount > 0);
  GerouSim1.Enabled := Habilita and (QrFrmFilPosGerou.Value = 0);
  GerouNao1.Enabled := Habilita and (QrFrmFilPosGerou.Value = 1);
end;

procedure TFmOSFlhAdm.QrFrmFilAfterScroll(DataSet: TDataSet);
begin
  ReopenFrmIts();
end;

procedure TFmOSFlhAdm.QrFrmFilBeforeClose(DataSet: TDataSet);
begin
  QrFrmIts.Close;
end;

procedure TFmOSFlhAdm.ReopenFrmFil(Codigo, Controle: Integer);
var
  Txt: String;
  Tipos, SQL_ANTER, SQL_LJOIN, SQL: String;
begin
  FOSProlepse :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_OSProlepse, DmodG.QrUpdPID1, False);
  Tipos :=
    dmkPF.ArrayToTexto('xxx.EmisStatus', 'NO_EmisStatus', pvPos, True, sListaEmisStatus);
  //
  SQL_ANTER := Geral.ATS([
  'SELECT ',
  'IF(xxx.EmisUltDta < SYSDATE() , "Sim", "N�o") PelaEmisUltDta, ',
  'IF(cab.DtaExeFim > "1900-01-01", "Sim", "N�o") PelaDtaExeFim, ',
  'IF(cab.FatoGeradr=20, "Sim", "N�o") PeloFatoGeradr20, ',
  'IF(cab.PosGerou, "Sim", "N�o") PeloPosGerou0, ',
  'IF( ',
  '          cab.NumContrat=0 ',
  '          AND ',
  '          xxx.EmisStatus IN (0,3) ',
  ', "Sim (A)", ',
  'IF( ',
  '( ',
  '     ( ',
  '          cab.NumContrat<>0 ',
  '          AND ',
  '          ctr.DtaCntrFim < "1900-01-01" ',
  '          AND ',
  '          xxx.EmisStatus <> 2 ',
  '     ) ',
  '), "Sim (C)", "N�o") ) PelaFormula, ',
  'cab.Empresa, cab.Grupo, cab.Opcao, ',
  'xxx.Codigo, xxx.Controle, xxx.Conta, xxx.DdPostero, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,',
  Tipos,
  'stc.Nome NO_STC, frm.Codigo CO_FORMULA, frm.Nome NO_FORMULA, cab.Entidade, ',
  'cab.SiapTerCad, cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw, ',
  'xxx.EmisUltDta, xxx.PerioDd, xxx.ExtenDd, xxx.EmisStatus, ',
  'cab.PosGerou, cab.Ativo, ',
  '']);
  //
  SQL_LJOIN := Geral.ATS([
  'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=xxx.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.formulas frm ON frm.Codigo=xxx.Formula ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=cab.Entidade',
  'LEFT JOIN ' + TMeuDB + '.siaptercad stc ON stc.Codigo=cab.SiapTerCad',
  'LEFT JOIN ' + TMeuDB + '.contratos ctr ON ctr.Codigo=cab.NumContrat ',
  'LEFT JOIN ' + TMeuDB + '.cunscad cun ON cun.Codigo=cab.Entidade ',
  '']);
  //
  //
  SQL := Geral.ATS([
  'DROP TABLE IF EXISTS ' +  FOSProlepse + '; ',
  'CREATE TABLE ' + FOSProlepse,
  '',
  SQL_ANTER,
  //////////////  DIFERENTE  NAS TABELAS !!!  /////////////////////////////////
  Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_FILHA) + ' Tabela, ',
  '"' + CO_FORMUL_FILHA_MONIT_TXT_FILHA + '" SIGLA_TAB, ',
  'xxx.IDIts IDSuperior',
  'FROM ' + TMeuDB + '.osfrmflhcb xxx ',
   /////////////////////////////////////////////////////////////////////////////
  SQL_LJOIN,
  DmModOS.SQL_Filtr(),
  ' ',
  'UNION ',
  ' ',
  SQL_ANTER,
  //////////////  DIFERENTE  NAS TABELAS !!!  /////////////////////////////////
  Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_MONIT) + ' Tabela, ',
  '"' + CO_FORMUL_FILHA_MONIT_TXT_MONIT + '" SIGLA_TAB, ',
  'xxx.Conta IDSuperior ',
  'FROM ' + TMeuDB + '.osmoncab xxx ',
   /////////////////////////////////////////////////////////////////////////////
  SQL_LJOIN,
  DmModOS.SQL_Filtr(),
  ';  ',
  '']);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrmFil, DModG.MyPID_DB, [
  'SELECT DISTINCT PelaEmisUltDta, PelaDtaExeFim, PeloFatoGeradr20, ',
  'PeloPosGerou0, PelaFormula, Empresa, Codigo, NO_CLIENTE, NO_STC, ',
  'Entidade, SiapTerCad, DtaExeFim, NumContrat, DtaPrxRenw, ',
  'Tabela, SIGLA_TAB, Grupo, Opcao, PosGerou ',
  'FROM ' + FOSProlepse,
  '']);
  //
  if Codigo <> 0 then
  begin
    QrFrmFil.Locate('Codigo', Codigo, []);
    if Controle <> 0 then
      QrFrmIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmOSFlhAdm.ReopenFrmIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrmIts, DModG.MyPID_DB, [
  'SELECT ',
  'NO_EmisStatus, NO_FORMULA, IDSuperior, EmisUltDta, Periodd, ExtenDd, ',
  'EmisStatus, Controle, Conta, CO_FORMULA, DdPostero ',
  'FROM ' + FOSProlepse,
  'WHERE Codigo=' + Geral.FF0(QrFrmFilCodigo.Value),
  '']);
end;

end.
