object FmOSPrvSel: TFmOSPrvSel
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-026 :: Sele'#231#227'o de itens de Provid'#234'ncias'
  ClientHeight = 442
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 402
        Height = 32
        Caption = 'Sele'#231#227'o de itens de Provid'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 402
        Height = 32
        Caption = 'Sele'#231#227'o de itens de Provid'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 402
        Height = 32
        Caption = 'Sele'#231#227'o de itens de Provid'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 280
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 280
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 280
        Align = alClient
        TabOrder = 0
        object DBGrid1: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 263
          Align = alClient
          DataSource = DsPesq
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'DtHrContat'
              Title.Caption = 'Data / hora contato'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FormContat'
              Title.Caption = 'Forma de contato'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Contato'
              Title.Caption = 'Contato'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Agente'
              Title.Caption = 'Agente'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Solicita'#231#227'o de provid'#234'ncia'
              Width = 530
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 328
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 179
        Height = 16
        Caption = 'Selecione as linhas desejadas!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 179
        Height = 16
        Caption = 'Selecione as linhas desejadas!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 372
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prv.*, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, '
      'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_Agente, '
      'eco.Nome NO_Contato, fct.Nome NO_FormContat  '
      'FROM osprv prv '
      'LEFT JOIN entidades cli ON cli.Codigo=prv.Cliente '
      'LEFT JOIN entidades age ON age.Codigo=prv.Agente '
      'LEFT JOIN enticontat eco ON eco.Controle=prv.Contato '
      'LEFT JOIN formcontat fct ON fct.Codigo=prv.FormContat '
      ' '
      ' ')
    Left = 468
    Top = 12
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqDtHrContat: TDateTimeField
      FieldName = 'DtHrContat'
    end
    object QrPesqFormContat: TIntegerField
      FieldName = 'FormContat'
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesqContato: TIntegerField
      FieldName = 'Contato'
    end
    object QrPesqAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrPesqNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrPesqNO_Agente: TWideStringField
      FieldName = 'NO_Agente'
      Size = 100
    end
    object QrPesqNO_Contato: TWideStringField
      FieldName = 'NO_Contato'
      Size = 30
    end
    object QrPesqNO_FormContat: TWideStringField
      FieldName = 'NO_FormContat'
      Size = 60
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 496
    Top = 12
  end
end
