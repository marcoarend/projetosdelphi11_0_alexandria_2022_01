object FmOSFrmRec: TFmOSFrmRec
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-008 :: Ordem de Servi'#231'o - Formula'#231#227'o'
  ClientHeight = 661
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 386
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Formula'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 386
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Formula'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 386
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Formula'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 499
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 499
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 93
        Width = 784
        Height = 406
        Align = alClient
        TabOrder = 1
        object GBEdita: TGroupBox
          Left = 2
          Top = 15
          Width = 780
          Height = 389
          Align = alClient
          Caption = ' Itens da F'#243'rmula: '
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object DBGrid1: TDBGrid
            Left = 2
            Top = 15
            Width = 776
            Height = 372
            Align = alClient
            Color = clWhite
            DataSource = DsOSFrmRec
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnKeyDown = DBGrid1KeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'Ordem'
                Title.Caption = 'Seq.'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrvQtd'
                Title.Alignment = taRightJustify
                Title.Caption = 'Qtd de uso'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Alignment = taRightJustify
                Title.Caption = 'C'#243'd [F4]'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_GGX'
                Title.Caption = 'Descri'#231#227'o do Produto [F7]'
                Width = 331
                Visible = True
              end>
          end
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 93
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object Label7: TLabel
            Left = 8
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label5: TLabel
            Left = 108
            Top = 4
            Width = 53
            Height = 13
            Caption = 'ID Servi'#231'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label4: TLabel
            Left = 232
            Top = 4
            Width = 39
            Height = 13
            Caption = 'Servi'#231'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object DBEdit15: TDBEdit
            Left = 48
            Top = 0
            Width = 56
            Height = 21
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsOSSrv
            TabOrder = 0
          end
          object DBEdit8: TDBEdit
            Left = 164
            Top = 0
            Width = 64
            Height = 21
            TabStop = False
            DataField = 'Controle'
            DataSource = DsOSSrv
            TabOrder = 1
          end
          object DBEdit6: TDBEdit
            Left = 276
            Top = 0
            Width = 32
            Height = 21
            TabStop = False
            DataField = 'DesServico'
            DataSource = DsOSSrv
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 308
            Top = 0
            Width = 461
            Height = 21
            TabStop = False
            DataField = 'NO_DesServico'
            DataSource = DsOSSrv
            TabOrder = 3
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 41
          Width = 780
          Height = 50
          Align = alClient
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object Label10: TLabel
            Left = 8
            Top = 4
            Width = 54
            Height = 13
            Caption = 'ID F'#243'rmula:'
            FocusControl = DBEdit1
          end
          object Label11: TLabel
            Left = 128
            Top = 4
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
            FocusControl = DBEdit2
          end
          object Label12: TLabel
            Left = 448
            Top = 4
            Width = 66
            Height = 13
            Caption = 'F'#243'rmula base:'
            FocusControl = DBEdit3
          end
          object Label13: TLabel
            Left = 8
            Top = 28
            Width = 65
            Height = 13
            Caption = 'Equipamento:'
            FocusControl = DBEdit4
          end
          object Label14: TLabel
            Left = 396
            Top = 28
            Width = 81
            Height = 13
            Caption = 'Quantidade total:'
            FocusControl = DBEdit7
          end
          object Label15: TLabel
            Left = 584
            Top = 28
            Width = 83
            Height = 13
            Caption = 'Quantidade QSP:'
            FocusControl = DBEdit9
          end
          object DBEdit1: TDBEdit
            Left = 68
            Top = 0
            Width = 56
            Height = 21
            DataField = 'Conta'
            DataSource = DsOSFrmCab
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 184
            Top = 0
            Width = 257
            Height = 21
            DataField = 'Nome'
            DataSource = DsOSFrmCab
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 520
            Top = 0
            Width = 249
            Height = 21
            DataField = 'NO_FORMULA'
            DataSource = DsOSFrmCab
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 76
            Top = 24
            Width = 317
            Height = 21
            DataField = 'NO_EquipAplic'
            DataSource = DsOSFrmCab
            TabOrder = 3
          end
          object DBEdit7: TDBEdit
            Left = 480
            Top = 24
            Width = 100
            Height = 21
            DataField = 'QtdTot'
            DataSource = DsOSFrmCab
            TabOrder = 4
          end
          object DBEdit9: TDBEdit
            Left = 668
            Top = 24
            Width = 100
            Height = 21
            DataField = 'QtdQSP'
            DataSource = DsOSFrmCab
            TabOrder = 5
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 547
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 591
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        NumGlyphs = 2
      end
    end
  end
  object QrOSSrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT des.Nome NO_DesServico, srv.* '
      'FROM ossrv srv'
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico'
      'WHERE srv.Codigo=:P0')
    Left = 12
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSSrvNO_DesServico: TWideStringField
      FieldName = 'NO_DesServico'
      Size = 60
    end
    object QrOSSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSSrvDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrOSSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsOSSrv: TDataSource
    DataSet = QrOSSrv
    Left = 40
    Top = 12
  end
  object QrFormulas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM formulas'
      'ORDER BY Nome')
    Left = 464
    Top = 12
    object QrFormulasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 492
    Top = 12
  end
  object QrEquipAplic: TmySQLQuery
    Database = Dmod.MyDB
    Left = 520
    Top = 12
    object QrEquipAplicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEquipAplicNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsEquipAplic: TDataSource
    DataSet = QrEquipAplic
    Left = 548
    Top = 12
  end
  object QrOSFrmCab: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrOSFrmCabAfterScroll
    SQL.Strings = (
      'SELECT ofc.*, frm.Nome NO_FORMULA, eqa.Nome NO_EquipAplic '
      'FROM osfrmcab ofc'
      'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula'
      'LEFT JOIN equiaplic eqa ON eqa.Codigo=ofc.EquipAplic'
      'WHERE ofc.Controle=:P0'
      ''
      '')
    Left = 12
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmCabConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOSFrmCabFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrOSFrmCabEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrOSFrmCabQtdTot: TFloatField
      FieldName = 'QtdTot'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSFrmCabQtdQSP: TFloatField
      FieldName = 'QtdQSP'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSFrmCabCusTot: TFloatField
      FieldName = 'CusTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOSFrmCabNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrOSFrmCabNO_EquipAplic: TWideStringField
      FieldName = 'NO_EquipAplic'
      Size = 60
    end
  end
  object DsOSFrmCab: TDataSource
    DataSet = QrOSFrmCab
    Left = 40
    Top = 40
  end
  object TbOSFrmRec: TmySQLTable
    Database = Dmod.MyDB
    BeforeInsert = TbOSFrmRecBeforeInsert
    BeforePost = TbOSFrmRecBeforePost
    AfterPost = TbOSFrmRecAfterPost
    AfterDelete = TbOSFrmRecAfterDelete
    OnNewRecord = TbOSFrmRecNewRecord
    SortFieldNames = 'Ordem'
    TableName = 'osfrmrec'
    Left = 72
    Top = 196
    object TbOSFrmRecNO_GGX: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_GGX'
      LookupDataSet = QrGGXs
      LookupKeyFields = 'Controle'
      LookupResultField = 'NO_PRD_TAM_COR'
      KeyFields = 'GraGruX'
      Size = 120
      Lookup = True
    end
    object TbOSFrmRecCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbOSFrmRecControle: TIntegerField
      FieldName = 'Controle'
    end
    object TbOSFrmRecConta: TIntegerField
      FieldName = 'Conta'
    end
    object TbOSFrmRecIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object TbOSFrmRecGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object TbOSFrmRecPrvQtd: TFloatField
      FieldName = 'PrvQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object TbOSFrmRecPrvPrc: TFloatField
      FieldName = 'PrvPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSFrmRecPrvVal: TFloatField
      FieldName = 'PrvVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSFrmRecUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object TbOSFrmRecUsoPrc: TFloatField
      FieldName = 'UsoPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSFrmRecUsoVal: TFloatField
      FieldName = 'UsoVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSFrmRecUsoDec: TFloatField
      FieldName = 'UsoDec'
      DisplayFormat = '#,###,###,##0.00'
    end
    object TbOSFrmRecUsoTot: TFloatField
      FieldName = 'UsoTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object TbOSFrmRecOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object TbOSFrmRecReordem: TIntegerField
      FieldName = 'Reordem'
    end
  end
  object DsOSFrmRec: TDataSource
    DataSet = TbOSFrmRec
    Left = 100
    Top = 196
  end
  object QrGGXs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      '/*WHERE gg1.Nivel2 IN (-3,-2)'
      'AND gg1.PrdGrupTip=-2*/'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 72
    Top = 224
    object QrGGXsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsGGxs: TDataSource
    DataSet = QrGGXs
    Left = 100
    Top = 224
  end
  object QrReordem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE osfrmrec SET'
      'Ordem=Reordem'
      'WHERE Conta=:P0'
      '')
    Left = 128
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
end
