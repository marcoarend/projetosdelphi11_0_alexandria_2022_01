unit DesServico;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, ComCtrls, dmkRichEdit, UnDmkEnums, Vcl.Menus, Vcl.Grids,
  Vcl.DBGrids, dmkDBGridZTO, UnBugs_Tabs, Variants;

type
  TFmDesServico = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtFormulas: TBitBtn;
    BtServicoBase: TBitBtn;
    BtDesServico: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrDesServico: TmySQLQuery;
    DsDesServico: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrDesServicoCodigo: TIntegerField;
    QrDesServicoNome: TWideStringField;
    QrDesServicoLk: TIntegerField;
    QrDesServicoDataCad: TDateField;
    QrDesServicoDataAlt: TDateField;
    QrDesServicoUserCad: TIntegerField;
    QrDesServicoUserAlt: TIntegerField;
    QrDesServicoAlterWeb: TSmallintField;
    QrDesServicoAtivo: TSmallintField;
    QrDesServicoSigla: TWideStringField;
    QrDesServicoObservacao: TWideMemoField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    EdSigla: TdmkEdit;
    BtTexto: TBitBtn;
    ReObservacao: TdmkRichEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBREObservacao: TDBRichEdit;
    Panel6: TPanel;
    PMDesServico: TPopupMenu;
    IncluiDesServico1: TMenuItem;
    AlteraDesServico1: TMenuItem;
    ExcluiDesServico1: TMenuItem;
    QrPreSrv: TmySQLQuery;
    DsPreSrv: TDataSource;
    QrPreSrvCodigo: TIntegerField;
    QrPreSrvControle: TIntegerField;
    QrPreSrvNome: TWideStringField;
    QrPreSrvLk: TIntegerField;
    QrPreSrvDataCad: TDateField;
    QrPreSrvDataAlt: TDateField;
    QrPreSrvUserCad: TIntegerField;
    QrPreSrvUserAlt: TIntegerField;
    QrPreSrvAlterWeb: TSmallintField;
    QrPreSrvAtivo: TSmallintField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel7: TPanel;
    PMServicoBase: TPopupMenu;
    IncluiServicoBase1: TMenuItem;
    AlteraServicoBase1: TMenuItem;
    ExcluiServicoBase1: TMenuItem;
    PMFormulas: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    QrPreFrm: TmySQLQuery;
    QrPreMon: TmySQLQuery;
    DsPreFrm: TDataSource;
    DsPreMon: TDataSource;
    QrPreAlv: TmySQLQuery;
    DsPreAlv: TDataSource;
    GroupBox1: TGroupBox;
    dmkDBGridZTO2: TdmkDBGridZTO;
    GroupBox2: TGroupBox;
    Splitter1: TSplitter;
    QrPreFrmCodigo: TIntegerField;
    QrPreFrmControle: TIntegerField;
    QrPreFrmConta: TIntegerField;
    QrPreFrmFormula: TIntegerField;
    QrPreFrmLk: TIntegerField;
    QrPreFrmDataCad: TDateField;
    QrPreFrmDataAlt: TDateField;
    QrPreFrmUserCad: TIntegerField;
    QrPreFrmUserAlt: TIntegerField;
    QrPreFrmAlterWeb: TSmallintField;
    QrPreFrmAtivo: TSmallintField;
    QrPreFrmNO_FORMULA: TWideStringField;
    QrPreMonCodigo: TIntegerField;
    QrPreMonControle: TIntegerField;
    QrPreMonConta: TIntegerField;
    QrPreMonFormula: TIntegerField;
    QrPreMonLk: TIntegerField;
    QrPreMonDataCad: TDateField;
    QrPreMonDataAlt: TDateField;
    QrPreMonUserCad: TIntegerField;
    QrPreMonUserAlt: TIntegerField;
    QrPreMonAlterWeb: TSmallintField;
    QrPreMonAtivo: TSmallintField;
    QrPreMonNO_FORMULA: TWideStringField;
    dmkDBGridZTO3: TdmkDBGridZTO;
    QrPreAlvCodigo: TIntegerField;
    QrPreAlvControle: TIntegerField;
    QrPreAlvConta: TIntegerField;
    QrPreAlvNO_Praga_Z: TWideStringField;
    QrPreAlvPraga_Z: TIntegerField;
    GroupBox3: TGroupBox;
    dmkDBGridZTO4: TdmkDBGridZTO;
    BtPragas: TBitBtn;
    Splitter2: TSplitter;
    QrPreSrvGarantiaDd: TIntegerField;
    QrPreSrvHrEvacuar: TIntegerField;
    QrPreSrvHrExecutar: TFloatField;
    QrPreSrvValCalc: TFloatField;
    QrPreSrvValInfo: TFloatField;
    QrPreSrvValDesc: TFloatField;
    QrPreSrvValTota: TFloatField;
    QrPreSrvDetalhes: TWideMemoField;
    QrPreSrvMoniDdTotl: TIntegerField;
    QrPreSrvMoniDdIntv: TIntegerField;
    PMFormulasMon: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    Aplicao1: TMenuItem;
    Monitoramento1: TMenuItem;
    Adicionafrmulademonitoramento1: TMenuItem;
    Removefrmulademonitoramento1: TMenuItem;
    PMFormulasApl: TPopupMenu;
    Adicionafrmuladeaplicao1: TMenuItem;
    Removefrmuladeaplicao1: TMenuItem;
    N1: TMenuItem;
    Cadastrodefrmulas1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDesServicoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDesServicoBeforeOpen(DataSet: TDataSet);
    procedure BtDesServicoClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtTextoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure IncluiDesServico1Click(Sender: TObject);
    procedure AlteraDesServico1Click(Sender: TObject);
    procedure ExcluiDesServico1Click(Sender: TObject);
    procedure PMDesServicoPopup(Sender: TObject);
    procedure QrDesServicoBeforeClose(DataSet: TDataSet);
    procedure BtServicoBaseClick(Sender: TObject);
    procedure IncluiServicoBase1Click(Sender: TObject);
    procedure AlteraServicoBase1Click(Sender: TObject);
    procedure PMServicoBasePopup(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure PMFormulasPopup(Sender: TObject);
    procedure BtFormulasClick(Sender: TObject);
    procedure QrPreSrvBeforeClose(DataSet: TDataSet);
    procedure QrPreSrvAfterScroll(DataSet: TDataSet);
    procedure BtPragasClick(Sender: TObject);
    procedure dmkDBGridZTO4MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ExcluiServicoBase1Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure PMFormulasMonPopup(Sender: TObject);
    procedure Adicionafrmulademonitoramento1Click(Sender: TObject);
    procedure Removefrmulademonitoramento1Click(Sender: TObject);
    procedure PMFormulasAplPopup(Sender: TObject);
    procedure Adicionafrmuladeaplicao1Click(Sender: TObject);
    procedure Removefrmuladeaplicao1Click(Sender: TObject);
    procedure Cadastrodefrmulas1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    //procedure InsUpdServcoBase(SQLType: TSQLType);
    procedure MostraPreSrv(SQLType: TSQLType);
    procedure ReopenPreSrv(Controle: Integer);
    procedure ReopenPreAlv(Conta: Integer);
    procedure ReopenPreFrm(Conta: Integer);
    procedure ReopenPreMon(Conta: Integer);
    procedure SelFormula(GraBugsServi: TGraBugsServi);

  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmDesServico: TFmDesServico;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, MyDBCheck, PreSrv, OSAlv, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDesServico.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmDesServico.MenuItem5Click(Sender: TObject);
begin
  SelFormula(gbsMonitora);
end;

procedure TFmDesServico.MenuItem6Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
    'premon', 'Conta', QrPremonConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrPremon,
      QrPremonConta, QrPremonConta.Value);
    ReopenPremon(Conta);
  end;
end;

procedure TFmDesServico.MostraPreSrv(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmPreSrv, FmPreSrv, afmoNegarComAviso) then
  begin
    FmPreSrv.ImgTipo.SQLType := SQLType;
    FmPreSrv.FQrInsUpd := QrPreSrv;
    //
    FmPreSrv.QrDesServico.Close;
    FmPreSrv.QrDesServico.SQL.Text := QrDesServico.SQL.Text;
    FmPreSrv.QrDesServico.Params := QrDesServico.Params;
    UMyMod.AbreQuery(FmPreSrv.QrDesServico, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmPreSrv.EdControle.ValueVariant   := QrPreSrvControle.Value;
      FmPreSrv.EdNome.Text               := QrPreSrvNome.Value;
      FmPreSrv.EdGarantiaDd.ValueVariant := QrPreSrvGarantiaDd.Value;
      FmPreSrv.EdHrEvacuar.ValueVariant  := QrPreSrvHrEvacuar.Value;
      FmPreSrv.EdHrExecutar.ValueVariant := QrPreSrvHrExecutar.Value;
      //
      FmPreSrv.EdValCalc.ValueVariant    := QrPreSrvValCalc.Value;
      FmPreSrv.EdValInfo.ValueVariant    := QrPreSrvValInfo.Value;
      FmPreSrv.EdValDesc.ValueVariant    := QrPreSrvValDesc.Value;
      FmPreSrv.EdValTota.ValueVariant    := QrPreSrvValTota.Value;
      //
      FmPreSrv.MeDetalhes.Text           := QrPreSrvDetalhes.Value;
      //
      FmPreSrv.EdMoniDdTotl.ValueVariant := QrPreSrvMoniDdTotl.Value;
      FmPreSrv.EdMoniDdIntv.ValueVariant := QrPreSrvMoniDdIntv.Value;
      //
(*
      FmPreSrv.EdDesServico.ValueVariant := QrPreSrvDesServico.Value;
      FmPreSrv.CBDesServico.KeyValue := QrPreSrvDesServico.Value;
      FmPreSrv.RGAutorizado.ItemIndex  := QrPreSrvAutorizado.Value;
      FmPreSrv.RGTudoFeitoM.ItemIndex    := QrPreSrvTudoFeitoM.Value;
*)
      //
    end;
    FmPreSrv.FCriou := True;
    FmPreSrv.ShowModal;
    FmPreSrv.Destroy;
    PageControl1.ActivePageIndex := 0;
  end;
end;

procedure TFmDesServico.PMDesServicoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab  := (QrDesServico.State <> dsInactive) and (QrDesServico.RecordCount > 0);

  AlteraDesServico1.Enabled := Enab;
end;

procedure TFmDesServico.PMFormulasAplPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrDesServico.State <> dsInactive) and (QrDesServico.RecordCount > 0);
  Enab2 := (QrPreFrm.State <> dsInactive) and (QrPreFrm.RecordCount > 0);

  Adicionafrmuladeaplicao1.Enabled := Enab;
  Removefrmuladeaplicao1.Enabled   := Enab and Enab2;
end;

procedure TFmDesServico.PMFormulasMonPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrDesServico.State <> dsInactive) and (QrDesServico.RecordCount > 0);
  Enab2 := (QrPreMon.State <> dsInactive) and (QrPreMon.RecordCount > 0);

  MenuItem5.Enabled := Enab;
  MenuItem6.Enabled := Enab and Enab2;
end;

procedure TFmDesServico.PMFormulasPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrDesServico.State <> dsInactive) and (QrDesServico.RecordCount > 0);
  Enab2 := (QrPreFrm.State <> dsInactive) and (QrPreFrm.RecordCount > 0);
  Enab3 := (QrPreMon.State <> dsInactive) and (QrPreMon.RecordCount > 0);

  Inclui1.Enabled := Enab;
  Altera1.Enabled := Enab and Enab2;

  Adicionafrmulademonitoramento1.Enabled := Enab;
  Removefrmulademonitoramento1.Enabled   := Enab and Enab3
end;

procedure TFmDesServico.PMServicoBasePopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiServicoBase1, QrDesServico);
  MyObjects.HabilitaMenuItemItsUpd(AlteraServicoBase1, QrPreSrv);
  MyObjects.HabilitaMenuItemCabDelC1I3(ExcluiServicoBase1, QrPreSrv, QrPreAlv, QrPreFrm, QrPreMon);
end;

procedure TFmDesServico.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrDesServicoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDesServico.DefParams;
begin
  VAR_GOTOTABELA := 'desservico';
  VAR_GOTOMYSQLTABLE := QrDesServico;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM desservico');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmDesServico.dmkDBGridZTO4MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    BtPragasClick(Self);
end;

procedure TFmDesServico.ExcluiDesServico1Click(Sender: TObject);
begin
  //
end;

procedure TFmDesServico.ExcluiServicoBase1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'presrv', 'Controle', QrPreSrvControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrPreSrv,
      QrPreSrvControle, QrPreSrvControle.Value);
    ReopenPreSrv(Controle);
  end;
end;

procedure TFmDesServico.Cadastrodefrmulas1Click(Sender: TObject);
begin
  FmPrincipal.MostraFormFormulas(nil, 0, 0);
end;

procedure TFmDesServico.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmDesServico.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDesServico.Removefrmuladeaplicao1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
    'prefrm', 'Conta', QrPreFrmConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrPreFrm,
      QrPreFrmConta, QrPreFrmConta.Value);
    ReopenPreFrm(Conta);
  end;
end;

procedure TFmDesServico.Removefrmulademonitoramento1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
    'premon', 'Conta', QrPremonConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrPremon,
      QrPremonConta, QrPremonConta.Value);
    ReopenPremon(Conta);
  end;
end;

procedure TFmDesServico.ReopenPreAlv(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPreAlv, Dmod.MyDB, [
  'SELECT prc.Nome NO_Praga_Z, osa.* ',
  'FROM prealv osa ',
  'LEFT JOIN praga_z prc ON prc.Codigo=osa.Praga_Z ',
  'WHERE osa.Controle=' + Geral.FF0(QrPreSrvControle.Value),
  '']);
  //
  QrPreAlv.Locate('Conta', Conta, []);
end;

procedure TFmDesServico.ReopenPreFrm(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPreFrm, Dmod.MyDB, [
  'SELECT prf.*, frm.Nome NO_FORMULA ',
  'FROM prefrm prf ',
  'LEFT JOIN formulas frm ON frm.Codigo=prf.Formula ',
  'WHERE prf.Controle=' + Geral.FF0(QrPreSrvControle.Value),
  '']);
  //
  QrPreFrm.Locate('Conta', Conta, []);
end;

procedure TFmDesServico.ReopenPreMon(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPreMon, Dmod.MyDB, [
  'SELECT prf.*, frm.Nome NO_FORMULA ',
  'FROM premon prf ',
  'LEFT JOIN formulas frm ON frm.Codigo=prf.Formula ',
  'WHERE prf.Controle=' + Geral.FF0(QrPreSrvControle.Value),
  '']);
  //
  QrPreMon.Locate('Conta', Conta, []);
end;

procedure TFmDesServico.ReopenPreSrv(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPreSrv, Dmod.MyDB, [
  'SELECT *',
  'FROM presrv',
  'WHERE Codigo=' + Geral.FF0(QrDesServicoCodigo.Value),
  '']);
  //
  QrPreSrv.Locate('Controle', Controle, []);
end;

procedure TFmDesServico.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmDesServico.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDesServico.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDesServico.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDesServico.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDesServico.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDesServico.BtTextoClick(Sender: TObject);
begin
  MyObjects.EditaTextoRichEdit(REObservacao, 10, 'Tahoma', []);
end;

procedure TFmDesServico.Adicionafrmuladeaplicao1Click(Sender: TObject);
begin
  SelFormula(gbsAplica);
end;

procedure TFmDesServico.Adicionafrmulademonitoramento1Click(Sender: TObject);
begin
  SelFormula(gbsMonitora);
end;

procedure TFmDesServico.Altera1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
    'prefrm', 'Conta', QrPreFrmConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrPreFrm,
      QrPreFrmConta, QrPreFrmConta.Value);
    ReopenPreFrm(Conta);
  end;
end;

procedure TFmDesServico.AlteraDesServico1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrDesServico, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'desservico');
end;

procedure TFmDesServico.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDesServicoCodigo.Value;
  Close;
end;

procedure TFmDesServico.BtServicoBaseClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMServicoBase, BtServicoBase);
end;

procedure TFmDesServico.AlteraServicoBase1Click(Sender: TObject);
begin
  MostraPreSrv(stUpd);
  //InsUpdServcoBase(stUpd);
end;

procedure TFmDesServico.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('desservico', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrDesServicoCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'desservico', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmDesServico.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'desservico', 'Codigo');
end;

procedure TFmDesServico.BtDesServicoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDesServico, BtDesServico);
end;

procedure TFmDesServico.BtFormulasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFormulas, BtFormulas);
end;

procedure TFmDesServico.BtPragasClick(Sender: TObject);
const
  SQLType = stIns;
begin
  if (QrDesServico.State <> dsInactive) and (QrDesServico.RecordCount > 0) then
  begin
    if (QrPreSrv.State <> dsInactive) and (QrPreSrv.RecordCount > 0) then
    begin
      if DBCheck.CriaFm(TFmOSAlv, FmOSAlv, afmoNegarComAviso) then
      begin
        FmOSAlv.ImgTipo.SQLType := SQLType;
        FmOSAlv.FTabela   := 'prealv';
        FmOSAlv.FQrOSAlv  := QrPreAlv;
        FmOSAlv.FDatabase := Dmod.MyDB;
        FmOSAlv.FQrInsere := Dmod.QrUpd;
        FmOSAlv.FCodigo   := QrDesServicoCodigo.Value;
        FmOSAlv.FControle := QrPreSrvControle.Value;
        //
        FmOSAlv.QrOSSrv.Close;
        //FmOSAlv.QrOSSrv.SQL.Text := '';
        //FmOSAlv.QrOSSrv.Params := Qr??.Params;
        //UMyMod.AbreQuery(FmOSAlv.QrOSSrv, Dmod.MyDB);
        //
        { // N�o tem!
        if SQLType = stUpd then
        begin
          // N�o tem!
        end;
        }
        FmOSAlv.ShowModal;
        FmOSAlv.Destroy;
      end;
    end else
      Geral.MB_Aviso('Voc� deve incluir um servi�o base!')
  end;
end;

procedure TFmDesServico.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType              := stLok;
  ReObservacao.Align           := alClient;
  PageControl1.Align           := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  dmkDBGridZTO2.PopupMenu := PMFormulasApl;
  dmkDBGridZTO3.PopupMenu := PMFormulasMon;
end;

procedure TFmDesServico.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrDesServicoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDesServico.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmDesServico.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmDesServico.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrDesServicoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDesServico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDesServico.QrDesServicoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  ReopenPreSrv(0);
end;

procedure TFmDesServico.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDesServico.SbQueryClick(Sender: TObject);
begin
  LocCod(QrDesServicoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'desservico', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmDesServico.SelFormula(GraBugsServi: TGraBugsServi);
const
  Aviso  = '...';
  Titulo = 'Sele��o de f�rmula';
  Prompt = 'Informe a f�rmula: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Formula: Variant;
  SQL_WHERE, Tabela: String;
var
  Codigo, Controle, Conta: Integer;
begin
  case GraBugsServi of
    //gbsIndef,
    //gbsAplEMon  :
    gbsAplica   :
    begin
      Tabela := 'prefrm';
      SQL_WHERE := 'WHERE frm.Aplicacao & 1 ';
    end;
    gbsMonitora :
    begin
      Tabela := 'premon';
      SQL_WHERE := 'WHERE frm.Aplicacao & 2 ';
    end;
    //gbsMonMulti :
    else          SQL_WHERE := '';
  end;
  Formula := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, Nome ' + Campo,
    'FROM formulas frm ',
    SQL_WHERE,
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, True);
  if Formula <> Null then
  begin
    Codigo         := QrDesServicoCodigo.Value;
    Controle       := QrPreSrvControle.Value;
    Conta          := UMyMod.BPGS1I32(Tabela, 'Conta', '', '', tsPos, stIns, 0);
    //Formula        := ;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False, [
    'Codigo', 'Controle', 'Formula'], [
    'Conta'], [
    Codigo, Controle, Formula], [
    Conta], True) then
    begin
      case GraBugsServi of
        gbsAplica   : ReopenPreFrm(Conta);
        gbsMonitora : ReopenPreMon(Conta);
        else Geral.MB_Aviso('Tabela n�o informada para reabertura!');
      end;
    end;
  end;
end;

procedure TFmDesServico.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDesServico.Inclui1Click(Sender: TObject);
begin
  SelFormula(gbsAplica);
end;

procedure TFmDesServico.IncluiDesServico1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrDesServico, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'desservico');
end;

procedure TFmDesServico.IncluiServicoBase1Click(Sender: TObject);
begin
  MostraPreSrv(stIns);
  //InsUpdServcoBase(stIns);
end;

(*
procedure TFmDesServico.InsUpdServcoBase(SQLType: TSQLType);
var
  Nome: String;
  Codigo, Controle: Integer;
begin
  if SQLType = stUpd then
  begin
    Nome := QrPreSrvNome.Value;
    Controle := QrPreSrvControle.Value;
  end else
    Nome := QrDesServicoNome.Value;
  //
  if InputQuery('Servi�o Base', 'Informe o nome do servi�o base:', Nome) then
  begin
    Codigo         := QrDesServicoCodigo.Value;
    Controle       := UMyMod.BPGS1I32('presrv', 'Controle', '', '', tsPos,
      SQLType, Controle);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'presrv', False, [
    'Codigo', 'Nome'], [
    'Controle'], [
    Codigo, Nome], [
    Controle], True) then
    begin
      ReopenPreSrv(Controle);
      //
      if SQLType = stIns then
      begin
        // ?
      end;
    end;
  end;
end;
*)

procedure TFmDesServico.QrDesServicoBeforeClose(DataSet: TDataSet);
begin
  QrPreSrv.Close;
end;

procedure TFmDesServico.QrDesServicoBeforeOpen(DataSet: TDataSet);
begin
  QrDesServicoCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmDesServico.QrPreSrvAfterScroll(DataSet: TDataSet);
begin
  ReopenPreAlv(0);
  ReopenPreFrm(0);
  ReopenPreMon(0);

end;

procedure TFmDesServico.QrPreSrvBeforeClose(DataSet: TDataSet);
begin
  QrPreAlv.Close;
  QrPreFrm.Close;
  QrPreMon.Close;
end;

end.

