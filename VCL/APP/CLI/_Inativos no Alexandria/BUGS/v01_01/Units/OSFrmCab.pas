unit OSFrmCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, UnBugs_Tabs,
  dmkRadioGroup, dmkImage, UnDmkEnums;

type
  TFmOSFrmCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit15: TDBEdit;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    DsOSSrv: TDataSource;
    QrFormulas: TmySQLQuery;
    DsFormulas: TDataSource;
    GBEdita: TGroupBox;
    Label1: TLabel;
    Label9: TLabel;
    SbFormulas: TSpeedButton;
    Label3: TLabel;
    EdConta: TdmkEdit;
    EdNome: TdmkEdit;
    CBFormula: TdmkDBLookupComboBox;
    EdFormula: TdmkEditCB;
    Label2: TLabel;
    EdEquipAplic: TdmkEditCB;
    CBEquipAplic: TdmkDBLookupComboBox;
    SbEquipAplic: TSpeedButton;
    EdQtdTot: TdmkEdit;
    Label6: TLabel;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrEquipAplic: TmySQLQuery;
    DsEquipAplic: TDataSource;
    QrEquipAplicControle: TIntegerField;
    QrEquipAplicNome: TWideStringField;
    QrFormulasEquipAplic: TIntegerField;
    QrFormulasQtdTot: TFloatField;
    QrFormulasAplicacao: TIntegerField;
    RGDiluente: TdmkRadioGroup;
    QrFormulasDiluente: TSmallintField;
    QrTipoAplica: TmySQLQuery;
    QrTipoAplicaCodigo: TIntegerField;
    QrTipoAplicaNome: TWideStringField;
    DsTipoAplica: TDataSource;
    Label8: TLabel;
    EdTipoAplica: TdmkEditCB;
    CBTipoAplica: TdmkDBLookupComboBox;
    SBTipoAplica: TSpeedButton;
    QrFormulasTipoAplica: TIntegerField;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    Label11: TLabel;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrFormulasCU_UNIDMED: TIntegerField;
    QrFormulasSIGLA: TWideStringField;
    RGIndicUso: TdmkRadioGroup;
    QrEquipAplicNivel1: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbFormulasClick(Sender: TObject);
    procedure SbEquipAplicClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdFormulaChange(Sender: TObject);
    procedure SBTipoAplicaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CBFormulaExit(Sender: TObject);
  private
    { Private declarations }
    //F_OS_Alv_: String;
    //

  public
    { Public declarations }
    FCodigo, FControle, FConta: Integer;
    FQrOSFrmCab: TmySQLQuery;
  end;

  var
  FmOSFrmCab: TFmOSFrmCab;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral,
  Principal, ModProd, UnidMed, MyDBCheck, UnOSApp_PF, UnGrade_Jan;

{$R *.DFM}

procedure TFmOSFrmCab.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, Conta, Formula, EquipAplic, TipoAplica, Diluente,
  UnidMed, IndicUso: Integer;
  //, QtdQSPCusTot,
  QtdTot: Double;
begin
  Codigo         := FCodigo;
  Controle       := FControle;
  Nome           := EdNome.Text;
  Formula        := EdFormula.ValueVariant;
  EquipAplic     := EdEquipAplic.ValueVariant;
  QtdTot         := EdQtdTot.ValueVariant;
  //QtdQSP         := EdQtdQSP.ValueVariant;
  //CusTot         := ;
  TipoAplica     := EdTipoAplica.ValueVariant;
  Diluente       := RGDiluente.ItemIndex;
  IndicUso       := RGIndicUso.ItemIndex;
  if EdUnidMed.ValueVariant <> 0 then
    UnidMed := QrUnidMedCodigo.Value
  else
    Unidmed := 0;
  //
  if MyObjects.FIC(EdNome.Text = '', EdNome, 'Informe a descri��o!') then
    Exit;
  //
  if MyObjects.FIC(RGDiluente.ItemIndex = 0, EdNome, 'Defina o diluente!') then
    Exit;
  //
  if MyObjects.FIC(IndicUso = 0, EdNome, 'Defina a indica��o de uso!') then
    Exit;
  //
  Conta := EdConta.ValueVariant;
  Conta := UMyMod.BPGS1I32('osfrmcab', 'Conta', '', '', tsDef,
    ImgTipo.SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osfrmcab', False, [
  'Codigo', 'Controle', 'Nome',
  'Formula', 'EquipAplic', 'QtdTot'(*,
  'QtdQSP', 'CusTot'*), 'TipoAplica',
  'Diluente', 'UnidMed', 'IndicUso'], [
  'Conta'], [
  Codigo, Controle, Nome,
  Formula, EquipAplic, QtdTot(*,
  QtdQSP, CusTot*), TipoAplica,
  Diluente, UnidMed, IndicUso], [
  Conta], True) then
  begin
    OSApp_PF.ReopenOSFrmCab(FQrOSFrmCab, Controle, Conta);
    if ImgTipo.SQLType = stIns then
    begin
      // Necessario para pos fechamento
      FConta := Conta;
      //
      //  FIM 2013-07-28
{
      OSApp_PF.CopiaItensDaFormulaBase(
        gbsAplica, Formula, Codigo, Controle, Conta, False);
}
      OSApp_PF.CopiaItensDaFormulaBase(
        gbsAplica, Formula, Codigo, Controle, Conta, True);
      //  FIM 2013-07-28
      //
      OSApp_PF.ReopenOSFrmCab(FQrOSFrmCab, Controle, Conta);
    end;
    Close;
  end;
end;

procedure TFmOSFrmCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSFrmCab.CBFormulaExit(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    if EdNome.ValueVariant = '' then
      EdNome.ValueVariant := CBFormula.Text;
  end;
end;

procedure TFmOSFrmCab.EdFormulaChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdQtdTot.ValueVariant := QrFormulasQtdTot.Value;
    EdEquipAplic.ValueVariant := QrFormulasEquipAplic.Value;
    CBEquipAplic.KeyValue := QrFormulasEquipAplic.Value;
    EdTipoAplica.ValueVariant := QrFormulasTipoAplica.Value;
    CBTipoAplica.KeyValue := QrFormulasTipoAplica.Value;
    RGDiluente.ItemIndex := QrFormulasDiluente.Value;
    EdSigla.Text := QrFormulasSIGLA.Value;
    EdUnidMed.ValueVariant := QrFormulasCU_UnidMed.Value;
    CBUnidMed.KeyValue := QrFormulasCU_UnidMed.Value;
    //
  end;
end;

procedure TFmOSFrmCab.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdNome.Text := CBFormula.Text;
end;

procedure TFmOSFrmCab.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmOSFrmCab.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmOSFrmCab.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmOSFrmCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSFrmCab.FormCreate(Sender: TObject);
begin
  FConta := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT frm.*, med.CodUsu CU_UNIDMED, med.SIGLA ',
  'FROM formulas frm ',
  'LEFT JOIN unidmed med ON med.Codigo=frm.UnidMed ',
  //'WHERE frm.Aplicacao & 1 ',
  'ORDER BY frm.Nome ',
  '']);
  UnDmkDAC_PF.AbreQuery(QrTipoAplica, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  OSApp_PF.ReopenEquipAplic(QrEquipAplic, gbsAplica);
end;

procedure TFmOSFrmCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSFrmCab.SbEquipAplicClick(Sender: TObject);
var
  EquipAplic, Nivel1: Integer;
begin
  EquipAplic   := EdEquipAplic.ValueVariant;
  VAR_CADASTRO := 0;

  if EquipAplic <> 0 then
    Nivel1 := QrEquipAplicNivel1.Value
  else
    Nivel1 := 0;

  Grade_Jan.MostraFormGraGruN(Nivel1);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEquipAplic, CBEquipAplic, QrEquipAplic, VAR_CADASTRO, 'Controle');
    EdEquipAplic.SetFocus;
  end;
  (*
  //FmPrincipal.MostraFormGraG1EqAp();
  FmPrincipal.MostraFormGraG1Equi(gbsAplica, 0);
  UMyMod.SetaCodigoPesquisado(EdEquipAplic, CBEquipAplic, QrEquipAplic,
    VAR_CADASTRO, 'Controle');
  *)
end;

procedure TFmOSFrmCab.SbFormulasClick(Sender: TObject);
var
  Formula: Integer;
begin
  VAR_CADASTRO := 0;
  Formula      := EdFormula.ValueVariant;
  //
  FmPrincipal.MostraFormFormulas(nil, Formula, 0);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFormula, CBFormula, QrFormulas, VAR_CADASTRO);
    EdFormula.SetFocus;
  end;
end;


procedure TFmOSFrmCab.SBTipoAplicaClick(Sender: TObject);
begin
  FmPrincipal.MostraFormTipoAplica(0);
  UMyMod.SetaCodigoPesquisado(EdTipoAplica, CBTipoAplica, QrTipoAplica, VAR_CADASTRO);
end;

procedure TFmOSFrmCab.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    end;
  end;
end;

end.
