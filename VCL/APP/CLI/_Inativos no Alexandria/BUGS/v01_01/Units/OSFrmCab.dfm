object FmOSFrmCab: TFmOSFrmCab
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-008 :: Ordem de Servi'#231'o - Aplica'#231#227'o'
  ClientHeight = 640
  ClientWidth = 769
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 769
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 710
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 651
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 424
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 424
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 424
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 769
    Height = 441
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 769
      Height = 441
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 46
        Width = 769
        Height = 395
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 1
        object GBEdita: TGroupBox
          Left = 2
          Top = 18
          Width = 765
          Height = 239
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Label1: TLabel
            Left = 20
            Top = 20
            Width = 16
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label9: TLabel
            Left = 20
            Top = 74
            Width = 91
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o [F4]:'
            Color = clBtnFace
            ParentColor = False
          end
          object SbFormulas: TSpeedButton
            Left = 724
            Top = 39
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SbFormulasClick
          end
          object Label3: TLabel
            Left = 94
            Top = 20
            Width = 134
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'F'#243'rmula de refer'#234'ncia:'
          end
          object Label2: TLabel
            Left = 20
            Top = 128
            Width = 143
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Equipamento aplicador:'
          end
          object SbEquipAplic: TSpeedButton
            Left = 724
            Top = 148
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SbEquipAplicClick
          end
          object Label6: TLabel
            Left = 645
            Top = 182
            Width = 101
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade total:'
          end
          object Label8: TLabel
            Left = 20
            Top = 182
            Width = 113
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo de aplica'#231#227'o:'
          end
          object SBTipoAplica: TSpeedButton
            Left = 300
            Top = 202
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SBTipoAplicaClick
          end
          object Label11: TLabel
            Left = 330
            Top = 182
            Width = 149
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Unidade de Medida [F3]:'
          end
          object SpeedButton5: TSpeedButton
            Left = 617
            Top = 201
            Width = 25
            Height = 27
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton5Click
          end
          object EdConta: TdmkEdit
            Left = 20
            Top = 39
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Conta'
            UpdCampo = 'Conta'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNome: TdmkEdit
            Left = 20
            Top = 94
            Width = 730
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnKeyDown = EdNomeKeyDown
          end
          object CBFormula: TdmkDBLookupComboBox
            Left = 162
            Top = 39
            Width = 558
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsFormulas
            TabOrder = 2
            OnExit = CBFormulaExit
            dmkEditCB = EdFormula
            QryCampo = 'Formula'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdFormula: TdmkEditCB
            Left = 94
            Top = 39
            Width = 68
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Formula'
            UpdCampo = 'Formula'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFormulaChange
            DBLookupComboBox = CBFormula
            IgnoraDBLookupComboBox = False
          end
          object EdEquipAplic: TdmkEditCB
            Left = 20
            Top = 148
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EquipAplic'
            UpdCampo = 'EquipAplic'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEquipAplic
            IgnoraDBLookupComboBox = False
          end
          object CBEquipAplic: TdmkDBLookupComboBox
            Left = 89
            Top = 148
            Width = 631
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsEquipAplic
            TabOrder = 5
            dmkEditCB = EdEquipAplic
            QryCampo = 'EquipAplic'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdQtdTot: TdmkEdit
            Left = 645
            Top = 202
            Width = 105
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'QtdTot'
            UpdCampo = 'QtdTot'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTipoAplica: TdmkEditCB
            Left = 20
            Top = 202
            Width = 49
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'TipoAplica'
            UpdCampo = 'TipoAplica'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBTipoAplica
            IgnoraDBLookupComboBox = False
          end
          object CBTipoAplica: TdmkDBLookupComboBox
            Left = 69
            Top = 202
            Width = 233
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsTipoAplica
            TabOrder = 7
            dmkEditCB = EdTipoAplica
            QryCampo = 'TipoAplica'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdUnidMed: TdmkEditCB
            Left = 330
            Top = 202
            Width = 49
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdUnidMedChange
            DBLookupComboBox = CBUnidMed
            IgnoraDBLookupComboBox = False
          end
          object EdSigla: TdmkEdit
            Left = 379
            Top = 202
            Width = 49
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdSiglaChange
            OnExit = EdSiglaExit
          end
          object CBUnidMed: TdmkDBLookupComboBox
            Left = 428
            Top = 202
            Width = 189
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsUnidMed
            TabOrder = 10
            dmkEditCB = EdUnidMed
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object RGDiluente: TdmkRadioGroup
          Left = 2
          Top = 257
          Width = 765
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' Diluente: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'N'#227'o tem'
            #193'gua'
            'Produto indicado')
          TabOrder = 1
          QryCampo = 'Diluente'
          UpdCampo = 'Diluente'
          UpdType = utYes
          OldValor = 0
        end
        object RGIndicUso: TdmkRadioGroup
          Left = 2
          Top = 316
          Width = 765
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' Indica'#231#227'o de uso:'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'Previs'#227'o de uso'
            'Uso definitivo')
          TabOrder = 2
          QryCampo = 'IndicUso'
          UpdCampo = 'IndicUso'
          UpdType = utYes
          OldValor = 0
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 769
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        object Label7: TLabel
          Left = 10
          Top = 15
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label4: TLabel
          Left = 286
          Top = 15
          Width = 49
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Servi'#231'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label5: TLabel
          Left = 133
          Top = 15
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID Servi'#231'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdit6: TDBEdit
          Left = 340
          Top = 10
          Width = 39
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'DesServico'
          DataSource = DsOSSrv
          TabOrder = 1
        end
        object DBEdit5: TDBEdit
          Left = 379
          Top = 10
          Width = 335
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'NO_DesServico'
          DataSource = DsOSSrv
          TabOrder = 2
        end
        object DBEdit8: TDBEdit
          Left = 202
          Top = 10
          Width = 79
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Controle'
          DataSource = DsOSSrv
          TabOrder = 3
        end
        object DBEdit15: TDBEdit
          Left = 59
          Top = 10
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsOSSrv
          TabOrder = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 500
    Width = 769
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 765
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 554
    Width = 769
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 590
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 588
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOSSrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT des.Nome NO_DesServico, srv.* '
      'FROM ossrv srv'
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico'
      'WHERE srv.Codigo=:P0')
    Left = 12
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSSrvNO_DesServico: TWideStringField
      FieldName = 'NO_DesServico'
      Size = 60
    end
    object QrOSSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSSrvDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrOSSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsOSSrv: TDataSource
    DataSet = QrOSSrv
    Left = 40
    Top = 12
  end
  object QrFormulas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frm.*, med.CodUsu CU_UNIDMED, med.SIGLA'
      'FROM formulas frm'
      'LEFT JOIN unidmed med ON med.Codigo=frm.UnidMed'
      'WHERE frm.Aplicacao & 1'
      'ORDER BY frm.Nome')
    Left = 464
    Top = 12
    object QrFormulasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFormulasEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrFormulasQtdTot: TFloatField
      FieldName = 'QtdTot'
    end
    object QrFormulasAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrFormulasDiluente: TSmallintField
      FieldName = 'Diluente'
    end
    object QrFormulasTipoAplica: TIntegerField
      FieldName = 'TipoAplica'
    end
    object QrFormulasCU_UNIDMED: TIntegerField
      FieldName = 'CU_UNIDMED'
      Required = True
    end
    object QrFormulasSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 492
    Top = 12
  end
  object QrEquipAplic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Nome, gg1.Nivel1'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg2.Nivel3'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg3.PrdGrupTip')
    Left = 520
    Top = 12
    object QrEquipAplicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEquipAplicNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrEquipAplicNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object DsEquipAplic: TDataSource
    DataSet = QrEquipAplic
    Left = 548
    Top = 12
  end
  object QrTipoAplica: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tipoaplica'
      'ORDER BY Nome')
    Left = 464
    Top = 40
    object QrTipoAplicaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTipoAplicaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTipoAplica: TDataSource
    DataSet = QrTipoAplica
    Left = 492
    Top = 40
  end
  object QrUnidMed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 68
    Top = 12
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 96
    Top = 12
  end
end
