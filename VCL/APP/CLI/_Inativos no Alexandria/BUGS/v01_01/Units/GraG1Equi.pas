unit GraG1Equi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkRichEdit, UnBugs_Tabs, UnDmkEnums, dmkCheckBox, Variants,
  dmkValUsu;

type
  TFmGraG1Equi = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrMarcas: TmySQLQuery;
    QrMarcasControle: TIntegerField;
    QrMarcasNO_MARCA_FABR: TWideStringField;
    DsMarcas: TDataSource;
    BtTexto: TBitBtn;
    QrPrgLstCab: TmySQLQuery;
    DsPrgLstCab: TDataSource;
    QrPrgLstCabCodigo: TIntegerField;
    QrPrgLstCabNome: TWideStringField;
    DsPrgLstIts: TDataSource;
    QrPrgLstIts: TmySQLQuery;
    QrPrgLstItsControle: TIntegerField;
    QrPrgLstItsNO_PERGUNTA: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    SbMarcas: TSpeedButton;
    EdNivel1: TdmkEdit;
    EdNome: TdmkEdit;
    EdMarca: TdmkEditCB;
    CBMarca: TdmkDBLookupComboBox;
    PnPrgLstCab: TPanel;
    Label1: TLabel;
    SbPrgLstCab: TSpeedButton;
    SbVetorSVG: TSpeedButton;
    Label2: TLabel;
    Label3: TLabel;
    SBPrgICorSVG: TSpeedButton;
    Label5: TLabel;
    EdPrgLstCab: TdmkEditCB;
    CBPrgLstCab: TdmkDBLookupComboBox;
    CkNaoUsaPrdt: TdmkCheckBox;
    EdVetorArq: TdmkEdit;
    EdPrgICorSVG: TdmkEditCB;
    CBPrgICorSVG: TdmkDBLookupComboBox;
    EdDiasCorSVG: TdmkEdit;
    GroupBox1: TGroupBox;
    Verde: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    EdQtCorSVGgg: TdmkEdit;
    EdQtCorSVGgr: TdmkEdit;
    EdQtCorSVGrg: TdmkEdit;
    EdQtCorSVGrr: TdmkEdit;
    EdQtCorSVGrb: TdmkEdit;
    EdQtCorSVGbr: TdmkEdit;
    TabSheet2: TTabSheet;
    MeVetorSVG: TMemo;
    TabSheet3: TTabSheet;
    ReObservacao: TdmkRichEdit;
    Label13: TLabel;
    EdZumPadr: TdmkEdit;
    Label14: TLabel;
    EdSVGLegenda: TdmkEdit;
    VUUnidMed: TdmkValUsu;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    Label15: TLabel;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SBUnidMed: TSpeedButton;
    EdNCM: TdmkEdit;
    SpeedButton1: TSpeedButton;
    Label16: TLabel;
    Label17: TLabel;
    EdReferencia: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbMarcasClick(Sender: TObject);
    procedure BtTextoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbPrgLstCabClick(Sender: TObject);
    procedure SbVetorSVGClick(Sender: TObject);
    procedure EdPrgLstCabChange(Sender: TObject);
    procedure SBPrgICorSVGClick(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdNCMExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FFrmNome, FTabNome: String;
    FGraBugs: TGraBugsServi;
    //
    procedure FiltraItensAExibir(GraBugs: TGraBugsServi);
    procedure ReopenPrgICorSVG(Controle: Integer);
  end;

  var
  FmGraG1Equi: TFmGraG1Equi;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, DmkDAC_PF, UnDmkProcFunc,
  MyDBCheck, UnGrade_Jan, ModProd, UnMySQLCuringa, UnUMedi_PF;

{$R *.DFM}

procedure TFmGraG1Equi.BtOKClick(Sender: TObject);

  function InsUpdG1PrAp(SQLType: TSQLType): Boolean;
  var
    QtCorSVGgg, QtCorSVGgr, QtCorSVGrg, QtCorSVGrr, QtCorSVGrb, QtCorSVGbr,
    Nivel1, Marca, PrgLstCab, NaoUsaPrdt, PrgICorSVG, DiasCorSVG: Integer;
    Observacao, VetorArq, VetorSVG, SVGLegenda: String;
    ZumPadr: Double;
    //
    SQLCampos: TdmkArrStr;
    ValCampos: TdmkArrVar;
  begin
    Nivel1     := EdNivel1.ValueVariant;
    Marca      := EdMarca.ValueVariant;
    Observacao := MyObjects.ObtemTextoRichEdit(Self, ReObservacao);

    SQLCampos := Geral.ATA_Str(['Marca', 'Observacao']);

    ValCampos := Geral.ATA_Var([Marca, Observacao]);

    case FGraBugs of
      gbsAplica  : ; // nada
      gbsMonitora: 
      begin
        //Somente Monitoramento
        PrgLstCab  := EdPrgLstCab.ValueVariant;
        PrgICorSVG := EdPrgICorSVG.ValueVariant;
        NaoUsaPrdt := Geral.BoolToInt(CkNaoUsaPrdt.Checked);
        QtCorSVGgg := EdQtCorSVGgg.ValueVariant;
        QtCorSVGgr := EdQtCorSVGgr.ValueVariant;
        QtCorSVGrg := EdQtCorSVGrg.ValueVariant;
        QtCorSVGrr := EdQtCorSVGrr.ValueVariant;
        QtCorSVGrb := EdQtCorSVGrb.ValueVariant;
        QtCorSVGbr := EdQtCorSVGbr.ValueVariant;
        DiasCorSVG := EdDiasCorSVG.ValueVariant;
        ZumPadr    := EdZumPadr.ValueVariant;
        SVGLegenda := EdSVGLegenda.ValueVariant;
        VetorArq   := EdVetorArq.Text;
        VetorSVG   := MeVetorSVG.Text;

        SQLCampos  := Geral.ASA_Str(SQLCampos,
                      ['PrgLstCab', 'PrgICorSVG', 'NaoUsaPrdt', 'QtCorSVGgg',
                      'QtCorSVGgr', 'QtCorSVGrg', 'QtCorSVGrr', 'QtCorSVGrb',
                      'QtCorSVGbr', 'DiasCorSVG', 'ZumPadr', 'SVGLegenda',
                      'VetorArq', 'VetorSVG']);
        ValCampos  := Geral.ASA_Var(ValCampos,
                      [PrgLstCab, PrgICorSVG, NaoUsaPrdt, QtCorSVGgg,
                      QtCorSVGgr, QtCorSVGrg, QtCorSVGrr, QtCorSVGrb,
                      QtCorSVGbr, DiasCorSVG, ZumPadr, SVGLegenda,
                      VetorArq, VetorSVG]);
      end;
    end;
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTabNome, False,
                SQLCampos, ['Nivel1'], ValCampos, [Nivel1], True);
  end;
var
  Nivel1, UnidMed: Integer;
  Nome, NCM, Referencia: String;
  SQLType: TSQLType;
begin
  Nome       := EdNome.ValueVariant;
  NCM        := EdNCM.ValueVariant;
  UnidMed    := VUUnidMed.ValueVariant;
  Referencia := EdReferencia.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Nivel1 := EdNivel1.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False,
    ['Nome', 'NCM', 'UnidMed', 'Referencia'], ['Nivel1'],
    [Nome, NCM, UnidMed, Referencia], [Nivel1], True) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Nivel1 ',
      'FROM ' + FTabNome,
      'WHERE Nivel1=' + Geral.FF0(Nivel1),
      '']);

    if Nivel1 = Dmod.QrAux.FieldByName('Nivel1').AsInteger then
      SQLType := stUpd
    else
      SQLType := stIns;

    if InsUpdG1PrAp(SQLType) then
    begin
      {
      LocCod(Nivel1,Nivel1);
      ImgTipo.SQLType := stlok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
      }
      Close;
    end;
  end;
end;

procedure TFmGraG1Equi.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraG1Equi.BtTextoClick(Sender: TObject);
begin
  MyObjects.EditaTextoRichEdit(REObservacao, 10, 'Tahoma', []);
end;

procedure TFmGraG1Equi.CBUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraG1Equi.EdNCMExit(Sender: TObject);
begin
  EdNCM.Text := Geral.FormataNCM(EdNCM.Text);
end;

procedure TFmGraG1Equi.EdPrgLstCabChange(Sender: TObject);
begin
  EdPrgICorSVG.ValueVariant := 0;
  CBPrgICorSVG.KeyValue     := Null;
  //
  ReopenPrgICorSVG(0);
end;

procedure TFmGraG1Equi.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraG1Equi.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraG1Equi.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmGraG1Equi.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmGraG1Equi.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraG1Equi.FiltraItensAExibir(GraBugs: TGraBugsServi);
begin
  FGraBugs := GraBugs;
  case GraBugs of
    gbsAplica  : FTabNome := 'grag1eqap';
    gbsMonitora: FTabNome := 'grag1eqmo';
    else FTabNome := 'grag1eq??';
  end;
  //
  case GraBugs of
    gbsAplica  : FFrmNome := 'Aplica��o';
    gbsMonitora: FFrmNome := 'Monitoramento';
    else FFrmNome := '? ? ? ? ?';
  end;
  //
  PnPrgLstCab.Visible  := GraBugs = gbsMonitora;
  TabSheet2.TabVisible := GraBugs = gbsMonitora;
end;

procedure TFmGraG1Equi.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  MeVetorSVG.CharCase := ecNormal;
end;

procedure TFmGraG1Equi.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrMarcas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrgLstCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmGraG1Equi.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], 'PRD-GRUPO-025 :: Equipamentos de ' + FFrmNome, True, taCenter, 2, 10, 20);
end;

procedure TFmGraG1Equi.ReopenPrgICorSVG(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrgLstIts, Dmod.MyDB, [
    'SELECT pli.Controle, pcp.Nome NO_PERGUNTA ',
    'FROM prglstits pli ',
    'LEFT JOIN prgcadprg pcp ON pcp.Codigo=pli.Pergunta ',
    'WHERE pli.Codigo=' + Geral.FF0(QrPrgLstCabCodigo.Value),
    'ORDER BY NO_PERGUNTA ',
    '']);
end;

procedure TFmGraG1Equi.SbMarcasClick(Sender: TObject);
var
  Marca: Integer;
begin
  VAR_CADASTRO2 := 0;
  Marca         := EdMarca.ValueVariant;

  FmPrincipal.MostraFormGraFabCad(Marca);

  if VAR_CADASTRO2 <> 0 then
    UMyMod.SetaCodigoPesquisado(EdMarca, CBMarca, QrMarcas, VAR_CADASTRO2, 'Controle');
end;

procedure TFmGraG1Equi.SBPrgICorSVGClick(Sender: TObject);
var
  PrgICorSVG: Integer;
begin
  VAR_CADASTRO2 := 0;
  PrgICorSVG    := EdPrgICorSVG.ValueVariant;

  FmPrincipal.MostraFormPrgLstCab(0, PrgICorSVG);

  if VAR_CADASTRO2 <> 0 then
    UMyMod.SetaCodigoPesquisado(EdPrgICorSVG, CBPrgICorSVG, QrPrgLstIts, VAR_CADASTRO2, 'Controle');
end;

procedure TFmGraG1Equi.SbPrgLstCabClick(Sender: TObject);
var
  PrgLstCab: Integer;
begin
  VAR_CADASTRO := 0;
  PrgLstCab    := EdPrgLstCab.ValueVariant;

  FmPrincipal.MostraFormPrgLstCab(PrgLstCab, 0);

  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdPrgLstCab, CBPrgLstCab, QrPrgLstCab, VAR_CADASTRO, 'Codigo');
end;

procedure TFmGraG1Equi.SBUnidMedClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := VUUnidMed.ValueVariant;
  VAR_CADASTRO := 0;
  //
  UMedi_PF.MostraUnidMed(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    CBUnidMed.SetFocus;
  end;
end;

procedure TFmGraG1Equi.SbVetorSVGClick(Sender: TObject);
const
  sTag = 'dermatek:zoom';
var
  I, P: Integer;
  Linha, Arq: String;
  Ext: TStringList;
begin
  //if MyObjects.DefineArquivo1(Self, EdVetorArq) then
  //
  Arq := ExtractFileName(EdVetorArq.ValueVariant);
  Ext := TStringList.Create;
  try
    Ext.Add('.svg');
    //
    EdVetorArq.ValueVariant := FmPrincipal.SelecionaImg(EdVetorArq.ValueVariant, Arq, Ext);
  finally
    Ext.Free;
  end;
  //
  if EdVetorArq.ValueVariant <> '' then
  begin
    MeVetorSVG.Lines.LoadFromFile(EdVetorArq.Text);
    MeVetorSVG.Text := DmkPF.XMLEncode(MeVetorSVG.Text);
    //
    for I := 0 to MeVetorSVG.Lines.Count do
    begin
      Linha := MeVetorSVG.Lines[I];
      P     := pos(sTag, Linha);
      //
      if P > 0 then
      begin
        Linha := Copy(Linha, P + Length(sTag));
        P     := pos('"', Linha);
        Linha := Copy(Linha, P + 1);
        P     := pos('"', Linha);
        Linha := Copy(Linha, 1, P - 1);
        //
        EdZumPadr.ValueVariant := Geral.DMV_Dot(Linha);
      end;
    end;
  end;
end;

procedure TFmGraG1Equi.SpeedButton1Click(Sender: TObject);
var
  NCMSel: String;
begin
  VAR_CADASTRO := 0;
  NCMSel       := Grade_Jan.MostraFormClasFisc();
  //
  if NCMSel <> '' then
    EdNCM.Texto := NCMSel;
end;

end.
