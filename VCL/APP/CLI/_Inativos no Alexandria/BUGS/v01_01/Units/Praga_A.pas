unit Praga_A;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB;

type
  TFmPraga_A = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPraga_A: TmySQLQuery;
    DsPraga_A: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdNivSup: TdmkEditCB;
    CBNivSup: TdmkDBLookupComboBox;
    Label3: TLabel;
    QrPraga_9: TmySQLQuery;
    DsPraga_9: TDataSource;
    QrPraga_ANO_GRU: TWideStringField;
    QrPraga_ACodigo: TIntegerField;
    QrPraga_ANome: TWideStringField;
    QrPraga_ALk: TIntegerField;
    QrPraga_ADataCad: TDateField;
    QrPraga_ADataAlt: TDateField;
    QrPraga_AUserCad: TIntegerField;
    QrPraga_AUserAlt: TIntegerField;
    QrPraga_AAlterWeb: TSmallintField;
    QrPraga_AAtivo: TSmallintField;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    QrPraga_9Codigo: TIntegerField;
    QrPraga_9Nome: TWideStringField;
    SpeedButton5: TSpeedButton;
    QrPraga_ANivSup: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPraga_AAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPraga_ABeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmPraga_A: TFmPraga_A;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPraga_A.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPraga_A.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPraga_ACodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPraga_A.DefParams;
begin
  VAR_GOTOTABELA := 'praga_a';
  VAR_GOTOMYSQLTABLE := QrPraga_A;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT gru.Nome NO_GRU, cab.*');
  VAR_SQLx.Add('FROM praga_a cab');
  VAR_SQLx.Add('LEFT JOIN praga_9 gru ON gru.Codigo=cab.NivSup');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE cab.Codigo > 0');
  //
  VAR_SQL1.Add('AND cab.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND cab.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cab.Nome LIKE :P0');
  //
end;

procedure TFmPraga_A.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPraga_A.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPraga_A.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmPraga_A.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPraga_A.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPraga_A.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPraga_A.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPraga_A.SpeedButton5Click(Sender: TObject);
begin
  FmPrincipal.MostraFormPraga_9();
  UMyMod.SetaCodigoPesquisado(EdNivSup, CBNivSup, QrPraga_9, VAR_CADASTRO);
end;

procedure TFmPraga_A.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPraga_A.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPraga_A, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'praga_a');
end;

procedure TFmPraga_A.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPraga_ACodigo.Value;
  Close;
end;

procedure TFmPraga_A.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('praga_a', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrPraga_ACodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmPraga_A, PnEdita,
    'praga_a', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPraga_A.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'praga_a', 'Codigo');
end;

procedure TFmPraga_A.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPraga_A, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'praga_a');
end;

procedure TFmPraga_A.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrPraga_9, Dmod.MyDB);
end;

procedure TFmPraga_A.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPraga_ACodigo.Value, LaRegistro.Caption);
end;

procedure TFmPraga_A.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPraga_A.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPraga_ACodigo.Value, LaRegistro.Caption);
end;

procedure TFmPraga_A.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPraga_A.QrPraga_AAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPraga_A.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPraga_A.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPraga_ACodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'praga_a', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPraga_A.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPraga_A.QrPraga_ABeforeOpen(DataSet: TDataSet);
begin
  QrPraga_ACodigo.DisplayFormat := FFormatFloat;
end;

end.

