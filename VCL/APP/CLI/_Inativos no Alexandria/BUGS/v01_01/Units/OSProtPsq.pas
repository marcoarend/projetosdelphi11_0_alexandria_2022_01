unit OSProtPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, UnBugs_Tabs, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmOSProtPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrPsqSem: TmySQLQuery;
    DsPsqSem: TDataSource;
    QrPsqSemCodigo: TIntegerField;
    QrPsqSemStatus: TIntegerField;
    QrPsqSemEntidade: TIntegerField;
    QrPsqSemSiapTerCad: TIntegerField;
    QrPsqSemAtivo: TSmallintField;
    QrPsqSemID_Cod1: TIntegerField;
    QrPsqSemID_Cod2: TIntegerField;
    QrPsqSemNO_CLI: TWideStringField;
    RGPsqSel: TRadioGroup;
    DBGPsqOpn: TDBGrid;
    QrPsqOpn: TmySQLQuery;
    DsPsqOpn: TDataSource;
    QrPsqOpnCodigo: TIntegerField;
    QrPsqOpnControle: TIntegerField;
    QrPsqOpnConta: TIntegerField;
    QrPsqOpnLink_ID: TIntegerField;
    QrPsqOpnCliInt: TIntegerField;
    QrPsqOpnCliente: TIntegerField;
    QrPsqOpnCedente: TIntegerField;
    QrPsqOpnFornece: TIntegerField;
    QrPsqOpnPeriodo: TIntegerField;
    QrPsqOpnLancto: TIntegerField;
    QrPsqOpnDocum: TFloatField;
    QrPsqOpnDepto: TIntegerField;
    QrPsqOpnDataE: TDateField;
    QrPsqOpnDataD: TDateTimeField;
    QrPsqOpnRetorna: TSmallintField;
    QrPsqOpnCancelado: TIntegerField;
    QrPsqOpnMotivo: TIntegerField;
    QrPsqOpnID_Cod1: TIntegerField;
    QrPsqOpnID_Cod2: TIntegerField;
    QrPsqOpnID_Cod3: TIntegerField;
    QrPsqOpnID_Cod4: TIntegerField;
    QrPsqOpnVencto: TDateField;
    QrPsqOpnValor: TFloatField;
    QrPsqOpnMoraDiaVal: TFloatField;
    QrPsqOpnMultaVal: TFloatField;
    QrPsqOpnComoConf: TSmallintField;
    QrPsqOpnSerieCH: TWideStringField;
    QrPsqOpnManual: TIntegerField;
    QrPsqOpnTexto: TWideStringField;
    QrPsqOpnSaiu: TIntegerField;
    QrPsqOpnRecebeu: TIntegerField;
    QrPsqOpnRetornou: TIntegerField;
    QrPsqOpnLimiteSai: TDateField;
    QrPsqOpnLimiteRem: TDateField;
    QrPsqOpnLimiteRet: TDateField;
    QrPsqOpnDataSai: TDateTimeField;
    QrPsqOpnDataRec: TDateTimeField;
    QrPsqOpnDataRet: TDateTimeField;
    QrPsqOpnLk: TIntegerField;
    QrPsqOpnDataCad: TDateField;
    QrPsqOpnDataAlt: TDateField;
    QrPsqOpnUserCad: TIntegerField;
    QrPsqOpnUserAlt: TIntegerField;
    QrPsqOpnAlterWeb: TSmallintField;
    QrPsqOpnAtivo: TSmallintField;
    QrPsqOpnEntContrat: TIntegerField;
    QrPsqOpnEntPagante: TIntegerField;
    QrPsqOpnNO_ENT: TWideStringField;
    DBGPsqSem: TDBGrid;
    QrPsqSemEmpresa: TIntegerField;
    QrPsqSemCliInt: TIntegerField;
    QrPsqOpnCodCliInt: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGPsqSelClick(Sender: TObject);
    procedure QrPsqOpnDataSaiGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrPsqOpnDataRetGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
    procedure ReopenPsqSem();
    procedure ReopenPsqOpn();
  public
    { Public declarations }
  end;

  var
  FmOSProtPsq: TFmOSProtPsq;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, Protocolo;

{$R *.DFM}

procedure TFmOSProtPsq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSProtPsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSProtPsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DBGPsqOpn.Visible := False;
  DBGPsqSem.Visible := False;
  //
  DBGPsqOpn.Align := alClient;
  DBGPsqSem.Align := alClient;
end;

procedure TFmOSProtPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSProtPsq.QrPsqOpnDataRetGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := Geral.FDT(QrPsqOpnDataRet.Value, 109);
end;

procedure TFmOSProtPsq.QrPsqOpnDataSaiGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := Geral.FDT(QrPsqOpnDataSai.Value, 109);
end;

procedure TFmOSProtPsq.ReopenPsqOpn;
begin
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqOpn, Dmod.MyDB, [
    'SELECT eci.CodCliInt, ppi.*,',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT',
    'FROM protpakits ppi',
    'LEFT JOIN entidades ent ON ent.Codigo=ppi.Cliente',
    'LEFT JOIN enticliint eci ON eci.CodEnti=ppi.CliInt',
    'WHERE Link_ID=' + Geral.FF0(VAR_TIPO_LINK_ID_03_ORDEM_DE_SERVCO1),
    'AND Cancelado=0 ',
    'AND (',
    '  (Saiu=0)',
{
    '   OR',
    '  (Recebeu=0)',
}
    '   OR',
    '  (Retorna=1 AND Retornou=0)',
    ')',
    'ORDER BY ppi.Controle, ppi.Conta',
    '']);
    //
    DBGPsqOpn.Visible := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSProtPsq.ReopenPsqSem();
begin
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqSem, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _oss_periodo_; ',
    'CREATE TABLE _oss_periodo_ ( ',
    '  Codigo Int(11) NOT NULL DEFAULT "0", ',
    '  Status Int(11) NOT NULL DEFAULT "0", ',
    '  Empresa Int(11) NOT NULL DEFAULT "0", ',
    '  Entidade Int(11) NOT NULL DEFAULT "0", ',
    '  SiapTerCad Int(11) NOT NULL DEFAULT "0", ',
    ' ',
    '  Ativo tinyint(1) NOT NULL DEFAULT "1" ',
    ') CHARACTER SET latin1 COLLATE latin1_swedish_ci; ',
    ' ',
    'INSERT INTO _oss_periodo_ ',
    'SELECT Codigo, ' + Geral.FF0(CO_BUG_STATUS_0200_VISTORIA),
    ' Status, Empresa, Entidade, ',
    'SiapTerCad, 1 Ativo ',
    'FROM ' + TMeuDB + '.oscab ',
    'WHERE  DtaVisPrv > "1900-01-01" ',
    'AND DtaVisExe <= "1900-01-01" ',
    '; ',
    ' ',
    'INSERT INTO _oss_periodo_ ',
    'SELECT Codigo, ' + Geral.FF0(CO_BUG_STATUS_0600_EM_EXECU),
    ' Status, Empresa, Entidade, ',
    'SiapTerCad, 1 Ativo ',
    'FROM ' + TMeuDB + '.oscab ',
    'WHERE DtaExePrv > "1900-01-01" ',
    'AND DtaExeFim <= "1900-01-01" ',
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _oss_protocolos_; ',
    'CREATE TABLE _oss_protocolos_ ',
    'SELECT CliInt, ID_Cod1, ID_Cod2 ',
    'FROM ' + TMeuDB + '.protpakits ',
    'WHERE Link_ID=' + Geral.FF0(VAR_TIPO_LINK_ID_03_ORDEM_DE_SERVCO1),
    '; ',
    'SELECT cab.*, ptc.*, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLI ',
    'FROM _oss_periodo_ cab ',
    'LEFT JOIN _oss_protocolos_ ptc ON ptc.ID_Cod1=cab.Codigo ',
    '                              AND ptc.ID_Cod2=cab.Status ',
    'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=cab.Entidade ',
    'WHERE ID_Cod1 IS NULL ',
    ' ',
    'ORDER BY cab.Codigo, Cab.Status ',
    '; ',
    '']);
    //
    DBGPsqSem.Visible := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSProtPsq.RGPsqSelClick(Sender: TObject);
begin
  QrPsqOpn.Close;
  QrPsqSem.Close;
  //
  DBGPsqOpn.Visible := False;
  DBGPsqSem.Visible := False;
  //
  case RGPsqSel.ItemIndex of
    0: ;// Nada, apenas fechas queries
    1: ReopenPsqOpn();
    2: ReopenPsqSem();
  end;
end;

end.
