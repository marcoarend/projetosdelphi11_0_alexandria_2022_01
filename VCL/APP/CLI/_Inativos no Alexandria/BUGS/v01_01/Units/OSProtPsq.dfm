object FmOSProtPsq: TFmOSProtPsq
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-036 :: Pesquisa OSs x Protocolos'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 328
        Height = 32
        Caption = 'Pesquisa OSs x Protocolos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 328
        Height = 32
        Caption = 'Pesquisa OSs x Protocolos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 328
        Height = 32
        Caption = 'Pesquisa OSs x Protocolos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object RGPsqSel: TRadioGroup
          Left = 2
          Top = 15
          Width = 808
          Height = 42
          Align = alTop
          Caption = ' Dados a serem visualizados:'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Fechar pesquisa atual'
            'Protocolos abertos de OSs'
            'OSs sem procololo')
          TabOrder = 0
          OnClick = RGPsqSelClick
        end
        object DBGPsqOpn: TDBGrid
          Left = 2
          Top = 222
          Width = 808
          Height = 165
          Align = alTop
          DataSource = DsPsqOpn
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lote'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Title.Caption = 'Protocolo'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Cod1'
              Title.Caption = 'OS'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Cod2'
              Title.Caption = 'Status'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cliente'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENT'
              Title.Caption = 'Nome do (sub)cliente'
              Width = 430
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CodCliInt'
              Title.Caption = 'Empresa'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataSai'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataRet'
              Visible = True
            end>
        end
        object DBGPsqSem: TDBGrid
          Left = 2
          Top = 57
          Width = 808
          Height = 165
          Align = alTop
          DataSource = DsPsqSem
          TabOrder = 2
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'OS'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Status'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Entidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CLI'
              Title.Caption = 'Nome do (sub)cliente'
              Width = 559
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Empresa'
              Width = 46
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 249
        Height = 16
        Caption = 'Status para OS sem protocolo: (200 e 600)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 249
        Height = 16
        Caption = 'Status para OS sem protocolo: (200 e 600)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
      end
    end
  end
  object QrPsqSem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _oss_periodo_;'
      'CREATE TABLE _oss_periodo_ ('
      '  Codigo Int(11) NOT NULL DEFAULT "0",'
      '  Status Int(11) NOT NULL DEFAULT "0",'
      '  Entidade Int(11) NOT NULL DEFAULT "0",'
      '  SiapTerCad Int(11) NOT NULL DEFAULT "0",'
      ' /**/'
      '  Ativo tinyint(1) NOT NULL DEFAULT "1"'
      ') CHARACTER SET latin1 COLLATE latin1_swedish_ci;'
      ''
      'INSERT INTO _oss_periodo_'
      'SELECT Codigo, 200 Status, Entidade,'
      'SiapTerCad, 1 Ativo'
      'FROM bugstrol.oscab'
      'WHERE  DtaVisPrv > "1900-01-01"'
      'AND DtaVisExe <= "1900-01-01"'
      ';'
      ''
      'INSERT INTO _oss_periodo_'
      'SELECT Codigo, 600 Status, Entidade,'
      'SiapTerCad, 1 Ativo'
      'FROM bugstrol.oscab'
      'WHERE DtaExePrv > "1900-01-01"'
      'AND DtaExeFim <= "1900-01-01"'
      ';'
      ''
      'DROP TABLE IF EXISTS _oss_protocolos_;'
      'CREATE TABLE _oss_protocolos_'
      'SELECT ID_Cod1, ID_Cod2'
      'FROM bugstrol.protpakits'
      'WHERE Link_ID=3'
      ';'
      'SELECT cab.*, ptc.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLI'
      'FROM _oss_periodo_ cab'
      'LEFT JOIN _oss_protocolos_ ptc ON ptc.ID_Cod1=cab.Codigo'
      
        '                                                   AND ptc.ID_Co' +
        'd2=cab.Status'
      'LEFT JOIN bugstrol.entidades ent ON ent.Codigo=cab.Entidade'
      '/*WHERE ID_Cod1 IS NULL*/'
      '/*WHERE ID_Cod1 IS NULL*/'
      '/*WHERE NOT (ID_Cod1 IS NULL) AND ID_Cod2=500*/'
      ''
      'ORDER BY cab.Codigo, Cab.Status'
      ';')
    Left = 128
    Top = 208
    object QrPsqSemCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsqSemStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrPsqSemEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrPsqSemSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrPsqSemAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPsqSemID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
    end
    object QrPsqSemID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
    end
    object QrPsqSemNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrPsqSemEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPsqSemCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsPsqSem: TDataSource
    DataSet = QrPsqSem
    Left = 128
    Top = 256
  end
  object QrPsqOpn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eci.CodCliInt, ppi.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM protpakits ppi'
      'LEFT JOIN entidades ent ON ent.Codigo=ppi.Cliente'
      'LEFT JOIN enticliint eci ON eci.CodEnti=ppi.CliInt'
      'WHERE Link_ID=3'
      'AND Cancelado=0 '
      'AND ('
      '  (Saiu=0)'
      '   OR'
      '  (Recebeu=0)'
      '   OR'
      '  (Retorna=1 AND Retornou=0)'
      ')')
    Left = 200
    Top = 208
    object QrPsqOpnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsqOpnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPsqOpnConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPsqOpnLink_ID: TIntegerField
      FieldName = 'Link_ID'
    end
    object QrPsqOpnCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPsqOpnCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPsqOpnCedente: TIntegerField
      FieldName = 'Cedente'
    end
    object QrPsqOpnFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrPsqOpnPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrPsqOpnLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPsqOpnDocum: TFloatField
      FieldName = 'Docum'
    end
    object QrPsqOpnDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrPsqOpnDataE: TDateField
      FieldName = 'DataE'
    end
    object QrPsqOpnDataD: TDateTimeField
      FieldName = 'DataD'
    end
    object QrPsqOpnRetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object QrPsqOpnCancelado: TIntegerField
      FieldName = 'Cancelado'
    end
    object QrPsqOpnMotivo: TIntegerField
      FieldName = 'Motivo'
    end
    object QrPsqOpnID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
    end
    object QrPsqOpnID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
    end
    object QrPsqOpnID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
    end
    object QrPsqOpnID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
    end
    object QrPsqOpnVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrPsqOpnValor: TFloatField
      FieldName = 'Valor'
    end
    object QrPsqOpnMoraDiaVal: TFloatField
      FieldName = 'MoraDiaVal'
    end
    object QrPsqOpnMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrPsqOpnComoConf: TSmallintField
      FieldName = 'ComoConf'
    end
    object QrPsqOpnSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrPsqOpnManual: TIntegerField
      FieldName = 'Manual'
    end
    object QrPsqOpnTexto: TWideStringField
      FieldName = 'Texto'
      Size = 30
    end
    object QrPsqOpnSaiu: TIntegerField
      FieldName = 'Saiu'
    end
    object QrPsqOpnRecebeu: TIntegerField
      FieldName = 'Recebeu'
    end
    object QrPsqOpnRetornou: TIntegerField
      FieldName = 'Retornou'
    end
    object QrPsqOpnLimiteSai: TDateField
      FieldName = 'LimiteSai'
    end
    object QrPsqOpnLimiteRem: TDateField
      FieldName = 'LimiteRem'
    end
    object QrPsqOpnLimiteRet: TDateField
      FieldName = 'LimiteRet'
    end
    object QrPsqOpnDataSai: TDateTimeField
      FieldName = 'DataSai'
      OnGetText = QrPsqOpnDataSaiGetText
    end
    object QrPsqOpnDataRec: TDateTimeField
      FieldName = 'DataRec'
    end
    object QrPsqOpnDataRet: TDateTimeField
      FieldName = 'DataRet'
      OnGetText = QrPsqOpnDataRetGetText
    end
    object QrPsqOpnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPsqOpnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPsqOpnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPsqOpnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPsqOpnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPsqOpnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPsqOpnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPsqOpnEntContrat: TIntegerField
      FieldName = 'EntContrat'
    end
    object QrPsqOpnEntPagante: TIntegerField
      FieldName = 'EntPagante'
    end
    object QrPsqOpnNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrPsqOpnCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
  end
  object DsPsqOpn: TDataSource
    DataSet = QrPsqOpn
    Left = 200
    Top = 256
  end
end
