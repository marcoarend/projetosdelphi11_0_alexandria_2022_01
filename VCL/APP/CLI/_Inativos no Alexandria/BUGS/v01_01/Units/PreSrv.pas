unit PreSrv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, UnBugs_Tabs, dmkMemo, UnDmkEnums;

type
  TFmPreSrv = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    GBDados: TGroupBox;
    Label9: TLabel;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    QrDesServico: TmySQLQuery;
    DsDesServico: TDataSource;
    QrDesServicoCodigo: TIntegerField;
    QrDesServicoNome: TWideStringField;
    GBMonitoramento: TGroupBox;
    Label12: TLabel;
    EdMoniDdTotl: TdmkEdit;
    EdMoniDdIntv: TdmkEdit;
    Label13: TLabel;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    GroupBox4: TGroupBox;
    Panel7: TPanel;
    Label25: TLabel;
    EdGarantiaDd: TdmkEdit;
    Label26: TLabel;
    EdHrEvacuar: TdmkEdit;
    Label27: TLabel;
    EdHrExecutar: TdmkEdit;
    GroupBox5: TGroupBox;
    Panel8: TPanel;
    EdValCalc: TdmkEdit;
    EdValTota: TdmkEdit;
    Label11: TLabel;
    EdValDesc: TdmkEdit;
    Label10: TLabel;
    EdValInfo: TdmkEdit;
    Label8: TLabel;
    Label6: TLabel;
    MeDetalhes: TdmkMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdValCalcChange(Sender: TObject);
    procedure EdValInfoChange(Sender: TObject);
    procedure EdValDescChange(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrInsUpd: TmySQLQuery;
    FCriou: Boolean;
    //
    procedure CalculaTotal();
  end;

  var
  FmPreSrv: TFmPreSrv;

implementation

uses UnMyObjects, Module, UMySQLModule, UnOSApp_PF, DmkDAC_PF;

{$R *.DFM}

procedure TFmPreSrv.BtOKClick(Sender: TObject);
var
  GarantiaDd, HrEvacuar, Codigo, Controle,
  //DesServico, Autorizado, TudoFeitoM,
  MoniDdTotl, MoniDdIntv: Integer;
  HrExecutar, ValCalc, ValInfo, ValDesc, ValTota: Double;
  Nome, Detalhes: String;
  SemQtd: Boolean;
begin
  Codigo     := QrDesServicoCodigo.Value;
  Controle   := EdControle.ValueVariant;
  Nome       := EdNome.ValueVariant;
  GarantiaDd := EdGarantiaDd.ValueVariant;
  HrEvacuar  := EdHrEvacuar.ValueVariant;
  HrExecutar := EdHrExecutar.ValueVariant;
  //
  ValCalc := EdValCalc.ValueVariant;
  ValInfo := EdValInfo.ValueVariant;
  ValDesc := EdValDesc.ValueVariant;
  ValTota := EdValTota.ValueVariant;
  //
  Detalhes   := MeDetalhes.Text;
  //
  MoniDdTotl   := EdMoniDdTotl.ValueVariant;
  MoniDdIntv   := EdMoniDdIntv.ValueVariant;
  (*
  DesServico := EdDesServico.ValueVariant;
  if MyObjects.FIC(DesServico = 0, EdDesServico, 'Informe o servi�o!') then
    Exit;
  Autorizado := RGAutorizado.ItemIndex;
  TudoFeitoM := RGTudoFeitoM.ItemIndex;
  *)
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then
    Exit;
  SemQtd := (MoniDdTotl <> 0) and (MoniDdIntv = 0);
  if MyObjects.FIC(SemQtd, EdMoniDdTotl,
  'Informe o intervalo do monitoramento!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('presrv', 'Controle', '', '',
    tsDef, ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'presrv', False, [
  'Codigo', (*'DesServico',*) 'GarantiaDd',
  'HrEvacuar', 'HrExecutar', 'ValCalc',
  'ValInfo', 'ValDesc', 'ValTota',
  (*'Autorizado',*) 'Detalhes', 'MoniDdTotl',
  'MoniDdIntv', 'Nome'(*, 'TudoFeitoM'*)
  ], [
  'Controle'], [
  Codigo, (*DesServico,*) GarantiaDd,
  HrEvacuar, HrExecutar, ValCalc,
  ValInfo, ValDesc, ValTota,
  (*Autorizado,*) Detalhes, MoniDdTotl,
  MoniDdIntv, Nome(*, TudoFeitoM*)
  ], [
  Controle], True) then
  begin
    if FQrInsUpd <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrInsUpd, Dmod.MyDB);
      FQrInsUpd.Locate('Controle', Controle, []);
    end;
(*
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdControle.ValueVariant := 0;
      EdGarantiaDd.ValueVariant := 0;
      EdHrEvacuar.ValueVariant := 0;
      EdHrExecutar.ValueVariant := 0;
      EdValDesc.ValueVariant := 0; // Deve ser antes para n�o dar aviso de valor negativo
      EdValCalc.ValueVariant := 0;
      EdValInfo.ValueVariant := 0;
      EdValTota.ValueVariant := 0;
    end else
*)
      Close;
  end;
end;

procedure TFmPreSrv.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPreSrv.CalculaTotal();
var
  Calc, Info, Desc, Tota: Double;
begin
  if FCriou = False then
    Exit;
  //
  Calc := EdValCalc.ValueVariant;
  Info := EdValInfo.ValueVariant;
  Desc := EdValDesc.ValueVariant;
  Tota := OSApp_PF.TotalValorServico(Calc, Info, Desc);
  EdValTota.ValueVariant := Tota;
end;

procedure TFmPreSrv.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    MyObjects.SetTextAndGoEnd(EdNome, QrDesServicoNome.Value + ' - ');
end;

procedure TFmPreSrv.EdValCalcChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmPreSrv.EdValDescChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmPreSrv.EdValInfoChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmPreSrv.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPreSrv.FormCreate(Sender: TObject);
begin
  QrDesServico.Open;
  FCriou := False;
  //
  //N�o usa
  GBMonitoramento.Visible := False;
end;

procedure TFmPreSrv.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
