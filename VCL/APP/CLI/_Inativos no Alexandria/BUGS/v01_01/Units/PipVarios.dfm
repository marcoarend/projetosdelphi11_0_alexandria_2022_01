object FmPipVarios: TFmPipVarios
  Left = 339
  Top = 185
  Caption = 'PIP-GEREN-003 :: Cadastro de V'#225'rios PMVs'
  ClientHeight = 373
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 633
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 585
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 537
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 312
        Height = 32
        Caption = 'Cadastro de V'#225'rios PMVs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 312
        Height = 32
        Caption = 'Cadastro de V'#225'rios PMVs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 312
        Height = 32
        Caption = 'Cadastro de V'#225'rios PMVs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 633
    Height = 211
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 633
      Height = 211
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 633
        Height = 211
        Align = alClient
        TabOrder = 0
        object PnGeral: TPanel
          Left = 2
          Top = 15
          Width = 629
          Height = 194
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 629
            Height = 109
            Align = alTop
            Caption = ' Dados FIXOS para multi inclus'#227'o: '
            TabOrder = 0
            object Panel5: TPanel
              Left = 2
              Top = 15
              Width = 625
              Height = 92
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 500
                Top = 8
                Width = 89
                Height = 13
                Caption = 'Data de aquisi'#231#227'o:'
              end
              object Label2: TLabel
                Left = 12
                Top = 8
                Width = 65
                Height = 13
                Caption = 'Equipamento:'
              end
              object SbEquipAplic: TSpeedButton
                Left = 478
                Top = 24
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbEquipAplicClick
              end
              object Label9: TLabel
                Left = 12
                Top = 48
                Width = 177
                Height = 13
                Caption = 'Lista de perguntas no monitoramento:'
              end
              object SBPrgLstCab: TSpeedButton
                Left = 590
                Top = 64
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SBPrgLstCabClick
              end
              object TPDtaAquis: TdmkEditDateTimePicker
                Left = 500
                Top = 24
                Width = 112
                Height = 21
                Date = -3652.857134502316000000
                Time = -3652.857134502316000000
                TabOrder = 2
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
              end
              object EdEquipamento: TdmkEditCB
                Left = 12
                Top = 24
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'EquipAplic'
                UpdCampo = 'EquipAplic'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEquipamento
                IgnoraDBLookupComboBox = False
              end
              object CBEquipamento: TdmkDBLookupComboBox
                Left = 68
                Top = 24
                Width = 409
                Height = 21
                KeyField = 'Controle'
                ListField = 'Nome'
                ListSource = DsEquipAplic
                TabOrder = 1
                dmkEditCB = EdEquipamento
                QryCampo = 'EquipAplic'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdPrgLstCab: TdmkEditCB
                Left = 12
                Top = 64
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'EquipAplic'
                UpdCampo = 'EquipAplic'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBPrgLstCab
                IgnoraDBLookupComboBox = False
              end
              object CBPrgLstCab: TdmkDBLookupComboBox
                Left = 68
                Top = 64
                Width = 521
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPrgLstCab
                TabOrder = 4
                dmkEditCB = EdPrgLstCab
                QryCampo = 'EquipAplic'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 109
            Width = 629
            Height = 76
            Align = alTop
            Caption = ' Dados VARI'#193'VEIS para multi inclus'#227'o: '
            TabOrder = 1
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 625
              Height = 59
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label4: TLabel
                Left = 12
                Top = 8
                Width = 58
                Height = 13
                Caption = 'Quantidade:'
              end
              object Label3: TLabel
                Left = 96
                Top = 8
                Width = 79
                Height = 13
                Caption = 'Prefixo do nome:'
              end
              object Label5: TLabel
                Left = 256
                Top = 8
                Width = 70
                Height = 13
                Caption = 'Casas n'#250'mero:'
              end
              object Label6: TLabel
                Left = 332
                Top = 8
                Width = 76
                Height = 13
                Caption = 'Sufixo do nome:'
              end
              object Label7: TLabel
                Left = 416
                Top = 8
                Width = 151
                Height = 13
                Caption = 'Como ficar'#225' o nome do primeiro:'
                Enabled = False
              end
              object Label8: TLabel
                Left = 180
                Top = 8
                Width = 69
                Height = 13
                Caption = 'N'#250'mero inicial:'
              end
              object EdQtd: TdmkEdit
                Left = 12
                Top = 24
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ValMax = '1000'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
                ValWarn = False
              end
              object EdPre: TdmkEdit
                Left = 96
                Top = 24
                Width = 80
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdPreChange
              end
              object EdFmt: TdmkEdit
                Left = 256
                Top = 24
                Width = 73
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ValMax = '9'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '3'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 3
                ValWarn = False
                OnChange = EdFmtChange
              end
              object EdSuf: TdmkEdit
                Left = 332
                Top = 24
                Width = 80
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdSufChange
              end
              object EdPrimeiro: TdmkEdit
                Left = 416
                Top = 24
                Width = 205
                Height = 21
                Enabled = False
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '001'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '001'
                ValWarn = False
              end
              object EdIni: TdmkEdit
                Left = 180
                Top = 24
                Width = 73
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
                ValWarn = False
                OnChange = EdIniChange
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 259
    Width = 633
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 629
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 303
    Width = 633
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 487
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 485
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkMinusculas: TCheckBox
        Left = 192
        Top = 16
        Width = 193
        Height = 17
        Caption = 'Permitir letras min'#250'sculas no nome.'
        TabOrder = 1
        OnClick = CkMinusculasClick
      end
    end
  end
  object QrEquipAplic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM equiaplic'
      'ORDER BY Nome')
    Left = 520
    Top = 12
    object QrEquipAplicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEquipAplicNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsEquipAplic: TDataSource
    DataSet = QrEquipAplic
    Left = 548
    Top = 12
  end
  object QrPsq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) ITENS '
      'FROM pipcad '
      'WHERE Nome="01P" ')
    Left = 12
    Top = 8
    object QrPsqITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object QrPrgLstCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM prglstcab '
      'ORDER BY Nome')
    Left = 132
    Top = 172
    object QrPrgLstCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgLstCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPrgLstCab: TDataSource
    DataSet = QrPrgLstCab
    Left = 160
    Top = 172
  end
end
