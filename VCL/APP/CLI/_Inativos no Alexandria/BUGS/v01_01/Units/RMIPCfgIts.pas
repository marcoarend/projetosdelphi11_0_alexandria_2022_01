unit RMIPCfgIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, UnProjGroup_Consts;

type
  TFmRMIPCfgIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    RGItemRel: TdmkRadioGroup;
    Panel5: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label1: TLabel;
    EdOrdem: TdmkEdit;
    RGAplicacao: TdmkRadioGroup;
    Panel6: TPanel;
    Label2: TLabel;
    EdNome: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenRMIPCfgIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmRMIPCfgIts: TFmRMIPCfgIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmRMIPCfgIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, ItemRel, Ordem, Aplicacao: Integer;
  Nome: String;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  ItemRel        := RGItemRel.ItemIndex;
  Ordem          := EdOrdem.ValueVariant;
  Aplicacao      := RGAplicacao.ItemIndex;
  Nome           := EdNome.Text;
  //
  if MyObjects.FIC(ItemRel < 1, RGItemRel, 'Informe uma  Item (relat�rio) v�lido!') then
    Exit;
  //
  if UMyMod.VerificaDuplicadoInt(Dmod.MyDB, 'rmipcfgits', 'Codigo', 'ItemRel',
  'Controle', Codigo, Itemrel, Controle) then
    Exit;
  Controle := UMyMod.BPGS1I32('rmipcfgits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'rmipcfgits', False, [
  'Codigo', 'ItemRel', 'Ordem',
  'Aplicacao', 'Nome'], [
  'Controle'], [
  Codigo, ItemRel, Ordem,
  Aplicacao, Nome], [
  Controle], True) then
  begin
    ReopenRMIPCfgIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      RGAplicacao.ItemIndex    := 0;
      RGItemRel.ItemIndex      := 0;
      EdNome.ValueVariant      := '';
      //
      RGItemRel.SetFocus;
    end else Close;
  end;
end;

procedure TFmRMIPCfgIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRMIPCfgIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
  EdOrdem.SetFocus;
end;

procedure TFmRMIPCfgIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGAplicacao, sListaAplicacaoRMIPCfgIts, 4, 0);
  MyObjects.ConfiguraRadioGroup(RGItemRel, sListaItensRMIP, 2, 0);
end;

procedure TFmRMIPCfgIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRMIPCfgIts.ReopenRMIPCfgIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
