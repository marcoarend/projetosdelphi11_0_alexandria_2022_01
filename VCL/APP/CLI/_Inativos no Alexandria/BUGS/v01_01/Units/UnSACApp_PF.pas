unit UnSACApp_PF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, StdCtrls, ExtCtrls, ImgList, dmkGeral,
  dmkVariable, DB, mySQLDbTables, DmkDAC_PF, dmkEditCB, dmkDBLookupComboBox,
  dmkCheckGroup, dmkRadioGroup, dmkEdit, dmkImage, Buttons, UnInternalConsts,
  dmkCompoStore, dmkPageControl, UnDmkEnums, dmkMemo,
  SAC_01;

type
  TUnSACApp_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  MostraFormSAC_01_Pre(FmSAC_01: TFmSAC_01; AbaOrigem: TTabSheet;
              Componentes: array of TComponent): TForm;

  end;

var
  SACApp_PF: TUnSACApp_PF;

implementation

uses UnMyVCLRef, ModOS, SAC_01_Pre, Module;

{ TUnSACApp_PF }

function TUnSACApp_PF.MostraFormSAC_01_Pre(FmSAC_01: TFmSAC_01; AbaOrigem:
  TTabSheet; Componentes: array of TComponent): TForm;
var
  Form: TForm;
begin
  Form := TFmSAC_01(FmSAC_01).NovaAba(TFmSAC_01_Pre, 2);
  TFmSAC_01_Pre(Form).FFmCriou := FmSAC_01;
  MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsAbaOrigem',  AbaOrigem, True);
  MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsEdDepto',  TdmkEdit(Componentes[0]), True);
  MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsSbUserAction',  TSpeedButton(Componentes[1]), True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrOSCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM oscab ',
  'WHERE Codigo=' + Geral.FF0(TdmkEdit(Componentes[0]).ValueVariant),
  '']);
  //
  MyVCLref.SET_Component(TForm(Form), 'ImgTipo', 'SQLType', stUpd, TdmkImage);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdGrupo',  'ValueVariant', DmModOS.QrOSCabCodigo.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdNumero',  'ValueVariant', DmModOS.QrOSCabCodigo.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdOpcao',  'ValueVariant', DmModOS.QrOSCabCodigo.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdCodigo',  'ValueVariant', DmModOS.QrOSCabCodigo.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdEmpresa',  'ValueVariant',  DmModOS.QrOSCabEmpresa.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdEntidade',  'ValueVariant',  DmModOS.QrOSCabEntidade.Value, True);
  MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEntidade', 'KeyValue',  DmModOS.QrOSCabEntidade.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdNumContrat',  'ValueVariant',  DmModOS.QrOSCabNumContrat.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdSiapTerCad',  'ValueVariant',  DmModOS.QrOSCabSiapTerCad.Value, True);
  MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBSiapTerCad', 'KeyValue',  DmModOS.QrOSCabSiapTerCad.Value, True);
  //MyVCLref.SET_dmkEdit(TForm(Form), 'EdOSOrigem',  'ValueVariant',  DmModOS.QrOSCabOSOrigem.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdEstatus',  'ValueVariant',  DmModOS.QrOSCabEstatus.Value, True);
  MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEstatus', 'KeyValue',  DmModOS.QrOSCabEstatus.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdEntiContat',  'ValueVariant',  DmModOS.QrOSCabEntiContat.Value, True);
  MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEntiContat', 'KeyValue',  DmModOS.QrOSCabEntiContat.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdFatoGeradr',  'ValueVariant',  DmModOS.QrOSCabFatoGeradr.Value, True);
  MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBFatoGeradr', 'KeyValue',  DmModOS.QrOSCabFatoGeradr.Value, True);
  MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPDtaVisPrv', 'Date',  DmModOS.QrOSCabDtaVisPrv.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdDtaVisPrv',  'ValueVariant',  DmModOS.QrOSCabDtaVisPrv.Value, True);
  MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPDtaExePrv', 'Date',  DmModOS.QrOSCabDtaExePrv.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdDtaExePrv',  'ValueVariant',  DmModOS.QrOSCabDtaExePrv.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdEntContrat',  'ValueVariant',  DmModOS.QrOSCabEntContrat.Value, True);
  MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEntContrat', 'KeyValue',  DmModOS.QrOSCabEntContrat.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdEntPagante',  'ValueVariant',  DmModOS.QrOSCabEntPagante.Value, True);
  MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEntPagante', 'KeyValue',  DmModOS.QrOSCabEntPagante.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdValorPre',  'ValueVariant',  DmModOS.QrOSCabValorPre.Value, True);
  MyVCLref.SET_Component(TForm(Form), 'CGOperacao', 'Value',  DmModOS.QrOSCabOperacao.Value, TdmkCheckGroup);
  MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPFimVisPrv', 'Date',  DmModOS.QrOSCabFimVisPrv.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdFimVisPrv',  'ValueVariant',  DmModOS.QrOSCabFimVisPrv.Value, True);
  MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPFimExePrv', 'Date',  DmModOS.QrOSCabFimExePrv.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdFimExePrv',  'ValueVariant',  DmModOS.QrOSCabFimExePrv.Value, True);
  MyVCLref.SET_dmkEdit(TForm(Form), 'EdAgeEqiCab',  'ValueVariant',  DmModOS.QrOSCabAgeEqiCab.Value, True);
  MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBAgeEqiCab', 'KeyValue',  DmModOS.QrOSCabAgeEqiCab.Value, True);
  MyVCLref.SET_dmkMemo(TForm(Form), 'MeObsGaranti',  'Lines.Text',  DmModOS.QrOSCabObsGaranti.Value, True);
  MyVCLref.SET_dmkMemo(TForm(Form), 'MeObsExecuta',  'Lines.Text',  DmModOS.QrOSCabObsExecuta.Value, True);
  //
  Result := Form;
end;

end.
