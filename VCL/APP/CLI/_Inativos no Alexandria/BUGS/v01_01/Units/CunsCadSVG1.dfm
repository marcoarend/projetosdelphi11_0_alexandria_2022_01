object FmCunsCadSVG1: TFmCunsCadSVG1
  Left = 0
  Top = 0
  Caption = 'CAD-SUBCL-014 :: Croqui'
  ClientHeight = 658
  ClientWidth = 1217
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 17
  object Splitter1: TSplitter
    Left = 813
    Top = 0
    Width = 13
    Height = 658
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alRight
  end
  object PageControl1: TPageControl
    Left = 826
    Top = 0
    Width = 391
    Height = 658
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet3
    Align = alRight
    TabOrder = 0
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'PMVs croqui'
      ImageIndex = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 383
        Height = 626
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsPMVsSim
        PopupMenu = PMSim
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SvgZumF'
            Title.Caption = 'Zoom'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SvgPosX'
            Title.Caption = 'X'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SvgPosY'
            Title.Caption = 'Y'
            Width = 32
            Visible = True
          end>
      end
    end
    object TabSheet3: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Outros PMVs'
      ImageIndex = 2
      object LaAviso: TLabel
        Left = 0
        Top = 63
        Width = 383
        Height = 18
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Caption = 'De um duplo clique no item da grade para posicion'#225'-lo no croqui'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 474
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 81
        Width = 383
        Height = 545
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsPMVsNao
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = DBGrid2DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SiapImaSVG'
            Title.Caption = 'ID Croqui'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SvgZumF'
            Title.Caption = 'Zoom'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SvgPosX'
            Title.Caption = 'X'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SvgPosY'
            Title.Caption = 'Y'
            Width = 32
            Visible = True
          end>
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 383
        Height = 63
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 1
        object BtOK: TBitBtn
          Tag = 30
          Left = 5
          Top = 5
          Width = 53
          Height = 53
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtOKClick
        end
      end
    end
    object TabSheet6: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Itens'
      ImageIndex = 5
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 0
        Top = 0
        Width = 383
        Height = 626
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsItens
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'PipCad'
            Title.Caption = 'ID PMV'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Nome PMV'
            Width = 148
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ITENS'
            Width = 48
            Visible = True
          end>
      end
    end
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Vetor'
      ImageIndex = 4
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 383
        Height = 626
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object TreeView1: TTreeView
          Left = 1
          Top = 27
          Width = 381
          Height = 598
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          HideSelection = False
          Indent = 19
          ParentFont = False
          TabOrder = 0
          ExplicitTop = 22
          ExplicitHeight = 603
        end
        object Edit1: TEdit
          Left = 1
          Top = 1
          Width = 381
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 1
          TextHint = 'ID name to search'
          OnChange = Edit1Change
        end
      end
    end
    object TabSheet4: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Legenda'
      ImageIndex = 4
      object SbLegenda: TScrollBox
        Left = 0
        Top = 0
        Width = 383
        Height = 626
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        HorzScrollBar.Tracking = True
        VertScrollBar.Tracking = True
        Align = alClient
        AutoSize = True
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        DoubleBuffered = True
        Color = clBtnFace
        ParentColor = False
        ParentDoubleBuffered = False
        TabOrder = 0
        object RSSVGImgLegenda: TRSSVGImage
          Left = 0
          Top = 0
          Width = 383
          Height = 626
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Opacity = 1.000000000000000000
          WrapMode = iwOriginal
          ScaleOriginal = 1.000000000000000000
          Align = alClient
          AutoSize = True
          ExplicitWidth = 381
          ExplicitHeight = 621
        end
      end
    end
    object TabSheet5: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'TabSheet5'
      ImageIndex = 5
      object Panel2: TPanel
        Left = 78
        Top = 167
        Width = 242
        Height = 148
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Panel2'
        TabOrder = 0
        object Button1: TButton
          Left = 68
          Top = 52
          Width = 98
          Height = 33
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Button1'
          TabOrder = 0
          OnClick = Button1Click
        end
        object Button2: TButton
          Left = 68
          Top = 99
          Width = 98
          Height = 33
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Button2'
          TabOrder = 1
          OnClick = Button2Click
        end
      end
    end
  end
  object SBCroqui: TScrollBox
    Left = 0
    Top = 0
    Width = 813
    Height = 658
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    HorzScrollBar.Tracking = True
    VertScrollBar.Tracking = True
    Align = alClient
    AutoSize = True
    BevelInner = bvNone
    BevelOuter = bvNone
    BorderStyle = bsNone
    DoubleBuffered = True
    Color = clWhite
    ParentColor = False
    ParentDoubleBuffered = False
    TabOrder = 1
    object RSSVGImgCroqui: TRSSVGImage
      Left = 0
      Top = 0
      Width = 813
      Height = 658
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Opacity = 1.000000000000000000
      SVGDocument = RSSVGDocument1
      WrapMode = iwOriginal
      ScaleOriginal = 1.000000000000000000
      Align = alClient
      AutoSize = True
    end
  end
  object RSSVGDocument1: TRSSVGDocument
    DefaultTextRendering = txrnOptimizeLegibility
    PreferredLanguage = 'EN'
    Left = 68
    Top = 236
  end
  object QrSiapImaSVG: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM siapimasvg '
      'WHERE Controle>0')
    Left = 72
    Top = 188
    object QrSiapImaSVGCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaSVGControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapImaSVGNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrSiapImaSVGNoArq: TWideStringField
      FieldName = 'NoArq'
      Required = True
      Size = 255
    end
    object QrSiapImaSVGArquivo: TWideMemoField
      FieldName = 'Arquivo'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSiapImaSVGLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSiapImaSVGDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSiapImaSVGDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSiapImaSVGUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSiapImaSVGUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSiapImaSVGAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSiapImaSVGAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrPMVsSim: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mot.Nome NO_MOTDESAT, gg1.Nome NO_EQUI, gg1.Nivel1, '
      'plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA, '
      'IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT, '
      'IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT, '
      'cad.* '
      'FROM pipcad cad '
      'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ '
      'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab '
      'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci '
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci '
      'LEFT JOIN osmoncab omc ON omc.Conta=cad.OsMonCab '
      'LEFT JOIN oscab osc ON osc.Codigo=omc.Codigo '
      'WHERE osc.Entidade=1433'
      'AND (cad.MotInutili = 0 AND cad.MotInutili = 0) '
      ''
      'ORDER BY Nome ')
    Left = 72
    Top = 52
    object QrPMVsSimNO_MOTDESAT: TWideStringField
      FieldName = 'NO_MOTDESAT'
      Size = 60
    end
    object QrPMVsSimNO_EQUI: TWideStringField
      FieldName = 'NO_EQUI'
      Size = 120
    end
    object QrPMVsSimNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPMVsSimNO_LISTA: TWideStringField
      FieldName = 'NO_LISTA'
      Size = 100
    end
    object QrPMVsSimNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrPMVsSimDtaAquis_TXT: TWideStringField
      FieldName = 'DtaAquis_TXT'
      Required = True
      Size = 10
    end
    object QrPMVsSimDtaDesativ_TXT: TWideStringField
      FieldName = 'DtaDesativ_TXT'
      Required = True
      Size = 10
    end
    object QrPMVsSimCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPMVsSimNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPMVsSimEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrPMVsSimOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrPMVsSimMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrPMVsSimDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrPMVsSimDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrPMVsSimLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPMVsSimDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPMVsSimDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPMVsSimUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPMVsSimUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPMVsSimAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPMVsSimAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPMVsSimPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrPMVsSimDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrPMVsSimIntrvMonDD: TIntegerField
      FieldName = 'IntrvMonDD'
    end
    object QrPMVsSimOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPMVsSimReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrPMVsSimMotInutili: TIntegerField
      FieldName = 'MotInutili'
    end
    object QrPMVsSimDtaInutili: TDateField
      FieldName = 'DtaInutili'
    end
    object QrPMVsSimSvgPosX: TFloatField
      FieldName = 'SvgPosX'
    end
    object QrPMVsSimSvgPosY: TFloatField
      FieldName = 'SvgPosY'
    end
    object QrPMVsSimSvgZumF: TFloatField
      FieldName = 'SvgZumF'
    end
    object QrPMVsSimSiapImaSVG: TIntegerField
      FieldName = 'SiapImaSVG'
    end
  end
  object DsPMVsSim: TDataSource
    DataSet = QrPMVsSim
    Left = 72
    Top = 100
  end
  object QrPMVsNao: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mot.Nome NO_MOTDESAT, gg1.Nome NO_EQUI, gg1.Nivel1, '
      'plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA, '
      'IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT, '
      'IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT, '
      'cad.* '
      'FROM pipcad cad '
      'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ '
      'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab '
      'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci '
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci '
      'LEFT JOIN osmoncab omc ON omc.Conta=cad.OsMonCab '
      'LEFT JOIN oscab osc ON osc.Codigo=omc.Codigo '
      'WHERE osc.Entidade=1433'
      'AND (cad.MotInutili = 0 AND cad.MotInutili = 0) '
      ''
      'ORDER BY Nome ')
    Left = 136
    Top = 52
    object QrPMVsNaoNO_MOTDESAT: TWideStringField
      FieldName = 'NO_MOTDESAT'
      Size = 60
    end
    object QrPMVsNaoNO_EQUI: TWideStringField
      FieldName = 'NO_EQUI'
      Size = 120
    end
    object QrPMVsNaoNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPMVsNaoNO_LISTA: TWideStringField
      FieldName = 'NO_LISTA'
      Size = 100
    end
    object QrPMVsNaoNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrPMVsNaoDtaAquis_TXT: TWideStringField
      FieldName = 'DtaAquis_TXT'
      Required = True
      Size = 10
    end
    object QrPMVsNaoDtaDesativ_TXT: TWideStringField
      FieldName = 'DtaDesativ_TXT'
      Required = True
      Size = 10
    end
    object QrPMVsNaoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPMVsNaoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPMVsNaoEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrPMVsNaoOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrPMVsNaoMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrPMVsNaoDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrPMVsNaoDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrPMVsNaoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPMVsNaoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPMVsNaoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPMVsNaoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPMVsNaoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPMVsNaoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPMVsNaoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPMVsNaoPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrPMVsNaoDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrPMVsNaoIntrvMonDD: TIntegerField
      FieldName = 'IntrvMonDD'
    end
    object QrPMVsNaoOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPMVsNaoReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrPMVsNaoMotInutili: TIntegerField
      FieldName = 'MotInutili'
    end
    object QrPMVsNaoDtaInutili: TDateField
      FieldName = 'DtaInutili'
    end
    object QrPMVsNaoSvgPosX: TFloatField
      FieldName = 'SvgPosX'
    end
    object QrPMVsNaoSvgPosY: TFloatField
      FieldName = 'SvgPosY'
    end
    object QrPMVsNaoSvgZumF: TFloatField
      FieldName = 'SvgZumF'
    end
    object QrPMVsNaoSiapImaSVG: TIntegerField
      FieldName = 'SiapImaSVG'
    end
  end
  object DsPMVsNao: TDataSource
    DataSet = QrPMVsNao
    Left = 136
    Top = 100
  end
  object QrGraG1EqMo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM grag1eqmo')
    Left = 72
    Top = 144
    object QrGraG1EqMoNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraG1EqMoMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraG1EqMoObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGraG1EqMoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraG1EqMoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraG1EqMoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraG1EqMoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraG1EqMoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraG1EqMoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraG1EqMoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraG1EqMoPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrGraG1EqMoNaoUsaPrdt: TSmallintField
      FieldName = 'NaoUsaPrdt'
    end
    object QrGraG1EqMoVetorSvg: TWideMemoField
      FieldName = 'VetorSvg'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGraG1EqMoVetorArq: TWideStringField
      FieldName = 'VetorArq'
      Required = True
      Size = 255
    end
    object QrGraG1EqMoPrgICorSVG: TIntegerField
      FieldName = 'PrgICorSVG'
    end
    object QrGraG1EqMoDiasCorSVG: TIntegerField
      FieldName = 'DiasCorSVG'
    end
    object QrGraG1EqMoQtCorSVGgg: TIntegerField
      FieldName = 'QtCorSVGgg'
    end
    object QrGraG1EqMoQtCorSVGgr: TIntegerField
      FieldName = 'QtCorSVGgr'
    end
    object QrGraG1EqMoQtCorSVGrg: TIntegerField
      FieldName = 'QtCorSVGrg'
    end
    object QrGraG1EqMoQtCorSVGrr: TIntegerField
      FieldName = 'QtCorSVGrr'
    end
    object QrGraG1EqMoQtCorSVGrb: TIntegerField
      FieldName = 'QtCorSVGrb'
    end
    object QrGraG1EqMoQtCorSVGbr: TIntegerField
      FieldName = 'QtCorSVGbr'
    end
    object QrGraG1EqMoZumPadr: TFloatField
      FieldName = 'ZumPadr'
    end
    object QrGraG1EqMoSVGLegenda: TWideStringField
      FieldName = 'SVGLegenda'
      Size = 50
    end
  end
  object PMPMV: TPopupMenu
    Left = 252
    Top = 260
    object Zoom1: TMenuItem
      Caption = 'Altera &Zoom'
      OnClick = Zoom1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object DefiniroZoomatualcomopadro1: TMenuItem
      Caption = '&Definir o Zoom atual como padr'#227'o'
      OnClick = DefiniroZoomatualcomopadro1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object RemovePMVdocroqui1: TMenuItem
      Caption = '&Remove PMV do croqui'
      OnClick = RemovePMVdocroqui1Click
    end
  end
  object QrAtaques: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(opi.Conta) ITENS'
      'FROM ospipits opi'
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo'
      'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstits'
      'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts'
      'LEFT JOIN ospipmon omc ON omc.Controle=opi.Controle'
      'LEFT JOIN pipcad pmv ON pmv.Codigo=omc.PipCad'
      'LEFT JOIN grag1eqmo emo ON emo.Nivel1=pmv.Equipamento'
      'WHERE cab.Entidade>0'
      'AND omc.PipCad>0'
      'AND TO_DAYS(SYSDATE()) - TO_DAYS(cab.DtaExeFim) > 0'
      
        'AND TO_DAYS(SYSDATE()) - TO_DAYS(cab.DtaExeFim) <= emo.DiasCorSV' +
        'G'
      'AND opi.RespAtrIts>0'
      'AND pai.Efeito=0')
    Left = 184
    Top = 364
    object QrAtaquesITENS: TFloatField
      FieldName = 'ITENS'
    end
  end
  object QrItens: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT omc.PipCad, pmv.Nome, (COUNT(opi.Conta) + 0.000) ITENS  '
      'FROM ospipits opi '
      'LEFT JOIN ospipmon omc ON omc.Controle=opi.Controle '
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo '
      'LEFT JOIN pipcad pmv ON pmv.Codigo=omc.PipCad '
      'LEFT JOIN grag1eqmo emo ON emo.Nivel1=pmv.Equipamento '
      'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstits '
      'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts '
      'WHERE cab.SiapTerCad=1527'
      'AND TO_DAYS(SYSDATE()) - TO_DAYS(cab.DtaExeFim) > 0  '
      
        'AND TO_DAYS(SYSDATE()) - TO_DAYS(cab.DtaExeFim) <= emo.DiasCorSV' +
        'G '
      'AND opi.RespAtrIts>0  '
      'AND (pai.Efeito=0 OR pai.Efeito IS NULL) '
      'GROUP BY omc.PipCad')
    Left = 388
    Top = 208
    object QrItensPipCad: TIntegerField
      FieldName = 'PipCad'
    end
    object QrItensNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrItensITENS: TFloatField
      FieldName = 'ITENS'
    end
  end
  object DsItens: TDataSource
    DataSet = QrItens
    Left = 388
    Top = 256
  end
  object PMSim: TPopupMenu
    Left = 436
    Top = 80
    object Zoomeposio1: TMenuItem
      Caption = 'Zoom e posi'#231#227'o'
      OnClick = Zoomeposio1Click
    end
  end
  object QrGG1EM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome, gem.VetorSvg '
      'QtCorSVGgg, QtCorSVGgr, QtCorSVGrg, '
      'QtCorSVGrr, QtCorSVGrb, QtCorSVGbr '
      'FROM gragru1 gg1'
      'LEFT JOIN grag1eqmo gem ON gem.Nivel1=gg1.Nivel1'
      'WHERE gg1.GraTabApp=24002'
      'ORDER BY gg1.Nome')
    Left = 548
    Top = 232
    object QrGG1EMNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGG1EMNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrGG1EMVetorSvg: TWideMemoField
      FieldName = 'VetorSvg'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGG1EMQtCorSVGgg: TIntegerField
      FieldName = 'QtCorSVGgg'
    end
    object QrGG1EMQtCorSVGgr: TIntegerField
      FieldName = 'QtCorSVGgr'
    end
    object QrGG1EMQtCorSVGrg: TIntegerField
      FieldName = 'QtCorSVGrg'
    end
    object QrGG1EMQtCorSVGrr: TIntegerField
      FieldName = 'QtCorSVGrr'
    end
    object QrGG1EMQtCorSVGrb: TIntegerField
      FieldName = 'QtCorSVGrb'
    end
    object QrGG1EMQtCorSVGbr: TIntegerField
      FieldName = 'QtCorSVGbr'
    end
    object QrGG1EMSVGLegenda: TWideStringField
      FieldName = 'SVGLegenda'
      Size = 60
    end
    object QrGG1EMDiasCorSVG: TIntegerField
      FieldName = 'DiasCorSVG'
    end
  end
end
