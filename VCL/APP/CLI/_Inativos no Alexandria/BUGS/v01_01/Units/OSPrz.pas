unit OSPrz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, dmkValUsu, dmkCheckBox, UnDmkEnums;

type
  TFmOSPrz = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    QrOSCab: TmySQLQuery;
    DsOSCab: TDataSource;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit15: TDBEdit;
    QrPediPrzCab: TmySQLQuery;
    DsPediPrzCab: TDataSource;
    Label2: TLabel;
    EdCondicao: TdmkEditCB;
    CBCondicao: TdmkDBLookupComboBox;
    EdControle: TdmkEdit;
    Label1: TLabel;
    CkContinuar: TCheckBox;
    VuCondicao: TdmkValUsu;
    EdDescoPer: TdmkEdit;
    Label6: TLabel;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    CkEscolhido: TdmkCheckBox;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEmpresa: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabDtaLibFat: TDateTimeField;
    QrOSCabDtaFimFat: TDateTimeField;
    QrOSCabValorServi: TFloatField;
    QrOSCabValorDesco: TFloatField;
    QrOSCabValorOutrs: TFloatField;
    QrOSCabValorTotal: TFloatField;
    QrOSCabDdsPosVda: TIntegerField;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    QrOSCabEntContrat: TIntegerField;
    QrOSCabCondicaoPg: TIntegerField;
    QrOSCabCartEmis: TIntegerField;
    QrOSCabSerNF: TWideStringField;
    QrOSCabNumNF: TIntegerField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabInvalServi: TFloatField;
    QrOSCabInvalDesco: TFloatField;
    QrOSCabInvalOutrs: TFloatField;
    QrOSCabInvalTotal: TFloatField;
    QrOSCabOrcamServi: TFloatField;
    QrOSCabOrcamDesco: TFloatField;
    QrOSCabOrcamOutrs: TFloatField;
    QrOSCabOrcamTotal: TFloatField;
    QrOSCabValiDdOrca: TIntegerField;
    QrOSCabOperacao: TSmallintField;
    QrOSCabExeTxtCli1: TIntegerField;
    QrOSCabNO_FatoGeradr: TWideStringField;
    QrOSCabNO_ESTATUS: TWideStringField;
    QrOSCabNO_SiapTerCad: TWideStringField;
    QrOSCabNO_ENT: TWideStringField;
    SpeedButton6: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCondicaoChange(Sender: TObject);
    procedure QrOSCabAfterOpen(DataSet: TDataSet);
    procedure SpeedButton6Click(Sender: TObject);
    procedure EdDescoPerKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrInsUpd: TmySQLQuery;
    FValorIni, FValorFim, FPorcento: Double;
    //
    procedure ReopenPedPrzCab();
  end;

  var
  FmOSPrz: TFmOSPrz;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, UnOSApp_PF, CalculaPercentual,
  UnMyVCLRef, UnPraz_PF;

{$R *.DFM}

procedure TFmOSPrz.BtOKClick(Sender: TObject);
var
  Codigo, Controle, ConCodUsu, Condicao, Escolhido, Parcelas: Integer;
  DescoPer: Double;
begin
  Codigo     := QrOSCabCodigo.Value;
  Controle   := EdControle.ValueVariant;
  ConCodUsu  := EdCondicao.ValueVariant;
  DescoPer   := EdDescoPer.ValueVariant;
  Escolhido  := Geral.BoolToInt(CkEscolhido.Checked);
  Parcelas   := QrPediPrzCabParcelas.Value;
  //
  if MyObjects.FIC(ConCodUsu = 0, EdCondicao,
  'Informe a Condi��o de Pagamento') then
    Exit;

  if not UMyMod.ObtemCodigoDeCodUsu(EdCondicao, Condicao, '') then
    Exit;

  Controle := UMyMod.BPGS1I32('osprz', 'Controle', '', '',
    tsDef, ImgTipo.SQLType, Controle);

  //
  // Definir todas as condi��es da OS como n�o escolhidas
  if Escolhido = 1 then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osprz', False, [
    'Escolhido'], ['Codigo'], [0], [Codigo], True);
  end;
  //

  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osprz', False, [
  'Codigo', 'Condicao', 'DescoPer', 'Parcelas', 'Escolhido'], [
  'Controle'], [
  Codigo, Condicao, DescoPer, Parcelas, Escolhido
  ], [
  Controle], True) then
  begin
    //DmModOS.TotalValorOS(Codigo, TGraBugsOpera(QrOSCabOperacao.Value));
    OSApp_PF.TotalValorOS(Codigo);
    if FQrInsUpd <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrInsUpd, Dmod.MyDB);
      FQrInsUpd.Locate('Controle', Controle, []);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdControle.ValueVariant := 0;
      EdCondicao.ValueVariant := 0;
      CBCondicao.KeyValue := 0;
    end else
      Close;
  end;
{
var
  Codigo, Controle, Condicao: Integer;
  DescoPer, BaseVal, DescoVal, TotaVal: Double;
begin
  Codigo         := ;
  Controle       := ;
  Condicao       := ;
  DescoPer       := ;
  BaseVal        := ;
  DescoVal       := ;
  TotaVal        := ;

  //
? := UMyMod.BuscaEmLivreY_Def('osprz', 'Controle', ImgTipo.SQLType?, CodAtual?);
ou > ? := UMyMod.BPGS1I32('osprz', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, 'osprz', auto_increment?[
'Codigo', 'Condicao', 'DescoPer',
'BaseVal', 'DescoVal', 'TotaVal'], [
'Controle'], [
Codigo, Condicao, DescoPer,
BaseVal, DescoVal, TotaVal], [
Controle], UserDataAlterweb?, IGNORE?
}
end;

procedure TFmOSPrz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPrz.EdCondicaoChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    EdDescoPer.ValueVariant := QrPediPrzCabMaxDesco.Value;
end;

procedure TFmOSPrz.EdDescoPerKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const
  CasasValI = 2;
  CasasValF = 2;
  CasasPerc = 6;
var
  ValorValI, ValorValF, ValorPerc: Double;
begin
  if Key = VK_F4 then
  begin
    ValorValI := FValorIni;
    ValorValF := 0;
    ValorPerc := EdDescoPer.ValueVariant;
    if MyVCLRef.CalculaPercentual(TFmCalculaPercentual, FmCalculaPercentual,
    fcpercDesconto,
    CasasValI, CasasValF, CasasPerc,
    ValorValI, ValorValF, ValorPerc) then
      EdDescoPer.ValueVariant := ValorPerc;
  end;
end;

procedure TFmOSPrz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSPrz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPrz.QrOSCabAfterOpen(DataSet: TDataSet);
begin
  ReopenPedPrzCab();
end;

procedure TFmOSPrz.ReopenPedPrzCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT ppc.MaxDesco, ppc.Parcelas, ',
  'ppc.Codigo, ppc.CodUsu, ppc.Nome ',
  'FROM pediprzcab ppc ',
  'LEFT JOIN pediprzemp ppe ON ppe.Codigo=ppc.Codigo ',
  'LEFT JOIN enticliint eci ON eci.CodEnti=ppe.Empresa ',
  'WHERE eci.CodCliInt=' + Geral.FF0(QrOSCabEmpresa.Value),
  'ORDER BY ppc.Nome ',
  ' ']);
end;

procedure TFmOSPrz.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  Praz_PF.MostraFormPediPrzCab1(EdCondicao.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdCondicao, CBCondicao, QrPediPrzCab, VAR_CADASTRO);
    //
    EdCondicao.SetFocus;
  end;
end;

(*
object LaOSOrigem: TLabel
  Left = 608
  Top = 60
  Width = 54
  Height = 13
  Caption = 'OS Origem:'
  Color = clBtnFace
  Enabled = False
  ParentColor = False
end
object DBEdit9: TDBEdit
  Left = 664
  Top = 56
  Width = 105
  Height = 21
  TabStop = False
  DataField = 'OSOrigem'
  DataSource = DsOSCab
  TabOrder = 9
end

*)
end.
