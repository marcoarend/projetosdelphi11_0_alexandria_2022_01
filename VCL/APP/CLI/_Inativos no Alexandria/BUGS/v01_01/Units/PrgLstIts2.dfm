object FmPrgLstIts2: TFmPrgLstIts2
  Left = 339
  Top = 185
  Caption = 'PRG-LISTA-002 :: Item de Lista de Pergunta'
  ClientHeight = 774
  ClientWidth = 999
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 999
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 940
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 881
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 366
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Lista de Pergunta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 366
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Lista de Pergunta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 366
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Lista de Pergunta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 999
    Height = 575
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 999
      Height = 575
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 999
        Height = 575
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PnDados: TPanel
          Left = 2
          Top = 18
          Width = 995
          Height = 170
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaPergunta: TLabel
            Left = 10
            Top = 10
            Width = 57
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pergunta:'
          end
          object SbPrgCadPrg: TSpeedButton
            Left = 465
            Top = 31
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SbPrgCadPrgClick
          end
          object RGFuncoes: TdmkRadioGroup
            Left = 10
            Top = 65
            Width = 481
            Height = 101
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Fun'#231#227'o desta pergunta: '
            Columns = 2
            Items.Strings = (
              '1'
              '2'
              '3. AppListas.sListaFuncaoPrgAtrCab'
              '4'
              '5')
            TabOrder = 0
            OnClick = RGFuncoesClick
            QryCampo = 'Funcoes'
            UpdCampo = 'Funcoes'
            UpdType = utYes
            OldValor = 0
          end
          object CBPergunta: TComboBox
            Left = 10
            Top = 31
            Width = 450
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            AutoDropDown = True
            TabOrder = 1
            OnExit = CBPerguntaExit
          end
        end
        object PnFuncao: TPanel
          Left = 2
          Top = 188
          Width = 995
          Height = 165
          Align = alTop
          BevelOuter = bvNone
          Caption = 'PnFuncao'
          TabOrder = 1
          object PCFuncoes: TPageControl
            Left = 0
            Top = 0
            Width = 995
            Height = 165
            ActivePage = TS1
            Align = alClient
            TabOrder = 0
            object TS1: TTabSheet
              Caption = 'TS1'
              object LaBinarCad0: TLabel
                Left = 10
                Top = 10
                Width = 92
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Texto negativo:'
              end
              object SBBinarCad0: TSpeedButton
                Left = 165
                Top = 31
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SBBinarCad0Click
              end
              object LaBinarCad1: TLabel
                Left = 10
                Top = 58
                Width = 98
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Texto afirmativo:'
              end
              object SBBinarCad1: TSpeedButton
                Left = 165
                Top = 79
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SBBinarCad1Click
              end
              object CBBinarCad0: TComboBox
                Left = 10
                Top = 31
                Width = 150
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                AutoDropDown = True
                TabOrder = 0
                OnExit = CBBinarCad0Exit
              end
              object CBBinarCad1: TComboBox
                Left = 10
                Top = 79
                Width = 150
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                AutoDropDown = True
                TabOrder = 1
                OnExit = CBBinarCad1Exit
              end
            end
            object TS2: TTabSheet
              Caption = 'TS2'
              ImageIndex = 1
              object RGCasasQtde: TdmkRadioGroup
                Left = 12
                Top = 12
                Width = 267
                Height = 56
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Casas decimais  para quantidade: '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  '0'
                  '1'
                  '2'
                  '3')
                TabOrder = 0
                QryCampo = 'CasasQtde'
                UpdCampo = 'CasasQtde'
                UpdType = utYes
                OldValor = 0
              end
            end
            object TS3: TTabSheet
              Caption = 'TS3'
              ImageIndex = 2
              object RGAcaoPrd: TdmkRadioGroup
                Left = 12
                Top = 12
                Width = 730
                Height = 61
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Item obrigatorio para a fun'#231#227'o "Adi/reti. Produto": '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  'N/I'
                  'Adiciona'
                  'Substitui'
                  'Remove')
                TabOrder = 0
                QryCampo = 'Relacao'
                UpdCampo = 'Relacao'
                UpdType = utYes
                OldValor = 0
              end
            end
            object TS4: TTabSheet
              Caption = 'TS4'
              ImageIndex = 3
            end
            object TS5: TTabSheet
              Caption = 'TS5'
              ImageIndex = 4
            end
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 353
          Width = 995
          Height = 220
          Align = alClient
          BevelInner = bvLowered
          BevelKind = bkFlat
          BevelOuter = bvLowered
          TabOrder = 2
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 634
    Width = 999
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 995
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 688
    Width = 999
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 820
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 818
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
  object QrPrgCadPrg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM prgcadprg'
      'ORDER BY Nome')
    Left = 580
    Top = 76
    object QrPrgCadPrgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgCadPrgNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrPrgBinCad0: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM prgbincad'
      'ORDER BY Nome')
    Left = 584
    Top = 156
    object QrPrgBinCad0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgBinCad0Nome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object QrPrgBinCad1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM prgbincad'
      'ORDER BY Nome')
    Left = 688
    Top = 156
    object QrPrgBinCad1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgBinCad1Nome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
end
