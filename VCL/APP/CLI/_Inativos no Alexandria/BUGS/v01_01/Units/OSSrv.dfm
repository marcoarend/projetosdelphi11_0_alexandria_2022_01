object FmOSSrv: TFmOSSrv
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-003 :: Ordem de Servi'#231'o - Servi'#231'o'
  ClientHeight = 569
  ClientWidth = 597
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 549
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 501
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 333
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 333
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 333
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 597
    Height = 407
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 597
      Height = 407
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 85
        Width = 597
        Height = 322
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 593
          Height = 166
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label2: TLabel
            Left = 96
            Top = 0
            Width = 39
            Height = 13
            Caption = 'Servi'#231'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label1: TLabel
            Left = 16
            Top = 0
            Width = 51
            Height = 13
            Caption = 'ID servi'#231'o:'
            Color = clBtnFace
            Enabled = False
            ParentColor = False
          end
          object Label25: TLabel
            Left = 16
            Top = 41
            Width = 64
            Height = 13
            Caption = 'Garantia (dd):'
          end
          object Label26: TLabel
            Left = 88
            Top = 41
            Width = 73
            Height = 13
            Caption = 'Evacua'#231#227'o (h):'
          end
          object Label27: TLabel
            Left = 168
            Top = 41
            Width = 66
            Height = 13
            Caption = 'Execu'#231#227'o (h):'
          end
          object Label6: TLabel
            Left = 240
            Top = 41
            Width = 59
            Height = 13
            Caption = '$ Calculado:'
            Enabled = False
          end
          object Label8: TLabel
            Left = 324
            Top = 41
            Width = 59
            Height = 13
            Caption = '$ Informado:'
          end
          object Label10: TLabel
            Left = 408
            Top = 41
            Width = 58
            Height = 13
            Caption = '$ Desconto:'
            Enabled = False
            Visible = False
          end
          object Label11: TLabel
            Left = 492
            Top = 41
            Width = 36
            Height = 13
            Caption = '$ Total:'
            Visible = False
          end
          object SpeedButton6: TSpeedButton
            Left = 557
            Top = 16
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton6Click
          end
          object EdDesServico: TdmkEditCB
            Left = 96
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'DesServico'
            UpdCampo = 'DesServico'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBDesServico
            IgnoraDBLookupComboBox = False
          end
          object CBDesServico: TdmkDBLookupComboBox
            Left = 152
            Top = 16
            Width = 402
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsDesServico
            TabOrder = 2
            dmkEditCB = EdDesServico
            QryCampo = 'DesServico'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdControle: TdmkEdit
            Left = 16
            Top = 16
            Width = 76
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object CkContinuar: TCheckBox
            Left = 16
            Top = 144
            Width = 213
            Height = 17
            Caption = 'Continuar inserindo ap'#243's a confirma'#231#227'o.'
            TabOrder = 13
          end
          object EdGarantiaDd: TdmkEdit
            Left = 16
            Top = 56
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'GarantiaDd'
            UpdCampo = 'GarantiaDd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdHrEvacuar: TdmkEdit
            Left = 88
            Top = 56
            Width = 77
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'HrEvacuar'
            UpdCampo = 'HrEvacuar'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdHrExecutar: TdmkEdit
            Left = 168
            Top = 56
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'HrExecutar'
            UpdCampo = 'HrExecutar'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdValCalc: TdmkEdit
            Left = 240
            Top = 56
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValCalc'
            UpdCampo = 'ValCalc'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdValCalcChange
          end
          object EdValInfo: TdmkEdit
            Left = 324
            Top = 56
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValInfo'
            UpdCampo = 'ValInfo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdValInfoChange
          end
          object EdValDesc: TdmkEdit
            Left = 408
            Top = 56
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 8
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValDesc'
            UpdCampo = 'ValDesc'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdValDescChange
          end
          object EdValTota: TdmkEdit
            Left = 492
            Top = 56
            Width = 86
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 9
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValTota'
            UpdCampo = 'ValTota'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object RGAutorizado: TdmkRadioGroup
            Left = 184
            Top = 80
            Width = 115
            Height = 61
            Caption = ' Autorizado: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o'
              'Sim')
            TabOrder = 11
            QryCampo = 'Autorizado'
            UpdCampo = 'Autorizado'
            UpdType = utYes
            OldValor = 0
          end
          object GBMonitoramento: TGroupBox
            Left = 16
            Top = 80
            Width = 161
            Height = 61
            Caption = ' Monitoramento: '
            TabOrder = 10
            object Label12: TLabel
              Left = 12
              Top = 17
              Width = 47
              Height = 13
              Caption = 'Dias total:'
            end
            object Label13: TLabel
              Left = 84
              Top = 17
              Width = 66
              Height = 13
              Caption = 'Intervalo dias:'
            end
            object EdMoniDdTotl: TdmkEdit
              Left = 12
              Top = 32
              Width = 68
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'MoniDdTotl'
              UpdCampo = 'MoniDdTotl'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdMoniDdIntv: TdmkEdit
              Left = 84
              Top = 32
              Width = 68
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'MoniDdIntv'
              UpdCampo = 'MoniDdIntv'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
          object RGTudoFeitoM: TdmkRadioGroup
            Left = 304
            Top = 80
            Width = 115
            Height = 61
            Caption = ' 100% realizado: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o'
              'Sim')
            TabOrder = 12
            QryCampo = 'TudoFeitoM'
            UpdCampo = 'TudoFeitoM'
            UpdType = utYes
            OldValor = 0
          end
        end
        object MeDetalhes: TdmkMemo
          Left = 2
          Top = 181
          Width = 593
          Height = 139
          Align = alClient
          TabOrder = 1
          QryCampo = 'Detalhes'
          UpdCampo = 'Detalhes'
          UpdType = utYes
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 597
        Height = 85
        Align = alTop
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        object Label7: TLabel
          Left = 16
          Top = 12
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label9: TLabel
          Left = 16
          Top = 36
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label4: TLabel
          Left = 356
          Top = 12
          Width = 33
          Height = 13
          Caption = 'Status:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label3: TLabel
          Left = 16
          Top = 60
          Width = 30
          Height = 13
          Caption = 'Lugar:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label5: TLabel
          Left = 116
          Top = 12
          Width = 63
          Height = 13
          Caption = 'Fato gerador:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdit4: TDBEdit
          Left = 56
          Top = 32
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Entidade'
          DataSource = DsOSCab
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 112
          Top = 32
          Width = 468
          Height = 21
          TabStop = False
          DataField = 'NO_ENT'
          DataSource = DsOSCab
          TabOrder = 2
        end
        object DBEdit6: TDBEdit
          Left = 392
          Top = 8
          Width = 32
          Height = 21
          TabStop = False
          DataField = 'Estatus'
          DataSource = DsOSCab
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 424
          Top = 8
          Width = 156
          Height = 21
          TabStop = False
          DataField = 'NO_ESTATUS'
          DataSource = DsOSCab
          TabOrder = 4
        end
        object DBEdit2: TDBEdit
          Left = 56
          Top = 56
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'SiapTerCad'
          DataSource = DsOSCab
          TabOrder = 5
        end
        object DBEdit1: TDBEdit
          Left = 112
          Top = 56
          Width = 468
          Height = 21
          TabStop = False
          DataField = 'NO_SiapTerCad'
          DataSource = DsOSCab
          TabOrder = 6
        end
        object DBEdit8: TDBEdit
          Left = 184
          Top = 8
          Width = 28
          Height = 21
          TabStop = False
          DataField = 'FatoGeradr'
          DataSource = DsOSCab
          TabOrder = 7
        end
        object DBEdit7: TDBEdit
          Left = 212
          Top = 8
          Width = 140
          Height = 21
          TabStop = False
          DataField = 'NO_FatoGeradr'
          DataSource = DsOSCab
          TabOrder = 8
        end
        object DBEdit15: TDBEdit
          Left = 56
          Top = 8
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsOSCab
          TabOrder = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 455
    Width = 597
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 593
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 499
    Width = 597
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 451
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 449
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOSCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.*,  fge.Nome NO_FatoGeradr,'
      'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM oscab cab'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad'
      ''
      'WHERE cab.Codigo > 0')
    Left = 264
    Top = 84
    object QrOSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrOSCabEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object QrOSCabFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrOSCabDtaContat: TDateTimeField
      FieldName = 'DtaContat'
    end
    object QrOSCabDtaVisPrv: TDateTimeField
      FieldName = 'DtaVisPrv'
    end
    object QrOSCabDtaVisExe: TDateTimeField
      FieldName = 'DtaVisExe'
    end
    object QrOSCabDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
    end
    object QrOSCabDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
    end
    object QrOSCabDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrOSCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCabNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
      Size = 60
    end
    object QrOSCabNO_ESTATUS: TWideStringField
      FieldName = 'NO_ESTATUS'
      Size = 60
    end
    object QrOSCabNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrOSCabSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrOSCabNO_SiapTerCad: TWideStringField
      FieldName = 'NO_SiapTerCad'
      Size = 100
    end
    object QrOSCabTXTVisPrv: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTVisPrv'
      Calculated = True
    end
    object QrOSCabTXTContat: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTContat'
      Calculated = True
    end
    object QrOSCabTXTVisExe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTVisExe'
      Calculated = True
    end
    object QrOSCabTXTExePrv: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExePrv'
      Calculated = True
    end
    object QrOSCabTXTExeIni: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExeIni'
      Calculated = True
    end
    object QrOSCabTXTExeFim: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExeFim'
      Calculated = True
    end
  end
  object DsOSCab: TDataSource
    DataSet = QrOSCab
    Left = 292
    Top = 84
  end
  object QrDesServico: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM desservico'
      'ORDER BY Nome')
    Left = 48
    Top = 316
    object QrDesServicoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDesServicoNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsDesServico: TDataSource
    DataSet = QrDesServico
    Left = 48
    Top = 360
  end
end
