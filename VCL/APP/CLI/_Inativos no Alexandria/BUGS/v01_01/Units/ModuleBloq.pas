unit ModuleBloq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DbGrids, mySQLDbTables, ComCtrls, frxClass, frxDBSet, UnMLAGeral;

type
  TDmBloq = class(TDataModule)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DmBloq: TDmBloq;

implementation

uses UnInternalConsts, Module, UCreate, UMySQLModule, ModuleGeral,
  MyGlyfs, Principal, MyDBCheck, UnGOTOy, MeuFrx, UnFinanceiro;

{$R *.DFM}

end.

