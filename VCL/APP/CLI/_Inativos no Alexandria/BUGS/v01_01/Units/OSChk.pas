unit OSChk;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, PlannerMonthView, DBPlannerMonthView, Planner, DBPlanner,
  UnDmkEnums;

type
  TFmOSChk = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    EdChekLstCab: TdmkEditCB;
    CBChekLstCab: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrChekLstCab: TmySQLQuery;
    DsChekLstCab: TDataSource;
    QrChekLstCabCodigo: TIntegerField;
    QrChekLstCabNome: TWideStringField;
    SBCadastro: TSpeedButton;
    SBReabre: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
    procedure EdChekLstCabChange(Sender: TObject);
    procedure SBReabreClick(Sender: TObject);
  private
    { Private declarations }
    procedure FechaPesquisa();
    procedure ReabrePesquisa();
  public
    { Public declarations }
    FQrOSChk: TmySQLQuery;
    FOSCab: Integer;
    //
  end;

  var
  FmOSChk: TFmOSChk;

implementation

uses UnMyObjects, Module, UnChekLstJan, DmkDAC_PF, UMySQLModule, MyDBCheck;

{$R *.DFM}

procedure TFmOSChk.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSChk.EdChekLstCabChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOSChk.FechaPesquisa();
begin
  SBReabre.Enabled := EdChekLstCab.ValueVariant <> 0;
end;

procedure TFmOSChk.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSChk.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrChekLstCab, Dmod.MyDB);
end;

procedure TFmOSChk.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSChk.ReabrePesquisa();
const
  Aviso  = '...';
  Titulo = 'Sele��o de itens de check list';
  Prompt = 'Seleciones os itens desejados:';
  //Campo  = 'Descricao';
  //
  DestTab = 'oschk';
  DestMaster = 'Codigo';
  DestDetail = 'Controle';
  DestSelec1 = 'ChekLstIts';
  SorcField  = 'ChekLstIts';
  ExcluiAnteriores = False;
var
  ValrMaster: Integer;
begin
  ValrMaster := FOSCab;
  if DBCheck.EscolheCodigosMultiplos_A(
  Aviso, Titulo, Prompt, nil, 'Ativo', 'Nivel2', 'Nome', [
  'DELETE FROM _selcods_; ',
  'INSERT INTO _selcods_ ',
  'SELECT Codigo Nivel1, Controle Nivel2, ',
  'Ordem Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
  'FROM ' + TMeuDB + '.cheklstits',
  'WHERE Codigo=' + Geral.FF0(EdChekLstCab.ValueVariant),
  ''],[
  'SELECT * FROM _selcods_ ',
  'ORDER BY Nivel3, Nome;',
  ''], DestTab, [DestMaster], DestDetail, DestSelec1, [ValrMaster],
  FQrOSChk, SorcField, ExcluiAnteriores, Dmod.QrUpd) then
  begin
    FQrOSChk.Close;
    FQrOSChk.Open;
  end;
end;

procedure TFmOSChk.SBCadastroClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  ChekLstJan.MostraChekLstCab();
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdChekLstCab, CBChekLstCab, QrChekLstCab,
      VAR_CADASTRO, 'Codigo');
end;

procedure TFmOSChk.SBReabreClick(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
