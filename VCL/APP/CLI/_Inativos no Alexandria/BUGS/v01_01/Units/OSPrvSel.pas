unit OSPrvSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, UnMyObjects, UnDmkEnums,
  dmkDBGridZTO;

type
  TFmOSPrvSel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrPesq: TmySQLQuery;
    QrPesqCodigo: TIntegerField;
    QrPesqControle: TIntegerField;
    QrPesqDtHrContat: TDateTimeField;
    QrPesqFormContat: TIntegerField;
    QrPesqCliente: TIntegerField;
    QrPesqContato: TIntegerField;
    QrPesqAgente: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqNO_Cliente: TWideStringField;
    QrPesqNO_Agente: TWideStringField;
    QrPesqNO_Contato: TWideStringField;
    QrPesqNO_FormContat: TWideStringField;
    DsPesq: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
    FQrOSPrv: TmySQLQuery;
    procedure ReopenOSPrv(Cliente: Integer);
  end;

  var
  FmOSPrvSel: TFmOSPrvSel;

implementation

uses Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmOSPrvSel.BtOKClick(Sender: TObject);
  procedure AdicionaAtual(var Contador: Integer);
  var
    Codigo, Controle, PrvStatus: Integer;
  begin
    Codigo   := FCodigo;
    Controle := QrPesqControle.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osprv', False, [
    'Codigo'], [
    'Controle'], [
    Codigo], [
    Controle], True) then
    begin
      Contador := Contador + 1;
      // Mudar automaticamente para "solucionada"!
      if Dmod.QrOpcoesBugsOSPrvStaUsa.Value = 1 then
      begin
        PrvStatus := Dmod.QrOpcoesBugsOSPrvStaCod.Value;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osprv', False, [
        'PrvStatus'], [
        'Controle'], [
        PrvStatus], [
        Controle], True) then
      end;
    end;
  end;
var
  I, N: Integer;
begin
  N := 0;
  QrPesq.DisableControls;
  try
    if DBGrid1.SelectedRows.Count > 1 then
    begin
      with DBGrid1.DataSource.DataSet do
      for i:= 0 to DBGrid1.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGrid1.SelectedRows.Items[i]));
        AdicionaAtual(N);
      end;
    end else AdicionaAtual(N);
  finally
    QrPesq.EnableControls;
  end;
  //
  UnDmkDAC_PF.AbreQuery(QrPesq, Dmod.MyDB);
  if FQrOSPrv <> nil then
    UnDmkDAC_PF.AbreQuery(FQrOSPrv, Dmod.MyDB);
  if QrPesq.RecordCount = 0 then
    Close
  else
  case N of
    0: Geral.MB_Aviso('Nenhum registro foi inclu�do');
    1: Geral.MB_Info('Um registro foi inclu�do com sucesso');
    else Geral.MB_Info(Geral.FF0(N) + ' registros foram inclu�dos com sucesso');
  end;
end;

procedure TFmOSPrvSel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPrvSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSPrvSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSPrvSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPrvSel.ReopenOSPrv(Cliente: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT prv.*, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ',
  'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_Agente, ',
  'eco.Nome NO_Contato, fct.Nome NO_FormContat ',
  'FROM osprv prv ',
  'LEFT JOIN entidades cli ON cli.Codigo=prv.Cliente ',
  'LEFT JOIN entidades age ON age.Codigo=prv.Agente ',
  'LEFT JOIN enticontat eco ON eco.Controle=prv.Contato ',
  'LEFT JOIN formcontat fct ON fct.Codigo=prv.FormContat ',
  'WHERE prv.Cliente=' + Geral.FF0(Cliente),
  'AND prv.Codigo=0',
  '']);
end;

end.
