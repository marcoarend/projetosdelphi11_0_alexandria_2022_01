unit RMIP_R007;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, mySQLDbTables, frxClass,
  frxDBSet;

type
  TFmRMIP_R007 = class(TForm)
    Qr007OSCab: TmySQLQuery;
    Qr007OSCabCodigo: TIntegerField;
    Qr007OSCabEntidade: TIntegerField;
    Qr007OSCabEstatus: TIntegerField;
    Qr007OSCabFatoGeradr: TIntegerField;
    Qr007OSCabDtaContat: TDateTimeField;
    Qr007OSCabDtaVisPrv: TDateTimeField;
    Qr007OSCabDtaVisExe: TDateTimeField;
    Qr007OSCabDtaExePrv: TDateTimeField;
    Qr007OSCabDtaExeIni: TDateTimeField;
    Qr007OSCabDtaExeFim: TDateTimeField;
    Qr007OSCabLk: TIntegerField;
    Qr007OSCabDataCad: TDateField;
    Qr007OSCabUserCad: TIntegerField;
    Qr007OSCabDataAlt: TDateField;
    Qr007OSCabUserAlt: TIntegerField;
    Qr007OSCabAlterWeb: TSmallintField;
    Qr007OSCabAtivo: TSmallintField;
    Qr007OSCabNO_FatoGeradr: TWideStringField;
    Qr007OSCabNO_ESTATUS: TWideStringField;
    Qr007OSCabNO_ENT: TWideStringField;
    Qr007OSCabSiapTerCad: TIntegerField;
    Qr007OSCabNO_SiapTerCad: TWideStringField;
    Qr007OSCabTXTVisPrv: TWideStringField;
    Qr007OSCabTXTContat: TWideStringField;
    Qr007OSCabTXTVisExe: TWideStringField;
    Qr007OSCabTXTExePrv: TWideStringField;
    Qr007OSCabTXTExeIni: TWideStringField;
    Qr007OSCabTXTExeFim: TWideStringField;
    Qr007OSCabDdsPosVda: TIntegerField;
    Qr007OSCabEntiContat: TIntegerField;
    Qr007OSCabNumContrat: TIntegerField;
    Qr007OSCabEntPagante: TIntegerField;
    Qr007OSCabEntContrat: TIntegerField;
    Qr007OSCabEmpresa: TIntegerField;
    Qr007OSCabNO_PAG: TWideStringField;
    Qr007OSCabNO_CTR: TWideStringField;
    Qr007OSCabDtaLibFat: TDateTimeField;
    Qr007OSCabDtaFimFat: TDateTimeField;
    Qr007OSCabCondicaoPg: TIntegerField;
    Qr007OSCabCartEmis: TIntegerField;
    Qr007OSCabNO_CART: TWideStringField;
    Qr007OSCabNO_PRZ: TWideStringField;
    Qr007OSCabSerNF: TWideStringField;
    Qr007OSCabNumNF: TIntegerField;
    Qr007OSCabNO_ENTICONTAT: TWideStringField;
    Qr007OSCabTel_ENT: TWideStringField;
    Qr007OSCabTXTTel_ENT: TWideStringField;
    Qr007OSCabValorServi: TFloatField;
    Qr007OSCabValorDesco: TFloatField;
    Qr007OSCabValorOutrs: TFloatField;
    Qr007OSCabInvalServi: TFloatField;
    Qr007OSCabValorTotal: TFloatField;
    Qr007OSCabInvalDesco: TFloatField;
    Qr007OSCabInvalOutrs: TFloatField;
    Qr007OSCabInvalTotal: TFloatField;
    Qr007OSCabOrcamServi: TFloatField;
    Qr007OSCabOrcamDesco: TFloatField;
    Qr007OSCabOrcamOutrs: TFloatField;
    Qr007OSCabOrcamTotal: TFloatField;
    Qr007OSCabValiDdOrca: TIntegerField;
    Qr007OSCabOperacao: TIntegerField;
    Qr007OSCabExeTxtCli1: TIntegerField;
    Qr007OSCabNO_ExeTxtCli1: TWideStringField;
    Qr007OSCabExeTxtCli2: TIntegerField;
    Qr007OSCabNO_ExeTxtCli2: TWideStringField;
    Qr007OSCabValorPre: TFloatField;
    Qr007OSCabNO_OPERACAO: TWideStringField;
    Qr007OSCabObsGaranti: TWideMemoField;
    Qr007OSCabObsExecuta: TWideMemoField;
    Qr007OSCabFimVisPrv: TDateTimeField;
    Qr007OSCabFimVisExe: TDateTimeField;
    Qr007OSCabFimExePrv: TDateTimeField;
    Qr007OSCabOptado: TSmallintField;
    Qr007OSCabGrupo: TIntegerField;
    Qr007OSCabNumero: TIntegerField;
    Qr007OSCabOpcao: TIntegerField;
    Qr007OSCabAgeEqiCab: TIntegerField;
    Qr007OSCabNO_AgeEqiCab: TWideStringField;
    Qr007OSCabSohInicial: TSmallintField;
    Qr007OSCabStPipAdPrg: TSmallintField;
    Qr007OSCabHowGerou: TSmallintField;
    Qr007OSCabPosGerou: TSmallintField;
    Qr007OSCabOSFlhUltGe: TIntegerField;
    Qr007OSCabMulServico: TLargeintField;
    Qr007OSCabOSFlhGrCab: TIntegerField;
    Qr007OSCabOSFlhGrIts: TIntegerField;
    Qr007OSCabLstCusPrd: TIntegerField;
    Qr007OSCabLstUplWeb: TDateTimeField;
    Qr007OSCabConta: TIntegerField;
    Qr007OSCabLCPUsed: TIntegerField;
    Qr007OSCabDtIniMonGr: TDateField;
    Qr007OSCabMobiliCad: TIntegerField;
    Qr007OSCabNO_MobiliCad: TWideStringField;
    frxDs007OSCab: TfrxDBDataset;
    frxReport007A: TfrxReport;
    procedure frxReport007AGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    procedure ReopenOSCab();
  public
    { Public declarations }
    FEmpresa, FCliente: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    //
    procedure GeraImp_ResumoOSs();
    procedure frxReport000GetValue(frxReport: TfrxReport; const VarName: string; var Value: Variant);
  end;

var
  FmRMIP_R007: TFmRMIP_R007;

implementation

{$R *.dfm}

uses Module, DmkDAC_PF, dmkGeral, ModOS, UnDmkProcFunc, UnMyObjects;


{ TFmRMIP_R007 }

procedure TFmRMIP_R007.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
begin
  if VarName = 'VARF_DESCRI_ATIVI' then
    Value := DmModOS.DadosOSBgstrl(Qr007OSCabCodigo.Value)
  else
end;

procedure TFmRMIP_R007.frxReport007AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport007A, VarName, Value);
end;

procedure TFmRMIP_R007.GeraImp_ResumoOSs();
begin
  ReopenOSCab();
  //
  frxReport007A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport007A.Variables['VARF_DATA']    := FDtaImp;
  frxReport007A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport007A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  frxReport007A.Variables['VARF_PERIODO']  := QuotedStr(dmkPF.PeriodoImp(FDtaIni,
    FDtaFim, 0, 0, True, True, False, False, '', ''));
  //
  MyObjects.frxDefineDataSets(frxReport007A, [
  frxDs007OSCab
  ]);
  //MyObjects.frxPrepara(frxReport007A, 'Resumo de OSs');
end;

procedure TFmRMIP_R007.ReopenOSCab();
var
  Ini, Fim, SQL_Cliente: String;
begin
  Ini := Geral.FDT(FDtaIni, 1);
  Fim := Geral.FDT(FDtaFim, 1);
  //
  if FCliente <> 0 then
    SQL_Cliente := 'AND cab.Entidade=' + Geral.FF0(FCliente)
  else
    SQL_Cliente := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr007OSCab, Dmod.MyDB, [
  'SELECT eco.Nome NO_ENTICONTAT, cab.*,  ',
  'fge.Nome NO_FatoGeradr, sta.Nome NO_ESTATUS, ',
  'stc.Nome NO_SiapTerCad, stc.LstCusPrd, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT, ',
  'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG, ',
  'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR, ',
  'car.Nome NO_CART, ppc.Nome NO_PRZ, ',
  'tg1.Nome NO_ExeTxtCli1, tg2.Nome NO_ExeTxtCli2, ',
  'aec.Nome NO_AgeEqiCab, moc.Nome NO_MobiliCad ',
  'FROM oscab cab ',
  'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
  'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante ',
  'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat ',
  'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
  'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
  'LEFT JOIN carteiras car ON car.Codigo=cab.CartEmis ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondicaoPg ',
  'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat ',
  'LEFT JOIN txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1  ',
  'LEFT JOIN txtgeneric tg2 ON tg2.Codigo=cab.ExeTxtCli2  ',
  'LEFT JOIN ageeqicab aec ON aec.Codigo=cab.AgeEqiCab  ',
  'LEFT JOIN mobilicad moc ON moc.Codigo=cab.MobiliCad ',
  'WHERE cab.Empresa=' + Geral.FF0(FEmpresa),
  SQL_Cliente,
  'AND cab.DtaExeFim BETWEEN "' + Ini + '" AND "' + Fim + '"',
  'ORDER BY cab.DtaExeFim, cab.Codigo',
  '']);
end;

end.
