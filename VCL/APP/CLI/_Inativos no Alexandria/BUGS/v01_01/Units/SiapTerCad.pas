unit SiapTerCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  Menus, dmkImage, UnDmkEnums, Vcl.Mask;

type
  TFmSiapTerCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PMCEP: TPopupMenu;
    Descobrir1: TMenuItem;
    Verificar1: TMenuItem;
    QrBacen_Pais: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsBacen_Pais: TDataSource;
    QrMunici: TmySQLQuery;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    DsMunici: TDataSource;
    GroupBox2: TGroupBox;
    QrCunsCad: TmySQLQuery;
    DsCunsCad: TDataSource;
    QrCunsCadCodigo: TIntegerField;
    QrCunsCadNO_ENT: TWideStringField;
    Label1: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdCodigo: TdmkEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    Label3: TLabel;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    DsListaLograd: TDataSource;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    QrRotaLatLon: TmySQLQuery;
    DsRotaLatLon: TDataSource;
    QrRotaLatLonCodigo: TIntegerField;
    QrRotaLatLonNome: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox11: TGroupBox;
    Panel44: TPanel;
    Label5: TLabel;
    Label43: TLabel;
    EdLstCusPrd: TdmkEditCB;
    CBLstCusPrd: TdmkDBLookupComboBox;
    EdPdrMntsMon: TdmkEdit;
    GroupBox4: TGroupBox;
    Panel5: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    SBCoord: TSpeedButton;
    SbRotaLatLon: TSpeedButton;
    SbListaRotas: TSpeedButton;
    EdLatitude: TdmkEdit;
    EdLongitude: TdmkEdit;
    EdRotaLatLon: TdmkEditCB;
    CBRotaLatLon: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    Panel8: TPanel;
    Label12: TLabel;
    Label97: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label38: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label106: TLabel;
    Label109: TLabel;
    Label112: TLabel;
    BtCEP_S: TBitBtn;
    EdSCEP: TdmkEdit;
    CBSLograd: TdmkDBLookupComboBox;
    EdSRua: TdmkEdit;
    EdSNumero: TdmkEdit;
    EdSBairro: TdmkEdit;
    EdSUF: TdmkEdit;
    EdSLograd: TdmkEditCB;
    EdSCompl: TdmkEdit;
    EdSEndeRef: TdmkEdit;
    EdSCodMunici: TdmkEditCB;
    CBSCodMunici: TdmkDBLookupComboBox;
    EdSCodiPais: TdmkEditCB;
    CBSCodiPais: TdmkDBLookupComboBox;
    EdSTe1: TdmkEdit;
    BtCopia: TBitBtn;
    GroupBox5: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    EdM2Constru: TdmkEdit;
    EdM2NaoBuild: TdmkEdit;
    EdM2Terreno: TdmkEdit;
    EdM2Total: TdmkEdit;
    GroupBox6: TGroupBox;
    Label35: TLabel;
    EdSCidade: TdmkEdit;
    Label15: TLabel;
    EdSPais: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdSCEPEnter(Sender: TObject);
    procedure EdSCEPExit(Sender: TObject);
    procedure BtCEP_SClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Descobrir1Click(Sender: TObject);
    procedure Verificar1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdM2ConstruChange(Sender: TObject);
    procedure EdM2NaoBuildChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtCopiaClick(Sender: TObject);
    procedure SBCoordClick(Sender: TObject);
    procedure SbRotaLatLonClick(Sender: TObject);
    procedure SbListaRotasClick(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FSCEP: String;
    //
    procedure ReopenTabePrcCab();
    procedure ReopenRotaLatLon();
  public
    { Public declarations }
    FQrSiapterCad: TmySQLQuery;
    FCompl: String;
  end;

  var
  FmSiapTerCad: TFmSiapTerCad;

implementation

uses UnMyObjects, Module, EntiCEP, MyDBCheck, UnCEP, (*CunsCad,*) UMySQLModule,
ModuleGeral, ModuleFatura, DmkDAC_PF, UnContratUnit, UnDmkWeb, UnDmkProcFunc,
RotaLatLon, RotaSel, UnSACAll_PF;

{$R *.DFM}

procedure TFmSiapTerCad.BtCEP_SClick(Sender: TObject);
begin
  MyObjects.MostraPMCEP(PMCEP, BtCEP_S);
  try
    EdSNumero.SetFocus;
  except
    ;
  end;
end;

procedure TFmSiapTerCad.BtCopiaClick(Sender: TObject);
begin
  DmodG.ReopenEndereco(EdCliente.ValueVariant);
  //
  EdSCEP.ValueVariant       := DModG.QrEnderecoCEP.Value;
  EdSLograd.ValueVariant    := DModG.QrEnderecoLOGRAD.Value;
  CBSLograd.KeyValue        := DModG.QrEnderecoLOGRAD.Value;
  EdSRua.ValueVariant       := DModG.QrEnderecoRUA.Value;
  EdSNumero.ValueVariant    := DModG.QrEnderecoNUMERO.Value;
  EdSCompl.ValueVariant     := DModG.QrEnderecoCOMPL.Value;
  EdSBairro.ValueVariant    := DModG.QrEnderecoBAIRRO.Value;
  EdSCidade.ValueVariant    := DModG.QrEnderecoCIDADE.Value;
  EdSUF.ValueVariant        := DModG.QrEnderecoNOMEUF.Value;
  EdSPais.ValueVariant      := DModG.QrEnderecoPais.Value;
  EdSEndeRef.ValueVariant   := DModG.QrEnderecoENDEREF.Value;
  EdSTe1.ValueVariant       := DModG.QrEnderecoTE1.Value;
  EdSCodMunici.ValueVariant := DModG.QrEnderecoCODMUNICI.Value;
  CBSCodMunici.KeyValue     := DModG.QrEnderecoCODMUNICI.Value;
  EdSCodiPais.ValueVariant  := DModG.QrEnderecoCODPAIS.Value;
  CBSCodiPais.KeyValue      := DModG.QrEnderecoCODPAIS.Value;
end;

procedure TFmSiapTerCad.BtOKClick(Sender: TObject);
var
  Nome, SRua, SCompl, SBairro, SCidade, SUF, SPais, SEndeRef, STe1: String;
  Codigo, Cliente, SLograd, SNumero, SCEP, SCodMunici, SCodiPais, LstCusPrd,
  PdrMntsMon, RotaLatLon: Integer;
  M2Constru, M2NaoBuild, M2Terreno, M2Total, Latitude, Longitude: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  Cliente        := EdCliente.ValueVariant;
  SLograd        := EdSLograd.ValueVariant;
  SRua           := EdSRua.Text;
  SNumero        := Geral.IMV(Geral.SoNumero_TT(EdSNumero.ValueVariant));
  SCompl         := EdSCompl.Text;
  SBairro        := EdSBairro.Text;
  SCidade        := EdSCidade.Text;
  SUF            := EdSUF.Text;
  SCEP           := Geral.IMV(Geral.SoNumero_TT(EdSCEP.ValueVariant));
  SPais          := EdSPais.Text;
  SEndeRef       := EdSEndeRef.Text;
  SCodMunici     := EdSCodMunici.ValueVariant;
  SCodiPais      := EdSCodiPais.ValueVariant;
  STe1           := Geral.SoNumero_TT(EdSTe1.Text);
  M2Constru      := EdM2Constru.ValueVariant;
  M2NaoBuild     := EdM2NaoBuild.ValueVariant;
  M2Terreno      := EdM2Terreno.ValueVariant;
  M2Total        := EdM2Total.ValueVariant;
  PdrMntsMon     := EdPdrMntsMon.ValueVariant;
  Latitude       := EdLatitude.ValueVariant;     // -23.429647
  Longitude      := EdLongitude.ValueVariant;    // -51.879038
  RotaLatLon     := EdRotaLatLon.ValueVariant;
  //
  // CUIDADO com o CodUsu!
  //if not
  UMyMod.ObtemCodigoDeCodUsu(EdLstCusPrd, LstCusPrd, '');
  //'Informe a lista de pre�os para custos!') then
  //  Exit;
  //
  Codigo := UMyMod.BPGS1I32(
    'siaptercad', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'siaptercad', False, [
    'Nome', 'Cliente', 'SLograd',
    'SRua', 'SNumero', 'SCompl',
    'SBairro', 'SCidade', 'SUF',
    'SCEP', 'SPais', 'SEndeRef',
    'SCodMunici', 'SCodiPais', 'STe1',
    'M2Constru', 'M2NaoBuild',
    'M2Terreno', 'M2Total',
    'LstCusPrd', 'PdrMntsMon',
    'Latitude', 'Longitude', 'RotaLatLon'], [
    'Codigo'], [
    Nome, Cliente, SLograd,
    SRua, SNumero, SCompl,
    SBairro, SCidade, SUF,
    SCEP, SPais, SEndeRef,
    SCodMunici, SCodiPais, STe1,
    M2Constru, M2NaoBuild,
    M2Terreno, M2Total,
    LstCusPrd, PdrMntsMon,
    Latitude, Longitude, RotaLatLon], [
    Codigo], True) then
  begin
    if FQrSiapterCad <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrSiapterCad, FQrSiapterCad.Database);
      FQrSiapterCad.Locate('Codigo', Codigo, []);
    end;
    Close;
  end;
end;

procedure TFmSiapTerCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSiapTerCad.Descobrir1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    FmEntiCEP.FEdCEP := EdSCep;
    FmEntiCEP.FEdLograd := EdSLograd;
    FmEntiCEP.FCBLograd := CBSLograd;
    FmEntiCEP.FEdRua := EdSRua;
    FmEntiCEP.FEdBairro := EdSBairro;
    FmEntiCEP.FEdCidade := EdSCidade;
    FmEntiCEP.FEdUF := EdSUF;
    FmEntiCEP.FEdPais := EdSPais;
    FmEntiCEP.FEdCodMunici := EdSCodMunici;
    FmEntiCEP.FCBCodMunici := CBSCodMunici;
    FmEntiCEP.FEdCodiPais := EdSCodiPais;
    FmEntiCEP.FCBCodiPais := CBSCodiPais;
    //
    FmEntiCEP.DBGrid2.Visible := False;
    FmEntiCEP.Painel2.Visible := False;
    FmEntiCEP.ShowModal;
    FmEntiCEP.Destroy;
  end;
end;

procedure TFmSiapTerCad.EdM2ConstruChange(Sender: TObject);
begin
  SACAll_PF.CalculaM2Total(EdM2Constru, EdM2NaoBuild, EdM2Total);
end;

procedure TFmSiapTerCad.EdM2NaoBuildChange(Sender: TObject);
begin
  SACAll_PF.CalculaM2Total(EdM2Constru, EdM2NaoBuild, EdM2Total);
end;

procedure TFmSiapTerCad.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdNome.ValueVariant := FCompl;
end;

procedure TFmSiapTerCad.EdSCEPEnter(Sender: TObject);
begin
  FSCEP := Geral.SoNumero_TT(EdSCEP.Text);
end;

procedure TFmSiapTerCad.EdSCEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdSCEP.Text);
  if CEP <> FSCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdSCEP, EdSLograd, EdSRua, EdSNumero, EdSBairro,
        EdSCidade, EdSUF, EdSPais, EdSNumero, EdSCompl, CBSLograd,
        EdSCodMunici, CBSCodMunici, EdSCodiPais, CBSCodiPais);
  end;
end;

procedure TFmSiapTerCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSiapTerCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  VAR_CADASTRO := EdCodigo.ValueVariant;
end;

procedure TFmSiapTerCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType              := stPsq;
  PageControl1.ActivePageIndex := 0;
  //
  UMyMod.AbreQuery(QrBacen_Pais, DModG.AllID_DB);
  UMyMod.AbreQuery(QrMunici, DModG.AllID_DB);
  UMyMod.AbreQuery(QrCunsCad, Dmod.MyDB);
  UMyMod.AbreQuery(QrListaLograd, Dmod.MyDB);
  ReopenTabePrcCab();
  ReopenRotaLatLon();
end;

procedure TFmSiapTerCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSiapTerCad.ReopenRotaLatLon;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRotaLatLon, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM rotalatlon ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmSiapTerCad.ReopenTabePrcCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraCusPrc, Dmod.MyDB, [
    'SELECT Codigo, CodUsu, Nome ',
    'FROM gracusprc ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmSiapTerCad.SBCoordClick(Sender: TObject);
var
  MapaDescri, //Coords,
  Nome, Rua, Cidade, UF, Pais: String;
  Numero: Integer;
  Latitude, Longitude: Double;
begin
  // Mostrar mapa npo app padrao de navegacao web
  Nome   := EdNome.Text;
  Rua    := EdSRua.Text;
  Numero := EdSNumero.ValueVariant;
  Cidade := EdSCidade.Text;
  UF     := EdSUF.Text;
  Pais   := EdSPais.Text;
  //
  if Length(Nome) > 0 then
    MapaDescri := Nome;
  if Length(Rua) > 0 then
    MapaDescri := Rua; // + ' ' + Geral.FF0(Numero) + ' ';
  if Numero > 0 then
    MapaDescri := Rua + ', ' + Geral.FF0(Numero) + ' '
  else
    MapaDescri := Rua + ', S/N ';
  if Length(Cidade) > 0 then
    MapaDescri := MapaDescri + Cidade + ' ';
  if Length(UF) > 0 then
    MapaDescri := MapaDescri + ' - ' + UF + ' ';
  if Length(Pais) > 0 then
    MapaDescri := MapaDescri + Pais;


(*
  DmkWeb.MostraGoogleMapsPlace(MapaDescri, True, False, 0, 0);
  //
  // Separar texto
  Coords := '';
  if InputQuery('Coordenadas', 'Cole aqui a coordenada:', Coords) then
  begin
    DmkPF.SeparaCoordenadas(Coords, Latitude, Longitude);
    EdLatitude.ValueVariant := Latitude;
    EdLongitude.ValueVariant := Longitude;
*)
  if DmkWeb.DefineCoordenadas(MapaDescri, Latitude, Longitude, True, False, 0, 0) then
  begin
    EdLatitude.ValueVariant := Latitude;
    EdLongitude.ValueVariant := Longitude;
  end;

{
SELECT *, ( acos( cos( radians(34.054366) ) * -- latitude
cos( radians( Latitude ) ) * cos( radians( Longitude ) - radians(-118.258574) ) + -- longitude
sin( radians(34.054366) ) * -- latitude sin( radians( Latitude ) ) ) ) AS distance
FROM crime_data
ORDER BY distance LIMIT 0, 30;



-- with spatial math for geolocation constraints for distance away from

SELECT *, (
        acos(
            cos( radians(34.054366) ) *
            cos( radians( Latitude ) ) *
            cos( radians( Longitude ) -
            radians(-118.258574) ) +
            sin( radians(34.054366) ) *
            sin( radians( Latitude ) )
        )
    )
    AS distance
FROM
    crime_data
ORDER BY
    distance
LIMIT 0, 30;

}
end;

procedure TFmSiapTerCad.SbListaRotasClick(Sender: TObject);
var
  Latitude, Longitude: Double;
  RotaLatLon, Codigo: Integer;
begin
  Latitude  := EdLatitude.ValueVariant;
  Longitude := EdLongitude.ValueVariant;
  Codigo    := EdCodigo.ValueVariant;
  if (Latitude = 0) or (Longitude = 0) then
  begin
    Geral.MB_Aviso(
    'Informe a latitude e longitude para visualizar clientes pr�ximos!');
    Exit;
  end;
  if DBCheck.CriaFm(TFmRotaSel, FmRotaSel, afmoNegarComAviso) then
  begin
    FmRotaSel.ReopenRotaLatLon(Codigo, Latitude, Longitude);
    FmRotaSel.ShowModal;
    RotaLatLon := FmRotaSel.FSelecionado;
    FmRotaSel.Destroy;
    //
    if RotaLatLon <> 0 then
    begin
      ReopenRotaLatLon();
      EdRotaLatLon.ValueVariant := RotaLatLon;
      CBRotaLatLon.KeyValue     := RotaLatLon;
    end;
  end;
end;

procedure TFmSiapTerCad.SbRotaLatLonClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRotaLatLon, FmRotaLatLon, afmoNegarComAviso) then
  begin
    FmRotaLatLon.ShowModal;
    FmRotaLatLon.Destroy;
  end;
end;

procedure TFmSiapTerCad.Verificar1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    with FmEntiCEP do
    begin
      DBGrid1.Visible := False;
      Painel1.Visible := False;
      case VAR_ACTIVEPAGE of
        1: EdCEPLoc.Text := EdSCEP.Text;
        {  Parei Aqui! falta fazer
        2: EdCEPLoc.Text := EdECEP.Text;
        5: EdCEPLoc.Text := EdCCEP.Text;
        6: EdCEPLoc.Text := EdLCEP.Text;
        }
      end;
      ShowModal;
      Destroy;
    end;
  end;
end;

{
SELECT *, (
        acos(
            cos( radians(-29.648294) ) *
            cos( radians( Latitude ) ) *
            cos( radians( Longitude ) -
            radians(-51.157219) ) +
            sin( radians(-29.648294) ) *
            sin( radians( Latitude ) )
        )
    ) * 6371
    AS Distance
FROM
    siaptercad
WHERE
    Latitude <> 0
ORDER BY
    Distance
LIMIT 0, 30;
}


{
SELECT *
FROM siaptercad
WHERE Codigo IN (
    SELECT DISTINCT SiapTerCad
    FROM oscab
    WHERE Codigo IN (
        SELECT ID_Cod1
        FROM protpakits
        WHERE Controle=309
        )
    )


}

{
SELECT *
FROM siaptercad

WHERE SRua LIKE "%neo al%"
AND SCidade LIKE "%maringa%"
AND SUF = "PR"
ORDER BY SNumero
}


end.
