object FmDesServico: TFmDesServico
  Left = 368
  Top = 194
  Caption = 'DES-SERVI-001 :: Cadastro de Servi'#231'os'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 535
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 672
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Sigla: '
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 593
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSigla: TdmkEdit
        Left = 672
        Top = 32
        Width = 100
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sigla'
        UpdCampo = 'Sigla'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 472
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 14
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 898
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 0
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtTexto: TBitBtn
        Tag = 121
        Left = 332
        Top = 14
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Observa'#231#245'es'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtTextoClick
      end
    end
    object ReObservacao: TdmkRichEdit
      Left = 0
      Top = 61
      Width = 1008
      Height = 124
      Align = alTop
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      QryCampo = 'Observacao'
      UpdCampo = 'Observacao'
      UpdType = utYes
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 535
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 672
        Top = 16
        Width = 23
        Height = 13
        Caption = 'Sigla'
        FocusControl = DBEdit1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsDesServico
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 593
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsDesServico
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 672
        Top = 32
        Width = 100
        Height = 21
        DataField = 'Sigla'
        DataSource = DsDesServico
        TabOrder = 2
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 471
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 288
        Top = 15
        Width = 718
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtFormulas: TBitBtn
          Tag = 178
          Left = 366
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&F'#243'rmulas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFormulasClick
        end
        object BtServicoBase: TBitBtn
          Tag = 263
          Left = 125
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'Serv. &Base'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtServicoBaseClick
        end
        object BtDesServico: TBitBtn
          Tag = 547
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Servi'#231'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesServicoClick
        end
        object Panel2: TPanel
          Left = 588
          Top = 0
          Width = 130
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 4
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtPragas: TBitBtn
          Tag = 519
          Left = 246
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pragas alvo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtPragasClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 65
      Width = 1008
      Height = 344
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Servi'#231'os base '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 316
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Splitter2: TSplitter
            Left = 0
            Top = 120
            Width = 1000
            Height = 5
            Cursor = crVSplit
            Align = alTop
            ExplicitTop = 80
          end
          object dmkDBGridZTO1: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 1000
            Height = 120
            Align = alTop
            DataSource = DsPreSrv
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            PopupMenu = PMServicoBase
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Nome do servi'#231'o base'
                Width = 435
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GarantiaDd'
                Title.Caption = 'dd Garantia'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'HrEvacuar'
                Title.Caption = 'h evacuar'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'HrExecutar'
                Title.Caption = 'h exec.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValInfo'
                Title.Caption = '$ Info'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTota'
                Title.Caption = '$ Total'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MoniDdTotl'
                Title.Caption = 'Monit. dd.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MoniDdIntv'
                Title.Caption = 'Interv. dd.'
                Visible = True
              end>
          end
          object Panel7: TPanel
            Left = 293
            Top = 125
            Width = 707
            Height = 191
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Splitter1: TSplitter
              Left = 0
              Top = 81
              Width = 707
              Height = 10
              Cursor = crVSplit
              Align = alBottom
              ExplicitTop = 64
            end
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 707
              Height = 81
              Align = alClient
              Caption = ' F'#243'rmulas de aplica'#231#227'o: '
              TabOrder = 0
              object dmkDBGridZTO2: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 703
                Height = 64
                Align = alClient
                DataSource = DsPreFrm
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                PopupMenu = PMFormulasApl
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Title.Caption = 'ID'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Formula'
                    Title.Caption = 'F'#243'rmula'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FORMULA'
                    Title.Caption = 'Nome da f'#243'rmula'
                    Width = 528
                    Visible = True
                  end>
              end
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 91
              Width = 707
              Height = 100
              Align = alBottom
              Caption = ' F'#243'rmulas de monitoramento: '
              TabOrder = 1
              object dmkDBGridZTO3: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 703
                Height = 83
                Align = alClient
                DataSource = DsPreMon
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                PopupMenu = PMFormulasMon
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Title.Caption = 'ID'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Formula'
                    Title.Caption = 'F'#243'rmula'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FORMULA'
                    Title.Caption = 'Nome da f'#243'rmula'
                    Width = 528
                    Visible = True
                  end>
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 125
            Width = 293
            Height = 191
            Align = alLeft
            Caption = ' Pragas: '
            TabOrder = 2
            object dmkDBGridZTO4: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 289
              Height = 174
              Align = alClient
              DataSource = DsPreAlv
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              OnMouseDown = dmkDBGridZTO4MouseDown
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_Praga_Z'
                  Title.Caption = 'Nome'
                  Width = 242
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Observa'#231#245'es '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBREObservacao: TDBRichEdit
          Left = 0
          Top = 0
          Width = 1000
          Height = 316
          Align = alClient
          DataField = 'Observacao'
          DataSource = DsDesServico
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 261
        Height = 32
        Caption = 'Cadastro de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 261
        Height = 32
        Caption = 'Cadastro de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 261
        Height = 32
        Caption = 'Cadastro de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrDesServico: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrDesServicoBeforeOpen
    AfterOpen = QrDesServicoAfterOpen
    BeforeClose = QrDesServicoBeforeClose
    SQL.Strings = (
      'SELECT *'
      'FROM desservico')
    Left = 76
    object QrDesServicoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDesServicoNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrDesServicoSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 10
    end
    object QrDesServicoObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrDesServicoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDesServicoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDesServicoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDesServicoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDesServicoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDesServicoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrDesServicoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsDesServico: TDataSource
    DataSet = QrDesServico
    Left = 76
    Top = 44
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtDesServico
    CanUpd01 = BtServicoBase
    CanDel01 = BtFormulas
    Left = 272
    Top = 76
  end
  object PMDesServico: TPopupMenu
    OnPopup = PMDesServicoPopup
    Left = 516
    Top = 12
    object IncluiDesServico1: TMenuItem
      Caption = 'Inclui novo servi'#231'o'
      OnClick = IncluiDesServico1Click
    end
    object AlteraDesServico1: TMenuItem
      Caption = 'Altera servi'#231'o atual'
      OnClick = AlteraDesServico1Click
    end
    object ExcluiDesServico1: TMenuItem
      Caption = 'Exclui servi'#231'o atual'
      Enabled = False
      OnClick = ExcluiDesServico1Click
    end
  end
  object QrPreSrv: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPreSrvBeforeClose
    AfterScroll = QrPreSrvAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM presrv'
      'WHERE Codigo=0'
      '')
    Left = 148
    object QrPreSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPreSrvNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrPreSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPreSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPreSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPreSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPreSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPreSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPreSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPreSrvGarantiaDd: TIntegerField
      FieldName = 'GarantiaDd'
    end
    object QrPreSrvHrEvacuar: TIntegerField
      FieldName = 'HrEvacuar'
    end
    object QrPreSrvHrExecutar: TFloatField
      FieldName = 'HrExecutar'
    end
    object QrPreSrvValCalc: TFloatField
      FieldName = 'ValCalc'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPreSrvValInfo: TFloatField
      FieldName = 'ValInfo'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPreSrvValDesc: TFloatField
      FieldName = 'ValDesc'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPreSrvValTota: TFloatField
      FieldName = 'ValTota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrPreSrvDetalhes: TWideMemoField
      FieldName = 'Detalhes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPreSrvMoniDdTotl: TIntegerField
      FieldName = 'MoniDdTotl'
    end
    object QrPreSrvMoniDdIntv: TIntegerField
      FieldName = 'MoniDdIntv'
    end
  end
  object DsPreSrv: TDataSource
    DataSet = QrPreSrv
    Left = 148
    Top = 44
  end
  object PMServicoBase: TPopupMenu
    OnPopup = PMServicoBasePopup
    Left = 588
    Top = 12
    object IncluiServicoBase1: TMenuItem
      Caption = '&Inclui servi'#231'o base'
      OnClick = IncluiServicoBase1Click
    end
    object AlteraServicoBase1: TMenuItem
      Caption = '&Altera servi'#231'o base atual'
      OnClick = AlteraServicoBase1Click
    end
    object ExcluiServicoBase1: TMenuItem
      Caption = '&Exclui servi'#231'o base atual'
      OnClick = ExcluiServicoBase1Click
    end
  end
  object PMFormulas: TPopupMenu
    OnPopup = PMFormulasPopup
    Left = 656
    Top = 12
    object Aplicao1: TMenuItem
      Caption = '&Aplica'#231#227'o'
      object Inclui1: TMenuItem
        Caption = '&Adiciona f'#243'rmula de aplica'#231#227'o'
        OnClick = Inclui1Click
      end
      object Altera1: TMenuItem
        Caption = '&Remove f'#243'rmula de aplica'#231#227'o'
        OnClick = Altera1Click
      end
    end
    object Monitoramento1: TMenuItem
      Caption = '&Monitoramento'
      object Adicionafrmulademonitoramento1: TMenuItem
        Caption = '&Adiciona f'#243'rmula de monitoramento'
        OnClick = Adicionafrmulademonitoramento1Click
      end
      object Removefrmulademonitoramento1: TMenuItem
        Caption = '&Remove f'#243'rmula de monitoramento'
        OnClick = Removefrmulademonitoramento1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Cadastrodefrmulas1: TMenuItem
      Caption = '&Cadastro de f'#243'rmulas'
      OnClick = Cadastrodefrmulas1Click
    end
  end
  object QrPreFrm: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prf.*, frm.Nome NO_FORMULA'
      'FROM prefrm prf'
      'LEFT JOIN formulas frm ON frm.Codigo=prf.Formula'
      'WHERE prf.Controle>0')
    Left = 24
    Top = 272
    object QrPreFrmCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreFrmControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPreFrmConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPreFrmFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrPreFrmLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPreFrmDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPreFrmDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPreFrmUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPreFrmUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPreFrmAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPreFrmAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPreFrmNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
  end
  object QrPreMon: TmySQLQuery
    Database = Dmod.MyDB
    Left = 24
    Top = 320
    object QrPreMonCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreMonControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPreMonConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPreMonFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrPreMonLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPreMonDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPreMonDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPreMonUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPreMonUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPreMonAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPreMonAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPreMonNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
  end
  object DsPreFrm: TDataSource
    DataSet = QrPreFrm
    Left = 84
    Top = 272
  end
  object DsPreMon: TDataSource
    DataSet = QrPreMon
    Left = 84
    Top = 320
  end
  object QrPreAlv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prc.Nome NO_Praga_Z, osa.*'
      'FROM osalv osa'
      'LEFT JOIN praga_z prc ON prc.Codigo=osa.Praga_Z'
      'WHERE osa.Controle=:P0')
    Left = 24
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPreAlvCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osalv.Codigo'
    end
    object QrPreAlvControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osalv.Controle'
    end
    object QrPreAlvConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'osalv.Conta'
    end
    object QrPreAlvNO_Praga_Z: TWideStringField
      FieldName = 'NO_Praga_Z'
      Origin = 'praga_z.Nome'
      Size = 60
    end
    object QrPreAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
      Origin = 'osalv.Praga_Z'
    end
  end
  object DsPreAlv: TDataSource
    DataSet = QrPreAlv
    Left = 84
    Top = 224
  end
  object PMFormulasMon: TPopupMenu
    OnPopup = PMFormulasMonPopup
    Left = 720
    Top = 12
    object MenuItem5: TMenuItem
      Caption = '&Adiciona f'#243'rmula de monitoramento'
      OnClick = MenuItem5Click
    end
    object MenuItem6: TMenuItem
      Caption = '&Remove f'#243'rmula de monitoramento'
      OnClick = MenuItem6Click
    end
  end
  object PMFormulasApl: TPopupMenu
    OnPopup = PMFormulasAplPopup
    Left = 784
    Top = 12
    object Adicionafrmuladeaplicao1: TMenuItem
      Caption = '&Adiciona f'#243'rmula de aplica'#231#227'o'
      OnClick = Adicionafrmuladeaplicao1Click
    end
    object Removefrmuladeaplicao1: TMenuItem
      Caption = '&Remove f'#243'rmula de aplica'#231#227'o'
      OnClick = Removefrmuladeaplicao1Click
    end
  end
end
