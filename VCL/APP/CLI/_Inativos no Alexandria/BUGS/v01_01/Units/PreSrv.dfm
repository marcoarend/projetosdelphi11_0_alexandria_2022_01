object FmPreSrv: TFmPreSrv
  Left = 339
  Top = 185
  Caption = 'DES-SERVI-002 :: Cadastro de Servi'#231'o Base'
  ClientHeight = 467
  ClientWidth = 597
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 549
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 501
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 317
        Height = 32
        Caption = 'Cadastro de Servi'#231'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 32
        Caption = 'Cadastro de Servi'#231'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 317
        Height = 32
        Caption = 'Cadastro de Servi'#231'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 597
    Height = 305
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 597
      Height = 305
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 29
        Width = 597
        Height = 276
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 593
          Height = 259
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GBMonitoramento: TGroupBox
            Left = 0
            Top = 183
            Width = 593
            Height = 61
            Align = alTop
            Caption = ' Monitoramento: '
            TabOrder = 3
            object Label12: TLabel
              Left = 12
              Top = 17
              Width = 47
              Height = 13
              Caption = 'Dias total:'
            end
            object Label13: TLabel
              Left = 84
              Top = 17
              Width = 66
              Height = 13
              Caption = 'Intervalo dias:'
            end
            object EdMoniDdTotl: TdmkEdit
              Left = 12
              Top = 32
              Width = 68
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'MoniDdTotl'
              UpdCampo = 'MoniDdTotl'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdMoniDdIntv: TdmkEdit
              Left = 84
              Top = 32
              Width = 68
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'MoniDdIntv'
              UpdCampo = 'MoniDdIntv'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 593
            Height = 61
            Align = alTop
            Caption = ' Identifica'#231#227'o: '
            TabOrder = 0
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 589
              Height = 44
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 16
                Top = 0
                Width = 77
                Height = 13
                Caption = 'ID servi'#231'o base:'
                Color = clBtnFace
                Enabled = False
                ParentColor = False
              end
              object Label2: TLabel
                Left = 100
                Top = 0
                Width = 72
                Height = 13
                Caption = 'Descri'#231#227'o [F4]:'
              end
              object EdControle: TdmkEdit
                Left = 16
                Top = 16
                Width = 76
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Controle'
                UpdCampo = 'Controle'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNome: TdmkEdit
                Left = 100
                Top = 16
                Width = 473
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Nome'
                UpdCampo = 'Nome'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdNomeKeyDown
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 0
            Top = 61
            Width = 593
            Height = 61
            Align = alTop
            Caption = ' Caracter'#237'sticas: '
            TabOrder = 1
            object Panel7: TPanel
              Left = 2
              Top = 15
              Width = 589
              Height = 44
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label25: TLabel
                Left = 16
                Top = 1
                Width = 64
                Height = 13
                Caption = 'Garantia (dd):'
              end
              object Label26: TLabel
                Left = 88
                Top = 1
                Width = 73
                Height = 13
                Caption = 'Evacua'#231#227'o (h):'
              end
              object Label27: TLabel
                Left = 168
                Top = 1
                Width = 66
                Height = 13
                Caption = 'Execu'#231#227'o (h):'
              end
              object EdGarantiaDd: TdmkEdit
                Left = 16
                Top = 16
                Width = 69
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'GarantiaDd'
                UpdCampo = 'GarantiaDd'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdHrEvacuar: TdmkEdit
                Left = 88
                Top = 16
                Width = 77
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'HrEvacuar'
                UpdCampo = 'HrEvacuar'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdHrExecutar: TdmkEdit
                Left = 168
                Top = 16
                Width = 69
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                QryCampo = 'HrExecutar'
                UpdCampo = 'HrExecutar'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
          object GroupBox5: TGroupBox
            Left = 0
            Top = 122
            Width = 593
            Height = 61
            Align = alTop
            Caption = ' Valores: '
            TabOrder = 2
            object Panel8: TPanel
              Left = 2
              Top = 15
              Width = 589
              Height = 44
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label11: TLabel
                Left = 268
                Top = 1
                Width = 36
                Height = 13
                Caption = '$ Total:'
                Visible = False
              end
              object Label10: TLabel
                Left = 184
                Top = 1
                Width = 58
                Height = 13
                Caption = '$ Desconto:'
                Enabled = False
                Visible = False
              end
              object Label8: TLabel
                Left = 100
                Top = 1
                Width = 59
                Height = 13
                Caption = '$ Informado:'
              end
              object Label6: TLabel
                Left = 16
                Top = 1
                Width = 59
                Height = 13
                Caption = '$ Calculado:'
                Enabled = False
              end
              object EdValCalc: TdmkEdit
                Left = 16
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ValCalc'
                UpdCampo = 'ValCalc'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValCalcChange
              end
              object EdValTota: TdmkEdit
                Left = 268
                Top = 16
                Width = 86
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 3
                Visible = False
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ValTota'
                UpdCampo = 'ValTota'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdValDesc: TdmkEdit
                Left = 184
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 2
                Visible = False
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ValDesc'
                UpdCampo = 'ValDesc'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValDescChange
              end
              object EdValInfo: TdmkEdit
                Left = 100
                Top = 16
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ValInfo'
                UpdCampo = 'ValInfo'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValInfoChange
              end
            end
          end
          object MeDetalhes: TdmkMemo
            Left = 0
            Top = 244
            Width = 593
            Height = 15
            Align = alClient
            TabOrder = 4
            Visible = False
            QryCampo = 'Detalhes'
            UpdCampo = 'Detalhes'
            UpdType = utYes
          end
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 597
        Height = 29
        Align = alTop
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        object Label9: TLabel
          Left = 12
          Top = 8
          Width = 39
          Height = 13
          Caption = 'Servi'#231'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdit4: TDBEdit
          Left = 56
          Top = 4
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsDesServico
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 112
          Top = 4
          Width = 468
          Height = 21
          TabStop = False
          DataField = 'Nome'
          DataSource = DsDesServico
          TabOrder = 1
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 353
    Width = 597
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 593
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 397
    Width = 597
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 451
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 449
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrDesServico: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM desservico'
      'ORDER BY Nome')
    Left = 536
    Top = 84
    object QrDesServicoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDesServicoNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsDesServico: TDataSource
    DataSet = QrDesServico
    Left = 536
    Top = 128
  end
end
