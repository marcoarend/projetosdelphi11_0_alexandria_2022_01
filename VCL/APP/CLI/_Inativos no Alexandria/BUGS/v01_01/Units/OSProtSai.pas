unit OSProtSai;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, dmkCompoStore,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnDmkEnums, dmkEditDateTimePicker;

type
  TFmOSProtSai = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    TbOSProtSai: TmySQLTable;
    DsOSProtSai: TDataSource;
    DBGOSsStat: TDBGrid;
    TbOSProtSaiID_Cod1: TIntegerField;
    TbOSProtSaiID_Cod2: TIntegerField;
    TbOSProtSaiID_Cod3: TIntegerField;
    TbOSProtSaiID_Cod4: TIntegerField;
    TbOSProtSaiID_Txt1: TWideStringField;
    TbOSProtSaiID_Txt2: TWideStringField;
    TbOSProtSaiID_Txt3: TWideStringField;
    TbOSProtSaiID_Txt4: TWideStringField;
    QrEstatusOSs: TmySQLQuery;
    DsEstatusOSs: TDataSource;
    QrEstatusOSsCodigo: TIntegerField;
    QrEstatusOSsNome: TWideStringField;
    TbOSProtSaiNO_ESTATUS: TWideStringField;
    QrLoc: TmySQLQuery;
    QrLocCodigo: TIntegerField;
    QrProtPakIts: TmySQLQuery;
    QrProtPakItsConta: TIntegerField;
    CSTabSheetChamou: TdmkCompoStore;
    TbOSProtSaiSession: TDateTimeField;
    PnEmpresa: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    BtOSPsq: TBitBtn;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbOSProtSaiBeforePost(DataSet: TDataSet);
    procedure TbOSProtSaiAfterPost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure TbOSProtSaiAfterRefresh(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure TbOSProtSaiAfterDelete(DataSet: TDataSet);
    procedure BtOSPsqClick(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaBtOK();
  public
    { Public declarations }
    FProtAddIts: String;
    FSessionDT: TDateTime;
    //
    procedure ReopenProtAddIts();
  end;

  var
  FmOSProtSai: TFmOSProtSai;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, Protocolo, MyGlyfs, Principal,
MyDBCheck, OSPsq, UnProtocoUnit;

{$R *.DFM}

procedure TFmOSProtSai.BtOKClick(Sender: TObject);
var
  ProtocoPakControle, Entidade: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
  Entidade := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
    if MyObjects.FIC(Entidade = 0, EdEmpresa, 'Informe a empresa!') then
      Exit;
    ProtocoPakControle := UnProtocolo.ProtocolosCD_OS1(istTodos,
      TDBGrid(DBGOSsStat), TmySQLQuery(TBOSProtSai), Entidade);
    //
    DBGOSsStat.Enabled := False;
    BtOK.Visible := True;
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'DELETE FROM ' + FProtAddIts,
    'WHERE Session="' + Geral.FDT(FSessionDT, 109) + '"',
    '']);
    //
    BtSaidaClick(Self);
    //
    if ProtocoPakControle <> 0 then
    begin
      if Geral.MB_Pergunta('Deseja visualizar o lote?') = ID_YES then
        ProtocoUnit.MostraFormProtocolos(ProtocoPakControle, 0);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSProtSai.BtOSPsqClick(Sender: TObject);
var
  Selecionados: TArrSelIdxInt2;
  N: Integer;
  I: Integer;
begin
  if DBCheck.CriaFm(TFmOSPsq, FmOSPsq, afmoNegarComAviso) then
  begin
    FmOSPsq.ConfiguraParaMultiSelecao(Selecionados, EdEmpresa.ValueVariant);
    FmOSPsq.ShowModal;
    MyObjects.GetSelecionadosBookmark_Int2(Self, TDBGrid(FmOSPsq.DBGPsq),
    'Codigo', 'Estatus', Selecionados);
    FmOSPsq.Destroy;
    //
    N := Length(Selecionados);
    if N > 0 then
    begin
      PB1.Position := 0;
      PB1.Max := N;
      for I := 0 to N - 1 do
      begin
        PB1.Position := PB1.Position + 1;
        //if not TbOSProtsai.Locate('ID_Cod1') then
        begin
          TbOSProtsai.Append;
          TbOSProtSaiID_Cod1.Value := Selecionados[I][0];
          TbOSProtSaiID_Cod2.Value := Selecionados[I][1];
          TbOSProtsai.Post;
        end;
      end;
      // Parei aqui!!!
      //Como fazer com o status?
    end;
  end;
end;

procedure TFmOSProtSai.BtSaidaClick(Sender: TObject);
begin
  if TFmOSProtSai(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmOSProtSai(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmOSProtSai.EdEmpresaChange(Sender: TObject);
begin
  DBGOSsStat.Visible := EdEmpresa.ValueVariant <> 0;
  HabilitaBtOK();
end;

procedure TFmOSProtSai.FormActivate(Sender: TObject);
begin
  if TFmOSProtSai(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmOSProtSai.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPDataIni.Date := Date;
  TPDataFim.Date := Date;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrEstatusOSs, Dmod.MyDB);
  //
  EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
end;

procedure TFmOSProtSai.FormDestroy(Sender: TObject);
begin
  //DModG.MyPID_DB_Excl(FProtAddIts);
end;

procedure TFmOSProtSai.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSProtSai.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmOSProtSai.HabilitaBtOK();
begin
  BtOK.Enabled := (TBOSProtSai.State <> dsInactive)
  and (TBOSProtSai.RecordCount > 0) and (EdEmpresa.ValueVariant <> 0);
end;

procedure TFmOSProtSai.ReopenProtAddIts();
begin
  TBOSProtSai.Close;
  TBOSProtSai.TableName := FProtAddIts;
  TBOSProtSai.Filter := 'Session=''' + Geral.FDT(FSessionDT, 109) + '''';
  TBOSProtSai.Filtered := True;
  UnDmkDAC_PF.AbreTable(TBOSProtSai, DModG.MyPID_DB);
end;

procedure TFmOSProtSai.TbOSProtSaiAfterDelete(DataSet: TDataSet);
begin
  HabilitaBtOK();
end;

procedure TFmOSProtSai.TbOSProtSaiAfterPost(DataSet: TDataSet);
begin
  if (TBOSProtSaiID_Cod1.Value = 0) and (TBOSProtSaiID_Cod2.Value = 0) then
    TBOSProtSai.Delete;
  HabilitaBtOK();
end;

procedure TFmOSProtSai.TbOSProtSaiAfterRefresh(DataSet: TDataSet);
begin
  PnEmpresa.Enabled := TBOSProtSai.RecordCount = 0;
  HabilitaBtOK();
end;

procedure TFmOSProtSai.TbOSProtSaiBeforePost(DataSet: TDataSet);
var
  OSCab, Estatus: Integer;
  Continua: Boolean;
  Txt: String;
begin
  //Continua := True;
  OSCab   := TBOSProtSaiID_Cod1.Value;
  Estatus := TBOSProtSaiID_Cod2.Value;
  //
  Continua := not MyObjects.FIC(OSCab = 0, nil, 'Informe o localizador!');
  if Continua then
    Continua := not MyObjects.FIC(Estatus = 0, nil, 'Informe um status v�lido!');
  if Continua then
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM oscab ',
    'WHERE Codigo=' + Geral.FF0(OSCab),
    '']);
    //
    Continua := not MyObjects.FIC(QrLocCodigo.Value = 0, nil,
    'OS n�o localizada!' + sLineBreak + 'Localizador: ' + Geral.FF0(OSCab));
  end;
  if Continua then
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM estatusoss ',
    'WHERE Codigo=' + Geral.FF0(Estatus),
    '']);
    //
    Continua := not MyObjects.FIC(QrLocCodigo.Value = 0, nil,
    'Status n�o localizado!' + sLineBreak + 'C�digo: ' + Geral.FF0(Estatus));
  end;
  if Continua then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrProtPakIts, Dmod.MyDB, [
    'SELECT Conta, ',
    'ID_Cod1, ID_Cod2, ID_Cod3, ID_Cod4 ',
    'FROM protpakits ',
    'WHERE Link_ID=' + Geral.FF0(VAR_TIPO_LINK_ID_03_ORDEM_DE_SERVCO1),
    'AND ID_Cod1=' + Geral.FF0(OSCab),
    'AND ID_COD2=' + Geral.FF0(Estatus),
    '']);
    //
    Continua := QrProtPakIts.RecordCount = 0;
    if not Continua then
    begin
      Txt := 'OS ' + Geral.FF0(OSCab) + ' status ' + Geral.FF0(Estatus);
      if QrProtPakIts.RecordCount = 1 then
        Txt := 'J� existe um protoclo para a ' + Txt + '!' + sLineBreak +
        'Deseja criar mais um mesmo assim?'
      else
        Txt := 'J� existem ' + Geral.FF0(QrProtPakIts.RecordCount) +
        ' protocolos para a ' + Txt + '!' + sLineBreak +
        'Deseja criar mais um mesmo assim?';
      Continua := Geral.MB_Pergunta(Txt) = ID_YES;
    end;
  end;
  if not Continua then
  begin
    TBOSProtSaiSession.Value := FSessionDT;
    TBOSProtSaiID_Cod1.Value := 0;
    TBOSProtSaiID_Cod2.Value := 0;
    //
  end else
  begin
    TBOSProtSaiSession.Value := FSessionDT;
  end;
end;

end.
