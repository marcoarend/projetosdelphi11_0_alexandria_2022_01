unit RMIP_R012;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, mySQLDbTables, frxClass,
  frxDBSet, dmkGeral;

type
  TFmRMIP_R012 = class(TForm)
    Qr012OSPrv: TmySQLQuery;
    Qr012OSPrvCodigo: TIntegerField;
    Qr012OSPrvControle: TIntegerField;
    Qr012OSPrvDtHrContat: TDateTimeField;
    Qr012OSPrvFormContat: TIntegerField;
    Qr012OSPrvCliente: TIntegerField;
    Qr012OSPrvContato: TIntegerField;
    Qr012OSPrvAgente: TIntegerField;
    Qr012OSPrvNome: TWideStringField;
    Qr012OSPrvNO_Cliente: TWideStringField;
    Qr012OSPrvNO_Agente: TWideStringField;
    Qr012OSPrvNO_Contato: TWideStringField;
    Qr012OSPrvNO_FormContat: TWideStringField;
    Qr012OSPrvNO_PrvStatus: TWideStringField;
    frxReport012A: TfrxReport;
    frxDs012OSPrv: TfrxDBDataset;
  private
    { Private declarations }
    procedure ReopenOSPrv();
  public
    { Public declarations }
    FEmpresa, FCliente, FItemRel: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    //FMemo1: TMemo;
    procedure GeraImp_SPs();
  end;

var
  FmRMIP_R012: TFmRMIP_R012;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UnMyObjects, UnDmkProcFunc;

{$R *.dfm}

{ TFmRMIP_R012 }

procedure TFmRMIP_R012.GeraImp_SPs();
begin
  ReopenOSPrv;
  //
  MyObjects.frxDefineDatasets(frxReport012A, [
  DModG.frxDsDono,
  frxDs012OSPrv
  ]);
  //
  frxReport012A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport012A.Variables['VARF_DATA']    := FDtaImp;
  frxReport012A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport012A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  frxReport012A.Variables['VARF_PERIODO']  := QuotedStr(dmkPF.PeriodoImp(FDtaIni,
    FDtaFim, 0, 0, True, True, False, False, '', ''));
  //
  //MyObjects.frxPrepara(frxReport012A, 'SPs');
end;

procedure TFmRMIP_R012.ReopenOSPrv;
var
  Ini, Fim, SQL_Cliente: String;
begin
  Ini := Geral.FDT(FDtaIni, 1);
  Fim := Geral.FDT(FDtaFim, 1);
  //
(*
  if FCliente <> 0 then
    SQL_Cliente := 'AND prv.Entidade=' + Geral.FF0(FCliente)
  else
    SQL_Cliente := '';
*)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr012OSPrv, Dmod.MyDB, [
  'SELECT ops.Nome NO_PrvStatus, prv.*, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ',
  'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_Agente, ',
  'eco.Nome NO_Contato, fct.Nome NO_FormContat ',
  'FROM osprv prv ',
  'LEFT JOIN oscab cab ON cab.Codigo=prv.Codigo',
  'LEFT JOIN entidades cli ON cli.Codigo=prv.Cliente ',
  'LEFT JOIN entidades age ON age.Codigo=prv.Agente ',
  'LEFT JOIN enticontat eco ON eco.Controle=prv.Contato ',
  'LEFT JOIN formcontat fct ON fct.Codigo=prv.FormContat ',
  'LEFT JOIN osprvsta ops ON ops.Codigo=prv.PrvStatus ',
  'WHERE prv.Cliente=' + Geral.FF0(FCliente),
  'AND (cab.DtaExeFim BETWEEN "' + Ini + '" AND "' + Fim + '"',
  'OR prv.Codigo=0) ',
  '']);
end;

end.
