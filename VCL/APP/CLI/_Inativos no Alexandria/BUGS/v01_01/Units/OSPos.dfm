object FmOSPos: TFmOSPos
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-017 :: P'#243's-venda'
  ClientHeight = 356
  ClientWidth = 626
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 626
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 578
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 530
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 128
        Height = 32
        Caption = 'P'#243's-venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 128
        Height = 32
        Caption = 'P'#243's-venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 128
        Height = 32
        Caption = 'P'#243's-venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 626
    Height = 194
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 626
      Height = 194
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 37
        Width = 626
        Height = 157
        Align = alClient
        TabOrder = 1
        object GBEdita: TGroupBox
          Left = 2
          Top = 15
          Width = 622
          Height = 106
          Align = alTop
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 16
            Width = 14
            Height = 13
            Caption = 'ID:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label9: TLabel
            Left = 76
            Top = 16
            Width = 24
            Height = 13
            Caption = 'Dias:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label2: TLabel
            Left = 12
            Top = 56
            Width = 47
            Height = 13
            Caption = 'Pr'#233' Email:'
          end
          object SBAplicID: TSpeedButton
            Left = 584
            Top = 72
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SBAplicIDClick
          end
          object EdControle: TdmkEdit
            Left = 12
            Top = 32
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdDias: TdmkEdit
            Left = 76
            Top = 32
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Dias'
            UpdCampo = 'Dias'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object RGAplicacao: TdmkRadioGroup
            Left = 132
            Top = 8
            Width = 473
            Height = 53
            Caption = ' A'#231#227'o a ser tomada nesta P'#243's Venda: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'sListaAcaoPosVda> Nenhuma'
              'sListaAcaoPosVda> Telefonar'
              'sListaAcaoPosVda> Enviar email')
            TabOrder = 2
            QryCampo = 'Aplicacao'
            UpdCampo = 'Aplicacao'
            UpdType = utYes
            OldValor = 0
          end
          object EdAplicID: TdmkEditCB
            Left = 12
            Top = 72
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'AplicID'
            UpdCampo = 'AplicID'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBAplicID
            IgnoraDBLookupComboBox = False
          end
          object CBAplicID: TdmkDBLookupComboBox
            Left = 72
            Top = 72
            Width = 513
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPreEmail
            TabOrder = 4
            dmkEditCB = EdAplicID
            QryCampo = 'AplicID'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object CkContinuar: TCheckBox
          Left = 16
          Top = 128
          Width = 113
          Height = 17
          Caption = 'Continuar inserindo.'
          TabOrder = 1
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 626
        Height = 37
        Align = alTop
        Color = clBtnFace
        Enabled = False
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        object Label7: TLabel
          Left = 8
          Top = 12
          Width = 57
          Height = 13
          Caption = 'Localizador:'
          Color = clBtnFace
          ParentColor = False
        end
        object EdCodigo: TdmkEdit
          Left = 68
          Top = 8
          Width = 93
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 242
    Width = 626
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 622
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 286
    Width = 626
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 480
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 478
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPreEmail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 136
    Top = 180
    object QrPreEmailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreEmailNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreEmail: TDataSource
    DataSet = QrPreEmail
    Left = 164
    Top = 180
  end
end
