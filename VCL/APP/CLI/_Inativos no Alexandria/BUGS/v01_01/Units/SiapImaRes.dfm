object FmSiapImaRes: TFmSiapImaRes
  Left = 339
  Top = 185
  Caption = 'CAD-SUBCL-004 :: Cadastro de Residente'
  ClientHeight = 437
  ClientWidth = 560
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 560
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 512
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 464
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 278
        Height = 32
        Caption = 'Cadastro de Residente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 278
        Height = 32
        Caption = 'Cadastro de Residente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 278
        Height = 32
        Caption = 'Cadastro de Residente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 560
    Height = 275
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 560
      Height = 275
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 560
        Height = 275
        Align = alClient
        TabOrder = 0
        object LaOrdem: TLabel
          Left = 12
          Top = 56
          Width = 83
          Height = 13
          Caption = 'Nome da pessoa:'
        end
        object SBResidente: TSpeedButton
          Left = 520
          Top = 28
          Width = 21
          Height = 22
          Caption = '...'
          OnClick = SBResidenteClick
        end
        object LaPrompt: TLabel
          Left = 12
          Top = 12
          Width = 98
          Height = 13
          Caption = 'Grau de Parentesco:'
        end
        object Label1: TLabel
          Left = 492
          Top = 56
          Width = 50
          Height = 13
          Caption = 'Ano Natal:'
        end
        object Label2: TLabel
          Left = 12
          Top = 96
          Width = 42
          Height = 13
          Caption = 'Cuidado:'
        end
        object SBCuidado: TSpeedButton
          Left = 520
          Top = 112
          Width = 21
          Height = 22
          Caption = '...'
          OnClick = SBCuidadoClick
        end
        object Label3: TLabel
          Left = 12
          Top = 136
          Width = 66
          Height = 13
          Caption = 'Observa'#231#245'es:'
        end
        object Label4: TLabel
          Left = 460
          Top = 56
          Width = 27
          Height = 13
          Caption = '[F4] >'
        end
        object CkContinuar: TCheckBox
          Left = 12
          Top = 248
          Width = 213
          Height = 17
          Caption = 'Continuar inserindo ap'#243's a confirma'#231#227'o.'
          Checked = True
          State = cbChecked
          TabOrder = 7
        end
        object CBResidente: TdmkDBLookupComboBox
          Left = 80
          Top = 28
          Width = 437
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsResisdentes
          TabOrder = 1
          dmkEditCB = EdResidente
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdNome: TdmkEdit
          Left = 12
          Top = 72
          Width = 476
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdResidente: TdmkEditCB
          Left = 12
          Top = 28
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBResidente
          IgnoraDBLookupComboBox = False
        end
        object EdAnoNatal: TdmkEdit
          Left = 492
          Top = 72
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCuidado: TdmkEditCB
          Left = 12
          Top = 112
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCuidado
          IgnoraDBLookupComboBox = False
        end
        object CBCuidado: TdmkDBLookupComboBox
          Left = 80
          Top = 112
          Width = 437
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCuidados
          TabOrder = 5
          dmkEditCB = EdCuidado
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object MeObservacao: TdmkMemo
          Left = 12
          Top = 152
          Width = 533
          Height = 89
          TabOrder = 6
          UpdType = utYes
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 323
    Width = 560
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 556
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 367
    Width = 560
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 414
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 412
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrResidentes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM residentes'
      'ORDER BY Nome')
    Left = 208
    Top = 66
    object QrResidentesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrResidentesNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsResisdentes: TDataSource
    DataSet = QrResidentes
    Left = 236
    Top = 66
  end
  object QrCuidados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cuidados'
      'ORDER BY Nome')
    Left = 208
    Top = 150
    object QrCuidadosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCuidadosNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCuidados: TDataSource
    DataSet = QrCuidados
    Left = 236
    Top = 150
  end
end
