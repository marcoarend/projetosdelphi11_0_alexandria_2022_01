unit CunsCadSVGZumPos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, RSSVGCtrls;

type
  TFmCunsCadSVGZumPos = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    EdSvgZumF: TdmkEdit;
    Label1: TLabel;
    EdSvgPosX: TdmkEdit;
    Label2: TLabel;
    EdSvgPosY: TdmkEdit;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    Label4: TLabel;
    CkAplicAll: TCheckBox;
    EdEquipamento: TdmkEdit;
    Label5: TLabel;
    EdSiapImaSVG: TdmkEdit;
    Label6: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCunsCadSVGZumPos: TFmCunsCadSVGZumPos;

implementation

uses UnMyObjects, Module, UMySQLModule, CunsCadSVG1, MyDBCheck;

{$R *.DFM}

procedure TFmCunsCadSVGZumPos.BtOKClick(Sender: TObject);
var
  Codigo, SiapImaSVG, Equipamento: Integer;
  SvgPosX, SvgPosY, SvgZumF: Double;
  //
  Cliente, SiapTerCad, SiapImaCad: Integer;
  PMV: TRSSVGImage;
begin
  Codigo         := EdCodigo.ValueVariant;
  SvgPosX        := EdSvgPosX.ValueVariant;
  SvgPosY        := EdSvgPosY.ValueVariant;
  SvgZumF        := EdSvgZumF.ValueVariant;
  SiapImaSVG     := EdSiapImaSVG.ValueVariant;
  Equipamento    := EdEquipamento.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False, [
  'SvgPosX', 'SvgPosY', 'SvgZumF'], [
  'Codigo'], [
  SvgPosX, SvgPosY, SvgZumF], [
  Codigo], True) then
  begin
    if CkAplicAll.Checked then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pipcad', False, [
      'SvgZumF'], [
      'SiapImaSVG', 'Equipamento'], [
      SvgZumF], [
      SiapImaSVG, Equipamento], True) then
      begin
        FmCunsCadSVG1.LimpaPMVs();
        FmCunsCadSVG1.CarregaDadosCliente(
          FmCunsCadSVG1.FCliente, FmCunsCadSVG1.FSiapTerCad,
          FmCunsCadSVG1.FSiapImaCad, FmCunsCadSVG1.FSiapImaSVG);
      end;
    end else
    begin
      if FmCunsCadSVG1.SelecionaPMV_PeloCodigo(Codigo, PMV) then
      begin
        PMV.Left := Trunc(SvgPosX);
        PMV.Top  := Trunc(SvgPosY);
        FmCunsCadSVG1.RefazZoomPMV(Codigo, SvgZumF, PMV);
      end else
        Geral.MB_Erro('N�o foi poss�vel localizar o PMV selecionado!');
    end;
    //
    Close;
  end;
end;

procedure TFmCunsCadSVGZumPos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCunsCadSVGZumPos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCunsCadSVGZumPos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCunsCadSVGZumPos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
