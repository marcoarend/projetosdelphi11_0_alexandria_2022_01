unit CunsImgNCT;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums;

type
  TFmCunsImgNCT = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCunsImgNCT: TmySQLQuery;
    QrCunsImgNCTCodigo: TIntegerField;
    QrCunsImgNCTNome: TWideStringField;
    DsCunsImgNCT: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label3: TLabel;
    EdTxtCAC: TdmkEdit;
    QrCunsImgNCTTxtCAC: TWideStringField;
    QrCunsImgNCTLk: TIntegerField;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCunsImgNCTAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCunsImgNCTBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCunsImgNCT: TFmCunsImgNCT;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCunsImgNCT.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCunsImgNCT.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCunsImgNCTCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCunsImgNCT.DefParams;
begin
  VAR_GOTOTABELA := 'cunsimgnct';
  VAR_GOTOMYSQLTABLE := QrCunsImgNCT;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cunsimgnct');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCunsImgNCT.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCunsImgNCT.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCunsImgNCT.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmCunsImgNCT.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCunsImgNCT.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCunsImgNCT.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCunsImgNCT.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCunsImgNCT.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCunsImgNCT.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCunsImgNCT, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'cunsimgnct');
end;

procedure TFmCunsImgNCT.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCunsImgNCTCodigo.Value;
  Close;
end;

procedure TFmCunsImgNCT.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('cunsimgnct', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrCunsImgNCTCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'cunsimgnct', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCunsImgNCT.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cunsimgnct', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCunsImgNCT.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCunsImgNCT, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'cunsimgnct');
end;

procedure TFmCunsImgNCT.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmCunsImgNCT.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCunsImgNCTCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCunsImgNCT.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCunsImgNCT.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCunsImgNCTCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCunsImgNCT.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCunsImgNCT.QrCunsImgNCTAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCunsImgNCT.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCunsImgNCT.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCunsImgNCTCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cunsimgnct', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCunsImgNCT.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCunsImgNCT.QrCunsImgNCTBeforeOpen(DataSet: TDataSet);
begin
  QrCunsImgNCTCodigo.DisplayFormat := FFormatFloat;
end;

end.

