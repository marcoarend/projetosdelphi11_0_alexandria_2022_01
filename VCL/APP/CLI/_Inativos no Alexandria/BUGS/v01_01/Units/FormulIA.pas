unit FormulIA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, dmkDBGrid, UnDmkEnums;

type
  TFmFormulIA = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Timer1: TTimer;
    Qr_OS_Frm_Abr: TmySQLQuery;
    Ds_OS_Frm_Abr: TDataSource;
    QrOFA: TmySQLQuery;
    QrOFAAtivo: TSmallintField;
    Qr_OS_Frm_AbrCodigo: TIntegerField;
    Qr_OS_Frm_AbrNome: TWideStringField;
    Qr_OS_Frm_AbrAtivo: TSmallintField;
    dmkDBGrid1: TdmkDBGrid;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrOFACodigo: TIntegerField;
    QrOFANome: TWideStringField;
    QrFormulas: TmySQLQuery;
    QrFormulasNO_EQUIAPLIC: TWideStringField;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasLk: TIntegerField;
    QrFormulasDataCad: TDateField;
    QrFormulasDataAlt: TDateField;
    QrFormulasUserCad: TIntegerField;
    QrFormulasUserAlt: TIntegerField;
    QrFormulasAlterWeb: TSmallintField;
    QrFormulasAtivo: TSmallintField;
    QrFormulasEquipAplic: TIntegerField;
    QrFormulasQtdTot: TFloatField;
    QrFormulasQtdQSP: TFloatField;
    QrFormulasCusTot: TFloatField;
    DsFormulas: TDataSource;
    GBDados: TGroupBox;
    Panel6: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FCriou: Boolean;
    F_OS_Frm_Abr: String;
    //
    procedure Reopen_OS_Frm_Abr(Codigo: Integer);
    procedure AtivarTudo(Ativa: Integer);

  public
    { Public declarations }
    procedure ReopenFormulas();
  end;

  var
  FmFormulIA: TFmFormulIA;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, CreateBugs, ModuleGeral,
  Formulas, CfgCadLista;

{$R *.DFM}

procedure TFmFormulIA.AtivarTudo(Ativa: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
    UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
    'UPDATE ' + F_OS_Frm_Abr ,
    'SET Ativo=' + Geral.FF0(Ativa),
    '']);
    //
    Reopen_OS_Frm_Abr(0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFormulIA.BitBtn1Click(Sender: TObject);
var
  Abrangencia: Integer;
begin
  Abrangencia := Qr_OS_Frm_AbrCodigo.Value;

  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'abrangicie', 60, ncGerlSeq1,
    'Cadastro de Abrangências (superfícies de aplicação)',
    [], False, Null, [], [], False, Abrangencia);

  Reopen_OS_Frm_Abr(QrFormulasCodigo.Value);
end;

procedure TFmFormulIA.BtNenhumClick(Sender: TObject);
begin
  AtivarTudo(0);
end;

procedure TFmFormulIA.BtOKClick(Sender: TObject);
var
  Codigo: String;
  Controle, Abrangicie: Integer;
begin
  //Controle := 0;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo selecionados');
    //
    Codigo   := Geral.FF0(QrFormulascodigo.Value);
    //
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'INSERT INTO livre2 (Tabela, Campo, Codigo, Vezes, AlterWeb, Ativo) ',
    'SELECT "formulia" Tabela, "Controle" Campo, ',
    'Controle Codigo, 1 Vezes, 0 AlterWeb, 0 Ativo ',
    'FROM formulia ',
    'WHERE Codigo=' + Codigo,
    ';',
    'DELETE FROM formulia ',
    'WHERE Codigo=' + Codigo,
    '']);
    //
    QrOFA.Close;
    UnDmkDAC_PF.AbreMySQLQuery0(QrOFA, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_OS_Frm_Abr,
    'WHERE Ativo=1 ',
    'ORDER BY Nome',
    '']);
    QrOFA.First;
    while not QrOFA.Eof do
    begin
      Controle       := 0;
      Abrangicie     := QrOFACodigo.Value;
      //
      Controle := UMyMod.BPGS1I32_Reaproveita('formulia', 'Controle', '', '', tsPos, stIns, Controle);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'formulia', False, [
      'Codigo', 'Abrangicie'], [
      'Controle'], [
      Codigo, Abrangicie], [
      Controle], True);
      //
      QrOFA.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFormulIA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulIA.BtTodosClick(Sender: TObject);
begin
  AtivarTudo(1);
end;

procedure TFmFormulIA.dmkDBGrid1CellClick(Column: TColumn);
var
  Codigo, Ativo: Integer;
begin
  Codigo         := Qr_OS_Frm_AbrCodigo.Value;
  Ativo          := Qr_OS_Frm_AbrAtivo.Value;
  if Ativo = 0 then
    Ativo := 1
  else
    Ativo := 0;  
  //
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_OS_Frm_Abr, False, [
  'Ativo'], ['Codigo'
  ], [
  Ativo], [Codigo
  ], False) then
    Reopen_OS_Frm_Abr(Codigo);
end;

procedure TFmFormulIA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := True;
end;

procedure TFmFormulIA.FormCreate(Sender: TObject);
begin
  FCriou := False;
end;

procedure TFmFormulIA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulIA.ReopenFormulas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT gg1.Nome NO_EQUIAPLIC, frm.* ',
  'FROM formulas frm ',
  'LEFT JOIN gragrux ggx ON ggx.Controle = frm.EquipAplic ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE frm.Codigo=' + Geral.FF0(FmFormulas.QrFormulasCodigo.Value),
  '']);
end;

procedure TFmFormulIA.Reopen_OS_Frm_Abr(Codigo: Integer);
begin
  Qr_OS_Frm_Abr.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr_OS_Frm_Abr, DModG.MyPID_DB, [
  'SELECT oa.*',
  'FROM ' + F_OS_Frm_Abr + ' oa ',
  'ORDER BY oa.Nome ',
  '']);
  Qr_OS_Frm_Abr.Locate('Codigo', Codigo, [])
end;

procedure TFmFormulIA.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if not FCriou then
  begin
    Screen.Cursor := crHourGlass;
    try
      FCriou := True;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela temporária!');
      F_OS_Frm_Abr :=
        UnCreateBugs.RecriaTempTableNovo(ntrtt_OS_Frm_Abr, DmodG.QrUpdPID1, False);
      //
      ReopenFormulas();
      //
      UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
      'INSERT INTO ' + F_OS_Frm_Abr,
      'SELECT Codigo, Nome, 0 Ativo ',
      'FROM ' + TMeuDB + '.abrangicie; ',
      '']);
      UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
      'UPDATE ' + F_OS_Frm_Abr,
      'SET Ativo=1 ',
      'WHERE Codigo IN ( ',
      '  SELECT abrangicie ',
      '  FROM ' + TMeuDB + '.formulia ',
      '  WHERE Codigo=' + Geral.FF0(QrFormulasCodigo.Value),
      ')',
      '']);
      //
      Reopen_OS_Frm_Abr(0);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
