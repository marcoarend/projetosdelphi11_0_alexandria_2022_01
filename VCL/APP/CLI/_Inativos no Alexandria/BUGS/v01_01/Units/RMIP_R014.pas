unit RMIP_R014;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Data.DB, mySQLDbTables, dmkGeral, UnDmkEnums, frxClass,
  frxDBSet, frxChart, UnProjGroup_Consts, UnInternalConsts, UnMyObjects,
  UnDmkProcFunc, VCLTee.Chart, VCLTee.Series(*, fs_ichartrtti*);

type
  TFmRMIP_R014 = class(TForm)
    Qr014A_PrgLstIts: TmySQLQuery;
    frxReport014A: TfrxReport;
    Qr014A_PrgLstItsPrgLstCab: TIntegerField;
    Qr014A_PrgLstItsPrgLstIts: TIntegerField;
    Qr014A_PrgLstItsNO_LST: TWideStringField;
    Qr014A_PrgLstItsSigla: TWideStringField;
    Qr014A_PrgLstItsNome: TWideStringField;
    Qr014A_PrgLstItsFuncoes: TSmallintField;
    Qr014Grupos: TmySQLQuery;
    Qr014NResp: TmySQLQuery;
    IntegerField1: TIntegerField;
    Qr014XVals: TmySQLQuery;
    Qr014Itens: TmySQLQuery;
    frxDs014A_PrgLstIts: TfrxDBDataset;
    Qr014ItensITENS: TFloatField;
    Qr014ItensDtaExeFim: TDateField;
    Qr014XValsDtaExeFim: TDateField;
    Qr014XValsDIAS: TIntegerField;
    Qr014ItensDIAS: TIntegerField;
    Qr014Dados: TmySQLQuery;
    Qr014DadosResposta: TWideStringField;
    Qr014DadosCorPizza: TIntegerField;
    Qr014GruposResposta: TWideStringField;
    Qr014A_OSs: TmySQLQuery;
    Qr014A_OSsCodigo: TIntegerField;
    Qr014A_OSsEntidade: TIntegerField;
    Qr014A_OSsSiapTerCad: TIntegerField;
    Qr014A_OSsEstatus: TIntegerField;
    Qr014A_OSsDtaExeFim: TDateTimeField;
    Qr014A_OSsNumContrat: TIntegerField;
    Qr014A_OSsGrupo: TIntegerField;
    Qr014A_OSsNO_FatoGeradr: TWideStringField;
    Qr014A_OSsNO_ESTATUS: TWideStringField;
    Qr014A_OSsNO_SiapTerCad: TWideStringField;
    Qr014A_OSsNO_ENT: TWideStringField;
    frxDs014A_OSs: TfrxDBDataset;
    procedure FormCreate(Sender: TObject);
    procedure Qr014A_PrgLstItsAfterScroll(DataSet: TDataSet);
    procedure Qr014GruposAfterOpen(DataSet: TDataSet);
    procedure frxReport014AGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    F_RMIP_014Atr: String;
    //
    function  ObtemNomeArr(Nome: String; Index: Integer): String;
    procedure Reopen004A_PrgLstIts();
    procedure R014_GeraDados(PrgLstIts, Cliente: Integer; DtaIni, DtaFim:
              TDateTime; Funcoes: Integer);
  public
    { Public declarations }
    FCliente, FAplicacao: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL, FAplic_Tit: String;
    //
    procedure GeraImp_LinhaEvolucaoHistorico((*FmRMIP: TForm;*));
    procedure frxReport000GetValue(frxReport: TfrxReport; const VarName: string; var Value: Variant);
  end;

var
  FmRMIP_R014: TFmRMIP_R014;

implementation

uses Module, ModuleRMIP, DmkDAC_PF, ModuleGeral, CreateBugs;

var
  FSeries: Integer = 0;
  FYCount: Integer = 0;
  FNResp: Boolean;
  FXSource: String;
  FXValues: array of String;
  FYValues: array of array of String;
  FTitles: array of String;
  FCores: array of TColor;

{$R *.dfm}

procedure TFmRMIP_R014.FormCreate(Sender: TObject);
var
  I: Integer;
  Chart1: TfrxChartView;
begin
  F_RMIP_014Atr := UnCreateBugs.RecriaTempTableNovo(ntrtt_RMIP_014Atr,
    DmodG.QrUpdPID1, False);
  //
  //////////// Saber (registrar) as mudancas para outros gr�ficos
  Chart1 := frxReport014A.FindObject('Chart014A') as TfrxChartView;
  Chart1.Chart.LeftAxis.Axis.Color := clSilver;
  Chart1.Chart.LeftAxis.Axis.Width := 1;
  Chart1.Chart.LeftAxis.Grid.Color := clSilver;
  Chart1.Chart.LeftAxis.Grid.Width := 1;
  Chart1.Chart.LeftAxis.Grid.SmallDots := True;
  Chart1.Chart.BottomAxis.Axis.Color := clSilver;
  Chart1.Chart.BottomAxis.Axis.Width := 1;
  Chart1.Chart.BottomAxis.Grid.Color := clSilver;
  Chart1.Chart.BottomAxis.Grid.Width := 1;
  Chart1.Chart.BottomAxis.Grid.SmallDots := True;
  Chart1.Chart.Legend.LegendStyle := lsSeries;  // VCLTee.Chart
  // FIM Chart 1
end;

procedure TFmRMIP_R014.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
var
  Chart1: TfrxChartView;
  //x: TfrxChartSeries;
  x: TfrxSeriesItem;
  //
  YSource: String;
  I, J: Integer;
begin
  if VarName = 'VAR_LINE_TEE_014' then
  begin
    Value := True;
    //
    Chart1 := frxReport.FindObject('Chart014A') as TfrxChartView;
    while Chart1.SeriesData.Count > 0 do
      Chart1.SeriesData[0].Free;
    while Chart1.Chart.SeriesCount > 0 do
      Chart1.Chart.Series[0].Free;
    //
    //Chart1.Chart.Legend.LegendStyle := lsSerie;
    for I := 0 to FSeries - 1 do
    begin
      YSource := '';
      for J := 0 to FYCount - 1 do
      begin
        YSource := YSource + ';' + FYValues[I][J];
      end;
      if Length(YSource) > 0 then
        YSource := Copy(YSource, 2);
      //
      Chart1.AddSeries(TfrxChartSeries.csBar);

      // do VCLTee.Chart.CustomChart:
      TCustomBarSeries(Chart1.Chart.Series[I]).BarPen.Style := psClear; //VCLTee.Series
      TCustomBarSeries(Chart1.Chart.Series[I]).BarPen.Width := 0; //VCLTee.Series
      TCustomBarSeries(Chart1.Chart.Series[I]).Marks.Visible := False; //VCLTee.Series
      Chart1.Chart.Series[I].LegendTitle := FTitles[I];

      // do frxChat
      Chart1.SeriesData[I].DataType := dtFixedData;
      Chart1.SeriesData[I].XSource := FXSource; //'value1; value2; value3';
      Chart1.SeriesData[I].YSource := YSource; //'31.5;28.54;31.58';
      Chart1.SeriesData[I].SortOrder := soNone;
      Chart1.SeriesData[I].TopN := 0;
      Chart1.SeriesData[I].XType := xtText;
    end;
  end;
end;

procedure TFmRMIP_R014.frxReport014AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport014A, VarName, Value)
end;

procedure TFmRMIP_R014.GeraImp_LinhaEvolucaoHistorico();
const
  OSCab = 0;
  Sim = 1;
  //
var
  Aviso: String;
  I, J, PrgLstIts: Integer;
  Qryes: array of TmySQLQuery;
  //Chart1: TfrxChartView;
  //
begin
  Qr014A_PrgLstIts.Close;
  Aviso := ' ';
  Reopen004A_PrgLstIts();
  //
  if Qr014A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso :=
      'Question�rios dos monitoramentos pesquisados sem perguntas geradoras do gr�fico "' +
      FAplic_Tit + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Per�odo: ' + Geral.FDT(FDtaIni, 3) + ' a ' + Geral.FDT(FDtaFim, 2);
  //
  frxReport014A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport014A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport014A.Variables['VARF_DATA']    := FDtaImp;
  frxReport014A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport014A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  //
  MyObjects.frxDefineDataSets(frxReport014A, [
  frxDs014A_PrgLstIts,
  frxDs014A_OSs
  ]);
  //MyObjects.frxPrepara(frxReport014A, FAplic_Tit);
end;

function TFmRMIP_R014.ObtemNomeArr(Nome: String; Index: Integer): String;
begin
  Result := Nome + FormatFloat('00', Index);
end;

procedure TFmRMIP_R014.Qr014GruposAfterOpen(DataSet: TDataSet);
  procedure DefineDadosInformados(Item: Integer);
  begin
    Qr014Itens.First;
    while not Qr014Itens.Eof do
    begin
      if Qr014XVals.Locate('DIAS', Qr014ItensDIAS.Value, []) then
      begin
        FYValues[Item][Qr014XVals.RecNo - 1] := Geral.FFI(Qr014ItensITENS.Value);
      end else
      begin
        raise Exception.Create('Erro: Data n�o localizada (1)!');
      end;
      //
      Qr014Itens.Next;
    end;
  end;
var
  I, J, Item: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr014XVals, DModG.MyPID_DB, [
  'SELECT DISTINCT DtaExeFim, ',
  'TO_DAYS(DtaExeFim) DIAS ',
  'FROM ' + F_RMIP_014Atr,
  'ORDER BY DtaExeFim ',
  '']);
  FYCount := Qr014XVals.RecordCount;
  SetLength(FXValues, FYCount);
  Qr014XVals.First;
  FXSource := '';
  while not Qr014XVals.Eof do
  begin
    FXSource := FXSource + ';' + Geral.FDT(Qr014XValsDtaExeFim.Value, 3);
    FXValues[Qr014XVals.RecNo - 1] := Geral.FDT(Qr014XValsDtaExeFim.Value, 3);
    //
    Qr014XVals.Next;
  end;
  if Length(FXSource) > 1 then
    FXSource := Copy(FXSource, 2);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr014NResp, DModG.MyPID_DB, [
  'SELECT RespAtrIts ',
  'FROM ' + F_RMIP_014Atr,
  'WHERE Respondido = 0 ',
  '']);
  FNResp := Qr014NResp.RecordCount > 0;
  if FNResp then
    FSeries := 1
  else
    FSeries := 0;
  FSeries := FSeries + Qr014Grupos.RecordCount;
  SetLength(FYValues, FSeries);
  SetLength(FTitles, FSeries);
  SetLength(FCores, FSeries);
  for I := 0 to FSeries - 1 do
  begin
    SetLength(FYValues[I], FYCount);
    for J := 0 to FYCount - 1 do
      FYValues[I][J] := '0';
  end;
  Item := -1;
///////////////////////// Nao respondidos
  if FNResp then
  begin
    Item := Item + 1;
    FTitles[Item] := CO_NAO_RESPONDIDO;
    FCores[Item]  := 8421504; // Cinza
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr014Itens, DModG.MyPID_DB, [
    'SELECT DtaExeFim, SUM(ITENS) + 0.000 ITENS, ',
    'TO_DAYS(DtaExeFim) DIAS ',
    'FROM ' + F_RMIP_014Atr,
    'WHERE Respondido = 0 ',
    'GROUP BY DtaExeFim ',
    'ORDER BY DtaExeFim ',
    '']);
    DefineDadosInformados(Item);
  end;

///////////////////////// Respondidos
  Qr014Grupos.First;
  while not Qr014Grupos.Eof do
  begin
    Item := Item +1;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qr014Dados, DmodG.MyPID_DB, [
    'SELECT Resposta, CorPizza ',
    'FROM ' + F_RMIP_014Atr,
    'WHERE Respondido = 1 ',
    'AND Resposta="' + Qr014GruposResposta.Value + '" ',
    '']);

    FTitles[Item] := Qr014DadosResposta.Value;
    FCores[Item] := Qr014DadosCorPizza.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr014Itens, DModG.MyPID_DB, [
    'SELECT DtaExeFim, SUM(ITENS) + 0.000 ITENS, ',
    'TO_DAYS(DtaExeFim) DIAS ',
    'FROM ' + F_RMIP_014Atr,
    'WHERE Respondido = 1 ',
    'AND Resposta="' + Qr014GruposResposta.Value + '" ',
    'GROUP BY DtaExeFim ',
    'ORDER BY DtaExeFim ',
    '']);
    DefineDadosInformados(Item);
    //
    Qr014Grupos.Next;
  end;
end;

procedure TFmRMIP_R014.Qr014A_PrgLstItsAfterScroll(DataSet: TDataSet);
const
  Sim = 1;
var
  PrgLstIts, Funcoes: Integer;
begin
  PrgLstIts := Qr014A_PrgLstIts.FieldByName('PrgLstIts').AsInteger;
  Funcoes   := Qr014A_PrgLstIts.FieldByName('Funcoes').AsInteger;
  R014_GeraDados(PrgLstIts, FCliente, FDtaIni, FDtaFim, Funcoes);
  //
  UnDMkDAC_PF.AbreMySQLQuery0(Qr014Grupos, DmodG.MyPID_DB, [
  'SELECT DISTINCT Resposta ',
  'FROM ' + F_RMIP_014Atr,
  'WHERE Respondido=' + Geral.FF0(Sim),
  '']);
end;

procedure TFmRMIP_R014.R014_GeraDados(PrgLstIts, Cliente: Integer; DtaIni,
  DtaFim: TDateTime; Funcoes: Integer);
var
  SQL_Exec, SQL_Extra, SQL_Qtde, SQLPadrao, FldAgrup,
  Resposta, CorPizza, LeftJoin, GroupBy, OrderBy: String;
begin
  if Cliente <> 0 then
    SQL_Extra := 'AND cab.Entidade=' + Geral.FF0(Cliente)
  else
    SQL_Extra := '';
  //
  SQL_Extra := Geral.ATS([
    SQL_Extra,
    dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True)
  ]);
  if not DmRMIP.SQL_Funcoes(PrgLstIts, Funcoes, Resposta, CorPizza, LeftJoin,
  OrderBy, GroupBy, False) then
    Exit;
  SQL_Exec := Geral.ATS([
  'DELETE FROM ' + F_RMIP_014Atr + '; ',
  'INSERT INTO ' + F_RMIP_014Atr,
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  Resposta,
  CorPizza,
  ', 1 Ativo ',
  LeftJoin,
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  GroupBy,
  '']);
  //if Funcoes = CO_PRG_LST_BINARIO then
    //Geral.MB_Info(SQL_Exec);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL_Exec);

  // OSs afetadas:
  DmRMIP.ReopenOSsAfetadas(Qr014A_OSs, PrgLstIts, LeftJoin, SQL_Extra);
end;

procedure TFmRMIP_R014.Reopen004A_PrgLstIts;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr014A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(FAplicacao),
  dmkPF.SQL_Periodo('AND cab.DtaExeFim', FDtaIni, FDtaFim, True, True),
  Geral.ATS_IF(FCliente <> 0, [
  'AND cab.Entidade=' + Geral.FF0(FCliente)]),
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
end;

end.
