unit OSFlhGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, frxClass, frxDBSet,
  Vcl.Menus, UnBugs_Tabs, UnDmkEnums, UnProjGroup_Consts, dmkDBGridZTO,
  dmkCompoStore, dmkPermissoes, System.Variants;

type
  TFmOSFlhGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBEtapa1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaTitulo1C: TLabel;
    QrFrmFil: TmySQLQuery;
    DBGrid1: TdmkDBGridZTO;
    DsFrmFil: TDataSource;
    QrFFLE: TmySQLQuery;
    QrFFII: TmySQLQuery;
    QrFFIICodigo: TIntegerField;
    QrFFIIControle: TIntegerField;
    QrFFIIConta: TIntegerField;
    QrFFIIIDIts: TIntegerField;
    QrFFIIIDIt2: TIntegerField;
    QrFFIIOrdem: TIntegerField;
    QrFFIIDias: TIntegerField;
    QrFFIIEmisUltDta: TDateField;
    QrFFIIFormula: TIntegerField;
    QrFFIIPeriodd: TIntegerField;
    QrFFIIDdPostero: TIntegerField;
    QrFFIINumContrat: TIntegerField;
    QrFFIIDtaExeFim: TDateTimeField;
    QrFFIIDtaCntrFim: TDateField;
    QrCtrIndf: TmySQLQuery;
    QrCtrIndfCONTRATO: TIntegerField;
    QrFFLECodigo: TIntegerField;
    QrFFLEControle: TIntegerField;
    QrFFLEConta: TIntegerField;
    QrFFLEIDIts: TIntegerField;
    QrFFLEEmisUltDta: TDateField;
    QrFFLEFormula: TIntegerField;
    QrFFLEPeriodd: TIntegerField;
    QrFFLEDdPostero: TIntegerField;
    QrFFLENumContrat: TIntegerField;
    QrFFLEDtaExeFim: TDateTimeField;
    QrFFLEDtaCntrFim: TDateField;
    QrFFIIEntidade: TIntegerField;
    QrFFIISiapTerCad: TIntegerField;
    QrFFLEEntidade: TIntegerField;
    QrFFLESiapTerCad: TIntegerField;
    QrLocais: TmySQLQuery;
    QrLocaisCliente: TIntegerField;
    QrLocaisSiapTerCad: TIntegerField;
    QrDatas: TmySQLQuery;
    QrDatasCodigo: TIntegerField;
    QrDatasControle: TIntegerField;
    QrDatasConta: TIntegerField;
    QrDatasIDIts: TIntegerField;
    QrDatasIDIt2: TIntegerField;
    QrDatasOrdem: TIntegerField;
    QrDatasDias: TIntegerField;
    QrDatasEmisUltDta: TDateField;
    QrDatasFormula: TIntegerField;
    QrDatasPeriodd: TIntegerField;
    QrDatasDdPostero: TIntegerField;
    QrDatasNumContrat: TIntegerField;
    QrDatasDtaExeFim: TDateField;
    QrDatasDtaCntrFim: TDateField;
    QrDatasDtaOSCalc: TDateField;
    QrDatasDtaOSReal: TDateField;
    QrDatasCliente: TIntegerField;
    QrDatasSiapTerCad: TIntegerField;
    QrDatasAtivo: TSmallintField;
    QrDatasIDItZ: TIntegerField;
    QrFFLEEmisStatus: TIntegerField;
    QrFFIIEmisStatus: TIntegerField;
    GBBotoes1: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida1: TBitBtn;
    Panel1: TPanel;
    BtPrepara1: TBitBtn;
    GBEtapa2: TGroupBox;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    BtSaida2: TBitBtn;
    Panel6: TPanel;
    BtImprime2: TBitBtn;
    DBGrid2: TDBGrid;
    QrMoniNewOSs: TmySQLQuery;
    QrMoniNewOSsNO_CLIENTE: TWideStringField;
    QrMoniNewOSsNO_EmisStatus: TWideStringField;
    QrMoniNewOSsNO_STC: TWideStringField;
    QrMoniNewOSsDtaPrxRenw: TDateField;
    QrMoniNewOSsCodigo: TIntegerField;
    QrMoniNewOSsControle: TIntegerField;
    QrMoniNewOSsConta: TIntegerField;
    QrMoniNewOSsIDIts: TIntegerField;
    QrMoniNewOSsIDIt2: TIntegerField;
    QrMoniNewOSsIDItZ: TIntegerField;
    QrMoniNewOSsOrdem: TIntegerField;
    QrMoniNewOSsDias: TIntegerField;
    QrMoniNewOSsEmisUltDta: TDateField;
    QrMoniNewOSsFormula: TIntegerField;
    QrMoniNewOSsPeriodd: TIntegerField;
    QrMoniNewOSsDdPostero: TIntegerField;
    QrMoniNewOSsNumContrat: TIntegerField;
    QrMoniNewOSsDtaExeFim: TDateField;
    QrMoniNewOSsDtaCntrFim: TDateField;
    QrMoniNewOSsDtaOSCalc: TDateField;
    QrMoniNewOSsDtaOSReal: TDateField;
    QrMoniNewOSsCliente: TIntegerField;
    QrMoniNewOSsSiapTerCad: TIntegerField;
    QrMoniNewOSsEmisStatus: TIntegerField;
    QrMoniNewOSsGrupoOS: TIntegerField;
    QrMoniNewOSsAtivo: TSmallintField;
    DsMoniNewOSs: TDataSource;
    frxGER_OSERV_030_002_A: TfrxReport;
    frxDsMoniNewOSs: TfrxDBDataset;
    QrDatasDtaOSUtil: TDateField;
    QrMoniNewOSsDtaOSUtil: TDateField;
    QrMoniNewOSsDiaSemTxt: TWideStringField;
    BtGera: TBitBtn;
    QrFFIIEmpresa: TIntegerField;
    QrFFLEEmpresa: TIntegerField;
    QrLocaisEmpresa: TIntegerField;
    QrMoniNewOSsEmpresa: TIntegerField;
    QrOSCab: TmySQLQuery;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    QrOSCabEntContrat: TIntegerField;
    QrOSCabCondicaoPg: TIntegerField;
    QrOSCabCartEmis: TIntegerField;
    QrOSSrv: TmySQLQuery;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvGarantiaDd: TIntegerField;
    QrOSSrvHrEvacuar: TIntegerField;
    QrOSSrvHrExecutar: TFloatField;
    QrOSSrvValCalc: TFloatField;
    QrOSSrvValInfo: TFloatField;
    QrOSSrvValDesc: TFloatField;
    QrOSSrvValTota: TFloatField;
    QrOSSrvAutorizado: TSmallintField;
    QrOSSrvDetalhes: TWideMemoField;
    QrOSSrvTudoFeitoM: TSmallintField;
    QrOSSrvTudoFeitoA: TSmallintField;
    QrOSSrvHowEstav: TSmallintField;
    QrOSSrvHowOrige: TIntegerField;
    QrOSSrvMoniDdTotl: TIntegerField;
    QrOSSrvMoniDdIntv: TSmallintField;
    QrSNOS: TmySQLQuery;
    QrSNOSGrupoOS: TIntegerField;
    QrMoniNewOSsSeqNovaOS: TIntegerField;
    QrNovasOSs: TmySQLQuery;
    QrNovasOSsSeqNovaOS: TIntegerField;
    QrFFIIDesServico: TIntegerField;
    QrFFLEDesServico: TIntegerField;
    QrOSCabEmpresa: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabCodigo: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrNovosSrv: TmySQLQuery;
    QrFFIIHrEvacuar: TIntegerField;
    QrFFLEHrEvacuar: TIntegerField;
    QrFFIIHrExecutar: TFloatField;
    QrFFLEHrExecutar: TFloatField;
    QrNovosSrvControle: TIntegerField;
    QrOSAlv: TmySQLQuery;
    QrOSAlvPraga_Z: TIntegerField;
    QrFrmFilNO_CLIENTE: TWideStringField;
    QrFrmFilNO_EmisStatus: TWideStringField;
    QrFrmFilNO_STC: TWideStringField;
    QrFrmFilNO_FORMULA: TWideStringField;
    QrFrmFilEntidade: TIntegerField;
    QrFrmFilSiapTerCad: TIntegerField;
    QrFrmFilDtaExeFim: TDateTimeField;
    QrFrmFilNumContrat: TIntegerField;
    QrFrmFilDtaPrxRenw: TDateField;
    QrFrmFilTabela: TLargeintField;
    QrFrmFilIDSuperior: TIntegerField;
    QrFrmFilEmisUltDta: TDateField;
    QrFrmFilPeriodd: TIntegerField;
    QrFrmFilEmisStatus: TSmallintField;
    QrFrmFilSIGLA_TAB: TWideStringField;
    QrFMOI: TmySQLQuery;
    QrFMOICodigo: TIntegerField;
    QrFMOIControle: TIntegerField;
    QrFMOIConta: TIntegerField;
    QrFMOIOrdem: TIntegerField;
    QrFMOIDias: TIntegerField;
    QrFMOIEmisUltDta: TDateField;
    QrFMOIPrgLstCab: TIntegerField;
    QrFMOIPeriodd: TIntegerField;
    QrFMOIDdPostero: TIntegerField;
    QrFMOIEmisStatus: TSmallintField;
    QrFMOINumContrat: TIntegerField;
    QrFMOIDtaExeFim: TDateTimeField;
    QrFMOIEntidade: TIntegerField;
    QrFMOISiapTerCad: TIntegerField;
    QrFMOIEmpresa: TIntegerField;
    QrFMOIDesServico: TIntegerField;
    QrFMOIHrEvacuar: TIntegerField;
    QrFMOIHrExecutar: TFloatField;
    QrFMOIDtaCntrFim: TDateField;
    QrFrmFilCodigo: TIntegerField;
    QrFrmFilControle: TIntegerField;
    QrFrmFilConta: TIntegerField;
    QrFMOIIDIts: TIntegerField;
    QrFMOE: TmySQLQuery;
    QrFMOECodigo: TIntegerField;
    QrFMOEControle: TIntegerField;
    QrFMOEConta: TIntegerField;
    QrFMOEEmisUltDta: TDateField;
    QrFMOEFormula: TIntegerField;
    QrFMOEPeriodd: TIntegerField;
    QrFMOEDdPostero: TIntegerField;
    QrFMOEEmisStatus: TSmallintField;
    QrFMOENumContrat: TIntegerField;
    QrFMOEDtaExeFim: TDateTimeField;
    QrFMOEEntidade: TIntegerField;
    QrFMOESiapTerCad: TIntegerField;
    QrFMOEEmpresa: TIntegerField;
    QrFMOEDesServico: TIntegerField;
    QrFMOEHrEvacuar: TIntegerField;
    QrFMOEHrExecutar: TFloatField;
    QrFMOEDtaCntrFim: TDateField;
    QrFMOEPrgLstCab: TIntegerField;
    QrFFIINO_FORMULA: TWideStringField;
    QrFMOINO_PrgLstCab: TWideStringField;
    QrFFLENO_FORMULA: TWideStringField;
    QrFMOENO_PrgLstCab: TWideStringField;
    QrMoniNewOSsTitItemTab: TWideStringField;
    QrMoniNewOSsTitTipoTab: TWideStringField;
    QrOper: TmySQLQuery;
    QrOperTabela: TIntegerField;
    QrNewF_M: TmySQLQuery;
    QrNewF_MTabela: TIntegerField;
    QrNewF_MCodigo: TIntegerField;
    QrNewF_MControle: TIntegerField;
    QrNewF_MConta: TIntegerField;
    QrNewF_MIDIts: TIntegerField;
    QrNewF_MIDIt2: TIntegerField;
    QrNewF_MIDItZ: TIntegerField;
    QrNewF_MOrdem: TIntegerField;
    QrNewF_MDias: TIntegerField;
    QrNewF_MEmisUltDta: TDateField;
    QrNewF_MDesServico: TIntegerField;
    QrNewF_MHrEvacuar: TIntegerField;
    QrNewF_MHrExecutar: TFloatField;
    QrNewF_MFormula: TIntegerField;
    QrNewF_MPrgLstCab: TIntegerField;
    QrNewF_MPeriodd: TIntegerField;
    QrNewF_MDdPostero: TIntegerField;
    QrNewF_MNumContrat: TIntegerField;
    QrNewF_MDtaExeFim: TDateField;
    QrNewF_MDtaCntrFim: TDateField;
    QrNewF_MDtaOSCalc: TDateField;
    QrNewF_MDtaOSUtil: TDateField;
    QrNewF_MDtaOSReal: TDateField;
    QrNewF_MEmpresa: TIntegerField;
    QrNewF_MCliente: TIntegerField;
    QrNewF_MSiapTerCad: TIntegerField;
    QrNewF_MEmisStatus: TIntegerField;
    QrNewF_MGrupoOS: TIntegerField;
    QrNewF_MSeqNovaOS: TIntegerField;
    QrNewF_MDiaSemTxt: TWideStringField;
    QrNewF_MTitTipoTab: TWideStringField;
    QrNewF_MTitItemTab: TWideStringField;
    QrNewF_MAtivo: TSmallintField;
    QrFormula: TmySQLQuery;
    QrFormulaCodigo: TIntegerField;
    QrFormulaNome: TWideStringField;
    QrFormulaLk: TIntegerField;
    QrFormulaDataCad: TDateField;
    QrFormulaDataAlt: TDateField;
    QrFormulaUserCad: TIntegerField;
    QrFormulaUserAlt: TIntegerField;
    QrFormulaAlterWeb: TSmallintField;
    QrFormulaAtivo: TSmallintField;
    QrFormulaEquipAplic: TIntegerField;
    QrFormulaQtdTot: TFloatField;
    QrFormulaQtdQSP: TFloatField;
    QrFormulaCusTot: TFloatField;
    QrFormulaAplicacao: TIntegerField;
    QrFormulaDiluente: TSmallintField;
    QrFormulaTipoAplica: TIntegerField;
    QrFormulaUnidMed: TIntegerField;
    QrOSFrmDep: TmySQLQuery;
    QrOSFrmDepCodigo: TIntegerField;
    QrOSFrmDepControle: TIntegerField;
    QrOSFrmDepConta: TIntegerField;
    QrOSFrmDepIDIts: TIntegerField;
    QrOSFrmDepTabela: TSmallintField;
    QrOSFrmDepCadastro: TIntegerField;
    QrOSFrmDepLk: TIntegerField;
    QrOSFrmDepDataCad: TDateField;
    QrOSFrmDepDataAlt: TDateField;
    QrOSFrmDepUserCad: TIntegerField;
    QrOSFrmDepUserAlt: TIntegerField;
    QrOSFrmDepAlterWeb: TSmallintField;
    QrOSFrmDepAtivo: TSmallintField;
    QrOSAge: TmySQLQuery;
    QrOSAgeCodigo: TIntegerField;
    QrOSAgeControle: TIntegerField;
    QrOSAgeAgente: TIntegerField;
    QrOSAgeResponsa: TSmallintField;
    QrOSAgeLk: TIntegerField;
    QrOSAgeDataCad: TDateField;
    QrOSAgeDataAlt: TDateField;
    QrOSAgeUserCad: TIntegerField;
    QrOSAgeUserAlt: TIntegerField;
    QrOSAgeAlterWeb: TSmallintField;
    QrOSAgeAtivo: TSmallintField;
    QrOSCabMulServico: TLargeintField;
    QrOSCabAgeEqiCab: TIntegerField;
    GBEtapa3: TGroupBox;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    BtSaida3: TBitBtn;
    Panel8: TPanel;
    BitBtn2: TBitBtn;
    DBGrid3: TDBGrid;
    DsNovasOSs: TDataSource;
    PB1: TProgressBar;
    QrLocPip: TmySQLQuery;
    QrLocPipCodigo: TIntegerField;
    QrLocPipPrgLstCab: TIntegerField;
    QrDtaIndf: TmySQLQuery;
    _QrNovasOSs_: TmySQLQuery;
    DateField1: TDateField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    QrOSs: TmySQLQuery;
    QrOSsDtaOSReal: TDateField;
    QrOSsCodigo: TIntegerField;
    QrSiapTerFlh: TmySQLQuery;
    QrSiapTerFlhCodigo: TIntegerField;
    QrSiapTerFlhControle: TIntegerField;
    QrSiapTerFlhEmpresa: TIntegerField;
    QrSiapTerFlhMonAgeEqi: TIntegerField;
    QrSiapTerFlhMonEntCtt: TIntegerField;
    QrSiapTerFlhMonNumCtr: TIntegerField;
    QrSiapTerFlhMonEntCtr: TIntegerField;
    QrSiapTerFlhMonEntPag: TIntegerField;
    QrSiapTerFlhMonCondPg: TIntegerField;
    QrSiapTerFlhMonCrtEmi: TIntegerField;
    QrNovosSrvDdPostero: TIntegerField;
    QrLocPipNome: TWideStringField;
    QrLocPipEquipamento: TIntegerField;
    QrLocPipOSMonCab: TIntegerField;
    QrLocPipMotDesativ: TIntegerField;
    QrLocPipDtaAquis: TDateField;
    QrLocPipDtaDesativ: TDateField;
    QrLocPipDependenci: TIntegerField;
    QrDistin: TmySQLQuery;
    QrDistinSubTab: TIntegerField;
    QrAtzSrc: TmySQLQuery;
    QrLastAd: TmySQLQuery;
    QrLastAdDtaExePrv: TDateTimeField;
    QrAtzSrcConta: TIntegerField;
    QrErrOMC: TmySQLQuery;
    frxGER_OSERV_030_002_B: TfrxReport;
    frxDsErrOMC: TfrxDBDataset;
    QrErrOMCCodigo: TIntegerField;
    QrErrOMCControle: TIntegerField;
    QrErrOMCConta: TIntegerField;
    QrErrOMCNome: TWideStringField;
    frxGER_OSERV_030_001_A: TfrxReport;
    frxDsFrmFil: TfrxDBDataset;
    QrFrmFilCO_FORMULA: TIntegerField;
    QrFrmFilDdPostero: TIntegerField;
    BtImprime1: TBitBtn;
    QrIncompl: TmySQLQuery;
    QrIncomplCodigo: TIntegerField;
    QrIncomplControle: TIntegerField;
    QrIncomplConta: TIntegerField;
    QrIncomplDdPostero: TIntegerField;
    QrIncomplDtaExeFim: TDateTimeField;
    QrIncomplNumContrat: TIntegerField;
    QrIncomplDtaPrxRenw: TDateField;
    QrIncomplIDSuperior: TIntegerField;
    QrIncomplEmisUltDta: TDateField;
    QrIncomplPeriodd: TIntegerField;
    QrIncomplEmisStatus: TSmallintField;
    QrIncomplNO_TAB: TWideStringField;
    frxGER_OSERV_030_002_C: TfrxReport;
    frxDsIncompl: TfrxDBDataset;
    QrIncomplFaltaPip: TWideStringField;
    QrIncomplTabela: TLargeintField;
    PMGrid1: TPopupMenu;
    Cliente1: TMenuItem;
    OS1: TMenuItem;
    QrFrmFilEmpresa: TIntegerField;
    QrSiapTerFlhDataSincOS: TDateField;
    QrDtaIndfDataSincOS: TDateField;
    QrSTCsemEMP: TmySQLQuery;
    frxDsSTCsemEMP: TfrxDBDataset;
    frxGER_OSERV_030_001_B: TfrxReport;
    QrSTCsemEMPSiapTerCad: TIntegerField;
    QrSTCsemEMPEmpresa: TIntegerField;
    QrSTCsemEMPAtivo: TSmallintField;
    QrSTCsemEMPNome: TWideStringField;
    QrSTCsemEMPCliente: TIntegerField;
    QrSTCsemEMPNO_CLIENTE: TWideStringField;
    QrLocPipOrdem: TIntegerField;
    QrNaoRatif: TmySQLQuery;
    frxGER_OSERV_030_001_C: TfrxReport;
    frxDsNaoRatif: TfrxDBDataset;
    QrNaoRatifCodigo: TIntegerField;
    QrNaoRatifSiapterCad: TIntegerField;
    QrNaoRatifNO_STC: TWideStringField;
    QrNaoRatifEntidade: TIntegerField;
    QrNaoRatifNO_CLI: TWideStringField;
    QrOSCabPdrMntsMon: TIntegerField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrOSCabAlv: TmySQLQuery;
    QrOSCabAlvCodigo: TIntegerField;
    QrOSCabAlvControle: TIntegerField;
    QrOSCabAlvOrdem: TSmallintField;
    QrOSCabAlvLk: TIntegerField;
    QrOSCabAlvDataCad: TDateField;
    QrOSCabAlvDataAlt: TDateField;
    QrOSCabAlvUserCad: TIntegerField;
    QrOSCabAlvUserAlt: TIntegerField;
    QrOSCabAlvAlterWeb: TSmallintField;
    QrOSCabAlvAtivo: TSmallintField;
    QrOSCabAlvPraga_A: TIntegerField;
    QrOSCabAlvPraga_Z: TIntegerField;
    QrOSCabAlvNO_NIVEL: TWideStringField;
    QrOSCabAlvNO_PRAGA: TWideStringField;
    QrLocPipPerioDd: TIntegerField;
    QrLocPipDdPostero: TIntegerField;
    QrSiapTerFlhMinHAgeExe: TTimeField;
    QrOSsMinHAgeExe: TTimeField;
    QrOSsDtInicial: TDateField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrPIP: TmySQLQuery;
    QrPIPOSMonCab: TIntegerField;
    QrPIPCodigo: TIntegerField;
    QrPIPOrdem: TIntegerField;
    QrPIPPrgLstCab: TIntegerField;
    QrFrmFilExtenDd: TIntegerField;
    QrFFIIExtenDD: TIntegerField;
    QrFMOIExtenDd: TIntegerField;
    QrFFLEExtenDd: TIntegerField;
    QrFMOEExtenDd: TIntegerField;
    QrIncomplExtenDd: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    QrFrmFilOpcao: TIntegerField;
    QrFrmFilGrupo: TIntegerField;
    QrFrmFilDataSincOS: TDateField;
    N1: TMenuItem;
    AlteradatadaltimaemissodeOSsfilhas1: TMenuItem;
    QrOSCabMobiliCad: TIntegerField;
    LaTotal: TLabel;
    procedure BtSaida1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPrepara1Click(Sender: TObject);
    procedure QrFrmFilEmisUltDtaGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure BtImprime2Click(Sender: TObject);
    procedure frxGER_OSERV_030_002_AGetValue(const VarName: string;
      var Value: Variant);
    procedure BtGeraClick(Sender: TObject);
    procedure QrNovasOSsAfterOpen(DataSet: TDataSet);
    procedure QrMoniNewOSsEmisUltDtaGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure BtImprime1Click(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtSaida3Click(Sender: TObject);
    procedure BtSaida2Click(Sender: TObject);
    procedure Cliente1Click(Sender: TObject);
    procedure OS1Click(Sender: TObject);
    procedure PMGrid1Popup(Sender: TObject);
    procedure AlteradatadaltimaemissodeOSsfilhas1Click(Sender: TObject);
    procedure QrFrmFilAfterScroll(DataSet: TDataSet);
    procedure QrFrmFilBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FTotalOSs, F_MoniNewOSs, F_STC_sem_Emp, FTextoCriacao, FTxtSrv, FTxtF_M: String;
    FIDItZ, FGrupoOS: Integer;
    //
    //function  ClientesSemDataInicial(): Boolean;
    function  ClientesSemDataInicial2(): Boolean;
    function  ConsumosNaoRatificados(): Boolean;
    function  ContratosIndefinidos(): Boolean;
    procedure CriaItensLugarAtual();

    function  DefineAgeEqiCab(): Integer;
    function  DefineEntiContat(): Integer;
    function  DefineEntContrat(): Integer;
    function  DefineEntPagante(): Integer;
    function  DefineNumContrat(): Integer;
    function  DefineCondicaoPg(): Integer;
    function  DefineCartEmiss(): Integer;
    function  DefinePrePragas(): Integer;

    procedure DefineSequenciaDeNovasOSs();
    //
    //function  GeraOSCab_A(var Codigo: Integer): Boolean;
    function  GeraOSCab_B(var Codigo: Integer; const OSFlhGrCab, OSFlhGrIts:
              Integer): Boolean;
    function  GeraOSCabAlv(Codigo: Integer): Boolean;
    function  GeraOSAlv(Codigo, Controle: Integer): Boolean;
    function  GeraOSFrmCab(const Codigo, Controle: Integer; var Conta:
              Integer): Boolean;
    function  GeraOSMonCab(Codigo: Integer; DEP: TDateTime): Boolean;
    function  GeraOSSrv(const Codigo: Integer; var Controle: Integer): Boolean;
    //
    procedure MsgFaltaDef(DescriCampo: String);

    procedure MostraEtapa2();
    procedure MostraEtapa3();
    function  ObtemConjuntoDeOperacoesDeItens(): Integer;
    procedure OrganizaRotas();
    //function  OSMonCab_Incompletas(): Boolean;
    function  OSxxx_Incompletas(): Boolean;
    //
    function  ReopenDatas(Empresa, Cliente, Lugar: Integer): Boolean;
    procedure ReopenMoniNewOSs();
    procedure ReopenFrmFil(IDSuperior: Integer = 0; Tabela: LargeInt = 0);
    //
    procedure ReopenOSAge(Codigo: Integer);
    procedure ReopenOSCab(Codigo: Integer);
    procedure ReopenOSSrv(Controle: Integer);
    procedure ReopenOSAlv(Controle: Integer);
    procedure ReopenSiapTerFlh(SiapterCad, Empresa: Integer);
    //
    procedure VerificaSeEhDiaUtil();
    procedure FechaForm;
  public
    { Public declarations }
  end;

  var
  FmOSFlhGer: TFmOSFlhGer;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc, UnAppListas, CreateBugs,
  {$IfNDef SNoti} UnitNotificacoesEdit, {$EndIf}
  ModuleGeral, UMySQLModule, UnOSApp_PF, ModOS, Principal, ModAgenda, MyGlyfs;

{$R *.DFM}

{
procedure TFmOSFlhGer.BtGeraClick(Sender: TObject);
var
  CodOri, Codigo, Controle, Conta, NovaOS: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    MostraEtapa3();
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Agrupando itens por Sequ�ncia e data');
    Update;
    Application.ProcessMessages;
    //Codigo   := 0;
    //Controle := 0;
    NovaOS  := -1;
    // Parei aqui! Tirar c�digo?
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNovasOSs, DModG.MyPID_DB, [
    'SELECT DISTINCT DtaOSReal, SeqNovaOS, Codigo ',
    'FROM ' + F_MoniNewOSs,
    'ORDER BY DtaOSReal, SeqNovaOS, Codigo ',
    '']);
    PB1.Position := 0;
    PB1.Max := QrNovasOSs.RecordCount;
    QrNovasOSs.First;
    while not QrNovasOSs.Eof do
    begin
      Codigo := 0;
      CodOri := QrNovasOSsCodigo.Value;
      //
      FTextoCriacao := 'Gerando: OS sequencial ' +
        Geral.FF0(QrNovasOSs.RecNo) + ' de ' + FTotalOSs + ' Data: ' +
        Geral.FDT(QrNovasOSsDtaOSReal.Value, 109) + '. ';
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTextoCriacao);
      //
      if GeraOSCab_A(Codigo) then
      begin
        if GeraOSAge_A(CodOri, Codigo) then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao +
          'Preparando servi�os da sequ�ncia');
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QrNovosSrv, DModG.MyPID_DB, [
          //'SELECT DISTINCT DesServico, DdPostero, HrEvacuar, HrExecutar ',
          // O certo n�o seria pelo DesServico ?
          'SELECT DISTINCT Controle, DdPostero ',
          'FROM ' + F_MoniNewOSs,
          'WHERE SeqNovaOS=' + Geral.FF0(QrNovasOSsSeqNovaOS.Value),
          'AND Codigo=' + Geral.FF0(QrNovasOSsCodigo.Value),
          'AND DtaOSReal="' + Geral.FDT(QrNovasOSsDtaOSReal.Value, 1) + '"',
          '']);
          QrNovosSrv.First;
          while not QrNovosSrv.Eof do
          begin
            Controle := 0;
            if GeraOSSrv(Codigo, Controle) then
            begin
              if GeraOSAlv(Codigo, Controle) then
              //if
              Conta := 0;
              GeraOSFrmCab(Codigo, Controle, Conta);// then
              Conta := 0;
              // if
              GeraOSMonCab(Codigo, Controle, Conta);// then

              //if GeraOS???(Codigo, Controle, ???) then
              //if GeraOS???(Codigo, Controle, ???) then
              //if GeraOS???(Codigo, Controle, ???) then
              //if GeraOS???(Codigo, Controle, ???) then
              //if GeraOS???(Codigo, Controle, ???) then
              //if GeraOS???(Codigo, Controle, ???) then
            // Parei Aqui!
              //
            end;
            //
            QrNovosSrv.Next;
          end; // Gerou Servi�os
        end; // Gerou Agentes
      end; // Gerou OS
      //
      // Atualiza OS!
      OSApp_PF.TotalValorOS(Codigo);
      OSApp_PF.EquipeDeAgentes(Codigo, nil, nil);
      //
      QrNovasOSs.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmOSFlhGer.AlteradatadaltimaemissodeOSsfilhas1Click(Sender: TObject);
begin
  if QrFrmFilTabela.Value = CO_FORMUL_FILHA_MONIT_EH_FILHA then
  begin
    if OSApp_PF.AlteraDataDaUltimaEmissoDeFormulasFilhas(QrFrmFilEmisUltDta.Value,
      QrFrmFilIDSuperior.Value, istosfrmflhcb)
    then
      ReopenFrmFil(QrFrmFilIDSuperior.Value, QrFrmFilTabela.Value);
  end else
  if QrFrmFilTabela.Value = CO_FORMUL_FILHA_MONIT_EH_MONIT then
  begin
    if OSApp_PF.AlteraDataDaUltimaEmissoDeFormulasFilhas(QrFrmFilEmisUltDta.Value,
      QrFrmFilIDSuperior.Value, istosmoncab)
    then
      ReopenFrmFil(QrFrmFilIDSuperior.Value, QrFrmFilTabela.Value);
  end;
end;

procedure TFmOSFlhGer.BtGeraClick(Sender: TObject);
const
  EmisStatus1 = CO_FORMUL_FILHA_MONIT_EmisStat_PARCIAL; // 1
  EmisStatus2 = CO_FORMUL_FILHA_MONIT_EmisStat_TOTAL;   // 2
var
  (*CodOri,*) Codigo, Controle, Conta, IDIts, NovaOS, OSFlhGrCab: Integer;
  EmisUltDta: String;
  DEP: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  try
    MostraEtapa3();
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Agrupando itens por sequ�ncia');
    Update;
    Application.ProcessMessages;
    //Codigo   := 0;
    //Controle := 0;
    NovaOS  := -1;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNovasOSs, DModG.MyPID_DB, [
    'SELECT DISTINCT SeqNovaOS',
    'FROM ' + F_MoniNewOSs,
    'ORDER BY SeqNovaOS',
    '']);
    PB1.Position := 0;
    PB1.Max := QrNovasOSs.RecordCount;
    //
    OSFlhGrCab := UMyMod.BPGS1I32('oscab', 'OSFlhGrCab', '', '', tsPos, stIns, 0);
    //
    QrNovasOSs.First;
    while not QrNovasOSs.Eof do
    begin
      Codigo := 0;
      //CodOri := QrNovasOSsCodigo.Value;
      FTextoCriacao := 'Gerando: OS sequencial ' + Geral.FF0(QrNovasOSs.RecNo) + '. ';
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTextoCriacao);
      //
      if GeraOSCab_B(Codigo, OSFlhGrCab, QrNovasOSs.RecNo) then
      begin
        DEP := QrOSsDtInicial.Value;
        // Pragas do pr�-atendimento
        GeraOSCabAlv(Codigo);
        // Lista de PIPs
        GeraOSMonCab(Codigo, DEP);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao +
        'Preparando servi�os da sequ�ncia');
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrNovosSrv, DModG.MyPID_DB, [
        //'SELECT DISTINCT DesServico, DdPostero, HrEvacuar, HrExecutar ',
        // O certo n�o seria pelo DesServico ?
        'SELECT DISTINCT Controle, DdPostero ',
        'FROM ' + F_MoniNewOSs,
        'WHERE SeqNovaOS=' + Geral.FF0(QrNovasOSsSeqNovaOS.Value),
        //'AND Codigo=' + Geral.FF0(QrNovasOSsCodigo.Value),
        //'AND DtaOSReal="' + Geral.FDT(QrNovasOSsDtaOSReal.Value, 1) + '"',
        '']);
        QrNovosSrv.First;
        while not QrNovosSrv.Eof do
        begin
          Controle := 0;
          if GeraOSSrv(Codigo, Controle) then
          begin
            if GeraOSAlv(Codigo, Controle) then ;
            //
            Conta := 0;
            GeraOSFrmCab(Codigo, Controle, Conta);// then
          end;
          //
          QrNovosSrv.Next;
        end; // Gerou Servi�os
      end; // Gerou OS
      //
      // Atualiza OS!
      OSApp_PF.TotalValorOS(Codigo);
      // J� fez na cria��o!
      //OSApp_PF.EquipeDeAgentes(Codigo, nil, nil);
      //
      QrNovasOSs.Next;
    end;
    // Atualizar OS m�es informando que as OSs filhas foram criadas
    // F�rmulas de aplica��o
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando f�rmulas m�es de aplica��o');
    UnDmkDAC_PF.AbreMySQLQuery0(QrAtzSrc, Dmod.MyDB, [
    'SELECT DISTINCT frc.OSFFCbMae Conta',
    'FROM osfrmcab frc ',
    'LEFT JOIN oscab cab ON cab.Codigo=frc.Codigo ',
    'WHERE cab.OSFlhGrCab=' + Geral.FF0(OSFlhGrCab),
    'AND frc.OSFFCbMae > 0 ',
    'ORDER BY OSFFCbMae ',
    '']);
    QrAtzSrc.First;
    while not QrAtzSrc.Eof do
    begin
      IDIts := QrAtzSrcConta.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrLastAd, Dmod.MyDB, [
      'SELECT MAX(cab.DtaExePrv) DtaExePrv ',
      'FROM osfrmcab ofc ',
      'LEFT JOIN oscab cab ON cab.Codigo=ofc.Codigo ',
      'WHERE ofc.OSFFCbMae=' + Geral.FF0(IDIts),
      '']);
      //
      EmisUltDta := Geral.FDT(QrLastAdDtaExePrv.Value, 1);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osfrmflhcb', False, [
      'EmisStatus', 'EmisUltDta'], [
      'IDIts'], [
      EmisStatus1, EmisUltDta], [
      IDIts], True);
      //
      QrAtzSrc.Next;
    end;

    // F�rmulas de monitoramento:
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando f�rmulas m�es de monitoramento');
    UnDmkDAC_PF.AbreMySQLQuery0(QrAtzSrc, Dmod.MyDB, [
    'SELECT DISTINCT opm.OSFMCbMae Conta',
    'FROM ospipmon opm ',
    'LEFT JOIN oscab cab ON cab.Codigo=opm.Codigo ',
    'WHERE cab.OSFlhGrCab=' + Geral.FF0(OSFlhGrCab),
    'AND opm.OSFMCbMae > 0 ',
    'ORDER BY OSFMCbMae ',
    '']);
    QrAtzSrc.First;
    while not QrAtzSrc.Eof do
    begin
      Conta := QrAtzSrcConta.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrLastAd, Dmod.MyDB, [
      'SELECT MAX(cab.DtaExePrv) DtaExePrv ',
      'FROM ospipmon opm ',
      'LEFT JOIN oscab cab ON cab.Codigo=opm.Codigo ',
      'WHERE opm.OSFMCbMae=' + Geral.FF0(Conta),
      '']);
      //
      EmisUltDta := Geral.FDT(QrLastAdDtaExePrv.Value, 1);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osmoncab', False, [
      'EmisStatus', 'EmisUltDta'], [
      'Conta'], [
      EmisStatus1, EmisUltDta], [
      Conta], True);
      //
      QrAtzSrc.Next;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Atualiza��o finalizada!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSFlhGer.BtImprime1Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxGER_OSERV_030_001_A, [
  DmodG.frxDsDono,
  frxDsFrmFil
  ]);
  //
  MyObjects.frxMostra(frxGER_OSERV_030_001_A, 'F�rmulas Filhas a Serem Geradas');
end;

procedure TFmOSFlhGer.BtImprime2Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxGER_OSERV_030_002_A, [
  DmodG.frxDsDono,
  frxDsMoniNewOSs
  ]);
  //
  MyObjects.frxMostra(frxGER_OSERV_030_002_A, 'OS autom�ticas');
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
  'Clique em "Gera OSs" para gerar as OSs dos itens listados abaixo!');
end;

procedure TFmOSFlhGer.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(DBGrid1), False);
end;

procedure TFmOSFlhGer.BtPrepara1Click(Sender: TObject);
begin
  if DBGrid1.SelectedRows.Count = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi selecionado!');
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    FIDItZ   := 0;
    FGrupoOS := 0;
    //
    if ContratosIndefinidos() then
      Exit;
    if ClientesSemDataInicial2() then
      Exit;
    // Incluso no "OSxxx_Incompletas()"
    //if OSMonCab_Incompletas() then
      //Exit;
    if ConsumosNaoRatificados() then
      Exit;
    if OSxxx_Incompletas() then
      Exit;
    DBGrid1.Enabled := False;
    F_MoniNewOSs :=
      UnCreateBugs.RecriaTempTableNovo(ntrtt_MoniNewOSs, DmodG.QrUpdPID1, False);
    //
    CriaItensLugarAtual();
    //
    VerificaSeEhDiaUtil();
    //
    OrganizaRotas();
    //
    DefineSequenciaDeNovasOSs();
    //
    MostraEtapa2();
    //
  finally
    DBGrid1.Enabled := True;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSFlhGer.FechaForm();
begin
  if TFmOSFlhGer(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmOSFlhGer.BtSaida1Click(Sender: TObject);
begin
  FechaForm;
end;

procedure TFmOSFlhGer.BtSaida2Click(Sender: TObject);
begin
  FechaForm;
end;

procedure TFmOSFlhGer.BtSaida3Click(Sender: TObject);
begin
  FechaForm;
end;

procedure TFmOSFlhGer.BtTodosClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(DBGrid1), True);
end;

procedure TFmOSFlhGer.Cliente1Click(Sender: TObject);
var
  Aba: Boolean;
begin
  if TFmOSFlhGer(Self).Owner is TApplication then
    Aba := False
  else
    Aba := True;
  //
  FmPrincipal.MostraFormCunsCad(Aba, QrFrmFilEntidade.Value, 0);
  //
  ReopenFrmFil();
end;

function TFmOSFlhGer.ClientesSemDataInicial2(): Boolean;

  procedure MostraCunsCad();
  var
    Aba: Boolean;
  begin
    if TFmOSFlhGer(Self).Owner is TApplication then
      Aba := False
    else
      Aba := True;
    //
    FmPrincipal.MostraFormCunsCad(Aba, QrFrmFilEntidade.Value, 0, False, 5, True);
    //
    ReopenFrmFil();
  end;
var
  Res: Boolean;
begin
  Res := True;
  //
  QrFrmFil.DisableControls;
  try
    QrFrmFil.First;
    while not QrFrmFil.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrDtaIndf, Dmod.MyDB, [
        'SELECT DataSincOS ',
        'FROM siapterflh ',
        'WHERE Codigo=' + Geral.FF0(QrFrmFilSiapTerCad.Value),
        'AND Empresa=' + Geral.FF0(QrFrmFilEmpresa.Value),
        '']);
      //
      if (QrDtaIndf.RecordCount = 0)
        or (QrDtaIndfDataSincOS.Value < 2) then
      begin
        Res := True;
        //
        if Geral.MB_Pergunta('Data de refer�ncia n�o informada para gera��o de OSs autom�ticas!' +
          sLineBreak + 'Deseja informar agora?') = ID_YES
        then
          MostraCunsCad;
        Exit;
      end else
        Res := False;
      //
      QrFrmFil.Next;
    end;
  finally
    QrFrmFil.First;
    QrFrmFil.EnableControls;
    //
    Result := Res;
  end;
end;

(*
function TFmOSFlhGer.ClientesSemDataInicial(): Boolean;
const
  Ativo = 1;
var
  Txt: String;
  Lista: array of array[0..1] of Integer;
  SiapTerCad, Empresa,
  I, K: Integer;
begin
  K := 0;
  QrFrmFil.DisableControls;
  try
    QrFrmFil.First;
    while not QrFrmFil.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrDtaIndf, Dmod.MyDB, [
      'SELECT DataSincOS ',
      'FROM siapterflh ',
      'WHERE Codigo=' + Geral.FF0(QrFrmFilSiapTerCad.Value),
      'AND Empresa=' + Geral.FF0(QrFrmFilEmpresa.Value),
      '']);
      //
      if (QrDtaIndf.RecordCount = 0)
      or (QrDtaIndfDataSincOS.Value < 2) then
      begin
        K := K + 1;
        SetLength(Lista, K);
        Lista[K - 1][0] := QrFrmFilSiapTerCad.Value;
        Lista[K - 1][1] := QrFrmFilEmpresa.Value;
        //
        QrDtaIndf.Next;
      end;
      //
      QrFrmFil.Next;
    end;
  finally
    QrFrmFil.First;
    QrFrmFil.EnableControls;
  end;
  //
  Result := K > 0;
  if Result then
  begin
    F_STC_sem_Emp :=
      UnCreateBugs.RecriaTempTableNovo(ntrtt_STC_sem_Emp, DmodG.QrUpdPID1, False);
    //
    for I := 0 to K - 1 do
    begin
      SiapTerCad  := Lista[I][0];
      Empresa     := Lista[I][1];
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_STC_sem_Emp, False, [
      'SiapTerCad', 'Empresa', 'Ativo'], [
      ], [
      SiapTerCad, Empresa, Ativo], [
      ], False);
      //
    end;
    //
    UnDmkDac_PF.AbreMySQLQuery0(QrSTCsemEMP, DModG.MyPID_DB, [
    'SELECT DISTINCT sse.*, stc.Nome, stc.Cliente, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE ',
    'FROM _stc_sem_emp sse ',
    'LEFT JOIN ' + TMeuDB + '.siaptercad stc ON stc.Codigo=sse.SiapTerCad ',
    'LEFT JOIN ' + TMeuDB + '.Entidades ent ON ent.Codigo=stc.Cliente ',
    'ORDER BY NO_CLIENTE, stc.Nome ',
    '']);
    //
    MyObjects.frxDefineDataSets(frxGER_OSERV_030_001_B, [
      DmodG.frxDsDono,
      frxDsSTCsemEMP
    ]);
    MyObjects.frxMostra(frxGER_OSERV_030_001_B,
    'Lugares sem data de refer�ncia para gera��o de OSs autom�ticas (Padr�es de OSs filhas)');
  end;

{
  UnDmkDAC_PF.AbreMySQLQuery0(QrDtaIndf, Dmod.MyDB, [
  'SELECT DISTINCT cli.Codigo CLIENTE',
  'FROM osfrmflhcb ffc  ',
  'LEFT JOIN oscab cab ON cab.Codigo=ffc.Codigo  ',
  'LEFT JOIN cunscad cli ON cli.Codigo=cab.Entidade',
  'WHERE ffc.EmisStatus < ' + Geral.FF0(CO_FORMUL_FILHA_MONIT_EmisStat_TOTAL), // 2
  'AND ffc.EmisUltDta < SYSDATE()  ',
  'AND ffc.Ativo > 0 ',
  'AND cab.DtaExeFim > "1900-01-01"  ',
  'AND cli.DataSincOS < "1900-01-01"  ',
  '',
  'UNION',
  '',
  'SELECT DISTINCT cli.Codigo CLIENTE',
  'FROM osmoncab omc  ',
  'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo  ',
  'LEFT JOIN cunscad cli ON cli.Codigo=cab.Entidade',
  'WHERE omc.EmisStatus < ' + Geral.FF0(CO_FORMUL_FILHA_MONIT_EmisStat_TOTAL), // 2
  'AND omc.EmisUltDta < SYSDATE()  ',
  'AND cab.DtaExeFim > "1900-01-01"  ',
  'AND cli.DataSincOS < "1900-01-01"  ',
  '']);
  //
  Result := QrDtaIndf.RecordCount > 0;
  if Result then
  begin
    Txt := 'Os seguintes clientes precisam de defini��o da ' + sLineBreak +
    '"Data de refer�ncia para gera��o de OSs autom�ticas":' + sLineBreak;
    QrDtaIndf.First;
    while not QrDtaIndf.Eof do
    begin
      Txt := Txt + Geral.FF0(QrDtaIndfCLIENTE.Value) + sLineBreak;
      //
      QrDtaIndf.Next;
    end;
    //
    Geral.MB_Aviso(Txt);
  end;
}
end;
*)

function TFmOSFlhGer.ConsumosNaoRatificados(): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNaoRatif, Dmod.MyDB, [
  'SELECT DISTINCT cab.Codigo, cab.SiapterCad,',
  'stc.Nome NO_STC, cab.Entidade, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLI',
  'FROM osmonrec omr',
  'LEFT JOIN oscab cab ON cab.Codigo=omr.Codigo',
  'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade',
  'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapterCad',
  'WHERE omr.RatifUso=0 ',
  'AND cab.DtaExeFim > "1900-01-01"',
  '']);
  //
  Result := QrNaoRatif.RecordCount > 0;
  if Result then
  begin
    //
    MyObjects.frxDefineDataSets(frxGER_OSERV_030_001_C, [
      DmodG.frxDsDono,
      frxDsNaoRatif
    ]);
    MyObjects.frxMostra(frxGER_OSERV_030_001_C,
    'OSs com f�rmulas de monitoramento com consumos n�o ratificados');
  end;
end;

function TFmOSFlhGer.ContratosIndefinidos(): Boolean;
var
  Txt: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtrIndf, Dmod.MyDB, [
  'SELECT DISTINCT ctr.Codigo CONTRATO ',
  'FROM osfrmflhcb ffc  ',
  'LEFT JOIN oscab cab ON cab.Codigo=ffc.Codigo  ',
  'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat  ',
  'WHERE ffc.EmisStatus < ' + Geral.FF0(CO_FORMUL_FILHA_MONIT_EmisStat_TOTAL), // 2
  'AND ffc.EmisUltDta < SYSDATE() ',
  'AND ffc.Ativo > 0 ',
  'AND cab.DtaExeFim > "1900-01-01"  ',
  'AND cab.NumContrat<>0  ',
  //'AND cab.PosGerou=0  ', // 2014-07-13 nao adianta, pega igual!!!
  'AND ctr.DtaPrxRenw < SYSDATE() ',
  'AND  ',
  '(  ',
  '     ctr.DtaCntrFim < "1900-01-01"  ',
  ')  ',
  '',
  'UNION',
  '',
  'SELECT DISTINCT ctr.Codigo CONTRATO ',
  'FROM osmoncab omc  ',
  'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo  ',
  'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat  ',
  'WHERE omc.EmisStatus < ' + Geral.FF0(CO_FORMUL_FILHA_MONIT_EmisStat_TOTAL), // 2
  'AND omc.EmisUltDta < SYSDATE()  ',
  'AND cab.DtaExeFim > "1900-01-01"  ',
  'AND cab.NumContrat>0  ',
  //'AND cab.PosGerou=0  ', // 2014-07-13 nao adianta, pega igual!!!
  'AND ctr.DtaPrxRenw < SYSDATE() ',
  'AND  ',
  '(  ',
  '     ctr.DtaCntrFim < "1900-01-01"  ',
  ')  ',
  '']);
  //
  Result := QrCtrIndf.RecordCount > 0;
  if Result then
  begin
    Txt := 'Os seguintes contratos precisam nova defini��o de pr�xima renova��o ou rescis�o:' + #13#10;
    QrCtrIndf.First;
    while not QrCtrIndf.Eof do
    begin
      Txt := Txt + Geral.FF0(QrCtrIndfCONTRATO.Value) + #13#10;
      //
      QrCtrIndf.Next;
    end;
    //
    Geral.MB_Aviso(Txt);
  end;
end;

procedure TFmOSFlhGer.CriaItensLugarAtual();
var
  EmisUltDta, DtaExeFim, DtaCntrFim, DtaOSCalc, DtaOSUtil, DtaOSReal,
  TitItemTab, TitTipoTab: String;
  Codigo, Controle, Conta, IDIts, IDIt2, IDItZ, Ordem, Dias, Formula, Periodd,
  ExtenDd, DdPostero, NumContrat, Cliente, SiapTerCad, IDFrm, EmisStatus,
  Empresa, DesServico, HrEvacuar, PrgLstCab, Tabela, SubTab: Integer;
  HrExecutar: Double;
  MinHAgeExe: String;
  procedure IncluiAtual(Qry: TmySQLQuery; DtIni, DtParcial, DtFinal: TDate);
  var
    DtInicial: String;
  begin
    FIDItZ := FIDItZ + 1;
    IDItZ := FIDItZ;
    if DtParcial > DtFinal then
      Exit;
    //
    DtaOSCalc := Geral.FDT(DtParcial, 1);
    DtaOSUtil := '0000-00-00';
    DtaOSReal := '0000-00-00';
    DtInicial := Geral.FDT(DtIni, 1);
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_MoniNewOSs, False, [
    'Codigo', 'Controle', 'Conta',
    'IDIts', 'IDIt2', 'Ordem',
    'Dias', 'EmisUltDta', 'Formula',
    'Periodd', 'DdPostero', 'NumContrat',
    'DtaExeFim', 'DtaCntrFim', 'Empresa',
    'DtaOSCalc', 'DtaOSUtil', 'DtaOSReal',
    'Cliente', 'SiapTerCad', 'EmisStatus',
    'DesServico', 'HrEvacuar', 'HrExecutar',
    'PrgLstCab', 'Tabela', 'TitItemTab',
    'TitTipoTab', 'SubTab',
    'MinHAgeExe', 'DtInicial',
    'ExtenDd'], [
    'IDItZ'], [
    Codigo, Controle, Conta,
    IDIts, IDIt2, Ordem,
    Dias, EmisUltDta, Formula,
    Periodd, DdPostero, NumContrat,
    DtaExeFim, DtaCntrFim, Empresa,
    DtaOSCalc, DtaOSUtil, DtaOSReal,
    Cliente, SiapTerCad, EmisStatus,
    DesServico, HrEvacuar, HrExecutar,
    PrgLstCab, Tabela, TitItemTab,
    TitTipoTab, SubTab,
    MinHAgeExe, DtInicial,
    ExtenDd], [
    IDItZ], False);
  end;
var
  DtInicial, DtFinal, DtParcial, DtLinSepa, DataSincOS: TDate;
  Max: String;
  Saltos: Integer;
const
  sTxt = 'Criando registros bases tempor�rios. ';

  procedure CriaItemAtual();
  begin
    ReopenSiapTerFlh(QrFrmFilSiapTerCad.Value, QrFrmFilEmpresa.Value);
    MinHAgeExe := Geral.FDT(QrSiapTerFlhMinHAgeExe.Value, 100);
{
SELECT DataSincOS
FROM siapterflh
WHERE Codigo<>0
AND Empresa <> 0
}
    DataSincOS := QrSiapTerFlhDataSincOS.Value;
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, sTxt + 'Sequencia: ' +
      Geral.FF0(QrFrmFil.RecNo) + ' de ' + Max);
    IDFrm := QrFrmFilIDSuperior.Value;

    if QrFrmFilEmisUltDta.Value > QrFrmFilDtaExeFim.Value then
      DtInicial := QrFrmFilEmisUltDta.Value
    else
      DtInicial := QrFrmFilDtaExeFim.Value;
    //
    DtParcial := DtInicial;
    //
    if QrFrmFilNumContrat.Value <> 0 then
    begin
      DtFinal := QrFrmFilDtaPrxRenw.Value;
      if DtFinal < DtInicial then
      begin
        // como conseguiu passar pelo filtro da procedure "ContratosIndefinidos()" ): ????
        Geral.MB_Erro('Data final: ' + Geral.FDT(DtFinal, 2) + sLineBreak +
        '� menor que a inicial: ' + Geral.FDT(DtInicial, 2));
        Exit;
      end;
    end else
    begin
      //DtFinal := DtInicial + QrFrmFilPerioDd.Value + QrFrmFilExtenDd.Value;
      DtFinal := QrFrmFilDtaExeFim.Value + QrFrmFilPerioDd.Value + QrFrmFilExtenDd.Value;
    end;
    //
    ////////////////////////////////////////////////////////////////////////////////
    ///  INTERVALOS INICIAIS
    ////////////////////////////////////////////////////////////////////////////////
    // S� executa se estiver setado que ainda n�o executou.
    if QrFrmFilEmisStatus.Value = CO_FORMUL_FILHA_MONIT_EmisStat_ABERTO then // 0
    begin
      // SQLs diferentes para tabelas diferentes!
      if QrFrmFilTabela.Value = CO_FORMUL_FILHA_MONIT_EH_FILHA then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrFFII, Dmod.MyDB, [
        'SELECT ffd.Codigo, ffd.Controle, ffd.Conta, ffd.IDIts, ',
        'ffd.IDIt2, ffd.Ordem, ffd.Dias, xxx.EmisUltDta, ',
        'xxx.Formula, xxx.Periodd, xxx.ExtenDd, xxx.DdPostero, ',
        'xxx.EmisStatus, ',
        'cab.NumContrat, cab.DtaExeFim, cab.Entidade, ',
        'cab.SiapTerCad, cab.Empresa, ',
        'srv.DesServico, srv.HrEvacuar, srv.HrExecutar, ',
        'ctr.DtaCntrFim, frm.Nome NO_FORMULA ',
        'FROM osfrmflhdd ffd ',
        'LEFT JOIN osfrmflhcb xxx ON xxx.IDIts=ffd.IDIts ',
        'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo ',
        'LEFT JOIN ossrv srv ON srv.Controle=ffd.Controle',
        'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat ',
        'LEFT JOIN formulas frm ON frm.Codigo=xxx.Formula ',
         DmModOS.SQL_Filtr(),
        'AND ffd.IDIts=' + Geral.FF0(IDFrm),
        '']);
        //
        QrFFII.First;
        while not QrFFII.Eof do
        begin
          Codigo         := QrFFIICodigo.Value;
          Controle       := QrFFIIControle.Value;
          Conta          := QrFFIIConta.Value;
          IDIts          := QrFFIIIDIts.Value;
          IDIt2          := QrFFIIIDIt2.Value;
          Ordem          := QrFFIIOrdem.Value;
          Dias           := QrFFIIDias.Value;
          EmisUltDta     := Geral.FDT(QrFFIIEmisUltDta.Value, 1);
          DesServico     := QrFFIIDesServico.Value;
          HrEvacuar      := QrFFIIHrEvacuar.Value;
          HrExecutar     := QrFFIIHrExecutar.Value;
          Formula        := QrFFIIFormula.Value;
          PrgLstCab      := 0;
          Periodd        := QrFFIIPerioDd.Value;
          ExtenDd        := QrFFIIExtenDd.Value;
          DdPostero      := QrFFIIDdPostero.Value;
          NumContrat     := QrFFIINumContrat.Value;
          DtaExeFim      := Geral.FDT(QrFFIIDtaExeFim.Value, 1);
          DtaCntrFim     := Geral.FDT(QrFFIIDtaCntrFim.Value, 1);
          EmisStatus     := QrFFIIEmisStatus.Value;
          //
          Empresa        := QrFFIIEmpresa.Value;
          Cliente        := QrFFIIEntidade.Value;
          SiapTerCad     := QrFFIISiapterCad.Value;
          //
          Tabela         := CO_FORMUL_FILHA_MONIT_EH_FILHA;
          SubTab         := CO_FORMUL_FILHA_INICIAL;
          TitTipoTab     := CO_FORMUL_FILHA_MONIT_Txt_FILHA;
          TitItemTab     := QrFFIINO_FORMULA.Value;
          //
          if QrFFIIDias.Value > 0 then
          begin
            DtParcial := DtParcial + QrFFIIDias.Value;
            IncluiAtual(QrFFII, DtInicial, DtParcial, DtFinal);
          end;
          //
          QrFFII.Next;
        end;
      end else
      if QrFrmFilTabela.Value = CO_FORMUL_FILHA_MONIT_EH_MONIT then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrFMOI, Dmod.MyDB, [
        'SELECT xxx.Codigo, xxx.Controle, xxx.Conta, ',
        'omd.IDIts, omd.Ordem, omd.Dias, xxx.EmisUltDta, ',
        'xxx.Periodd, xxx.ExtenDd, xxx.DdPostero, pip.PrgLstCab, ',
        'xxx.EmisStatus, plc.Nome NO_PrgLstCab, ',
        'cab.NumContrat, cab.DtaExeFim, cab.Entidade, ',
        'cab.SiapTerCad, cab.Empresa, ',
        'srv.DesServico, srv.HrEvacuar, srv.HrExecutar, ',
        'ctr.DtaCntrFim ',
        'FROM osmoncab xxx ',
        'LEFT JOIN osmonpipdd omd ON omd.Conta=xxx.Conta ',
        'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo ',
        'LEFT JOIN ossrv srv ON srv.Controle=xxx.Controle',
        'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat ',
        'LEFT JOIN pipcad pip ON pip.Codigo=xxx.PipCad',
        'LEFT JOIN prglstcab plc ON plc.Codigo=pip.PrgLstCab ',
        DmModOS.SQL_Filtr(),
        'AND xxx.Conta=' + Geral.FF0(QrFrmFilConta.Value),
        'AND pip.DtaDesativ < "1900-01-01" ',
        '']);
        //
        QrFMOI.First;
        while not QrFMOI.Eof do
        begin
          Codigo         := QrFMOICodigo.Value;
          Controle       := QrFMOIControle.Value;
          Conta          := QrFMOIConta.Value;
          IDIts          := QrFMOIIDIts.Value;
          IDIt2          := 0;//QrFMOIIDIt2.Value;
          Ordem          := QrFMOIOrdem.Value;
          Dias           := QrFMOIDias.Value;
          EmisUltDta     := Geral.FDT(QrFMOIEmisUltDta.Value, 1);
          DesServico     := QrFMOIDesServico.Value;
          HrEvacuar      := QrFMOIHrEvacuar.Value;
          HrExecutar     := QrFMOIHrExecutar.Value;
          Formula        := 0;
          PrgLstCab      := QrFMOIPrgLstCab.Value;
          Periodd        := QrFMOIPeriodd.Value;
          ExtenDd        := QrFMOIExtenDd.Value;
          DdPostero      := QrFMOIDdPostero.Value;
          NumContrat     := QrFMOINumContrat.Value;
          DtaExeFim      := Geral.FDT(QrFMOIDtaExeFim.Value, 1);
          DtaCntrFim     := Geral.FDT(QrFMOIDtaCntrFim.Value, 1);
          EmisStatus     := QrFMOIEmisStatus.Value;
          //
          Empresa        := QrFMOIEmpresa.Value;
          Cliente        := QrFMOIEntidade.Value;
          SiapTerCad     := QrFMOISiapterCad.Value;
          //
          Tabela         := CO_FORMUL_FILHA_MONIT_EH_MONIT;
          SubTab         := CO_FORMUL_FILHA_INICIAL;
          TitTipoTab     := CO_FORMUL_FILHA_MONIT_Txt_MONIT;
          TitItemTab     := QrFMOINO_PrgLstCab.Value;
          //
          if QrFMOIDias.Value > 0 then
          begin
            DtParcial := DtParcial + QrFMOIDias.Value;
            IncluiAtual(QrFMOI, DtInicial, DtParcial, DtFinal);
          end;
          //
          QrFMOI.Next;
        end;
      end else
      begin
        Geral.MB_Aviso('Identificador de tabela origin�ria desconhecida: ' +
          Geral.FF0(QrFrmFilTabela.Value));
      end;
    end;
    //
    ////////////////////////////////////////////////////////////////////////////////
    ///  INTERVALOS P�STEROS
    ///
    //if Dias > 0 then
    //begin
    if QrFrmFilTabela.Value = CO_FORMUL_FILHA_MONIT_EH_FILHA then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrFFLE, Dmod.MyDB, [
       'SELECT xxx.Codigo, xxx.Controle, xxx.Conta, xxx.IDIts,  ',
        'xxx.EmisUltDta, xxx.Formula, xxx.Periodd, xxx.ExtenDd, xxx.DdPostero,  ',
        'xxx.EmisStatus, ',
        'cab.NumContrat, cab.DtaExeFim, cab.Entidade, ',
        'cab.SiapTerCad, cab.Empresa, ',
        'srv.DesServico, srv.HrEvacuar, srv.HrExecutar, ',
        'ctr.DtaCntrFim, frm.Nome NO_FORMULA ',
        'FROM osfrmflhcb xxx  ',
        'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo  ',
        'LEFT JOIN ossrv srv ON srv.Controle=xxx.Controle',
        'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat  ',
        'LEFT JOIN formulas frm ON frm.Codigo=xxx.Formula ',
        DmModOS.SQL_Filtr(),
        'AND xxx.IDIts=' + Geral.FF0(IDFrm),
        '']);
      // Caso houver intervalos p�steros:
      if QrFFLEDdPostero.Value > 0 then
      begin
    ////////////////////////////////////////////////////////////////////////////////
    //
    /////// Emparelhamento de datas de itens de diferentes formulas m�es //////
// Mudado em 2013-09-10
{
    Dias := Trunc(DtParcial - DataSincOS);
    if Dias < 1 then
      DtParcial := DataSincOS
    else
    begin
      Saltos := Dias div QrFFLEDdPostero.Value;
      if Saltos = 0 then
        DtParcial := DataSincOS
      else
        DtParcial := DataSincOS + (Saltos * QrFFLEDdPostero.Value);
     end;
}
        if DtParcial < DataSincOS then
          DtParcial := DataSincOS;
// FIM Mudado em 2013-09-10
    //
    ///////////////////////////////////////////////////////////////////////////
        while DtParcial <= DtFinal do
        begin
          Codigo         := QrFFLECodigo.Value;
          Controle       := QrFFLEControle.Value;
          Conta          := QrFFLEConta.Value;
          IDIts          := QrFFLEIDIts.Value;
          IDIt2          := 0;
          Ordem          := 0;
          Dias           := QrFFLEDdPostero.Value;
          EmisUltDta     := Geral.FDT(QrFFLEEmisUltDta.Value, 1);
          DesServico     := QrFFLEDesServico.Value;
          HrEvacuar      := QrFFLEHrEvacuar.Value;
          HrExecutar     := QrFFLEHrExecutar.Value;
          Formula        := QrFFLEFormula.Value;
          PrgLstCab      := 0;
          Periodd        := QrFFLEPerioDd.Value;
          ExtenDd        := QrFFLEExtenDd.Value;
          DdPostero      := QrFFLEDdPostero.Value;
          NumContrat     := QrFFLENumContrat.Value;
          DtaExeFim      := Geral.FDT(QrFFLEDtaExeFim.Value, 1);
          DtaCntrFim     := Geral.FDT(QrFFLEDtaCntrFim.Value, 1);
          EmisStatus     := QrFFLEEmisStatus.Value;
          //
          Empresa        := QrFFLEEmpresa.Value;
          Cliente        := QrFFLEEntidade.Value;
          SiapTerCad     := QrFFLESiapterCad.Value;
          //
          Tabela         := CO_FORMUL_FILHA_MONIT_EH_FILHA;
          SubTab         := CO_FORMUL_FILHA_POSTERO;
          TitTipoTab     := CO_FORMUL_FILHA_MONIT_Txt_FILHA;
          TitItemTab     := QrFFLENO_FORMULA.Value;
          //
          DtParcial :=  DtParcial + QrFFLEDdPostero.Value;
          IncluiAtual(QrFFLE, DtInicial, DtParcial, DtFinal);
        end;
      end;
    end else
    if QrFrmFilTabela.Value = CO_FORMUL_FILHA_MONIT_EH_MONIT then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrFMOE, Dmod.MyDB, [
        'SELECT xxx.Codigo, xxx.Controle, xxx.Conta,',
        'xxx.EmisUltDta, xxx.Formula, xxx.Periodd, xxx.ExtenDd, xxx.DdPostero,',
        'xxx.EmisStatus, pip.PrgLstCab, plc.Nome NO_PrgLstCab, ',
        'cab.NumContrat, cab.DtaExeFim, cab.Entidade,',
        'cab.SiapTerCad, cab.Empresa,',
        'srv.DesServico, srv.HrEvacuar, srv.HrExecutar,',
        'ctr.DtaCntrFim',
        'FROM osmoncab xxx',
        'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo',
        'LEFT JOIN ossrv srv ON srv.Controle=xxx.Controle',
        'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat',
        'LEFT JOIN pipcad pip ON pip.Codigo=xxx.PipCad',
        'LEFT JOIN prglstcab plc ON plc.Codigo=pip.PrgLstCab ',
        DmModOS.SQL_Filtr(),
        'AND xxx.Conta=' + Geral.FF0(QrFrmFilConta.Value),
        'AND pip.DtaDesativ < "1900-01-01" ',
        '']);
      // Caso houver intervalos p�steros:
      if QrFMOEDdPostero.Value > 0 then
      begin
    ////////////////////////////////////////////////////////////////////////////////
    //
    /////// Emparelhamento de datas de itens de diferentes formulas m�es //////
// Mudado em 2013-09-10
{
    Dias := Trunc(DtParcial - DataSincOS);
    if Dias < 1 then
      DtParcial := DataSincOS
    else
    begin
      Saltos := Dias div QrFMOEDdPostero.Value;
      if Saltos = 0 then
        DtParcial := DataSincOS
      else
        DtParcial := DataSincOS + (Saltos * QrFMOEDdPostero.Value);
     end;
    //
}
        if DtParcial < DataSincOS then
          DtParcial := DataSincOS;
// FIM Mudado em 2013-09-10
    ///////////////////////////////////////////////////////////////////////////
        while DtParcial <= DtFinal do
        begin
          Codigo         := QrFMOECodigo.Value;
          Controle       := QrFMOEControle.Value;
          Conta          := QrFMOEConta.Value;
          IDIts          := 0;
          IDIt2          := 0;
          Ordem          := 0;
          Dias           := QrFMOEDdPostero.Value;
          EmisUltDta     := Geral.FDT(QrFMOEEmisUltDta.Value, 1);
          DesServico     := QrFMOEDesServico.Value;
          HrEvacuar      := QrFMOEHrEvacuar.Value;
          HrExecutar     := QrFMOEHrExecutar.Value;
          Formula        := 0;
          PrgLstCab      := QrFMOEPrgLstCab.Value;
          Periodd        := QrFMOEPeriodd.Value;
          ExtenDd        := QrFMOEExtenDd.Value;
          DdPostero      := QrFMOEDdPostero.Value;
          NumContrat     := QrFMOENumContrat.Value;
          DtaExeFim      := Geral.FDT(QrFMOEDtaExeFim.Value, 1);
          DtaCntrFim     := Geral.FDT(QrFMOEDtaCntrFim.Value, 1);
          EmisStatus     := QrFMOEEmisStatus.Value;
          //
          Empresa        := QrFMOEEmpresa.Value;
          Cliente        := QrFMOEEntidade.Value;
          SiapTerCad     := QrFMOESiapterCad.Value;
          //
          Tabela         := CO_FORMUL_FILHA_MONIT_EH_MONIT;
          SubTab         := CO_FORMUL_FILHA_POSTERO;
          TitTipoTab     := CO_FORMUL_FILHA_MONIT_Txt_MONIT;
          TitItemTab     := QrFMOENO_PrgLstCab.Value;
          //
          DtParcial :=  DtParcial + QrFMOEDdPostero.Value;
          IncluiAtual(QrFMOE, DtInicial, DtParcial, DtFinal);
        end;
      end;
    end else
    begin
      Geral.MB_Aviso('Identificador de tabela origin�ria desconhecida: ' +
        Geral.FF0(QrFrmFilTabela.Value));
    end;
    //end;
    //
    //
  end;
var
  Itens, I: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sTxt + 'Preparando');
  //
  PB1.Position := 0;

(*
  Itens := QrFrmFil.RecordCount;
  Max := Geral.FF0(Itens);
  PB1.Max := Itens;
  QrFrmFil.First;
  while not QrFrmFil.Eof do
  begin
    CriaItemAtual();
    QrFrmFil.Next;
  end;
*)
  Dmod.ReopenOpcoesBugs;
  //
  Itens := DBGrid1.SelectedRows.Count;
  Max := Geral.FF0(Itens);
  PB1.Max := Itens;
  if Itens > 0 then
  begin
    with DBGrid1.DataSource.DataSet do
    for I := 0 to Itens - 1 do
    begin
      GotoBookmark(pointer(DBGrid1.SelectedRows.Items[I]));
      CriaItemAtual();
    end;
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmOSFlhGer.DBGrid1DblClick(Sender: TObject);
var
  Selected: Boolean;
  STC, Conta: Integer;
begin
  Selected := not DBGrid1.SelectedRows.CurrentRowSelected;
  STC      := QrFrmFilSiapTerCad.Value;
  Conta    := QrFrmFilConta.Value;
  //
  with DBGrid1.DataSource.DataSet do
  begin
    DisableControls;
    First;
    try
      while not EOF do
      begin
        if STC = QrFrmFilSiapTerCad.Value then
        begin
          DBGrid1.SelectedRows.CurrentRowSelected := Selected;
        end;
        Next;
      end;
    finally
      EnableControls;
    end;
  end;
  QrFrmFil.Locate('Conta', Conta, []);
end;

function TFmOSFlhGer.DefineAgeEqiCab: Integer;
begin
  Result := 0;
  case Dmod.QrOpcoesBugsFlhEntCtt.Value of
    CO_OSFlh_OPT_NAO_DEFINID: Result := 0;
    CO_OSFlh_OPT_DEIXR_VAZIO: Result := 0;
    CO_OSFlh_OPT_DEF_IN_LUGR: Result := QrSiapTerFlhMonAgeEqi.Value;
    CO_OSFlh_OPT_FROM_OS_INI: Result := QrOSCabAgeEqiCab.Value;
    //CO_OSFlh_OPT_AGR_AGE_OSs = 4;
    else
    begin
      Result := 0;
      MsgFaltaDef('Equipe de agentes');
    end;
  end;
end;

function TFmOSFlhGer.DefineCartEmiss: Integer;
begin
  Result := 0;
  case Dmod.QrOpcoesBugsFlhEntCtt.Value of
    CO_OSFlh_OPT_NAO_DEFINID: Result := 0;
    CO_OSFlh_OPT_DEIXR_VAZIO: Result := 0;
    CO_OSFlh_OPT_DEF_IN_LUGR: Result := QrSiapTerFlhMonCrtEmi.Value;
    CO_OSFlh_OPT_FROM_OS_INI: Result := QrOSCabCartEmis.Value;
    //CO_OSFlh_OPT_AGR_AGE_OSs = 4;
    else
    begin
      Result := 0;
      MsgFaltaDef('Carteira de emiss�o');
    end;
  end;
end;

function TFmOSFlhGer.DefineCondicaoPg: Integer;
begin
  Result := 0;
  case Dmod.QrOpcoesBugsFlhEntCtt.Value of
    CO_OSFlh_OPT_NAO_DEFINID: Result := 0;
    CO_OSFlh_OPT_DEIXR_VAZIO: Result := 0;
    CO_OSFlh_OPT_DEF_IN_LUGR: Result := QrSiapTerFlhMonCondPg.Value;
    CO_OSFlh_OPT_FROM_OS_INI: Result :=
      DmModOS.CondicaoDePagamentoSelecionada(QrOSCabCodigo.Value);
    //CO_OSFlh_OPT_AGR_AGE_OSs = 4;
    else
    begin
      Result := 0;
      MsgFaltaDef('Condi��o de pagamento');
    end;
  end;
end;

function TFmOSFlhGer.DefineEntContrat: Integer;
begin
  Result := 0;
  case Dmod.QrOpcoesBugsFlhEntCtt.Value of
    CO_OSFlh_OPT_NAO_DEFINID: Result := 0;
    CO_OSFlh_OPT_DEIXR_VAZIO: Result := 0;
    CO_OSFlh_OPT_DEF_IN_LUGR: Result := QrSiapTerFlhMonEntCtr.Value;
    CO_OSFlh_OPT_FROM_OS_INI: Result := QrOSCabEntContrat.Value;
    //CO_OSFlh_OPT_AGR_AGE_OSs = 4;
    else
    begin
      Result := 0;
      MsgFaltaDef('Contratante');
    end;
  end;
end;

function TFmOSFlhGer.DefineEntiContat(): Integer;
begin
  Result := 0;
  case Dmod.QrOpcoesBugsFlhEntCtt.Value of
    CO_OSFlh_OPT_NAO_DEFINID: Result := 0;
    CO_OSFlh_OPT_DEIXR_VAZIO: Result := 0;
    CO_OSFlh_OPT_DEF_IN_LUGR: Result := QrSiapTerFlhMonEntCtt.Value;
    CO_OSFlh_OPT_FROM_OS_INI: Result := QrOSCabEntiContat.Value;
    //CO_OSFlh_OPT_AGR_AGE_OSs = 4;
    else
    begin
      Result := 0;
      MsgFaltaDef('Contato');
    end;
  end;
{
object QrOpcoesBugsFlhAgeEqi: TSmallintField
object QrOpcoesBugsFlhCrtEmi: TSmallintField
}
end;

function TFmOSFlhGer.DefineEntPagante: Integer;
begin
  Result := 0;
  case Dmod.QrOpcoesBugsFlhEntCtt.Value of
    CO_OSFlh_OPT_NAO_DEFINID: Result := 0;
    CO_OSFlh_OPT_DEIXR_VAZIO: Result := 0;
    CO_OSFlh_OPT_DEF_IN_LUGR: Result := QrSiapTerFlhMonEntPag.Value;
    CO_OSFlh_OPT_FROM_OS_INI: Result := QrOSCabEntPagante.Value;
    //CO_OSFlh_OPT_AGR_AGE_OSs = 4;
    else
    begin
      Result := 0;
      MsgFaltaDef('Pagante');
    end;
  end;
end;

function TFmOSFlhGer.DefineNumContrat(): Integer;
begin
  Result := 0;
  case Dmod.QrOpcoesBugsFlhEntCtt.Value of
    CO_OSFlh_OPT_NAO_DEFINID: Result := 0;
    CO_OSFlh_OPT_DEIXR_VAZIO: Result := 0;
    CO_OSFlh_OPT_DEF_IN_LUGR: Result := QrSiapTerFlhMonNumCtr.Value;
    CO_OSFlh_OPT_FROM_OS_INI: Result := QrOSCabNumContrat.Value;
    //CO_OSFlh_OPT_AGR_AGE_OSs = 4;
    else
    begin
      Result := 0;
      MsgFaltaDef('N�mero do contrato');
    end;
  end;
end;

function TFmOSFlhGer.DefinePrePragas(): Integer;
begin
  Result := 0;
  case Dmod.QrOpcoesBugsFlhPrePrg.Value of
    CO_OSFlh_OPT_NAO_DEFINID: Result := 0;
    CO_OSFlh_OPT_DEIXR_VAZIO: Result := 0;
    //CO_OSFlh_OPT_DEF_IN_LUGR: Result := 0;
    CO_OSFlh_OPT_FROM_OS_INI: Result := QrOSCabCodigo.Value;
    //CO_OSFlh_OPT_AGR_AGE_OSs = 4;
    else
    begin
      Result := 0;
      MsgFaltaDef('Pragas do pr�-atendimento');
    end;
  end;
end;

procedure TFmOSFlhGer.DefineSequenciaDeNovasOSs();
const
  TxtBase = 'Preparando: Definindo sequencia das OSs.';
var
  TxtGru: String;
  GrupoOS, (*Codigo,*) SeqNovaOS: Integer;
begin
////////////////////////////////////////////////////////////////////////////////
///  Com a retirada do campo "codigo" da sql abaixo esta procedure inteira ficou
///  deprecada, mas vou deix�-la ativa por enquanto para n�o correr o risco de
///  precisar reativa-l� num futuro pr�ximo - 2013-08-06
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TxtBase);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSNOS, DModG.MyPID_DB, [
  'SELECT DISTINCT GrupoOS',
  'FROM ' + F_MoniNewOSs,
  'ORDER BY GrupoOS',
  '']);
  //
  SeqNovaOS := 0;
  PB1.Position := 0;
  PB1.Max := QrSNOS.RecordCount;
  QrSNOS.First;
  while not QrSNOS.Eof do
  begin
    TxtGru := ' Grupo ' + Geral.FF0(QrSNOSGrupoOS.Value) + '.';
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, TxtBase + TxtGru);
    //
    SeqNovaOS := SeqNovaOS + 1;
    GrupoOS   := QrSNOSGrupoOS.Value;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_MoniNewOSs, False, [
    'SeqNovaOS'], [
    'GrupoOS'], [
    SeqNovaOS], [
    GrupoOS], False);
    //
    QrSNOS.Next;
  end;
end;

procedure TFmOSFlhGer.FormActivate(Sender: TObject);
begin
  if TFmOSFlhGer(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmOSFlhGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  GBEtapa1.Align := alClient;
  GBEtapa2.Align := alClient;
  GBEtapa3.Align := alClient;
  //
  ReopenFrmFil();
end;

procedure TFmOSFlhGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSFlhGer.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmOSFlhGer.frxGER_OSERV_030_002_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then Value := Date()
  else
end;

(*
function TFmOSFlhGer.GeraOSAge_A(CodOri, Codigo: Integer): Boolean;
var
  Controle: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao +
  'Preparando agentes. ');
  //
  ReopenOSAge(CodOri);
  QrOSAge.First;
  while not QrOSAge.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao +
    'Inserindo agente: ' + Geral.FF0(QrOSAgeAgente.Value));
    //
    Controle  := UMyMod.BPGS1I32('osage', 'Controle', '', '', tsDef, stIns, 0);
    //
    UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osage', TMeuDB,
    ['Controle'], [QrOSAgeControle.Value],
    ['Codigo', 'Controle'], [Codigo, Controle], '', True, nil, nil);
    //
    QrOSAge.Next;
  end;
  Result := True;
end;
*)

function TFmOSFlhGer.GeraOSAlv(Codigo, Controle: Integer): Boolean;
var
  Conta, Praga_Z: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv +
  'Preparando pragas alvo.');
  //
  ReopenOSAlv(QrNovosSrvControle.Value);
  //
  QrOSAlv.First;
  while not QrOSAlv.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv +
    'Incluindo praga alvo: ' + Geral.FF0(QrOSAlvPraga_Z.Value));
    //
    Praga_Z := QrOSAlvPraga_Z.Value;
    //
    Conta := UMyMod.BPGS1I32_Reaproveita('osalv', 'Conta', '', '', tsDef, stIns, 0);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osalv', False, [
    'Codigo', 'Controle', 'Praga_Z'], [
    'Conta'], [
    Codigo, Controle, Praga_Z], [
    Conta], True);
    //
    QrOSAlv.Next;
  end;
  //
  Result := True;
end;

{
function TFmOSFlhGer.GeraOSCab_A(var Codigo: Integer): Boolean;
var
  DtaContat, DtaVisPrv, DtaVisExe, DtaExePrv, DtaExeIni, DtaExeFim, DtaLibFat,
  DtaFimFat, SerNF, ObsGaranti, ObsExecuta, FimVisPrv, FimVisExe, FimExePrv: String;
  Empresa, Entidade, SiapTerCad, Estatus, FatoGeradr, (*OSOrigem,*) DdsPosVda,
  EntiContat, NumContrat, EntPagante, EntContrat, CondicaoPg, CartEmis, NumNF,
  ValiDdOrca, Operacao, ExeTxtCli1, ExeTxtCli2, Grupo, Numero, Opcao, Optado,
  HowGerou, PosGerou, OSFlhUltGe, GTSROTDR: Integer;
  ValorServi, ValorDesco, ValorOutrs, ValorTotal, InvalServi, InvalDesco,
  InvalOutrs, InvalTotal, OrcamServi, OrcamDesco, OrcamOutrs, OrcamTotal,
  ValorPre: Double;
  MulServico, AgeEqiCab: Int64;
begin
  ReopenOSCab(QrNovasOSsCodigo.Value);
  //
  Codigo         := 0;
  Empresa        := QrOSCabEmpresa.Value;
  Entidade       := QrOSCabEntidade.Value;
  SiapTerCad     := QrOSCabSiapTerCad.Value;
  Estatus        := Dmod.QrOpcoesBugsOSPrvSta.Value;
  FatoGeradr     := CO_COD_FatoGeradr_Preventiva;
  //OSOrigem       := QrOSCabCodigo.Value; // Apenas informativo, o usu�rio consegue editar
  DtaContat      := '0000-00-00';
  DtaVisPrv      := '0000-00-00';
  DtaVisExe      := '0000-00-00';
  DtaExePrv      := Geral.FDT(QrNovasOSsDtaOSReal.Value, 1);
  DtaExeIni      := '0000-00-00';
  DtaExeFim      := '0000-00-00';
  DtaLibFat      := '0000-00-00';
  DtaFimFat      := '0000-00-00';
  ValorServi     := 0;
  ValorDesco     := 0;
  ValorOutrs     := 0;
  ValorTotal     := 0;
  DdsPosVda      := 0;
  EntiContat     := QrOSCabEntiContat.Value;
  NumContrat     := QrOSCabNumContrat.Value;
  EntPagante     := QrOSCabEntPagante.Value;
  EntContrat     := QrOSCabEntContrat.Value;
  CondicaoPg     := QrOSCabCondicaoPg.Value;
  CartEmis       := QrOSCabCartEmis.Value;
  SerNF          := '';
  NumNF          := 0;
  InvalServi     := 0;
  InvalDesco     := 0;
  InvalOutrs     := 0;
  InvalTotal     := 0;
  OrcamServi     := 0;
  OrcamDesco     := 0;
  OrcamOutrs     := 0;
  OrcamTotal     := 0;
  ValiDdOrca     := 1;
  Operacao       := ObtemConjuntoDeOperacoesDeItens();
  ExeTxtCli1     := Dmod.QrOpcoesBugsOSPrvETC.Value;
  ValorPre       := 0;
  ObsGaranti     := '';
  ObsExecuta     := '';
  ExeTxtCli2     := 0;
  FimVisPrv      := '0000-00-00';
  FimVisExe      := '0000-00-00';
  FimExePrv      := '0000-00-00';
  Grupo          := 0; // Definido abaixo
  Numero         := 0; // Definido abaixo
  Opcao          := 1; // 1� or�amento
  Optado         := 1; // Sim, selecionado!
  HowGerou       := CO_OS_HOWGEROU_OS_AUTO_PREVENT;
  PosGerou       := 0; // N�o se aplica. (se aplica � sua m�e)
  OSFlhUltGe     := 0; // N�o se aplica. (se aplica � sua m�e)
  GTSROTDR       := 0; // Ainda n�o gerou todos sub registros de outras tabelas deste registro
  MulServico     := QrOSCabMulServico.Value;
  AgeEqiCab      := QrOSCabAgeEqiCab.Value;
  //
  //Grupo := QrGruposGrupo.Value;
  Grupo := UMyMod.BPGS1I32('oscab', 'Grupo', '', '', tsDef, stIns, Grupo);
  Numero := Grupo; // N�o usa para nada! S� para imprimir como n�mero da OS!

  //
  Codigo := UMyMod.BPGS1I32('oscab', 'Codigo', '', '', tsPos, stIns, 0);
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'oscab', false, [
  'Empresa', 'Entidade', 'SiapTerCad',
  'Estatus', 'FatoGeradr', (*'OSOrigem',*)
  'DtaContat', 'DtaVisPrv', 'DtaVisExe',
  'DtaExePrv', 'DtaExeIni', 'DtaExeFim',
  'DtaLibFat', 'DtaFimFat', 'ValorServi',
  'ValorDesco', 'ValorOutrs', 'ValorTotal',
  'DdsPosVda', 'EntiContat', 'NumContrat',
  'EntPagante', 'EntContrat', 'CondicaoPg',
  'CartEmis', 'SerNF', 'NumNF',
  'InvalServi', 'InvalDesco', 'InvalOutrs',
  'InvalTotal', 'OrcamServi', 'OrcamDesco',
  'OrcamOutrs', 'OrcamTotal', 'ValiDdOrca',
  'Operacao', 'ExeTxtCli1', 'ValorPre',
  'ObsGaranti', 'ObsExecuta',
  'ExeTxtCli2', 'FimVisPrv',
  'FimVisExe', 'FimExePrv', 'Grupo',
  'Numero', 'Opcao', 'Optado',
  'HowGerou', 'PosGerou', 'OSFlhUltGe',
  'GTSROTDR'], [
  'Codigo'], [
  Empresa, Entidade, SiapTerCad,
  Estatus, FatoGeradr, (*OSOrigem,*)
  DtaContat, DtaVisPrv, DtaVisExe,
  DtaExePrv, DtaExeIni, DtaExeFim,
  DtaLibFat, DtaFimFat, ValorServi,
  ValorDesco, ValorOutrs, ValorTotal,
  DdsPosVda, EntiContat, NumContrat,
  EntPagante, EntContrat, CondicaoPg,
  CartEmis, SerNF, NumNF,
  InvalServi, InvalDesco, InvalOutrs,
  InvalTotal, OrcamServi, OrcamDesco,
  OrcamOutrs, OrcamTotal, ValiDdOrca,
  Operacao, ExeTxtCli1, ValorPre,
  ObsGaranti, ObsExecuta,
  ExeTxtCli2, FimVisPrv,
  FimVisExe, FimExePrv, Grupo,
  Numero, Opcao, Optado,
  HowGerou, PosGerou, OSFlhUltGe,
  GTSROTDR], [
  Codigo], True);
end;
}

function TFmOSFlhGer.GeraOSCabAlv(Codigo: Integer): Boolean;
var
  OSCab, Controle, Praga_A, Praga_Z, Ordem: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv +
  'Preparando pragas do pr�-atendimento.');
  //
  OSCab := DefinePrePragas();
  if OSCab > 0 then
  begin
    OSApp_PF.ReopenOSCabAlv(QrOSCabAlv, OSCab, 0);
    //
    QrOSCabAlv.First;
    while not QrOSCabAlv.Eof do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv +
      'Incluindo praga pr�-atendimento: ' + Geral.FF0(QrOSCabAlvPraga_Z.Value));
      //
      Praga_A := QrOSCabAlvPraga_A.Value;
      Praga_Z := QrOSCabAlvPraga_Z.Value;
      Ordem   := QrOSCabAlvOrdem.Value;
      //
      Controle  := UMyMod.BPGS1I32('oscabalv', 'Controle', '', '', tsDef, stIns, 0);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'oscabalv', False, [
      'Codigo', 'Praga_A', 'Praga_Z',
      'Ordem'], [
      'Controle'], [
      Codigo, Praga_A, Praga_Z,
      Ordem], [
      Controle], True);
      //
      QrOSCabAlv.Next;
    end;
  end;
  //
  Result := True;
end;

function TFmOSFlhGer.GeraOSCab_B(var Codigo: Integer; const OSFlhGrCab,
  OSFlhGrIts: Integer): Boolean;
var
  DtaContat, DtaVisPrv, DtaVisExe, DtaExePrv, DtaExeIni, DtaExeFim, DtaLibFat,
  DtaFimFat, SerNF, ObsGaranti, ObsExecuta, FimVisPrv, FimVisExe, FimExePrv: String;
  Empresa, Entidade, SiapTerCad, Estatus, FatoGeradr, (*OSOrigem,*) DdsPosVda,
  EntiContat, NumContrat, EntPagante, EntContrat, CondicaoPg, CartEmis, NumNF,
  ValiDdOrca, Operacao, ExeTxtCli1, ExeTxtCli2, Grupo, Numero, Opcao, Optado,
  HowGerou, PosGerou, OSFlhUltGe(*, GTSROTDR*), PdrMntsMon, MobiliCad: Integer;
  ValorServi, ValorDesco, ValorOutrs, ValorTotal, InvalServi, InvalDesco,
  InvalOutrs, InvalTotal, OrcamServi, OrcamDesco, OrcamOutrs, OrcamTotal,
  ValorPre, MinIni, MinExe: Double;
  MulServico, AgeEqiCab: Int64;
  //
  Inicio, Termino: TDateTime;
  Nome, Notas: String;
  Cor, Status: Integer;
  Cption: Byte;
  // OsPrz
  Controle, Condicao, Parcelas, Escolhido: Integer;
  DescoPer: Double;
  Hora: TTime;
  DtIniMonGr: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSs, DModG.MyPID_DB, [
  'SELECT Codigo, DtaOSReal, MinHAgeExe, DtInicial ',
  'FROM _moninewoss ',
  'WHERE SeqNovaOS=' + Geral.FF0(QrNovasOSsSeqNovaOS.Value),
  'ORDER BY Codigo, DtaOSReal ',
  '']);
  if QrOSsCodigo.Value = 0 then
  begin
    Geral.MB_Erro('OS n�o informada para o sequencial de OSs n�mero ' +
      Geral.FF0(QrNovasOSsSeqNovaOS.Value));
    //
    Exit;
  end;
  //
  ReopenOSCab(QrOSsCodigo.Value);
  //
  DtIniMonGr := Geral.FDT(QrOSsDtInicial.Value, 1);
  PdrMntsMon := QrOSCabPdrMntsMon.Value;
  if PdrMntsMon = 0 then
    PdrMntsMon := Dmod.QrOpcoesBugsPdrMntsMon.Value;
  if PdrMntsMon = 0 then
    PdrMntsMon := 30;
  MinExe := PdrMntsMon / 1440 (*60 min x 24 h*);
  MinIni := QrOSCabDtaExePrv.Value - Trunc(QrOSCabDtaExePrv.Value);
  //
  Codigo         := 0;
  Empresa        := QrOSCabEmpresa.Value;
  Entidade       := QrOSCabEntidade.Value;
  SiapTerCad     := QrOSCabSiapTerCad.Value;
  MobiliCad      := QrOSCabMobiliCad.Value;
  Estatus        := Dmod.QrOpcoesBugsOSPrvSta.Value;
  FatoGeradr     := CO_COD_FatoGeradr_Preventiva;
  //OSOrigem       := QrOSCabCodigo.Value; // Apenas informativo, o usu�rio consegue editar
  DtaContat      := '0000-00-00';
  //
  DtaVisPrv      := '0000-00-00';
  FimVisPrv      := '0000-00-00';
  //
  DtaVisExe      := '0000-00-00';
  FimVisExe      := '0000-00-00';
  //  Parei Aqui! Ver o que Fazer!!
  if MinIni < 8/24 then
  begin
    //ReopenSiapTerFlh(SiapTerCad, Empresa);
    if QrOSsMinHAgeExe.Value > 0 then
    begin
      Hora := QrOSsMinHAgeExe.Value;
      MinIni := Hora;
    end else
    if Dmod.QrOpcoesBugsMinHAgeExe.Value > 0 then
    begin
      Hora := Dmod.QrOpcoesBugsMinHAgeExe.Value;
      MinIni := Hora;
    end else
      MinIni := 8/24;
  end;
  //
  DtaExePrv      := Geral.FDT(QrOSsDtaOSReal.Value + MinIni, 109);
  FimExePrv      := Geral.FDT(QrOSsDtaOSReal.Value + MinIni + MinExe, 109);
  //
  DtaExeIni      := '0000-00-00';
  DtaExeFim      := '0000-00-00';
  //
  DtaLibFat      := '0000-00-00';
  DtaFimFat      := '0000-00-00';
  ValorServi     := 0;
  ValorDesco     := 0;
  ValorOutrs     := 0;
  ValorTotal     := 0;
  DdsPosVda      := 0;
  ReopenSiapTerFlh(SiapTerCad, Empresa);
  EntiContat     := DefineEntiContat(); // QrOSCabEntiContat.Value;
  NumContrat     := DefineNumContrat(); // QrOSCabNumContrat.Value;
  EntPagante     := DefineEntPagante(); // QrOSCabEntPagante.Value;
  EntContrat     := DefineEntContrat(); // QrOSCabEntContrat.Value;
  CondicaoPg     := DefineCondicaoPg(); // QrOSCabCondicaoPg.Value;
  CartEmis       := DefineCartEmiss();  // QrOSCabCartEmis.Value;
  SerNF          := '';
  NumNF          := 0;
  InvalServi     := 0;
  InvalDesco     := 0;
  InvalOutrs     := 0;
  InvalTotal     := 0;
  OrcamServi     := 0;
  OrcamDesco     := 0;
  OrcamOutrs     := 0;
  OrcamTotal     := 0;
  ValiDdOrca     := 1;
  Operacao       := ObtemConjuntoDeOperacoesDeItens();
  ExeTxtCli1     := Dmod.QrOpcoesBugsOSPrvETC.Value;
  ValorPre       := 0;
  ObsGaranti     := '';
  ObsExecuta     := '';
  ExeTxtCli2     := Dmod.QrOpcoesBugsOSPrvOTC.Value;
  Grupo          := 0; // Definido abaixo
  Numero         := 0; // Definido abaixo
  Opcao          := 1; // 1� or�amento
  Optado         := 1; // Sim, selecionado!
  HowGerou       := CO_OS_HOWGEROU_OS_AUTO_PREVENT;
  PosGerou       := 0; // N�o se aplica. (se aplica � sua m�e)
  OSFlhUltGe     := 0; // N�o se aplica. (se aplica � sua m�e)
  //GTSROTDR       := 0; // Ainda n�o gerou todos sub registros de outras tabelas deste registro
(*
DELETE
FROM ossrv
WHERE Codigo IN (
  SELECT Codigo
  FROM oscab
  WHERE GTSROTDR=0
);

DELETE
FROM oscab
WHERE GTSROTDR=0;
*)
  MulServico     := QrOSCabMulServico.Value;
  AgeEqiCab      := DefineAgeEqiCab(); // QrOSCabAgeEqiCab.Value;
  //
  //Grupo := QrGruposGrupo.Value;
  Grupo := UMyMod.BPGS1I32('oscab', 'Grupo', '', '', tsDef, stIns, Grupo);
  Numero := Grupo; // N�o usa para nada! S� para imprimir como n�mero da OS!
  //
  Codigo := UMyMod.BPGS1I32('oscab', 'Codigo', '', '', tsPos, stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'oscab', false, [
  'Empresa', 'Entidade', 'SiapTerCad',
  'Estatus', 'FatoGeradr', (*'OSOrigem',*)
  'DtaContat', 'DtaVisPrv', 'DtaVisExe',
  'DtaExePrv', 'DtaExeIni', 'DtaExeFim',
  'DtaLibFat', 'DtaFimFat', 'ValorServi',
  'ValorDesco', 'ValorOutrs', 'ValorTotal',
  'DdsPosVda', 'EntiContat', 'NumContrat',
  'EntPagante', 'EntContrat', 'CondicaoPg',
  'CartEmis', 'SerNF', 'NumNF',
  'InvalServi', 'InvalDesco', 'InvalOutrs',
  'InvalTotal', 'OrcamServi', 'OrcamDesco',
  'OrcamOutrs', 'OrcamTotal', 'ValiDdOrca',
  'Operacao', 'ExeTxtCli1', 'ValorPre',
  'ObsGaranti', 'ObsExecuta',
  'ExeTxtCli2', 'FimVisPrv',
  'FimVisExe', 'FimExePrv', 'Grupo',
  'Numero', 'Opcao', 'Optado',
  'HowGerou', 'PosGerou', 'OSFlhUltGe',
  (*'GTSROTDR',*) 'AgeEqiCab', 'OSFlhGrCab',
  'OSFlhGrIts', 'DtIniMonGr', 'MobiliCad'], [
  'Codigo'], [
  Empresa, Entidade, SiapTerCad,
  Estatus, FatoGeradr, (*OSOrigem,*)
  DtaContat, DtaVisPrv, DtaVisExe,
  DtaExePrv, DtaExeIni, DtaExeFim,
  DtaLibFat, DtaFimFat, ValorServi,
  ValorDesco, ValorOutrs, ValorTotal,
  DdsPosVda, EntiContat, NumContrat,
  EntPagante, EntContrat, CondicaoPg,
  CartEmis, SerNF, NumNF,
  InvalServi, InvalDesco, InvalOutrs,
  InvalTotal, OrcamServi, OrcamDesco,
  OrcamOutrs, OrcamTotal, ValiDdOrca,
  Operacao, ExeTxtCli1, ValorPre,
  ObsGaranti, ObsExecuta,
  ExeTxtCli2, FimVisPrv,
  FimVisExe, FimExePrv, Grupo,
  Numero, Opcao, Optado,
  HowGerou, PosGerou, OSFlhUltGe,
  (*GTSROTDR,*) AgeEqiCab, OSFlhGrCab,
  OSFlhGrIts, DtIniMonGr, MobiliCad], [
  Codigo], True) then
  begin
    OSApp_PF.CriaItensDeAgentes(Codigo, AgeEqiCab);
    Result := True;
    //
    // Gerar agenda!
{   N�o tem
    // Vistoria
    Inicio  := Geral.STD_109(DtaVisPrv);
    Termino := Geral.STD_109(FimVisPrv);
    Nome    := '';
    Notas   := '';
    Cor     := 0;
    Cption  := 0;
    Status  := Estatus;
    //
    DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagVistoria, Codigo,
    Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
    Cor, Cption, Status);
    // Execu��o
}
    Inicio  := Geral.STD_109(DtaExePrv);
    Termino := Geral.STD_109(FimExePrv);
    Nome    := '';
    Notas   := '';
    Cor     := 0;
    Cption  := 0;
    DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagExecucao, Codigo,
    Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
    Cor, Cption, Status);
    //
    // Condi��o de pagamento
    UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
    'SELECT ppc.MaxDesco, ppc.Parcelas, ',
    'ppc.Codigo, ppc.CodUsu, ppc.Nome ',
    'FROM pediprzcab ppc ',
    'WHERE ppc.Codigo=' + Geral.FF0(CondicaoPG),
    '']);
    Condicao  := QrPediPrzCabCodigo.Value;
    Parcelas  := QrPediPrzCabParcelas.Value;
    Escolhido := 1;
    DescoPer  := QrPediPrzCabMaxDesco.Value;
    Controle  := UMyMod.BPGS1I32('osprz', 'Controle', '', '',
      tsDef, stIns, Controle);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osprz', False, [
    'Codigo', 'Condicao', 'DescoPer', 'Parcelas', 'Escolhido'], [
    'Controle'], [
    Codigo, Condicao, DescoPer, Parcelas, Escolhido
    ], [
    Controle], True);
  end;
end;

function TFmOSFlhGer.GeraOSFrmCab(const Codigo, Controle: Integer;
  var Conta: Integer): Boolean;
  //
  function IncluiOsFrmCabAtual(const Formula: Integer; var Conta: Integer): Boolean;
  var
    Nome: String;
    EquipAplic, Diluente, TipoAplica, UnidMed, OSFFCbMae: Integer;
    QtdTot, QtdQSP, CusTot, PercFeito: Double;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrFormula, Dmod.MyDB, [
    'SELECT * ',
    'FROM Formulas ',
    'WHERE Codigo=' + Geral.FF0(Formula),
    '']);
    Conta          := 0;
    Nome           := QrFormulaNome.Value;
    EquipAplic     := QrFormulaEquipAplic.Value;
    QtdTot         := QrFormulaQtdTot.Value;
    //QtdQSP         := ;
    //CusTot         := ;
    Diluente       := QrFormulaDiluente.Value;
    TipoAplica     := QrFormulaTipoAplica.Value;
    UnidMed        := QrFormulaUnidMed.Value;
    //PercFeito      := ;
    OSFFCbMae      := QrNewF_MIDIts.Value;

    //
    Conta := UMyMod.BPGS1I32('osfrmcab', 'Conta', '', '', tsDef, stIns, Conta);
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osfrmcab', False, [
    'Codigo', 'Controle', 'Nome',
    'Formula', 'EquipAplic', 'QtdTot',
    (*'QtdQSP', 'CusTot',*) 'Diluente',
    'TipoAplica', 'UnidMed', (*'PercFeito',*)
    'OSFFCbMae'], [
    'Conta'], [
    Codigo, Controle, Nome,
    Formula, EquipAplic, QtdTot,
    (*QtdQSP, CusTot,*) Diluente,
    TipoAplica, UnidMed, (*PercFeito,*)
    OSFFCbMae], [
    Conta], True);
  end;

  function  InsereOSFrmDepAtual(Conta: Integer): Boolean;
  var
    IDIts, Tabela, Cadastro: Integer;
  begin
    IDIts          := 0;
    Tabela         := QrOSFrmDepTabela.Value;
    Cadastro       := QrOSFrmDepCadastro.Value;;
    //
    IDIts := UMyMod.BPGS1I32_Reaproveita('osfrmdep', 'IDIts', '', '', tsDef, stIns, IDIts);
    Result :=  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osfrmdep', False, [
    'Codigo', 'Controle', 'Conta',
    'Tabela', 'Cadastro'], [
    'IDIts'], [
    Codigo, Controle, Conta,
    Tabela, Cadastro], [
    IDIts], True);
  end;

  var
    CtOri, Formula: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv +
  'Preparando f�rmulas.');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNewF_M, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + F_MoniNewOSs,
  'WHERE SeqNovaOS=' + Geral.FF0(QrNovasOSsSeqNovaOS.Value),
(*
  'AND Codigo=' + Geral.FF0(QrNovasOSsCodigo.Value),
  'AND DtaOSReal="' + Geral.FDT(QrNovasOSsDtaOSReal.Value, 1) + '"',
*)
  'AND Controle=' + Geral.FF0(QrNovosSrvControle.Value),
  'AND DdPostero=' + Geral.FF0(QrNovosSrvDdPostero.Value),
  'AND Tabela=' + Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_FILHA), // CUIDADO!!! Aqui Muda!!!
  '']);
  //
  QrNewF_M.First;
  while not QrNewF_M.Eof do
  begin
    FTxtF_M := 'Incluindo f�rmula: ' + Geral.FF0(QrNewF_MFormula.Value) + '. ';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv + FTxtF_M);
    //
    CtOri   := QrNewF_MConta.Value;
    Conta   := 0;
    Formula := QrNewF_MFormula.Value;
    if IncluiOSFrmCabAtual(Formula, Conta) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv +
      FTxtF_M + 'Incluindo itens');
      // Itens e abrangencias da formula base!
      OSApp_PF.CopiaItensDaFormulaBase(
        gbsAplica, Formula, Codigo, Controle, Conta, False);
      //
      // Dependencias
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv +
      FTxtF_M + 'Incluindo depend�ncias');
      UnDmkDAC_PF.AbreMySQLQuery0(QrOSFrmDep, Dmod.MyDB, [
      'SELECT * ',
      'FROM osfrmdep ',
      'WHERE Conta=' + Geral.FF0(CtOri),
      '']);
      QrOSFrmDep.First;
      while not QrOSFrmDep.Eof do
      begin
        InsereOSFrmDepAtual(Conta);
        //
        QrOSFrmDep.Next;
      end;
    end;
    //
    QrNewF_M.Next;
  end;
  Result := True;
end;

// 2014-08-07
{
function TFmOSFlhGer.GeraOSMonCab(Codigo: Integer; DEP: TDateTime): Boolean;
(*
  / n�o tem como fazer asim!
  / se houver adi��o de PIP em outra OS, ter� grande chance de haver datas
  / desencontradas de monitoramento!
  //
  function IncluiOSMonPipAtual(SiapterCad, OSFMCbMae: Integer): Boolean;
  var
    Controle, PipCad, Ordem, Reordem, PrgLstCab: Integer;
  begin
      Controle       := 0;
      PipCad         := QrLocPipCodigo.Value;
      Ordem          := QrLocPip.RecNo;
      Reordem        := Ordem;
      PrgLstCab      := QrLocPipPrgLstCab.Value;
      //
      Controle := UMyMod.BPGS1I32('ospipmon', 'Controle', '', '', tsPos, stIns, 0);
      UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'ospipmon', False, [
      'Codigo', 'PipCad', 'Ordem',
      'Reordem', 'PrgLstCab', 'OSFMCbMae'], [
      'Controle'], [
      'Controle'], [
      Codigo, PipCad, Ordem,
      Reordem, PrgLstCab, OSFMCbMae], [
      Controle], [
      Controle], True, 'Insercoes');
  end;
*)
var
  SiapterCad, CtOri, OSFMCbMae, PipCad, Ordem, PrgLstCab, Dias, DdRota: Integer;
  Ciclos: Double;
  SohInicial: Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv +
  'Preparando monitoramentos.');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDistin, DModG.MyPID_DB, [
  'SELECT DISTINCT SubTab ',
  'FROM ' + F_MoniNewOSs,
  'WHERE SeqNovaOS=' + Geral.FF0(QrNovasOSsSeqNovaOS.Value),
  'AND Tabela=' + Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_MONIT), // CUIDADO!!! Aqui Muda!!!
  'ORDER BY SubTab ',
  '']);
  SohInicial := (QrDistin.RecordCount = 1)
    and (QrDistinSubTab.Value = CO_FORMUL_FILHA_INICIAL);
  if SohInicial then
  begin
    // Para n�o adicionar PIPs j� em monitoramento de cruzeiro a toa na OS
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
    'SohInicial'], [
    'Codigo'], [
    1], [
    Codigo], True);
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNewF_M, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + F_MoniNewOSs,
  'WHERE SeqNovaOS=' + Geral.FF0(QrNovasOSsSeqNovaOS.Value),
  'AND Tabela=' + Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_MONIT), // CUIDADO!!! Aqui Muda!!!
  'ORDER BY Codigo, Controle, Conta ',
  '']);
  //
  QrNewF_M.First;
  while not QrNewF_M.Eof do
  begin
    CtOri   := QrNewF_MConta.Value;
    FTxtF_M := 'Incluindo monitoramento ID Origem: ' + Geral.FF0(CtOri) + '. ';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv + FTxtF_M);
    //
    SiapterCad := QrNewF_MSiapterCad.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocPIP, Dmod.MyDB, [
    'SELECT DISTINCT pip.*, ',
    // 2014-04-22
    'omc.PerioDd, omc.DdPostero ',
    // FIM 2014-04-22
    'FROM pipcad pip ',
    'LEFT JOIN osmoncab omc ON omc.PipCad=pip.Codigo ',
    'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo ',
    'WHERE cab.SiapTerCad=' + Geral.FF0(SiapTerCad),
    'AND pip.DtaDesativ < "1900-01-01" ',
    // Se for so inicial entao pega de apenas um osmoncab
    Geral.ATS_If(SohInicial, ['AND omc.Conta=' + Geral.FF0(QrNewF_MConta.Value)]),
    'ORDER BY pip.Ordem, pip.Codigo ',
    '']);
    QrLocPIP.First;
    while not QrLocPIP.Eof do
    begin
      // 2014-04-22 Visto com Eslauco!!
      Dias := Trunc(QrNewF_MDtaOSCalc.Value) - Trunc(DEP);
      DdRota := Dias mod QrLocPipDdPostero.Value;
      // 2014-08-05 nao adiciona PIP com intervalo inicial de 14 dias!!!
      //if DdRota < Dmod.QrOpcoesBugsDdRotaFlh.Value then
      if (DdRota < Dmod.QrOpcoesBugsDdRotaFlh.Value) or (SohInicial) then
      // FIM 2014-08-05
      // FIM 2014-04-22
      begin
        // FIM 2014-04-22
        // Aqui pode dar problema se n�o estiver bem setado!
        // O problema vai acontecer na atualiza��o da OSMonCab.Conta!
        // > Seta em procedure TDmod.AtualizaPIP(Codigo: Integer);
        OSFMCbMae := QrLocPIPOSMonCab.Value;
        //
        //
        PipCad    := QrLocPipCodigo.Value;
        Ordem     := QrLocPipOrdem.Value;
        PrgLstCab := QrLocPipPrgLstCab.Value;
        //
        OSApp_PF.IncluiOSMonPipAtual(Codigo, SiapterCad, OSFMCbMae,
          PipCad, Ordem, PrgLstCab);
        //
      end;
      QrLocPIP.Next;
    end;
    //
    QrNewF_M.Next;
  end;
  Result := True;
end;
}

function TFmOSFlhGer.GeraOSMonCab(Codigo: Integer; DEP: TDateTime): Boolean;
(*
  / n�o tem como fazer asim!
  / se houver adi��o de PIP em outra OS, ter� grande chance de haver datas
  / desencontradas de monitoramento!
  //
  function IncluiOSMonPipAtual(SiapterCad, OSFMCbMae: Integer): Boolean;
  var
    Controle, PipCad, Ordem, Reordem, PrgLstCab: Integer;
  begin
      Controle       := 0;
      PipCad         := QrLocPipCodigo.Value;
      Ordem          := QrLocPip.RecNo;
      Reordem        := Ordem;
      PrgLstCab      := QrLocPipPrgLstCab.Value;
      //
      Controle := UMyMod.BPGS1I32('ospipmon', 'Controle', '', '', tsPos, stIns, 0);
      UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'ospipmon', False, [
      'Codigo', 'PipCad', 'Ordem',
      'Reordem', 'PrgLstCab', 'OSFMCbMae'], [
      'Controle'], [
      'Controle'], [
      Codigo, PipCad, Ordem,
      Reordem, PrgLstCab, OSFMCbMae], [
      Controle], [
      Controle], True, 'Insercoes');
  end;
*)
var
  // Dias, DdRota
  PipCad, Ordem, PrgLstCab, OSFMCbMae, CtOri, SiapterCad: Integer;
  //Ciclos: Double;
  SohInicial: Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv +
  'Preparando monitoramentos.');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDistin, DModG.MyPID_DB, [
  'SELECT DISTINCT SubTab ',
  'FROM ' + F_MoniNewOSs,
  'WHERE SeqNovaOS=' + Geral.FF0(QrNovasOSsSeqNovaOS.Value),
  'AND Tabela=' + Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_MONIT), // CUIDADO!!! Aqui Muda!!!
  'ORDER BY SubTab ',
  '']);
  SohInicial := (QrDistin.RecordCount = 1)
    and (QrDistinSubTab.Value = CO_FORMUL_FILHA_INICIAL);
  if SohInicial then
  begin
    // Para n�o adicionar PIPs j� em monitoramento de cruzeiro a toa na OS
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
    'SohInicial'], [
    'Codigo'], [
    1], [
    Codigo], True);
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNewF_M, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + F_MoniNewOSs,
  'WHERE SeqNovaOS=' + Geral.FF0(QrNovasOSsSeqNovaOS.Value),
  'AND Tabela=' + Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_MONIT), // CUIDADO!!! Aqui Muda!!!
  'ORDER BY Codigo, Controle, Conta ',
  '']);
  //
  QrNewF_M.First;
  while not QrNewF_M.Eof do
  begin
    CtOri   := QrNewF_MConta.Value;
    FTxtF_M := 'Incluindo monitoramento ID Origem: ' + Geral.FF0(CtOri) + '. ';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv + FTxtF_M);
    //
    SiapterCad := QrNewF_MSiapterCad.Value;
    //
{
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocPIP, Dmod.MyDB, [
    'SELECT DISTINCT pip.*, ',
    // 2014-04-22
    'omc.PerioDd, omc.DdPostero ',
    // FIM 2014-04-22
    'FROM pipcad pip ',
    'LEFT JOIN osmoncab omc ON omc.PipCad=pip.Codigo ',
    'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo ',
    'WHERE cab.SiapTerCad=' + Geral.FF0(SiapTerCad),
    'AND pip.DtaDesativ < "1900-01-01" ',
    // Se for so inicial entao pega de apenas um osmoncab
    Geral.ATS_If(SohInicial, ['AND omc.Conta=' + Geral.FF0(QrNewF_MConta.Value)]),
    'ORDER BY pip.Ordem, pip.Codigo ',
    '']);
    QrLocPIP.First;
    while not QrLocPIP.Eof do
    begin
      // 2014-04-22 Visto com Eslauco!!
      Dias := Trunc(QrNewF_MDtaOSCalc.Value) - Trunc(DEP);
      DdRota := Dias mod QrLocPipDdPostero.Value;
      // 2014-08-05 nao adiciona PIP com intervalo inicial de 14 dias!!!
      //if DdRota < Dmod.QrOpcoesBugsDdRotaFlh.Value then
      if (DdRota < Dmod.QrOpcoesBugsDdRotaFlh.Value) or (SohInicial) then
      // FIM 2014-08-05
      // FIM 2014-04-22
      begin
        // FIM 2014-04-22
        // Aqui pode dar problema se n�o estiver bem setado!
        // O problema vai acontecer na atualiza��o da OSMonCab.Conta!
        // > Seta em procedure TDmod.AtualizaPIP(Codigo: Integer);
        OSFMCbMae := QrLocPIPOSMonCab.Value;
        //
        //
        PipCad    := QrLocPipCodigo.Value;
        Ordem     := QrLocPipOrdem.Value;
        PrgLstCab := QrLocPipPrgLstCab.Value;
        //
        OSApp_PF.IncluiOSMonPipAtual(Codigo, SiapterCad, OSFMCbMae,
          PipCad, Ordem, PrgLstCab);
        //
      end;
      QrLocPIP.Next;
    end;
    //}
    UnDMkDAC_PF.AbreMySQLQuery0(QrPIP, Dmod.MyDB, [
    'SELECT pip.OSMonCab, pip.Codigo, pip.Ordem, ',
    'pip.PrgLstCab ',
    'FROM pipcad pip ',
    'LEFT JOIN osmoncab omc ON omc.PipCad=pip.Codigo ',
    'WHERE omc.Conta=' + Geral.FF0(QrNewF_MConta.Value),
    '']);
    OSFMCbMae := QrPIPOSMonCab.Value;
    PipCad    := QrPIPCodigo.Value;
    Ordem     := QrPIPOrdem.Value;
    PrgLstCab := QrPIPPrgLstCab.Value;
    OSApp_PF.IncluiOSMonPipAtual(Codigo, SiapterCad, OSFMCbMae,
      PipCad, Ordem, PrgLstCab);
    //
    QrNewF_M.Next;
  end;
  Result := True;
end;
// FIM 2014-08-07

function TFmOSFlhGer.GeraOSSrv(const Codigo: Integer; var Controle: Integer): Boolean;
var
  //Detalhes: String;
  DesServico, GarantiaDd, HrEvacuar, Autorizado(*, TudoFeitoM, TudoFeitoA,
  HowEstav, HowOrige, MoniDdTotl, MoniDdIntv*): Integer;
  HrExecutar(*, ValCalc, ValInfo, ValDesc, ValTota*): Double;
begin
  Result := False;
  FTxtSrv := '. Incluindo servi�o do ID origem: ' + Geral.FF0(QrNovosSrvControle.Value) + '. ';
  MyObjects.Informa2(LaAviso1, LaAviso2, True, FTextoCriacao + FTxtSrv);
  //
  ReopenOSSrv(QrNovosSrvControle.Value);
  //
  //Codigo         := ;
  Controle       := 0;
  DesServico     := QrOSSrvDesServico.Value;
  GarantiaDd     := QrNovosSrvDdPostero.Value + Dmod.QrOpcoesBugsOSPrvXtraD.Value;
  HrEvacuar      := QrOSSrvHrEvacuar.Value;
  HrExecutar     := QrOSSrvHrExecutar.Value;
  //ValCalc        := 0;
  //ValInfo        := 0;
  //ValDesc        := 0;
  //ValTota        := 0;
  Autorizado     := 1; // Sim
  //Detalhes       := '';
  //TudoFeitoM     := 0;
  //TudoFeitoA     := 0;
  //HowEstav       := 0;
  //HowOrige       := 0;
  //MoniDdTotl     := 0;
  //MoniDdIntv     := 0;

  //
  Controle := UMyMod.BPGS1I32('ossrv', 'Controle', '', '', tsPos, stIns, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ossrv', False, [
  'Codigo', 'DesServico', 'GarantiaDd',
  'HrEvacuar', 'HrExecutar', (*'ValCalc',
  'ValInfo', 'ValDesc', 'ValTota',*)
  'Autorizado'(*, 'Detalhes', 'TudoFeitoM',
  'TudoFeitoA', 'HowEstav', 'HowOrige',
  'MoniDdTotl', 'MoniDdIntv'*)], [
  'Controle'], [
  Codigo, DesServico, GarantiaDd,
  HrEvacuar, HrExecutar, (*ValCalc,
  ValInfo, ValDesc, ValTota,*)
  Autorizado(*, Detalhes, TudoFeitoM,
  TudoFeitoA, HowEstav, HowOrige,
  MoniDdTotl, MoniDdIntv*)], [
  Controle], True) then
  begin
    Result := True;
    //
  end;
end;

procedure TFmOSFlhGer.MostraEtapa2();
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando etapa 2');
  ReopenMoniNewOSs();
  GBEtapa2.Visible := True;
  GBEtapa1.Visible := False;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
  'Pronto para gera��o! Imprima para visualizar antes de gerar ou "Gera OSs" para gerar as OSs dos itens listados abaixo!');
end;

procedure TFmOSFlhGer.MostraEtapa3();
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando etapa 3');
  PB1.Position := 0;
  GBEtapa3.Visible := True;
  GBEtapa2.Visible := False;
  DmModOS.VerificaMulServico(GBAvisos1, LaAviso1, LaAviso2, PB1);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmOSFlhGer.MsgFaltaDef(DescriCampo: String);
begin
  Geral.MB_Aviso('Forma de defini��o de: ' + sLineBreak + DescriCampo + sLineBreak +
  'Lugar: ' + Geral.FF0(QrOSCabSiapTerCad.Value) + sLineBreak +
  'Empresa: ' + Geral.FF0(QrOSCabEMpresa.Value));
end;

function TFmOSFlhGer.ObtemConjuntoDeOperacoesDeItens(): Integer;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOper, DModG.MyPID_DB, [
  'SELECT DISTINCT Tabela ',
  'FROM ' + F_MoniNewOSs,
  'WHERE SeqNovaOS=' + Geral.FF0(QrNovasOSsSeqNovaOS.Value),
  //'AND Codigo=' + Geral.FF0(QrNovasOSsCodigo.Value),
  //'AND DtaOSReal=" ' + Geral.FDT(QrNovasOSsDtaOSReal.Value, 1) + '" ',
  '']);
  if QrOper.Locate('Tabela', CO_FORMUL_FILHA_MONIT_EH_FILHA, []) then
    Result := Result + CO_OS_OPERACAO_SERVICO_DEDETIZACAO; // 1 > Dedetiza��o
  if QrOper.Locate('Tabela', CO_FORMUL_FILHA_MONIT_EH_MONIT, []) then
    Result := Result + CO_OS_OPERACAO_SERVICO_MONITORAMENTO; // 4 > Monitoramento
end;

procedure TFmOSFlhGer.OrganizaRotas();
const
  TxtBase = 'Preparando: Organizando rotas facilitadoras.';
var
  DtaOSReal, TxtLug, TxtDta: String;
  IDItZ, GrupoOS: Integer;
  MaxData: TDate;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TxtBase);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocais, DModG.MyPID_DB, [
  'SELECT DISTINCT Cliente, SiapTerCad, Empresa ',
  'FROM ' + F_MoniNewOSs,
  // Colocar DdPostero no ORDER BY para "s�" alterar as datas com intervalos mais longos!
  'ORDER BY Cliente, SiapTerCad, DdPostero ',
  '']);
  //
  PB1.Position := 0;
  PB1.Max := QrLocais.RecordCount;
  QrLocais.First;
  while not QrLocais.Eof do
  begin
    TxtLug := ' Lugar ' + Geral.FF0(QrLocaisSiapTerCad.Value) + '.';
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, TxtBase + TxtLug);
    while ReopenDatas(QrLocaisEmpresa.Value, QrLocaisCliente.Value, QrLocaisSiapTerCad.Value) do
    begin
      QrDatas.First;
      DtaOSReal := Geral.FDT(QrDatasDtaOSUtil.Value, 1);
      TxtDta := ' Data ' + DtaOSReal + '.';
      MyObjects.Informa2(LaAviso1, LaAviso2, True, TxtBase + TxtLug + TxtDta);
      //
      MaxData := QrDatasDtaOSUtil.Value + Dmod.QrOpcoesBugsDdRotaFlh.Value;
      GrupoOS := FGrupoOS;
      while not QrDatas.Eof do
      begin
        IDItZ   := QrDatasIDItZ.Value;
        if QrDatasDtaOSUtil.Value <= MaxData then
        begin
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_MoniNewOSs, False, [
          'DtaOSReal', 'GrupoOS'], [
          'IDItZ'], [
          DtaOSReal, GrupoOS], [
          IDItZ], False);
          //
          QrDatas.Next;
        end else
          QrDatas.Last;
      end;
    end;
    //
    QrLocais.Next;
  end;
end;

{
function TFmOSFlhGer.OSMonCab_Incompletas(): Boolean;
var
  Msg: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrOMC, Dmod.MyDB, [
  'SELECT Codigo, Controle, Conta, Nome ',
  'FROM osmoncab ',
  'WHERE PipCad=0 ',
  (*'/* ',
  'OR PerioDd=0 ',
  'OR DdPostero=0 ',
  '*/ '*)
  'ORDER BY Codigo, Controle, Conta ',
  '']);
  //
  if QrErrOMC.RecordCount > 0 then
  begin
    Result := True;
    //
    if QrErrOMC.RecordCount = 1 then
      Msg := 'Existe uma f�rmula de monitoramento incompleta!'
    else
      Msg := 'Existem ' + Geral.FF0(QrErrOMC.RecordCount) +
      ' f�rmulas de monitoramento incompletas!';
    if Geral.MB_Pergunta(Msg + sLineBreak + 'Deseja ver o relat�rio?') = ID_YES then
    begin
      MyObjects.frxDefineDataSets(frxGER_OSERV_030_002_B, [
      DmodG.frxDsDono,
      frxDsErrOMC
      ]);
      MyObjects.frxMostra(frxGER_OSERV_030_002_B, 'F�rmulas de Monitoramento Incompletas');
    end;
  end else
    Result := False;
end;
}

procedure TFmOSFlhGer.OS1Click(Sender: TObject);
var
  Codigo, Grupo, SiapTerCad, Opcao: Integer;
  Aba: Boolean;
begin
  if TFmOSFlhGer(Self).Owner is TApplication then
    Aba := False
  else
    Aba := True;
  //
  Codigo     := QrFrmFilCodigo.Value;
  Grupo      := QrFrmFilGrupo.Value;
  SiapTerCad := QrFrmFilSiapTerCad.Value;
  Opcao      := QrFrmFilOpcao.Value;
  //
  FmPrincipal.MostraOSCab(Aba, Codigo, Grupo, SiapTerCad, Opcao);
  //
  ReopenFrmFil();
end;

function TFmOSFlhGer.OSxxx_Incompletas(): Boolean;
var
  Msg: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIncompl, Dmod.MyDB, [
  'SELECT xxx.Codigo, xxx.Controle, xxx.Conta, xxx.DdPostero, ',
  'cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw, ',
  'xxx.IDIts IDSuperior, ',
  Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_FILHA) + ' Tabela, ',
  '"' + CO_FORMUL_FILHA_MONIT_TXT_FILHA + '" SIGLA_TAB, ',
  '"N�O" FaltaPip, ',
  'xxx.EmisUltDta, xxx.Periodd, xxx.ExtenDd, xxx.EmisStatus ',
  'FROM osfrmflhcb xxx ',
  'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo ',
  'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat ',
  'LEFT JOIN cunscad cun ON cun.Codigo=cab.Entidade ',
  DmModOS.SQL_Filtr(),
  'AND (cab.NumContrat=0 AND xxx.Periodd=0) ',       // ExtenDd ???
  ' ',
  'UNION ',
  ' ',
  'SELECT xxx.Codigo, xxx.Controle, xxx.Conta, xxx.DdPostero, ',
  'cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw, ',
  'xxx.Conta IDSuperior, ',
  Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_MONIT) + ' Tabela, ',
  '"' + CO_FORMUL_FILHA_MONIT_TXT_MONIT + '" SIGLA_TAB, ',
  'IF(xxx.PipCad=0, "SIM", "N�O") FaltaPip, ',
  'xxx.EmisUltDta, xxx.Periodd, xxx.ExtenDd, xxx.EmisStatus ',
  'FROM osmoncab xxx ',
  'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo ',
  'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat ',
  'LEFT JOIN cunscad cun ON cun.Codigo=cab.Entidade ',
  DmModOS.SQL_Filtr(),
  'AND  ',
  '( ',
  '  (cab.NumContrat=0 AND xxx.Periodd=0) ',      // ExtenDd ???
  '  OR ',
  '  xxx.PipCad=0 ',
  ') ',
  'ORDER BY Codigo, Controle, Conta ',
  '']);
  if QrIncompl.RecordCount > 0 then
  begin
    Result := True;
    //
    if QrIncompl.RecordCount = 1 then
      Msg := 'Existe uma f�rmula m�e incompleta!'
    else
      Msg := 'Existem ' + Geral.FF0(QrIncompl.RecordCount) +
      ' f�rmulas m�e incompletas!';
    if Geral.MB_Pergunta(Msg + sLineBreak + 'Deseja ver o relat�rio?') = ID_YES then
    begin
      MyObjects.frxDefineDataSets(frxGER_OSERV_030_002_C, [
      DmodG.frxDsDono,
      frxDsIncompl
      ]);
      MyObjects.frxMostra(frxGER_OSERV_030_002_C, 'F�rmulas "M�e" Incompletas');
    end;
  end else
    Result := False;
end;

procedure TFmOSFlhGer.PMGrid1Popup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrFrmFil.State <> dsInactive) and (QrFrmFil.RecordCount > 0);
  //
  Cliente1.Enabled                                         := Enab;
  OS1.Enabled                                              := Enab;
  AlteradatadaltimaemissodeOSsfilhas1.Enabled              := Enab;
end;

procedure TFmOSFlhGer.QrFrmFilAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrFrmFil.RecordCount);
end;

procedure TFmOSFlhGer.QrFrmFilBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmOSFlhGer.QrFrmFilEmisUltDtaGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrFrmFilEmisUltDta.Value, 3);
end;

procedure TFmOSFlhGer.QrMoniNewOSsEmisUltDtaGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrMoniNewOSsEmisUltDta.Value, 2);
end;

procedure TFmOSFlhGer.QrNovasOSsAfterOpen(DataSet: TDataSet);
begin
  FTotalOSs := Geral.FF0(QrNovasOSs.RecordCount);
end;

function TFmOSFlhGer.ReopenDatas(Empresa, Cliente, Lugar: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, DModG.MyPID_DB, [
  'SELECT *  ',
  'FROM ' + F_MoniNewOSs,
  'WHERE DtaOSReal < 2 ',
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND Cliente=' + Geral.FF0(Cliente),
  'AND SiapTerCad=' + Geral.FF0(Lugar),
  'ORDER BY DtaOSUtil ',
  '']);
  //
  FGrupoOS := FGrupoOS + 1;
  Result   := QrDatas.RecordCount > 0;
end;

procedure TFmOSFlhGer.ReopenFrmFil(IDSuperior: Integer = 0; Tabela: LargeInt = 0);
var
  Txt: String;
  Tipos, SQL_ANTER, SQL_LJOIN: String;
begin
  Tipos :=
    dmkPF.ArrayToTexto('xxx.EmisStatus', 'NO_EmisStatus', pvPos, True, sListaEmisStatus);
  //
  SQL_ANTER := Geral.ATS([
  'SELECT cab.Empresa, stf.DataSincOS, ',
  'xxx.Codigo, xxx.Controle, xxx.Conta, xxx.DdPostero, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,',
  Tipos,
  'cab.Opcao, cab.Grupo, ',
  'stc.Nome NO_STC, frm.Codigo CO_FORMULA, frm.Nome NO_FORMULA, cab.Entidade, ',
  'cab.SiapTerCad, cab.DtaExeFim, cab.NumContrat, ctr.DtaPrxRenw, ',
  'xxx.EmisUltDta, xxx.Periodd, xxx.ExtenDd, xxx.EmisStatus, ',
  '']);
  //
  SQL_LJOIN := Geral.ATS([
  'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo ',
  'LEFT JOIN formulas frm ON frm.Codigo=xxx.Formula ',
  'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade',
  'LEFT JOIN SiapTerCad stc ON stc.Codigo=cab.SiapTerCad',
  'LEFT JOIN SiapTerFlh stf ON stf.Codigo=stc.Codigo',
  'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat ',
  'LEFT JOIN cunscad cun ON cun.Codigo=cab.Entidade ',
  '']);
  //
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrmFil, Dmod.MyDB, [
  SQL_ANTER,
  //////////////  DIFERENTE  NAS TABELAS !!!  /////////////////////////////////
  Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_FILHA) + ' Tabela, ',
  '"' + CO_FORMUL_FILHA_MONIT_TXT_FILHA + '" SIGLA_TAB, ',
  'xxx.IDIts IDSuperior ',
  'FROM osfrmflhcb xxx ',
   /////////////////////////////////////////////////////////////////////////////
  SQL_LJOIN,
  DmModOS.SQL_Filtr(),
  ' ',
  'UNION ',
  ' ',
  SQL_ANTER,
  //////////////  DIFERENTE  NAS TABELAS !!!  /////////////////////////////////
  Geral.FF0(CO_FORMUL_FILHA_MONIT_EH_MONIT) + ' Tabela, ',
  '"' + CO_FORMUL_FILHA_MONIT_TXT_MONIT + '" SIGLA_TAB, ',
  'xxx.Conta IDSuperior ',
  'FROM osmoncab xxx ',
   /////////////////////////////////////////////////////////////////////////////
  SQL_LJOIN,
  DmModOS.SQL_Filtr(),
  '  ',
  'ORDER BY NO_CLIENTE, SIGLA_TAB ',
  '']);
  //
  //Geral.MB_SQL(Self, QrFrmFil);
  //
  {$IfNDef SNoti}
  if QrFrmFil.RecordCount > 0 then
    UnNotificacoesEdit.CriaNotificacaoPublica(Dmod.MyDB, ntfBugsOSsFilhas,
      DmodG.ObtemAgora(True))
  else
    UnNotificacoesEdit.ExcluiNotificacao(Dmod.MyDB, Integer(ntfBugsOSsFilhas));
  {$EndIf}
  if (IDSuperior <> 0) and (Tabela <> 0) then
    QrFrmFil.Locate('IDSuperior;Tabela', VarArrayOf([IDSuperior,Tabela]), []);
end;

procedure TFmOSFlhGer.ReopenMoniNewOSs();
var
  Txt: String;
  Tipos: String;
begin
  Tipos :=
    dmkPF.ArrayToTexto('mno.EmisStatus', 'NO_EmisStatus', pvPos, True, sListaEmisStatus);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMoniNewOSs, DModG.MyPID_DB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,',
  Tipos,
  'stc.Nome NO_STC, ctr.DtaPrxRenw, mno.* ',
  'FROM ' + F_MoniNewOSs + ' mno ',
  'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=mno.Codigo  ',
  'LEFT JOIN ' + TMeuDB + '.formulas frm ON frm.Codigo=mno.Formula  ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=cab.Entidade ',
  'LEFT JOIN ' + TMeuDB + '.SiapTerCad stc ON stc.Codigo=cab.SiapTerCad ',
  'LEFT JOIN ' + TMeuDB + '.contratos ctr ON ctr.Codigo=cab.NumContrat  ',
  'ORDER BY mno.SeqNovaOS, mno.DtaOSReal, mno.DtaOsCalc, mno.DdPostero, ',
  'mno.Codigo, mno.Controle, mno.Conta, mno.IDIts ',
  ' ']);
end;

procedure TFmOSFlhGer.ReopenOSAge(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSAge, Dmod.MyDB, [
  'SELECT age.* ',
  'FROM osage age ',
  'WHERE age.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TFmOSFlhGer.ReopenOSAlv(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSAlv, Dmod.MyDB, [
  'SELECT osa.Praga_Z ',
  'FROM osalv osa ',
  'WHERE osa.Controle=' + Geral.FF0(Controle),
  '']);
end;

procedure TFmOSFlhGer.ReopenOSCab(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCab, Dmod.MyDB, [
  'SELECT stc.PdrMntsMon, cab.*  ',
  'FROM oscab cab ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
  'WHERE cab.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TFmOSFlhGer.ReopenOSSrv(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSSrv, Dmod.MyDB, [
  'SELECT * FROM ossrv',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
end;

procedure TFmOSFlhGer.ReopenSiapTerFlh(SiapterCad, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapTerFlh, Dmod.MyDB, [
  'SELECT * ',
  'FROM siapterflh ',
  'WHERE Codigo=' + Geral.FF0(SiapTerCad),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TFmOSFlhGer.VerificaSeEhDiaUtil();
const
  Txt = 'Ajustando datas para dias �teis. ';
  Seg = 0;
  Ter = 0;
  Qua = 0;
  Qui = 0;
  Sex = 0;
var
  Qry: TmySQLQuery;
  Max, Cpl, DtaOSUtil, DiaSemTxt: String;
  IDItZ: Integer;
  //
  DOSU, DOSC, DtaOSCalc, Util: TDateTime;
  Dom, Sab, Feriado: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, Txt);
  Dom := Dmod.QrOpcoesBugsDdRotaDom.Value - 2;
  Sab := Dmod.QrOpcoesBugsDdRotaSab.Value - 1;
  if Dmod.QrOpcoesBugsDdRotaFer.Value = 0 then
    Feriado := -1
  else
    Feriado := 1;
  //
  Qry := TmySQLQuery.Create(DModG);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'SELECT *  ',
    'FROM ' + F_MoniNewOSs,
    'ORDER BY DtaOSCalc ',
    '']);
    Max := ' de ' + Geral.FF0(Qry.RecordCount);
    DtaOSCalc := 0;
    DtaOSUtil := '0000-00-00';
    Qry.First;
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    while not Qry.Eof do
    begin
      Cpl := IntToStr(Qry.RecNo);
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, Txt + Cpl + Max);
      //
      DOSC := Qry.FieldByName('DtaOSCalc').AsDateTime;
      if Int(DOSC) <> Int(DtaOSCalc) then
      begin
        DtaOSCalc := DOSC;
        UMyMod.ProximoDiaUtil(DOSC, Dom, Seg, Ter, Qua, Qui, Sex, Sab, Feriado,
        DOSU, DiaSemTxt);
        DtaOSUtil := Geral.FDT(DOSU, 1);
      end;
      IDItZ := Qry.FieldByName('IDItZ').AsInteger;
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_MoniNewOSs, False, [
      'DtaOSUtil', 'DiaSemTxt'], [
      'IDItZ'], [
      DtaOSUtil, DiaSemTxt], [
      IDItZ], False);
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

(*
Os seguintes contratos precisam nova defini��o de pr�xima renova��o ou rescis�o:
8
57
38




DDTUDO CONTROLE INTEGRADO DE PRAGAS



Cliente
1822
210

1822	ADUEM ASSOC DOS DOCENTES DA UNIVERSIDADE EST DE MARINGA	1696	ADUEM
210	PLAENGE EMPREENDIMENTOS LTDA	1724	BARRAC�O AV. PEDRO TAQUES

*)

(*Os seguintes contratos precisam nova defini��o de pr�xima renova��o ou rescis�o:
GeraOSMonCab();
SELECT PipCad
FROM osmoncab
WHERE Conta=1353
*)

end.
