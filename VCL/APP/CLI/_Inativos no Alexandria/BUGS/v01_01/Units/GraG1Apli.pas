unit GraG1Apli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkRichEdit, dmkRadioGroup, UnBugs_Tabs, Menus, dmkLabelRotate,
  UnDmkEnums, Variants;

type
  TFmGraG1Apli = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label10: TLabel;
    SbMarcas: TSpeedButton;
    EdNivel1: TdmkEdit;
    EdNome: TdmkEdit;
    RGImpInOrca: TdmkRadioGroup;
    EdMarca: TdmkEditCB;
    EdRegMinSaud: TdmkEdit;
    BtTexto: TBitBtn;
    QrMarcas: TmySQLQuery;
    QrMarcasControle: TIntegerField;
    QrMarcasNO_MARCA_FABR: TWideStringField;
    DsMarcas: TDataSource;
    CBMarca: TdmkDBLookupComboBox;
    Label111: TLabel;
    EdFoneCIT: TdmkEdit;
    EdFoneCEATOX: TdmkEdit;
    Label1: TLabel;
    QrG1PriAtiI: TmySQLQuery;
    DsG1PriAtiI: TDataSource;
    QrG1PriAtiICodigo: TIntegerField;
    QrG1PriAtiINO_GRU_QUIMICO: TWideStringField;
    EdToxicidade: TdmkEdit;
    Label3: TLabel;
    Label5: TLabel;
    EdConcentrac: TdmkEdit;
    Label6: TLabel;
    EdAcaoToxica: TdmkEdit;
    Label8: TLabel;
    EdAntidoto: TdmkEdit;
    Panel5: TPanel;
    ReObservacao: TdmkRichEdit;
    DBGGraG1PrPA: TDBGrid;
    QrGraG1PrPA: TmySQLQuery;
    DsGraG1PrPA: TDataSource;
    QrGraG1PrPANO_PRIATI: TWideStringField;
    QrGraG1PrPANivel1: TIntegerField;
    QrGraG1PrPAControle: TIntegerField;
    QrGraG1PrPAG1PriAtiI: TIntegerField;
    BtPriAti: TBitBtn;
    PMPriAti: TPopupMenu;
    IncluiPrincpioativo1: TMenuItem;
    AlteraPrincpioativo1: TMenuItem;
    Retiraprincpioativo1: TMenuItem;
    N1: TMenuItem;
    Cadastrodeprincpioativo1: TMenuItem;
    PnValCliDd: TPanel;
    Label2: TLabel;
    EdValCliDd: TdmkEdit;
    dmkLabelRotate1: TdmkLabelRotate;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    Label11: TLabel;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    Label12: TLabel;
    EdAtuNumLot: TdmkEdit;
    Label13: TLabel;
    EdAtuNumLaud: TdmkEdit;
    Label14: TLabel;
    EdNCM: TdmkEdit;
    SpeedButton2: TSpeedButton;
    Label17: TLabel;
    EdReferencia: TdmkEdit;
    Label15: TLabel;
    EdFrmApres: TdmkEditCB;
    CBFrmApres: TdmkDBLookupComboBox;
    SpeedButton3: TSpeedButton;
    QrFrmApres: TmySQLQuery;
    DsFrmApres: TDataSource;
    QrFrmApresCodigo: TIntegerField;
    QrFrmApresNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTextoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbMarcasClick(Sender: TObject);
    procedure EdNivel1Change(Sender: TObject);
    procedure BtPriAtiClick(Sender: TObject);
    procedure IncluiPrincpioativo1Click(Sender: TObject);
    procedure Retiraprincpioativo1Click(Sender: TObject);
    procedure AlteraPrincpioativo1Click(Sender: TObject);
    procedure PMPriAtiPopup(Sender: TObject);
    procedure Cadastrodeprincpioativo1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure CBUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNCMExit(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
  private
    { Private declarations }
    FGraBugs: TGraBugsServi;
    procedure ReopenGraG1PrPA(Controle: Integer);
    procedure InsAltPriAti(SQLType: TSQLType);
  public
    { Public declarations }
    FFrmNome, FTabNome: String;

    //
    procedure FiltraItensAExibir(GraBugs: TGraBugsServi);
  end;

  var
    FmGraG1Apli: TFmGraG1Apli;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, DmkDAC_PF, MyDBCheck, SelCod,
  ModuleGeral, ModProd, UnGrade_Jan, UnMySQLCuringa;

{$R *.DFM}

procedure TFmGraG1Apli.AlteraPrincpioativo1Click(Sender: TObject);
begin
  InsAltPriAti(stUpd);
end;

procedure TFmGraG1Apli.BtOKClick(Sender: TObject);

  function InsUpdG1PrAp(SQLType: TSQLType): Boolean;
  var
    RegMinSaud, Observacao, FoneCIT, FoneCEATOX, Toxicidade, Concentrac,
    AcaoToxica, Antidoto, AtuNumLot, AtuNumLaud, FrmApres: String;
    Nivel1, Marca, ImpInOrca, ValCliDd, UnidMed: Integer;
    //
    SQLCampos: TdmkArrStr;
    ValCampos: TdmkArrVar;
  begin
    Nivel1     := EdNivel1.ValueVariant;
    RegMinSaud := EdRegMinSaud.ValueVariant;
    Marca      := EdMarca.ValueVariant;
    ImpInOrca  := RGImpInOrca.ItemIndex;
    FoneCIT    := Geral.SoNumero_TT(EdFoneCIT.ValueVariant);
    FoneCEATOX := Geral.SoNumero_TT(EdFoneCEATOX.ValueVariant);
    Toxicidade := EdToxicidade.ValueVariant;
    Concentrac := EdConcentrac.ValueVariant;
    AcaoToxica := EdAcaoToxica.ValueVariant;
    Antidoto   := EdAntidoto.ValueVariant;
    Observacao := MyObjects.ObtemTextoRichEdit(Self, ReObservacao);
    AtuNumLot  := EdAtuNumLot.Text;
    AtuNumLaud := EdAtuNumLaud.Text;
    FrmApres   := EdFrmApres.ValueVariant;

    if EdUnidMed.ValueVariant <> 0 then
      UnidMed := QrUnidMedCodigo.value
    else
      UnidMed := 0;

    SQLCampos := Geral.ATA_Str([
      'RegMinSaud', 'Marca', 'ImpInOrca',
      'FoneCIT', 'FoneCEATOX', 'Toxicidade',
      'Concentrac', 'AcaoToxica', 'Antidoto',
      'Observacao', 'AtuNumLot', 'AtuNumLaud',
      'FrmApres']);

    ValCampos := Geral.ATA_Var([
      RegMinSaud, Marca, ImpInOrca,
      FoneCIT, FoneCEATOX, Toxicidade,
      Concentrac, AcaoToxica, Antidoto,
      Observacao, AtuNumLot, AtuNumLaud,
      FrmApres]);

    case FGraBugs of
      gbsAplica  : ; // nada
      gbsMonitora:
      begin
        // Somente Monitoramento
        ValCliDd  := EdValCliDd.ValueVariant;
        SQLCampos := Geral.ASA_Str(SQLCampos, ['ValCliDd']);
        ValCampos := Geral.ASA_Var(ValCampos, [ ValCliDd ]);
      end;
    end;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False,
      ['UnidMed'], ['Nivel1'], [UnidMed], [Nivel1], True);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTabNome, False,
                SQLCampos, ['Nivel1'], ValCampos, [Nivel1], True);
  end;

var
  Nivel1: Integer;
  Nome, NCM, Referencia: String;
  SQLType: TSQLType;
begin
  Nome       := EdNome.ValueVariant;
  NCM        := EdNCM.ValueVariant;
  Referencia := EdReferencia.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Nivel1 := EdNivel1.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False,
    ['Nome', 'NCM', 'Referencia'], ['Nivel1'],
    [Nome, NCM, Referencia], [Nivel1], True) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Nivel1 ',
      'FROM ' + FTabNome,
      'WHERE Nivel1=' + Geral.FF0(Nivel1),
      '']);

    if Nivel1 = Dmod.QrAux.FieldByName('Nivel1').AsInteger then
      SQLType := stUpd
    else
      SQLType := stIns;

    if InsUpdG1PrAp(SQLType) then
    begin
      {
      LocCod(Nivel1,Nivel1);
      ImgTipo.SQLType := stlok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
      }
      Close;
    end;
  end;
end;

procedure TFmGraG1Apli.BtPriAtiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPriAti, BtPriAti);
end;

procedure TFmGraG1Apli.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraG1Apli.BtTextoClick(Sender: TObject);
begin
  MyObjects.EditaTextoRichEdit(REObservacao, 10, 'Tahoma', []);
end;

procedure TFmGraG1Apli.Cadastrodeprincpioativo1Click(Sender: TObject);
begin
  FmPrincipal.PrincpiosAtivos1Click(Self);
end;

procedure TFmGraG1Apli.CBUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraG1Apli.EdNCMExit(Sender: TObject);
begin
  EdNCM.Text := Geral.FormataNCM(EdNCM.Text);
end;

procedure TFmGraG1Apli.EdNivel1Change(Sender: TObject);
begin
  ReopenGraG1PrPA(0);
end;

procedure TFmGraG1Apli.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraG1Apli.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraG1Apli.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmGraG1Apli.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmGraG1Apli.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraG1Apli.FiltraItensAExibir(GraBugs: TGraBugsServi);
begin
  FGraBugs := GraBugs;
  case GraBugs of
    gbsAplica  : FTabNome := 'grag1prap';
    gbsMonitora: FTabNome := 'grag1prmo';
    else FTabNome := 'grag1pr??';
  end;
  //
  case GraBugs of
    gbsAplica  : FFrmNome := 'Aplica��o';
    gbsMonitora: FFrmNome := 'Monitoramento';
    else FFrmNome := '? ? ? ? ?';
  end;
  PnValCliDd.Visible := GraBugs = gbsMonitora;
end;

procedure TFmGraG1Apli.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraG1Apli.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrMarcas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrG1PriAtiI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFrmApres, Dmod.MyDB);
end;

procedure TFmGraG1Apli.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], 'PRD-GRUPO-023 :: Produtos de ' + FFrmNome, True, taCenter, 2, 10, 20);
end;

procedure TFmGraG1Apli.IncluiPrincpioativo1Click(Sender: TObject);
begin
  InsAltPriAti(stIns);
end;

procedure TFmGraG1Apli.InsAltPriAti(SQLType: TSQLType);
const
  Aviso   = '...';
  Caption = 'Adi��o de Princ�pio Ativo';
  Prompt  = 'Informe o princ�pio ativo';
  Campo   = 'Descricao';
var
  Cod: Variant;
  Codigo, Nivel1, Controle, G1PriAtiI: Integer;
begin
  Cod := DBCheck.EscolheCodigoUnico(Aviso, Caption, Prompt, nil, nil, Campo, 0, [
    'SELECT pai.Codigo, CONCAT( ',
    '  pai.Nome, ',
    '  " [", ',
    '  IF(pag.Nome IS NULL, "", pag.Nome), ',
    '  "]") ' + Campo,
    'FROM g1priatii pai ',
    'LEFT JOIN g1priatig pag ON pag.Codigo=pai.NivSup ',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, False);
  if Cod <> Null then
  begin
    Codigo := Cod;
    if Codigo <> 0 then
    begin
      Controle  := QrGraG1PrPAControle.Value;
      Nivel1    := EdNivel1.ValueVariant;
      Controle  := UMyMod.BPGS1I32('grag1prpa', 'Controle', '', '', tsPos, SQLType, Controle);
      G1PriAtiI := VAR_SELCOD;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'grag1prpa', False, [
        'Nivel1', 'G1PriAtiI'], [
        'Controle'], [
        Nivel1, G1PriAtiI], [
        Controle], True)
      then
        ReopenGraG1PrPA(Controle);
    end;
  end;
end;

procedure TFmGraG1Apli.PMPriAtiPopup(Sender: TObject);
begin
  IncluiPrincpioativo1.Enabled := EdNivel1.ValueVariant <> 0;
  MyObjects.HabilitaMenuItemItsUpd(AlteraPrincpioativo1, QrGraG1PrPA);
  MyObjects.HabilitaMenuItemItsDel(Retiraprincpioativo1, QrGraG1PrPA);
end;

procedure TFmGraG1Apli.ReopenGraG1PrPA(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraG1PrPA, Dmod.MyDB, [
    'SELECT IF(pai.NivSup=0, pai.Nome, CONCAT(pai.Nome, ',
    '" [", pag.Nome, "]")) NO_PRIATI, ppa.* ',
    'FROM grag1prpa ppa ',
    'LEFT JOIN g1priatii pai ON pai.Codigo=ppa.G1PriAtiI ',
    'LEFT JOIN g1priatig pag ON pag.Codigo=pai.NivSup ',
    'WHERE Nivel1 <> 0 ',
    'AND Nivel1=' + Geral.FF0(EdNivel1.ValueVariant),
    '']);

  if Controle <> 0 then
    QrGraG1PrPA.Locate('Controle', Controle, []);
end;

procedure TFmGraG1Apli.Retiraprincpioativo1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrGraG1PrPA, DBGGraG1PrPA,
    'grag1prpa', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmGraG1Apli.SbMarcasClick(Sender: TObject);
var
  Marca: Integer;
begin
  VAR_CADASTRO2 := 0;
  Marca         := EdMarca.ValueVariant;

  FmPrincipal.MostraFormGraFabCad(Marca);

  if VAR_CADASTRO2 <> 0 then
    UMyMod.SetaCodigoPesquisado(EdMarca, CBMarca, QrMarcas, VAR_CADASTRO2, 'Controle');
end;

procedure TFmGraG1Apli.SpeedButton1Click(Sender: TObject);
var
  UnidMed: Integer;
begin
  VAR_CADASTRO := 0;
  UnidMed      := EdUnidMed.ValueVariant;
  //
  Grade_Jan.MostraFormUnidMed(UnidMed);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    CBUnidMed.SetFocus;
  end;
end;

procedure TFmGraG1Apli.SpeedButton2Click(Sender: TObject);
var
  NCMSel: String;
begin
  VAR_CADASTRO := 0;
  NCMSel       := Grade_Jan.MostraFormClasFisc();
  //
  if NCMSel <> '' then
    EdNCM.Texto := NCMSel;
end;

procedure TFmGraG1Apli.SpeedButton3Click(Sender: TObject);
var
  FrmApres: Integer;
begin
  VAR_CADASTRO := 0;
  FrmApres     := EdMarca.ValueVariant;

  FmPrincipal.MostraFrmApres(FrmApres);

  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdFrmApres, CBFrmApres, QrFrmApres, VAR_CADASTRO, 'Codigo');
end;

end.
