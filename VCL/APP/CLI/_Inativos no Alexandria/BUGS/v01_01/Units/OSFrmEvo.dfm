object FmOSFrmEvo: TFmOSFrmEvo
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-023 :: Aplica'#231#227'o - Progresso do Servi'#231'o'
  ClientHeight = 342
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 402
        Height = 32
        Caption = 'Aplica'#231#227'o - Progresso do Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 402
        Height = 32
        Caption = 'Aplica'#231#227'o - Progresso do Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 402
        Height = 32
        Caption = 'Aplica'#231#227'o - Progresso do Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 624
    Height = 180
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 180
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 37
        Width = 624
        Height = 143
        Align = alClient
        TabOrder = 0
        object Label52: TLabel
          Left = 16
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Data:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label1: TLabel
          Left = 196
          Top = 16
          Width = 34
          Height = 13
          Caption = '% feito:'
        end
        object Label2: TLabel
          Left = 16
          Top = 56
          Width = 66
          Height = 13
          Caption = 'Observa'#231#245'es:'
        end
        object Label3: TLabel
          Left = 124
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Hora:'
          Color = clBtnFace
          ParentColor = False
        end
        object TPDataHora: TdmkEditDateTimePicker
          Left = 16
          Top = 32
          Width = 108
          Height = 21
          Date = 0.639644131944805900
          Time = 0.639644131944805900
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataHora'
          UpdCampo = 'DataHora'
          UpdType = utYes
        end
        object EdDataHora: TdmkEdit
          Left = 124
          Top = 32
          Width = 60
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          QryCampo = 'DataHora'
          UpdCampo = 'DataHora'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPercFeito: TdmkEdit
          Left = 196
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '100'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'PercFeito'
          UpdCampo = 'PercFeito'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object MeTexto: TdmkMemo
          Left = 16
          Top = 72
          Width = 593
          Height = 61
          TabOrder = 3
          QryCampo = 'Texto'
          UpdCampo = 'Texto'
          UpdType = utYes
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 624
        Height = 37
        Align = alTop
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        object Label7: TLabel
          Left = 8
          Top = 12
          Width = 18
          Height = 13
          Caption = 'OS:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label4: TLabel
          Left = 216
          Top = 12
          Width = 64
          Height = 13
          Caption = 'ID Aplica'#231#227'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label5: TLabel
          Left = 92
          Top = 12
          Width = 53
          Height = 13
          Caption = 'ID Servi'#231'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdit6: TDBEdit
          Left = 284
          Top = 8
          Width = 56
          Height = 21
          DataField = 'Conta'
          DataSource = DsOSFrmCab
          TabOrder = 1
        end
        object DBEdit5: TDBEdit
          Left = 340
          Top = 8
          Width = 268
          Height = 21
          DataField = 'Nome'
          DataSource = DsOSFrmCab
          TabOrder = 2
        end
        object DBEdit8: TDBEdit
          Left = 148
          Top = 8
          Width = 64
          Height = 21
          DataField = 'Controle'
          DataSource = DsOSFrmCab
          TabOrder = 3
        end
        object DBEdit15: TDBEdit
          Left = 32
          Top = 8
          Width = 56
          Height = 21
          DataField = 'Codigo'
          DataSource = DsOSFrmCab
          TabOrder = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 228
    Width = 624
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 272
    Width = 624
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 478
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOSFrmCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle, Conta, Nome '
      'FROM osfrmcab'
      'WHERE Conta=:P0')
    Left = 12
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmCabConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOSFrmCab: TDataSource
    DataSet = QrOSFrmCab
    Left = 40
    Top = 16
  end
end
