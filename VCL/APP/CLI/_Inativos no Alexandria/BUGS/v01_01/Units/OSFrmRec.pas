unit OSFrmRec;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, UnAppListas, UnBugs_Tabs,
  UnDmkEnums, UnProjGroup_Consts, Vcl.Menus;

type
  THackDBGrid = class(TDBGrid);
  TFmOSFrmRec = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    DsOSSrv: TDataSource;
    QrFormulas: TmySQLQuery;
    DsFormulas: TDataSource;
    GBEdita: TGroupBox;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrEquipAplic: TmySQLQuery;
    DsEquipAplic: TDataSource;
    Panel5: TPanel;
    Label7: TLabel;
    DBEdit15: TDBEdit;
    Label5: TLabel;
    DBEdit8: TDBEdit;
    Label4: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    Panel6: TPanel;
    QrOSFrmCab: TmySQLQuery;
    QrOSFrmCabCodigo: TIntegerField;
    QrOSFrmCabControle: TIntegerField;
    QrOSFrmCabConta: TIntegerField;
    QrOSFrmCabNome: TWideStringField;
    QrOSFrmCabFormula: TIntegerField;
    QrOSFrmCabEquipAplic: TIntegerField;
    QrOSFrmCabQtdTot: TFloatField;
    QrOSFrmCabQtdQSP: TFloatField;
    QrOSFrmCabCusTot: TFloatField;
    QrOSFrmCabNO_FORMULA: TWideStringField;
    QrOSFrmCabNO_EquipAplic: TWideStringField;
    DsOSFrmCab: TDataSource;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label12: TLabel;
    DBEdit4: TDBEdit;
    Label13: TLabel;
    DBEdit7: TDBEdit;
    Label14: TLabel;
    DBEdit9: TDBEdit;
    Label15: TLabel;
    DBGrid1: TDBGrid;
    TbOSFrmRec: TmySQLTable;
    TbOSFrmRecNO_GGX: TWideStringField;
    DsOSFrmRec: TDataSource;
    QrGGXs: TmySQLQuery;
    QrGGXsGraGru1: TIntegerField;
    QrGGXsControle: TIntegerField;
    QrGGXsNO_PRD_TAM_COR: TWideStringField;
    DsGGxs: TDataSource;
    QrReordem: TmySQLQuery;
    TbOSFrmRecCodigo: TIntegerField;
    TbOSFrmRecControle: TIntegerField;
    TbOSFrmRecConta: TIntegerField;
    TbOSFrmRecIDIts: TIntegerField;
    TbOSFrmRecGraGruX: TIntegerField;
    TbOSFrmRecPrvQtd: TFloatField;
    TbOSFrmRecPrvPrc: TFloatField;
    TbOSFrmRecPrvVal: TFloatField;
    TbOSFrmRecUsoQtd: TFloatField;
    TbOSFrmRecUsoPrc: TFloatField;
    TbOSFrmRecUsoVal: TFloatField;
    TbOSFrmRecUsoDec: TFloatField;
    TbOSFrmRecUsoTot: TFloatField;
    TbOSFrmRecOrdem: TIntegerField;
    TbOSFrmRecReordem: TIntegerField;
    QrEquipAplicControle: TIntegerField;
    QrEquipAplicNome: TWideStringField;
    TbOSFrmRecEhDiluente: TSmallintField;
    QrOSFrmCabDiluente: TSmallintField;
    QrGGXsSIGLAUNIDMED: TWideStringField;
    QrGGXsCODUSUUNIDMED: TIntegerField;
    QrGGXsNOMEUNIDMED: TWideStringField;
    TbOSFrmRecSIGLAUNIDMED: TWideStringField;
    TbOSFrmRecNumLote: TWideStringField;
    TbOSFrmRecNumLaudo: TWideStringField;
    QrG1Apl: TmySQLQuery;
    QrG1AplValCliDd: TIntegerField;
    QrG1AplAtuNumLot: TWideStringField;
    QrG1AplAtuNumLaud: TWideStringField;
    PMLote: TPopupMenu;
    Pesquisa1: TMenuItem;
    Ativadesatova1: TMenuItem;
    Gerencia1: TMenuItem;
    TbOSFrmRecRatifUso: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbOSFrmRecAfterDelete(DataSet: TDataSet);
    procedure TbOSFrmRecAfterPost(DataSet: TDataSet);
    procedure TbOSFrmRecBeforeInsert(DataSet: TDataSet);
    procedure TbOSFrmRecBeforePost(DataSet: TDataSet);
    procedure TbOSFrmRecNewRecord(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOSFrmCabAfterScroll(DataSet: TDataSet);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    //F_OS_Alv_: String;
    FReordenendo: Boolean;
    FOrdem: Integer;
    procedure ReordenaLinhas();
    procedure AtualizaTbFormulas();
    //procedure DuplicaRegistro();
    //
    procedure Sair();

  public
    { Public declarations }
    FCodigo, FControle, FConta: Integer;
    FQrOSFrmRec, FQrOSFrmCab: TmySQLQuery;
  end;

  var
  FmOSFrmRec: TFmOSFrmRec;

implementation


uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral,
{$IfNDef SemGraGruN}GraGruN, {$EndIf}
UnOSApp_PF, Principal, MyDBCheck, MeuDBUses, MyVCLSkin, ModProd;

{$R *.DFM}

procedure TFmOSFrmRec.AtualizaTbFormulas();
begin
  // Fazer?
end;

procedure TFmOSFrmRec.BtOKClick(Sender: TObject);
begin
  Sair();
end;

procedure TFmOSFrmRec.BtSaidaClick(Sender: TObject);
begin
  Sair();
end;

procedure TFmOSFrmRec.DBGrid1CellClick(Column: TColumn);
var
  EhDiluente, AcaoSobreDil: Byte;
  UsaDil: String;
  Continua: Boolean;
begin
  if Column.FieldName = CO_FldEhDil then
  begin
    if TbOSFrmRecEhDiluente.Value = 0 then
    begin
      if QrOSFrmCabDiluente.Value <> CO_COD_BUGS_DILUENTE_003 then
      begin
        UsaDil := '"' + sListaDiluidoresBugs[CO_COD_BUGS_DILUENTE_003] + '"';
        AcaoSobreDil := MyObjects.SelRadioGroup('O Que Fazer?',
        'O tipo de diluente difere de ' + UsaDil +  '. O que fazer?',
        ['Manter do jeito que est�', 'Mudar para '+ UsaDil, 'Desistir'], 1);
        case AcaoSobreDil of
          0: Continua := True;
          1:
          begin
            Continua := True;
            OSApp_PF.DefineTemProdutoDiluente(gbsAplica, FQrOSFrmCab (*DmModOS.QrOSFrmCabConta.Value*));
          end;
          else Continua := False;
        end;
        if not Continua then
          Exit;
      end;
      OSApp_PF.ZeraDiluente(gbsAplica, FConta (*DmModOS.QrOSFrmCabConta.Value*));
      EhDiluente := 1;
    end else
      EhDiluente := 0;
    //  
    TbOSFrmRec.Edit;
    TbOSFrmRecEhDiluente.Value := EhDiluente;
    TbOSFrmRec.Post;
    //
    TbOSFrmRec.Refresh;
  end;
end;

procedure TFmOSFrmRec.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = CO_FldEhDil then
    DBGrid1.Options := DBGrid1.Options - [dgEditing] else
    DBGrid1.Options := DBGrid1.Options + [dgEditing];
end;

procedure TFmOSFrmRec.DBGrid1ColExit(Sender: TObject);
begin
  DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmOSFrmRec.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = CO_FldEhDil then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbOSFrmRecEhDiluente.Value);
end;

procedure TFmOSFrmRec.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  function SelCod(ItTxt, TabName: String): String;
  const
    Aviso  = '...';
    FldTxt = 'Descricao';
    FldInt = 'Codigo';
  var
    Res: Variant;
    SQL_WHERE, Tabela, Titulo, Prompt: String;
    Qry: TmySQLQuery;
  begin
    Result := '';
    Titulo := 'Sele��o de ' + ItTxt;
    Prompt := 'Informe o ' + ItTxt + ': [F7 para pesquisar]';
    //
    Res := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, FldTxt, 0, [
    'SELECT Controle ' + FldInt + ', Nome ' + FldTxt,
    'FROM ' + TabName,
    'WHERE Ativo=1 ',
    'ORDER BY ' + FldTxt,
    ''], Dmod.MyDB, True);
    //
    if Res <> Null then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Nome ',
        'FROM ' + TabName,
        'WHERE Controle=' + Geral.FF0(Res),
        'AND Ativo=1 ',
        '']);
        Result := Qry.FieldByName('Nome').AsString;
      finally
        Qry.Free;
      end;
    end;
  end;
  procedure AtivaCods(Titulo, Tabela: String);
  const
    Aviso   = '...';
    FldNome = 'Nome';
    FldCodi = 'Controle';
    FldAtiv = 'Ativo';
    DefLoc  = 0;
  begin
    Titulo := '';
    DBCheck.AtivaCodigos(Aviso, Titulo, Tabela, FldCodi, FldNome, FldAtiv, [
    'SELECT ' + FldCodi + ' Codigo, ' +
    FldNome + ' Nome, ' + FldAtiv + ' Ativo, ',
    'DataCad, Usercad, DataAlt, UserAlt ',
    'FROM ' + Tabela,
    ''], Dmod.MyDB, DefLoc);
  end;
var
  Ordem, IDIts: Integer;
  Campo, Txt, Nome: String;
  Nivel1, Controle: Integer;
  Qry: TmySQLQuery;
  IDCtrl: Int64;
begin
  Campo := LowerCase(DBGrid1.Columns[THackDBGrid(DBGrid1).Col-1].FieldName);
  if key=VK_INSERT then
  begin
    if DBGrid1.ReadOnly = False then
    begin
      Ordem := TbOSFrmRecOrdem.Value-1;
      TbOSFrmRec.Insert;
      TbOSFrmRecOrdem.Value := Ordem;
      key := 0;
    end;
  end else
  if key=VK_F4 then
  begin
    if (Campo = 'gragrux') or (Campo = 'no_ggx') then
    begin
      {$IfNDef SemGraGruN}
      //if ImgTipo.SQLType in ([stIns, stUpd]) then
      begin
        VAR_CADASTRO := 0;
        //FmPrincipal.CadastroPQ(TbOSFrmRecProduto.Value);
        if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
        begin
          FmGraGruN.ShowModal;
          FmGraGruN.Destroy;
          // Parei Aqui
          // Como gerar o VAR_CADASTRO?
        end;
        if VAR_CADASTRO <> 0 then
        begin
          Screen.Cursor := crHourGlass;
          try
            QrGGXs.Close;
            QrGGXs.Open;
            //
            TbOSFrmRec.Edit;
            TbOSFrmRecGraGruX.Value := VAR_CADASTRO;
            TbOSFrmRec.Post;
          finally
            Screen.Cursor := crDefault;
          end;
        end;
      end;
      {$EndIf}
    end
    else if Campo = 'numlote' then
    begin
      if TbOSFrmRecGraGruX.Value <> 0 then
      begin
        Nivel1 := DmProd.ObtemGraGru1deGraGruX(TbOSFrmRecGraGruX.Value);
        if Nivel1 <> 0 then
          if DmProd.MostraGraGru1Xxx(
          'Lote', 'gragru1lot', Nivel1, Controle, Nome) then
          begin
            TbOSFrmRec.Edit;
            TbOSFrmRecNumLote.Value := Nome;
            TbOSFrmRec.Post;
          end;
      end else Geral.MB_Aviso('Selecione um produto antes!');
    end
    else if Campo = 'numlaudo' then
    begin
      if TbOSFrmRecGraGruX.Value <> 0 then
      begin
        Nivel1 := DmProd.ObtemGraGru1deGraGruX(TbOSFrmRecGraGruX.Value);
        if Nivel1 <> 0 then
          if DmProd.MostraGraGru1Xxx(
          'Lote', 'gragru1lau', Nivel1, Controle, Nome) then
          begin
            TbOSFrmRec.Edit;
            TbOSFrmRecNumLaudo.Value := Nome;
            TbOSFrmRec.Post;
          end;
      end else Geral.MB_Aviso('Selecione um produto antes!');
    end;
  end else
  if key=VK_F7 then
  begin
    IDIts := TbOSFrmRecIDIts.Value;
    if (Campo = 'gragrux') or (Campo = 'no_ggx') then
    begin
      VAR_CADASTRO := 0;
      FmMeuDBUses.PesquisaNome('gragrux', '', '');
      if VAR_CADASTRO <> 0 then
      begin
        TbOSFrmRec.Edit;
        TbOSFrmRecGraGruX.Value := VAR_CADASTRO;
      end;
    end
    else
    if Campo = 'numlote' then
    begin
      Txt := SelCod('N�mero de Lote', 'gragru1lot');
      if Txt <> '' then
      begin
        TbOSFrmRec.Edit;
        TbOSFrmRecNumLote.Value := Txt;
      end;
    end
    else if Campo = 'numlaudo' then
    begin
      Txt := SelCod('N�mero do Laudo', 'gragru1lau');
      if Txt <> '' then
      begin
        TbOSFrmRec.Edit;
        TbOSFrmRecNumLaudo.Value := Txt;
      end;
    end;
    if IDIts <> 0 then
      if TbOSFrmRec.State = dsEdit then
        TbOSFrmRec.Post;
  end else
  if key=VK_F8 then
  begin
    if (Campo = 'gragrux') or (Campo = 'no_ggx') then
    begin
      // nada
    end
    else if Campo = 'numlote' then
    begin
      AtivaCods('N�mero de Lote', 'gragru1lot');
    end
    else if Campo = 'numlaudo' then
    begin
      AtivaCods('N�mero do Laudo', 'gragru1lau');
    end;
  end else if
  (key=VK_DELETE) and (ssCtrl in Shift) then
  begin
    if TbOSFrmRecRatifUso.Value <> 0 then
    begin
      Key := 0; // Cancela exclusao
      if Geral.MB_Pergunta('O item selecionado j� teve seu uso ratificado!' +
      sLineBreak + 'Deseja exclu�-lo assim mesmo?') = ID_YES then
      begin
        Qry := TmySQLQuery.Create(Dmod);
        try
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT SMI_IDCtrl ',
          'FROM osfrmrec ',
          'WHERE IDIts=' + Geral.FF0(TbOSFrmRecIDIts.Value),
          '']);
          IDCtrl := Qry.FieldByName('SMI_IDCtrl').AsLargeInt;
          if IDCtrl <> 0 then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
            'DELETE FROM stqmovitsa ',
            'WHERE IDCtrl=' + Geral.FI64(IDCtrl),
            '']);
          end;
          TbOSFrmRec.Delete;
        finally
          Qry.Free;
        end;
      end else
    end;
  end;
end;

{
procedure TFmOSFrmRec.DuplicaRegistro();
begin
// Fazer?
end;
}

procedure TFmOSFrmRec.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSFrmRec.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ReordenaLinhas();
  OSApp_PF.ReopenOSFrmCab(FQrOSFrmCab, FControle, FConta(*DmModOS.QrOSFrmCabConta.Value*));
end;

procedure TFmOSFrmRec.FormCreate(Sender: TObject);
var
  FiltroNivel: String;
begin
  ImgTipo.SQLType := stLok;
  UMyMod.AbreQuery(QrFormulas, Dmod.MyDB);
  //UMyMod.AbreQuery(QrGGXs, Dmod.MyDB);
  OSApp_PF.FiltroGrade(gbsAplica, gbmProduto, FiltroNivel);
  UnDmkDAC_PF.AbreMySQLQuery0(QrGGXs, DMod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed',
  // 2013-08-03
  //'WHERE ' + FiltroNivel,
  // FIM 2013-08-03
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
  OSApp_PF.ReopenEquipAplic(QrEquipAplic, gbsAplica);
end;

procedure TFmOSFrmRec.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSFrmRec.QrOSFrmCabAfterScroll(DataSet: TDataSet);
begin
  TbOSFrmRec.Close;
  TbOSFrmRec.Filter := 'Conta=' + Geral.FF0(QrOSFrmCabConta.Value);
  TbOSFrmRec.Filtered := True;
  TbOSFrmRec.Open;
end;

procedure TFmOSFrmRec.ReordenaLinhas();
var
  i, Item: Integer;
begin
  if FReordenendo then Exit;
  FReordenendo := True;
  Screen.Cursor := crHourGlass;
  TbOSFrmRec.DisableControls;
  Item := TbOSFrmRecIDIts.Value;
  i := 0;
  TbOSFrmRec.First;
  while not TbOSFrmRec.Eof do
  begin
    inc(i, 1);
    TbOSFrmRec.Edit;
    TbOSFrmRecReordem.Value := i;
    TbOSFrmRec.Post;
    TbOSFrmRec.Next;
  end;
  QrReordem.Params[0].AsInteger := QrOSFrmCabConta.Value;
  QrReordem.ExecSQL;
  TbOSFrmRec.Refresh;
  TbOSFrmRec.Locate('IDIts', Item, []);
  TbOSFrmRec.EnableControls;
  FReordenendo := False;
  Screen.Cursor := crDefault;
end;

procedure TFmOSFrmRec.Sair();
begin
  if (TbOSFrmRec.State <> dsInactive) and (TbOSFrmRec.RecordCount > 0) and
    (FQrOSFrmCab.FieldByName('Diluente').AsInteger = CO_COD_BUGS_DILUENTE_003) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT IDIts ',
    'FROM osfrmrec ',
    'WHERE Conta=' + Geral.FF0(FConta),
    'AND EhDiluente=1 ',
    '']);
    if Dmod.QrAux.Recordcount = 0 then
      Geral.MB_Aviso('Defina o produto diluente!')
    else
      Close;
  end else
    Close;
end;

procedure TFmOSFrmRec.TbOSFrmRecAfterDelete(DataSet: TDataSet);
begin
  if not FReordenendo then
    ReordenaLinhas;
  AtualizaTbFormulas;
end;

procedure TFmOSFrmRec.TbOSFrmRecAfterPost(DataSet: TDataSet);
begin
  if not FReordenendo then
    ReordenaLinhas;
  AtualizaTbFormulas;
end;

procedure TFmOSFrmRec.TbOSFrmRecBeforeInsert(DataSet: TDataSet);
begin
  FOrdem := TbOSFrmRecOrdem.Value;
end;

procedure TFmOSFrmRec.TbOSFrmRecBeforePost(DataSet: TDataSet);
{
var
  Atual: Double;
}
begin
  if TbOSFrmRecConta.Value < 0.1 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '');
    TbOSFrmRecIDIts.Value :=   UMyMod.BPGS1I32(
      'osfrmrec', 'IDIts', '', '', tsDef, stIns, 0);
    TbOSFrmRecCodigo.Value := FCodigo; //FmOSCab.QrOSCabCodigo.Value;
    TbOSFrmRecControle.Value := FControle; //DmModOS.QrOSSrvControle.Value;
    TbOSFrmRecConta.Value := FConta; //DmModOS.QrOSFrmCabConta.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrG1Apl, Dmod.MyDB, [
    'SELECT ggx.Controle, ',
    'pap.ValCliDd, pap.AtuNumLot, pap.AtuNumLaud ',
    'FROM grag1prap pap ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pap.Nivel1 ',
    'LEFT JOIN gragrux ggx ON ggx.GraGru1=pap.Nivel1 ',
    'WHERE ggx.Controle=' + Geral.FF0(TbOSFrmRecGraGruX.Value),
    '',
    'UNION',
    '',
    'SELECT ggx.Controle, ',
    'pmo.ValCliDd, pmo.AtuNumLot, pmo.AtuNumLaud ',
    'FROM grag1prmo pmo ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pmo.Nivel1 ',
    'LEFT JOIN gragrux ggx ON ggx.GraGru1=pmo.Nivel1 ',
    'WHERE ggx.Controle=' + Geral.FF0(TbOSFrmRecGraGruX.Value),
    '']);
    { N�o usa
    if TbOSFrmRecValCliDd.Value = 0 then
      TbOSFrmRecValCliDd.Value := QrG1AplValCliDd.Value;
    }
    if TbOSFrmRecNumLote.Value = '' then
      TbOSFrmRecNumLote.Value := QrG1AplAtuNumLot.Value;
    if TbOSFrmRecNumLaudo.Value = '' then
      TbOSFrmRecNumLaudo.Value := QrG1AplAtuNumLaud.Value;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TFmOSFrmRec.TbOSFrmRecNewRecord(DataSet: TDataSet);
begin
  if FOrdem > 0 then
    TbOSFrmRecOrdem.Value := FOrdem
  else
    TbOSFrmRecOrdem.Value := 1;
end;

end.
