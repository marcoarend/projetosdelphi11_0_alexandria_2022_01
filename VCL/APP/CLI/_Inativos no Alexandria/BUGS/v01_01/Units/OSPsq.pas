unit OSPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnAppListas, Menus, dmkCompoStore, dmkDBGridZTO, UnDmkEnums,
  UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmOSPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesCodUsu: TIntegerField;
    QrClientesCliInt: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    QrEntContrat: TmySQLQuery;
    QrEntContratCodigo: TIntegerField;
    QrEntContratCliInt: TIntegerField;
    QrEntContratCodUsu: TIntegerField;
    QrEntContratNOMEENTIDADE: TWideStringField;
    DsEntContrat: TDataSource;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    DsEntiContat: TDataSource;
    GroupBox5: TGroupBox;
    Panel12: TPanel;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqEntidade: TIntegerField;
    QrPesqSiapTerCad: TIntegerField;
    QrPesqEstatus: TIntegerField;
    QrPesqFatoGeradr: TIntegerField;
    QrPesqDtaContat: TDateTimeField;
    QrPesqDtaVisPrv: TDateTimeField;
    QrPesqDtaVisExe: TDateTimeField;
    QrPesqDtaExePrv: TDateTimeField;
    QrPesqDtaExeIni: TDateTimeField;
    QrPesqDtaExeFim: TDateTimeField;
    QrPesqValorTotal: TFloatField;
    QrPesqDdsPosVda: TIntegerField;
    QrPesqEntiContat: TIntegerField;
    QrPesqNumContrat: TIntegerField;
    QrPesqEntPagante: TIntegerField;
    QrPesqEntContrat: TIntegerField;
    QrPesqEmpresa: TIntegerField;
    QrPesqNO_ENTIDADE: TWideStringField;
    QrPesqNO_ENTCONTRAT: TWideStringField;
    QrPesqNO_ENTPAGENTE: TWideStringField;
    QrPesqGrupo: TIntegerField;
    QrPesqOpcao: TIntegerField;
    DsEstatusOSs: TDataSource;
    QrEstatusOSs: TmySQLQuery;
    QrEstatusOSsCodigo: TIntegerField;
    QrEstatusOSsNome: TWideStringField;
    QrDesServico: TmySQLQuery;
    QrDesServicoCodigo: TIntegerField;
    QrDesServicoNome: TWideStringField;
    DsDesServico: TDataSource;
    PMOSCabAlv: TPopupMenu;
    IncluiOSCabAlv1: TMenuItem;
    ExcluiOSCabAlv1: TMenuItem;
    PMOSAge: TPopupMenu;
    IncluiOSAge1: TMenuItem;
    RemoveOSAge1: TMenuItem;
    QrPsqOSCabAlv: TmySQLQuery;
    QrPsqOSCabAlvCodigo: TIntegerField;
    QrPsqOSCabAlvControle: TIntegerField;
    QrPsqOSCabAlvOrdem: TSmallintField;
    QrPsqOSCabAlvLk: TIntegerField;
    QrPsqOSCabAlvDataCad: TDateField;
    QrPsqOSCabAlvDataAlt: TDateField;
    QrPsqOSCabAlvUserCad: TIntegerField;
    QrPsqOSCabAlvUserAlt: TIntegerField;
    QrPsqOSCabAlvAlterWeb: TSmallintField;
    QrPsqOSCabAlvAtivo: TSmallintField;
    QrPsqOSCabAlvPraga_A: TIntegerField;
    QrPsqOSCabAlvPraga_Z: TIntegerField;
    QrPsqOSCabAlvNO_NIVEL: TWideStringField;
    QrPsqOSCabAlvNO_PRAGA: TWideStringField;
    DsPsqOSCabAlv: TDataSource;
    QrPsqOSAlv: TmySQLQuery;
    QrPsqOSAlvCodigo: TIntegerField;
    QrPsqOSAlvControle: TIntegerField;
    QrPsqOSAlvConta: TIntegerField;
    QrPsqOSAlvNO_Praga_Z: TWideStringField;
    QrPsqOSAlvPraga_Z: TIntegerField;
    DsPsqOSAlv: TDataSource;
    QrPsqOSAge: TmySQLQuery;
    QrPsqOSAgeNO_AGENTE: TWideStringField;
    QrPsqOSAgeCodigo: TIntegerField;
    QrPsqOSAgeControle: TIntegerField;
    QrPsqOSAgeAgente: TIntegerField;
    QrPsqOSAgeResponsa: TSmallintField;
    DsPsqOSAge: TDataSource;
    QrFatoGeradr: TmySQLQuery;
    QrFatoGeradrCodigo: TIntegerField;
    QrFatoGeradrNome: TWideStringField;
    QrFatoGeradrNeedOS_Ori: TSmallintField;
    DsFatoGeradr: TDataSource;
    PMOSAlv: TPopupMenu;
    xxx1: TMenuItem;
    Retirapragaalvoatual1: TMenuItem;
    CSTabSheetChamou: TdmkCompoStore;
    QrPesqDtaLibFat: TDateTimeField;
    QrPesqDtaFimFat: TDateTimeField;
    QrPesqValorServi: TFloatField;
    QrPesqValorDesco: TFloatField;
    QrPesqValorOutrs: TFloatField;
    QrPesqCondicaoPg: TIntegerField;
    QrPesqCartEmis: TIntegerField;
    QrPesqSerNF: TWideStringField;
    QrPesqNumNF: TIntegerField;
    QrPesqLk: TIntegerField;
    QrPesqDataCad: TDateField;
    QrPesqDataAlt: TDateField;
    QrPesqUserCad: TIntegerField;
    QrPesqUserAlt: TIntegerField;
    QrPesqAlterWeb: TSmallintField;
    QrPesqAtivo: TSmallintField;
    QrPesqInvalServi: TFloatField;
    QrPesqInvalDesco: TFloatField;
    QrPesqInvalOutrs: TFloatField;
    QrPesqInvalTotal: TFloatField;
    QrPesqOrcamServi: TFloatField;
    QrPesqOrcamDesco: TFloatField;
    QrPesqOrcamOutrs: TFloatField;
    QrPesqOrcamTotal: TFloatField;
    QrPesqValiDdOrca: TIntegerField;
    QrPesqOperacao: TIntegerField;
    QrPesqExeTxtCli1: TIntegerField;
    QrPesqValorPre: TFloatField;
    QrPesqObsExecuta: TWideMemoField;
    QrPesqExeTxtCli2: TIntegerField;
    QrPesqFimVisPrv: TDateTimeField;
    QrPesqFimVisExe: TDateTimeField;
    QrPesqFimExePrv: TDateTimeField;
    QrPesqNumero: TIntegerField;
    QrPesqOptado: TSmallintField;
    QrPesqHowGerou: TSmallintField;
    QrPesqPosGerou: TSmallintField;
    QrPesqOSFlhUltGe: TIntegerField;
    QrPesqEstatus_TXT: TWideStringField;
    QrPesqAF_ENTIDADE: TWideStringField;
    QrPesqAF_ENTPAGANTE: TWideStringField;
    QrPesqNO_MUL_SRV: TWideStringField;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    DBGOSCabAlv: TDBGrid;
    GroupBox6: TGroupBox;
    DBGOSAlv: TDBGrid;
    QrPesqNO_AgeEqiCab: TWideStringField;
    DBGPsq: TdmkDBGridZTO;
    QrPesqNO_ENTICONTAT: TWideStringField;
    QrPesqAF_ENTCONTRAT: TWideStringField;
    QrPesqNO_LUGAR: TWideStringField;
    Timer1: TTimer;
    QrPesqOSFlhGrCab: TIntegerField;
    Panel5: TPanel;
    LaCliInt: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    LaEntidade1: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    LaEntidade2: TLabel;
    EdEntContrat: TdmkEditCB;
    CBEntContrat: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdEntiContat: TdmkEditCB;
    CBEntiContat: TdmkDBLookupComboBox;
    CkOrcamTotal: TCheckBox;
    EdOrcamTotal_Min: TdmkEdit;
    Label5: TLabel;
    EdOrcamTotal_Max: TdmkEdit;
    LaSC_Parci: TLabel;
    EdSC_Parci: TdmkEdit;
    LaCO_Parci: TLabel;
    EdCO_Parci: TdmkEdit;
    LaIN_Parci: TLabel;
    EdIN_Parci: TdmkEdit;
    Panel6: TPanel;
    CGOperacao: TdmkCheckGroup;
    CGStPipAdPrg: TdmkCheckGroup;
    GroupBox7: TGroupBox;
    DBGOSAge: TDBGrid;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    TPDtaContatIni: TdmkEditDateTimePicker;
    TPDtaContatFim: TdmkEditDateTimePicker;
    GroupBox8: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    TPDtaExecutIni: TdmkEditDateTimePicker;
    TPDtaExecutFim: TdmkEditDateTimePicker;
    CkDtaExecut: TCheckBox;
    GBOSFlhGrCab: TGroupBox;
    Label8: TLabel;
    CkOSFlhGrCab: TCheckBox;
    EdOSFlhGrCab: TdmkEdit;
    Panel7: TPanel;
    CkEstatus: TCheckBox;
    EdEstatus: TdmkEditCB;
    CBEstatus: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdDesServico: TdmkEditCB;
    CBDesServico: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdFatoGeradr: TdmkEditCB;
    CBFatoGeradr: TdmkDBLookupComboBox;
    Label9: TLabel;
    EdEntPagante: TdmkEditCB;
    CBEntPagante: TdmkDBLookupComboBox;
    LaPA_Parci: TLabel;
    EdPA_Parci: TdmkEdit;
    QrEntPagante: TmySQLQuery;
    DsEntPagante: TDataSource;
    QrEntPaganteCodigo: TIntegerField;
    QrEntPaganteCliInt: TIntegerField;
    QrEntPaganteCodUsu: TIntegerField;
    QrEntPaganteNOMEENTIDADE: TWideStringField;
    GBProtocolo: TGroupBox;
    CkProtocolo: TCheckBox;
    Label10: TLabel;
    EdProtocolo: TdmkEdit;
    PMProtocolo: TPopupMenu;
    Adicionaaumlote1: TMenuItem;
    RGOSAge: TRadioGroup;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPesqNO_OPERACOES: TWideStringField;
    GroupBox9: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    TPDtaVisPrvIni: TdmkEditDateTimePicker;
    TPDtaVisPrvFim: TdmkEditDateTimePicker;
    CkDtaVisPrv: TCheckBox;
    GroupBox10: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    TPDtaExePrvIni: TdmkEditDateTimePicker;
    TpDtaExePrvFim: TdmkEditDateTimePicker;
    CkDtaExePrv: TCheckBox;
    N1: TMenuItem;
    Panel8: TPanel;
    BtOK: TBitBtn;
    PnProtocolo: TPanel;
    BtProtocolo: TBitBtn;
    Panel10: TPanel;
    LaInfo1: TLabel;
    LaInfo2: TLabel;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrPesqObsGaranti: TWideMemoField;
    QrPesqRotaLatLon: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdEntContratChange(Sender: TObject);
    procedure EdEntiContatChange(Sender: TObject);
    procedure DBGPsqDblClick(Sender: TObject);
    procedure DBGOSCabAlvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiOSCabAlv1Click(Sender: TObject);
    procedure ExcluiOSCabAlv1Click(Sender: TObject);
    procedure DBGOSAlvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure CkEstatusClick(Sender: TObject);
    procedure DBGOSAgeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiOSAge1Click(Sender: TObject);
    procedure RemoveOSAge1Click(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure xxx1Click(Sender: TObject);
    procedure Retirapragaalvoatual1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrPesqDtaExeFimGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrPesqDtaExePrvGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure EdOSFlhGrCabKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkOSFlhGrCabClick(Sender: TObject);
    procedure EdOSFlhGrCabChange(Sender: TObject);
    procedure CkDtaExecutClick(Sender: TObject);
    procedure TPDtaExecutIniChange(Sender: TObject);
    procedure TPDtaExecutIniClick(Sender: TObject);
    procedure TPDtaExecutFimChange(Sender: TObject);
    procedure TPDtaExecutFimClick(Sender: TObject);
    procedure CGOperacaoClick(Sender: TObject);
    procedure CkProtocoloClick(Sender: TObject);
    procedure EdProtocoloChange(Sender: TObject);
    procedure EdProtocoloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Adicionaaumlote1Click(Sender: TObject);
    procedure BtProtocoloClick(Sender: TObject);
    procedure RGOSAgeClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    { Private declarations }
    F_Psq_OSAlv_, F_Psq_OSAge_, F_Psq_OSCabAlv_: String;
    FMultiSel, FConfirmou: Boolean;
    //
    procedure DesHabilita();
    procedure ReopenEntiContat();
    procedure ReopenPsq_OSAge();
    procedure ReopenPsq_OSAlv();
    procedure ReopenPsq_OSCabAlv();

  public
    { Public declarations }
    FGrupo, FOpcao, FLugar, FOSCab: Integer;
    //FSelecionados: TArrSelIdxInt2;
    //
    procedure ConfiguraParaMultiSelecao(var Selecionados: TArrSelIdxInt2;
              Empresa: Integer);
  end;

  var
  FmOSPsq: TFmOSPsq;

implementation

uses UnMyObjects, Module, dmkDAC_PF, UnDmkProcFunc, CreateBugs, ModuleGeral,
UnOSApp_PF, OSCabAlv, MyDBCheck, UMySQLModule, MyGlyfs, Principal, OSCab2,
DmkDBGrid, ModOS, UnBugs_Tabs, Protocolo, OSAlv;

{$R *.DFM}

procedure TFmOSPsq.Adicionaaumlote1Click(Sender: TObject);
var
  Entidade: Integer;
begin
{
  Entidade := DModG.ObtemEntidadeDeFilial(QrPesqEmpresa.Value);
  if MyObjects.FIC(Entidade = 0, EdEmpresa, 'Informe a empresa!') then
    Exit;
  UnProtocolo.ProtocolosCD_OS1(istAtual, TDBGrid(DBGPsq), QrPesq, Entidade);
}
end;

procedure TFmOSPsq.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGPsq), False);
end;

procedure TFmOSPsq.BtOKClick(Sender: TObject);
var
  I, Empresa, Entidade, EntContrat, EntPagante, EntiContat, Status, Servico,
  FatoGeradr, OSFlhGrCab, Protocolo, AgeEqiCab: Integer;
  bSC, bCO, bPA, bIN: Boolean;
  sSC, sCo, sPA, sIN, sPM: String;
  OrcamTotal_Max, OrcamTotal_Min, PragasCabAlv_A, PragasCabAlv_Z, PragasAlv,
  SQLPragas, SQLCabAlv, SQLAlv, SQLAge, Agentes, DataExecucao, ExeIni, ExeFim,
  TXT_NOT, Operacoes, DtaVisPrv, FimVisPrv, DtaPreVis,
  DtaExePrv, FimExePrv, DtaPreExe: String;
begin
  Operacoes := dmkPF.ArrayToTexto('osc.Operacao', 'NO_OPERACOES', pvPos, True,
    sListaOperacoesBugstrol);
  Empresa := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then
    Exit;
  //
  sPM := '';
  TXT_NOT := '';
  if CGStPipAdPrg.Enabled and (CGStPipAdPrg.Value < CGStPipAdPrg.MaxValue) then
  begin
    if CGStPipAdPrg.Value = 0 then
    begin
      Geral.MB_Aviso(
        'Informe pelo menos uma situa��o de "perguntas de monitoramento"!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    for I := 0 to CGStPipAdPrg.Items.Count - 1 do
    begin
      if CGStPipAdPrg.Checked[I] then
        sPM := sPM + ', ' + Geral.FF0(I);
    end;
    sPM := 'AND osc.StPipAdPrg IN (' + Copy(sPM, 3) + ') ';
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Executando pesquisa');
  //
  PragasCabAlv_A := '';
  PragasCabAlv_Z := '';
  if QrPsqOSCabAlv.RecordCount > 0 then
  begin
    QrPsqOSCabAlv.First;
    while not QrPsqOSCabAlv.Eof do
    begin
      if QrPsqOSCabAlvPraga_Z.Value <> 0 then
        PragasCabAlv_Z :=
          PragasCabAlv_Z + ', ' + Geral.FF0(QrPsqOSCabAlvPraga_Z.Value)
      else
      if QrPsqOSCabAlvPraga_A.Value <> 0 then
        PragasCabAlv_A :=
          PragasCabAlv_A + ', ' + Geral.FF0(QrPsqOSCabAlvPraga_A.Value);
      //
      QrPsqOSCabAlv.Next;
    end;
  end;
  //
  PragasAlv := '';
  if QrPsqOSAlv.RecordCount > 0 then
  begin
    QrPsqOSAlv.First;
    while not QrPsqOSAlv.Eof do
    begin
      if QrPsqOSAlvPraga_Z.Value <> 0 then
        PragasAlv := PragasAlv + ', ' + Geral.FF0(QrPsqOSAlvPraga_Z.Value);
      //
      QrPsqOSAlv.Next;
    end;
  end;
  Agentes := '';
  SQLAge := '';
  if RGOSAge.ItemIndex > 0 then
  begin
    case RGOSAge.ItemIndex of
      1, 2:
      begin
        if QrPsqOSAge.RecordCount > 0 then
        begin
          QrPsqOSAge.First;
          while not QrPsqOSAge.Eof do
          begin
            if QrPsqOSAgeAgente.Value <> 0 then
              Agentes := Agentes + ', ' + Geral.FF0(QrPsqOSAgeAgente.Value);
            //
            QrPsqOSAge.Next;
          end;
        end;
        if RGOSAge.ItemIndex = 2 then
          TXT_NOT := 'NOT ';
        if Agentes <> '' then
        begin
          SQLAge  := 'LEFT JOIN osage age ON age.Codigo=osc.Codigo';
          Agentes := 'AND age.Agente ' + TXT_NOT + 'IN (' + Copy(Agentes, 2) + ')';
        end;
      end;
      3,4:
      begin
        AgeEqiCab := DmModOS.MD5_ObtemAgeEqiCab(
          F_Psq_OSAge_, 'Codigo', 'Agente', 0, DModG.MyPID_DB);
        if AgeEqiCab < 1 then
        begin
          Geral.MB_Aviso(
            'N�o h� cadastro de grupo de agentes para a sele��o de agentes!');
          Screen.Cursor := crDefault;
          Exit;
        end else
        begin
          if RGOSAge.ItemIndex = 3 then
            TXT_NOT := '='
          else
            TXT_NOT := '<>';
          SQLAge  := '';
          Agentes := 'AND osc.AgeEqiCab' + TXT_NOT + Geral.FF0(AgeEqiCab);
        end;
      end;
      5:
      begin
        SQLAge  := '';
        Agentes := 'AND osc.AgeEqiCab IN (-1,0) ';
      end;
    end;
  end;
  //
  if (PragasCabAlv_A <> '') or (PragasCabAlv_Z <> '') or (PragasAlv <> '') then
  begin
    SQLPragas := 'AND (';
    //
    if PragasCabAlv_A <> '' then
    begin
      SQLCabAlv := 'LEFT JOIN oscabalv oca ON oca.Codigo=osc.Codigo';
      PragasCabAlv_A :=
        'OR (oca.Praga_A IN (' + Copy(PragasCabAlv_A, 2) + '))' + sLineBreak;
    end;
    if PragasCabAlv_Z <> '' then
    begin
      SQLCabAlv := 'LEFT JOIN oscabalv oca ON oca.Codigo=osc.Codigo';
      PragasCabAlv_Z :=
        'OR (oca.Praga_Z IN (' + Copy(PragasCabAlv_Z, 2) + '))' + sLineBreak;
    end;
    if PragasAlv <> '' then
    begin
      SQLAlv := 'LEFT JOIN osalv oal ON oal.Codigo=osc.Codigo';
      PragasAlv :=
        'OR (oal.Praga_Z IN (' + Copy(PragasAlv, 2) + '))' + sLineBreak;
    end;
    //
    SQLPragas := SQLPragas + Copy(PragasCabAlv_A + PragasCabAlv_Z + PragasAlv, 3);
    SQLPragas := SQLPragas + sLineBreak + ')';
  end else
  begin
    SQLPragas := '';
    SQLCabAlv := '';
    SQLAlv := '';
  end;
  //
  DataExecucao := '';
  if CkDtaExecut.Checked then
  begin
    ExeIni := dmkPF.SQL_Periodo('osc.DtaExeIni ', TPDtaExecutIni.Date, TPDtaExecutFim.Date, True, True);
    ExeFim := dmkPF.SQL_Periodo('osc.DtaExeFim ', TPDtaExecutIni.Date, TPDtaExecutFim.Date, True, True);
    //
    DataExecucao := Geral.ATS([
    'AND ( ',
    '(' + ExeIni + ') ',
    'OR ',
    '(' + ExeFim + ') ',
    ') ']);
  end;
  //
  DtaPreVis := '';
  if CkDtaVisPrv.Checked then
  begin
    DtaVisPrv := dmkPF.SQL_Periodo('osc.DtaVisPrv ', TPDtaVisPrvIni.Date, TPDtaVisPrvFim.Date, True, True);
    FimVisPrv := dmkPF.SQL_Periodo('osc.FimVisPrv ', TPDtaVisPrvIni.Date, TPDtaVisPrvFim.Date, True, True);
    //
    DtaPreVis := Geral.ATS([
    'AND ( ',
    '(' + DtaVisPrv + ') ',
    'OR ',
    '(' + FimVisPrv + ') ',
    ') ']);
  end;
  //
  DtaPreExe := '';
  if CkDtaExePrv.Checked then
  begin
    DtaExePrv := dmkPF.SQL_Periodo('osc.DtaExePrv ', TPDtaExePrvIni.Date, TPDtaExePrvFim.Date, True, True);
    FimExePrv := dmkPF.SQL_Periodo('osc.FimExePrv ', TPDtaExePrvIni.Date, TPDtaExePrvFim.Date, True, True);
    //
    DtaPreExe := Geral.ATS([
    'AND ( ',
    '(' + DtaExePrv + ') ',
    'OR ',
    '(' + FimExePrv + ') ',
    ') ']);
  end;
  //
  bSC := (Trim(EdSC_Parci.Text) <> '') and (EdSC_Parci.Enabled);
  bCO := (Trim(EdCO_Parci.Text) <> '') and (EdCO_Parci.Enabled);
  bPA := (Trim(EdPA_Parci.Text) <> '') and (EdPA_Parci.Enabled);
  bIN := (Trim(EdIN_Parci.Text) <> '') and (EdIN_Parci.Enabled);
  //
  sSC := 'AND IF(en1.Tipo=0, en1.RazaoSocial, en1.Nome) LIKE "%' + EdSC_Parci.Text + '%" ';
  sCO := 'AND IF(en2.Tipo=0, en2.RazaoSocial, en2.Nome) LIKE "%' + EdCO_Parci.Text + '%" ';
  sPA := 'AND IF(en3.Tipo=0, en3.RazaoSocial, en3.Nome) LIKE "%' + EdPA_Parci.Text + '%" ';
  sIN := 'AND enc.Nome LIKE "%' + EdIN_Parci.Text + '%" ';
  //
  Entidade    := EdEntidade.ValueVariant;
  EntContrat  := EdEntContrat.ValueVariant;
  EntPagante  := EdEntPagante.ValueVariant;
  EntiContat  := EdEntiContat.ValueVariant;
  Status      := EdEstatus.ValueVariant;
  Servico     := EdDesServico.ValueVariant;
  FatoGeradr  := EdFatoGeradr.ValueVariant;

  OrcamTotal_Max := Geral.FFT_Dot(EdOrcamTotal_Max.ValueVariant, 2, siNegativo);
  OrcamTotal_Min := Geral.FFT_Dot(EdOrcamTotal_Min.ValueVariant, 2, siNegativo);
  //
  OSFlhGrCab := EdOSFlhGrCab.ValueVariant;
  Protocolo  := EdProtocolo.ValueVariant;

  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT DISTINCT  aec.Nome NO_AgeEqiCab, msv.Nome NO_MUL_SRV, ',
  'CONCAT(osc.Estatus, " - ", sta.Nome) Estatus_TXT, ',
  'cun.Nome NO_LUGAR, cun.RotaLatLon, ',
  Operacoes,
  'osc.*, ',
  'IF(en1.Tipo=0, en1.RazaoSocial, en1.Nome) NO_ENTIDADE, ',
  'IF(en2.Tipo=0, en2.RazaoSocial, en2.Nome) NO_ENTCONTRAT, ',
  'IF(en3.Tipo=0, en3.RazaoSocial, en3.Nome) NO_ENTPAGANTE, ',
  'IF(en1.Tipo=0, en1.Fantasia, en1.Apelido) AF_ENTIDADE, ',
  'IF(en2.Tipo=0, en2.Fantasia, en2.Apelido) AF_ENTCONTRAT, ',
  'IF(en3.Tipo=0, en3.Fantasia, en3.Apelido) AF_ENTPAGANTE, ',
  'enc.Nome NO_ENTICONTAT ',
  'FROM oscab osc ',
  'LEFT JOIN entidades en1 ON en1.Codigo=osc.Entidade ',
  'LEFT JOIN entidades en2 ON en2.Codigo=osc.EntContrat ',
  'LEFT JOIN entidades en3 ON en3.Codigo=osc.EntPagante ',
  'LEFT JOIN enticontat enc ON enc.Controle=osc.EntiContat ',
  'LEFT JOIN estatusoss sta ON sta.Codigo=osc.Estatus ',
  'LEFT JOIN mulservico msv ON msv.Codigo=osc.MulServico ',
  'LEFT JOIN ageeqicab aec ON aec.Codigo=osc.AgeEqiCab ',
  'LEFT JOIN siaptercad cun ON cun.Codigo=osc.SiapTerCad ',
  SQLCabAlv,
  SQLAlv,
  SQLAge,
  Geral.ATS_if(Servico <> 0, ['LEFT JOIN ossrv srv ON srv.Codigo=osc.Codigo']),
  'WHERE osc.Empresa=' + Geral.FF0(Empresa),
  dmkPF.SQL_Periodo(
  'AND osc.DtaContat ', TPDtaContatIni.Date, TPDtaContatFim.Date, True, True),
  Geral.ATS_if(CGOperacao.Value >=1, ['AND osc.Operacao & ' + Geral.FF0(CGOperacao.Value)]),
  Geral.ATS_if(Entidade <> 0, ['AND osc.Entidade=' + Geral.FF0(Entidade)]),
  Geral.ATS_if(EntContrat <> 0, ['AND osc.EntContrat=' + Geral.FF0(EntContrat)]),
  Geral.ATS_if(EntPagante <> 0, ['AND osc.EntPagante=' + Geral.FF0(EntPagante)]),
  Geral.ATS_if(EntiContat <> 0, ['AND osc.EntiContat=' + Geral.FF0(EntiContat)]),
  Geral.ATS_if(bSC, sSC),
  Geral.ATS_if(bCO, sCO),
  Geral.ATS_if(bPA, sPA),
  Geral.ATS_if(bIN, sIN),
  Geral.ATS_if(CkOSFlhGrCab.Checked, ['AND osc.OSFlhGrCab=' + Geral.FF0(OSFlhGrCab)]),
  Geral.ATS_if(CkProtocolo.Checked, ['AND osc.Protocolo=' + Geral.FF0(Protocolo)]),
  Geral.ATS_if(CkEstatus.Checked, ['AND osc.Estatus=' + Geral.FF0(Status)]),
  Geral.ATS_if(FatoGeradr <> 0, ['AND osc.FatoGeradr=' + Geral.FF0(FatoGeradr)]),
  Geral.ATS_if(Servico <> 0, ['AND srv.DesServico=' + Geral.FF0(Servico)]),
  Geral.ATS_if(CkOrcamTotal.Checked, ['AND OrcamTotal BETWEEN ' +
    OrcamTotal_Min + ' AND ' + OrcamTotal_Max]),
  SQLPragas,
  Agentes,
  DataExecucao,
  DtaPreVis,
  DtaPreExe,
  sPM,
  //
  'ORDER BY Codigo DESC',
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmOSPsq.BtProtocoloClick(Sender: TObject);
var
  I: Integer;
begin
  // MyObjects.MostraPopupDeBotao(PMProtocolo, BtProtocolo);
  if DBGPsq.SelectedRows.Count = 0 then
  begin
    FConfirmou := False;
    Geral.MB_Aviso('Nenhuma OS foi selecionada!');
  end else
  begin
    FConfirmou := True;
(*
    MyObjects.GetSelecionadosBookmark_Int2(
      Self, TDBGrid(DBGPsq), 'Codigo', 'Estatus', FSelecionados);
*)
    Close;
  end;
end;

procedure TFmOSPsq.BtSaidaClick(Sender: TObject);
begin
  FGrupo := 0;
  FOpcao := 0;
  FLugar := 0;
  FOSCab := 0;
  //
  if TFmOSPsq(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmOSPsq(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmOSPsq.BtTodosClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGPsq), True);
end;

procedure TFmOSPsq.CGOperacaoClick(Sender: TObject);
begin
  CGStPipAdPrg.Enabled := CGOperacao.Value = CO_OS_OPERACAO_SERVICO_MONITORAMENTO; //4
  DesHabilita();
end;

procedure TFmOSPsq.CkDtaExecutClick(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.CkEstatusClick(Sender: TObject);
begin
  EdEstatus.Enabled := CkEstatus.Checked;
  CBEstatus.Enabled := CkEstatus.Checked;
end;

procedure TFmOSPsq.CkOSFlhGrCabClick(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.CkProtocoloClick(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.ConfiguraParaMultiSelecao(var Selecionados: TArrSelIdxInt2;
Empresa: Integer);
begin
  PnProtocolo.Visible := True;
  DBGPsq.Options := DBGPsq.Options + [dgMultiSelect];
  //FSelecionados := Selecionados;
  //
  //SetLength(FSelecionados, 0);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
  CGOperacao.SetMaxValue();
  //
  CkDtaExePrv.Checked := True;
  TPDtaExePrvIni.Date := Date;
  TpDtaExePrvFim.Date := Date + 1;
  //
  CkDtaVisPrv.Checked := True;
  TPDtaVisPrvIni.Date := Date;
  TpDtaVisPrvFim.Date := Date + 1;
end;

procedure TFmOSPsq.DBGOSAgeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSAge, DBGOSAge, X,Y);
end;

procedure TFmOSPsq.DBGOSCabAlvMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSCabAlv, DBGOSCabAlv, X,Y);
end;

procedure TFmOSPsq.DBGPsqDblClick(Sender: TObject);
const
  Atual = 0;
  Disposicao = dispIndefinido;
  ReabreOSCab = True;
var
  Form: TForm;
begin
  if FMultiSel then
  begin
    Geral.MB_Aviso('Duplo clique desabilitado para multi sele��o');
    Exit;
  end;
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    FGrupo := QrPesqGrupo.Value;
    FOpcao := QrPesqOpcao.Value;
    FLugar := QrPesqSiapTerCad.Value;
    FOSCab := QrPesqCodigo.Value;
    //
    if TFmOSPsq(Self).Owner is TApplication then
      Close
    else
    begin
      Form := MyObjects.FormTDICria(TFmOSCab2, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
      TFmOSCab2(Form).LocCod(Atual, FGrupo, Disposicao, FLugar, FOpcao,
        ReabreOSCab, FOSCab);
    end;
  end;
end;

procedure TFmOSPsq.DBGOSAlvMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSAlv, DBGOSAlv, X,Y);
end;

procedure TFmOSPsq.DesHabilita();
var
  Habil: Boolean;
begin
  Habil := EdEntidade.ValueVariant = 0;
  LaSC_Parci.Enabled := Habil;
  EdSC_Parci.Enabled := Habil;
  //
  Habil := EdEntContrat.ValueVariant = 0;
  LaCO_Parci.Enabled := Habil;
  EdCO_Parci.Enabled := Habil;
  //
  Habil := EdEntPagante.ValueVariant = 0;
  LaPA_Parci.Enabled := Habil;
  EdPA_Parci.Enabled := Habil;
  //
  Habil := EdEntiContat.ValueVariant = 0;
  LaIN_Parci.Enabled := Habil;
  EdIN_Parci.Enabled := Habil;
  //
end;

procedure TFmOSPsq.EdEntContratChange(Sender: TObject);
begin
  DesHabilita();
  ReopenEntiContat();
end;

procedure TFmOSPsq.EdEntiContatChange(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.EdEntidadeChange(Sender: TObject);
begin
  DesHabilita();
  ReopenEntiContat();
end;

procedure TFmOSPsq.EdOSFlhGrCabChange(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.EdOSFlhGrCabKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Qry: TmySQLQuery;
  OSFlhGrCab: Integer;
begin
  if Key = VK_F4 then
  begin
    Qry := TmySQLQuery.Create(DMod);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(OSFlhGrCab) OSFlhGrCab ',
    'FROM oscab ',
    '']);
    OSFlhGrCab := Qry.FieldByName('OSFlhGrCab').AsInteger;
    if OSFlhGrCab <> 0 then
    begin
      EdOSFlhGrCab.ValueVariant := OSFlhGrCab;
      CkOSFlhGrCab.Checked := True;
    end;
  end;
end;

procedure TFmOSPsq.EdProtocoloChange(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.EdProtocoloKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Qry: TmySQLQuery;
  Protocolo: Integer;
begin
  if Key = VK_F4 then
  begin
    Qry := TmySQLQuery.Create(DMod);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Protocolo) Protocolo ',
    'FROM oscab ',
    '']);
    Protocolo := Qry.FieldByName('Protocolo').AsInteger;
    if Protocolo <> 0 then
    begin
      EdProtocolo.ValueVariant := Protocolo;
      CkProtocolo.Checked := True;
    end;
  end;
end;

procedure TFmOSPsq.ExcluiOSCabAlv1Click(Sender: TObject);
{
var
  Controle: Integer;
}
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada da praga alvo selecionada?', F_Psq_OSCabAlv_, 'Controle',
  QrPsqOSCabAlvControle.Value, DModG.MyPID_DB) = ID_YES then
  begin
    ReopenPsq_OSCabAlv();
    DesHabilita();
  end;
end;

procedure TFmOSPsq.FormActivate(Sender: TObject);
begin
  if TFmOSPsq(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmOSPsq.FormCreate(Sender: TObject);
begin
  FMultiSel := False;
  FConfirmou := False;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;

  ImgTipo.SQLType := stPsq;
  TPDtaContatFim.Date := Date;
  TPDtaExecutFim.Date := Date;
  TPDtaVisPrvIni.Date := Date;
  TPDtaVisPrvFim.Date := Date + 90;
  TPDtaExePrvIni.Date := Date;
  TpDtaExePrvFim.Date := Date + 90;
  //
  MyObjects.ConfiguraCheckGroup(CGOperacao, sListaOperacoesBugs, 3, 0, False);
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntContrat, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntPagante, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEstatusOSs, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDesServico, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFatoGeradr, Dmod.MyDB);
  ReopenEntiContat();
  //
  FGrupo := 0;
  FOpcao := 0;
  FLugar := 0;
  FOSCab := 0;
  //
  F_Psq_OSAlv_ :=
        UnCreateBugs.RecriaTempTableNovo(ntrtt_Psq_OSAlv, DmodG.QrUpdPID1, False);
  ReopenPsq_OSAlv();
  //
  F_Psq_OSCabAlv_ :=
        UnCreateBugs.RecriaTempTableNovo(ntrtt_Psq_OSCabAlv, DmodG.QrUpdPID1, False);
  ReopenPsq_OSCabAlv();
  //
  F_Psq_OSAge_ :=
        UnCreateBugs.RecriaTempTableNovo(ntrtt_Psq_OSAge, DmodG.QrUpdPID1, False);
  ReopenPsq_OSAge();
  //
  //DmModOS.VerificaMulServico(nil, nil, nil, nil);
  CGStPipAdPrg.SetMaxValue;
end;

procedure TFmOSPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPsq.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmOSPsq.IncluiOSAge1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Agentes';
  Prompt = 'Seleciones os agentes operacionais:';
  //Campo  = 'Descricao';
  //
  DestMaster = 'Codigo';
  DestDetail = 'Controle';
  DestSelec1 = 'Agente';
  SorcField  = 'Agente';
  ExcluiAnteriores = False;
var
  ValrMaster: Integer;
  DestTab: String;
begin
  DestTab := F_Psq_OSAge_;
  ValrMaster := 0;
  if DBCheck.EscolheCodigosMultiplos_A(
  Aviso, Titulo, Prompt, nil, 'Ativo', 'Nivel1', 'Nome', [
  'DELETE FROM _selcods_; ',
  'INSERT INTO _selcods_ ',
  'SELECT Codigo Nivel1, 0 Nivel2, ',
  '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
  'FROM ' + TMeuDB + '.entidades',
  'WHERE Nome <> "" ',
  'AND ' + VAR_FP_FUNCION,
  ''],[
  'SELECT * FROM _selcods_ ',
  'ORDER BY Nome;',
  ''], DestTab, [DestMaster], DestDetail, DestSelec1, [ValrMaster],
  QrPsqOSAge, SorcField, ExcluiAnteriores, DModG.QrUpdPID1) then
  begin
    ReopenPsq_OSAge();
    DesHabilita();
  end;
end;

procedure TFmOSPsq.IncluiOSCabAlv1Click(Sender: TObject);
const
  SQLType = stIns;
begin
  //OSApp_PF.InsAltOSCabAlv(stIns, QrPsqOSCabAlv, -1);
  if DBCheck.CriaFm(TFmOSCabAlv, FmOSCabAlv, afmoNegarComAviso) then
  begin
    FmOSCabAlv.ImgTipo.SQLType := SQLType;
    FmOSCabAlv.FTabela := F_Psq_OSCabAlv_;
    FmOSCabAlv.FQrSelect := QrPsqOSCabAlv;
    FmOSCabAlv.FQrInsere := DmodG.QrUpdPID1;
    FmOSCabAlv.FCodigo := 0;
    //
    //
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    FmOSCabAlv.ShowModal;
    FmOSCabAlv.Destroy;
    DesHabilita();
  end;
end;

procedure TFmOSPsq.QrPesqAfterOpen(DataSet: TDataSet);
const
  Txt = 'Registros localizados na pesquisa: ';
begin
  BtProtocolo.Enabled := QrPesq.RecordCount > 0;
  //
  MyObjects.Informa2(LaInfo1, LaInfo2, False, Txt + IntToStr(QrPesq.RecordCount));
end;

procedure TFmOSPsq.QrPesqBeforeClose(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaInfo1, LaInfo2, False, '');
  BtProtocolo.Enabled := False;
end;

procedure TFmOSPsq.QrPesqDtaExeFimGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := Geral.FDT(QrPesqDtaExeFim.Value, 106);
end;

procedure TFmOSPsq.QrPesqDtaExePrvGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := Geral.FDT(QrPesqDtaExePrv.Value, 106);
end;

procedure TFmOSPsq.RGOSAgeClick(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.RemoveOSAge1Click(Sender: TObject);
{
var
  Controle: Integer;
}
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada do agente selecionado?',
  F_Psq_OSAge_, 'Agente', QrPsqOSAgeAgente.Value, DmodG.MyPID_DB) = ID_YES then
  begin
    ReopenPsq_OSAge();
    DesHabilita();
  end;
end;

procedure TFmOSPsq.ReopenEntiContat();
var
  Clien, Contr: Integer;
  SQL: String;
begin
  Screen.Cursor := crHourGlass;
  try
    SQL := '';
    Clien := EdEntidade.ValueVariant;
    Contr := EdEntContrat.ValueVariant;
    if (Clien <> 0) or (Contr <> 0) then
    begin
      if Clien <> 0 then
      begin
        SQL := SQL + 'WHERE (Codigo=' + Geral.FF0(Clien);
        if Contr <> 0 then
          SQL := SQL + ' OR Codigo=' + Geral.FF0(Contr);
      end else
      if Contr <> 0 then
          SQL := SQL + 'WHERE (Codigo=' + Geral.FF0(Contr);
      SQL := SQL + ')';
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntiContat, Dmod.MyDB, [
    'SELECT Controle, Nome ',
    'FROM enticontat ',
    SQL,
    'ORDER BY Nome',
    '']);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSPsq.ReopenPsq_OSAge();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqOSAge, DModG.MyPID_DB, [
  'SELECT ent.Nome NO_AGENTE, age.* ',
  'FROM ' + F_Psq_OSAge_ + ' age',
  'LEFT JOIN ' + TMeuDB + '.entidades ent on ent.Codigo=age.Agente',
  ' ']);
end;

procedure TFmOSPsq.ReopenPsq_OSAlv();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqOSAlv, DModG.MyPID_DB, [
  'SELECT prc.Nome NO_Praga_Z, osa.* ',
  'FROM ' + F_Psq_OSAlv_ + ' osa ',
  'LEFT JOIN ' + TMeuDB + '.praga_z prc ON prc.Codigo=osa.Praga_Z ',
  'WHERE osa.Controle=0 ',
  ' ']);
end;

procedure TFmOSPsq.ReopenPsq_OSCabAlv();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqOSCabAlv, DModG.MyPID_DB, [
  'SELECT oca.*, IF(oca.Praga_Z <> 0, "E", "G") NO_NIVEL, ',
  'IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) NO_PRAGA ',
  'FROM ' + F_Psq_OSCabAlv_ + ' oca ',
  'LEFT JOIN ' + TMeuDB + '.praga_a pca ON pca.Codigo=oca.Praga_A ',
  'LEFT JOIN ' + TMeuDB + '.praga_z pcz ON pcz.Codigo=oca.Praga_Z ',
  'WHERE oca.Codigo=0 ',
  'ORDER BY oca.Ordem ',
  ' ']);
end;

procedure TFmOSPsq.Retirapragaalvoatual1Click(Sender: TObject);
{
var
  Controle: Integer;
}
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada da praga alvo selecionada?', F_Psq_OSAlv_, 'Praga_Z',
  QrPsqOSAlvPraga_Z.Value, DModG.MyPID_DB) = ID_YES then
  begin
    ReopenPsq_OSAlv();
    DesHabilita();
  end;
end;

procedure TFmOSPsq.TPDtaExecutFimChange(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.TPDtaExecutFimClick(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.TPDtaExecutIniChange(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.TPDtaExecutIniClick(Sender: TObject);
begin
  DesHabilita();
end;

procedure TFmOSPsq.xxx1Click(Sender: TObject);
const
  SQLType = stIns;
begin
  if DBCheck.CriaFm(TFmOSAlv, FmOSAlv, afmoNegarComAviso) then
  begin
    FmOSAlv.ImgTipo.SQLType := SQLType;
    FmOSAlv.FTabela := F_Psq_OSAlv_;
    FmOSAlv.FQrOSAlv := QrPsqOSAlv;
    FmOSAlv.FDatabase := DModG.MyPID_DB;
    FmOSAlv.FQrInsere := DModG.QrUpdPID1;
    FmOSAlv.FCodigo := 0;
    FmOSAlv.FControle := 0;
    //
    FmOSAlv.QrOSSrv.Close;
    FmOSAlv.QrOSSrv.SQL.Text := '';//QrOSSrv.SQL.Text;
    //FmOSAlv.QrOSSrv.Params := QrOSSrv.Params;
    //UMyMod.AbreQuery(FmOSAlv.QrOSSrv, Dmod.MyDB);
    //
    { // N�o tem!
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    }
    FmOSAlv.ShowModal;
    FmOSAlv.Destroy;
    DesHabilita();
  end;
end;

end.
