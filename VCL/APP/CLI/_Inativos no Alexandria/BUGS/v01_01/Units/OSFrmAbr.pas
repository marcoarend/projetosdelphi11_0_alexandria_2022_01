unit OSFrmAbr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, dmkDBGrid, UnDmkEnums;

type
  TFmOSFrmAbr = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit15: TDBEdit;
    QrOSFrmCab: TmySQLQuery;
    DsOSFrmCab: TDataSource;
    Timer1: TTimer;
    Qr_OS_Frm_Abr: TmySQLQuery;
    Ds_OS_Frm_Abr: TDataSource;
    QrPsq1: TmySQLQuery;
    QrPsq1o4Ter: TIntegerField;
    QrPsq1c4Ter: TIntegerField;
    QrPsq1n4Ter: TWideStringField;
    QrPsq1a4Ter: TSmallintField;
    QrPsq1o3Loc: TIntegerField;
    QrPsq1c3Loc: TIntegerField;
    QrPsq1n3Loc: TWideStringField;
    QrPsq1a3Loc: TSmallintField;
    QrPsq1o2Typ: TIntegerField;
    QrPsq1c2Typ: TIntegerField;
    QrPsq1n2Typ: TWideStringField;
    QrPsq1a2Typ: TSmallintField;
    QrPsq1o1Dep: TIntegerField;
    QrPsq1c1Dep: TIntegerField;
    QrPsq1n1Dep: TWideStringField;
    QrPsq1a1Dep: TSmallintField;
    QrPsq1Tabela: TSmallintField;
    QrPsq1Ativo: TSmallintField;
    QrOFA: TmySQLQuery;
    QrOFAAtivo: TSmallintField;
    QrOSFrmCabCodigo: TIntegerField;
    QrOSFrmCabControle: TIntegerField;
    QrOSFrmCabConta: TIntegerField;
    QrOSFrmCabNome: TWideStringField;
    Qr_OS_Frm_AbrCodigo: TIntegerField;
    Qr_OS_Frm_AbrNome: TWideStringField;
    Qr_OS_Frm_AbrAtivo: TSmallintField;
    dmkDBGrid1: TdmkDBGrid;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrOFACodigo: TIntegerField;
    QrOFANome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AdvGridDepClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
  private
    { Private declarations }
    FCriou: Boolean;
    F_OS_Frm_Abr: String;
    //
    procedure Reopen_OS_Frm_Abr(Codigo: Integer);
    procedure AtivarTudo(Ativa: Integer);

  public
    { Public declarations }
    FQrOSFrmAbr: TmySQLQuery;
    FCodigo, FControle, FConta: Integer;
  end;

  var
  FmOSFrmAbr: TFmOSFrmAbr;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, CreateBugs, ModuleGeral,
  UnOSApp_PF;

{$R *.DFM}

procedure TFmOSFrmAbr.AdvGridDepClickCell(Sender: TObject; ARow, ACol: Integer);
{
  function CampoDeNivel(Nivel: Integer; Letra: String): String;
  begin
    case Nivel of
      1: Result := 'Dep';
      2: Result := 'Typ';
      3: Result := 'Loc';
      4: Result := 'Ter';
      else Result := '???';
    end;
    //
    Result := Letra + Geral.FF0(Nivel) + Result;
  end;
const
  MaxNivel = 4;
var
  cN1, Ativo, Nivel, I, Tabela: Integer;
  Campo, Codigo, AtivS, FldAtiv, SQL, _AND_, xFld, nFld: String;
}
begin
{
  FldAtiv := TDBAdvGrid(AdvGridDep).Columns[ACol].FieldName;
  if Copy(FldAtiv, 1, 1) = 'a' then
  begin
    Nivel := Geral.IMV(Copy(FldAtiv, 2, 1));
    if Nivel = 0 then
      Exit;
    cN1 := Geral.IMV(TDBAdvGrid(Sender).Cells[2, ARow]);
    Tabela := Geral.IMV(TDBAdvGrid(Sender).Cells[1, ARow]);
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq1, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_OS_Frm_Abr,
    'WHERE c1Dep=' + Geral.FF0(cN1),
    'AND Tabela=' + Geral.FF0(Tabela),
    '']);
    Ativo := QrPsq1.FieldByName(FldAtiv).AsInteger;
    Campo := CampoDeNivel(Nivel, 'c');
    Codigo := Geral.FF0(QrPsq1.FieldByName(Campo).AsInteger);
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    AtivS := Geral.FF0(Ativo);
    //
    SQL := '';
    for I := 1 to Nivel do
      SQL := SQL + ' ' + CampoDeNivel(I, 'a') + '=' + AtivS + ', ';
    SQL := SQL + 'Ativo=' + AtivS;
    //
    _AND_ := '';
    for I := Nivel to MaxNivel do
    begin
      xFld := CampoDeNivel(I, 'c');
      nFld := Geral.FF0(QrPsq1.FieldByName(xFld).AsInteger);
      _AND_ := _AND_ + 'AND ' + xFld + '=' + nFld + sLineBreak;
    end;
    //
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'UPDATE ' + F_OS_Frm_Abr,
    ' SET ',
    SQL,
    'WHERE Tabela=' + Geral.FF0(Tabela),
    //'AND ' + Campo + '=' + Codigo,
    _AND_ +
    '']);
    //
    Reopen_OS_Frm_Abr('Tabela;c1Dep', Tabela, cN1);
  end;
}
end;

procedure TFmOSFrmAbr.AtivarTudo(Ativa: Integer);
(*
var
  Codigo: Integer;
*)
begin
  Screen.Cursor := crHourGlass;
  try
    UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
    'UPDATE ' + F_OS_Frm_Abr ,
    'SET Ativo=' + Geral.FF0(Ativa),
    '']);
    //
    Reopen_OS_Frm_Abr(0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSFrmAbr.BtNenhumClick(Sender: TObject);
begin
  AtivarTudo(0);
end;

procedure TFmOSFrmAbr.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta: String;
  IDIts, Abrangicie: Integer;
begin
  IDIts := 0;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo selecionados');
    //
    Codigo   := Geral.FF0(FCodigo);
    Controle := Geral.FF0(FControle);
    Conta    := Geral.FF0(FConta);
    //
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'INSERT INTO livre2 (Tabela, Campo, Codigo, Vezes, AlterWeb, Ativo) ',
    'SELECT "osfrmabr" Tabela, "IDIts" Campo, ',
    'IDIts Codigo, 1 Vezes, 0 AlterWeb, 0 Ativo ',
    'FROM osfrmabr ',
    'WHERE Codigo=' + Codigo,
    'AND Controle=' + Controle,
    'AND Conta=' + Conta,
    ';',
    'DELETE FROM osfrmabr ',
    'WHERE Codigo=' + Codigo,
    'AND Controle=' + Controle,
    'AND Conta=' + Conta,
    '']);
    //
    QrOFA.Close;
    UnDmkDAC_PF.AbreMySQLQuery0(QrOFA, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_OS_Frm_Abr,
    'WHERE Ativo=1 ',
    'ORDER BY Nome',
    '']);
    QrOFA.First;
    while not QrOFA.Eof do
    begin
      IDIts          := 0;
      Abrangicie     := QrOFACodigo.Value;
      //
      IDIts := UMyMod.BPGS1I32_Reaproveita('osfrmabr', 'IDIts', '', '', tsPos, stIns, IDIts);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osfrmabr', False, [
      'Codigo', 'Controle', 'Conta',
      'Abrangicie'], [
      'IDIts'], [
      Codigo, Controle, Conta,
      Abrangicie], [
      IDIts], True);
      //
      QrOFA.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    OSApp_PF.ReopenOSFrmAbr(FQrOSFrmAbr, FConta, IDIts);
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSFrmAbr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSFrmAbr.BtTodosClick(Sender: TObject);
begin
  AtivarTudo(1);
end;

procedure TFmOSFrmAbr.dmkDBGrid1CellClick(Column: TColumn);
var
  Codigo, Ativo: Integer;
begin
  Codigo         := Qr_OS_Frm_AbrCodigo.Value;
  Ativo          := Qr_OS_Frm_AbrAtivo.Value;
  if Ativo = 0 then
    Ativo := 1
  else
    Ativo := 0;  
  //
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_OS_Frm_Abr, False, [
  'Ativo'], ['Codigo'
  ], [
  Ativo], [Codigo
  ], False) then
    Reopen_OS_Frm_Abr(Codigo);
end;

procedure TFmOSFrmAbr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := True;
end;

procedure TFmOSFrmAbr.FormCreate(Sender: TObject);
begin
  FConta := 0;
  FCriou := False;
end;

procedure TFmOSFrmAbr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSFrmAbr.Reopen_OS_Frm_Abr(Codigo: Integer);
begin
  Qr_OS_Frm_Abr.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr_OS_Frm_Abr, DModG.MyPID_DB, [
  'SELECT oa.*',
  'FROM ' + F_OS_Frm_Abr + ' oa ',
  'ORDER BY oa.Nome ',
  '']);
  Qr_OS_Frm_Abr.Locate('Codigo', Codigo, [])
end;

procedure TFmOSFrmAbr.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if not FCriou then
  begin
    Screen.Cursor := crHourGlass;
    try
      if FConta <> 0 then
      begin
        FCriou := True;
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela temporária!');
        F_OS_Frm_Abr :=
          UnCreateBugs.RecriaTempTableNovo(ntrtt_OS_Frm_Abr, DmodG.QrUpdPID1, False);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrOSFrmCab, Dmod.MyDB, [
        'SELECT Codigo, Controle, Conta, Nome ',
        'FROM osfrmcab ',
        'WHERE Conta=' + Geral.FF0(FConta),
        '']);
        //UMyMod.AbreQuery(QrOSFrmCab, Dmod.MyDB);
        //
        UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
        'INSERT INTO ' + F_OS_Frm_Abr,
        'SELECT Codigo, Nome, 0 Ativo ',
        'FROM ' + TMeuDB + '.abrangicie; ',
        '']);
        UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
        'UPDATE ' + F_OS_Frm_Abr,
        'SET Ativo=1 ',
        'WHERE Codigo IN ( ',
        '  SELECT abrangicie ',
        '  FROM ' + TMeuDB + '.osfrmabr ',
        '  WHERE Conta=' + Geral.FF0(FConta),
        ')',
        '']);
        //
        Reopen_OS_Frm_Abr(0);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      end;
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
