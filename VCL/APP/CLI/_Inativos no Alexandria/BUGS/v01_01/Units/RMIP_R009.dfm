object FmRMIP_R009: TFmRMIP_R009
  Left = 0
  Top = 0
  Caption = 'RMIP 009'
  ClientHeight = 457
  ClientWidth = 299
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object frxReport009A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41864.484406273150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo17OnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  X: Extended;                                    '
      'begin'
      '  if <frxDs009A_OSPipSUM."ITENS"> = 0 then           '
      '    X := 0'
      '  else'
      
        '    X := <frxDs009A_OSPipIts."ITENS"> / <frxDs009A_OSPipSUM."ITE' +
        'NS"> * 100;'
      '  //          '
      
        '  Memo17.Memo.Text := FormatFloat('#39'0.00'#39', X);                   ' +
        '                             '
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxReport009AGetValue
    Left = 52
    Top = 8
    Datasets = <
      item
        DataSet = frxDs009A_OSPipIts
        DataSetName = 'frxDs009A_OSPipIts'
      end
      item
        DataSet = frxDs009A_OSs
        DataSetName = 'frxDs009A_OSs'
      end
      item
        DataSet = frxDs009A_PrgLstIts
        DataSetName = 'frxDs009A_PrgLstIts'
      end
      item
        DataSet = frxDs009A_OSPipSUM
        DataSetName = 'frxDs009A_OSPipSUM'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        Height = 124.724463150000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 60.472480000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 41.574803150000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 83.149660000000000000
          Width = 680.315400000000000000
          Height = 41.574803150000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVISO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        Height = 56.692950000000010000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDs009A_PrgLstIts
        DataSetName = 'frxDs009A_PrgLstIts'
        KeepTogether = True
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Question'#225'rio: [frxDs009A_PrgLstIts."NO_LST"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pergunta: [frxDs009A_PrgLstIts."Nome"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 634.961040000000000000
          Top = 37.795300000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDs009A_OSPipIts
          DataSetName = 'frxDs009A_OSPipIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Percetual')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 574.488560000000000000
          Top = 37.795300000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDs009A_OSPipIts
          DataSetName = 'frxDs009A_OSPipIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Itens')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Top = 37.795300000000000000
          Width = 574.488560000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDs009A_OSPipIts
          DataSetName = 'frxDs009A_OSPipIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Resposta')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 498.897960000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        Height = 18.897650000000000000
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        DataSet = frxDs009A_OSPipIts
        DataSetName = 'frxDs009A_OSPipIts'
        KeepTogether = True
        RowCount = 0
        object Memo17: TfrxMemoView
          Left = 634.961040000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'Memo17OnBeforePrint'
          ShowHint = False
          DataSet = frxDs009A_OSPipIts
          DataSetName = 'frxDs009A_OSPipIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '???')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 574.488560000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDs009A_OSPipIts
          DataSetName = 'frxDs009A_OSPipIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs009A_OSPipIts."ITENS"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Width = 574.488560000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Resposta'
          DataSet = frxDs009A_OSPipIts
          DataSetName = 'frxDs009A_OSPipIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs009A_OSPipIts."Resposta"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 37.795300000000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          Top = 18.897649999999880000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 139.842610000000000000
          Top = 18.897649999999880000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lugar')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 540.472790000000000000
          Top = 18.897649999999880000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status da OS')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 52.913420000000000000
          Top = 18.897649999999880000
          Width = 86.929133859999990000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Final da execu'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 442.205010000000000000
          Top = 18.897649999999880000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fato gerador')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 627.401980000000000000
          Top = 18.897649999999880000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID Contrato')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ordens de servi'#231'o envolvidas')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 11.338590000000000000
        Top = 427.086890000000000000
        Width = 680.315400000000000000
      end
      object DetailData2: TfrxDetailData
        Height = 18.897650000000000000
        Top = 385.512060000000000000
        Width = 680.315400000000000000
        DataSet = frxDs009A_OSs
        DataSetName = 'frxDs009A_OSs'
        KeepTogether = True
        RowCount = 0
        object Memo4: TfrxMemoView
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs009A_OSs."Codigo"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 139.842610000000000000
          Width = 302.362365830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_SiapTerCad'
          DataSet = frxDs009A_OSs
          DataSetName = 'frxDs009A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs009A_OSs."NO_SiapTerCad"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 540.472790000000000000
          Width = 86.929155830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_ESTATUS'
          DataSet = frxDs009A_OSs
          DataSetName = 'frxDs009A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs009A_OSs."NO_ESTATUS"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 52.913420000000000000
          Width = 86.929133859999990000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DtaExeFim'
          DataSet = frxDs009A_OSs
          DataSetName = 'frxDs009A_OSs'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs009A_OSs."DtaExeFim"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 442.205010000000000000
          Width = 98.267745830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_FatoGeradr'
          DataSet = frxDs009A_OSs
          DataSetName = 'frxDs009A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs009A_OSs."NO_FatoGeradr"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NumContrat'
          DataSet = frxDs009A_OSs
          DataSetName = 'frxDs009A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs009A_OSs."NumContrat"]')
          ParentFont = False
        end
      end
    end
  end
  object Qr009A_PrgLstIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = Qr009A_PrgLstItsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts, plc.Nome NO_LST,'
      'plp.Sigla, plp.Nome'
      'FROM ospipits opi'
      'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab'
      'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts'
      'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta'
      'WHERE opi.Codigo=879'
      'AND pli.Aplicacao & 1'
      'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome')
    Left = 52
    Top = 56
    object Qr009A_PrgLstItsPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object Qr009A_PrgLstItsPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
    object Qr009A_PrgLstItsNO_LST: TWideStringField
      FieldName = 'NO_LST'
      Size = 100
    end
    object Qr009A_PrgLstItsSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 100
    end
    object Qr009A_PrgLstItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Qr009A_PrgLstItsFuncoes: TIntegerField
      FieldName = 'Funcoes'
    end
    object Qr009A_PrgLstItsCodigo: TFloatField
      FieldName = 'Codigo'
    end
  end
  object frxDs009A_PrgLstIts: TfrxDBDataset
    UserName = 'frxDs009A_PrgLstIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PrgLstCab=PrgLstCab'
      'PrgLstIts=PrgLstIts'
      'NO_LST=NO_LST'
      'Sigla=Sigla'
      'Nome=Nome'
      'Funcoes=Funcoes'
      'Codigo=Codigo')
    DataSet = Qr009A_PrgLstIts
    BCDToCurrency = False
    Left = 52
    Top = 104
  end
  object Qr009A_OSPipIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(opi.RespAtrIts) ITENS, '
      'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts,'
      'IF(opi.Respondido=0, "N'#195'O RESPONDIDO", '
      'IF(opi.RespAtrIts > 0, pat.Nome, pac.Padrao)) Resposta,'
      'IF(opi.Respondido=0, 8421504/*cinza*/, '
      'IF(opi.RespAtrIts > 0, pat.CorPie, pac.CorPie)) + 0.000 CorPizza'
      'FROM ospipits opi'
      'LEFT JOIN prgatrcad pac ON pac.Codigo=opi.PrgLstCab'
      'LEFT JOIN prgatrits pat ON pat.Controle=opi.RespAtrIts'
      'WHERE opi.Codigo=879'
      'AND opi.PrgLstIts=2'
      'GROUP BY opi.Respondido, opi.RespAtrCad, opi.RespAtrIts'
      'ORDER BY ITENS DESC')
    Left = 52
    Top = 152
    object Qr009A_OSPipItsITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object Qr009A_OSPipItsRespondido: TSmallintField
      FieldName = 'Respondido'
      Origin = 'ospipits.Respondido'
    end
    object Qr009A_OSPipItsRespAtrCad: TIntegerField
      FieldName = 'RespAtrCad'
      Origin = 'ospipits.RespAtrCad'
    end
    object Qr009A_OSPipItsRespAtrIts: TIntegerField
      FieldName = 'RespAtrIts'
      Origin = 'ospipits.RespAtrIts'
    end
    object Qr009A_OSPipItsResposta: TWideStringField
      FieldName = 'Resposta'
      Size = 50
    end
    object Qr009A_OSPipItsCorPizza: TFloatField
      FieldName = 'CorPizza'
    end
    object Qr009A_OSPipItsDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
  end
  object frxDs009A_OSPipIts: TfrxDBDataset
    UserName = 'frxDs009A_OSPipIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ITENS=ITENS'
      'Respondido=Respondido'
      'RespAtrCad=RespAtrCad'
      'RespAtrIts=RespAtrIts'
      'Resposta=Resposta'
      'CorPizza=CorPizza'
      'DtaExeFim=DtaExeFim')
    DataSet = Qr009A_OSPipIts
    BCDToCurrency = False
    Left = 52
    Top = 200
  end
  object Qr009A_OSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, cab.Entidade, cab.SiapTerCad, cab.Estatus, '
      ' cab.DtaExeFim, cab.NumContrat, cab.Grupo,'
      'fge.Nome NO_FatoGeradr,'
      'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM oscab cab'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad'
      'WHERE cab.Codigo=0')
    Left = 52
    Top = 248
    object Qr009A_OSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr009A_OSsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object Qr009A_OSsSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object Qr009A_OSsEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object Qr009A_OSsDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object Qr009A_OSsNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object Qr009A_OSsGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object Qr009A_OSsNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
    end
    object Qr009A_OSsNO_ESTATUS: TWideStringField
      FieldName = 'NO_ESTATUS'
      Size = 60
    end
    object Qr009A_OSsNO_SiapTerCad: TWideStringField
      FieldName = 'NO_SiapTerCad'
      Size = 100
    end
    object Qr009A_OSsNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object frxDs009A_OSs: TfrxDBDataset
    UserName = 'frxDs009A_OSs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Entidade=Entidade'
      'SiapTerCad=SiapTerCad'
      'Estatus=Estatus'
      'DtaExeFim=DtaExeFim'
      'NumContrat=NumContrat'
      'Grupo=Grupo'
      'NO_FatoGeradr=NO_FatoGeradr'
      'NO_ESTATUS=NO_ESTATUS'
      'NO_SiapTerCad=NO_SiapTerCad'
      'NO_ENT=NO_ENT')
    DataSet = Qr009A_OSs
    BCDToCurrency = False
    Left = 52
    Top = 296
  end
  object Qr009A_OSPipSUM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(opi.RespAtrIts) ITENS, '
      'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts,'
      'IF(opi.Respondido=0, "N'#195'O RESPONDIDO", '
      'IF(opi.RespAtrIts > 0, pat.Nome, pac.Padrao)) Resposta,'
      'IF(opi.Respondido=0, 8421504/*cinza*/, '
      'IF(opi.RespAtrIts > 0, pat.CorPie, pac.CorPie)) + 0.000 CorPizza'
      'FROM ospipits opi'
      'LEFT JOIN prgatrcad pac ON pac.Codigo=opi.PrgLstCab'
      'LEFT JOIN prgatrits pat ON pat.Controle=opi.RespAtrIts'
      'WHERE opi.Codigo=879'
      'AND opi.PrgLstIts=2'
      'GROUP BY opi.Respondido, opi.RespAtrCad, opi.RespAtrIts'
      'ORDER BY ITENS DESC')
    Left = 48
    Top = 344
    object Qr009A_OSPipSUMITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object frxDs009A_OSPipSUM: TfrxDBDataset
    UserName = 'frxDs009A_OSPipSUM'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ITENS=ITENS')
    DataSet = Qr009A_OSPipSUM
    BCDToCurrency = False
    Left = 48
    Top = 392
  end
end
