object DfModAppGraG1: TDfModAppGraG1
  Left = 0
  Top = 0
  Caption = 'DfModAppGraG1'
  ClientHeight = 347
  ClientWidth = 184
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 120
  TextHeight = 17
  object QrGraG1EqAp: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT * FROM grag1eqap'
      'WHERE Nivel1=1')
    Left = 40
    Top = 4
    object QrGraG1EqApNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraG1EqApMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraG1EqApObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGraG1EqApNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraG1EqApUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraG1EqApReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
  end
  object QrGraG1EqMo: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT * FROM grag1eqmo'
      'WHERE Nivel1=1')
    Left = 40
    Top = 52
    object QrGraG1EqMoNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraG1EqMoMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraG1EqMoObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGraG1EqMoPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrGraG1EqMoNaoUsaPrdt: TSmallintField
      FieldName = 'NaoUsaPrdt'
    end
    object QrGraG1EqMoVetorArq: TWideStringField
      FieldName = 'VetorArq'
      Size = 255
    end
    object QrGraG1EqMoVetorSVG: TWideMemoField
      FieldName = 'VetorSVG'
      BlobType = ftWideMemo
    end
    object QrGraG1EqMoPrgICorSVG: TIntegerField
      FieldName = 'PrgICorSVG'
    end
    object QrGraG1EqMoDiasCorSVG: TIntegerField
      FieldName = 'DiasCorSVG'
    end
    object QrGraG1EqMoQtCorSVGgg: TIntegerField
      FieldName = 'QtCorSVGgg'
    end
    object QrGraG1EqMoQtCorSVGgr: TIntegerField
      FieldName = 'QtCorSVGgr'
    end
    object QrGraG1EqMoQtCorSVGrg: TIntegerField
      FieldName = 'QtCorSVGrg'
    end
    object QrGraG1EqMoQtCorSVGrr: TIntegerField
      FieldName = 'QtCorSVGrr'
    end
    object QrGraG1EqMoQtCorSVGrb: TIntegerField
      FieldName = 'QtCorSVGrb'
    end
    object QrGraG1EqMoQtCorSVGbr: TIntegerField
      FieldName = 'QtCorSVGbr'
    end
    object QrGraG1EqMoSVGLegenda: TWideStringField
      FieldName = 'SVGLegenda'
      Size = 50
    end
    object QrGraG1EqMoZumPadr: TFloatField
      FieldName = 'ZumPadr'
    end
    object QrGraG1EqMoNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraG1EqMoUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraG1EqMoFrmApres: TIntegerField
      FieldName = 'FrmApres'
    end
    object QrGraG1EqMoReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
  end
  object QrGraG1PrAp: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT pap.* , gg1.UnidMed, med.SIGLA, '
      'med.CodUsu CU_UnidMed'
      'FROM grag1prap pap'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pap.Nivel1'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE pap.Nivel1>0')
    Left = 40
    Top = 100
    object QrGraG1PrApNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraG1PrApRegMinSaud: TWideStringField
      FieldName = 'RegMinSaud'
      Size = 60
    end
    object QrGraG1PrApMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraG1PrApImpInOrca: TSmallintField
      FieldName = 'ImpInOrca'
    end
    object QrGraG1PrApObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGraG1PrApFoneCIT: TWideStringField
      FieldName = 'FoneCIT'
      Size = 18
    end
    object QrGraG1PrApFoneCEATOX: TWideStringField
      FieldName = 'FoneCEATOX'
      Size = 18
    end
    object QrGraG1PrApToxicidade: TWideStringField
      FieldName = 'Toxicidade'
      Size = 255
    end
    object QrGraG1PrApConcentrac: TWideStringField
      FieldName = 'Concentrac'
      Size = 255
    end
    object QrGraG1PrApAcaoToxica: TWideStringField
      FieldName = 'AcaoToxica'
      Size = 255
    end
    object QrGraG1PrApAntidoto: TWideStringField
      FieldName = 'Antidoto'
      Size = 255
    end
    object QrGraG1PrApUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraG1PrApSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrGraG1PrApCU_UnidMed: TIntegerField
      FieldName = 'CU_UnidMed'
      Required = True
    end
    object QrGraG1PrApAtuNumLot: TWideStringField
      FieldName = 'AtuNumLot'
      Size = 60
    end
    object QrGraG1PrApAtuNumLaud: TWideStringField
      FieldName = 'AtuNumLaud'
      Size = 60
    end
    object QrGraG1PrApNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraG1PrApReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraG1PrApFrmApres: TIntegerField
      FieldName = 'FrmApres'
    end
  end
  object QrGraG1PrMo: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT pmo.* , gg1.UnidMed, med.SIGLA,'
      'med.CodUsu CU_UnidMed'
      'FROM grag1prmo pmo'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pmo.Nivel1'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE pmo.Nivel1>0')
    Left = 40
    Top = 148
    object QrGraG1PrMoNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraG1PrMoRegMinSaud: TWideStringField
      FieldName = 'RegMinSaud'
      Size = 60
    end
    object QrGraG1PrMoMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraG1PrMoImpInOrca: TSmallintField
      FieldName = 'ImpInOrca'
    end
    object QrGraG1PrMoObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGraG1PrMoFoneCIT: TWideStringField
      FieldName = 'FoneCIT'
      Size = 18
    end
    object QrGraG1PrMoFoneCEATOX: TWideStringField
      FieldName = 'FoneCEATOX'
      Size = 18
    end
    object QrGraG1PrMoToxicidade: TWideStringField
      FieldName = 'Toxicidade'
      Size = 255
    end
    object QrGraG1PrMoConcentrac: TWideStringField
      FieldName = 'Concentrac'
      Size = 255
    end
    object QrGraG1PrMoAcaoToxica: TWideStringField
      FieldName = 'AcaoToxica'
      Size = 255
    end
    object QrGraG1PrMoAntidoto: TWideStringField
      FieldName = 'Antidoto'
      Size = 255
    end
    object QrGraG1PrMoValCliDd: TIntegerField
      FieldName = 'ValCliDd'
    end
    object QrGraG1PrMoUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraG1PrMoSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrGraG1PrMoCU_UnidMed: TIntegerField
      FieldName = 'CU_UnidMed'
      Required = True
    end
    object QrGraG1PrMoAtuNumLot: TWideStringField
      FieldName = 'AtuNumLot'
      Size = 60
    end
    object QrGraG1PrMoAtuNumLaud: TWideStringField
      FieldName = 'AtuNumLaud'
      Size = 60
    end
    object QrGraG1PrMoNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraG1PrMoFrmApres: TIntegerField
      FieldName = 'FrmApres'
    end
    object QrGraG1PrMoReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
  end
  object QrGraG1Veic: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT pmo.* , gg1.UnidMed, med.SIGLA,'
      'med.CodUsu CU_UnidMed'
      'FROM grag1prmo pmo'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pmo.Nivel1'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE pmo.Nivel1>0')
    Left = 40
    Top = 196
    object QrGraG1VeicNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraG1VeicMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraG1VeicPlaca: TWideStringField
      FieldName = 'Placa'
      Size = 10
    end
    object QrGraG1VeicCustoKm: TFloatField
      FieldName = 'CustoKm'
      DisplayFormat = '#,###,##0.00'
    end
    object QrGraG1VeicUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraG1VeicNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraG1VeicReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
  end
end
