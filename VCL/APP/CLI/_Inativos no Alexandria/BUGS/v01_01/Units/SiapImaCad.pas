unit SiapImaCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  Menus, dmkImage, UnDmkEnums;

type
  TFmSiapImaCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Panel8: TPanel;
    Label16: TLabel;
    EdSCompl2: TdmkEdit;
    EdCodigo: TdmkEdit;
    Label2: TLabel;
    EdTer_Nome: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdM2Constru: TdmkEdit;
    EdM2NaoBuild: TdmkEdit;
    EdM2Terreno: TdmkEdit;
    EdM2Total: TdmkEdit;
    EdSiapImaTer: TdmkEdit;
    QrObjetos: TmySQLQuery;
    DsObjetos: TDataSource;
    QrFinalidads: TmySQLQuery;
    DsFinalidads: TDataSource;
    QrTpConstrus: TmySQLQuery;
    DsTpConstrus: TDataSource;
    EdObjeto: TdmkEditCB;
    Label1: TLabel;
    CBObjeto: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdFinalidade: TdmkEditCB;
    CBFinalidade: TdmkDBLookupComboBox;
    Label7: TLabel;
    EdTpConstru: TdmkEditCB;
    CBTpConstru: TdmkDBLookupComboBox;
    QrObjetosCodigo: TIntegerField;
    QrObjetosNome: TWideStringField;
    QrFinalidadsCodigo: TIntegerField;
    QrFinalidadsNome: TWideStringField;
    QrTpConstrusCodigo: TIntegerField;
    QrTpConstrusNome: TWideStringField;
    SbObjeto: TSpeedButton;
    SbFinalidade: TSpeedButton;
    SbTpConstru: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdM2ConstruChange(Sender: TObject);
    procedure EdM2NaoBuildChange(Sender: TObject);
    procedure SbObjetoClick(Sender: TObject);
    procedure SbFinalidadeClick(Sender: TObject);
    procedure SbTpConstruClick(Sender: TObject);
    procedure EdSCompl2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    //FSCEP: String;
  public
    { Public declarations }
    FCompl: String;
    FForm: TForm;
  end;

  var
  FmSiapImaCad: TFmSiapImaCad;

implementation

uses UnMyObjects, Module, EntiCEP, MyDBCheck, UnCEP, CunsCad, UMySQLModule,
  Principal, UnSACAll_PF;

{$R *.DFM}

procedure TFmSiapImaCad.BtOKClick(Sender: TObject);
var
  SCompl2: String;
  Objeto, Finalidade, TpConstru, Codigo, SiapImaTer: Integer;
  M2Constru, M2NaoBuild, M2Terreno, M2Total: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  SiapImaTer     := EdSiapImaTer.ValueVariant;
  M2Constru      := EdM2Constru.ValueVariant;
  M2NaoBuild     := EdM2NaoBuild.ValueVariant;
  M2Terreno      := EdM2Terreno.ValueVariant;
  M2Total        := EdM2Total.ValueVariant;
  //
  Objeto         := EdObjeto.ValueVariant;
  Finalidade     := EdFinalidade.ValueVariant;
  TpConstru      := EdTpConstru.ValueVariant;
  //
  SCompl2        := EdSCompl2.Text;
  //
  Codigo := UMyMod.BPGS1I32(
    'siapimacad', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'siapimacad', false, [
  'SiapImaTer', 'Objeto', 'Finalidade',
  'M2Constru', 'M2NaoBuild', 'M2Terreno',
  'M2Total', 'TpConstru', 'SCompl2'], [
  'Codigo'], [
  SiapImaTer, Objeto, Finalidade,
  M2Constru, M2NaoBuild, M2Terreno,
  M2Total, TpConstru, SCompl2], [
  Codigo], True) then
  begin
    TFmCunsCad(FForm).ReopenSiapImaCad(Codigo);
    Close;
  end;
end;

procedure TFmSiapImaCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSiapImaCad.EdM2ConstruChange(Sender: TObject);
begin
  SACAll_PF.CalculaM2Total(EdM2Constru, EdM2NaoBuild, EdM2Total);
end;

procedure TFmSiapImaCad.EdM2NaoBuildChange(Sender: TObject);
begin
  SACAll_PF.CalculaM2Total(EdM2Constru, EdM2NaoBuild, EdM2Total);
end;

procedure TFmSiapImaCad.EdSCompl2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdSCompl2.ValueVariant := FCompl;
end;

procedure TFmSiapImaCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSiapImaCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  QrObjetos.Open;
  QrTpConstrus.Open;
  QrFinalidads.Open;
end;

procedure TFmSiapImaCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSiapImaCad.SbFinalidadeClick(Sender: TObject);
begin
  FmPrincipal.MostraFormFinalidads();
  UMyMod.SetaCodigoPesquisado(EdFinalidade, CBFinalidade, QrFinalidads, VAR_CADASTRO);
end;

procedure TFmSiapImaCad.SbObjetoClick(Sender: TObject);
begin
  FmPrincipal.MostraFormObjetos();
  UMyMod.SetaCodigoPesquisado(EdObjeto, CBObjeto, QrObjetos, VAR_CADASTRO);
end;

procedure TFmSiapImaCad.SbTpConstruClick(Sender: TObject);
begin
  FmPrincipal.MostraFormTpConstrus();
  UMyMod.SetaCodigoPesquisado(EdTpConstru, CBTpConstru, QrTpConstrus, VAR_CADASTRO);
end;

end.
