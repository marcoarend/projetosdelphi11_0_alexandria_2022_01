unit PipPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnBugs_Tabs, UnDmkEnums, dmkDBGridZTO, UnDmkProcFunc;

type
  TFmPipPesq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEquipAplic: TmySQLQuery;
    QrEquipAplicControle: TIntegerField;
    QrEquipAplicNome: TWideStringField;
    DsEquipAplic: TDataSource;
    QrMotDesativ: TmySQLQuery;
    DsMotDesativ: TDataSource;
    QrMotDesativCodigo: TIntegerField;
    QrMotDesativNome: TWideStringField;
    PnGeral: TPanel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    TPDtaAquisIni: TdmkEditDateTimePicker;
    Label2: TLabel;
    EdEquipamento: TdmkEditCB;
    CBEquipamento: TdmkDBLookupComboBox;
    PnDesativa: TPanel;
    CBMotDesativ: TdmkDBLookupComboBox;
    TPDtaDesativIni: TdmkEditDateTimePicker;
    Label3: TLabel;
    EdMotDesativ: TdmkEditCB;
    Label4: TLabel;
    Label5: TLabel;
    EdPrgLstCab: TdmkEditCB;
    CBPrgLstCab: TdmkDBLookupComboBox;
    QrPrgLstCab: TmySQLQuery;
    QrPrgLstCabCodigo: TIntegerField;
    QrPrgLstCabNome: TWideStringField;
    DsPrgLstCab: TDataSource;
    DsSiapImaDep: TDataSource;
    QrSiapImaDep: TmySQLQuery;
    QrSiapImaDepNO_DEPENDENCI: TWideStringField;
    QrSiapImaDepControle: TIntegerField;
    Label6: TLabel;
    EdDependenci: TdmkEditCB;
    CBDependenci: TdmkDBLookupComboBox;
    Label11: TLabel;
    EdMotInutili: TdmkEditCB;
    CBMotInutili: TdmkDBLookupComboBox;
    Label10: TLabel;
    TPDtaInutiliIni: TdmkEditDateTimePicker;
    QrMotInutili: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsMotInutili: TDataSource;
    dmkDBGridZTO1: TdmkDBGridZTO;
    TPDtaAquisFim: TdmkEditDateTimePicker;
    Label7: TLabel;
    TPDtaDesativFim: TdmkEditDateTimePicker;
    Label8: TLabel;
    Label12: TLabel;
    TPDtaInutiliFim: TdmkEditDateTimePicker;
    QrPipCad: TmySQLQuery;
    QrPipCadNO_MOTDESAT: TWideStringField;
    QrPipCadNO_EQUI: TWideStringField;
    QrPipCadNO_LISTA: TWideStringField;
    QrPipCadCodigo: TIntegerField;
    QrPipCadNome: TWideStringField;
    QrPipCadEquipamento: TIntegerField;
    QrPipCadOSMonCab: TIntegerField;
    QrPipCadMotDesativ: TIntegerField;
    QrPipCadDtaAquis: TDateField;
    QrPipCadDtaDesativ: TDateField;
    QrPipCadPrgLstCab: TIntegerField;
    QrPipCadDependenci: TIntegerField;
    QrPipCadDtaAquis_TXT: TWideStringField;
    QrPipCadDtaDesativ_TXT: TWideStringField;
    QrPipCadNO_DEPENDENCIA: TWideStringField;
    QrPipCadDtaInutili_TXT: TWideStringField;
    QrPipCadMotInutili: TIntegerField;
    QrPipCadNO_MOTINUTI: TWideStringField;
    DsPipCad: TDataSource;
    Panel5: TPanel;
    Label13: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdEntContrat: TdmkEditCB;
    CBEntContrat: TdmkDBLookupComboBox;
    QrEntContrat: TmySQLQuery;
    QrEntContratCodigo: TIntegerField;
    QrEntContratCliInt: TIntegerField;
    QrEntContratCodUsu: TIntegerField;
    QrEntContratNOMEENTIDADE: TWideStringField;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesCodUsu: TIntegerField;
    QrClientesCliInt: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    DsEntContrat: TDataSource;
    QrPipCadNO_ENTIDADE: TWideStringField;
    QrPipCadNO_ENTCONTRAT: TWideStringField;
    LaTotal: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dmkDBGridZTO1DblClick(Sender: TObject);
    procedure QrPipCadAfterScroll(DataSet: TDataSet);
    procedure QrPipCadBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigoSel: Integer;

    //
    procedure ReopenSiapImaDep(SiapterCad: Integer);
  end;

  var
  FmPipPesq: TFmPipPesq;

implementation

uses UnMyObjects, Module, ModOS, Principal, UMySQLModule, DmkDAC_PF,
UnOSApp_PF, PrgLstCab;

{$R *.DFM}

procedure TFmPipPesq.BtOKClick(Sender: TObject);
var
  Nome, SQLDtaAquis, SQLDtaDesativ, SQLDtaInutili: String;
  DtaAquisIni, DtaAquisFim, DtaDesativIni, DtaDesativFim, DtaInutiliIni,
  DtaInutiliFim: TDateTime;
  Equipamento, MotDesativ, MotInutili, PrgLstCab, Dependenci, Entidade,
  EntContrat: Integer;
  OkDtaAquis, OkDtaDesativ, OkDtaInutili: Boolean;
begin
  Nome := EdNome.ValueVariant;
  PrgLstCab     := EdPrgLstCab.ValueVariant;
  DtaAquisIni   := TPDtaAquisIni.Date;
  DtaAquisFim   := TPDtaAquisFim.Date;
  Equipamento   := EdEquipamento.ValueVariant;
  Dependenci    := EdDependenci.ValueVariant;
  DtaDesativIni := TPDtaDesativIni.Date;
  DtaDesativFim := TPDtaDesativFim.Date;
  MotDesativ    := EdMotDesativ.ValueVariant;
  DtaInutiliIni := TPDtaInutiliIni.Date;
  DtaInutiliFim := TPDtaInutiliFim.Date;
  MotInutili    := EdMotInutili.ValueVariant;
  Entidade      := EdEntidade.ValueVariant;
  EntContrat    := EdEntContrat.ValueVariant;
  //
  OkDtaAquis := (DtaAquisFim > DtaAquisIni) or (DtaAquisFim > 2);
  OkDtaDesativ := (DtaDesativFim > DtaDesativIni) or (DtaDesativFim > 2);
  OkDtaInutili := (DtaInutiliFim > DtaInutiliIni) or (DtaInutiliFim > 2);
  //
  SQLDtaAquis := dmkPF.SQL_Periodo('AND cad.DtaAquis', DtaAquisIni, DtaAquisFim, True, True);
  SQLDtaDesativ := dmkPF.SQL_Periodo('AND cad.DtaDesativ', DtaDesativIni, DtaDesativFim, True, True);
  SQLDtaInutili := dmkPF.SQL_Periodo('AND cad.DtaInutili', DtaInutiliIni, DtaInutiliFim, True, True);

  UnDmkDAC_PF.AbreMySQLQuery0(QrPipCad, Dmod.MyDB, [
  'SELECT omc.Codigo Localizador, omc.Conta OSMonCab,',
  'mot.Nome NO_MOTDESAT, inu.Nome NO_MOTINUTI,',
  'gg1.Nome NO_EQUI, plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA,',
  'IF(cad.DtaAquis <= "1900-01-01", "",',
  '  DATE_FORMAT(cad.DtaAquis, "%d/%m/%y")) DtaAquis_TXT,',
  'IF(cad.DtaDesativ<= "1900-01-01", "",',
  '  DATE_FORMAT(cad.DtaDesativ, "%d/%m/%y")) DtaDesativ_TXT,',
  'IF(cad.DtaInutili<= "1900-01-01", "",',
  '  DATE_FORMAT(cad.DtaInutili, "%d/%m/%y")) DtaInutili_TXT, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENTIDADE, ',
  'IF(ect.Tipo=0, ect.RazaoSocial, ECT.Nome) NO_ENTCONTRAT, ',
  'cad.*',
  'FROM pipcad cad',
  'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ',
  'LEFT JOIN motdesativ inu ON inu.Codigo=cad.MotInutili',
  'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab',
  'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci',
  'LEFT JOIN osmoncab omc ON omc.Conta=cad.OSMonCab',
  'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo ',
  'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
  'LEFT JOIN entidades ect ON ect.Codigo=cab.EntContrat  ',
  'WHERE cad.Codigo <> 0',
  Geral.ATS_If(Trim(Nome) <> '', ['AND cad.Nome LIKE "%' + Nome + '%"']),
  Geral.ATS_If(Equipamento <> 0, ['AND cad.Equipamento=' + Geral.FF0(Equipamento)]),
  Geral.ATS_If(PrgLstCab <> 0, ['AND cad.PrgLstCab=' + Geral.FF0(PrgLstCab)]),
  Geral.ATS_If(Dependenci <> 0, ['AND cad.Dependenci=' + Geral.FF0(Dependenci)]),
  Geral.ATS_If(MotDesativ <> 0, ['AND cad.MotDesativ=' + Geral.FF0(MotDesativ)]),
  Geral.ATS_If(Motinutili <> 0, ['AND cad.MotInutili=' + Geral.FF0(MotInutili)]),
  Geral.ATS_If(OkDtaAquis, [SQLDtaAquis]),
  Geral.ATS_If(OkDtaAquis, [SQLDtaAquis]),
  Geral.ATS_If(OkDtaAquis, [SQLDtaAquis]),
  Geral.ATS_If(Entidade <> 0, ['AND cab.Entidade=' + Geral.FF0(Entidade)]),
  Geral.ATS_If(EntContrat <> 0, ['AND cab.EntContrat=' + Geral.FF0(EntContrat)]),
  '']);
end;

procedure TFmPipPesq.BtSaidaClick(Sender: TObject);
begin
  FCodigoSel := 0;
  Close;
end;

procedure TFmPipPesq.dmkDBGridZTO1DblClick(Sender: TObject);
begin
  FCodigoSel := QrPipCadCodigo.Value;
  Close;
end;

procedure TFmPipPesq.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdNome.CharCase := ecNormal;
end;

procedure TFmPipPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPipPesq.FormCreate(Sender: TObject);
begin
  FCodigoSel := 0;
  ImgTipo.SQLType := stPsq;
  UnDmkDAC_PF.AbreQuery(QrPrgLstCab, Dmod.MyDB);
  OSApp_PF.ReopenEquipAplic(QrEquipAplic, gbsMonitora);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntContrat, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrMotDesativ, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM motdesativ ',
  'ORDER BY Nome ',
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrMotInutili, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM motdesativ ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmPipPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPipPesq.QrPipCadAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrPipCad.RecordCount)
end;

procedure TFmPipPesq.QrPipCadBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmPipPesq.ReopenSiapImaDep(SiapterCad: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapImaDep, Dmod.MyDB, [
  'SELECT sid.Controle, CONCAT(sic.SCompl2, " => ", dep.Nome) NO_DEPENDENCI ',
  'FROM siapimadep sid ',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
  'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=sic.SiapImaTer ',
  'WHERE stc.Cliente=' + Geral.FF0(SiapTerCad),
  'ORDER BY NO_DEPENDENCI ',
  ' ']);
end;

end.
