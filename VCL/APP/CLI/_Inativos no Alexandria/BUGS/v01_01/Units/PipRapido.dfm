object FmPipRapido: TFmPipRapido
  Left = 339
  Top = 185
  Caption = 'PIP-GEREN-002 :: Cadastro de PMV'
  ClientHeight = 434
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 633
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 585
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 537
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 214
        Height = 32
        Caption = 'Cadastro de PMV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 214
        Height = 32
        Caption = 'Cadastro de PMV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 214
        Height = 32
        Caption = 'Cadastro de PMV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 633
    Height = 272
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 633
      Height = 272
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 633
        Height = 272
        Align = alClient
        TabOrder = 0
        object PnGeral: TPanel
          Left = 2
          Top = 15
          Width = 629
          Height = 162
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 16
            Top = 0
            Width = 14
            Height = 13
            Caption = 'ID:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label9: TLabel
            Left = 76
            Top = 0
            Width = 191
            Height = 13
            Caption = 'Refer'#234'ncia: (F4 para permitir min'#250'sculas)'
            Color = clBtnFace
            ParentColor = False
          end
          object Label1: TLabel
            Left = 504
            Top = 0
            Width = 89
            Height = 13
            Caption = 'Data de aquisi'#231#227'o:'
          end
          object Label2: TLabel
            Left = 16
            Top = 40
            Width = 65
            Height = 13
            Caption = 'Equipamento:'
          end
          object SbEquipAplic: TSpeedButton
            Left = 595
            Top = 56
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbEquipAplicClick
          end
          object Label5: TLabel
            Left = 16
            Top = 80
            Width = 177
            Height = 13
            Caption = 'Lista de perguntas no monitoramento:'
          end
          object SbPrgLstCab: TSpeedButton
            Left = 595
            Top = 96
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbPrgLstCabClick
          end
          object Label6: TLabel
            Left = 16
            Top = 120
            Width = 188
            Height = 13
            Caption = 'Depend'#234'ncia onde o PMV se encontra:'
          end
          object SpeedButton1: TSpeedButton
            Left = 595
            Top = 136
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object EdCodigo: TdmkEdit
            Left = 16
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNome: TdmkEdit
            Left = 76
            Top = 16
            Width = 425
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnKeyDown = EdNomeKeyDown
          end
          object TPDtaAquis: TdmkEditDateTimePicker
            Left = 504
            Top = 16
            Width = 112
            Height = 21
            Date = -3652.857134502316000000
            Time = -3652.857134502316000000
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdEquipamento: TdmkEditCB
            Left = 16
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EquipAplic'
            UpdCampo = 'EquipAplic'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEquipamento
            IgnoraDBLookupComboBox = False
          end
          object CBEquipamento: TdmkDBLookupComboBox
            Left = 72
            Top = 56
            Width = 521
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsEquipAplic
            TabOrder = 4
            dmkEditCB = EdEquipamento
            QryCampo = 'EquipAplic'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPrgLstCab: TdmkEditCB
            Left = 16
            Top = 96
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EquipAplic'
            UpdCampo = 'EquipAplic'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPrgLstCab
            IgnoraDBLookupComboBox = False
          end
          object CBPrgLstCab: TdmkDBLookupComboBox
            Left = 72
            Top = 96
            Width = 521
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPrgLstCab
            TabOrder = 6
            dmkEditCB = EdPrgLstCab
            QryCampo = 'EquipAplic'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdDependenci: TdmkEditCB
            Left = 16
            Top = 136
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Dependenci'
            UpdCampo = 'Dependenci'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBDependenci
            IgnoraDBLookupComboBox = False
          end
          object CBDependenci: TdmkDBLookupComboBox
            Left = 72
            Top = 136
            Width = 521
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_DEPENDENCI'
            ListSource = DsSiapImaDep
            TabOrder = 8
            dmkEditCB = EdDependenci
            QryCampo = 'Dependenci'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object PnDesativa: TPanel
          Left = 2
          Top = 177
          Width = 629
          Height = 93
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          object Label3: TLabel
            Left = 504
            Top = 0
            Width = 102
            Height = 13
            Caption = 'Data de desativa'#231#227'o:'
          end
          object Label4: TLabel
            Left = 16
            Top = 0
            Width = 111
            Height = 13
            Caption = 'Motivo da desativa'#231#227'o:'
          end
          object Label11: TLabel
            Left = 16
            Top = 44
            Width = 105
            Height = 13
            Caption = 'Motivo da inutiliza'#231#227'o:'
          end
          object Label10: TLabel
            Left = 504
            Top = 44
            Width = 96
            Height = 13
            Caption = 'Data de inutiliza'#231#227'o:'
          end
          object SpeedButton2: TSpeedButton
            Left = 475
            Top = 16
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object SpeedButton3: TSpeedButton
            Left = 475
            Top = 60
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton3Click
          end
          object CBMotDesativ: TdmkDBLookupComboBox
            Left = 72
            Top = 16
            Width = 400
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsMotDesativ
            TabOrder = 1
            dmkEditCB = EdMotDesativ
            QryCampo = 'EquipAplic'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object TPDtaDesativ: TdmkEditDateTimePicker
            Left = 504
            Top = 16
            Width = 112
            Height = 21
            Date = -3652.857134502316000000
            Time = -3652.857134502316000000
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdMotDesativ: TdmkEditCB
            Left = 16
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EquipAplic'
            UpdCampo = 'EquipAplic'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBMotDesativ
            IgnoraDBLookupComboBox = False
          end
          object EdMotInutili: TdmkEditCB
            Left = 16
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EquipAplic'
            UpdCampo = 'EquipAplic'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBMotInutili
            IgnoraDBLookupComboBox = False
          end
          object CBMotInutili: TdmkDBLookupComboBox
            Left = 72
            Top = 60
            Width = 400
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsMotInutili
            TabOrder = 4
            dmkEditCB = EdMotInutili
            QryCampo = 'EquipAplic'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object TPDtaInutili: TdmkEditDateTimePicker
            Left = 504
            Top = 60
            Width = 112
            Height = 21
            Date = -3652.857134502316000000
            Time = -3652.857134502316000000
            TabOrder = 5
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 320
    Width = 633
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 629
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 364
    Width = 633
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 487
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 485
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEquipAplic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM equiaplic'
      'ORDER BY Nome')
    Left = 520
    Top = 12
    object QrEquipAplicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEquipAplicNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrEquipAplicNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object DsEquipAplic: TDataSource
    DataSet = QrEquipAplic
    Left = 548
    Top = 12
  end
  object QrMotDesativ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM motdesativ'
      'ORDER BY Nome'
      '')
    Left = 240
    Top = 20
    object QrMotDesativCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMotDesativNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsMotDesativ: TDataSource
    DataSet = QrMotDesativ
    Left = 268
    Top = 20
  end
  object QrPrgLstCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM prglstcab '
      'ORDER BY Nome')
    Left = 432
    Top = 16
    object QrPrgLstCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrgLstCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPrgLstCab: TDataSource
    DataSet = QrPrgLstCab
    Left = 460
    Top = 16
  end
  object DsSiapImaDep: TDataSource
    DataSet = QrSiapImaDep
    Left = 372
    Top = 20
  end
  object QrSiapImaDep: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sid.Controle, dep.Nome NO_DEPENDENCI'
      'FROM siapimadep sid'
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci'
      'WHERE sid.Codigo=:P0'
      'ORDER BY NO_DEPENDENCI')
    Left = 344
    Top = 20
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapImaDepNO_DEPENDENCI: TWideStringField
      FieldName = 'NO_DEPENDENCI'
      Size = 60
    end
    object QrSiapImaDepControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrMotInutili: TmySQLQuery
    Database = Dmod.MyDB
    DataSource = Dmod.DsOpcoesBugs
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM motdesativ'
      'ORDER BY Nome'
      '')
    Left = 240
    Top = 68
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsMotInutili: TDataSource
    DataSet = QrMotInutili
    Left = 268
    Top = 68
  end
end
