object FmOSCabEncerr: TFmOSCabEncerr
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-047 :: Ordem de Servi'#231'o - Encerramento'
  ClientHeight = 362
  ClientWidth = 544
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 544
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 496
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 448
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 413
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Encerramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 413
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Encerramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 413
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Encerramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 544
    Height = 200
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 544
      Height = 200
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 544
        Height = 200
        Align = alClient
        TabOrder = 0
        object Label14: TLabel
          Left = 95
          Top = 8
          Width = 33
          Height = 13
          Caption = 'Status:'
          Color = clBtnFace
          ParentColor = False
        end
        object GroupBox3: TGroupBox
          Left = 95
          Top = 52
          Width = 370
          Height = 60
          Caption = ' Data Vistoria: '
          TabOrder = 2
          object Label18: TLabel
            Left = 12
            Top = 36
            Width = 50
            Height = 13
            Caption = 'Realizado:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label52: TLabel
            Left = 64
            Top = 16
            Width = 51
            Height = 13
            Caption = 'In'#237'cio: [F4]'
            Color = clBtnFace
            ParentColor = False
          end
          object Label53: TLabel
            Left = 216
            Top = 16
            Width = 62
            Height = 13
            Caption = 'T'#233'rmino: [F4]'
            Color = clBtnFace
            ParentColor = False
          end
          object TPDtaVisExe: TdmkEditDateTimePicker
            Left = 64
            Top = 32
            Width = 108
            Height = 21
            Date = 0.639644131944805900
            Time = 0.639644131944805900
            TabOrder = 0
            OnExit = TPDtaVisExeExit
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtaVisExe'
            UpdCampo = 'DtaVisExe'
            UpdType = utYes
          end
          object EdDtaVisExe: TdmkEdit
            Left = 172
            Top = 32
            Width = 40
            Height = 21
            TabOrder = 1
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '00:00'
            QryCampo = 'DtaVisExe'
            UpdCampo = 'DtaVisExe'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdDtaVisExeExit
          end
          object TPFimVisExe: TdmkEditDateTimePicker
            Left = 216
            Top = 32
            Width = 108
            Height = 21
            Date = 0.639644131944805900
            Time = 0.639644131944805900
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'FimVisExe'
            UpdCampo = 'FimVisExe'
            UpdType = utYes
          end
          object EdFimVisExe: TdmkEdit
            Left = 324
            Top = 32
            Width = 40
            Height = 21
            TabOrder = 3
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '00:00'
            QryCampo = 'FimVisExe'
            UpdCampo = 'FimVisExe'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox4: TGroupBox
          Left = 95
          Top = 118
          Width = 370
          Height = 60
          Caption = ' Data Execu'#231#227'o: '
          TabOrder = 3
          object Label20: TLabel
            Left = 64
            Top = 16
            Width = 51
            Height = 13
            Caption = 'In'#237'cio: [F4]'
            Color = clBtnFace
            ParentColor = False
          end
          object Label21: TLabel
            Left = 216
            Top = 16
            Width = 62
            Height = 13
            Caption = 'T'#233'rmino: [F4]'
            Color = clBtnFace
            ParentColor = False
          end
          object Label51: TLabel
            Left = 12
            Top = 36
            Width = 50
            Height = 13
            Caption = 'Realizado:'
            Color = clBtnFace
            ParentColor = False
          end
          object TPDtaExeIni: TdmkEditDateTimePicker
            Left = 64
            Top = 32
            Width = 108
            Height = 21
            Date = 0.639644131944805900
            Time = 0.639644131944805900
            TabOrder = 0
            OnExit = TPDtaExeIniExit
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtaExeIni'
            UpdCampo = 'DtaExeIni'
            UpdType = utYes
          end
          object TPDtaExeFim: TdmkEditDateTimePicker
            Left = 216
            Top = 32
            Width = 108
            Height = 21
            Date = 0.639644131944805900
            Time = 0.639644131944805900
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtaExeFim'
            UpdCampo = 'DtaExeFim'
            UpdType = utYes
          end
          object EdDtaExeIni: TdmkEdit
            Left = 172
            Top = 32
            Width = 40
            Height = 21
            TabOrder = 1
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '00:00'
            QryCampo = 'DtaExeIni'
            UpdCampo = 'DtaExeIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdDtaExeIniExit
          end
          object EdDtaExeFim: TdmkEdit
            Left = 324
            Top = 32
            Width = 40
            Height = 21
            TabOrder = 3
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '00:00'
            QryCampo = 'DtaExeFim'
            UpdCampo = 'DtaExeFim'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object CBEstatus: TdmkDBLookupComboBox
          Left = 139
          Top = 24
          Width = 326
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsEstatusOSs
          TabOrder = 1
          dmkEditCB = EdEstatus
          QryCampo = 'Estatus'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdEstatus: TdmkEditCB
          Left = 95
          Top = 24
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Estatus'
          UpdCampo = 'Estatus'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEstatus
          IgnoraDBLookupComboBox = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 248
    Width = 544
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 540
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 292
    Width = 544
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 398
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 396
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEstatusOSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM estatusoss'
      'WHERE Execucao IN (2,3)'
      'ORDER BY Codigo')
    Left = 22
    Top = 82
    object QrEstatusOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstatusOSsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEstatusOSs: TDataSource
    DataSet = QrEstatusOSs
    Left = 50
    Top = 82
  end
end
