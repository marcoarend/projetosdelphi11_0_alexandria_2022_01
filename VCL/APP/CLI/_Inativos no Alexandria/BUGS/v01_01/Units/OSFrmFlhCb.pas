unit OSFrmFlhCb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  UndmkProcFunc, dmkValUsu, dmkImage, dmkRadioGroup, dmkCheckBox, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmOSFrmFlhCb = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdIDIts: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    DBEdControle: TdmkDBEdit;
    Label2: TLabel;
    DBEdConta: TdmkDBEdit;
    Label4: TLabel;
    QrFormulas: TmySQLQuery;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    DsFormulas: TDataSource;
    Label1: TLabel;
    EdFormula: TdmkEditCB;
    CBFormula: TdmkDBLookupComboBox;
    Label7: TLabel;
    EdPerioDd: TdmkEdit;
    Label8: TLabel;
    EdDdPostero: TdmkEdit;
    RGEmisStatus: TdmkRadioGroup;
    Label9: TLabel;
    CkAtivo: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenOSFrmFlhCb(IDIts: Integer);
    function  ProximaOrdem(Codigo, Controle, Conta: Integer): Integer;
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOSFrmFlhCb: TFmOSFrmFlhCb;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
Formulas, UnAppListas;

{$R *.DFM}

procedure TFmOSFrmFlhCb.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, IDIts, Formula, PerioDd, DdPostero, EmisStatus: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := Geral.IMV(DBEdConta.Text);
  IDIts          := EdIDIts.ValueVariant;
  Formula        := EdFormula.ValueVariant;
  PerioDd        := EdPerioDd.ValueVariant;
  EmisStatus     := RGEmisStatus.ItemIndex;
  DdPostero      := EdDdPostero.ValueVariant;
  //
  IDIts := UMyMod.BPGS1I32('osfrmflhcb', 'IDIts', '', '', tsPos, ImgTipo.SQLType, IDIts);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osfrmflhcb', False, [
  'Codigo', 'Controle', 'Conta',
  'Formula', 'PerioDd', 'EmisStatus',
  'DdPostero'], [
  'IDIts'], [
  Codigo, Controle, Conta,
  Formula, PerioDd, EmisStatus,
  DdPostero], [
  IDIts], True) then
  begin
    ReopenOSFrmFlhCb(IDIts);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdIDIts.ValueVariant     := 0;
      EdFormula.ValueVariant   := 0;
      CBFormula.KeyValue       := 0;
      EdPerioDd.ValueVariant   := 0;
      RGEmisStatus.ItemIndex   := 0;
      //
      EdFormula.SetFocus;
    end else Close;
  end;
end;

procedure TFmOSFrmFlhCb.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSFrmFlhCb.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCab;
  DBEdControle.DataSource := FDsCab;
  DBEdConta.DataSource    := FDsCab;
  DBEdNome.DataSource     := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmOSFrmFlhCb.FormCreate(Sender: TObject);
const
  Colunas = 3;
begin
  ImgTipo.SQLType := stLok;
  MyObjects.ConfiguraRadioGroup(RGEmisStatus, sListaEmisStatus, Colunas, 0);
  UnDmkDAC_PF.AbreQuery(QrFormulas, Dmod.MyDB);
end;

procedure TFmOSFrmFlhCb.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmOSFrmFlhCb.ProximaOrdem(Codigo, Controle, Conta: Integer): Integer;
const
  Incremento = 1;
  Base       = 0;
begin
  Result := UnDmkDAC_PF.ObtemProximaOrdem(Dmod.MyDB, 'osfrmflhcb', 'Ordem',
              vpLast, dmktfInteger, Incremento, Base, ['Codigo', 'Controle',
              'Conta'], [Codigo, Controle, Conta]);
end;

procedure TFmOSFrmFlhCb.ReopenOSFrmFlhCb(IDIts: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Conta').AsInteger;
    FQrIts.Open;
    //
    if IDIts <> 0 then
      FQrIts.Locate('IDIts', IDIts, []);
  end;
end;

end.
