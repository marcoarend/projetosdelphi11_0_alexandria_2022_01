object FmPipCad: TFmPipCad
  Left = 368
  Top = 194
  Caption = 'PIP-GEREN-001 :: PMVs'
  ClientHeight = 626
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 530
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 792
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo_: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome_: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 467
      Width = 792
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma_: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 652
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste_: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 530
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 792
      Height = 197
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 180
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 16
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 76
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Refer'#234'ncia:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label3: TLabel
          Left = 504
          Top = 16
          Width = 89
          Height = 13
          Caption = 'Data de aquisi'#231#227'o:'
        end
        object Label4: TLabel
          Left = 16
          Top = 56
          Width = 65
          Height = 13
          Caption = 'Equipamento:'
        end
        object Label5: TLabel
          Left = 16
          Top = 96
          Width = 177
          Height = 13
          Caption = 'Lista de perguntas no monitoramento:'
        end
        object Label13: TLabel
          Left = 16
          Top = 136
          Width = 153
          Height = 13
          Caption = 'Depend'#234'ncia onde se encontra:'
          FocusControl = DBEdit13
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 16
          Top = 32
          Width = 56
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsPipCad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 76
          Top = 32
          Width = 424
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsPipCad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit1: TDBEdit
          Left = 504
          Top = 32
          Width = 112
          Height = 21
          DataField = 'DtaAquis_TXT'
          DataSource = DsPipCad
          TabOrder = 2
        end
        object DBEdit2: TDBEdit
          Left = 16
          Top = 72
          Width = 56
          Height = 21
          DataField = 'Equipamento'
          DataSource = DsPipCad
          TabOrder = 3
        end
        object DBEdit3: TDBEdit
          Left = 76
          Top = 72
          Width = 540
          Height = 21
          DataField = 'NO_EQUI'
          DataSource = DsPipCad
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 16
          Top = 112
          Width = 56
          Height = 21
          DataField = 'PrgLstCab'
          DataSource = DsPipCad
          TabOrder = 5
        end
        object DBEdit7: TDBEdit
          Left = 76
          Top = 112
          Width = 541
          Height = 21
          DataField = 'NO_LISTA'
          DataSource = DsPipCad
          TabOrder = 6
        end
        object DBEdit13: TDBEdit
          Left = 16
          Top = 152
          Width = 56
          Height = 21
          DataField = 'Dependenci'
          DataSource = DsPipCad
          TabOrder = 7
        end
        object DBEdit14: TDBEdit
          Left = 76
          Top = 152
          Width = 540
          Height = 21
          DataField = 'NO_DEPENDENCIA'
          DataSource = DsPipCad
          TabOrder = 8
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 466
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 95
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = False
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object GBDesat: TGroupBox
      Left = 0
      Top = 301
      Width = 792
      Height = 128
      Align = alTop
      Caption = ' Desativa'#231#227'o:'
      TabOrder = 2
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 111
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label6: TLabel
          Left = 132
          Top = 4
          Width = 111
          Height = 13
          Caption = 'Motivo de desativa'#231#227'o:'
          FocusControl = DBEdit4
        end
        object Label8: TLabel
          Left = 16
          Top = 4
          Width = 74
          Height = 13
          Caption = 'Desativado em:'
          FocusControl = DBEdit8
        end
        object Label14: TLabel
          Left = 16
          Top = 44
          Width = 67
          Height = 13
          Caption = 'Inutilizado em:'
          FocusControl = DBEdit15
        end
        object Label15: TLabel
          Left = 132
          Top = 44
          Width = 105
          Height = 13
          Caption = 'Motivo de inutiliza'#231#227'o:'
          FocusControl = DBEdit16
        end
        object DBEdit4: TDBEdit
          Left = 132
          Top = 20
          Width = 56
          Height = 21
          DataField = 'MotDesativ'
          DataSource = DsPipCad
          TabOrder = 0
        end
        object DBEdit5: TDBEdit
          Left = 192
          Top = 20
          Width = 425
          Height = 21
          DataField = 'NO_MOTDESAT'
          DataSource = DsPipCad
          TabOrder = 1
        end
        object DBEdit8: TDBEdit
          Left = 16
          Top = 20
          Width = 112
          Height = 21
          DataField = 'DtaDesativ_TXT'
          DataSource = DsPipCad
          TabOrder = 2
        end
        object DBEdit15: TDBEdit
          Left = 16
          Top = 60
          Width = 112
          Height = 21
          DataField = 'DtaInutili_TXT'
          DataSource = DsPipCad
          TabOrder = 3
        end
        object DBEdit16: TDBEdit
          Left = 132
          Top = 60
          Width = 56
          Height = 21
          DataField = 'MotInutili'
          DataSource = DsPipCad
          TabOrder = 4
        end
        object DBEdit17: TDBEdit
          Left = 192
          Top = 60
          Width = 425
          Height = 21
          DataField = 'NO_MOTINUTI'
          DataSource = DsPipCad
          TabOrder = 5
        end
      end
    end
    object GBOrig: TGroupBox
      Left = 0
      Top = 197
      Width = 792
      Height = 104
      Align = alTop
      Caption = ' Dados de instala'#231#227'o: '
      TabOrder = 3
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 87
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label10: TLabel
          Left = 16
          Top = 4
          Width = 106
          Height = 13
          Caption = 'Localizador de origem:'
          FocusControl = DBEdit9
        end
        object Label11: TLabel
          Left = 132
          Top = 4
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          FocusControl = DBEdit10
        end
        object Label12: TLabel
          Left = 16
          Top = 44
          Width = 127
          Height = 13
          Caption = 'Lugar em que se encontra:'
          FocusControl = DBEdit11
        end
        object DBEdit9: TDBEdit
          Left = 16
          Top = 20
          Width = 112
          Height = 21
          DataField = 'Codigo'
          DataSource = DsOrig
          TabOrder = 0
        end
        object DBEdit10: TDBEdit
          Left = 132
          Top = 20
          Width = 484
          Height = 21
          DataField = 'NO_CLIENTE'
          DataSource = DsOrig
          TabOrder = 1
        end
        object DBEdit11: TDBEdit
          Left = 16
          Top = 60
          Width = 112
          Height = 21
          DataField = 'SiapTerCad'
          DataSource = DsOrig
          TabOrder = 2
        end
        object DBEdit12: TDBEdit
          Left = 132
          Top = 60
          Width = 484
          Height = 21
          DataField = 'Nome'
          DataSource = DsOrig
          TabOrder = 3
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 71
        Height = 32
        Caption = 'PMVs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 71
        Height = 32
        Caption = 'PMVs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 71
        Height = 32
        Caption = 'PMVs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrPipCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPipCadBeforeOpen
    AfterOpen = QrPipCadAfterOpen
    BeforeClose = QrPipCadBeforeClose
    AfterScroll = QrPipCadAfterScroll
    SQL.Strings = (
      'SELECT mot.Nome NO_MOTDESAT, gg1.Nome NO_EQUI,'
      'plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA,'
      'IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT,'
      'IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT,'
      'cad.*'
      'FROM pipcad cad'
      'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ'
      'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab'
      '/**/'
      'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci'
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci')
    Left = 64
    Top = 64
    object QrPipCadNO_MOTDESAT: TWideStringField
      FieldName = 'NO_MOTDESAT'
      Size = 60
    end
    object QrPipCadNO_EQUI: TWideStringField
      FieldName = 'NO_EQUI'
      Size = 120
    end
    object QrPipCadNO_LISTA: TWideStringField
      FieldName = 'NO_LISTA'
      Size = 100
    end
    object QrPipCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPipCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPipCadEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrPipCadOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrPipCadMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrPipCadDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrPipCadDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrPipCadPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrPipCadDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrPipCadDtaAquis_TXT: TWideStringField
      FieldName = 'DtaAquis_TXT'
      Size = 10
    end
    object QrPipCadDtaDesativ_TXT: TWideStringField
      FieldName = 'DtaDesativ_TXT'
      Size = 10
    end
    object QrPipCadNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrPipCadDtaInutili_TXT: TWideStringField
      FieldName = 'DtaInutili_TXT'
      Size = 10
    end
    object QrPipCadMotInutili: TIntegerField
      FieldName = 'MotInutili'
    end
    object QrPipCadNO_MOTINUTI: TWideStringField
      FieldName = 'NO_MOTINUTI'
      Size = 60
    end
  end
  object DsPipCad: TDataSource
    DataSet = QrPipCad
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrOrig: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, cab.Entidade, cab.SiapTerCad, stc.Nome,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE'
      'FROM osmoncab omc'
      'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapterCad'
      'WHERE omc.Conta=44')
    Left = 64
    Top = 92
    object QrOrigCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOrigSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrOrigNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOrigNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrOrigEntidade: TSmallintField
      FieldName = 'Entidade'
    end
  end
  object DsOrig: TDataSource
    DataSet = QrOrig
    Left = 92
    Top = 92
  end
end
