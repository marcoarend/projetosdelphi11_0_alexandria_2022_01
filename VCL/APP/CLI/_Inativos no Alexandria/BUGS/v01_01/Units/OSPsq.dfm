object FmOSPsq: TFmOSPsq
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-012 :: Ordem de Servi'#231'o - Pesquisa'
  ClientHeight = 790
  ClientWidth = 1556
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1556
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1497
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 469
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 420
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Pesquisa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 420
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Pesquisa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 420
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Pesquisa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 528
      Top = 0
      Width = 969
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 18
        Width = 964
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 16
          Top = 2
          Width = 385
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Duplo clique na grade localiza a OS da linha clicada.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 15
          Top = 1
          Width = 385
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Duplo clique na grade localiza a OS da linha clicada.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1556
    Height = 645
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1556
      Height = 645
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1556
        Height = 645
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object GroupBox5: TGroupBox
          Left = 2
          Top = 18
          Width = 1552
          Height = 414
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' Relacionamentos neste atendimento: '
          TabOrder = 0
          ExplicitWidth = 1551
          object Panel12: TPanel
            Left = 2
            Top = 18
            Width = 1223
            Height = 394
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitHeight = 393
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 1223
              Height = 150
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitWidth = 1222
              object LaCliInt: TLabel
                Left = 5
                Top = 7
                Width = 87
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cliente interno:'
              end
              object LaEntidade1: TLabel
                Left = 5
                Top = 37
                Width = 93
                Height = 18
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '[Sub]Cliente:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object LaEntidade2: TLabel
                Left = 5
                Top = 66
                Width = 88
                Height = 18
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Contratante:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label4: TLabel
                Left = 5
                Top = 126
                Width = 60
                Height = 18
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Contato:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label5: TLabel
                Left = 1083
                Top = 7
                Width = 19
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'at'#233
              end
              object LaSC_Parci: TLabel
                Left = 783
                Top = 37
                Width = 96
                Height = 18
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Nome parcial:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object LaCO_Parci: TLabel
                Left = 783
                Top = 66
                Width = 96
                Height = 18
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Nome parcial:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object LaIN_Parci: TLabel
                Left = 783
                Top = 126
                Width = 96
                Height = 18
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Nome parcial:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label9: TLabel
                Left = 5
                Top = 96
                Width = 62
                Height = 18
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Pagante:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clPurple
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object LaPA_Parci: TLabel
                Left = 783
                Top = 96
                Width = 96
                Height = 18
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Nome parcial:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clPurple
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object EdEmpresa: TdmkEditCB
                Left = 94
                Top = 2
                Width = 59
                Height = 28
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEmpresa
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEmpresa: TdmkDBLookupComboBox
                Left = 153
                Top = 2
                Width = 625
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Filial'
                ListField = 'NOMEFILIAL'
                ListSource = DModG.DsEmpresas
                TabOrder = 1
                dmkEditCB = EdEmpresa
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdEntidade: TdmkEditCB
                Left = 94
                Top = 32
                Width = 59
                Height = 27
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdEntidadeChange
                DBLookupComboBox = CBEntidade
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEntidade: TdmkDBLookupComboBox
                Left = 153
                Top = 32
                Width = 625
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                KeyField = 'Codigo'
                ListField = 'NOMEENTIDADE'
                ListSource = DsClientes
                ParentFont = False
                TabOrder = 3
                dmkEditCB = EdEntidade
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdEntContrat: TdmkEditCB
                Left = 94
                Top = 62
                Width = 59
                Height = 27
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdEntContratChange
                DBLookupComboBox = CBEntContrat
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEntContrat: TdmkDBLookupComboBox
                Left = 153
                Top = 62
                Width = 625
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                KeyField = 'Codigo'
                ListField = 'NOMEENTIDADE'
                ListSource = DsEntContrat
                ParentFont = False
                TabOrder = 5
                dmkEditCB = EdEntContrat
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdEntiContat: TdmkEditCB
                Left = 94
                Top = 121
                Width = 59
                Height = 27
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdEntiContatChange
                DBLookupComboBox = CBEntiContat
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEntiContat: TdmkDBLookupComboBox
                Left = 153
                Top = 121
                Width = 625
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                KeyField = 'Controle'
                ListField = 'Nome'
                ListSource = DsEntiContat
                ParentFont = False
                TabOrder = 9
                dmkEditCB = EdEntiContat
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object CkOrcamTotal: TCheckBox
                Left = 881
                Top = 5
                Width = 90
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Valores de:'
                TabOrder = 10
                OnClick = CkEstatusClick
              end
              object EdOrcamTotal_Min: TdmkEdit
                Left = 975
                Top = 2
                Width = 98
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 11
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdOrcamTotal_Max: TdmkEdit
                Left = 1118
                Top = 2
                Width = 98
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 12
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdSC_Parci: TdmkEdit
                Left = 881
                Top = 32
                Width = 335
                Height = 27
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 13
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCO_Parci: TdmkEdit
                Left = 881
                Top = 62
                Width = 335
                Height = 27
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 14
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIN_Parci: TdmkEdit
                Left = 881
                Top = 121
                Width = 335
                Height = 27
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 16
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdEntPagante: TdmkEditCB
                Left = 94
                Top = 91
                Width = 59
                Height = 27
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clPurple
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdEntiContatChange
                DBLookupComboBox = CBEntPagante
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEntPagante: TdmkDBLookupComboBox
                Left = 153
                Top = 91
                Width = 625
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clPurple
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                KeyField = 'Codigo'
                ListField = 'NOMEENTIDADE'
                ListSource = DsEntPagante
                ParentFont = False
                TabOrder = 7
                dmkEditCB = EdEntPagante
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdPA_Parci: TdmkEdit
                Left = 881
                Top = 91
                Width = 335
                Height = 27
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clPurple
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 15
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object Panel6: TPanel
              Left = 0
              Top = 176
              Width = 1223
              Height = 218
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              ExplicitWidth = 1222
              ExplicitHeight = 217
              object CGOperacao: TdmkCheckGroup
                Left = 5
                Top = 2
                Width = 597
                Height = 56
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Opera'#231#227'o: '
                Columns = 3
                Items.Strings = (
                  'Dedetiza'#231#227'o'
                  'Caixa d'#39#225'gua'
                  'Monitoramento')
                TabOrder = 0
                OnClick = CGOperacaoClick
                QryCampo = 'Operacao'
                UpdCampo = 'Operacao'
                UpdType = utYes
                Value = 0
                OldValor = 0
              end
              object CGStPipAdPrg: TdmkCheckGroup
                Left = 606
                Top = 2
                Width = 296
                Height = 56
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Perguntas de monitoramento:'
                Columns = 3
                Enabled = False
                Items.Strings = (
                  'A criar'
                  'Iniciado'
                  'Finalizado')
                TabOrder = 1
                OnClick = CGOperacaoClick
                QryCampo = 'Operacao'
                UpdCampo = 'Operacao'
                UpdType = utYes
                Value = 0
                OldValor = 0
              end
              object GroupBox7: TGroupBox
                Left = 912
                Top = 0
                Width = 311
                Height = 218
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alRight
                Caption = ' Agentes de controle:'
                TabOrder = 2
                ExplicitLeft = 911
                ExplicitHeight = 217
                object DBGOSAge: TDBGrid
                  Left = 2
                  Top = 100
                  Width = 307
                  Height = 114
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsPsqOSAge
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -14
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnMouseUp = DBGOSAgeMouseUp
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Responsa'
                      Title.Caption = 'R'
                      Width = 17
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_AGENTE'
                      Title.Caption = 'Agente'
                      Width = 197
                      Visible = True
                    end>
                end
                object RGOSAge: TRadioGroup
                  Left = 2
                  Top = 18
                  Width = 307
                  Height = 82
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Caption = ' Forma de pesquisa por agentes:'
                  Columns = 2
                  ItemIndex = 0
                  Items.Strings = (
                    'Ignorar'
                    'Est'#225' contido'
                    'N'#227'o est'#225' contido'
                    #201' o grupo'
                    'N'#227'o '#233' o grupo'
                    'Sem agentes')
                  TabOrder = 1
                  OnClick = RGOSAgeClick
                end
              end
              object GroupBox2: TGroupBox
                Left = 5
                Top = 59
                Width = 295
                Height = 76
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Per'#237'odo do contato: '
                TabOrder = 3
                object Label1: TLabel
                  Left = 10
                  Top = 25
                  Width = 72
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data inicial: '
                end
                object Label2: TLabel
                  Left = 153
                  Top = 25
                  Width = 59
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data final:'
                end
                object TPDtaContatIni: TdmkEditDateTimePicker
                  Left = 10
                  Top = 44
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 0.730390057869954000
                  Time = 0.730390057869954000
                  TabOrder = 0
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPDtaContatFim: TdmkEditDateTimePicker
                  Left = 153
                  Top = 44
                  Width = 137
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 41108.730390057870000000
                  Time = 41108.730390057870000000
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
              object GroupBox8: TGroupBox
                Left = 305
                Top = 59
                Width = 296
                Height = 76
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '                                           '
                TabOrder = 4
                object Label6: TLabel
                  Left = 10
                  Top = 25
                  Width = 72
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data inicial: '
                end
                object Label7: TLabel
                  Left = 153
                  Top = 25
                  Width = 59
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data final:'
                end
                object TPDtaExecutIni: TdmkEditDateTimePicker
                  Left = 10
                  Top = 44
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 0.730390057869954000
                  Time = 0.730390057869954000
                  TabOrder = 1
                  OnClick = TPDtaExecutIniClick
                  OnChange = TPDtaExecutIniChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPDtaExecutFim: TdmkEditDateTimePicker
                  Left = 153
                  Top = 44
                  Width = 137
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 41108.730390057870000000
                  Time = 41108.730390057870000000
                  TabOrder = 2
                  OnClick = TPDtaExecutFimClick
                  OnChange = TPDtaExecutFimChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object CkDtaExecut: TCheckBox
                  Left = 15
                  Top = 0
                  Width = 227
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Per'#237'odo da execu'#231#227'o realizada: '
                  TabOrder = 0
                  OnClick = CkDtaExecutClick
                end
              end
              object GBOSFlhGrCab: TGroupBox
                Left = 606
                Top = 59
                Width = 178
                Height = 76
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '                                           '
                TabOrder = 5
                object Label8: TLabel
                  Left = 10
                  Top = 25
                  Width = 153
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'C'#243'digo ([F4] para '#250'ltimo): '
                end
                object CkOSFlhGrCab: TCheckBox
                  Left = 15
                  Top = 0
                  Width = 154
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Grupo de OSs Filhas: '
                  TabOrder = 0
                  OnClick = CkOSFlhGrCabClick
                end
                object EdOSFlhGrCab: TdmkEdit
                  Left = 10
                  Top = 44
                  Width = 98
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdOSFlhGrCabChange
                  OnKeyDown = EdOSFlhGrCabKeyDown
                end
              end
              object GBProtocolo: TGroupBox
                Left = 788
                Top = 59
                Width = 114
                Height = 76
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '                        '
                TabOrder = 6
                object Label10: TLabel
                  Left = 11
                  Top = 25
                  Width = 96
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'N'#176' do protocolo:'
                end
                object CkProtocolo: TCheckBox
                  Left = 15
                  Top = 0
                  Width = 82
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Protocolo:  '
                  TabOrder = 0
                  OnClick = CkProtocoloClick
                end
                object EdProtocolo: TdmkEdit
                  Left = 10
                  Top = 44
                  Width = 98
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdProtocoloChange
                  OnKeyDown = EdProtocoloKeyDown
                end
              end
              object GroupBox9: TGroupBox
                Left = 5
                Top = 138
                Width = 295
                Height = 76
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '                                           '
                TabOrder = 7
                object Label11: TLabel
                  Left = 10
                  Top = 25
                  Width = 72
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data inicial: '
                end
                object Label12: TLabel
                  Left = 153
                  Top = 25
                  Width = 59
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data final:'
                end
                object TPDtaVisPrvIni: TdmkEditDateTimePicker
                  Left = 10
                  Top = 44
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 0.730390057869954000
                  Time = 0.730390057869954000
                  TabOrder = 1
                  OnClick = TPDtaExecutIniClick
                  OnChange = TPDtaExecutIniChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPDtaVisPrvFim: TdmkEditDateTimePicker
                  Left = 153
                  Top = 44
                  Width = 137
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 41108.730390057870000000
                  Time = 41108.730390057870000000
                  TabOrder = 2
                  OnClick = TPDtaExecutFimClick
                  OnChange = TPDtaExecutFimChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object CkDtaVisPrv: TCheckBox
                  Left = 15
                  Top = 0
                  Width = 188
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Per'#237'odo da vistoria prevista: '
                  TabOrder = 0
                  OnClick = CkDtaExecutClick
                end
              end
              object GroupBox10: TGroupBox
                Left = 305
                Top = 138
                Width = 296
                Height = 76
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '                                           '
                TabOrder = 8
                object Label13: TLabel
                  Left = 10
                  Top = 25
                  Width = 72
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data inicial: '
                end
                object Label14: TLabel
                  Left = 153
                  Top = 25
                  Width = 59
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data final:'
                end
                object TPDtaExePrvIni: TdmkEditDateTimePicker
                  Left = 10
                  Top = 44
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 0.730390057869954000
                  Time = 0.730390057869954000
                  TabOrder = 1
                  OnClick = TPDtaExecutIniClick
                  OnChange = TPDtaExecutIniChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TpDtaExePrvFim: TdmkEditDateTimePicker
                  Left = 153
                  Top = 44
                  Width = 137
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 41108.730390057870000000
                  Time = 41108.730390057870000000
                  TabOrder = 2
                  OnClick = TPDtaExecutFimClick
                  OnChange = TPDtaExecutFimChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object CkDtaExePrv: TCheckBox
                  Left = 15
                  Top = 0
                  Width = 208
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Per'#237'odo da execu'#231#227'o prevista: '
                  TabOrder = 0
                  OnClick = CkDtaExecutClick
                end
              end
            end
            object Panel7: TPanel
              Left = 0
              Top = 150
              Width = 1223
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 2
              ExplicitWidth = 1222
              object Label3: TLabel
                Left = 350
                Top = 5
                Width = 49
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Servi'#231'o:'
                Color = clBtnFace
                ParentColor = False
              end
              object Label15: TLabel
                Left = 709
                Top = 5
                Width = 132
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Fato gerador (Motivo):'
                Color = clBtnFace
                ParentColor = False
              end
              object CkEstatus: TCheckBox
                Left = 5
                Top = 2
                Width = 65
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status:'
                TabOrder = 0
                OnClick = CkEstatusClick
              end
              object EdEstatus: TdmkEditCB
                Left = 94
                Top = 0
                Width = 54
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Estatus'
                UpdCampo = 'Estatus'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEstatus
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEstatus: TdmkDBLookupComboBox
                Left = 148
                Top = 0
                Width = 197
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Enabled = False
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEstatusOSs
                TabOrder = 2
                dmkEditCB = EdEstatus
                QryCampo = 'Estatus'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdDesServico: TdmkEditCB
                Left = 404
                Top = 0
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'DesServico'
                UpdCampo = 'DesServico'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBDesServico
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBDesServico: TdmkDBLookupComboBox
                Left = 473
                Top = 0
                Width = 232
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsDesServico
                TabOrder = 4
                dmkEditCB = EdDesServico
                QryCampo = 'DesServico'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdFatoGeradr: TdmkEditCB
                Left = 847
                Top = 0
                Width = 54
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'FatoGeradr'
                UpdCampo = 'FatoGeradr'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBFatoGeradr
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBFatoGeradr: TdmkDBLookupComboBox
                Left = 901
                Top = 0
                Width = 316
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsFatoGeradr
                TabOrder = 6
                dmkEditCB = EdFatoGeradr
                QryCampo = 'FatoGeradr'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 1225
            Top = 18
            Width = 325
            Height = 394
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            TabOrder = 1
            ExplicitWidth = 323
            ExplicitHeight = 393
            object GroupBox4: TGroupBox
              Left = 2
              Top = 18
              Width = 319
              Height = 136
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = ' Pragas pr'#233'-atendimento:'
              TabOrder = 0
              object DBGOSCabAlv: TDBGrid
                Left = 2
                Top = 18
                Width = 314
                Height = 115
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsPsqOSCabAlv
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnMouseUp = DBGOSCabAlvMouseUp
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_NIVEL'
                    Title.Caption = 'N'
                    Width = 17
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRAGA'
                    Title.Caption = 'Praga'
                    Width = 200
                    Visible = True
                  end>
              end
            end
            object GroupBox6: TGroupBox
              Left = 2
              Top = 154
              Width = 319
              Height = 236
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = ' Pragas OS:'
              TabOrder = 1
              object DBGOSAlv: TDBGrid
                Left = 2
                Top = 18
                Width = 314
                Height = 216
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsPsqOSAlv
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnMouseUp = DBGOSAlvMouseUp
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_Praga_Z'
                    Title.Caption = 'Praga'
                    Width = 216
                    Visible = True
                  end>
              end
            end
          end
        end
        object DBGPsq: TdmkDBGridZTO
          Left = 2
          Top = 432
          Width = 1552
          Height = 211
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsPesq
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGPsqDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'OS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RotaLatLon'
              Title.Caption = 'Rota'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Estatus_TXT'
              Title.Caption = 'Status'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Entidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENTIDADE'
              Title.Caption = 'Sub-cliente (EP)'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AF_ENTIDADE'
              Title.Caption = 'Sub-cliente (FA)'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EntContrat'
              Title.Caption = 'Contratante'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENTCONTRAT'
              Title.Caption = 'Contratante (EP)'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AF_ENTCONTRAT'
              Title.Caption = 'Contratante (FA)'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EntPagante'
              Title.Caption = 'Pagante'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENTPAGANTE'
              Title.Caption = 'Pagante (EP)'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AF_ENTPAGANTE'
              Title.Caption = 'Pagante (FA)'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENTICONTAT'
              Title.Caption = 'Contato'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaContat'
              Title.Caption = 'Contato'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaExePrv'
              Title.Caption = 'Prev. Exec.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaExeFim'
              Title.Caption = 'Fim execu'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InvalTotal'
              Title.Caption = '$ N'#227'o aprov.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorTotal'
              Title.Caption = '$ Aprovado'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrcamTotal'
              Title.Caption = '$ Or'#231'ado'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_OPERACOES'
              Title.Caption = 'Opera'#231#245'es'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MUL_SRV'
              Title.Caption = 'Multi servi'#231'os'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_AgeEqiCab'
              Title.Caption = 'Equipe de agentes'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OSFlhGrCab'
              Title.Caption = 'Grupo filhas'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_LUGAR'
              Title.Caption = 'Lugar'
              Width = 300
              Visible = True
            end>
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 704
    Width = 1556
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 1376
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1374
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 164
        Height = 65
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object BtOK: TBitBtn
          Tag = 18
          Left = 15
          Top = 5
          Width = 147
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Pesquisa'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtOKClick
        end
      end
      object PnProtocolo: TPanel
        Left = 164
        Top = 0
        Width = 467
        Height = 65
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object BtProtocolo: TBitBtn
          Tag = 10051
          Left = 0
          Top = 5
          Width = 148
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtProtocoloClick
        end
        object BtTodos: TBitBtn
          Tag = 127
          Left = 148
          Top = 5
          Width = 156
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Todos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTodosClick
        end
        object BtNenhum: TBitBtn
          Tag = 128
          Left = 305
          Top = 5
          Width = 157
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Nenhum'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtNenhumClick
        end
      end
      object Panel10: TPanel
        Left = 631
        Top = 0
        Width = 743
        Height = 65
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object LaInfo1: TLabel
          Left = 1
          Top = 27
          Width = 15
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaInfo2: TLabel
          Left = 0
          Top = 26
          Width = 15
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 156
    Top = 65524
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrClientesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 184
    Top = 65524
  end
  object QrEntContrat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 216
    Top = 65528
    object QrEntContratCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntContratCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
    end
    object QrEntContratCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntContratNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEntContrat: TDataSource
    DataSet = QrEntContrat
    Left = 240
    Top = 65524
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '    SELECT Controle, Nome '
      '    FROM enticontat '
      '    ORDER BY Nome')
    Left = 392
    Top = 65524
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 420
    Top = 65524
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    SQL.Strings = (
      'SELECT osc.* ,'
      'IF(en1.Tipo=0, en1.RazaoSocial, en1.Nome) NO_ENTIDADE,'
      'IF(en2.Tipo=0, en2.RazaoSocial, en2.Nome) NO_ENTCONTRAT,'
      'enc.Nome NO_ENTICONTAT'
      'FROM oscab osc'
      ''
      'LEFT JOIN entidades en1 ON en1.Codigo=osc.Entidade'
      'LEFT JOIN entidades en2 ON en2.Codigo=osc.EntContrat'
      'LEFT JOIN enticontat enc ON enc.Controle=osc.EntiContat'
      'WHERE osc.Empresa=0'
      'AND osc.DtaContat BETWEEN "1899-01-01" AND "2013-01-01"'
      'AND osc.Entidade <> 0'
      'AND EntContrat <> 0'
      'AND EntiContat <> 0'
      'AND IF(en1.Tipo=0, en1.RazaoSocial, en1.Nome) LIKE "%%"'
      'AND IF(en2.Tipo=0, en2.RazaoSocial, en2.Nome) LIKE "%%"'
      'AND enc.Nome LIKE "%%"')
    Left = 624
    Top = 12
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrPesqSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrPesqEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object QrPesqFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrPesqDtaContat: TDateTimeField
      FieldName = 'DtaContat'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqDtaVisPrv: TDateTimeField
      FieldName = 'DtaVisPrv'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqDtaVisExe: TDateTimeField
      FieldName = 'DtaVisExe'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
      OnGetText = QrPesqDtaExePrvGetText
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
      OnGetText = QrPesqDtaExeFimGetText
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqValorTotal: TFloatField
      FieldName = 'ValorTotal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqDdsPosVda: TIntegerField
      FieldName = 'DdsPosVda'
    end
    object QrPesqEntiContat: TIntegerField
      FieldName = 'EntiContat'
    end
    object QrPesqNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrPesqEntPagante: TIntegerField
      FieldName = 'EntPagante'
    end
    object QrPesqEntContrat: TIntegerField
      FieldName = 'EntContrat'
    end
    object QrPesqEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPesqNO_ENTIDADE: TWideStringField
      FieldName = 'NO_ENTIDADE'
      Size = 100
    end
    object QrPesqAF_ENTIDADE: TWideStringField
      FieldName = 'AF_ENTIDADE'
      Size = 100
    end
    object QrPesqAF_ENTCONTRAT: TWideStringField
      FieldName = 'AF_ENTCONTRAT'
      Size = 100
    end
    object QrPesqAF_ENTPAGANTE: TWideStringField
      FieldName = 'AF_ENTPAGANTE'
      Size = 100
    end
    object QrPesqNO_ENTCONTRAT: TWideStringField
      FieldName = 'NO_ENTCONTRAT'
      Size = 100
    end
    object QrPesqNO_ENTICONTAT: TWideStringField
      FieldName = 'NO_ENTICONTAT'
      Size = 30
    end
    object QrPesqNO_ENTPAGENTE: TWideStringField
      FieldName = 'NO_ENTPAGANTE'
      Size = 30
    end
    object QrPesqGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrPesqOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrPesqDtaLibFat: TDateTimeField
      FieldName = 'DtaLibFat'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqDtaFimFat: TDateTimeField
      FieldName = 'DtaFimFat'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqValorServi: TFloatField
      FieldName = 'ValorServi'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqValorDesco: TFloatField
      FieldName = 'ValorDesco'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqValorOutrs: TFloatField
      FieldName = 'ValorOutrs'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrPesqCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrPesqSerNF: TWideStringField
      FieldName = 'SerNF'
      Size = 3
    end
    object QrPesqNumNF: TIntegerField
      FieldName = 'NumNF'
    end
    object QrPesqLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesqUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesqUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPesqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPesqAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPesqInvalServi: TFloatField
      FieldName = 'InvalServi'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqInvalDesco: TFloatField
      FieldName = 'InvalDesco'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqInvalOutrs: TFloatField
      FieldName = 'InvalOutrs'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqInvalTotal: TFloatField
      FieldName = 'InvalTotal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqOrcamServi: TFloatField
      FieldName = 'OrcamServi'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqOrcamDesco: TFloatField
      FieldName = 'OrcamDesco'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqOrcamOutrs: TFloatField
      FieldName = 'OrcamOutrs'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqOrcamTotal: TFloatField
      FieldName = 'OrcamTotal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqValiDdOrca: TIntegerField
      FieldName = 'ValiDdOrca'
    end
    object QrPesqOperacao: TIntegerField
      FieldName = 'Operacao'
    end
    object QrPesqExeTxtCli1: TIntegerField
      FieldName = 'ExeTxtCli1'
    end
    object QrPesqValorPre: TFloatField
      FieldName = 'ValorPre'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqObsGaranti: TWideMemoField
      FieldName = 'ObsGaranti'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPesqObsExecuta: TWideMemoField
      FieldName = 'ObsExecuta'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPesqExeTxtCli2: TIntegerField
      FieldName = 'ExeTxtCli2'
    end
    object QrPesqFimVisPrv: TDateTimeField
      FieldName = 'FimVisPrv'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqFimVisExe: TDateTimeField
      FieldName = 'FimVisExe'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqFimExePrv: TDateTimeField
      FieldName = 'FimExePrv'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrPesqNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrPesqOptado: TSmallintField
      FieldName = 'Optado'
    end
    object QrPesqHowGerou: TSmallintField
      FieldName = 'HowGerou'
    end
    object QrPesqPosGerou: TSmallintField
      FieldName = 'PosGerou'
    end
    object QrPesqOSFlhUltGe: TIntegerField
      FieldName = 'OSFlhUltGe'
    end
    object QrPesqEstatus_TXT: TWideStringField
      FieldName = 'Estatus_TXT'
      Size = 100
    end
    object QrPesqNO_MUL_SRV: TWideStringField
      FieldName = 'NO_MUL_SRV'
      Size = 255
    end
    object QrPesqNO_AgeEqiCab: TWideStringField
      FieldName = 'NO_AgeEqiCab'
      Size = 255
    end
    object QrPesqNO_LUGAR: TWideStringField
      FieldName = 'NO_LUGAR'
      Size = 100
    end
    object QrPesqOSFlhGrCab: TIntegerField
      FieldName = 'OSFlhGrCab'
    end
    object QrPesqNO_OPERACOES: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_OPERACOES'
      Size = 255
    end
    object QrPesqRotaLatLon: TIntegerField
      FieldName = 'RotaLatLon'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 652
    Top = 12
  end
  object DsEstatusOSs: TDataSource
    DataSet = QrEstatusOSs
    Left = 568
    Top = 460
  end
  object QrEstatusOSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM estatusoss'
      'ORDER BY Codigo')
    Left = 540
    Top = 460
    object QrEstatusOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstatusOSsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object QrDesServico: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM desservico'
      'ORDER BY Nome')
    Left = 596
    Top = 460
    object QrDesServicoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDesServicoNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsDesServico: TDataSource
    DataSet = QrDesServico
    Left = 624
    Top = 460
  end
  object PMOSCabAlv: TPopupMenu
    Left = 496
    Top = 20
    object IncluiOSCabAlv1: TMenuItem
      Caption = '&Adiciona, muda ou retira pragas alvo'
      OnClick = IncluiOSCabAlv1Click
    end
    object ExcluiOSCabAlv1: TMenuItem
      Caption = '&Retira a praga alvo selecionada'
      OnClick = ExcluiOSCabAlv1Click
    end
  end
  object PMOSAge: TPopupMenu
    Left = 524
    Top = 20
    object IncluiOSAge1: TMenuItem
      Caption = '&Adiciona agente(s)'
      OnClick = IncluiOSAge1Click
    end
    object RemoveOSAge1: TMenuItem
      Caption = '&Retira agente'
      OnClick = RemoveOSAge1Click
    end
  end
  object QrPsqOSCabAlv: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT oca.*, IF(oca.Praga_Z <> 0, '#39'E'#39', '#39'G'#39') NO_NIVEL, '
      'IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) NO_PRAGA'
      'FROM _psq_oscabalv oca'
      'LEFT JOIN bugstrol.praga_a pca ON pca.Codigo=oca.Praga_A'
      'LEFT JOIN bugstrol.praga_z pcz ON pcz.Codigo=oca.Praga_Z'
      'WHERE oca.Codigo=:P0'
      'ORDER BY oca.Ordem')
    Left = 652
    Top = 460
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPsqOSCabAlvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsqOSCabAlvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPsqOSCabAlvOrdem: TSmallintField
      FieldName = 'Ordem'
    end
    object QrPsqOSCabAlvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPsqOSCabAlvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPsqOSCabAlvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPsqOSCabAlvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPsqOSCabAlvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPsqOSCabAlvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPsqOSCabAlvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPsqOSCabAlvPraga_A: TIntegerField
      FieldName = 'Praga_A'
    end
    object QrPsqOSCabAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
    end
    object QrPsqOSCabAlvNO_NIVEL: TWideStringField
      FieldName = 'NO_NIVEL'
      Required = True
      Size = 1
    end
    object QrPsqOSCabAlvNO_PRAGA: TWideStringField
      FieldName = 'NO_PRAGA'
      Size = 60
    end
  end
  object DsPsqOSCabAlv: TDataSource
    DataSet = QrPsqOSCabAlv
    Left = 680
    Top = 460
  end
  object QrPsqOSAlv: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT prc.Nome NO_Praga_Z, osa.*'
      'FROM osalv osa'
      'LEFT JOIN praga_z prc ON prc.Codigo=osa.Praga_Z'
      'WHERE osa.Controle=:P0'
      '')
    Left = 708
    Top = 460
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPsqOSAlvCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osalv.Codigo'
    end
    object QrPsqOSAlvControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osalv.Controle'
    end
    object QrPsqOSAlvConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'osalv.Conta'
    end
    object QrPsqOSAlvNO_Praga_Z: TWideStringField
      FieldName = 'NO_Praga_Z'
      Origin = 'praga_z.Nome'
      Size = 60
    end
    object QrPsqOSAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
      Origin = 'osalv.Praga_Z'
    end
  end
  object DsPsqOSAlv: TDataSource
    DataSet = QrPsqOSAlv
    Left = 736
    Top = 460
  end
  object QrPsqOSAge: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Nome NO_AGENTE, age.* '
      'FROM osage age'
      'LEFT JOIN entidades ent on ent.Codigo=age.Agente'
      'WHERE age.Codigo=:P0')
    Left = 540
    Top = 508
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPsqOSAgeNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
    object QrPsqOSAgeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsqOSAgeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPsqOSAgeAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrPsqOSAgeResponsa: TSmallintField
      FieldName = 'Responsa'
    end
  end
  object DsPsqOSAge: TDataSource
    DataSet = QrPsqOSAge
    Left = 568
    Top = 508
  end
  object QrFatoGeradr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, NeedOS_Ori'
      'FROM fatogeradr'
      'ORDER BY Nome')
    Left = 596
    Top = 508
    object QrFatoGeradrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFatoGeradrNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFatoGeradrNeedOS_Ori: TSmallintField
      FieldName = 'NeedOS_Ori'
    end
  end
  object DsFatoGeradr: TDataSource
    DataSet = QrFatoGeradr
    Left = 624
    Top = 508
  end
  object PMOSAlv: TPopupMenu
    Left = 556
    Top = 20
    object xxx1: TMenuItem
      Caption = 'Adiciona pragas alvo'
      OnClick = xxx1Click
    end
    object Retirapragaalvoatual1: TMenuItem
      Caption = 'Retira praga alvo atual'
      OnClick = Retirapragaalvoatual1Click
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 4
    Top = 12
  end
  object Timer1: TTimer
    Left = 340
    Top = 348
  end
  object QrEntPagante: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 268
    Top = 65528
    object QrEntPaganteCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntPaganteCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
    end
    object QrEntPaganteCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntPaganteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEntPagante: TDataSource
    DataSet = QrEntPagante
    Left = 296
    Top = 65528
  end
  object PMProtocolo: TPopupMenu
    Left = 352
    Top = 540
    object Adicionaaumlote1: TMenuItem
      Caption = 'Adiciona a um lote'
      Enabled = False
      OnClick = Adicionaaumlote1Click
    end
    object N1: TMenuItem
      Caption = '&'
    end
  end
end
