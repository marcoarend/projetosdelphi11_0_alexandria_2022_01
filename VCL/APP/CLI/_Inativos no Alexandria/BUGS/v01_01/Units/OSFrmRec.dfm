object FmOSFrmRec: TFmOSFrmRec
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-008 :: Ordem de Servi'#231'o - Formula'#231#227'o'
  ClientHeight = 814
  ClientWidth = 965
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 906
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 847
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 455
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Formula'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 455
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Formula'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 455
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de Servi'#231'o - Formula'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 965
    Height = 614
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 965
      Height = 614
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 114
        Width = 965
        Height = 500
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 1
        object GBEdita: TGroupBox
          Left = 2
          Top = 18
          Width = 961
          Height = 480
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Itens da F'#243'rmula: '
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object DBGrid1: TDBGrid
            Left = 2
            Top = 18
            Width = 957
            Height = 460
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Color = clWhite
            DataSource = DsOSFrmRec
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGrid1CellClick
            OnColEnter = DBGrid1ColEnter
            OnColExit = DBGrid1ColExit
            OnDrawColumnCell = DBGrid1DrawColumnCell
            OnKeyDown = DBGrid1KeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'EhDiluente'
                Title.Caption = 'Dil'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ordem'
                Title.Caption = 'Seq.'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrvQtd'
                Title.Alignment = taRightJustify
                Title.Caption = 'Quantidade'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLAUNIDMED'
                Title.Caption = 'Unidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Alignment = taRightJustify
                Title.Caption = 'C'#243'd [F4]'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_GGX'
                Title.Caption = 'Descri'#231#227'o do Produto [F7]'
                Width = 270
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NumLote'
                Title.Caption = 'Lote [F7][F4][F8]'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NumLaudo'
                Title.Caption = 'Laudo [F7][F4][F8]'
                Width = 100
                Visible = True
              end>
          end
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 965
        Height = 114
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 18
          Width = 961
          Height = 32
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object Label7: TLabel
            Left = 10
            Top = 5
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label5: TLabel
            Left = 133
            Top = 5
            Width = 65
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID Servi'#231'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label4: TLabel
            Left = 286
            Top = 5
            Width = 49
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Servi'#231'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object DBEdit15: TDBEdit
            Left = 59
            Top = 0
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsOSSrv
            TabOrder = 0
          end
          object DBEdit8: TDBEdit
            Left = 202
            Top = 0
            Width = 79
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            DataField = 'Controle'
            DataSource = DsOSSrv
            TabOrder = 1
          end
          object DBEdit6: TDBEdit
            Left = 340
            Top = 0
            Width = 39
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            DataField = 'DesServico'
            DataSource = DsOSSrv
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 379
            Top = 0
            Width = 567
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            DataField = 'NO_DesServico'
            DataSource = DsOSSrv
            TabOrder = 3
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 50
          Width = 961
          Height = 62
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object Label10: TLabel
            Left = 10
            Top = 5
            Width = 68
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID F'#243'rmula:'
            FocusControl = DBEdit1
          end
          object Label11: TLabel
            Left = 158
            Top = 5
            Width = 65
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
            FocusControl = DBEdit2
          end
          object Label12: TLabel
            Left = 551
            Top = 5
            Width = 86
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'F'#243'rmula base:'
            FocusControl = DBEdit3
          end
          object Label13: TLabel
            Left = 10
            Top = 34
            Width = 83
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Equipamento:'
            FocusControl = DBEdit4
          end
          object Label14: TLabel
            Left = 487
            Top = 34
            Width = 101
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade total:'
            FocusControl = DBEdit7
          end
          object Label15: TLabel
            Left = 719
            Top = 34
            Width = 104
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade QSP:'
            FocusControl = DBEdit9
          end
          object DBEdit1: TDBEdit
            Left = 84
            Top = 0
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Conta'
            DataSource = DsOSFrmCab
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 226
            Top = 0
            Width = 317
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Nome'
            DataSource = DsOSFrmCab
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 640
            Top = 0
            Width = 306
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_FORMULA'
            DataSource = DsOSFrmCab
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 94
            Top = 30
            Width = 390
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_EquipAplic'
            DataSource = DsOSFrmCab
            TabOrder = 3
          end
          object DBEdit7: TDBEdit
            Left = 591
            Top = 30
            Width = 123
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'QtdTot'
            DataSource = DsOSFrmCab
            TabOrder = 4
          end
          object DBEdit9: TDBEdit
            Left = 822
            Top = 30
            Width = 123
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'QtdQSP'
            DataSource = DsOSFrmCab
            TabOrder = 5
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 673
    Width = 965
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 961
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 336
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Para excluir um item digite as teclas Ctrl + Del'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 336
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Para excluir um item digite as teclas Ctrl + Del'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 727
    Width = 965
    Height = 87
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 786
      Top = 18
      Width = 177
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 784
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOSSrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT des.Nome NO_DesServico, srv.* '
      'FROM ossrv srv'
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico'
      'WHERE srv.Codigo=:P0')
    Left = 12
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSSrvNO_DesServico: TWideStringField
      FieldName = 'NO_DesServico'
      Size = 60
    end
    object QrOSSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSSrvDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrOSSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsOSSrv: TDataSource
    DataSet = QrOSSrv
    Left = 40
    Top = 12
  end
  object QrFormulas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM formulas'
      'ORDER BY Nome')
    Left = 464
    Top = 12
    object QrFormulasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 492
    Top = 12
  end
  object QrEquipAplic: TmySQLQuery
    Database = Dmod.MyDB
    Left = 520
    Top = 12
    object QrEquipAplicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEquipAplicNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsEquipAplic: TDataSource
    DataSet = QrEquipAplic
    Left = 548
    Top = 12
  end
  object QrOSFrmCab: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrOSFrmCabAfterScroll
    SQL.Strings = (
      'SELECT ofc.*, frm.Nome NO_FORMULA, eqa.Nome NO_EquipAplic '
      'FROM osfrmcab ofc'
      'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula'
      'LEFT JOIN equiaplic eqa ON eqa.Codigo=ofc.EquipAplic'
      'WHERE ofc.Controle=:P0'
      ''
      '')
    Left = 12
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSFrmCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSFrmCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSFrmCabConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSFrmCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOSFrmCabFormula: TIntegerField
      FieldName = 'Formula'
    end
    object QrOSFrmCabEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrOSFrmCabQtdTot: TFloatField
      FieldName = 'QtdTot'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSFrmCabQtdQSP: TFloatField
      FieldName = 'QtdQSP'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrOSFrmCabCusTot: TFloatField
      FieldName = 'CusTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOSFrmCabNO_FORMULA: TWideStringField
      FieldName = 'NO_FORMULA'
      Size = 60
    end
    object QrOSFrmCabNO_EquipAplic: TWideStringField
      FieldName = 'NO_EquipAplic'
      Size = 60
    end
    object QrOSFrmCabDiluente: TSmallintField
      FieldName = 'Diluente'
    end
  end
  object DsOSFrmCab: TDataSource
    DataSet = QrOSFrmCab
    Left = 40
    Top = 40
  end
  object TbOSFrmRec: TmySQLTable
    Database = Dmod.MyDB
    BeforeInsert = TbOSFrmRecBeforeInsert
    BeforePost = TbOSFrmRecBeforePost
    AfterPost = TbOSFrmRecAfterPost
    AfterDelete = TbOSFrmRecAfterDelete
    OnNewRecord = TbOSFrmRecNewRecord
    SortFieldNames = 'Ordem'
    TableName = 'osfrmrec'
    Left = 72
    Top = 196
    object TbOSFrmRecNO_GGX: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_GGX'
      LookupDataSet = QrGGXs
      LookupKeyFields = 'Controle'
      LookupResultField = 'NO_PRD_TAM_COR'
      KeyFields = 'GraGruX'
      Size = 120
      Lookup = True
    end
    object TbOSFrmRecCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbOSFrmRecControle: TIntegerField
      FieldName = 'Controle'
    end
    object TbOSFrmRecConta: TIntegerField
      FieldName = 'Conta'
    end
    object TbOSFrmRecIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object TbOSFrmRecGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object TbOSFrmRecPrvQtd: TFloatField
      FieldName = 'PrvQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object TbOSFrmRecPrvPrc: TFloatField
      FieldName = 'PrvPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSFrmRecPrvVal: TFloatField
      FieldName = 'PrvVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSFrmRecUsoQtd: TFloatField
      FieldName = 'UsoQtd'
      DisplayFormat = '#,###,###,##0.000'
    end
    object TbOSFrmRecUsoPrc: TFloatField
      FieldName = 'UsoPrc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSFrmRecUsoVal: TFloatField
      FieldName = 'UsoVal'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object TbOSFrmRecUsoDec: TFloatField
      FieldName = 'UsoDec'
      DisplayFormat = '#,###,###,##0.00'
    end
    object TbOSFrmRecUsoTot: TFloatField
      FieldName = 'UsoTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object TbOSFrmRecOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object TbOSFrmRecReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object TbOSFrmRecEhDiluente: TSmallintField
      FieldName = 'EhDiluente'
    end
    object TbOSFrmRecSIGLAUNIDMED: TWideStringField
      FieldKind = fkLookup
      FieldName = 'SIGLAUNIDMED'
      LookupDataSet = QrGGXs
      LookupKeyFields = 'Controle'
      LookupResultField = 'SIGLAUNIDMED'
      KeyFields = 'GraGruX'
      Size = 10
      Lookup = True
    end
    object TbOSFrmRecNumLote: TWideStringField
      FieldName = 'NumLote'
      Size = 60
    end
    object TbOSFrmRecNumLaudo: TWideStringField
      FieldName = 'NumLaudo'
      Size = 60
    end
    object TbOSFrmRecRatifUso: TSmallintField
      FieldName = 'RatifUso'
    end
  end
  object DsOSFrmRec: TDataSource
    DataSet = TbOSFrmRec
    Left = 72
    Top = 244
  end
  object QrGGXs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, gcc.*, CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam IS NULL OR gti.PrintTam=0, " ", CONCAT(" ", gti.' +
        'Nome)), '
      
        'IF(gcc.PrintCor IS NULL OR gcc.PrintCor=0," ", CONCAT(" ", gcc.N' +
        'ome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      '/*WHERE gg1.Nivel2 IN (-3,-2)*/'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 172
    Top = 196
    object QrGGXsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXsCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGxs: TDataSource
    DataSet = QrGGXs
    Left = 172
    Top = 244
  end
  object QrReordem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE osfrmrec SET'
      'Ordem=Reordem'
      'WHERE Conta=:P0'
      '')
    Left = 72
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrG1Apl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pmo.ValCliDd '
      'FROM grag1prmo pmo '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pmo.Nivel1 '
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=pmo.Nivel1 '
      'WHERE ggx.Controle=:P0 '
      '')
    Left = 172
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrG1AplValCliDd: TIntegerField
      FieldName = 'ValCliDd'
    end
    object QrG1AplAtuNumLot: TWideStringField
      FieldName = 'AtuNumLot'
      Size = 60
    end
    object QrG1AplAtuNumLaud: TWideStringField
      FieldName = 'AtuNumLaud'
      Size = 60
    end
  end
  object PMLote: TPopupMenu
    Left = 440
    Top = 336
    object Pesquisa1: TMenuItem
      Caption = 'Pesquisa'
    end
    object Ativadesatova1: TMenuItem
      Caption = 'Gerencia ativos/inativos'
    end
    object Gerencia1: TMenuItem
      Caption = 'Gerencia'
    end
  end
end
