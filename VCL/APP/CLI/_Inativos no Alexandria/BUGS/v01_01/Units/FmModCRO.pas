unit FmModCRO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  frxClass, frxDBSet;

type
  TFmOSRapida = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    frxCRO_AUXIL_000_01: TfrxReport;
    QrExecucoes: TmySQLQuery;
    QrExecucoesCodigo: TIntegerField;
    QrExecucoesInicio: TDateTimeField;
    QrExecucoesFim: TDateTimeField;
    QrExecucoesEscalonamento: TFloatField;
    QrExecucoesDIAS: TFloatField;
    frxDsExecucoes: TfrxDBDataset;
    QrExecucoesDIAS_TXT: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrExecucoesCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    //
    function VerificaHorariosDeExecucao(): Boolean;
  end;

  var
  FmOSRapida: TFmOSRapida;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral;

{$R *.DFM}

procedure TFmOSRapida.BtOKClick(Sender: TObject);
begin
;
end;

procedure TFmOSRapida.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSRapida.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSRapida.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSRapida.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSRapida.QrExecucoesCalcFields(DataSet: TDataSet);
var
  Dias, Horas, Minutos: Integer;
  Texto: String;
begin
  Dias := Trunc(QrExecucoesDIAS.Value);
  if Dias <> 0 then
    Texto := Geral.FF0(Dias) + 'dias, '
  else
    Texto := '';
  ///
  Horas := Trunc((QrExecucoesDIAS.Value - Dias) * 24);
  Texto := Texto + Geral.FF0(Horas) + ' horas e ';
  //
  Minutos := Trunc((QrExecucoesDIAS.Value - Dias - (horas / 24)) * 60);
  Texto := Texto + Geral.FF0(Minutos) + ' minutos ';
end;

function TFmOSRapida.VerificaHorariosDeExecucao: Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrExecucoes, Dmod.MyDB, [
  'SELECT cab.Codigo, DtaExeIni Inicio, DtaExeFim Fim, ',
  '0.000 Escalonamento, ',
  '((UNIX_TIMESTAMP(DtaExeFim) -  ',
  '  UNIX_TIMESTAMP(DtaExeIni))) / 86400 DIAS ',
  'FROM oscab cab ',
  'WHERE (DtaExeIni > "1900-01-01" ',
  'OR DtaExeFim > "1900-01-01") ',
  'AND ( ',
  '      ( ',
  '        ((UNIX_TIMESTAMP(DtaExeFim) -  ',
  '        UNIX_TIMESTAMP(DtaExeIni))) / 86400 > 1 ',
  '      ) ',
  '    OR ',
  '      ( ',
  '        ((UNIX_TIMESTAMP(DtaExeFim) -  ',
  '        UNIX_TIMESTAMP(DtaExeIni)))  < 1 ',
  '      ) ',
  ') ',
  ' ',
  'UNION ',
  ' ',
  'SELECT cab.Codigo, ocx.DtHrIni Inicio, ocx.DtHrFim Final, ',
  'ocx.Controle + 0.000 Escalonamento, ',
  '((UNIX_TIMESTAMP(ocx.DtHrFim) - ',
  '  UNIX_TIMESTAMP(ocx.DtHrIni))) / 86400 DIAS ',
  'FROM oscabxtr ocx ',
  'LEFT JOIN oscab cab ON cab.Codigo=ocx.Codigo ',
  'WHERE (ocx.DtHrIni > "1900-01-01" ',
  'OR ocx.DtHrFim > "1900-01-01") ',
  'AND ( ',
  '      ( ',
  '        ((UNIX_TIMESTAMP(ocx.DtHrFim) - ',
  '        UNIX_TIMESTAMP(ocx.DtHrIni))) / 86400 > 1 ',
  '      ) ',
  '    OR ',
  '      ( ',
  '        ((UNIX_TIMESTAMP(ocx.DtHrFim) - ',
  '        UNIX_TIMESTAMP(ocx.DtHrIni)))  < 1 ',
  '      ) ',
  ') ',
  'ORDER BY DIAS ',
  '']);
  Result := QrExecucoes.RecordCount > 0;
  if Result then
  begin
    MyObjects.frxDefineDataSets(frxCRO_AUXIL_000_01, [
      DmodG.frxDsDono,
      frxDsExecucoes
    ]);
    frxCRO_AUXIL_000_01.Variables['VARF_DATA'] := DModG.ObtemAgora();
    MyObjects.frxMostra(frxCRO_AUXIL_000_01, 'Tempos de Execu��es Inv�lidos');
  end;
(*
SELECT cab.Codigo, ocx.DtHrIni, ocx.DtHrFim,
((UNIX_TIMESTAMP(ocx.DtHrFim) -
  UNIX_TIMESTAMP(ocx.DtHrIni))) / 86400 DIAS
FROM oscabxtr ocx
LEFT JOIN oscab cab ON cab.Codigo=ocx.Codigo
WHERE (ocx.DtHrIni > "1900-01-01"
OR ocx.DtHrFim > "1900-01-01")
AND (
      (
        ((UNIX_TIMESTAMP(ocx.DtHrFim) -
        UNIX_TIMESTAMP(ocx.DtHrIni))) / 86400 > 1
      )
    OR
      (
        ((UNIX_TIMESTAMP(ocx.DtHrFim) -
        UNIX_TIMESTAMP(ocx.DtHrIni)))  < 1
      )
)
ORDER BY DIAS
*)

end;

end.
