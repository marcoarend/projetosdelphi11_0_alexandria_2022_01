unit OSRec;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkCheckBox;

type
  TFmOSRec = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit15: TDBEdit;
    Label2: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label1: TLabel;
    CkContinuar: TCheckBox;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    DsOSSrv: TDataSource;
    EdConta: TdmkEdit;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGruX: TIntegerField;
    QrGraGruXNivel3: TIntegerField;
    QrGraGruXNivel2: TIntegerField;
    QrGraGruXNivel1: TIntegerField;
    QrGraGruXNome: TWideStringField;
    QrGraGruXPrdGrupTip: TIntegerField;
    QrGraGruXGraTamCad: TIntegerField;
    QrGraGruXHowBxaEstq: TSmallintField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNOMEGRATAMCAD: TWideStringField;
    QrGraGruXCODUSUGRATAMCAD: TIntegerField;
    QrGraGruXCST_A: TSmallintField;
    QrGraGruXCST_B: TSmallintField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXPeso: TFloatField;
    QrGraGruXFatorClas: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    EdPrvQtd: TdmkEdit;
    Label3: TLabel;
    Label6: TLabel;
    EdPrvPrc: TdmkEdit;
    Label8: TLabel;
    EdPrvVal: TdmkEdit;
    CkEhQSP: TdmkCheckBox;
    Label9: TLabel;
    EdPrvDes: TdmkEdit;
    Label10: TLabel;
    EdPrvTot: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPrvQtdChange(Sender: TObject);
    procedure EdPrvPrcChange(Sender: TObject);
    procedure EdPrvValChange(Sender: TObject);
    procedure EdPrvDesChange(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValor();
    procedure CalculaTotal();
  public
    { Public declarations }
    FQrInsUpd: TmySQLQuery;
  end;

  var
  FmOSRec: TFmOSRec;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmOSRec.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, Tabela, Cadastro: Integer;
begin
{
  Codigo     := QrOSSrvCodigo.Value;
  Controle   := QrOSSrvControle.Value;
  Conta      := EdConta.ValueVariant;
  //
  Tabela := RGTabela.ItemIndex;
  Cadastro := EdCadastro.ValueVariant;
  if MyObjects.FIC(Tabela = 0, RGTabela, 'Informe a tabela de origem!') then
    Exit;
  if MyObjects.FIC(Cadastro = 0, RGTabela, 'Informe o perímetro!') then
    Exit;
  //
  Conta := UMyMod.BPGS1I32('osrec', 'Conta', '', '',
    tsDef, ImgTipo.SQLType, Conta);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osrec', False, [
  'Codigo', 'Controle', 'Tabela',
  'Cadastro'], ['Conta'], [
  Codigo, Controle, Tabela,
  Cadastro], [Conta], True) then
  begin
    if FQrInsUpd <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrInsUpd, Dmod.MyDB);
      FQrInsUpd.Locate('Conta', Conta, []);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdConta.ValueVariant := 0;
      RGTabela.ItemIndex := 0;
      EdCadastro.ValueVariant := 0;
      CBCadastro.KeyValue := 0;
    end else
      Close;
  end;
}
end;

procedure TFmOSRec.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSRec.CalculaTotal();
var
  Total: Double;
begin
  Total := EdPrvVal.ValueVariant - EdPrvDes.ValueVariant;
  if Total < 0 then
  begin
     Total := 0;
     EdPrvDes.ValueVariant := EdPrvVal.ValueVariant;
  end else
    EdPrvTot.ValueVariant := Total;
end;

procedure TFmOSRec.CalculaValor();
var
  Valor: Double;
begin
  Valor := EdPrvQtd.ValueVariant - EdPrvPrc.ValueVariant;
  EdPrvVal.ValueVariant := Valor;
end;

procedure TFmOSRec.EdPrvDesChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmOSRec.EdPrvPrcChange(Sender: TObject);
begin
  CalculaValor();
end;

procedure TFmOSRec.EdPrvQtdChange(Sender: TObject);
begin
  CalculaValor();
end;

procedure TFmOSRec.EdPrvValChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmOSRec.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSRec.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
