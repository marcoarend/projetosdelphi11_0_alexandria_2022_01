object FmPraga_Z: TFmPraga_Z
  Left = 368
  Top = 194
  Caption = 'CAD-PRAGA-001 :: Cadastro de Esp'#233'cies de Pragas'
  ClientHeight = 514
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 418
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 145
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 128
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 0
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 72
          Top = 0
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label6: TLabel
          Left = 648
          Top = 0
          Width = 34
          Height = 13
          Caption = 'Ordem:'
          FocusControl = DBEdit3
        end
        object Label5: TLabel
          Left = 12
          Top = 44
          Width = 71
          Height = 13
          Caption = 'N'#237'vel Superior:'
          FocusControl = DBEdit2
        end
        object Label8: TLabel
          Left = 344
          Top = 44
          Width = 41
          Height = 13
          Caption = 'Esp'#233'cie:'
          FocusControl = DBEdit4
        end
        object Label14: TLabel
          Left = 12
          Top = 88
          Width = 80
          Height = 13
          Caption = 'Subdiret'#243'rio foto:'
        end
        object Label17: TLabel
          Left = 240
          Top = 111
          Width = 5
          Height = 13
          Caption = '\'
        end
        object Label15: TLabel
          Left = 260
          Top = 88
          Width = 60
          Height = 13
          Caption = 'Arquivo foto:'
        end
        object Label16: TLabel
          Left = 484
          Top = 88
          Width = 114
          Height = 13
          Caption = 'Direitos autorais da foto:'
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 12
          Top = 16
          Width = 56
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsPraga_Z
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 72
          Top = 16
          Width = 573
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsPraga_Z
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit3: TDBEdit
          Left = 648
          Top = 16
          Width = 56
          Height = 21
          DataField = 'Ordem'
          DataSource = DsPraga_Z
          TabOrder = 2
        end
        object DBEdit2: TDBEdit
          Left = 12
          Top = 60
          Width = 56
          Height = 21
          DataField = 'NivSup'
          DataSource = DsPraga_Z
          TabOrder = 3
        end
        object DBEdit1: TDBEdit
          Left = 72
          Top = 60
          Width = 269
          Height = 21
          DataField = 'NO_NIVSUP'
          DataSource = DsPraga_Z
          TabOrder = 4
        end
        object DBEdit4: TDBEdit
          Left = 344
          Top = 60
          Width = 361
          Height = 21
          DataField = 'Especie'
          DataSource = DsPraga_Z
          TabOrder = 5
        end
        object DBEdit5: TDBEdit
          Left = 12
          Top = 103
          Width = 220
          Height = 21
          DataField = 'ArqDir'
          DataSource = DsPraga_Z
          TabOrder = 6
        end
        object DBEdit7: TDBEdit
          Left = 260
          Top = 103
          Width = 220
          Height = 21
          DataField = 'ArqImg'
          DataSource = DsPraga_Z
          TabOrder = 7
        end
        object DBEdit6: TDBEdit
          Left = 484
          Top = 103
          Width = 220
          Height = 21
          DataField = 'Copyright'
          DataSource = DsPraga_Z
          TabOrder = 8
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 354
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 102
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 276
        Top = 15
        Width = 506
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 376
          Top = 0
          Width = 130
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 145
      Width = 784
      Height = 124
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Taxonomia'
        object PnFoto: TPanel
          Left = 597
          Top = 0
          Width = 179
          Height = 96
          Align = alClient
          Alignment = taLeftJustify
          Caption = 'Foto n'#227'o dispon'#237'vel'
          TabOrder = 0
          object Image1: TImage
            Left = 1
            Top = 1
            Width = 177
            Height = 94
            Cursor = crHandPoint
            Align = alClient
            Proportional = True
            Stretch = True
            OnClick = Image1Click
            ExplicitLeft = 36
            ExplicitTop = 68
            ExplicitWidth = 105
            ExplicitHeight = 105
          end
        end
        object DBGDados: TDBGrid
          Left = 0
          Top = 0
          Width = 597
          Height = 96
          Align = alLeft
          DataSource = DsPraga_ZTax
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TAXON_ITS'
              Title.Caption = 'N'#237'vel Taxon'#244'mico'
              Width = 244
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231'ao'
              Width = 311
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Caracter'#237'sticas'
        ImageIndex = 1
        object DBRichEdit1: TDBRichEdit
          Left = 0
          Top = 0
          Width = 776
          Height = 96
          Align = alClient
          DataField = 'Observacao'
          DataSource = DsPraga_Z
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Preven'#231#227'o'
        ImageIndex = 2
        object DBRichEdit2: TDBRichEdit
          Left = 0
          Top = 0
          Width = 776
          Height = 96
          Align = alClient
          DataField = 'Prevencao'
          DataSource = DsPraga_Z
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 418
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 177
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 60
        Width = 71
        Height = 13
        Caption = 'N'#237'vel Superior:'
        Color = clBtnFace
        ParentColor = False
      end
      object SbNivSup: TSpeedButton
        Left = 324
        Top = 76
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbNivSupClick
      end
      object Label4: TLabel
        Left = 652
        Top = 16
        Width = 34
        Height = 13
        Caption = 'Ordem:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 348
        Top = 60
        Width = 41
        Height = 13
        Caption = 'Esp'#233'cie:'
      end
      object SpeedButton5: TSpeedButton
        Left = 240
        Top = 120
        Width = 21
        Height = 21
        Caption = '<>'
        OnClick = SpeedButton5Click
      end
      object Label11: TLabel
        Left = 16
        Top = 104
        Width = 80
        Height = 13
        Caption = 'Subdiret'#243'rio foto:'
      end
      object Label12: TLabel
        Left = 264
        Top = 104
        Width = 60
        Height = 13
        Caption = 'Arquivo foto:'
      end
      object Label13: TLabel
        Left = 488
        Top = 104
        Width = 114
        Height = 13
        Caption = 'Direitos autorais da foto:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCodigoChange
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 572
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNivSup: TdmkEditCB
        Left = 16
        Top = 76
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NivSup'
        UpdCampo = 'NivSup'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBNivSup
        IgnoraDBLookupComboBox = False
      end
      object CBNivSup: TdmkDBLookupComboBox
        Left = 76
        Top = 76
        Width = 245
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPraga_A
        TabOrder = 4
        dmkEditCB = EdNivSup
        QryCampo = 'NivSup'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdOrdem: TdmkEdit
        Left = 652
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEspecie: TdmkEdit
        Left = 348
        Top = 76
        Width = 361
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Especie'
        UpdCampo = 'Especie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdArqDir: TdmkEdit
        Left = 16
        Top = 120
        Width = 220
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ArqDir'
        UpdCampo = 'ArqDir'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdArqImg: TdmkEdit
        Left = 264
        Top = 120
        Width = 220
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ArqImg'
        UpdCampo = 'ArqImg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCopyright: TdmkEdit
        Left = 488
        Top = 120
        Width = 220
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Copyright'
        UpdCampo = 'Copyright'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Panel6: TPanel
        Left = 2
        Top = 148
        Width = 780
        Height = 27
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 9
        object Label18: TLabel
          Left = 241
          Top = 6
          Width = 322
          Height = 16
          Caption = 'O bot'#227'o "importar s'#243' fica ativo em modo de "Altera'#231#227'o"!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label19: TLabel
          Left = 240
          Top = 5
          Width = 322
          Height = 16
          Caption = 'O bot'#227'o "importar s'#243' fica ativo em modo de "Altera'#231#227'o"!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 355
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object Panel1: TPanel
        Left = 648
        Top = 15
        Width = 134
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 646
        Height = 46
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 12
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
        object BtImportar: TBitBtn
          Tag = 19
          Left = 136
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Importar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtImportarClick
        end
        object BtVerFoto: TBitBtn
          Tag = 227
          Left = 260
          Top = 2
          Width = 120
          Height = 40
          Caption = 'Ver &Foto'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 2
          Visible = False
        end
        object BtTexto: TBitBtn
          Tag = 121
          Left = 384
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Caracter'#237'sticas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtTextoClick
        end
        object BitBtn1: TBitBtn
          Tag = 121
          Left = 508
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Preven'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BitBtn1Click
        end
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 177
      Width = 784
      Height = 132
      ActivePage = TabSheet4
      Align = alTop
      TabOrder = 2
      object TabSheet4: TTabSheet
        Caption = 'Caracter'#237'sticas'
        object REObservacao: TdmkRichEdit
          Left = 0
          Top = 0
          Width = 776
          Height = 104
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          QryCampo = 'Observacao'
          UpdCampo = 'Observacao'
          UpdType = utYes
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Preven'#231#227'o'
        ImageIndex = 1
        object RePrevencao: TdmkRichEdit
          Left = 0
          Top = 0
          Width = 776
          Height = 104
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          QryCampo = 'Prevencao'
          UpdCampo = 'Prevencao'
          UpdType = utYes
        end
      end
    end
    object CkContinuar: TCheckBox
      Left = 0
      Top = 338
      Width = 784
      Height = 17
      Align = alBottom
      Caption = 'Continuar inserindo.'
      TabOrder = 3
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 399
        Height = 32
        Caption = 'Cadastro de Esp'#233'cies de Pragas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 399
        Height = 32
        Caption = 'Cadastro de Esp'#233'cies de Pragas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 399
        Height = 32
        Caption = 'Cadastro de Esp'#233'cies de Pragas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrPraga_Z: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPraga_ZBeforeOpen
    AfterOpen = QrPraga_ZAfterOpen
    BeforeClose = QrPraga_ZBeforeClose
    AfterScroll = QrPraga_ZAfterScroll
    SQL.Strings = (
      'SELECT gru.Nome NO_NIVSUP, cab.* '
      'FROM praga_z cab'
      'LEFT JOIN praga_a gru ON gru.Codigo=cab.NivSup')
    Left = 64
    Top = 64
    object QrPraga_ZCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'praga_z.Codigo'
    end
    object QrPraga_ZNome: TWideStringField
      DisplayWidth = 255
      FieldName = 'Nome'
      Origin = 'praga_z.Nome'
      Size = 60
    end
    object QrPraga_ZNivSup: TIntegerField
      FieldName = 'NivSup'
      Origin = 'praga_z.NivSup'
    end
    object QrPraga_ZNO_NIVSUP: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_NIVSUP'
      Origin = 'praga_a.Nome'
      Size = 60
    end
    object QrPraga_ZOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'praga_z.Ordem'
    end
    object QrPraga_ZEspecie: TWideStringField
      FieldName = 'Especie'
      Origin = 'praga_z.Especie'
      Size = 60
    end
    object QrPraga_ZObservacao: TWideMemoField
      FieldName = 'Observacao'
      Origin = 'praga_z.Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPraga_ZPrevencao: TWideMemoField
      FieldName = 'Prevencao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPraga_ZArqDir: TWideStringField
      FieldName = 'ArqDir'
      Origin = 'praga_z.ArqDir'
      Size = 60
    end
    object QrPraga_ZCopyright: TWideStringField
      FieldName = 'Copyright'
      Origin = 'praga_z.Copyright'
      Size = 60
    end
    object QrPraga_ZArqImg: TWideStringField
      FieldName = 'ArqImg'
      Origin = 'praga_z.ArqImg'
      Size = 60
    end
  end
  object DsPraga_Z: TDataSource
    DataSet = QrPraga_Z
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    Left = 120
    Top = 64
  end
  object QrPraga_A: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPraga_ZBeforeOpen
    AfterOpen = QrPraga_ZAfterOpen
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM praga_a'
      'ORDER BY Nome'
      '')
    Left = 148
    Top = 64
    object QrPraga_ACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPraga_ANome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsPraga_A: TDataSource
    DataSet = QrPraga_A
    Left = 176
    Top = 64
  end
  object QrPraga_ZTax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pzt.Codigo, pzt.TaxonIts,pzt.Nome, txi.Nome NO_TAXON_ITS'
      'FROM praga_ztax pzt'
      'LEFT JOIN taxonits txi ON txi.Codigo=pzt.TaxonIts'
      'WHERE pzt.Codigo=:P0'
      'ORDER BY txi.Ordem')
    Left = 216
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPraga_ZTaxCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPraga_ZTaxTaxonIts: TIntegerField
      FieldName = 'TaxonIts'
    end
    object QrPraga_ZTaxNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrPraga_ZTaxNO_TAXON_ITS: TWideStringField
      FieldName = 'NO_TAXON_ITS'
      Size = 100
    end
  end
  object DsPraga_ZTax: TDataSource
    DataSet = QrPraga_ZTax
    Left = 244
    Top = 264
  end
end
