unit Formulas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, dmkCheckGroup, dmkDBLookupComboBox, dmkEditCB, dmkValUsu,
  UnDmkEnums, Variants, Vcl.ComCtrls;

type
  TFmFormulas = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEditaA: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrFormulas: TmySQLQuery;
    DsFormulas: TDataSource;
    QrFormulIF: TmySQLQuery;
    DsFormulIF: TDataSource;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrFormulasNO_EQUIAPLIC: TWideStringField;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasLk: TIntegerField;
    QrFormulasDataCad: TDateField;
    QrFormulasDataAlt: TDateField;
    QrFormulasUserCad: TIntegerField;
    QrFormulasUserAlt: TIntegerField;
    QrFormulasAlterWeb: TSmallintField;
    QrFormulasAtivo: TSmallintField;
    QrFormulasEquipAplic: TIntegerField;
    QrFormulasQtdTot: TFloatField;
    QrFormulasQtdQSP: TFloatField;
    QrFormulasCusTot: TFloatField;
    BtAbr: TBitBtn;
    QrFormulIA: TmySQLQuery;
    DsFormulIA: TDataSource;
    PnGrades: TPanel;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    GroupBox17: TGroupBox;
    DBGOSFrmAbr: TDBGrid;
    CGAplicacao: TdmkCheckGroup;
    QrFormulasAplicacao: TIntegerField;
    Label6: TLabel;
    EdQtdTot: TdmkEdit;
    Label3: TLabel;
    EdEquipAplic: TdmkEditCB;
    CBEquipAplic: TdmkDBLookupComboBox;
    SBEquip: TSpeedButton;
    QrEquipamentos: TmySQLQuery;
    QrEquipamentosControle: TIntegerField;
    QrEquipamentosNome: TWideStringField;
    DsEquipamentos: TDataSource;
    PMEquip: TPopupMenu;
    Aplicao1: TMenuItem;
    Monitoramento1: TMenuItem;
    RGDiluente: TdmkRadioGroup;
    QrFormulasDiluente: TIntegerField;
    Label8: TLabel;
    EdTipoAplica: TdmkEditCB;
    CBTipoAplica: TdmkDBLookupComboBox;
    SBTipoAplica: TSpeedButton;
    QrTipoAplica: TmySQLQuery;
    DsTipoAplica: TDataSource;
    QrTipoAplicaCodigo: TIntegerField;
    QrTipoAplicaNome: TWideStringField;
    QrFormulasNO_TipoAplica: TWideStringField;
    QrFormulasTipoAplica: TIntegerField;
    Label11: TLabel;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    dmkValUsu1: TdmkValUsu;
    QrFormulasUnidMed: TIntegerField;
    QrFormulasSIGLA: TWideStringField;
    QrFormulasNO_UNIDMED: TWideStringField;
    QrFormulasCU_UNIDMED: TIntegerField;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label10: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    dmkCheckGroup1: TdmkDBCheckGroup;
    BtFormulFiCb: TBitBtn;
    PMFormulFiCb: TPopupMenu;
    Panel8: TPanel;
    QrFormulFiCb: TmySQLQuery;
    DsFormulFiCb: TDataSource;
    QrFormulFiCbCodigo: TIntegerField;
    QrFormulFiCbControle: TIntegerField;
    QrFormulFiCbFormula: TIntegerField;
    QrFormulFiCbPeriodd: TIntegerField;
    QrFormulFiCbLk: TIntegerField;
    QrFormulFiCbDataCad: TDateField;
    QrFormulFiCbDataAlt: TDateField;
    QrFormulFiCbUserCad: TIntegerField;
    QrFormulFiCbUserAlt: TIntegerField;
    QrFormulFiCbAlterWeb: TSmallintField;
    QrFormulFiCbAtivo: TSmallintField;
    QrFormulFiCbNO_FORMULA: TWideStringField;
    Incluifrmulafilha1: TMenuItem;
    Alterafrmulafilha1: TMenuItem;
    Excluifrmulafilha1: TMenuItem;
    QrFormulFiDd: TmySQLQuery;
    DsFormulFiDd: TDataSource;
    QrFormulFiDdCodigo: TIntegerField;
    QrFormulFiDdControle: TIntegerField;
    QrFormulFiDdConta: TIntegerField;
    QrFormulFiDdOrdem: TIntegerField;
    QrFormulFiDdDias: TIntegerField;
    QrFormulFiDdLk: TIntegerField;
    QrFormulFiDdDataCad: TDateField;
    QrFormulFiDdDataAlt: TDateField;
    QrFormulFiDdUserCad: TIntegerField;
    QrFormulFiDdUserAlt: TIntegerField;
    QrFormulFiDdAlterWeb: TSmallintField;
    QrFormulFiDdAtivo: TSmallintField;
    BtFormulFiDd: TBitBtn;
    PMFormulFiDd: TPopupMenu;
    Incluiintervalo1: TMenuItem;
    Alteraintervalo1: TMenuItem;
    Excluiintervalo1: TMenuItem;
    QrFormulFiCbDdPostero: TIntegerField;
    QrEquipamentosNivel1: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    TabSheet2: TTabSheet;
    Panel47: TPanel;
    BtFormulEPI: TBitBtn;
    DBGFormulEPI: TDBGrid;
    PMFormulEPI: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrFormulEPI: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    DsFormulEPI: TDataSource;
    N1: TMenuItem;
    Cadatrodeprodutos1: TMenuItem;
    QrFormulEPIGraGruX: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFormulasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFormulasBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrFormulasAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure BtAbrClick(Sender: TObject);
    procedure SBEquipClick(Sender: TObject);
    procedure Aplicao1Click(Sender: TObject);
    procedure Monitoramento1Click(Sender: TObject);
    procedure SBTipoAplicaClick(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure QrFormulasBeforeClose(DataSet: TDataSet);
    procedure Excluifrmulafilha1Click(Sender: TObject);
    procedure Incluifrmulafilha1Click(Sender: TObject);
    procedure Alterafrmulafilha1Click(Sender: TObject);
    procedure QrFormulFiCbBeforeClose(DataSet: TDataSet);
    procedure QrFormulFiCbAfterScroll(DataSet: TDataSet);
    procedure BtFormulFiCbClick(Sender: TObject);
    procedure PMFormulFiCbPopup(Sender: TObject);
    procedure BtFormulFiDdClick(Sender: TObject);
    procedure Excluiintervalo1Click(Sender: TObject);
    procedure Incluiintervalo1Click(Sender: TObject);
    procedure Alteraintervalo1Click(Sender: TObject);
    procedure PMFormulFiDdPopup(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtFormulEPIClick(Sender: TObject);
    procedure PMFormulEPIPopup(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Cadatrodeprodutos1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormulIF();
    procedure MostraFormulIA();
    procedure MostraFormulFiCb(SQLTYpe: TSQLType);
    procedure MostraFormulFiDd(SQLTYpe: TSQLType);
    procedure AdicionaEPIs();
    procedure ReopenFormulEPI(Controle: Integer);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    FQrOSFrmRec: TmySQLQuery;
    FConta: Integer;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure ReopenFormulIF(Controle: Integer);
    procedure ReopenFormulIA(Controle: Integer);
    procedure ReopenFormulFiCb(Controle: Integer);
    procedure ReopenFormulFiDd(Conta: Integer);
    //
    function  ProximaOrdem(): Integer;
  end;

var
  FmFormulas: TFmFormulas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, FormulIF, FormulIA, DmkDAC_PF, Principal,
UnBugs_Tabs, ModProd, UnOSApp_PF, FormulFiCb, FormulFiDd, UnGrade_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFormulas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFormulas.Monitoramento1Click(Sender: TObject);
begin
  FmPrincipal.MostraFormGraG1Equi(gbsMonitora, 0);
  UMyMod.SetaCodigoPesquisado(EdEquipAplic, CBEquipAplic, QrEquipamentos,
    VAR_CADASTRO, 'Controle');
end;

procedure TFmFormulas.MostraFormulIA();
begin
  if DBCheck.CriaFm(TFmFormulIA, FmFormulIA, afmoNegarComAviso) then
  begin
    FmFormulIA.ReopenFormulas();
    //
    FmFormulIA.ShowModal;
    FmFormulIA.Destroy;
    //
    ReopenFormulIA(0);
  end;
end;

procedure TFmFormulas.MostraFormulFiCb(SQLTYpe: TSQLType);
begin
  if DBCheck.CriaFm(TFmFormulFiCb, FmFormulFiCb, afmoNegarComAviso) then
  begin
    FmFormulFiCb.ImgTipo.SQLType := SQLType;
    FmFormulFiCb.FQrCab := QrFormulas;
    FmFormulFiCb.FDsCab := DsFormulas;
    FmFormulFiCb.FQrIts := QrFormulFiCb;
    if SQLType = stIns then
    begin
      //
    end else
    begin
      FmFormulFiCb.EdControle.ValueVariant  := QrFormulFiCbControle.Value;
      //
      FmFormulFiCb.EdFormula.ValueVariant   := QrFormulFiCbFormula.Value;
      FmFormulFiCb.CBFormula.KeyValue       := QrFormulFiCbFormula.Value;
      FmFormulFiCb.EdPerioDd.ValueVariant   := QrFormulFiCbPerioDd.Value;
      FmFormulFiCb.EdDdPostero.ValueVariant := QrFormulFiCbDdPostero.Value;
    end;
    FmFormulFiCb.ShowModal;
    FmFormulFiCb.Destroy;
  end;
end;

procedure TFmFormulas.MostraFormulFiDd(SQLTYpe: TSQLType);
begin
  if DBCheck.CriaFm(TFmFormulFiDd, FmFormulFiDd, afmoNegarComAviso) then
  begin
    FmFormulFiDd.ImgTipo.SQLType := SQLType;
    FmFormulFiDd.FQrCab := QrFormulFiCb;
    FmFormulFiDd.FDsCab := DsFormulFiCb;
    FmFormulFiDd.FQrIts := QrFormulFiDd;
    if SQLType = stIns then
    begin
      FmFormulFiDd.EdOrdem.ValueVariant := ProximaOrdem();
    end else
    begin
      FmFormulFiDd.EdConta.ValueVariant := QrFormulFiDdConta.Value;
      //
      FmFormulFiDd.EdOrdem.ValueVariant := QrFormulFiDdOrdem.Value;
      FmFormulFiDd.EdDias.ValueVariant  := QrFormulFiDdDias.Value;
    end;
    FmFormulFiDd.ShowModal;
    FmFormulFiDd.Destroy;
  end;
end;

procedure TFmFormulas.MostraFormulIF();
begin
  if DBCheck.CriaFm(TFmFormulIF, FmFormulIF, afmoNegarComAviso) then
  begin
    FmFormulIF.FQrOSFrmRec := FQrOSFrmRec;
    FmFormulIF.FConta := FConta;
    //
    FmFormulIF.ReopenFormulas();
    //
    FmFormulIF.ShowModal;
    FmFormulIF.Destroy;
    //
    ReopenFormulIF(0);
  end;
end;

procedure TFmFormulas.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrFormulas);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrFormulas, QrFormulIF);
end;

procedure TFmFormulas.PMFormulFiCbPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluifrmulafilha1, QrFormulas);
  MyObjects.HabilitaMenuItemItsUpd(Alterafrmulafilha1, QrFormulFiCb);
  MyObjects.HabilitaMenuItemCabDel(Excluifrmulafilha1, QrFormulFiCb, QrFormulFiDd);
end;

procedure TFmFormulas.PMFormulFiDdPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluiintervalo1, QrFormulFiCb);
  MyObjects.HabilitaMenuItemItsUpd(Alteraintervalo1, QrFormulFiDd);
  MyObjects.HabilitaMenuItemItsDel(Excluiintervalo1, QrFormulFiDd);
end;

procedure TFmFormulas.PMFormulEPIPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Inclui1, QrFormulas);
  MyObjects.HabilitaMenuItemItsDel(Exclui1, QrFormulEPI);
  MyObjects.HabilitaMenuItemItsIns(Cadatrodeprodutos1, QrFormulas);
end;

function TFmFormulas.ProximaOrdem(): Integer;
const
  Incremento = 1;
  Base       = 0;
var
  Codigo, Controle: Integer;
begin
  Codigo   := QrFormulasCodigo.Value;
  Controle := QrFormulFiCbControle.Value;
  //
  Result := UnDmkDAC_PF.ObtemProximaOrdem(Dmod.MyDB, 'formulfidd', 'Ordem',
              vpLast, dmktfInteger, Incremento, Base, ['Codigo', 'Controle'],
              [Codigo, Controle])
end;

procedure TFmFormulas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFormulasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFormulas.DefParams;
begin
  VAR_GOTOTABELA := 'formulas';
  VAR_GOTOMYSQLTABLE := QrFormulas;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT tpa.Nome NO_TipoAplica,');
  VAR_SQLx.Add('gg1.Nome NO_EQUIAPLIC, med.CodUsu CU_UNIDMED, ');
  VAR_SQLx.Add('med.SIGLA, med.Nome NO_UNIDMED, frm.* ');
  VAR_SQLx.Add('FROM formulas frm');
  VAR_SQLx.Add('LEFT JOIN tipoaplica tpa ON tpa.Codigo = frm.TipoAplica');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle = frm.EquipAplic');
  VAR_SQLx.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN unidmed med ON med.Codigo=frm.UnidMed');
  VAR_SQLx.Add('WHERE frm.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND frm.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND frm.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND frm.Nome Like :P0');
  //
end;

procedure TFmFormulas.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmFormulas.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmFormulas.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmFormulas.Exclui1Click(Sender: TObject);
begin
  if DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFormulEPI, DBGFormulEPI,
    'formulepi', ['Controle'], ['Controle'], istPergunta, '') = ID_YES then
  begin
    ReopenFormulEPI(0);
  end;
end;

procedure TFmFormulas.Excluifrmulafilha1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da f�rmula filha selecionada?',
  'formulficb', 'Controle', QrFormulFiCbControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFormulFiCb,
      QrFormulFiCbControle, QrFormulFiCbControle.Value);
    ReopenFormulFiCb(Controle);
  end;
end;

procedure TFmFormulas.Excluiintervalo1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do intervalo selecionado?',
  'formulfidd', 'Conta', QrFormulFiDdConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrFormulFiDd,
      QrFormulFiDdConta, QrFormulFiDdConta.Value);
    ReopenFormulFiDd(Conta);
  end;
end;

procedure TFmFormulas.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + #13#10 +
  Caption + #13#10 + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmFormulas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFormulas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFormulas.ReopenFormulEPI(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulEPI, Dmod.MyDB, [
    'SELECT epi.Controle, ggx.Controle GraGruX, ',
    'gg1.GraTabApp, gg1.Nivel1, gg1.Nome ',
    'FROM formulepi epi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle = epi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE epi.Codigo=' + Geral.FF0(QrFormulasCodigo.Value),
    'AND gg1.GraTabApp = 0 ',
    'AND gg1.Ativo = 1 ',
    'ORDER BY gg1.Nome ',
    '']);
  //
  if Controle <> 0 then
    QrFormulEPI.Locate('Controle', Controle, []);
end;

procedure TFmFormulas.ReopenFormulFiCb(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulFiCB, Dmod.MyDB, [
  'SELECT ffc.*, frm.Nome NO_FORMULA ',
  'FROM formulficb ffc ',
  'LEFT JOIN formulas frm ON frm.Codigo=ffc.Formula ',
  'WHERE ffc.Codigo=' + Geral.FF0(QrFormulasCodigo.Value),
  '']);
  //
  if Controle <> 0 then
    QrFormulFiCb.Locate('Controle', Controle, []);
end;

procedure TFmFormulas.ReopenFormulIA(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulIA, Dmod.MyDB, [
  'SELECT abr.Nome NO_ABRANGE, ofa.* ',
  'FROM formulia ofa ',
  'LEFT JOIN abrangicie abr ON abr.Codigo=ofa.Abrangicie ',
  'WHERE ofa.Codigo=' + Geral.FF0(QrFormulasCodigo.Value),
  '']);
  //
  if Controle <> 0 then
    QrFormulIA.Locate('Controle', Controle, []);
end;

procedure TFmFormulas.ReopenFormulFiDd(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulFiDd, Dmod.MyDB, [
  'SELECT ffd.* ',
  'FROM formulfidd ffd ',
  'WHERE ffd.Controle=' + Geral.FF0(QrFormulFiCbControle.Value),
  'ORDER BY Ordem, Controle ',
  '']);
  //
  if Conta <> 0 then
    QrFormulFiDd.Locate('Conta', Conta, []);
end;

procedure TFmFormulas.ReopenFormulIF(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulIF, Dmod.MyDB, [
{
  'SELECT gg1.Nome NO_GG1, ofr.*',
  'FROM formulif ofr',
  'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX',
  'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1',
  'WHERE ofr.Codigo=' + Geral.FF0(QrFormulasCodigo.Value),
  'ORDER BY Ordem',
}
  'SELECT med.Sigla SIGLAUNIDMED, gg1.Nome NO_GG1, ofr.* ',
  'FROM formulif ofr ',
  'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX ',
  'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed med on med.Codigo=gg1.UnidMed ',
  'WHERE ofr.Codigo=' + Geral.FF0(QrFormulasCodigo.Value),
  'ORDER BY Ordem ',
  '']);
  //
  if Controle <> 0 then
    QrFormulIF.Locate('Controle', Controle, []);
end;


procedure TFmFormulas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFormulas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFormulas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFormulas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFormulas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFormulas.SpeedButton5Click(Sender: TObject);
var
  UnidMed: Integer;
begin
  VAR_CADASTRO := 0;
  UnidMed      := EdUnidMed.ValueVariant;

  Grade_Jan.MostraFormUnidMed(UnidMed);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO, 'Codigo', 'CodUsu');
    EdUnidMed.SetFocus;
  end;
end;

procedure TFmFormulas.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFormulasCodigo.Value;
  Close;
end;

procedure TFmFormulas.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFormulas, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'formulas');
end;

procedure TFmFormulas.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(RGDiluente.ItemIndex = 0, EdNome, 'Defina o diluente!') then Exit;
  //
  Codigo := EdCodigo.ValueVariant;
  Codigo := UMyMod.BPGS1I32('formulas', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'formulas',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmFormulas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'formulas', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'formulas', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFormulas.BtFormulFiCbClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFormulFiCb, BtFormulFiCb);
end;

procedure TFmFormulas.BtFormulFiDdClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFormulFiDd, BtFormulFiDd);
end;

procedure TFmFormulas.BtItsClick(Sender: TObject);
begin
  MostraFormulIF();
end;

procedure TFmFormulas.BtFormulEPIClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFormulEPI, BtFormulEPI);
end;

procedure TFmFormulas.AdicionaEPIs;
var
  Codigo, Controle: Integer;
  GraGruX: Variant;
begin
  Codigo  := QrFormulasCodigo.Value;
  GraGruX := DBCheck.EscolheCodigoUnico('...',
              'F�rmulas Bases',
              'Informe o produto: [F7 para pesquisar]', nil, nil, 'Descricao', 0, [
              'SELECT ggx.Controle Codigo, gg1.Nome Descricao',
              'FROM gragrux ggx ',
              'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
              'WHERE gg1.GraTabApp = 0 ',
              'AND gg1.Ativo = 1 ',
              'AND ggx.Controle NOT IN ',
              '( ',
              'SELECT GraGruX ',
              'FROM formulepi ',
              'WHERE Codigo=' + Geral.FF0(Codigo),
              ') ',
              'ORDER BY gg1.Nome ',
              ''], Dmod.MyDB, True);
  //
  if (GraGruX <> Null) and (Codigo <> 0) then
  begin
    Controle := UMyMod.BPGS1I32('formulepi', 'Controle', '', '', tsPos, stIns, 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'formulepi', False,
      ['GraGrux', 'Codigo'], ['Controle'],
      [GraGruX, Codigo], [Controle], True) then
    begin
      ReopenFormulEPI(Controle);
    end;
  end;
end;

procedure TFmFormulas.Alterafrmulafilha1Click(Sender: TObject);
begin
  MostraFormulFiCb(stUpd);
end;

procedure TFmFormulas.Alteraintervalo1Click(Sender: TObject);
begin
  MostraFormulFiDd(stUpd);
end;

procedure TFmFormulas.Aplicao1Click(Sender: TObject);
begin
  FmPrincipal.MostraFormGraG1Equi(gbsAplica, 0);
  UMyMod.SetaCodigoPesquisado(EdEquipAplic, CBEquipAplic, QrEquipamentos,
    VAR_CADASTRO, 'Controle');
end;

procedure TFmFormulas.BtAbrClick(Sender: TObject);
begin
  MostraFormulIA();
end;

procedure TFmFormulas.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmFormulas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnGrades.Align  := alClient;
  CriaOForm;
  FSeq := 0;
  OSApp_PF.ReopenEquipAplic(QrEquipamentos, gbsAplEMon);
  UnDmkDAC_PF.AbreQuery(QrTipoAplica, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  //
  PageControl1.ActivePageIndex := 0;
  DBGFormulEPI.PopupMenu       := PMFormulEPI;
end;

procedure TFmFormulas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFormulasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFormulas.SBEquipClick(Sender: TObject);
var
  EquipAplic, Nivel1: Integer;
begin
  EquipAplic   := EdEquipAplic.ValueVariant;
  VAR_CADASTRO := 0;

  if EquipAplic <> 0 then  
    Nivel1 := QrEquipamentosNivel1.Value
  else
    Nivel1 := 0;

  Grade_Jan.MostraFormGraGruN(Nivel1);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEquipAplic, CBEquipAplic, QrEquipamentos, VAR_CADASTRO, 'Controle');
    EdEquipAplic.SetFocus;
  end;
  //MyObjects.MostraPopUpDeBotao(PMEquip, SBEquip);
end;

procedure TFmFormulas.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmFormulas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFormulas.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrFormulasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFormulas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFormulas.QrFormulasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFormulas.QrFormulasAfterScroll(DataSet: TDataSet);
begin
  ReopenFormulIF(0);
  ReopenFormulIA(0);
  ReopenFormulFiCb(0);
  ReopenFormulEPI(0);
end;

procedure TFmFormulas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrFormulasCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmFormulas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFormulasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'formulas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFormulas.SBTipoAplicaClick(Sender: TObject);
var
  TipoAplica: Integer;
begin
  VAR_CADASTRO := 0;
  TipoAplica   := EdTipoAplica.ValueVariant;

  FmPrincipal.MostraFormTipoAplica(TipoAplica);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTipoAplica, CBTipoAplica, QrTipoAplica, VAR_CADASTRO);
    EdTipoAplica.SetFocus;
  end;
end;

procedure TFmFormulas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulas.Inclui1Click(Sender: TObject);
begin
  AdicionaEPIs();
end;

procedure TFmFormulas.Incluifrmulafilha1Click(Sender: TObject);
begin
  MostraFormulFiCb(stIns);
end;

procedure TFmFormulas.Incluiintervalo1Click(Sender: TObject);
begin
  MostraFormulFiDd(stIns);
end;

procedure TFmFormulas.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFormulas, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'formulas');
end;

procedure TFmFormulas.Cadatrodeprodutos1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruN(0);
end;

procedure TFmFormulas.QrFormulasBeforeClose(DataSet: TDataSet);
begin
  QrFormulIF.Close;
  QrFormulIA.Close;
  QrFormulFiCb.Close;
  QrFormulEPI.Close;
end;

procedure TFmFormulas.QrFormulasBeforeOpen(DataSet: TDataSet);
begin
  QrFormulasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFormulas.QrFormulFiCbAfterScroll(DataSet: TDataSet);
begin
  ReopenFormulFiDd(0);
end;

procedure TFmFormulas.QrFormulFiCbBeforeClose(DataSet: TDataSet);
begin
  QrFormulFiDd.Close;
end;

end.

