unit UnAppPF;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, SHDocVw,
  UnInternalConsts2, ComCtrls, dmkGeral,
  mySQLDBTables, UnDmkEnums;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function AcaoEspecificaDeApp(Servico: String; CliInt, EntCliInt: Integer;
               Query: TmySQLQuery = nil; Query2: TmySQLQuery = nil): Boolean;
    //
    procedure Html_ConfigarImagem(WebBrowser: TWebBrowser);
    procedure Html_ConfigarUrl(WebBrowser: TWebBrowser);
    procedure Html_ConfigarVideo(WebBrowser: TWebBrowser);
    //
    procedure CadastroMateriaPrima(GraGruX: Integer; NewNome: String);
    procedure CadastroProdutoAcabado(GraGruX: Integer; NewNome: String);
    procedure CadastroServicoNFe(GraGruX: Integer; NewNome: String);
    procedure CadastroSubProduto(GraGruX: Integer; NewNome: String);
    procedure CadastroUsoEConsumo(GraGruX: Integer; NewNome: String);
    procedure CadastroUsoPermanente(GraGruX: Integer; NewNome: String);
    procedure CadastroInsumo(GraGruX: Integer; NewNome: String);
    //
    // C O R R E � � E S
    procedure AtualizaTextosGenericos();
    procedure AtualizaRespTec();
    procedure AtualizaDadosWeb();
    procedure AtualizaRelatiosEspecificos();
    (* J� executada
    procedure AgendarOSsJaCriadas();
    procedure AtualizarOSCabAgeEqiCab();
    procedure AtualizarOSMulServico();
    procedure AtualizarOSGruposEOpcoes();
    *)
  end;

var
  AppPF: TUnAppPF;

implementation

uses UMySQLModule, UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral,
  UnBloqGerl_Jan;

{ TUnAppPF }

function TUnAppPF.AcaoEspecificaDeApp(Servico: String; CliInt,
  EntCliInt: Integer; Query, Query2: TmySQLQuery): Boolean;
begin
  if (Servico = 'ArreFut') and (Query <> nil) and (Query2 <> nil) then
  begin
    UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, CliInt, EntCliInt, nil, Query, Query2);
    //
    Result := True;
  end else
  if (Servico = 'LctGer2') and (Query <> nil) and (Query2 = nil) then
  begin
    UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, CliInt, EntCliInt, nil, Query, nil);
    //
    Result := True;
  end else
    Result := True;
end;

procedure TUnAppPF.CadastroInsumo(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroUsoPermanente(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroMateriaPrima(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroProdutoAcabado(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroServicoNFe(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroSubProduto(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroUsoEConsumo(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarImagem(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarUrl(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarVideo(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.AtualizaTextosGenericos();
var
  TxtFAES: String;
begin
  TxtFAES := 'Declaro que recebi a rela��o de medidas preventivas antes e ap�s o servi�o, ' +
             'c�pia das Licen�as Sanit�ria e Ambiental vigentes, ' +
             'e que as embalagens dos desinfestantes domissanit�rios ' +
             'utilizados foram recolhidas pela empresa prestadora do servi�o ' +
             'de imuniza��o e controle de vetores e pragas urbanas.';
  //
  try
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'txtgeneric', False,
      ['Nome', 'Aplicacao', 'Texto'], ['Codigo'],
      ['Texto padr�o FAES', 16, TxtFAES], [-1], True);
  except
    ;
  end;
end;

procedure TUnAppPF.AtualizaDadosWeb;
begin
  //Dmod.qr
end;

procedure TUnAppPF.AtualizaRelatiosEspecificos();
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'UPDATE opcoesfili SET ',
      'ImgTitRelL = ',
      '( ',
      'SELECT ImgTitRelL ',
      'FROM opcoesgerl ',
      'LIMIT 1 ',
      '); ',
      'UPDATE opcoesfili SET ',
      'ImgTitRelR = ',
      '( ',
      'SELECT ImgTitRelR ',
      'FROM opcoesgerl ',
      'LIMIT 1 ',
      '); ',
      'UPDATE opcoesfili SET ',
      'TxtTitRelC = ',
      '( ',
      'SELECT TxtTitRelC ',
      'FROM opcoesgerl ',
      'LIMIT 1 ',
      '); ',
      '']);
  finally
    Qry.Free;
  end;
end;

procedure TUnAppPF.AtualizaRespTec();
var
  Entidade, Codigo, OpcoesFili: Integer;
  RespTecNome, RespTecDocu, RespTec1: String;
begin
  Dmod.ReopenOpcoesFiliPelaEmpresa(DmodG.QrFiliLogCodigo.Value);
  //
  if Dmod.QrOpcoesFili.RecordCount > 0 then
  begin
    OpcoesFili  := Dmod.QrOpcoesFiliCodigo.Value;
    RespTecNome := Dmod.QrOpcoesFiliRespTecNome.Value;
    RespTecDocu := Dmod.QrOpcoesFiliRespTecDocu.Value;
    RespTec1    := Dmod.QrOpcoesFiliRespTec1.Value;
    //
    if (OpcoesFili <> 0 ) and (RespTecNome <> '') and (RespTec1 <> '') then
    begin
      Entidade := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
      //
      if Entidade <> 0 then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False,
          ['Nome', 'Tipo', 'CodUsu', 'indIEDest'], ['Codigo'],
          [RespTecNome, 1, Entidade, 9], [Entidade], True) then
        begin
          Codigo := UMyMod.BuscaEmLivreY_Def('entiassina', 'Codigo', stIns, 0);
          //
          if Codigo <> 0 then
          begin
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entiassina', False,
              ['Entidade', 'Nome', 'Dados', 'Imagem'], ['Codigo'],
              [Entidade, RespTecNome, RespTecDocu, RespTec1], [Codigo], True) then
            begin
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoesfili', False,
                ['RespTec'], ['Codigo'], [Entidade], [OpcoesFili], True);
            end;
          end;
        end;
      end;
    end;
  end;
end;

(* J� executada
procedure TUnAppPF.AgendarOSsJaCriadas();
var
  QrOSs: TmySQLQuery;
  //
  Inicio, Termino: TDateTime;
  Nome, Notas: String;
  Cor, Empresa, Entidade, SiapTerCad, Codigo, FatoGeradr: Integer;
  Cption: Byte;
begin
  QrOSs := TmySQLQuery.Create(Dmod);
  GBAvisos1.Visible := True;
  try
    QrOSs.Close;
    QrOSs.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(QrOSs, Dmod.MyDB, [
    'SELECT * ',
    'FROM oscab ',
    'ORDER BY Codigo ',
    '']);
    //
    PB1.Position := 0;
    PB1.Max := QrOSs.RecordCount;
    QrOSs.First;
    while not QrOSs.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAvisoA1, LaAvisoA2, True,
      'Agendando OS ' + Geral.FF0(QrOSs.FieldByName('Codigo').AsInteger));
      Codigo      := QrOSs.FieldByName('Codigo'    ).AsInteger;
      Empresa     := QrOSs.FieldByName('Empresa'   ).AsInteger;
      Entidade    := QrOSs.FieldByName('Entidade'  ).AsInteger;
      SiapTerCad  := QrOSs.FieldByName('SiapTerCad').AsInteger;
      FatoGeradr  := QrOSs.FieldByName('FatoGeradr').AsInteger;
      // Vistoria
      Inicio  := QrOSs.FieldByName('DtaVisPrv').AsDateTime;
      Termino := QrOSs.FieldByName('FimVisPrv').AsDateTime;
      Nome    := '';
      Notas   := '';
      Cor     := 0;
      Cption  := 0;
      DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagVistoria, Codigo,
      Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
      Cor, Cption);
      // Execu��o
      Inicio  := QrOSs.FieldByName('DtaExePrv').AsDateTime;
      Termino := QrOSs.FieldByName('FimExePrv').AsDateTime;
      Nome    := '';
      Notas   := '';
      Cor     := 0;
      Cption  := 0;
      DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagExecucao, Codigo,
      Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
      Cor, Cption);
      //
      //DmModOS.AtualizaCadastroCunsIts(EntContrat, Entidade);
      //DmModOS.TotalValorOS(Codigo);
      //
      QrOSs.Next;
    end;
    GBAvisos1.Visible := False;
  finally
    QrOSs.Free;
  end;
end;
*)

(* J� executada
procedure TUnAppPF.AtualizarOSCabAgeEqiCab();
var
  Qry1: TmySQLQuery;
  Codigo, Nao: Integer;
begin
  Nao := 0;
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    GBAvisos1.Visible := True;
    MyObjects.Informa2(LaAvisoA1, LaAvisoA2, True,
    'Preparando para atualizar Equipes de Agentes');
    GBAvisos1.Visible := True;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM oscab ',
      //'WHERE AgeEqiCab=0 ',
      'ORDER BY Codigo ',
      '']);
      PB1.Position := 0;
      PB1.Max := Qry1.RecordCount;
      Qry1.First;
      while not Qry1.Eof do
      begin
        Codigo := Qry1.FieldByName('Codigo').AsInteger;
        MyObjects.Informa2EUpdPB(PB1, LaAvisoA1, LaAvisoA2, True,
        'Atualizando Equipe de Agentes da OS ' + Geral.FF0(Codigo));
        //
        if DmModOS.MD5_AtualizaCheckSumOSAge(Codigo) = 0 then
          Nao := Nao + 1;
        //
        Qry1.Next;
      end;
      if Nao > 0 then
        Geral.MB_Aviso(Geral.FF0(Nao) + ' OSs ficaram sem equipe de agentes definidos!');
    GBAvisos1.Visible := False;
  finally
    Qry1.Free;
  end;
end;
*)

(* J� executada
procedure TUnAppPF.AtualizarOSMulServico();
var
  Qry: TmySQLQuery;
  procedure ReabreQry();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM oscab ',
    'WHERE MulServico=0 ',
    'ORDER BY Codigo ',
    '']);
  end;
var
  Codigo: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    MyObjects.Informa2(LaAvisoA1, LaAvisoA2, True,
    'Preparando para atualizar Multiplos Servi�os');
    GBAvisos1.Visible := True;
    //
    ReabreQry();
    //
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      Codigo := Qry.FieldByName('Codigo').AsInteger;
      MyObjects.Informa2EUpdPB(PB1, LaAvisoA1, LaAvisoA2, True,
      'Atualizando Multiplos Servi�os da OS ' + Geral.FF0(Codigo));
      //
      OSApp_PF.TotalValorOS(Codigo);
      //
      Qry.Next;
    end;
    //
    ReabreQry();
    if Qry.RecordCount > 0 then
      Geral.MB_Aviso(Geral.FF0(Qry.RecordCount) + ' OSs ficaram sem defini��o de multi servi�os!');
  finally
    Qry.Free;
    GBAvisos1.Visible := False;
  end;
end;
*)

(* J� executada
procedure TUnAppPF.AtualizarOSGruposEOpcoes();
var
  QrOSs: TmySQLQuery;
  Codigo, Grupo, Numero, Opcao: Integer;
begin
  Codigo := 0;
  QrOSs  := TmySQLQuery.Create(Dmod);
  GBAvisos1.Visible := True;
  try
    QrOSs.Close;
    QrOSs.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(QrOSs, Dmod.MyDB, [
      'SELECT * ',
      'FROM oscab ',
      'WHERE Codigo <> 0 ',
      'AND (Grupo=0 ',
      'OR Numero=0',
      'OR Opcao=0)',
      'ORDER BY Codigo ',
      '']);
    //
    if QrOSs.RecordCount > 0 then
    begin
      // Criar registro na tabela gerlseq1 se ainda n�o existe!
      //Grupo :=  // desmarcado para n�o aparecer no "Messages" do compilador!
        UMyMod.BPGS1I32('oscab', 'Grupo', '', '', tsDef, stIns, 0);
      //
      PB1.Position := 0;
      PB1.Max := QrOSs.RecordCount;
      QrOSs.First;
      while not QrOSs.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAvisoA1, LaAvisoA2, True,
        'Corrigindo OS ' + Geral.FF0(QrOSs.FieldByName('Codigo').AsInteger));
        Codigo      := QrOSs.FieldByName('Codigo'    ).AsInteger;
        Grupo       := Codigo;
        Numero      := Codigo;
        Opcao       := 1; // 1=Sim
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
          'Grupo', 'Numero', 'Opcao'], [
          'Codigo'], [
          Grupo, Numero, Opcao], [
          Codigo], True);
        //
        QrOSs.Next;
      end;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'LOCK TABLES gerlseq1 WRITE; ',
        ' ',
        'UPDATE gerlseq1 ',
        'SET BigIntPos=' + Geral.FF0(Codigo),
        'WHERE Tabela = "oscab" ',
        'AND Campo = "Grupo" ',
        'AND BigIntPos<' + Geral.FF0(Codigo) + '; ',
        ' ',
        'UNLOCK TABLES; ',
        ' ',
        'SELECT *  ',
        'FROM gerlseq1 ',
        'WHERE Tabela = "oscab" ',
        'AND Campo = "Grupo"; ',
        ' ']);
    end;
    GBAvisos1.Visible := False;
  finally
    QrOSs.Free;
  end;
end;
*)

end.
