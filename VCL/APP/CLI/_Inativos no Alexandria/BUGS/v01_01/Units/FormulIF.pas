unit FormulIF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, UnBugs_Tabs, UnAppListas,
  UnDmkEnums, UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmFormulIF = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    GBEdita: TGroupBox;
    Panel6: TPanel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    Label13: TLabel;
    DBEdit7: TDBEdit;
    Label14: TLabel;
    DBEdit9: TDBEdit;
    Label15: TLabel;
    TbFormulIF: TmySQLTable;
    TbFormulIFNO_GGX: TWideStringField;
    DsOSFrmRec: TDataSource;
    DsGGxs: TDataSource;
    QrReordem: TmySQLQuery;
    QrFormulas: TmySQLQuery;
    QrFormulasNO_EQUIAPLIC: TWideStringField;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrFormulasLk: TIntegerField;
    QrFormulasDataCad: TDateField;
    QrFormulasDataAlt: TDateField;
    QrFormulasUserCad: TIntegerField;
    QrFormulasUserAlt: TIntegerField;
    QrFormulasAlterWeb: TSmallintField;
    QrFormulasAtivo: TSmallintField;
    QrFormulasEquipAplic: TIntegerField;
    QrFormulasQtdTot: TFloatField;
    QrFormulasQtdQSP: TFloatField;
    QrFormulasCusTot: TFloatField;
    DsFormulas: TDataSource;
    TbFormulIFCodigo: TIntegerField;
    TbFormulIFControle: TIntegerField;
    TbFormulIFGraGruX: TIntegerField;
    TbFormulIFPrvQtd: TFloatField;
    TbFormulIFPrvPrc: TFloatField;
    TbFormulIFPrvVal: TFloatField;
    TbFormulIFUsoQtd: TFloatField;
    TbFormulIFUsoPrc: TFloatField;
    TbFormulIFUsoVal: TFloatField;
    TbFormulIFUsoDec: TFloatField;
    TbFormulIFUsoTot: TFloatField;
    TbFormulIFOrdem: TIntegerField;
    TbFormulIFReordem: TIntegerField;
    TbFormulIFEhDiluente: TSmallintField;
    QrFormulasDiluente: TSmallintField;
    QrGGXs: TmySQLQuery;
    QrGGXsGraGru1: TIntegerField;
    QrGGXsControle: TIntegerField;
    QrGGXsNO_PRD_TAM_COR: TWideStringField;
    QrGGXsSIGLAUNIDMED: TWideStringField;
    QrGGXsCODUSUUNIDMED: TIntegerField;
    QrGGXsNOMEUNIDMED: TWideStringField;
    TbFormulIFSIGLAUNIDMED: TWideStringField;
    DBGrid1: TDBGrid;
    BtIts: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbFormulIFAfterDelete(DataSet: TDataSet);
    procedure TbFormulIFAfterPost(DataSet: TDataSet);
    procedure TbFormulIFBeforeInsert(DataSet: TDataSet);
    procedure TbFormulIFBeforePost(DataSet: TDataSet);
    procedure TbFormulIFNewRecord(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFormulasAfterScroll(DataSet: TDataSet);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtItsClick(Sender: TObject);
  private
    { Private declarations }
    //F_OS_Alv_: String;
    FReordenendo: Boolean;
    FOrdem: Integer;
    procedure ReordenaLinhas();
    procedure AtualizaTbFormulas();
    //procedure DuplicaRegistro();
    procedure Sair();
  public
    { Public declarations }
    FQrOSFrmRec: TmySQLQuery;
    FConta: Integer;
    //
    procedure ReopenFormulas();
  end;

  var
  FmFormulIF: TFmFormulIF;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral,
{$IfNDef SemGraGruN}GraGruN, {$EndIf} UnGrade_Jan, ModProd,
UnOSApp_PF, Principal, MyDBCheck, MeuDBUses, Formulas, MyVCLSkin;

{$R *.DFM}

procedure TFmFormulIF.AtualizaTbFormulas();
begin
  // Fazer?
end;

procedure TFmFormulIF.BtItsClick(Sender: TObject);
var
  GraGruX, Nivel1, Controle: Integer;
begin
  GraGruX  := TbFormulIFGraGruX.Value;
  Controle := TbFormulIFControle.Value;

  if GraGruX <> 0 then
    Nivel1 := DmProd.ObtemGraGru1DeGraGruX(GraGruX)
  else
    Nivel1 := 0;

  Grade_Jan.MostraFormGraGruN(Nivel1);
  //
  QrGGXs.Close;
  QrGGXs.Open;
  //
  ReopenFormulas();
end;

procedure TFmFormulIF.BtOKClick(Sender: TObject);
begin
  Sair();
end;

procedure TFmFormulIF.BtSaidaClick(Sender: TObject);
begin
  Sair();
end;

procedure TFmFormulIF.DBGrid1CellClick(Column: TColumn);
var
  EhDiluente, AcaoSobreDil: Byte;
  UsaDil: String;
  Continua: Boolean;
begin
  if Column.FieldName = CO_FldEhDil then
  begin
    if TbFormulIFEhDiluente.Value = 0 then
    begin
      if QrFormulasDiluente.Value <> CO_COD_BUGS_DILUENTE_003 then
      begin
        UsaDil := '"' + sListaDiluidoresBugs[CO_COD_BUGS_DILUENTE_003] + '"';
        AcaoSobreDil := MyObjects.SelRadioGroup('O Que Fazer?',
        'O tipo de diluente difere de ' + UsaDil +  '. O que fazer?',
        ['Manter do jeito que est�', 'Mudar para '+ UsaDil, 'Desistir'], 1);
        case AcaoSobreDil of
          0: Continua := True;
          1:
          begin
            Continua := True;
            OSApp_PF.DefineTemProdutoDiluente(gbsAplEMon, QrFormulas);
          end;
          else Continua := False;
        end;
        if not Continua then
          Exit;
      end;
      OSApp_PF.ZeraDiluente(gbsAplEMon, QrFormulasCodigo.Value);
      EhDiluente := 1;
    end else
      EhDiluente := 0;
    //
    TbFormulIF.Edit;
    TbFormulIFEhDiluente.Value := EhDiluente;
    TbFormulIF.Post;
    //
    TbFormulIF.Refresh;
  end;
end;

procedure TFmFormulIF.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = CO_FldEhDil then
    DBGrid1.Options := DBGrid1.Options - [dgEditing] else
    DBGrid1.Options := DBGrid1.Options + [dgEditing];
end;

procedure TFmFormulIF.DBGrid1ColExit(Sender: TObject);
begin
  DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmFormulIF.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = CO_FldEhDil then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbFormulIFEhDiluente.Value);
end;

procedure TFmFormulIF.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Ordem, Controle: Integer;
begin
  if key=VK_INSERT then
  begin
    if DBGrid1.ReadOnly = False then
    begin
      Ordem := TbFormulIFOrdem.Value-1;
      TbFormulIF.Insert;
      TbFormulIFOrdem.Value := Ordem;
      key := 0;
    end;
  end else
  if key=VK_F4 then
  begin
    {$IfNDef SemGraGruN}
    //if ImgTipo.SQLType in ([stIns, stUpd]) then
    begin
      VAR_CADASTRO := 0;
      //FmPrincipal.CadastroPQ(TbFormulIFProduto.Value);
      if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
      begin
        FmGraGruN.FEmSelecao := True;
        FmGraGruN.ShowModal;
        FmGraGruN.Destroy;
      end;
      if VAR_CADASTRO <> 0 then
      begin
        Screen.Cursor := crHourGlass;
        try
          QrGGXs.Close;
          QrGGXs.Open;
          //
          TbFormulIF.Edit;
          TbFormulIFGraGruX.Value := VAR_CADASTRO;
          TbFormulIF.Post;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
    {$EndIf}
  end else
  if key=VK_F7 then
  begin
    VAR_CADASTRO := 0;
    FmMeuDBUses.PesquisaNome('gragrux', '', '');
    if VAR_CADASTRO <> 0 then
    begin
      Controle := TbFormulIFControle.Value;
      TbFormulIF.Edit;
      TbFormulIFGraGruX.Value := VAR_CADASTRO;
      if Controle <> 0 then
      begin
        TbFormulIF.Post;
        //TbFormulIF.Locate('Controle', Controle, []);
      end;
    end;
  end;
end;

procedure TFmFormulIF.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulIF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ReordenaLinhas();
  if FQrOSFrmRec <> nil then
    OSApp_PF.ReopenOSFrmRec(FQrOSFrmRec, FConta, 0);
end;

procedure TFmFormulIF.FormCreate(Sender: TObject);
var
  FiltroNivel: String;
begin
  ImgTipo.SQLType := stLok;
  UMyMod.AbreQuery(QrFormulas, Dmod.MyDB);
  //UMyMod.AbreQuery(QrGGXs, Dmod.MyDB);
  OSApp_PF.FiltroGrade(gbsAplica, gbmProduto, FiltroNivel);
  UnDmkDAC_PF.AbreMySQLQuery0(QrGGXs, DMod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed',
  // 2013-08-03
  //'WHERE ' + FiltroNivel,
  // FIM 2013-08-03
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
end;

procedure TFmFormulIF.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulIF.QrFormulasAfterScroll(DataSet: TDataSet);
begin
  TbFormulIF.Close;
  TbFormulIF.Filter := 'Codigo=' + Geral.FF0(QrFormulasCodigo.Value);
  TbFormulIF.Filtered := True;
  TbFormulIF.Open;
end;

procedure TFmFormulIF.ReopenFormulas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT gg1.Nome NO_EQUIAPLIC, frm.* ',
  'FROM formulas frm ',
  'LEFT JOIN gragrux ggx ON ggx.Controle = frm.EquipAplic ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE frm.Codigo=' + Geral.FF0(FmFormulas.QrFormulasCodigo.Value),
  '']);
end;

procedure TFmFormulIF.ReordenaLinhas();
var
  i, Controle: Integer;
begin
  if FReordenendo then Exit;
  FReordenendo := True;
  Screen.Cursor := crHourGlass;
  TbFormulIF.DisableControls;
  Controle := TbFormulIFControle.Value;
  i := 0;
  TbFormulIF.First;
  while not TbFormulIF.Eof do
  begin
    inc(i, 1);
    TbFormulIF.Edit;
    TbFormulIFReordem.Value := i;
    TbFormulIF.Post;
    TbFormulIF.Next;
  end;
  QrReordem.Params[0].AsInteger := QrFormulasCodigo.Value;
  QrReordem.ExecSQL;
  TbFormulIF.Refresh;
  TbFormulIF.Locate('Controle', Controle, []);
  TbFormulIF.EnableControls;
  FReordenendo := False;
  Screen.Cursor := crDefault;
end;

procedure TFmFormulIF.Sair();
begin
  if (TbFormulIF.State <> dsInactive) and (TbFormulIF.RecordCount > 0) and
    (QrFormulasDiluente.Value = CO_COD_BUGS_DILUENTE_003) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM formulif ',
      'WHERE Codigo=' + Geral.FF0(QrFormulasCodigo.Value),
      'AND EhDiluente=1 ',
      '']);
    if Dmod.QrAux.Recordcount = 0 then
      Geral.MB_Aviso('Defina o produto diluente!')
    else
      Close;
  end else
    Close;
end;

procedure TFmFormulIF.TbFormulIFAfterDelete(DataSet: TDataSet);
begin
  if not FReordenendo then
    ReordenaLinhas;
  AtualizaTbFormulas;
end;

procedure TFmFormulIF.TbFormulIFAfterPost(DataSet: TDataSet);
begin
  if not FReordenendo then
    ReordenaLinhas;
  AtualizaTbFormulas;
end;

procedure TFmFormulIF.TbFormulIFBeforeInsert(DataSet: TDataSet);
begin
  FOrdem := TbFormulIFOrdem.Value;
end;

procedure TFmFormulIF.TbFormulIFBeforePost(DataSet: TDataSet);
{
var
  Atual: Double;
}
begin
  if TbFormulIFCodigo.Value < 0.1 then
  begin
    TbFormulIFCodigo.Value := QrFormulasCodigo.Value;
    TbFormulIFControle.Value :=   UMyMod.BPGS1I32(
      'formulif', 'Controle', '', '', tsDef, stIns, 0);
  end;
end;

procedure TFmFormulIF.TbFormulIFNewRecord(DataSet: TDataSet);
begin
  if FOrdem > 0 then
    TbFormulIFOrdem.Value := FOrdem
  else
    TbFormulIFOrdem.Value := 1;
end;

end.
