unit DesServico;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, ComCtrls, dmkRichEdit, UnDmkEnums;

type
  TFmDesServico = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrDesServico: TmySQLQuery;
    DsDesServico: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrDesServicoCodigo: TIntegerField;
    QrDesServicoNome: TWideStringField;
    QrDesServicoLk: TIntegerField;
    QrDesServicoDataCad: TDateField;
    QrDesServicoDataAlt: TDateField;
    QrDesServicoUserCad: TIntegerField;
    QrDesServicoUserAlt: TIntegerField;
    QrDesServicoAlterWeb: TSmallintField;
    QrDesServicoAtivo: TSmallintField;
    QrDesServicoSigla: TWideStringField;
    QrDesServicoObservacao: TWideMemoField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    ReObservacao: TdmkRichEdit;
    Label4: TLabel;
    EdSigla: TdmkEdit;
    BtTexto: TBitBtn;
    DBREObservacao: TDBRichEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDesServicoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDesServicoBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtTextoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmDesServico: TFmDesServico;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDesServico.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmDesServico.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrDesServicoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDesServico.DefParams;
begin
  VAR_GOTOTABELA := 'desservico';
  VAR_GOTOMYSQLTABLE := QrDesServico;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM desservico');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmDesServico.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmDesServico.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDesServico.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmDesServico.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDesServico.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDesServico.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDesServico.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDesServico.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDesServico.BtTextoClick(Sender: TObject);
begin
  MyObjects.EditaTextoRichEdit(REObservacao, 10, 'Tahoma', []);
end;

procedure TFmDesServico.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrDesServico, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'desservico');
end;

procedure TFmDesServico.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDesServicoCodigo.Value;
  Close;
end;

procedure TFmDesServico.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('desservico', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrDesServicoCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'desservico', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmDesServico.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'desservico', 'Codigo');
end;

procedure TFmDesServico.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrDesServico, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'desservico');
end;

procedure TFmDesServico.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  ReObservacao.Align := alClient;
  DBReObservacao.Align := alClient;
  CriaOForm;
end;

procedure TFmDesServico.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrDesServicoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDesServico.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmDesServico.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmDesServico.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrDesServicoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDesServico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDesServico.QrDesServicoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmDesServico.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDesServico.SbQueryClick(Sender: TObject);
begin
  LocCod(QrDesServicoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'desservico', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmDesServico.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDesServico.QrDesServicoBeforeOpen(DataSet: TDataSet);
begin
  QrDesServicoCodigo.DisplayFormat := FFormatFloat;
end;

end.

