unit RMIP_R016;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  mySQLDbTables, dmkGeral, UnDmkEnums, Vcl.ComCtrls, Vcl.StdCtrls,
  frxClass, frxDBSet, RSSVGCtrls, UnDmkImg;

type
  THackRSSVGImage = class(TRSSVGImage);
  TFmRMIP_R016 = class(TForm)
    frxReport016A: TfrxReport;
    Qr016SiapImaSVG: TmySQLQuery;
    Qr016SiapImaSVGCodigo: TIntegerField;
    Qr016SiapImaSVGControle: TIntegerField;
    Qr016SiapImaSVGNome: TWideStringField;
    Qr016SiapImaSVGNoArq: TWideStringField;
    Qr016SiapImaSVGSiapImaTer: TIntegerField;
    frxDs016SiapImaSVG: TfrxDBDataset;
    Qr016SiapImaSVGRotarGraus: TIntegerField;
    procedure frxReport016AGetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FInfo: String;
    FCroquis: array of TBitmap;
    FLegenda: TBitmap;
    //
    procedure CriaImagemSVGAtual(I: Integer);
    procedure Reopen016SiapImaSVG();
  public
    { Public declarations }
    FSiapImaSvg, FEmpresa, FCliente: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    FCLR, FDoLocalize: Boolean;
    FPB1: TProgressBar;
    FLaAviso1, FLaAviso2: TLabel;
    //
    procedure frxReport000GetValue(frxReport: TfrxReport; const VarName: string; var Value: Variant);
    function  GeraImp_CrroquiPMVsColoridos(): Boolean;
  end;

var
  FmRMIP_R016: TFmRMIP_R016;

implementation

{$R *.dfm}

uses Module, CunsCadSVG1, DmkDAC_PF, UnMyObjects, MyDBCheck;

{ TForm2 }

procedure TFmRMIP_R016.frxReport016AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport016A, VarName, Value);
end;

procedure TFmRMIP_R016.CriaImagemSVGAtual(I: Integer);
var
  Cliente, SiapTerCad, SiapImaCad, SiapImaSVG: Integer;
  Campo: String;
begin
  if (Qr016SiapImaSVG.State <> dsInactive) and (Qr016SiapImaSVG.Recordcount > 0) then
  begin
    if Qr016SiapImaSVGControle.Value > 0 then
    begin
      MyObjects.Informa2(FLaAviso1, FLaAviso2, True, FInfo + '. Item ' + Geral.FF0(
      Qr016SiapImaSVG.RecNo) + ' de ' + Geral.FF0(Qr016SiapImaSVG.RecordCount));
      //Campo := 'Teste' + Geral.FF0(I);
      //frxReport016A.Variables[Campo]   := I;
      //frxReport016A.Script.Variables[Campo] := I;
      if DBCheck.CriaFm(TFmCunsCadSVG1, FmCunsCadSVG1, afmoLiberado) then
      begin
        Cliente    := FCliente;
        SiapTerCad := Qr016SiapImaSVGSiapImaTer.Value;
        SiapImaCad := Qr016SiapImaSVGCodigo.Value;
        SiapImaSVG := Qr016SiapImaSVGControle.Value;
        //
        FmCunsCadSVG1.CarregaDadosCliente(Cliente, SiapTerCad, SiapImaCad, SiapImaSVG);
        //FmCunsCadSVG1.ShowModal;
        try
          FCroquis[I] := TBitmap.Create;
          // So desenha o croqui!
          //THAckRSSVGImage(FmCunsCadSVG1.RSSVGImgCroqui).Bitmap.SaveToBitmap(clWhite, FCroquis[I]);
          //PrintControl(FmCunsCadSVG1.SbCroqui,  FCroquis[I]);
        // Para usar o Control2Bitmap...
          FmCunsCadSVG1.WindowState := wsNormal;
          FmCunsCadSVG1.SBCroqui.Align := alNone;
          FmCunsCadSVG1.SBCroqui.Width  := FmCunsCadSVG1.RSSVGImgCroqui.Width;
          FmCunsCadSVG1.SBCroqui.Height := FmCunsCadSVG1.RSSVGImgCroqui.Height;
        // Fim Para usar o Control2Bitmap...
         //FCroquis[I] := Control2Bitmap(FmCunsCadSVG1.SbCroqui);
          if DmkImg.CapturaImagemDaInterfaceDeComponente(FmCunsCadSVG1.SbCroqui,
          fcicPaintTo, FCroquis[I]) then
          begin
            if Qr016SiapImaSVGRotarGraus.Value > 0 then
            begin
             FCroquis[I].PixelFormat := pf24bit;
             FCroquis[I] := DmkImg.RotateScanLine90(
               Qr016SiapImaSVGRotarGraus.Value * 90, FCroquis[I]);
            end;
          end;
          if I = 0 then
          begin
            FLegenda := TBitmap.Create;
            //
            FmCunsCadSVG1.SBLegenda.Align := alNone;
            FmCunsCadSVG1.SBLegenda.Width  := FmCunsCadSVG1.RSSVGImgLegenda.Width;
            FmCunsCadSVG1.SBLegenda.Height := FmCunsCadSVG1.RSSVGImgLegenda.Height;
            if DmkImg.CapturaImagemDaInterfaceDeComponente(FmCunsCadSVG1.SBLegenda,
            fcicPaintTo, FLegenda) then
            begin
             FLegenda.PixelFormat := pf24bit;
             FLegenda := DmkImg.RotateScanLine90(90, FLegenda);
            end;
          end;
        finally
          FmCunsCadSVG1.Destroy;
        end;
        //
      end;
    end //else Geral.MB_Aviso('N�o h� item para vizualizar!');
  end;
end;

procedure TFmRMIP_R016.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
var
  frxBmp: TfrxPictureView;
  Item: Integer;
begin
  if VarName = 'SVG_BMP' then
  begin
    Item := Qr016SiapImaSVG.RecNo - 1;
    frxBmp := frxReport.FindObject('Picture1') as TfrxPictureView;
    frxBmp.Picture.Bitmap.Assign(FCroquis[Item]);
    //
    frxBmp := frxReport.FindObject('Picture2') as TfrxPictureView;
    frxBmp.Picture.Bitmap.Assign(FLegenda);
    //
    Value := '';
  end;
//
end;

function TFmRMIP_R016.GeraImp_CrroquiPMVsColoridos(): Boolean;
var
  Aviso: String;
var
  frxMD: TfrxMasterData;
begin
  FInfo := FLaAviso1.Caption;
  Qr016SiapImaSVG.Close;
  Aviso := ' ';
  Reopen016SiapImaSVG();
  //
  if Qr016SiapImaSVG.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Cliente sem Croqui com PMVs coloridos!';
  end;
  //
  frxReport016A.Variables['VARF_AVISO']   := QuotedStr(Aviso);
  frxReport016A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport016A.Variables['VARF_DATA']    := FDtaImp;
  frxReport016A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport016A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  //
//begin
  //frxMD := frxReport016A.FindObject('MasterData1') as TfrxMasterData;
  //frxMD.RowCount := Qr016SiapImaSVG.RecordCount;
  MyObjects.frxDefineDataSets(frxReport016A, [
  frxDs016SiapImaSVG
  ]);
  //MyObjects.frxPrepara(frxReport016A, FAplic_Tit);
end;

procedure TFmRMIP_R016.Reopen016SiapImaSVG();
begin
  if FSiapImaSvg <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr016SiapImaSVG, Dmod.MyDB, [
    'SELECT svg.*, sic.SiapImaTer ',
    'FROM siapimasvg svg',
    'LEFT JOIN siapimacad sic ON sic.Codigo=svg.Codigo ',
    'WHERE svg.Codigo=' + Geral.FF0(FSiapImaSvg),
    '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr016SiapImaSVG, Dmod.MyDB, [
    'SELECT svg.*, sic.SiapImaTer ',
    'FROM siaptercad stc ',
    'LEFT JOIN siapimacad sic ON sic.SiapImaTer=stc.Codigo ',
    'LEFT JOIN siapimasvg svg ON svg.Codigo=sic.Codigo ',
    'WHERE stc.Cliente=' + Geral.FF0(FCliente),
    'AND NOT svg.Controle IS NULL ',
    '']);
  end;
  SetLength(FCroquis, Qr016SiapImaSVG.RecordCount);
  Qr016SiapImaSVG.First;
  while not Qr016SiapImaSVG.Eof do
  begin
    CriaImagemSVGAtual(Qr016SiapImaSVG.RecNo - 1);
    //
    Qr016SiapImaSVG.Next;
  end;
  Qr016SiapImaSVG.First;
end;

end.
