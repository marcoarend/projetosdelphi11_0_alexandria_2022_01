unit OSMonRec;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, UnBugs_Tabs, UnAppListas,
  UnDmkEnums, UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmOSMonRec = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    DsOSSrv: TDataSource;
    QrFormulas: TmySQLQuery;
    DsFormulas: TDataSource;
    GBEdita: TGroupBox;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrEquipAplic: TmySQLQuery;
    DsEquipAplic: TDataSource;
    Panel5: TPanel;
    Label7: TLabel;
    DBEdit15: TDBEdit;
    Label5: TLabel;
    DBEdit8: TDBEdit;
    Label4: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    Panel6: TPanel;
    QrOSMonCab: TmySQLQuery;
    QrOSMonCabCodigo: TIntegerField;
    QrOSMonCabControle: TIntegerField;
    QrOSMonCabConta: TIntegerField;
    QrOSMonCabNome: TWideStringField;
    QrOSMonCabFormula: TIntegerField;
    QrOSMonCabEquipAplic: TIntegerField;
    QrOSMonCabQtdTot: TFloatField;
    QrOSMonCabQtdQSP: TFloatField;
    QrOSMonCabCusTot: TFloatField;
    QrOSMonCabNO_FORMULA: TWideStringField;
    QrOSMonCabNO_EquipAplic: TWideStringField;
    DsOSMonCab: TDataSource;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label12: TLabel;
    DBEdit4: TDBEdit;
    Label13: TLabel;
    DBEdit7: TDBEdit;
    Label14: TLabel;
    DBEdit9: TDBEdit;
    Label15: TLabel;
    TbOSMonRec: TmySQLTable;
    TbOSMonRecNO_GGX: TWideStringField;
    DsOSMonRec: TDataSource;
    QrGGXs: TmySQLQuery;
    QrGGXsGraGru1: TIntegerField;
    QrGGXsControle: TIntegerField;
    QrGGXsNO_PRD_TAM_COR: TWideStringField;
    DsGGxs: TDataSource;
    QrReordem: TmySQLQuery;
    TbOSMonRecCodigo: TIntegerField;
    TbOSMonRecControle: TIntegerField;
    TbOSMonRecConta: TIntegerField;
    TbOSMonRecIDIts: TIntegerField;
    TbOSMonRecGraGruX: TIntegerField;
    TbOSMonRecPrvQtd: TFloatField;
    TbOSMonRecPrvPrc: TFloatField;
    TbOSMonRecPrvVal: TFloatField;
    TbOSMonRecUsoQtd: TFloatField;
    TbOSMonRecUsoPrc: TFloatField;
    TbOSMonRecUsoVal: TFloatField;
    TbOSMonRecUsoDec: TFloatField;
    TbOSMonRecUsoTot: TFloatField;
    TbOSMonRecOrdem: TIntegerField;
    TbOSMonRecReordem: TIntegerField;
    QrEquipAplicControle: TIntegerField;
    QrEquipAplicNome: TWideStringField;
    TbOSMonRecValCliDd: TIntegerField;
    QrG1Mon: TmySQLQuery;
    QrG1MonValCliDd: TIntegerField;
    TbOSMonRecEhDiluente: TSmallintField;
    TbOSMonRecSIGLAUNIDMED: TWideStringField;
    TbOSMonRecNumLote: TWideStringField;
    TbOSMonRecNumLaudo: TWideStringField;
    QrGGXsSIGLAUNIDMED: TWideStringField;
    QrGGXsCODUSUUNIDMED: TIntegerField;
    QrGGXsNOMEUNIDMED: TWideStringField;
    DBGrid1: TDBGrid;
    QrG1MonAtuNumLaud: TWideStringField;
    QrG1MonAtuNumLot: TWideStringField;
    TbOSMonRecGraGru1: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbOSMonRecAfterDelete(DataSet: TDataSet);
    procedure TbOSMonRecAfterPost(DataSet: TDataSet);
    procedure TbOSMonRecBeforeInsert(DataSet: TDataSet);
    procedure TbOSMonRecBeforePost(DataSet: TDataSet);
    procedure TbOSMonRecNewRecord(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOSMonCabAfterScroll(DataSet: TDataSet);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    //F_OS_Alv_: String;
    FReordenendo: Boolean;
    FOrdem: Integer;
    procedure ReordenaLinhas();
    procedure AtualizaTbFormulas();
    //procedure DuplicaRegistro();
    //
    procedure Sair();

  public
    { Public declarations }
    FQrOSMonRec, FQrOSMonCab: TmySQLQuery;
    FDiluente, FCodigo, FControle, FConta: Integer;
  end;

  var
  FmOSMonRec: TFmOSMonRec;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral,
{$IfNDef SemGraGruN}GraGruN, {$EndIf}
Principal, MyDBCheck, MeuDBUses, MyVCLSkin, UnOSApp_PF, UnGrade_Jan;

{$R *.DFM}

procedure TFmOSMonRec.AtualizaTbFormulas();
begin
  // Fazer?
end;

procedure TFmOSMonRec.BtOKClick(Sender: TObject);
begin
  Sair();
end;

procedure TFmOSMonRec.BtSaidaClick(Sender: TObject);
begin
  Sair();
end;

procedure TFmOSMonRec.DBGrid1CellClick(Column: TColumn);
var
  EhDiluente, AcaoSobreDil: Byte;
  UsaDil: String;
  Continua: Boolean;
begin
  if Column.FieldName = CO_FldEhDil then
  begin
    if TbOSMonRecEhDiluente.Value = 0 then
    begin
      if FDiluente <> CO_COD_BUGS_DILUENTE_003 then
      begin
        UsaDil := '"' + sListaDiluidoresBugs[CO_COD_BUGS_DILUENTE_003] + '"';
        AcaoSobreDil := MyObjects.SelRadioGroup('O Que Fazer?',
        'O tipo de diluente difere de ' + UsaDil +  '. O que fazer?',
        ['Manter do jeito que est�', 'Mudar para '+ UsaDil, 'Desistir'], 1);
        case AcaoSobreDil of
          0: Continua := True;
          1:
          begin
            Continua := True;
            OSApp_PF.DefineTemProdutoDiluente(gbsMonitora, FQrOSMonCab(*DmModOS.QrOSMonCab*));
          end;
          else Continua := False;
        end;
        if not Continua then
          Exit;
      end;
      OSApp_PF.ZeraDiluente(gbsMonitora, FConta);
      EhDiluente := 1;
    end else
      EhDiluente := 0;
    //
    TbOSMonRec.Edit;
    TbOSMonRecEhDiluente.Value := EhDiluente;
    TbOSMonRec.Post;
    //
    TbOSMonRec.Refresh;
  end;
end;

procedure TFmOSMonRec.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = CO_FldEhDil then
    DBGrid1.Options := DBGrid1.Options - [dgEditing] else
    DBGrid1.Options := DBGrid1.Options + [dgEditing];
end;

procedure TFmOSMonRec.DBGrid1ColExit(Sender: TObject);
begin
  DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmOSMonRec.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = CO_FldEhDil then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbOSMonRecEhDiluente.Value);
end;

procedure TFmOSMonRec.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Ordem, IDIts, Nivel1: Integer;
begin
  if key=VK_INSERT then
  begin
    if DBGrid1.ReadOnly = False then
    begin
      Ordem := TbOSMonRecOrdem.Value-1;
      TbOSMonRec.Insert;
      TbOSMonRecOrdem.Value := Ordem;
      key := 0;
    end;
  end else
  if key=VK_F4 then
  begin
    {$IfNDef SemGraGruN}
    (*
    //if ImgTipo.SQLType in ([stIns, stUpd]) then
    begin
      VAR_CADASTRO := 0;
      //FmPrincipal.CadastroPQ(TbOSMonRecProduto.Value);
      if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
      begin
        FmGraGruN.ShowModal;
        FmGraGruN.Destroy;
        // Parei Aqui
        // Como gerar o VAR_CADASTRO?
      end;
      if VAR_CADASTRO <> 0 then
      begin
        Screen.Cursor := crHourGlass;
        try
          QrGGXs.Close;
          QrGGXs.Open;
          //
          TbOSMonRec.Edit;
          TbOSMonRecGraGruX.Value := VAR_CADASTRO;
          TbOSMonRec.Post;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
    *)
    VAR_CADASTRO := 0;
    Nivel1       := TbOSMonRecGraGru1.Value;

    Grade_Jan.MostraFormGraGruN(Nivel1);

    if VAR_CADASTRO <> 0 then
    begin
      Screen.Cursor := crHourGlass;
      try
        QrGGXs.Close;
        QrGGXs.Open;
        //
        TbOSMonRec.Edit;
        TbOSMonRecGraGruX.Value := VAR_CADASTRO;
        TbOSMonRec.Post;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    {$EndIf}
  end else
  if key=VK_F7 then
  begin
    VAR_CADASTRO := 0;
    FmMeuDBUses.PesquisaNome('gragrux', '', '');
    if VAR_CADASTRO <> 0 then
    begin
      IDIts := TbOSMonRecIDIts.Value;
      TbOSMonRec.Edit;
      TbOSMonRecGraGruX.Value := VAR_CADASTRO;
      if IDIts <> 0 then
      begin
        TbOSMonRec.Post;
        //TbOSMonRec.Locate('IDIts', IDIts, []);
      end;
    end;
  end;
end;

{
procedure TFmOSMonRec.DuplicaRegistro();
begin
// Fazer?
end;
}

procedure TFmOSMonRec.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSMonRec.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ReordenaLinhas();
  OSApp_PF.ReopenOSMonCab(FQrOSMonCab, FControle, FConta);
end;

procedure TFmOSMonRec.FormCreate(Sender: TObject);
var
  FiltroNivel: String;
begin
  ImgTipo.SQLType := stLok;
  UMyMod.AbreQuery(QrFormulas, Dmod.MyDB);
  //
  OSApp_PF.FiltroGrade(gbsMonitora, gbmProduto, FiltroNivel);
  UnDmkDAC_PF.AbreMySQLQuery0(QrGGXs, DMod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed',
  // 2013-08-03
  //'WHERE ' + FiltroNivel,
  // FIM 2013-08-03
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
  OSApp_PF.ReopenEquipAplic(QrEquipAplic, gbsMonitora);
end;

procedure TFmOSMonRec.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSMonRec.QrOSMonCabAfterScroll(DataSet: TDataSet);
begin
  TbOSMonRec.Close;
  TbOSMonRec.Filter := 'Conta=' + Geral.FF0(QrOSMonCabConta.Value);
  TbOSMonRec.Filtered := True;
  TbOSMonRec.Open;
end;

procedure TFmOSMonRec.ReordenaLinhas();
var
  i, Item: Integer;
begin
  if FReordenendo then Exit;
  FReordenendo := True;
  Screen.Cursor := crHourGlass;
  TbOSMonRec.DisableControls;
  Item := TbOSMonRecIDIts.Value;
  i := 0;
  TbOSMonRec.First;
  while not TbOSMonRec.Eof do
  begin
    inc(i, 1);
    TbOSMonRec.Edit;
    TbOSMonRecReordem.Value := i;
    TbOSMonRec.Post;
    TbOSMonRec.Next;
  end;
  QrReordem.Params[0].AsInteger := QrOSMonCabConta.Value;
  QrReordem.ExecSQL;
  TbOSMonRec.Refresh;
  TbOSMonRec.Locate('IDIts', Item, []);
  TbOSMonRec.EnableControls;
  FReordenendo := False;
  Screen.Cursor := crDefault;
end;

procedure TFmOSMonRec.Sair();
begin
  if (TbOSMonRec.State <> dsInactive) and (TbOSMonRec.RecordCount > 0) and  
    (FDiluente = CO_COD_BUGS_DILUENTE_003) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT IDIts ',
    'FROM osmonrec ',
    'WHERE Conta=' + Geral.FF0(FConta),
    'AND EhDiluente=1 ',
    '']);
    if Dmod.QrAux.Recordcount = 0 then
      Geral.MB_Aviso('Defina o produto diluente!')
    else
      Close;
  end else
    Close;
end;

procedure TFmOSMonRec.TbOSMonRecAfterDelete(DataSet: TDataSet);
begin
  if not FReordenendo then
    ReordenaLinhas;
  AtualizaTbFormulas;
end;

procedure TFmOSMonRec.TbOSMonRecAfterPost(DataSet: TDataSet);
begin
  if not FReordenendo then
    ReordenaLinhas;
  AtualizaTbFormulas;
end;

procedure TFmOSMonRec.TbOSMonRecBeforeInsert(DataSet: TDataSet);
begin
  FOrdem := TbOSMonRecOrdem.Value;
end;

procedure TFmOSMonRec.TbOSMonRecBeforePost(DataSet: TDataSet);
{
var
  Atual: Double;
}
begin
  if TbOSMonRecConta.Value < 0.1 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '');
    TbOSMonRecIDIts.Value :=   UMyMod.BPGS1I32(
      'osmonrec', 'IDIts', '', '', tsDef, stIns, 0);
    TbOSMonRecCodigo.Value := FCodigo;
    TbOSMonRecControle.Value := FControle;
    TbOSMonRecConta.Value := FConta;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrG1Mon, Dmod.MyDB, [
    'SELECT ggx.Controle, ',
    'pap.ValCliDd, pap.AtuNumLot, pap.AtuNumLaud ',
    'FROM grag1prap pap ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pap.Nivel1 ',
    'LEFT JOIN gragrux ggx ON ggx.GraGru1=pap.Nivel1 ',
    'WHERE ggx.Controle=' + Geral.FF0(TbOSMonRecGraGruX.Value),
    '',
    'UNION',
    '',
    'SELECT ggx.Controle, ',
    'pmo.ValCliDd, pmo.AtuNumLot, pmo.AtuNumLaud ',
    'FROM grag1prmo pmo ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pmo.Nivel1 ',
    'LEFT JOIN gragrux ggx ON ggx.GraGru1=pmo.Nivel1 ',
    'WHERE ggx.Controle=' + Geral.FF0(TbOSMonRecGraGruX.Value),
    '']);
    if TbOSMonRecValCliDd.Value = 0 then
      TbOSMonRecValCliDd.Value := QrG1MonValCliDd.Value;
    if TbOSMonRecNumLote.Value = '' then
      TbOSMonRecNumLote.Value := QrG1MonAtuNumLot.Value;
    if TbOSMonRecNumLaudo.Value = '' then
      TbOSMonRecNumLaudo.Value := QrG1MonAtuNumLaud.Value;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TFmOSMonRec.TbOSMonRecNewRecord(DataSet: TDataSet);
begin
  if FOrdem > 0 then
    TbOSMonRecOrdem.Value := FOrdem
  else
    TbOSMonRecOrdem.Value := 1;
end;

end.
