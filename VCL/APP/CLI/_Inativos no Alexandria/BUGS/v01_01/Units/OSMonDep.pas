unit OSMonDep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid;

type
  TFmOSMonDep = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit15: TDBEdit;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    DsOSSrv: TDataSource;
    Timer1: TTimer;
    Qr_OS_Dep_: TmySQLQuery;
    Ds_OS_Dep_: TDataSource;
    AdvGridDep: TDBAdvGrid;
    Qr_OS_Dep_o4Ter: TIntegerField;
    Qr_OS_Dep_c4Ter: TIntegerField;
    Qr_OS_Dep_n4Ter: TWideStringField;
    Qr_OS_Dep_a4Ter: TSmallintField;
    Qr_OS_Dep_o3Loc: TIntegerField;
    Qr_OS_Dep_c3Loc: TIntegerField;
    Qr_OS_Dep_n3Loc: TWideStringField;
    Qr_OS_Dep_a3Loc: TSmallintField;
    Qr_OS_Dep_o2Typ: TIntegerField;
    Qr_OS_Dep_c2Typ: TIntegerField;
    Qr_OS_Dep_n2Typ: TWideStringField;
    Qr_OS_Dep_a2Typ: TSmallintField;
    Qr_OS_Dep_o1Dep: TIntegerField;
    Qr_OS_Dep_c1Dep: TIntegerField;
    Qr_OS_Dep_n1Dep: TWideStringField;
    Qr_OS_Dep_a1Dep: TSmallintField;
    Qr_OS_Dep_Ativo: TSmallintField;
    QrPsq1: TmySQLQuery;
    Qr_OS_Dep_Tabela: TSmallintField;
    QrPsq1o4Ter: TIntegerField;
    QrPsq1c4Ter: TIntegerField;
    QrPsq1n4Ter: TWideStringField;
    QrPsq1a4Ter: TSmallintField;
    QrPsq1o3Loc: TIntegerField;
    QrPsq1c3Loc: TIntegerField;
    QrPsq1n3Loc: TWideStringField;
    QrPsq1a3Loc: TSmallintField;
    QrPsq1o2Typ: TIntegerField;
    QrPsq1c2Typ: TIntegerField;
    QrPsq1n2Typ: TWideStringField;
    QrPsq1a2Typ: TSmallintField;
    QrPsq1o1Dep: TIntegerField;
    QrPsq1c1Dep: TIntegerField;
    QrPsq1n1Dep: TWideStringField;
    QrPsq1a1Dep: TSmallintField;
    QrPsq1Tabela: TSmallintField;
    QrPsq1Ativo: TSmallintField;
    QrOD: TmySQLQuery;
    QrODo4Ter: TIntegerField;
    QrODc4Ter: TIntegerField;
    QrODn4Ter: TWideStringField;
    QrODa4Ter: TSmallintField;
    QrODo3Loc: TIntegerField;
    QrODc3Loc: TIntegerField;
    QrODn3Loc: TWideStringField;
    QrODa3Loc: TSmallintField;
    QrODo2Typ: TIntegerField;
    QrODc2Typ: TIntegerField;
    QrODn2Typ: TWideStringField;
    QrODa2Typ: TSmallintField;
    QrODo1Dep: TIntegerField;
    QrODc1Dep: TIntegerField;
    QrODn1Dep: TWideStringField;
    QrODa1Dep: TSmallintField;
    QrODTabela: TSmallintField;
    QrODAtivo: TSmallintField;
    Qr_OS_Dep_NO_Ativo: TWideStringField;
    QrOSMonCab: TmySQLQuery;
    QrOSMonCabCodigo: TIntegerField;
    QrOSMonCabControle: TIntegerField;
    QrOSMonCabConta: TIntegerField;
    QrOSMonCabNome: TWideStringField;
    QrOSMonCabFormula: TIntegerField;
    QrOSMonCabEquipAplic: TIntegerField;
    QrOSMonCabQtdTot: TFloatField;
    QrOSMonCabQtdQSP: TFloatField;
    QrOSMonCabCusTot: TFloatField;
    QrOSMonCabNO_FORMULA: TWideStringField;
    QrOSMonCabNO_EquipAplic: TWideStringField;
    DsOSMonCab: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AdvGridDepClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure Qr_OS_Dep_AfterOpen(DataSet: TDataSet);
    procedure AdvGridDepClick(Sender: TObject);
  private
    { Private declarations }
    FCriou: Boolean;
    F_OS_Dep_: String;
    //
    procedure Reopen_OS_Desp_(Campos: String; Tabela, Codigo: Integer);

  public
    { Public declarations }
    FQrOSMonDep: TmySQLQuery;
    FSiapTerCad, FEntidade, FCodigo, FControle, FConta: Integer;
  end;

  var
  FmOSMonDep: TFmOSMonDep;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, CreateBugs, ModuleGeral,
  OSUnit, Bugs_Tabs;

{$R *.DFM}

const
  FColsToMerge: array[0..3] of Integer = (4,6,8,10);

procedure TFmOSMonDep.AdvGridDepClick(Sender: TObject);
begin
  MyObjects.UpdMergeDBAvGrid(AdvGridDep, FColsToMerge);
end;

procedure TFmOSMonDep.AdvGridDepClickCell(Sender: TObject; ARow, ACol: Integer);
  function CampoDeNivel(Nivel: Integer; Letra: String): String;
  begin
    case Nivel of
      1: Result := 'Dep';
      2: Result := 'Typ';
      3: Result := 'Loc';
      4: Result := 'Ter';
      else Result := '???';
    end;
    //
    Result := Letra + Geral.FF0(Nivel) + Result;
  end;
const
  MaxNivel = 4;
var
  cN1, Ativo, Nivel, I, Tabela: Integer;
  Campo, Codigo, AtivS, FldAtiv, SQL, _AND_, xFld, nFld: String;
begin
  FldAtiv := TDBAdvGrid(AdvGridDep).Columns[ACol].FieldName;
  if Copy(FldAtiv, 1, 1) = 'a' then
  begin
    Nivel := Geral.IMV(Copy(FldAtiv, 2, 1));
    if Nivel = 0 then
      Exit;
    cN1 := Geral.IMV(TDBAdvGrid(Sender).Cells[2, ARow]);
    Tabela := Geral.IMV(TDBAdvGrid(Sender).Cells[1, ARow]);
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq1, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_OS_Dep_,
    'WHERE c1Dep=' + Geral.FF0(cN1),
    'AND Tabela=' + Geral.FF0(Tabela),
    '']);
    Ativo := QrPsq1.FieldByName(FldAtiv).AsInteger;
    Campo := CampoDeNivel(Nivel, 'c');
    Codigo := Geral.FF0(QrPsq1.FieldByName(Campo).AsInteger);
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    AtivS := Geral.FF0(Ativo);
    //
    SQL := '';
    for I := 1 to Nivel do
      SQL := SQL + ' ' + CampoDeNivel(I, 'a') + '=' + AtivS + ', ';
    SQL := SQL + 'Ativo=' + AtivS;
    //
    _AND_ := '';
    for I := Nivel to MaxNivel do
    begin
      xFld := CampoDeNivel(I, 'c');
      nFld := Geral.FF0(QrPsq1.FieldByName(xFld).AsInteger);
      _AND_ := _AND_ + 'AND ' + xFld + '=' + nFld + #13#10;
    end;
    //
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'UPDATE ' + F_OS_Dep_,
    ' SET ',
    SQL,
    'WHERE Tabela=' + Geral.FF0(Tabela),
    //'AND ' + Campo + '=' + Codigo,
    _AND_ +
    '']);
    //
    Reopen_OS_Desp_('Tabela;c1Dep', Tabela, cN1);
  end;
end;

procedure TFmOSMonDep.BtOKClick(Sender: TObject);
var
  IDIts, Tabela, Cadastro: Integer;
  Codigo, Controle, Conta: String;
begin
  IDIts := 0;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo selecionados');
    //
    Codigo := Geral.FF0(FCodigo);
    Controle := Geral.FF0(FControle);
    Conta := Geral.FF0(FConta);
    //
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'INSERT INTO livre2 ',
    'SELECT "osmondep" Tabela, "IDIts" Campo, ',
    'IDIts Codigo, 1 Vezes, 0 AlterWeb, 0 Ativo ',
    'FROM osmondep ',
    'WHERE Codigo=' + Codigo,
    'AND Controle=' + Controle,
    'AND Conta=' + Conta,
    ';',
    'DELETE FROM osmondep ',
    'WHERE Codigo=' + Codigo,
    'AND Controle=' + Controle,
    'AND Conta=' + Conta,
    '']);
    //
    QrOD.Close;
    UnDmkDAC_PF.AbreMySQLQuery0(QrOD, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_OS_Dep_,
    'WHERE Ativo=1 ',
    'ORDER BY Tabela, o4Ter, n4Ter, ',
    'c4Ter, o3Loc, n3Loc, c3Loc, o2Typ, ',
    'n2Typ, c2Typ, o1Dep, n1Dep, c1Dep ',
    '']);
    QrOD.First;
    while not QrOD.Eof do
    begin
      Tabela         := QrODTabela.Value;
      Cadastro       := QrODc1Dep.Value;
      //
      IDIts := UMyMod.BPGS1I32_Reaproveita('osmondep', 'IDIts', '', '', tsDef, stIns, 0);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osmondep', False, [
      'Codigo', 'Controle', 'Conta', 'Tabela',
      'Cadastro'], [
      'IDIts'], [
      Codigo, Controle, Conta, Tabela,
      Cadastro], [
      IDIts], True);
      //
      QrOD.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    UnOSUnit.ReopenOSMonDep(FQrOSMonDep, FConta, IDIts);
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSMonDep.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSMonDep.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := True;
end;

procedure TFmOSMonDep.FormCreate(Sender: TObject);
begin
  FCriou := False;
end;

procedure TFmOSMonDep.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  //
  MyObjects.UpdMergeDBAvGrid(AdvGridDep, FColsToMerge);
end;

procedure TFmOSMonDep.Qr_OS_Dep_AfterOpen(DataSet: TDataSet);
begin
  MyObjects.UpdMergeDBAvGrid(AdvGridDep, FColsToMerge);
end;

procedure TFmOSMonDep.Reopen_OS_Desp_(Campos: String; Tabela, Codigo: Integer);
begin
  Qr_OS_Dep_.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr_OS_Dep_, DModG.MyPID_DB, [
  'SELECT od.*, ELT(od.Ativo + 1, "N", "S") NO_Ativo ',
  'FROM ' + F_OS_Dep_ + ' od ',
  'ORDER BY od.Tabela, od.o4Ter, od.n4Ter, ',
  'od.c4Ter, od.o3Loc, od.n3Loc, od.c3Loc, od.o2Typ, ',
  'od.n2Typ, od.c2Typ, od.o1Dep, od.n1Dep, od.c1Dep ',
  '']);
  if Trim(Campos) <> '' then
  begin
    Qr_OS_Dep_.Locate(Campos, VarArrayOf([Tabela, Codigo]), [])
  end;
end;

procedure TFmOSMonDep.Timer1Timer(Sender: TObject);
const
  a1Dep = 1;
  Ativo = 1;
var
  c1Dep, Tabela: Integer;
begin
  Timer1.Enabled := False;
  if not FCriou then
  begin
    Screen.Cursor := crHourGlass;
    try
      FCriou := True;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela tempor�ria!');
      F_OS_Dep_ :=
        UnCreateBugs.RecriaTempTableNovo(ntrtt_OS_Dep_, DmodG.QrUpdPID1, False);
      //
      UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
      'DELETE FROM ' + F_OS_Dep_ + ' ; ',
      'INSERT INTO ' + F_OS_Dep_ + ' ',
      'SELECT 0 o4Ter, sic.SiapImaTer c4Ter, sit.Nome n4Ter, 0 a4Ter, ',
      '0 o3Loc, sid.Codigo c3Loc, sic.SCompl2 n3Loc, 0 a3Loc, ',
      '0 o2Typ, det.Codigo c2Typ, det.Nome n2Typ, 0 a2Typ, ',
      '0 o1Dep, sid.Controle c1dep, dep.Nome n1Dep, 0 a1Dep, ',
      Geral.FF0(CO_TABELA_TIPO_LUGAR_001_SIAPIMADEP) + ' Tabela, 0 Ativo ',
      'FROM ' + TMeuDB + '.siapimadep sid ',
      'LEFT JOIN ' + TMeuDB + '.siapimacad sic ON sic.Codigo=sid.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.siaptercad sit ON sit.Codigo=sic.SiapImaTer ',
      'LEFT JOIN ' + TMeuDB + '.dependenci dep ON dep.Codigo=sid.Dependenci ',
      'LEFT JOIN ' + TMeuDB + '.dependtype det ON det.Codigo=dep.DependType ',
      //'WHERE sit.Cliente=' + Geral.FF0(FmOSCab.QrOSCabEntidade.Value),
      'WHERE sit.Codigo=' + Geral.FF0(FSiapTerCad),
      '']);
      //
      UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
      'INSERT INTO ' + F_OS_Dep_ + ' ',
      'SELECT 0 o4Ter, 0 c4Ter, "" n4Ter, 0 a4Ter, ',
      '0 o3Loc, 0 c3Loc, "" n3Loc, 0 a3Loc, ',
      '0 o2Typ, mac.Tipo c2Typ, ELT(mac.Tipo, "M�vel", "Autom�vel", ',
      '"Animal", "Outro", "???") n2Typ, 0 a2Typ, ',
      '0 o1Dep, mac.Codigo c1dep, mac.Nome n1Dep, 0 a1Dep, ',
      Geral.FF0(CO_TABELA_TIPO_LUGAR_002_MOVAMOVCAD) + ' Tabela, 0 Ativo ',
      'FROM ' + TMeuDB + '.movamovcad mac ',
      'WHERE mac.Cliente=' + Geral.FF0(FEntidade),
      '']);
      //
      FQrOSMonDep.First;
      while not FQrOSMonDep.Eof do
      begin
        c1Dep  := FQrOSMonDep.FieldByName('Cadastro').AsInteger;
        Tabela := FQrOSMonDep.FieldByName('Tabela').AsInteger;
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_OS_Dep_, False, [
        'a1Dep', 'Ativo'], [
        'c1Dep', 'Tabela'], [
        a1Dep, Ativo], [
        c1Dep, Tabela], False);
        //
        FQrOSMonDep.Next;
      end;
      //
      Reopen_OS_Desp_('', 0, 0);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
