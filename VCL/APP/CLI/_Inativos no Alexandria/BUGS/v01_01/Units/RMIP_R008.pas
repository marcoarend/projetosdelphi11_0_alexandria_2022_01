unit RMIP_R008;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, mySQLDbTables,
  dmkGeral, UnDmkEnums, Vcl.ComCtrls, Vcl.StdCtrls;

type
  TFmRMIP_R008 = class(TForm)
    Qr008Grupos: TmySQLQuery;
    Qr008GruposGrupo: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
    FEmpresa, FCliente: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    FCLR, FDoLocalize: Boolean;
    FPB1: TProgressBar;
    FLaAviso1, FLaAviso2: TLabel;
    //
    function  GeraImp_ComprovantesDeExecucao(): Boolean;
  end;

var
  FmRMIP_R008: TFmRMIP_R008;

implementation

uses Module, UnMyObjects, DmkDAC_PF, OSImp2, MyDBCheck, RMIP_Cab;

{$R *.dfm}


{ TFmRMIP_R008 }

function TFmRMIP_R008.GeraImp_ComprovantesDeExecucao(): Boolean;
var
  Ini, Fim: String;
  Continua: Boolean;
begin
  Result := True;
  Continua := True;
  Ini := Geral.FDT(FDtaIni, 1);
  Fim := Geral.FDT(FDtaFIm, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr008Grupos, Dmod.MyDB, [
  'SELECT DISTINCT cab.Grupo',
  'FROM oscab cab ',
  'WHERE cab.Empresa=' + Geral.FF0(FEmpresa),
  'AND cab.Entidade=' + Geral.FF0(FCliente),
  'AND cab.DtaExeFim BETWEEN "' + Ini + '" AND "' + Fim + '"',
  'ORDER BY cab.DtaExeFim, cab.Codigo',
  '']);
  //
  FPB1.Max := Qr008Grupos.RecordCount;
  FPB1.Position := 0;
  Qr008Grupos.First;
  while not Qr008Grupos.Eof do
  begin
    MyObjects.Informa2EUpdPB(FPB1, FLaAviso1, FLaAviso2, True,
    'Gerando comprovantes de execu��o das OSs da proforma ' + Geral.FF0(Qr008GruposGrupo.Value));
    if DBCheck.CriaFm(TFmOsImp2, FmOsImp2, afmoNegarComAviso) then
    begin
      //FmOsImp2.EdOSCab.ValueVariant  := QrOSCabCodigo.Value;
      FmOsImp2.FCliente              := 0;//QrOSCabEntidade.Value;
      FmOsImp2.FContratante          := 0;//QrOSCabEntContrat.Value;
      FmOsImp2.FPagante              := 0;//QrOSCabEntPagante.Value;
      //
      FmOsImp2.EdGrupo.ValueVariant  := Qr008GruposGrupo.Value;
      FmOsImp2.EdCliCod.ValueVariant := 0;//QrOSCabEntidade.Value;
      FmOsImp2.EdCliNom.Text         := '';//QrOSCabNO_ENT.Value;
      FmOsImp2.EdTel_ENT.Text        := '';//Geral.FormataTelefone_TT_Curto(QrOSCabTXTTel_ENT.Value);
      FmOsImp2.PageControl1.ActivePageIndex := 6;
      FmOsImp2.CkDoLocalize.Checked  := FDoLocalize;
      FmOsImp2.FDoLocDefined         := True;
      FmOsImp2.FApenasGera           := True;
      //FmOsImp2.ShowModal;
      FmOSImp2.PreparaSelacaoDeOSs(Qr008GruposGrupo.Value);
      if FmOSImp2.GeraImpressao() then
        FmRMIP_Cab.frxComposite(FmOSImp2.frxGER_OSERV_014_006_A, FCLR)
      else
        Continua := False;
      //
      FmOsImp2.Destroy;
      if not Continua then
      begin
        Result := False;
        Exit;
      end;
    end;
    //
    Qr008Grupos.Next;
  end;
end;

//� obrigat�rio a informa��o de pelo menos uma praga alvo de pr�-atendimento na OS!

end.
